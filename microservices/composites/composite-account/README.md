# MS Composite account

Cyberbank StarterKit composite account microservice.

## Code analysis

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=technisys_tec-cyberbank-starterkit-composite-account&token=cb138f0d52573c04e5f52ff515f5d47a6061bbf0)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-account)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-account&metric=bugs&token=cb138f0d52573c04e5f52ff515f5d47a6061bbf0)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-account)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-account&metric=vulnerabilities&token=cb138f0d52573c04e5f52ff515f5d47a6061bbf0)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-account)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-account&metric=code_smells&token=cb138f0d52573c04e5f52ff515f5d47a6061bbf0)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-account)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-account&metric=coverage&token=cb138f0d52573c04e5f52ff515f5d47a6061bbf0)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-account)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-account&metric=duplicated_lines_density&token=cb138f0d52573c04e5f52ff515f5d47a6061bbf0)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-account)

### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"


## Pipelines Status

 CICD     | DEV         | TEST-ORACLE         | DEMO 
----------|-------------|---------------------|---------------
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20Composites/Composite%20Account/Master%20-%20Composite%20Account?repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=1041&repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master) | [![Deploy DEV](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/62/120)](https://dev.azure.com/technisys/TEC%20-%20Digital/_release?view=mine&_a=releases&definitionId=62) | [![Deploy TEST-ORACLE](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/62/247)](https://dev.azure.com/technisys/TEC%20-%20Digital/_release?view=mine&_a=releases&definitionId=62) | [![Deploy DEMO](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/62/122)](https://dev.azure.com/technisys/TEC%20-%20Digital/_release?view=mine&_a=releases&definitionId=62)