# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

## [1.1.2] - 2021-10-21
### Changed
- Update platform version to 1.1.3 [TECDIGSK-679](https://technisys.atlassian.net/browse/TECDIGSK-679)

## [1.1.1] - 2021-10-04

### Changed
- Update platform version to 1.1.2 [TECDIGSK-646](https://technisys.atlassian.net/browse/TECDIGSK-646)

### Added
- Added a method to list the Accounts of a user . [TECHBANK20-1253](https://technisys.atlassian.net/browse/TECHBANK20-1253)

## [1.1.0] - 2021-08-03
### Changed
- Moved to P2 structure

### Fixed
- Fixed some conflicting parameters for SwaggerIU try outs.  [TECDISK-441](https://technisys.atlassian.net/browse/TECDIGSK-441)
- Updated fastrack-server to 3.2.14 final version, and flows to 1.0.11  [TECHBANK20-637](https://technisys.atlassian.net/browse/TECHBANK20-637)

## [1.0.9] - 2021-06-07
### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
### Changed
- Improve API documentation [TECDIGSK-415](https://technisys.atlassian.net/browse/TECDIGSK-415)

## [1.0.8] - 2021-05-05
### Added
- Dummy response added for v2 controller. 2 Dummy services added [TECDIGSK-351](https://technisys.atlassian.net/browse/TECDIGSK-351)
- Dummy response added for v2 controller (Manage Account, Remove Account, Change Alias) . 2 Dummy services added [TECDIGSK-235](https://technisys.atlassian.net/browse/TECHBANK20-235)

## [1.0.7] - 2021-03-31
### Added
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)

### Changed
- Open API v3 standard documentation [TECDIGSK-323](https://technisys.atlassian.net/browse/TECDIGSK-323)
- Updated fastrack-api to 3.2.0 final version [TECDIGSK-316](https://technisys.atlassian.net/browse/TECDIGSK-316)
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-655](https://technisys.atlassian.net/browse/TECENG-655). 
- Update director-core to 4.0.0 [TECDIGSK-272](https://technisys.atlassian.net/browse/TECDIGSK-272)

## [1.0.6] - 2021-01-19
### Changed
- Final version configuration-client-1.0.1 and flows-1.0.6 added [TECDIGSK-264](https://technisys.atlassian.net/browse/TECDIGSK-264)

## [1.0.5] - 2020-12-02
### Fixed
- Update helm values: pullPolicy, docker resgistry  [TECDISK-192](https://technisys.atlassian.net/browse/TECDIGSK-192)
- Helm, update chart & app version [TECDISK-166](https://technisys.atlassian.net/browse/TECDIGSK-166)
### Added
- List statements using nonTransactionalProcessingFlow process. [TECDISK-149](https://technisys.atlassian.net/browse/TECDIGSK-149)

### Changed
- Apply trivy's security container scanning [TECENG-422](https://technisys.atlassian.net/browse/TECENG-422)
- Update version of flow to 1.0.5 and configuration-client to 1.0.0 [TECDIGSK-154](https://technisys.atlassian.net/browse/TECDIGSK-154)
- Changes for MS Config integration on tec_digitl_flows [TECDIGSK-130](https://technisys.atlassian.net/browse/TECDIGSK-130)

## [1.0.4] - 2020-10-07
## [1.0.3] - 2020-10-06
### Changed
- Update digital.starterkit.api.version 3.0.1-SNAPSHOT -> 3.0.1, digital.platform.api.version 3.0.1-SNAPSHOT -> 3.0.1, flows version to 1.0.4 [TECDIGSK-114](https://technisys.atlassian.net/browse/TECDIGSK-114)
- Refactor release pipeline to use profile template [TECDIGSK-99](https://technisys.atlassian.net/browse/TECDIGSK-99)
- Update version of director-spring-boot-parent 3.0.0  [TECDIGSK-89](https://technisys.atlassian.net/browse/TECDIGSK-89)
- ECS Logback Encoder dependency updated to `0.5.1` [TECDIGSK-57](https://technisys.atlassian.net/browse/TECDIGSK-57)
- Added new configuration value to configure forkJoinPool size used by director [TECDIGSK-101](https://technisys.atlassian.net/browse/TECDIGSK-101)
- Update values.yaml for each env

## [1.0.2] - 2020-09-10
### Changed
- Authorization header was removed from controller methods [TECDIGSK-49](https://technisys.atlassian.net/browse/TECDIGSK-49) 
- Refactor CICD pipelines to use profile templates [TECDIGSK-50](https://technisys.atlassian.net/browse/TECDIGSK-50)

### Fixed
- Fixed composite account directory name on master branch trigger [TECDIGSK-26](https://technisys.atlassian.net/browse/TECDIGSK-26)
- Fixed code coverage analyzer not finding the reports folder [TECDIGSK-29](https://technisys.atlassian.net/browse/TECDIGSK-29)

### Security
- Exclude guava dependency to fix security issue [TECDIGSK-86](https://technisys.atlassian.net/browse/TECDIGSK-86).
- Update parent image to fix security issues: RHSA-2020:2969 [TECDIGSK-32](https://technisys.atlassian.net/browse/TECDIGSK-32). 

## [1.0.1] - 2020-08-07
### Added
- Lock image task added to the release pipeline [TECDIGSK-18](https://technisys.atlassian.net/browse/TECDIGSK-18)
- Added new tests [TECDIGSK-4](https://technisys.atlassian.net/browse/TECDIGSK-4)

### Changed
- Source code moved to the new StarterKit repository [TECDIGSK-17](https://technisys.atlassian.net/browse/TECDIGSK-17)
- Build and release pipelines updated [TECDIGSK-17](https://technisys.atlassian.net/browse/TECDIGSK-17)

## [1.0.0] - 2020-07-14
### Added
- First feature in composite account. Account balance using nonTransactionalProcessingFlow process. [TECDIGSK-1](https://technisys.atlassian.net/browse/TECDIGSK-14)
