# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

## [1.0.3] - 2021-10-04
- Update platform version to 1.0.3 [TECDIGSK-648](https://technisys.atlassian.net/browse/TECDIGSK-648)

## [1.0.2] - 2021-07-28
- Separate P2 and P3 [TECDIGSK-1](https://technisys.atlassian.net/browse/TECDIGSK-14)

## [1.0.1] - Not deployed release

## [1.0.0] - 2021-05-10
### Added 
- First feature in composite channel support. [TECDIGSK-1](https://technisys.atlassian.net/browse/TECDIGSK-14)
