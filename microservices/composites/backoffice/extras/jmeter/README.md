# Stress test

You can use the `backoffice.jmx` JMeter script to run the stress test.
This script uses an external groovy file to calculate DWR session information (filename: `dwr_sessionid_generator.java`).

It takes the user list from another file: `users.csv` and a list of transactions file: `transactions.csv`.
The files follow this structure:

# users.csv

```
USERNAME,PASSWORD,CUSTOMER_LIST,ENVIRONMENT_LIST
admin,password,john@technisys.com:Demo|joao@technisys.com:Demo,Antares S.A.:5|Devkoa LLC:12
admin2,password,robert@technisys.com:Demo|joao@technisys.com:Demo,Orion L.T.D.A.:7
....
```

The `users.csv` file must be generated through an utility app because in the database the Last name of the users are encrypted.

# transactions.csv

```
USERNAME,CREATION_DATE
john@technisys.com,19/07/2020 10:59:54|13/05/2020 13:20:00
robert@technisys.com,11/05/2020 09:32:30
....
```

The transactions list can be obtained with this database script (SqlServer):
Warn: Slow query if database is big

```
select U.USERNAME, R.CREATION_DATE from (
    select top 100 id_user_creator, string_agg(format(creation_date_time,'dd/MM/yyyy hh:mm:ss'), '|') as CREATION_DATE
    from (
        select id_user_creator, creation_date_time, row_number() over( partition by id_user_creator order by newid()) as rn
        from transactions
    ) trx
    where rn <= 3
    group by id_user_creator
    order by newid()
) R JOIN users U on (R.id_user_creator = U.id_user)
```
