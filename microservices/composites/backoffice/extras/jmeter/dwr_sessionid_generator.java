def tokenify(long number) {
    def tokenbuf = [];
    def charmap = '1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ*$';
    long remainder = number;
    while (remainder > 0) {
        int index = (remainder & 0x3F);
        tokenbuf.push(charmap[index]);
        remainder = (long) Math.floor(remainder / 64);
    }
    return tokenbuf.join();
}

// Set the Script Session ID Variable
var _pageId = tokenify(new Date().getTime()) + "-" + tokenify((long) (Math.random() * 1E16));
var _scriptSessionId = vars.get('DWR_SESSIONID') + "/" + _pageId;

vars.put('DWR_SCRIPT_SESSIONID', _scriptSessionId);