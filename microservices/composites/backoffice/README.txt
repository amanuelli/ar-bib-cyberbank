Omnichannel
https://www.technisys.com/solutions-benefits/omnichannel/
-------------------------------------------

Important notes:

1. Install the EL replacement libs in your application server
   Follow the instructions in omnichannel/extras/tomcat_libs/install.txt

2. Install JCE Unlimited Strength Jurisdiction Policy Files in your JVM:
   http://www.oracle.com/technetwork/java/javase/downloads/index.html

3. To run the Client in Glassfish 2 it's requiered to delete the bundled jackson
   version (this is used to parse json by the REST webservices implementation).
   Delete the file: <glassfish_base>/lib/jackson-asl-0.9.4.jar