# Default values for backoffice.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

# Backoffice
bo:
  name: backoffice

  labels:
    app: backoffice
    lang: java
    platform: digital

  selectorLabels:
    app: backoffice

  deployment:
    image:
      repository: tecdigitalacr.azurecr.io/techbank/backoffice
      version: #{MS_VERSION}#
      pullPolicy: Always

    replicaCount: 1

    port: 8080
    
    envFrom:
      - configMapRef:
          name: backoffice-config
      - configMapRef:
          name: backoffice-bo-config
      - configMapRef:
          name: backoffice-apm-config
      - secretRef:
          name: backoffice-secrets

    imagePullSecrets:
      - name: regcred

    serviceAccountName: tecdigital-sa

    resources:
      limits:
        cpu: 1250m
        memory: 2000Mi
      requests:
        cpu: 500m
        memory: 512Mi

  service:
    type: ClusterIP
    port: 80
    targetPort: 8080

  configPodReset:
    - secrets.yaml
    - configmap.yaml

  backofficeApmConfig:
    name: backoffice-apm-config
    ELASTIC_APM_SERVER_URLS: "#{apmserver.url}#"
    ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
    ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
    ELASTIC_APM_APPLICATION_PACKAGES: "com.technisys"
    ELASTIC_APM_CENTRAL_CONFIG: "true"
    ELASTIC_APM_SERVICE_NAME: "backoffice"
    ELASTIC_APM_LOG_LEVEL: "INFO"
    ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "false"
    ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: "com.technisys.omnichannel.api.resource.v1.*, com.technisys.omnichannel.activities.*, com.technisys.omnichannel.client.activities.*, com.technisys.omnichannel.Dispatcher"
    ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: "(?-i)java.*, (?-i)javax.*, (?-i)sun.*, (?-i)com.sun.*,
                                                            (?-i)jdk.*, (?-i)org.apache.tomcat.*,
                                                            (?-i)org.apache.catalina.*, (?-i)org.apache.coyote.*,
                                                            (?-i)org.eclipse.jetty.*, (?-i)org.springframework.*"
    ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc, redis, jax-rs, jax-ws"
    ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME: "true"

  backofficeBoConfig:
    name: backoffice-bo-config
    JAVA_OPTS: "-javaagent:/usr/local/tomcat/elastic-apm-agent.jar"

# Backoffice API
boapi:
  name: backoffice-api

  labels:
    app: backoffice-api
    lang: java
    platform: digital

  selectorLabels:
    app: backoffice-api

  deployment:
    image:
      repository: tecdigitalacr.azurecr.io/digital/backoffice-api
      version: #{MS_VERSION}#
      pullPolicy: Always

    replicaCount: 1

    port: 8080

    envFrom:
      - configMapRef:
          name: backoffice-config
      - configMapRef:
          name: backoffice-api-config
      - configMapRef:
          name: backoffice-api-apm-config
      - secretRef:
          name: backoffice-secrets

    livenessProbe:
      httpGet:
        path: /actuator/health
        port: 8080
      initialDelaySeconds: 60
      periodSeconds: 10

    readinessProbe:
      httpGet:
        path: /actuator/health
        port: 8080
      initialDelaySeconds: 60
      periodSeconds: 15

    imagePullSecrets:
      - name: regcred

    serviceAccountName: tecdigital-sa

    resources:
      limits:
        cpu: 500m
        memory: 1024Mi
      requests:
        cpu: 20m
        memory: 512Mi

  service:
    type: ClusterIP
    port: 80
    targetPort: 8080

  configPodReset:
    - configmap.yaml
    - boapi-configmap.yaml
    - secrets.yaml

  backofficeApiApmConfig:
    name: backoffice-api-apm-config
    ELASTIC_APM_SERVER_URLS: "#{apmserver.url}#"
    ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
    ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
    ELASTIC_APM_APPLICATION_PACKAGES: "com.technisys"
    ELASTIC_APM_CENTRAL_CONFIG: "true"
    ELASTIC_APM_SERVICE_NAME: "backoffice-api"
    ELASTIC_APM_LOG_LEVEL: "INFO"
    ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "false"
    ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: "com.technisys.omnichannel.api.resource.v1.*, com.technisys.omnichannel.activities.*, com.technisys.omnichannel.client.activities.*, com.technisys.omnichannel.Dispatcher"
    ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: "(?-i)java.*, (?-i)javax.*, (?-i)sun.*, (?-i)com.sun.*,
                                                            (?-i)jdk.*, (?-i)org.apache.tomcat.*,
                                                            (?-i)org.apache.catalina.*, (?-i)org.apache.coyote.*,
                                                            (?-i)org.eclipse.jetty.*, (?-i)org.springframework.*"
    ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc, redis, jax-rs, jax-ws"
    ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME: "false"

  backofficeApiConfig:
    name: backoffice-api-config
    SERVER_ERROR_WHITELABEL_ENABLED: "false"
    SPRING_MVC_THROW_EXCEPTION_IF_NO_HANDLER_FOUND: "true"
    SPRING_RESOURCES_ADD_MAPPINGS: "false"
    JAVA_TOOL_OPTIONS: "-javaagent:/home/technisys/elastic-apm-agent.jar"


# Common Configuration
configMap:
  name: backoffice-config
  dbserver:
    url: jdbc:sqlserver://tec-digital.database.windows.net;databaseName=tec-digital-test-aks
    driver: com.microsoft.sqlserver.jdbc.SQLServerDriver
  database_poolPingQuery: "SELECT GETDATE()"
  database_maxActiveConnections: "200"
  database_dbms: "mssql"
  database_maxIdleConnections: "20"
  database_poolMaximumCheckoutTime: "30000"
  database_bpm_maxActiveConnections: "20"
  database_bpm_poolMaximumCheckoutTime: "20000"
  database_bpm_maxIdleConnections: "5"
  REDIS_HOST: "redis.digital-test"
  REDIS_PORT: "6379"
  forms_mapper_cache_flushInterval: "0"
  configuration_mapper_cache_flushInterval: "0"
  bo_backofficeDispatcher_componentFQN: "com.technisys.omnichannel.BackofficeDispatcherImpl"
  core_configuration_componentFQN: "com.technisys.omnichannel.core.configuration.ms.ConfigurationMS"
  core_scheduler_componentFQN: "com.technisys.omnichannel.core.scheduler.legacy.SchedulerHandlerLegacy"
  core_transactions_componentFQN: "com.technisys.omnichannel.core.transactions.legacy.TransactionHandlerLegacy"
  AUDIT_URL: "http://audit.digital-test"
  AUDIT_ACTIVE_SIGNED: "false"
  AUDIT_BROKER: "KafkaMQ"
  AUDIT_SPRING_KAFKAMQ_ADDRESS: "tec-confluent-oss-cp-kafka-headless"
  AUDIT_SPRING_KAFKAMQ_CLIENTID: "digital"
  AUDIT_SPRING_KAFKAMQ_PORT: "9092"
  AUDIT_SPRING_KAFKAMQ_SCHEMA_REGISTRY_URL: "http://tec-confluent-oss-cp-schema-registry:8081"
  AUDIT_SPRING_KAFKAMQ_TOPICNAME: queue.audit.logs
  AUDIT_MAXIMUM_NUMBER_THREAD: "8"
  AUDIT_MINIMUM_OBJECT: "4"
  AUDIT_MAXIMUM_OBJECTS: "10"
  AUDIT_VALIDATION_INTERVAL: "5"
  KAFKA_BROKER_URL: "tec-confluent-oss-cp-kafka-headless:9092"
  SCHEMA_REGISTRY_URL: "http://tec-confluent-oss-cp-schema-registry:8081"

# Common Secrets
secrets:
  name: "backoffice-secrets"
  sesUsageKey: "#{ses.usageKey}#"
  dbserver:
    username: "#{database.username}#"
    password: "#{database.password}#"
  rabbitmq:
    username: "#{rabbitmq.username}#"
    password: "#{rabbitmq.password}#"
  elastic:
    apmtoken: "#{elastic.apmtoken}#"
