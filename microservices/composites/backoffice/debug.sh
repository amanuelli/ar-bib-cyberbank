#!/bin/bash

# Prerequisites - Have the deployment running on a kubernetes cluster (can be local or remote)
#
# Script example:
# sh starterkit/debug.sh -module api
# sh starterkit/debug.sh -module backoffice -p 9090:8080

#Parameters load
namespace="digital-local"
while test $# -gt 0; do
           case "$1" in
                --module | -m) #Project module or microservice / K8s Deployment
                    shift
                    if [ -z "$module" ]; then
                      module=$1
                    else
                      echo "Only one deployment per command is allowed. '$1' ignored"
                    fi
                    shift
                    ;;
                 -p) #Ports
                    shift
                    ports+=" -p $1"
                    shift
                    ;;
                -e) #Env
                    shift
                     dockerenvs+=" -e $1"
                    shift
                    ;;
                -n) #Namespace
                    shift
                     namespace="$1"
                    shift
                    ;;
                -v) #Volumes
                    shift
                     dockervols+=" -v $1"
                    shift
                    ;;
                -i)
                    shift
                    if [ -z "$dockerImage" ]; then
                      dockerImage=$1
                    else
                      echo "Only one image can be set. '$1' ignored"
                    fi
                    shift
                    ;;
                -dp) #Debug port. ex: 5006
                    shift
                    if [ -z "$debugPort" ]; then
                      debugPort=$1
                    else
                      echo "Only one debug port can be set. '$1' ignored"
                    fi
                    shift
                    ;;
                *)
                   echo "$1 is not a recognized flag!"
                   shift
                   shift
                   ;;
          esac
done
debugPortDefault="5005"
debugPort=${debugPort:=$debugPortDefault}

#Read from dockerfile the image to use.
dockerImage () {
    dockerFromImage=$(head -n 1 $1/Dockerfile) # Extract first line of dockerfile "FROM <BASE-IMAGE>"
    echo $(echo ${dockerFromImage} | cut -c5-) #Remove "FROM " prefix of line
}

case "$module" in
  backoffice-api)
    imageDefault="$(dockerImage $(pwd)/${module})"
    cmd="java -jar /home/technisys/backoffice-api.jar"
    baseExtraClassPath="/usr/local/tomcat/extra_class_path"
    debugConf="-p ${debugPort}:5005 -e JPDA_ADDRESS=5005 -e HS_AGENT_OPTS=-Dextra.class.path=${baseExtraClassPath}/skit/${module},${baseExtraClassPath}/platform/${module},${baseExtraClassPath}/platform/server"
    defaultPort="-p 8080:8080"
    echo "-v $(pwd)/${module}/target/${module}.jar:/usr/local/app/${module}.jar"
    defaultVolumes="-v $(pwd)/${module}/target/${module}.jar:/home/technisys/${module}.jar"
    ;;
  api | backoffice)
    imageDefault="tecdigitalacr.azurecr.io/digital/tomcat:8.5-jdk8-dev-v2"
    cmd="catalina.sh run"
    baseExtraClassPath="/usr/local/tomcat/extra_class_path"
    debugConf="-p ${debugPort}:5005 -e JPDA_ADDRESS=5005 -e HS_AGENT_OPTS=-Dextra.class.path=${baseExtraClassPath}/skit/${module},${baseExtraClassPath}/platform/${module},${baseExtraClassPath}/platform/server"
    defaultPort="-p 8080:8080" #https://bitbucket.org/manentia/digital-tomcat/src/master/8.5-jdk8/Dockerfile.centos7
    defaultVolumes="-v $(pwd)/${module}/src/main/devops/logback.xml:/usr/local/tomcat/shared/logback.xml \
                    -v $(pwd)/${module}/target/${module}.war:/usr/local/tomcat/webapps/${module}.war \
                    -v $(pwd)/${module}/target/classes:${baseExtraClassPath}/skit/${module} \
                    -v $(pwd)/../platform/${module}/target/classes:${baseExtraClassPath}/platform/${module} \
                    -v $(pwd)/../platform/server/target/classes:${baseExtraClassPath}/platform/server"
    ;;
  das)
    imageDefault="tecdigitalacr.azurecr.io/digital/node8:1.0.0"
    cmd="node --inspect=0.0.0.0:5858 /home/node/app/index.js --exec babel-node -e js"
    debugConf="-p 5858:5858"
    defaultPort="" #https://bitbucket.org/manentia/digital-tomcat/src/master/8.5-jdk8/Dockerfile.centos7
    defaultVolumes="-v $(pwd)/${module}/build/:/home/node/app/ -v $(pwd)/${module}/config/config.js:/home/node/app/config/config.js"
    ;;
  web)
    imageDefault="$(dockerImage $(pwd)/${module})"
    cmd="/usr/local/bin/docker-entrypoint.sh"
    debugConf=""
    defaultPort="-p 8080:8080" #https://bitbucket.org/manentia/digital-nginx/src/master/1.16/Dockerfile.centos7
    defaultVolumes="-v $(pwd)/${module}/build:/data/www"
     ;;
  i18n)
    imageDefault="$(dockerImage $(pwd)/microservices/${module})"
    debugConf="-p ${debugPort}:5005 -e DEBUG_MODE=true"
    defaultPort="-p 8080:8080"
    defaultVolumes="-v $(pwd)/microservices/${module}/messages:/home/technisys/starterkit"
    cmd="cp /home/technisys/starterkit /home/technisys && sh /home/technisys/start.sh"
    ;;
  internalgateway)
    imageDefault="$(dockerImage $(pwd)/microservices/${module})"
    debugConf="-p ${debugPort}:5005 -e DEBUG_MODE=true"
    defaultPort="-p 8080:8080"
    defaultVolumes="-v $(pwd)/microservices/${module}/application.yml:/home/technisys/application.yml"
    cmd="sh /home/technisys/start.sh"
    ;;
  authserver)
    imageDefault="tecdigitalacr.azurecr.io/digital/authserver:latest"
    debugConf="-p ${debugPort}:5005 -e DEBUG_MODE=true"
    defaultPort="-p 8080:8080"
    defaultVolumes="-v $(pwd)/microservices/${module}/target/authserver-ext-custom.jar:/authserver-ext-custom.jar"
    cmd="java -cp authorization-server.jar -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -Djava.security.egd=file:/dev/./urandom -Dloader.path=authserver-ext-custom.jar -Dloader.main=com.technisys.digital.auth.server.boot.AuthorizationServerApplication org.springframework.boot.loader.PropertiesLauncher"
    ;;
  *)
    echo "Not recognized module for defaults loading"
    ;;
esac

dockerImage=${dockerImage:=$imageDefault}
ports=${ports:=$defaultPort}
dockervols=${dockervols:=$defaultVolumes}


errorMsg="value must be set. "
errorDkr="Image FROM not loaded. "
errorVol="Volumes must be set. "

echo "Volumes: $dockervols"
echo "Ports: $ports"
echo "Debug: $debugConf"
echo "dockerImage: $dockerImage"
echo "namespace: $namespace"
echo "ENV: $dockerenvs"
echo "CMD: $cmd"

telepresence --swap-deployment ${module:?$errorMsg} --namespace ${namespace} --docker-run --rm ${dockervols:?$errorVol} ${dockerenvs} ${ports} ${debugConf} ${dockerImage:?$errorDkr} ${cmd}


