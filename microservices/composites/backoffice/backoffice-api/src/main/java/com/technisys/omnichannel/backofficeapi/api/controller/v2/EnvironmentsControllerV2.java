/*
 * Copyright 2021 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.backofficeapi.api.controller.v2;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.backoffice.business.cybo.environments.requests.*;
import com.technisys.omnichannel.backoffice.domain.cybo.Environment;
import com.technisys.omnichannel.backofficeapi.api.controller.BaseController;
import com.technisys.omnichannel.backofficeapi.api.controller.v1.responses.BaseResponse;
import com.technisys.omnichannel.core.TransactionRequest;
import com.technisys.omnichannel.core.exceptions.SessionException;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@RestController
@RequestMapping("v2/environments")
public class EnvironmentsControllerV2 extends BaseController {

    @ApiOperation("Modify general information of the environment identified by id.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Success")
    })
    @PutMapping(value = "{id}/general")
    public ResponseEntity<BaseResponse<Environment>> modifyGeneralInfo(
            @ApiParam(value = "Environment id", example = "1") @PathVariable int id,
            @ApiParam("Environment info") @RequestBody Map<String, Object> parameters) throws SessionException {

        TransactionRequest request = new TransactionRequest();
        prepareRequestWithAccessToken(request, "cybo.environments.general.modify");

        ModifyGeneralData data = new ModifyGeneralData();

        data.setId(id);
        data.setName(RequestParamsUtils.getValue(parameters, "name", String.class));
        data.setType(RequestParamsUtils.getValue(parameters, "type", String.class));
        data.setScheme(RequestParamsUtils.getValue(parameters, "administrationSchema", String.class));
        data.setHoldingClientsToAdd(RequestParamsUtils.getValue(parameters, "workspacesToAdd", String.class));
        data.setHoldingClientsToRemove(RequestParamsUtils.getValue(parameters, "workspacesToRemove", String.class));

        data.setComment(RequestParamsUtils.getValue(parameters, "comments", String.class));

        request.setTransactionData(data);

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

}
