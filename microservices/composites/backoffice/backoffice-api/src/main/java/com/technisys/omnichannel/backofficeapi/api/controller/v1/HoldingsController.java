/*
 * Copyright 2020 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.backofficeapi.api.controller.v1;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.backoffice.business.cybo.environments.holding.requests.ReadAdminData;
import com.technisys.omnichannel.backoffice.business.cybo.environments.holding.requests.CreateHoldingData;
import com.technisys.omnichannel.backoffice.business.cybo.environments.holding.requests.ReadClientRequest;
import com.technisys.omnichannel.backofficeapi.api.controller.BaseController;
import com.technisys.omnichannel.backofficeapi.api.controller.v1.responses.BaseResponse;
import com.technisys.omnichannel.core.TransactionRequest;
import com.technisys.omnichannel.core.exceptions.SessionException;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@RequestMapping("v1/environments/holdings")
public class HoldingsController extends BaseController {

    @ApiOperation(value = "Create a new holding")
    @ApiResponses({
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 202, message = "Accepted"),
            @ApiResponse(code = 400, message = "Bad request")})
    @PostMapping(path = "")
    public ResponseEntity<BaseResponse<Object>> createHolding(
            @ApiParam("Holding's parameters") @RequestBody Map<String, Object> parameters) throws SessionException {
        TransactionRequest request = new TransactionRequest();

        prepareRequestWithAccessToken(request, "cybo.environments.holdings.create");

        CreateHoldingData data = new CreateHoldingData();

        data.setAdminLanguage(RequestParamsUtils.getValue(parameters, "adminAdminLanguage", String.class));
        data.setAdministrationScheme(RequestParamsUtils.getValue(parameters, "adminAdministrationScheme", String.class));
        data.setHoldingName(RequestParamsUtils.getValue(parameters, "holdingName", String.class));
        data.setHoldingMembers(RequestParamsUtils.getValue(parameters, "holdingMembers", String.class));
        data.setSendAdminInvite(RequestParamsUtils.getValue(parameters, "adminSendAdminInvite", Boolean.class));
        data.setComment(RequestParamsUtils.getValue(parameters, "comments", String.class));

        Map<String, Object> userParameters = RequestParamsUtils.getValue(parameters, "user", Map.class);
        if (userParameters != null && !userParameters.isEmpty()) {
            data.setAdminDocumentCountry(RequestParamsUtils.getValue(userParameters, "documentCountry", String.class));
            data.setAdminDocumentType(RequestParamsUtils.getValue(userParameters, "documentType", String.class));
            data.setAdminDocumentNumber(RequestParamsUtils.getValue(userParameters, "documentNumber", String.class));

            Map<String, Object> userMap = RequestParamsUtils.getValue(userParameters, "user", Map.class);
            data.setAdminEmail(RequestParamsUtils.getValue(userMap, "email", String.class));
            data.setAdminFirstName(RequestParamsUtils.getValue(userMap, "firstName", String.class));
            data.setAdminLastName(RequestParamsUtils.getValue(userMap, "lastName", String.class));
            data.setAdminAccessType(RequestParamsUtils.getValue(userMap, "accessType", String.class));
            data.setAdminMobileNumber(RequestParamsUtils.getValue(userMap, "mobileNumber", String.class));

        }

        request.setTransactionData(data);

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = "Returns the information of a user")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = EnvironmentsController.EnvironmentListResponse.class),
            @ApiResponse(code = 400, message = "Bad Request")
    })
    @GetMapping(value = "/users/find")
    public ResponseEntity<BaseResponse<Object>> findUser(
            @ApiParam("Document country") @RequestParam(name = "documentCountry", required = false) String documentCountry,
            @ApiParam("Document type") @RequestParam(name = "documentType", required = false) String documentType,
            @ApiParam("Document number") @RequestParam(name = "documentNumber", required = false) String documentNumber)
            throws SessionException {

        ReadAdminData request = new ReadAdminData();

        prepareRequestWithAccessToken(request, "cybo.environments.holdings.users.find");

        request.setAdminDocumentCountry(documentCountry);
        request.setAdminDocumentType(documentType);
        request.setAdminDocumentNumber(documentNumber);

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = "Read a client env")
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success"),
            @ApiResponse(code = 400, message = "Bad Request")
    })
    @GetMapping(path = "/client/read")
    public ResponseEntity<BaseResponse<Object>> readClient(
            @ApiParam("idClient") @RequestParam(name = "idClient", required = true) String idClient)
            throws SessionException {

        ReadClientRequest request = new ReadClientRequest();

        prepareRequestWithAccessToken(request, "cybo.environments.holdings.readClient");

        request.setIdClient(idClient);

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }


}
