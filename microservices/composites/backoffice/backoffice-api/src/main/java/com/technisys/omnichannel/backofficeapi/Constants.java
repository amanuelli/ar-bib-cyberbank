package com.technisys.omnichannel.backofficeapi;

public class Constants {

    private Constants() {
    }

    public static final String BASE_PACKAGE = "com.technisys.omnichannel.backofficeapi";

}
