package com.technisys.omnichannel.backofficeapi.api.config;

import com.technisys.omnichannel.backofficeapi.Constants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

import springfox.bean.validators.configuration.BeanValidatorPluginsConfiguration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
@Import(BeanValidatorPluginsConfiguration.class)
public class SwaggerConfig {  

    @Value("${microservice.version}")
    private String microserviceVersion;

    @Autowired
    private ApiDocProperties apiDocProps;
                     
    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
          .groupName(apiDocProps.getGroup())
          .select()
          .apis(RequestHandlerSelectors.basePackage(Constants.BASE_PACKAGE+".api.controller"))
          .paths(PathSelectors.regex("/.*"))
          .build().apiInfo(apiEndPointsInfo());
    }

    private ApiInfo apiEndPointsInfo() {
        return new ApiInfoBuilder().title(apiDocProps.getTitle())
                .description(apiDocProps.getDescription())
                .contact(new Contact(apiDocProps.getName(), apiDocProps.getUrl(), apiDocProps.getEmail()))
                .version(microserviceVersion)
                .build();
    }
}