package com.technisys.omnichannel.backofficeapi.api.controller.v1.swagger;

import com.technisys.omnichannel.backofficeapi.api.controller.v1.responses.BaseResponse;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.InvitationCodesStatus;

import java.util.List;
import java.util.Map;

public class InvitationCodesDocs {
    public static class ListInvitationCodeStatuses {
        public static final String VALUE = "Returns a list of statuses";
        public static final String NOTES = "## Returns a list of statuses for Invitation Codes \n" +
                "Activity: cybo.invitationcodes.listStatuses";
    }

    public static class ListInvitationCodeSchemas {
        public static final String VALUE = "Returns a list of schemas";
        public static final String NOTES = "## Returns a list of schemas for Invitation Codes \n" +
                "Activity: cybo.invitationcodes.listSchemas";
    }

    public static class ReadInvitationCode {
        public static final String VALUE = "Returns the invitation code information identified by id.";
        public static final String NOTES = "## Read a Invitation Code \n" +
                "### This endpoint retrieves the invitation code information identified by the passed id.\n\n" +
                "#### Parameters\n" +
                "   * id: (String) Represents the invitation code unique identification.\n" +
                "\n" +
                "Activity: cybo.invitationcodes.read \n";
    }

    public static class CancelInvitationCode {
        public static final String VALUE = "Cancel the invitation code information identified by id.";
        public static final String NOTES = "## Cancel a Invitation Code \n" +
                "### This endpoint cancels the invitation code for the identified by the passed id.\n\n" +
                "#### Parameters\n" +
                "   *   id: (String) Represents the invitation code unique identification.\n" +
                "   *   documentCountry: Document country of the customer \n" +
                "   *   documentType: Document type of the customer \n" +
                "   *   documentNumber: Document number of the customer \n" +
                "   *   name: Customer full name \n" +
                "   *   account: Account ID \n" +
                "   *   accountName: Account name \n" +
                "\n" +
                "Activity: cybo.invitationcodes.cancel \n";
    }

    public static class ResendInvitationCode {
        public static final String VALUE = "Send a new invitation code for the code identified by id.";
        public static final String NOTES = "## Send a new Invitation Code \n" +
                "### This endpoint sends a new invitation code for the code identified by the passed id.\n\n" +
                "#### Parameters\n" +
                "   *   id: (String) Represents the invitation code unique identification.\n" +
                "   *   documentCountry: Document country of the customer \n" +
                "   *   documentType: Document type of the customer \n" +
                "   *   documentNumber: Document number of the customer \n" +
                "   *   name: Customer full name \n" +
                "   *   account: Account ID \n" +
                "   *   accountName: Account name \n" +
                "\n" +
                "Activity: cybo.invitationcodes.resend \n";
    }

    public class InvitationCodeStatusesListResponse extends BaseResponse<List<InvitationCodesStatus>> {}

    public class InvitationCodeSchemasListResponse extends BaseResponse<List<String>> {}
    
    public class InvitationCodeCreateResponse extends BaseResponse<String> {}

    public class InvitationCodeSearchUserResponse extends BaseResponse<Map<String, Object>> {}

    public class InvitationCodeListResponse extends BaseResponse<List<InvitationCode>> {}

    public class InvitationCodeReadResponse extends BaseResponse<List<InvitationCodesStatus>> {}

    public static class OrderBy {

        public static final String VALUE = "Field used to order the list";
        public static final String ALLOWABLE_VALUES = "creation_date";
    }

    public static class Export {

        public static final String VALUE = "Returns a file content in Base64 with exported invitation codes records information";
        public static final String NOTES = "## Export Invitation codes list \n"
                + "### Returns a file content in Base64 with exported invitation codes records information\n"
                + "\n"
                + "Activity: cybo.invitationcodes.export \n";
    }

    public static class Create {

        public static final String VALUE = "Generate and Send an invitation code";
        public static final String NOTES = "## Creates a new invitation code \n" +
                "### Generate and Send an invitation code \n\n" +
                "Fields to provide in the body parameter: \n" +
                "   *   documentCountry: Document country of the customer \n" +
                "   *   documentType: Document type of the customer \n" +
                "   *   documentNumber: Document number of the customer \n" +
                "   *   name: Customer full name \n" +
                "   *   language: Customer default language \n" +
                "   *   accessType: Type of access to grant to the user \n" +
                "   *   account: Account ID \n" +
                "   *   accountName: Account name \n" +
                "   *   schema: Workspace schema type (Required if account not registered in backoffice) \n" +
                "   *   signatureLevel: Workspace signature level (Required if account not registered in backoffice).\n" +
                "   *   comments: string that represents the comments explaining why the workspaces are being recovered.\n" +
                "\n" +
                "Activity: cybo.invitationcodes.create \n";
    }

    public static class SearchUser {

        public static final String VALUE = "Returns customer accounts information";
        public static final String NOTES = "## Search customer accounts information \n"
                + "### Returns the client information from core and backoffice records and their account list in the bank \n"
                + "Activity: cybo.invitationcodes.readUser \n";
    }
}
