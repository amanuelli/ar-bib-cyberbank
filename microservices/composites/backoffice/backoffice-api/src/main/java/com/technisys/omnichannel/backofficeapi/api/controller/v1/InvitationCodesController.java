package com.technisys.omnichannel.backofficeapi.api.controller.v1;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.backoffice.business.cybo.ExportRequest;
import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.CancelInvitationCodeData;
import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.CreateInvitationCodeData;
import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.ListRequest;
import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.ReadRequest;
import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.ReadUserRequest;
import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.ResendInvitationCodeData;
import com.technisys.omnichannel.backofficeapi.api.controller.BaseController;
import com.technisys.omnichannel.backofficeapi.api.controller.utils.ParamsUtil;
import com.technisys.omnichannel.backofficeapi.api.controller.v1.responses.BaseResponse;
import com.technisys.omnichannel.backofficeapi.api.controller.v1.swagger.CommonDocs;
import com.technisys.omnichannel.backofficeapi.api.controller.v1.swagger.EnvironmentUserDocs.DocumentCountry;
import com.technisys.omnichannel.backofficeapi.api.controller.v1.swagger.EnvironmentUserDocs.DocumentType;
import com.technisys.omnichannel.backofficeapi.api.controller.v1.swagger.EnvironmentUserDocs.DocumentNumber;
import com.technisys.omnichannel.backofficeapi.api.controller.v1.swagger.InvitationCodesDocs.*;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.TransactionRequest;
import com.technisys.omnichannel.core.domain.InvitationCodesStatus;
import com.technisys.omnichannel.core.exceptions.SessionException;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Pattern;
import java.io.IOException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@RestController
@Api(tags = "Invitation Codes", description = " ")
@RequestMapping("v1/invitationcodes")
public class InvitationCodesController extends BaseController {

    @ApiOperation(value = "Returns a list of Invitation Code records", notes = "Endpoint with pagination and search capabilities.")
    @ApiResponses({
        @ApiResponse(code = 200, message = "Success", response = InvitationCodeListResponse.class),
        @ApiResponse(code = 400, message = "Bad Request")
    })
    @GetMapping(value = "")
    public ResponseEntity<BaseResponse<Object>> list(
            @ApiParam(value = CommonDocs.Page.VALUE, example = CommonDocs.Page.EXAMPLE) @RequestParam(name = "page", required = false) Optional<Integer> page,
            @ApiParam(value = OrderBy.VALUE, allowableValues = OrderBy.ALLOWABLE_VALUES, required = false) @RequestParam(name = "orderBy") Optional<String> orderBy,
            @ApiParam(value = CommonDocs.OrderDir.VALUE, allowableValues = CommonDocs.OrderDir.ALLOWABLE_VALUES) @Pattern(regexp = "(asc|desc)") @RequestParam(name = "orderDir", required = false) Optional<String> orderDir,
            @ApiParam("Json with attribute-value to filter (Ex: {\"id\":\"XXX\",\"status\":\"expired\"})") @RequestParam(name = "filters", required = false) Optional<String> filters)
            throws SessionException, IOException {
        ListRequest request = new ListRequest();
        prepareRequestWithAccessToken(request, "cybo.invitationcodes.list");
        setPaginableData(request, page, orderBy, orderDir);

        Map<String, Object> filtersMap = ParamsUtil.getParsedFilters(filters);

        request.setCreationDateFrom(RequestParamsUtils.getValue(filtersMap, "creationDateFrom", Date.class));
        request.setCreationDateTo(RequestParamsUtils.getValue(filtersMap, "creationDateTo", Date.class));
        request.setProductGroupId(RequestParamsUtils.getValue(filtersMap, "productGroupId", String.class));
        request.setEmail(RequestParamsUtils.getValue(filtersMap, "email", String.class));
        request.setDocumentNumber(RequestParamsUtils.getValue(filtersMap, "documentNumber", String.class));
        request.setStatus(RequestParamsUtils.getValue(filtersMap, "status", String.class));
        request.setMobileNumber(RequestParamsUtils.getValue(filtersMap, "mobileNumber", String.class));
        request.setProductGroupName(RequestParamsUtils.getValue(filtersMap, "productGroupName", String.class));

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = Create.VALUE, notes = Create.NOTES)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = InvitationCodeCreateResponse.class)
    })
    @PostMapping(path = "")
    public ResponseEntity<BaseResponse<String>> createInvitation(
            @ApiParam("Invitation code parameters") @RequestBody Map<String, Object> parameters) throws SessionException {
        TransactionRequest request = new TransactionRequest();

        prepareRequestWithAccessToken(request, "cybo.invitationcodes.create");

        CreateInvitationCodeData data = new CreateInvitationCodeData();

        data.setDocumentCountry(RequestParamsUtils.getValue(parameters, "documentCountry", String.class));
        data.setDocumentType(RequestParamsUtils.getValue(parameters, "documentType", String.class));
        data.setDocumentNumber(RequestParamsUtils.getValue(parameters, "documentNumber", String.class));
        
        data.setFullName(RequestParamsUtils.getValue(parameters, "name", String.class));
        data.setUserLang(RequestParamsUtils.getValue(parameters, "language", String.class));
        data.setAccessType(RequestParamsUtils.getValue(parameters, "accessType", String.class));
        data.setAccount(RequestParamsUtils.getValue(parameters, "account", String.class));
        data.setAccountName(RequestParamsUtils.getValue(parameters, "accountName", String.class));
        data.setAdministrationScheme(RequestParamsUtils.getValue(parameters, "schema", String.class));
        data.setSignatureQty(RequestParamsUtils.getValue(parameters, "signatureLevel", Integer.class));

        data.setComment(RequestParamsUtils.getValue(parameters, "comments", String.class));

        request.setTransactionData(data);

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = CancelInvitationCode.VALUE, notes = CancelInvitationCode.NOTES)
    @ApiResponses({
        @ApiResponse(code = 200, message = "Success", response = BaseResponse.class)
    })
    @PatchMapping(path = "/{id}/cancel")
    public ResponseEntity<BaseResponse<Object>> cancelInvitationCode(
            @ApiParam(value = "Invitation code ID", example = "1") @PathVariable int id,
            @ApiParam("Parameter that should at least contain a variable called 'offline'.") @RequestBody Map<String, Object> parameters) throws SessionException {
        TransactionRequest request = new TransactionRequest();
        prepareRequestWithAccessToken(request, "cybo.invitationcodes.cancel");
        
        CancelInvitationCodeData data = new CancelInvitationCodeData();
        data.setIdCode(id);
        data.setDocumentCountry(RequestParamsUtils.getValue(parameters, "documentCountry", String.class));
        data.setDocumentType(RequestParamsUtils.getValue(parameters, "documentType", String.class));
        data.setDocumentNumber(RequestParamsUtils.getValue(parameters, "documentNumber", String.class));
        data.setName(RequestParamsUtils.getValue(parameters, "name", String.class));
        data.setAccount(RequestParamsUtils.getValue(parameters, "account", String.class));
        data.setAccountName(RequestParamsUtils.getValue(parameters, "accountName", String.class));
        data.setComment(RequestParamsUtils.getValue(parameters, "comments", String.class));
        
        request.setTransactionData(data);

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = ResendInvitationCode.VALUE, notes = ResendInvitationCode.NOTES)
    @ApiResponses({
        @ApiResponse(code = 200, message = "Success", response = BaseResponse.class)
    })
    @PostMapping(path = "/{id}/resend")
    public ResponseEntity<BaseResponse<Object>> resendInvitationCode(
            @ApiParam(value = "Invitation code ID", example = "1") @PathVariable int id,
            @ApiParam("Parameter that should at least contain a variable called 'offline'.") @RequestBody Map<String, Object> parameters) throws SessionException {
        TransactionRequest request = new TransactionRequest();
        prepareRequestWithAccessToken(request, "cybo.invitationcodes.resend");
        
        ResendInvitationCodeData data = new ResendInvitationCodeData();
        data.setIdCode(id);
        data.setDocumentCountry(RequestParamsUtils.getValue(parameters, "documentCountry", String.class));
        data.setDocumentType(RequestParamsUtils.getValue(parameters, "documentType", String.class));
        data.setDocumentNumber(RequestParamsUtils.getValue(parameters, "documentNumber", String.class));
        data.setName(RequestParamsUtils.getValue(parameters, "name", String.class));
        data.setAccount(RequestParamsUtils.getValue(parameters, "account", String.class));
        data.setAccountName(RequestParamsUtils.getValue(parameters, "accountName", String.class));
        data.setComment(RequestParamsUtils.getValue(parameters, "comments", String.class));
        
        request.setTransactionData(data);

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = ReadInvitationCode.VALUE, notes = ReadInvitationCode.NOTES)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = InvitationCodeReadResponse.class)
    })
    @GetMapping(path = "/{id}")
    public ResponseEntity<BaseResponse<List<InvitationCodesStatus>>> readInvitationCode(
            @ApiParam(value = "Invitation code ID", example = "1") @PathVariable int id) throws SessionException {
        ReadRequest request = new ReadRequest();
        prepareRequestWithAccessToken(request, "cybo.invitationcodes.read");

        request.setInvitationCodeID(id);

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = ListInvitationCodeStatuses.VALUE, notes = ListInvitationCodeStatuses.NOTES)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = InvitationCodeStatusesListResponse.class)
    })
    @GetMapping(path = "/statuses")
    public ResponseEntity<BaseResponse<List<InvitationCodesStatus>>> listStatuses() throws SessionException {
        IBRequest request = new IBRequest();
        prepareRequestWithAccessToken(request, "cybo.invitationcodes.listStatuses");

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = Export.VALUE, notes = Export.NOTES)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = CommonDocs.ExportResponse.class),
            @ApiResponse(code = 400, message = "Bad Request", response = BaseResponse.class)
    })
    @GetMapping(value = "/export")
    public ResponseEntity<BaseResponse<Object>> exportInvitationCodesRecords(
            @ApiParam(value = CommonDocs.ExportFormat.VALUE, required = false) @RequestParam(name = "format") String format,
            @ApiParam(value = OrderBy.VALUE, allowableValues = OrderBy.ALLOWABLE_VALUES, required = false) @RequestParam(name = "orderBy") String orderBy,
            @ApiParam(value = CommonDocs.OrderDir.VALUE, allowableValues = CommonDocs.OrderDir.ALLOWABLE_VALUES, required = false) @RequestParam(name = "orderDir") String orderDir,
            @ApiParam(value = CommonDocs.Filters.VALUE, required = false) @RequestParam(name = "filters") String filters) throws SessionException {

        ExportRequest request = new ExportRequest(format, orderBy, orderDir, filters);
        prepareRequestWithAccessToken(request, "cybo.invitationcodes.export");

        return this.buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = ListInvitationCodeSchemas.VALUE, notes = ListInvitationCodeSchemas.NOTES)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = InvitationCodeSchemasListResponse.class)
    })
    @GetMapping(path = "/schemas")
    public ResponseEntity<BaseResponse<List<String>>> listSchemas() throws SessionException {
        IBRequest request = new IBRequest();
        prepareRequestWithAccessToken(request, "cybo.invitationcodes.listSchemas");

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }

    @ApiOperation(value = SearchUser.VALUE, notes = SearchUser.NOTES)
    @ApiResponses({
            @ApiResponse(code = 200, message = "Success", response = InvitationCodeSearchUserResponse.class)
    })
    @GetMapping(value = "/search-user")
    public ResponseEntity<BaseResponse<Object>> searchUser(
            @ApiParam(value = DocumentCountry.VALUE, required = true) @RequestParam(name = "documentCountry") Optional<String> documentCountry,
            @ApiParam(value = DocumentType.VALUE, required = true) @RequestParam(name = "documentType") Optional<String> documentType,
            @ApiParam(value = DocumentNumber.VALUE, required = true) @RequestParam(name = "documentNumber") Optional<String> documentNumber)
            throws SessionException {

        ReadUserRequest request = new ReadUserRequest();

        prepareRequestWithAccessToken(request, "cybo.invitationcodes.readUser");

        request.setDocumentCountry(documentCountry.orElse(null));
        request.setDocumentType(documentType.orElse(null));
        request.setDocumentNumber(documentNumber.orElse(null));

        return buildResponse(BackofficeDispatcher.getInstance().execute(request));
    }
}
