package com.technisys.omnichannel.backofficeapi;

import org.springframework.boot.SpringApplication;

public class StartApplication {

    public static void main(String[] args) {
        SpringApplication.run(BackofficeApiApplication.class);
    }
}
