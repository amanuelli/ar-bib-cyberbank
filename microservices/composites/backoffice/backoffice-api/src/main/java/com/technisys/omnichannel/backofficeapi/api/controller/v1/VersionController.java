package com.technisys.omnichannel.backofficeapi.api.controller.v1;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.backofficeapi.api.controller.BaseController;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

/**
 * VersionController
 */
@Configuration
@RestController
@RequestMapping("/v1/version")
public class VersionController extends BaseController {

    @ApiOperation(value = "Returns the version")
    @ApiResponses({ @ApiResponse(code = 200, message = "Success") })
    @GetMapping(path = "")
    public String getVersion() {
        return BackofficeDispatcher.getInstance().getClientVersion();
    }
}