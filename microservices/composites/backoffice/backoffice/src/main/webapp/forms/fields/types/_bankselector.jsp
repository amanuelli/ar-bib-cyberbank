<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="cf" %>

<%--
    El contenido del div de clase "noLangDependant" contiene los campos que no dependen de lenguaje. Ingresar en esta seccion todos los 
    campos que vayan a desplegarse luego de los datos no dependientes del tipo de campo.

    El contenido de cada div de clase "langDependant" contiene los campos que se desplegaran en cada seccion del formulario especifica para 
    el lenguaje definido por el atributo "lang" del div
--%>
<div class="noLangDependant">
    <p id="p_field_displayType" class="typeDependant">
        <label for="displayType"><i18n:message key="backoffice.forms.fields.bankselector.displayType" /><span class="mandatory_field">*</span></label>
        <select id="displayType" name="displayType">
            <option value=""></option>
            <c:forEach items="${displayTypes}" var="displayType">
                <option value="${displayType}" <c:if test="${field.displayType == displayType}">selected="true"</c:if>><i18n:message key="backoffice.forms.fields.bankselector.displayType.${displayType}" /></option>
            </c:forEach>
        </select>
        <span class="field_error_message" id="error_msg_displayType"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.bankselector.displayType.hint" /></span>
    </p>

    <div id="p_field_codeTypeList" class="typeDependant foreigners" style="display: none">
        <label for="codeTypeList"><i18n:message key="backoffice.forms.fields.bankselector.codeTypeList" /></label>
        <select id="codeTypeList" multiple="multiple" name="codeTypeList" title="<i18n:message key="backoffice.forms.fields.bankselector.codeTypeList.hint" />">
            <c:forEach items="${codeTypeList}" var="codeType">
                <option value="${codeType}" <c:if test="${cf:contains(field.codeTypeList, codeType)}">selected="selected"</c:if>><i18n:message key="bankselector.codeType.${codeType}" /></option>
            </c:forEach>
        </select>
        <span class="field_error_message" id="error_msg_codeTypeList"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.bankselector.codeTypeList.hint" /></span>
    </div>
</div>

<c:forEach items="${languages}" var="language" varStatus="status">
    <div class="langDependant" lang="${language}" style="display: none;">
        <p id="p_field_placeholder${language}" class="typeDependant">
            <label for="placeholder${language}"><i18n:message key="backoffice.forms.fields.bankselector.placeholder" /></label>
            <input type="text" id="placeholder${language}" name="placeholder${language}" value="${field.placeholderMap[language]}" />
            <span class="field_error_message" id="error_msg_placeholder${language}"></span>
        </p>
        <p id="p_field_requiredError${language}" class="typeDependant">
            <label for="requiredError${language}"><i18n:message key="backoffice.forms.fields.bankselector.requiredError" /></label>
            <input type="text" id="requiredError${language}" name="requiredError${language}" value="${field.requiredErrorMap[language]}" />
            <span class="field_error_message" id="error_msg_requiredError${language}"></span>
        </p>
        <p id="p_field_invalidError${language}" class="typeDependant">
            <label for="invalidError${language}"><i18n:message key="backoffice.forms.fields.bankselector.invalidError" /></label>
            <input type="text" id="invalidError${language}" name="invalidError${language}" value="${field.invalidErrorMap[language]}" />
            <span class="field_error_message" id="error_msg_invalidError${language}"></span>
        </p>
    </div>
</c:forEach>
        
<script type="text/javascript">
    <%-- Metodo luego de procesada la carga de esta seccion. Hacer en este metodo los cambios que se precisen
    en datos generales del campo --%>
    function afterLoading(){
        clearASM('p_field_codeTypeList');
        
        $("#codeTypeList").asmSelect({
            addItemTarget: 'bottom',
            animate: true,
            highlight: true,
            highlightAddedLabel: '<i18n:message key="backoffice.label.added"/>: ',
            highlightRemovedLabel: '<i18n:message key="backoffice.label.removed"/>: '
        });
        
        $('#subType').change(function(){
            var subType = $('#subType').val();
            if (subType === 'foreigners'){
                $('.foreigners').show();
            } else {
                $('.foreigners').hide();
            }
        });
        $('#subType').trigger('change');
    }
    
</script>