<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="cf" %>
<%--
    El contenido del div de clase "noLangDependant" contiene los campos que no dependen de lenguaje. Ingresar en esta seccion todos los 
    campos que vayan a desplegarse luego de los datos no dependientes del tipo de campo.

    El contenido de cada div de clase "langDependant" contiene los campos que se desplegaran en cada seccion del formulario especifica para 
    el lenguaje definido por el atributo "lang" del div
--%>
<div class="noLangDependant">
    <div id="p_field_maxFileSizeMB" class="typeDependant">
        <label for="maxFileSizeMB"><i18n:message key="backoffice.forms.fields.inputfile.maxFileSizeMB" /><span class="mandatory_field">*</span></label>
        <input type="text" id="maxFileSizeMB" name="maxFileSizeMB" value="${field.maxFileSizeMB}" class="integerField" maxlength="4"/>
        <span class="field_error_message" id="error_msg_maxFileSizeMB"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.inputfile.maxFileSizeMB.hint" /></span>
    </div>
        
    <p id="p_field_allowMultiple" class="typeDependant">
        <label for="allowMultiple"><i18n:message key="backoffice.forms.fields.allowMultiple" /></label>
        <select id="allowMultiple" name="allowMultiple" class="short_field">
            <option value="false" <c:if test="${!field.allowMultiple}">selected="true"</c:if>><i18n:message key="backoffice.no" /></option>
            <option value="true" <c:if test="${field.allowMultiple}">selected="true"</c:if>><i18n:message key="backoffice.yes" /></option>
        </select>
        <span class="field_error_message" id="error_msg_allowMultiple"></span>
    </p>
    
    <p id="p_field_maxTotalFileSizeMB" class="typeDependant" style="display: none;">
        <label for="maxTotalFileSizeMB"><i18n:message key="backoffice.forms.fields.inputfile.maxTotalFileSizeMB" /><span class="mandatory_field">*</span></label>
        <input type="text" id="maxTotalFileSizeMB" name="maxTotalFileSizeMB" value="${field.maxTotalFileSizeMB}" class="integerField" maxlength="4"/>
        <span class="field_error_message" id="error_msg_maxTotalFileSizeMB"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.inputfile.maxTotalFileSizeMB.hint" /></span>
    </p>
    
    <p id="p_field_maxFiles" class="typeDependant" style="display: none;">
        <label for="maxFiles"><i18n:message key="backoffice.forms.fields.inputfile.maxFiles" /></label>
        <input type="text" id="maxFiles" name="maxFiles" value="${field.maxFiles}" class="integerField" maxlength="4"/>
        <span class="field_error_message" id="error_msg_maxFiles"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.inputfile.maxFiles.hint" /></span>
    </p>
    
    <div id="p_field_acceptedFileTypes" class="typeDependant">
        <label for="acceptedFileTypes"><i18n:message key="backoffice.forms.fields.inputfile.acceptedFileTypes" /></label>
        <select id="acceptedFileTypes" multiple="multiple" name="acceptedFileTypes" title="<i18n:message key="backoffice.forms.fields.inputfile.acceptedFileTypes.hint" />">
            <c:forEach items="${fileTypesList}" var="fileType">
                <option value="${fileType}" <c:if test="${cf:contains(field.acceptedFileTypes, fileType)}">selected="selected"</c:if>>${fileType}</option>
            </c:forEach>
        </select>
        <span class="field_error_message" id="error_msg_acceptedFileTypes"></span>
    </div>
</div>

<c:forEach items="${languages}" var="language" varStatus="status">
    <div class="langDependant" lang="${language}" style="display: none;">
        <p id="p_field_requiredError${language}" class="typeDependant">
            <label for="requiredError"><i18n:message key="backoffice.forms.fields.inputfile.requiredError" /></label>
            <input type="text" id="requiredError${language}" name="requiredError${language}" value="${field.requiredErrorMap[language]}" />
            <span class="field_error_message" id="error_msg_requiredError${language}"></span>
        </p>
    </div>
</c:forEach>

<script type="text/javascript">
    <%-- Metodo luego de procesada la carga de esta seccion. Hacer en este metodo los cambios que se precisen
    en datos generales del campo --%>
    function afterLoading(){
        clearASM('p_field_fileTypesList');
        
        $("#acceptedFileTypes").asmSelect({
            addItemTarget: 'bottom',
            animate: true,
            highlight: true,
            highlightAddedLabel: '<i18n:message key="backoffice.label.added"/>: ',
            highlightRemovedLabel: '<i18n:message key="backoffice.label.removed"/>: '
        });
        
        $('#allowMultiple').change(function(){
            if($(this).val() === 'true'){
                $('#p_field_maxTotalFileSizeMB').show();
                $('#p_field_maxFiles').show();
            }else{
                $('#p_field_maxTotalFileSizeMB').hide();
                $('#p_field_maxFiles').hide();
            }
        });
        
        $('#allowMultiple').trigger('change');
    }
</script>