<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="cf" %>

<%--
    El contenido del div de clase "noLangDependant" contiene los campos que no dependen de lenguaje. Ingresar en esta seccion todos los 
    campos que vayan a desplegarse luego de los datos no dependientes del tipo de campo.

    El contenido de cada div de clase "langDependant" contiene los campos que se desplegaran en cada seccion del formulario especifica para 
    el lenguaje definido por el atributo "lang" del div
--%>
<div class="noLangDependant">
    <p id="p_field_displayType" class="typeDependant">
        <label for="displayType"><i18n:message key="backoffice.forms.fields.paycreditcardamount.displayType" /><span class="mandatory_field">*</span></label>
        <select id="displayType" name="displayType">
            <option value=""></option>
            <c:forEach items="${displayTypes}" var="displayType">
                <option value="${displayType}" <c:if test="${field.displayType == displayType}">selected="true"</c:if>><i18n:message key="backoffice.forms.fields.paycreditcardamount.displayType.${displayType}" /></option>
            </c:forEach>
        </select>
        <span class="field_error_message" id="error_msg_displayType"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.paycreditcardamount.displayType.hint" /></span>
    </p>
    
    <p id="p_field_idCreditCardSelector" class="typeDependant">
        <label for="idCreditCardSelector"><i18n:message key="backoffice.forms.fields.paycreditcardamount.idCreditCardSelector" /><span class="mandatory_field">*</span></label>
        <select id="idCreditCardSelector" name="idCreditCardSelector">
            <option value=""></option>
            <c:forEach items="${productSelectorFieldList}" var="ccsField">
                <option value="${ccsField.idField}" <c:if test="${field.idCreditCardSelector == ccsField.idField}">selected="selected"</c:if>>${ccsField.labelDefault} (${ccsField.idField})</option>
            </c:forEach>
        </select>
        <span class="field_error_message" id="error_msg_idCreditCardSelector"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.paycreditcardamount.idCreditCardSelector.hint" /></span>
    </p>
    
    <p id="p_field_currencyList" class="typeDependant">
        <label for="currencyList"><i18n:message key="backoffice.forms.fields.paycreditcardamount.currencyList" /></label>
        <select id="currencyList" multiple="multiple" name="currencyList" title="<i18n:message key="backoffice.forms.fields.paycreditcardamount.currencyList.hint" />">
            <c:forEach items="${currencyList}" var="currency">
                <option value="${currency}" <c:if test="${cf:contains(field.currencyList, currency)}">selected="selected"</c:if>><i18n:message key="core.currency.label.${currency}" /></option>
            </c:forEach>
        </select>
        <span class="field_error_message" id="error_msg_currencyList"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.paycreditcardamount.currencyList.hint" /></span>
    </p>
    
    <c:if test="${form.type != 'activity'}">
        <p id="p_field_controlLimits" class="typeDependant">
            <label for="controlLimits"><i18n:message key="backoffice.forms.fields.paycreditcardamount.controlLimits" /><span class="mandatory_field">*</span></label>
            <select id="controlLimits" name="controlLimits" class="short_field">
                <option value="false" <c:if test="${!field.validateCap}">selected="selected"</c:if>><i18n:message key="backoffice.no" /></option>
                <option value="true" <c:if test="${field.validateCap}">selected="selected"</c:if>><i18n:message key="backoffice.yes" /></option>
            </select>
            <span class="field_error_message" id="error_msg_controlLimits"></span>
            <span class="field_hint"><i18n:message key="backoffice.forms.fields.paycreditcardamount.controlLimits.hint" /></span>
        </p>

        <c:if test="${capsByProductEnabled}">
            <p id="p_field_controlLimitOverProduct" class="typeDependant">
                <label for="controlLimitOverProduct"><i18n:message key="backoffice.forms.fields.paycreditcardamount.idProductSelectorCurrencyCheck" /></label>
                <select id="controlLimitOverProduct" name="controlLimitOverProduct">
                    <option value=""></option>
                    <c:forEach items="${productSelectorFieldList}" var="psField">
                        <option value="${psField.idField}" <c:if test="${field.idFieldProductToValidateCap == psField.idField}">selected="selected"</c:if>>${psField.labelDefault} (${psField.idField})</option>
                    </c:forEach>
                </select>
                <span class="field_error_message" id="error_msg_controlLimitOverProduct"></span>
                <span class="field_hint"><i18n:message key="backoffice.forms.fields.paycreditcardamount.controlLimitOverProduct.hint" /></span>
            </p>
        </c:if>
    </c:if>
    
</div>

<c:forEach items="${languages}" var="language" varStatus="status">
    <div class="langDependant" lang="${language}" style="display: none;">
        <p id="p_field_placeholder${language}" class="typeDependant">
            <label for="placeholder${language}"><i18n:message key="backoffice.forms.fields.paycreditcardamount.placeholder" /></label>
            <input type="text" id="placeholder${language}" name="placeholder${language}" value="${field.placeholderMap[language]}" />
            <span class="field_error_message" id="error_msg_placeholder${language}"></span>
        </p>
        <p id="p_field_requiredError${language}" class="typeDependant">
            <label for="requiredError"><i18n:message key="backoffice.forms.fields.paycreditcardamount.requiredError" /></label>
            <input type="text" id="requiredError${language}" name="requiredError${language}" value="${field.requiredErrorMap[language]}" />
            <span class="field_error_message" id="error_msg_requiredError${language}"></span>
        </p>
        <p id="p_field_invalidError" class="typeDependant">
            <label for="invalidError"><i18n:message key="backoffice.forms.fields.paycreditcardamount.invalidError" /></label>
            <input type="text" id="invalidError${language}" name="invalidError${language}" value="${field.invalidErrorMap[language]}" />
            <span class="field_error_message" id="error_msg_invalidError${language}"></span>
        </p>

    </div>
</c:forEach>
<script type="text/javascript">
    <%-- Metodo luego de procesada la carga de esta seccion. Hacer en este metodo los cambios que se precisen
    en datos generales del campo --%>
    function afterLoading(){
        clearASM('p_field_currencyList');
        
        $("#currencyList").asmSelect({
            addItemTarget: 'bottom',
            animate: true,
            highlight: true,
            highlightAddedLabel: '<i18n:message key="backoffice.label.added"/>: ',
            highlightRemovedLabel: '<i18n:message key="backoffice.label.removed"/>: '
        });
    }
</script>