<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@ taglib uri="/WEB-INF/custom-functions.tld" prefix="cf" %>
<%--
    El contenido del div de clase "noLangDependant" contiene los campos que no dependen de lenguaje. Ingresar en esta seccion todos los
    campos que vayan a desplegarse luego de los datos no dependientes del tipo de campo.

    El contenido de cada div de clase "langDependant" contiene los campos que se desplegaran en cada seccion del formulario especifica para
    el lenguaje definido por el atributo "lang" del div
--%>
<div class="noLangDependant">
    <div id="p_field_maxAcceptedLines" class="typeDependant">
        <label for="maxAcceptedLines"><i18n:message key="backoffice.forms.fields.transactionlines.maxAcceptedLines" /><span class="mandatory_field">*</span></label>
        <input type="text" id="maxAcceptedLines" name="maxAcceptedLines" value="${field.maxAcceptedLines}" class="integerField" maxlength="10"/>
        <span class="field_error_message" id="error_msg_maxAcceptedLines"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.transactionlines.maxAcceptedLines.hint" /></span>
    </div>

    <p id="p_field_useForTotalAmount" class="typeDependant">
        <label for="useForTotalAmount"><i18n:message key="backoffice.forms.fields.amount.useForTotalAmount" /><span class="mandatory_field">*</span></label>
        <select id="useForTotalAmount" name="useForTotalAmount" class="short_field">
            <option value="false" <c:if test="${!field.useForTotalAmount}">selected="selected"</c:if>><i18n:message key="backoffice.no" /></option>
            <option value="true" <c:if test="${field.useForTotalAmount}">selected="selected"</c:if>><i18n:message key="backoffice.yes" /></option>
        </select>
        <span class="field_error_message" id="error_msg_useForTotalAmount"></span>
        <span class="field_hint"><i18n:message key="backoffice.forms.fields.amount.useForTotalAmount.hint" /></span>
    </p>

    <c:if test="${form.type != 'activity'}">
        <p id="p_field_controlLimits" class="typeDependant">
            <label for="controlLimits"><i18n:message key="backoffice.forms.fields.amount.controlLimits" /><span class="mandatory_field">*</span></label>
            <select id="controlLimits" name="controlLimits" class="short_field">
                <option value="false" <c:if test="${!field.validateCap}">selected="selected"</c:if>><i18n:message key="backoffice.no" /></option>
                <option value="true" <c:if test="${field.validateCap}">selected="selected"</c:if>><i18n:message key="backoffice.yes" /></option>
            </select>
            <span class="field_error_message" id="error_msg_controlLimits"></span>
            <span class="field_hint"><i18n:message key="backoffice.forms.fields.amount.controlLimits.hint" /></span>
        </p>

        <c:if test="${capsByProductEnabled}">
            <p id="p_field_controlLimitOverProduct" class="typeDependant">
                <label for="controlLimitOverProduct"><i18n:message key="backoffice.forms.fields.amount.controlLimitOverProduct" /></label>
                <select id="controlLimitOverProduct" name="controlLimitOverProduct">
                    <option value=""></option>
                    <c:forEach items="${productSelectorFieldList}" var="psField">
                        <option value="${psField.idField}" <c:if test="${field.idFieldProductToValidateCap == psField.idField}">selected="selected"</c:if>>${psField.labelDefault} (${psField.idField})</option>
                    </c:forEach>
                </select>
                <span class="field_error_message" id="error_msg_controlLimitOverProduct"></span>
                <span class="field_hint"><i18n:message key="backoffice.forms.fields.amount.controlLimitOverProduct.hint" /></span>
            </p>
        </c:if>
    </c:if>
</div>

<c:forEach items="${languages}" var="language" varStatus="status">
    <div class="langDependant" lang="${language}" style="display: none;">
        <p id="p_field_requiredError${language}" class="typeDependant">
            <label for="requiredError"><i18n:message key="backoffice.forms.fields.transactionlines.requiredError" /></label>
            <input type="text" id="requiredError${language}" name="requiredError${language}" value="${field.requiredErrorMap[language]}" />
            <span class="field_error_message" id="error_msg_requiredError${language}"></span>
        </p>
    </div>
</c:forEach>

