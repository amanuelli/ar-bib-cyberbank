<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@taglib uri="/WEB-INF/configuration.tld" prefix="configuration" %>
<%@taglib uri="/WEB-INF/auth.tld" prefix="auth" %>

<div id="page_actions" class="clearfix">
    <auth:hasPermission permission="backoffice.invitationCodes.manage">
        <a href="javascript:create()" class="regular_button create_button float_left"><span><i18n:message key="backoffice.invitationCodes.newInvitationCode"/></span></a>
    </auth:hasPermission>
    <a href="javascript:toogleHelp();" class="float_right inline_button"><span class="help_small"><i18n:message key="backoffice.button.help"/></span></a>
    <a href="javascript:printPage();" class="float_right inline_button"><span class="print"><i18n:message key="backoffice.button.print"/></span></a>
    <auth:hasPermission permission="backoffice.massiveData.allow">
        <a href="javascript:exportToExcelMethod('listado_de_codigos_de_invitacion.xls');" class="view_list float_right inline_button"><span class="export_excel"><i18n:message key="backoffice.button.exportToExcel"/></span></a>
    </auth:hasPermission>
</div>

<tiles:insertTemplate template="/default-help.jsp"/>


<div id="content" class="search_open">
        <div id="invitationcodes" class="toggle_section">
            <h2><i18n:message key="backoffice.invitationCodes.title"/></h2>
            <a id="backToList" href="javascript:toggleList();" class="regular_button title_button view_details"><span><i18n:message key="backoffice.button.backToList"/></span></a>
    
            <h3 id="search_button" class="view_list"><a href="javascript:toggleSearchbox()"><i18n:message key="backoffice.tab.search"/></a></h3>
            <div class="view_list">
                <form method="POST" action="" onsubmit="list(1); return false;" id="searchForm">
                    <div id="search_box">
                        <fieldset class="all_fields">
                            <p>
                                <label for="invitationCode_search"><i18n:message key="backoffice.invitationCodes.filter.invitationCode"/></label>
                                <input type="text" id="invitationCode_search" name="invitationCode" maxlength="36" tabindex="1"/>
                            </p>
                            <p>
                                <label for="documentNumber_search"><i18n:message key="backoffice.invitationCodes.filter.documentNumber"/></label>
                                <input type="text" id="documentNumber_search" name="documentNumber" maxlength="20" tabindex="4"/>
                            </p>      
                            <p>
                                <label for="clientId_search"><i18n:message key="backoffice.invitationCodes.filter.clientId"/></label>
                                <input type="text" id="clientId_search" name="clientId" maxlength="20" tabindex="6"/>
                            </p>                      
                            <p>
                                <label for="email_search"><i18n:message key="backoffice.invitationCodes.filter.email"/></label>
                                <input type="text" id="email_search" name="email" class="emailField" maxlength="254" tabindex="5"/>
                            </p>
                            <p>
                                <label for="creationDateFrom_search"><i18n:message key="backoffice.invitationCodes.filter.creationDateFrom"/></label>
                                <input type="text" id="creationDateFrom_search" name="creationDateFrom" readonly tabindex="2"/>
                            </p>
                            <p>
                                <label for="creationDateTo_search"><i18n:message key="backoffice.invitationCodes.filter.creationDateTo"/></label>
                                <input type="text" id="creationDateTo_search" name="creationDateTo" readonly tabindex="3"/>
                            </p>  
                            <p>
                                <label for="mobileNumber_search"><i18n:message key="backoffice.invitationCodes.filter.mobileNumber"/></label>
                                <input type="tel" id="mobileNumber_search" name="mobileNumber" maxlength="20" tabindex="7"/>
                            </p>                    
                            <p>
                                <label for="status_search"><i18n:message key="backoffice.invitationCodes.filter.status"/></label>
                                <select id="status_search" name="status" tabindex="8">
                                    <option value=""></option>
                                    <c:forEach items="${statusList}" var="status">
                                    	<option value="${status.id}"><i18n:message key="${status.key}"/></option>
                                    </c:forEach>
							</select>
                            </p>
                        </fieldset>
                        <fieldset class="button_row">
                            <a class="regular_button" href="javascript:list(1, '')"><span><i18n:message key="backoffice.button.search"/></span></a>
                        </fieldset>
                        <input type="submit" class="subbut" />
                    </div>
                    <div class="view_list" id="list_result">
                        <jsp:include page="_list.jsp" />
                    </div>
                </form>
                <h4 class="search_hint"><i18n:message key="backoffice.invitationCodes.list.searchHint"/>.</h4>
            </div>
        </div>

    <div id="create" class="toggle_section hidden"></div>
    <div id="cancel" class="toggle_section hidden"></div>
    <div id="resend" class="toggle_section hidden"></div>
    <div id="details" class="toggle_section hidden"></div>
</div>
