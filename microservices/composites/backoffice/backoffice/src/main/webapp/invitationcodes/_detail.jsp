<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@taglib uri="/WEB-INF/auth.tld" prefix="auth" %>
<%@taglib uri="/WEB-INF/configuration.tld" prefix="config" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="omnichannel" %>

<auth:hasPermission permission="backoffice.invitationCodes.manage">
    <c:set var="hasManage" value="${true}" />
</auth:hasPermission>

<div id="details_content">
    <h4>
        <em>${invitationCode.invitationCodeMasked}</em> 
        <c:if test="${hasManage && invitationCode.status == 'NOT_USED'}">
            <span class="item_options">
                [ <a href="javascript:cancel('${invitationCode.id}');"><i18n:message key="backoffice.list.actions.cancel"/></a> ]
                [ <a href="javascript:resend('${invitationCode.id}');"><i18n:message key="backoffice.list.actions.reSend"/></a> ]
            </span>
        </c:if>
    </h4>
    <dl class="clearfix">

        <dt><i18n:message key="backoffice.invitationCodes.detail.fullName"/></dt>
        <dd>${invitationCode.firstName} ${invitationCode.lastName}</dd>

        <dt><i18n:message key="backoffice.invitationCodes.detail.document"/></dt>
        <dd>${invitationCode.documentNumber} (<i18n:message key="documentType.label.${invitationCode.documentType}" />, <i18n:message key="country.name.${invitationCode.documentCountry}" />)</dd>

        <dt><i18n:message key="backoffice.invitationCodes.detail.email"/></dt>
        <dd>${invitationCode.email}</dd>

        <dt><i18n:message key="backoffice.invitationCodes.detail.mobileNumber"/></dt>
        <dd>${invitationCode.mobileNumber}</dd>

        <dt><i18n:message key="backoffice.invitationCodes.detail.backendUser"/></dt>
        <dd><i18n:message key="backoffice.${invitationCode.backendUser ? 'yes' : 'no'}" /></dd>

        <dt><i18n:message key="backoffice.invitationCodes.detail.invitationCode"/></dt>
        <dd>${invitationCode.invitationCodeMasked}</dd>

        <dt><i18n:message key="backoffice.invitationCodes.detail.creationDate"/></dt>
        <dd><omnichannel:formatDate value="${invitationCode.creationDate}" format="full" /></dd>

        <dt><i18n:message key="backoffice.invitationCodes.detail.channelSent"/></dt>
        <dd>${invitationCode.channelSent}</dd>

        <dt><i18n:message key="backoffice.invitationCodes.detail.status"/></dt>
        <dd>${invitationCode.statusLabel}</dd>

        <dt><i18n:message key="backoffice.invitationCodes.detail.account"/></dt>
        <dd>${invitationCode.productGroupId}</dd>

        <c:if test="${!empty invitationCode.accessType}">
            <dt><i18n:message key="backoffice.invitationCodes.detail.accessType"/></dt>
            <dd><i18n:message key="backoffice.invitation.role.${invitationCode.accessType}"/></dd>
        </c:if>

        <c:if test="${!empty invitationCode.administrationScheme}">
            <dt><i18n:message key="backoffice.invitationCodes.detail.administrationScheme"/></dt>
            <dd><i18n:message key="backoffice.environments.schemes.${invitationCode.administrationScheme}"/></dd>
        </c:if>

        <c:if test="${!empty invitationCode.signatureLevel}">
            <dt><i18n:message key="backoffice.invitationCodes.detail.signatureLevel"/></dt>
            <dd>${invitationCode.signatureLevel}</dd>
        </c:if>
            
        <c:if test="${!empty invitationCode.signatureQty and invitationCode.signatureQty > 0}">
            <dt><i18n:message key="backoffice.invitationCodes.detail.signatureScheme"/></dt>
            <dd><i18n:message key="backoffice.invitationCodes.signatureScheme.${invitationCode.signatureQty > 1 ? 'joint' : 'indistinct'}"/></dd>
        </c:if>

        <c:if test="${!empty invitationCode.groups}">
            <dt><i18n:message key="backoffice.invitationCodes.detail.groups"/></dt>
            <dd>
                <c:forTokens var="id" items="${invitationCode.groups}" delims="," varStatus="status">
                    <c:set var="group" value="${groupList[status.index]}" />

                    <c:if test="${!status.first}"><br/></c:if>
                    <c:choose>
                        <c:when test="${!empty group}">
                            ${id} (${group.name}) <c:choose><c:when test="${group.deleted}">- <strong><i18n:message key="backoffice.invitationCodes.detail.groups.groupDeleted"/></strong></c:when><c:when test="${group.blocked}">- <strong><i18n:message key="backoffice.invitationCodes.detail.groups.groupBlocked"/></strong></c:when></c:choose>
                        </c:when>
                        <c:otherwise>
                            <%-- El grupo no existe en la base o el código de invitación contenía un id invalido --%>
                            ${id} - <strong><i18n:message key="backoffice.invitationCodes.detail.groups.groupNotValid"/></strong>
                        </c:otherwise>
                    </c:choose>
                </c:forTokens>
            </dd>
        </c:if>

    </dl>
</div>

<c:set var="position" value="${currentPage > 1 ? (rowsPerPage * (currentPage-1)) + position : position}" />
<div id="details_footer" class="button_row clearfix">
    <%-- Desplazarse entre los registros de la página desde el detalle --%>
    <c:if test="${position < totalRows}">
        <a href="javascript:nextInList('invitationCodeList', '${invitationCode.id}','id');" class="regular_button float_right"><span><i18n:message key="backoffice.button.nextOnList"/></span></a>
    </c:if>
    <c:if test="${position != 1}">
        <a href="javascript:previousInList('invitationCodeList','${invitationCode.id}','id');" class="regular_button float_left"><span><i18n:message key="backoffice.button.previousOnList"/></span></a>
    </c:if>
    <p><i18n:message key="backoffice.invitationCodes.detail.watching"/> ${position} <i18n:message key="backoffice.detail.of"/> ${totalRows} [ <a href="javascript:toggleList();"><i18n:message key="backoffice.invitationCodes.detail.watchAll"/></a> ]</p>
</div>