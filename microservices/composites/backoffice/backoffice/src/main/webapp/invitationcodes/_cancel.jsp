<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@taglib uri="/WEB-INF/auth.tld" prefix="auth" %>
<%@taglib uri="/WEB-INF/configuration.tld" prefix="config" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="omnichannel" %>

<%@page import="com.technisys.omnichannel.ReturnCodes"%>

<form method="POST" action="" class="regular_form" id="cancel_element">
    <h4><i18n:message key="backoffice.invitationCodes.cancel.title"/></h4>
    <fieldset>
        <p>
            <label><i18n:message key="backoffice.invitationCodes.cancel.name"/></label>
            <span class="display_field">${invitationCode.firstName} ${invitationCode.lastName}</span>
        </p>
        <p>
            <c:set var="document">${invitationCode.documentNumber} (<i18n:message key="country.name.${invitationCode.documentCountry}" />, <i18n:message key="documentType.label.${invitationCode.documentType}" />)</c:set>
            <label><i18n:message key="backoffice.invitationCodes.cancel.document"/></label>
            <span class="display_field">${document}</span>
        </p>
        <p>
            <label><i18n:message key="backoffice.invitationCodes.cancel.account"/></label>
            <span class="display_field">${invitationCode.productGroupId}</span>
        </p>
    </fieldset>
    <div class="dotted_separator">&nbsp;</div>
    <fieldset>
        <p>
            <label for="comment"><i18n:message key="backoffice.invitationCodes.cancel.comments"/></label>
            <textarea cols="" rows="5" id="comment"></textarea>
        </p>
    </fieldset>
    <div class="dotted_separator">&nbsp;</div>
    <fieldset class="button_row">
        <a href="javascript:doCancel();" class="regular_button"><span><i18n:message key="backoffice.button.accept"/></span></a>
        <a href="javascript:toggleList();" class="regular_button"><span><i18n:message key="backoffice.button.cancel"/></span></a>
        <p class="form_error"></p>
    </fieldset>
</form>

<script type="text/javascript">
    /* <![CDATA[ */
    
    function doCancel() {
        startWorking();
        submitData({
            url: '<c:url value="/invitationcodes/do-cancel"/>',
            data: {
                idCode: ${invitationCode.id}, 
                name: "${invitationCode.firstName} ${invitationCode.lastName}", 
                document: "${document}",
                account: "${invitationCode.productGroupId}",
                comment: $("#comment").val()
            },
            callback: handleDoCancel
        });
    }
    
    function handleDoCancel(data) {
        stopWorking();

        if (data["returnCode"] === '<%= ReturnCodes.OK %>') {
            successDialog('<i18n:message key="backoffice.invitationCodes.cancel.confirmation"/>', function(event, ui){
                list();
                toggleList();
            });
        } else if (data["returnCode"] === '<%= ReturnCodes.BACKOFFICE_REQUIRES_APPROVAL %>') {
            alertDialog('<i18n:message key="backoffice.confirmation"/>.', function(event, ui){
                toggleList();
            });
        }
    }
    
    /* ]]> */
</script>
