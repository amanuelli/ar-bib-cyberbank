<%@page import="com.technisys.omnichannel.backoffice.BackofficeConstants"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@taglib uri="/WEB-INF/auth.tld" prefix="auth" %>

<script type='text/javascript'>
/* <![CDATA[ */

    var actionMenuHeight = 136;
    
    $(document).ready(function() {
        $('#creationDateFrom_search, #creationDateTo_search').datepicker({
            dateFormat: 'dd/mm/yy'
        }).keydown(function(e) {
            <%-- Delete and backspace --%>
            if (e.keyCode === 46 || e.keyCode === 8) {
                $(this).val('');
                $(this).datepicker("hide");
                $(this).blur();
            }
            <%-- If not TAB, prevent keystroke from working --%>
            if (e.keyCode !== 9){
                e.preventDefault();
            }
        });

    });


    <%-- Search --%>
    function goToPage(nro) {
        list(nro);
    }

    function list(nroPag, orderBy, recordToShow) {
        $('.no_result, .search_hint').hide();

        if (typeof nroPag !== 'undefined' && nroPag > -1) {
            $('#pageNumber').val(nroPag);
        }
        if (typeof orderBy !== 'undefined') {
            $('#orderBy').val(orderBy);
        }
        $('#recordToShow').val(typeof recordToShow !== 'undefined' ? recordToShow : '');
        
        postAndLoad({url: '<c:url value="/invitationcodes/list"/>'});
    }

    function toggleList(){
        $('#search_box').show();
        $('#invitationCodes').hide();
        $('#search_button').show();
        $('.toggle_section').addClass('hidden');
        $('#backToList').hide();
        $('.create_button').show();
        $('#invitationcodes').removeClass('hidden');
        list(1);
    }
    
    <%-- Detail --%>
    function detail(id, currentPage, rowsPerPage, totalRows, position) {
        var options = {
            url: '<c:url value="/invitationcodes/read"/>',
            params: {
                idCode: id,
                currentPage: currentPage,
                rowsPerPage: rowsPerPage,
                totalRows: totalRows,
                position: position
            },
            result_container: '#details',
            callback: toggleDetail
        };
        
        load(options);
    }
    
    function toggleDetail(){
        $('#search_box').hide();
        $('#invitationCodes').hide();
        $('#search_button').hide();
        $('#backToList').show();
        $('#details').removeClass('hidden');
        $('.create_button').hide();
        hideActionMenues();
    }
    
    
    <%-- Create --%>
    function create(){
        var options = {
            url: '<c:url value="/invitationcodes/create"/>',
            params: {},
            result_container: '#create',
            callback: toggleCreate
        };
        
        load(options);
    }
    
   function toggleCreate(){
         $('#invitationCodes').hide();
        $('#search_button').hide();
        $('#search_box').hide();
        $('#search_hint').show();

        $('#backToList').show();
        $('#create').removeClass('hidden');
        $('.create_button').hide();
        hideActionMenues();
    }



    <%-- Cancel --%>
    function cancel(id){
        var options = {
            url: '<c:url value="/invitationcodes/cancel"/>',
            params: {idCode: id},
            result_container: '#cancel',
            callback: toggleCancel,
            use_working: false
        };
        
        load(options);
    }
    
    function toggleCancel(){
        $('.toggle_section').addClass('hidden').filter(':not(#invitationcodes,#cancel)').empty();
        $('#backToList').show();
        $('#cancel').removeClass('hidden');
        $('.create_button').hide();
        hideActionMenues();
    }

    
    <%-- Resend --%>
    function resend(id){
        var options = {
            url: '<c:url value="/invitationcodes/resend"/>',
            params: {idCode: id},
            result_container: '#resend',
            callback: toggleResend,
            use_working: false
        };
        
        load(options);
    }
    
    function toggleResend(){
        $('.toggle_section').addClass('hidden').filter(':not(#invitationcodes,#resend)').empty();
        $('#backToList').show();
        $('#resend').removeClass('hidden');
        $('.create_button').hide();
        hideActionMenues();
    }
    
    function hideActionMenues() {
        $(".actions_menu").hide();
    }
    
    <auth:hasPermission permission="backoffice.massiveData.allow">
        function exportToExcelMethod(fileName) {
            location.href = '<c:url value="/invitationcodes/export" />?filename=' +fileName + '&' + $('#searchForm').serialize();
        }    
    </auth:hasPermission>
/* ]]> */
</script>