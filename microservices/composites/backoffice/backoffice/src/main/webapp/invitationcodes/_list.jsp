<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@taglib uri="/WEB-INF/auth.tld" prefix="auth" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="omnichannel" %>

<c:set var='orderedField'>${fn:substringBefore(orderBy, " ")}</c:set>
<c:set var='orderedOrder'>${fn:substringAfter(orderBy, " ")}</c:set>

<auth:hasPermission permission="backoffice.invitationcodes.manage">
    <c:set var="hasManage" value="${true}" />
</auth:hasPermission>

<input type="hidden" id="orderBy" name="orderBy" value="${orderBy}"/>
<input type="hidden" id="pageNumber" name="pageNumber" value="${pageNumber}"/>
<input type="hidden" id="recordToShow" name="recordToShow" value="${recordToShow}"/>

<c:choose>
    <c:when test="${totalRows > 0}">
        <table id='invitationCodes' class="regular_table">
            <thead>
                <tr>
                    <th class="sortable"><a href="javascript:;" id="sort_link_document_number"><i18n:message key="backoffice.invitationCodes.list.column.documentNumber"/></a></th>
                    <th class="sortable"><a href="javascript:;" id="sort_link_email"><i18n:message key="backoffice.invitationCodes.list.column.email"/></a></th>
                    <th class="sortable"><a href="javascript:;" id="sort_link_mobile_number"><i18n:message key="backoffice.invitationCodes.list.column.mobileNumber"/></a></th>
                    <th class="sortable"><a href="javascript:;" id="sort_link_creation_date"><i18n:message key="backoffice.invitationCodes.list.column.creationDate"/></a></th>
                    <th class="sortable"><a href="javascript:;" id="sort_link_status"><i18n:message key="backoffice.invitationCodes.list.column.status"/></a></th>
                    <th class="last">&nbsp;</th>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td colspan="6" id="paging_container">
                        <jsp:include page="/_paging.jsp"/>
                    </td>
                </tr>
            </tfoot>
            <tbody>
                <c:forEach items="${list}" var="item" varStatus="status">
                    <c:set var="rowClass" value="${(status.index)%2 eq 0 ? '' : 'even'}"/>
                    <c:set var="position" value="${status.index + 1}" />
                    <tr class="${rowClass}">
                        <td class="pointer" onclick="detail('${item.id}', ${pageNumber}, ${rowsPerPage}, ${totalRows}, ${position});">${item.documentNumber} (<i18n:message key="documentType.label.${item.documentType}" />, <i18n:message key="country.name.${item.documentCountry}" />)</td>
                        <td class="pointer" onclick="detail('${item.id}', ${pageNumber}, ${rowsPerPage}, ${totalRows}, ${position});">${item.email}</td>
                        <td class="pointer" onclick="detail('${item.id}', ${pageNumber}, ${rowsPerPage}, ${totalRows}, ${position});">${item.mobileNumber}</td>
                        <td class="pointer" onclick="detail('${item.id}', ${pageNumber}, ${rowsPerPage}, ${totalRows}, ${position});"><omnichannel:formatDate value="${item.creationDate}" format="full" /></td>
                        <td class="pointer" onclick="detail('${item.id}', ${pageNumber}, ${rowsPerPage}, ${totalRows}, ${position});">${item.statusLabel}</td>
                        <td class="actions_menu_cell">
                            <c:set var="showActionMenu" value="${hasManage and item.status == 'NOT_USED'}" />
                            <c:if test="${showActionMenu}">
                                <div class="actions_menu_wrapper" id="toogle_${item.id}">
                                    <a href="javascript:toggleTableActionsMenu('${item.id}');" class="actions_menu_button"><i18n:message key="backoffice.list.actions"/></a>
                                    <ul id="actions_menu_${item.id}" class="actions_menu">
                                        <li><a href="javascript:cancel('${item.id}');"><i18n:message key="backoffice.list.actions.cancel"/></a></li>
                                        <li><a href="javascript:resend('${item.id}');"><i18n:message key="backoffice.list.actions.reSend"/></a></li>
                                    </ul>
                                </div>
                            </c:if>
                        </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
    </c:when>
    <c:when test="${totalRows == 0}">
        <h4 class="no_result"><i18n:message key="backoffice.invitationCodes.list.noResults"/></h4>
    </c:when>
</c:choose>

<script type='text/javascript'>
    /* <![CDATA[ */
    <%-- Compatibilidad con los metodos nextInList y previousInList --%>
    var currentListData = {
        currentPage: <c:out value="${pageNumber}" default="0" />,
        totalPages: <c:out value="${totalPages}" default="0" />,
        rowsPerPage: <c:out value="${rowsPerPage}" default="0" />,
        totalRows: <c:out value="${totalRows}" default="0" />,
        orderBy: '${orderBy}',
        invitationCodeList: [<c:forEach var="code" items="${list}" varStatus="status"><c:if test="${!status.first}">,</c:if>{id: ${code.id}}</c:forEach>]
    };
    
    $(document).ready(function () {
        $('#sort_link_${orderedField}').addClass(('${orderedOrder}' === 'ASC') ? 'ascending' : 'descending');

        $('.sortable a').click(function () {
            setOrderAndSearch({order_by: this.id.replace('sort_link_', '')});
        });
        
        <c:if test="${!empty recordToShow}">
            showNextOrPreviousInList("invitationCodeList","id",'${recordToShow}');
        </c:if>
    });

    /* ]]> */
</script>