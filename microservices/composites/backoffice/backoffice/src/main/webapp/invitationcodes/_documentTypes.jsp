<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>

<option value=""></option>
<c:forEach var="documentType" items="${documentTypeList}">
    <option value="${documentType}" <c:if test="${defaultDocumentType eq documentType}">selected="selected"</c:if>><i18n:message key="documentType.label.${documentType}"/></option>
</c:forEach>