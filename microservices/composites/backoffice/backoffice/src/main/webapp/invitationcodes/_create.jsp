<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@taglib uri="/WEB-INF/auth.tld" prefix="auth" %>
<%@taglib uri="/WEB-INF/configuration.tld" prefix="config" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="omnichannel" %>

<%@page import="com.technisys.omnichannel.ReturnCodes"%>

<c:set var="maxSignaturesNedded"><config:parameter key="client.administration.signatures.maxNeeded" /></c:set>
    <form method="POST" action="" class="regular_form" id="create_element" onsubmit="return false;">
        <h4><i18n:message key="backoffice.invitationCodes.create.title"/></h4>
        <fieldset>
            <p id="p_field_documentCountry">
                <label for="documentCountry"><i18n:message key="backoffice.invitationCodes.create.documentCountry"/><span class="mandatory_field">*</span></label>
                <select id="documentCountry" name="documentCountry">
                    <option value=""></option>
                <c:forEach var="country" items="${countryList}">
                    <option value="${country.id}" <c:if test="${defaultCountry eq country.id}">selected="selected"</c:if>>${country.name}</option>
                </c:forEach>
            </select>
            <span class="field_error_message" id="error_msg_documentCountry"></span>
        </p>

        <p id="p_field_documentType">
            <label for="documentType"><i18n:message key="backoffice.invitationCodes.create.documentType"/><span class="mandatory_field">*</span></label>
            <select id="documentType" name="documentType">
                <tiles:insertTemplate template="_documentTypes.jsp" />
            </select>

            <span class="field_error_message" id="error_msg_documentType"></span>
        </p>

        <p id="p_field_documentNumber">
            <label for="documentNumber"><i18n:message key="backoffice.invitationCodes.create.documentNumber"/><span class="mandatory_field">*</span></label>
            <input type="text" id="documentNumber" name="documentNumber" maxlength="25"/>
            <a class="regular_button title_button view_details" href="javascript:readUser();" id="findClient"><span><i18n:message key="backoffice.invitationCodes.create.document.search"/></span></a>
            <span class="field_error_message" id="error_msg_documentNumber"></span>
        </p>

        <p id="p_field_name">
            <label for="name"><i18n:message key="backoffice.invitationCodes.create.name"/></label>
            <input type="hidden" name="fullName" id="fullName" value="" autocomplete="off" />
            <span id="name" class="field-description"></span>
        </p>

        <p id="p_field_email">
            <label for="email"><i18n:message key="backoffice.invitationCodes.create.email"/></label>
            <span id="email" class="field-description"></span>
        </p>

        <p id="p_field_mobileNumber">
            <label for="mobileNumber"><i18n:message key="backoffice.invitationCodes.create.mobileNumber"/></label>
            <span id="mobileNumber" class="field-description"></span>
        </p>

        <p id="p_field_userLang">
            <label for="lang"><i18n:message key="backoffice.invitationCodes.create.lang"/><span class="mandatory_field">*</span></label>
            <select disabled="disabled" name="userLang" id="userLang">
                <c:set var="languages"><config:parameter key="core.languages" /></c:set>
                <c:forTokens var="lang" items="${languages}" delims="|">
                    <option value="${lang}"><i18n:message key="backoffice.language.${lang}"/></option>
                </c:forTokens>
            </select>
            <span class="field_error_message" id="error_msg_userLang"></span>
        </p>

        <p id="p_field_accessType">
            <label for="accessType"><i18n:message key="backoffice.invitationCodes.create.accessType"/><span class="mandatory_field">*</span></label>
            <select disabled="disabled" name="accessType" id="accessType">
                <c:set var="roles"><config:parameter key="invitation.permissions.roleList.backoffice" /></c:set>
                <c:forTokens var="role" items="${roles}" delims="|">
                    <option value="${role}"><i18n:message key="backoffice.invitation.role.${role}"/></option>
                </c:forTokens>
            </select>
        </p>

        <p id="p_field_account">
            <label for="account"><i18n:message key="backoffice.invitationCodes.create.account"/><span class="mandatory_field">*</span></label>
            <select disabled="disabled" id="account" name="account">
            </select>
            <span class="field_error_message" id="error_msg_account"></span>
        </p>

        <p id="p_field_accountSegment">
            <label for="accountSegment"><i18n:message key="backoffice.invitationCodes.create.accountSegment"/></label>
            <span id="accountSegment" class="field-description"></span>
        </p>

        <div id="environmentData" style="display: none;">
            <p id="p_field_administrationScheme">
                <label for="administrationScheme"><i18n:message key="backoffice.invitationCodes.create.administrationScheme"/><span class="mandatory_field">*</span></label>
                <c:forEach var="scheme" items="${adminSchemeList}">
                    <input type="radio" name="administrationScheme" id="administrationScheme_${scheme}" value="${scheme}" />
                    <label for="administrationScheme_${scheme}" class="inline_label"><i18n:message key="backoffice.environments.schemes.${scheme}"/></label>
                </c:forEach>

                <span class="field_error_message" id="error_msg_administrationScheme"></span>
            </p>

            <p id="p_field_signatureQty">
                <label for="signatureQty"><i18n:message key="backoffice.invitationCodes.create.signatureScheme"/><span class="mandatory_field">*</span></label>
                <input type="radio" name="signatureQty" id="signatureQty_indistinct" value="1" />
                <label for="signatureScheme_indistinct" class="inline_label"><i18n:message key="backoffice.invitationCodes.signatureScheme.indistinct"/></label>
                <input type="radio" name="signatureQty" id="signatureQty_joint" value="2" />
                <label for="signatureQty_joint" class="inline_label"><i18n:message key="backoffice.invitationCodes.signatureScheme.joint"/></label>

                <span class="field_error_message" id="error_msg_signatureQty"></span>
            </p>
        </div>

    </fieldset>

    <div class="dotted_separator">&nbsp;</div>
    <fieldset>
        <p>
            <label for="comments"><i18n:message key="backoffice.invitationCodes.create.comments"/></label>
            <textarea cols="" rows="5" id="comments"></textarea>
        </p>
    </fieldset>
    <div class="dotted_separator">&nbsp;</div>
    <fieldset class="button_row">
        <a href="javascript:doCreate();" class="regular_button"><span><i18n:message key="backoffice.button.accept"/></span></a>
        <a href="javascript:toggleList();" class="regular_button"><span><i18n:message key="backoffice.button.cancel"/></span></a>
        <p class="form_error notificationArea"></p>
    </fieldset>
</form>

<script type="text/javascript">
    /* <![CDATA[ */

    $(document).ready(function () {
        $('#documentCountry').change(function () {
            var options = {
                url: '<c:url value="/invitationcodes/document-types"/>',
                params: {
                    country: $(this).val()
                },
                result_container: '#documentType'
            };
            load(options);
        });
        $("#documentCountry, #documentType, #documentNumber").change(function () {
            cleanData();
        });

        $('#account').change(readEnvironment);
    });

    function doCreate() {
        startWorking();
        submitData({
            url: '<c:url value="/invitationcodes/do-create"/>',
            data: $('#create_element').serialize(),
            callback: handleDoCreate
        });
    }

    function handleDoCreate(data) {
        stopWorking();

        if (data["returnCode"] === '<%= ReturnCodes.OK %>') {
            successDialog('<i18n:message key="backoffice.invitationCodes.create.confirmation"/>', function (event, ui) {
                list();
                toggleList();
            });
        } else if (data["returnCode"] === '<%= ReturnCodes.BACKOFFICE_REQUIRES_APPROVAL %>') {
            alertDialog('<i18n:message key="backoffice.confirmation"/>.', function (event, ui) {
                toggleList();
            });
        }
    }

    function readUser() {
        startWorking();
        submitData({
            url: '<c:url value="/invitationcodes/read-user"/>',
            data: $('#create_element').serialize(),
            callback: handleReadUser
        });
    }
    function handleReadUser(data) {
        stopWorking();

        if (data['user'] === null) {
            $('#userLang').removeAttr('disabled');
            
            $('#fullName').val(data['clientUser'].firstName + ' ' + data['clientUser'].lastName);
            $('#name').text($('#fullName').val());
            $('#email').text(data['clientUser'].email);
            $('#mobileNumber').text(data['clientUser'].mobileNumber);
        } else {
            $('#userLang').attr('disabled', 'disabled').val(data['user'].lang);
            
            $('#fullName').val(data['user'].firstName + ' ' + data['user'].lastName);
            $('#name').text($('#fullName').val());
            $('#email').text(data['user'].email);
            $('#mobileNumber').text(data['clientUser'].mobileNumber);
        }

        var _account = $('#account');
        _account.empty();
        for (var i = 0; i < data['accountList'].length; i++) {
            _account.append($('<option>', {value: data['accountList'][i]}).text(data['accountList'][i]));
        }

        $('#account, #accessType').removeAttr('disabled');

        <%-- Precargo los datos de la primer cuenta seleccionada --%>
        handleReadEnvironment(data);
    }

    function readEnvironment() {
        startWorking();
        submitData({
            url: '<c:url value="/invitationcodes/read-environment"/>',
            data: $('#create_element').serialize(),
            callback: handleReadEnvironment
        });
    }
    function handleReadEnvironment(data) {
        stopWorking();

        $('#accountSegment').text(data['clientEnvironment'].segment);

        if (data['environment'] === null) {
            $('#environmentData').show();
        } else {
            $('#environmentData').hide();
        }
    }


    function cleanData() {
        $('#name, #email, #mobileNumber, #account, #accountSegment').empty();
        $('#userLang, #account, #accessType').attr('disabled', 'disabled');
        $('#environmentData').hide();
        $('#fullName').val('');
    }
    /* ]]> */
</script>
