<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@taglib uri="/WEB-INF/auth.tld" prefix="auth" %>
<%@taglib uri="/WEB-INF/configuration.tld" prefix="config" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="omnichannel" %>

<auth:hasPermission permission="backoffice.productRequest.manage">
    <c:set var="hasManage" value="${true}" />
</auth:hasPermission>

<div id="details_content">
    <dl class="clearfix">

        <dt><i18n:message key="backoffice.productRequest.detail.fullName"/></dt>
        <dd>
            <c:if test="${fullName != null}">
                <c:out value="${fullName}" />
            </c:if>
        </dd>
        <dt><i18n:message key="backoffice.productRequest.detail.document"/></dt>
        <dd><c:out value="${onboarding.documentNumber}" /></dd>

        <dt><i18n:message key="backoffice.productRequest.detail.email"/></dt>
        <dd><c:out value="${onboarding.email}" /></dd>

        <dt><i18n:message key="backoffice.productRequest.detail.mobileNumber"/></dt>
        <dd><c:out value="${onboarding.mobileNumber}" /></dd>

        <dt><i18n:message key="backoffice.productRequest.detail.creationDate"/></dt>
        <dd><omnichannel:formatDate value="${onboarding.creationDate}" format="full" /></dd>

        <dt><i18n:message key="backoffice.productRequest.detail.status"/></dt>
        <dd><c:out value="${onboarding.status}" /></dd>

        <dt><i18n:message key="backoffice.productRequest.detail.type"/></dt>
        <dd><c:out value="${onboarding.type}" /></dd>

        <c:if test="${!empty creditCard}">
            <dt><i18n:message key="backoffice.productRequest.detail.creditCard"/></dt>
            <dd><c:out value="${creditCard}" /></dd>
        </c:if>

        <c:forEach items="${documents}" var="item">
            <dt><i18n:message key="backoffice.productRequest.detail.image"/></dt>
            <dd><img src="data:image/png;base64,${item}" style="width: 300px;"/></dd>
        </c:forEach>
    </dl>
</div>

<c:set var="position" value="${currentPage > 1 ? (rowsPerPage * (currentPage-1)) + position : position}" />
<div id="details_footer" class="button_row clearfix">
    <%-- Desplazarse entre los registros de la página desde el detalle --%>
    <c:if test="${position < totalRows}">
        <a href="javascript:nextInList('productRequestList', '${onboarding.id}','id');" class="regular_button float_right"><span><i18n:message key="backoffice.button.nextOnList"/></span></a>
        </c:if>
        <c:if test="${position != 1}">
        <a href="javascript:previousInList('productRequestList','${onboarding.id}','id');" class="regular_button float_left"><span><i18n:message key="backoffice.button.previousOnList"/></span></a>
        </c:if>
    <p><i18n:message key="backoffice.productRequest.detail.watching"/> ${position} <i18n:message key="backoffice.detail.of"/> ${totalRows} [ <a href="javascript:toggleList();"><i18n:message key="backoffice.productRequest.detail.watchAll"/></a> ]</p>
</div>