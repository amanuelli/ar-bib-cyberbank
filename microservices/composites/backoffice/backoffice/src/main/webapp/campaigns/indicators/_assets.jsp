<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib tagdir="/WEB-INF/tags" prefix="omnichannel" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%-- 
- Se debe definir en esta pagina los campos particulares del indicador
- Se dispone a este nivel de un id unico de regla y de un id unico de indicador (a nivel de p�gina, no se corresponde con el id en la base dado que el indicador puede no existir inclusive a�n).
  Se recomienda utilizar estos id para no "pisar" IDs o Names con otro indicador del mismo o diferente tipo
- Se debe implementar una funci�n de nombre get<TipoProducto>IndicatorInfo (idRegla, idIndicador), por ejemplo getProductIndicatorInfo. Verificar que la misma ya no est� definida en la p�gina
  dado que puede haber mas de un indicador del mismo tipo en la regla. Esta funcion debe devolver en formato json algo del tipo <TipoProducto>IndicatorInfo
--%>
<div class="indicator-wrapper__form product-selector clearfix">
    <h3><i18n:message key="backoffice.campaigns.indicators.assets.title"/></h3>
    <hr class="indicator-wrapper__divider">
    <div class="indicator-wrapper__field" id="p_field_${idxRule}_${idxIndicator}_amount">
        <p class="indicator-wrapper__input-text">
            <label for="${idxRule}_${idxIndicator}_amount"><i18n:message key="backoffice.campaigns.indicators.assets.amount"/> <omnichannel:currencySymbol/></label>
            <input type="text" id="${idxRule}_${idxIndicator}_amount" name="${idxRule}_${idxIndicator}_amount" class="amount"/>
        </p>
        <span class="field_error_message" id="error_msg_${idxRule}_${idxIndicator}_amount"></span>
    </div>
    <div class="indicator-wrapper__field" id="p_field_${idxRule}_${idxIndicator}_operator" >
        <fieldset class="product-selector__operator">
            <div class="radio-group">
                <c:forEach items="${resources.operators}" var="operator" varStatus="status">
                    <div class="radio-group__item">
                        <input type="radio" value="${operator}" name="${idxRule}_${idxIndicator}_operator" <c:if test="${status.index==0}">checked="true"</c:if> id="${idxRule}_${idxIndicator}_operator_${operator}">
                        <label for="${idxRule}_${idxIndicator}_operator_${operator}"> <i18n:message key="backoffice.campaigns.indicators.assets.operator.${operator}"/> </label>
                    </div>
                </c:forEach>
            </div>
            <legend><i18n:message key="backoffice.campaigns.indicators.assets.operatorHint"/></legend>
        </fieldset>
        <span class="field_error_message" id="error_msg_${idxRule}_${idxIndicator}_operator"></span>
    </div>
</div>

<script type="text/javascript">
    if (typeof window.getAssetsIndicatorInfo === 'undefined') {
        window.getAssetsIndicatorInfo = function(idxRule, idxIndicator) {
            var indicatorInfo = {};
            indicatorInfo["operator"] = $("input[name='" + idxRule + "_" + idxIndicator + "_operator']:checked"). val();
            if ($("#"+ idxRule + "_" + idxIndicator + "_amount").val()){
                indicatorInfo["amount"] = toNumber($("#"+ idxRule + "_" + idxIndicator + "_amount").val());
            }
            return JSON.stringify(indicatorInfo);
        };
    }
</script>