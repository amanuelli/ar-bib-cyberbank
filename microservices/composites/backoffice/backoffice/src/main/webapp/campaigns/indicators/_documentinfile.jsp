<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@taglib uri="/WEB-INF/i18n.tld" prefix="i18n" %>
<%@taglib uri="/WEB-INF/configuration.tld" prefix="config" %>
<%-- 
- Se debe definir en esta pagina los campos particulares del indicador
- Se dispone a este nivel de un id unico de regla y de un id unico de indicador (a nivel de p�gina, no se corresponde con el id en la base de datos dado que el indicador puede no existir inclusive a�n).
  Se recomienda utilizar estos id para no "pisar" IDs o Names con otro indicador del mismo o diferente tipo
- Se debe implementar una funci�n de nombre get<TipoProducto>IndicatorInfo (idxRegla, idxIndicador), por ejemplo getDocumentInFileIndicatorInfo. Verificar que la misma ya no est� definida en la p�gina
  dado que puede haber mas de un indicador del mismo tipo en la regla. Esta funcion debe devolver en formato json algo del tipo <TipoProducto>IndicatorInfo
--%>

<div class="indicator-wrapper__form file-uploader clearfix">
    <h3><i18n:message key="backoffice.campaigns.indicators.documentInFile.title"/></h3>
    <hr class="indicator-wrapper__divider">
    <div class="indicator-wrapper__field" id="p_field_${idxRule}_${idxIndicator}_file">
        <div class="file-uploader__input clearfix">
            <label for="${idxRule}_${idxIndicator}_upload_button" id="label_${idxRule}_${idxIndicator}"><i18n:message key="backoffice.campaigns.indicators.documentInFile.file"/><span class="mandatory_field">*</span></label>
            <a style="position: relative; overflow: hidden; direction: ltr;" id="${idxRule}_${idxIndicator}_upload_button" class="regular_button" href="javascript:;">
                <span><i18n:message key="backoffice.campaigns.indicators.documentInFile.uploadFile"/></span>
            </a>
            <span id="${idxRule}_${idxIndicator}_progress"></span>    
        </div>
        <span id="error_msg_${idxRule}_${idxIndicator}_file" class="field_error_message"></span>
    </div>
    <div class="indicator-wrapper__field" id="p_field_${idxRule}_${idxIndicator}_operator">
        <fieldset class="product-selector__operator">
            <div class="radio-group">
                <c:forEach items="${resources.operators}" var="operator" varStatus="status">
                    <div class="radio-group__item">
                        <input type="radio" id="${idxRule}_${idxIndicator}_operator_${operator}" value="${operator}" <c:if test="${status.index==0}">checked="true"</c:if> name="${idxRule}_${idxIndicator}_operator">
                        <label for="${idxRule}_${idxIndicator}_operator_${operator}"> <i18n:message key="backoffice.campaigns.indicators.documentInFile.operator.${operator}"/> </label>
                    </div>
                </c:forEach>
            </div>
            <legend><i18n:message key="backoffice.campaigns.indicators.documentInFile.operatorHint"/></legend>
        </fieldset>
        <span id="error_msg_${idxRule}_${idxIndicator}_operator" class="field_error_message"></span>
    </div>
</div>
<script type="text/javascript">
    var idNewContent = -1;
    var fileName;
    
    if (typeof window.getDocumentInFileIndicatorInfo === 'undefined') {
        window.getDocumentInFileIndicatorInfo = function(idxRule, idxIndicator) {
            var indicatorInfo = {};
            indicatorInfo["operator"] = $("input[name='" + idxRule + "_" + idxIndicator + "_operator']:checked"). val();
            <%-- el dia que se pueda editar, hay que enviar el idContent actual en este campo, y no el nuevo --%>
            indicatorInfo["idContent"] = idNewContent;
            indicatorInfo["idNewContent"] = idNewContent;
            
            return JSON.stringify(indicatorInfo);
        };
    }
    
    $(document).ready(function(){
        var idUpload = '${idxRule}_${idxIndicator}';
        new qq.FineUploaderBasic({
            button: $('#' + idUpload + '_upload_button')[0],
            request: {
                endpoint: '<c:url value='/campaigns/uploadBigData'/>',
                forceMultipart: true,
                params: {
                }
            },
            validation: {
                allowedExtensions: 'csv,txt'.split(","),
                sizeLimit: <config:parameter key="backoffice.campaigns.maxFileSize"/>
            },
            debug: true,
            multiple: false,
            messages: {
                typeError: "<i18n:message key='backoffice.files.invalidExtension'/>",
                sizeError: "<i18n:message key='backoffice.files.tooLarge'/>",
                minSizeError: "<i18n:message key='backoffice.files.tooSmall'/>",
                emptyError: "<i18n:message key='backoffice.files.empty'/>",
                noFilesError: "<i18n:message key='backoffice.files.noFile'/>",
                onLeave: "<i18n:message key='backoffice.files.dontLeave'/>"
            },
            callbacks: {
                onError: function(id, fileName, errorReason){
                    errorDialog(errorReason);
                },
                onSubmit: function(id, fileName) {
                    disableAddRule();
                    idNewContent = -1;
                    removeErrorField('${idxRule}_${idxIndicator}_file');
                },
                onUpload: function(id, fileName) {},
                onProgress: function(id, fileName, loaded, total) {
                    percent = Math.round(loaded*100/total);
                    $('#' + idUpload + '_progress').html(percent + "%");
                },
                onComplete: function(id, fileName, responseJSON){
                    $('#' + idUpload + '_progress').html("");
                    if (responseJSON.success) {
                        idNewContent = responseJSON.idContent;
                        fileName = responseJSON.fileName;
                        
                        if($('#' + idUpload + '_list').length === 0){
                            $('#label_' + idUpload).after('<label class="pseudo_label one_line_label" id="' + idUpload + '_list">' + responseJSON.fileName + '</label>');
                            $('#' + idUpload + '_upload_button span').html('<i18n:message key="backoffice.campaigns.indicators.documentInFile.replaceFile" />');
                            
                            removeErrorField('image_' + idUpload);
                            $('.notificationArea').hide();
                        }else{
                            $('#' + idUpload + '_list').html(responseJSON.fileName);
                        }
                    }
                    enableAddRule();
                }
            }
        });
    });
</script>