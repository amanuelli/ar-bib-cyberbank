
-- usuario y ambiente

INSERT INTO users
  (id_user, email, id_user_status, deleted, first_name, last_name, password, lang, logged_in, creation_date, id_seal, mobile_number, trustfull, last_password_change, username, document, document_type, document_country)
  VALUES ('f6cadabe64fe4fdead71456813d41ab0', 'omnichannel-development@technisys.com', 'active', 0, 'John', 'Snow', '5f4dcc3b5aa765d61d8327deb882cf99', 'es', 0, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS'), 8, '59894007032', 1, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS'), 'demos', '27687110', 'CI', 'UY') GO

INSERT INTO environments
  (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('Tech Robot LLC', 'active', 'corporate', 0, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS'), 'advanced', 'daily', 1, '999') GO

INSERT INTO environments
  (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('Devkoa LLC', 'active', 'corporate', 0, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS'), 'medium', 'daily', 1, '1000') GO

INSERT INTO environments
  (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('John Snow', 'active', 'retail', 0, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS'), 'simple', 'daily', 1, '1001') GO

-- relacion usuario-ambiente

INSERT INTO environment_users
  (id_user, id_environment, id_user_status, signature_level, creation_date, enabled_channels)
  SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'active', 'A', TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS'),'frontend,phonegap' FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO

-- grupos y permisos

INSERT INTO groups
  (id_environment, name, blocked, deleted, description)
  SELECT id_environment, 'Administrator', 0, 0, 'Administrators' FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO

INSERT INTO group_users (id_group, id_user)
  SELECT G.id_group, 'f6cadabe64fe4fdead71456813d41ab0'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, P.id_permission, 'ALL_CA'
  FROM groups G, environments E, permissions P
  WHERE G.id_environment = E.id_environment AND E.name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow')
    AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook') GO

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, P.id_permission, 'ALL_CC'
  FROM groups G, environments E, permissions P
  WHERE G.id_environment = E.id_environment AND E.name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow')
    AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook') GO

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_PA'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_PF'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_PI'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_TC'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, P.id_permission, 'NONE'
  FROM groups G, environments E, permissions P
  WHERE G.id_environment = E.id_environment AND E.name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow')
    AND P.id_permission not in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook') GO

--    Bitters 30 June 2020: Signatures & Caps must be inserted into Limits MS database!!!!

---- esquemas de firma
--
--INSERT INTO signatures (id_environment, signature_group, signature_type)
--  SELECT id_environment, 'A', 'ADM'
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO signatures (id_environment, signature_group, signature_type)
--  SELECT id_environment, 'A', 'AMOUNT'
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO signatures (id_environment, signature_group, signature_type)
--  SELECT id_environment, 'A', 'NO_AMOUNT'
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO

---- limites de usuario
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'all', 'f6cadabe64fe4fdead71456813d41ab0', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'frontend', 'f6cadabe64fe4fdead71456813d41ab0', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'phonegap', 'f6cadabe64fe4fdead71456813d41ab0', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'facebook', 'f6cadabe64fe4fdead71456813d41ab0', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'payments', 'f6cadabe64fe4fdead71456813d41ab0', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'sms', 'f6cadabe64fe4fdead71456813d41ab0', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'twitter', 'f6cadabe64fe4fdead71456813d41ab0', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
---- limites de ambiente
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'all', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'frontend', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'phonegap', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'facebook', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'payments', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'sms', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'twitter', 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO
--
---- firmas
--
--INSERT INTO caps_for_signature (id_environment, channel, id_signature, frequency, maximum, used, used_last_reset)
--   SELECT S.id_environment, 'all', S.id_signature, 'daily', -1, 0.000, TO_DATE('2017-11-14 12:35:23','yyyy-mm-dd HH24:MI:SS')
--   FROM environments E, signatures S WHERE E.id_environment = S.id_environment AND E.name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') AND S.signature_type = 'AMOUNT' GO

-- widgets

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'notifications', 1, 1
    FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC', 'John Snow') GO

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'investments', 1, 2
    FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC') GO

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'exchangeRates', 1, 3
    FROM environments WHERE name IN ('Tech Robot LLC', 'Devkoa LLC') GO

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'accounts', 1, 2
    FROM environments WHERE name IN ('John Snow') GO

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'creditCards', 1, 3
    FROM environments WHERE name IN ('John Snow') GO

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'loans', 1, 4
    FROM environments WHERE name IN ('John Snow') GO

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'exchangeRates', 1, 5
    FROM environments WHERE name IN ('John Snow') GO
