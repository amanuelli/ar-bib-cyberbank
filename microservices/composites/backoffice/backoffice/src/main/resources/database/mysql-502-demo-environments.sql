-- 3 nuevos ambientes, uno argentino, otro brasilero y otro EEUU
INSERT INTO environments
  (`name`, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('Juan Corporate', 'active', 'corporate', 0, '2018-12-20 12:00:00', 'advanced', 'daily', 1, '900');

INSERT INTO environments
  (`name`, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('Joao Corporate', 'active', 'corporate', 0, '2018-12-20 12:00:00', 'advanced', 'daily', 1, '901');

INSERT INTO environments
  (`name`, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('John Corporate', 'active', 'corporate', 0, '2018-12-20 12:00:00', 'advanced', 'daily', 1, '902');

INSERT INTO environments
  (`name`, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('Juan Retail', 'active', 'retail', 0, '2018-12-20 12:00:00', 'simple', 'daily', 1, '903');

INSERT INTO environments
  (`name`, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('Joao Retail', 'active', 'retail', 0, '2018-12-20 12:00:00', 'simple', 'daily', 1, '904');

INSERT INTO environments
  (`name`, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('John Retail', 'active', 'retail', 0, '2018-12-20 12:00:00', 'simple', 'daily', 1, '905');

-- Creo un usuario para cada uno de los 3 ambientes anteriores
REPLACE INTO users
  (id_user, email, id_user_status, deleted, first_name, last_name, password, lang, logged_in, creation_date, id_seal, mobile_number, trustfull, last_password_change, username, document, document_type, document_country)
  VALUES ('f6cadaap64fe4hjead71456813d41afo', 'juan@technisys.com', 'active', 0, 'Juan', 'Demo', '5f4dcc3b5aa765d61d8327deb882cf99', 'es', 0, '2018-12-20 12:00:00', 8, '59914007050', 1, '2018-12-20 12:00:00', 'juan@technisys.com', '27687110', 'DNI', 'AR');

REPLACE INTO users
  (id_user, email, id_user_status, deleted, first_name, last_name, password, lang, logged_in, creation_date, id_seal, mobile_number, trustfull, last_password_change, username, document, document_type, document_country)
  VALUES ('f6cadaap64fe4hjead71456813d41af1', 'john@technisys.com', 'active', 0, 'John', 'Demo', '5f4dcc3b5aa765d61d8327deb882cf99', 'en', 0, '2018-12-20 12:00:00', 8, '59914007051', 1, '2018-12-20 12:00:00', 'john@technisys.com', '27687111', 'PAS', 'US');

REPLACE INTO users
  (id_user, email, id_user_status, deleted, first_name, last_name, password, lang, logged_in, creation_date, id_seal, mobile_number, trustfull, last_password_change, username, document, document_type, document_country)
  VALUES ('f6cadaap64fe4hjead71456813d41af2', 'joao@technisys.com', 'active', 0, 'Joao', 'Demo', '5f4dcc3b5aa765d61d8327deb882cf99', 'pt', 0, '2018-12-20 12:00:00', 8, '59914007052', 1, '2018-12-20 12:00:00', 'joao@technisys.com', '27687112', 'PAS', 'BR');


REPLACE INTO environment_users
  (id_user, id_environment, id_user_status, signature_level, creation_date, enabled_channels)
  SELECT 'f6cadaap64fe4hjead71456813d41afo', id_environment, 'active', 'A', '2018-12-20 12:00:00','frontend,phonegap' FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');

REPLACE INTO environment_users
  (id_user, id_environment, id_user_status, signature_level, creation_date, enabled_channels)
  SELECT 'f6cadaap64fe4hjead71456813d41af1', id_environment, 'active', 'A', '2018-12-20 12:00:00','frontend,phonegap' FROM environments WHERE name IN ('John Corporate', 'John Retail');

REPLACE INTO environment_users
  (id_user, id_environment, id_user_status, signature_level, creation_date, enabled_channels)
  SELECT 'f6cadaap64fe4hjead71456813d41af2', id_environment, 'active', 'A', '2018-12-20 12:00:00','frontend,phonegap' FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');

-- grupos y permisos

REPLACE INTO groups
  (id_environment, name, blocked, deleted, description)
  SELECT id_environment, 'Administrator', 0, 0, 'Administrators' FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

REPLACE INTO group_users (id_group, id_user)
  SELECT G.id_group, 'f6cadaap64fe4hjead71456813d41afo'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'Juan Retail');

REPLACE INTO group_users (id_group, id_user)
  SELECT G.id_group, 'f6cadaap64fe4hjead71456813d41af1'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('John Corporate', 'John Retail');

REPLACE INTO group_users (id_group, id_user)
  SELECT G.id_group, 'f6cadaap64fe4hjead71456813d41af2'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Joao Corporate', 'Joao Retail');

REPLACE INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, P.id_permission, 'ALL_CA'
  FROM groups G, environments E, permissions P
  WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail')
    AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

REPLACE INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, P.id_permission, 'ALL_CC'
  FROM groups G, environments E, permissions P
  WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail')
    AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

REPLACE INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_PA'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

REPLACE INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_PF'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

REPLACE INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_PI'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

REPLACE INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_TC'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

REPLACE INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, P.id_permission, 'NONE'
  FROM groups G, environments E, permissions P
  WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail')
    AND P.id_permission not in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

-- widgets

REPLACE INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41afo', id_environment, 'accounts', 1, 1
    FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');

REPLACE INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41afo', id_environment, 'creditCards', 1, 1
    FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');

REPLACE INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41afo', id_environment, 'loans', 1, 3
    FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');

REPLACE INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41af1', id_environment, 'accounts', 1, 1
    FROM environments WHERE name IN ('John Corporate', 'John Retail');

REPLACE INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41af1', id_environment, 'creditCards', 1, 1
    FROM environments WHERE name IN ('John Corporate', 'John Retail');

REPLACE INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41af1', id_environment, 'loans', 1, 3
    FROM environments WHERE name IN ('John Corporate', 'John Retail');

REPLACE INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41af2', id_environment, 'accounts', 1, 1
    FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');

REPLACE INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41af2', id_environment, 'creditCards', 1, 1
    FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');

REPLACE INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41af2', id_environment, 'loans', 1, 3
    FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');
