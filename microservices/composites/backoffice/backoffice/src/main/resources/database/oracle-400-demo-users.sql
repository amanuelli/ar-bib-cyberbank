-- Rol para super administradores
INSERT INTO roles (id_role, role_type, name) VALUES ('client_operator', 'client', 'Cliente - Operador');
INSERT INTO role_permissions (id_role, id_permission)
    SELECT 'client_operator' AS id_role, id_permission FROM permissions WHERE id_permission NOT LIKE '%manage' AND permission_type = 'client';

INSERT INTO roles (id_role, role_type, name) VALUES ('client_administrator', 'client', 'Cliente - Administrador');
INSERT INTO role_permissions (id_role, id_permission)
    SELECT 'client_administrator' AS id_role, id_permission FROM permissions WHERE permission_type = 'client';

-- Ambiente Corporativo
DROP SEQUENCE env_id_environm_seq;

ALTER TRIGGER env_id_environm_trg DISABLE;

INSERT INTO environments (id_environment, name, id_environment_status, environment_type, deleted)
    VALUES (2, 'Ambiente Corporativo I', 'active', 'corporate', 0);

CREATE SEQUENCE env_id_environm_seq START WITH 3 INCREMENT BY 1 NOMAXVALUE;

ALTER TRIGGER env_id_environm_trg ENABLE;

INSERT INTO environment_product (id_product, id_environment, product_type) VALUES ('RUBICON', 2, 'dummy');