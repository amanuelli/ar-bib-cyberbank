-- =============================================================================
-- ======== v0.0.1_004 =========================================================
-- =============================================================================

INSERT INTO client_desktop_widgets (id, name, uri, default_column, default_row)
    VALUES (1, 'accounts', '/widgets/accounts.jsp', 1, 1);

INSERT INTO client_desktop_widgets (id, name, uri, default_column, default_row)
    VALUES (2, 'creditCards', '/widgets/creditCards.jsp', 1, 2);

INSERT INTO client_desktop_widgets (id, name, uri, default_column, default_row)
    VALUES (3, 'transactions', '/widgets/pendingTransactions.jsp', 1, 3);

INSERT INTO client_desktop_widgets (id, name, uri, default_column, default_row)
    VALUES (4, 'news', '/widgets/news.jsp', 1, 4);

INSERT INTO client_desktop_widgets (id, name, uri, default_column, default_row)
    VALUES (5, 'notifications', '/widgets/notifications.jsp', 2, 1);

INSERT INTO client_desktop_widgets (id, name, uri, default_column, default_row)
    VALUES (6, 'frequentTasks', '/widgets/frequentTasks.jsp', 2, 2);

INSERT INTO client_desktop_widgets (id, name, uri, default_column, default_row)
    VALUES (7, 'banners', '/widgets/banners.jsp', 2, 3);
	
INSERT INTO client_desktop_widgets(id, name, uri, default_column, default_row)
    VALUES(8, 'loans', '/widgets/loans.jsp', 1, 5);
