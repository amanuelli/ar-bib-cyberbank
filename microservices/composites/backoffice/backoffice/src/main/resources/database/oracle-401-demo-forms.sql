-- -----------------------------------------------------------------------------
-- Formularios Dinamicos.
-- -----------------------------------------------------------------------------
DELETE FROM forms;

DROP SEQUENCE for_id_form_seq;

ALTER TRIGGER for_id_form_trg DISABLE;

INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(1, 'Solicitud de depósito a plazo fijo', 'La solicitud de la apertura de un plazo fijo requiere que el cliente pueda realizar el depósito desde una cuenta ya existente.', 1, 'accounts', 'demo-1', '<fieldset>
<form:field idField="importe"/>
<form:field idField="cuentaDebito"/>
<form:field idField="usoDeFirmas"/>
<form:field idField="plazo"/>
<form:field idField="cuentaIntereses"/>
<form:field idField="instruccionesVencimiento"/>
<form:field idField="cotitularSeparador"/>
<form:field idField="cotitularApellidos"/>
<form:field idField="cotitularNombres"/>
<form:field idField="cotitularCalle"/>
<form:field idField="cotitularNumero"/>
<form:field idField="cotitularApartamento"/>
<form:field idField="cotitularCodi;Postal"/>
<form:field idField="cotitularLocalidad"/>
<form:field idField="cotitularPais"/>
<form:field idField="cotitularResidente"/>
<form:field idField="cotitularTelefono"/>
<form:field idField="cotitularCelular"/>
<form:field idField="cotitularFax"/>
<form:field idField="cotitularEMail"/>
<form:field idField="1_cotFechaNac"/>
<form:field idField="cotitularLugarDeNacimiento"/>
<form:field idField="cotitularNacionalidad"/>
<form:field idField="cotitularTipoDeVivienda"/>
<form:field idField="cotitularCuotaHipoteca"/>
<form:field idField="cotitularAlquilerImporte"/>
<form:field idField="cotitularEstadoCivil"/>
<form:field idField="2_cotECivilFecha"/>
<form:field idField="cotitularSexo"/>
<form:field idField="cotitularPersonasACar;"/>
<form:field idField="cotitularNumeroDeHijos"/>
<form:field idField="cotitularLaboralSeparador"/>
<form:field idField="cotitularLaboralEmpresa"/>
<form:field idField="cotitularLaboralRubro"/>
<form:field idField="cotitularLaboralCalle"/>
<form:field idField="cotitularLaboralNumero"/>
<form:field idField="cotitularLaboralApartamento"/>
<form:field idField="cotitularLaboralCodi;Postal"/>
<form:field idField="cotitularLaboralLocalidad"/>
<form:field idField="cotitularLaboralProfesion"/>
<form:field idField="cotitularLaboralCar;"/>
<form:field idField="3_cotLabFechaIni"/>
<form:field idField="cotitularLaboralTelefono"/>
<form:field idField="cotitularLaboralEMail"/>
<form:field idField="cotitularLaboralIngresosNetos"/>
<form:field idField="cotitularLaboralOtrosIngresos"/>
<form:field idField="conyugeCotitularSeparador"/>
<form:field idField="conyugeCotitularApellidos"/>
<form:field idField="conyugeCotitularNombres"/>
<form:field idField="conyugeCotitularDocumentoTipo"/>
<form:field idField="conyugeCotitularDocumentoNumero"/>
<form:field idField="conyugeCotitularTelefono"/>
<form:field idField="conyugeCotitularCelular"/>
<form:field idField="conyugeCotitularEmail"/>
<form:field idField="4_conyCotFechaNac"/>
<form:field idField="conyugueCotitularLugar"/>
<form:field idField="conyugueCotitularNacionalidad"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(2, 'Solicitud de acuerdo de crédito en cuenta', 'La solicitud de crédito en cuenta se envía al backoffice de Omnichannel para procesar por el banco en un flujo centralizado.', 1, 'accounts', 'demo-1', '<fieldset>
<form:field idField="cuenta"/>
<form:field idField="limiteOMonto"/>
<form:field idField="plazo"/>
<form:field idField="disclaimer"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(4, 'Solicitud de Leasing', 'La solicitud de un Leasing para empresas se envía al backoffice de Omnichannel para procesar por el banco en un flujo centralizado.', 1, 'loans', 'demo-1', '<fieldset>
<form:field idField="valorNetoDelBien"/>
<form:field idField="incluyeSeguro"/>
<form:field idField="capitalAFinanciar"/>
<form:field idField="plazo"/>
<form:field idField="periodicidadDePa;"/>
<form:field idField="informaciónAdicional"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(5, 'Solicitud de préstamo para empresas PF', 'La solicitud de la apertura de un préstamo a plazo fijo para empresas se envía al backoffice de Omnichannel para procesar por el banco
en un flujo centralizado.', 1, 'loans', 'demo-1', '<fieldset>
<form:field idField="moneda"/>
<form:field idField="plazo"/>
<form:field idField="destino"/>
<form:field idField="destinoOtros"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(6, 'Solicitud de tarjeta débito', 'La solicitud de la tarjeta débito se realiza a través de un flujo de trabajo y un formulario descentralizado.', 1, 'accounts', 'demo-1', '<fieldset>
<form:field idField="tipoPersonaLegal"/>
<form:field idField="numeroDeCuenta"/>
<form:field idField="tipoDeTarjetaJuridica"/>
<form:field idField="tipoDeTarjetaFisica"/>
<form:field idField="programaVisaJuridica"/>
<form:field idField="programaVisaFisica"/>
<form:field idField="cuentaDebitoDeCostos"/>
<form:field idField="dependenciaDeRetiro"/>
<form:field idField="limiteDiarioRetiro"/>
<form:field idField="limiteDiarioDePago"/>
<form:field idField="limiteDiarioDeCompraVisaJuridica"/>
<form:field idField="limiteDiarioDeCompraVisaFisica"/>
<form:field idField="subtituloAdicional"/>
<form:field idField="requiereAdicional"/>
<form:field idField="tipoDeDocumento"/>
<form:field idField="numeroDeDocumento"/>
<form:field idField="primerNombre"/>
<form:field idField="segundoNombre"/>
<form:field idField="primerApellido"/>
<form:field idField="segundoApellido"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(7, 'Solicitud de préstamo para empresas', 'La solicitud de la apertura de un préstamo para empresas se envía al backoffice de Omnichannel para procesar por el banco en un flujo
centralizado.', 1, 'loans', 'demo-1', '<fieldset>
<form:field idField="liquido"/>
<form:field idField="cantidadDeCuotas"/>
<form:field idField="cuentaDeDebitoDeCuota"/>
<form:field idField="destinoDeFondos"/>
<form:field idField="destinoDeFondosOtros"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(8, 'Solicitud de compensación entre cuentas', 'La solicitud de la la compensación entre cuentas se envía al backoffice de Omnichannel para procesar por el banco en un flujo centralizado.', 1, 'accounts', 'demo-1', '<fieldset>
<form:field idField="cuentaDeudoraA"/>
<form:field idField="cuentaDeudoraB"/>
<form:field idField="cuentaDeudoraC"/>
<form:field idField="cuentaDeudoraD"/>
<form:field idField="cuentaDeudoraE"/>
<form:field idField="cuentaDeudoraF"/>
<form:field idField="cuentaDeudoraG"/>
<form:field idField="cuentaDeudoraH"/>
<form:field idField="cuentaAcreedoraA"/>
<form:field idField="cuentaAcreedoraB"/>
<form:field idField="cuentaAcreedoraC"/>
<form:field idField="cuentaAcreedoraD"/>
<form:field idField="cuentaAcreedoraE"/>
<form:field idField="cuentaAcreedoraF"/>
<form:field idField="cuentaAcreedoraG"/>
<form:field idField="cuentaAcreedoraH"/>
<form:field idField="momentoDeCompensacion"/>
<form:field idField="tipoDeCompensacion"/>
<form:field idField="compensacionA"/>
<form:field idField="compensacionB"/>
<form:field idField="compensacionC"/>
<form:field idField="compensacionD"/>
<form:field idField="compensacionE"/>
<form:field idField="compensacionF"/>
<form:field idField="compensacionG"/>
<form:field idField="compensacionH"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(9, 'Solicitud de chequera', 'Solicitud de chequera proceso manual a realizarse en la sucursal de retiro.', 1, 'accounts', 'demo-1', '<fieldset>
<form:field idField="cuenta"/>
<form:field idField="tipoDeCheque"/>
<form:field idField="cantidadDeChequerasNormalSinCruzar"/>
<form:field idField="cantidadDeChequesFanfold"/>
<form:field idField="cantidadDeChequerasNormalCruzado"/>
<form:field idField="cantidadDeChequerasDiferidoSinCruzar"/>
<form:field idField="cantidadDeChequerasDiferidoCruzado"/>
<form:field idField="lugarDeRetiro"/>
<form:field idField="domicilio"/>
<form:field idField="sucursal"/>
<form:field idField="cuentaDebitoDeCosto"/>
<form:field idField="autorizadoARetirarRecibirChequera"/>
<form:field idField="documento"/>
<form:field idField="tipoDeDocumento"/>
<form:field idField="nombre"/>
<form:field idField="primerApellido"/>
<form:field idField="segundoApellido"/>
<form:field idField="terminosYCondiciones"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(10, 'Transferencia al exterior', 'Transferencia al exterior', 1, 'accounts', 'demo-1', '<fieldset>
<form:field idField="importe"/>
<form:field idField="cuentaDebito"/>
<form:field idField="gastosEnElExteriorACargoDe"/>
<form:field idField="subtituloDatosDelCredito"/>
<form:field idField="bancoDestino"/>
<form:field idField="cuentaCredito"/>
<form:field idField="nombreApellidoDestinatario"/>
<form:field idField="ciudadDestino"/>
<form:field idField="paisDestino"/>
<form:field idField="conceptoDelPago"/>
<form:field idField="subtituloBancoIntermediario"/>
<form:field idField="bancoIntermediario"/>
<form:field idField="codigoSWIFTBancoIntermediario"/>
<form:field idField="ciudadIntermediario"/>
<form:field idField="paisBeneficiario"/>
<form:field idField="terminosYCondiciones"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(11, 'Transferencia plaza', 'Transferencia entre bancos de plaza.', 1, 'accounts', 'demo-1', '<fieldset>
<form:field idField="importe"/>
<form:field idField="cuentaDebito"/>
<form:field idField="cuentaDebitoDeGastos"/>
<form:field idField="datosDelCredito"/>
<form:field idField="bancoDestino"/>
<form:field idField="cuentaCredito"/>
<form:field idField="nombreYApellidoDestinatrio"/>
<form:field idField="ciudadDestino"/>
<form:field idField="conceptoDelPago"/>
<form:field idField="terminosYCondiciones"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(12, 'Solicitud de pr&eacute;stamo personal', 'Este formulario sirve de inicio del tramite de solicitud de pr&eacute;stamo de una persona física. La lista de pr&eacute;stamos disponibles se obtiene de una tabla del sistema que el Banco deber mantener a través del Backoffice de Omnichannel. 
No se incluyen productos especiales o formularios especiales para persona jurídica.
El formulario se recibe en el Backoffice de Omnichannel a través de un flujo de trabajo centralizado.', 1, 'loans', 'demo-1', '<fieldset>
<form:field idField="prestamoTipo"/>
<form:field idField="importe"/>
<form:field idField="cantidadDeCuotas"/>
<form:field idField="cuentaAutorizadaADebitar"/>
<form:field idField="destinoDeFondos"/>
<form:field idField="separadorHipotecario"/>
<form:field idField="afiliadoAFAP"/>
<form:field idField="tvCable"/>
<form:field idField="empresaTVCable"/>
<form:field idField="otrasFuentesDeIngreso"/>
<form:field idField="actividadesAnteriores"/>
<form:field idField="otrosTelefonosDeContacto"/>
<form:field idField="separadorDatosDelInmueble"/>
<form:field idField="padronNumero"/>
<form:field idField="tipoDeVivienda"/>
<form:field idField="areaDelTerreno"/>
<form:field idField="areaEdificada"/>
<form:field idField="departamento"/>
<form:field idField="localidad"/>
<form:field idField="barrio"/>
<form:field idField="seccionJudicial"/>
<form:field idField="calle"/>
<form:field idField="numeroDePuerta"/>
<form:field idField="entre"/>
<form:field idField="piso"/>
<form:field idField="apto"/>
<form:field idField="antiguedadConstruccion"/>
<form:field idField="valorProbableInteresado"/>
<form:field idField="descripcionDeComodidades"/>
<form:field idField="destinoDelInmueble"/>
<form:field idField="linderosAumentanRiesgo"/>
<form:field idField="ocupacionLinderos"/>
<form:field idField="techosParedesCombustibles"/>
<form:field idField="detalle"/>
<form:field idField="otrasDeudas"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(13, 'Solicitud de reimpresión', 'Refiere a un formulario donde el cliente solicita la reimpresión de su tarjeta de crédito. Se resuelve con un flujo centralizado gestionado a través del Backoffice de Omnichannel.', 1, 'creditcards', 'demo-1', '<fieldset>
<form:field idField="tarjeta"/>
<form:field idField="motivoDeLaSolicitud"/>
<form:field idField="nombres"/>
<form:field idField="apellidos"/>
<form:field idField="advertencia1"/>
<form:field idField="advertencia2"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(14, 'Solicitud de cambio de condiciones', 'Refiere a un formulario donde el cliente puede cambiar las condiciones de contratación de una tarjeta de crdito. Se procesa a través de un flujo centralizado gestionado a través del Backoffice de Omnichannel', 1, 'creditcards', 'demo-1', '<fieldset>
<form:field idField="numeroTarjeta"/>
<form:field idField="marca"/>
<form:field idField="alcanceMaster"/>
<form:field idField="alcanceVisa"/>
<form:field idField="alcanceAmexCenturion"/>
<form:field idField="alcanceAmexBlueBox"/>
<form:field idField="programaMaster"/>
<form:field idField="programaVisa"/>
<form:field idField="programaAmexCenturion"/>
<form:field idField="programaAmexBlueBox"/>
<form:field idField="cierreVisa"/>
<form:field idField="cierreMaster"/>
<form:field idField="cierreAmexCenturion"/>
<form:field idField="cierreAmexBlueBox"/>
<form:field idField="tipoOperacion"/>
<form:field idField="traspasoLimiteCredito"/>
<form:field idField="montoLimiteTraspasar"/>
<form:field idField="limiteDeCredito"/>
<form:field idField="datosLaborales"/>
<form:field idField="empresa"/>
<form:field idField="rubro"/>
<form:field idField="calle"/>
<form:field idField="numeroLaboral"/>
<form:field idField="codi;Postal"/>
<form:field idField="localidad"/>
<form:field idField="profesion"/>
<form:field idField="car;"/>
<form:field idField="fechaInicio"/>
<form:field idField="telefono"/>
<form:field idField="email"/>
<form:field idField="ingresosNetos"/>
<form:field idField="ingresosOtros"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(15, 'Solicitud de débito automático en tarjeta', 'Solicitud de alta y baja de débito automático en tarjeta de crédito. Es un proceso que se realiza a través de un backoffice con un
flujo de trabajo centralizado del tipo hecho/revisado.
Para el caso de los servicios la lista de débitos que el Cliente puede asociar a sus tarjetas no está actualmente disponibles a
través de un servicio por lo que debe mantenerse en una lista interna de Omnichannel.', 1, 'creditcards', 'demo-1', CONCAT(TO_CLOB('<fieldset>
<form:field idField="tipoOperacion"/>
<form:field idField="tarjetaDebito"/>
<form:field idField="marca"/>
<form:field idField="tipoDebitoVisa"/>
<form:field idField="tipoDebitoMaster"/>
<form:field idField="tipoDebitoVisaIMMMont"/>
<form:field idField="modoPa;Patente"/>
<form:field idField="cuentaCorrientePatente"/>
<form:field idField="cuentaCorrienteSaneamiento"/>
<form:field idField="modoPa;Contribucion"/>
<form:field idField="cuentaCorrienteContribucion"/>
<form:field idField="tipoDebitoVisaIMMald"/>
<form:field idField="modoPa;PatenteMald"/>
<form:field idField="matriculaPatenteMald"/>
<form:field idField="tarjetaContribucionMald"/>
<form:field idField="localidadContribucionMald"/>
<form:field idField="padronContribucionMald"/>
<form:field idField="apartamentoContribucionMald"/>
<form:field idField="tipoDebitoVisaBSE"/>
<form:field idField="seccionCarpetaBSE"/>
<form:field idField="carpetaCarpetaBSE"/>
<form:field idField="ramoPolizaBSE"/>
<form:field idField="polizaPolizaBSE"/>
<form:field idField="certificadoPolizaBSE"/>
<form:field idField="verificadorPolizaBSE"/>
<form:field idField="modoPa;ANEP"/>
<form:field idField="identificadorANEP"/>
<form:field idField="tipoDebitoVisaCJPPU"/>
<form:field idField="numeroAfiliadoCJPPU"/>
<form:field idField="nombreCJPPU"/>
<form:field idField="referenciaCobroUTE"/>
<form:field idField="numeroCuentaANTEL"/>
<form:field idField="tipoDebitoVisaANCEL"/>
<form:field idField="numeroContratoADSLMovil"/>
<form:field idField="numeroContratoFacAncel"/>
<form:field idField="numeroServicioFacAncel"/>
<form:field idField="tipoDebitoVisaMovistar"/>
<form:field idField="numeroMovilmMovAdsl"/>
<form:field idField="numeroClienteMovEmpresas"/>
<form:field idField="rutMovEmpresas"/>
<form:field idField="numeroMovilMovPersonas"/>
<form:field idField="cedulaClienteMovPersonas"/>
<form:field idField="cedulaSocioClaro"/>
<form:field idField="celularClaro"/>
<form:field idField="numeroCuentaClaro"/>
<form:field idField="contratoMasterAncel"/><form:field idField="numeroCelularMasterAncel"/>'),TO_CLOB('<form:field idField="titularMasterAncel"/>
<form:field idField="cuentaMasterAntel"/>
<form:field idField="titularMasterAntel"/>
<form:field idField="identificadorMasterAnep"/>
<form:field idField="seccionMasterBse"/>
<form:field idField="carpetaMasterBse"/>
<form:field idField="monedaMasterBse"/>
<form:field idField="afiliadoMasterCJPPU"/>
<form:field idField="nombreMasterCJPPU"/>
<form:field idField="tipoDebitoMasterIMMont"/>
<form:field idField="cuentaCorrientePatenteMontMaster"/>
<form:field idField="matriculaPatenteMontMaster"/>
<form:field idField="modoDePa;PatenteMontMaster"/>
<form:field idField="cuentaCorrienteContribucionMontMaster"/>
<form:field idField="padronContribucionMontMaster"/>
<form:field idField="unidadOtrosContribucionMontMaster"/>
<form:field idField="modoDePa;ContribucionMontMaster"/>
<form:field idField="cuentaCorrienteTributoMontMaster"/>
<form:field idField="cuentaCorrienteSaneamientoMontMaster"/>
<form:field idField="tipoDebitoMasterIMMald"/>
<form:field idField="padronPatenteMaldMaster"/>
<form:field idField="matriculaPatenteMaldMaster"/>
<form:field idField="modoDePa;PatenteMaldMaster"/>
<form:field idField="tipoContribucionMaldMaster"/>
<form:field idField="localidadContribucionMaldMaster"/>
<form:field idField="padronContribucionMaldMaster"/>
<form:field idField="unidadUbicacionContribucionMaldMaster"/>
<form:field idField="departamentoInteriorMaster"/>
<form:field idField="tipoDebitoMasterIMInterior"/>
<form:field idField="tipoContribucionInteriorMaster"/>
<form:field idField="localidadContribucionInteriorMaster"/>
<form:field idField="padronContribucionInteriorMaster"/>
<form:field idField="ubicacionContribucionInteriorMaster"/>
<form:field idField="modoDePa;ContribucionInteriorMaster"/>
<form:field idField="padronPatenteInteriorMaster"/>
<form:field idField="matriculaPatenteInteriorMaster"/>
<form:field idField="modoDePa;PatenteInteriorMaster"/>
<form:field idField="nombreApellidoVisa"/>
<form:field idField="vencimientoMes"/>
<form:field idField="vencimientoAno"/>
<form:field idField="domicilio"/>
<form:field idField="localidad"/>
<form:field idField="departamento"/>
<form:field idField="telefonoParticular"/>
<form:field idField="telefonoLaboral"/>
<form:field idField="telefonoCelular"/>
</fieldset>')));
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(16, 'Solicitud de débito automático en cuenta', 'Solicitud modificación y baja de débito automático en cuenta de préstamo o servicios. Es un proceso que se realiza a través de
un backoffice con un flujo de trabajo centralizado del tipo hecho/revisado.
Para el caso de los servicios la lista de débitos que el Cliente puede asociar a sus cuentas o tarjetas no está actualmente
disponibles a través de un servicio por lo que debe mantenerse en una lista interna de Omnichannel.
Para el caso de los préstamos se presenta la lista de préstamos del cliente y se le permite agregar otro digitando el número de
producto.
También se incluye una página de visualización para que el usuario pueda ver la lista de pa;s que tiene en débito automático.', 1, 'accounts', 'demo-1', '<fieldset>
<form:field idField="tipoDeOperacion"/>
<form:field idField="cuentaDebito"/>
<form:field idField="tipoDeDebito"/>
<form:field idField="servicioIMMont"/>
<form:field idField="servicioIMMald"/>
<form:field idField="servicioIMSanJose"/>
<form:field idField="numeroCuentaAntel"/>
<form:field idField="numeroContratoAncel"/>
<form:field idField="numeroCelularAncel"/>
<form:field idField="referenciaDeCobroUte"/>
<form:field idField="numeroDeCuentaCorrienteContribIMMont"/>
<form:field idField="formaDePagoContribIMMont"/>
<form:field idField="numeroDeClienteMontGas"/>
<form:field idField="numeroDePadronContrUrbIMMald"/>
<form:field idField="unidadContrUrbIMMald"/>
<form:field idField="manzanaContrUrbIMMald"/>
<form:field idField="solarContrUrbIMMald"/>
<form:field idField="padronPatenteIMMald"/>
<form:field idField="matriculaPatenteIMMald"/>
<form:field idField="marcaVehiculoPatenteIMMald"/>
<form:field idField="anoVehiculoPatenteIMMald"/>
<form:field idField="numeroCuentaOse"/>
<form:field idField="numeroDeCuentaCorrienteSaneamIMMont"/>
<form:field idField="numeroDeCuentaCorrienteImpuestosIMMont"/>
<form:field idField="numeroDeCuentaCorrientePatenteIMMont"/>
<form:field idField="formaDePagoPatenteIMMont"/>
<form:field idField="formaDePagoContrUrbIMMald"/>
<form:field idField="formaDePagoPatenteIMMald"/>
<form:field idField="numeroPadronContrUrbIMSJ"/>
<form:field idField="localidadAbreviadaContrUrbIMSJ"/>
<form:field idField="formaDePagoContrUrbIMSJ"/>
<form:field idField="numeroDePadronContrRuralIMSJ"/>
<form:field idField="localidadAbreviadaContrRuralIMSJ"/>
<form:field idField="formaDePagoContrRuralIMSJ"/>
<form:field idField="numeroDePadronPatenteIMSJ"/>
<form:field idField="matriculaPatenteIMSJ"/>
<form:field idField="formaDePagoPatenteIMSJ"/>
<form:field idField="numeroClienteMovistar"/>
<form:field idField="numeroContratoDedicado"/>
<form:field idField="prestamoAPagar"/>
<form:field idField="prestamoPersonal"/>
<form:field idField="prestamoTerceroDependencia"/>
<form:field idField="prestamoTerceroMoneda"/>
<form:field idField="prestamoTerceroNumero"/>
<form:field idField="prestamoTerceroCodigo"/>
<form:field idField="aceptacionDeSuscripcion"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(17, 'Solicitud de tarjeta de crédito', 'La solicitud de una tarjeta de crédito sigue un flujo de trabajo centralizado', 1, 'creditcards', 'demo-1', '<fieldset>
<form:field idField="marca"/>
<form:field idField="tipoAmericanExpressBlueBox"/>
<form:field idField="tipoAmericanExpressCenturion"/>
<form:field idField="tipoMasterCard"/>
<form:field idField="tipoVisa"/>
<form:field idField="programaAmericanExpressBlueBox"/>
<form:field idField="programaAmericanExpressCenturion"/>
<form:field idField="programaMasterCard"/>
<form:field idField="programaVisa"/>
<form:field idField="subtituloDatosPersonalesDelTitular"/>
<form:field idField="telefonoDeContacto"/>
<form:field idField="celular"/>
<form:field idField="vivienda"/>
<form:field idField="cuotaHipoteca"/>
<form:field idField="alquiler"/>
<form:field idField="personasACargo"/>
<form:field idField="numeroDeHijos"/>
<form:field idField="subtituloDatosPersonalesAdicional"/>
<form:field idField="apellidos"/>
<form:field idField="nombres"/>
<form:field idField="calle"/>
<form:field idField="numero"/>
<form:field idField="apartamento"/>
<form:field idField="codigoPostal"/>
<form:field idField="localidadDepartamento"/>
<form:field idField="pais"/>
<form:field idField="residente"/>
<form:field idField="telefonoAdicional"/>
<form:field idField="celularAdicional"/>
<form:field idField="faxAdicional"/>
<form:field idField="email"/>
<form:field idField="1_fechaNacimiento"/>
<form:field idField="lugarNacimiento"/>
<form:field idField="nacionalidadAdicional"/>
<form:field idField="viviendaAdicional"/>
<form:field idField="cuotaHipotecaAdicional"/>
<form:field idField="alquilerAdicional"/>
<form:field idField="estadoCivilAdicional"/>
<form:field idField="2_fechaEstadoCivilAdicional"/>
<form:field idField="sexoAdicional"/>
<form:field idField="personasACargoAdicional"/>
<form:field idField="numeroHijosAdicional"/>
<form:field idField="subtituloDatosLaboralesDelAdicional"/>
<form:field idField="empresaDatosLaboralesAdicional"/>
<form:field idField="rubroActividadDatosLaboralesAdicional"/>
<form:field idField="domicilioDatosLaboralesAdicional"/>
<form:field idField="numeroDatosLaboralesAdicional"/>
<form:field idField="apartamentoDatosLaboralesAdicional"/>
<form:field idField="codigoPostalDatosLaboralesAdicional"/>
<form:field idField="localidadDepartamentoDatosLaboralesAdicional"/>
<form:field idField="profesionDatosLaboralesAdicional"/>
<form:field idField="cargoFuncionDatosLaboralesAdicional"/>
<form:field idField="3_fechaInicio"/>
<form:field idField="telefonoDatosLaboralesAdicional"/>
<form:field idField="emailDatosLaboralesAdicional"/>
<form:field idField="ingresosNetosMensuales"/>
<form:field idField="otrosIngresos"/>
<form:field idField="subtituloDatosPersonalesDelConyugeDelAdiciona"/>
<form:field idField="apellidosConyugueAdicional"/>
<form:field idField="nombresConyugueAdicional"/>
<form:field idField="tipoYNumeroDocumentoConyugueAdicional"/>
<form:field idField="telefonoConyugueAdicional"/>
<form:field idField="celularConyugueAdicional"/>
<form:field idField="emailConyugueAdicional"/>
<form:field idField="4_fechaDeNacimientoConyugueAdicional"/>
<form:field idField="lugarConyugueAdicional"/>
<form:field idField="nacionalidadConyugueAdicional"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(18, 'Apertura de cartas de crédito', 'Este formulario es el presentado a los clientes que realizan transacciones de comercio exterior. Corresponde a la subsección de
Importaciones y requiere un tratamiento manual centralizado a través del Backoffice de Omnichannel. Se agrega un disclaimer que el
usuario debe aceptar a través de la marca de un campo de tipo check (verificación). Si no se aceptan los términos del disclaimer
no se permite enviar el formulario al Banco.
Para este formulario se requiere de una versión imprimible.', 1, 'foreigntrade_import', 'demo-1', '<fieldset>
<form:field idField="cuentaDebitoComision"/>
<form:field idField="bancoDestino"/>
<form:field idField="transferible"/>
<form:field idField="confirmada"/>
<form:field idField="fechaVencimiento"/>
<form:field idField="nombreYApellidoOrdenante"/>
<form:field idField="beneficiario"/>
<form:field idField="direccionBeneficiario"/>
<form:field idField="ciudadBeneficiario"/>
<form:field idField="paisBeneficiario"/>
<form:field idField="monedaCarta"/>
<form:field idField="importeAdicionalCubierto"/>
<form:field idField="lugarDeOrigen"/>
<form:field idField="lugarDeDestino"/>
<form:field idField="fechaDeVencimientoParaEmbarque"/>
<form:field idField="embarquesParciales"/>
<form:field idField="trasbordos"/>
<form:field idField="descripcionMercaderiaYServicios"/>
<form:field idField="terminosIncoterms"/>
<form:field idField="embarqueMaritimo"/>
<form:field idField="otroTransaporte"/>
<form:field idField="otroTransporteOtros"/>
<form:field idField="disponibleComoSigue"/>
<form:field idField="porPagoDiferidoA"/>
<form:field idField="aceptaciónDeLetraDias"/>
<form:field idField="aceptaciónDeLetraReferencia"/>
<form:field idField="envioReferenciaOtro"/>
<form:field idField="mixto"/>
</fieldset>
<div class="dotted_separator">&nbsp;</div>
<fieldset class=''letter_form''>
    <legend>Documentos exigidos</legend>
    <p><form:field idField="documentosExigidoFacturaComercial"/></p>
    <p><form:field idField="docExigidosDocTransporte"/><form:field idField="docExigidosDocTransporteSel"/></p>
    <p><form:field idField="certificadoDeOrigen"/></p>
    <p><form:field idField="mercosur"/></p>
    <p><form:field idField="listaDeEmpaque"/></p>
    <p><form:field idField="declaracionEscrita1"/></p>
    <p>Declaración escrita original del beneficiario certificando haber enviado al ordenante via especial courier, dentro de los<form:field idField="declaracionEscrita2"/>días posteriores al embarque un juego de documentos originales</p>
    <p>Póliza/certificado de seguro por el<form:field idField="poliza"/>% del valor CIF/CIP a la orden y endosado en blanco cubriendo: para embarques marítimos y terrestres, cláusulas del instituto de aseguradores de Londres para mercancías, (A), guerra y huelgas, para embarques aéreos, cláusulas del instituto de aseguradores de Londres para mercancías, avíón, guerra y huelgas.</p>
</fieldset>
<div class="dotted_separator">&nbsp;</div>
<fieldset class=''letter_form''>
    <legend>Condiciones adicionales</legend>
    <p><form:field idField="condicionesAdicionales"/></p>
    <p>Los documentos deben ser presentados a más tardar<form:field idField="documentosPresentados"/>días después de la fecha de embarque</p>
    <p>Gastos bancarios fuera del Uruguay por cuenta del<form:field idField="gastosBancarios"/></p>
    <p><form:field idField="disclaimer"/></p>
</fieldset>
<fieldset>
<form:field idField="datosNecesarioDelSolicitante"/>
<form:field idField="personaEncargada"/>
<form:field idField="telefono"/>
<form:field idField="fax"/>
<form:field idField="email"/>
<form:field idField="bancoCorresponsalSugerido"/>
<form:field idField="observaciones"/>
<form:field idField="aceptacion"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(41, 'Transferencia de carta de crédito', NULL, 1, 'foreigntrade_export', 'demo-1', '<fieldset class=''letter_form''>
<p>Montevideo, <form:field idField="fecha"/></p>
<p class=''bold''>Sr. gerente de<br />Nuevo Banco Comercial S.A.<br />Comercio Exterior<br />Presente.</p>
<p>Muy señores nuestros:</p>
<p>Por la presente solicitamos a ustedes se sirvan transferir en forma irrevocable el crédito nro. <form:field idField="NroCartaCredito"/> (vuestra referencia
E00 <form:field idField="referencia"/>) a nuestro favor por <form:field idField="montoOrigen"/> según el siguiente detalle:</p>
<p>Importe a transferir: <form:field idField="importeTransferir"/></p>
<p>Segundo beneficiario: <form:field idField="segundoBeneficiario"/></p>
<p>A través del banco: <form:field idField="banco"/></p>
<p>Fecha de vencimiento: <form:field idField="fechaVencimiento"/></p>
<p>Ultima fecha para embarque: <form:field idField="ultimaFechaVencimiento"/></p>
<p>Plazo para presentación de documentos: <form:field idField="plazo"/></p>
<p>A partir de: <form:field idField="fchaAPartirDe"/></p>
<p>Conservamos el derecho a sustitución de factura: <form:field idField="derechoSustitucionFactura"/></p>
<p>Autorizamos a transferir las modificaciones emitidas a la fecha: <form:field idField="autorizacionTransferirModificaciones"/></p>
<p>Conservamos el derecho a transferir las futuras modificaciones previa autorización nuestra por escrito: <form:field idField="conservarDerechoTransferirModificaciones"/></p>
<p>Gastos y comisiones en Uruguay a car; de <form:field idField="gastosComisionesUruguay"/></p>
<p>Gastos y comisiones en el exterior a car; de <form:field idField="gastosComisionesExterior"/></p>
<p>Saludamos a Usted muy atentamente,</p>
<form:field idField="disclaimer"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(61, 'Solicitud de apertura de CC persona física y jurídica', 'El alta de una cuenta corriente se procesará a través del Backoffice de Omnichannel con un formulario y flujo definidos para tal fin. Al
inicio de la solicitud se le pide al Cliente que indique si es persona jurídica o física para presentarle el formulario adecuado.', 1, 'accounts', 'demo-1', '<fieldset>
<form:field idField="subtituloReferencia1"/>
<form:field idField="referencia1Nombre"/>
<form:field idField="referencia1Apellido"/>
<form:field idField="referencia1Telefono"/>
<form:field idField="subtituloReferencia2"/>
<form:field idField="referencia2Nombre"/>
<form:field idField="referencia2Apellido"/>
<form:field idField="referencia2Telefono"/>
<form:field idField="subtituloReferencia3"/>
<form:field idField="referencia3Nombre"/>
<form:field idField="referencia3Apellido"/>
<form:field idField="referencia3Telefono"/>
<form:field idField="subtituloTarjetaDebito"/>
<form:field idField="tarjetaDebito"/>
<form:field idField="tarjetaDebitoNumeroDeCuenta"/>
<form:field idField="tarjetaDebitoTipoTarjeta"/>
<form:field idField="tarjetaDebitoProgramaVisa"/>
<form:field idField="tarjetaDebitoCuentaDebitoCostos"/>
<form:field idField="tarjetaDebitoDependenciaRetiro"/>
<form:field idField="tarjetaDebitoLimiteDiarioRetiro"/>
<form:field idField="tarjetaDebitoLimiteDiarioPago"/>
<form:field idField="tarjetaDebitoVisaLimiteDiarioCompra"/>
<form:field idField="tarjetaDebitoSubtituloAdicional"/>
<form:field idField="tarjetaDebitoAdicionalTipoDocumento"/>
<form:field idField="tarjetaDebitoAdicionalNumeroDocumento"/>
<form:field idField="tarjetaDebitoAdicionalPrimerNombre"/>
<form:field idField="tarjetaDebitoAdicional"/>
<form:field idField="tarjetaDebitoAdicionalPrimerApellido"/>
<form:field idField="tarjetaDebitoAdicionalSegundoApellido"/>
<form:field idField="subtituloFirmas"/>
<form:field idField="usoDeFirmas"/>
<form:field idField="subtituloDPCotitular"/>
<form:field idField="dPCotitularEsConyugue"/>
<form:field idField="dPCotitularApellidos"/>
<form:field idField="dPCotitularNombres"/>
<form:field idField="dPCotitularCalle"/>
<form:field idField="dPCotitularNumero"/>
<form:field idField="dPCotitularApartamento"/>
<form:field idField="dPCotitularCodigoPostal"/>
<form:field idField="dPCotitularLocalidad"/>
<form:field idField="dPCotitularPais"/>
<form:field idField="dPCotitularResidente"/>
<form:field idField="dpCotitularTelefono"/>
<form:field idField="dPCotitularCelular"/>
<form:field idField="dPCotitularFax"/>
<form:field idField="dPCotitularEmail"/>
<form:field idField="dPCotitularFechaNacimiento"/>
<form:field idField="dPCotitularLugarNacimiento"/><form:field idField="dPCotitularNacionalidad"/>
<form:field idField="dPCotitularTipoVivienda"/>
<form:field idField="dPCotitularCuotaHipoteca"/>
<form:field idField="dPCotitularAlquiler"/>
<form:field idField="dPCotitularEstadoCivil"/>
<form:field idField="dPCotitularFechaEstadoCivil"/>
<form:field idField="dPCotitularSexo"/>
<form:field idField="dPCotitularPersonasACargo"/>
<form:field idField="dPCotitularNumeroDeHijos"/>
<form:field idField="subtituloDLCotitular"/>
<form:field idField="dLCotitularEmpresa"/>
<form:field idField="dLCotitularRubro"/>
<form:field idField="dLCotitularCalle"/>
<form:field idField="dLCotitularNumero"/>
<form:field idField="dLCotitularApartamento"/>
<form:field idField="dLCotitularCodigoPostal"/>
<form:field idField="dLCotitularLocalidad"/>
<form:field idField="dLCotitularProfesion"/>
<form:field idField="dLCotitularCargo"/>
<form:field idField="dLCotitularFechaInicio"/>
<form:field idField="dLCotitularTelefono"/>
<form:field idField="dLCotitularEmail"/>
<form:field idField="dLCotitularIngresosNetosMensuales"/>
<form:field idField="dLCotitularOtrosIngresos"/>
<form:field idField="subtituloDPCCotitular"/>
<form:field idField="dPCCotitularApellidos"/>
<form:field idField="dPCCotitularNombres"/>
<form:field idField="dPCCotitularTipoDocumento"/>
<form:field idField="dPCCotitularNumeroDocumento"/>
<form:field idField="dPCCotitularTelefono"/>
<form:field idField="dPCCotitularCelular"/>
<form:field idField="dPCCotitularEmail"/>
<form:field idField="dPCCotitularFechaNacimiento"/>
<form:field idField="dPCCotitularLugar"/>
<form:field idField="dPCCotitularNacionalidad"/>
<form:field idField="terminosYCondiciones"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(62, 'Solicitud de apertura de CA persona física y jurídica', 'El alta de una caja de ahorro se procesará a través del Backoffice de Omnichannel con un formulario y flujo definidos para tal fin. Al
inicio de la solicitud se le pide al Cliente que indique si es persona jurídica o física para presentarle el formulario adecuado.', 1, 'accounts', 'demo-1', CONCAT(TO_CLOB('<fieldset>
<form:field idField="tarjetaDebito"/>
<form:field idField="usoDeFirmas"/>
<form:field idField="subtituloTarjetaDebito"/>
<form:field idField="tarjetaDebitoNumeroDeCuenta"/>
<form:field idField="tarjetaDebitoTipoTarjeta"/>
<form:field idField="tarjetaDebitoProgramaVisa"/>
<form:field idField="tarjetaDebitoCuentaDebitoCostos"/>
<form:field idField="tarjetaDebitoDependenciaRetiro"/>
<form:field idField="tarjetaDebitoLimiteDiarioRetiro"/>
<form:field idField="tarjetaDebitoLimiteDiarioPago"/>
<form:field idField="tarjetaDebitoVisaLimiteDiarioCompra"/>
<form:field idField="tarjetaDebitoSubtituloAdicional"/>
<form:field idField="tarjetaDebitoAdicionalTipoDocumento"/>
<form:field idField="tarjetaDebitoAdicionalNumeroDocumento"/>
<form:field idField="tarjetaDebitoAdicionalPrimerNombre"/>
<form:field idField="tarjetaDebitoAdicional"/>
<form:field idField="tarjetaDebitoAdicionalPrimerApellido"/>
<form:field idField="tarjetaDebitoAdicionalSegundoApellido"/>
<form:field idField="subtituloDatosPersonalesCotitular"/>
<form:field idField="datosPersonalesCotitularEsConyuge"/>
<form:field idField="datosPersonalesCotitularApellidos"/>
<form:field idField="datosPersonalesCotitularNombres"/>
<form:field idField="datosPersonalesCotitularCalle"/>
<form:field idField="datosPersonalesCotitularNumero"/>
<form:field idField="datosPersonalesCotitularApartamento"/>
<form:field idField="datosPersonalesCotitularCodigoPostal"/>
<form:field idField="datosPersonalesCotitularLocalidad"/>
<form:field idField="datosPersonalesCotitularPais"/>
<form:field idField="datosPersonalesCotitularResidente"/>
<form:field idField="datosPersonalesCotitularTelefono"/>
<form:field idField="datosPersonalesCotitularCelular"/>
<form:field idField="datosPersonalesCotitularFax"/>
<form:field idField="datosPersonalesCotitularEmail"/>
<form:field idField="datosPersonalesCotitularFechaDeNacimiento"/>
<form:field idField="datosPersonalesCotitularLugarDeNacimiento"/>
<form:field idField="datosPersonalesCotitularNacionalidad"/>'),TO_CLOB('
<form:field idField="datosPersonalesCotitularTipoVivienda"/>
<form:field idField="datosPersonalesCotitularCuotaHipoteca"/>
<form:field idField="datosPersonalesCotitularAlquiler"/>
<form:field idField="datosPersonalesCotitularEstadoCivil"/>
<form:field idField="datosPersonalesCotitularFechaEstadoCivil"/>
<form:field idField="datosPersonalesCotitularSexo"/>
<form:field idField="datosPersonalesCotitularPersonasACargo"/>
<form:field idField="datosPersonalesCotitularNumeroDeHijos"/>
<form:field idField="subtituloDatosLaboralesDelCotitular"/>
<form:field idField="datosLaboralesDelCotitularEmpresa"/><form:field idField="datosLaboralesDelCotitularRubro"/>
<form:field idField="datosLaboralesDelCotitularCalle"/>
<form:field idField="datosLaboralesDelCotitularNumero"/>
<form:field idField="datosLaboralesDelCotitularApartamento"/>
<form:field idField="datosLaboralesDelCotitularCodigoPostal"/>
<form:field idField="datosLaboralesDelCotitularLocalidad"/>
<form:field idField="datosLaboralesDelCotitularProfesion"/>
<form:field idField="datosLaboralesDelCotitularCargo"/>
<form:field idField="datosLaboralesDelCotitularFechaInicio"/>
<form:field idField="datosLaboralesDelCotitularTelefono"/>
<form:field idField="datosLaboralesDelCotitularEmail"/>
<form:field idField="datosLaboralesDelCotitularIngresosNetosMensuales"/>
<form:field idField="datosLaboralesDelCotitularOtrosIngresos"/>
<form:field idField="subtituloDatosPersonalesDelConyugeDelCotitular"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularApellidos"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularNombres"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularTipoDocumento"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularNumDocumento"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularTelefono"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularCelular"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularEmail"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularFechaNac"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularLugar"/>
<form:field idField="datosPersonalesDelConyugeDelCotitularNacionalidad"/>
<form:field idField="terminosYCondiciones"/>
</fieldset>')));
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(63, 'Solicitud de documentación', 'La solicitud de documentación se envía al backoffice de Omnichannel con un flujo centralizado.', 1, 'others', 'demo-1', '<fieldset>
<form:field idField="documentoSolicitado"/>
<form:field idField="informacionAdicional"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(64, 'Cancelación de cobranza', 'Este formulario permite solicitar el pa; de una cobranza o carta de crédito. Se agrega un disclaimer que el usuario debe aceptar
a través de la marca de un campo de tipo check (verificación). Si no se aceptan los términos del disclaimer no se permite enviar
el formulario al Banco.
El formulario es recibido en el Backoffice de Omnichannel a través de un flujo centralizado.
Se debe poder imprimir.', 1, 'foreigntrade_import', 'demo-1', '<fieldset>
    <form:field idField="referencia"/>
    <form:field idField="tipo"/>
    <form:field idField="numeroDocumento"/>
    <form:field idField="cuentaDebito"/>
    <form:field idField="observaciones"/>
    <form:field idField="verificacion"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(65, 'Modificación de cartas de crédito', 'Este formulario es el presentado a los clientes que realizan transacciones de comercio exterior. Corresponde a la subsección de
Importaciones y requiere un tratamiento manual centralizado a través del Backoffice de Omnichannel', 1, 'foreigntrade_import', 'demo-1', '<fieldset>
    <form:field idField="cartaCreditoNumero"/>
    <form:field idField="nuevaFechaVencimiento"/>
    <form:field idField="nuevaFechaVencimientoEmbarque"/>
    <form:field idField="incrementoDisminucion"/>
    <form:field idField="nuevoImporte"/>
    <form:field idField="cambios"/>
    <form:field idField="disclaimer"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(66, 'Compra de cartas de crédito', 'Formulario en formato carta que el cliente completa. Se agrega un disclaimer que el usuario debe aceptar a través de la marca de
un campo de tipo check (verificación). Si no se aceptan los términos del disclaimer no se permite enviar el formulario al Banco.
El formulario es recibido en el Backoffice de Omnichannel a través de un flujo centralizado. Se debe poder imprimir.', 1, 'foreigntrade_export', 'demo-1', '<fieldset  class=''letter_form''>
    <p>Montevideo, <form:field idField="fecha"/></p>
    <p>Sr. Gerente de<br/>
    <em>NUEVO BANCO COMERCIAL S.A.</em><br/>
    Presente</p>
    <p>Ref: Carta de Crédito Nro. <form:field idField="cartaCreditoNro"/><br/>
    Banco emisor: <form:field idField="bancoEmisor"/></p>
    <p>De nuestra consideración:</p>
    <p>Nos referimos a la documentación por valor de <form:field idField="valor"/> entregada a Uds. al amparo de la Carta de Crédito mencionada en la referencia.</p>
    <p>Habiendo cumplido los requisitos establecidos en la citada carta de crédito solicitamos a Uds. descontar la documentación en cuestión aplicando el líquido resultante a <form:field idField="descuentoDocumentacion"/></p>
    <br/>
    <p>A tales efectos, cedemos sin recurso y a vuestro favor la suma de <form:field idField="montoCedido"/> producto de la negociación de documentos arriba mencionada.</p>
    <p>Esta cesión es irrevocable de nuestra parte y no podrá ser modificada sin vuestro previo consentimiento escrito.</p>
    <p>Sin otro particular aprovechamos para saludar a Uds. muy atentamente.</p>
    <br/>
    <form:field idField="disclaimer"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(67, 'Declaración jurada', NULL, 1, 'foreigntrade_export', 'demo-1', '<fieldset class=''letter_form''>
<p>DECLARACION JURADA (ANEXO 1)</p>
<p>Montevideo, <form:field idField="fecha"/>
<p>Señor Gerente del
<p>BANCO CENTRAL DEL URUGUAY
<p>PRESENTE
<p>La empresa (exportador) <form:field idField="exportador"/>. Nº de RUT <form:field idField="rut"/> en su calidad de <form:field idField="calidad"/> (exportador directo, exportador indirecto) expresa su voluntad de acogerse al Régimen de Financiamiento de Exportaciones previsto en la Circular Nº1972 de fecha 8 de junio de 2007 y
declara que su volumen de exportaciones no supera los USD 5 millones (cinco millores de dòlares estadounidenses) anuales.</p>
<p>El firmante declara conocer que la inclusiòn en la presente declaraciòn de cualquier dato que no se ajuste a la verdad, harà pasible al
declarante de ser criminalmente denunciado, sin perjuicio de las responsabilidades de otra naturaleza que puedan hacerse valer contra él.</p>
<form:field idField="disclaimer"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(68, 'Prefinanciación', NULL, 1, 'foreigntrade_export', 'demo-1', CONCAT(CONCAT(TO_CLOB('<ol class="multi_page_form_navigation clearfix">
	<li class="last"><a href="#" title="anexo3">Carta</a></li>
	<li><a href="#" title="anexo2">Anexo 2</a></li>
	<li class="first"><a href="#" title="anexo1">Anexo 1</a></li>
</ol>

<div id="anexo1" class="multi_page_form_page">
	<h2>Anexo 1</h2>
        <fieldset class="letter_form">
	<p><em>Solicitud de Financiaci&oacute;n</em></p>
	<p>Montevideo, <form:field idField="Fecha"/></p>
	<p class="bold">Sr. gerente de<br />Nuevo Banco Comercial<br />Presente.</p>
	<p>
		De conformidad con lo establecido en la Circular Nro. 1456 y 1972, modificativas y concordantes y Comunicación No. 98/135 del Banco
		Central del Uruguay, solicitamos por intermedio de vuestra institución, la contitución de una operación de financiación de
		exportaciones ante el Banco Central por un importe de <form:field idField="Importe"/> (dólares USA <form:field idField="MontoEnLetras"/>), a <form:field idField="CantidadDias"/> días de plazo.
	</p>
	<p>
		Declaramos bajo juramento que la citada operación se destinará exclusivamente a pre/post financiar la exportación de
		mercaderías catalogadas bajo los rubros NCM <form:field idField="RubrosNCM"/>.
	</p>
	<p>
		Asimismo autorizamos al Banco Central del Uruguay a realizar las inspecciones contables, administrativas o técnicas que juzgue
		oportunas a efectos de verificar la correcta aplicación de los fondos obtenidos mediante este mecanismo de financiación.
	</p>
	<p>Saludamos a Ud. muy atentamente.</p>
        </fieldset>
        <form:field idField="verificacion"/>
</div>

<div id="anexo2" class="multi_page_form_page">
	<h2>Anexo 2</h2>
        <fieldset class="letter_form">
        <p><em>BANCO CENTRAL DEL URUGUAY</em></p>
	<p><em>Constitución de Financiación</em></p>
	<p>Montevideo, <form:field idField="Fecha"/></p>
	<p>
		De acuerdo a lo dispuesto por la Circular Nro. 1456 y 1972, modificativas y concordantes emitidas por ese Banco Central, solicitamos
		se contituya la operación de financiamiento de exportaciones que se detalla, en los términos y condiciones establecidas en la
		normativa referenciada:'),
TO_CLOB('        </p>
	<table>
		<tr>
			<td>1. Fecha de solicitud</td>
			<td><form:field idField="Fecha"/></td>
		</tr>
		<tr>
			<td>2. Códi; de Institución interviniente</td>
			<td><form:field idField="Codi;Institucion"/></td>
		</tr><tr>
			<td>3. Calidad de Exportador (D/I)</td>
			<td><form:field idField="CalidadExportador"/></td>
		</tr>
                <tr>
			<td>4. No. de RUT de exportador</td>
			<td><form:field idField="RUTExportador"/></td>
		</tr>
		<tr>
			<td>5. Nombre del exportador</td>
			<td><form:field idField="NombreExportador"/></td>
		</tr>
		<tr>
			<td>6. Códi; NCM de mercadería</td>
			<td><form:field idField="RubrosNCM"/></td>
		</tr>
		<tr>
			<td>7. Importe en dólares USA</td>
			<td><form:field idField="Importe"/></td>
		</tr>
		<tr>
			<td>8. Códi; de referencia</td>
			<td><form:field idField="Codi;Referencia"/></td>
		</tr>
		<tr>
			<td>9. Códi; de país de destino</td>
			<td><form:field idField="Codi;PaisDestino"/></td>
	</table>
        </fieldset>
</div>
<div id="anexo3" class="multi_page_form_page">')),
TO_CLOB('	<h2>Carta</h2>
        <fieldset class="letter_form">
	<p>Montevideo, <form:field idField="Fecha"/></p>
	<p class="bold">Sr. gerente de<br />Nuevo Banco Comercial SA<br />Presente.</p>
	<p>
		Hacemos referencia a nuestra solicitud de financiación Nº _________________________ presentada a Ustedes con fecha <form:field idField="Fecha"/> de
                conformidad con la Circular Nro. 1456 y 1972, modificativas y concordantes de Banco Central del Uruguay por un importe de <form:field idField="Importe"/> y al
                vale número _________________________ suscrito el día _________________________ por la suma de <form:field idField="ImporteVale"/>.
	</p>
	<p>
		Por la presente, y en garantía del vale precedentemente referido, así como de sus eventuales renovaciones totales o parciales, son sus
		intereses y demás accesorios, cedemos al Nuevo Banco Comercial SA la cantidad de _________________________, más todos los intereses
		correspondientes, que a nuestro favor determina el régimen de la financiación mencionada a ser  liquidados en oportunidad de la
		cancelación de dicho financiamiento.
	</p>
	<p>
		En consecuencia, el Nuevo Banco Comercial SA queda desde ya autorizado en forma irrevocable a proceder a la amortización del vale
		que se garantiza hasta la suma concurrente de los importes que le sean acreditados por el Banco Central del Uruguay de conformidad con
		las previsiones de la Circular Nro. 156 y 1972 citadas y por la operación de financiamiento referida.
	</p><p>
		Dicha amotización podrá ser realizada sin necesidad de intimación, notificación o gestión judicial o extrajudicial alguna y en caso de
		que los importes a ser aplicados a dicha amortización sean acreditados al Nuevo Banco Comercial SA en fecha anterior al vencimiento
		del vale que se garantiza, el Banco podrá optar entre mantener los mismos en calidad de prenda o proceder a la amortización anticipada
		del vale.
	</p>
	<p>
		El Nuevo Banco Comercial SA queda asimismo autorizado irrevocablemente a realizar las conversiones de moneda necesarias a los efectos
		de la amortización, a los tipos de cambios correspondientes al día en que aquellas operaciones se realicen, y en cualquier momento
		a partir de la fecha en que el Banco Central del Uruguay acredite su cuenta por los importes de que se trata. Las partes acuerdan que
		el Banco notificará en forma fehaciente al Banco Central del Uruguay de la presente cesión de crédito.
	</p>
	<p>Sin otro particular, aaludamos a Ud. atentamente.</p>
        </fieldset>
</div>')));
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(69, 'Reclamo de cobranza', NULL, 1, 'foreigntrade_export', 'demo-1', '<fieldset>
<form:field idField="ReferenciaDeCobranza"/>
<form:field idField="Importe"/>
<form:field idField="mensajeAlBanco"/>
<form:field idField="CuentaDebitoGastos"/>
<form:field idField="Disclaimer"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(70, 'Solicitud AVAL/Garantía', NULL, 1, 'foreigntrade_bondsAndGuarantees', 'demo-1', CONCAT(CONCAT(CONCAT(TO_CLOB('<ol class="multi_page_form_navigation clearfix">
	<li class="last"><a href="#" title="anexo2">Anexo 1</a></li>
	<li class="first"><a href="#" title="anexo1">Principal</a></li>
</ol>

<div id="anexo1" class="multi_page_form_page">
    <h2></h2>
<fieldset class="letter_form">
    <p>Montevideo, <form:field idField="fecha"/></p>
    <p class="bold">Sr. gerente de<br />Nuevo Banco Comercial<br />Presente.</p>
    <br/>
    <p>De nuestra consideraci&oacute;n:</p>
    <br/>
    <p>
        Por la presente solicitamos a Uds. se sirvan emitir una garantía irrevocable a favor del Beneficiario mas abajo indicado y de
        acuerdo a los siguientes detalles:
    </p>
    <table>
	    <tr>
		<td>Beneficiario:</td><td><form:field idField="beneficiario"/></td>
            </tr>
            <tr>
                <td>por un monto máximo de:</td><td><form:field idField="montoMaximo"/></td>
            </tr>
            <tr>
                <td>con validez hasta el:</td><td><form:field idField="validez"/></td>
            </tr>
            <tr>
                <td>en garantía de las obligaciones de:</td><td><form:field idField="obligacionesDe"/></td>
            </tr>
            <tr>
                <td>por concepto de:</td><td><form:field idField="concepto"/></td>
	    </tr>
    </table>
    <p>
        Ustedes pagarán contra simple requerimiento del Beneficiario en la forma y condiciones establecidas en el texto de la garantía,
        la que tendrá el texto que se identifica como Anexo I a la presente y que firmado por nosotros forma parte de la presente.
    </p>
    <p>
        Nos obligamos, con relación a la emisión de esta garantía, a reembolsar e indemnizar a ustedes por cualquier desembolso'),
TO_CLOB('        que realicen o cualquier pérdida en la que pudieran incurrir, y a reembolsarles todo gasto que efectúen, así como también a
        mantenerlos libres de toda responsabilidad, todo ello con prescindencia del fundamento del o de los reclamos que motiven el
        pa; o el gasto. Renunciamos expresamente a los derechos emergentes de los artículos 2136 inciso 2° y 3° y 2137 del
        Códi; Civil, así como de los artículos 614, 615 y 616 del Códi; de Comercio sin que esta renuncia afecte las acciones que
        a Uds. le puedan corresponder por derecho.
    </p><p>
        A simple requerimiento de ustedes y contra exhibición del o de los recibos o constancias del o de los pa;s efectuados por
        ustedes al Beneficiario de esta garantía nos obligamos a reembolsar a ustedes en su domicilio de Misiones 1352,
        Montevideo, dicho(s) importe(s), intereses, gastos, tributos y costos el mismo día en que ustedes nos presenten su
        liquidación, que desde ya reconocemos como líquida y exigible a su favor.
    </p>
    <p>
        También a simple requerimiento de ustedes en cualquier momento y sin expresión de causa, nos obligamos a proveer fondos
        suficientes o a constituir a su favor garantías adicionales a satisfacción de ustedes para cubrir los ries;s totales de la
        operación.
    </p>
    <p>
        Todos los pa;s de capital o intereses a ser efectuados por nosotros como consecuencia de esta solicitud serán para ustedes
        libres de toda imposición, gravamen, tributo, deducción, etc. presente o futura de cualquier naturaleza. Lo que antecede
        incluye nuestra expresa obligación al pa; o reembolso a ustedes de cualquier gasto, costo o tributo actual o futuro de
        cualquier naturaleza que ustedes deban pagar y que tenga relación con esta operación, incluido el Impuesto a los Activos
        Bancarios (IMABA), dentro de los diez días siguientes a la notificación que ustedes nos realicen por cualquier medio, incluido
        telegrama colacionado, y aceptando su liquidación como líquida y exigible.
    </p>
    <p>
        Ustedes quedan facultados para debitar y compensar cualquier importe adeudado por cualquier concepto, en cualquiera de
        nuestras cuentas con saldo acreedor en moneda nacional o extranjera, así como a realizar operaciones de cambio o arbitraje,
        sin necesidad de interpelación ni gestión judicial o extrajudicial de especie alguna, debiendo solamente dar aviso a posteriori
        de los importes correspondientes a comisiones, tributos y demás desembolsos que nos hubieren debitado o compensado en
        la forma aquí prevista.
    </p>
    <p>
        En caso de ser aceptada nuestra solicitud, suscribiremos un vale con las siguientes estipulaciones: con fecha de emisión del
        mismo día en que se emita la garantía solicitada. El monto del vale estará en blanco y ustedes completarán el mismo con el
        importe correspondiente a todas las cantidades desembolsadas como consecuencia de la garantía emitida, mas toda
        cantidad que debamos a ustedes por concepto de gastos, intereses, comisión y tributos generados de acuerdo a lo
        establecido en la presente solicitud. La fecha de vencimiento también estará en blanco la que ustedes completarán con la
        fecha en que realicen algún desembolso o el último de ellos o en la fecha de vencimiento o devolución o extinción de la
        garantía inclusive. Dicho vale no devengará intereses compensatorios pero tendrá una tasa de interés moratorio del
        _________________________% anual efectiva que regirá a partir de la fecha de vencimiento del vale.')),
TO_CLOB('    </p>
    <p>
        Dicho vale nos será devuelto una vez que hayamos cancelado con ustedes todo lo que pudieran adeudar por cualquier
        concepto con relación a esta operación o lue; de haberse extinguido la garantía que ustedes emitan sin que se haya
        producido desembolso alguno.
    <p>
    </p>
        Incurriremos en mora de pleno derecho en caso de incumplimiento de cualquiera de las obligaciones que asumimos en este
        documento.
    </p>
    <p>
        Los firmantes establecen la solidaridad e indivisibilidad del objeto de las obligaciones que asumen por la presente solicitud.
    </p><p>
        La comisión sobre esta operación queda fijada en  _________________________% trimestral sobre la suma solicitada estableciéndose un
        mínimo equivalente a U$S 100.-
    </p>
    <p>
        La comisión es pagadera por adelantado y por todo el período, sin opción a devolución en caso de cancelación anticipada.
        A su solicitud suscribiremos una copia de la garantía que emitan a favor del beneficiario, en señal de nuestra conformidad y
        aprobación de su alcance y contenido.
    </p>
    <p>
        Dejamos constancia que los firmantes conservan en su poder copia de esta carta con acuse de recibo por ustedes.
    </p>
    <p>
        Constituimos domicilio a todos los efectos derivados de esta solicitud en @{DOMICILIO}
    </p>
    <p>
        Sin otro particular, saludamos a ustedes muy atentamente,
    </p>
</fieldset>
</div>

<div id="anexo2" class="multi_page_form_page">
    <h2>Anexo</h2>
    <fieldset class="letter_form">
        <p>Montevideo, <form:field idField="fecha"/></p>
        <p>Sres.</p>
        <p>Presente</p>
        <p>Nuestra referencia Número: _________________________</p>')),
TO_CLOB('        <br/>
        <p>De nuestra consideraci&oacute;n:</p>
        <p>
            Por el presente documento NUEVO BANCO COMERCIAL S.A. se constituye en fiador solidario a favor de: <form:field idField="beneficiario"/>
            por hasta la suma de: <form:field idField="montoMaximo"/> (Son <form:field idField="montoEnLetras"/>) amparando las obligaciones de: <form:field idField="obligacionesDe"/> por concepto de: <form:field idField="concepto"/>
        </p>
        <p>
            La presente garantía tendrá vigencia hasta el <form:field idField="validez"/> y cualquier reclamo de vuestra parte deberá ser presentado en nuestras
            oficinas, Misiones 1399, 3er. Piso Comercio Exterior, Montevideo, Uruguay hasta la fecha de vencimiento inclusive, fecha en la cual
            caducará todo derecho a reclamar en función de la misma.
        </p>
        <p>
            Dentro de los plazos indicados precedentemente esta garantía estará incondicionalmente a vuestra disposición y no demandará trámite
            especial ni discusión alguna para hacer efectivo su cobro en el transcurso de las 24 horas siguientes a la presentación por escrito de
            vuestra solicitud reclamando el pa; e indicando el número de referencia de la presente garantía.
        </p>
        <p>
            Solicitamos que para cualquier comunicación relativa a esta transacción se sirvan mencionar nuestro número de referencia.
        </p>
        <p>
            Sin otro particular saludamos a Uds. muy atentamente.
        </p>
        <p>NUEVO BANCO COMERCIAL S.A.</p>
    </fieldset>
    <form:field idField="disclaimer"/>
</div>')));
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(71, 'Levantamiento de carta de crédito con discrepancias', 'Formulario en formato carta que el cliente completa. Se agrega un disclaimer que el usuario debe aceptar a través de la marca de
un campo de tipo check (verificación). Si no se aceptan los términos del disclaimer no se permite enviar el formulario al Banco.
El formulario es recibido en el Backoffice de Omnichannel a través de un flujo centralizado. Se debe poder imprimir.', 1, 'foreigntrade_import', 'demo-1', '<fieldset class=''letter_form''>
    <p>NUEVO BANCO COMERCIAL S.A.<br/>Comercio exterior - Misiones 1399<br/>11.000 Montevideo-Uruguay, Swift:COMEUYMM<br/>Montevideo, <form:field idField="fecha"/></p>
    <p>Sr. Gerente del<br/>NUEVO BANCO COMERCIAL S.A.<br/>Presente</p>
    <br/>
    <p>Ref: crédito documentario nro:<form:field idField="numeroDocCredito"/><br/>Documentos por valor de:<form:field idField="importe"/></p>
    <p>Nos referimos a vuestra atenta de la fecha en la que nos informan haber recibido la documentación que se menciona en la referencia.</p>
    <p>Confirmo(amos) haber recibido de ud.(s) la documentación completa relativa al crédito de referencia emitido en virtud del documento suscrito por mi(nuestra) cuenta.</p>
    <p>Hemos examinado esta documentación y le otorgamos nuestra conformidad a los documentos tal cual fueron presentados y a las operaciones cumplidas por nuestra cuenta, tanto por ese banco como por sus corresponsales y al saldo deudor originado por ellas registrado en la cuenta a mi (nuestro) nombre.</p>
    <p>Autorizo(amos) al(la) portador(a) de la presente,<br/>Nombre:<form:field idField="nombre"/><br/>C.I. Nro.:<form:field idField="ci"/></p>
    <p>A retirar los documentos en mi (nuestro) nombre sin otro particular, saludamos a uds. muy atentamente</p>
    <form:field idField="disclaimer"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(72, 'Autorización de documentos de carta de crédito con discrepancias', 'Se toma como base el formulario de Omnichannel. Se agrega un disclaimer que el usuario debe aceptar a través de la marca de un
campo de tipo check (verificación). Si no se aceptan los términos del disclaimer no se permite enviar el formulario al Banco.
El formulario es recibido en el Backoffice de Omnichannel a través de un flujo centralizado.
Se debe poder imprimir.', 1, 'foreigntrade_export', 'demo-1', '<fieldset>
    <form:field idField="cartaCreditoNro"/>
    <form:field idField="observaciones"/>
    <form:field idField="disclaimer"/>
</fieldset>');
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(81, 'GAOT Alquileres', NULL, 1, 'foreigntrade_bondsAndGuarantees', 'demo-1', CONCAT(CONCAT(CONCAT(TO_CLOB('<ol class="multi_page_form_navigation clearfix">
	<li class="last"><a href="#" title="anexo2">Anexo 1</a></li>
	<li class="first"><a href="#" title="anexo1">Principal</a></li>
</ol>

<div id="anexo1" class="multi_page_form_page">
<h2></h2>
<fieldset class="letter_form">
    <p>Montevideo, <form:field idField="fecha"/></p>
    <p class="bold">Sr. gerente de<br />Nuevo Banco Comercial<br />Presente.</p>
    <br/>
    <p>De mi consideraci&oacute;n:</p>
    <br/>
    <p>
        Por la presente solicito a Uds. se sirvan emitir una garantía irrevocable a favor del Beneficiario mas abajo indicado y de acuerdo a los
        siguientes detalles:
    </p>
    <table>
	    <tr>
		<td>Beneficiario:</td><td><form:field idField="beneficiario"/></td>
            </tr>
            <tr>
                <td>por un monto máximo de:</td><td><form:field idField="montoMaximo"/></td>
            </tr>
            <tr>
                <td>con validez hasta el:</td><td><form:field idField="validez"/></td>
            </tr>
            <tr>
                <td>en garantía de las obligaciones de:</td><td><form:field idField="obligacionesDe"/></td>
            </tr>
            <tr>
                <td>por concepto de:</td><td><form:field idField="concepto"/></td>
	    </tr>
    </table>
    <p>
        Ustedes pagarán contra simple requerimiento del Beneficiario en la forma y condiciones establecidas en el texto de la garantía,
        la que tendrá el texto que se identifica como Anexo I a la presente y que firmado por nosotros forma parte de la presente.
    </p>
    <p>
        Nos obligamos, con relación a la emisión de esta garantía, a reembolsar e indemnizar a ustedes por cualquier desembolso'),
TO_CLOB('		        que realicen o cualquier pérdida en la que pudieran incurrir, y a reembolsarles todo gasto que efectúen, así como también a
        mantenerlos libres de toda responsabilidad, todo ello con prescindencia del fundamento del o de los reclamos que motiven el
        pa; o el gasto. Renunciamos expresamente a los derechos emergentes de los artículos 2136 inciso 2° y 3° y 2137 del
        Códi; Civil, así como de los artículos 614, 615 y 616 del Códi; de Comercio sin que esta renuncia afecte las acciones que
        a Uds. le puedan corresponder por derecho.
    </p>
    <p>
        A simple requerimiento de ustedes y contra exhibición del o de los recibos o constancias del o de los pa;s efectuados por
        ustedes al Beneficiario de esta garantía nos obligamos a reembolsar a ustedes en su domicilio de Misiones 1352,
        Montevideo, dicho(s) importe(s), intereses, gastos, tributos y costos el mismo día en que ustedes nos presenten su
        liquidación, que desde ya reconocemos como líquida y exigible a su favor.
    </p><p>
        También a simple requerimiento de ustedes en cualquier momento y sin expresión de causa, nos obligamos a proveer fondos
        suficientes o a constituir a su favor garantías adicionales a satisfacción de ustedes para cubrir los ries;s totales de la
        operación.
    </p>
    <p>
        Todos los pa;s de capital o intereses a ser efectuados por nosotros como consecuencia de esta solicitud serán para ustedes
        libres de toda imposición, gravamen, tributo, deducción, etc. presente o futura de cualquier naturaleza. Lo que antecede
        incluye nuestra expresa obligación al pa; o reembolso a ustedes de cualquier gasto, costo o tributo actual o futuro de
        cualquier naturaleza que ustedes deban pagar y que tenga relación con esta operación, incluido el Impuesto a los Activos
        Bancarios (IMABA), dentro de los diez días siguientes a la notificación que ustedes nos realicen por cualquier medio, incluido
        telegrama colacionado, y aceptando su liquidación como líquida y exigible.
    </p>
    <p>
        Ustedes quedan facultados para debitar y compensar cualquier importe adeudado por cualquier concepto, en cualquiera de
        nuestras cuentas con saldo acreedor en moneda nacional o extranjera, así como a realizar operaciones de cambio o arbitraje,
        sin necesidad de interpelación ni gestión judicial o extrajudicial de especie alguna, debiendo solamente dar aviso a posteriori
        de los importes correspondientes a comisiones, tributos y demás desembolsos que nos hubieren debitado o compensado en
        la forma aquí prevista.
    </p>
    <p>
        En caso de ser aceptada nuestra solicitud, suscribiremos un vale con las siguientes estipulaciones: con fecha de emisión del
        mismo día en que se emita la garantía solicitada. El monto del vale estará en blanco y ustedes completarán el mismo con el
        importe correspondiente a todas las cantidades desembolsadas como consecuencia de la garantía emitida, mas toda
        cantidad que debamos a ustedes por concepto de gastos, intereses, comisión y tributos generados de acuerdo a lo
        establecido en la presente solicitud. La fecha de vencimiento también estará en blanco la que ustedes completarán con la
        fecha en que realicen algún desembolso o el último de ellos o en la fecha de vencimiento o devolución o extinción de la
        garantía inclusive. Dicho vale no devengará intereses compensatorios pero tendrá una tasa de interés moratorio del
        _________________________% anual efectiva que regirá a partir de la fecha de vencimiento del vale.')),
TO_CLOB('
    </p>
    <p>
        Dicho vale nos será devuelto una vez que hayamos cancelado con ustedes todo lo que pudieran adeudar por cualquier
        concepto con relación a esta operación o lue; de haberse extinguido la garantía que ustedes emitan sin que se haya
        producido desembolso alguno.
    </p>
    <p>
        Incurriremos en mora de pleno derecho en caso de incumplimiento de cualquiera de las obligaciones que asumimos en este
        documento.
    </p>
    <p>
        Los firmantes establecen la solidaridad e indivisibilidad del objeto de las obligaciones que asumen por la presente solicitud.
    </p>
    <p>
        A su solicitud suscribiremos una copia de la garantía que emitan a favor del beneficiario, en señal de nuestra conformidad y
        aprobación de su alcance y contenido.
    </p>
    <p>
        Dejamos constancia que los firmantes conservan en su poder copia de esta carta con acuse de recibo por ustedes.
    </p>
    <p>
        Constituimos domicilio a todos los efectos derivados de esta solicitud en @{DOMICILIO}
    </p>
    <p>
        Sin otro particular, saludamos a ustedes muy atentamente,
    </p>
</fieldset>
</div><div id="anexo2" class="multi_page_form_page">
    <h2>Anexo I</h2>
    <fieldset class="letter_form">

        <p class="bold">GARANTIA IRREVOCABLE A PRIMER REQUERIMIENTO</p>
        <p class="bold">(Garantía de alquileres)</p>
        <br/>
        <p>Montevideo, <form:field idField="fecha"/></p>
        <p>Sres.</p>
        <p>Presente</p>
        <p>Nuestra referencia Número: _________________________</p>
        <br/>')),
TO_CLOB('
        <p>De nuestra consideraci&oacute;n:</p>
        <p>
            Por &oacute;rden de nuestro cliente @{NOMBREYAPELLIDO}, domiciliado en @{DOMICILIO}, por la presente nos obligamos a favor de ustedes,
            a trav&eacute;s de esta garant&iacute;a irrevocable stand-by, por un monto que no exceder&aacute; de <form:field idField="montoMaximo"/>, disponible a
            partir del d&iacute;a de la fecha y hasta el d&iacute;a <form:field idField="validez"/> inclusive en nuestras oficinas en Misiones 1399 3er. Piso
            Comercio Exterior, Montevideo, Uruguay en relaci&oacute;n con el cumplimiento por parte de nuestro cliente de las siguientes
            obligaciones emergentes de <form:field idField="obligacionesDe"/>
        </p>
        <p>
            Nos obligamos a pagar en cumplimiento de la presente garantía: a) contra su simple requerimiento por telegrama colacionado u otro medio
            escrito fehaciente, expresando que el Arrendatario le adeuda la suma que se reclama y especificando el concepto y cláusulas del contrato
            en que se funda, y ademas b) contra la entrega en nuestro domicilio de un certificado notarial que acredite haber intimado judicial o
            extrajudicialmente al Arrendatario el pa; de la suma que se nos reclama en virtud de las obligaciones asumidas por éste en el contrato
            de arrendamiento referido, y c) en caso de tratarse de un pa; hasta el monto máximo aquí establecido se pagará contra la devolución de
            esta garantía.
        </p>
        <p>
            Si recibimos un requerimiento de pa; y una declaración en la forma indicada en el párrafo anterior, en nuestras oficinas en Misiones
            1399 3er. Piso Comercio Exterior, Montevideo, Uruguay antes del <form:field idField="validez"/> o hasta dicha fecha inclusive pagaremos bajo esta
            garantía inmediatamente, exclusivamente en la forma establecida anteriormente. Pasada dicha fecha nada adeudaremos ni garantizaremos
            ni pagaremos a uds. por ningún motivo derivado de esta garantía. En consecuencia esta garantía no cubre ninguna deuda posterior a la
            fecha indicada ni tampoco ninguna deuda anterior a la misma pero cuyo reclamo sea recibido por nosotros después de dicha fecha.
        </p>
        <p>
            Esta garantía no es transferible sin el consentimiento previo por escrito nuestro.
        </p>
        <p>
            Esta garantía irrevocable está sujeta a lo dispuesto por las International Standby Practices 1998 de la C&aacute;mara de Comercio Internacional.
        </p>
        <p>
           NUEVO BANCO COMERCIAL S.A.
        </p>
        <form:field idField="disclaimer"/>
    </fieldset>
</div>')));
INSERT INTO forms(id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES(82, 'Prueba', 'para borrar', 0, 'accounts', 'demo-1', '<fieldset>
<form:field idField="tipoSolicitud"/>
<form:field idField="descripcion"/>
</fieldset>');
--83
INSERT INTO forms (id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES (83, 'Solicitud de activación de chequera', 'Formulario que permite que un cliente solicite activar una chequera que se encuentra en su poder.', 1, 'accounts', 'demo-1', '<fieldset>\n<form:field idField="cuenta"/>\n<form:field idField="tipoDeChequera"/>\n<form:field idField="moneda"/>\n<form:field idField="primerChequeSerie"/>\n<form:field idField="primerChequeNumero"/>\n<form:field idField="cantidadSerie"/>\n<form:field idField="cantidadNumero"/>\n</fieldset>');
--84
INSERT INTO forms (id_form, form_name, description, enabled, location, id_jbpm_process, template_es)
  VALUES (84, 'Solicitud de denuncia de cheque', NULL, 1, 'accounts', 'demo-1', '<fieldset>\n<form:field idField="cuenta"/>\n<form:field idField="tipoDeChequera"/>\n<form:field idField="moneda"/>\n<form:field idField="numeroDeChequeSerie"/>\n<form:field idField="numeroDeChequeNumero"/>\n<form:field idField="motivo"/>\n</fieldset>');

CREATE SEQUENCE for_id_form_seq START WITH 85 INCREMENT BY 1 NOMAXVALUE;

ALTER TRIGGER for_id_form_trg ENABLE;