-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.accounts.sendCheck
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.accounts.sendCheck', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.accounts.sendCheck', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.sendCheckPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.sendCheckPre', 1, 1, 'com.technisys.rubicon.business.accounts.activities.SendCheckPreActivity', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.sendCheck
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.sendCheck', 1, 1, 'com.technisys.rubicon.business.accounts.activities.SendCheckActivity', 'rub.accounts.sendCheck');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.sendCheckImport
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.sendCheckImport', 1, 1, 'com.technisys.rubicon.business.accounts.activities.ImportSendCheckActivity', null);

-------
INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.producttype.sendCheck.list', 'CA|CC', null, 'Rubicon');

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.currency.sendCheck.list', 'UYP|USD', null, 'Rubicon');

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.accounts.sendCheck.jbpmProcessId', 'demo-1', null, 'Rubicon');

--DESCUENTO DE CHEQUES
-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.accounts.checkDiscount
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.accounts.checkDiscount', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.accounts.checkDiscount', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.checkDiscount
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.checkDiscount', 1, 1, 'com.technisys.rubicon.business.accounts.activities.CheckDiscountActivity', 'rub.accounts.checkDiscount');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.checkDiscountPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.checkDiscountPre', 1, 1, 'com.technisys.rubicon.business.accounts.activities.CheckDiscountPreActivity', null);

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.producttype.checkDiscount.list', 'CA|CC', null, 'Rubicon');

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.producttype.checkDiscount.commission', '1', null, 'Rubicon');

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.accounts.checkDiscount.jbpmProcessId', 'demo-1', null, 'Rubicon');