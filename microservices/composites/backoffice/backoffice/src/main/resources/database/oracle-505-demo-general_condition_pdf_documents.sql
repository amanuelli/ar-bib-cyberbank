insert into general_conditions_documents (id_condition, file_name_key, content)
values ((SELECT MAX(id_condition) FROM general_conditions), 'generalConditionDocument.online_banking_service_agreement.pdf', HEXTORAW('0'));

insert into general_conditions_documents (id_condition, file_name_key, content)
values ((SELECT MAX(id_condition) FROM general_conditions), 'generalConditionDocument.terms_and_conditions.pdf', HEXTORAW('0'));

insert into general_conditions_documents (id_condition, file_name_key, content)
values ((SELECT MAX(id_condition) FROM general_conditions), 'generalConditionDocument.deposit_agreement.pdf', HEXTORAW('0'));

insert into general_conditions_documents (id_condition, file_name_key, content)
values ((SELECT MAX(id_condition) FROM general_conditions), 'generalConditionDocument.privacy_notice.pdf', HEXTORAW('0'));

insert into general_conditions_documents (id_condition, file_name_key, content)
values ((SELECT MAX(id_condition) FROM general_conditions), 'generalConditionDocument.privacy_notice_fror_california_residents.pdf', HEXTORAW('0'));

insert into general_conditions_documents (id_condition, file_name_key, content)
values ((SELECT MAX(id_condition) FROM general_conditions), 'generalConditionDocument.electronic_signature_disclosure_and_consent.pdf', HEXTORAW('0'));
