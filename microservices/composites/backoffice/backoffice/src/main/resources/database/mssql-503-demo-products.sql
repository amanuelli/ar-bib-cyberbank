-- This script is not intended to be executed using dbvs. Only for the purpose of creating a new user with products in a certain environment

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- USER VARIABLE SETTING --
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @client varchar(30) = '124'; -- id_environment
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- END USER VARIABLE SETTING --
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

DECLARE @date datetime = getdate();
DECLARE @account varchar(30) = CAST ((SELECT MAX(numero_cuenta) FROM demo_cuentas) AS numeric) + 1;
DECLARE @loan varchar(30) = CAST ((SELECT MAX(numero_prestamo) FROM demo_prestamos) AS numeric) + 1;
DECLARE @ccard varchar(30) = CAST ((SELECT MAX(numero_tarjeta) FROM demo_tarjetas) AS numeric) + 1;
DECLARE @ccardaccount varchar(30) = CAST ((SELECT MAX(cuenta) FROM demo_tarjetas) AS numeric) + 1;

---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
-- INSERTS (Account, Loan & Credit Card) --

-- Account (1)
INSERT INTO demo_cuentas (numero_cuenta, cliente, moneda, tipo, linea_sobregiro, saldo_pendiente)
        VALUES (@account, @client, 'EUR', 'CA', 130000, 0);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 90, @date - 90, 'Initial Balance', '', 0, 15000, 15000);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 85, @date - 85, 'Service payment', '', 1000, 0, 14000);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 81, @date - 81, 'ATM Cash out', '', 2000, 0, 12000);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 74, @date - 74, 'Depósito ATM', '', 0, 3800, 15800);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 57, @date - 57, 'Cash debit - Commerce', '', 700, 0, 15100);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 52, @date - 52, 'Service payment', '', 1100, 0, 14000);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 45, @date - 45, 'Service payment', '', 290, 0, 13710);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 39, @date - 39, 'Service payment', '', 800, 0, 12910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 34, @date - 34, 'Salary', '', 0, 43000, 55910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 33, @date - 33, 'Cash debit - Commerce', '', 1000, 0, 54910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 30, @date - 30, 'Cash debit - Commerce', '', 7000, 0, 47910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 28, @date - 28, 'ATM Cash out', '', 3000, 0, 44910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 26, @date - 26, 'Cash debit - Commerce', '', 900, 0, 44010);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 18, @date - 18, 'Cash debit - Commerce', '', 450, 0, 43560);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 15, @date - 15, 'Cash debit - Commerce', '', 70, 0, 43490);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 11, @date - 11, 'Service payment', '', 2500, 0, 40990);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 7, @date - 7, 'ATM Cash out', '', 4000, 0, 36990);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account, @date - 3, @date - 3, 'Salary', '', 0, 42000, 78990);


-- Loan (1)
INSERT INTO demo_prestamos (numero_prestamo, cliente, tipo, importe_total, fecha_proximo_vencimiento, valor_cuota, moneda)
        VALUES (@loan, @client, 'PA', 50000, @date + 30, 5000, 'EUR');
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date - 90, 5000, 45000, 'Payment fee', 1, 1);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date - 60, 5000, 40000, 'Payment fee', 1, 2);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date - 30, 5000, 35000, 'Payment fee', 1, 3);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date + 30, 5000, 30000, 'Payment fee', 0, 4);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date + 60, 5000, 25000, 'Payment fee', 0, 5);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date + 90, 5000, 20000, 'Payment fee', 0, 6);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date + 120, 5000, 15000, 'Payment fee', 0, 7);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date + 150, 5000, 10000, 'Payment fee', 0, 8);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date + 180, 5000, 5000, 'Payment fee', 0, 9);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan, @date + 210, 5000, 0, 'Payment fee', 0, 10);

UPDATE environment_product SET product_alias = 'Personal Loan' WHERE product_type = 'CA' and id_environment = @client;


-- Card (1)
INSERT INTO demo_tarjetas (numero_tarjeta, cliente, cuenta, saldo_uyp, saldo_usd, vencimiento, cierre, saldo_disponible, limite_credito)
    VALUES (@ccard, @client, @ccardaccount, 12300, 311, @date + 360, @date + 20, 60000, 0);

UPDATE environment_product SET product_alias = 'VISA Platinum' WHERE product_type = 'TC' and id_environment = @client;