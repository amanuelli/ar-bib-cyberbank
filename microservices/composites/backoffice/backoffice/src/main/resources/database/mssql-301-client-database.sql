-- -----------------------------------------------------
-- Table client_users
-- -----------------------------------------------------
CREATE TABLE client_users (
  id_user VARCHAR(254) NOT NULL ,
  address VARCHAR(254) NULL ,
  phone VARCHAR(45) NULL ,
  mobile_phone VARCHAR(45) NULL ,
  document VARCHAR(45) NULL ,
  document_type VARCHAR(20) NULL ,
  document_country VARCHAR(70) NULL ,
  nationality VARCHAR(70) NULL ,
  marital_status VARCHAR(20) NULL ,
  gender VARCHAR(1) NULL ,
  ocupation VARCHAR(254) NULL ,
  PRIMARY KEY (id_user) 
);

-- -----------------------------------------------------
-- Table client_banks
-- -----------------------------------------------------
CREATE TABLE client_banks (
    direccionbic    VARCHAR(50),
    nombrebanco     VARCHAR(100),
    PRIMARY KEY (direccionbic)
);