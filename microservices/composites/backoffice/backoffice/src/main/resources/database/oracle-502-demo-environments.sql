-- 3 nuevos ambientes, uno argentino, otro brasilero y otro EEUU
INSERT INTO environments
 (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
 VALUES ('Juan Corporate', 'active', 'corporate', 0, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'advanced', 'daily', 1, '900');

INSERT INTO clients
 (name, id_client)
 VALUES ('Juan Corporate', '900');

INSERT INTO ENVIRONMENT_CLIENTS
 (id_environment, id_client)
 VALUES ((SELECT MAX(id_environment) FROM environments), '900');

INSERT INTO environments
 (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
 VALUES ('Joao Corporate', 'active', 'corporate', 0, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'advanced', 'daily', 1, '901');

INSERT INTO clients
 (name, id_client)
 VALUES ('Joao Corporate', '901');

INSERT INTO ENVIRONMENT_CLIENTS
 (id_environment, id_client)
 VALUES ((SELECT MAX(id_environment) FROM environments), '901');

INSERT INTO environments
 (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
 VALUES ('John Corporate', 'active', 'corporate', 0, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'advanced', 'daily', 1, '902');

INSERT INTO clients
 (name, id_client)
 VALUES ('John Corporate', '902');

INSERT INTO ENVIRONMENT_CLIENTS
 (id_environment, id_client)
 VALUES ((SELECT MAX(id_environment) FROM environments), '902');

INSERT INTO environments
 (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
 VALUES ('Juan Retail', 'active', 'retail', 0, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'simple', 'daily', 1, '903');

INSERT INTO clients
 (name, id_client)
 VALUES ('Juan Retail', '903');

INSERT INTO ENVIRONMENT_CLIENTS
 (id_environment, id_client)
 VALUES ((SELECT MAX(id_environment) FROM environments), '903');

INSERT INTO environments
 (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
 VALUES ('Joao Retail', 'active', 'retail', 0, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'simple', 'daily', 1, '904');

INSERT INTO clients
 (name, id_client)
 VALUES ('Joao Retail', '904');

INSERT INTO ENVIRONMENT_CLIENTS
 (id_environment, id_client)
 VALUES ((SELECT MAX(id_environment) FROM environments), '904');

INSERT INTO environments
 (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
 VALUES ('John Retail', 'active', 'retail', 0, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'simple', 'daily', 1, '905');

INSERT INTO clients
 (name, id_client)
 VALUES ('John Retail', '905');

INSERT INTO ENVIRONMENT_CLIENTS
 (id_environment, id_client)
 VALUES ((SELECT MAX(id_environment) FROM environments), '905');

-- Creo un usuario para cada uno de los 3 ambientes anteriores
 INSERT INTO users
  (id_user, email, id_user_status, deleted, first_name, last_name, password, lang, logged_in, creation_date, id_seal, mobile_number, trustfull, last_password_change, username, document, document_type, document_country)
  VALUES ('f6cadaap64fe4hjead71456813d41afo', 'juan@technisys.com', 'active', 0, 'Juan', 'Demo', '5f4dcc3b5aa765d61d8327deb882cf99', 'es', 0, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 8, '59914007050', 1, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'juan@technisys.com', '27687110', 'DNI', 'AR');

 INSERT INTO users
  (id_user, email, id_user_status, deleted, first_name, last_name, password, lang, logged_in, creation_date, id_seal, mobile_number, trustfull, last_password_change, username, document, document_type, document_country)
  VALUES ('f6cadaap64fe4hjead71456813d41af1', 'john@technisys.com', 'active', 0, 'John', 'Demo', '5f4dcc3b5aa765d61d8327deb882cf99', 'en', 0, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 8, '59914007051', 1, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'john@technisys.com', '27687111', 'PAS', 'US');

 INSERT INTO users
  (id_user, email, id_user_status, deleted, first_name, last_name, password, lang, logged_in, creation_date, id_seal, mobile_number, trustfull, last_password_change, username, document, document_type, document_country)
  VALUES ('f6cadaap64fe4hjead71456813d41af2', 'joao@technisys.com', 'active', 0, 'Joao', 'Demo', '5f4dcc3b5aa765d61d8327deb882cf99', 'pt', 0, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 8, '59914007052', 1, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'joao@technisys.com', '27687112', 'PAS', 'BR');


INSERT INTO environment_users
  (id_user, id_environment, id_user_status, signature_level, creation_date, enabled_channels)
 SELECT 'f6cadaap64fe4hjead71456813d41afo', id_environment, 'active', 'A', TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'),'frontend,phonegap' FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');

INSERT INTO environment_users
  (id_user, id_environment, id_user_status, signature_level, creation_date, enabled_channels)
 SELECT 'f6cadaap64fe4hjead71456813d41af1', id_environment, 'active', 'A', TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'),'frontend,phonegap' FROM environments WHERE name IN ('John Corporate', 'John Retail');

INSERT INTO environment_users
  (id_user, id_environment, id_user_status, signature_level, creation_date, enabled_channels)
 SELECT 'f6cadaap64fe4hjead71456813d41af2', id_environment, 'active', 'A', TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS'),'frontend,phonegap' FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');

-- grupos y permisos

INSERT INTO groups
  (id_environment, name, blocked, deleted, description)
 SELECT id_environment, 'Administrator', 0, 0, 'Administrators' FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

INSERT INTO group_users (id_group, id_user)
 SELECT G.id_group, 'f6cadaap64fe4hjead71456813d41afo'
 FROM groups G, environments E
 WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'Juan Retail');

INSERT INTO group_users (id_group, id_user)
 SELECT G.id_group, 'f6cadaap64fe4hjead71456813d41af1'
 FROM groups G, environments E
 WHERE G.id_environment = E.id_environment AND E.name IN ('John Corporate', 'John Retail');

INSERT INTO group_users (id_group, id_user)
 SELECT G.id_group, 'f6cadaap64fe4hjead71456813d41af2'
 FROM groups G, environments E
 WHERE G.id_environment = E.id_environment AND E.name IN ('Joao Corporate', 'Joao Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
 SELECT G.id_group, P.id_permission, 'ALL_CA'
 FROM groups G, environments E, permissions P
 WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail')
  AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

INSERT INTO group_permissions(id_group, id_permission, target)
 SELECT G.id_group, P.id_permission, 'ALL_CC'
 FROM groups G, environments E, permissions P
 WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail')
  AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

INSERT INTO group_permissions(id_group, id_permission, target)
 SELECT G.id_group, 'product.read', 'ALL_PA'
 FROM groups G, environments E
 WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
 SELECT G.id_group, 'product.read', 'ALL_PF'
 FROM groups G, environments E
 WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
 SELECT G.id_group, 'product.read', 'ALL_PI'
 FROM groups G, environments E
 WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
 SELECT G.id_group, 'product.read', 'ALL_TC'
 FROM groups G, environments E
 WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
 SELECT G.id_group, P.id_permission, 'NONE'
 FROM groups G, environments E, permissions P
 WHERE G.id_environment = E.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail')
  AND P.id_permission not in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

--    Bitters 30 June 2020: Signatures & Caps must be inserted into Limits MS database!!!!

---- esquemas de firma
--
--INSERT INTO signatures (id_environment, signature_group, signature_type)
-- SELECT id_environment, 'A', 'ADM'
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
--INSERT INTO signatures (id_environment, signature_group, signature_type)
-- SELECT id_environment, 'A', 'AMOUNT'
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
--INSERT INTO signatures (id_environment, signature_group, signature_type)
-- SELECT id_environment, 'A', 'NO_AMOUNT'
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
---- limites de usuario
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'all', 'f6cadaap64fe4hjead71456813d41afo', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'frontend', 'f6cadaap64fe4hjead71456813d41afo', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'phonegap', 'f6cadaap64fe4hjead71456813d41afo', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'facebook', 'f6cadaap64fe4hjead71456813d41afo', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'payments', 'f6cadaap64fe4hjead71456813d41afo', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'sms', 'f6cadaap64fe4hjead71456813d41afo', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'twitter', 'f6cadaap64fe4hjead71456813d41afo', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'all', 'f6cadaap64fe4hjead71456813d41af1', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('John Corporate', 'John Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'frontend', 'f6cadaap64fe4hjead71456813d41af1', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('John Corporate', 'John Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'phonegap', 'f6cadaap64fe4hjead71456813d41af1', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('John Corporate', 'John Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'facebook', 'f6cadaap64fe4hjead71456813d41af1', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('John Corporate', 'John Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'payments', 'f6cadaap64fe4hjead71456813d41af1', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('John Corporate', 'John Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'sms', 'f6cadaap64fe4hjead71456813d41af1', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('John Corporate', 'John Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'twitter', 'f6cadaap64fe4hjead71456813d41af1', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('John Corporate', 'John Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'all', 'f6cadaap64fe4hjead71456813d41af2', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'frontend', 'f6cadaap64fe4hjead71456813d41af2', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'phonegap', 'f6cadaap64fe4hjead71456813d41af2', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'facebook', 'f6cadaap64fe4hjead71456813d41af2', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'payments', 'f6cadaap64fe4hjead71456813d41af2', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'sms', 'f6cadaap64fe4hjead71456813d41af2', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'twitter', 'f6cadaap64fe4hjead71456813d41af2', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');
--
---- limites de ambiente
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'all', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'frontend', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'phonegap', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'facebook', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'payments', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'sms', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
-- SELECT id_environment, 'twitter', 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments WHERE name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail');
--
---- firmas
--
--INSERT INTO caps_for_signature (id_environment, channel, id_signature, frequency, maximum, used, used_last_reset)
-- SELECT S.id_environment, 'all', S.id_signature, 'daily', -1, 0.000, TO_DATE('2018-12-20 12:00:00','yyyy-mm-dd HH24:MI:SS')
-- FROM environments E, signatures S WHERE E.id_environment = S.id_environment AND E.name IN ('Juan Corporate', 'John Corporate', 'Joao Corporate', 'Juan Retail', 'John Retail', 'Joao Retail') AND S.signature_type = 'AMOUNT';

-- widgets

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
 SELECT 'f6cadaap64fe4hjead71456813d41afo', id_environment, 'accounts', 1, 1
 FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
 SELECT 'f6cadaap64fe4hjead71456813d41afo', id_environment, 'creditCards', 1, 1
 FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
 SELECT 'f6cadaap64fe4hjead71456813d41afo', id_environment, 'loans', 1, 3
 FROM environments WHERE name IN ('Juan Corporate', 'Juan Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
 SELECT 'f6cadaap64fe4hjead71456813d41af1', id_environment, 'accounts', 1, 1
 FROM environments WHERE name IN ('John Corporate', 'John Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
 SELECT 'f6cadaap64fe4hjead71456813d41af1', id_environment, 'creditCards', 1, 1
 FROM environments WHERE name IN ('John Corporate', 'John Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
 SELECT 'f6cadaap64fe4hjead71456813d41af1', id_environment, 'loans', 1, 3
 FROM environments WHERE name IN ('John Corporate', 'John Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
 SELECT 'f6cadaap64fe4hjead71456813d41af2', id_environment, 'accounts', 1, 1
 FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
 SELECT 'f6cadaap64fe4hjead71456813d41af2', id_environment, 'creditCards', 1, 1
 FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
 SELECT 'f6cadaap64fe4hjead71456813d41af2', id_environment, 'loans', 1, 3
 FROM environments WHERE name IN ('Joao Corporate', 'Joao Retail');
