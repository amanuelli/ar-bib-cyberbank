INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('plazo', 1, NULL, 'select', 1, 3, NULL, '30|60|90|180|360', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('importe', 1, NULL, 'amount', 1, 0, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('usoDeFirmas', 1, NULL, 'select', 1, 2, NULL, 'No aplica|Indistinto|Conjunta', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularFax', 1, NULL, 'short_text', 1, 18, NULL, NULL, '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDebito', 1, NULL, 'productList', 1, 1, NULL, 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('1_cotFechaNac', 1, NULL, 'date', 1, 55, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularPais', 1, NULL, 'short_text', 1, 14, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularSexo', 1, NULL, 'select', 0, 26, NULL, 'F|M', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularCalle', 1, NULL, 'short_text', 1, 9, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularEMail', 1, NULL, 'short_text', 0, 57, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularNumero', 1, NULL, 'short_text', 1, 10, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaIntereses', 1, NULL, 'productList', 1, 4, NULL, 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('2_cotECivilFecha', 1, NULL, 'date', 0, 25, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('3_cotLabFechaIni', 1, NULL, 'date', 0, 39, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularCelular', 1, NULL, 'short_text', 1, 17, NULL, NULL, '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularNombres', 1, NULL, 'short_text', 1, 8, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('4_conyCotFechaNac', 1, NULL, 'date', 0, 52, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularTelefono', 1, NULL, 'short_text', 1, 16, NULL, NULL, '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularApellidos', 1, NULL, 'short_text', 1, 7, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLocalidad', 1, NULL, 'short_text', 1, 13, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularResidente', 1, NULL, 'select', 1, 15, NULL, 'Si|No', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularSeparador', 1, NULL, 'subtitle', 1, 6, 'Datos personales cotitular', NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularApartamento', 1, NULL, 'short_text', 1, 11, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularEstadoCivil', 1, NULL, 'select', 0, 24, NULL, 'Soltero|Casado|Divorciado|Viudo', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugeCotitularEmail', 1, NULL, 'short_text', 0, 51, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularCodigoPostal', 1, NULL, 'short_text', 0, 12, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralCalle', 1, NULL, 'short_text', 0, 32, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralCargo', 1, NULL, 'short_text', 0, 38, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralEMail', 1, NULL, 'short_text', 0, 41, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralRubro', 1, NULL, 'short_text', 0, 31, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularNacionalidad', 1, NULL, 'short_text', 1, 20, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugueCotitularLugar', 1, NULL, 'short_text', 0, 53, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularCuotaHipoteca', 1, NULL, 'amount', 0, 22, NULL, 'UYP|USD|UI', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralNumero', 1, NULL, 'short_text', 0, 33, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularNumeroDeHijos', 1, NULL, 'short_text', 0, 28, NULL, NULL, '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugeCotitularCelular', 1, NULL, 'short_text', 0, 50, NULL, NULL, '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugeCotitularNombres', 1, NULL, 'short_text', 0, 46, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralEmpresa', 1, NULL, 'short_text', 0, 30, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularPersonasACargo', 1, NULL, 'short_text', 0, 27, NULL, NULL, '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularTipoDeVivienda', 1, NULL, 'select', 0, 21, NULL, 'Propia|Alquilada|Familiar', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugeCotitularTelefono', 1, NULL, 'short_text', 0, 49, NULL, NULL, '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularAlquilerImporte', 1, NULL, 'amount', 0, 23, NULL, 'UYP|USD|UI', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralTelefono', 1, NULL, 'short_text', 0, 40, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('instruccionesVencimiento', 1, NULL, 'select', 1, 5, NULL, 'Renovar|Cancelar', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugeCotitularApellidos', 1, NULL, 'short_text', 0, 45, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugeCotitularSeparador', 1, NULL, 'subtitle', 1, 44, 'Datos personales del cónyuge del cotitular', NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralLocalidad', 1, NULL, 'short_text', 0, 36, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralProfesion', 1, NULL, 'short_text', 0, 37, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralSeparador', 1, NULL, 'subtitle', 1, 29, 'Datos laborales del cotitular', NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLugarDeNacimiento', 1, NULL, 'short_text', 1, 19, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralApartamento', 1, NULL, 'short_text', 0, 34, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralCodigoPostal', 1, NULL, 'short_text', 0, 35, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugeCotitularDocumentoTipo', 1, NULL, 'select', 0, 47, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugueCotitularNacionalidad', 1, NULL, 'short_text', 0, 54, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralIngresosNetos', 1, NULL, 'amount', 0, 42, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cotitularLaboralOtrosIngresos', 1, NULL, 'amount', 0, 43, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conyugeCotitularDocumentoNumero', 1, NULL, 'short_text', 0, 48, NULL, NULL, NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('plazo', 2, NULL, 'short_text', 1, 3, NULL, NULL, '([1-9][0-9]*)', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuenta', 2, NULL, 'productList', 1, 1, NULL, 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 2, NULL, 'disclaimer', 1, 4, 'Texto parametrizable desde el backoffice que debe ser aceptaco a través de un check antes de enviarse al backoffice.', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('limiteOMonto', 2, NULL, 'amount', 1, 2, NULL, 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('plazo', 4, NULL, 'select', 1, 2, '', '36|48', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('incluyeSeguro', 4, NULL, 'select', 1, 5, '', 'Si|No', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('valorNetoDelBien', 4, NULL, 'short_text', 1, 0, '', '', '([1-9][0-9]*(,[0-9]+)?)', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('capitalAFinanciar', 4, NULL, 'amount', 1, 1, '', 'USD|UI', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('periodicidadDePago', 4, NULL, 'select', 1, 3, '', 'Mensual|Trimestral', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('informacionAdicional', 4, NULL, 'long_text', 1, 4, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('plazo', 5, NULL, 'select', 1, 2, '', '30|60|180', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('moneda', 5, NULL, 'select', 1, 1, '', 'UYP|USD|UI', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('destino', 5, NULL, 'select', 1, 3, '', 'Compra de mercadería|Giro al exterior|Pago a proveedores|Compra de maquinaria|Compra de ganado|Compra de equipamiento|Pre-financiación de exportaciones Circ. 1456|Otros', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('destinoOtros', 5, NULL, 'short_text', 1, 4, '', '', NULL, 'destino', 'Otros');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('primerNombre', 6, NULL, 'short_text', 1, 12, NULL, NULL, NULL, 'requiereAdicional', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('segundoNombre', 6, NULL, 'short_text', 0, 13, NULL, NULL, NULL, 'requiereAdicional', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDeCuenta', 6, NULL, 'productList', 1, 0, NULL, 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('primerApellido', 6, NULL, 'short_text', 1, 14, NULL, NULL, NULL, 'requiereAdicional', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('segundoApellido', 6, NULL, 'short_text', 0, 15, NULL, NULL, NULL, 'requiereAdicional', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDeDocumento', 6, NULL, 'select', 1, 10, NULL, NULL, NULL, 'requiereAdicional', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoPersonaLegal', 6, NULL, 'type_legal_person', 0, 16, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDeDocumento', 6, NULL, 'short_text', 1, 11, NULL, NULL, NULL, 'requiereAdicional', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('requiereAdicional', 6, NULL, 'select', 1, 20, 'No', 'No|Si', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('limiteDiarioDePago', 6, NULL, 'short_text', 1, 6, NULL, NULL, '([0-9]+(,[0-9]+)?)', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('limiteDiarioRetiro', 6, NULL, 'short_text', 1, 5, NULL, NULL, '([0-9]+(,[0-9]+)?)', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaVisaFisica', 6, NULL, 'select', 1, 18, NULL, 'Clásica|Mileage plus|Club card|Gauchito|Diamante|Smiles|Gaviotas|Clásica - AUDAVI|Tres cruces|Pétalos', NULL, 'tipoDeTarjetaFisica', 'Visa débito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloAdicional', 6, NULL, 'subtitle', 1, 9, 'Adicional', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dependenciaDeRetiro', 6, NULL, 'branches', 1, 4, NULL, 'MALVIN', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDeTarjetaFisica', 6, NULL, 'select', 1, 17, NULL, 'Visa débito', NULL, 'tipoPersonaLegal', 'Física');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDebitoDeCostos', 6, NULL, 'productList', 1, 3, NULL, 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaVisaJuridica', 6, NULL, 'select', 1, 2, NULL, 'Clásica|Mileage plus|Club card|Gauchito|Diamante|Smiles|Gaviotas|Clásica - AUDAVI|Tres cruces|Pétalos', NULL, 'tipoDeTarjetaJuridica', 'Visa débito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDeTarjetaJuridica', 6, NULL, 'select', 1, 1, NULL, 'Automatismos|Banred|Visa débito', NULL, 'tipoPersonaLegal', 'Jurídica');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('limiteDiarioDeCompraVisaFisica', 6, NULL, 'short_text', 1, 19, NULL, NULL, '([0-9]+(,[0-9]+)?)', 'tipoDeTarjetaFisica', 'Visa débito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('limiteDiarioDeCompraVisaJuridica', 6, NULL, 'short_text', 1, 7, NULL, NULL, '([0-9]+(,[0-9]+)?)', 'tipoDeTarjetaJuridica', 'Visa débito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('liquido', 7, NULL, 'amount', 1, 0, '', 'UYP|USD|UI', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('destinoDeFondos', 7, NULL, 'select', 1, 3, '', 'Compra de mercadería|Giro al exterior|Pago a proveedores|Compra de maquinaria|Compra de ganado|Compra de equipamiento|Otros', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cantidadDeCuotas', 7, NULL, 'select', 1, 1, '', '3|6|12', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('destinoDeFondosOtros', 7, NULL, 'short_text', 1, 5, '', '', NULL, 'destinoDeFondos', 'Otros');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDeDebitoDeCuota', 7, NULL, 'productList', 0, 2, '', 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('compensacionA', 8, NULL, 'select', 0, 4, NULL, 'A|B|C|D|E|F|G|H', NULL, 'tipoDeCompensacion', 'Particular');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('compensacionB', 8, NULL, 'select', 0, 20, NULL, 'A|B|C|D|E|F|G|H', NULL, 'tipoDeCompensacion', 'Particular');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('compensacionC', 8, NULL, 'select', 0, 21, NULL, 'A|B|C|D|E|F|G|H', NULL, 'tipoDeCompensacion', 'Particular');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('compensacionD', 8, NULL, 'select', 0, 22, NULL, 'A|B|C|D|E|F|G|H', NULL, 'tipoDeCompensacion', 'Particular');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('compensacionE', 8, NULL, 'select', 0, 23, NULL, 'A|B|C|D|E|F|G|H', NULL, 'tipoDeCompensacion', 'Particular');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('compensacionF', 8, NULL, 'select', 0, 24, NULL, 'A|B|C|D|E|F|G|H', NULL, 'tipoDeCompensacion', 'Particular');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('compensacionG', 8, NULL, 'select', 0, 25, NULL, 'A|B|C|D|E|F|G|H', NULL, 'tipoDeCompensacion', 'Particular');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('compensacionH', 8, NULL, 'select', 0, 26, NULL, 'A|B|C|D|E|F|G|H', NULL, 'tipoDeCompensacion', 'Particular');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDeudoraA', 8, NULL, 'productList', 0, 1, NULL, 'CC', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDeudoraB', 8, NULL, 'productList', 0, 5, NULL, 'CC', NULL, 'cuentaDeudoraA', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDeudoraC', 8, NULL, 'productList', 0, 6, NULL, 'CC', NULL, 'cuentaDeudoraB', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDeudoraD', 8, NULL, 'productList', 0, 7, NULL, 'CC', NULL, 'cuentaDeudoraC', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDeudoraE', 8, NULL, 'productList', 0, 8, NULL, 'CC', NULL, 'cuentaDeudoraD', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDeudoraF', 8, NULL, 'productList', 0, 9, NULL, 'CC', NULL, 'cuentaDeudoraE', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDeudoraG', 8, NULL, 'productList', 0, 10, NULL, 'CC', NULL, 'cuentaDeudoraF', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDeudoraH', 8, NULL, 'productList', 0, 11, NULL, 'CC', NULL, 'cuentaDeudoraG', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaAcreedoraA', 8, NULL, 'productList', 0, 2, NULL, 'CC', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaAcreedoraB', 8, NULL, 'productList', 0, 12, NULL, 'CC', NULL, 'cuentaAcreedoraA', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaAcreedoraC', 8, NULL, 'productList', 0, 13, NULL, 'CC', NULL, 'cuentaAcreedoraB', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaAcreedoraD', 8, NULL, 'productList', 0, 14, NULL, 'CC', NULL, 'cuentaAcreedoraC', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaAcreedoraE', 8, NULL, 'productList', 0, 15, NULL, 'CC', NULL, 'cuentaAcreedoraD', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaAcreedoraF', 8, NULL, 'productList', 0, 16, NULL, 'CC', NULL, 'cuentaAcreedoraE', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaAcreedoraG', 8, NULL, 'productList', 0, 17, NULL, 'CC', NULL, 'cuentaAcreedoraF', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaAcreedoraH', 8, NULL, 'productList', 0, 18, NULL, 'CC', NULL, 'cuentaAcreedoraG', NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDeCompensacion', 8, NULL, 'select', 1, 19, NULL, 'Entre todas|Particular', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('momentoDeCompensacion', 8, NULL, 'select', 1, 3, NULL, 'En línea|Al cierre', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuenta', 9, NULL, 'productList', 1, 0, NULL, 'CC', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombre', 9, NULL, 'short_text', 0, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('sucursal', 9, NULL, 'branches', 1, 18, NULL, NULL, NULL, 'lugarDeRetiro', 'Sucursal');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('documento', 9, NULL, 'short_text', 0, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('domicilio', 9, NULL, 'short_text', 1, 14, NULL, NULL, NULL, 'lugarDeRetiro', 'Envío a domicilio');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDeCheque', 9, NULL, 'select', 1, 1, NULL, 'Normal sin cruzar|Normal cruzado|Diferido sin cruzar|Diferido cruzado|Fanfold', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('lugarDeRetiro', 9, NULL, 'select', 1, 4, NULL, 'Envío a domicilio|Sucursal', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('primerApellido', 9, NULL, 'short_text', 0, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('segundoApellido', 9, NULL, 'short_text', 0, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDeDocumento', 9, NULL, 'short_text', 0, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDebitoDeCosto', 9, NULL, 'productList', 1, 5, NULL, 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('terminosYCondiciones', 9, NULL, 'disclaimer', 1, 12, 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry''s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has s', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cantidadDeChequesFanfold', 9, NULL, 'short_text', 1, 3, NULL, NULL, '([5-9]00)|([1-9][0-9]+00)', 'tipoDeCheque', 'Fanfold');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cantidadDeChequerasNormalCruzado', 9, NULL, 'short_text', 1, 15, NULL, NULL, '[1-9][0-9]*', 'tipoDeCheque', 'Normal cruzado');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('autorizadoARetirarRecibirChequera', 9, NULL, 'subtitle', 0, 6, 'Autorizado a retirar/recibir chequera', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cantidadDeChequerasDiferidoCruzado', 9, NULL, 'short_text', 1, 17, NULL, NULL, '[1-9][0-9]*', 'tipoDeCheque', 'Diferido cruzado');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cantidadDeChequerasNormalSinCruzar', 9, NULL, 'short_text', 1, 2, NULL, NULL, '([1-9][0-9]*)', 'tipoDeCheque', 'Normal sin cruzar');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cantidadDeChequerasDiferidoSinCruzar', 9, NULL, 'short_text', 1, 16, NULL, NULL, '[1-9][0-9]*', 'tipoDeCheque', 'Diferido sin cruzar');

INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('cuentaDebito', 10, NULL, 'productList', 1, 1, 'CC|CA', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('gastosEnElExteriorACargoDe', 10, NULL, 'select', 1, 2, 'OUR|SHA|BEN', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('subtituloDatosDelCredito', 10, NULL, 'subtitle', 1, 3, NULL, NULL, NULL, NULL, 'Datos del crédito');
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('bancoDestino', 10, NULL, 'banks', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('cuentaCredito', 10, NULL, 'short_text', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('nombreApellidoDestinatario', 10, NULL, 'short_text', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('importe', 10, NULL, 'amount', 1, 0, 'USD|EUR', '(([1-9][0-9]*)|([1-9]\d{0,2}(\.\d{3})*))(,\d*[1-9]\d*)?', NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('ciudadDestino', 10, NULL, 'short_text', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('paisDestino', 10, NULL, 'short_text', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('conceptoDelPago', 10, NULL, 'long_text', 0, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('subtituloBancoIntermediario', 10, NULL, 'subtitle', 1, 10, NULL, NULL, NULL, NULL, 'Banco intermediario');
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('bancoIntermediario', 10, NULL, 'short_text', 0, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('codigoSWIFTBancoIntermediario', 10, NULL, 'short_text', 0, 12, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('ciudadIntermediario', 10, NULL, 'short_text', 0, 13, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('paisBeneficiario', 10, NULL, 'short_text', 0, 14, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(ID_FIELD, ID_FORM, DESCRIPTION, FIELD_TYPE, MANDATORY, ORDINAL, POSSIBLE_VALUES, VALIDATION_REGEXP, DEPENDS_ON_ID_FIELD, DEPENDS_ON_VALUE, DEFAULT_VALUE)
  VALUES('terminosYCondiciones', 10, NULL, 'disclaimer', 1, 15, NULL, NULL, NULL, NULL, 'Autorizo(amos) que con cargo a la cuenta y rubro indicados, se sirvan dar cumplimiento a la solicitud  ingresada, sin responsabilidad para el Banco por errores o demoras originadas fuera de su control. A los efectos de dar cumplimiento a los requerimientos de la entidad receptora y/o intermediaria del giro, autorizo(amos) al Banco a facilitar los datos identificatorios referidos a mi persona y/o de la cuenta origen de la transferencia, por lo que relevo(amos) al Banco de su obligaci&oacute;n de sigilo bancario en los t&eacute;rminos del Art. 25 de la Ley 15.322.');



INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('importe', 11, NULL, 'amount', 1, 0, '', 'USD|EUR|PU', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('bancoDestino', 11, NULL, 'select', 1, 4, '', 'Banco Comercial|Banco Santander|República AFAP|Banco República', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDebito', 11, NULL, 'productList', 1, 1, '', 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ciudadDestino', 11, NULL, 'short_text', 1, 7, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaCredito', 11, NULL, 'short_text', 1, 5, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conceptoDelPago', 11, NULL, 'long_text', 0, 8, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosDelCredito', 11, NULL, 'subtitle', 0, 3, 'Datos del crédito', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDebitoDeGastos', 11, NULL, 'productList', 0, 2, '', 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('terminosYCondiciones', 11, NULL, 'disclaimer', 1, 9, 'Es un texto que el Banco agrega y el usuario tiene que aceptar para poder enviar la transacción.', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombreYApellidoDestinatrio', 11, NULL, 'short_text', 1, 6, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('apto', 12, NULL, 'short_text', 1, 26, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('piso', 12, NULL, 'short_text', 0, 25, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('calle', 12, NULL, 'short_text', 1, 22, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('entre', 12, NULL, 'short_text', 1, 24, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('barrio', 12, NULL, 'short_text', 1, 20, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('detalle', 12, NULL, 'long_text', 0, 34, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('importe', 12, NULL, 'amount', 1, 1, '', 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tvCable', 12, NULL, 'select', 1, 8, '', 'Si|No', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidad', 12, NULL, 'short_text', 1, 19, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('otrasDeudas', 12, NULL, 'short_text', 1, 35, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('afiliadoAFAP', 12, NULL, 'select', 1, 7, '', 'Si|No', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('departamento', 12, NULL, 'short_text', 1, 18, 'Montevideo', 'Artigas|Canelones|Cerro Largo|Colonia|Durazno|Flores|Florida|Lavalleja|Maldonado|Montevideo|Paysandú|Río Negro|Rivera|Rocha|Salto|San José|Soriano|Tacuarembó|Treinta y Tres', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('padronNumero', 12, NULL, 'short_text', 1, 14, '', '', '[0-9]*', 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('prestamoTipo', 12, NULL, 'select', 1, 0, '', 'Amortizable|Hipotecario|Automotor', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('areaEdificada', 12, NULL, 'short_text', 1, 17, '', '', '[1-9][0-9]*', 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('areaDelTerreno', 12, NULL, 'short_text', 1, 16, '', '', '[1-9][0-9]*', 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('empresaTVCable', 12, NULL, 'short_text', 1, 9, '', '', NULL, 'tvCable', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDePuerta', 12, NULL, 'short_text', 0, 23, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDeVivienda', 12, NULL, 'select', 1, 15, '', 'Casa|Apartamento', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('destinoDeFondos', 12, NULL, 'short_text', 1, 4, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('seccionJudicial', 12, NULL, 'short_text', 0, 21, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cantidadDeCuotas', 12, NULL, 'short_text', 1, 2, '', '', '[1-9][0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ocupacionLinderos', 12, NULL, 'short_text', 1, 32, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('destinoDelInmueble', 12, NULL, 'short_text', 1, 30, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('separadorHipotecario', 12, NULL, 'subtitle', 1, 6, 'Préstamo hipotecario', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('actividadesAnteriores', 12, NULL, 'short_text', 1, 11, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('otrasFuentesDeIngreso', 12, NULL, 'short_text', 1, 10, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('antiguedadConstruccion', 12, NULL, 'short_text', 1, 27, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('linderosAumentanRiesgo', 12, NULL, 'select', 1, 31, '', 'Si|No', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('valorProbableInteresado', 12, NULL, 'short_text', 1, 28, '', '', '[1-9][0-9]*', 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaAutorizadaADebitar', 12, NULL, 'productList', 0, 3, '', 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('descripcionDeComodidades', 12, NULL, 'long_text', 1, 29, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('otrosTelefonosDeContacto', 12, NULL, 'short_text', 0, 12, '', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('separadorDatosDelInmueble', 12, NULL, 'subtitle', 1, 13, 'Datos del inmueble ofrecido en hipoteca', '', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('techosParedesCombustibles', 12, NULL, 'select', 1, 33, '', 'Si|No', NULL, 'prestamoTipo', 'Hipotecario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombres', 13, NULL, 'short_text', 1, 3, NULL, NULL, '[a-zA-Z ]{0,27}', 'motivoDeLaSolicitud', 'Plástico por datos mal grabados');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjeta', 13, NULL, 'productList', 1, 1, NULL, 'VISA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('apellidos', 13, NULL, 'short_text', 1, 4, NULL, NULL, '[a-zA-Z ]{0,27}', 'motivoDeLaSolicitud', 'Plástico por datos mal grabados');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('advertencia1', 13, NULL, 'note', 1, 5, 'Si el motivo de la solicitud es robo o pérdida, debe realizar la solicitud al SAC. No se acepta por esta vía esta causal.', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('advertencia2', 13, NULL, 'note', 1, 6, 'La tarjeta actual se bloqueará y deberá esperar recibir el nuevo plástico en el domicilio.', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('motivoDeLaSolicitud', 13, NULL, 'select', 1, 2, NULL, 'Plástico retenido|Plástico por datos mal grabados|Plástico por deterioro|Plástico por renovación no recibida', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('calle', 14, NULL, 'short_text', 1, 16, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cargo', 14, NULL, 'short_text', 1, 20, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('email', 14, NULL, 'short_text', 1, 23, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('marca', 14, NULL, 'select', 0, 1, NULL, 'MASTER|VISA|AMEX Centurión|AMEX Blue Box', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('rubro', 14, NULL, 'short_text', 1, 15, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('empresa', 14, NULL, 'short_text', 1, 14, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('telefono', 14, NULL, 'short_text', 1, 22, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidad', 14, NULL, 'short_text', 1, 18, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('profesion', 14, NULL, 'short_text', 1, 19, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cierreVisa', 14, NULL, 'select', 0, 8, NULL, NULL, NULL, 'marca', 'VISA');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('alcanceVisa', 14, NULL, 'select', 0, 3, NULL, 'Local|Regional|Internacional|Oro', NULL, 'marca', 'VISA');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fechaInicio', 14, NULL, 'date', 1, 21, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cierreMaster', 14, NULL, 'select', 0, 29, NULL, NULL, NULL, 'marca', 'MASTER');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('codigoPostal', 14, NULL, 'short_text', 0, 17, NULL, NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaVisa', 14, NULL, 'select', 0, 6, NULL, 'Clásica|Mileage plus|Club card|Gauchito|Diamante|Smiles|Gaviotas|Clásica - AUDAVI|Tres cruces|Pétalos', NULL, 'marca', 'VISA');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('alcanceMaster', 14, NULL, 'select', 0, 2, NULL, 'Regional|Internacional|Oro', NULL, 'marca', 'MASTER');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ingresosNetos', 14, NULL, 'amount', 1, 24, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ingresosOtros', 14, NULL, 'amount', 1, 25, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroLaboral', 14, NULL, 'short_text', 1, 32, NULL, NULL, '[0-9]*', 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroTarjeta', 14, NULL, 'productList', 0, 0, NULL, 'VISA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoOperacion', 14, NULL, 'select', 1, 9, NULL, 'Cambio Programa|Cambio límite de crédito|Traspaso de línea de crédito', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaborales', 14, NULL, 'subtitle', 1, 13, 'Datos laborales', NULL, NULL, 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaMaster', 14, NULL, 'select', 0, 5, NULL, 'Clásica|Mileage plus|Club card|Smiles|Pétalos|TOTO', NULL, 'marca', 'MASTER');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('limiteDeCredito', 14, NULL, 'amount', 1, 12, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'tipoOperacion', 'Cambio límite de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cierreAmexBlueBox', 14, NULL, 'select', 0, 31, NULL, NULL, NULL, 'marca', 'AMEX Blue Box');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('alcanceAmexBlueBox', 14, NULL, 'select', 0, 27, NULL, 'Internacional|Gold', NULL, 'marca', 'AMEX Blue Box');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cierreAmexCenturion', 14, NULL, 'select', 0, 30, NULL, NULL, NULL, 'marca', 'AMEX Centurión');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaAmexBlueBox', 14, NULL, 'select', 0, 28, NULL, 'Clásica|Club card|Diamante|Gaviotas', NULL, 'marca', 'AMEX Blue Box');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('alcanceAmexCenturion', 14, NULL, 'select', 0, 4, NULL, 'Green Compra|Green Crédito|Gold Compra|Gold Crédito', NULL, 'marca', 'AMEX Centurión');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('montoLimiteTraspasar', 14, NULL, 'amount', 1, 11, NULL, 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'tipoOperacion', 'Traspaso de línea de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaAmexCenturion', 14, NULL, 'select', 0, 7, NULL, 'Clásica|Club card|Diamante|Gaviotas', NULL, 'marca', 'AMEX Centurión');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('traspasoLimiteCredito', 14, NULL, 'productList', 0, 10, NULL, 'VISA', NULL, 'tipoOperacion', 'Traspaso de línea de crédito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('marca', 15, NULL, 'select', 1, 3, NULL, 'VISA|MASTER|AMEX', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('domicilio', 15, NULL, 'short_text', 1, 88, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidad', 15, NULL, 'short_text', 1, 89, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombreCJPPU', 15, NULL, 'short_text', 1, 23, NULL, NULL, NULL, 'tipoDebitoVisa', 'C.J.P.P.U.');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('celularClaro', 15, NULL, 'short_text', 1, 37, NULL, NULL, NULL, 'tipoDebitoVisa', 'Claro');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('departamento', 15, NULL, 'short_text', 1, 90, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('modoPagoANEP', 15, NULL, 'select', 1, 19, NULL, 'Contado|3 Cuotas', NULL, 'tipoDebitoVisa', 'A.N.E.P.');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ramoPolizaBSE', 15, NULL, 'short_text', 1, 15, NULL, NULL, NULL, 'tipoDebitoVisaBSE', 'Póliza BSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebito', 15, NULL, 'productList', 1, 2, NULL, 'VISA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoOperacion', 15, NULL, 'select', 1, 1, NULL, 'Alta|Baja', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('rutMovEmpresas', 15, NULL, 'short_text', 1, 33, NULL, NULL, NULL, 'tipoDebitoVisaMovistar', 'Mensualidad empresas');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoVisa', 15, NULL, 'select', 1, 4, NULL, 'I.M. Montevideo|I.M. Maldonado|B.S.E.|A.N.E.P.|C.J.P.P.U.|U.T.E.|A.N.T.E.L.|A.N.C.E.L.|Movistar|Claro', NULL, 'marca', 'VISA');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('vencimientoAno', 15, NULL, 'short_text', 1, 87, NULL, NULL, '([0-9]{4})', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('vencimientoMes', 15, NULL, 'short_text', 1, 86, NULL, NULL, '([1-9]|1[0-2])', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('modoPagoPatente', 15, NULL, 'select', 1, 7, NULL, 'Contado|3 Pagos', NULL, 'tipoDebitoVisaIMMMont', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('monedaMasterBse', 15, NULL, 'select', 1, 47, NULL, 'UYP|USD', NULL, 'tipoDebitoMaster', 'BSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('polizaPolizaBSE', 15, NULL, 'short_text', 1, 16, NULL, NULL, NULL, 'tipoDebitoVisaBSE', 'Póliza BSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('telefonoCelular', 15, NULL, 'short_text', 1, 93, NULL, NULL, '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('telefonoLaboral', 15, NULL, 'short_text', 1, 92, NULL, NULL, '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('carpetaMasterBse', 15, NULL, 'short_text', 1, 46, NULL, NULL, NULL, 'tipoDebitoMaster', 'BSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cedulaSocioClaro', 15, NULL, 'short_text', 1, 36, NULL, NULL, NULL, 'tipoDebitoVisa', 'Claro');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('seccionMasterBse', 15, NULL, 'short_text', 1, 45, NULL, NULL, NULL, 'tipoDebitoMaster', 'BSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoMaster', 15, NULL, 'select', 1, 5, NULL, 'ANCEL|ANTEL|ANEP|BSE|CJPPU|I.M. Montevideo|I.M. Maldonado|I.M. Interior', NULL, 'marca', 'MASTER');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('carpetaCarpetaBSE', 15, NULL, 'short_text', 1, 14, NULL, NULL, NULL, 'tipoDebitoVisaBSE', 'Carpeta BSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaMasterAntel', 15, NULL, 'short_text', 1, 42, NULL, NULL, NULL, 'tipoDebitoMaster', 'ANTEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('identificadorANEP', 15, NULL, 'short_text', 1, 20, NULL, NULL, NULL, 'tipoDebitoVisa', 'A.N.E.P.');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombreMasterCJPPU', 15, NULL, 'short_text', 1, 49, NULL, NULL, NULL, 'tipoDebitoMaster', 'CJPPU');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroCuentaANTEL', 15, NULL, 'short_text', 1, 25, NULL, NULL, NULL, 'tipoDebitoVisa', 'A.N.T.E.L.');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroCuentaClaro', 15, NULL, 'short_text', 1, 38, NULL, NULL, NULL, 'tipoDebitoVisa', 'Claro');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('seccionCarpetaBSE', 15, NULL, 'short_text', 1, 13, NULL, NULL, NULL, 'tipoDebitoVisaBSE', 'Carpeta BSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoVisaBSE', 15, NULL, 'select', 1, 12, NULL, 'Carpeta BSE|Póliza BSE', NULL, 'tipoDebitoVisa', 'B.S.E.');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombreApellidoVisa', 15, NULL, 'short_text', 1, 85, NULL, NULL, NULL, 'marca', 'VISA');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referenciaCobroUTE', 15, NULL, 'short_text', 1, 24, NULL, NULL, NULL, 'tipoDebitoVisa', 'U.T.E.');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('telefonoParticular', 15, NULL, 'short_text', 1, 91, NULL, NULL, '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('titularMasterAncel', 15, NULL, 'short_text', 1, 41, NULL, NULL, NULL, 'tipoDebitoMaster', 'ANCEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('titularMasterAntel', 15, NULL, 'select', 1, 43, NULL, 'S&iacute;|No', NULL, 'tipoDebitoMaster', 'ANTEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('afiliadoMasterCJPPU', 15, NULL, 'short_text', 1, 48, NULL, NULL, NULL, 'tipoDebitoMaster', 'CJPPU');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('contratoMasterAncel', 15, NULL, 'short_text', 1, 39, NULL, NULL, NULL, 'tipoDebitoMaster', 'ANCEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('modoPagoPatenteMald', 15, NULL, 'select', 1, 51, NULL, 'Contado|2 pagos', NULL, 'tipoDebitoVisaIMMald', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroAfiliadoCJPPU', 15, NULL, 'short_text', 1, 22, NULL, NULL, NULL, 'tipoDebitoVisa', 'C.J.P.P.U.');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroMovilmMovAdsl', 15, NULL, 'short_text', 1, 31, NULL, NULL, NULL, 'tipoDebitoVisaMovistar', 'ADSL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoVisaANCEL', 15, NULL, 'select', 1, 26, NULL, 'ADSL móvil|Factura mensual ANCEL', NULL, 'tipoDebitoVisa', 'A.N.C.E.L.');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoVisaCJPPU', 15, NULL, 'select', 1, 21, NULL, 'Aportes|Préstamo', NULL, 'tipoDebitoVisa', 'C.J.P.P.U.');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('certificadoPolizaBSE', 15, NULL, 'short_text', 1, 17, NULL, NULL, NULL, 'tipoDebitoVisaBSE', 'Póliza BSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('matriculaPatenteMald', 15, NULL, 'short_text', 1, 52, NULL, NULL, NULL, 'tipoDebitoVisaIMMald', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('modoPagoContribucion', 15, NULL, 'select', 1, 10, NULL, 'Contado|3 Pagos', NULL, 'tipoDebitoVisaIMMMont', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoVisaIMMald', 15, NULL, 'select', 1, 50, NULL, 'Patente|Contribución', NULL, 'tipoDebitoVisa', 'I.M. Maldonado');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('verificadorPolizaBSE', 15, NULL, 'short_text', 1, 18, NULL, NULL, NULL, 'tipoDebitoVisaBSE', 'Póliza BSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoVisaIMMMont', 15, NULL, 'select', 1, 6, NULL, 'Patente|Saneamiento|Contribución', NULL, 'tipoDebitoVisa', 'I.M. Montevideo');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaCorrientePatente', 15, NULL, 'short_text', 1, 8, NULL, NULL, NULL, 'tipoDebitoVisaIMMMont', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroContratoFacAncel', 15, NULL, 'short_text', 1, 28, NULL, NULL, NULL, 'tipoDebitoVisaANCEL', 'Factura mensual ANCEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroMovilMovPersonas', 15, NULL, 'short_text', 1, 34, NULL, NULL, NULL, 'tipoDebitoVisaMovistar', 'Mensualidad personas');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroServicioFacAncel', 15, NULL, 'short_text', 1, 29, NULL, NULL, NULL, 'tipoDebitoVisaANCEL', 'Factura mensual ANCEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('padronContribucionMald', 15, NULL, 'short_text', 1, 55, NULL, NULL, NULL, 'tipoDebitoVisaIMMald', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoMasterIMMald', 15, NULL, 'select', 1, 67, NULL, 'Patente de rodados|Contribución inmobiliaria', NULL, 'tipoDebitoMaster', 'I.M. Maldonado');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoMasterIMMont', 15, NULL, 'select', 1, 57, NULL, 'Patente de rodados|Contribución inmobiliaria|Tributo domiciliario|Tasa de saneamiento', NULL, 'tipoDebitoMaster', 'I.M. Montevideo');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoVisaMovistar', 15, NULL, 'select', 1, 30, NULL, 'ADSL|Mensualidad empresas|Mensualidad personas', NULL, 'tipoDebitoVisa', 'Movistar');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('identificadorMasterAnep', 15, NULL, 'short_text', 1, 44, NULL, NULL, NULL, 'tipoDebitoMaster', 'ANEP');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroContratoADSLMovil', 15, NULL, 'short_text', 1, 27, NULL, NULL, NULL, 'tipoDebitoVisaANCEL', 'ADSL móvil');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('padronPatenteMaldMaster', 15, NULL, 'short_text', 1, 68, NULL, NULL, NULL, 'tipoDebitoMasterIMMald', 'Patente de rodados');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaContribucionMald', 15, NULL, 'short_text', 1, 53, NULL, NULL, NULL, 'tipoDebitoVisaIMMald', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cedulaClienteMovPersonas', 15, NULL, 'short_text', 1, 35, NULL, NULL, NULL, 'tipoDebitoVisaMovistar', 'Mensualidad personas');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroCelularMasterAncel', 15, NULL, 'short_text', 1, 40, NULL, NULL, NULL, 'tipoDebitoMaster', 'ANCEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroClienteMovEmpresas', 15, NULL, 'short_text', 1, 32, NULL, NULL, NULL, 'tipoDebitoVisaMovistar', 'Mensualidad empresas');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidadContribucionMald', 15, NULL, 'short_text', 1, 54, NULL, NULL, NULL, 'tipoDebitoVisaIMMald', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaCorrienteSaneamiento', 15, NULL, 'short_text', 1, 9, NULL, 'Saneamiento', NULL, 'tipoDebitoVisaIMMMont', 'Saneamiento');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('departamentoInteriorMaster', 15, NULL, 'select', 1, 75, NULL, 'Artigas|Canelones|Cerro Largo|Colonia|Durazno|Flores|Florida|Lavalleja|Paysandú|Río Negro|Rivera|Rocha|Salto|San José|Soriano|Tacuarembó|Treinta y Tres', NULL, 'tipoDebitoMaster', 'I.M. Interior');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('matriculaPatenteMaldMaster', 15, NULL, 'short_text', 1, 69, NULL, NULL, NULL, 'tipoDebitoMasterIMMald', 'Patente de rodados');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('matriculaPatenteMontMaster', 15, NULL, 'short_text', 1, 59, NULL, NULL, NULL, 'tipoDebitoMasterIMMont', 'Patente de rodados');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoContribucionMaldMaster', 15, NULL, 'select', 1, 71, NULL, 'Rural|Urbana|Suburbana', NULL, 'tipoDebitoMasterIMMald', 'Contribución inmobiliaria');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDebitoMasterIMInterior', 15, NULL, 'select', 1, 76, NULL, 'Contribución|Patente', NULL, 'tipoDebitoMaster', 'I.M. Interior');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('apartamentoContribucionMald', 15, NULL, 'short_text', 1, 56, NULL, NULL, NULL, 'tipoDebitoVisaIMMald', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaCorrienteContribucion', 15, NULL, 'short_text', 1, 11, NULL, NULL, NULL, 'tipoDebitoVisaIMMMont', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('modoDePagoPatenteMaldMaster', 15, NULL, 'select', 1, 70, NULL, 'Contado|Cuotas', NULL, 'tipoDebitoMasterIMMald', 'Patente de rodados');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('modoDePagoPatenteMontMaster', 15, NULL, 'select', 1, 60, NULL, 'Contado|Cuotas', NULL, 'tipoDebitoMasterIMMont', 'Patente de rodados');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('padronPatenteInteriorMaster', 15, NULL, 'short_text', 1, 82, NULL, NULL, NULL, 'tipoDebitoMasterIMInterior', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('padronContribucionMaldMaster', 15, NULL, 'short_text', 1, 73, NULL, NULL, NULL, 'tipoDebitoMasterIMMald', 'Contribución inmobiliaria');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('padronContribucionMontMaster', 15, NULL, 'short_text', 1, 62, NULL, NULL, NULL, 'tipoDebitoMasterIMMont', 'Contribución inmobiliaria');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('matriculaPatenteInteriorMaster', 15, NULL, 'short_text', 1, 83, NULL, NULL, NULL, 'tipoDebitoMasterIMInterior', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoContribucionInteriorMaster', 15, NULL, 'select', 1, 77, NULL, 'Rural|Urbana|Suburbana', NULL, 'tipoDebitoMasterIMInterior', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidadContribucionMaldMaster', 15, NULL, 'short_text', 1, 72, NULL, NULL, NULL, 'tipoDebitoMasterIMMald', 'Contribución inmobiliaria');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('modoDePagoPatenteInteriorMaster', 15, NULL, 'select', 1, 84, NULL, 'Contado|Cuotas', NULL, 'tipoDebitoMasterIMInterior', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaCorrientePatenteMontMaster', 15, NULL, 'short_text', 1, 58, NULL, NULL, NULL, 'tipoDebitoMasterIMMont', 'Patente de rodados');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaCorrienteTributoMontMaster', 15, NULL, 'short_text', 1, 65, NULL, NULL, NULL, 'tipoDebitoMasterIMMont', 'Tributo domiciliario');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('modoDePagoContribucionMontMaster', 15, NULL, 'select', 1, 64, NULL, 'Contado|Cuotas', NULL, 'tipoDebitoMasterIMMont', 'Contribución inmobiliaria');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('padronContribucionInteriorMaster', 15, NULL, 'short_text', 1, 79, NULL, NULL, NULL, 'tipoDebitoMasterIMInterior', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('unidadOtrosContribucionMontMaster', 15, NULL, 'short_text', 1, 63, NULL, NULL, NULL, 'tipoDebitoMasterIMMont', 'Contribución inmobiliaria');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidadContribucionInteriorMaster', 15, NULL, 'short_text', 1, 78, NULL, NULL, NULL, 'tipoDebitoMasterIMInterior', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ubicacionContribucionInteriorMaster', 15, NULL, 'short_text', 1, 80, NULL, NULL, NULL, 'tipoDebitoMasterIMInterior', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaCorrienteSaneamientoMontMaster', 15, NULL, 'short_text', 1, 66, NULL, NULL, NULL, 'tipoDebitoMasterIMMont', 'Tasa de saneamiento');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('modoDePagoContribucionInteriorMaster', 15, NULL, 'select', 1, 81, NULL, 'Contado|Cuotas', NULL, 'tipoDebitoMasterIMInterior', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaCorrienteContribucionMontMaster', 15, NULL, 'short_text', 1, 61, NULL, NULL, NULL, 'tipoDebitoMasterIMMont', 'Contribución inmobiliaria');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('unidadUbicacionContribucionMaldMaster', 15, NULL, 'short_text', 1, 74, NULL, NULL, NULL, 'tipoDebitoMasterIMMald', 'Contribución inmobiliaria');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDebito', 16, NULL, 'productList', 1, 1, '', 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDeDebito', 16, NULL, 'select', 1, 2, '', 'ANTEL|ANCEL|OSE|UTE|I.M. Montevideo|Montevideo Gas|I.M. Maldonado|I.M. de San José|Movistar|Dedicado|Préstamos', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('prestamoAPagar', 16, NULL, 'select', 1, 19, '', 'Préstamo personal|Préstamo de tercero', NULL, 'tipoDeDebito', 'Préstamos');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('servicioIMMald', 16, NULL, 'select', 1, 26, '', 'Contribución urbana|Patente', NULL, 'tipoDeDebito', 'I.M. Maldonado');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('servicioIMMont', 16, NULL, 'select', 1, 25, '', 'Contribución|Saneamiento|Impuestos|Patente', NULL, 'tipoDeDebito', 'I.M. Montevideo');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroCuentaOse', 16, NULL, 'short_text', 1, 28, '', '', NULL, 'tipoDeDebito', 'OSE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoDeOperacion', 16, NULL, 'select', 1, 0, '', 'Alta|Baja|Modificación', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('prestamoPersonal', 16, NULL, 'productList', 1, 20, '', 'PA|PI', NULL, 'prestamoAPagar', 'Préstamo personal');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroCuentaAntel', 16, NULL, 'short_text', 1, 3, '', '', NULL, 'tipoDeDebito', 'ANTEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('servicioIMSanJose', 16, NULL, 'select', 1, 27, '', 'Contribución urbana|Contribución rural|Patente', NULL, 'tipoDeDebito', 'I.M. de San José');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroCelularAncel', 16, NULL, 'short_text', 1, 5, '', '', '[0-9]*', 'tipoDeDebito', 'ANCEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroContratoAncel', 16, NULL, 'short_text', 1, 4, '', '', NULL, 'tipoDeDebito', 'ANCEL');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('padronPatenteIMMald', 16, NULL, 'short_text', 1, 35, '', '', NULL, 'servicioIMMald', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('solarContrUrbIMMald', 16, NULL, 'short_text', 1, 13, '', '', NULL, 'servicioIMMald', 'Contribución urbana');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('matriculaPatenteIMSJ', 16, NULL, 'short_text', 1, 43, '', '', '[A-S][A-Z]{2}(-?)[0-9]{4}', 'servicioIMSanJose', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referenciaDeCobroUte', 16, NULL, 'short_text', 1, 6, '', '', '(0{4}[0-9]{11})', 'tipoDeDebito', 'UTE');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('unidadContrUrbIMMald', 16, NULL, 'short_text', 1, 11, '', '', NULL, 'servicioIMMald', 'Contribución urbana');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('manzanaContrUrbIMMald', 16, NULL, 'short_text', 1, 12, '', '', NULL, 'servicioIMMald', 'Contribución urbana');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroClienteMovistar', 16, NULL, 'short_text', 1, 45, '', '', NULL, 'tipoDeDebito', 'Movistar');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('prestamoTerceroCodigo', 16, NULL, 'select', 1, 24, '', 'PA|PI', NULL, 'prestamoAPagar', 'Préstamo de tercero');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('prestamoTerceroMoneda', 16, NULL, 'select', 1, 22, '', 'UYP|USD', NULL, 'prestamoAPagar', 'Préstamo de tercero');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('prestamoTerceroNumero', 16, NULL, 'short_text', 1, 23, '', '', NULL, 'prestamoAPagar', 'Préstamo de tercero');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('formaDePagoPatenteIMSJ', 16, NULL, 'select', 1, 44, '', 'Contado|Cuotas', NULL, 'servicioIMSanJose', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('matriculaPatenteIMMald', 16, NULL, 'short_text', 1, 14, '', '', '[A-S][A-Z]{2}(-?)[0-9]{4}', 'servicioIMMald', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroContratoDedicado', 16, NULL, 'short_text', 1, 46, '', '', NULL, 'tipoDeDebito', 'Dedicado');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDeClienteMontGas', 16, NULL, 'short_text', 1, 9, '', '', NULL, 'tipoDeDebito', 'Montevideo Gas');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('aceptacionDeSuscripcion', 16, NULL, 'disclaimer', 1, 18, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('formaDePagoContrUrbIMSJ', 16, NULL, 'select', 1, 38, '', 'Contado|Cuotas', NULL, 'servicioIMSanJose', 'Contribución urbana');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('anoVehiculoPatenteIMMald', 16, NULL, 'short_text', 1, 16, '', '', '[1-9][0-9]{3}', 'servicioIMMald', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('formaDePagoContribIMMont', 16, NULL, 'select', 1, 8, '', 'Contado|3 cuotas', NULL, 'servicioIMMont', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('formaDePagoPatenteIMMald', 16, NULL, 'select', 1, 36, '', 'Contado|Cuotas', NULL, 'servicioIMMald', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('formaDePagoPatenteIMMont', 16, NULL, 'select', 1, 32, '', 'Contado|3 cuotas', NULL, 'servicioIMMont', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroPadronContrUrbIMSJ', 16, NULL, 'short_text', 1, 37, '', '', NULL, 'servicioIMSanJose', 'Contribución urbana');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('formaDePagoContrRuralIMSJ', 16, NULL, 'select', 1, 41, '', 'Contado|Cuotas', NULL, 'servicioIMSanJose', 'Contribución rural');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('formaDePagoContrUrbIMMald', 16, NULL, 'select', 1, 33, '', 'Contado|Cuotas', NULL, 'servicioIMMald', 'Contribución urbana');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDePadronPatenteIMSJ', 16, NULL, 'short_text', 1, 42, '', '', NULL, 'servicioIMSanJose', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('marcaVehiculoPatenteIMMald', 16, NULL, 'short_text', 1, 15, '', '', NULL, 'servicioIMMald', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('prestamoTerceroDependencia', 16, NULL, 'branches', 1, 21, '', '', NULL, 'prestamoAPagar', 'Préstamo de tercero');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDePadronContrRuralIMSJ', 16, NULL, 'short_text', 1, 39, '', '', NULL, 'servicioIMSanJose', 'Contribución rural');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDePadronContrUrbIMMald', 16, NULL, 'short_text', 1, 10, '', '', NULL, 'servicioIMMald', 'Contribución urbana');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidadAbreviadaContrUrbIMSJ', 16, NULL, 'short_text', 1, 17, '', '', NULL, 'servicioIMSanJose', 'Contribución urbana');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidadAbreviadaContrRuralIMSJ', 16, NULL, 'short_text', 1, 40, '', '', NULL, 'servicioIMSanJose', 'Contribución rural');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDeCuentaCorrienteSaneamIMMont', 16, NULL, 'short_text', 1, 29, '', '', NULL, 'servicioIMMont', 'Saneamiento');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDeCuentaCorrienteContribIMMont', 16, NULL, 'short_text', 1, 7, '', '', '([0-9]{8})', 'servicioIMMont', 'Contribución');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDeCuentaCorrientePatenteIMMont', 16, NULL, 'short_text', 1, 31, '', '', NULL, 'servicioIMMont', 'Patente');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDeCuentaCorrienteImpuestosIMMont', 16, NULL, 'short_text', 1, 30, '', '', NULL, 'servicioIMMont', 'Impuestos');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('pais', 17, NULL, 'short_text', 1, 25, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('calle', 17, NULL, 'short_text', 1, 20, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('email', 17, NULL, 'short_text', 0, 30, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('marca', 17, NULL, 'select', 1, 0, '', 'American Express Blue Box|American Express Centurion|MasterCard|Visa', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numero', 17, NULL, 'short_text', 1, 21, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('celular', 17, NULL, 'short_text', 1, 11, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombres', 17, NULL, 'short_text', 1, 19, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('alquiler', 17, NULL, 'amount', 0, 14, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoVisa', 17, NULL, 'select', 1, 4, '', 'Local|Regional|Internacional|Oro', NULL, 'marca', 'Visa');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('vivienda', 17, NULL, 'select', 0, 12, '', 'Propia|Alquilada|Familiar', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('apellidos', 17, NULL, 'short_text', 1, 18, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('residente', 17, NULL, 'select', 1, 26, '', 'Si|No', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('apartamento', 17, NULL, 'short_text', 1, 22, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('codigoPostal', 17, NULL, 'short_text', 0, 23, '', '', '[1-9][0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('faxAdicional', 17, NULL, 'short_text', 1, 29, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaVisa', 17, NULL, 'select', 1, 8, '', 'Clásica|Mileage plus|Club card|Gauchito|Diamante|Smiles|Gaviotas|Clásica - AUDAVI|Tres cruces|Pétalos', NULL, 'marca', 'Visa');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('3_fechaInicio', 17, NULL, 'date', 0, 52, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuotaHipoteca', 17, NULL, 'amount', 0, 13, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDeHijos', 17, NULL, 'short_text', 0, 16, '', '', '[1-9][0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('otrosIngresos', 17, NULL, 'amount', 0, 56, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('sexoAdicional', 17, NULL, 'select', 1, 39, '', 'F|M', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('personasACargo', 17, NULL, 'short_text', 0, 15, '', '', '[1-9][0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoMasterCard', 17, NULL, 'select', 1, 3, '', 'Regional|Internacional|Oro', NULL, 'marca', 'MasterCard');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('lugarNacimiento', 17, NULL, 'short_text', 1, 32, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('celularAdicional', 17, NULL, 'short_text', 1, 28, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('1_fechaNacimiento', 17, NULL, 'date', 1, 31, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('alquilerAdicional', 17, NULL, 'amount', 0, 36, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('telefonoAdicional', 17, NULL, 'short_text', 1, 27, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('viviendaAdicional', 17, NULL, 'select', 0, 34, '', 'Propia|Alquilada|Familiar', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaMasterCard', 17, NULL, 'select', 1, 7, '', 'Clásicas|Mileage plus|Club card|Smiles|Pétalos|TOTO', NULL, 'marca', 'MasterCard');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('telefonoDeContacto', 17, NULL, 'short_text', 1, 10, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('estadoCivilAdicional', 17, NULL, 'select', 0, 37, '', 'Soltero|Casado|Divorciado|Viudo', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroHijosAdicional', 17, NULL, 'short_text', 0, 41, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidadDepartamento', 17, NULL, 'short_text', 1, 24, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nacionalidadAdicional', 17, NULL, 'short_text', 1, 33, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuotaHipotecaAdicional', 17, NULL, 'amount', 0, 35, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('emailConyugueAdicional', 17, NULL, 'short_text', 0, 63, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ingresosNetosMensuales', 17, NULL, 'amount', 0, 55, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('lugarConyugueAdicional', 17, NULL, 'short_text', 0, 65, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('personasACargoAdicional', 17, NULL, 'short_text', 0, 40, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('celularConyugueAdicional', 17, NULL, 'short_text', 0, 62, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombresConyugueAdicional', 17, NULL, 'short_text', 0, 59, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('telefonoConyugueAdicional', 17, NULL, 'short_text', 0, 61, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('apellidosConyugueAdicional', 17, NULL, 'short_text', 0, 58, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoAmericanExpressBlueBox', 17, NULL, 'select', 1, 1, '', 'Internacional|Gold', NULL, 'marca', 'American Express Blue Box');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('2_fechaEstadoCivilAdicional', 17, NULL, 'date', 0, 38, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('emailDatosLaboralesAdicional', 17, NULL, 'short_text', 0, 54, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoAmericanExpressCenturion', 17, NULL, 'select', 1, 2, '', 'Green Compra|Green Crédito|Gold Compra|Gold Crédito', NULL, 'marca', 'American Express Centurion');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nacionalidadConyugueAdicional', 17, NULL, 'short_text', 0, 66, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDatosLaboralesAdicional', 17, NULL, 'short_text', 0, 46, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('empresaDatosLaboralesAdicional', 17, NULL, 'short_text', 0, 43, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaAmericanExpressBlueBox', 17, NULL, 'select', 1, 5, '', 'Clásica|Club card|Diamante|Gaviotas', NULL, 'marca', 'American Express Blue Box');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('telefonoDatosLaboralesAdicional', 17, NULL, 'short_text', 0, 53, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('domicilioDatosLaboralesAdicional', 17, NULL, 'short_text', 0, 45, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('profesionDatosLaboralesAdicional', 17, NULL, 'short_text', 1, 50, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('programaAmericanExpressCenturion', 17, NULL, 'select', 1, 6, '', 'Clásica|Club card|Diamante|Gaviotas', NULL, 'marca', 'American Express Centurion');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDatosPersonalesAdicional', 17, NULL, 'subtitle', 1, 17, 'Datos personales Adicional', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('apartamentoDatosLaboralesAdicional', 17, NULL, 'short_text', 1, 47, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDatosPersonalesDelTitular', 17, NULL, 'subtitle', 1, 9, 'Datos personales del titular', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cargoFuncionDatosLaboralesAdicional', 17, NULL, 'short_text', 1, 51, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('codigoPostalDatosLaboralesAdicional', 17, NULL, 'short_text', 0, 48, '', '', '[1-9][0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDatosLaboralesDelAdicional', 17, NULL, 'subtitle', 1, 42, 'Datos laborales del Adicional', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('4_fechaDeNacimientoConyugueAdicional', 17, NULL, 'date', 0, 64, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('rubroActividadDatosLaboralesAdicional', 17, NULL, 'short_text', 0, 44, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoYNumeroDocumentoConyugueAdicional', 17, NULL, 'short_text', 0, 60, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('localidadDepartamentoDatosLaboralesAdicional', 17, NULL, 'short_text', 1, 49, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDatosPersonalesDelConyugeDelAdiciona', 17, NULL, 'subtitle', 1, 57, 'Datos personales del cónyuge del Adicional', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fax', 18, NULL, 'short_text', 1, 35, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('email', 18, NULL, 'short_text', 1, 34, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('mixto', 18, NULL, 'short_text', 1, 26, '', '', NULL, 'disponibleComoSigue', 'Mixto');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('poliza', 18, NULL, 'short_text', 0, 47, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('mercosur', 18, NULL, 'multiple', 0, 43, '', 'Mercosur (ACE Nro. 18)', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('telefono', 18, NULL, 'short_text', 1, 33, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('aceptacion', 18, NULL, 'disclaimer', 1, 39, 'Es una leyenda que nos va a dar el banco y que el cliente tiene que aceptar. El cliente tiene que poner la fecha y número de contrato. Homebanking lo graba para luego recordar. Aquí puede ir el texto sujeta a la UCP 600 de la ICC o versión vigente', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('confirmada', 18, NULL, 'select', 1, 3, 'No confirmada', 'No confirmada|Confirmada', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 18, NULL, 'disclaimer', 1, 30, 'Esta carta de crédito está sujeta a las reglas y usos uniformes relativos a los créditos documentarios, así como a los incoterms publicados por la cámara de comercio internacional última versión vigente a la fecha.', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('trasbordos', 18, NULL, 'select', 1, 38, '', 'Permitidos|Prohibidos', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('monedaCarta', 18, NULL, 'select', 1, 10, '', 'USD|UYP', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('bancoDestino', 18, NULL, 'banks', 1, 1, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('beneficiario', 18, NULL, 'short_text', 1, 6, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('transferible', 18, NULL, 'select', 1, 2, 'Intransferible', 'Intransferible|Transferible', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('lugarDeOrigen', 18, NULL, 'short_text', 1, 11, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('observaciones', 18, NULL, 'long_text', 1, 16, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('listaDeEmpaque', 18, NULL, 'multiple', 0, 44, '', 'Lista de empaque', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('lugarDeDestino', 18, NULL, 'short_text', 0, 12, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('gastosBancarios', 18, NULL, 'select', 1, 29, '', 'Beneficiario|Ordenante', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('otroTransaporte', 18, NULL, 'select', 1, 19, '', 'FCA|CPT|CIP|Otros', NULL, 'terminosIncoterms', 'Otro transaporte');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('embarqueMaritimo', 18, NULL, 'select', 1, 18, '', 'FOB|CFR|CIF', NULL, 'terminosIncoterms', 'Embarque Marítimo');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fechaVencimiento', 18, NULL, 'date', 1, 4, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('otrasCondiciones', 18, NULL, 'subtitle', 1, 28, 'Condiciones adicionales', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('paisBeneficiario', 18, NULL, 'short_text', 1, 9, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('personaEncargada', 18, NULL, 'short_text', 1, 32, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('porPagoDiferidoA', 18, NULL, 'short_text', 1, 22, '', '', '[1-9][0-9]*', 'disponibleComoSigue', 'Por pago diferido a');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('terminosIncoterms', 18, NULL, 'select', 1, 17, '', 'Embarque Marítimo|Otro transaporte', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ciudadBeneficiario', 18, NULL, 'short_text', 1, 8, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('embarquesParciales', 18, NULL, 'select', 1, 14, '', 'Permitidos|Prohibidos', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('certificadoDeOrigen', 18, NULL, 'multiple', 0, 42, '', 'Certificado de orígen', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('declaracionEscrita1', 18, NULL, 'multiple', 0, 45, '', 'Declaración escrita original del beneficiario/transportista certificando que los documentos originales viajan con la mercadería para ser entregados a los compradores.', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('declaracionEscrita2', 18, NULL, 'short_text', 0, 46, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disponibleComoSigue', 18, NULL, 'select', 1, 21, '', 'Por pago a la vista|Por pago diferido a|Aceptación de letra(s)|Mixto', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('envioReferenciaOtro', 18, NULL, 'short_text', 1, 25, '', '', NULL, 'aceptaciónDeLetraReferencia', 'Otro');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('otroTransporteOtros', 18, NULL, 'short_text', 1, 20, '', '', NULL, 'otroTransaporte', 'Otros');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDebitoComision', 18, NULL, 'productList', 1, 0, '', 'CA|CC', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('direccionBeneficiario', 18, NULL, 'short_text', 1, 7, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('documentosPresentados', 18, NULL, 'short_text', 1, 48, '21', '', '[1-9][0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('aceptaciónDeLetraDias', 18, NULL, 'short_text', 1, 23, '', '', '[1-9][0-9]*', 'disponibleComoSigue', 'Aceptación de letra(s)');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('condicionesAdicionales', 18, NULL, 'long_text', 1, 50, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('docExigidosDocTransporte', 18, NULL, 'multiple', 0, 49, '', 'Documento de transporte', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('importeAdicionalCubierto', 18, NULL, 'short_text', 1, 37, '', '', '([1-9][0-9]*(,[0-9]+)?)', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombreYApellidoOrdenante', 18, NULL, 'short_text', 1, 5, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('bancoCorresponsalSugerido', 18, NULL, 'short_text', 1, 36, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('docExigidosDocTransporteSel', 18, NULL, 'select', 0, 41, '', 'Marítimo|Aéreo|Terrestre|Multimodal', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('documentosExigidosSubtitulo', 18, NULL, 'subtitle', 1, 27, 'Documentos exigidos', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('aceptaciónDeLetraReferencia', 18, NULL, 'select', 1, 24, '', 'Embarque|Factura|Otro', NULL, 'disponibleComoSigue', 'Aceptación de letra(s)');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosNecesarioDelSolicitante', 18, NULL, 'subtitle', 1, 31, 'Datos necesarios del solicitante', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fechaDeVencimientoParaEmbarque', 18, NULL, 'date', 1, 13, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('descripcionMercaderiaYServicios', 18, NULL, 'long_text', 1, 15, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('documentosExigidoFacturaComercial', 18, NULL, 'multiple', 0, 40, '', 'Factura comercial', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('banco', 41, NULL, 'short_text', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fecha', 41, NULL, 'date', 1, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('plazo', 41, 'Es la cantidad de días que se tienen a partir de un evento
determinado que se selecciona a continuación', 'short_text', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 41, NULL, 'disclaimer', 1, 15, 'Uno de los textos a incluir aquí puede ser que La transferencia de carta de crédito está sujeta a la UCP 600 de la ICC o versión vigente', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia', 41, 'Campo a completar con la referncia en el Banco', 'short_text', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('montoOrigen', 41, NULL, 'amount', 1, 17, NULL, 'EUR|GBP|JPY|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fchaAPartirDe', 41, 'Lista de eventos: Fecha de embarque, Otro', 'select', 1, 9, NULL, 'Fecha de embarque|Otro', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('NroCartaCredito', 41, NULL, 'short_text', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fechaVencimiento', 41, NULL, 'date', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('importeTransferir', 41, NULL, 'amount', 1, 3, NULL, 'EUR|GBP|JPY|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('segundoBeneficiario', 41, 'Segundo beneficiario', 'short_text', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ultimaFechaVencimiento', 41, NULL, 'date', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('gastosComisionesUruguay', 41, NULL, 'select', 1, 13, NULL, 'A cargo del primer beneficiario|A cargo del segundo beneficiario', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('gastosComisionesExterior', 41, NULL, 'select', 1, 14, NULL, 'A cargo del primer beneficiario|A cargo del segundo beneficiario', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('derechoSustitucionFactura', 41, NULL, 'select', 1, 10, NULL, 'Sí|No', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('autorizacionTransferirModificaciones', 41, NULL, 'select', 1, 11, NULL, 'Sí|No', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('conservarDerechoTransferirModificaciones', 41, NULL, 'select', 1, 12, NULL, 'Sí|No', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('usoDeFirmas', 61, NULL, 'select', 1, 10, '', 'No aplica|Indistinto|Conjunta', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebito', 61, NULL, 'select', 0, 68, 'No', 'Si|No', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularFax', 61, NULL, 'short_text', 1, 24, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularPais', 61, NULL, 'short_text', 1, 20, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularSexo', 61, NULL, 'select', 0, 34, '', 'F|M', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloFirmas', 61, NULL, 'subtitle', 1, 67, 'Firmas', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularCalle', 61, NULL, 'short_text', 0, 40, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularCargo', 61, NULL, 'short_text', 0, 46, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularEmail', 61, NULL, 'short_text', 0, 49, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularRubro', 61, NULL, 'short_text', 0, 39, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularCalle', 61, NULL, 'short_text', 1, 15, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularEmail', 61, NULL, 'short_text', 0, 25, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularNumero', 61, NULL, 'short_text', 0, 41, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularEmail', 61, NULL, 'short_text', 0, 59, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularLugar', 61, NULL, 'short_text', 0, 61, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularNumero', 61, NULL, 'short_text', 1, 16, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia1Nombre', 61, NULL, 'short_text', 1, 1, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia2Nombre', 61, NULL, 'short_text', 1, 4, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia3Nombre', 61, NULL, 'short_text', 1, 7, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularEmpresa', 61, NULL, 'short_text', 0, 38, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularCelular', 61, NULL, 'short_text', 1, 23, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularNombres', 61, NULL, 'short_text', 1, 14, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularTelefono', 61, NULL, 'short_text', 0, 48, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularCelular', 61, NULL, 'short_text', 0, 58, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularNombres', 61, NULL, 'short_text', 0, 54, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularAlquiler', 61, NULL, 'amount', 0, 31, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dpCotitularTelefono', 61, NULL, 'short_text', 1, 22, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia1Apellido', 61, NULL, 'short_text', 1, 2, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia1Telefono', 61, NULL, 'short_text', 1, 3, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia2Apellido', 61, NULL, 'short_text', 1, 5, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia2Telefono', 61, NULL, 'short_text', 1, 6, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia3Apellido', 61, NULL, 'short_text', 1, 8, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia3Telefono', 61, NULL, 'short_text', 1, 9, '', '', '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularLocalidad', 61, NULL, 'short_text', 0, 44, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularProfesion', 61, NULL, 'short_text', 0, 45, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularTelefono', 61, NULL, 'short_text', 0, 57, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularApellidos', 61, NULL, 'short_text', 1, 13, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularLocalidad', 61, NULL, 'short_text', 1, 19, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularResidente', 61, NULL, 'select', 1, 21, '', 'Si|No', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDLCotitular', 61, NULL, 'subtitle', 1, 37, 'Datos laborales del Cotitular', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDPCotitular', 61, NULL, 'subtitle', 1, 11, 'Datos personales Cotitular', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloReferencia1', 61, NULL, 'subtitle', 1, 64, 'Referencia 1', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloReferencia2', 61, NULL, 'subtitle', 1, 65, 'Referencia 2', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloReferencia3', 61, NULL, 'subtitle', 1, 66, 'Referencia 3', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('terminosYCondiciones', 61, NULL, 'disclaimer', 1, 63, 'Se mostrará al Cliente un link que despliega los términos y condiciones asociados con la cuenta que solicita y un check de verificación que el Cliente deberá aceptar para poder continuar con el envío de la solicitud. Los textos se agregarán a posteriori ', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularApellidos', 61, NULL, 'short_text', 0, 53, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularEsConyugue', 61, NULL, 'select', 1, 12, '', 'Si|No', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDPCCotitular', 61, NULL, 'subtitle', 1, 52, 'Datos personales del cónyuge del Cotitular', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularApartamento', 61, NULL, 'short_text', 0, 42, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularFechaInicio', 61, NULL, 'date', 0, 47, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularApartamento', 61, NULL, 'short_text', 1, 17, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularEstadoCivil', 61, NULL, 'select', 0, 32, '', 'Soltero|Casado|Divorciado|Viudo', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloTarjetaDebito', 61, NULL, 'subtitle', 1, 69, 'Solicitud de tarjeta de débito', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicional', 61, NULL, 'short_text', 0, 82, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularCodigoPostal', 61, NULL, 'short_text', 0, 43, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularCodigoPostal', 61, NULL, 'short_text', 0, 18, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularNacionalidad', 61, NULL, 'short_text', 1, 28, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularTipoVivienda', 61, NULL, 'select', 0, 29, '', 'Propia|Alquilada|Familiar', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularOtrosIngresos', 61, NULL, 'amount', 0, 51, '', 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularNacionalidad', 61, NULL, 'short_text', 0, 62, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularCuotaHipoteca', 61, NULL, 'amount', 0, 30, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularNumeroDeHijos', 61, NULL, 'short_text', 0, 36, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoTipoTarjeta', 61, NULL, 'select', 1, 71, '', 'Automatismos|Banred|Visa débito', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularTipoDocumento', 61, NULL, 'select', 0, 55, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularPersonasACargo', 61, NULL, 'short_text', 0, 35, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoProgramaVisa', 61, NULL, 'select', 1, 72, '', 'Clásica|Mileage plus|Club card|Gauchito|Diamante|Smiles|Gaviotas|Clásica - AUDAVI|Tres cruces|Pétalos', NULL, 'tarjetaDebitoTipoTarjeta', 'Visa débito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularFechaNacimiento', 61, NULL, 'date', 1, 26, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularLugarNacimiento', 61, NULL, 'short_text', 1, 27, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularFechaNacimiento', 61, NULL, 'date', 0, 60, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCCotitularNumeroDocumento', 61, NULL, 'short_text', 0, 56, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dPCotitularFechaEstadoCivil', 61, NULL, 'date', 0, 33, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoNumeroDeCuenta', 61, NULL, 'productList', 0, 70, '', 'CC|CA', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoLimiteDiarioPago', 61, NULL, 'short_text', 1, 76, '', '', '([1-9][0-9]*(,[0-9]+)?)', 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoDependenciaRetiro', 61, NULL, 'select', 1, 74, '', 'Casa central', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoCuentaDebitoCostos', 61, NULL, 'productList', 0, 73, '', 'CC|CA', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoLimiteDiarioRetiro', 61, NULL, 'short_text', 1, 75, '', '', '([1-9][0-9]*(,[0-9]+)?)', 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoSubtituloAdicional', 61, NULL, 'subtitle', 1, 78, 'Solicitud de tarjeta de débito - Adicional', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('dLCotitularIngresosNetosMensuales', 61, NULL, 'amount', 0, 50, '', 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalPrimerNombre', 61, NULL, 'short_text', 1, 81, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalTipoDocumento', 61, NULL, 'select', 1, 79, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoVisaLimiteDiarioCompra', 61, NULL, 'short_text', 1, 77, '', '', '([1-9][0-9]*(,[0-9]+)?)', 'tarjetaDebitoTipoTarjeta', 'Visa débito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalPrimerApellido', 61, NULL, 'short_text', 1, 83, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalNumeroDocumento', 61, NULL, 'short_text', 1, 80, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalSegundoApellido', 61, NULL, 'short_text', 0, 84, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('usoDeFirmas', 62, NULL, 'select', 1, 2, '', 'No aplica|Indistinto|Conjunta', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebito', 62, NULL, 'select', 0, 1, 'No', 'Si|No', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('terminosYCondiciones', 62, NULL, 'disclaimer', 1, 55, 'Se mostrará al Cliente un link que despliega los términos y condiciones asociados con la cuenta que solicita y un check de verificación que el Cliente deberá aceptar para poder continuar con el envío de la solicitud. Los textos se agregarán a posteriori ', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloTarjetaDebito', 62, NULL, 'subtitle', 1, 56, 'Solicitud de tarjeta de débito', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicional', 62, NULL, 'short_text', 0, 69, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoTipoTarjeta', 62, NULL, 'select', 1, 58, '', 'Automatismos|Banred|Visa débito', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoProgramaVisa', 62, NULL, 'select', 1, 59, '', 'Clásica|Mileage plus|Club card|Gauchito|Diamante|Smiles|Gaviotas|Clásica - AUDAVI|Tres cruces|Pétalos', NULL, 'tarjetaDebitoTipoTarjeta', 'Visa débito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularFax', 62, NULL, 'short_text', 1, 16, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoNumeroDeCuenta', 62, NULL, 'productList', 1, 57, '', 'CC|CA', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularPais', 62, NULL, 'short_text', 1, 12, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularSexo', 62, NULL, 'select', 0, 26, '', 'F|M', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularCalle', 62, NULL, 'short_text', 1, 7, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularEmail', 62, NULL, 'short_text', 0, 17, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoLimiteDiarioPago', 62, NULL, 'short_text', 1, 63, '', '', '([1-9][0-9]*(,[0-9]+)?)', 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularNumero', 62, NULL, 'short_text', 1, 8, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoDependenciaRetiro', 62, NULL, 'select', 1, 61, '', 'Casa central', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularCalle', 62, NULL, 'short_text', 0, 32, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularCargo', 62, NULL, 'short_text', 0, 38, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularEmail', 62, NULL, 'short_text', 0, 41, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularRubro', 62, NULL, 'short_text', 0, 31, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularCelular', 62, NULL, 'short_text', 1, 15, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularNombres', 62, NULL, 'short_text', 1, 6, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoCuentaDebitoCostos', 62, NULL, 'productList', 1, 60, '', 'CC|CA', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoLimiteDiarioRetiro', 62, NULL, 'short_text', 1, 62, '', '', '([1-9][0-9]*(,[0-9]+)?)', 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoSubtituloAdicional', 62, NULL, 'subtitle', 1, 65, 'Solicitud de tarjeta de débito - Adicional', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularNumero', 62, NULL, 'short_text', 0, 33, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularAlquiler', 62, NULL, 'amount', 0, 23, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularTelefono', 62, NULL, 'short_text', 1, 14, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularEmpresa', 62, NULL, 'short_text', 0, 30, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularApellidos', 62, NULL, 'short_text', 1, 5, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularEsConyuge', 62, NULL, 'select', 1, 4, '', 'Si|No', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularLocalidad', 62, NULL, 'short_text', 1, 11, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularResidente', 62, NULL, 'select', 1, 13, '', 'Si|No', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDatosPersonalesCotitular', 62, NULL, 'subtitle', 1, 3, 'Datos personales Cotitular', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularTelefono', 62, NULL, 'short_text', 0, 40, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalPrimerNombre', 62, NULL, 'short_text', 1, 68, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularLocalidad', 62, NULL, 'short_text', 0, 36, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularProfesion', 62, NULL, 'short_text', 0, 37, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularApartamento', 62, NULL, 'short_text', 1, 9, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularEstadoCivil', 62, NULL, 'select', 0, 24, '', 'Soltero|Casado|Divorciado|Viudo', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDatosLaboralesDelCotitular', 62, NULL, 'subtitle', 1, 29, 'Datos laborales del Cotitular', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalTipoDocumento', 62, NULL, 'select', 1, 66, 'CI', 'CI|RUT', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoVisaLimiteDiarioCompra', 62, NULL, 'short_text', 1, 64, '', '', '([1-9][0-9]*(,[0-9]+)?)', 'tarjetaDebitoTipoTarjeta', 'Visa débito');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularCodigoPostal', 62, NULL, 'short_text', 0, 10, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularNacionalidad', 62, NULL, 'short_text', 1, 20, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularTipoVivienda', 62, NULL, 'select', 0, 21, '', 'Propia|Alquilada|Familiar', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalPrimerApellido', 62, NULL, 'short_text', 1, 70, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularApartamento', 62, NULL, 'short_text', 0, 34, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularFechaInicio', 62, NULL, 'date', 0, 39, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularCuotaHipoteca', 62, NULL, 'amount', 0, 22, '', 'USD|UYP', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularNumeroDeHijos', 62, NULL, 'short_text', 0, 28, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalNumeroDocumento', 62, NULL, 'short_text', 1, 67, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tarjetaDebitoAdicionalSegundoApellido', 62, NULL, 'short_text', 0, 71, '', '', NULL, 'tarjetaDebito', 'Si');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularCodigoPostal', 62, NULL, 'short_text', 0, 35, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularPersonasACargo', 62, NULL, 'short_text', 0, 27, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularOtrosIngresos', 62, NULL, 'amount', 0, 43, '', 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularFechaEstadoCivil', 62, NULL, 'date', 0, 25, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularFechaDeNacimiento', 62, NULL, 'date', 1, 18, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesCotitularLugarDeNacimiento', 62, NULL, 'short_text', 1, 19, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularEmail', 62, NULL, 'short_text', 0, 51, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularLugar', 62, NULL, 'short_text', 0, 53, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularCelular', 62, NULL, 'short_text', 0, 50, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularNombres', 62, NULL, 'short_text', 0, 46, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularFechaNac', 62, NULL, 'date', 0, 52, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularTelefono', 62, NULL, 'short_text', 0, 49, '', '', '[0-9]*', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularApellidos', 62, NULL, 'short_text', 0, 45, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('subtituloDatosPersonalesDelConyugeDelCotitular', 62, NULL, 'subtitle', 1, 44, 'Datos personales del cónyuge del Cotitular', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosLaboralesDelCotitularIngresosNetosMensuales', 62, NULL, 'amount', 0, 42, '', 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularNacionalidad', 62, NULL, 'short_text', 0, 54, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularNumDocumento', 62, NULL, 'short_text', 0, 48, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('datosPersonalesDelConyugeDelCotitularTipoDocumento', 62, NULL, 'select', 0, 47, '', '', NULL, 'usoDeFirmas', 'Indistinto|Conjunta');
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('documentoSolicitado', 63, NULL, 'select', 1, 1, NULL, 'Carta formal de certificación de saldos|Comprobante de liquidación de descuentos|Comprobante de liquidación de vale|Comprobante de liquidación de apertura de carta de crédito|Comprobante de liquidación de débito por devolución de letras por pago a proveedores', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('informacionAdicional', 63, NULL, 'short_text', 0, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipo', 64, NULL, 'select', 1, 2, NULL, 'Cobranza|Carta de crédito', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('referencia', 64, NULL, 'short_text', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cuentaDebito', 64, NULL, 'productList', 1, 4, NULL, 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('verificacion', 64, NULL, 'disclaimer', 1, 6, 'Se mostrará al Cliente un link que despliega los términos y condiciones asociados con la cuenta que solicita y un check de verificación que el Cliente deberá aceptar para poder continuar con el envío de la solicitud. Los textos se agregarán a posteriori ', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('observaciones', 64, NULL, 'long_text', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDocumento', 64, NULL, 'short_text', 1, 3, NULL, NULL, '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cambios', 65, NULL, 'long_text', 0, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 65, NULL, 'disclaimer', 1, 7, 'Demás términos y condiciones permanecen incambiados', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nuevoImporte', 65, NULL, 'amount', 0, 5, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cartaCreditoNumero', 65, NULL, 'short_text', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('incrementoDisminucion', 65, NULL, 'select', 0, 4, NULL, 'Incremento|Disminución', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nuevaFechaVencimiento', 65, NULL, 'date', 0, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nuevaFechaVencimientoEmbarque', 65, NULL, 'date', 0, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fecha', 66, NULL, 'date', 1, 1, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('valor', 66, NULL, 'amount', 1, 4, '', 'USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 66, NULL, 'disclaimer', 1, 7, 'Texto no modificable que se presenta al cliente y que debe aceptar a través de la verificación de un campo de aceptación para poder continuar.', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('bancoEmisor', 66, NULL, 'banks', 1, 3, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('montoCedido', 66, NULL, 'amount', 1, 6, '', 'USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cartaCreditoNro', 66, NULL, 'short_text', 1, 2, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('descuentoDocumentacion', 66, NULL, 'short_text', 1, 5, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('rut', 67, NULL, 'short_text', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fecha', 67, NULL, 'date', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('calidad', 67, NULL, 'select', 1, 4, NULL, 'exportador directo|exportador indirecto', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 67, NULL, 'disclaimer', 1, 5, 'Texto no modificable que se presenta al cliente y que debe aceptar a través de la verificación de un campo de aceptación para poder continuar.', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('exportador', 67, NULL, 'short_text', 1, 2, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('Fecha', 68, NULL, 'date', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('Importe', 68, NULL, 'amount', 1, 2, NULL, 'USD', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('RubrosNCM', 68, NULL, 'short_text', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ImporteVale', 68, NULL, 'amount', 1, 12, NULL, 'USD', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('CantidadDias', 68, NULL, 'short_text', 1, 4, NULL, NULL, 'd*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('verificacion', 68, NULL, 'disclaimer', 1, 13, 'Se mostrará al Cliente un link que despliega los términos y condiciones asociados con la cuenta que solicita y un check de verificación que el Cliente deberá aceptar para poder continuar con el envío de la solicitud. Los textos se agregarán a posteriori ', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('MontoEnLetras', 68, NULL, 'short_text', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('RUTExportador', 68, NULL, 'short_text', 1, 8, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('CodigoReferencia', 68, NULL, 'short_text', 1, 10, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('NombreExportador', 68, NULL, 'short_text', 1, 9, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('CalidadExportador', 68, NULL, 'short_text', 1, 7, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('CodigoInstitucion', 68, NULL, 'short_text', 1, 6, NULL, NULL, '.*128.*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('CodigoPaisDestino', 68, NULL, 'short_text', 1, 11, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('Importe', 69, NULL, 'amount', 1, 2, NULL, 'UYP|USD|EUR', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('Disclaimer', 69, NULL, 'disclaimer', 1, 4, 'Texto no modificable que se presenta al cliente y que debe aceptar a través de la verificación de un campo de aceptación para poder continuar.', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('mensajeAlBanco', 69, NULL, 'long_text', 1, 5, 'Sírvase enviar SWIFT al exterior reclamando el pago de la cobranza de referencia', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('CuentaDebitoGastos', 69, NULL, 'productList', 1, 3, NULL, 'CC|CA', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ReferenciaDeCobranza', 69, NULL, 'short_text', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fecha', 70, NULL, 'date', 1, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('validez', 70, NULL, 'date', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('concepto', 70, NULL, 'short_text', 1, 6, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 70, NULL, 'disclaimer', 1, 7, 'Texto no modificable que se presenta al cliente y que debe aceptar a través de la verificación de un campo de aceptación para poder continuar.', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('montoMaximo', 70, NULL, 'amount', 1, 2, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('beneficiario', 70, NULL, 'short_text', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('montoEnLetras', 70, NULL, 'short_text', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('obligacionesDe', 70, NULL, 'short_text', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('ci', 71, NULL, 'short_text', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fecha', 71, NULL, 'date', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('nombre', 71, NULL, 'short_text', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('importe', 71, NULL, 'amount', 1, 3, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 71, NULL, 'disclaimer', 1, 6, 'Texto no modificable que se presenta al cliente y que debe aceptar a través de la verificación de un campo de aceptación para poder continuar.', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('numeroDocCredito', 71, NULL, 'short_text', 1, 2, NULL, NULL, '[0-9]*', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 72, NULL, 'disclaimer', 1, 3, 'Texto no modificable que se presenta al cliente y que debe aceptar a través de la verificación de un campo de aceptación para poder continuar.', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('cartaCreditoNro', 72, NULL, 'short_text', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('fecha', 81, NULL, 'date', 1, 0, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('validez', 81, NULL, 'date', 1, 3, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('concepto', 81, NULL, 'short_text', 1, 5, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('disclaimer', 81, NULL, 'disclaimer', 1, 6, 'Texto no modificable que se presenta al cliente y que debe aceptar a través de la verificación de un campo de aceptación para poder continuar.', NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('montoMaximo', 81, NULL, 'amount', 1, 2, NULL, 'UYP|USD', '(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?', NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('beneficiario', 81, NULL, 'short_text', 1, 1, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('obligacionesDe', 81, NULL, 'short_text', 1, 4, NULL, NULL, NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('descripcion', 82, NULL, 'long_text', 1, 2, '', '', NULL, NULL, NULL);
INSERT INTO form_fields(id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value)
  VALUES('tipoSolicitud', 82, NULL, 'select', 1, 1, '', 'Documentacion|Certificado|Otro', NULL, NULL, NULL);
--83
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('cantidadNumero', 83, NULL, 'short_text', 1, 7, '', '', '[\\d][\\d]*', NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('cantidadSerie', 83, NULL, 'short_text', 1, 6, '', '', '[0-9a-zA-Z][0-9a-zA-Z]*', NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('cuenta', 83, NULL, 'productList', 1, 1, '', 'CC', NULL, NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('moneda', 83, NULL, 'select', 1, 3, '', 'UYP|USD', NULL, NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('primerChequeNumero', 83, NULL, 'short_text', 1, 5, '', '', '[\\d][\\d]*', NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('primerChequeSerie', 83, NULL, 'short_text', 1, 4, '', '', '[0-9a-zA-Z][0-9a-zA-Z]*', NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('tipoDeChequera', 83, NULL, 'select', 1, 2, '', 'Normal sin cruzar|Normal cruzado|Diferido sin cruzar|Diferido cruzado|Fanfold', NULL, NULL, NULL, NULL);
--84
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('cuenta', 84, NULL, 'productList', 1, 1, '', 'CC', NULL, NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('moneda', 84, NULL, 'select', 1, 3, '', 'UYP|USD', NULL, NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('motivo', 84, NULL, 'long_text', 1, 6, '', '', NULL, NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('numeroDeChequeNumero', 84, NULL, 'short_text', 1, 5, '', '', '[\\d][\\d]*', NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('numeroDeChequeSerie', 84, NULL, 'short_text', 1, 4, '', '', '[0-9a-zA-Z][0-9a-zA-Z]*', NULL, NULL, NULL);
INSERT INTO form_fields (id_field, id_form, description, field_type, mandatory, ordinal, default_value, possible_values, validation_regexp, depends_on_id_field, depends_on_value, required_permission)
  VALUES ('tipoDeChequera', 84, NULL, 'select', 1, 2, '', 'Normal sin cruzar|Normal cruzado|Diferido sin cruzar|Diferido cruzado|Fanfold', NULL, NULL, NULL, NULL);