-- This script is not intended to be executed using dbvs. Only for the purpose of creating a new user with products in a certain environment

BEGIN TRAN

    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    -- USER VARIABLE SETTING --
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
DECLARE @name varchar(30) = 'Juan Eduardo';
    DECLARE @lastname varchar(30) = 'Perez';
    DECLARE @document varchar(30) = '61633589';
    DECLARE @email varchar(50) = 'jeperez@technisys.com';
    DECLARE @address varchar(50) = 'Dionsio Oribe 3071';
    DECLARE @gender varchar(50) = 'M'; -- M/F
    DECLARE @cellphone varchar(30) = '59899507450';
    DECLARE @doctype varchar(30) = 'PAS'; -- List
    DECLARE @country varchar(30) = 'UY'; -- List
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    -- END USER VARIABLE SETTING --
    ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------


    -- General variables
    DECLARE @date datetime = getdate();
    DECLARE @corpenv varchar(30);
    DECLARE @retailenv varchar(30);
    DECLARE @groupcorpenv varchar(30);
    DECLARE @iduser varchar(30);
    DECLARE @fullname varchar(30);

    -- Accounts, Loans & Credit Cards
    DECLARE @account1 varchar(30) = CAST ((SELECT MAX(numero_cuenta) FROM demo_cuentas) AS numeric) + 1;
    DECLARE @account2 varchar(30) = CAST ((SELECT MAX(numero_cuenta) FROM demo_cuentas) AS numeric) + 2;
    DECLARE @account3 varchar(30) = CAST ((SELECT MAX(numero_cuenta) FROM demo_cuentas) AS numeric) + 3;
    DECLARE @account4 varchar(30) = CAST ((SELECT MAX(numero_cuenta) FROM demo_cuentas) AS numeric) + 4;
    DECLARE @account5 varchar(30) = CAST ((SELECT MAX(numero_cuenta) FROM demo_cuentas) AS numeric) + 5;
    DECLARE @account6 varchar(30) = CAST ((SELECT MAX(numero_cuenta) FROM demo_cuentas) AS numeric) + 6;
    DECLARE @loan1 varchar(30) = CAST ((SELECT MAX(numero_prestamo) FROM demo_prestamos) AS numeric) + 1;
    DECLARE @loan2 varchar(30) = CAST ((SELECT MAX(numero_prestamo) FROM demo_prestamos) AS numeric) + 2;
    DECLARE @loan3 varchar(30) = CAST ((SELECT MAX(numero_prestamo) FROM demo_prestamos) AS numeric) + 3;
    DECLARE @ccard1 varchar(30) = CAST ((SELECT MAX(numero_tarjeta) FROM demo_tarjetas) AS numeric) + 1;
    DECLARE @ccard1account varchar(30) = CAST ((SELECT MAX(cuenta) FROM demo_tarjetas) AS numeric) + 1;
    DECLARE @ccard2 varchar(30) = CAST ((SELECT MAX(numero_tarjeta) FROM demo_tarjetas) AS numeric) + 2;
    DECLARE @ccard2account varchar(30) = CAST ((SELECT MAX(cuenta) FROM demo_tarjetas) AS numeric) + 2;

   -- User Variables internal-automatic load
    SET @iduser = @country + @document;
    SET @fullname = @name + ' ' + @lastname;
---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

IF not exists(select id_user from users where id_user = @iduser)
BEGIN
    -- Create 3 new Environments: Corporate, Retail & Corporate Group




    INSERT INTO environments
    (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
    VALUES (@fullname + ' Corporate', 'active', 'corporate', 0, @date - 365, 'advanced', 'daily', 1, null);

    -- Corporate
    SET @corpenv = (SELECT MAX(id_environment) FROM environments);

    UPDATE environments SET product_group_id = @corpenv where id_environment = @corpenv

    INSERT INTO clients (id_client, name) values (@corpenv, @fullname + ' Corporate');

    INSERT INTO environment_clients (id_client, id_environment) values (@corpenv, @corpenv);

    INSERT INTO environments
    (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
    VALUES (@fullname + ' Retail', 'active', 'retail', 0, @date - 365, 'simple', 'daily', 1, null);


    SET @retailenv = (SELECT MAX(id_environment) FROM environments);

    UPDATE environments SET product_group_id = @retailenv where id_environment = @retailenv

    INSERT INTO clients (id_client, name) values (@retailenv, @fullname + ' Retail');

    INSERT INTO environment_clients (id_client, id_environment) values (@retailenv, @retailenv);

    INSERT INTO environments
    (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
    VALUES (@fullname + ' Holding', 'active', 'corporateGroup', 0, @date - 365, 'simple', 'daily', 1, NULL);

    SET @groupcorpenv = (SELECT MAX(id_environment) FROM environments);

    INSERT INTO clients (id_client, name) values (@groupcorpenv, @fullname + ' Holding');

    -- Association with the other user Environments ("Clients") - Corporate and Retail - to the Corporate Group
    INSERT INTO environment_clients (id_client, id_environment) values (@corpenv, @groupcorpenv);
    INSERT INTO environment_clients (id_client, id_environment) values (@retailenv, @groupcorpenv);

    -- Association with de other Environments ("Clients", without permissions) to the Corporate Group / Devkoa y Jhon Snow
    --INSERT INTO environment_clients (id_client, id_environment) values ('1000', @groupcorpenv);
    --INSERT INTO environment_clients (id_client, id_environment) values ('1001', @groupcorpenv);


    -- User's creation for the 3 Environments
    INSERT INTO users
    (id_user, email, id_user_status, deleted, first_name, last_name, password, lang, logged_in, creation_date, id_seal, mobile_number, trustfull, last_password_change, username, document, document_type, document_country)
    VALUES (@iduser, @email, 'active', 0, @name, @lastname, '5f4dcc3b5aa765d61d8327deb882cf99', 'en', 0, @date - 365, 5, @cellphone, 1, @date - 365, @email, @document, @doctype, @country);

    INSERT INTO environment_users
    (id_user, id_environment, id_user_status, signature_level, creation_date, enabled_channels)
    SELECT @iduser, id_environment, 'active', 'A', @date - 365,'frontend,phonegap' FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail', @fullname + ' Holding');

    -- Groups & permissions
    INSERT INTO groups
    (id_environment, name, blocked, deleted, description)
    SELECT id_environment, 'Administrator', 0, 0, 'Administrators' FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');

    INSERT INTO groups
    (id_environment, name, blocked, deleted, description)
    SELECT id_environment, 'Holding', 0, 0, 'Holding' FROM environments WHERE name IN (@fullname + ' Holding');

    INSERT INTO group_users (id_group, id_user)
    SELECT G.id_group, @iduser
    FROM groups G, environments E
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Corporate', @fullname + ' Retail', @fullname + ' Holding');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, P.id_permission, 'ALL_CA'
    FROM groups G, environments E, permissions P
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Corporate', @fullname + ' Retail')
        AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, P.id_permission, 'ALL_CC'
    FROM groups G, environments E, permissions P
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Corporate', @fullname + ' Retail')
        AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, P.id_permission, 'ALL_CA'
    FROM groups G, environments E, permissions P
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Holding')
        AND P.id_permission in ('product.read');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, P.id_permission, 'ALL_CC'
    FROM groups G, environments E, permissions P
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Holding')
        AND P.id_permission in ('product.read');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, P.id_permission, 'NONE'
    FROM groups G, environments E, permissions P
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Holding')
        AND P.id_permission in ('core.authenticated', 'core.forms.send', 'core.listTransactions', 'core.loginWithOTP', 'core.loginWithPassword', 'core.loginWithPIN', 'core.readForm', 'core.readTransaction', 'user.preferences', 'user.preferences.withSecondFactor');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, 'product.read', 'ALL_PA'
    FROM groups G, environments E
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Corporate', @fullname + ' Retail', @fullname + ' Holding');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, 'product.read', 'ALL_PF'
    FROM groups G, environments E
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Corporate', @fullname + ' Retail', @fullname + ' Holding');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, 'product.read', 'ALL_PI'
    FROM groups G, environments E
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Corporate', @fullname + ' Retail', @fullname + ' Holding');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, 'product.read', 'ALL_TC'
    FROM groups G, environments E
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Corporate', @fullname + ' Retail', @fullname + ' Holding');

    INSERT INTO group_permissions(id_group, id_permission, target)
    SELECT G.id_group, P.id_permission, 'NONE'
    FROM groups G, environments E, permissions P
    WHERE G.id_environment = E.id_environment AND E.name IN (@fullname + ' Corporate', @fullname + ' Retail')
        AND P.id_permission not in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

--    Bitters 30 June 2020: Signatures & Caps must be inserted into Limits MS database!!!!

    -- Signature schemes
--    INSERT INTO signatures (id_environment, signature_group, signature_type)
--    SELECT id_environment, 'A', 'ADM'
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO signatures (id_environment, signature_group, signature_type)
--    SELECT id_environment, 'A', 'AMOUNT'
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO signatures (id_environment, signature_group, signature_type)
--    SELECT id_environment, 'A', 'NO_AMOUNT'
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    -- User limits
--    INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'all', @iduser, 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'frontend', @iduser, 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'phonegap', @iduser, 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'facebook', @iduser, 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'payments', @iduser, 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'sms', @iduser, 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'twitter', @iduser, 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    -- Environments limits
--    INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'all', 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'frontend', 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'phonegap', 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'facebook', 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'payments', 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'sms', 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--    SELECT id_environment, 'twitter', 'daily', -1, 0.000, @date - 365
--    FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');
--
--    -- Signatures
--    INSERT INTO caps_for_signature (id_environment, channel, id_signature, frequency, maximum, used, used_last_reset)
--    SELECT S.id_environment, 'all', S.id_signature, 'daily', -1, 0.000, @date - 365
--    FROM environments E, signatures S WHERE E.id_environment = S.id_environment AND E.name IN (@fullname + ' Corporate', @fullname + ' Retail') AND S.signature_type = 'AMOUNT';

    -- Widgets
    INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
        SELECT @iduser, id_environment, 'portfolio', 1, 0
        FROM environments WHERE name IN (@fullname + ' Corporate');

    INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
        SELECT @iduser, id_environment, 'accounts', 1, 1
        FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');

    INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
        SELECT @iduser, id_environment, 'creditCards', 1, 2
        FROM environments WHERE name IN (@fullname + ' Retail');

    INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
        SELECT @iduser, id_environment, 'loans', 1, 3
        FROM environments WHERE name IN (@fullname + ' Retail');

    INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
        SELECT @iduser, id_environment, 'exchangeRates', 1, 4
        FROM environments WHERE name IN (@fullname + ' Corporate', @fullname + ' Retail');

    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    -- Data for DEMO
    -----------------------------------------------------------------------
    INSERT INTO demo_clientes
    (id_cliente, nombres, apellidos, email, direccion, telefono, celular, documento, tipo_documento, pais_residencia, nacionalidad, estado_civil, sexo, ocupacion, segmento,  direccion_correspondencia, envio_fisico_estado_cuenta)
    VALUES (@corpenv, @fullname + ' Corporate', @lastname, @email, @address, '', @cellphone, @document, @doctype, @country, 'Argentina', 'N/A', @gender, 'N/A', 'CMB', '', 1);

    INSERT INTO demo_clientes_persona (id_cliente, documento, tipo_documento, pais_documento)
    VALUES (@corpenv, @document, @doctype, @country);

    INSERT INTO demo_clientes
    (id_cliente, nombres, apellidos, email, direccion, telefono, celular, documento, tipo_documento, pais_residencia, nacionalidad, estado_civil, sexo, ocupacion, segmento,  direccion_correspondencia, envio_fisico_estado_cuenta)
    VALUES (@retailenv, @fullname + ' Retail', @lastname, @email, @address, '', @cellphone, @document, @doctype, @country, 'Argentina', 'N/A', @gender, 'N/A', 'PFS', '', 1);

    INSERT INTO demo_clientes_persona (id_cliente, documento, tipo_documento, pais_documento)
    VALUES (@retailenv, @document, @doctype, @country);

    -----------------------------------------------------------------------
    -- PRODUCTS AND MOVEMENTS FOR ENVIRONMENT (Corporate)
    -----------------------------------------------------------------------

    -- Cuentas
    INSERT INTO demo_cuentas (numero_cuenta, cliente, moneda, tipo, linea_sobregiro, saldo_pendiente)
        VALUES (@account1, @corpenv, 'USD', 'CA', 178000, 0);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account1, @date - 50, @date - 50, 'Initial Balance', '', 0, 24500, 24500);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account1, @date - 42, @date - 42, 'Service Payment', '', 1000, 0, 23500);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account1, @date - 9, @date - 9, 'ATM Cash out', '', 2500, 0, 21000);

    INSERT INTO demo_cuentas (numero_cuenta, cliente, moneda, tipo, linea_sobregiro, saldo_pendiente)
        VALUES (@account2, @corpenv, 'EUR', 'CA', 68000, 0);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account2, @date - 20, @date - 20, 'Initial Balance', '', 0, 3000, 3000);

    INSERT INTO demo_cuentas (numero_cuenta, cliente, moneda, tipo, linea_sobregiro, saldo_pendiente)
        VALUES (@account3, @corpenv, 'CAD', 'CC', 15000, 0);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account3, @date - 10, @date - 10, 'Initial Balance', '', 0, 87000, 87000);

    INSERT INTO demo_cuentas (numero_cuenta, cliente, moneda, tipo, linea_sobregiro, saldo_pendiente)
        VALUES (@account4, @corpenv, 'USD', 'CC', 2000, 0);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account4, @date - 25, @date - 25, 'Initial Balance', '', 0, 3000, 45000);

    -- Cards
    INSERT INTO demo_tarjetas (numero_tarjeta, cliente, cuenta, saldo_uyp, saldo_usd, vencimiento, cierre, saldo_disponible, limite_credito)
        VALUES (@ccard1, @corpenv, @ccard1account, 17980, 869, @date + 360, @date + 20, 30000, 0);

    -- Loans (2)
    INSERT INTO demo_prestamos (numero_prestamo, cliente, tipo, importe_total, fecha_proximo_vencimiento, valor_cuota, moneda)
        VALUES (@loan1, @corpenv, 'PA', 50000, @date + 30, 5000, 'EUR');
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date - 90, 5000, 45000, 'Payment fee', 1, 1);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date - 60, 5000, 40000, 'Payment fee', 1, 2);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date - 30, 5000, 35000, 'Payment fee', 1, 3);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date + 30, 5000, 30000, 'Payment fee', 0, 4);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date + 60, 5000, 25000, 'Payment fee', 0, 5);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date + 90, 5000, 20000, 'Payment fee', 0, 6);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date + 120, 5000, 15000, 'Payment fee', 0, 7);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date + 150, 5000, 10000, 'Payment fee', 0, 8);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date + 180, 5000, 5000, 'Payment fee', 0, 9);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan1, @date + 210, 5000, 0, 'Payment fee', 0, 10);

    INSERT INTO demo_prestamos (numero_prestamo, cliente, tipo, importe_total, fecha_proximo_vencimiento, valor_cuota, moneda)
        VALUES (@loan2, @corpenv, 'PA', 5000, @date + 15, 500, 'USD');
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date - 105, 500, 4500, 'Payment fee', 1, 1);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date - 90, 500, 4000, 'Payment fee', 1, 2);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date - 75, 500, 3500, 'Payment fee', 1, 3);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date - 60, 500, 3000, 'Payment fee', 1, 4);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date - 45, 500, 2500, 'Payment fee', 1, 5);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date - 30, 500, 2000, 'Payment fee', 1, 6);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date - 15, 500, 1500, 'Payment fee', 1, 7);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date + 10, 500, 1000, 'Payment fee', 0, 8);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date + 30, 500, 500, 'Payment fee', 0, 9);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan2, @date + 45, 500, 0, 'Payment fee', 0, 10);

    --------------------------------------------------------------------
    -- PRODUCTS AND MOVEMENTS FOR ENVIRONMENT (Retail)
    --------------------------------------------------------------------
    -- Accounts
    INSERT INTO demo_cuentas (numero_cuenta, cliente, moneda, tipo, linea_sobregiro, saldo_pendiente)
        VALUES (@account5, @retailenv, 'USD', 'CA', 178000, 0);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 90, @date - 90, 'Initial Balance', '', 0, 15000, 15000);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 85, @date - 85, 'Service payment', '', 1000, 0, 14000);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 81, @date - 81, 'ATM Cash out', '', 2000, 0, 12000);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 74, @date - 74, 'Depósito ATM', '', 0, 3800, 15800);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 57, @date - 57, 'Cash debit - Commerce', '', 700, 0, 15100);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 52, @date - 52, 'Service payment', '', 1100, 0, 14000);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 45, @date - 45, 'Service payment', '', 290, 0, 13710);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 39, @date - 39, 'Service payment', '', 800, 0, 12910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 34, @date - 34, 'Salary', '', 0, 43000, 55910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 33, @date - 33, 'Cash debit - Commerce', '', 1000, 0, 54910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 30, @date - 30, 'Cash debit - Commerce', '', 7000, 0, 47910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 28, @date - 28, 'ATM Cash out', '', 3000, 0, 44910);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 26, @date - 26, 'Cash debit - Commerce', '', 900, 0, 44010);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 18, @date - 18, 'Cash debit - Commerce', '', 450, 0, 43560);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 15, @date - 15, 'Cash debit - Commerce', '', 70, 0, 43490);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 11, @date - 11, 'Service payment', '', 2500, 0, 40990);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 7, @date - 7, 'ATM Cash out', '', 4000, 0, 36990);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account5, @date - 3, @date - 3, 'Salary', '', 0, 42000, 78990);

    INSERT INTO demo_cuentas (numero_cuenta, cliente, moneda, tipo, linea_sobregiro, saldo_pendiente)
        VALUES (@account6, @retailenv, 'EUR', 'CA', 0, 0);
    INSERT INTO demo_cuentas_movimientos (numero_cuenta, fecha, fecha_valor, concepto, referencia, debito, credito, saldo)
    VALUES (@account6, @date - 42, @date - 42, 'Initial Balance', '', 0, 3000, 3000);

    -- Cards
    INSERT INTO demo_tarjetas (numero_tarjeta, cliente, cuenta, saldo_uyp, saldo_usd, vencimiento, cierre, saldo_disponible, limite_credito)
        VALUES (@ccard2, @retailenv, @ccard2account, 8000, 4000, @date + 360, @date + 15, 30000, 0);

    -- Loan (1)
    INSERT INTO demo_prestamos (numero_prestamo, cliente, tipo, importe_total, fecha_proximo_vencimiento, valor_cuota, moneda)
        VALUES (@loan3, @retailenv, 'PA', 50000, @date + 30, 5000, 'USD');
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date - 90, 5000, 45000, 'Payment fee', 1, 1);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date - 60, 5000, 40000, 'Payment fee', 1, 2);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date - 30, 5000, 35000, 'Payment fee', 1, 3);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date + 30, 5000, 30000, 'Payment fee', 0, 4);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date + 60, 5000, 25000, 'Payment fee', 0, 5);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date + 90, 5000, 20000, 'Payment fee', 0, 6);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date + 120, 5000, 15000, 'Payment fee', 0, 7);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date + 150, 5000, 10000, 'Payment fee', 0, 8);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date + 180, 5000, 5000, 'Payment fee', 0, 9);
    INSERT INTO demo_prestamos_movimientos (numero_prestamo, fecha, importe, saldo, concepto, paga, numero_cuota)
    VALUES (@loan3, @date + 210, 5000, 0, 'Payment fee', 0, 10);

END
ELSE
BEGIN
	PRINT N'Document: '+@iduser+' already exists in the Database.';
END
commit