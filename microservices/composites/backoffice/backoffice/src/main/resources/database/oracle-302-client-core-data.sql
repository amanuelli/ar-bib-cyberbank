-- =============================================================================
-- ======== v0.0.1_004 =========================================================
-- =============================================================================

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Configuracion
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.rowsPerPage', '10', '', 'Rubicon');
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.dateFormat', 'd-MMM-yyyy', '', 'Rubicon');
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.monthYearFormat', 'MMM-yyyy', '', 'Rubicon');
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.decimalFormat', '#,##0.00', '', 'Rubicon');
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.maxStatementsToShow', '100', '', 'Rubicon');
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.defaultUserRole', 'client_operator', '', 'Rubicon');
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.digitalCertificate.outboxPath', 'c:/tmp', '', 'Core');

-- Convertidor de monedas
INSERT INTO configuration (id_field, value, possible_values, id_group) VALUES ('currencyConverter', 'com.technisys.rubicon.utils.RubiconCurrencyConverter', '', 'Core');
INSERT INTO configuration (id_field, value, possible_values, id_group) VALUES ('masterCurrency', 'UYP', 'UYP|USD|EUR', 'Core');

INSERT INTO configuration (id_field, value, possible_values, id_group) VALUES ('rubicon_accounts_max_movements', '100', '', 'Rubicon');


-- Actividad rub.login
INSERT INTO permissions (id_permission, permission_type) VALUES ('rub.login', 'client');

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.login', 1, 1, 'com.technisys.rubicon.business.misc.activities.LoginActivity', 'rub.login');

INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.logout', 1, 1, 'com.technisys.rubicon.business.misc.activities.LogoutActivity', null);

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) VALUES ('rub.login', 'onlyPassword');

-- Permission rub.account.read
INSERT INTO permissions (id_permission, permission_type) VALUES ('rub.account.read', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) VALUES ('rub.account.read', 'onlyPassword');


-- Actividad rub.account.list
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.account.list', 1, 1, 'com.technisys.rubicon.business.accounts.activities.ListAccountsActivity', null);

-- Actividad rub.account.accountDetails
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.account.accountDetails', 1, 1, 'com.technisys.rubicon.business.accounts.activities.ReadAccountActivity', 'rub.account.read');

-- Actividad rub.account.exportList
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.account.exportList', 1, 1, 'com.technisys.rubicon.business.accounts.activities.ExportListActivity', 'rub.account.read');

-- Actividad rub.account.exportAccountDetails
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.account.exportAccountDetails', 1, 1, 'com.technisys.rubicon.business.accounts.activities.ExportAccountActivity', 'rub.account.read');

-- Actividad rub.product.list
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.product.list', 1, 1, 'com.technisys.rubicon.business.misc.activities.ListProductsActivity', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.loan.read
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.loan.read', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.loan.read', 'onlyPassword');


-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.loan.exportList
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.loans.exportLoansMovementsList', 1, 1, 'com.technisys.rubicon.business.loans.activities.ExportLoansMovementsActivity', 'rub.loan.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.loan.exportLoanDetails
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.loans.exportLoanDetails', 1, 1, 'com.technisys.rubicon.business.loans.activities.ExportLoanActivity', 'rub.loan.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.loans.list
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.loans.list', 1, 1, 'com.technisys.rubicon.business.loans.activities.ListLoansActivity', 'rub.loan.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.loans.readDetail
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.loans.readDetail', 1, 1, 'com.technisys.rubicon.business.loans.activities.ReadLoanActivity', 'rub.loan.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.creditcard.read
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.creditcard.read', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.creditcard.read', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.creditcards.list
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.creditcards.list', 1, 1, 'com.technisys.rubicon.business.creditcards.activities.ListActivity', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.creditcards.detailPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.creditcards.detailPre', 1, 1, 'com.technisys.rubicon.business.creditcards.activities.DetailPreActivity', 'rub.creditcard.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.creditcards.readCreditCard
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.creditcards.readCreditCard', 1, 1, 'com.technisys.rubicon.business.creditcards.activities.ReadCreditCardActivity', 'rub.creditcard.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.creditcards.exportTransactions
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.creditcards.exportTransactions', 1, 1, 'com.technisys.rubicon.business.creditcards.activities.ExportTransactionsActivity', 'rub.creditcard.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.desktop.loadLayout
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.desktop.loadLayout', 1, 1, 'com.technisys.rubicon.business.desktop.activities.LoadLayout', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.desktop.saveLayout
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.desktop.saveLayout', 1, 1, 'com.technisys.rubicon.business.desktop.activities.SaveLayout', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.desktop.listPendingTransactions
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.desktop.listPendingTransactions', 1, 1, 'com.technisys.rubicon.business.desktop.activities.ListPendingTransactionsActivity', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.desktop.listAccounts
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.desktop.listAccounts', 1, 1, 'com.technisys.rubicon.business.desktop.activities.ListAccountsActivity', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.accounts.transferInternal
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type) VALUES ('rub.accounts.transferInternal', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) VALUES ('rub.accounts.transferInternal', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.transferInternal
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.transferInternal', 1, 1, 'com.technisys.rubicon.business.accounts.activities.TransferInternalActivity', 'rub.accounts.transferInternal');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.transferInternalPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.transferInternalPre', 1, 1, 'com.technisys.rubicon.business.accounts.activities.TransferInternalPreActivity', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permission rub.users.manage
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type) VALUES ('rub.users.manage', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) VALUES ('rub.users.manage', 'onlyPassword');

-- Actividad rub.users.list
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.list', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.ListUsersActivity', 'rub.users.manage');

-- Actividad rub.users.userDetails
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.userDetails', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.ReadUsersActivity', 'rub.users.manage');

-- Actividad rub.users.createPre
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.createPre', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.CreatePreUsersActivity', 'rub.users.manage');

-- Actividad rub.users.create
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.create', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.CreateUsersActivity', 'rub.users.manage');

-- Actividad rub.users.modifyPre
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.modifyPre', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.ModifyPreUsersActivity', 'rub.users.manage');

-- Actividad rub.users.modify
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.modify', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.ModifyUsersActivity', 'rub.users.manage');

-- Actividad rub.users.generatePassword
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.generatePassword', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.GeneratePasswordActivity', 'rub.users.manage');

-- Actividad rub.users.blockUnblock
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.blockUnblock', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.BlockUnblockUsersActivity', 'rub.users.manage');

-- Actividad rub.users.delete
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.delete', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.DeleteUsersActivity', 'rub.users.manage');

-- Actividad rub.users.exportListUsers
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.users.exportListUsers', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.ExportListUsersActivity', 'rub.users.manage');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permission rub.userconfiguration.manage
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type) VALUES ('rub.userconfiguration.manage', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) VALUES ('rub.userconfiguration.manage', 'onlyPassword');

-- Actividad rub.userconfiguration.modifyPre
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.userconfiguration.modifyPre', 1, 1, 'com.technisys.rubicon.business.userconfiguration.activities.ModifyPreActivity', 'rub.userconfiguration.manage');

-- Actividad rub.userconfiguration.modify
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.userconfiguration.modify', 1, 1, 'com.technisys.rubicon.business.userconfiguration.activities.ModifyActivity', 'rub.userconfiguration.manage');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permission rub.groups.manage
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type) VALUES ('rub.groups.manage', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) VALUES ('rub.groups.manage', 'onlyPassword');

-- Actividad rub.groups.list
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.groups.list', 1, 1, 'com.technisys.rubicon.business.administration.groups.activities.ListGroupsActivity', 'rub.groups.manage');

-- Actividad rub.groups.groupDetails
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.groups.groupDetails', 1, 1, 'com.technisys.rubicon.business.administration.groups.activities.ReadGroupsActivity', 'rub.groups.manage');

-- Actividad rub.groups.createPre
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.groups.createPre', 1, 1, 'com.technisys.rubicon.business.administration.groups.activities.CreatePreGroupsActivity', 'rub.groups.manage');

-- Actividad rub.groups.create
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.groups.create', 1, 1, 'com.technisys.rubicon.business.administration.groups.activities.CreateGroupsActivity', 'rub.groups.manage');

-- Actividad rub.groups.modifyPre
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.groups.modifyPre', 1, 1, 'com.technisys.rubicon.business.administration.groups.activities.ModifyPreGroupsActivity', 'rub.groups.manage');

-- Actividad rub.groups.modify
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.groups.modify', 1, 1, 'com.technisys.rubicon.business.administration.groups.activities.ModifyGroupsActivity', 'rub.groups.manage');

-- Actividad rub.groups.blockUnblock
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.groups.blockUnblock', 1, 1, 'com.technisys.rubicon.business.administration.groups.activities.BlockUnblockGroupsActivity', 'rub.groups.manage');

-- Actividad rub.groups.delete
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.groups.delete', 1, 1, 'com.technisys.rubicon.business.administration.groups.activities.DeleteGroupsActivity', 'rub.groups.manage');

-- Actividad rub.groups.exportListGroups
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.groups.exportListGroups', 1, 1, 'com.technisys.rubicon.business.administration.groups.activities.ExportListGroupsActivity', 'rub.groups.manage');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permission rub.authorizations.manage
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.authorizations.manage', 'client');
INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.authorizations.manage', 'onlyPassword');

-- Actividad rub.authorizations.list
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.authorizations.list', 1, 1, 'com.technisys.rubicon.business.administration.authorizations.activities.ListAuthorizationsActivity', 'rub.authorizations.manage');

-- Actividad rub.authorizations.authorizationDetails
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.authorizations.authorizationDetails', 1, 1, 'com.technisys.rubicon.business.administration.authorizations.activities.ReadAuthorizationActivity', 'rub.authorizations.manage');

-- Actividad rub.authorizations.createPre
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.authorizations.createPre', 1, 1, 'com.technisys.rubicon.business.administration.authorizations.activities.CreatePreAuthorizationActivity', 'rub.authorizations.manage');

-- Actividad rub.authorizations.create
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.authorizations.create', 1, 1, 'com.technisys.rubicon.business.administration.authorizations.activities.CreateAuthorizationActivity', 'rub.authorizations.manage');

-- Actividad rub.authorizations.modifyPre
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.authorizations.modifyPre', 1, 1, 'com.technisys.rubicon.business.administration.authorizations.activities.ModifyPreAuthorizationActivity', 'rub.authorizations.manage');

-- Actividad rub.authorizations.modify
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.authorizations.modify', 1, 1, 'com.technisys.rubicon.business.administration.authorizations.activities.ModifyAuthorizationActivity', 'rub.authorizations.manage');

-- Actividad rub.authorizations.delete
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.authorizations.delete', 1, 1, 'com.technisys.rubicon.business.administration.authorizations.activities.DeleteAuthorizationActivity', 'rub.authorizations.manage');

-- Actividad rub.authorizations.exportAuthorizations
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.authorizations.exportAuthorizations', 1, 1, 'com.technisys.rubicon.business.administration.authorizations.activities.ExportListAuthorizationsActivity', 'rub.authorizations.manage');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.accounts.transferAbroad
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type) VALUES ('rub.accounts.transferAbroad', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) VALUES ('rub.accounts.transferAbroad', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.transferAbroad
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.transferAbroad', 1, 1, 'com.technisys.rubicon.business.accounts.activities.TransferAbroadActivity', 'rub.accounts.transferAbroad');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.transferAbroadPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.transferAbroadPre', 1, 1, 'com.technisys.rubicon.business.accounts.activities.TransferAbroadPreActivity', 'rub.accounts.transferAbroad');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.accounts.transferBatch
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.accounts.transferBatch', 'client');
INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.accounts.transferBatch', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.transferBatch
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.transferBatch', 1, 1, 'com.technisys.rubicon.business.accounts.activities.TransferBatchActivity', 'rub.accounts.transferBatch');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.transferBatchPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.transferBatchPre', 1, 1, 'com.technisys.rubicon.business.accounts.activities.TransferBatchPreActivity', 'rub.accounts.transferBatch');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.accounts.transferLocal
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type) VALUES ('rub.accounts.transferLocal', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) VALUES ('rub.accounts.transferLocal', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.transferLocalPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.transferLocalPre', 1, 1, 'com.technisys.rubicon.business.accounts.activities.TransferLocalPreActivity', 'rub.accounts.transferLocal');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.accounts.transferLocal
-- ---------------------------------------------------------------------------------------------------------------------------------------------


INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.accounts.transferLocal', 1, 1, 'com.technisys.rubicon.business.accounts.activities.TransferLocalActivity', 'rub.accounts.transferLocal');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.creditcard.pay
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.creditcard.pay', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) 
    VALUES ('rub.creditcard.pay', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.creditcards.payCreditCard
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.creditcards.payCreditCard', 1, 1, 'com.technisys.rubicon.business.creditcards.activities.PayCreditCardActivity', 'rub.creditcard.pay');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.creditcards.payCreditCardPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.creditcards.payCreditCardPre', 1, 1, 'com.technisys.rubicon.business.creditcards.activities.PayCreditCardPreActivity', 'rub.creditcard.pay');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.loan.pay
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.loan.pay', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) 
    VALUES ('rub.loan.pay', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.loans.payLoanPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.loans.payLoanPre', 1, 1, 'com.technisys.rubicon.business.loans.activities.PayLoanPreActivity', 'rub.loan.pay');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.loans.payLoan
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.loans.payLoan', 1, 1, 'com.technisys.rubicon.business.loans.activities.PayLoanActivity', 'rub.loan.pay');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.files.list
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.files.list', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) 
    VALUES ('rub.files.list', 'onlyPassword');

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.files.list', 1, 1, 'com.technisys.rubicon.business.file.activities.ListFilesActivity', 'rub.files.list');


-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.files.uploadPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.files.upload', 'client');

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.files.uploadPre', 1, 1, 'com.technisys.rubicon.business.file.activities.UploadFilePreActivity', 'rub.files.upload');


-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.files.upload
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.files.upload', 'onlyPassword');

INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.files.upload', 1, 1, 'com.technisys.rubicon.business.file.activities.UploadFileActivity', 'rub.files.upload');


-- =============================================================================
-- ======== v0.0.1_005 =========================================================
-- =============================================================================
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.batch.maxErrorsBeforeAbort', '10', '', 'Rubicon');

-- =============================================================================
-- ======== v0.0.1_006 =========================================================
-- =============================================================================

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permission rub.transactionhistory.read
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type) VALUES ('rub.transactionhistory.read', 'client');
INSERT INTO permissions_credentials_groups (id_permission, id_credential_group) VALUES ('rub.transactionhistory.read', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.transactionhistory.listPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.transactionhistory.listPre', 1, 1, 'com.technisys.rubicon.business.administration.transactionhistory.activities.ListPreActivity', 'rub.transactionhistory.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.transactionhistory.listPredefined
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.transactionhistory.listPredefined', 1, 1, 'com.technisys.rubicon.business.administration.transactionhistory.activities.ListPredefinedActivity', 'rub.transactionhistory.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.transactionhistory.listPersonalized
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.transactionhistory.listPersonalized', 1, 1, 'com.technisys.rubicon.business.administration.transactionhistory.activities.ListPersonalizedActivity', 'rub.transactionhistory.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.transactionhistory.detail
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.transactionhistory.detail', 1, 1, 'com.technisys.rubicon.business.administration.transactionhistory.activities.DetailActivity', 'rub.transactionhistory.read');

-- Actividad rub.notificationconfig.modifyPre
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.notificationconfig.modifyPre', 1, 1, 'com.technisys.rubicon.business.notificationsconfig.activities.ModifyPreActivity', null);

-- Actividad rub.userconfiguration.modify
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
        VALUES ('rub.notificationconfig.modify', 1, 1, 'com.technisys.rubicon.business.notificationsconfig.activities.ModifyActivity', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.read
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.read', 'client');


INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.read', 'onlyPassword');


-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade.read
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.read', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.read', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.loan.payDebit
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.loan.payDebit', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.loan.payDebit', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.loan.request
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.loan.request', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.loan.request', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.creditcards.request
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.creditcards.request', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.creditcards.request', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.creditcards.rePrint
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.creditcards.rePrint', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.creditcards.rePrint', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.accounts.requestCheckbook
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.accounts.requestCheckbook', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.accounts.requestCheckbook', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.accounts.requestDebit
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.accounts.requestDebit', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.accounts.requestDebit', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.accounts.requestAccountOpening
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.accounts.requestAccountOpening', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.accounts.requestAccountOpening', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.paySuppliers
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.paySuppliers', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.paySuppliers', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.payment.paySuppliers
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.payment.paySuppliers', 1, 1, 'com.technisys.rubicon.business.payments.activities.PaySuppliersActivity', 'rub.payment.paySuppliers');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.payment.paySuppliersPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.payment.paySuppliersPre', 1, 1, 'com.technisys.rubicon.business.payments.activities.PaySuppliersPreActivity', 'rub.payment.paySuppliers');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.paySalary
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.paySalary', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.paySalary', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.payment.paySalary
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.payment.paySalary', 1, 1, 'com.technisys.rubicon.business.payments.activities.PaySalaryActivity', 'rub.payment.paySalary');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.payment.paySalaryPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.payment.paySalaryPre', 1, 1, 'com.technisys.rubicon.business.payments.activities.PaySalaryPreActivity', 'rub.payment.paySalary');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.checkDiscount
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.checkDiscount', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.checkDiscount', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.billDiscount
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.billDiscount', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.billDiscount', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.serviceAssociation
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.serviceAssociation', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.serviceAssociation', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.payService
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.payService', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.payService', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.payDGI
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.payDGI', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.payDGI', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.payBPS
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.payBPS', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.payBPS', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.payIMC
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.payIMC', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.payIMC', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.payment.payIMF
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.payment.payIMF', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.payment.payIMF', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade.exportCreditLetterPurchase
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.exportCreditLetterPurchase', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.exportCreditLetterPurchase', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade..exportCreditLetterTransfer
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.exportCreditLetterTransfer', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.exportCreditLetterTransfer', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade.exportCreditLetterApproval
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.exportCreditLetterApproval', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.exportCreditLetterApproval', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade.exportPreFinancing
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.exportPreFinancing', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.exportPreFinancing', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade.importCreditLetterOpening
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.importCreditLetterOpening', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.importCreditLetterOpening', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade.importCreditLetterCanceling
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.importCreditLetterCanceling', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.importCreditLetterCanceling', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade.importCreditLetterDiscrepancies
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.importCreditLetterDiscrepancies', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.importCreditLetterDiscrepancies', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade.importCreditLetterRejection
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.importCreditLetterRejection', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.importCreditLetterRejection', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.foreigntrade.importCreditLetterModify
-- ---------------------------------------------------------------------------------------------------------------------------------------------

INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.foreigntrade.importCreditLetterModify', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.foreigntrade.importCreditLetterModify', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Configuracion
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO configuration (id_field, value, possible_values, id_group) VALUES ('rubicon.transferLocal.destinationBanks', 'Banco Comercial|||Banco Santander|||Rep&uacute;blica AFAP|||Banco Rep&uacute;blica', '', 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.fullDateTimeFormat', 'd-MMM-yyyy HH:mm', '', 'Rubicon');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Permiso rub.notificationinbox.read
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO permissions (id_permission, permission_type)
    VALUES ('rub.notificationinbox.read', 'client');

INSERT INTO permissions_credentials_groups (id_permission, id_credential_group)
    VALUES ('rub.notificationinbox.read', 'onlyPassword');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.notificationinbox.list
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.notificationinbox.list', 1, 1, 'com.technisys.rubicon.business.notificationinbox.activities.ListActivity', 'rub.notificationinbox.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.notificationinbox.markAsRead
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.notificationinbox.markAsRead', 1, 1, 'com.technisys.rubicon.business.notificationinbox.activities.MarkAsReadActivity', 'rub.notificationinbox.read');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Configuracion
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO configuration (id_field, value, possible_values, id_group) VALUES ('rubicon.notification.daysToEnd', '7', '', 'Rubicon');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.desktop.listNotifications
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.desktop.listNotifications', 1, 1, 'com.technisys.rubicon.business.desktop.activities.ListNotificationsActivity', null);

-- =============================================================================
-- ======== v0.0.1_007 =========================================================
-- =============================================================================

INSERT INTO configuration (id_field, value, possible_values, id_group) VALUES ('rubicon.debitAccounts', 'CC|CA', null, 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rub.administration.invitation.acceptUrl', '<PROTOCOL>://<SERVER>:<PORT>/<APP_NAME>/invitation/accept', '', 'Core');
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rub.administration.invitation.rejectUrl', '<PROTOCOL>://<SERVER>:<PORT>/<APP_NAME>/invitation/reject', '', 'Core');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.users.loadInvitation
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required) VALUES ('rub.users.loadInvitation', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.LoadInvitationActivity', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.users.rejectInvitation
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required) VALUES ('rub.users.rejectInvitation', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.RejectInvitationActivity', null);

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.users.acceptInvitationExistingUser
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required) VALUES ('rub.users.acceptInvitationExistingUser', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.AcceptInvitationExistingUserActivity', 'rub.login');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.users.acceptInvitationNewUser
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required) VALUES ('rub.users.acceptInvitationNewUser', 1, 1, 'com.technisys.rubicon.business.administration.users.activities.AcceptInvitationNewUserActivity', null);

-- Tipos de Ambientes
INSERT INTO configuration (id_field, value, possible_values, id_group) VALUES ('rubicon.environmentType.retail', 'retail', null, 'Rubicon');
INSERT INTO configuration (id_field, value, possible_values, id_group) VALUES ('rubicon.environmentType.corporate', 'corporate', null, 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.desktop.defaultWidgets.retail', 'transactions|news|notifications|frequentTasks|banners|productDetails|productSummarized|scheduler', '', 'Rubicon');
INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('rubicon.desktop.defaultWidgets.corporate', 'transactions|news|notifications|frequentTasks|banners|productDetails|productSummarized|scheduler', '', 'Rubicon');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Cambio de email
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO configuration (id_field, value, possible_values, id_group) VALUES ('rubicon.userconfiguration.confirmationmail.url', 'http://localhost:8084/client/userconfiguration/emailupdate', null, 'Rubicon');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.userconfiguration.changeUserEmailPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required) VALUES ('rub.userconfiguration.changeUserEmailPre', 1, 1, 'com.technisys.rubicon.business.userconfiguration.activities.ChangeUserEmailPreActivity', 'rub.userconfiguration.manage');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.userconfiguration.changeUserEmailPre
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required) VALUES ('rub.userconfiguration.changeUserEmail', 1, 1, 'com.technisys.rubicon.business.userconfiguration.activities.ChangeUserEmailActivity', 'rub.userconfiguration.manage');

-- Actividad rub.digitalCertificate.download
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO permissions (id_permission,permission_type) VALUES ('rub.certificate.download','client');
INSERT INTO permissions_credentials_groups (id_permission,id_credential_group) VALUES ('rub.certificate.download','otp');

INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required) VALUES ('rub.digitalCertificate.download', 1, 1, 'com.technisys.rubicon.business.digitalcertificate.activities.DigitalCertificateDownload', 'rub.certificate.download');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.banks.list
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.banks.list', 1, 1, 'com.technisys.rubicon.business.banks.activities.ListBankActivity', null);

INSERT INTO configuration (id_field, value, possible_values, id_group)
VALUES ('rubicon.currency.transferInternal.list', 'UYP|USD|EUR', null, 'Rubicon');

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.mobile.agentList', 'IPHONE|IPOD|BLACKBERRY|NOKIA|MOTOROLA|MOT-|SAMSUNG|SEC-|LG-|SONYERICSSON|SIE-|ANDROID', null, 'Rubicon');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- URL FORWADS VALID HOSTS
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.forward.url.validHosts', 'nbc.com.uy|bancocomercial.com.uy|192.168.253.55|192.168.254.55|200.40.163.20', null, 'Rubicon');

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.login.validElapsedTime', '40', null, 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group)
    VALUES('rubicon.productTypes.CA', 'CA', '', 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group)
    VALUES('rubicon.productTypes.CC', 'CC', '', 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group)
    VALUES('rubicon.productTypes.PF', 'PF', '', 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group)
    VALUES('rubicon.productTypes.PSI', 'PSI', '', 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group)
    VALUES('rubicon.productTypes.PCF', 'PCF', '', 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group)
    VALUES('rubicon.productTypes.PSIA', 'PSIA', '', 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group)
    VALUES('rubicon.productTypes.PSII', 'PSII', '', 'Rubicon');

INSERT INTO configuration(id_field, value, possible_values, id_group)
    VALUES('rubicon.productTypes.TC', 'TC|VISA|MASTER', '', 'Rubicon');

-- ---------------------------------------------------------------------------------------------------------------------------------------------
-- Actividad rub.notificationinbox.lastNotification
-- ---------------------------------------------------------------------------------------------------------------------------------------------
INSERT  INTO activities (id_activity, version, enabled, component_fqn, id_permission_required)
    VALUES ('rub.notificationinbox.lastNotification', 1, 1, 'com.technisys.rubicon.business.notificationinbox.activities.LastNotificationActivity', 'rub.notificationinbox.read');

INSERT INTO configuration(id_field, value, possible_values, id_group) VALUES('core.productLabeler', 'com.technisys.rubicon.utils.ProductLabeler', '', 'Core');

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.currency.payCreditCard.list', 'UYP|USD', null, 'Rubicon');

UPDATE configuration SET value = 'com.technisys.rubicon.utils.DependencyReader' WHERE id_field = 'core.dependencyReaderReader';

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.currency.paySuppliers.list', 'UYP|USD|EUR', null, 'Rubicon');

INSERT INTO configuration (id_field, value, possible_values, id_group)
    VALUES ('rubicon.currency.paySalary.list', 'UYP|USD|EUR', null, 'Rubicon');
