-- Dos nuevos ambientes, uno retail y otro corporate
INSERT INTO environments
  (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('John Doe Corporate', 'active', 'corporate', 0, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'advanced', 'daily', 1, '910');

INSERT INTO environments
  (name, id_environment_status, environment_type, deleted, creation_date, administration_scheme, cap_frequency, sms_enabled, product_group_id)
  VALUES ('John Doe Retail', 'active', 'retail', 0, TO_DATE('2018-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'simple', 'daily', 1, '911');

-- Creo un usuario los dos ambientes anteriores
INSERT INTO users
  (id_user, email, id_user_status, deleted, first_name, last_name, password, lang, logged_in, creation_date, id_seal, mobile_number, trustfull, last_password_change, username, document, document_type, document_country)
  VALUES ('f6cadaap64fe4hjead71456813d41af3', 'mush@forexfy.com', 'active', 0, 'John', 'Doe', '5f4dcc3b5aa765d61d8327deb882cf99', 'en', 0, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS'), 8, '59914007051', 1, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS'), 'mush@forexfy.com', '27687111', 'PAS', 'US');

-- Vinculo el usuario con los ambientes
INSERT INTO environment_users
  (id_user, id_environment, id_user_status, signature_level, creation_date, enabled_channels)
  SELECT 'f6cadaap64fe4hjead71456813d41af3', id_environment, 'active', 'A', TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS'),'frontend,phonegap' FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');

-- Grupos y permisos
INSERT INTO groups
  (id_environment, name, blocked, deleted, description)
  SELECT id_environment, 'Administrator', 0, 0, 'Administrators' FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');

INSERT INTO group_users (id_group, id_user)
  SELECT G.id_group, 'f6cadaap64fe4hjead71456813d41af3'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('John Doe Corporate', 'John Doe Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, P.id_permission, 'ALL_CA'
  FROM groups G, environments E, permissions P
  WHERE G.id_environment = E.id_environment AND E.name IN ('John Doe Corporate', 'John Doe Retail')
    AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, P.id_permission, 'ALL_CC'
  FROM groups G, environments E, permissions P
  WHERE G.id_environment = E.id_environment AND E.name IN ('John Doe Corporate', 'John Doe Retail')
    AND P.id_permission in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_PA'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('John Doe Corporate', 'John Doe Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_PF'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('John Doe Corporate', 'John Doe Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_PI'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('John Doe Corporate', 'John Doe Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, 'product.read', 'ALL_TC'
  FROM groups G, environments E
  WHERE G.id_environment = E.id_environment AND E.name IN ('John Doe Corporate', 'John Doe Retail');

INSERT INTO group_permissions(id_group, id_permission, target)
  SELECT G.id_group, P.id_permission, 'NONE'
  FROM groups G, environments E, permissions P
  WHERE G.id_environment = E.id_environment AND E.name IN ('John Doe Corporate', 'John Doe Retail')
    AND P.id_permission not in ('product.read', 'transfer.internal', 'transfer.thirdParties', 'transfer.local', 'transfer.foreign', 'pay.loan', 'pay.creditCard', 'pay.creditCard.thirdParties', 'pay.loan.thirdParties', 'accounts.requestCheckbook');

--    Bitters 30 June 2020: Signatures & Caps must be inserted into Limits MS database!!!!

---- Esquemas de firma
--INSERT INTO signatures (id_environment, signature_group, signature_type)
--  SELECT id_environment, 'A', 'ADM'
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO signatures (id_environment, signature_group, signature_type)
--  SELECT id_environment, 'A', 'AMOUNT'
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO signatures (id_environment, signature_group, signature_type)
--  SELECT id_environment, 'A', 'NO_AMOUNT'
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
---- Limites de usuario
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'all', 'f6cadaap64fe4hjead71456813d41af3', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'frontend', 'f6cadaap64fe4hjead71456813d41af3', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'phonegap', 'f6cadaap64fe4hjead71456813d41af3', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'facebook', 'f6cadaap64fe4hjead71456813d41af3', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'payments', 'f6cadaap64fe4hjead71456813d41af3', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'sms', 'f6cadaap64fe4hjead71456813d41af3', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_user (id_environment, channel, id_user, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'twitter', 'f6cadaap64fe4hjead71456813d41af3', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
---- Limites de ambiente
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'all', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'frontend', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'phonegap', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'facebook', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'payments', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'sms', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
--INSERT INTO caps_for_environment (id_environment, channel, frequency, maximum, used, used_last_reset)
--  SELECT id_environment, 'twitter', 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--  FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
--
---- Firmas
--INSERT INTO caps_for_signature (id_environment, channel, id_signature, frequency, maximum, used, used_last_reset)
--   SELECT S.id_environment, 'all', S.id_signature, 'daily', -1, 0.000, TO_DATE('2019-01-17 12:00:00','yyyy-mm-dd HH24:MI:SS')
--   FROM environments E, signatures S WHERE E.id_environment = S.id_environment AND E.name IN ('John Doe Corporate', 'John Doe Retail') AND S.signature_type = 'AMOUNT';

-- Widgets
INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41af3', id_environment, 'accounts', 1, 1
    FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41af3', id_environment, 'creditCards', 1, 1
    FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');

INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number)
    SELECT 'f6cadaap64fe4hjead71456813d41af3', id_environment, 'loans', 1, 3
    FROM environments WHERE name IN ('John Doe Corporate', 'John Doe Retail');
