/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4214
 *
 * @author tech-uy
 */
public class DB20180410_1609_4214 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration( "core.activities.dontExtendSession", "widgets.notifications|session.get", null, "others", new String[]{});
    }
}
