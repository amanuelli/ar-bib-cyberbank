/*
 * Copyright 2018 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6237
 *
 * @author mcheveste
 */
public class DB20181228_1053_6237 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("core.activities.dontExtendSession", "widgets.notifications|session.get|communications.latestCommunications|pay.multiline.salary.status", null, "others", new String[]{});
    }
}
