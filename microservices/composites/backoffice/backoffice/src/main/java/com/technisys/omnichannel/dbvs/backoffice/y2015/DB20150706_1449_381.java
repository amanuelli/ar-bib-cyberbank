/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author fpena
 */
public class DB20150706_1449_381 extends DBVSUpdate {

    @Override
    public void up() {
        
        delete("permissions", "id_permission='transfer.internal'");
        insert("permissions", new String[]{"id_permission"}, new String[]{"transfer.internal"});
        
        delete("permissions_credentials_groups", "id_permission='transfer.internal'");
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"transfer.internal", "accessToken-otp"});
        
        deleteActivity("transfers.internal.preview");
        insertActivity("transfers.internal.preview", "com.technisys.omnichannel.client.activities.transfers.internal.TransfersInternalPreviewActivity", "transferInternal", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        deleteActivity("transfers.internal.send");        
        insertActivity("transfers.internal.send", "com.technisys.omnichannel.client.activities.transfers.internal.TransfersInternalSendActivity", "transferInternal", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, null);
        
        String[] fieldNames = new String[]{"id_activity", "id_field", "id_permission"};
        String[] fieldValues = new String[]{"transfers.internal.send", "debitAccount", "transfer.internal"};
        insert("activity_products", fieldNames, fieldValues);
        
        fieldNames = new String[]{"id_activity", "id_field_amount", "id_field_product"};
        fieldValues = new String[]{"transfers.internal.send", "amount", "debitAccount"};
        insert("activity_caps", fieldNames, fieldValues);
        
    }
}
