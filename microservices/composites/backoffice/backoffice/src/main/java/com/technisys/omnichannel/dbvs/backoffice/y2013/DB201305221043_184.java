/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author Sebastian Barbosa
 */
public class DB201305221043_184 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"(([1-9][0-9]*)|([1-9]\\d{0,2}(\\.\\d{3})*)|0)(,\\d*)?"}, "validation_regexp='(([1-9][0-9]*)|([1-9]d{0,2}(.d{3})*)|0)(,d*[1-9]d*)?'");
    }
}