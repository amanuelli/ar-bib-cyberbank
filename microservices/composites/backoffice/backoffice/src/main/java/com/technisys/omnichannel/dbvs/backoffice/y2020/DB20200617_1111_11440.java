/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author dmazzuco
 */

public class DB20200617_1111_11440 extends DBVSUpdate {

    @Override
    public void up() {
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-502-demo-environments.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-503-demo-environments.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-504-demo-assistant.sql", ";", "UTF-8");
    }

}