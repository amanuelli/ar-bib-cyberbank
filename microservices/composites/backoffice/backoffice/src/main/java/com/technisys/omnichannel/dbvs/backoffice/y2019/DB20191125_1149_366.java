/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ahernandez
 */

public class DB20191125_1149_366 extends DBVSUpdate {

    @Override
    public void up() {
        String bpmnFlow = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<bpmn:definitions xmlns:bpmn=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:camunda=\"http://camunda.org/schema/1.0/bpmn\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"Definitions_1\" targetNamespace=\"http://bpmn.io/schema/bpmn\" exporter=\"Camunda Modeler\" exporterVersion=\"3.4.1\">\n" +
                "  <bpmn:process id=\"request-account\" name=\"Request Account\" isClosed=\"false\" isExecutable=\"true\" camunda:versionTag=\"1\">\n" +
                "    <bpmn:startEvent id=\"inicio\" name=\"Inicio\">\n" +
                "      <bpmn:outgoing>SequenceFlow_18t0i75</bpmn:outgoing>\n" +
                "    </bpmn:startEvent>\n" +
                "    <bpmn:userTask id=\"enEjecucion\" name=\"En ejecucion\" camunda:candidateGroups=\"10000,10001\">\n" +
                "      <bpmn:incoming>SequenceFlow_18t0i75</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_1mk2slu</bpmn:outgoing>\n" +
                "    </bpmn:userTask>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_18t0i75\" sourceRef=\"inicio\" targetRef=\"enEjecucion\" />\n" +
                "    <bpmn:exclusiveGateway id=\"ExclusiveGateway_0qmslys\">\n" +
                "      <bpmn:incoming>SequenceFlow_1mk2slu</bpmn:incoming>\n" +
                "      <bpmn:outgoing>rechazar</bpmn:outgoing>\n" +
                "      <bpmn:outgoing>aceptar</bpmn:outgoing>\n" +
                "    </bpmn:exclusiveGateway>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_1mk2slu\" sourceRef=\"enEjecucion\" targetRef=\"ExclusiveGateway_0qmslys\" />\n" +
                "    <bpmn:endEvent id=\"aceptado\" name=\"Aceptado\">\n" +
                "      <bpmn:incoming>SequenceFlow_1efkp7d</bpmn:incoming>\n" +
                "    </bpmn:endEvent>\n" +
                "    <bpmn:endEvent id=\"rechazado\" name=\"Rechazado\">\n" +
                "      <bpmn:incoming>SequenceFlow_05mvt8h</bpmn:incoming>\n" +
                "      <bpmn:errorEventDefinition errorRef=\"Error_07g0lf5\" />\n" +
                "    </bpmn:endEvent>\n" +
                "    <bpmn:sequenceFlow id=\"rechazar\" name=\"Rechazar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0knb9w2\">\n" +
                "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\">${action == ''rechazar''}</bpmn:conditionExpression>\n" +
                "    </bpmn:sequenceFlow>\n" +
                "    <bpmn:sequenceFlow id=\"aceptar\" name=\"Aceptar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0xnr1oc\">\n" +
                "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\">${action == ''aceptar''}</bpmn:conditionExpression>\n" +
                "    </bpmn:sequenceFlow>\n" +
                "    <bpmn:serviceTask id=\"Task_0xnr1oc\" name=\"Aceptando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.RequestAccount\">\n" +
                "      <bpmn:incoming>aceptar</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_12ktbww</bpmn:outgoing>\n" +
                "    </bpmn:serviceTask>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_12ktbww\" sourceRef=\"Task_0xnr1oc\" targetRef=\"Task_1l4zkyo\" />\n" +
                "    <bpmn:serviceTask id=\"Task_0knb9w2\" name=\"Rechazando\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.CancelTransaction\">\n" +
                "      <bpmn:incoming>rechazar</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_0gd6rpt</bpmn:outgoing>\n" +
                "    </bpmn:serviceTask>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_0gd6rpt\" sourceRef=\"Task_0knb9w2\" targetRef=\"SendTask_0hn6yj8\" />\n" +
                "    <bpmn:sendTask id=\"Task_1l4zkyo\" name=\"Notificando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.SendNotifications\">\n" +
                "      <bpmn:incoming>SequenceFlow_12ktbww</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_1efkp7d</bpmn:outgoing>\n" +
                "    </bpmn:sendTask>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_1efkp7d\" sourceRef=\"Task_1l4zkyo\" targetRef=\"aceptado\" />\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_05mvt8h\" sourceRef=\"SendTask_0hn6yj8\" targetRef=\"rechazado\" />\n" +
                "    <bpmn:sendTask id=\"SendTask_0hn6yj8\" name=\"Notificando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.SendNotifications\">\n" +
                "      <bpmn:incoming>SequenceFlow_0gd6rpt</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_05mvt8h</bpmn:outgoing>\n" +
                "    </bpmn:sendTask>\n" +
                "  </bpmn:process>\n" +
                "  <bpmn:error id=\"Error_07g0lf5\" name=\"Rechazado\" errorCode=\"rechazado\" />\n" +
                "</bpmn:definitions>";

        customSentence(DBVS.DIALECT_MYSQL, "UPDATE ACT_GE_BYTEARRAY set BYTES_ = '" + bpmnFlow + "' WHERE NAME_ = 'request-account.bpmn'");
        customSentence(DBVS.DIALECT_MSSQL, "UPDATE ACT_GE_BYTEARRAY set BYTES_ = CAST('" + bpmnFlow + "' AS IMAGE) WHERE NAME_ = 'request-account.bpmn'");
        customSentence(DBVS.DIALECT_ORACLE, "UPDATE ACT_GE_BYTEARRAY set BYTES_ = xmltype('" + bpmnFlow + "').getblobval(nls_charset_id('UTF8')) WHERE NAME_ = 'request-account.bpmn'");

    }

}