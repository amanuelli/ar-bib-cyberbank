/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.productrequest.requests;

import com.technisys.omnichannel.backoffice.business.PaginatedListRequest;
import com.technisys.omnichannel.core.utils.DateUtils;
import java.util.Date;

/**
 *
 * @author Ignacio Ocampo
 */
public class ListRequest extends PaginatedListRequest {

    private long id;
    private String documentNumber;
    private String status;
    private Date creationDateFrom;
    private Date creationDateTo;
    private String email;
    private String mobileNumber;
    private String type;
    private boolean export;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Date getCreationDateFrom() {
        return creationDateFrom;
    }

    public void setCreationDateFrom(Date creationDateFrom) {
        this.creationDateFrom = creationDateFrom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreationDateTo() {
        return creationDateTo;
    }

    public void setCreationDateTo(Date creationDateTo) {
        this.creationDateTo = creationDateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public boolean isExport() {
        return export;
    }

    public void setExport(boolean export) {
        this.export = export;
    }

    @Override
    public String toString() {
        return "ListRequest{" + "creationDateFrom="
                + ((creationDateFrom == null) ? "null" : DateUtils.formatFullDate(creationDateFrom)) + ", email="
                + email + ", creationDateTo="
                + ((creationDateTo == null) ? "null" : DateUtils.formatFullDate(creationDateTo)) + ", documentNumber="
                + documentNumber + ", status=" + status + ", mobile_number=" + mobileNumber + "}";
    }

}
