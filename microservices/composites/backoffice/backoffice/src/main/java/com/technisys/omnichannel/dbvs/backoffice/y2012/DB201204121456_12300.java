/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 * @author grosso
 */
public class DB201204121456_12300 extends DBVSUpdate {

    @Override
    public void up() {
        dropColumn("client_statement_lines_notes", "note");

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "ALTER TABLE \"CLIENT_STATEMENT_LINES_NOTES\" ADD \"NOTE\" CLOB NULL");
        } else {
            addColumn("client_statement_lines_notes", new ColumnDefinition("note", Types.VARCHAR, 8000, 0, true, null));
        }
    }
}