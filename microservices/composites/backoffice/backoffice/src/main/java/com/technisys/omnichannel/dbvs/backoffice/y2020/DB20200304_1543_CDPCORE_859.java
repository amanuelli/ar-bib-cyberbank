/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ahernandez
 */

public class DB20200304_1543_CDPCORE_859 extends DBVSUpdate {

    @Override
    public void up() {
        update("activity_products",
                new String[]{"id_field"}, new String[]{"debitAccount"},
                "id_activity='transfers.foreign.send' or id_activity='transfers.local.send'");
    }

}