/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201107081136_10536 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};
        String[] fieldValues = new String[]{"rubicon.userconfiguration.password.pattern", "[A-Za-z0-9ñÑáéíóúÁÉÍÓÚ_]*", "es", "rubicon"};
        insert("configuration", fieldNames, fieldValues);
    }
}
