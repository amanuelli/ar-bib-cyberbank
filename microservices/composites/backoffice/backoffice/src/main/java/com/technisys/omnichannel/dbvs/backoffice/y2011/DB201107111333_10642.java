/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201107111333_10642 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_role", "id_permission"};

        String[] fieldValues = new String[]{"client_operator", "rub.accounts.checkDiscount"};
        insert("role_permissions", fieldNames, fieldValues);
        fieldValues = new String[]{"client_administrator", "rub.accounts.checkDiscount"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_operator", "rub.accounts.sendCheck"};
        insert("role_permissions", fieldNames, fieldValues);
        fieldValues = new String[]{"client_administrator", "rub.accounts.sendCheck"};
        insert("role_permissions", fieldNames, fieldValues);
    }
}
