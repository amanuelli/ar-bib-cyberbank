/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author mcommand
 */
public class DB20170901_1541_3198 extends DBVSUpdate {

    @Override
    public void up() {        
        update("form_field_messages", new String[]{"value"}, new String[]{"Información de notificación"}, "id_message = 'fields.transferForeign.sectionNotificationInfo.label' and lang = 'es'");
    }
}
