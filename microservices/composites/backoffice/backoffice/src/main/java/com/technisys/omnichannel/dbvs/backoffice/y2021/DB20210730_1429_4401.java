/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2021;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;

/**
 * Related issue: TECCDPBO-4401
 *
 * @author Mauricio Uribe
 */
public class DB20210730_1429_4401 extends DBVSUpdate {

    @Override
    public void up() {

        // Export Activity
        delete("backoffice_activities", "id_activity = 'cybo.invitationcodes.export'");
        insertBackofficeActivity("cybo.invitationcodes.export",
                "com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities.ExportActivity",
                "backoffice.invitationCodes.list", null, "backoffice.invitationcodes",
                ActivityDescriptor.AuditLevel.Header);
    }
}
