package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author marcelobruno
 */
public class DB20180702_1630_4009 extends DBVSUpdate {

    @Override
    public void up() {
       
        //Hago lugar para el campo apellido
        update("form_field_validations", new String[]{"regular_expresion"}, new String[]{"^[a-zñ0-9._%+-]+@[a-zñ0-9.-]+.(?:[a-zñ]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$"}, "id_validation='email'");
        
        update("form_fields", new String[]{"ordinal"}, new String[]{"17"}, "id_form='creditCardRequest' AND id_field='document'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"18"}, "id_form='creditCardRequest' AND id_field='horizontalrule'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"19"}, "id_form='creditCardRequest' AND id_field='termsandconditions'");
        
        insert("form_fields", new String[]{"id_field", "id_form", "form_version", "type", "sub_type", "ordinal", "visible", "required", "ticket_only", "visible_in_mobile", "read_only"}, 
                new String[]{"surname", "creditCardRequest", "1", "text", "default", "16", "value(requestAdditional) == 'si'", "value(requestAdditional) == 'si'", "0", "1", "0"});
        update("form_field_messages", new String[]{"value"}, new String[]{"Debe ingresar el nombre del adicional"}, "id_message='fields.creditCardRequest.name.requiredError' AND id_form='creditCardRequest' AND lang='es'"); 
        update("form_field_messages", new String[]{"value"}, new String[]{"Nombre"}, "id_message='fields.creditCardRequest.name.label' AND id_form='creditCardRequest' AND lang='es'");
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"}, new String[]{"surname", "creditCardRequest", "1", "1", "100", "field-normal"});
        
        //placeholders and error messages
        String [] fieldNames = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value" };
        ArrayList<String[]> fieldValuesList = new ArrayList<>();
        fieldValuesList.add(new String[] {"fields.creditCardRequest.creditCard.placeholder", "es", "creditCard", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione una tarjeta" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.americanExpressTypeCard.placeholder", "es", "americanExpressTypeCard", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un tipo de tarjeta" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.masterCardTypeCard.placeholder", "es", "masterCardTypeCard", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un tipo de tarjeta" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.visaTypeCard.placeholder", "es", "visaTypeCard", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un tipo de tarjeta" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.americanExpressAwardsProgram.placeholder", "es", "americanExpressAwardsProgram", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un programa de premios" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.masterCardAwardsProgram.placeholder", "es", "masterCardAwardsProgram", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un programa de premios" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.visaAwardsProgram.placeholder", "es", "visaAwardsProgram", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un programa de premios" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.surname.label", "es", "surname", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Apellido" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.surname.requiredError", "es", "surname", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Debe ingresar el apellido del adicional" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.creditCard.requiredError", "es", "creditCard", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Debe seleccionar una marca de tarjeta" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.americanExpressTypeCard.requiredError", "es", "americanExpressTypeCard", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Debe seleccionar un tipo de tarjeta" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.masterCardTypeCard.requiredError", "es", "masterCardTypeCard", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Debe seleccionar un tipo de tarjeta" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.visaTypeCard.requiredError", "es", "visaTypeCard", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Debe seleccionar un tipo de tarjeta" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.americanExpressAwardsProgram.requiredError", "es", "americanExpressAwardsProgram", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Debe seleccionar un programa de premios" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.masterCardAwardsProgram.requiredError", "es", "masterCardAwardsProgram", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Debe seleccionar un programa de premios" });
        fieldValuesList.add(new String[] {"fields.creditCardRequest.visaAwardsProgram.requiredError", "es", "visaAwardsProgram", "creditCardRequest", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Debe seleccionar un programa de premios" });
        
        String [] fieldValues;
        for(int i = 0; i < fieldValuesList.size(); i++){
            fieldValues = fieldValuesList.get(i);
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                String insertSentence = String.format("INSERT INTO form_field_messages (%1$s, %2$s, %3$s, %4$s, %5$s, %6$s, %7$s) "
                                                    + "VALUES ('%8$s', '%9$s', '%10$s', '%11$s', '%12$s', TO_DATE('%13$s', 'YYYY-MM-DD HH24:MI:SS'), '%14$s')",
                                                    fieldNames[0], fieldNames[1], fieldNames[2], fieldNames[3], fieldNames[4], fieldNames[5], fieldNames[6],
                                                    fieldValues[0], fieldValues[1], fieldValues[2], fieldValues[3], fieldValues[4], fieldValues[5], fieldValues[6]);

                customSentence(DBVS.DIALECT_ORACLE, insertSentence);
            } else {
                insert("form_field_messages", fieldNames, fieldValues);
            }
        } 
        
    }
    
}
