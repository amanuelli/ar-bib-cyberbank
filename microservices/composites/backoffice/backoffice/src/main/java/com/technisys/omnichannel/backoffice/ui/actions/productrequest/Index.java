    /*
 *  Copyright 2019 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.productrequest;

import static com.opensymphony.xwork2.Action.SUCCESS;
import com.opensymphony.xwork2.ActionSupport;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;

/**
 *
 * @author iocampo
 */
@Action(results = {
    @Result(name = "success", location = "productrequest", type = "tiles")
})
public class Index extends ActionSupport {

    @Override
    public String execute() throws Exception {
        return SUCCESS;
    }
}
