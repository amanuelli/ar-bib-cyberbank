/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author cristobal
 */

public class DB20200814_1019_13858 extends DBVSUpdate {

    @Override
    public void up() {
        // IP ambiente DEV de CORE CPG
        update("configuration",new String[]{"value"},  new String[]{"https://172.64.3.151:8080/engine/json/restapi"}, "id_field = 'connector.cyberbank.webservices.url'");
    }

}