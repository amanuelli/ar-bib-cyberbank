/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3453
 *
 * @author msouza
 */
public class DB20171130_1518_3491 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("frontend.chat.zendesk.pathList.hide", "", ConfigurationGroup.TECNICAS, "zendesk", new String[]{});
        update("configuration", new String[]{"possible_values"}, new String[]{"true|false"}, "id_field like '%frontend.chat.zendesk.bubbleEnable%'");
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='frontend.chat.zendesk.pathList.hide'");
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='frontend.chat.zendesk.bubbleEnable'");
       
    }
}
