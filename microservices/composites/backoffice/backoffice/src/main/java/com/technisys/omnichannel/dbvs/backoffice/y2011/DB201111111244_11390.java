/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201111111244_11390 extends DBVSUpdate {

    @Override
    public void up() {
        delete("configuration", "id_field='rubicon.dateFormat'");
        delete("configuration", "id_field='rubicon.fullDateTimeFormat'");

        String[] fieldNames = new String[]{"id_field", "value", "id_group"};
        String[] fieldValues = new String[]{"rubicon.creditCardDateFormat", "dd/MMM/yyyy", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        update("configuration", new String[]{"value"}, new String[]{"MMM/yyyy"}, "id_field = 'rubicon.monthYearFormat'");
    }
}
