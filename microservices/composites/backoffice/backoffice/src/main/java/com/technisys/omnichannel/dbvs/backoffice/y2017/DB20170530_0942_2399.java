/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * Related issue: MANNAZCA-2399
 *
 * @author sbarbosa
 */
public class DB20170530_0942_2399 extends DBVSUpdate {

    @Override
    public void up() {
        // Pago de préstamos
        // ********************************************************************************************
        // ********************************************************************************************
        // ********************************************************************************************
        Map<String, String> messages = new HashMap<>();

        String idForm = "payLoan";
        addNotificationsFieldsToForm(idForm, messages);

        // Campos específicos del ticket
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, form_version, max(ordinal) + 1, 'debitAmount', 'amount', 'TRUE', 'FALSE', 'default', 1, 1 "
                + " FROM form_fields "
                + " WHERE id_form = '" + idForm + "' "
                + " GROUP BY id_form, form_version");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_amount (id_form, form_version, id_field, display_type, control_limits) "
                + " SELECT id_form, version, 'debitAmount', 'field-normal', 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "'");

        messages.put("debitAmount.help", null);
        messages.put("debitAmount.hint", null);
        messages.put("debitAmount.label", "Monto total debitado");

        // Cotización aplicada (rate)
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, form_version, max(ordinal) + 1, 'rate', 'text', 'TRUE', 'FALSE', 'default', 1, 1 "
                + " FROM form_fields "
                + " WHERE id_form = '" + idForm + "' "
                + " GROUP BY id_form, form_version");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_text (id_form, form_version, id_field, min_length, max_length, display_type, id_validation) "
                + " SELECT id_form, version, 'rate', 0, 50, 'field-normal', null "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "'");

        messages.put("rate.help", null);
        messages.put("rate.hint", null);
        messages.put("rate.label", "Cotización aplicada");

        // Comisiones (commission)
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, form_version, max(ordinal) + 1, 'commission', 'amount', 'TRUE', 'FALSE', 'default', 1, 1 "
                + " FROM form_fields "
                + " WHERE id_form = '" + idForm + "' "
                + " GROUP BY id_form, form_version");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_amount (id_form, form_version, id_field, display_type, control_limits) "
                + " SELECT id_form, version, 'commission', 'field-normal', 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "'");

        messages.put("commission.help", null);
        messages.put("commission.hint", null);
        messages.put("commission.label", "Comisiones");

        // Inserto los mensajes del formulario
        insertFormMessages(idForm, messages);

        // Transferencia exterior
        // ********************************************************************************************
        // ********************************************************************************************
        // ********************************************************************************************
        messages.clear();
        idForm = "transferForeign";

        // Información de notifiación (sectionNotificationInfo)
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile) "
                + " SELECT id_form, form_version, max(ordinal) + 1, 'sectionNotificationInfo', 'sectiontitle', 'TRUE', 'FALSE', 'default', 1 "
                + " FROM form_fields "
                + " WHERE id_form = '" + idForm + "' "
                + " GROUP BY id_form, form_version");

        messages.put("sectionNotificationInfo.label", "Información de notifiación");

        addNotificationsFieldsToForm(idForm, messages);

        // Inserto los mensajes del formulario
        insertFormMessages(idForm, messages);

        update("form_field_messages", new String[]{"value"}, new String[]{" "}, "id_form = 'transferForeign' and id_message in ('fields.transferForeign.beneficiaryInfo2.label', 'fields.transferForeign.beneficiaryInfo3.label', 'fields.transferForeign.beneficiaryInfo4.label')");
        update("form_fields", new String[]{"visible"}, new String[]{"TRUE"}, "id_form = 'transferForeign' and id_field in ('beneficiaryInfo2', 'beneficiaryInfo3', 'beneficiaryInfo4')");

        // Transferencia interna
        // ********************************************************************************************
        // ********************************************************************************************
        // ********************************************************************************************
        messages.clear();
        idForm = "transferInternal";

        update("form_fields", new String[]{"required"}, new String[]{"FALSE"}, "id_form = 'transferInternal' and id_field in ('creditReference', 'debitReference')");

        messages.put("creditReference.help", "Texto que le ayude al destinatario a identificar su transferencia.");
        messages.put("debitReference.help", "Texto que le ayude a identificar su transferencia.");

        // Inserto los mensajes del formulario
        insertFormMessages(idForm, messages);

        
    }

    private void addNotificationsFieldsToForm(String idForm, Map<String, String> messages) {
        // Emails de notificación (notificationEmails)
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile) "
                + " SELECT id_form, form_version, max(ordinal) + 1, 'notificationEmails', 'emaillist', 'TRUE', 'FALSE', 'default', 1 "
                + " FROM form_fields "
                + " WHERE id_form = '" + idForm + "' "
                + " GROUP BY id_form, form_version");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_emaillist (id_form, form_version, id_field, display_type) "
                + " SELECT id_form, version, 'notificationEmails', 'field-normal' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "'");

        messages.put("notificationEmails.help", "E-mails a notificar el envío de la transferencia.");
        messages.put("notificationEmails.hint", null);
        messages.put("notificationEmails.label", "Emails de notificación");

        // Cuerpo de notificación (notificationBody)
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile) "
                + " SELECT id_form, form_version, max(ordinal) + 1, 'notificationBody', 'textarea', 'TRUE', 'FALSE', 'default', 1 "
                + " FROM form_fields "
                + " WHERE id_form = '" + idForm + "' "
                + " GROUP BY id_form, form_version");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_textarea (id_form, form_version, id_field, min_length, max_length, display_type) "
                + " SELECT id_form, version, 'notificationBody', 0, 500, 'field-normal' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "'");

        messages.put("notificationBody.help", null);
        messages.put("notificationBody.hint", null);
        messages.put("notificationBody.label", "Cuerpo de notificación");
    }

    private void insertFormMessages(String idForm, Map<String, String> messages) {
        messages.keySet().forEach((key) -> {
            customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO form_field_messages (id_form, form_version, modification_date, id_field, id_message, lang, value) "
                            + " SELECT id_form, form_version, max(modification_date), '" + key.substring(0, key.indexOf(".")) + "', 'fields." + idForm + "." + key + "', 'es', " + StringUtils.wrap(messages.get(key), "'") + " "
                                    + " FROM form_field_messages "
                                    + " WHERE id_form = '" + idForm + "' and lang = 'es' "
                                            + " GROUP BY id_form, form_version");
        });
    }

}
