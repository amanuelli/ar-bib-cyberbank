/* 
 * Copyright 2019 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-7809
 *
 * @author Jhossept Garay
 */
public class DB20190722_1052_7809 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("massive.payments.allowed.banks.sameBank", "Techbank", ConfigurationGroup.TECNICAS, "forms", new String[]{"notEmpty"});
    }

}