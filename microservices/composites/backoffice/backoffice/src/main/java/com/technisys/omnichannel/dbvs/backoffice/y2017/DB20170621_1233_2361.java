/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author danireb
 */
public class DB20170621_1233_2361 extends DBVSUpdate {

    @Override
    public void up() {
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"frequentDestinations.manage"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"frequentDestinations.manage", "accessToken-pin"});
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"frequentDestinations"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"frequentDestinations", "accessToken"});

        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"frequentDestinations.manage", "transfers", null, null, "0", "21", "transfers", null, null, "0", "21", "transfers", null, null, "1", "21", null, null, null});
        
        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"frequentDestinations", "transfers", null, null, "0", "22", "transfers", null, null, "0", "22", "transfers", null, null, "1", "22", null, null, null});
        
        insertOrUpdateConfiguration("frequentDestinations.statementsPerPage", "10", com.technisys.omnichannel.DBVSUpdate.ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
        
        insertActivity("frequentdestinations.list", "com.technisys.omnichannel.client.activities.frequentdestinations.ListFrequentDestinationsActivity","frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "frequentDestinations");

        insertActivity("frequentdestinations.create", "com.technisys.omnichannel.client.activities.frequentdestinations.CreateFrequentDestinationActivity", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "frequentDestinations.manage");
        insertActivity("frequentdestinations.create.validate", "com.technisys.omnichannel.client.activities.frequentdestinations.CreateFrequentDestinationActivity", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "frequentDestinations");
        
        insertActivity("frequentdestinations.create.preview", "com.technisys.omnichannel.client.activities.frequentdestinations.CreateFrequentDestinationPreviewActivity", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "frequentDestinations");
        
        insertActivity("frequentdestinations.modify.pre", "com.technisys.omnichannel.client.activities.frequentdestinations.ModifyFrequentDestinationPreActivity", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "frequentDestinations");
        insertActivity("frequentdestinations.modify", "com.technisys.omnichannel.client.activities.frequentdestinations.ModifyFrequentDestinationActivity", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "frequentDestinations.manage");
        insertActivity("frequentdestinations.modify.preview", "com.technisys.omnichannel.client.activities.frequentdestinations.ModifyFrequentDestinationPreviewActivity", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "frequentDestinations");        
        
        insertActivity("frequentdestinations.delete", "com.technisys.omnichannel.client.activities.frequentdestinations.DeleteFrequentDestinationActivity", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "frequentDestinations.manage");
        insertActivity("frequentdestinations.delete.preview", "com.technisys.omnichannel.client.activities.frequentdestinations.DeleteFrequentDestinationPreviewActivity", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "frequentDestinations");
        
        //FORMULARIO//
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        String[] formFieldsValues;
        String idForm = "frequentDestination";
        String version = "1";
       
        /**** Insert Form - Destinos frecuentes ****/    
        String[] formsFields = new String[]{"id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option","id_activity", "templates_enabled", "drafts_enabled", "schedulable"};
        String[] formsFieldsValues = new String[]{idForm, version, "1", null, "activity", null, null, "frequentdestinations.create.send", "0", "0", "0"};
        insert("forms", formsFields, formsFieldsValues);
    
        String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_messages (id_message, id_form,version,lang,value, modification_date )  VALUES ('forms." + idForm + ".formName', '"+idForm+"','"+version+"','es','Destinos frecuentes',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'))");
            
        }else{
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Destinos frecuentes", date});
        }
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

        String idField;
        
        
        /**** Insert field hidden - modal ****/
        idField = "modal";
        formFieldsValues = new String[]{idField, idForm, version, "hidden", "1", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Es modal')");
        }else{   
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Es modal"});
        }    
        
        /**** Insert field selector - productType ****/
        idField = "productType";
        formFieldsValues = new String[]{idField, idForm, version, "selector", "2", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as"}, new String[]{idField, idForm, version, "field-normal", "1", "combo"});
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Tipo de producto')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe seleccionar un tipo de producto')");
        }else{ 
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Tipo de producto"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe seleccionar un tipo de producto"});
        }    
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "account"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "externalAccount"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "foreignAccount"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "creditCard"});        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "externalCreditCard"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "loan"});
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".option.account', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Cuenta')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".option.externalAccount', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Cuenta otro banco')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".option.foreignAccount', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Cuenta banco exterior')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".option.creditCard', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Tarjeta crédito otro banco')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".option.loan', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Préstamo')");
        }else{ 
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".option.account", "es", idField, idForm, version, date, "Cuenta"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".option.externalAccount", "es", idField, idForm, version, date, "Cuenta otro banco"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".option.foreignAccount", "es", idField, idForm, version, date, "Cuenta banco exterior"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".option.creditCard", "es", idField, idForm, version, date, "Tarjeta de crédito"});        
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".option.externalCreditCard", "es", idField, idForm, version, date, "Tarjeta crédito otro banco"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".option.loan", "es", idField, idForm, version, date, "Préstamo"});
        
        }
        
        /**** Insert field bankSelector - localBank ****/
        idField = "localBank";
        formFieldsValues = new String[]{idField, idForm, version, "bankselector", "3", "value(productType) == 'externalAccount|externalCreditCard'", "value(productType) == 'externalAccount|externalCreditCard'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_bankselector", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{idField, idForm, version, "field-small"});
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Banco plaza destino')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Banco plaza destino"});
        }    
        
        
        /**** Insert field bankSelector - exteriorBank ****/
        idField = "exteriorBank";
        formFieldsValues = new String[]{idField, idForm, version, "bankselector", "4", "value(productType) == 'foreignAccount'", "value(productType) == 'foreignAccount'", "foreigners", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_bankselector", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{idField, idForm, version, "field-normal"});
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Banco exterior destino')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Banco exterior destino"});
        }
     
        /**** Insert field text - accountNumber ****/
        idField = "accountNumber";
        formFieldsValues = new String[]{idField, idForm, version, "text", "5", "value(productType) == 'account'", "value(productType) == 'account'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation"}, new String[]{idField, idForm, version, "field-smaller", "9", "9", "onlyNumbers"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Para cuentas de banco exterior: \"Ingrese el número de cuenta o IBAN\"')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Número de cuenta')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar un número de cuenta')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, "Para cuentas de banco exterior: \"Ingrese el número de cuenta o IBAN\""});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Número de cuenta"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar un número de cuenta"});
        }    
        
        /**** Insert field text - externalAccountNumber ****/
        idField = "externalAccountNumber";
        formFieldsValues = new String[]{idField, idForm, version, "text", "6", "value(productType) == 'externalAccount'", "value(productType) == 'externalAccount'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation"}, new String[]{idField, idForm, version, "field-small", "1", "20", "onlyNumbers"});

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Para cuentas de banco exterior: \"Ingrese el número de cuenta o IBAN\"')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Número de cuenta')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar un número de cuenta')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, "Para cuentas de banco exterior: \"Ingrese el número de cuenta o IBAN\""});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Número de cuenta"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar un número de cuenta"});
        }
        /**** Insert field text - foreignAccountNumber ****/
        idField = "foreignAccountNumber";
        formFieldsValues = new String[]{idField, idForm, version, "text", "7", "value(productType) == 'foreignAccount'", "value(productType) == 'foreignAccount'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation"}, new String[]{idField, idForm, version, "field-normal", "1", "34", "onlyNumbers"});

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Para cuentas de banco exterior: \"Ingrese el número de cuenta o IBAN\"')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Número de cuenta')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar un número de cuenta')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, "Para cuentas de banco exterior: \"Ingrese el número de cuenta o IBAN\""});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Número de cuenta"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar un número de cuenta"});
        }    
        
        
        /**** Insert field text - recipientName ****/
        idField = "recipientName";
        formFieldsValues = new String[]{idField, idForm, version, "text", "8", "value(productType) == 'externalAccount|foreignAccount|externalCreditCard'", "value(productType) == 'externalAccount|foreignAccount|externalCreditCard'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation"}, new String[]{idField, idForm, version, "field-normal", "1", "50", "withoutSpecialChars"});

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Nombre del beneficiario')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar el nombre de la persona o empresa beneficiaria')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Nombre del beneficiario"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar el nombre de la persona o empresa beneficiaria"});
        }    
        
        
        /**** Insert field selector - recipientDocumentType ****/
        idField = "recipientDocumentType";
        formFieldsValues = new String[]{idField, idForm, version, "selector", "9", "value(productType) == 'externalAccount'", "value(productType) == 'externalAccount'", "documentTypes", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as"}, new String[]{idField, idForm, version, "field-small", "1", "combo"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Tipo de documento del beneficiario')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar el tipo de documento del beneficiario')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Tipo de documento del beneficiario"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar el tipo de documento del beneficiario"});
        }
        
        /**** Insert field text - recipientDocumentNumber ****/
        idField = "recipientDocumentNumber";
        formFieldsValues = new String[]{idField, idForm, version, "text", "10", "value(productType) == 'externalAccount'", "value(productType) == 'externalAccount'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation"}, new String[]{idField, idForm, version, "field-small", "1", "10", null});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Número de documento del beneficiario')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar el documento del beneficiario')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Número de documento del beneficiario"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar el documento del beneficiario"});
        }
        
        /**** Insert field text - recipientAddress ****/
        idField = "recipientAddress";
        formFieldsValues = new String[]{idField, idForm, version, "text", "11", "value(productType) == 'foreignAccount'", "value(productType) == 'foreignAccount'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation"}, new String[]{idField, idForm, version, "field-big", "1", "100", null});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Dirección beneficiario')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Dirección beneficiario"});        
        }
        
        /**** Insert field text - creditCardNumber ****/
        idField = "creditCardNumber";
        formFieldsValues = new String[]{idField, idForm, version, "text", "12", "value(productType) == 'creditCard|externalCreditCard'", "value(productType) == 'creditCard|externalCreditCard'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation"}, new String[]{idField, idForm, version, "field-small", "16", "16", "onlyNumbers"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Número de tarjeta de crédito')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar un número de tarjeta')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Número de tarjeta de crédito"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar un número de tarjeta"});
        }
        
        /**** Insert field text - loanNumber ****/
        idField = "loanNumber";
        formFieldsValues = new String[]{idField, idForm, version, "text", "13", "value(productType) == 'loan'", "value(productType) == 'loan'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation"}, new String[]{idField, idForm, version, "field-small", "1", "12", "onlyNumbers"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Número de préstamo')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar un número de préstamo')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Número de préstamo"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar un número de préstamo"});
        }
        
        /**** Insert field selector - saveData ****/
        idField = "saveData";
        formFieldsValues = new String[]{idField, idForm, version, "selector", "14", "value(modal) == 'yes'", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"}, new String[]{idField, idForm, version, "field-smaller", "yes", "1", "check"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Guardar destino frecuente')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe seleccionar una opción')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Guardar destino frecuente"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe seleccionar una opción"});
        }    
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "yes"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".option.yes', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'SI')");
        }else{   
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".option.yes", "es", idField, idForm, version, date, "SI"});
        }
                
        /**** Insert field text - name ****/
        idField = "name";
        formFieldsValues = new String[]{idField, idForm, version, "text", "15", "value(saveData) != 'no'", "value(saveData) != 'no'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation"}, new String[]{idField, idForm, version, "field-normal", "1", "50", null});
       
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Texto que le permita identificar al destino beneficiario')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Nombre')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar un nombre')");
            
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, "Texto que le permita identificar al destino beneficiario"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Nombre"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar un nombre"});
        }
        
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB}, "UPDATE form_field_ps SET apply_frequent_destinations = 1 WHERE id_form = 'transferInternal' AND id_field = 'creditAccount'");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB}, "UPDATE form_field_ps SET apply_frequent_destinations = 1 WHERE id_form = 'payCreditCard' AND id_field = 'creditCard'");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB}, "UPDATE form_field_ps SET apply_frequent_destinations = 1 WHERE id_form = 'payLoan' AND id_field = 'loan'");
         
        
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB}, "UPDATE form_field_ps SET show_other_option = 0, show_other_permission = null, show_other_id_ps = null, show_other_option_in_mobile = 0 WHERE id_form = 'transferInternal' AND id_field = 'creditAccount'");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB}, "UPDATE form_field_ps SET show_other_option = 0, show_other_permission = null, show_other_id_ps = null, show_other_option_in_mobile = 0 WHERE id_form = 'payCreditCard' AND id_field = 'creditCard'");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB}, "UPDATE form_field_ps SET show_other_option = 0, show_other_permission = null, show_other_id_ps = null, show_other_option_in_mobile = 0 WHERE id_form = 'payLoan' AND id_field = 'loan'");
        
        //formulario tranferLocal
        delete("form_field_text", "id_form = 'transferLocal' and id_field = 'creditBank'");
        delete("form_field_text", "id_form = 'transferLocal' and id_field = 'creditAccountNumber'");
        delete("form_field_selector", "id_form = 'transferLocal' and id_field = 'beneficiaryDocument'");
        delete("form_field_text", "id_form = 'transferLocal' and id_field = 'beneficiaryName'");
        
        delete("form_fields", "id_form = 'transferLocal' and id_field = 'creditBank'");
        delete("form_fields", "id_form = 'transferLocal' and id_field = 'creditAccountNumber'");
        delete("form_fields", "id_form = 'transferLocal' and id_field = 'beneficiaryName'");
        delete("form_fields", "id_form = 'transferLocal' and id_field = 'beneficiaryDocument'");
        
        
        
        
        Map<String, String> messages = new HashMap<>();
        
        
        
        
        
        /**** Insert field product selector - Cuentas locales ****/
        idField = "creditAccount";
        idForm = "transferLocal";
       
        
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, version, 7, '" + idField + "', 'productselector', 'TRUE', 'TRUE', 'localAccountSelector', 1, 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");        
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps (id_field, id_form, form_version, display_type, apply_frequent_destinations) "
                + " SELECT '" + idField + "', id_form, version, 'field-normal', 1 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps_product_types (id_field, id_form, form_version, id_product_type) "
                + " SELECT '" + idField + "', id_form, version, 'CA' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps_permissions (id_field, id_form, form_version, id_permission) "
                + " SELECT '" + idField + "', id_form, version, 'transfer.thirdParties' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");                

        messages.put(idField + ".label", "Cuenta crédito");
        messages.put(idField + ".requiredError", "Debe ingresar una cuenta crédito");
        
        
     // Inserto los mensajes del formulario
        insertFormMessages(idForm, messages);
        
        messages.clear();
        
        
        //formulario tranferForeign
        delete("form_field_bankselector", "id_form = 'transferForeign' and id_field = 'creditBank'");
        delete("form_field_text", "id_form = 'transferForeign' and id_field = 'creditAccountNumbe'");
        delete("form_field_text", "id_form = 'transferForeign' and id_field = 'beneficiaryName'");
        delete("form_field_text", "id_form = 'transferForeign' and id_field = 'beneficiaryAddress'");
        delete("form_fields", "id_form = 'transferForeign' and id_field = 'creditBank'");
        delete("form_fields", "id_form = 'transferForeign' and id_field = 'creditAccountNumbe'");
        delete("form_fields", "id_form = 'transferForeign' and id_field = 'beneficiaryName'");
        delete("form_fields", "id_form = 'transferForeign' and id_field = 'beneficiaryAddress'");
        
        /**** Insert field product selector - Cuentas exterior ****/
        idField = "creditAccount";
        idForm = "transferForeign";

        
        
        
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, version, 7, '" + idField + "', 'productselector', 'TRUE', 'TRUE', 'foreignAccountSelector', 1, 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");        
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps (id_field, id_form, form_version, display_type, apply_frequent_destinations) "
                + " SELECT '" + idField + "', id_form, version, 'field-normal', 1 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps_product_types (id_field, id_form, form_version, id_product_type) "
                + " SELECT '" + idField + "', id_form, version, 'CA' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps_permissions (id_field, id_form, form_version, id_permission) "
                + " SELECT '" + idField + "', id_form, version, 'transfer.foreign' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");                

        messages.put(idField + ".label", "Cuenta crédito");
        messages.put(idField + ".requiredError", "Debe ingresar una cuenta crédito");
        
        
     // Inserto los mensajes del formulario
        insertFormMessages(idForm, messages);
        
        messages.clear();
        
        
        //formulario payLoan
        delete("form_field_text", "id_form = 'payLoan' and id_field = 'otherLoanNumber'");
        delete("form_field_ps", "id_form = 'payLoan' and id_field = 'loan'");        
        delete("form_fields", "id_form = 'payLoan' and id_field = 'otherLoanNumber'");
        delete("form_fields", "id_form = 'payLoan' and id_field = 'loan'");
        delete("form_fields", "id_form = 'payLoan' and id_field = 'otherLoanAmount'");
        delete("form_field_amount", "id_form = 'payLoan' and id_field = 'otherLoanAmount'");
        delete("form_fields", "id_form = 'payLoan' and id_field = 'loanPayment'");
 
        
        /**** Insert field product selector - Prestamo ****/
        
        idField = "loan";
        idForm = "payLoan";
       
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, version, 2, '" + idField + "', 'productselector', 'TRUE', 'TRUE', 'loanSelector', 1, 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");        
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps (id_field, id_form, form_version, display_type, apply_frequent_destinations) "
                + " SELECT '" + idField + "', id_form, version, 'field-normal', 1 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps_product_types (id_field, id_form, form_version, id_product_type) "
                + " SELECT '" + idField + "', id_form, version, 'PA' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps_product_types (id_field, id_form, form_version, id_product_type) "
                + " SELECT '" + idField + "', id_form, version, 'PI' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps_permissions (id_field, id_form, form_version, id_permission) "
                + " SELECT '" + idField + "', id_form, version, 'product.read' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");    
        
        
        messages.put(idField + ".label", "Préstamo");
        messages.put(idField + ".requiredError", "Debe ingresar una número de préstamo");
        
     // Inserto los mensajes del formulario
        insertFormMessages(idForm, messages);
        
        messages.clear();

        
        idField = "loanPayment";
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, version, 4, '" + idField + "', 'loanpaymentamount', 'value(loan.frequentDestination) == ''0''', 'value(loan.frequentDestination) == ''0''', 'default', 1, 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_loanpaymentamount (id_field, id_form, form_version, display_type, control_limits, depends_on_product) "
                + " SELECT '" + idField + "', id_form, version, 'field-small', 1 , 'loan'"
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");

        
        
        
        messages.put(idField + ".label","Tipo de pago");
        messages.put(idField + ".labelPartialPay","Importe a pagar");
        messages.put(idField + ".labelTotalPay","Monto total a pagar");
        messages.put(idField + ".partialPay","Pago parcial");
        messages.put(idField + ".totalPay","Pago total");
        
        
     // Inserto los mensajes del formulario
        insertFormMessages(idForm, messages);
        
        messages.clear();
        
        
        
        idField = "otherLoanAmount";
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, version, 5, '" + idField + "', 'amount', 'value(loan.frequentDestination) != ''0''', 'value(loan.frequentDestination) != ''0''', 'default', 1, 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");    
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_amount (id_field, id_form, form_version, display_type, control_limits, use_for_total_amount) "
                + " SELECT '" + idField + "', id_form, version, 'field-small', 1, 1 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        

        
        messages.put(idField + ".label", "Importe a pagar");
        messages.put(idField + ".requiredError", "Debe ingresar un importe");

        
        // Inserto los mensajes del formulario
       insertFormMessages(idForm, messages);
       
       messages.clear();
        
        
        
        
        
        
        //formulario payCreditCard
        delete("form_field_ps", "id_form = 'payCreditCard' and id_field = 'creditCard'");        
        delete("form_field_bankselector", "id_form = 'payCreditCard' and id_field = 'creditCardBank'");        
        delete("form_field_text", "id_form = 'payCreditCard' and id_field = 'otherCreditCardNumber'");
        delete("form_fields", "id_form = 'payCreditCard' and id_field = 'otherCreditCard'");
        delete("form_fields", "id_form = 'payCreditCard' and id_field = 'creditCard'");
        delete("form_fields", "id_form = 'payCreditCard' and id_field = 'creditCardBank'");
        delete("form_fields", "id_form = 'payCreditCard' and id_field = 'otherCreditCardNumber'");
        
        delete("form_fields", "id_form = 'payCreditCard' and id_field = 'amount'");
        delete("form_field_cc_amount", "id_form = 'payCreditCard' and id_field = 'amount'");
        delete("form_field_cc_amount_curr", "id_form = 'payCreditCard' and id_field = 'amount'");
        
        delete("form_fields", "id_form = 'payCreditCard' and id_field = 'otherAmount'");
        delete("form_field_amount", "id_form = 'payCreditCard' and id_field = 'otherAmount'");
        
     
        
        /**** Insert field product selector - Tarjeta ****/
        idField = "creditCard";
        idForm = "payCreditCard";
      
        
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, version, 2, '" + idField + "', 'productselector', 'TRUE', 'TRUE', 'creditCardSelector', 1, 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");        
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps (id_field, id_form, form_version, display_type, apply_frequent_destinations) "
                + " SELECT '" + idField + "', id_form, version, 'field-normal', 1 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps_product_types (id_field, id_form, form_version, id_product_type) "
                + " SELECT '" + idField + "', id_form, version, 'TC' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_ps_permissions (id_field, id_form, form_version, id_permission) "
                + " SELECT '" + idField + "', id_form, version, 'product.read' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");                


        messages.put(idField + ".label", "Tarjeta");
        messages.put(idField + ".requiredError", "Debe ingresar un número de tarjeta");

        
        // Inserto los mensajes del formulario
       insertFormMessages(idForm, messages);
       
       messages.clear();
        
        idField = "amount";
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, version, 7, '" + idField + "', 'paycreditcardamount', 'value(creditCard.frequentDestination) == ''0''', 'value(creditCard.frequentDestination) == ''0''', 'default', 1, 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");    
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_cc_amount (id_field, id_form, form_version, display_type, id_cc_selector, control_limits) "
                + " SELECT '" + idField + "', id_form, version, 'field-normal', 'creditCard', 1 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_cc_amount_curr (id_field, id_form, form_version, id_currency) "
                + " SELECT '" + idField + "', id_form, version, '000' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
    
        
        messages.put(idField + ".label", "Monto a pagar");
        messages.put(idField + ".requiredError", "Debe ingresar un monto");

        
        // Inserto los mensajes del formulario
       insertFormMessages(idForm, messages);
       
       messages.clear();
       
        
        idField = "otherAmount";
        
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_fields (id_form, form_version, ordinal, id_field, type, visible, required, sub_type, visible_in_mobile, ticket_only) "
                + " SELECT id_form, version, 6, '" + idField + "', 'amount', 'value(creditCard.frequentDestination) != ''0''', 'value(creditCard.frequentDestination) != ''0''', 'default', 1, 0 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");    
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_amount (id_field, id_form, form_version, display_type, control_limits, use_for_total_amount) "
                + " SELECT '" + idField + "', id_form, version, 'field-normal', 1, 1 "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_amount_currencies (id_field, id_form, form_version, id_currency) "
                + " SELECT '" + idField + "', id_form, version, '000' "
                + " FROM forms "
                + " WHERE id_form = '" + idForm + "' ");
        
        messages.put(idField + ".label", "Monto a pagar");
        messages.put(idField + ".requiredError", "Debe ingresar un monto");

        
        // Inserto los mensajes del formulario
       insertFormMessages(idForm, messages);
       
       messages.clear();
       
    }
    
    
    private void insertFormMessages(String idForm, Map<String, String> messages) {
        messages.keySet().forEach((key) -> {
            customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO form_field_messages (id_form, form_version, modification_date, id_field, id_message, lang, value) "
                            + " SELECT id_form, form_version, max(modification_date), '" + key.substring(0, key.indexOf(".")) + "', 'fields." + idForm + "." + key + "', 'es', " + StringUtils.wrap(messages.get(key), "'") + " "
                                    + " FROM form_field_messages "
                                    + " WHERE id_form = '" + idForm + "' and lang = 'es' "
                                            + " GROUP BY id_form, form_version");
        });
    }
    
}
