    /*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ReadEnvironmentRequest;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.ReadEnvironmentResponse;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.IBResponse;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 */
@Results({
    @Result(name = "success", type = "json", params = {"root", "resultMap"})
})
public class ReadEnvironment extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ReadEnvironmentRequest request = new ReadEnvironmentRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.invitationCodes.readEnvironment");

        request.setAccount(account);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        if (ReturnCodes.OK.equals(response.getReturnCode())) {
            ReadEnvironmentResponse auxResponse = (ReadEnvironmentResponse) response;
            resultMap = UIUtils.generateResultMap(response);

            resultMap.put("environment", auxResponse.getEnvironment());
            resultMap.put("clientEnvironment", auxResponse.getClientEnvironment());

            return SUCCESS;
        } else {
            throw new JSONException(response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private String account;

    public void setAccount(String account) {
        this.account = account;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private transient Map<String, Object> resultMap;
    private ClientUser client;

    public Map<String, Object> getResultMap() {
        return resultMap;
    }

    public ClientUser getClient() {
        return client;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
