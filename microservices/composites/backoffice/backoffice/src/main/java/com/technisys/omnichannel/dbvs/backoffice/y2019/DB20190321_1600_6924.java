package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

public class DB20190321_1600_6924 extends DBVSUpdate {
    @Override
    public void up(){
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-502-demo-environments.sql", ";", "UTF-8");
    }
}
