/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5406
 *
 * @author Marcelo Bruno
 */
public class DB20181026_1807_5406 extends DBVSUpdate {

    @Override
    public void up() {

        deleteActivity("administration.medium.read.details");
        
        insertActivity("administration.medium.user.details.read", "com.technisys.omnichannel.client.activities.administration.medium.ReadUserDetailsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.advanced.user.details.read", "com.technisys.omnichannel.client.activities.administration.advanced.ReadUserDetailsActivity" , "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.user.detail.groups.modify.preview", "com.technisys.omnichannel.client.activities.administration.advanced.ModifyGroupsOfUserPreviewActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.user.detail.groups.modify.send", "com.technisys.omnichannel.client.activities.administration.advanced.ModifyGroupsOfUserActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Admin, "administration.manage");

    }
    
}