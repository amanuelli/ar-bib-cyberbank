/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author diegor
 */
public class DB20150827_1628_807 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_branch_code"};

        delete("branch_codes", "1 = 1");
        insert("branch_codes", fieldNames, new String[]{"01"});
        insert("branch_codes", fieldNames, new String[]{"02"});
        insert("branch_codes", fieldNames, new String[]{"03"});
        insert("branch_codes", fieldNames, new String[]{"04"});
        insert("branch_codes", fieldNames, new String[]{"05"});
        insert("branch_codes", fieldNames, new String[]{"06"});
    }
}
