/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3330
 *
 * @author dimoda
 */
public class DB20171013_1723_3330 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("accounts.details.filter.lastN", "10", ConfigurationGroup.NEGOCIO, null, new String[]{});
        update("configuration", new String[]{"id_sub_group","channels"}, new String[]{"frontend","frontend"}, "id_field='accounts.details.filter.lastN'");
    }

}