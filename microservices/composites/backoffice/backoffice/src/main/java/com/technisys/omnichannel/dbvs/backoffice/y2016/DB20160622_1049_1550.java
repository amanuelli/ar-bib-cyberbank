/* 
 * Copyright 2016 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2016;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20160622_1049_1550 extends DBVSUpdate {

    @Override
    public void up() {
        
        insert("campaing_indicator_types", new String[]{"id_type", "ordinal"}, new String[]{"documentInFile", "2"});
        
        updateConfiguration("backoffice.environments.productsTypes", "CA|CC|PF|PA|PI|TC");
        
        insertOrUpdateConfiguration("campaigns.indicators.documentInFile.cache.maximumSize", "50", ConfigurationGroup.TECNICAS, null, new String[]{});
        insertOrUpdateConfiguration("campaigns.indicators.documentInFile.cache.expireAfter", "10h", ConfigurationGroup.TECNICAS, null, new String[]{});
    }
}
