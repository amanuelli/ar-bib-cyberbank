/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2298
 *
 * @author deybi
 */
public class DB20170523_1147_2298 extends DBVSUpdate {

    @Override
    public void up() {
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile" };
        String[] formFieldsMessages = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value" };
        String[] formFieldsValues;
        String idForm = "cashAdvance";
        String version = "1";

        /**
         * ** Insert Form - AvanceEnEfectivo ***
         */
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[] { "id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable" };
        String[] formsFieldsValues = new String[] { idForm, version, "1", "creditcards", "process", "demo:1:3", "creditCards", null, "1", "0", "1" };
        insert("forms", formsFields, formsFieldsValues);

        String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value", "modification_date" };
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_messages (id_message, id_form,version, lang, value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "', '" + version + "', 'es', 'Avance en Efectivo', TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "es", "Avance en Efectivo", date });
        }

        delete("permissions", "id_permission = 'client.form." + idForm + ".send'");
        insert("permissions", new String[] { "id_permission" }, new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "accessToken-pin"});

        /**
         * ** Insert field product selector - Tarjeta ***
         */
        formFieldsValues = new String[] { "creditcard", idForm, version, "productselector", "1", "TRUE", "TRUE", "creditCardSelector", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_ps", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { "creditcard", idForm, version, "field-normal" });
        insert("form_field_ps_product_types", new String[] { "id_field", "id_form", "form_version", "id_product_type" }, new String[] { "creditcard", idForm, version, "TC" });
        insert("form_field_ps_permissions", new String[] { "id_field", "id_form", "form_version", "id_permission" }, new String[] { "creditcard", idForm, version, "product.read" });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".creditcard.label', 'es', 'creditcard','" + idForm + "','" + version + "',TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Tarjeta')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".creditcard.requiredError', 'es', 'creditcard','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe seleccionar una tarjeta')");

        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".creditcard.label", "es", "creditcard", idForm, version, date, "Tarjeta" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".creditcard.requiredError", "es", "creditcard", idForm, version, date, "Debe seleccionar una tarjeta" });
        }

        /**
         * ** Insert field amount - Monto ***
         */
        formFieldsValues = new String[] { "amount", idForm, version, "amount", "2", "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[] { "id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount" }, new String[] { "amount", idForm, version, "field-small", "1", "0" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".amount.label', 'es', 'amount','" + idForm + "','" + version + "',TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Monto')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".amount.requiredError', 'es', 'amount','" + idForm + "','" + version + "',TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar un importe mayor a cero y que no supere el monto disponible')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".amount.label", "es", "amount", idForm, version, date, "Monto" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".amount.requiredError", "es", "amount", idForm, version, date, "Debe ingresar un importe mayor a cero y que no supere el monto disponible" });
        }
        /**
         * ** Insert field selector - cuotas ***
         */
        formFieldsValues = new String[] { "installments", idForm, version, "selector", "3", "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as" }, new String[] { "installments", idForm, version, "field-smaller", "0", "combo" });

        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "01" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "02" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "03" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "04" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "05" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "06" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "07" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "08" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "09" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "10" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "11" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "installments", idForm, version, "12" });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.01', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'1')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.02', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'2')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.03', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'3')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.04', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'4')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.05', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'5')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.06', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'6')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.07', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'7')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.08', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'8')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.09', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'9')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.10', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'10')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.11', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'11')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.option.12', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'12')");

            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.label', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Cuotas')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.help', 'es', 'installments','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Corresponde a la cantidad de cuotas en las que se cobrará el adelanto de efectivo en la tarjeta de crédito')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".installments.requiredError', 'es', 'installments','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe seleccionar un número de cuotas')");

        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.01", "es", "installments", idForm, version, date, "1" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.02", "es", "installments", idForm, version, date, "2" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.03", "es", "installments", idForm, version, date, "3" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.04", "es", "installments", idForm, version, date, "4" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.05", "es", "installments", idForm, version, date, "5" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.06", "es", "installments", idForm, version, date, "6" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.07", "es", "installments", idForm, version, date, "7" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.08", "es", "installments", idForm, version, date, "8" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.09", "es", "installments", idForm, version, date, "9" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.10", "es", "installments", idForm, version, date, "10" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.11", "es", "installments", idForm, version, date, "11" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.option.12", "es", "installments", idForm, version, date, "12" });

            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.label", "es", "installments", idForm, version, date, "Cuotas" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.help", "es", "installments", idForm, version, date, "Corresponde a la cantidad de cuotas en las que se cobrará el adelanto de efectivo en la tarjeta de crédito" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".installments.requiredError", "es", "installments", idForm, version, date, "Debe seleccionar un número de cuotas" });
        }
        /**
         * ** Insert field text - referencia ***
         */
        formFieldsValues = new String[] { "reference", idForm, version, "text", "5", "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "reference", idForm, version, "0", "30", "field-normal", null });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".reference.label', 'es', 'reference','" + idForm + "','" + version + "',TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Referencia')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".reference.label", "es", "reference", idForm, version, date, "Referencia" });
        }
        /**
         * ** Insert field product selector - cuenta Destino ***
         */
        formFieldsValues = new String[] { "creditAccount", idForm, version, "productselector", "4", "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);

        formFields = new String[] { "id_field", "id_form", "form_version", "display_type", "show_other_option", "filter_actives", "show_other_option_in_mobile" };
        formFieldsValues = new String[] { "creditAccount", idForm, version, "field-small", "0", "1", "0" };
        insert("form_field_ps", formFields, formFieldsValues);

        formFields = new String[] { "id_field", "id_form", "form_version", "id_permission" };
        formFieldsValues = new String[] { "creditAccount", idForm, version, "product.read" };
        insert("form_field_ps_permissions", formFields, formFieldsValues);

        formFields = new String[] { "id_field", "id_form", "form_version", "id_product_type" };
        formFieldsValues = new String[] { "creditAccount", idForm, version, "CA" };
        insert("form_field_ps_product_types", formFields, formFieldsValues);

        formFields = new String[] { "id_field", "id_form", "form_version", "id_product_type" };
        formFieldsValues = new String[] { "creditAccount", idForm, version, "CC" };
        insert("form_field_ps_product_types", formFields, formFieldsValues);

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".creditAccount.invalidError', 'es', 'creditAccount','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe seleccionar una cuenta destino')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".creditAccount.label', 'es', 'creditAccount','" + idForm + "','" + version + "',TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Cuenta Destino')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + ".creditAccount.requiredError', 'es', 'creditAccount','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe seleccionar una cuenta destino')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".creditAccount.invalidError", "es", "creditAccount", idForm, version, date, "Debe seleccionar una cuenta destino" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".creditAccount.label", "es", "creditAccount", idForm, version, date, "Cuenta Destino" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".creditAccount.requiredError", "es", "creditAccount", idForm, version, date, "Debe seleccionar una cuenta destino" });
        }

    }

}
