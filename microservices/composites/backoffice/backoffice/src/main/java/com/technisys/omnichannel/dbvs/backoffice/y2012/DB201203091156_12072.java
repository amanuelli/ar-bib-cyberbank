/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 * @author grosso
 */
public class DB201203091156_12072 extends DBVSUpdate {

    @Override
    public void up() {
        createIndex("client_banks", "idx_cb_nombrebanco", new String[]{"nombrebanco"});
        createIndex("client_environments", "idx_ce_id_environment", new String[]{"id_environment"});

        dropTable("client_stetement_lines_notes");

        ColumnDefinition[] columns = new ColumnDefinition[]{
            new ColumnDefinition("id_movement", Types.INTEGER, 10, 0, false, null),
            new ColumnDefinition("id_product", Types.VARCHAR, 254, 0, false, null),
            new ColumnDefinition("note", Types.VARCHAR, 254, 0, true, null)
        };

        String[] primaryKey = new String[]{"id_movement", "id_product"};
        createTable("client_statement_lines_notes", columns, primaryKey);
    }
}
