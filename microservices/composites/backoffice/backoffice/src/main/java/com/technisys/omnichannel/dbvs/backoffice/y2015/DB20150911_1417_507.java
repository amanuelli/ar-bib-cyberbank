/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150911_1417_507 extends DBVSUpdate {

    @Override
    public void up() {
        delete("group_permissions", "id_permission='core.loginWithPIN' OR id_permission='user.preferences.pin'");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions (id_group, id_permission, target) select id_group, 'core.loginWithPIN', 'NONE' FROM groups");
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions (id_group, id_permission, target) select id_group, 'user.preferences.pin', 'NONE' FROM groups");
        
        updateConfiguration("auth.login.credentialRequested", "pin");
        
    }
}
