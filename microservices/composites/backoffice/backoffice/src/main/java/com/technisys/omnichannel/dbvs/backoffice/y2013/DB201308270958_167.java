/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201308270958_167 extends DBVSUpdate {

    @Override
    public void up() {
       
        insert("form_fields", new String[]{"id_field", "id_form", "description", "field_type", "mandatory", "default_value", "possible_values", "validation_regexp", "depends_on_id_field", "depends_on_value", "required_permission", "control_cap", "transaction_product"},
                new String[]{"disclaimer", "4", null, "disclaimer", "1", "Texto no modificable que se presenta al cliente y que debe aceptar a través de la verificación de un campo de aceptación para poder continuar.", null, null, null, null, null, "0", "0"});

        update("forms", new String[]{"template_es"}, new String[]{"<form:form> <form:section> <form:field idField='valorNetoDelBien'/> <form:field idField='incluyeSeguro'/> <form:field idField='capitalAFinanciar'/> <form:field idField='plazo'/> <form:field idField='periodicidadDePago'/> <form:field idField='informacionAdicional'/> <form:field idField='disclaimer'/> </form:section></form:form>"}, "id_form=4");

    }
}