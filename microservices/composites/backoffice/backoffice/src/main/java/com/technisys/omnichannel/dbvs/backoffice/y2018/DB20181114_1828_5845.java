/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author msouza
 */
public class DB20181114_1828_5845 extends DBVSUpdate {

    @Override
    public void up() {
    	
    	String notEmptyValidation = "notEmpty";
    	
        insertOrUpdateConfiguration("environment.type.corporate.segments", "CMB", ConfigurationGroup.NEGOCIO, null, new String[]{notEmptyValidation});
        insertOrUpdateConfiguration("environment.type.retail.segments", "PFS", ConfigurationGroup.NEGOCIO, null ,new String[]{notEmptyValidation});
        
        insertOrUpdateConfiguration("environment.type.corporate.adminScheme", "medium", ConfigurationGroup.NEGOCIO, null, new String[]{notEmptyValidation}, "simple|medium|advanced");
        insertOrUpdateConfiguration("environment.type.retail.adminScheme", "simple", ConfigurationGroup.NEGOCIO, null, new String[]{notEmptyValidation}, "simple|medium|advanced");
    }
}
