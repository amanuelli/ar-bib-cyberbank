/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2318
 *
 * @author dimoda
 */
public class DB20170427_1146_2318 extends DBVSUpdate {

    @Override
    public void up() {
       
        update("form_field_text", new String[]{"max_length"}, new String[]{"50"}, "id_field='name' and id_form='additionalCreditCardRequest'");
        update("form_field_text", new String[]{"max_length"}, new String[]{"50"}, "id_field='lastName' and id_form='additionalCreditCardRequest'");
   
    }

}