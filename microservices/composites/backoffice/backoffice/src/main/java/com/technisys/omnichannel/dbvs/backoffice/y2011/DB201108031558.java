/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 *
 * Fix configuration
 */
public class DB201108031558 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.account.list'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.AMEX'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.creditCard.list'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.loan.list'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.MASTER'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.payCreditCard.list'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.payLoan.list'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.paySalary.list'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.paySuppliers.list'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.transferBatch.list'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.transferInternal.list'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.productTypes.VISA'");
        update("configuration", new String[]{"possible_values"}, new String[]{""}, "id_field = 'rubicon.userconfiguration.password.pattern'");
    }
}