/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4324
 *
 * @author msouza
 */
public class DB20180504_1142_4324 extends DBVSUpdate {

    @Override
    public void up() {
        deleteConfiguration("client.emailValidationFormat");
    }
}