/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author mquerves
 */
public class DB201108151031_10869 extends DBVSUpdate {

    @Override
    public void up() {

        String[] fieldNames = new String[]{"template_es"};
        String[] fieldValues = new String[]{"<form:form>	<form:section>		<form:field idField=\"valorNetoDelBien\"/>		<form:field idField=\"incluyeSeguro\"/>		<form:field idField=\"capitalAFinanciar\"/>		<form:field idField=\"plazo\"/>		<form:field idField=\"periodicidadDePago\"/>		<form:field idField=\"informacionAdicional\"/>	</form:section></form:form>"};
        update("forms", fieldNames, fieldValues, "id_form = 4");

        fieldNames = new String[]{"value"};
        fieldValues = new String[]{"accounts|loans|creditcards|payments|foreigntrade|foreigntrade_bondsAndGuarantees|foreigntrade_export|foreigntrade_import|others"};
        update("configuration", fieldNames, fieldValues, "id_field ='backoffice.forms.locations'");
    }
}
