/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.requests;

import com.technisys.omnichannel.backoffice.business.PaginatedListRequest;
import com.technisys.omnichannel.core.utils.DateUtils;
import java.util.Date;

/**
 *
 * @author Sebastian Barbosa
 */
public class ListRequest extends PaginatedListRequest {

    private String invitationCode;
    private String productGroupId;
    private String documentNumber;
    private String status;

    private Date creationDateFrom;
    private Date creationDateTo;
    private String email;
    private String mobileNumber;

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public String getProductGroupId() {
        return productGroupId;
    }

    public void setProductGroupId(String productGroupId) {
        this.productGroupId = productGroupId;
    }

    public Date getCreationDateFrom() {
        return creationDateFrom;
    }

    public void setCreationDateFrom(Date creationDateFrom) {
        this.creationDateFrom = creationDateFrom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getCreationDateTo() {
        return creationDateTo;
    }

    public void setCreationDateTo(Date creationDateTo) {
        this.creationDateTo = creationDateTo;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    @Override
    public String toString() {
        return "ListRequest{" + "invitationCode=" + invitationCode + ", productGroupId=" + productGroupId + ", creationDateFrom="
                + ((creationDateFrom == null) ? "null" : DateUtils.formatFullDate(creationDateFrom)) + ", email="
                + email + ", creationDateTo="
                + ((creationDateTo == null) ? "null" : DateUtils.formatFullDate(creationDateTo)) + ", documentNumber="
                + documentNumber + ", status=" + status + ", mobile_number=" + mobileNumber + "}";
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
}
