/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6391
 *
 * @author emordezki
 */
public class DB20190123_1124_6391 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fields = new String[] {"value"};
        String[] fieldValues = new String[] {"Payments list management"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.file.label' AND lang = 'en'");
        fieldValues = new String[] {"Tipo de alta"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.file.label' AND lang = 'es'");
    }
}
