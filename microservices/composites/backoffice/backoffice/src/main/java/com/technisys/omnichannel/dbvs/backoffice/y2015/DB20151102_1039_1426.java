/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20151102_1039_1426 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("core.attempts.features", "session.loginWithSecondFactorAndPassword.step1=login|session.loginWithSecondFactorAndPassword.step2=login|session.recoverPassword.step1=login|enrollment.associate.verifyStep1=login|enrollment.associate.verifyStep2=login|session.recoverPinAndPassword.step2=recoverCredential|session.incrementCaptchaAttempts=login|session.recoverPin.step1=login");
        updateConfiguration("session.exchangeToken.duration", "5m");
    }
}
