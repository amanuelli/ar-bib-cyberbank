/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.cybo.ListResponse;
import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.ListRequest;
import com.technisys.omnichannel.backoffice.domain.cybo.invitationcodes.InvitationCode;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author Mauricio Uribe
 */
public class ListActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        ListResponse<InvitationCode> response = new ListResponse<InvitationCode>(request);

        try {
            ListRequest listRequest = (ListRequest) request;
            int rowsPerPage = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "backoffice.rowsPerPage");

            PaginatedList<com.technisys.omnichannel.core.domain.InvitationCode> list;

            Date creationDateFrom = (listRequest.getCreationDateFrom() != null) ? DateUtils.trunc(listRequest.getCreationDateFrom(), ChronoUnit.DAYS) : null;
            Date creationDateTo = (listRequest.getCreationDateTo() != null) ? DateUtils.changeTime(listRequest.getCreationDateTo(), 23, 59, 59, 998) : null;

            int limit = rowsPerPage;
            int offset = listRequest.getPageNumber() * limit;

            list = InvitationCodesHandler.listInvitationCodes(listRequest.getInvitationCode(),
                    listRequest.getProductGroupId(), null, null, listRequest.getDocumentNumber(), listRequest.getEmail(),
                    creationDateFrom, creationDateTo, listRequest.getStatus(),
                    listRequest.getMobileNumber(), offset, limit, listRequest.getOrderBy(), listRequest.getProductGroupName());

            List<InvitationCode> content = list.getElementList().stream().map(InvitationCode::new).collect(Collectors.toList());
            
            response.setPage(listRequest.getPageNumber());
            response.setTotal(list.getTotalPages());
            response.setTotalElements(list.getTotalRows());
            response.setContent(content);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        ListRequest listRequest = (ListRequest) request;
        Map<String, String> result = new HashMap<>();
        final int MAX_HOUR_INTERVAL = 12;
        long maxHoursIntervalInMillis = MAX_HOUR_INTERVAL * 60 * 60 * 1000L;
        String workspace = listRequest.getProductGroupName();
        String documentNumber = listRequest.getDocumentNumber();

        try {
            if (StringUtils.isAllBlank(workspace, documentNumber)) {
                Date valueDateTimeFrom = listRequest.getCreationDateFrom();
                Date valueDateTimeTo = listRequest.getCreationDateTo();

                long dateDiff = validateDateRange(valueDateTimeFrom, valueDateTimeTo, result);
                if(dateDiff > maxHoursIntervalInMillis) {
                    result.put("@creationDateTo", "cybo.invitationCodesManagement.validations.submittedRange.invalid");
                }
            }

            // To export
            if (listRequest.getPageNumber() <= 0 && !Authorization.hasBackofficePermission(request.getIdUser(), "backoffice.massiveData.allow")) {
                    throw new ActivityException(ReturnCodes.NOT_AUTHORIZED,
                            "User not authorized to export data");
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    private long validateDateRange(Date from, Date to, Map<String, String> errors) {
        long dateDiff = -1;
        if (from != null || to != null) {
            if (from == null) {
                errors.put("@creationDateFrom", "cybo.invitationCodesManagement.validations.dateFrom.empty");
            } else if (to == null) {
                errors.put("@creationDateTo", "cybo.invitationCodesManagement.validations.dateTo.empty");
            } else {
                dateDiff = to.getTime() - from.getTime();
                if (dateDiff < 0) {
                    errors.put("@creationDateTo", "cybo.invitationCodesManagement.search.advanced.validations.creationDateTo.mustBeGreater");
                }
            }
        } else {
            errors.put("@creationDateFrom", "cybo.invitationCodesManagement.validations.dateFrom.empty");
        }

        return dateDiff;
    }
}
