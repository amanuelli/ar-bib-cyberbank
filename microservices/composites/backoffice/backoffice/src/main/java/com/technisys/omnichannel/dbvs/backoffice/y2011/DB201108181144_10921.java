/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Diego Curbelo
 */
public class DB201108181144_10921 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_field", "value", "id_group"};
        String[] fieldValues = new String[]{"rubicon.documentsTypes.list", "ci|rut|passport|other", "id_group"};
        insert("configuration", fieldNames, fieldValues);

        fieldNames = new String[]{"possible_values"};
        fieldValues = new String[]{"CI|RUT|Pasaporte|Otro"};
        update("form_fields", fieldNames, fieldValues, "id_field= 'tipoDeDocumento' AND id_form = 6");

        fieldNames = new String[]{"possible_values"};
        fieldValues = new String[]{"CI|RUT|Pasaporte|Otro"};
        update("form_fields", fieldNames, fieldValues, "id_field= 'conyugeCotitularDocumentoTipo' AND id_form = 1");

        fieldNames = new String[]{"possible_values"};
        fieldValues = new String[]{"CI|RUT|Pasaporte|Otro"};
        update("form_fields", fieldNames, fieldValues, "id_field= 'datosPersonalesDelConyugeDelCotitularTipoDocumento' AND id_form = 62");

        fieldNames = new String[]{"possible_values"};
        fieldValues = new String[]{"CI|RUT|Pasaporte|Otro"};
        update("form_fields", fieldNames, fieldValues, "id_field= 'dPCCotitularTipoDocumento' AND id_form = 61");

        fieldNames = new String[]{"possible_values"};
        fieldValues = new String[]{"CI|RUT|Pasaporte|Otro"};
        update("form_fields", fieldNames, fieldValues, "id_field= 'tarjetaDebitoAdicionalTipoDocumento' AND id_form = 61");


        fieldNames = new String[]{"possible_values"};
        fieldValues = new String[]{"CI|RUT|Pasaporte|Otro"};
        update("form_fields", fieldNames, fieldValues, "id_field= 'tarjetaDebitoAdicionalTipoDocumento' AND id_form = 62");

    }
}