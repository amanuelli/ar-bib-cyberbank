/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2888
 *
 * @author pdominguez
 */
public class DB20170808_1159_2888 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "presentationOfDocumentsUnderCollection";

        update("form_field_textarea", new String[]{"max_length"}, new String[]{"500"}, "id_field = 'observations' AND id_form = '" + idForm + "'");
        update("form_field_textarea", new String[]{"max_length"}, new String[]{"500"}, "id_field = 'notificationBody' AND id_form = '" + idForm + "'");
    }

}
