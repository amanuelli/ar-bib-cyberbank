/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5306
 *
 * @author msouza
 */
public class DB20180820_1652_5306 extends DBVSUpdate {

    @Override
    public void up() {
               
        insertActivity(
            "weather.get",
            "com.technisys.omnichannel.client.activities.adaptative.WeatherActivity",
            "other", 
            ActivityDescriptor.AuditLevel.None, 
            ClientActivityDescriptor.Kind.Other, 
            null);
                   
        insertOrUpdateConfiguration("adaptative.accuWeather.apiKey", "FrrPCmscm0W1ofrU7v1nOTPFnel7lHXP", ConfigurationGroup.TECNICAS, "weather", new String[]{"notEmpty"}, null, true);
        insertOrUpdateConfiguration("adaptative.accuWeather.serviceUrl.cityKey.ip", "https://dataservice.accuweather.com/locations/v1/cities/ipaddress?apikey=${cnf:adaptative.accuWeather.apiKey}&language=en&q=${CLIENT_IP}&details=false", ConfigurationGroup.TECNICAS, "weather", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("adaptative.accuWeather.serviceUrl.cityKey.latitudeAndLongitude", "http://dataservice.accuweather.com/locations/v1/cities/geoposition/search?apikey=${cnf:adaptative.accuWeather.apiKey}&q=${LATITUDE}%2C${LONGITUDE}&language=en&details=false&toplevel=true", ConfigurationGroup.TECNICAS, "weather", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("adaptative.accuWeather.serviceUrl.currentConditions", "https://dataservice.accuweather.com/currentconditions/v1/${CITY_KEY}?apikey=${cnf:adaptative.accuWeather.apiKey}&language=en&details=false", ConfigurationGroup.TECNICAS, "weather", new String[]{"notEmpty"});

        
    }
}