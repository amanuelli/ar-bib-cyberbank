/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

/*
 * @author pbanales
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

public class DB20190308_1544_6805 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("backoffice.environments.holding.envType",
                "corporateGroup",
                ConfigurationGroup.OTHER,
                null,
                new String[] {});
    }
}
