/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2503
 *
 * @author ivitale
 */
public class DB20170628_1228_2503 extends DBVSUpdate {

    @Override
    public void up() {
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String idForm = "authorizeDiscrepancyDocuments";
        String version = "1";
        Map<String, String> messages = new HashMap();
        
        //Tambien borrar dbvs,auditlog
        delete("forms", "id_form='" + idForm + "'");
        delete("form_fields", "id_form='" + idForm + "'");
        delete("form_field_text", "id_form='" + idForm + "'");
        delete("form_field_messages", "id_form='" + idForm + "'");
        delete("form_field_amount", "id_form='" + idForm + "'");
        delete("form_field_textarea", "id_form='" + idForm + "'");
        delete("form_field_terms_conditions", "id_form='" + idForm + "'");
        delete("form_field_emaillist", "id_form='" + idForm + "'");
        delete("form_field_selector", "id_form='" + idForm + "'");
        delete("form_field_selector_options", "id_form='" + idForm + "'");
        delete("form_field_date", "id_form='" + idForm + "'"); 
        
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};
        String[] formFieldsText = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        String[] forms = new String[]{"id_form", "version", "enabled", "category", "type", "admin_option", "id_activity", "last", "deleted", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow", "id_bpm_process"};
        String[] formFieldsAmount = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount"};
        String[] formFieldsTextArea = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        String[] formFieldsTermsConditions = new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"};
        String[] formFieldsEmailList = new String[]{"id_field", "id_form", "form_version", "display_type"};
        String[] formFieldsDate = new String[]{"id_field", "id_form", "form_version", "display_type"};
        String[] formFieldsSelector = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"};
        String[] formFieldsSelectorOption = new String[]{"id_field", "id_form", "form_version", "value"};
          
        // Insert Form -   - Autorización de envío de documentos con discrepancia        
        String[] formsValues = new String[]{idForm, version, "1", "creditcards", "process", null, null, "1", "0", "1", "1", "1", "1", "1", "demo:1:3"};
        insert("forms", forms, formsValues);

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', 'Autorización de envío de documentos con discrepancia', TO_DATE('2017-04-03 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Autorización de envío de documentos con discrepancia", date});    
        }
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});
        
        /**** 1 Nro. de operación ****/        
        String[] formFieldsValues = new String[]{"operationNumber", idForm, version , "text", "1", "TRUE", "TRUE",null, "1", "0", "default", "0"};
        String[] formFieldsTextValues = new String[]{"operationNumber", idForm, version, "0", "30", "field-normal", "onlyNumbers"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        
        messages.put("operationNumber.label", "Nro. de operación");
        messages.put("operationNumber.requiredError", "Debe ingresar un Nro. de operación");
        
        //Segundo campo//
        //Numero de factura//
        formFieldsValues = new String[]{"bill", idForm, version, "text", "2", "TRUE", "TRUE", null, "1", "0", "default", "0"};    
        formFieldsTextValues = new String[]{"bill", idForm, version, "1", "80", "field-normal", null};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
  
        messages.put("bill.label", "Nro. de factura");
        messages.put("bill.requiredError", "Debe ingresar un Nro. de factura");     

        //Tercer campo//
        //Importe de Facturas// 
        formFieldsValues = new String[]{"billAmount", idForm, version, "amount", "3", "TRUE", "TRUE", null, "1", "0", "default", "0"};    
        String[] formFieldsAmountValues = new String[]{"billAmount", idForm, version, "field-normal", "1", "1"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", formFieldsAmount, formFieldsAmountValues);
  
        messages.put("billAmount.label", "Importe de la factura");
        messages.put("billAmount.requiredError", "El importe de la utilización no es válido");

        //Selector       
        formFieldsValues = new String[]{"operationType", idForm, version, "selector", "4", "TRUE", "TRUE",null, "1", "0", "default", "0"};
        String[] formFieldsSelectorValues = new String[]{"operationType", idForm, version, "field-normal", "aerial", "0", "combo"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", formFieldsSelector, formFieldsSelectorValues);
        messages.put("operationType.label", "Tipo de operación");        
        
        String[] formFieldsSelectorOptionValues = new String[]{"operationType", idForm, version, "aprobation"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("operationType.option.aprobation", "Envío para aprobación y pago");        
        
        formFieldsSelectorOptionValues = new String[]{"operationType", idForm, version, "discrepanciesFound"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("operationType.option.discrepanciesFound", "Envío con discrepancias encontradas por Uds");
        
        formFieldsSelectorOptionValues = new String[]{"operationType", idForm, version, "discrepanciesNext"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("operationType.option.discrepanciesNext", "Envío con las siguientes discrepancias");
       
        //Discrepancias//
        formFieldsValues = new String[]{"discrepancies", idForm, version, "textarea", "5", "TRUE", "TRUE", null, "1", "0", "default", "0"};    
        String[] formFieldsTextAreaValues = new String[]{"discrepancies", idForm, version, "0", "500", "field-normal"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", formFieldsTextArea, formFieldsTextAreaValues);
  
        messages.put("discrepancies.label", "Discrepancias");
        messages.put("discrepancies.requiredError", "Debe ingresar al menos una discrepancia");
        messages.put("discrepancies.help", "Ingrese aquí las discrepancias");

        //Disclaimer        
        formFieldsValues = new String[]{"disclaimer", idForm, version, "termsandconditions", "6", "TRUE", "FALSE", null, "1", "1", "default", "0"};
        String[] formFieldsTermsConditionsValues = new String[]{"disclaimer", idForm, version, "field-normal", "0", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", formFieldsTermsConditions, formFieldsTermsConditionsValues);
        
        messages.put("disclaimer.label", "Disclaimer");
        messages.put("disclaimer.termsAndConditions", "Las instrucciones enviadas al banco por usted luego de las <hora de corte>. serán procesadas al siguiente día hábil bancario.\n" +
            "Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.");
        
        //Email List
        formFieldsValues = new String[]{"emaillist", idForm, version, "emaillist", "7", "TRUE", "FALSE", null, "1", "0", "default", "0"};
        String[] formFieldsEmailListValues = new String[]{"emaillist", idForm, version, "field-normal"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_emaillist", formFieldsEmailList, formFieldsEmailListValues);
        messages.put("emaillist.label", "Emails de notificación");

        //Cuerpo de Notificacion//
        formFieldsValues = new String[]{"notification", idForm, version, "textarea", "8", "TRUE", "FALSE", null, "1", "0", "default", "0"};    
        formFieldsTextAreaValues = new String[]{"notification", idForm, version, "0", "500", "field-normal"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", formFieldsTextArea, formFieldsTextAreaValues);
  
        messages.put("notification.label", "Cuerpo de notificación");      
        
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        
        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }
}