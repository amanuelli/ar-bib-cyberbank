/* 
 * Copyright 2016 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2016;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20160921_1721_1889 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("core.cache.rubiconCoreConnectorC.listExchangeRates.maximumSize", "50", ConfigurationGroup.TECNICAS, null, new String[]{});
        insertOrUpdateConfiguration("core.cache.rubiconCoreConnectorC.listWidgetsExchangeRates.maximumSize", "50", ConfigurationGroup.TECNICAS, null, new String[]{});
        insertOrUpdateConfiguration("core.cache.rubiconCoreConnectorC.listExchangeRates.expireAfter", "2h", ConfigurationGroup.TECNICAS, null, new String[]{});
        insertOrUpdateConfiguration("core.cache.rubiconCoreConnectorC.listWidgetsExchangeRates.expireAfter", "2h", ConfigurationGroup.TECNICAS, null, new String[]{});
        insertOrUpdateConfiguration("core.scheduler.pay.loan.send.saturdayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
        insertOrUpdateConfiguration("core.scheduler.pay.loan.send.sundayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
        insertOrUpdateConfiguration("core.scheduler.transfers.internal.send.saturdayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
        insertOrUpdateConfiguration("core.scheduler.transfers.internal.send.sundayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
        insertOrUpdateConfiguration("core.scheduler.transfers.internal.preview.saturdayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
        insertOrUpdateConfiguration("core.scheduler.transfers.internal.preview.sundayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
        
    }
}
