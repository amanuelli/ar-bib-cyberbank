/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author AAbalo
 */
public class DB20150807_1730_651 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("credential.password.componentFQN", "com.technisys.omnichannel.credentials.builtin.PasswordPlugin");
    }
}
