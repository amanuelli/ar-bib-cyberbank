/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * This class insert in params to management the Audit service
 * @author hbermudez@technisys.com
 */
public class DB20200522_1625_5140 extends DBVSUpdate {

    @Override
    public void up() {

        insertOrUpdateConfiguration(
                "audit.encrypt.keyName",
                null,
                DBVSUpdate.ConfigurationGroup.TECNICAS,
                "others",
                new String[]{});

        insertOrUpdateConfiguration(
                "audit.encrypt.algorithm",
                "EC",
                DBVSUpdate.ConfigurationGroup.TECNICAS,
                "others",
                new String[]{"notEmpty"},
                "EC|RSA");
    }
}
