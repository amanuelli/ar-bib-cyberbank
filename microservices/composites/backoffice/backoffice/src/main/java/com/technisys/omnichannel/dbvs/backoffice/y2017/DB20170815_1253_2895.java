/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2895
 *
 * @author rmedina
 */
public class DB20170815_1253_2895 extends DBVSUpdate {

    @Override
    public void up() {
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String idForm = "presentationOfDocumentsUnderCollection";

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) 
             customSentence(DBVS.DIALECT_ORACLE, "UPDATE form_field_messages "
                     + "SET modification_date = TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),"
                     + "value = 'Ingrese el nombre del courier con quien usted tiene cuenta' "
                     + "WHERE id_message = 'fields." + idForm + ".courierName.help' AND lang = 'es'"); 
        else
            update("form_field_messages", new String[] { "modification_date", "value" }, new String[]{date, "Ingrese el nombre del courier con quien usted tiene cuenta"}, "id_message = '" + "fields." + idForm + ".courierName.help' AND lang = 'es'" );
      
    }
}