/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author AndresCasas
 */

public class DB20200729_1325_13828 extends DBVSUpdate {

    @Override
    public void up() {
        String idMessage = "fields.transferLocal.creditAccountName.requiredError";

        update("form_field_messages", new String[]{"value"}, new String[]{"Debe ingresar un nombre"}, "id_message='" + idMessage + "' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"You must enter a name"}, "id_message='" + idMessage + "' and lang='en'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Você deve digitar um nome"}, "id_message='" + idMessage + "' and lang='pt'");
    }

}