/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author lpoll
 */

public class DB20190311_1058_6792 extends DBVSUpdate {

    @Override
    public void up() {
        insertBackofficePermission("backoffice.environments.holdings.approve");
        insertBackofficePermission("backoffice.environments.holdings.manage");

        insertOrUpdateConfiguration("country.code.default", "UY", ConfigurationGroup.TECNICAS, "others", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("document.type.default", "I", ConfigurationGroup.TECNICAS, "others", new String[]{"notEmpty"});
    }

}