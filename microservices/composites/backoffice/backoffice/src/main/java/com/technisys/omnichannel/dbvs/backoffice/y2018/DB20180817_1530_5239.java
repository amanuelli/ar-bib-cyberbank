/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author aalves
 */
public class DB20180817_1530_5239 extends DBVSUpdate {

    @Override
    public void up() {
        deleteConfiguration("breadcrumb.accounts.url");
        deleteConfiguration("breadcrumb.creditcards.url");
        deleteConfiguration("breadcrumb.deposits.url");
        deleteConfiguration("breadcrumb.loans.url");
        deleteConfiguration("breadcrumb.transfers.url");

        deleteConfiguration("client.emailValidationFormat");
        deleteConfiguration("client.sidebar.hideFormIds");

        deleteConfiguration("frontend.ajax.timeout");
        deleteConfiguration("frontend.availabelFeatures.refresh.interval");
        deleteConfiguration("frontend.date.format");
        deleteConfiguration("frontend.date.format.internal");
        deleteConfiguration("frontend.forms.refresh.interval");
        deleteConfiguration("frontend.google.api.key");
        deleteConfiguration("frontend.mediumDateFormat");
        deleteConfiguration("frontend.session.headerEnvironments.maxToShow");
        deleteConfiguration("frontend.timeFormat");

        deleteConfiguration("global.displayMessageKeys");

        deleteConfiguration("product.list.maxBoxDisplay");

        deleteConfiguration("widget.products.account.maxToSmallCards");
        deleteConfiguration("widget.products.creditCard.maxToSmallCards");
        deleteConfiguration("widget.products.loan.maxToSmallCards");

        deleteConfiguration("widgets.notifications.max.simple.cards");
    }
    
}
