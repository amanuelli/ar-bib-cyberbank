/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.i18n.I18n;

import java.util.HashMap;
import java.util.Map;

/**
 * @author lpoll
 */

public class DB20190225_1328_5274 extends DBVSUpdate {

    @Override
    public void up() {
        String[][] currentBankselectors = new String[][]{
            { "frequentDestination", "exteriorBank" },
            { "frequentDestination", "localBank" },
            { "transferForeign", "intermediaryBank" },
            { "transferForeign", "recipientAccountCode" },
            { "creditLetterTransfer", "searchBank" },
        };

        Map<String, String> invalidMessages = new HashMap();
        invalidMessages.put(I18n.LANG_ENGLISH, "Invalid code");
        invalidMessages.put(I18n.LANG_SPANISH, "Código inválido");
        invalidMessages.put(I18n.LANG_PORTUGUESE, "Código inválido");
        for (String[] currentBankselector: currentBankselectors) {
            for (Map.Entry<String, String> invalidMessage: invalidMessages.entrySet()) {
                customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                        "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                                + "SELECT 'fields." + currentBankselector[0] + "." + currentBankselector[1] + ".invalidError', '" + invalidMessage.getKey() + "', '"
                                + currentBankselector[1] +"', '" + currentBankselector[0] + "', MAX(version), '" + invalidMessage.getValue()
                                + "', '2019-02-25 00:00:01' FROM forms WHERE id_form = '" + currentBankselector[0] + "' ");

                customSentence(new String[]{DBVS.DIALECT_ORACLE},
                        "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                                + "SELECT 'fields." + currentBankselector[0] + "." + currentBankselector[1] + ".invalidError', '" + invalidMessage.getKey() + "', '"
                                + currentBankselector[1] +"', '" + currentBankselector[0] + "', MAX(version), '" + invalidMessage.getValue()
                                + "', TO_DATE('2019-02-25 00:00:01', 'YYYY-MM-DD HH24:MI:SS') FROM forms WHERE id_form = '" + currentBankselector[0] + "' ");
            }
        }
    }
}