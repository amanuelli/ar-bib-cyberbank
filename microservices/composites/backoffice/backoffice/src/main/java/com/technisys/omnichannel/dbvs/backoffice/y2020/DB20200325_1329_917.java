/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author Cristobal Meneses
 */

public class DB20200325_1329_917 extends DBVSUpdate {

    @Override
    public void up() {
        insert("permissions",new String[]{"id_permission"}, new String[]{"requestLoan.send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"requestLoan.send", "accessToken-pin"});

        insertActivity("request.loan.send",
                "com.technisys.omnichannel.client.activities.loans.request.RequestLoanSendActivity",
                "loans",
                ActivityDescriptor.AuditLevel.Full,
                ClientActivityDescriptor.Kind.Transactional,
                "requestLoan.send", "NONE"
        );

        update("forms",
                new String[]{"type","id_activity"},
                new String[]{"activity","request.loan.send"},
                "id_form = 'requestLoan'");

        String[] fieldNames = new String[] { "id_permission", "simple_id_category", "simple_ordinal", "simple_allow_prod_selection", "medium_id_category", "medium_ordinal", "medium_allow_prod_selection", "advanced_id_category", "advanced_ordinal", "advanced_allow_prod_selection", "product_types" };
        String[] fieldValues = new String[] { "requestLoan.send", "payments", "60", "0", "payments", "60", "0", "payments", "60", "1", null };
        insert("adm_ui_permissions", fieldNames, fieldValues);

        insertActivity("request.loan.preview", "com.technisys.omnichannel.client.activities.loans.request.RequestLoanPreviewActivity", "loans", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }

}