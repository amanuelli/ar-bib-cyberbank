/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201108020959_10651 extends DBVSUpdate {

    @Override
    public void up() {

        update("configuration", new String[]{"value"}, new String[]{"000|222|111"}, "id_field = 'rubicon.currency.transferInternal.list'");
        update("configuration", new String[]{"value", "possible_values"}, new String[]{"000", "000|222|111"}, "id_field = 'masterCurrency'");

        update("configuration", new String[]{"value"}, new String[]{"000|222"}, "id_field = 'rubicon.currency.payCreditCard.list'");
        update("configuration", new String[]{"value"}, new String[]{"000|222|111"}, "id_field = 'rubicon.currency.paySalary.list'");
        update("configuration", new String[]{"value"}, new String[]{"000|222|111"}, "id_field = 'rubicon.currency.paySuppliers.list'");
        update("configuration", new String[]{"value"}, new String[]{"000|222"}, "id_field = 'rubicon.currency.sendCheck.list'");
        update("configuration", new String[]{"value"}, new String[]{"222|111"}, "id_field = 'rubicon.widget.quotation.list'");



        String[] fieldNames = new String[]{"id_field", "value", "id_group"};

        String[] fieldValues = new String[]{"rubicon.currency.$", "000", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.currency.U$S", "222", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.currency.EUR", "111", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.currency.GBP", "333", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.currency.JPY", "444", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.currency.UI", "555", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'alquiler' and id_form = '17'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'alquilerAdicional' and id_form = '17'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|555"}, "id_field = 'capitalAFinanciar' and id_form = '4'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222|555"}, "id_field = 'cotitularAlquilerImporte' and id_form = '1'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222|555"}, "id_field = 'cotitularCuotaHipoteca' and id_form = '1'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'cotitularLaboralIngresosNetos' and id_form = '1'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'cotitularLaboralOtrosIngresos' and id_form = '1'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'cuotaHipoteca' and id_form = '17'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'cuotaHipotecaAdicional' and id_form = '17'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'datosLaboralesDelCotitularIngresosNetosMensuales' and id_form = '62'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'datosLaboralesDelCotitularOtrosIngresos' and id_form = '62'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'datosPersonalesCotitularAlquiler' and id_form = '62'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'datosPersonalesCotitularCuotaHipoteca' and id_form = '62'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'dLCotitularIngresosNetosMensuales' and id_form = '61'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'dLCotitularOtrosIngresos' and id_form = '61'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'dPCotitularAlquiler' and id_form = '61'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'dPCotitularCuotaHipoteca' and id_form = '61'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'importe' and id_form = '1'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|111"}, "id_field = 'importe' and id_form = '10'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|111|000"}, "id_field = 'importe' and id_form = '11'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'importe' and id_form = '12'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222"}, "id_field = 'Importe' and id_form = '68'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222|111"}, "id_field = 'Importe' and id_form = '69'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'importe' and id_form = '71'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"111|333|444|222"}, "id_field = 'importeTransferir' and id_form = '41'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222"}, "id_field = 'ImporteVale' and id_form = '68'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'ingresosNetos' and id_form = '14'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'ingresosNetosMensuales' and id_form = '17'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'ingresosOtros' and id_form = '14'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'limiteDeCredito' and id_form = '14'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'limiteOMonto' and id_form = '2'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222|555"}, "id_field = 'liquido' and id_form = '7'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222|555"}, "id_field = 'moneda' and id_form = '5'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'moneda' and id_form = '83'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'moneda' and id_form = '84'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'monedaCarta' and id_form = '18'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'monedaMasterBse' and id_form = '15'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222"}, "id_field = 'montoCedido' and id_form = '66'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'montoLimiteTraspasar' and id_form = '14'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'montoMaximo' and id_form = '70'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'montoMaximo' and id_form = '81'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"111|333|444|222"}, "id_field = 'montoOrigen' and id_form = '41'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'nuevoImporte' and id_form = '65'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222|000"}, "id_field = 'otrosIngresos' and id_form = '17'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"000|222"}, "id_field = 'prestamoTerceroMoneda' and id_form = '16'");
        update("form_fields", new String[]{"possible_values"}, new String[]{"222"}, "id_field = 'valor' and id_form = '66'");


    }
}