/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author cmeneses
 */

public class DB20200218_1732_455 extends DBVSUpdate {

    @Override
    public void up() {

        updateActivity("transfers.foreign.send",
                "com.technisys.omnichannel.client.activities.transfers.foreing.TransfersForeingSendActivity",
                "transfer.foreign",
                "transferForeign",
                ActivityDescriptor.AuditLevel.Full,
                ClientActivityDescriptor.Kind.Transactional
        );
    }

}