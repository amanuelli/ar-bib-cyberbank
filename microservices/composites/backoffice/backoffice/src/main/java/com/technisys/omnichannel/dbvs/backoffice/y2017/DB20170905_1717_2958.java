/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2958
 *
 * @author dimoda
 */
public class DB20170905_1717_2958 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_selector", new String[]{"show_blank_option"}, new String[]{"1"}, "id_field = 'banksExpenses' AND id_form = 'creditLetterTransfer'");
        update("form_field_selector", new String[]{"show_blank_option"}, new String[]{"1"}, "id_field = 'confirmationExpenses' AND id_form = 'creditLetterTransfer'");
        update("form_field_selector", new String[]{"show_blank_option"}, new String[]{"1"}, "id_field = 'creditTransferExpenses' AND id_form = 'creditLetterTransfer'");
        update("form_field_selector", new String[]{"show_blank_option"}, new String[]{"1"}, "id_field = 'expensesIssuingBank' AND id_form = 'creditLetterTransfer'");
        update("form_field_selector", new String[]{"show_blank_option"}, new String[]{"1"}, "id_field = 'from' AND id_form = 'openCreditLetter'");
        update("form_field_selector", new String[]{"show_blank_option"}, new String[]{"1"}, "id_field = 'fundsDestination' AND id_form = 'requestLoan'");
        update("form_field_selector", new String[]{"show_blank_option"}, new String[]{"1"}, "id_field = 'selectCreditLetter' AND id_form = 'modifyCreditLetter'");
        update("form_field_selector", new String[]{"show_blank_option"}, new String[]{"1"}, "id_field = 'yourOtherExpenses' AND id_form = 'creditLetterTransfer'");
    }

}