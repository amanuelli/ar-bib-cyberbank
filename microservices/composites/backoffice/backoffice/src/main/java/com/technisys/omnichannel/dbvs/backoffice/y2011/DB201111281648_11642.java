/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201111281648_11642 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"name", "uri", "default_column", "default_row"};
        String[] fieldValues = new String[]{"twitter", "/widgets/twitter.jsp", "2", "4"};
        insert("client_desktop_widgets", fieldNames, fieldValues);

        update("configuration", new String[]{"value"}, new String[]{"transactions|news|notifications|frequentTasks|banners|productDetails|productSummarized|scheduler|twitter"}, "id_field = 'rubicon.desktop.defaultWidgets.retail'");
        update("configuration", new String[]{"value"}, new String[]{"transactions|news|notifications|frequentTasks|banners|productDetails|productSummarized|scheduler|twitter"}, "id_field = 'rubicon.desktop.defaultWidgets.corporate'");

        fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};

        fieldValues = new String[]{"rubicon.twitterAccount", "RubiconBank", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.urlToShare", "http://www.rubiconbank.com", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.facebookURL", "http://www.facebook.com/pages/Rubicon-Bank/249236341803578", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.googlePlusURL", "https://plus.google.com/u/0/113774870811010368549", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);
    }
}