/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ?
 */
public class DB201201241114_11728 extends DBVSUpdate {

    @Override
    public void up() {
        delete("activities", "id_activity = 'rub.validatePassword'");

        delete("role_permissions", "id_permission = 'rub.validatePassword'");
        delete("permissions_credentials_groups", "id_permission = 'rub.validatePassword'");
        delete("permissions", "id_permission = 'rub.validatePassword'");

        String[] fieldNames = new String[]{"id_permission", "permission_type"};
        String[] fieldValues = new String[]{"rub.loginII", "client"};
        insert("permissions", fieldNames, fieldValues);

        fieldNames = new String[]{"id_permission", "id_credential_group"};
        fieldValues = new String[]{"rub.loginII", "onlyPassword"};
        insert("permissions_credentials_groups", fieldNames, fieldValues);

        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required", "id_group", "auditable"};
        fieldValues = new String[]{"rub.loginII", "1", "1", "com.technisys.rubicon.business.misc.activities.LoginIIActivity", "rub.loginII", "rub.login", "2"};
        insert("activities", fieldNames, fieldValues);

        customSentence(getDialect(), "INSERT INTO role_permissions(id_role, id_permission) (SELECT DISTINCT(R.id_role), 'rub.loginII' FROM roles R, role_permissions RP WHERE R.id_role=RP.id_role AND R.role_type='client' AND RP.id_permission='rub.login')");
    }
}