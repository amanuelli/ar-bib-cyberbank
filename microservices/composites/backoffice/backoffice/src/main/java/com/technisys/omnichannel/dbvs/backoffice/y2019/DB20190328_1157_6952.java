/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author iocampo
 */

public class DB20190328_1157_6952 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE}, "UPDATE configuration SET value = '^(\\+?\\d{3}[- ]?)?([0])?([9])\\d{1}([- ])?\\d{3}([- ])?\\d{3}$' WHERE id_field = 'cellPhone.code.URY.format'");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE}, "UPDATE configuration SET value = '^(\\?(55\\d{2})|\\d{2})[6-9]\\d{8}$' WHERE id_field = 'cellPhone.code.BRA.format'");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE}, "UPDATE configuration SET value = '^(\\([0-9]{3}\\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$' WHERE id_field = 'cellPhone.code.USA.format'");
    }

}