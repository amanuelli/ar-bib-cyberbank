/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.environments.holding.requests;

import com.technisys.omnichannel.core.IBRequest;

public class ReadClientRequest extends IBRequest {

    private String idClient;

    public String getIdClient() {
        return idClient;
    }

    public void setIdClient(String idClient) {
        this.idClient = idClient;
    }

    @Override
    public String toString() {
        return "FindClientRequest{" + "idClient=" + idClient + '}';
    }

}
