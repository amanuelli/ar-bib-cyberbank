/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2203
 *
 * @author dimoda
 */
public class DB20170331_1345_2203 extends DBVSUpdate {

    @Override
    public void up() {
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        String[] formFieldsValues;
        String idForm = "reissueCreditCard";
        String version = "1";
       
        /**** Insert Form - Solicitud de reimpresión de tarjeta ****/    
        delete("forms", "id_form = '" + idForm + "'");
        
        String[] formsFields = new String[]{"id_form", "version", "enabled", "category", "type", "id_jbpm_process", "admin_option","id_activity", "templates_enabled", "drafts_enabled", "schedulable"};
        String[] formsFieldsValues = new String[]{idForm, version, "1", "creditcards", "process", "demo-1", "creditCards", null, "1", "1", "0"};                
        insert("forms", formsFields, formsFieldsValues);
    
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', 'Solicitud de re-impresión de tarjeta de crédito', TO_DATE('2017-03-31 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};    
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Solicitud de re-impresión de tarjeta de crédito", date});
        }
        
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

        
        /**** Insert field product selector - Tarjeta ****/
        formFieldsValues = new String[]{"creditCard", idForm, version,"productselector", "1", "TRUE", "TRUE", "creditCardSelector", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{"creditCard", idForm, version, "field-normal"});
        insert("form_field_ps_product_types", new String[]{"id_field","id_form", "form_version", "id_product_type"}, new String[]{"creditCard", idForm, version, "TC"});
        insert("form_field_ps_permissions", new String[]{"id_field","id_form", "form_version", "id_permission"}, new String[]{"creditCard", idForm, version, "product.read"});  
        
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".creditCard.label', 'es', 'creditCard', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Tarjeta')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".creditCard.requiredError', 'es', 'creditCard', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Debe seleccionar una tarjeta')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".creditCard.label", "es", "creditCard", idForm, version, date, "Tarjeta"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".creditCard.requiredError", "es", "creditCard", idForm, version, date, "Debe seleccionar una tarjeta"});
        }
         
    
        /**** Insert field selector - Motivo ****/
        formFieldsValues = new String[]{"reason", idForm, version, "selector", "2", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"reason", idForm, version, "field-normal", "0"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".reason.label', 'es', 'reason', '" + idForm + "'," + version  + ", TO_DATE('2017-03-31 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Motivo')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".reason.requiredError', 'es', 'reason', '" + idForm + "'," + version  + ", TO_DATE('2017-03-31 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Debe seleccionar un motivo')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".reason.label", "es", "reason", idForm, version, date, "Motivo"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".reason.requiredError", "es", "reason", idForm, version, date, "Debe seleccionar un motivo"}); 
        }
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"reason", idForm, version, "datosMalGrabados"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"reason", idForm, version, "deterioroPlasticoActual"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version" ,"value"}, new String[]{"reason", idForm, version, "plasticoPorRenovacionNoRecibida"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"reason", idForm, version, "plasticoRetenido"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".reason.option.datosMalGrabados', 'es', 'reason', '" + idForm + "'," + version  + ", TO_DATE('2017-03-31 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Datos mal grabados')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".reason.option.deterioroPlasticoActual', 'es', 'reason', '" + idForm + "'," + version  + ", TO_DATE('2017-03-31 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Deterioro de plástico actual')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".reason.option.plasticoPorRenovacionNoRecibida', 'es', 'reason', '" + idForm + "'," + version  + ", TO_DATE('2017-03-31 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Plástico por renovación no recibida')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".reason.option.plasticoRetenido', 'es', 'reason', '" + idForm + "'," + version  + ", TO_DATE('2017-03-31 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Plástico retenido')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".reason.option.datosMalGrabados", "es", "reason", idForm, version, date, "Datos mal grabados"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".reason.option.deterioroPlasticoActual", "es", "reason", idForm, version, date, "Deterioro de plástico actual"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".reason.option.plasticoPorRenovacionNoRecibida", "es", "reason", idForm, version, date, "Plástico por renovación no recibida"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".reason.option.plasticoRetenido", "es", "reason", idForm, version, date, "Plástico retenido"});
        }
    }

}
