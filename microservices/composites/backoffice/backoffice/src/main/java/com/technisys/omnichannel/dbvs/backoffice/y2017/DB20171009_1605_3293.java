/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3293
 *
 * @author rdosantos
 */
public class DB20171009_1605_3293 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_fields", new String[]{"sub_type"}, new String[]{"foreigners.intermediary"}, "id_field = 'intermediaryBank' AND id_form = 'transferForeign'");
    }

}
