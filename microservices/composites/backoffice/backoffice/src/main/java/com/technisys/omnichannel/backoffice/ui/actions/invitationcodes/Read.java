/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ReadRequest;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.ReadResponse;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.opensymphony.xwork2.ActionSupport;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 * @author Sebastian Barbosa
 */
@Action(results = {
    @Result(location = "/invitationcodes/_detail.jsp")
})
public class Read extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ReadRequest request = new ReadRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.invitationCodes.read");

        request.setIdInvitationCode(idCode);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);
        if (!ReturnCodes.OK.equals(response.getReturnCode())) {
            throw new JSONException(response);
        }

        ReadResponse readResponse = (ReadResponse) response;
        invitationCode = readResponse.getInvitationCode();
        groupList = readResponse.getGroupList();

        return SUCCESS;
    }
    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private int idCode;
    private int currentPage;
    private int rowsPerPage;
    private int totalRows;
    private int position;

    public void setIdCode(int idCode) {
        this.idCode = idCode;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public void setPosition(int position) {
        this.position = position;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private InvitationCode invitationCode;
    private java.util.List<Group> groupList;

    public InvitationCode getInvitationCode() {
        return invitationCode;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public int getIdCode() {
        return idCode;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public int getPosition() {
        return position;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
