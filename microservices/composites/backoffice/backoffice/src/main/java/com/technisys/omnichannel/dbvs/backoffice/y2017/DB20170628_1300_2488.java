/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2488
 * 
 * @author mbustos
 *
 */
public class DB20170628_1300_2488 extends DBVSUpdate {

    @Override
    public void up() {

        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"))
                .format(new Date());
        String[] formFieldsValues;
        String idForm = "requestLoan";
        String version = "1";
        Map<String, String> messages = new HashMap<String, String>();
        /**** Insert Form - Solifitud de Prestamo ****/
        String[] formColumn = new String[] { "id_form", "version", "enabled",
                "category", "type", "admin_option", "id_activity", "last",
                "deleted", "schedulable", "templates_enabled",
                "drafts_enabled", "editable_in_mobile", "editable_in_narrow",
                "id_bpm_process" };
        String[] formValues = new String[] { idForm, version, "1", "loans",
                "process", "payments", null, "1", "0", "1", "1", "1", "1", "1",
                "demo:1:3" };
        insert("forms", formColumn, formValues);
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(
                    DBVS.DIALECT_ORACLE,
                    "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                            + " VALUES ('forms."
                            + idForm
                            + ".formName', '"
                            + idForm
                            + "', '"
                            + version
                            + "', 'es', 'Solicitud de Préstamo', TO_DATE('2017-04-03 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[] { "id_message",
                    "id_form", "version", "lang", "value", "modification_date" };
            insert("form_messages", formMessageFields, new String[] {
                    "forms." + idForm + ".formName", idForm, version, "es",
                    "Solicitud de Préstamo", date });
        }
        insert("permissions", new String[] { "id_permission" },
                new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[] {
                "id_permission", "id_credential_group" }, new String[] {
                "client.form." + idForm + ".send", "accessToken-pin" });
        /**
         * ** Form Fields ***
         */
        String[] formFields = new String[] { "id_field", "id_form",
                "form_version", "type", "ordinal", "visible", "required",
                "note", "visible_in_mobile", "read_only", "sub_type",
                "ticket_only" };
        /* amountOfFees */
        String idField = "amountOfFees";
        formFieldsValues = new String[] { idField, idForm, version, "selector",
                "4", "TRUE", "TRUE", "", "1", "0", "default", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form",
                "form_version", "display_type", "default_value",
                "show_blank_option", "render_as" }, new String[] { idField,
                idForm, version, "field-smaller", "", "1", "combo" });
        String messageIdLabel = idField + ".label";
        String messageIdOption = idField + ".option";
        messages.put(messageIdLabel, "Cantidad de Cuotas");
        messages.put(messageIdOption + ".12", "12");
        messages.put(messageIdOption + ".3", "3");
        messages.put(messageIdOption + ".6", "6");
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "12" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "3" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "6" });
        /* amountToRequest */
        idField = "amountToRequest";
        formFieldsValues = new String[] { idField, idForm, version, "amount",
                "3", "TRUE", "TRUE", "", "1", "0", "default", "0" };
        messageIdLabel = idField + ".label";
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[] { "id_field", "id_form",
                "form_version", "display_type", "id_ps_currency_check",
                "control_limits", "control_limit_over_product",
                "use_for_total_amount" }, new String[] { idField, idForm,
                version, "field-small", "", "0", "", "0" });
        messages.put(messageIdLabel, "Monto a Solicitar");
        /* fundsDestination */
        idField = "fundsDestination";
        formFieldsValues = new String[] { idField, idForm, version, "selector",
                "6", "TRUE", "FALSE", "", "1", "0", "default", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form",
                "form_version", "display_type", "default_value",
                "show_blank_option", "render_as" }, new String[] { idField,
                idForm, version, "field-normal", "", "0", "combo" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "0" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "2" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "3" });
        messageIdLabel = idField + ".label";
        messageIdOption = idField + ".option";
        messages.put(messageIdLabel, "Destino de fondos");
        messages.put(messageIdOption + ".0", "Compra de mercaderia");
        messages.put(messageIdOption + ".1", "Giro al exterior");
        messages.put(messageIdOption + ".2", "Compra de maquinaria");
        messages.put(messageIdOption + ".3", "Otros");
        /* iAuthorizeDebitToAccount */
        idField = "iAuthorizeDebitToAccount";
        formFieldsValues = new String[] { idField, idForm, version,
                "productselector", "5", "TRUE", "FALSE", "", "1", "0",
                "default", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_ps", new String[] { "id_field", "id_form",
                "form_version", "display_type", "id_ps_currency_check",
                "show_other_option", "show_other_permission",
                "show_other_id_ps", "show_other_pfnovc", "filter_actives",
                "show_other_option_in_mobile", "show_other_by" }, new String[] {
                idField, idForm, version, "field-normal", "", "0", "", "", "",
                "0", "0", "" });
        insert("form_field_ps_product_types", new String[] { "id_field",
                "id_form", "form_version", "id_product_type" }, new String[] {
                idField, idForm, version, "CA" });
        insert("form_field_ps_product_types", new String[] { "id_field",
                "id_form", "form_version", "id_product_type" }, new String[] {
                idField, idForm, version, "CC" });
        insert("form_field_ps_permissions", new String[] { "id_field",
                "id_form", "form_version", "id_permission" }, new String[] {
                idField, idForm, version, "pay.loan" });
        insert("form_field_ps_permissions", new String[] { "id_field",
                "id_form", "form_version", "id_permission" }, new String[] {
                idField, idForm, version, "product.read" });
        messageIdLabel = idField + ".label";
        messages.put(messageIdLabel, "Autorizo a debitar en la cuenta");
        /* loanType */
        idField = "loanType";
        formFieldsValues = new String[] { idField, idForm, version, "selector",
                "2", "TRUE", "TRUE", "", "1", "0", "default", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form",
                "form_version", "display_type", "default_value",
                "show_blank_option", "render_as" }, new String[] { idField,
                idForm, version, "field-small", "", "1", "combo" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "0" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "2" });
        insert("form_field_selector_options", new String[] { "id_field",
                "id_form", "form_version", "value" }, new String[] { idField,
                idForm, version, "3" });
        messageIdLabel = idField + ".label";
        messageIdOption = idField + ".option";
        messages.put(messageIdLabel, "Tipo de préstamo");
        messages.put(messageIdOption + ".0", "Amortizable");
        messages.put(messageIdOption + ".1", "Hipotecario");
        messages.put(messageIdOption + ".2", "Plazo fijo");
        messages.put(messageIdOption + ".3", "Automotor");
        /* termsAndCondLoan */
        idField = "termsAndCondLoan";
        formFieldsValues = new String[] { idField, idForm, version,
                "termsandconditions", "9", "TRUE", "TRUE",
                "Agregar terminos y condiciones", "1", "0", "default", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field",
                "id_form", "form_version", "display_type",
                "show_accept_option", "show_label" }, new String[] {
                "termsAndCondLoan", idForm, version, "field-medium", "1", "1" });
        messageIdLabel = idField + ".label";
        String messageIdAceptOptionText = idField + ".showAcceptOptionText";
        String messageIdTermsAndConditions = idField + ".termsAndConditions";
        messages.put(messageIdLabel, "Términos y Condiciones");
        messages.put(messageIdAceptOptionText, "Aceptar");
        messages.put(
                messageIdTermsAndConditions,
                "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        /* LineaSeparadora */
        idField = "lineaSeparadora";
        messageIdLabel = idField + ".label";
        formFieldsValues = new String[] { idField, idForm, version,
                "horizontalrule", "7", "TRUE", "FALSE", "", "1", "0",
                "default", "0" };
        insert("form_fields", formFields, formFieldsValues);
        messages.put(messageIdLabel, " ");
        String[] formFieldsMessages = new String[] { "id_message", "lang",
                "id_field", "id_form", "form_version", "value",
                "modification_date" };
        for (String key : messages.keySet()) {
            formFieldsValues = new String[] { "fields." + idForm + "." + key,
                    "es", key.substring(0, key.indexOf(".")), idForm, version,
                    messages.get(key), date };
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(
                        DBVS.DIALECT_ORACLE,
                        "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                                + " VALUES ('"
                                + formFieldsValues[0]
                                + "', '"
                                + formFieldsValues[1]
                                + "', '"
                                + formFieldsValues[2]
                                + "', '"
                                + formFieldsValues[3]
                                + "', '"
                                + formFieldsValues[4]
                                + "','"
                                + formFieldsValues[5]
                                + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages,
                        formFieldsValues);
            }
        }
    }
}
