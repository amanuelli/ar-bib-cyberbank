/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Diego Curbelo
 */
public class DB201112231640_11539 extends DBVSUpdate {

    @Override
    public void up() {

        update("configuration", new String[]{"value"}, new String[]{"-34.908333,-56.151389#Pocitos|-34.905678,-56.208243#Ciudad Vieja|-34.905537,-56.186872#Cordon"},
                "id_field='rubicon.widgets.geolocalization.areas'");

        update("configuration", new String[]{"value"}, new String[]{"-34.908539,-56.159733#Cajero 1|-34.910826,-56.162737#Cajero 2|-34.909092,-56.199274#Cajero 3|-34.908986,-56.210131#Cajero 4|-34.904692,-56.202707#Cajero 5|-34.911484,-56.194468#Cajero 6|-34.909584,-56.180778#Cajero 7|-34.901102,-56.184426#Cajero 8|-34.904023,-56.189060#Cajero 9"},
                "id_field='rubicon.widgets.geolocalization.atmList'");

        update("configuration", new String[]{"value"}, new String[]{"-34.909221,-56.153725#Sucursal Pocitos|-34.906687,-56.163853#Sucursal Parque Rodo|-34.905291,-56.206397#Casa Central|-34.905853,-56.195069#Sucursal Entrevero|-34.905361,-56.184383#Sucursal Gaucho"},
                "id_field='rubicon.widgets.geolocalization.offices'");

        update("configuration", new String[]{"value"}, new String[]{"-34.909245,-56.147723#Pizeria Trouville|-34.907684,-56.206226#Porto Vanila|-34.905290,-56.181550#Mosca|-34.909162,-56.188588#Cordon"},
                "id_field='rubicon.widgets.geolocalization.promotions'");
    }
}