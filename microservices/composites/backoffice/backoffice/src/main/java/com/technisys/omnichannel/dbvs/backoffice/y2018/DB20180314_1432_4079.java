/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

public class DB20180314_1432_4079 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("preferences.changepin.preview");
        deleteActivity("preferences.changepin.send");
        delete("permissions", "id_permission='user.preferences.pin'");
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"user.preferences.withSecondFactor"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"user.preferences.withSecondFactor", "accessToken-pin"});
        
        //accessToken-pin
        String[] keys = new String[]{"id_permission"};
        String[] values = new String[]{"user.preferences.withSecondFactor"};
        
        update("activity_products", keys, values, "id_activity in ('preferences.securityseals.modify', 'preferences.userData.mail.sendCode', 'preferences.userData.mobilePhone.sendCode')");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions (id_group, id_permission, target) SELECT id_group, 'user.preferences.withSecondFactor', target FROM group_permissions WHERE id_permission='user.preferences'");
    }
}
