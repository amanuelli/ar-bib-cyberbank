/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2199
 *
 * @author sbarbosa
 */
public class DB20170523_1019_2199 extends DBVSUpdate {

    @Override
    public void up() {
        // Fix permiso de pago de tarjetas
        delete("permissions_credentials_groups", "id_permission in ('pay.creditCard', 'pay.creditCard.thirdParties')");
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"pay.creditCard", "accessToken-pin"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"pay.creditCard.thirdParties", "accessToken-pin"});

        
        delete("permissions", "id_permission='transfer.foreign'");
        insert("permissions", new String[]{"id_permission"}, new String[]{"transfer.foreign"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"transfer.foreign", "accessToken-pin"});

        deleteActivity("transfers.foreign.preview");
        deleteActivity("transfers.foreign.send");

        insertActivity("transfers.foreign.preview", "com.technisys.omnichannel.activities.forms.PreviewFormActivity", "transferForeign", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("transfers.foreign.send", "com.technisys.omnichannel.activities.forms.SendFormActivity", "transferForeign", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, "transfer.foreign", "debitAccount");

        updateConfiguration("core.permissionsForProducts", "product.read|transfer.internal|transfer.thirdParties|transfer.foreign|pay.loan|pay.loan.thirdParties|pay.creditCard|pay.creditCard.thirdParties|accounts.requestCheckbook");

        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"transfer.foreign", "transfers", null, null, "0", "20", "transfers", null, null, "0", "20", "transfers", null, null, "1", "20", "CA,CC", null, null});

        update("adm_ui_permissions", new String[]{"simple_ordinal", "medium_ordinal", "advanced_ordinal"}, new String[]{"13", "13", "13"}, "id_permission='transfer.thirdParties'");

        // Form (Transferencia exterior)
        String idForm = "transferForeign";
        String version = "1";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        delete("forms", "id_form='" + idForm + "' and version=" + version);

        insert("forms", new String[]{"id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow"},
                new String[]{idForm, version, "1", "transfers", "activity", "demo:1:3", null, "transfers.foreign.send", "1", "1", "1", "1", "1"});

        //Campos
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};

        int ordinal = 1;
        Map<String, String> messages = new HashMap();

        // Cuenta débito (debitAccount)
        insert("form_fields", formFields, new String[]{"debitAccount", idForm, version, "productselector", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option", "show_other_permission"},
                new String[]{"debitAccount", idForm, version, "field-normal", "0", null});

        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"debitAccount", idForm, version, "CC"});
        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"debitAccount", idForm, version, "CA"});

        insert("form_field_ps_permissions", new String[]{"id_field", "id_form", "form_version", "id_permission"},
                new String[]{"debitAccount", idForm, version, "transfer.foreign"});

        messages.put("debitAccount.label", "Cuenta débito");
        messages.put("debitAccount.requiredError", "Debe seleccionar una cuenta débito");
        messages.put("debitAccount.hint", "Todas las comisiones se debitarán de la cuenta seleccionada, si lo desea puede cargar los gastos a otra cuenta");

        // Cuenta débito de gastos  (expenseAccount)
        insert("form_fields", formFields, new String[]{"expenseAccount", idForm, version, "productselector", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option", "show_other_permission"},
                new String[]{"expenseAccount", idForm, version, "field-normal", "0", null});

        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"expenseAccount", idForm, version, "CC"});
        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"expenseAccount", idForm, version, "CA"});

        insert("form_field_ps_permissions", new String[]{"id_field", "id_form", "form_version", "id_permission"},
                new String[]{"expenseAccount", idForm, version, "transfer.internal"});

        messages.put("expenseAccount.label", "Cuenta débito de gastos");

        // Monto a pagar (amount)
        insert("form_fields", formFields, new String[]{"amount", idForm, version, "amount", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"},
                new String[]{"amount", idForm, version, "field-normal", "1"});

        messages.put("amount.help", "Se realizarán los cambios de moneda necesarios en función de las monedas de las cuentas de débito y la moneda de la transferencia.");
        messages.put("amount.label", "Importe");
        messages.put("amount.requiredError", "Debe ingresar un importe");

        // Tipo de gastos (typeOfExpenses)
        insert("form_fields", formFields, new String[]{"typeOfExpenses", idForm, version, "selector", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"},
                new String[]{"typeOfExpenses", idForm, version, "field-normal", null, "0", "combo"});

        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"typeOfExpenses", idForm, version, "BEN"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"typeOfExpenses", idForm, version, "OUR"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"typeOfExpenses", idForm, version, "SHA"});

        messages.put("typeOfExpenses.label", "Tipo de gastos");
        messages.put("typeOfExpenses.requiredError", "Debe ingresar el tipo de gasto");

        messages.put("typeOfExpenses.option.BEN", "BEN");
        messages.put("typeOfExpenses.option.OUR", "OUR");
        messages.put("typeOfExpenses.option.SHA", "SHA");

        // Referencia de débito (debitReference)
        insert("form_fields", formFields, new String[]{"debitReference", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"debitReference", idForm, version, "0", "20", "field-normal", null});

        messages.put("debitReference.help", "Texto que le ayude a identificar su transferencia.");
        messages.put("debitReference.hint", null);
        messages.put("debitReference.label", "Referencia de débito");
        messages.put("debitReference.requiredError", "Debe ingresar un número de tarjeta");

        // Datos del crédito (sectionCreditData)
        insert("form_fields", formFields, new String[]{"sectionCreditData", idForm, version, "sectiontitle", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});

        messages.put("sectionCreditData.label", "Datos del crédito");

        // Banco credito (creditBank)
        insert("form_fields", formFields, new String[]{"creditBank", idForm, version, "bankselector", String.valueOf(ordinal++), "TRUE", "TRUE", "foreigners", "1"});
        insert("form_field_bankselector", new String[]{"id_field", "id_form", "form_version", "display_type"},
                new String[]{"creditBank", idForm, version, "field-normal"});

        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"creditBank", idForm, version, "ABA"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"creditBank", idForm, version, "BLZ"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"creditBank", idForm, version, "CHIPS"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"creditBank", idForm, version, "SWIFT"});

        messages.put("creditBank.requiredError", "Debe seleccionar un banco");
        messages.put("creditBank.help", null);
        messages.put("creditBank.hint", null);
        messages.put("creditBank.label", "Banco de crédito");

        // Cuenta crédito (creditAccountNumber)
        insert("form_fields", formFields, new String[]{"creditAccountNumbe", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"creditAccountNumbe", idForm, version, "0", "34", "field-normal", null});

        messages.put("creditAccountNumbe.help", "Ingrese el número de cuenta del beneficiario (o IBAN para Europa Turquía y Arabia)");
        messages.put("creditAccountNumbe.hint", null);
        messages.put("creditAccountNumbe.label", "Cuenta crédito");
        messages.put("creditAccountNumbe.requiredError", "Debe ingresar un número de cuenta");

        // Nombre del beneficiario (beneficiaryName)
        insert("form_fields", formFields, new String[]{"beneficiaryName", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"beneficiaryName", idForm, version, "0", "35", "field-normal", null});

        messages.put("beneficiaryName.help", "Si el espacio disponible no es suficiente para completar el nombre, puede continuar en el campo dirección.");
        messages.put("beneficiaryName.hint", null);
        messages.put("beneficiaryName.label", "Nombre del beneficiario");
        messages.put("beneficiaryName.requiredError", "Debe ingresar el nombre del beneficiario");

        // Dirección del beneficiario (beneficiaryAddress)
        insert("form_fields", formFields, new String[]{"beneficiaryAddress", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"beneficiaryAddress", idForm, version, "0", "35", "field-normal", null});

        messages.put("beneficiaryAddress.label", "Dirección del beneficiario");
        messages.put("beneficiaryAddress.requiredError", "Debe ingresar la dirección del beneficiario");

        // Información para el beneficiario (beneficiaryInfo1)
        insert("form_fields", formFields, new String[]{"beneficiaryInfo1", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"beneficiaryInfo1", idForm, version, "0", "30", "field-normal", null});

        messages.put("beneficiaryInfo1.label", "Información para el beneficiario");

        // Información para el beneficiario (beneficiaryInfo2)
        insert("form_fields", formFields, new String[]{"beneficiaryInfo2", idForm, version, "text", String.valueOf(ordinal++), "hasValue(beneficiaryInfo1)", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"beneficiaryInfo2", idForm, version, "0", "30", "field-normal", null});

        messages.put("beneficiaryInfo2.label", "&nbsp;");

        // Información para el beneficiario (beneficiaryInfo3)
        insert("form_fields", formFields, new String[]{"beneficiaryInfo3", idForm, version, "text", String.valueOf(ordinal++), "hasValue(beneficiaryInfo1)", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"beneficiaryInfo3", idForm, version, "0", "35", "field-normal", null});

        messages.put("beneficiaryInfo3.label", "&nbsp;");

        // Información para el beneficiario (beneficiaryInfo4)
        insert("form_fields", formFields, new String[]{"beneficiaryInfo4", idForm, version, "text", String.valueOf(ordinal++), "hasValue(beneficiaryInfo1)", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"beneficiaryInfo4", idForm, version, "0", "35", "field-normal", null});

        messages.put("beneficiaryInfo4.label", "&nbsp;");

        // Banco intermediario (sectionIntermediaryBank)
        insert("form_fields", formFields, new String[]{"sectionIntermediaryBank", idForm, version, "sectiontitle", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});

        messages.put("sectionIntermediaryBank.label", "Banco intermediario");

        // Banco intermediario (intermediaryBank)
        insert("form_fields", formFields, new String[]{"intermediaryBank", idForm, version, "bankselector", String.valueOf(ordinal++), "TRUE", "FALSE", "foreigners", "1"});
        insert("form_field_bankselector", new String[]{"id_field", "id_form", "form_version", "display_type"},
                new String[]{"intermediaryBank", idForm, version, "field-normal"});

        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"intermediaryBank", idForm, version, "ABA"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"intermediaryBank", idForm, version, "BLZ"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"intermediaryBank", idForm, version, "CHIPS"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"intermediaryBank", idForm, version, "SWIFT"});
        
        messages.put("intermediaryBank.requiredError", "Debe seleccionar un banco");
        messages.put("intermediaryBank.help", null);
        messages.put("intermediaryBank.hint", null);
        messages.put("intermediaryBank.label", "Banco intermediario");

        // Cuenta del banco beneficiario en el intermediario (intermediaryAccountNumber)
        insert("form_fields", formFields, new String[]{"intermediaryAccountNumber", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "hasValue(intermediaryBank)", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"intermediaryAccountNumber", idForm, version, "0", "30", "field-normal", null});

        messages.put("intermediaryAccountNumber.hint", null);
        messages.put("intermediaryAccountNumber.label", "Cuenta del banco beneficiario en el intermediario");
        messages.put("intermediaryAccountNumber.requiredError", "Debe ingresar un número de cuenta");

        //Insertado de Mensajes
        //*********************************************************************
        String[] formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        String[] formFieldsValues = new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Transferencia exterior", date};

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form,version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "', TO_DATE('"+formFieldsValues[5]+"', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formField, formFieldsValues);
        }

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, date, messages.get(key)};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date,value) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "', TO_DATE('"+formFieldsValues[5]+"', 'YYYY-MM-DD HH24:MI:SS'),'" + formFieldsValues[6] + "')");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

    }
}
