/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pbanales
 */

public class DB20190417_1430_7073 extends DBVSUpdate {

    @Override
    public void up() {
        Map<String, String> messages = new HashMap();
        messages.put("amount.label","Quantidade");
        messages.put("amount.requiredError","Você deve inserir um valor");
        messages.put("bankSelector.label","Banco local");
        messages.put("bankSelector.option.bandes","BANDES");
        messages.put("bankSelector.option.bbva","BBVA");
        messages.put("bankSelector.option.brou","BROU");
        messages.put("bankSelector.option.hsbc","HSBC");
        messages.put("bankSelector.option.santander","SANTANDER");
        messages.put("bankSelector.option.scotiabank","SCOTIABANK");
        messages.put("bankSelector.requiredError", "Você deve selecionar um banco");
        messages.put("creditAccountName.label", "Nome do destinatário");
        messages.put("creditAccountName.requiredError", "Você deve inserir uma conta de destino");
        messages.put("creditAccountNumber.label", "Conta de destino");
        messages.put("creditAccountNumber.requiredError", "Você deve inserir uma conta de destino");
        messages.put("creditReference.label", "Referência");
        messages.put("debitAccount.label", "Conta de origem");
        messages.put("document.label", "Documento do destinatário");
        messages.put("document.requiredError", "Você deve digitar um documento");
        messages.put("notificationBody.label", "Mensagem");
        messages.put("notificationEmails.label", "Endereço de email de notificação");


        String[] formFieldsValues;
        String[] formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.transferLocal." + key, "pt", key.substring(0, key.indexOf(".")), "transferLocal", "1", messages.get(key), "2019-04-17 12:00:00"};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2]  + "', '"
                        + formFieldsValues[3]  + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2019-04-17 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }

}
