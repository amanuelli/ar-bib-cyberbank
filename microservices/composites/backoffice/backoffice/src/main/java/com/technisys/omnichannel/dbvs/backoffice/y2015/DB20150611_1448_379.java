/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author aelters
 */
public class DB20150611_1448_379 extends DBVSUpdate {

    @Override
    public void up() {
       insertActivity("deposits.read", "com.technisys.omnichannel.client.activities.deposits.ReadDepositActivity", "deposits", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idDeposit");
       insertActivity("deposits.listStatements", "com.technisys.omnichannel.client.activities.deposits.ListStatementsActivity", "deposits", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idDeposit");
       insertActivity("deposits.listStatementDetails", "com.technisys.omnichannel.client.activities.deposits.ListStatementDetailsActivity", "deposits", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idDeposit");
       insertOrUpdateConfiguration("deposits.statementsPerPage", "10", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});

    }
}
