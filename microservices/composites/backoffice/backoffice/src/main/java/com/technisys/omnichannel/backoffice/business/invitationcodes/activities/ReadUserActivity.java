/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.activities;

import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ReadUserRequest;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.ReadUserResponse;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.documenttypes.DocumentTypesHandler;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sebastian Barbosa
 */
public class ReadUserActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        ReadUserResponse response = new ReadUserResponse(request);

        try {
            ReadUserRequest auxRequest = (ReadUserRequest) request;

            ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber(), null);
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber());

            response.setClientUser(client);
            response.setUser(user);

            // Cuentas del usuario
            List<ClientEnvironment> accounts = RubiconCoreConnectorC.listClients(request.getIdTransaction(), auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber());
            List<String> clients = new ArrayList<>();
            for (ClientEnvironment c : accounts) {
                clients.add(c.getProductGroupId());
            }
            response.setAccountList(clients);

            String selectedAccount = accounts.get(0).getProductGroupId();
            ClientEnvironment clientEnv = RubiconCoreConnectorC.readClient(request.getIdTransaction(), selectedAccount);
            response.setSelectedClientEnvironment(clientEnv);

            Environment environment = Administration.getInstance().readEnvironmentByProductGroupId(selectedAccount);
            if (environment != null) {
                response.setSelectedEnvironment(Administration.getInstance().readEnvironment(environment.getIdEnvironment()));
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            ReadUserRequest auxRequest = (ReadUserRequest) request;

            if (StringUtils.isBlank(auxRequest.getDocumentNumber())) {
                result.put("documentNumber", "activities.backoffice.invitationCodes.documentNumberEmpty");
            } else if (auxRequest.getDocumentNumber().length() > 25) {
                result.put("documentNumber", "activities.backoffice.invitationCodes.documentNumberMaxLengthExceeded");
            }

            List<String> validCountryCodes = CountryCodesHandler.listCountryCodes();
            if (StringUtils.isBlank(auxRequest.getDocumentCountry())) {
                result.put("documentCountry", "activities.backoffice.invitationCodes.documentCountryEmpty");
            } else if (!validCountryCodes.contains(auxRequest.getDocumentCountry())) {
                result.put("documentCountry", "activities.backoffice.invitationCodes.documentCountryInvalid");
            }

            if (StringUtils.isBlank(auxRequest.getDocumentType())) {
                result.put("documentType", "activities.backoffice.invitationCodes.documentTypeEmpty");
            } else if (result.get("documentCountry") == null) {
                List<String> validDocumentTypes = DocumentTypesHandler.listDocumentTypes(auxRequest.getDocumentCountry());
                List<String> invalidDocumentTypes = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "backoffice.invitationCodes.invalidDocumentTypes");
                if (!validDocumentTypes.contains(auxRequest.getDocumentType())
                        || invalidDocumentTypes.contains(auxRequest.getDocumentType())) {
                    result.put("documentType", "activities.backoffice.invitationCodes.documentTypeInvalid");
                }
            }

            if (result.isEmpty()) {
                ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber(), null);
                if (client == null) {
                    result.put("documentNumber", "activities.backoffice.invitationCodes.clientNotInBackend");
                } else {
                    List<ClientEnvironment> accounts = RubiconCoreConnectorC.listClients(request.getIdTransaction(), auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber());
                    if (accounts == null || accounts.isEmpty()) {
                        result.put("documentNumber", "activities.backoffice.invitationCodes.clientDontHaveAccounts");
                    }
                }
            }
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }
}
