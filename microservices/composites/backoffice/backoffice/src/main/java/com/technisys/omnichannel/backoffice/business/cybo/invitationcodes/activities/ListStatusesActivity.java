/* 
 * Copyright 2021 Technisys.
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities;

import java.io.IOException;
import java.util.List;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.cybo.Response;
import com.technisys.omnichannel.core.domain.InvitationCodesStatus;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;

/**
 * 
 * @author Mauricio Uribe
 *
 */
public class ListStatusesActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        Response<List<InvitationCodesStatus>> response = new Response<>(request);
        try {
            List<InvitationCodesStatus> statusList = InvitationCodesHandler.listInvitationCodesStatus();
            response.setData(statusList);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
