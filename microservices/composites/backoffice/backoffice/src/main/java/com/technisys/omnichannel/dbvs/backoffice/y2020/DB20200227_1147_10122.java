/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author emordezki
 */

public class DB20200227_1147_10122 extends DBVSUpdate {

    @Override
    public void up() {
        update("activities", new String[] { "id_activity", "component_fqn" }, new String[] { "session.login.legacy.step1", "com.technisys.omnichannel.client.activities.session.legacy.LoginWithSecondFactorAndPasswordStep1Activity" }, "id_activity = 'session.login.step1'");
        update("activities", new String[] { "id_activity", "component_fqn" }, new String[] { "session.login.legacy.step2", "com.technisys.omnichannel.client.activities.session.legacy.LoginWithSecondFactorAndPasswordStep2Activity" }, "id_activity = 'session.login.step2'");
        update("activities", new String[] { "id_activity", "component_fqn" }, new String[] { "session.login.legacy.step3", "com.technisys.omnichannel.client.activities.session.legacy.SelectEnvironmentActivity" }, "id_activity = 'session.login.step3'");
        update("activities", new String[] { "id_activity", "component_fqn" }, new String[] { "session.login.legacy.step4", "com.technisys.omnichannel.client.activities.session.legacy.AcceptGeneralConditionsActivity" }, "id_activity = 'session.login.step4'");
        update("activities", new String[] { "id_activity", "component_fqn" }, new String[] { "session.login.legacy.registerUserDevice", "com.technisys.omnichannel.client.activities.session.legacy.RegisterUserDeviceActivity" }, "id_activity = 'session.registerUserDevice'");

        insertActivity("session.login.oauth.step1", "com.technisys.omnichannel.client.activities.session.oauth.OauthLoginWithSecondFactorAndPasswordStep1Activity", "login", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);
        update("activities", new String[] { "id_activity", "component_fqn" }, new String[] { "session.login.oauth.step2bapidocs", "com.technisys.omnichannel.client.activities.session.oauth.OAuthActivity" }, "id_activity = 'session.login.oauth'");
        update("activities", new String[] { "id_activity", "component_fqn" }, new String[] { "session.login.oauth.step2b", "com.technisys.omnichannel.client.activities.session.oauth.ListEnvironmentActivity" }, "id_activity = 'session.login.step2b'");
        update("activities", new String[] { "id_activity", "component_fqn" }, new String[] { "session.login.oauth.step3", "com.technisys.omnichannel.client.activities.session.oauth.OauthSelectEnvironmentActivity" }, "id_activity = 'session.login.step3Oauth'");
        insertActivity("session.login.oauth.step4", "com.technisys.omnichannel.client.activities.session.oauth.OauthAcceptGeneralConditionsActivity", "login", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);
        update("activities", new String[] { "id_activity", "component_fqn" }, new String[] { "session.login.oauth.registerUserDevice", "com.technisys.omnichannel.client.activities.session.oauth.OauthRegisterUserDeviceActivity" }, "id_activity = 'session.registerUserDeviceOauth'");
        update("activities", new String[] { "component_fqn" }, new String[] { "com.technisys.omnichannel.client.activities.session.oauth.ValidateOauthPasswordActivity" }, "id_activity = 'oauth.password.validate'");
    }

}