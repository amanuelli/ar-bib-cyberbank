/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests;

import com.technisys.omnichannel.core.domain.TransactionData;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;

/**
 *
 * @author azeballos
 */
public class CancelInvitationCodeData extends TransactionData {
    private int idCode;

    private String documentCountry;
    private String documentType;
    private String documentNumber;

    private String name;
    private String account;
    private String accountName;

    @Override
    public String calculateAdditionalInfoToHash() {
        StringBuilder additionalInfo = new StringBuilder(super.calculateAdditionalInfoToHash());

        additionalInfo.append("-");
        additionalInfo.append(idCode);

        return additionalInfo.toString();
    }

    @Override
    public String getIdentifier() {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append(documentNumber)
                .append(" (").append(i18n.getMessage("documentType.label." + documentType, getLang()))
                .append(", ").append(i18n.getMessage("country.name." + documentCountry, getLang())).append(")");

        return strBuilder.toString();
    }
    
    @Override
    public String getShortDescription(String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<h5>Acci&oacute;n: <a href=\"javascript:showPendingAction('").append(idTransaction).append("')\"> ").append(activityName).append("</a></h5>");
        strBuilder.append("<p class='element_details'>");
        strBuilder.append("      ").append(i18n.getMessage("backoffice.invitationCodes.cancel.document", getLang())).append(": <em>")
                .append(documentNumber)
                .append(" (").append(i18n.getMessage("documentType.label." + documentType, getLang()))
                .append(", ").append(i18n.getMessage("country.name." + documentCountry, getLang())).append(")")
                .append("</em><br/>");
        strBuilder.append("      ").append(i18n.getMessage("backoffice.invitationCodes.cancel.comments", getLang())).append(": <em>")
                .append(this.getComment()).append("</em><br/>");

        return strBuilder.toString();
    }

    @Override
    public String getFullDescription(String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<dl class = 'element_details clearfix'>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.cancel.name", getLang())).append(":</dt>")
                .append("<dd>").append(name).append("</dd>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.cancel.document", getLang())).append(":</dt>")
                .append("<dd>")
                .append(documentNumber)
                .append(" (").append(i18n.getMessage("documentType.label." + documentType, getLang()))
                .append(", ").append(i18n.getMessage("country.name." + documentCountry, getLang())).append(")")
                .append("</dd>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.cancel.account", getLang())).append(":</dt>")
                .append("<dd>").append(account).append(" - ").append(accountName).append("</dd>");
        strBuilder.append("</dl>");

        return strBuilder.toString();
    }

    public int getIdCode() {
        return idCode;
    }

    public void setIdCode(int idCode) {
        this.idCode = idCode;
    }

    public String getDocumentCountry() {
        return documentCountry;
    }

    public void setDocumentCountry(String documentCountry) {
        this.documentCountry = documentCountry;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }
    

    public String getAccountName() {
		return accountName;
	}

	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}

	@Override
    public String toString() {
        return "CancelInvitationCodeData{" + "idCode=" + idCode + ", documentCountry=" + documentCountry + ", documentType=" + documentType+ ", documentNumber=" + documentNumber + ", name=" + name + ", account=" + account + '}';
    }

}
