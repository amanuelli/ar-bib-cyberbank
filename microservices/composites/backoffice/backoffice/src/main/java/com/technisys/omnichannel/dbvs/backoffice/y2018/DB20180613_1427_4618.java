/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4618
 *
 * @author dimoda
 */
public class DB20180613_1427_4618 extends DBVSUpdate {

    @Override
    public void up() {
        update("activities", new String[]{"id_activity"}, new String[]{"session.login.step1"}, "id_activity = 'session.loginWithSecondFactorAndPassword.step1'");
        update("activities", new String[]{"id_activity"}, new String[]{"session.login.step2"}, "id_activity = 'session.loginWithSecondFactorAndPassword.step2'");
        update("activities", new String[]{"id_activity"}, new String[]{"session.login.step3"}, "id_activity = 'session.selectEnvironment'");
        update("activities", new String[]{"id_activity"}, new String[]{"session.login.step4"}, "id_activity = 'session.acceptGeneralConditions'");
    
        updateConfiguration("core.attempts.features", "session.login.step1=login|session.login.step2=login|session.recoverPassword.step1=login|enrollment.associate.verifyStep1=login|enrollment.associate.verifyStep2=login|session.recoverPinAndPassword.step2=recoverCredential|session.incrementCaptchaAttempts=login|session.recoverPin.step1=login");  
    }

}