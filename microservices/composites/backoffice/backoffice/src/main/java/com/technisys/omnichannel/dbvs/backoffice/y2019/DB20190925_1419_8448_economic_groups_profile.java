/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author pbanales
 */

public class DB20190925_1419_8448_economic_groups_profile extends DBVSUpdate {

    @Override
    public void up() {
        String[] dialects = new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL};

        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT profiles ON");
        String sentence = "INSERT INTO profiles (id_profile, name, created_at, modified_at, description) SELECT CASE WHEN MAX(id_profile) is NULL THEN 1 ELSE MAX(id_profile)+1 END, 'Economic Groups frontend limitations', " +
                nowDialectDate() + ", " + nowDialectDate() + ", 'Limit the actions just to read only' FROM profiles";
        customSentence(dialects, sentence);
        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT profiles OFF");

        sentence = "INSERT INTO profile_environment_types (id_profile,id_environment_type) VALUES ( (SELECT id_profile FROM profiles WHERE name = 'Economic Groups frontend limitations'), 'corporateGroup')";
        customSentence(dialects, sentence);

        String[] permissions = new String[]{"core.authenticated",
                "core.forms.send",
                "core.listTransactions",
                "core.loginWithOTP",
                "core.loginWithPassword",
                "core.loginWithPIN",
                "core.readForm",
                "core.readTransaction",
                "position",
                "product.read",
                "user.preferences",
                "user.preferences.withSecondFactor"};

        for (String permission: permissions) {
            sentence = "INSERT INTO profile_permissions (id_profile, id_permission) " +
                    "SELECT id_profile, '"+ permission +"' FROM profiles WHERE name = 'Economic Groups frontend limitations'";
            customSentence(dialects, sentence);
        }
    }
}
