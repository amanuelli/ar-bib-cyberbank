/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.domain.cybo.holdings;

import com.technisys.omnichannel.annotations.docs.DocumentedType;
import com.technisys.omnichannel.annotations.docs.DocumentedTypeParam;
import com.technisys.omnichannel.core.domain.User;

/**
 *
 * @author lgaudio
 */
@DocumentedType
public class AdminUser {

    @DocumentedTypeParam(description = "Admin first name")
    private String firstName;

    @DocumentedTypeParam(description = "Admin last name")
    private String lastName;

    @DocumentedTypeParam(description = "Admin identifier")
    private String idUser;

    @DocumentedTypeParam(description = "Admin email")
    private String email;

    @DocumentedTypeParam(description = "Admin language")
    private String lang;

    @DocumentedTypeParam(description = "Admin document number")
    private String documentNumber;

    @DocumentedTypeParam(description = "Admin document type")
    private String documentType;

    @DocumentedTypeParam(description = "Admin document country")
    private String documentCountry;

    @DocumentedTypeParam(description = "Admin mobile number")
    private String mobileNumber;

    @DocumentedTypeParam(description = "Indicates if the user has use the back-office")
    private Boolean isBackofficeUser;

    public AdminUser(User admin, Boolean isBackofficeUser) {
        this.firstName = admin.getFirstName();
        this.lastName = admin.getLastName();
        this.email = admin.getEmail();
        this.lang = admin.getLang();
        this.documentNumber = admin.getDocumentNumber();
        this.documentType = admin.getDocumentType();
        this.documentCountry = admin.getDocumentCountry();
        this.mobileNumber = admin.getMobileNumber();
        this.isBackofficeUser = isBackofficeUser;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public Boolean getBackofficeUser() {
        return isBackofficeUser;
    }

    public void setBackofficeUser(Boolean backofficeUser) {
        isBackofficeUser = backofficeUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdUser() {
        return idUser;
    }

    public void setIdUser(String idUser) {
        this.idUser = idUser;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentCountry() {
        return documentCountry;
    }

    public void setDocumentCountry(String documentCountry) {
        this.documentCountry = documentCountry;
    }

}
