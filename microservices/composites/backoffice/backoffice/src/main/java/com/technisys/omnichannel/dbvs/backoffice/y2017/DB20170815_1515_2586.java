/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author rschiappapietra
 */
public class DB20170815_1515_2586 extends DBVSUpdate {

    @Override
    public void up() {
        
        deleteActivity("widgets.products.creditcads");
        insertActivity("widgets.products.creditcads", "com.technisys.omnichannel.client.activities.widgets.CreditCadsActivity", 
                "desktop", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

        deleteActivity("widgets.investments");
        insertActivity("widgets.investments", "com.technisys.omnichannel.client.activities.widgets.InvestmentsActivity", 
                "desktop", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

        delete("widgets", "id='creditCards'");
        insert("widgets", new String[] { "id", "uri" }, new String[] { "creditCards", "widgets/credit-cards" });

        deleteActivity("widgets.products.accounts");
        insertActivity("widgets.products.accounts", "com.technisys.omnichannel.client.activities.widgets.AccountsActivity", "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

        delete("widgets", "id='accounts'");
        insert("widgets", new String[] { "id", "uri" }, new String[] { "accounts", "widgets/accounts" });

        deleteActivity("widgets.products.loans");
        insertActivity("widgets.products.loans", "com.technisys.omnichannel.client.activities.widgets.LoansActivity", "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

        delete("widgets", "id='loans'");
        insert("widgets", new String[] { "id", "uri" }, new String[] { "loans", "widgets/loans" });

        insertOrUpdateConfiguration("widget.products.account.maxToSmallCards", "2", ConfigurationGroup.NEGOCIO, "widgetAcounts", new String[] { "notEmpty", "integer" });
        update("configuration", new String[] { "channels" }, new String[] { "frontend" }, "id_field='widget.products.account.maxToSmallCards'");

        insertOrUpdateConfiguration("widget.products.loan.maxToSmallCards", "2", ConfigurationGroup.NEGOCIO, "widgetLoans", new String[] { "notEmpty", "integer" });
        update("configuration", new String[] { "channels" }, new String[] { "frontend" }, "id_field='widget.products.loan.maxToSmallCards'");

        insertOrUpdateConfiguration("widget.products.creditCard.maxToSmallCards", "2", ConfigurationGroup.NEGOCIO, "widgetCreditCard", new String[] { "notEmpty", "integer" });
        update("configuration", new String[] { "channels" }, new String[] { "frontend" }, "id_field='widget.products.creditCard.maxToSmallCards'");

        delete("widgets", "id='investments'");
        insert("widgets", new String[] { "id", "uri" }, new String[] { "investments", "widgets/investments" });

    }
}
