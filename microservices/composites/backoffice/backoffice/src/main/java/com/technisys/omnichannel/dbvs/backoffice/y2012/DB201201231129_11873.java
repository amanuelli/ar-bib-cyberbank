/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201201231129_11873 extends DBVSUpdate {

    @Override
    public void up() {
        // PERMISSIONS
        String[] fieldNames = new String[]{"id_permission", "permission_type"};

        String[] fieldValues = new String[]{"rub.statementlines.read", "client"};
        insert("permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.statementlines.manage", "client"};
        insert("permissions", fieldNames, fieldValues);

        fieldNames = new String[]{"id_permission", "id_credential_group"};

        fieldValues = new String[]{"rub.statementlines.read", "onlyPassword"};
        insert("permissions_credentials_groups", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.statementlines.manage", "onlyPassword"};
        insert("permissions_credentials_groups", fieldNames, fieldValues);

        fieldNames = new String[]{"id_role", "id_permission"};

        fieldValues = new String[]{"client_administrator", "rub.statementlines.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.statementlines.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.statementlines.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_administrator", "rub.statementlines.manage"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.statementlines.manage"};
        insert("role_permissions", fieldNames, fieldValues);

        // ACTIVITIES
        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required"};

        fieldValues = new String[]{"rub.statementline.createNote", "1", "1", "com.technisys.rubicon.business.statementlines.activities.CreateStatementLineNoteActivity", "rub.statementlines.manage"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.statementline.readNote", "1", "1", "com.technisys.rubicon.business.statementlines.activities.ReadStatementLineNoteActivity", "rub.statementlines.read"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.statementline.modifyNote", "1", "1", "com.technisys.rubicon.business.statementlines.activities.ModifyStatementLineNoteActivity", "rub.statementlines.manage"};
        insert("activities", fieldNames, fieldValues);

    }
}