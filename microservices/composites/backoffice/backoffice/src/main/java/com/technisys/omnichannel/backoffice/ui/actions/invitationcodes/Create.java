/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.opensymphony.xwork2.ActionSupport;
import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.CreatePreResponse;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.domain.Country;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 * @author Sebastian Barbosa
 */
@Action(results = {
    @Result(location = "/invitationcodes/_create.jsp")
})
public class Create extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        IBRequest request = new IBRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.invitationCodes.createPre");

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);
        if (!ReturnCodes.OK.equals(response.getReturnCode())) {
            throw new JSONException(response);
        }

        CreatePreResponse createResponse = (CreatePreResponse) response;
        countryList = createResponse.getCountryList();
        documentTypeList = createResponse.getDocumentTypeList();
        defaultCountry = createResponse.getDefaultCountry();
        defaultDocumentType = createResponse.getDefaultDocumentType();

        adminSchemeList = createResponse.getAdminSchemeList();

        return SUCCESS;
    }

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private java.util.List<String> adminSchemeList;
    private java.util.List<Country> countryList;
    private java.util.List<String> documentTypeList;
    private String defaultCountry;
    private String defaultDocumentType;

    public List<String> getAdminSchemeList() {
        return adminSchemeList;
    }

    public java.util.List<Country> getCountryList() {
        return countryList;
    }

    public java.util.List<String> getDocumentTypeList() {
        return documentTypeList;
    }

    public String getDefaultCountry() {
        return defaultCountry;
    }

    public String getDefaultDocumentType() {
        return defaultDocumentType;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
