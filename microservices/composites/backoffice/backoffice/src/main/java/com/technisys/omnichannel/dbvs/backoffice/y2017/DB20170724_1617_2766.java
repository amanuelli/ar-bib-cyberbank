/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author danireb
 */
public class DB20170724_1617_2766 extends DBVSUpdate {

    @Override
    public void up() {
        
        
        
        String idForm = "payLoan";
        String idField = "loanPayment";
        
        update("form_fields", new String[]{"visible", "required"}, new String[]{"value(loan.isFrequentDestination) == 'false'", "value(loan.isFrequentDestination) == 'false'"}, "id_form = '" + idForm + "' AND id_field = '" + idField + "'");
        
        
        idField = "otherLoanAmount";
        
        update("form_fields", new String[]{"visible", "required"}, new String[]{"value(loan.isFrequentDestination) == 'true'", "value(loan.isFrequentDestination) == 'true'"}, "id_form = '" + idForm + "' AND id_field = '" + idField + "'");
        
        
        idForm = "payCreditCard";
        idField = "amount";
        
        update("form_fields", new String[]{"visible", "required"}, new String[]{"value(creditCard.isFrequentDestination) == 'false'", "value(creditCard.isFrequentDestination) == 'false'"}, "id_form = '" + idForm + "' AND id_field = '" + idField + "'");
        
        idField = "otherAmount";
        
        update("form_fields", new String[]{"visible", "required"}, new String[]{"value(creditCard.isFrequentDestination) == 'true'", "value(creditCard.isFrequentDestination) == 'true'"}, "id_form = '" + idForm + "' AND id_field = '" + idField + "'");
        
        
    }
    
}
