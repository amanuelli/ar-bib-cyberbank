/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150902_1828_859 extends DBVSUpdate {

    @Override
    public void up() {
        //inserto el grupo de credenciales con password y accesToken
        insert("credential_groups", new String[]{"id_credential_group", "id_credential", "ordinal"}, new String[]{"accessToken-password", "accessToken", "1"});
        insert("credential_groups", new String[]{"id_credential_group", "id_credential", "ordinal"}, new String[]{"accessToken-password", "password", "2"});
        
        //inserto el permiso "core.loginWithPIN"
        insert("permissions", new String[]{"id_permission"}, new String[]{"user.preferences.pin"});
        
        //inserto asociacion entre permiso y grupo de credenciales
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"user.preferences.pin", "accessToken-password"});
        
        insertActivity("preferences.changepin.preview", "com.technisys.omnichannel.client.activities.preferences.pin.ChangePinPreviewActivity", "preferences", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.changepin.send", "com.technisys.omnichannel.client.activities.preferences.pin.ChangePinActivity", "preferences", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "user.preferences.pin");
    
        updateConfiguration("client.permissions.defaults", "core.authenticated|core.loginWithOTP|core.loginWithPassword|user.preferences|user.preferences.pin");
    
        
    }
}
