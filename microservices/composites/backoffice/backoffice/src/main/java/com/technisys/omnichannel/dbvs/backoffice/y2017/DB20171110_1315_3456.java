/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3456
 *
 * @author fpena
 */
public class DB20171110_1315_3456 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB, DBVS.DIALECT_ORACLE}, "UPDATE configuration SET value = REPLACE(value, '/com/manentia/safeway/', '/com/technisys/safeway/')");
    }
}
