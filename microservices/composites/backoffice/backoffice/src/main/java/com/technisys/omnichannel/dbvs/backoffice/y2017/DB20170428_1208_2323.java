/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2323
 *
 * @author dimoda
 */
public class DB20170428_1208_2323 extends DBVSUpdate {

    @Override
    public void up() {
        update("forms", new String[]{"templates_enabled"}, new String[]{"0"}, "id_form='lostOrStolenCreditCard'");
    }

}