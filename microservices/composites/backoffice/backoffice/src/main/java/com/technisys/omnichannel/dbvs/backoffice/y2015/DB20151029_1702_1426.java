/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20151029_1702_1426 extends DBVSUpdate {

    @Override
    public void up() {
        delete("activity_products", "id_activity='session.recoverPinWithSecondFactor.step1'");
        delete("activity_products", "id_activity='session.recoverPasswordWithSecondFactor.step1'");

        update("activities", new String[]{"id_activity"}, new String[]{"session.recoverPassword.step1"}, "id_activity='session.recoverPasswordWithSecondFactor.step1'");
        update("activities", new String[]{"id_activity"}, new String[]{"session.recoverPin.step1"}, "id_activity='session.recoverPinWithSecondFactor.step1'");

        updateConfiguration("core.attempts.features", "session.loginWithSecondFactorAndPassword.step1=login|session.loginWithSecondFactorAndPassword.step2=login|session.recoverPassword.step1=login|session.recoverPinAndPassword.step2=recoverCredential|session.incrementCaptchaAttempts=login|session.recoverPin.step1=login");

        deleteActivity("session.postLogin");

        delete("activity_products", "id_activity='session.recoverPin.step1'");
        delete("activity_products", "id_activity='session.recoverPassword.step1'");
    }
}
