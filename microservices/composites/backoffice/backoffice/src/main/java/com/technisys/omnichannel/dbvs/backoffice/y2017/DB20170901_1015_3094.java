/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author rschiappapietra
 */
public class DB20170901_1015_3094 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("widgets.products");
        deleteActivity("widgets.pendingTransactions");
        deleteActivity("widgets.nextExpirations");

        delete("widgets", "id = 'pendingTransactions'");
        delete("widgets", "id = 'nextExpirations'");
    }
}
