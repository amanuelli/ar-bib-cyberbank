/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.responses;

import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;

/**
 *
 * @author Sebastian Barbosa
 */
public class CreateResponse extends IBResponse {

    private String invitationCode;

    public CreateResponse() {
        super();
    }

    public CreateResponse(IBRequest request) {
        super(request);
    }

    public String getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    @Override
    public String toString() {
        return "CreateResponse{" + "invitationCode=" + invitationCode + '}';
    }
}
