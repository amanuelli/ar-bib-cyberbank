package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20180416_1141_4272 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("client.permissions.defaults", "core.authenticated|user.preferences|user.preferences.withSecondFactor");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions (id_group, id_permission, target) "
                + "SELECT id_group, 'user.preferences', 'NONE' FROM groups G WHERE NOT EXISTS "
                + "(SELECT id_group FROM group_permissions GP WHERE G.id_group = GP.id_group and GP.id_permission = 'user.preferences')");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions (id_group, id_permission, target) "
                + "SELECT id_group, 'user.preferences.withSecondFactor', 'NONE' FROM groups G WHERE NOT EXISTS "
                + "(SELECT id_group FROM group_permissions GP WHERE G.id_group = GP.id_group and GP.id_permission = 'user.preferences.withSecondFactor')");
    }
}
