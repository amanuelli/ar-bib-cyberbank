/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor.AuditLevel;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor.Kind;

/**
 *
 * @author Diego Curbelo
 */
public class DB20150803_1542_581 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("core.forms.backoffice.read", "com.technisys.omnichannel.activities.forms.BackofficeReadFormActivity",
                null, AuditLevel.Full, Kind.Other, null);
    }
}
