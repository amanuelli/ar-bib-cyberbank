/*
 * Copyright 2021 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */

package com.technisys.omnichannel.backoffice.business.cybo.environments.activities;

import com.technisys.omnichannel.backoffice.business.cybo.Response;
import com.technisys.omnichannel.backoffice.business.cybo.environments.requests.ModifyGeneralData;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.handlers.environment.EnvironmentHandler;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.TransactionRequest;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.core.domain.Environment.ENVIRONMENT_TYPE_CORPORATE_GROUP;

/**
 * @author azeballos
 */
public class ModifyGeneralActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        TransactionRequest tRequest = (TransactionRequest) request;
        Response response = new Response<>(request);

        try {
            String transactionID = request.getIdTransaction();

            I18n i18n = I18nFactory.getHandler();
            ModifyGeneralData data = (ModifyGeneralData) tRequest.getTransactionData();
            int environmentId = data.getId();

            Environment env = Administration.getInstance().readEnvironment(environmentId);
            if (env == null) {
                throw new ActivityException(ReturnCodes.BAD_REQUEST, i18n.getMessageForBackofficeUser("cybo.environments.notFound", request.getIdUser()));
            }

            env.setName(data.getName());

            if (ENVIRONMENT_TYPE_CORPORATE_GROUP.equals(data.getType())) {
                if (!StringUtils.isBlank(data.getHoldingClientsToAdd())) {
                    List<String> clientsToAdd = Arrays.asList(data.getHoldingClientsToAdd().split(", "));
                    if (!clientsToAdd.isEmpty()) {
                        EnvironmentHandler.getInstance().addHoldingClients(transactionID, data.getId(), clientsToAdd);
                    }
                }
                if (!StringUtils.isBlank(data.getHoldingClientsToRemove())) {
                    List<String> clientsToRemove = Arrays.asList(data.getHoldingClientsToRemove().split(", "));
                    if (!clientsToRemove.isEmpty()) {
                        EnvironmentHandler.getInstance().removeHoldingClients(data.getId(), clientsToRemove);
                    }
                }
            } else {
                env.setEnvironmentType(data.getType());
                env.setAdministrationScheme(data.getScheme());
            }

            Administration.getInstance().modifyEnvironment(env);

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        TransactionRequest tRequest = (TransactionRequest) request;
        Map<String, String> result = new HashMap<>();

        ModifyGeneralData data = (ModifyGeneralData) tRequest.getTransactionData();

        try {
            if (ENVIRONMENT_TYPE_CORPORATE_GROUP.equals(data.getType())) {

                //validate holding name
                validateHoldingName(result, data);

                //validate holding members
                if (!StringUtils.isBlank(data.getHoldingClientsToRemove())) {
                    Integer totalClients = Administration.getInstance().readEnvironment(data.getId()).getClients().size()
                            - data.getHoldingClientsToRemove().split(", ").length;

                    if(!StringUtils.isBlank(data.getHoldingClientsToAdd())) {
                        totalClients =  totalClients + data.getHoldingClientsToAdd().split(", ").length;
                    }
                    if (totalClients < 2) {
                        result.put("@environment", "backoffice.environments.holdings.error.membersRequired");
                    }
                }
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    private void validateHoldingName(Map<String, String> result, ModifyGeneralData data) throws IOException {
        if (StringUtils.isBlank(data.getName())) {
            result.put("@name", "backoffice.environments.holdings.error.requiredName");
        } else {
            //look for an environment with the same name
            Environment environment = Administration.getInstance().readEnvironment(data.getId());
            if (!environment.getName().equals(data.getName())) {
                if (Administration.getInstance().getEnvironment(data.getName()) != null) {
                    result.put("@name", "backoffice.environments.holdings.error.duplicatedName");
                }
            }
        }
    }
}
