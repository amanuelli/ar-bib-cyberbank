/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5576
 *
 * @author dimoda
 */
public class DB20181012_1620_5576 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, "DELETE FROM sections WHERE id_section != 'desktop-header' AND id_section NOT IN (SELECT id_section FROM campaign_sections)");
    }

}