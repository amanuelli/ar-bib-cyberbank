/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

public class DB20180523_1334_3979 extends DBVSUpdate {

    @Override
    public void up() {
        //Borrar titulos de sección, mail de notificación, etc.
        delete("form_fields", "id_form='transferInternal' AND id_field in ('creditSection', 'debitSection', 'debitReference', 'notificationBody', 'notificationMails', 'otherCreditAccount', 'horizontalrule')");

        //Reordenar el campo monto y cuenta débito
        update("form_fields", new String[]{"ordinal"}, new String[]{"1"}, "id_form='transferInternal' AND id_field = 'amount'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"2"}, "id_form='transferInternal' AND id_field = 'debitAccount'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"3"}, "id_form='transferInternal' AND id_field = 'creditAccount'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"4"}, "id_form='transferInternal' AND id_field = 'creditReference'");

        //Apago destinos frecuentes
        update("form_field_ps", new String[]{"apply_frequent_destinations"}, new String[]{"0"}, "apply_frequent_destinations = 1");

        //Ajusto labels
        update("form_field_messages", new String[]{"value"}, new String[]{"Referencia"}, "id_message='fields.transferInternal.creditReference.label' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Reference"}, "id_message='fields.transferInternal.creditReference.label' AND lang='en'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Referência"}, "id_message='fields.transferInternal.creditReference.label' AND lang='pt'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Cuenta de destino"}, "id_message='fields.transferInternal.creditAccount.label' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Cuenta de origen"}, "id_message='fields.transferInternal.debitAccount.label' AND lang='es'");

        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_message='fields.transferInternal.amount.help'");
        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_message='fields.transferInternal.creditReference.help'");

        update("form_messages", new String[]{"value"}, new String[]{"Transferir entre mis cuentas"}, "id_form='transferInternal' AND id_message='forms.transferInternal.formName' AND lang='es'");
    }
}
