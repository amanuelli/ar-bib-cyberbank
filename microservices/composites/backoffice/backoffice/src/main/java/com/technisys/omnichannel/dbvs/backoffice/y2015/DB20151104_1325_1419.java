/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author ?
 */
public class DB20151104_1325_1419 extends DBVSUpdate {

    @Override
    public void up() {
        //creo las secciones para el login
        insert("sections", new String[]{"id_section", "name"}, new String[]{"desktop-header", "Escritorio"});

        //tamanio de imagenes
        insert("image_sizes", new String[]{"display", "image_width", "image_height"}, new String[]{"MOBILE", "1536", "350"});
        insert("image_sizes", new String[]{"display", "image_width", "image_height"}, new String[]{"NARROW", "2160", "240"});
        insert("image_sizes", new String[]{"display", "image_width", "image_height"}, new String[]{"NORMAL", "3160", "260"});

        //asociacion de imagenes a secciones
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, "INSERT INTO sections_images (id_section, id_image_size) SELECT 'desktop-header', id_image_size FROM image_sizes WHERE image_width=1536 AND image_height=350");
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, "INSERT INTO sections_images (id_section, id_image_size) SELECT 'desktop-header', id_image_size FROM image_sizes WHERE image_width=2160 AND image_height=240");
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, "INSERT INTO sections_images (id_section, id_image_size) SELECT 'desktop-header', id_image_size FROM image_sizes WHERE image_width=3160 AND image_height=260");

    }
}
