/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.activities;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ReadRequest;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.ReadResponse;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sebastian Barbosa
 */
public class ReadActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        ReadResponse response = new ReadResponse(request);

        try {
            ReadRequest req = (ReadRequest) request;
            InvitationCode code = InvitationCodesHandler.readInvitationCode(req.getIdInvitationCode());

            // Enmascaro el código de invitación
            code.setInvitationCodeMasked(maskInvitationCode(code.getInvitationCode()));

            if (StringUtils.isNotBlank(code.getGroups())) {
                List<Group> groupList = new ArrayList<>();

                String[] ids = StringUtils.split(code.getGroups(), ",");
                for (String id : ids) {
                    if (!StringUtils.isNumeric(id.trim())) {
                        groupList.add(null); // Agrego un null para poder saber que el grupo era inválido en la página
                    } else {
                        groupList.add(AccessManagementHandlerFactory.getHandler().getGroup(Integer.parseInt(id.trim())));
                    }
                }

                response.setGroupList(groupList);
            }

            response.setInvitationCode(code);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            ReadRequest req = (ReadRequest) request;
            if (InvitationCodesHandler.readInvitationCode(req.getIdInvitationCode()) == null) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }

    private static String maskInvitationCode(String code) {
        int maskLength = StringUtils.length(code) - ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "backoffice.invitationCodes.unmaskedLength", 4);
        return maskLength > 0 ? StringUtils.repeat("*", maskLength) + code.substring(maskLength) : code;
    }
}
