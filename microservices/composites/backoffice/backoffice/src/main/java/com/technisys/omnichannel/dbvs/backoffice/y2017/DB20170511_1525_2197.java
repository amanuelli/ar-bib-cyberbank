/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jbaccino
 */
public class DB20170511_1525_2197 extends DBVSUpdate {

    @Override
    public void up() {
        // Cleanup
        delete("permissions", "id_permission='pay.creditCard'");
        delete("permissions", "id_permission='pay.creditCard.thirdParties'");

        deleteActivity("pay.creditcard.send");
        deleteActivity("pay.creditcard.preview");

        // Form
        String idForm = "payCreditCard";
        String version = "1";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        delete("forms", "id_form='" + idForm + "' and version=" + version);

        insert("permissions", new String[]{"id_permission"}, new String[]{"pay.creditCard"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"pay.creditCard", "accessToken"});

        insert("permissions", new String[]{"id_permission"}, new String[]{"pay.creditCard.thirdParties"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"pay.creditCard.thirdParties", "accessToken"});

        insertActivity("pay.creditcard.send", "com.technisys.omnichannel.client.activities.pay.creditcard.PayCreditCardSendActivity", "creditCards", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, "pay.creditCard", "debitAccount");
        insertActivity("pay.creditcard.preview", "com.technisys.omnichannel.client.activities.pay.creditcard.PayCreditCardPreviewActivity", "creditCards", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "");

        updateConfiguration("core.permissionsForProducts", "product.read|transfer.internal|transfer.thirdParties|pay.loan|pay.loan.thirdParties|pay.creditCard|pay.creditCard.thirdParties|accounts.requestCheckbook");

        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"pay.creditCard", "payments", null, null, "0", "30", "payments", null, null, "0", "30", "payments", null, null, "1", "30", "CA,CC", null, null});

        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"pay.creditCard.thirdParties", "payments", null, null, "0", "40", "payments", null, null, "0", "40", "payments", null, null, "1", "40", "CA,CC", "pay.creditCard", null});

        //forms
        insert("forms", new String[]{"id_form", "version", "enabled", "category", "type", "id_jbpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable"},
                new String[]{idForm, version, "1", "payments", "activity", null, null, "pay.creditcard.send", "1", "1", "1"});

        //form_fields
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};

        int ordinal = 1;
        Map<String, String> messages = new HashMap();

        // Cuenta (debitAccount)
        insert("form_fields", formFields, new String[]{"debitAccount", idForm, version, "productselector", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option", "show_other_permission"},
                new String[]{"debitAccount", idForm, version, "field-normal", "0", null});

        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"debitAccount", idForm, version, "CC"});
        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"debitAccount", idForm, version, "CA"});

        insert("form_field_ps_permissions", new String[]{"id_field", "id_form", "form_version", "id_permission"},
                new String[]{"debitAccount", idForm, version, "pay.creditCard"});

        messages.put("debitAccount.label", "Cuenta");
        messages.put("debitAccount.requiredError", "Debe seleccionar una cuenta");

        // Tarjeta (creditCard)
        insert("form_fields", formFields, new String[]{"creditCard", idForm, version, "productselector", String.valueOf(ordinal++), "TRUE", "TRUE", "creditCardSelector", "1"});
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option", "show_other_permission", "show_other_by", "show_other_id_ps"},
                new String[]{"creditCard", idForm, version, "field-normal", "1", "pay.creditCard.thirdParties", "field", "debitAccount"});

        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"creditCard", idForm, version, "TC"});
        insert("form_field_ps_permissions", new String[]{"id_field", "id_form", "form_version", "id_permission"}, new String[]{"creditCard", idForm, version, "product.read"});

        messages.put("creditCard.label", "Tarjeta");
        messages.put("creditCard.requiredError", "Debe seleccionar una tarjeta");
        messages.put("creditCard.showOtherText", "Otra tarjeta");

        // OtraTarjeta - Section (otherCreditCard)
        insert("form_fields", formFields, new String[]{"otherCreditCard", idForm, version, "sectiontitle", String.valueOf(ordinal++), "value(creditCard) == 'other'", "FALSE", "default", "1"});

        messages.put("otherCreditCard.help", null);
        messages.put("otherCreditCard.hint", null);
        messages.put("otherCreditCard.label", "Otra tarjeta");

        // Banco de la tarjeta (creditCardBank)
        insert("form_fields", formFields, new String[]{"creditCardBank", idForm, version, "bankselector", String.valueOf(ordinal++), "shown(otherCreditCard)", "shown(otherCreditCard)", "default", "1"});
        insert("form_field_bankselector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option"},
                new String[]{"creditCardBank", idForm, version, "field-normal", null, "1"});

        messages.put("creditCardBank.requiredError", "Debe seleccionar un banco");
        messages.put("creditCardBank.help", null);
        messages.put("creditCardBank.hint", null);
        messages.put("creditCardBank.label", "Banco de la tarjeta");

        // Número de tarjeta (otherCreditCardNumber)
        insert("form_fields", formFields, new String[]{"otherCreditCardNumber", idForm, version, "text", String.valueOf(ordinal++), "shown(otherCreditCard)", "shown(otherCreditCard)", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"otherCreditCardNumber", idForm, version, "16", "16", "field-normal", "onlyNumbers"});

        messages.put("otherCreditCardNumber.help", "Debe ingresar aqui el número de plástico a pagar");
        messages.put("otherCreditCardNumber.hint", null);
        messages.put("otherCreditCardNumber.label", "Número de tarjeta");
        messages.put("otherCreditCardNumber.requiredError", "Debe ingresar un número de tarjeta");

        // Monto a pagar (otherAmount)
        insert("form_fields", formFields, new String[]{"otherAmount", idForm, version, "amount", String.valueOf(ordinal++), "shown(otherCreditCard)", "shown(otherCreditCard)", "default", "1"});
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"},
                new String[]{"otherAmount", idForm, version, "field-normal", "1"});

        insert("form_field_amount_currencies", new String[]{"id_field", "id_form", "form_version", "id_currency"},
                new String[]{"otherAmount", idForm, version, "000"});

        messages.put("otherAmount.help", null);
        messages.put("otherAmount.hint", null);
        messages.put("otherAmount.label", "Monto a pagar");
        messages.put("otherAmount.requiredError", "Debe ingresar un monto");

        // Monto a pagar (amount)
        insert("form_fields", formFields, new String[]{"amount", idForm, version, "paycreditcardamount", String.valueOf(ordinal++), "value(creditCard) != 'other'", "TRUE", "default", "1"});
        insert("form_field_cc_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "id_cc_selector", "control_limits"},
                new String[]{"amount", idForm, version, "field-normal", "creditCard", "1"});
        insert("form_field_cc_amount_curr", new String[]{"id_field", "id_form", "form_version", "id_currency"},
                new String[]{"amount", idForm, version, "000"});

        insert("form_field_dependencies", new String[]{"id_field", "id_form", "form_version", "id_dependency_field"},
                new String[]{"amount", idForm, version, "creditCard"});

        messages.put("amount.help", null);
        messages.put("amount.hint", null);
        messages.put("amount.label", "Monto a pagar");
        messages.put("amount.requiredError", "Debe ingresar un monto");

        // line
        insert("form_fields", formFields, new String[]{"line", idForm, version, "horizontalrule", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});

        messages.put("line.help", null);
        messages.put("line.hint", null);
        messages.put("line.label", " ");

        // Referencia (reference)
        insert("form_fields", formFields, new String[]{"reference", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"reference", idForm, version, "0", "20", "field-normal", null});

        messages.put("reference.help", "Texto que le ayude a identificar su pago.");
        messages.put("reference.hint", null);
        messages.put("reference.label", "Referencia");

        // Emails de notificación (notificationEmails)
        insert("form_fields", formFields, new String[]{"notificationEmails", idForm, version, "emaillist", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_emaillist", new String[]{"id_field", "id_form", "form_version", "display_type"},
                new String[]{"notificationEmails", idForm, version, "field-normal"});

        messages.put("notificationEmails.help", "E-mails a notificar el envío de la transferencia.");
        messages.put("notificationEmails.hint", null);
        messages.put("notificationEmails.label", "Emails de notificación");

        // Cuerpo de notificación (notificationBody)
        insert("form_fields", formFields, new String[]{"notificationBody", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_textarea", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"},
                new String[]{"notificationBody", idForm, version, "0", "500", "field-normal"});

        messages.put("notificationBody.help", null);
        messages.put("notificationBody.hint", null);
        messages.put("notificationBody.label", "Cuerpo de notificación");

        // Campos específicos del ticket
        formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile", "ticket_only"};

        // Monto total debitado (debitAmount)
        insert("form_fields", formFields, new String[]{"debitAmount", idForm, version, "amount", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1", "1"});
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"},
                new String[]{"debitAmount", idForm, version, "field-normal", "0"});

        messages.put("debitAmount.help", null);
        messages.put("debitAmount.hint", null);
        messages.put("debitAmount.label", "Monto total debitado");

        // Cotización aplicada (rate)
        insert("form_fields", formFields, new String[]{"rate", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"rate", idForm, version, "0", "50", "field-normal", null});

        messages.put("rate.help", null);
        messages.put("rate.hint", null);
        messages.put("rate.label", "Cotización aplicada");

        // Comisiones (commission)
        insert("form_fields", formFields, new String[]{"commission", idForm, version, "amount", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1", "1"});
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"},
                new String[]{"commission", idForm, version, "field-normal", "0"});

        messages.put("commission.help", null);
        messages.put("commission.hint", null);
        messages.put("commission.label", "Comisiones");

        //Insertado de Mensajes
        //*********************************************************************
        String[] formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        String[] formFieldsValues = new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Pago de Tarjeta", date};

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form,version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formField, formFieldsValues);
        }

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }
    
}
