/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DB20170718_1721_2494 extends DBVSUpdate {

    @Override
    public void up() {

        /** Datos del FORMULARIO ***/
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "sub_type", "visible_in_mobile", "read_only", "ticket_only" };
        String[] formFieldsValues;
        String idForm = "surveyOfDiscrepancies";
        String version = "1";
        Map<String, String> messages = new HashMap();


        /**** Insert Form - Levantamiento de discrepancias ****/
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[] { "id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable", "editable_in_mobile", "editable_in_narrow" };
        String[] formsFieldsValues = new String[] { idForm, version, "1", "comex", "process", "demo:1:3", "comex", null, "1", "1", "1", "0", "1" };
        insert("forms", formsFields, formsFieldsValues);

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version,lang,value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "','" + version + "','es',  'Cancelación de carta de crédito',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value", "modification_date" };
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "es", "Levantamiento de discrepancias", date });
        }

        String clientFormId = "client.form." + idForm + ".send";
        delete("permissions", "id_permission = '" + clientFormId + "'");
        delete("permissions_credentials_groups", "id_permission = '" + clientFormId + "'");
        delete("group_permissions", "id_permission = '" + clientFormId + "'");

        insert("permissions", new String[] { "id_permission" }, new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "accessToken-pin"});

        String idField;
        /**** Insert field text - Carta de crédito ****/
        idField = "creditLetter";
        formFieldsValues = new String[] { idField, idForm, version, "text", "1", "TRUE", "FALSE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation" }, new String[] { idField, idForm, version, "field-medium", "0", "30", null });
        
        messages.put("creditLetter.label","Carta de crédito");
        messages.put("creditLetter.help","Seleccione la carta de crédito a la que desea levantarle discrepancias");
        messages.put("creditLetter.requiredError","Debe ingresar un nro. de carta de crédito");
        
        /**** Insert field termsandconditions - Autorización débito ****/
        idField = "debitAuthorization";
        formFieldsValues = new String[] { idField, idForm, version, "termsandconditions", "2", "TRUE", "FALSE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label" }, new String[] { idField, idForm, version, "field-medium", "1", "1" });

        messages.put("debitAuthorization.label","Autorización débito");
        messages.put("debitAuthorization.showAcceptOptionText","Si");
        messages.put("debitAuthorization.termsAndConditions","Autorizo a debitar dicho importe de nuestra cuenta.");

        /**** Insert field textarea - Observaciones ****/
        idField = "observations";
        formFieldsValues = new String[] { idField, idForm, version, "textarea", "3", "TRUE", "FALSE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { idField, idForm, version, "0", "500", "field-medium" });
        
        messages.put("observations.label","Observaciones");

        /**** Insert field document - Documento persona a retirar ****/
        idField = "documentPersonWithdraw";
        formFieldsValues = new String[] { idField, idForm, version, "document", "4", "TRUE", "FALSE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_document", new String[] { "id_field", "id_form", "form_version", "default_country", "default_document_type" }, new String[] { idField, idForm, version, "UY", "CI" });

        messages.put("documentPersonWithdraw.label","Documento persona a retirar");
     
        /**** Insert field text - Nombre autorizado a retirar los documentos ****/
        idField = "authorizedNameWithdrawDocuments";
        formFieldsValues = new String[] { idField, idForm, version, "text", "5", "TRUE", "FALSE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation" }, new String[] { idField, idForm, version, "field-medium", "0", "140", null });
        
        messages.put("authorizedNameWithdrawDocuments.label", "Nombre autorizado a retirar los documentos");
        
        /**** Insert field termsandconditions - Disclaimer ****/
        idField = "disclaimer";
        formFieldsValues = new String[] { idField, idForm, version, "termsandconditions", "6", "TRUE", "FALSE", null, "default", "1", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label" }, new String[] { idField, idForm, version, "field-medium", "0", "0" });
        
        messages.put("disclaimer.label",null);
        messages.put("disclaimer.termsAndConditions", "Las instrucciones enviadas al banco por usted luego de las <hora de corte> serán procesadas al siguiente día hábil bancario. Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.");
        
        /**** Insert field emaillist - Emails de notificación ****/
        idField = "listaEmailsNotificacion";
        formFieldsValues = new String[] { idField, idForm, version, "emaillist", "7", "TRUE", "FALSE", null, "default", "1", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_emaillist", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { idField, idForm, version, "field-normal" });
        
        messages.put("listaEmailsNotificacion.help","E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");
        messages.put("listaEmailsNotificacion.label","Emails de notificación");

        /**** Insert field textarea - Cuerpo de notificación ****/
        idField = "notificationBody";
        formFieldsValues = new String[] { idField, idForm, version, "textarea", "8", "TRUE", "FALSE", null, "default", "1", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { idField, idForm, version, "0", "500", "field-medium" });
        
        messages.put("notificationBody.label", "Cuerpo de notificación");
        
        
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }
}
