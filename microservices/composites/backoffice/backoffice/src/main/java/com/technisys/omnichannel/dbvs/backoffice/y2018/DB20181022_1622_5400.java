package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5400
 *
 * @author mcheveste
 */
public class DB20181022_1622_5400 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.advanced.group.read", "com.technisys.omnichannel.client.activities.administration.advanced.GroupReadActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
    }
}
