/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3718
 *
 * @author ahernandez
 */
public class DB20180111_2102_3718 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(DBVS.DIALECT_MYSQL, "update desktop_layouts, (select row_number, id_user, id_environment from desktop_layouts where id_widget='fingerprint') finger_desktop_layout_result set desktop_layouts.row_number = desktop_layouts.row_number - 1 where desktop_layouts.row_number > finger_desktop_layout_result.row_number and desktop_layouts.id_user = finger_desktop_layout_result.id_user and desktop_layouts.id_environment = finger_desktop_layout_result.id_environment;");
        customSentence(DBVS.DIALECT_MSSQL, "update desktop_layouts set desktop_layouts.row_number = desktop_layouts.row_number - 1  where desktop_layouts.row_number > (select dl.row_number as finger_row_number from desktop_layouts as dl where id_widget='fingerprint' and dl.id_user=desktop_layouts.id_user and dl.id_environment=desktop_layouts.id_environment)");
        customSentence(DBVS.DIALECT_ORACLE, "update desktop_layouts dl set dl.row_number = dl.row_number - 1 where dl.row_number > (select row_number from desktop_layouts where id_widget='fingerprint' and id_user=dl.id_user and id_environment=dl.id_environment)");
        customSentence(new String[] {DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB}, "delete from desktop_layouts where id_widget='fingerprint'");
        customSentence(new String[] {DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB}, "update configuration set value = replace(value, 'fingerprint|', '') where id_field='environments.desktopLayout.corporate' or id_field='environments.desktopLayout.retail'");
        customSentence(new String[] {DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB}, "update configuration set value = replace(value, '|fingerprint', '') where id_field='environments.desktopLayout.corporate' or id_field='environments.desktopLayout.retail'");
        delete("widgets", "id='fingerprint'");
    }

}