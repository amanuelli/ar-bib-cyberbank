/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Jhossept Garay
 */

public class DB20190718_1015_7816 extends DBVSUpdate {

    @Override
    public void up() {
        String idField = "administration.signatures.signatureLevels";
        updateConfiguration(idField, "A|B|C|D|E|F|G");
        update("configuration", new String[]{"possible_values"}, new String[]{null}, "id_field='" + idField + "'");
    }

}