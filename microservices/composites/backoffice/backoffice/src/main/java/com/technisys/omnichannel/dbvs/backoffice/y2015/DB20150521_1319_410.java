/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150521_1319_410 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("client.permissions.defaults", "core.authenticated|core.loginWithOTP|core.loginWithPassword", ConfigurationGroup.SEGURIDAD, null, new String[]{});

        insertOrUpdateConfiguration("client.permissions.full.none", "core.cancelTransaction|core.listTransactions|core.moveToDraftTransaction|core.readTransaction|administration.manage|administration.view|", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.read.none", "", ConfigurationGroup.SEGURIDAD, null, new String[]{});

        insertOrUpdateConfiguration("client.permissions.read.allCC", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.full.allCC", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.read.allCA", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.full.allCA", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.read.allPF", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.full.allPF", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.read.allPI", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.full.allPI", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.read.allPA", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.full.allPA", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.read.allTC", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("client.permissions.full.allTC", "product.read", ConfigurationGroup.SEGURIDAD, null, new String[]{});
    }
}