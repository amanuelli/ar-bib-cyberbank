/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150721_1611_415 extends DBVSUpdate {

    @Override
    public void up() {
        // Cleanup
        delete("adm_ui_categories", "1=1");
        delete("adm_ui_permissions", "1=1");

        // Categorías
        insert("adm_ui_categories", new String[]{"id_category", "ordinal"}, new String[]{"transfers", "100"});
        insert("adm_ui_categories", new String[]{"id_category", "ordinal"}, new String[]{"payments", "200"});
        insert("adm_ui_categories", new String[]{"id_category", "ordinal"}, new String[]{"checkbooks", "300"});
        insert("adm_ui_categories", new String[]{"id_category", "ordinal"}, new String[]{"factoring", "400"});
        insert("adm_ui_categories", new String[]{"id_category", "ordinal"}, new String[]{"investments", "500"});
        insert("adm_ui_categories", new String[]{"id_category", "ordinal"}, new String[]{"swift", "600"});
        insert("adm_ui_categories", new String[]{"id_category", "ordinal"}, new String[]{"comex", "700"});

        insert("adm_ui_subcategories", new String[]{"id_subcategory", "id_category", "ordinal"}, new String[]{"import", "comex", "100"});
        insert("adm_ui_subcategories", new String[]{"id_subcategory", "id_category", "ordinal"}, new String[]{"export", "comex", "200"});
        insert("adm_ui_subcategories", new String[]{"id_subcategory", "id_category", "ordinal"}, new String[]{"prefinancing", "comex", "300"});
        insert("adm_ui_subcategories", new String[]{"id_subcategory", "id_category", "ordinal"}, new String[]{"bondsAndGuarantees", "comex", "400"});

        // Permisos
        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"product.read", null, null, null, "1", "0", null, null, null, "1", "0", null, null, null, "1", "0", "CA,CC,PF,PI,PA,TC", null, null});

        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"position", null, null, null, "0", "800", null, null, null, "0", "800", null, null, null, "0", "800", null, null, null});

        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"transfer.internal", "transfers", null, null, "0", "10", "transfers", null, null, "0", "10", "transfers", null, null, "1", "10", "CA,CC", null, null});

        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"transfer.thirdParties", "transfers", null, null, "0", "20", "transfers", null, null, "0", "10", "transfers", null, null, "1", "10", "CA,CC", "transfer.internal", null});

        // Permisos por defecto para todos los grupos
        updateConfiguration("client.permissions.defaults", "core.authenticated|core.loginWithOTP|core.loginWithPassword|user.preferences");
    }
}
