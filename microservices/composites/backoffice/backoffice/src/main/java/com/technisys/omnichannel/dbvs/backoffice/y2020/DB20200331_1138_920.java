/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Cristobal Meneses
 */

public class DB20200331_1138_920 extends DBVSUpdate {

    @Override
    public void up() {
        insert("adm_ui_categories", new String[]{"id_category", "ordinal"}, new String[]{"loans", "1100"});
        update("adm_ui_permissions",
                new String[]{"simple_id_category","medium_id_category","advanced_id_category", },
                new String[]{"loans","loans","loans"},
                "id_permission = 'requestLoan.send'");
    }

}