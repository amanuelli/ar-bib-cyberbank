/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author Diego Curbelo
 */
public class DB20150528_1542_363 extends DBVSUpdate {

    @Override
    public void up() {

        insertOrUpdateConfiguration("frontend.ajax.timeout",
                "30s",
                com.technisys.omnichannel.DBVSUpdate.ConfigurationGroup.TECNICAS, "frontend", new String[]{},
                "30s|1m|2m");
    }
}