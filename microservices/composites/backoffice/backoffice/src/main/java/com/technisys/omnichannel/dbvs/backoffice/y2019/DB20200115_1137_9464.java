/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author emordezki
 */

public class DB20200115_1137_9464 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("session.login.oauth",
                "com.technisys.omnichannel.client.activities.session.OAuthActivity",
                "login",
                ActivityDescriptor.AuditLevel.Full,
                ClientActivityDescriptor.Kind.Other,
                null
        );
        update("activities", new String[] { "id_activity" }, new String[] { "campaigns.readRandomCampaignBySectionAnonymous" }, "id_activity = 'campaigns.readRandomCampaignBySection.anonymous'");
        update("activities", new String[] { "id_activity" }, new String[] { "session.login.step3Oauth" }, "id_activity = 'session.login.step3.oauth'");
        update("activities", new String[] { "id_activity" }, new String[] { "session.login.step4Oauth" }, "id_activity = 'session.login.step4.oauth'");
    }

}