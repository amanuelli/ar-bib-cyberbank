/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3560
 *
 * @author rdosantos
 */
public class DB20171226_1440_3560 extends DBVSUpdate {

    @Override
    public void up() {
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-501-demo-environments.sql", "GO", "UTF-8");
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-501-demo-environments.sql", "GO", "UTF-8");
    }

}