/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.activities;

import java.io.IOException;
import java.util.Arrays;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.CreatePreResponse;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.documenttypes.DocumentTypesHandler;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;

/**
 *
 * @author Sebastian Barbosa
 */
public class CreatePreActivity extends BOActivity {

    private static final String DEFAULT_COUNTRY = "UY";
    private static final String DEFAULT_DOCUMENT_TYPE = "CI";

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        CreatePreResponse response = new CreatePreResponse(request);

        try {
            response.setCountryList(CountryCodesHandler.getCountryList(request.getLang()));
            response.setDocumentTypeList(DocumentTypesHandler.listDocumentTypes(DEFAULT_COUNTRY));
            response.setDefaultCountry(DEFAULT_COUNTRY);
            response.setDefaultDocumentType(DEFAULT_DOCUMENT_TYPE);

            response.setAdminSchemeList(Arrays.asList(new String[]{Environment.ADMINISTRATION_SCHEME_SIMPLE, Environment.ADMINISTRATION_SCHEME_MEDIUM, Environment.ADMINISTRATION_SCHEME_ADVANCED}));

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
