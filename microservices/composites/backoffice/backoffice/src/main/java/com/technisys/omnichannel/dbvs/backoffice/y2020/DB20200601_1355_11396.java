/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Marcelo Bruno
 */

public class DB20200601_1355_11396 extends DBVSUpdate {

    @Override
    public void up() {
        delete("general_conditions_signs", "1=1");

        //default signs for migration data
        customSentence(DBVS.DIALECT_MSSQL, "INSERT INTO general_conditions_signs (id_condition, id_user, id_environment, at_date) SELECT (SELECT MAX(id_condition) FROM general_conditions), id_user, id_environment, GETDATE() FROM environment_users");
        customSentence(DBVS.DIALECT_MYSQL, "INSERT INTO general_conditions_signs (id_condition, id_user, id_environment, at_date) SELECT (SELECT MAX(id_condition) FROM general_conditions), id_user, id_environment, now() FROM environment_users");
        customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO general_conditions_signs (id_condition, id_user, id_environment, at_date) SELECT (SELECT MAX(id_condition) FROM general_conditions), id_user, id_environment, SYSDATE FROM environment_users");
    }

}