/*
 *  Copyright 2012 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201211241611_117 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fields = new String[]{"id_user_status", "can_login"};
        insert("user_status", fields, new String[]{"pending", "0"});

        // Configuration
        fields = new String[]{"id_field", "value", "possible_values", "id_group", "encrypted", "must_be_encrypted"};
        insert("configuration", fields, new String[]{"rubicon.permissions.defaults", "rub.authenticated|rub.login|rub.loginII", "", "Rubicon", "0", "0"});

        insert("configuration", fields, new String[]{"rubicon.token.defaultValidity", "48h", "", "Rubicon", "0", "0"});

        insert("configuration", fields, new String[]{"rubicon.emailValidationFormat", "^\\w+([-\\.+]?\\w+)*@\\w+([\\.-]?\\w+)*\\.(\\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum))$", "", "Rubicon", "0", "0"});

        delete("configuration", "id_field='rubicon.baseURL'");
        insert("configuration", fields, new String[]{"rubicon.baseURL", "http://localhost:8080/frontend", "", "Rubicon", "0", "0"});

        insert("configuration", fields, new String[]{"rubicon.groups.maxProductsToDisplay", "9", "", "Rubicon", "0", "0"}); // <PROTOCOL>://<SERVER>:<PORT>/<APP_NAME>

        insert("configuration", fields, new String[]{"rubicon.permissions.handleMoneyList", "rub.product.transferAbroad|rub.product.transferInternal|rub.product.transferLocal|rub.product.transferThirdParties|rub.accounts.transferBatch|rub.creditcard.pay|rub.loan.pay|rub.payment.billDiscount|rub.payment.payBPS|rub.payment.payCustoms|rub.payment.payDGI|rub.payment.payIMC|rub.payment.payIMF|rub.payment.paySalary|rub.payment.paySuppliers", "", "Rubicon", "0", "0"});

        fields = new String[]{"value"};
        update("configuration", fields, new String[]{"${BASE_URL}/invitation/accept"}, "id_field='rub.administration.invitation.acceptUrl'");
        update("configuration", fields, new String[]{"${BASE_URL}/invitation/reject"}, "id_field='rub.administration.invitation.rejectUrl'");
        update("configuration", fields, new String[]{"48h"}, "id_field='rub.administration.invitation.validity'");
    }
}