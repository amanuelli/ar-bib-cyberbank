/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Diego Curbelo
 */
public class DB201108181555_10922 extends DBVSUpdate {

    @Override
    public void up() {
        insert("activities", new String[]{"id_activity", "version", "enabled", "component_fqn"},
                new String[]{"rub.users.autocomplete", "1", "1", "com.technisys.rubicon.business.administration.users.activities.AutocompleteUsersActivity"});

    }
}