package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author marcelobruno
 */
public class DB20180712_1400_4015 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_messages", new String[]{"value"}, new String[]{"El campo Nombre no puede estar vacío"}, "id_message='fields.additionalCreditCardRequest.name.requiredError' AND id_form='additionalCreditCardRequest' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"El campo Apellido no puede estar vacío"}, "id_message='fields.additionalCreditCardRequest.lastName.requiredError' AND id_form='additionalCreditCardRequest' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"El límite de compra debe ser mayor a cero y no superar el límite de la tarjeta"}, "id_message='fields.additionalCreditCardRequest.amount.requiredError' AND id_form='additionalCreditCardRequest' AND lang='es'");
        delete("form_field_messages", "id_form='additionalCreditCardRequest' AND id_message='fields.additionalCreditCardRequest.amount.help'");
        
        //Validacion de caracteres especiales para Nombre y Apeelido
        update("form_field_text", new String[]{"id_validation"}, new String[]{"withoutSpecialChars"}, "id_field='lastName' AND id_form='additionalCreditCardRequest'");
        update("form_field_text", new String[]{"id_validation"}, new String[]{"withoutSpecialChars"}, "id_field='name' AND id_form='additionalCreditCardRequest'");
        
        update("form_field_text", new String[]{"max_length"}, new String[]{"100"}, "id_field='lastName' AND id_form='additionalCreditCardRequest'");
        update("form_field_text", new String[]{"max_length"}, new String[]{"100"}, "id_field='name' AND id_form='additionalCreditCardRequest'");
    }
    
}
