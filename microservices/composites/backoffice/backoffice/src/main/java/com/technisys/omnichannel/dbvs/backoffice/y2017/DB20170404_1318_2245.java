/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2245
 *
 * @author deybi
 */
public class DB20170404_1318_2245 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"validations"}, new String[]{"notEmpty||interval"}, "id_field='session.timeBeforeTimeoutNotification'");
        
   }
}
