/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */


package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author cherta
 */
public class DB20190111_1533_5585 extends DBVSUpdate {
    @Override
    public void up() {
        deleteConfiguration("mobile.version");
        insertOrUpdateConfiguration("mobile.minSupportedVersion", "1.0", ConfigurationGroup.TECNICAS, "frontend", new String[]{ "notEmpty" });
    }
}
