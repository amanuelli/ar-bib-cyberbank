/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3790
 *
 * @author isilveira
 */
public class DB20180126_1234_3790 extends DBVSUpdate {

    @Override
    public void up() {
        if (DBVS.DIALECT_MSSQL.equals(getDialect())) {
            insertOrUpdateConfiguration("client.emailValidationFormat", "^[a-zA-Z0-9+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty"});
        }
    }
}