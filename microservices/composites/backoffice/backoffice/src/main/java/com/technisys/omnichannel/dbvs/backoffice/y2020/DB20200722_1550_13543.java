/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author KevinGaray
 */

public class DB20200722_1550_13543 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("enrollment.irs");

        deleteActivity("pendingActions.irs");
        insertActivity("pendingActions.irs", "com.technisys.omnichannel.client.activities.pendingactions.IRSActivity", null, ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        deleteActivity("pendingActions.pep");
        insertActivity("pendingActions.pep", "com.technisys.omnichannel.client.activities.pendingactions.PEPActivity", null, ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "core.authenticated");

    }

}