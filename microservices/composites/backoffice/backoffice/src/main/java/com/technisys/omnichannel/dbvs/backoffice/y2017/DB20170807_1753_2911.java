/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author danireb
 */
public class DB20170807_1753_2911 extends DBVSUpdate {

    @Override
    public void up() {
        
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"}, new String[]{"exteriorBank", "frequentDestination", "1", "ABA"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"}, new String[]{"exteriorBank", "frequentDestination", "1", "SWIFT"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"}, new String[]{"exteriorBank", "frequentDestination", "1", "BLZ"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"}, new String[]{"exteriorBank", "frequentDestination", "1", "CHIPS"});
        
    }
    
}
