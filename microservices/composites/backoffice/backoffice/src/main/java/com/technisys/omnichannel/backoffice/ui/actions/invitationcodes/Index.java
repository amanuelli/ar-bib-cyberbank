/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.opensymphony.xwork2.ActionSupport;
import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.ListPreResponse;
import com.technisys.omnichannel.core.domain.InvitationCodesStatus;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;

import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 * @author Sebastian Barbosa
 */
@Action(results = { @Result(name = "success", location = "invitationcodes", type = "tiles") })
public class Index extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    private static final long serialVersionUID = 1L;

    private List<InvitationCodesStatus> statusList;
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public String execute() throws Exception {
        IBRequest request = new IBRequest();
        UIUtils.prepareRequest(request, httpRequest);
        request.setIdActivity("backoffice.invitationCodes.listpre");

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        if (ReturnCodes.OK.equals(response.getReturnCode())) {
            ListPreResponse statusResponse = (ListPreResponse) response;
            statusList = statusResponse.getStatusList();
            return SUCCESS;
        } else {
            throw new JSONException(response);
        }
    }

    public List<InvitationCodesStatus> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<InvitationCodesStatus> statusList) {
        this.statusList = statusList;
    }

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }

}
