/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author jbaccino
 */
public class DB20170525_1109_2198 extends DBVSUpdate {

    @Override
    public void up() {
        // Cleanup
        delete("transactions", "id_form = 'tranfer_local'");
        delete("forms", "id_form = 'tranfer_local'");
        delete("permissions", "id_permission='client.form.tranfer_local.send'");

        // Permisos y actividades
        delete("permissions", "id_permission='transfer.local'");
        insert("permissions", new String[]{"id_permission"}, new String[]{"transfer.local"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"transfer.local", "accessToken-pin"});

        deleteActivity("transfers.local.preview");
        deleteActivity("transfers.local.send");

        insertActivity("transfers.local.preview", "com.technisys.omnichannel.activities.forms.PreviewFormActivity", "transferLocal", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("transfers.local.send", "com.technisys.omnichannel.activities.forms.SendFormActivity", "transferLocal", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, "transfer.local", "debitAccount");
        insert("activity_caps", new String[]{"id_activity", "id_field_amount", "id_field_product"}, new String[]{"transfers.local.send", "amount", "debitAccount"});

        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"transfer.local", "transfers", null, null, "0", "17", "transfers", null, null, "0", "17", "transfers", null, null, "1", "17", "CA,CC", null, null});

        // Formulario de Transferencia interbancaria
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String idForm = "transferLocal";
        String version = "1";

        delete("forms", "id_form = '" + idForm + "'");

        insert("forms", new String[]{"id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow"},
                new String[]{idForm, version, "1", "transfers", "activity", "demo:1:3", "transfers", "transfers.local.send", "1", "1", "1", "1", "1"});

        //Campos
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};

        int ordinal = 1;
        Map<String, String> messages = new HashMap();

        // Datos de débito (sectionDebitData)
        insert("form_fields", formFields, new String[]{"sectionDebitData", idForm, version, "sectiontitle", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});

        messages.put("sectionDebitData.label", "Datos de débito");

        // Cuenta débito (debitAccount)
        insert("form_fields", formFields, new String[]{"debitAccount", idForm, version, "productselector", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option", "show_other_permission"},
                new String[]{"debitAccount", idForm, version, "field-normal", "0", null});

        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"debitAccount", idForm, version, "CC"});
        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"debitAccount", idForm, version, "CA"});

        insert("form_field_ps_permissions", new String[]{"id_field", "id_form", "form_version", "id_permission"},
                new String[]{"debitAccount", idForm, version, "transfer.local"});

        messages.put("debitAccount.label", "Cuenta débito");
        messages.put("debitAccount.requiredError", "Debe seleccionar una cuenta débito");
        messages.put("debitAccount.hint", "Todas las comisiones se debitarán de la cuenta seleccionada, si lo desea puede cargar los gastos a otra cuenta");

        // Cuenta débito de gastos  (expenseAccount)
        insert("form_fields", formFields, new String[]{"expenseAccount", idForm, version, "productselector", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option", "show_other_permission"},
                new String[]{"expenseAccount", idForm, version, "field-normal", "0", null});

        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"expenseAccount", idForm, version, "CC"});
        insert("form_field_ps_product_types", new String[]{"id_field", "id_form", "form_version", "id_product_type"},
                new String[]{"expenseAccount", idForm, version, "CA"});

        insert("form_field_ps_permissions", new String[]{"id_field", "id_form", "form_version", "id_permission"},
                new String[]{"expenseAccount", idForm, version, "transfer.internal"});

        messages.put("expenseAccount.label", "Cuenta débito de gastos");

        // Importe (amount)
        insert("form_fields", formFields, new String[]{"amount", idForm, version, "amount", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"},
                new String[]{"amount", idForm, version, "field-normal", "1"});

        messages.put("amount.help", "Se realizarán los cambios de moneda necesarios en función de las monedas de las cuentas de débito y la moneda de la transferencia.");
        messages.put("amount.label", "Importe");
        messages.put("amount.requiredError", "Debe ingresar un importe");

        // Referencia origen (debitReference)
        insert("form_fields", formFields, new String[]{"debitReference", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"debitReference", idForm, version, "0", "20", "field-normal", null});

        messages.put("debitReference.help", "Texto que le ayude a identificar su transferencia.");
        messages.put("debitReference.hint", null);
        messages.put("debitReference.label", "Referencia origen");

        // Datos de crédito (sectionCreditData)
        insert("form_fields", formFields, new String[]{"sectionCreditData", idForm, version, "sectiontitle", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});

        messages.put("sectionCreditData.label", "Datos de crédito");

        // Banco destino (creditBank)
        insert("form_fields", formFields, new String[]{"creditBank", idForm, version, "bankselector", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_bankselector", new String[]{"id_field", "id_form", "form_version", "display_type"},
                new String[]{"creditBank", idForm, version, "field-normal"});

        messages.put("creditBank.requiredError", "Debe seleccionar un banco");
        messages.put("creditBank.help", null);
        messages.put("creditBank.hint", null);
        messages.put("creditBank.label", "Banco destino");

        // Cuenta crédito (creditAccountNumber)
        insert("form_fields", formFields, new String[]{"creditAccountNumber", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"creditAccountNumber", idForm, version, "0", "20", "field-normal", null});

        messages.put("creditAccountNumber.help", "Ingrese el número de cuenta completo en el banco destino.");
        messages.put("creditAccountNumber.hint", null);
        messages.put("creditAccountNumber.label", "Cuenta crédito");
        messages.put("creditAccountNumber.requiredError", "Debe ingresar un número de cuenta");

        // Nombre y apellido del destinatario (beneficiaryName)
        insert("form_fields", formFields, new String[]{"beneficiaryName", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"beneficiaryName", idForm, version, "0", "50", "field-normal", null});

        messages.put("beneficiaryName.help", null);
        messages.put("beneficiaryName.hint", null);
        messages.put("beneficiaryName.label", "Nombre y apellido del destinatario");
        messages.put("beneficiaryName.requiredError", "Debe ingresar el nombre y apellido del destinatario");

        
        // Documento del destinatario (beneficiaryDocument)
        insert("form_fields", formFields, new String[]{"beneficiaryDocument", idForm, version, "document", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_document", new String[]{"id_field", "id_form", "form_version", "default_country", "default_document_type"},
                new String[]{"beneficiaryDocument", idForm, version, null, null});

        messages.put("beneficiaryDocument.help", null);
        messages.put("beneficiaryDocument.hint", null);
        messages.put("beneficiaryDocument.label", "Documento del destinatario");

        
        // Referencia destino (creditReference)
        insert("form_fields", formFields, new String[]{"creditReference", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"creditReference", idForm, version, "0", "20", "field-normal", null});

        messages.put("creditReference.help", "Texto que le ayude al destinatario a identificar su transferencia.");
        messages.put("creditReference.hint", null);
        messages.put("creditReference.label", "Referencia destino");
        
        
        // Información de notificación (sectionNotificationInfo)
        insert("form_fields", formFields, new String[]{"sectionNotificationInfo", idForm, version, "sectiontitle", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});

        messages.put("sectionNotificationInfo.label", "Información de notificación");

        
        // Emails de notificación (notificationEmails)
        insert("form_fields", formFields, new String[]{"notificationEmails", idForm, version, "emaillist", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_emaillist", new String[]{"id_field", "id_form", "form_version", "display_type"},
                new String[]{"notificationEmails", idForm, version, "field-normal"});

        messages.put("notificationEmails.help", "E-mails a notificar el envío de la transferencia.");
        messages.put("notificationEmails.hint", null);
        messages.put("notificationEmails.label", "Emails de notificación");

        // Cuerpo de notificación (notificationBody)
        insert("form_fields", formFields, new String[]{"notificationBody", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_textarea", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"},
                new String[]{"notificationBody", idForm, version, "0", "500", "field-normal"});

        messages.put("notificationBody.help", null);
        messages.put("notificationBody.hint", null);
        messages.put("notificationBody.label", "Cuerpo de notificación");

        
        //Insertado de Mensajes
        //*********************************************************************
        String[] formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        String[] formFieldsValues = new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Transferencia interbancaria", date};

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form,version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "', TO_DATE('"+date+"', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formField, formFieldsValues);
        }

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, date, messages.get(key)};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form,form_version, modification_date,value) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "', TO_DATE('"+formFieldsValues[5]+"', 'YYYY-MM-DD HH24:MI:SS'),'"+formFieldsValues[6]+"')");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

    }
}
