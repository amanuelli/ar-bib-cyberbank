/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6765
 *
 * @author pbregonzio
 */

public class DB20190401_1447_6913 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"value"};
        String[] fieldValue = new String[]{"Seleccione la forma de carga de datos"};
        update("form_field_messages", fieldNames, fieldValue,"id_form='salaryPayment' AND id_message='fields.salaryPayment.uploadBy.label' AND lang='es'");
        fieldValue = new String[]{"Select the type of entered data"};
        update("form_field_messages", fieldNames, fieldValue,"id_form='salaryPayment' AND id_message='fields.salaryPayment.uploadBy.label' AND lang='en'");

        fieldValue = new String[]{"Mediante achivo"};
        update("form_field_messages", fieldNames, fieldValue,"id_form='salaryPayment' AND id_message='fields.salaryPayment.uploadBy.option.file' AND lang='es'");
        fieldValue = new String[]{"Through a file"};
        update("form_field_messages", fieldNames, fieldValue,"id_form='salaryPayment' AND id_message='fields.salaryPayment.uploadBy.option.file' AND lang='en'");

        fieldValue = new String[]{"Manualmente"};
        update("form_field_messages", fieldNames, fieldValue,"id_form='salaryPayment' AND id_message='fields.salaryPayment.uploadBy.option.inputManually' AND lang='es'");
        fieldValue = new String[]{"Manually"};
        update("form_field_messages", fieldNames, fieldValue,"id_form='salaryPayment' AND id_message='fields.salaryPayment.uploadBy.option.inputManually' AND lang='en'");
    }
}
