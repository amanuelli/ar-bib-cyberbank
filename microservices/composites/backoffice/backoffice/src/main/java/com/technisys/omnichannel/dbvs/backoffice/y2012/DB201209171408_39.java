/*
 *  Copyright 2012 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 * @author Diego Curbelo &lt;dcurbelo@technisys.com&gt;
 */
public class DB201209171408_39 extends DBVSUpdate {

    @Override
    public void up() {
        ColumnDefinition[] columns = new ColumnDefinition[]{
            new ColumnDefinition("id_row", Types.INTEGER, 4, 0, false, null, true),
            new ColumnDefinition("list_name", Types.VARCHAR, 254),
            new ColumnDefinition("amount", Types.DECIMAL, 15, 3, false, "0"),
            new ColumnDefinition("credit_account", Types.VARCHAR, 254),
            new ColumnDefinition("reference", Types.VARCHAR, 100),
            new ColumnDefinition("concept", Types.VARCHAR, 254),
            new ColumnDefinition("error_description", Types.VARCHAR, 254),
            new ColumnDefinition("currency", Types.VARCHAR, 10)
        };
        createTable("payment_lines", columns, new String[]{"id_row"});
        createIndex("payment_lines", "idx_payment_lines_list", new String[]{"list_name"});
    }
}