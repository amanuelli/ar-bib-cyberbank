/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author pbanales
 */

public class DB20191211_1227_8967 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("productrequest.onboarding.uploadBackDocument");
        deleteActivity("productrequest.onboarding.uploadFrontDocument");
        deleteActivity("productrequest.onboarding.uploadSelfie");
    }

}
