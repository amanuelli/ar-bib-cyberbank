/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Diego Curbelo
 */
public class DB201108181741_10919 extends DBVSUpdate {

    @Override
    public void up() {

        String[] fieldNames = new String[]{"possible_values"};
        String[] fieldValues = new String[]{"30 días|60 días|90 días|180 días|360 días"};
        update("form_fields", fieldNames, fieldValues, "id_field= 'plazo' AND id_form = 1");

        fieldNames = new String[]{"possible_values"};
        fieldValues = new String[]{"36 meses|48 meses"};
        update("form_fields", fieldNames, fieldValues, "id_field= 'plazo' AND id_form = 4");

        fieldNames = new String[]{"possible_values"};
        fieldValues = new String[]{"30 días|60 días|90 días|180 días|360 días"};
        update("form_fields", fieldNames, fieldValues, "id_field= 'plazo' AND id_form = 5");

    }
}