/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * 
 * @author fpena
 */
public class DB20170621_1702_2146 extends DBVSUpdate {

    @Override
    public void up() {
        
        deleteActivity("onboarding.wizard.uploadBackDocument");
        deleteActivity("onboarding.wizard.uploadFrontDocument");
        deleteActivity("onboarding.wizard.uploadSelfie");
        deleteActivity("onboarding.wizard.step5");
        
        insertActivity("onboarding.wizard.uploadBackDocument", "com.technisys.omnichannel.client.activities.onboarding.UploadBackDocumentActivity", "onboarding", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("onboarding.wizard.uploadFrontDocument", "com.technisys.omnichannel.client.activities.onboarding.UploadFrontDocumentActivity", "onboarding", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("onboarding.wizard.uploadSelfie", "com.technisys.omnichannel.client.activities.onboarding.UploadSelfieActivity","onboarding", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("onboarding.wizard.step5", "com.technisys.omnichannel.client.activities.onboarding.Step5Activity", "onboarding", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
    }

}
