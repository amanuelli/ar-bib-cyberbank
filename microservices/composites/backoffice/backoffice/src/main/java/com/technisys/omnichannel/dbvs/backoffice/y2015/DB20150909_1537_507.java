/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150909_1537_507 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("invitation.notification.transport", "SMS");
        updateConfiguration("administration.users.invite.enrollmentUrl", "${BASE_URL}/enrollment/start");

        //--------------------
        // Safeway
        updateConfiguration("safeway.clientSystem", "rubomnichannel");

        update("configuration",
                new String[]{"value", "encrypted", "must_be_encrypted"},
                new String[]{"Passw0rd", "1", "1"},
                "id_field='safeway.clientSystemPassword'");

        updateConfiguration("safeway.dateFormat", "dd/MM/yyyy HH:mm:ss");
        updateConfiguration("safeway.domainType", "RUBOMNICHANNEL");
        updateConfiguration("safeway.keyType", "ClaveRUBOmnichannel");
        updateConfiguration("safeway.pinKeyType", "ClavePINRUBOmnichannel");
        updateConfiguration("safeway.otps.applicationName", "");
        updateConfiguration("safeway.otps.info", "");
        updateConfiguration("safeway.otps.softTokenValidationType", "OTPMOBILE");
        updateConfiguration("safeway.otps.tokenValidationType", "VASCO");
        updateConfiguration("safeway.otps.url", "http://server:8080/safeway/com/technisys/safeway/OTPs.jws?wsdl");
        updateConfiguration("safeway.otps.validForXYears", "50");
        updateConfiguration("safeway.secretKeys.url", "http://server:8080/safeway/com/technisys/safeway/SecretKeys.jws?wsdl");
        updateConfiguration("safeway.secretKeys.validForXYears", "50");
        updateConfiguration("safeway.secretKeys.encryptionAlgorithm", "AES");
        updateConfiguration("safeway.secretKeys.encryptionAlgorithmForCipher", "AES/ECB/PKCS5Padding");

        update("configuration",
                new String[]{"value", "encrypted", "must_be_encrypted"},
                new String[]{"e23f381c2ad6a95a58e4307fd330dddc", "1", "1"},
                "id_field='safeway.secretKeys.encryptionKey'");

        updateConfiguration("credential.password.componentFQN", "com.technisys.omnichannel.credentials.safeway.PasswordPlugin");
        updateConfiguration("credential.pin.componentFQN", "com.technisys.omnichannel.credentials.safeway.PINPlugin");

        // Cambio de credenciales en actividades
        insert("credential_groups", new String[]{"id_credential_group", "id_credential", "ordinal"}, new String[]{"accessToken-pin", "accessToken", "1"});
        insert("credential_groups", new String[]{"id_credential_group", "id_credential", "ordinal"}, new String[]{"accessToken-pin", "pin", "2"});

        update("permissions_credentials_groups", new String[]{"id_credential_group"}, new String[]{"accessToken-pin"}, "id_credential_group='accessToken-otp'");
        update("permissions_credentials_groups", new String[]{"id_credential_group"}, new String[]{"accessToken-pin"}, "id_permission='administration.manage'");
        
        update("activity_products", new String[]{"id_permission"}, new String[]{"core.loginWithPIN"}, "id_activity in ('session.recoverPasswordWithSecondFactor.step1', 'session.loginWithPasswordAndSecondFactor.step1')");

        insertOrUpdateConfiguration(
                "enrollment.newUser.enabledChannels",
                "frontend|phonegap",
                null, "enrollment", new String[]{"notEmpty"});
    }
}
