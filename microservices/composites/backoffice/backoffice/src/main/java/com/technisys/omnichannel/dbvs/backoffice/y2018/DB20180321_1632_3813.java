/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * 
 * @author rdosantos
 */
public class DB20180321_1632_3813 extends DBVSUpdate {

    @Override
    public void up() {
        update("permissions_credentials_groups", new String[]{"id_credential_group"}, new String[]{"accessToken"}, "id_permission = 'user.preferences'");
    }
}