/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20151030_1309_956 extends DBVSUpdate {

    @Override
    public void up() {
        update("activities", new String[]{"id_group"}, new String[]{""}, "id_activity='misc.listDocumentTypes'");
        delete("activities", "id_activity='core.updateTransactionTemplate'");
    }
}
