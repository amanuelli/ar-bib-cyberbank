/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107271225_10298 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT forms ON");
        String[] fieldNames = new String[]{"id_form", "form_name", "description", "enabled", "location", "id_jbpm_process", "template_es"};
        String[] fieldValues = new String[]{"85", "Solicitud de ambiente", "Solicite un ambiente", "1", "others", "demo-1", "<form:form><form:section><form:field idField=\"environmentName\"/><form:field idField=\"environmentReason\"/></form:section><div class=\"dotted_separator\">&nbsp;</div><form:section><form:field idField=\"productsSubtitle\"/><form:field idField=\"product1\"/><form:field idField=\"product2\"/><form:field idField=\"product3\"/><form:field idField=\"product4\"/><form:field idField=\"product5\"/></form:section></form:form>"};
        insert("forms", fieldNames, fieldValues);
        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT forms OFF");

        fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value", "possible_values"};

        fieldValues = new String[]{"environmentName", "85", "short_text", "1", "", ""};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"environmentReason", "85", "long_text", "1", "", ""};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"productsSubtitle", "85", "subtitle", "0", "Productos", ""};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"product1", "85", "productList", "0", "", "CC|CA|TC|PI|PA|PF"};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"product2", "85", "productList", "0", "", "CC|CA|TC|PI|PA|PF"};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"product3", "85", "productList", "0", "", "CC|CA|TC|PI|PA|PF"};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"product4", "85", "productList", "0", "", "CC|CA|TC|PI|PA|PF"};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"product5", "85", "productList", "0", "", "CC|CA|TC|PI|PA|PF"};
        insert("form_fields", fieldNames, fieldValues);
    }
}