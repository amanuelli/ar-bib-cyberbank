package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
*
* @author danireb
*/
public class DB20170821_1733_3065 extends DBVSUpdate {    
    
    @Override
    public void up() {
        
        delete("form_field_messages", "value is null");
        delete("form_field_messages", "value like 'null'");
    
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            
            String idForm = "frequentDestination";
            String idField = "productType";
            String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
            String version = "1";
            
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "UPDATE form_field_messages SET value = 'Tarjeta de crédito' WHERE id_message = 'fields." + idForm + "." + idField + ".option.creditCard' and lang = 'es'");
            
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".option.externalCreditCard', 'es', '"+idField+"','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Tarjeta de crédito otro banco')");
        }
    }

}
