/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Marcelo Bruno
 */

public class DB20191002_1426_7972 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("onboarding.mode", "digital", ConfigurationGroup.NEGOCIO, "onboarding", new String[]{"notEmpty"}, "digital|safeway");
        insertOrUpdateConfiguration("safeway.apirest.baseurl", "http://10.20.20.27:8080/safeway/api", ConfigurationGroup.SEGURIDAD, "others", new String[]{"notEmpty||url"});
    }

}