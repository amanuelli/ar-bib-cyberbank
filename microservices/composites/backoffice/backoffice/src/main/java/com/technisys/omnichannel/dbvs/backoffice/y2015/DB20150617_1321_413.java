/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author aelters
 */
public class DB20150617_1321_413 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("creditCard.listStatementDetails", "com.technisys.omnichannel.client.activities.creditcards.ListStatementDetailsActivity", "creditCards", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idCreditCard");
    }
}
