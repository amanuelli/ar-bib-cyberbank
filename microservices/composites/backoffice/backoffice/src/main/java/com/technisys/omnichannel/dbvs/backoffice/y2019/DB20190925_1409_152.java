/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ahernandez
 * <p>
 * Relared issue CDPCORE-152
 */

public class DB20190925_1409_152 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("enrollment.cyberbank.account.productId", "01000", ConfigurationGroup.TECNICAS, "cyberbankcore", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("enrollment.cyberbank.account.subProductId", "10001", ConfigurationGroup.TECNICAS, "cyberbankcore", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("enrollment.cyberbank.account.currency", "1", ConfigurationGroup.TECNICAS, "cyberbankcore", new String[]{"notEmpty"});
    }

}