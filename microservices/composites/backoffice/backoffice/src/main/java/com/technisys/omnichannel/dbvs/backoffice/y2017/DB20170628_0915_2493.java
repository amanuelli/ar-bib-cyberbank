/* 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2493
 *
 * @author pantusap
 */
public class DB20170628_0915_2493 extends DBVSUpdate {

    @Override
    public void up() {

        insertOrUpdateConfiguration("backoffice.forms.categories", "accounts|deposits|loans|creditcards|payments|transfers|comex|others", ConfigurationGroup.NEGOCIO, null, new String[]{});

        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile" };
        String[] formFieldsReadOnly = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile", "read_only" };
        String[] formFieldsValues;
        String idForm = "modifyCreditLetter";
        String version = "1";

        Map<String, String> messages = new HashMap();
        Map<String, String> messagesPT = new HashMap();
        Map<String, String> messagesEN = new HashMap();

        /**** Insert Form - Modificación de carta de crédito ****/
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[] { "id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable" };
        String[] formsFieldsValues = new String[] { idForm, version, "1", "comex", "process", "demo:1:3", "comex", null, "1", "1", "1" };
        insert("forms", formsFields, formsFieldsValues);

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "', '" + version
                    + "', 'es', 'Modificación de carta de crédito', TO_DATE('2017-0623 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value", "modification_date" };
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "es", "Modificación de carta de crédito ", date });
        }

        insert("permissions", new String[] { "id_permission" }, new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

        /*
         * MANNAZCA 2493 (+)
         */
        int ordinal = 1;
        /**** Insert field text - Carta de credito ****/
        formFieldsValues = new String[] { "creditLetter", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "creditLetter", idForm, version, "0", "30", "field-medium", null });
        messages.put("creditLetter.label", "Carta de crédito");
        messages.put("creditLetter.requiredError", "Debe ingresar una carta de crédito");

        /**** Insert field selector - Incremento/Disminucion ****/
        formFieldsValues = new String[] { "selectCreditLetter", idForm, version, "selector", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as" }, new String[] { "selectCreditLetter", idForm, version, "field-normal", null, "0", "combo" });
        messages.put("selectCreditLetter.label", "Carta de Credito");

        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "selectCreditLetter", idForm, version, "carta_1" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "selectCreditLetter", idForm, version, "carta_2" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "selectCreditLetter", idForm, version, "carta_3" });

        messages.put("selectCreditLetter.option.carta_1", "Carta 1");
        messages.put("selectCreditLetter.option.carta_2", "Carta 2");
        messages.put("selectCreditLetter.option.carta_3", "Carta 3");

        /**** Insert field text - Nota ****/
        formFieldsValues = new String[] { "note", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1", "1" };
        insert("form_fields", formFieldsReadOnly, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "note", idForm, version, "0", "500", "field-medium", null });
        messages.put("note.label", "Nota");

        /**** Insert field text - fecha de vencimiento ****/
        insert("form_fields", formFields, new String[] { "dueDate", idForm, version, "date", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1" });
        insert("form_field_date", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { "dueDate", idForm, version, "field-small" });
        messages.put("dueDate.label", "Nueva fecha de vencimiento");

        /**** Insert field text - fecha de embarque ****/
        insert("form_fields", formFields, new String[] { "shippingDate", idForm, version, "date", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" });
        insert("form_field_date", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { "shippingDate", idForm, version, "field-small" });

        messages.put("shippingDate.label", "Nueva fecha de embarque");

        /**** Insert field selector - Incremento/Disminucion ****/
        formFieldsValues = new String[] { "increaseDecrease", idForm, version, "selector", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as" }, new String[] { "increaseDecrease", idForm, version, "field-normal", null, "1", "combo" });
        messages.put("increaseDecrease.label", "Incremento/Disminución");

        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "increaseDecrease", idForm, version, "incremento" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "increaseDecrease", idForm, version, "disminucion" });

        messages.put("increaseDecrease.option.incremento", "Incremento");
        messages.put("increaseDecrease.option.disminucion", "Disminución");

        /**** Insert field amount - Monto ****/
        // formFieldsValues = new String[]{"amount", idForm, version, "amount",
        // String.valueOf(ordinal++), "value(increaseDecrease) != '' ",
        // "value(increaseDecrease) != '' ", "default", "0"};
        formFieldsValues = new String[] { "amount", idForm, version, "amount", String.valueOf(ordinal++), "TRUE", "FALSE ", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[] { "id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount" }, new String[] { "amount", idForm, version, "field-normal", "0", "0" });

        messages.put("amount.label", "Límite de compra");
        messages.put("amount.requiredError", "Debe ingresar un monto mayor a cero");

        /**** Insert field textarea - Otras modificaciones ****/
        insert("form_fields", formFields, new String[] { "otherModifications", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" });
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { "otherModifications", idForm, version, "0", "500", "field-normal" });

        messages.put("otherModifications.help", null);
        messages.put("otherModifications.hint", null);
        messages.put("otherModifications.label", "Otras modificaciones");

        /**** Insert field text - Adjuntos ****/
        formFieldsValues = new String[] { "attachments", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1", "1" };
        insert("form_fields", formFieldsReadOnly, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "attachments", idForm, version, "0", "500", "field-medium", null });
        messages.put("attachments.label", "Adjuntos");

        // FALTA EL tipo link

        /**** Insert field textarea - Otras modificaciones ****/
        insert("form_fields", formFields, new String[] { "observation", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" });
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { "observation", idForm, version, "0", "500", "field-normal" });

        messages.put("observation.help", null);
        messages.put("observation.hint", null);
        messages.put("observation.label", "Observaciones");

        /**** Insert field text - Disclaimer ****/

        formFieldsValues = new String[] { "disclaimer", idForm, version, "termsandconditions", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label" }, new String[] { "disclaimer", idForm, version, "field-normal", "0", "0" });

        messages.put("disclaimer.label", "Disclaimer");
        messages.put("disclaimer.requiredError", "Debe aceptar los términos y condiciones para continuar");
        messages.put("disclaimer.showAcceptOptionText", "Aceptar");
        messages.put("disclaimer.termsAndConditions", "Las instrucciones enviadas al banco por usted luego de las 18 hs. serán procesadas al siguiente día hábil bancario.Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar."
                + "Sujeto a aprobación crediticia del banco Su instrucción quedará completada una vez recibidos la totalidad de los documentos correspondientes en nuestras sucursales y se haya enviado la presente debidamente firmada.");

        insert("form_fields", formFields, new String[] { "line", idForm, version, "horizontalrule", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" });
        messages.put("line.label", "");
        messagesPT.put("line.label", "");
        messagesEN.put("line.label", "");

        // Emails de notificación (notificationEmails)
        insert("form_fields", formFields, new String[] { "notificationEmails", idForm, version, "emaillist", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" });
        insert("form_field_emaillist", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { "notificationEmails", idForm, version, "field-normal" });

        messages.put("notificationEmails.help", "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");

        messages.put("notificationEmails.label", "Emails de notificación");

        /**** Insert field textarea - Cuerpo de notificación ****/
        insert("form_fields", formFields, new String[] { "notificationBody", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" });
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { "notificationBody", idForm, version, "0", "500", "field-normal" });

        messages.put("notificationBody.help", null);
        messages.put("notificationBody.hint", null);

        messages.put("notificationBody.label", "Cuerpo de notificación");

        String[] formFieldsMessages = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date" };

        for (String key : messages.keySet()) {
            formFieldsValues = new String[] { "fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date };

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) " + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3]
                        + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }

}
