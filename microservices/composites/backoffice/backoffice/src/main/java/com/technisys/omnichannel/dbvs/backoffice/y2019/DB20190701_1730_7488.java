/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author Marcelo Bruno
 */

public class DB20190701_1730_7488 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("transactions.get.pending.quantity", "com.technisys.omnichannel.client.activities.transactions.RefreshPendingTransactionsQuantityActivity", "communications", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

        insertOrUpdateConfiguration("transactions.pending.refreshRate", "60", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty", "integer"}, null, "frontend");
        insertOrUpdateConfiguration("feature.transactions.refreshPendingQuantity", "true", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty"}, "true|false", "frontend");

    }

}