/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150702_1536_415 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.users.modify.pre", "com.technisys.omnichannel.client.activities.administration.users.ModifyUsersPreActivity", "administration.users", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");

        insertActivity("administration.users.modify.preview", "com.technisys.omnichannel.client.activities.administration.users.ModifyUsersPreviewActivity", "administration.users", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.users.modify.send", "com.technisys.omnichannel.client.activities.administration.users.ModifyUsersActivity", "administration.users", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

    }
}
