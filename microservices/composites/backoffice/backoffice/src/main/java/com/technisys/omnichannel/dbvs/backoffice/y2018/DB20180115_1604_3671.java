/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3671
 *
 * @author msouza
 */
public class DB20180115_1604_3671 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_field", "value", "id_group"};
        String[] fieldValues = new String[]{"frontend.desktopDateFormat", "HH:mm:ss - d MMM yyyy", "Frontend"};
        insert("configuration", fieldNames, fieldValues);
        
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='frontend.desktopDateFormat'");

    }
}