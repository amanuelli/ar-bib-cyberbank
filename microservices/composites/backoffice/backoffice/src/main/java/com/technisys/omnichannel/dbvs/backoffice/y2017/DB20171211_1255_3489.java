/**
 * Copyright 2017 Technisys S.A.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written
 * consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3489
 *
 * @author Héctor Menéndez &lt;hmenendez@technisys.com&gt;
 */
public class DB20171211_1255_3489 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration( "core.activities.dontExtendSession", "widgets.notifications", null, "others", new String[]{});
    }
}
