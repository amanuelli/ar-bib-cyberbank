/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2021;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Jhossept
 */

public class DB20210112_1031_14532 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("session.recoverPin.step1");
        deleteActivity("session.recoverPin.step2");
        deleteActivity("session.recoverPin.step3");
        deleteActivity("session.recoverPinAndPassword.step1");
        deleteActivity("session.recoverPinAndPassword.step2");
        deleteActivity("session.recoverPinAndPassword.step3");
    }

}