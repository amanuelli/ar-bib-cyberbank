/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150605_1335_463 extends DBVSUpdate {

    @Override
    public void up() {
        //Borramos configuracion y mensajes 
        delete("configuration", "id_field like 'rubicon.%' OR id_field like 'rub.%'");
        deleteConfiguration("rubicon_accounts_max_movements");
        insertOrUpdateConfiguration("core.captcha.maxFailedAttempts", "2", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("core.captcha.privateKey", "6LcYKMsSAAAAACFDuz0X9fvV1mOyKOvXVoZwOab_", ConfigurationGroup.SEGURIDAD, null, new String[]{});
    }
}
