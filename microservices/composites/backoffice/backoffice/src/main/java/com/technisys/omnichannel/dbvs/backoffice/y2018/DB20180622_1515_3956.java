/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.util.HashMap;
import java.util.Map;

public class DB20180622_1515_3956 extends DBVSUpdate {

    @Override
    public void up() {
        //Pago otro prestamo de techbank
        deleteActivity("pay.thirdPartiesLoan.preview");
        insertActivity("pay.thirdPartiesLoan.preview", "com.technisys.omnichannel.client.activities.pay.thirdpartiesloan.PayThirdPartiesLoanPreviewActivity", "thirdpartiesloan", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        deleteActivity("pay.thirdPartiesLoan.send");        
        insertActivity("pay.thirdPartiesLoan.send", "com.technisys.omnichannel.client.activities.pay.thirdpartiesloan.PayThirdPartiesLoanSendActivity", "thirdpartiesloan", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, null);
        
        String[] fieldNames = new String[]{"id_activity", "id_field", "id_permission"};
        String[] fieldValues = new String[]{"pay.thirdPartiesLoan.send", "debitAccount", "pay.loan.thirdParties"};
        insert("activity_products", fieldNames, fieldValues);
        
        fieldNames = new String[]{"id_activity", "id_field_amount", "id_field_product"};
        fieldValues = new String[]{"pay.thirdPartiesLoan.send", "amount", "debitAccount"};
        insert("activity_caps", fieldNames, fieldValues);
        
        //Borramos el formulario payThirdPartiesLoan por si existe, y creamos el nuevo que es Pago de tarjetas de terceros
        delete("forms", "id_form = 'payThirdPartiesLoan'");
        
        
        String[] formField = new String[]{"id_form", "category", "enabled", "type", "id_activity", "version", "schedulable"};
        String[] formFieldValues = new String[]{"payThirdPartiesLoan", "payments", "1", "activity", "pay.thirdPartiesLoan.send", "1", "1"};
        insert("forms", formField, formFieldValues);
        
        formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        formFieldValues = new String[]{"forms.thirdPartiesLoan.formName", "payThirdPartiesLoan", "1", "es", "Pago de préstamo de terceros", "2018-01-01 12:00:00"};
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2]  + "', '" + formFieldValues[3] + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert ("form_messages", formField, formFieldValues);
        }
        
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "sub_type", "ordinal", "visible", "required"};
        
        String[] formFieldsValues = new String[]{"loanNumber", "payThirdPartiesLoan", "1", "text", "default", "1", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"amount", "payThirdPartiesLoan", "1", "amount", "default", "2", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesLoan", "1", "productselector", "default", "3", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
                
        formFieldsValues = new String[]{"notificationEmails", "payThirdPartiesLoan", "1", "emaillist", "default", "7", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"notificationBody", "payThirdPartiesLoan", "1", "textarea", "default", "8", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "type", "sub_type", "ordinal", "visible", "required", "ticket_only"};
        
        formFieldsValues = new String[]{"debitAmount", "payThirdPartiesLoan", "1", "amount", "default", "4", "TRUE", "FALSE", "1"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"rate", "payThirdPartiesLoan", "1", "text", "default", "5", "TRUE", "FALSE", "1"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
        formFieldsValues = new String[]{"amount", "payThirdPartiesLoan", "1", "field-small", "1"};
        insert("form_field_amount", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type"};
        formFieldsValues = new String[]{"notificationEmails", "payThirdPartiesLoan", "1", "field-normal"};
        insert("form_field_emaillist", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option"};
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesLoan", "1", "field-normal", "0"};
        insert("form_field_ps", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesLoan", "1", "pay.creditCard.thirdParties"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesLoan", "1", "CA"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesLoan", "1", "CC"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        formFieldsValues = new String[]{"loanNumber", "payThirdPartiesLoan", "1", "5", "30", "field-normal", "onlyNumbers"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"notificationBody", "payThirdPartiesLoan", "1", "0", "500", "field-medium"};
        insert("form_field_textarea", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
        formFieldsValues = new String[]{"debitAmount", "payThirdPartiesLoan", "1", "field-small", "1"};
        insert("form_field_amount", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_currency"};
        formFieldsValues = new String[]{"amount", "payThirdPartiesLoan", "1", "222"};
        insert("form_field_amount_currencies", formFields, formFieldsValues);
                
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"rate", "payThirdPartiesLoan", "1", "0", "50", "field-normal"};
        insert("form_field_text", formFields, formFieldsValues);
        
        
        //Solo doy de alta español
        Map<String, String> messages = new HashMap();
        messages.put("amount.label","Monto a pagar");
        messages.put("loanNumber.label","Préstamo");
        messages.put("debitAccount.label","Cuenta de origen");
        messages.put("notificationBody.label","Mensaje");
        messages.put("notificationEmails.label","Correo electrónico para notificaciones");
        messages.put("rate.label","Cotización aplicada");
        messages.put("debitAmount.label","Monto total debitado");
        
        formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.payThirdPartiesLoan." + key, "es", key.substring(0, key.indexOf(".")), "payThirdPartiesLoan", "1", messages.get(key), "2015-01-01 12:00:00"};
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2]  + "', '" + formFieldsValues[3]  + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }
}
