/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * 
 * @author fpena
 */
public class DB20170511_1610_2149 extends DBVSUpdate {

    @Override
    public void up() {
        
        insert ("ACT_RE_DEPLOYMENT", new String[]{"ID_"}, new String[]{"1"});
        
        customSentence(DBVS.DIALECT_MYSQL, "INSERT INTO ACT_GE_BYTEARRAY (ID_, REV_, NAME_, DEPLOYMENT_ID_, BYTES_, GENERATED_) VALUES ('2', '1', 'flujo-demo.bpmn', '1', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<bpmn:definitions xmlns:bpmn=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:camunda=\"http://camunda.org/schema/1.0/bpmn\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"Definitions_1\" targetNamespace=\"http://bpmn.io/schema/bpmn\" exporter=\"Camunda Modeler\" exporterVersion=\"1.7.2\">\n" +
                    "  <bpmn:process id=\"demo\" name=\"Demo\" isClosed=\"false\" isExecutable=\"true\" camunda:versionTag=\"1\">\n" +
                    "    <bpmn:startEvent id=\"inicio\" name=\"Inicio\">\n" +
                    "      <bpmn:outgoing>SequenceFlow_18t0i75</bpmn:outgoing>\n" +
                    "    </bpmn:startEvent>\n" +
                    "    <bpmn:userTask id=\"enEjecucion\" name=\"En proceso\" camunda:candidateGroups=\"10000,10001\">\n" +
                    "      <bpmn:incoming>SequenceFlow_18t0i75</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>SequenceFlow_1mk2slu</bpmn:outgoing>\n" +
                    "    </bpmn:userTask>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_18t0i75\" sourceRef=\"inicio\" targetRef=\"enEjecucion\" />\n" +
                    "    <bpmn:exclusiveGateway id=\"ExclusiveGateway_0qmslys\">\n" +
                    "      <bpmn:incoming>SequenceFlow_1mk2slu</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>rechazar</bpmn:outgoing>\n" +
                    "      <bpmn:outgoing>aceptar</bpmn:outgoing>\n" +
                    "    </bpmn:exclusiveGateway>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_1mk2slu\" sourceRef=\"enEjecucion\" targetRef=\"ExclusiveGateway_0qmslys\" />\n" +
                    "    <bpmn:endEvent id=\"aceptado\" name=\"Aceptado\">\n" +
                    "      <bpmn:incoming>SequenceFlow_12ktbww</bpmn:incoming>\n" +
                    "    </bpmn:endEvent>\n" +
                    "    <bpmn:endEvent id=\"rechazado\" name=\"Rechazado\">\n" +
                    "      <bpmn:incoming>SequenceFlow_0gd6rpt</bpmn:incoming>\n" +
                    "      <bpmn:errorEventDefinition errorRef=\"Error_07g0lf5\" />\n" +
                    "    </bpmn:endEvent>\n" +
                    "    <bpmn:sequenceFlow id=\"rechazar\" name=\"Rechazar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0knb9w2\">\n" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''rechazar''}]]></bpmn:conditionExpression>\n" +
                    "    </bpmn:sequenceFlow>\n" +
                    "    <bpmn:sequenceFlow id=\"aceptar\" name=\"Aceptar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0xnr1oc\">\n" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''aceptar''}]]></bpmn:conditionExpression>\n" +
                    "    </bpmn:sequenceFlow>\n" +
                    "    <bpmn:serviceTask id=\"Task_0xnr1oc\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.FinishTransaction\">\n" +
                    "      <bpmn:incoming>aceptar</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>SequenceFlow_12ktbww</bpmn:outgoing>\n" +
                    "    </bpmn:serviceTask>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_12ktbww\" sourceRef=\"Task_0xnr1oc\" targetRef=\"aceptado\" />\n" +
                    "    <bpmn:serviceTask id=\"Task_0knb9w2\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.CancelTransaction\">\n" +
                    "      <bpmn:incoming>rechazar</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>SequenceFlow_0gd6rpt</bpmn:outgoing>\n" +
                    "    </bpmn:serviceTask>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_0gd6rpt\" sourceRef=\"Task_0knb9w2\" targetRef=\"rechazado\" />\n" +
                    "  </bpmn:process>\n" +
                    "  <bpmn:error id=\"Error_07g0lf5\" name=\"Rechazado\" errorCode=\"rechazado\" />\n" +
                    "  <bpmndi:BPMNDiagram id=\"BPMNDiagram_1\">\n" +
                    "    <bpmndi:BPMNPlane id=\"BPMNPlane_1\" bpmnElement=\"demo\">\n" +
                    "      <bpmndi:BPMNShape id=\"_BPMNShape_StartEvent_2\" bpmnElement=\"inicio\">\n" +
                    "        <dc:Bounds x=\"167\" y=\"110\" width=\"36\" height=\"36\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"172\" y=\"146\" width=\"26\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNShape id=\"UserTask_1vng2m0_di\" bpmnElement=\"enEjecucion\">\n" +
                    "        <dc:Bounds x=\"363\" y=\"88\" width=\"100\" height=\"80\" />\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_18t0i75_di\" bpmnElement=\"SequenceFlow_18t0i75\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"203\" y=\"128\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"286\" y=\"128\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"286\" y=\"128\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"363\" y=\"128\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"301\" y=\"121.5\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNShape id=\"ExclusiveGateway_0qmslys_di\" bpmnElement=\"ExclusiveGateway_0qmslys\" isMarkerVisible=\"true\">\n" +
                    "        <dc:Bounds x=\"598\" y=\"103\" width=\"50\" height=\"50\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"623\" y=\"156\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_1mk2slu_di\" bpmnElement=\"SequenceFlow_1mk2slu\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"463\" y=\"128\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"598\" y=\"128\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"530.5\" y=\"106\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNShape id=\"EndEvent_0f4uktx_di\" bpmnElement=\"aceptado\">\n" +
                    "        <dc:Bounds x=\"865\" y=\"44\" width=\"36\" height=\"36\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"860\" y=\"83\" width=\"47\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNShape id=\"EndEvent_0xy6go4_di\" bpmnElement=\"rechazado\">\n" +
                    "        <dc:Bounds x=\"865\" y=\"191\" width=\"36\" height=\"36\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"855\" y=\"230\" width=\"56\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_18qjkrc_di\" bpmnElement=\"rechazar\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"623\" y=\"153\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"623\" y=\"205\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"713\" y=\"207\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"638.2947862313379\" y=\"215.87721759206383\" width=\"48\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_01odod8_di\" bpmnElement=\"aceptar\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"623\" y=\"103\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"623\" y=\"62\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"713\" y=\"62\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"638.8373914469487\" y=\"37.00002784612868\" width=\"39\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNShape id=\"ServiceTask_0rqf8n9_di\" bpmnElement=\"Task_0xnr1oc\">\n" +
                    "        <dc:Bounds x=\"713\" y=\"22\" width=\"100\" height=\"80\" />\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_12ktbww_di\" bpmnElement=\"SequenceFlow_12ktbww\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"813\" y=\"62\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"865\" y=\"62\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"839\" y=\"40.5\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNShape id=\"ServiceTask_044rx2r_di\" bpmnElement=\"Task_0knb9w2\">\n" +
                    "        <dc:Bounds x=\"713\" y=\"169\" width=\"100\" height=\"80\" />\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_0gd6rpt_di\" bpmnElement=\"SequenceFlow_0gd6rpt\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"813\" y=\"209\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"865\" y=\"209\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"839\" y=\"187\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "    </bpmndi:BPMNPlane>\n" +
                    "  </bpmndi:BPMNDiagram>\n" +
                    "</bpmn:definitions>', '0')");
        
        customSentence(DBVS.DIALECT_MSSQL, "INSERT INTO ACT_GE_BYTEARRAY (ID_, REV_, NAME_, DEPLOYMENT_ID_, BYTES_, GENERATED_) VALUES ('2', '1', 'flujo-demo.bpmn', '1', CAST('<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<bpmn:definitions xmlns:bpmn=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:camunda=\"http://camunda.org/schema/1.0/bpmn\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"Definitions_1\" targetNamespace=\"http://bpmn.io/schema/bpmn\" exporter=\"Camunda Modeler\" exporterVersion=\"1.7.2\">\n" +
                    "  <bpmn:process id=\"demo\" name=\"Demo\" isClosed=\"false\" isExecutable=\"true\" camunda:versionTag=\"1\">\n" +
                    "    <bpmn:startEvent id=\"inicio\" name=\"Inicio\">\n" +
                    "      <bpmn:outgoing>SequenceFlow_18t0i75</bpmn:outgoing>\n" +
                    "    </bpmn:startEvent>\n" +
                    "    <bpmn:userTask id=\"enEjecucion\" name=\"En proceso\" camunda:candidateGroups=\"10000,10001\">\n" +
                    "      <bpmn:incoming>SequenceFlow_18t0i75</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>SequenceFlow_1mk2slu</bpmn:outgoing>\n" +
                    "    </bpmn:userTask>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_18t0i75\" sourceRef=\"inicio\" targetRef=\"enEjecucion\" />\n" +
                    "    <bpmn:exclusiveGateway id=\"ExclusiveGateway_0qmslys\">\n" +
                    "      <bpmn:incoming>SequenceFlow_1mk2slu</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>rechazar</bpmn:outgoing>\n" +
                    "      <bpmn:outgoing>aceptar</bpmn:outgoing>\n" +
                    "    </bpmn:exclusiveGateway>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_1mk2slu\" sourceRef=\"enEjecucion\" targetRef=\"ExclusiveGateway_0qmslys\" />\n" +
                    "    <bpmn:endEvent id=\"aceptado\" name=\"Aceptado\">\n" +
                    "      <bpmn:incoming>SequenceFlow_12ktbww</bpmn:incoming>\n" +
                    "    </bpmn:endEvent>\n" +
                    "    <bpmn:endEvent id=\"rechazado\" name=\"Rechazado\">\n" +
                    "      <bpmn:incoming>SequenceFlow_0gd6rpt</bpmn:incoming>\n" +
                    "      <bpmn:errorEventDefinition errorRef=\"Error_07g0lf5\" />\n" +
                    "    </bpmn:endEvent>\n" +
                    "    <bpmn:sequenceFlow id=\"rechazar\" name=\"Rechazar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0knb9w2\">\n" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''rechazar''}]]></bpmn:conditionExpression>\n" +
                    "    </bpmn:sequenceFlow>\n" +
                    "    <bpmn:sequenceFlow id=\"aceptar\" name=\"Aceptar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0xnr1oc\">\n" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''aceptar''}]]></bpmn:conditionExpression>\n" +
                    "    </bpmn:sequenceFlow>\n" +
                    "    <bpmn:serviceTask id=\"Task_0xnr1oc\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.FinishTransaction\">\n" +
                    "      <bpmn:incoming>aceptar</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>SequenceFlow_12ktbww</bpmn:outgoing>\n" +
                    "    </bpmn:serviceTask>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_12ktbww\" sourceRef=\"Task_0xnr1oc\" targetRef=\"aceptado\" />\n" +
                    "    <bpmn:serviceTask id=\"Task_0knb9w2\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.CancelTransaction\">\n" +
                    "      <bpmn:incoming>rechazar</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>SequenceFlow_0gd6rpt</bpmn:outgoing>\n" +
                    "    </bpmn:serviceTask>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_0gd6rpt\" sourceRef=\"Task_0knb9w2\" targetRef=\"rechazado\" />\n" +
                    "  </bpmn:process>\n" +
                    "  <bpmn:error id=\"Error_07g0lf5\" name=\"Rechazado\" errorCode=\"rechazado\" />\n" +
                    "  <bpmndi:BPMNDiagram id=\"BPMNDiagram_1\">\n" +
                    "    <bpmndi:BPMNPlane id=\"BPMNPlane_1\" bpmnElement=\"demo\">\n" +
                    "      <bpmndi:BPMNShape id=\"_BPMNShape_StartEvent_2\" bpmnElement=\"inicio\">\n" +
                    "        <dc:Bounds x=\"167\" y=\"110\" width=\"36\" height=\"36\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"172\" y=\"146\" width=\"26\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNShape id=\"UserTask_1vng2m0_di\" bpmnElement=\"enEjecucion\">\n" +
                    "        <dc:Bounds x=\"363\" y=\"88\" width=\"100\" height=\"80\" />\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_18t0i75_di\" bpmnElement=\"SequenceFlow_18t0i75\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"203\" y=\"128\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"286\" y=\"128\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"286\" y=\"128\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"363\" y=\"128\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"301\" y=\"121.5\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNShape id=\"ExclusiveGateway_0qmslys_di\" bpmnElement=\"ExclusiveGateway_0qmslys\" isMarkerVisible=\"true\">\n" +
                    "        <dc:Bounds x=\"598\" y=\"103\" width=\"50\" height=\"50\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"623\" y=\"156\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_1mk2slu_di\" bpmnElement=\"SequenceFlow_1mk2slu\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"463\" y=\"128\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"598\" y=\"128\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"530.5\" y=\"106\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNShape id=\"EndEvent_0f4uktx_di\" bpmnElement=\"aceptado\">\n" +
                    "        <dc:Bounds x=\"865\" y=\"44\" width=\"36\" height=\"36\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"860\" y=\"83\" width=\"47\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNShape id=\"EndEvent_0xy6go4_di\" bpmnElement=\"rechazado\">\n" +
                    "        <dc:Bounds x=\"865\" y=\"191\" width=\"36\" height=\"36\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"855\" y=\"230\" width=\"56\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_18qjkrc_di\" bpmnElement=\"rechazar\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"623\" y=\"153\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"623\" y=\"205\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"713\" y=\"207\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"638.2947862313379\" y=\"215.87721759206383\" width=\"48\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_01odod8_di\" bpmnElement=\"aceptar\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"623\" y=\"103\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"623\" y=\"62\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"713\" y=\"62\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"638.8373914469487\" y=\"37.00002784612868\" width=\"39\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNShape id=\"ServiceTask_0rqf8n9_di\" bpmnElement=\"Task_0xnr1oc\">\n" +
                    "        <dc:Bounds x=\"713\" y=\"22\" width=\"100\" height=\"80\" />\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_12ktbww_di\" bpmnElement=\"SequenceFlow_12ktbww\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"813\" y=\"62\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"865\" y=\"62\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"839\" y=\"40.5\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "      <bpmndi:BPMNShape id=\"ServiceTask_044rx2r_di\" bpmnElement=\"Task_0knb9w2\">\n" +
                    "        <dc:Bounds x=\"713\" y=\"169\" width=\"100\" height=\"80\" />\n" +
                    "      </bpmndi:BPMNShape>\n" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_0gd6rpt_di\" bpmnElement=\"SequenceFlow_0gd6rpt\">\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"813\" y=\"209\" />\n" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"865\" y=\"209\" />\n" +
                    "        <bpmndi:BPMNLabel>\n" +
                    "          <dc:Bounds x=\"839\" y=\"187\" width=\"0\" height=\"13\" />\n" +
                    "        </bpmndi:BPMNLabel>\n" +
                    "      </bpmndi:BPMNEdge>\n" +
                    "    </bpmndi:BPMNPlane>\n" +
                    "  </bpmndi:BPMNDiagram>\n" +
                    "</bpmn:definitions>' AS IMAGE), '0')");
        
        customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO ACT_GE_BYTEARRAY (ID_, REV_, NAME_, DEPLOYMENT_ID_, BYTES_, GENERATED_) VALUES ('2', '1', 'flujo-demo.bpmn', '1', xmltype('<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                    "<bpmn:definitions xmlns:bpmn=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:camunda=\"http://camunda.org/schema/1.0/bpmn\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"Definitions_1\" targetNamespace=\"http://bpmn.io/schema/bpmn\" exporter=\"Camunda Modeler\" exporterVersion=\"1.7.2\">\n" +
                    "  <bpmn:process id=\"demo\" name=\"Demo\" isClosed=\"false\" isExecutable=\"true\" camunda:versionTag=\"1\">\n" +
                    "    <bpmn:startEvent id=\"inicio\" name=\"Inicio\">\n" +
                    "      <bpmn:outgoing>SequenceFlow_18t0i75</bpmn:outgoing>\n" +
                    "    </bpmn:startEvent>\n" +
                    "    <bpmn:userTask id=\"enEjecucion\" name=\"En ejecución\" camunda:candidateGroups=\"10000,10001\">\n" +
                    "      <bpmn:incoming>SequenceFlow_18t0i75</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>SequenceFlow_1mk2slu</bpmn:outgoing>\n" +
                    "    </bpmn:userTask>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_18t0i75\" sourceRef=\"inicio\" targetRef=\"enEjecucion\" />\n" +
                    "    <bpmn:exclusiveGateway id=\"ExclusiveGateway_0qmslys\">\n" +
                    "      <bpmn:incoming>SequenceFlow_1mk2slu</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>rechazar</bpmn:outgoing>\n" +
                    "      <bpmn:outgoing>aceptar</bpmn:outgoing>\n" +
                    "    </bpmn:exclusiveGateway>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_1mk2slu\" sourceRef=\"enEjecucion\" targetRef=\"ExclusiveGateway_0qmslys\" />\n" +
                    "    <bpmn:endEvent id=\"aceptado\" name=\"Aceptado\">\n" +
                    "      <bpmn:incoming>SequenceFlow_12ktbww</bpmn:incoming>\n" +
                    "    </bpmn:endEvent>\n" +
                    "    <bpmn:endEvent id=\"rechazado\" name=\"Rechazado\">\n" +
                    "      <bpmn:incoming>SequenceFlow_0gd6rpt</bpmn:incoming>\n" +
                    "      <bpmn:errorEventDefinition errorRef=\"Error_07g0lf5\" />\n" +
                    "    </bpmn:endEvent>\n" +
                    "    <bpmn:sequenceFlow id=\"rechazar\" name=\"Rechazar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0knb9w2\">\n" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''rechazar''}]]></bpmn:conditionExpression>\n" +
                    "    </bpmn:sequenceFlow>\n" +
                    "    <bpmn:sequenceFlow id=\"aceptar\" name=\"Aceptar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0xnr1oc\">\n" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''aceptar''}]]></bpmn:conditionExpression>\n" +
                    "    </bpmn:sequenceFlow>\n" +
                    "    <bpmn:serviceTask id=\"Task_0xnr1oc\" name=\"Aceptando\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.FinishTransaction\">\n" +
                    "      <bpmn:incoming>aceptar</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>SequenceFlow_12ktbww</bpmn:outgoing>\n" +
                    "    </bpmn:serviceTask>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_12ktbww\" sourceRef=\"Task_0xnr1oc\" targetRef=\"aceptado\" />\n" +
                    "    <bpmn:serviceTask id=\"Task_0knb9w2\" name=\"Rechazando\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.CancelTransaction\">\n" +
                    "      <bpmn:incoming>rechazar</bpmn:incoming>\n" +
                    "      <bpmn:outgoing>SequenceFlow_0gd6rpt</bpmn:outgoing>\n" +
                    "    </bpmn:serviceTask>\n" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_0gd6rpt\" sourceRef=\"Task_0knb9w2\" targetRef=\"rechazado\" />\n" +
                    "  </bpmn:process>\n" +
                    "  <bpmn:error id=\"Error_07g0lf5\" name=\"Rechazado\" errorCode=\"rechazado\" />\n" +
                    "</bpmn:definitions>').getblobval(nls_charset_id('UTF8')), '0')");
        
        insert ("ACT_RE_PROCDEF", new String[]{"ID_", "REV_", "CATEGORY_", "NAME_", "KEY_", "VERSION_", "DEPLOYMENT_ID_", "RESOURCE_NAME_", "HAS_START_FORM_KEY_", "SUSPENSION_STATE_", "VERSION_TAG_"}, 
                new String[]{"demo:1:3", "1", "http://bpmn.io/schema/bpmn", "Demo", "demo", "1", "1", "flujo-demo.bpmn", "0", "1", "1"});
        
        insert ("ACT_GE_PROPERTY", new String[]{"NAME_", "VALUE_", "REV_"}, new String[]{"historyLevel", "2", "1"});
        update ("ACT_GE_PROPERTY", new String[]{"VALUE_", "REV_"}, new String[]{"101", "2"}, "NAME_='next.dbid'");
        
        update("forms", new String[]{"id_bpm_process"}, new String[]{"demo:1:3"}, "id_bpm_process = 'demo-1'");
        
        deleteConfiguration("request.checkbook.jbpmProcessKey");        
        insertOrUpdateConfiguration("request.checkbook.bpmProcessKey", "demo:1:3", null, "requestCheckboook", new String[]{"notEmpty"});
    }
}
