/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ?
 */
public class DB201201121625_11728 extends DBVSUpdate {

    @Override
    public void up() {

        //modificacion del plugin de validacion de la credencial OTP
        String[] fieldNames = new String[]{"component_fqn"};
        String[] fieldValues = new String[]{"com.technisys.rubicon.business.credentials.otp.DummyOTPPlugin"};
        update("credentials", fieldNames, fieldValues, "id_credential = 'OTP'");

        //permiso nuevo
        fieldNames = new String[]{"id_permission", "permission_type"};
        fieldValues = new String[]{"rub.validatePassword", "client"};
        insert("permissions", fieldNames, fieldValues);

        //credenciales del nuevo permiso
        fieldNames = new String[]{"id_permission", "id_credential_group"};
        fieldValues = new String[]{"rub.validatePassword", "onlyPassword"};
        insert("permissions_credentials_groups", fieldNames, fieldValues);

        //activities
        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required", "id_group", "auditable"};
        fieldValues = new String[]{"rub.validatePassword", "1", "1", "com.technisys.rubicon.business.misc.activities.ValidatePasswordActivity", "rub.validatePassword", "rub.login", "2"};
        insert("activities", fieldNames, fieldValues);

        //agrego el nuevo permiso a todos los roles de tipo client y que tengan el permiso rub.login
        customSentence(getDialect(), "INSERT INTO role_permissions(id_role, id_permission) (SELECT DISTINCT(R.id_role), 'rub.validatePassword' FROM roles R, role_permissions RP WHERE R.id_role=RP.id_role AND R.role_type='client' AND RP.id_permission='rub.login')");
    }
}