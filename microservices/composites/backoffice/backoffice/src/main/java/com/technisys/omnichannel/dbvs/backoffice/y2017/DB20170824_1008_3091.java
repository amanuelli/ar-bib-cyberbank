/* 
 * Copyright 2017 Manentia Software. 
 * 
 * This software component is the intellectual property of Manentia Software S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * http://www.manentiasoftware.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-3091
 *
 * @author ldurand
 */
public class DB20170824_1008_3091 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "requestOfManagementCheck";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        update("form_fields", new String[]{"required"}, new String[]{"FALSE"}, "id_form = '" + idForm + "' AND id_field = 'costDebitAccount'");
        update("form_field_messages", new String[]{"modification_date", "value"}, new String[]{date, "Si no indica otra cuenta, se tomará la cuenta débito"}, "id_message = '" + "fields." + idForm + ".costDebitAccount.hint' AND lang = 'es'");
        update("form_field_messages", new String[]{"modification_date", "value"}, new String[]{date, "Términos y condiciones"}, "id_message = '" + "fields." + idForm + ".termsAndConditions.label' AND lang = 'es'");

    }

}
