package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author fpena
 */
public class DB20180614_1354_3997 extends DBVSUpdate {

    @Override
    public void up() {

        //Borrar titulos de sección, mail de notificación, etc.
        delete("form_fields", "id_form='lostOrStolenCreditCard' AND id_field in ('concept')");

        //Crear nuevo campo para el motivo
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "sub_type", "ordinal", "visible", "required"};
        String[] formFieldsValues = new String[]{"reason", "lostOrStolenCreditCard", "1", "selector", "default", "2", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"};
        formFieldsValues = new String[]{"reason", "lostOrStolenCreditCard", "1", "field-normal", "stolen", "0", "radio"};
        insert("form_field_selector", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "value"};
        formFieldsValues = new String[]{"reason", "lostOrStolenCreditCard", "1", "stolen"};
        insert("form_field_selector_options", formFields, formFieldsValues);
        formFieldsValues = new String[]{"reason", "lostOrStolenCreditCard", "1", "lost"};
        insert("form_field_selector_options", formFields, formFieldsValues);
        
        //Crear nuevo campo para el mensaje
        formFields = new String[]{"id_field", "id_form", "form_version", "type", "sub_type", "ordinal", "visible", "required"};
        formFieldsValues = new String[]{"message", "lostOrStolenCreditCard", "1", "textarea", "default", "3", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "min_length", "max_length"};
        formFieldsValues = new String[]{"message", "lostOrStolenCreditCard", "1", "field-normal", "0", "500"};
        insert("form_field_textarea", formFields, formFieldsValues);
        
        Map<String, String> messages = new HashMap();
        messages.put("reason.label","Motivo");
        messages.put("reason.option.lost","Extravío");
        messages.put("reason.option.stolen","Robo");
        messages.put("message.label","Mensaje");
        
        formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.lostOrStolenCreditCard." + key, "es", key.substring(0, key.indexOf(".")), "lostOrStolenCreditCard", "1", messages.get(key), "2015-01-01 12:00:00"};
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2]  + "', '" + formFieldsValues[3]  + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }
}
