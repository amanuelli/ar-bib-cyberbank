/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author KevinGaray
 */

public class DB20200611_1229_11252 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("enrollment.irs");
        insertActivity("enrollment.irs", "com.technisys.omnichannel.client.activities.pendingactions.IRSActivity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);

    }

}