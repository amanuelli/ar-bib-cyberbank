/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-378
 *
 * @author dimoda
 */

public class DB20191202_1447_378 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(DBVS.DIALECT_MSSQL, "UPDATE form_field_fl_file SET contents=convert(varbinary(max), '7487563622839,USD' + CHAR(13) + CHAR(10) + '12345679,USD,1.00,Fabian Gonzalez Perez,bandes' + CHAR(13) + CHAR(10) + '12345692,USD,1.00,Fabian Gonzalez Perez' + CHAR(13) + CHAR(10) + '12345679,USD,1.00,Fabian Gonzalez Perez,brou' + CHAR(13) + CHAR(10) + '12345679,USD,1.00,Fabian Gonzalez Perez,hsbc' + CHAR(13) + CHAR(10) + '12345679,USD,1.00,Fabian Gonzalez Perez' + CHAR(13) + CHAR(10) + '12345690,USD,1.00,Fabian Gonzalez Perez' + CHAR(13) + CHAR(10) + '12345694,USD,1.00,Fabian Gonzalez Perez,scotiabank' + CHAR(13) + CHAR(10) + '12345695,USD,1.00,Fabian Gonzalez Perez') WHERE file_name = 'archivoEjemplo.txt' OR file_name = 'sampleFile.txt'");
        customSentence(DBVS.DIALECT_MYSQL, "UPDATE form_field_fl_file SET contents=convert('7487563622839,USD\\n12345679,USD,1.00,Fabian Gonzalez Perez,bandes\\n12345692,USD,1.00,Fabian Gonzalez Perez\\n12345679,USD,1.00,Fabian Gonzalez Perez,brou\\n12345679,USD,1.00,Fabian Gonzalez Perez,hsbc\\n12345679,USD,1.00,Fabian Gonzalez Perez\\n12345690,USD,1.00,Fabian Gonzalez Perez\\n12345694,USD,1.00,Fabian Gonzalez Perez,scotiabank\\n12345695,USD,1.00,Fabian Gonzalez Perez', binary) WHERE file_name = 'archivoEjemplo.txt' OR file_name = 'sampleFile.txt'");
        customSentence(DBVS.DIALECT_ORACLE, "UPDATE form_field_fl_file SET contents=utl_raw.cast_to_raw('7487563622839,USD\\n12345679,USD,1.00,Fabian Gonzalez Perez,bandes\\n12345692,USD,1.00,Fabian Gonzalez Perez\\n12345679,USD,1.00,Fabian Gonzalez Perez,brou\\n12345679,USD,1.00,Fabian Gonzalez Perez,hsbc\\n12345679,USD,1.00,Fabian Gonzalez Perez\\n12345690,USD,1.00,Fabian Gonzalez Perez\\n12345694,USD,1.00,Fabian Gonzalez Perez,scotiabank\\n12345695,USD,1.00,Fabian Gonzalez Perez') WHERE file_name = 'archivoEjemplo.txt' OR file_name = 'sampleFile.txt'");
    }

}