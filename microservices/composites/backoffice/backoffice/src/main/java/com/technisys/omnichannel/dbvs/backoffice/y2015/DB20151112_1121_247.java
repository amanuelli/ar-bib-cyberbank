/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 *
 * @author salva
 */
public class DB20151112_1121_247 extends DBVSUpdate {

    @Override
    public void up() {
        //creo la tabla form_field_bankselector (para el campo de tipo selector de banco)
        ColumnDefinition[] columns = new ColumnDefinition[]{
            new ColumnDefinition("id_field", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("id_form", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("form_version", Types.INTEGER, 11, 0, false, null),
            new ColumnDefinition("display_type", Types.VARCHAR, 50, 0, true, null),
            new ColumnDefinition("default_value", Types.VARCHAR, 100, 0, true, null),
            new ColumnDefinition("show_blank_option", Types.BOOLEAN, 0, 0, false, "0"),};
        createTable("form_field_bankselector", columns, new String[]{"id_field", "id_form", "form_version"});

        //creo foreign key form_field_bankselector(id_field, id_form) - form_fields(id_field, id_form)
        createForeignKey("form_field_bankselector", "fk_ffbselector_ff", new String[]{"id_field", "id_form", "form_version"}, "form_fields", new String[]{"id_field", "id_form", "form_version"}, true, true);

        delete("form_field_types", "id_type='bankselector'");
        insert("form_field_types", new String[]{"id_type"}, new String[]{"bankselector"});

    }
}
