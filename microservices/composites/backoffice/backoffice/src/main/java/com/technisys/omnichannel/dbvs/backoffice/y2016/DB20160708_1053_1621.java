/* 
 * Copyright 2016 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2016;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20160708_1053_1621 extends DBVSUpdate {

    @Override
    public void up() {
        
        update ("configuration", new String[]{"validations"}, new String[]{"notEmpty||integer"}, " id_field = 'campaigns.indicators.documentInFile.cache.maximumSize'");
        update ("configuration", new String[]{"validations"}, new String[]{"notEmpty||interval"}, " id_field = 'campaigns.indicators.documentInFile.cache.expireAfter'");
        
        update ("configuration", new String[]{"validations"}, new String[]{"notEmpty||integer"}, " id_field = 'campaigns.indicators.liabilities.cache.maximumSize'");
        update ("configuration", new String[]{"validations"}, new String[]{"notEmpty||interval"}, " id_field = 'campaigns.indicators.liabilities.cache.expireAfter'");
        
        update ("configuration", new String[]{"validations"}, new String[]{"notEmpty||integer"}, " id_field = 'campaigns.indicators.assets.cache.maximumSize'");
        update ("configuration", new String[]{"validations"}, new String[]{"notEmpty||interval"}, " id_field = 'campaigns.indicators.assets.cache.expireAfter'");
        
        update ("configuration", new String[]{"id_sub_group"}, new String[]{"frontend"}, " id_sub_group = ''");
    }
}
