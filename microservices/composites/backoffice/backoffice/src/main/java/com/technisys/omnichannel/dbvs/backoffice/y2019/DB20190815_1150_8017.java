/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pbanales
 */

public class DB20190815_1150_8017 extends DBVSUpdate {

    @Override
    public void up() {
        String[] dialects = new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE};

        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                "required", "visible_in_mobile", "read_only", "sub_type", "ticket_only", "note"};

        String[] formFieldsValues = new String[]{"tax", "transferLocal", "1", "amount", "4", "TRUE", "FALSE", "1", "1", "default", "1", null};
        insert("form_fields", formFields, formFieldsValues);

        String sentence = "UPDATE form_fields SET form_version = (SELECT MAX(version) from forms where id_form = 'transferLocal') WHERE id_field = 'tax' AND id_form = 'transferLocal' ";
        customSentence(dialects, sentence);

        sentence = "UPDATE form_field_amount SET form_version = (SELECT MAX(version) from forms where id_form = 'transferLocal') WHERE (form_field_amount.id_field = 'tax' and form_field_amount.id_form = 'transferLocal')";
        customSentence(dialects, sentence);

        sentence = "UPDATE form_fields SET ordinal = 1 WHERE id_field = 'amount' AND id_form = 'transferLocal' AND form_version = (SELECT MAX(version) from forms where id_form = 'transferLocal')";
        customSentence(dialects, sentence);

        sentence = "UPDATE form_fields SET ordinal = 2 WHERE id_field = 'debitAccount' AND id_form = 'transferLocal' AND form_version = (SELECT MAX(version) from forms where id_form = 'transferLocal')";
        customSentence(dialects, sentence);

        sentence = "UPDATE form_fields SET ordinal = 3 WHERE id_field = 'creditAccount' AND id_form = 'transferLocal' AND form_version = (SELECT MAX(version) from forms where id_form = 'transferLocal')";
        customSentence(dialects, sentence);

        sentence = "UPDATE form_fields SET ordinal = 4 WHERE id_field = 'tax' AND id_form = 'transferLocal' AND form_version = (SELECT MAX(version) from forms where id_form = 'transferLocal')";
        customSentence(dialects, sentence);

        sentence = "UPDATE form_fields SET ordinal = 5 WHERE id_field = 'creditReference' AND id_form = 'transferLocal' AND form_version = (SELECT MAX(version) from forms where id_form = 'transferLocal')";
        customSentence(dialects, sentence);

        sentence = "UPDATE form_fields set ordinal = (ordinal + 1) \n" +
                "where id_field in ('creditReference', 'notificationEmails', 'notificationBody') and id_form = 'transferLocal' and form_version = (SELECT MAX(version) from forms where id_form = 'transferLocal')";
        customSentence(dialects, sentence);

        sentence = "INSERT INTO form_field_amount (id_field, id_form, form_version, display_type, control_limits, use_for_total_amount) VALUES('tax', 'transferLocal', (SELECT MAX(version) from forms where id_form = 'transferLocal'), 'field-normal', 0, 0)";
        customSentence(dialects, sentence);

        Map<String, String> messagesEN = new HashMap<>();
        Map<String, String> messagesES = new HashMap<>();
        Map<String, String> messagesPT = new HashMap<>();

        messagesEN.put("help", "");
        messagesES.put("help", "");
        messagesPT.put("help", "");

        messagesEN.put("hint", "");
        messagesES.put("hint", "");
        messagesPT.put("hint", "");

        messagesEN.put("label", "Tax/commission");
        messagesES.put("label", "Comisión");
        messagesPT.put("label", "Comissão");

        messagesEN.put("requiredError", "");
        messagesES.put("requiredError", "");
        messagesPT.put("requiredError", "");

        insertFormFieldMessages("transferLocal", "tax", messagesEN, messagesES, messagesPT);
    }
}
