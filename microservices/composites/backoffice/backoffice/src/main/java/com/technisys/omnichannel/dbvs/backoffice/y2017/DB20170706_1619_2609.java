/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2609
 *
 * @author rmedina
 */
public class DB20170706_1619_2609 extends DBVSUpdate {

    @Override
    public void up() {
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String idForm = "reissueCreditCard";
        String version = "1";
        String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value", "modification_date" };
        delete("form_messages", "id_form='" + idForm + "'");
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version,lang,value, modification_date) VALUES ('forms." + idForm + ".formName', '" + idForm + "','" + version + "','es',  'Solicitud de reimpresión de tarjeta de crédito',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "es", "Solicitud de reimpresión de tarjeta de crédito", date });
        }

    }
}