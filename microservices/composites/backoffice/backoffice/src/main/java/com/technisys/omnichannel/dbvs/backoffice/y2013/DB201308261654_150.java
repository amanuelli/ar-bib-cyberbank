/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201308261654_150 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("rubicon.widgets.geolocalization.promotions", "-34.909245,-56.147723#Pizería Trouville|-34.907684,-56.206226#Porto Vanila|-34.905290,-56.181550#Mosca|-34.909162,-56.188588#Cordón");

        updateConfiguration("rubicon.widgets.geolocalization.areas", "-34.908333,-56.151389#Pocitos|-34.905678,-56.208243#Ciudad Vieja|-34.905537,-56.186872#Cordón");

    }
}