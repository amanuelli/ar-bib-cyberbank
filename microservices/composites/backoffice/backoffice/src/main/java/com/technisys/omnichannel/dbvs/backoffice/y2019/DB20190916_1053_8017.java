/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author pbanales
 */

public class DB20190916_1053_8017 extends DBVSUpdate {

    @Override
    public void up() {
        updateActivity("transfers.local.send",
                "com.technisys.omnichannel.client.activities.transfers.local.TransferLocalSendActivity",
                "transfer.local",
                "transferLocal",
                ActivityDescriptor.AuditLevel.Full,
                ClientActivityDescriptor.Kind.Transactional
                );
    }
}
