/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author Diego Curbelo
 */
public class DB20150520_1344_363 extends DBVSUpdate {

    @Override
    public void up() {
                
        insertOrUpdateConfiguration(
                "frontend.i18n.default.lang",
                "es",
                ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"},
                "es|en|pt");
        
        insertOrUpdateConfiguration(
                "frontend.i18n.refresh.interval",
                "1h",
                ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty","interval"});
        
        insertOrUpdateConfiguration(
                "frontend.configuration.refresh.interval",
                "1h",
                ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty","interval"});
        
        insertOrUpdateConfiguration(
                "frontend.mode",
                "production",
                ConfigurationGroup.SEGURIDAD, "frontend", new String[]{"notEmpty"}, "production|development");
        
    }
}