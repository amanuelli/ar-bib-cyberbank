/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3386
 *
 * @author rschiappapietra
 */
public class DB20171011_1527_3386 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_amount", new String[] { "control_limit_over_product" }, new String[] { "debitAccount" }, "id_form = 'mobilePrepaiment' and id_field = 'amount'");
    }

}
