/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150729_1418_415 extends DBVSUpdate {

    @Override
    public void up() {

        // Permisos
        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"administration.view", null, null, "administration", "0", "99999", null, null, "administration", "0", "99999", null, null, "administration", "0", "99999", null, null, null});
        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"administration.manage", null, null, "administration", "0", "99999", null, null, "administration", "0", "99999", null, null, "administration", "0", "99999", null, null, null});

    }
}
