/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20151007_1609_929 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("backend.webservices.url", "http://localhost:8080/core-banking/", ConfigurationGroup.SEGURIDAD, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("backend.webservices.decimalFormat", "#0.00", ConfigurationGroup.SEGURIDAD, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("backend.webservices.dateFormat", "yyyy-MM-dd'T'HH:mm'Z'", ConfigurationGroup.SEGURIDAD, "frontend", new String[]{"notEmpty"});
    }
}
