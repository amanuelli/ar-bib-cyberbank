/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2504
 *
 * @author laduriz
 */
public class DB20170710_1722_2504 extends DBVSUpdate {

    @Override
    public void up() {
        
        final String idForm = "buyDocuments";
        final String descriptionForm = "Compra de documentos";
        String version = "1";
        
        String[] forms = new String[] {"id_form", "version", "enabled", "category", "type", "admin_option", "id_activity", "last", "deleted", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow", "id_bpm_process"};
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};
        String[] formFieldText = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        String[] formFieldAmount = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
        String[] formFieldSelector = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"};
        String[] formFieldSelectorOptions = new String[]{"id_field", "id_form", "form_version", "value"};
        String[] formFieldMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        String[] formMessages = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        String[] formFieldTextArea = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        String[] formFieldsTermsConditions = new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"};
        String[] formFieldEmailList = new String[]{"id_field", "id_form", "form_version", "display_type"};
        String[] formFieldsValues;
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        
        Map<String, String> messages = new HashMap();
        
        /** Insert Form - Compra de documentos **/
        insert("forms", forms, new String[]{idForm, version, "1", "comex", "process", "comex", null, "1", "0", "1", "1", "1", "1", "1", "demo:1:3"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" + idForm + "', '" + version + "', 'es', '" + descriptionForm + "', TO_DATE('2017-07-11 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formMessages, new String[]{"forms." + idForm + ".formName", idForm, version, "es", descriptionForm, date});
        }
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "accessToken-pin"});
        
        /** Insert field - Carta de crédito **/
        formFieldsValues = new String[]{"letterCredit", idForm, version , "text", "1", "TRUE", "TRUE",null, "1", "0", "default", "0"};
        String[] formFieldsTextValues = new String[]{"letterCredit", idForm, version, "0", "50", "field-normal", null};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldText, formFieldsTextValues);
        messages.put("letterCredit.label", "Carta de crédito");
        messages.put("letterCredit.requiredError", "Debe ingresar una carta de crédito");        
        //falta el otro mensaje de error
        messages.put("letterCredit.help", "Seleccione la carta de crédito que desea descontar");  
        
        
        /** BOTON BUSCAR **/
        
        
        /** Insert field - Recurso(Selector)  **/
        insert("form_fields", formFields, new String[]{"withResource", idForm, version, "selector", "2", "TRUE", "FALSE", null, "0", "0", "default", "0"});
        insert("form_field_selector", formFieldSelector, new String[]{"withResource", idForm, version, "field-normal", null, "0", "check"});
        // falta texto de ayuda al selector
        insert("form_field_selector_options", formFieldSelectorOptions, new String[]{"withResource", idForm, version, "resource"});
        messages.put("withResource.label", "Recursos");
        messages.put("withResource.option.resource", "Recurso");
        
        /** Insert field - Importe(Monto) **/
        insert("form_fields", formFields, new String[]{"amount", idForm, version, "amount", "3", "value(withResource) != 'resource'", "value(withResource) != 'resource'", null, "0", "0", "default", "0"});
        insert("form_field_amount", formFieldAmount, new String[]{"amount", idForm, version, "field-normal", "0"});
        // falta control maximo
        messages.put("amount.label", "Importe");
        messages.put("amount.requiredError", "El importe no es válido");
        
        /** Insert field - Tasa de interés **/
        formFieldsValues = new String[]{"interestRate", idForm, version, "text", "4", "value(withResource) != 'resource'", "value(withResource) != 'resource'", null, "1", "0", "default", "0"};
        formFieldsTextValues = new String[]{"interestRate", idForm, version, "0", "2", "field-normal", "withoutSpecialChars"};
        // falta minimo 0
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldText, formFieldsTextValues);
        messages.put("interestRate.label", "Tasa de interés");
        
        /** Insert field - Observaciones **/
        formFieldsValues = new String[]{"observations", idForm, version, "textarea", "5", "value(withResource) != 'resource'", "FALSE", null, "0", "0", "default", "0"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("observations.label", "Observaciones");
        insert("form_field_textarea", formFieldTextArea, new String[]{"observations", idForm, version, "0", "500", "field-big"});
        
        /** Insert field - Adjuntos **/
        insert("form_fields", formFields, new String[]{"attachments", idForm, version, "text", "6", "value(withResource) != 'resource'", "FALSE", null, "0", "0", "default", "0"});
        insert("form_field_text", formFieldText, new String[]{"attachments", idForm, version, "1", "80", "field-normal", null});
        messages.put("attachments.label", "Adjuntos");
        
        // LINK ADJUNTOS
        
        /** Insert field - Disclaimer **/
        formFieldsValues = new String[]{"disclaimer", idForm, version, "termsandconditions", "7", "TRUE", "FALSE", null, "1", "0", "default", "0"};
        String[] formFieldsTermsConditionsValues = new String[]{"disclaimer", idForm, version, "field-normal", "0", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", formFieldsTermsConditions, formFieldsTermsConditionsValues);
        messages.put("disclaimer.label", "Disclaimer");
        messages.put("disclaimer.requiredError", "");
        messages.put("disclaimer.showAcceptOptionText", "Aceptar");
        messages.put("disclaimer.termsAndConditions", "Sujeto a aprobación crediticia del banco." +
                "Las instrucciones enviadas al banco por usted luego de las <hora de corte>," + 
                " serán procesadas al siguiente día hábil bancario." +
                "Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.");
        
        /** Insert field - Emails de notificación **/
        insert("form_fields", formFields, new String[]{"notificationEmails", idForm, version, "emaillist", "8", "TRUE", "FALSE", null, "1", "0", "default", "0"});
        insert("form_field_emaillist", formFieldEmailList, new String[]{"notificationEmails", idForm, version, "field-normal"});
        messages.put("notificationEmails.label", "Emails de notificación");
        messages.put("notificationEmails.help", "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");

        /** Insert field - Cuerpo de notificación **/
        insert("form_fields", formFields, new String[]{"notificationBody", idForm, version, "textarea", "9", "TRUE", "FALSE", null, "1", "0", "default", "0"});
        insert("form_field_textarea", formFieldTextArea, new String[]{"notificationBody", idForm, version, "1", "500", "field-normal"});
        messages.put("notificationBody.label", "Cuerpo de notificación");

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "', '" + formFieldsValues[5]+"', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldMessages, formFieldsValues);
            }
        }
        
    }

}