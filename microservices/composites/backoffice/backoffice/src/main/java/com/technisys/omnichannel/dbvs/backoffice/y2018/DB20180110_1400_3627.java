/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-3627
 *
 * @author msouza
 */
public class DB20180110_1400_3627 extends DBVSUpdate {

    @Override
    public void up() {
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFieldsNames = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        String[] formFieldsValues_es = new String[]{"fields.preFinancingConstitution.disclaimer.requiredError", "es", "disclaimer", "preFinancingConstitution", "1", date, "Debe aceptar los términos y condiciones para continuar"};
        String[] formFieldsValues_pt = new String[]{"fields.preFinancingConstitution.disclaimer.requiredError", "pt", "disclaimer", "preFinancingConstitution", "1", date, "Deve aceitar os termos e condições para continuar"};
        String[] formFieldsValues_en = new String[]{"fields.preFinancingConstitution.disclaimer.requiredError", "en", "disclaimer", "preFinancingConstitution", "1", date, "You must accept the terms and conditions to continue"};

        delete("form_field_messages", "id_message = 'fields.preFinancingConstitution.disclaimer.requiredError'");

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            String insertSentence_es = String.format("INSERT INTO form_field_messages (%1$s, %2$s, %3$s, %4$s, %5$s, %6$s, %7$s)"
                                                + " VALUES ('%8$s', '%9$s', '%10$s', '%11$s', '%12$s', TO_DATE('%13$s', 'YYYY-MM-DD HH24:MI:SS'), '%14$s')",
                                                formFieldsNames[0], formFieldsNames[1], formFieldsNames[2], formFieldsNames[3], formFieldsNames[4], formFieldsNames[5], formFieldsNames[6],
                                                formFieldsValues_es[0], formFieldsValues_es[1], formFieldsValues_es[2], formFieldsValues_es[3], formFieldsValues_es[4], formFieldsValues_es[5], formFieldsValues_es[6]);
            String insertSentence_pt = String.format("INSERT INTO form_field_messages (%1$s, %2$s, %3$s, %4$s, %5$s, %6$s, %7$s)"
                                                + " VALUES ('%8$s', '%9$s', '%10$s', '%11$s', '%12$s', TO_DATE('%13$s', 'YYYY-MM-DD HH24:MI:SS'), '%14$s')",
                                                formFieldsNames[0], formFieldsNames[1], formFieldsNames[2], formFieldsNames[3], formFieldsNames[4], formFieldsNames[5], formFieldsNames[6],
                                                formFieldsValues_pt[0], formFieldsValues_pt[1], formFieldsValues_pt[2], formFieldsValues_pt[3], formFieldsValues_pt[4], formFieldsValues_pt[5], formFieldsValues_pt[6]);
            String insertSentence_en = String.format("INSERT INTO form_field_messages (%1$s, %2$s, %3$s, %4$s, %5$s, %6$s, %7$s)"
                                                + " VALUES ('%8$s', '%9$s', '%10$s', '%11$s', '%12$s', TO_DATE('%13$s', 'YYYY-MM-DD HH24:MI:SS'), '%14$s')",
                                                formFieldsNames[0], formFieldsNames[1], formFieldsNames[2], formFieldsNames[3], formFieldsNames[4], formFieldsNames[5], formFieldsNames[6],
                                                formFieldsValues_en[0], formFieldsValues_en[1], formFieldsValues_en[2], formFieldsValues_en[3], formFieldsValues_en[4], formFieldsValues_en[5], formFieldsValues_en[6]);
            
            customSentence(DBVS.DIALECT_ORACLE, insertSentence_es);
            customSentence(DBVS.DIALECT_ORACLE, insertSentence_pt);
            customSentence(DBVS.DIALECT_ORACLE, insertSentence_en);
        } else {
            insert("form_field_messages", formFieldsNames, formFieldsValues_es);
            insert("form_field_messages", formFieldsNames, formFieldsValues_pt);
            insert("form_field_messages", formFieldsNames, formFieldsValues_en);
        }
        
        
        update("form_fields", new String[] {"required"}, new String[] {"TRUE"}, "id_field = 'disclaimer'");
    }
}