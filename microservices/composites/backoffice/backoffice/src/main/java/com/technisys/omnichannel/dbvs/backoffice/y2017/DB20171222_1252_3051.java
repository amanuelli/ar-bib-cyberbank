/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * Related issue: MANNZACA-3051
 * 
 * @author isilveira
 */
public class DB20171222_1252_3051 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[] { "value" };
        String[] fieldValues = new String[] { "Aceptar términos y condiciones" };
        String whereClause = "id_message='fields.requestOfManagementCheck.termsAndConditions.showAcceptOptionText' and lang='es'";
        
        update("form_field_messages", fieldNames, fieldValues, whereClause);
        
        fieldNames = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value" };
        fieldValues = new String[] { "fields.requestOfManagementCheck.termsAndConditions.label", "es", "termsAndConditions", "requestOfManagementCheck", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Términos y condiciones" };
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            String insertSentence = String.format("INSERT INTO form_field_messages (%1$s, %2$s, %3$s, %4$s, %5$s, %6$s, %7$s) "
                                                + "VALUES ('%8$s', '%9$s', '%10$s', '%11$s', '%12$s', TO_DATE('%13$s', 'YYYY-MM-DD HH24:MI:SS'), '%14$s')",
                                                fieldNames[0], fieldNames[1], fieldNames[2], fieldNames[3], fieldNames[4], fieldNames[5], fieldNames[6],
                                                fieldValues[0], fieldValues[1], fieldValues[2], fieldValues[3], fieldValues[4], fieldValues[5], fieldValues[6]);
            
            customSentence(DBVS.DIALECT_ORACLE, insertSentence);
        } else {
            insert("form_field_messages", fieldNames, fieldValues);
        }
    }
}