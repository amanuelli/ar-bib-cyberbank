/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author KevinGaray
 */

public class DB20200204_1845_9501 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("administration.restrictions.list");

        insertActivity(
                "administration.restrictions.list", "com.technisys.omnichannel.client.activities.administration.restrictions.ListAccessRestrictionsActivity",
                "administration",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
    }

}