package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6051
 *
 * @author isilveira
 */
public class DB20181203_1625_6051 extends DBVSUpdate {
     @Override
     public void up() {
         insertOrUpdateConfiguration("field.subType.inputfile", "salaryPayment", ConfigurationGroup.OTHER, "others", new String[] {});
     }
}
