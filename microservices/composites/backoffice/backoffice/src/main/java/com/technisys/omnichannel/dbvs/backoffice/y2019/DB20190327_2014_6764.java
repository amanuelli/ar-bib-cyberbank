/* 
 * Copyright 2019 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6764
 *
 * @author Marcelo Bruno
 */
public class DB20190327_2014_6764 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='massive.payments.allowed.banks'");
    }

}