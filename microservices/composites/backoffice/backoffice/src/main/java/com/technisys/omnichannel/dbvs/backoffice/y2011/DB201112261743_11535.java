/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Diego Curbelo
 */
public class DB201112261743_11535 extends DBVSUpdate {

    @Override
    public void up() {

        insert("configuration", new String[]{"id_field", "value", "id_group"},
                new String[]{"rubicon.productTypes.transferBatch.credit.list", "CC|CA", "rubicon"});
    }
}