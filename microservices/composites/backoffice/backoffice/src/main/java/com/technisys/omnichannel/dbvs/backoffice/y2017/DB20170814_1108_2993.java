/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 *
 * @author danireb
 */
public class DB20170814_1108_2993 extends DBVSUpdate {

    @Override
    public void up() {

        dropColumn("frequent_destinations", "exterior_bank");

        ColumnDefinition column = new ColumnDefinition("exterior_bank_type", Types.VARCHAR, 10, 0, true, null);        
        addColumn("frequent_destinations", column);
        
        column = new ColumnDefinition("exterior_bank_code", Types.VARCHAR, 15, 0, true, null);        
        addColumn("frequent_destinations", column);

    }
    
}
