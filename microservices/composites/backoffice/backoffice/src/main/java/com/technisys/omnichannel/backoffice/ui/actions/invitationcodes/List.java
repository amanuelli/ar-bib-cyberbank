/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.opensymphony.xwork2.ActionSupport;
import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.PaginatedListResponse;
import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ListRequest;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.utils.DateUtils;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 * @author Sebastian Barbosa
 */
@Action(results = {
    @Result(location = "/invitationcodes/_list.jsp")
})
public class List extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ListRequest request = new ListRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.invitationCodes.list");

        request.setInvitationCode(invitationCode);
        request.setCreationDateFrom(creationDateFrom);
        request.setCreationDateTo(creationDateTo);
        request.setProductGroupId(clientId);
        request.setEmail(email);
        request.setDocumentNumber(documentNumber);
        request.setStatus(status);
        request.setMobileNumber(mobileNumber);

        request.setPageNumber(pageNumber);
        request.setOrderBy(orderBy);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        if (ReturnCodes.OK.equals(response.getReturnCode())) {
            PaginatedListResponse<InvitationCode> listResponse = (PaginatedListResponse<InvitationCode>) response;
            returnCode = response.getReturnCode().toString();

            list = listResponse.getItemList();
            totalPages = listResponse.getTotalPages();
            totalRows = listResponse.getTotalRows();
            rowsPerPage = listResponse.getRowsPerPage();
            startIndex = Math.max(1, pageNumber - 5);
            endIndex = Math.min(totalPages, pageNumber + 5);

            return SUCCESS;
        } else {
            throw new JSONException(response);
        }
    }
    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private String invitationCode;
    private String documentNumber;
    private String email;
    private Date creationDateFrom;
    private Date creationDateTo;
    private String status;
    private String clientId;
    private String mobileNumber;
    private int pageNumber = -1;
    private String orderBy = null;
    private String recordToShow = null;

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCreationDateFrom(String creationDateFrom) {
        try {
            if (StringUtils.isNotEmpty(creationDateFrom)) {
                this.creationDateFrom = DateUtils.parseShortDate(creationDateFrom);
            }
        } catch (ParseException e) {
            this.creationDateFrom = null;
        }
    }

    public void setCreationDateTo(String creationDateTo) {
        try {
            if (StringUtils.isNotEmpty(creationDateTo)) {
                this.creationDateTo = DateUtils.parseShortDate(creationDateTo);
            }
        } catch (ParseException e) {
            this.creationDateTo = null;
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setRecordToShow(String recordToShow) {
        this.recordToShow = recordToShow;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private java.util.List<InvitationCode> list;
    private String returnCode;
    private long totalPages;
    private long totalRows;
    private long rowsPerPage;
    private long startIndex;
    private long endIndex;

    public java.util.List<InvitationCode> getList() {
        return list;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public long getTotalRows() {
        return totalRows;
    }

    public long getRowsPerPage() {
        return rowsPerPage;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public String getRecordToShow() {
        return recordToShow;
    }

    public long getStartIndex() {
        return startIndex;
    }

    public long getEndIndex() {
        return endIndex;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
