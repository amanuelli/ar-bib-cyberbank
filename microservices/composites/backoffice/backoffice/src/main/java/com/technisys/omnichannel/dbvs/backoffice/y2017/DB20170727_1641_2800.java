/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2800
 *
 * @author pdominguez
 */
public class DB20170727_1641_2800 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "requestTransactionCancellation";
        String version = "1";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        
        delete("form_messages", "id_message = 'forms." + idForm + ".formName' and version=" + version);
        
        String[] formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        String[] formFieldsValues = new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Solicitud de cancelación de transacción", date};

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form,version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formField, formFieldsValues);
        }
    }

}