/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities;


import com.technisys.omnichannel.backoffice.business.cybo.MapResponse;
import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.ReadUserRequest;
import com.technisys.omnichannel.backoffice.domain.cybo.invitationcodes.ClientEnvironment;
import com.technisys.omnichannel.backoffice.domain.cybo.Customer;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.documenttypes.DocumentTypesHandler;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author azeballos
 */
public class ReadUserActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        MapResponse response = new MapResponse(request);

        try {
            ReadUserRequest auxRequest = (ReadUserRequest) request;

            ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber(), null);
            response.putItem("client", new Customer(client));

            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber());
            if (user != null) {
                response.putItem("user", new Customer(user));
            }
            
            // Cuentas del usuario
            List<com.technisys.omnichannel.client.domain.ClientEnvironment> accounts = RubiconCoreConnectorC.listClients(request.getIdTransaction(), auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber());
            List<ClientEnvironment> accountList = new ArrayList<>();
            for (com.technisys.omnichannel.client.domain.ClientEnvironment account : accounts) {
                Environment environment = Administration.getInstance().readEnvironmentByProductGroupId(account.getProductGroupId());
                accountList.add(new ClientEnvironment(account, environment));
            }
            response.putItem("accountList", accountList);

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            ReadUserRequest auxRequest = (ReadUserRequest) request;

            if (StringUtils.isBlank(auxRequest.getDocumentNumber())) {
                result.put("@documentNumber", "cybo.general.validations.empty");
            } else if (auxRequest.getDocumentNumber().length() > 25) {
                result.put("@documentNumber", "activities.backoffice.invitationCodes.documentNumberMaxLengthExceeded");
            }

            List<String> validCountryCodes = CountryCodesHandler.listCountryCodes();
            if (StringUtils.isBlank(auxRequest.getDocumentCountry())) {
                result.put("@documentCountry", "cybo.general.validations.empty");
            } else if (!validCountryCodes.contains(auxRequest.getDocumentCountry())) {
                result.put("@documentCountry", "activities.backoffice.invitationCodes.documentCountryInvalid");
            }

            if (StringUtils.isBlank(auxRequest.getDocumentType())) {
                result.put("@documentType", "cybo.general.validations.empty");
            } else if (result.get("documentCountry") == null) {
                List<String> validDocumentTypes = DocumentTypesHandler.listDocumentTypes(auxRequest.getDocumentCountry());
                List<String> invalidDocumentTypes = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "backoffice.invitationCodes.invalidDocumentTypes");
                if (!validDocumentTypes.contains(auxRequest.getDocumentType())
                        || invalidDocumentTypes.contains(auxRequest.getDocumentType())) {
                    result.put("@documentType", "activities.backoffice.invitationCodes.documentTypeInvalid");
                }
            }

            if (result.isEmpty()) {
                ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber(), null);
                if (client == null) {
                    result.put("@documentNumber", "activities.backoffice.invitationCodes.clientNotInBackend");
                } else {
                    List<com.technisys.omnichannel.client.domain.ClientEnvironment> accounts = RubiconCoreConnectorC.listClients(request.getIdTransaction(), auxRequest.getDocumentCountry(), auxRequest.getDocumentType(), auxRequest.getDocumentNumber());
                    if (accounts == null || accounts.isEmpty()) {
                        result.put("@documentNumber", "activities.backoffice.invitationCodes.clientDontHaveAccounts");
                    }
                }
            }
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }
}
