/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author cristobal
 */

public class DB20200325_1703_917 extends DBVSUpdate {

    @Override
    public void up() {
        delete("form_field_selector", "id_form ='requestLoan' and id_field = 'fundsDestination'");
        delete("form_fields", "id_form ='requestLoan' and id_field = 'fundsDestination'");
    }

}