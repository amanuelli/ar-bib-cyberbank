/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author KevinGaray
 */

public class DB20200827_1058_13958 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("safeway.apirest.baseurl", "https://digital-test-oracle-int.technisys.com/safeway/api");
        updateConfiguration("safeway.apirest.timeout.connect", "20000");
        updateConfiguration("safeway.apirest.timeout.read", "20000");
        updateConfiguration("safeway.apirest.timeout.write", "20000");
        updateConfiguration("demo.safeMode", "false");
    }

}