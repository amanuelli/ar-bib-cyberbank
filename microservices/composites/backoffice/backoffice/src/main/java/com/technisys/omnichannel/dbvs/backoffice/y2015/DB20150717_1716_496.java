package com.technisys.omnichannel.dbvs.backoffice.y2015;

/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author aelters
 */
public class DB20150717_1716_496 extends DBVSUpdate {

    @Override
    public void up() {
        deleteConfiguration("client.desktop.frequentActions.frequentActions.defaults.corporate");
        insertOrUpdateConfiguration("frequentActions.defaults.corporate", "accounts.list|loans.list", DBVSUpdate.ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        deleteConfiguration("client.desktop.frequentActions.frequentActions.defaults.retail");
        insertOrUpdateConfiguration("frequentActions.defaults.retail", "accounts.list|loans.list", DBVSUpdate.ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        deleteConfiguration("client.desktop.frequentActions.supportedActivityList");
        insertOrUpdateConfiguration("frequentActions.supportedActivityList", "accounts.requestCheckBook|accounts.internalTranference|accounts.vacancyTransference|creditCards.payment|accounts.foreignTransfer", DBVSUpdate.ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        deleteConfiguration("desktop.frequentActions.limit");
        insertOrUpdateConfiguration("frequentActions.limit", "3", DBVSUpdate.ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        deleteConfiguration("desktop.frequentActions.maxToShow");
        insertOrUpdateConfiguration("frequentActions.maxToShow", "3", DBVSUpdate.ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
    }
}