/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author dimoda
 */

public class DB20200401_1057_963 extends DBVSUpdate {

    @Override
    public void up() {

        update("adm_ui_permissions", new String[]{"advanced_allow_prod_selection"}, new String[]{"0"}, "id_permission='requestLoan.send'");

    }

}