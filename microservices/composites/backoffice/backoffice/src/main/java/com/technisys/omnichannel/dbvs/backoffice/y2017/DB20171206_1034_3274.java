/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3274
 *
 * @author naradogervino
 */
public class DB20171206_1034_3274 extends DBVSUpdate {

    @Override
    public void up() {
    	//Se eliminan variables de configuracion no utilizadas
    	deleteConfiguration("accounts.details.statementsPerPage");
    	deleteConfiguration("breadcrumb.payments.url");
    	deleteConfiguration("client.userconfiguration.forgotpasswordmail.url");
    	deleteConfiguration("desktop.widgets.nextExpirations.limit");
    	deleteConfiguration("hmac_hash_function");
    	deleteConfiguration("transactions.reminder.defaultCommunicationType");
    	deleteConfiguration("widget.transactions.maxPending");
    	deleteConfiguration("widget.products.deposits.statementsPerPage");
    	deleteConfiguration("widget.products.loans.statementsPerPage");
    	deleteConfiguration("widget.products.creditCards.statementsPerPage");
    	deleteConfiguration("widget.products.accounts.statementsPerPage");
    	
    	
    }
}