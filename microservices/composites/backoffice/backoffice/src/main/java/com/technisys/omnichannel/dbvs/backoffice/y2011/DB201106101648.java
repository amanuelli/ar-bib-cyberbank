/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201106101648 extends DBVSUpdate {

    @Override
    public void up() {

        String template = "<fieldset>";
        template += "<form:field idField='tipoDeOperacion'/>";
        template += "<form:field idField='cuentaDebito'/>";
        template += "<form:field idField='tipoDeDebito'/>";
        template += "<form:field idField='servicioIMMont'/>";
        template += "<form:field idField='servicioIMMald'/>";
        template += "<form:field idField='servicioIMSanJose'/>";
        template += "<form:field idField='numeroCuentaAntel'/>";
        template += "<form:field idField='numeroContratoAncel'/>";
        template += "<form:field idField='numeroCelularAncel'/>";
        template += "<form:field idField='referenciaDeCobroUte'/>";
        template += "<form:field idField='numeroDeCuentaCorrienteContribIMMont'/>";
        template += "<form:field idField='formaDePagoContribIMMont'/>";
        template += "<form:field idField='numeroDeClienteMontGas'/>";
        template += "<form:field idField='numeroDePadronContrUrbIMMald'/>";
        template += "<form:field idField='unidadContrUrbIMMald'/>";
        template += "<form:field idField='manzanaContrUrbIMMald'/>";
        template += "<form:field idField='solarContrUrbIMMald'/>";
        template += "<form:field idField='padronPatenteIMMald'/>";
        template += "<form:field idField='matriculaPatenteIMMald'/>";
        template += "<form:field idField='marcaVehiculoPatenteIMMald'/>";
        template += "<form:field idField='anoVehiculoPatenteIMMald'/>";
        template += "<form:field idField='numeroCuentaOse'/>";
        template += "<form:field idField='numeroDeCuentaCorrienteSaneamIMMont'/>";
        template += "<form:field idField='numeroDeCuentaCorrienteImpuestosIMMont'/>";
        template += "<form:field idField='numeroDeCuentaCorrientePatenteIMMont'/>";
        template += "<form:field idField='formaDePagoPatenteIMMont'/>";
        template += "<form:field idField='formaDePagoContrUrbIMMald'/>";
        template += "<form:field idField='formaDePagoPatenteIMMald'/>";
        template += "<form:field idField='numeroPadronContrUrbIMSJ'/>";
        template += "<form:field idField='localidadAbreviadaContrUrbIMSJ'/>";
        template += "<form:field idField='formaDePagoContrUrbIMSJ'/>";
        template += "<form:field idField='numeroDePadronContrRuralIMSJ'/>";
        template += "<form:field idField='localidadAbreviadaContrRuralIMSJ'/>";
        template += "<form:field idField='formaDePagoContrRuralIMSJ'/>";
        template += "<form:field idField='numeroDePadronPatenteIMSJ'/>";
        template += "<form:field idField='matriculaPatenteIMSJ'/>";
        template += "<form:field idField='formaDePagoPatenteIMSJ'/>";
        template += "<form:field idField='numeroClienteMovistar'/>";
        template += "<form:field idField='numeroContratoDedicado'/>";
        template += "<form:field idField='prestamoAPagar'/>";
        template += "<form:field idField='prestamoPersonal'/>";
        template += "<form:field idField='prestamoTerceroDependencia'/>";
        template += "<form:field idField='prestamoTerceroMoneda'/>";
        template += "<form:field idField='prestamoTerceroNumero'/>";
        template += "<form:field idField='prestamoTerceroCodigo'/>";
        template += "<form:field idField='aceptacionDeSuscripcion'/>";
        template += "</fieldset>";

        update("forms", new String[]{"template_es"}, new String[]{template}, "id_form = 16");
    }
}