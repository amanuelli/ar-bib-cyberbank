/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Jhossept Garay
 */

public class DB20190930_1132_5261 extends DBVSUpdate {

    @Override
    public void up() {
        delete("document_types_country_codes", "id_country_code = 'CL' and mrtd_document_type = 'I' and id_document_type = 'CI'");
    }

}