/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3222
 *
 * @author ahernandez
 */
public class DB20171122_1621_3222 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("frontend.avatar.provider", "com.technisys.omnichannel.client.frontend.utils.GravatarProvider", ConfigurationGroup.TECNICAS, "frontend", new String[]{"class"});
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='frontend.avatar.provider'"); 
        
        insertOrUpdateConfiguration("frontend.avatar.provider.Gravatar", "https://www.gravatar.com/avatar/", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty", "url"});
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='frontend.avatar.provider.Gravatar'");
        
    }
}