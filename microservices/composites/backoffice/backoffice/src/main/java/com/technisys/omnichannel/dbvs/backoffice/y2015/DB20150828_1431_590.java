/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author ?
 */
public class DB20150828_1431_590 extends DBVSUpdate {

    @Override
    public void up() {
        //borro la actividad anterior
        deleteActivity("session.recoverPasswordWithOTP.step1");
        
        //inserto la nueva actividad
        insertActivity("session.recoverPasswordWithSecondFactor.step1", "com.technisys.omnichannel.client.activities.recoverpassword.RecoverPasswordStep1Activity", "recoverPassword", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.loginWithOTP");

    }
}
