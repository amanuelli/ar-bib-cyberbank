
/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3822
 *
 * @author midned
 */
public class DB20180307_1720_3822 extends DBVSUpdate {

    @Override
    public void up() {
        
        String[] keys;
        String[] values;

        keys = new String[]{"id_permission"};
        values = new String[]{"user.preferences"};

        update("activity_products", keys, values, "id_activity='preferences.userData.updateMail.send'");
    
    
        keys = new String[]{"id_permission"};
        values = new String[]{"core.authenticated"};

        update("activity_products", keys, values, "id_activity='preferences.userData.modify.send'");
        
        
    }

}