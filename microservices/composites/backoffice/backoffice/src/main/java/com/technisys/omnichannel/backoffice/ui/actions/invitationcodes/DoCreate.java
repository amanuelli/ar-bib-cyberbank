    /*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.CreateInvitationCodeData;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.TransactionRequest;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 */
@Results({
    @Result(name = "success", type = "json", params = {"root", "resultMap"})
})
public class DoCreate extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        TransactionRequest request = new TransactionRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.invitationCodes.create");

        CreateInvitationCodeData data = new CreateInvitationCodeData();

        data.setDocumentCountry(documentCountry);
        data.setDocumentType(documentType);
        data.setDocumentNumber(documentNumber);
        data.setFullName(fullName);
        data.setUserLang(userLang);
        data.setAccessType(accessType);
        data.setAccount(account);
        data.setAdministrationScheme(administrationScheme);
        data.setSignatureQty(signatureQty);

        data.setComment(comments);

        request.setTransactionData(data);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        if (response != null && (response.getReturnCode().equals(ReturnCodes.OK) || response.getReturnCode().equals(ReturnCodes.BACKOFFICE_REQUIRES_APPROVAL))) {
            resultMap = UIUtils.generateResultMap(response);
            return SUCCESS;
        } else {
            throw new JSONException(response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private String documentCountry;
    private String documentType;
    private String documentNumber;
    private String fullName;
    private String userLang;
    private String accessType;
    private String account;
    private String administrationScheme;
    private Integer signatureQty;
    private String comments;

    public void setDocumentCountry(String documentCountry) {
        this.documentCountry = documentCountry;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public void setAdministrationScheme(String administrationScheme) {
        this.administrationScheme = administrationScheme;
    }

    public void setSignatureQty(Integer signatureQty) {
        this.signatureQty = signatureQty;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private transient Map<String, Object> resultMap;

    public Map<String, Object> getResultMap() {
        return resultMap;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
