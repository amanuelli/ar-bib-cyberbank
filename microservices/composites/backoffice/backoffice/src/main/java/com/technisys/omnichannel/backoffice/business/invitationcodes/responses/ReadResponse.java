/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.responses;

import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.InvitationCode;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sebastian Barbosa
 */
public class ReadResponse extends IBResponse {

    private InvitationCode invitationCode;
    private List<Group> groupList;

    public ReadResponse(IBRequest request) {
        super(request);

        groupList = new ArrayList<>();
    }

    public InvitationCode getInvitationCode() {
        return invitationCode;
    }

    public void setInvitationCode(InvitationCode invitationCode) {
        this.invitationCode = invitationCode;
    }

    public List<Group> getGroupList() {
        return groupList;
    }

    public void setGroupList(List<Group> groupList) {
        this.groupList = groupList;
    }

    public void addGroup(Group group) {
        this.groupList.add(group);
    }

    @Override
    public String toString() {
        return "ReadResponse{" + "invitationCode=" + invitationCode + '}';
    }
}
