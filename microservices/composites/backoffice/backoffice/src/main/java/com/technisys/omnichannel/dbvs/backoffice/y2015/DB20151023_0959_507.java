/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20151023_0959_507 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("enrollment.wizard.additionalFactorValidation");
        insertActivity("enrollment.wizard.additionalFactorValidation", "com.technisys.omnichannel.client.activities.enrollment.WizardAdditionalFactorValidationActivity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);

        update("activities", new String[]{"id_activity", "component_fqn"}, new String[]{"enrollment.wizard.finish", "com.technisys.omnichannel.client.activities.enrollment.WizardFinishActivity"}, "id_activity = 'enrollment.wizzard.finish'");
        update("activities", new String[]{"id_activity", "component_fqn"}, new String[]{"enrollment.wizard.personalData", "com.technisys.omnichannel.client.activities.enrollment.WizardPersonalDataActivity"}, "id_activity = 'enrollment.wizzard.personalData'");
        update("activities", new String[]{"id_activity", "component_fqn"}, new String[]{"enrollment.wizard.pre", "com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity"}, "id_activity = 'enrollment.wizzard.pre'");
        update("activities", new String[]{"id_activity", "component_fqn"}, new String[]{"enrollment.wizard.resendVerificationCode", "com.technisys.omnichannel.client.activities.enrollment.WizardResendVerificationCodeActivity"}, "id_activity = 'enrollment.wizzard.resendVerificationCode'");
        update("activities", new String[]{"id_activity", "component_fqn"}, new String[]{"enrollment.wizard.verificationCode", "com.technisys.omnichannel.client.activities.enrollment.WizardVerificationCodeActivity"}, "id_activity = 'enrollment.wizzard.verificationCode'");

    }
}
