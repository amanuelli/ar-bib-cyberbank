/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.requests;

import com.technisys.omnichannel.core.IBRequest;

/**
 *
 * @author Sebastian Barbosa
 */
public class ReadUserRequest extends IBRequest {

    private String documentCountry;
    private String documentType;
    private String documentNumber;

    public String getDocumentCountry() {
        return documentCountry;
    }

    public void setDocumentCountry(String documentCountry) {
        this.documentCountry = documentCountry;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    @Override
    public String toString() {
        return "FindClientRequest{" + "documentCountry=" + documentCountry + ", documentType=" + documentType + ", documentNumber=" + documentNumber + '}';
    }

}
