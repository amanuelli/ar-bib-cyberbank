/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4329
 *
 * @author msouza
 */
public class DB20180518_1724_4329 extends DBVSUpdate {

    @Override
    public void up() {
 
        // AUTH key del proyecto Firebase (FCM)
        insertOrUpdateConfiguration("pushNotifications.fcm.authKey", "AAAA1BuyPuM:APA91bG-DUi3B2W97s6lXSFMTYwVJV03Hxev6Sj_lv7jy2jHz1qjWmW99cawwDPiqDj4G56tGTTxEhyfjyNEKZg9s3XXfiIZtAg_Xk8Ci8Sa92jShVLQwPWqnkESN7wIrsSZ5xQ_zHs1",
                ConfigurationGroup.TECNICAS, "pushNotifications", new String[]{"notEmpty"});
    }
}