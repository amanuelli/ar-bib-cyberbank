/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.requests;

import com.technisys.omnichannel.core.IBRequest;

/**
 *
 * @author Sebastian Barbosa
 */
public class ReadEnvironmentRequest extends IBRequest {

    private String account;

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "FindEnvironmentRequest{" + "account=" + account + '}';
    }

}
