/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

public class DB20180802_1037_5070 extends DBVSUpdate {

    @Override
    public void up() {
        insert("activity_products", new String[] { "id_activity", "id_field", "id_permission" }, new String[] { "core.cancelTransaction", "NONE", "core.cancelTransaction" });
        update("permissions_credentials_groups", new String[] { "id_credential_group" }, new String[] { "accessToken-pin" }, "id_permission='core.cancelTransaction'");
        
    }
}