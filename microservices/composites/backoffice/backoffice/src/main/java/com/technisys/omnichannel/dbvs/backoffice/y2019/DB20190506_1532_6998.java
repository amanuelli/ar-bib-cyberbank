/* 
 * Copyright 2019 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6998
 *
 * @author Marcelo Bruno
 */
public class DB20190506_1532_6998 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("client.productRequest.invitationCode.validity", "48h", ConfigurationGroup.SEGURIDAD, "others", new String[]{"notEmpty", "interval"});
    }

}