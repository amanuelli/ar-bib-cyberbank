/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20151028_1702_1426 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("core.attempts.features", "session.loginWithSecondFactorAndPassword.step1=login|session.loginWithSecondFactorAndPassword.step2=login|session.recoverPasswordWithSecondFactor.step1=login|session.recoverPinAndPassword.step2=recoverCredential|session.incrementCaptchaAttempts=login|session.recoverPinWithSecondFactor.step1=login");
    }
}
