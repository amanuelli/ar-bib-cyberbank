/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author ?
 */
public class DB20151015_1308_930 extends DBVSUpdate {

    @Override
    public void up() {
       
        update("configuration", new String[]{"id_sub_group"}, new String[]{"frontend"}, "id_field = 'session.duration'");
        insertOrUpdateConfiguration("session.timeBeforeTimeoutNotification", "30s", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
        
    }
}
