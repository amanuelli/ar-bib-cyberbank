/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ?
 */
public class DB201202081410_11930 extends DBVSUpdate {

    @Override
    public void up() {

        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required", "id_group", "auditable"};
        String[] fieldValues = new String[]{"rub.mobile.desktop", "1", "1", "com.technisys.rubicon.business.desktop.activities.MobileDesktopActivity", null, null, "0"};
        insert("activities", fieldNames, fieldValues);

    }
}