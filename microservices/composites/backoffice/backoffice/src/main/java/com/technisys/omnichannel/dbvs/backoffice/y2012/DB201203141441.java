/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201203141441 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"value"}, new String[]{"transactions|news|notifications|frequentTasks|banners"}, "id_field = 'rubicon.desktop.defaultWidgets'");
        update("configuration", new String[]{"value"}, new String[]{"transactions|news|notifications|frequentTasks|banners|pymes|goals|geolocalization|twitter"}, "id_field = 'rubicon.desktop.defaultWidgets.corporate'");
        update("configuration", new String[]{"value"}, new String[]{"transactions|news|notifications|frequentTasks|banners|pymes|goals|geolocalization|twitter"}, "id_field = 'rubicon.desktop.defaultWidgets.retail'");

        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"1", "3"}, "id = '1'");
        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"1", "4"}, "id = '2'");
        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"1", "6"}, "id = '3'");
        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"1", "7"}, "id = '4'");
        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"2", "4"}, "id = '6'");
        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"2", "6"}, "id = '7'");
        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"2", "5"}, "id = '9'");
        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"2", "2"}, "id = '10'");
        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"2", "3"}, "id = '11'");
        update("client_desktop_widgets", new String[]{"default_column", "default_row"}, new String[]{"1", "1"}, "id = '12'");
    }
}