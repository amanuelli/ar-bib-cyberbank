/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author ?
 */
public class DB20200424_1315_11052 extends DBVSUpdate {

    @Override
    public void up() {

        insertOrUpdateConfiguration("services.notification.communicationTray.pending", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});;
        //Los asistentes digitales no estan habilitados para hacer transacciones
        insertOrUpdateConfiguration("core.notTransactionalChannels", "assistant", ConfigurationGroup.NEGOCIO, null, new String[]{});
    }
}
