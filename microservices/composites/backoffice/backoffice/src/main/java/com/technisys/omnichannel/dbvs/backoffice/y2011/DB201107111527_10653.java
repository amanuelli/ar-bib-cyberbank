/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107111527_10653 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"value"};
        String[] fieldValues = new String[]{"$|U$S"};

        update("configuration", fieldNames, fieldValues, "id_field='rubicon.currency.list'");
        update("configuration", fieldNames, fieldValues, "id_field='rubicon.currency.sendCheck.list'");


        fieldValues = new String[]{"$|U$S|EUR"};

        update("configuration", fieldNames, fieldValues, "id_field='rubicon.currency.paySalary.list'");
        update("configuration", fieldNames, fieldValues, "id_field='rubicon.currency.paySuppliers.list'");
        update("configuration", fieldNames, fieldValues, "id_field='rubicon.currency.transferInternal.list'");

        fieldValues = new String[]{"U$S|EUR"};
        update("configuration", fieldNames, fieldValues, "id_field='rubicon.widget.quotation.list'");

    }
}