package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author emordezki
 */
public class DB20190122_1124_6392 extends DBVSUpdate {
    @Override
    public void up() {
        insertActivity("files.downloadLines", "com.technisys.omnichannel.client.activities.files.DownloadLinesActivity", "files", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}