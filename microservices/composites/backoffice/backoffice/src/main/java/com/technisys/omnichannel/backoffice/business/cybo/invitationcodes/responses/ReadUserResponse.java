package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.responses;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import com.technisys.omnichannel.backoffice.domain.cybo.Customer;
import com.technisys.omnichannel.backoffice.domain.cybo.invitationcodes.ClientEnvironment;

public class ReadUserResponse implements Serializable {
    
    private Customer client;
    private Customer user;
    private List<ClientEnvironment> accountList;

    public ReadUserResponse() {
        this.accountList = new ArrayList<ClientEnvironment>();
    }

    public Customer getClient() {
        return client;
    }
    public void setClient(Customer client) {
        this.client = client;
    }
    public Customer getUser() {
        return user;
    }
    public void setUser(Customer user) {
        this.user = user;
    }
    public List<ClientEnvironment> getAccountList() {
        return accountList;
    }
    public void setAccountList(List<ClientEnvironment> accountList) {
        this.accountList = accountList;
    }
}
