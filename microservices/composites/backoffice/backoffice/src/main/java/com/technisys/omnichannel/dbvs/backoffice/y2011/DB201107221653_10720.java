/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107221653_10720 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};

        String[] fieldValues = new String[]{"rub.accounts.transferInternalToolTip", "1", "1", "com.technisys.rubicon.business.accounts.activities.TransferInternalToolTipActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.creditcards.payCreditCardToolTip", "1", "1", "com.technisys.rubicon.business.creditcards.activities.PayCreditCardToolTipActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.loans.payLoanToolTip", "1", "1", "com.technisys.rubicon.business.loans.activities.PayLoanToolTipActivity"};
        insert("activities", fieldNames, fieldValues);

    }
}