/*
 *  Copyright 2012 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Diego Curbelo &lt;dcurbelo@technisys.com&gt;
 */
public class DB201209201631_119 extends DBVSUpdate {

    @Override
    public void up() {

        insert("activities", new String[]{"id_activity", "version", "enabled", "component_fqn"},
                new String[]{"rub.listEnvironments", "1", "1", "com.technisys.rubicon.business.misc.activities.ListEnvironmentsActivity"});

        insert("activities", new String[]{"id_activity", "version", "enabled", "component_fqn"},
                new String[]{"rub.desktop.loadLayout", "1", "1", "com.technisys.rubicon.business.desktop.activities.LoadLayoutActivity"});

        insert("activities", new String[]{"id_activity", "version", "enabled", "component_fqn"},
                new String[]{"rub.environments.read", "1", "1", "com.technisys.rubicon.business.misc.activities.ReadEnvironmentActivity"});
    }
}