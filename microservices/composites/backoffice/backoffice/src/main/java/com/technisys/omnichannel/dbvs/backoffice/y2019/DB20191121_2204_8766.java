/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Marcelo Bruno
 */

public class DB20191121_2204_8766 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("onboarding.documents.MRZ.positions", "{URY:{start:9,end:17}}", ConfigurationGroup.NEGOCIO, "onboarding", new String[]{"notEmpty"});
    }

}