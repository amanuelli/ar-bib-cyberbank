/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2441
 *
 * @author mricotta
 */
public class DB20170704_1545_2441 extends DBVSUpdate {

    @Override
    public void up() {

        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        String[] formFieldsMessages = new String[] { "id_message", "lang", "id_field", "id_form", "form_version",
                "modification_date", "value" };

        String idField = "amount";
        String version = "1";
        String msg = "Ingrese el importe de la transacción, tenga en cuenta utilizar la coma como separador de decimales";
        String[] idForms = { "additionalCreditCardRequest", "cashAdvance", "transferInternal", "transferForeign",
                "transferLocal", "payCreditCard", "mobilePrepaiment" };

        // Agregado de mensajes de ayuda para additionalCreditCardRequest,
        // cashAdvance, transferInternal, transferForeign, transferLocal,
        // payCreditCard, mobilePrepaiment
        for (String idForm : idForms) {
            delete("form_field_messages",
                    "id_message = '" + "fields." + idForm + "." + idField + ".help" + "' AND lang = 'es'");
            if ("transferForeign".equals(idForm) || "transferLocal".equals(idForm)) {
                if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                    customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                            + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Se realizarán los cambios de moneda necesarios en función de las monedas de las cuentas de débito y la moneda de la transferencia. "+msg+"')");
                }else{
                    insert("form_field_messages", formFieldsMessages,
                        new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version,
                                date,
                                "Se realizarán los cambios de moneda necesarios en función de las monedas de las cuentas de débito y la moneda de la transferencia. "
                                        .concat(msg) });
                }     
            } else {
                if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                    customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                            + date + "', 'YYYY-MM-DD HH24:MI:SS'),'"+msg+"')");
                }else{    
                    insert("form_field_messages", formFieldsMessages, new String[] {
                        "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, msg });
                }    
            }
        }

        // Agregado de mensajes para el caso especial de payLoan por tener
        // distinto idField
        String idForm = "payLoan";
        idField = "otherLoanAmount";
        delete("form_field_messages",
                "id_message = '" + "fields." + idForm + "." + idField + ".help" + "' AND lang = 'es'");
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'"+msg+"')");
        }else{ 
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help",
                "es", idField, idForm, version, date, msg });
        }    
    }
}