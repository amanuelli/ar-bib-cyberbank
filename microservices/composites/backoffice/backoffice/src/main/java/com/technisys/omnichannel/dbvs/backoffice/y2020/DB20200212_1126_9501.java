/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author KevinGaray
 */

public class DB20200212_1126_9501 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("administration.restrictions.manage.pre");
        deleteActivity("administration.restrictions.manage.send");

        insertActivity(
                "administration.restrictions.manage.pre", "com.technisys.omnichannel.client.activities.administration.restrictions.ManageAccessRestrictionsPreActivity",
                "administration",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity(
                "administration.restrictions.manage.send", "com.technisys.omnichannel.client.activities.administration.restrictions.ManageAccessRestrictionsActivity",
                "administration",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Admin, "administration.manage");
    }

}