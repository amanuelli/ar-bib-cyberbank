/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5296
 *
 * @author lpoll
 */
public class DB20181107_0911_5296 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_messages", new String[]{"value"}, new String[]{"Cashier's cheque request"}, "id_form='requestOfManagementCheck' AND id_message='forms.requestOfManagementCheck.formName' AND lang='en'");
        
        update("form_field_messages", new String[]{"value"}, new String[]{"Importe"}, "id_form='payCreditCard' AND id_field='amount' AND id_message='fields.payCreditCard.amount.label' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Amount"}, "id_form='payCreditCard' AND id_field='amount' AND id_message='fields.payCreditCard.amount.label' AND lang='en'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Importância"}, "id_form='payCreditCard' AND id_field='amount' AND id_message='fields.payCreditCard.amount.label' AND lang='pt'");
    }

}