/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6072
 *
 * @author emordezki
 */
public class DB20181206_1033_6072 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("backoffice.campaigns.showDismissableOption", "false");
    }
}