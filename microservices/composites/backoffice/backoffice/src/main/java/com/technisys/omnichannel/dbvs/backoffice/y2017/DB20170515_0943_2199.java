/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.sql.Types;

/**
 * Related issue: MANNAZCA-2199
 *
 * @author sbarbosa
 */
public class DB20170515_0943_2199 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("field.subType.bankselector", "foreigners", ConfigurationGroup.OTHER, "forms", new String[]{"notEmpty"});

        deleteActivity("banks.list");
        insertActivity("banks.list", "com.technisys.omnichannel.client.activities.banks.ListBanksActivity", "forms", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

        // Recreo la tabla client_banks
        dropTable("client_banks");
        ColumnDefinition[] columns = new ColumnDefinition[]{
            new ColumnDefinition("code_type", Types.VARCHAR, 10, 0, false, null),
            new ColumnDefinition("code_number", Types.VARCHAR, 15, 0, false, null),
            new ColumnDefinition("bank_name", Types.VARCHAR, 255, 0, false, null),
            new ColumnDefinition("bank_address", Types.VARCHAR, 500, 0, true, null),
            new ColumnDefinition("bank_country_code", Types.VARCHAR, 2, 0, false, null)
        };
        createTable("client_banks", columns, new String[]{"code_type", "code_number"});
        createIndex("client_banks", "idx_cb_name", new String[]{"bank_name"});
        createIndex("client_banks", "idx_cb_country", new String[]{"bank_country_code"});

        columns = new ColumnDefinition[]{
            new ColumnDefinition("id_field", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("id_form", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("form_version", Types.INTEGER, 11, 0, false, null),
            new ColumnDefinition("code_type", Types.VARCHAR, 50, 0, false, null)
        };
        createTable("form_field_bankselector_codes", columns, new String[]{"id_field", "id_form", "form_version", "code_type"});

        createForeignKey("form_field_bankselector_codes", "fk_ffbscodes_ffbs", new String[]{"id_field", "id_form", "form_version"}, "form_field_bankselector", new String[]{"id_field", "id_form", "form_version"}, true, true);

        dropColumn("form_field_bankselector", "default_value");
        dropColumn("form_field_bankselector", "show_blank_option");

        insertOrUpdateConfiguration("bankselector.validCodeTypes", "ABA|BLZ|CHIPS|SWIFT", ConfigurationGroup.OTHER, "forms", new String[]{"notEmpty"});
    }

}
