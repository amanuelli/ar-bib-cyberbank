/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
/**
 *
 * @author fpena
 */
public class DB20150729_1312_433 extends DBVSUpdate {

    @Override
    public void up() {
        
        //No encontre el uso de este widget, y no estaba cargando en el escritorio, así que lo borré.
        delete("widgets", "id='position'");
    }
}
