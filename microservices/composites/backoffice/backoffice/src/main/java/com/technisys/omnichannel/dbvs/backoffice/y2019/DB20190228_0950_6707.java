/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author pbregonzio
 */

public class DB20190228_0950_6707 extends DBVSUpdate {

    @Override
    public void up() {
        String date = null;
        String[] fieldNames = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        String[] fieldValues = new String[]{"fields.salaryPayment.uploadBy.help", "en", "uploadBy", "salaryPayment", "1", "Please consider that changing this will erase your transaction data.", "2019-02-28 17:38:27"};
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                    + " VALUES ('" + fieldValues[0] + "', '" + fieldValues[1] + "', '" + fieldValues[2] + "', '" + fieldValues[3] + "', '" + fieldValues[4] + "','" + fieldValues[5] + "', TO_DATE('2019-02-28 17:38:27', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_field_messages", fieldNames, fieldValues);
        }

        fieldValues = new String[]{"fields.salaryPayment.uploadBy.help", "es", "uploadBy", "salaryPayment", "1", "Tenga en cuenta que cambiar esto borrará los datos para la transacción.", "2019-02-28 17:38:27"};
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                    + " VALUES ('" + fieldValues[0] + "', '" + fieldValues[1] + "', '" + fieldValues[2] + "', '" + fieldValues[3] + "', '" + fieldValues[4] + "','" + fieldValues[5] + "', TO_DATE('2019-02-28 17:38:27', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_field_messages", fieldNames, fieldValues);
        }

        String[] ordinal = new String[]{"ordinal"};

        update("form_fields", ordinal, new String[]{"1"},"id_form='salaryPayment' AND id_field='introSection'");
        update("form_fields", ordinal, new String[]{"2"},"id_form='salaryPayment' AND id_field='debitAccount'");
        update("form_fields", ordinal, new String[]{"3"},"id_form='salaryPayment' AND id_field='reference'");
        update("form_fields", ordinal, new String[]{"4"},"id_form='salaryPayment' AND id_field='uploadBy'");
        update("form_fields", ordinal, new String[]{"5"},"id_form='salaryPayment' AND id_field='horizontalLine'");
        update("form_fields", ordinal, new String[]{"6"},"id_form='salaryPayment' AND id_field='inputManually'");
        update("form_fields", ordinal, new String[]{"6"},"id_form='salaryPayment' AND id_field='fileSection'");
        update("form_fields", ordinal, new String[]{"7"},"id_form='salaryPayment' AND id_field='sampleFile'");
        update("form_fields", ordinal, new String[]{"8"},"id_form='salaryPayment' AND id_field='file'");
    }
}