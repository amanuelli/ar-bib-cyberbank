/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201112051628_11764 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};
        String[] fieldValues = new String[]{"rub.changeDefaultEnvironment", "1", "1", "com.technisys.rubicon.business.misc.activities.ChangeDefaultEnvironmentActivity"};
        insert("activities", fieldNames, fieldValues);

    }
}