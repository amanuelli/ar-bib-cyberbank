/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Marcelo Bruno
 */

public class DB20200117_1650_9452 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("googlemap.apikey", "AIzaSyAeG8KAM9fZ7amUikdy5AZvsD5bYCAXH40", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty"}, null, "frontend");
    }

}