/*
 *  Copyright 2012 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201211081310_115 extends DBVSUpdate {

    @Override
    public void up() {

        String[] fields = new String[]{"id_permission"};
        insert("permissions", fields, new String[]{"rub.authenticated"});
        insert("permissions", fields, new String[]{"rub.comex.read"});

        //Agregar la credencial a los nuevos permisos
        fields = new String[]{"id_permission", "id_credential_group"};
        insert("permissions_credentials_groups", fields, new String[]{"rub.administration", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.product.read", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.product.transferInternal", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.product.transferThirdParties", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.product.transferLocal", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.product.transferAbroad", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.payment.payCustoms", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.accounts.requestCheckbookActivation", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.accounts.requestCreditAgreement", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.accounts.requestAccountOpeningCA", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.accounts.requestAccountOpeningCC", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.accounts.requestOffsetAccount", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.accounts.requestCheckComplaint", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.accounts.requestFixedDeposits", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.accounts.requestDebitCard", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.creditcards.requestChangeConditions", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.creditcards.requestDebit", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.loans.requestLeasing", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.loans.requestBusinessLoan", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.loans.requestBusinessLoanPF", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.loans.requestPersonalLoan", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.requestEnvironment", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.requestDocumentation", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.affidavit", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.foreigntrade.GAOT", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.claimBilling", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.requestAVAL", "onlyPassword"});
        insert("permissions_credentials_groups", fields, new String[]{"rub.documentExchange", "onlyPassword"});

        insert("permissions_credentials_groups", fields, new String[]{"rub.product.transferInternal", "sms"});

        delete("permissions", "id_permission in ('rub.statementlines.manage', 'core.product.alias', 'rub.accounts.transferInternal', 'rub.accounts.checkDiscount', 'rub.account.read', 'rub.loan.read', 'rub.creditcard.read', 'rub.files.upload')");

        fields = new String[]{"id_permission_required"};
        update("activities", fields, new String[]{"rub.login"}, "id_activity='rub.login'");
        update("activities", fields, new String[]{"rub.loginII"}, "id_activity='rub.loginII'");

        update("activities", fields, new String[]{"rub.product.read"}, "id_activity in ('rub.accounts.transferInternalQuery', 'rub.accounts.transferInternalToolTip', 'rub.creditcards.payCreditCardQuery', 'rub.creditcards.payCreditCardToolTip', 'rub.loans.payLoanQuery', 'rub.loans.payLoanToolTip')");
        update("activities", fields, new String[]{"rub.authenticated"}, "id_activity in ('rub.loans.list', 'rub.account.exportList', 'core.cancelTransaction', 'core.createTransactionTemplate', 'core.deleteTransactionTemplate', 'core.forms.send', 'core.listTransactions', 'core.listTransactionTemplates', 'core.moveToDraftTransaction', 'core.readForm', 'core.readTransaction', 'core.readTransactionTemplate', 'core.saveDraftTransaction', 'core.signTransaction', 'rub.account.accountMovementsDetails', 'rub.account.list', 'rub.accounts.checkDiscountPre', 'rub.accounts.sendCheckImport', 'rub.accounts.sendCheckPre', 'rub.accounts.transferBatchPre', 'rub.accounts.transferInternalPre', 'rub.banks.list', 'rub.changeDefaultEnvironment', 'rub.changeEnvironment', 'rub.check.exportList', 'rub.check.list', 'rub.creditcards.list', 'rub.creditcards.payCreditCardPre', 'rub.desktop.ignorePostLoginChecks', 'rub.desktop.listAccounts', 'rub.desktop.listNotifications', 'rub.desktop.listPendingTransactions', 'rub.desktop.listPendingTransactionsForMobile', 'rub.desktop.listQuotes', 'rub.desktop.loadLayout', 'rub.desktop.loadPymesWidget', 'rub.desktop.readTransactionNextPrevForMobile', 'rub.desktop.saveLayout', 'rub.desktop.verifyPostLoginChecks', 'rub.digitalCertificate.download', 'rub.environments.read', 'rub.goals.CreateGoal', 'rub.goals.CreateGoalPre', 'rub.goals.DeleteGoal', 'rub.goals.ListGoals', 'rub.goals.ModifyGoal', 'rub.goals.ModifyGoalPre', 'rub.listEnvironments', 'rub.loans.calculateAmount', 'rub.loans.payLoanPre', 'rub.mobile.desktop', 'rub.notificationconfig.modify', 'rub.notificationconfig.modifyPre', 'rub.notificationinbox.lastNotification', 'rub.notificationinbox.list', 'rub.notificationinbox.markAsRead', 'rub.payment.paySalaryPre', 'rub.payment.paySuppliersPre', 'rub.payments.importListPaySalary', 'rub.payments.importListPaySuppliers', 'rub.product.list', 'rub.transactionhistory.detail', 'rub.transactionhistory.listPersonalized', 'rub.transactionhistory.listPre', 'rub.transactionhistory.listPredefined', 'rub.userconfiguration.changePassword', 'rub.userconfiguration.changeUserEmail', 'rub.userconfiguration.changeUserEmailPre', 'rub.userconfiguration.loadSeals', 'rub.userconfiguration.modify', 'rub.userconfiguration.modifyPre', 'rub.userconfiguration.sendForgotPasswordEmail', 'rub.userconfiguration.uploadSeal', 'rub.users.autocomplete')");
    }
}