    /*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ReadUserRequest;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.ReadUserResponse;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.IBResponse;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 */
@Results({
    @Result(name = "success", type = "json", params = {"root", "resultMap"})
})
public class ReadUser extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ReadUserRequest request = new ReadUserRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.invitationCodes.readUser");

        request.setDocumentCountry(documentCountry);
        request.setDocumentType(documentType);
        request.setDocumentNumber(documentNumber);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        if (ReturnCodes.OK.equals(response.getReturnCode())) {
            ReadUserResponse auxResponse = (ReadUserResponse) response;
            resultMap = UIUtils.generateResultMap(response);

            resultMap.put("user", auxResponse.getUser());
            resultMap.put("clientUser", auxResponse.getClientUser());
            resultMap.put("accountList", auxResponse.getAccountList());
            resultMap.put("environment", auxResponse.getSelectedEnvironment());
            resultMap.put("clientEnvironment", auxResponse.getSelectedClientEnvironment());

            return SUCCESS;
        } else {
            throw new JSONException(response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private String documentCountry;
    private String documentType;
    private String documentNumber;

    public void setDocumentCountry(String documentCountry) {
        this.documentCountry = documentCountry;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private transient Map<String, Object> resultMap;
    private ClientUser clientUser;
    private ClientUser user;

    public Map<String, Object> getResultMap() {
        return resultMap;
    }

    public ClientUser getClientUser() {
        return clientUser;
    }

    public ClientUser getUser() {
        return user;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
