/*
 *  Copyright (c) 2020 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author pbanales
 */

public class DB20200520_1249_11339 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.signatures.create.preview", "com.technisys.omnichannel.client.activities.administration.signatures.CreateSignaturesPreviewActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.signatures.modify.preview", "com.technisys.omnichannel.client.activities.administration.signatures.ModifySignaturesPreviewActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.signatures.delete.preview", "com.technisys.omnichannel.client.activities.administration.signatures.DeleteSignaturesPreviewActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
    }

}