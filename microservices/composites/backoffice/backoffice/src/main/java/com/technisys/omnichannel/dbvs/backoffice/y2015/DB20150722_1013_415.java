/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150722_1013_415 extends DBVSUpdate {

    @Override
    public void up() {

        insertActivity("administration.signatures.list", "com.technisys.omnichannel.client.activities.administration.signatures.ListSignaturesActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.signatures.export", "com.technisys.omnichannel.client.activities.administration.signatures.ExportSignaturesActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");

        insertActivity("administration.signatures.delete.preview", "com.technisys.omnichannel.client.activities.administration.signatures.DeleteSignaturesPreviewActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.signatures.delete.send", "com.technisys.omnichannel.client.activities.administration.signatures.DeleteSignaturesActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        insertActivity("administration.signatures.create.pre", "com.technisys.omnichannel.client.activities.administration.signatures.CreateSignaturesPreActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.signatures.create.preview", "com.technisys.omnichannel.client.activities.administration.signatures.CreateSignaturesPreviewActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.signatures.create.send", "com.technisys.omnichannel.client.activities.administration.signatures.CreateSignaturesActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        insertActivity("administration.signatures.modify.pre", "com.technisys.omnichannel.client.activities.administration.signatures.ModifySignaturesPreActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.signatures.modify.preview", "com.technisys.omnichannel.client.activities.administration.signatures.ModifySignaturesPreviewActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.signatures.modify.send", "com.technisys.omnichannel.client.activities.administration.signatures.ModifySignaturesActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

    }
}
