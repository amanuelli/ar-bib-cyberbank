/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author dimoda
 */

public class DB20191126_1556_378 extends DBVSUpdate {

    @Override
    public void up() {

        updateConfiguration("core.currencies", "EUR|USD");
        insertOrUpdateConfiguration("core.masterCurrency", "USD", ConfigurationGroup.NEGOCIO,
                "others", new String[]{"notEmpty"}, "UYU|EUR|USD|GBP|JYP|UYI|ARS|BRL|CLP|CAD", "frontend");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL},
                "UPDATE environments SET last_synchronization=null");

    }

}