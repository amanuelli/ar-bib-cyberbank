/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: aMANNAZCA-2506
 *
 * @author jmanrique
 */
public class DB20170628_1039_2506 extends DBVSUpdate {

    @Override
    public void up() {
    
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsValues;
        final String idForm = "creditLetterDocumentPresentation";
        final String descForm = "Presentación de documentos bajo carta de crédito";
        String version = "1";
        final String sTrue = "1";
       
        Map<String, String> messages = new HashMap();
        
        /**** Insert Form - Presentación de documentos bajo carta de crédito ****/    
        String[] formsFields = new String[]{"id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option","id_activity", "templates_enabled", "drafts_enabled", "schedulable"};
        String[] formsFieldsValues = new String[]{idForm, version, sTrue, "comex", "process", "demo:1:3", "comex", null, sTrue, sTrue, sTrue};
        insert("forms", formsFields, formsFieldsValues);
    
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', '" + descForm + "', TO_DATE('2017-06-28 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", descForm, date});    
        }
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});
        
        /**** Insert field product selector - Carta de Crédito ****/
        formFieldsValues = new String[]{"creditLetter", idForm, version, "selector", "1", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("creditLetter.label", "Carta de Crédito en poder del banco");
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option", "default_value", "render_as"}, new String[]{"creditLetter", idForm, version, "field-small", "1", "SI", "radio"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"creditLetter", idForm, version, "SI"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"creditLetter", idForm, version, "NO"});
        messages.put("creditLetter.option.SI", "Sí");
        messages.put("creditLetter.option.NO", "No");
        
        /**** Insert field product selector - Operación ****/
        formFieldsValues = new String[]{"operation", idForm, version, "selector", "2", "value(creditLetter) == 'SI'", "value(creditLetter) == 'SI'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("operation.label", "Operación");
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as"}, new String[]{"operation", idForm, version, "field-small", "1", "combo"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"operation", idForm, version, "Operacion1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"operation", idForm, version, "Operacion2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"operation", idForm, version, "Operacion3"});
        messages.put("operation.option.Operacion1", "Operación 1");
        messages.put("operation.option.Operacion2", "Operación 2");
        messages.put("operation.option.Operacion3", "Operación 3");
        messages.put("operation.requiredError", "Debe ingresar una Operación");
        
        /**** Insert field product selector - Nro Carta de crédito ****/
        formFieldsValues = new String[]{"creditLetterNumber", idForm, version, "text", "3", "value(creditLetter) == 'NO'", "value(creditLetter) == 'NO'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("creditLetterNumber.label", "Nro. Carta de crédito");
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "max_length", "display_type", "id_validation"}, new String[]{"creditLetterNumber", idForm, version,  "30", "field-medium","withoutSpecialChars"});
        messages.put("creditLetterNumber.requiredError", "Debe ingresar un Nro. de carta de crédito");
        
        /**** Insert field product selector - Monto ****/
        formFieldsValues = new String[]{"amount", idForm, version, "amount", "4", "value(creditLetter) == 'NO'", "value(creditLetter) == 'NO'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("amount.label", "Importe");
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{"amount", idForm, version, "field-normal"});
        insert("form_field_amount_currencies", new String[]{"id_field", "id_form", "form_version", "id_currency"}, new String[]{"amount", idForm, version, "222"});
        messages.put("amount.requiredError", "El importe no es válido");
        
        /**** Insert field product selector - Nro de factura ****/
        formFieldsValues = new String[]{"billNumber", idForm, version, "text", "5", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("billNumber.label", "Nro. de Factura");
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "max_length", "display_type", "id_validation"}, new String[]{"billNumber", idForm, version,  "80", "field-medium","withoutSpecialChars"});
        messages.put("billNumber.requiredError", "Debe ingresar un Nro. de factura");
        
        /**** Insert field product selector - Importe de la utilización ****/
        formFieldsValues = new String[]{"amountOfUse", idForm, version, "amount", "6", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("amountOfUse.label", "Importe de la utilización");
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{"amountOfUse", idForm, version, "field-normal"});
        messages.put("amountOfUse.help", "Este es el importe por el cual el banco reclamará el pago de los documentos presentados bajo la LC de referencia");
        messages.put("amountOfUse.invalidError", "El importe de la utilización no es válido");
        
        /**** Insert field product selector - Documento ****/
        formFieldsValues = new String[]{"document", idForm, version, "selector", "7", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("document.label", "Documento");
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as"}, new String[]{"document", idForm, version, "field-small", "1", "combo"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"document", idForm, version, "Documento1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"document", idForm, version, "Documento2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"document", idForm, version, "Documento3"});
        messages.put("document.option.Documento1", "Documento 1");
        messages.put("document.option.Documento2", "Documento 2");
        messages.put("document.option.Documento3", "Documento 3");
        messages.put("document.requiredError", "Debe ingresar un documento");
        
        /**** Insert field product selector - Nombre courier ****/
        formFieldsValues = new String[]{"courierName", idForm, version, "text", "8", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("courierName.label", " Nombre courier");
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "max_length", "display_type", "id_validation"}, new String[]{"courierName", idForm, version,  "30", "field-normal","withoutSpecialChars"});
        messages.put("courierName.help", "Ingrese el nombre del courier con quien usted tiene cuenta");
        
        /**** Insert field product selector - Nro cuenta courier ****/
        formFieldsValues = new String[]{"courierAccountNumber", idForm, version, "text", "9", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("courierAccountNumber.label", " Nro. cuenta courier");
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "max_length", "display_type", "id_validation"}, new String[]{"courierAccountNumber", idForm, version,  "20", "field-normal","withoutSpecialChars"});
        messages.put("courierAccountNumber.help", "Ingrese su número de cuenta");
        
        /**** Insert field product selector - Adjuntos ****/
        formFieldsValues = new String[]{"attachment", idForm, version, "selector", "10", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("attachment.label", "Adjunto");
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as"}, new String[]{"attachment", idForm, version, "field-small", "1", "combo"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"attachment", idForm, version, "Adjunto1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"attachment", idForm, version, "Adjunto2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"attachment", idForm, version, "Adjunto3"});
        messages.put("attachment.option.Adjunto1", "Adjunto 1");
        messages.put("attachment.option.Adjunto2", "Adjunto 2");
        messages.put("attachment.option.Adjunto3", "Adjunto 3");
        messages.put("attachment.requiredError", "Debe ingresar un adjunto");
        
        /**** Insert field product selector - Envío discrepancias ****/
        formFieldsValues = new String[]{"shipDiscrepancies", idForm, version, "selector", "11", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("shipDiscrepancies.label", "Envío discrepancias");
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as"}, new String[]{"shipDiscrepancies", idForm, version, "field-small", "1", "radio"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"shipDiscrepancies", idForm, version, "SI"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"shipDiscrepancies", idForm, version, "NO"});
        messages.put("shipDiscrepancies.option.SI", "Sí");
        messages.put("shipDiscrepancies.option.NO", "No");
        
        /**** Insert field product selector - Observaciones ****/
        formFieldsValues = new String[]{"observations", idForm, version, "textarea", "12", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("observations.label", "Observaciones");
        insert("form_field_textarea", new String[]{"id_field", "id_form", "form_version", "max_length", "display_type"}, new String[]{"observations", idForm, version,  "500", "field-big"});
        
        /**** Insert field product selector - Disclaimer ****/
        formFieldsValues = new String[]{"disclaimer", idForm, version, "termsandconditions", "13", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("disclaimer.label", "Disclaimer");
        insert("form_field_terms_conditions", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{"disclaimer", idForm, version, "field-big"});
        messages.put("disclaimer.termsAndConditions", "Las instrucciones enviadas al banco por usted luego de las <hora de corte>. serán procesadas al siguiente día hábil bancario.\n" +
                    "Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.\n" +
                    "Su instrucción quedará completada una vez recibidos la totalidad de los documentos correspondientes en nuestras sucursales \n" +
                    "y se haya enviado la presente debidamente firmada.");
        
        /**** Insert field product selector - Emails de notificación ****/
        formFieldsValues = new String[]{"notificationEmails", idForm, version, "emaillist", "14", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("notificationEmails.label", "Emails de notificación");
        insert("form_field_emaillist", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{"notificationEmails", idForm, version, "field-big"});
        messages.put("notificationEmails.help", "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");
        
        /**** Insert field product selector - Cuerpo de notificación ****/
        formFieldsValues = new String[]{"notificationBody", idForm, version, "textarea", "15", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("notificationBody.label", "Cuerpo de notificación");
        insert("form_field_textarea", new String[]{"id_field", "id_form", "form_version", "max_length", "display_type"}, new String[]{"notificationBody", idForm, version,  "500", "field-big"});
        
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }

}
