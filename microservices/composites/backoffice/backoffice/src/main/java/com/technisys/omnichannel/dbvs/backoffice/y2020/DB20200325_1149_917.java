  /*
   *
   *  *  Copyright 2020 Technisys.
   *  *
   *  *  This software component is the intellectual property of Technisys S.A.
   *  *  You are not allowed to use, change or distribute it without express written consent from its author.
   *  *
   *  *  https://www.technisys.com
   *
   */

  package com.technisys.omnichannel.dbvs.backoffice.y2020;

  import com.technisys.omnichannel.DBVSUpdate;

  /**
   * @author cristobal
   */

  public class DB20200325_1149_917 extends DBVSUpdate {

      @Override
      public void up() {
          String idForm = "requestLoan";
          update("form_fields", new String[]{"sub_type"}, new String[]{"coreLoanType"},
                  "id_form='" + idForm + "' and id_field='loanType'");

          update("form_fields", new String[]{"sub_type"}, new String[]{"coreFundsDestination"},
                  "id_form='" + idForm + "' and id_field='fundsDestination'");
      }

  }