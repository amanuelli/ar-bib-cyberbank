/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5961
 *
 * @author emordezki
 */
public class DB20181127_1549_5961 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"value", "possible_values"}, new String[]{"222", "111|222"}, "id_field='core.masterCurrency'");
        updateConfiguration("core.currencies", "111|222");
    }
}