/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201108041604_10479 extends DBVSUpdate {

    @Override
    public void up() {
        
        // ACTIVITIES
        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};
        String[] fieldValues = new String[]{"rub.loans.calculateAmount", "1", "1", "com.technisys.rubicon.business.loans.activities.CalculateAmountActivity"};
        insert("activities", fieldNames, fieldValues);
    }
}