/*
 * Copyright (c) 2019 Technisys.
 * 
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 * 
 * https://www.technisys.com.
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author divie
 */
public class DB20190806_1136_7934 extends DBVSUpdate {

    private final String FORM_FIELD_MESSAGES = "form_field_messages";
    private final String[] FIELD_VALUE = new String[]{"value"};

    @Override
    public void up() {

        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Introducción"}, "lang='es' and id_message = 'fields.salaryPayment.introSection.label' and id_form = 'salaryPayment'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Moedas"}, "lang='pt' and id_message = 'fields.accountOpening.currencies.label' and id_form = 'accountOpening'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Número de cartão de crédito"}, "lang='pt' and id_message = 'fields.frequentDestination.creditCardNumber.label' and id_form = 'frequentDestination'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Required"}, "lang='en' and id_message = 'fields.eTransferSend.amount.requiredError' and id_form = 'eTransferSend'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Requerido"}, "lang='es' and id_message = 'fields.eTransferSend.amount.requiredError' and id_form = 'eTransferSend'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Você deve digitar um email válido"}, "lang='pt' and id_message = 'fields.creditCardRequest.email.invalidError' and id_form = 'creditCardRequest'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Valor1"}, "lang='es' and id_message = 'fields.paymentClaim.search.option.cobranza1' and id_form = 'paymentClaim'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Valor1"}, "lang='pt' and id_message = 'fields.paymentClaim.search.option.cobranza1' and id_form = 'paymentClaim'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Valor2"}, "lang='es' and id_message = 'fields.paymentClaim.search.option.cobranza2' and id_form = 'paymentClaim'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Valor2"}, "lang='pt' and id_message = 'fields.paymentClaim.search.option.cobranza2' and id_form = 'paymentClaim'");

        
    }
}
