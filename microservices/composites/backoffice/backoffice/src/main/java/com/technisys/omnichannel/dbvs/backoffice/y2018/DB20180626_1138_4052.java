package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20180626_1138_4052 extends DBVSUpdate {

    @Override
    public void up() {
        
        update ("form_fields", new String[]{"ordinal"}, new String[]{"1"}, "id_form='mobilePrepaiment' and id_field='phone'");
        update ("form_fields", new String[]{"ordinal"}, new String[]{"2"}, "id_form='mobilePrepaiment' and id_field='amount'");
        update ("form_fields", new String[]{"ordinal"}, new String[]{"3"}, "id_form='mobilePrepaiment' and id_field='debitAccount'");
        delete ("form_field_messages", "id_message = 'fields.mobilePrepaiment.amount.help'");
        
        update("form_field_messages", new String []{"value"}, new String []{"Número de teléfono celular"}, "id_message ='fields.mobilePrepaiment.phone.label'");
        update("form_field_messages", new String []{"value"}, new String []{"Cuenta de débito"}, "id_message ='fields.mobilePrepaiment.debitAccount.label'");
    }
}
