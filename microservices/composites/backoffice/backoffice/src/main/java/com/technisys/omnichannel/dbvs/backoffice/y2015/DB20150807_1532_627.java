/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author diegor
 */
public class DB20150807_1532_627 extends DBVSUpdate {

    @Override
    public void up() {
       
        insertOrUpdateConfiguration("transactions.rowsPerPage", "10", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});

        deleteActivity("transactions.list");
        insertActivity("transactions.list", "com.technisys.omnichannel.client.activities.transactions.ListTransactionsActivity", "transactions", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        deleteActivity("transactions.list.pre");
        insertActivity("transactions.list.pre", "com.technisys.omnichannel.client.activities.transactions.ListTransactionsPreActivity", "transactions", ActivityDescriptor.AuditLevel.None, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}
