/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150630_1333_415 extends DBVSUpdate {

    @Override
    public void up() {
        delete("permissions_credentials_groups", "id_permission='administration.manage'");
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"administration.manage", "otp"});

        insertActivity("administration.users.list", "com.technisys.omnichannel.client.activities.administration.users.ListUsersActivity", "administration.users", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.users.export", "com.technisys.omnichannel.client.activities.administration.users.ExportUsersActivity", "administration.users", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");

        insertActivity("administration.users.blockunblock.preview", "com.technisys.omnichannel.client.activities.administration.users.BlockUnblockUsersPreviewActivity", "administration.users", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.users.blockunblock.send", "com.technisys.omnichannel.client.activities.administration.users.BlockUnblockUsersActivity", "administration.users", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        insertActivity("administration.users.delete.preview", "com.technisys.omnichannel.client.activities.administration.users.DeleteUsersPreviewActivity", "administration.users", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.users.delete.send", "com.technisys.omnichannel.client.activities.administration.users.DeleteUsersActivity", "administration.users", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        insertOrUpdateConfiguration("administration.rowsPerPage", "10", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("export.csv.dateFormat", "yyyy-MM-dd", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
    }
}
