/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150810_1338_415 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("misc.listDocumentTypes");
        insertActivity("misc.listDocumentTypes", "com.technisys.omnichannel.client.activities.misc.ListDocumentTypesActivity","misc", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);

        updateConfiguration("administration.users.invite.enabled", "true");

        insertOrUpdateConfiguration("administration.users.invite.enrollmentUrl", "${BASE_URL}/enrollment/start", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});

    }
}
