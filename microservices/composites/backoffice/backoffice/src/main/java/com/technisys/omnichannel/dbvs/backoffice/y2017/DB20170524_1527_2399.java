/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2399
 *
 * @author fpena
 */
public class DB20170524_1527_2399 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(DBVS.DIALECT_MSSQL, "UPDATE ACT_GE_BYTEARRAY SET BYTES_ = CAST('<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<bpmn:definitions xmlns:bpmn=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:camunda=\"http://camunda.org/schema/1.0/bpmn\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"Definitions_1\" targetNamespace=\"http://bpmn.io/schema/bpmn\" exporter=\"Camunda Modeler\" exporterVersion=\"1.7.2\">" +
                    "  <bpmn:process id=\"demo\" name=\"Demo\" isClosed=\"false\" isExecutable=\"true\" camunda:versionTag=\"1\">" +
                    "    <bpmn:startEvent id=\"inicio\" name=\"Inicio\">" +
                    "      <bpmn:outgoing>SequenceFlow_18t0i75</bpmn:outgoing>" +
                    "    </bpmn:startEvent>" +
                    "    <bpmn:userTask id=\"enEjecucion\" name=\"En ejecucion\" camunda:candidateGroups=\"10000,10001\">" +
                    "      <bpmn:incoming>SequenceFlow_18t0i75</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_1mk2slu</bpmn:outgoing>" +
                    "    </bpmn:userTask>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_18t0i75\" sourceRef=\"inicio\" targetRef=\"enEjecucion\" />" +
                    "    <bpmn:exclusiveGateway id=\"ExclusiveGateway_0qmslys\">" +
                    "      <bpmn:incoming>SequenceFlow_1mk2slu</bpmn:incoming>" +
                    "      <bpmn:outgoing>rechazar</bpmn:outgoing>" +
                    "      <bpmn:outgoing>aceptar</bpmn:outgoing>" +
                    "    </bpmn:exclusiveGateway>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_1mk2slu\" sourceRef=\"enEjecucion\" targetRef=\"ExclusiveGateway_0qmslys\" />" +
                    "    <bpmn:endEvent id=\"aceptado\" name=\"Aceptado\">" +
                    "      <bpmn:incoming>SequenceFlow_1efkp7d</bpmn:incoming>" +
                    "    </bpmn:endEvent>" +
                    "    <bpmn:endEvent id=\"rechazado\" name=\"Rechazado\">" +
                    "      <bpmn:incoming>SequenceFlow_05mvt8h</bpmn:incoming>" +
                    "      <bpmn:errorEventDefinition errorRef=\"Error_07g0lf5\" />" +
                    "    </bpmn:endEvent>" +
                    "    <bpmn:sequenceFlow id=\"rechazar\" name=\"Rechazar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0knb9w2\">" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''rechazar''}]]></bpmn:conditionExpression>" +
                    "    </bpmn:sequenceFlow>" +
                    "    <bpmn:sequenceFlow id=\"aceptar\" name=\"Aceptar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0xnr1oc\">" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''aceptar''}]]></bpmn:conditionExpression>" +
                    "    </bpmn:sequenceFlow>" +
                    "    <bpmn:serviceTask id=\"Task_0xnr1oc\" name=\"Aceptando\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.FinishTransaction\">" +
                    "      <bpmn:incoming>aceptar</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_12ktbww</bpmn:outgoing>" +
                    "    </bpmn:serviceTask>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_12ktbww\" sourceRef=\"Task_0xnr1oc\" targetRef=\"Task_1l4zkyo\" />" +
                    "    <bpmn:serviceTask id=\"Task_0knb9w2\" name=\"Rechazando\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.CancelTransaction\">" +
                    "      <bpmn:incoming>rechazar</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_0gd6rpt</bpmn:outgoing>" +
                    "    </bpmn:serviceTask>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_0gd6rpt\" sourceRef=\"Task_0knb9w2\" targetRef=\"SendTask_0hn6yj8\" />" +
                    "    <bpmn:sendTask id=\"Task_1l4zkyo\" name=\"Notificando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.SendNotifications\">" +
                    "      <bpmn:incoming>SequenceFlow_12ktbww</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_1efkp7d</bpmn:outgoing>" +
                    "    </bpmn:sendTask>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_1efkp7d\" sourceRef=\"Task_1l4zkyo\" targetRef=\"aceptado\" />" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_05mvt8h\" sourceRef=\"SendTask_0hn6yj8\" targetRef=\"rechazado\" />" +
                    "    <bpmn:sendTask id=\"SendTask_0hn6yj8\" name=\"Notificando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.SendNotifications\">" +
                    "      <bpmn:incoming>SequenceFlow_0gd6rpt</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_05mvt8h</bpmn:outgoing>" +
                    "    </bpmn:sendTask>" +
                    "  </bpmn:process>" +
                    "  <bpmn:error id=\"Error_07g0lf5\" name=\"Rechazado\" errorCode=\"rechazado\" />" +
                    "  <bpmndi:BPMNDiagram id=\"BPMNDiagram_1\">" +
                    "    <bpmndi:BPMNPlane id=\"BPMNPlane_1\" bpmnElement=\"demo\">" +
                    "      <bpmndi:BPMNShape id=\"_BPMNShape_StartEvent_2\" bpmnElement=\"inicio\">" +
                    "        <dc:Bounds x=\"167\" y=\"110\" width=\"36\" height=\"36\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"172\" y=\"146\" width=\"26\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNShape id=\"UserTask_1vng2m0_di\" bpmnElement=\"enEjecucion\">" +
                    "        <dc:Bounds x=\"284\" y=\"88\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_18t0i75_di\" bpmnElement=\"SequenceFlow_18t0i75\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"203\" y=\"128\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"243\" y=\"128\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"243\" y=\"128\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"284\" y=\"128\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"213\" y=\"121.5\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"ExclusiveGateway_0qmslys_di\" bpmnElement=\"ExclusiveGateway_0qmslys\" isMarkerVisible=\"true\">" +
                    "        <dc:Bounds x=\"453\" y=\"103\" width=\"50\" height=\"50\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"433\" y=\"156\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_1mk2slu_di\" bpmnElement=\"SequenceFlow_1mk2slu\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"384\" y=\"128\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"453\" y=\"128\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"373.5\" y=\"106.5\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"EndEvent_0f4uktx_di\" bpmnElement=\"aceptado\">" +
                    "        <dc:Bounds x=\"875\" y=\"44\" width=\"36\" height=\"36\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"870\" y=\"83\" width=\"47\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNShape id=\"EndEvent_0xy6go4_di\" bpmnElement=\"rechazado\">" +
                    "        <dc:Bounds x=\"875\" y=\"191\" width=\"36\" height=\"36\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"865\" y=\"230\" width=\"56\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_18qjkrc_di\" bpmnElement=\"rechazar\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"478\" y=\"153\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"478\" y=\"205\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"565\" y=\"207\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"491.96016186780764\" y=\"215.87684886964172\" width=\"48\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_01odod8_di\" bpmnElement=\"aceptar\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"478\" y=\"103\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"478\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"565\" y=\"62\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"492.6593864824097\" y=\"37.00002784612868\" width=\"39\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"ServiceTask_0rqf8n9_di\" bpmnElement=\"Task_0xnr1oc\">" +
                    "        <dc:Bounds x=\"565\" y=\"22\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_12ktbww_di\" bpmnElement=\"SequenceFlow_12ktbww\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"665\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"687\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"687\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"709\" y=\"62\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"657\" y=\"55.5\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"ServiceTask_044rx2r_di\" bpmnElement=\"Task_0knb9w2\">" +
                    "        <dc:Bounds x=\"565\" y=\"169\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_0gd6rpt_di\" bpmnElement=\"SequenceFlow_0gd6rpt\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"665\" y=\"209\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"707\" y=\"209\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"641\" y=\"187.5\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"SendTask_003fzlk_di\" bpmnElement=\"Task_1l4zkyo\">" +
                    "        <dc:Bounds x=\"709\" y=\"22\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_1efkp7d_di\" bpmnElement=\"SequenceFlow_1efkp7d\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"809\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"875\" y=\"62\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"842\" y=\"40.5\" width=\"0\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_05mvt8h_di\" bpmnElement=\"SequenceFlow_05mvt8h\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"809\" y=\"209\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"875\" y=\"209\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"842\" y=\"187.5\" width=\"0\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"SendTask_141qtum_di\" bpmnElement=\"SendTask_0hn6yj8\">" +
                    "        <dc:Bounds x=\"709\" y=\"169\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "    </bpmndi:BPMNPlane>" +
                    "  </bpmndi:BPMNDiagram>" +
                    "</bpmn:definitions>' AS IMAGE) WHERE ID_= 2");
        
        customSentence(DBVS.DIALECT_MYSQL, "UPDATE ACT_GE_BYTEARRAY SET BYTES_ = '<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                    "<bpmn:definitions xmlns:bpmn=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:camunda=\"http://camunda.org/schema/1.0/bpmn\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"Definitions_1\" targetNamespace=\"http://bpmn.io/schema/bpmn\" exporter=\"Camunda Modeler\" exporterVersion=\"1.7.2\">" +
                    "  <bpmn:process id=\"demo\" name=\"Demo\" isClosed=\"false\" isExecutable=\"true\" camunda:versionTag=\"1\">" +
                    "    <bpmn:startEvent id=\"inicio\" name=\"Inicio\">" +
                    "      <bpmn:outgoing>SequenceFlow_18t0i75</bpmn:outgoing>" +
                    "    </bpmn:startEvent>" +
                    "    <bpmn:userTask id=\"enEjecucion\" name=\"En ejecucion\" camunda:candidateGroups=\"10000,10001\">" +
                    "      <bpmn:incoming>SequenceFlow_18t0i75</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_1mk2slu</bpmn:outgoing>" +
                    "    </bpmn:userTask>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_18t0i75\" sourceRef=\"inicio\" targetRef=\"enEjecucion\" />" +
                    "    <bpmn:exclusiveGateway id=\"ExclusiveGateway_0qmslys\">" +
                    "      <bpmn:incoming>SequenceFlow_1mk2slu</bpmn:incoming>" +
                    "      <bpmn:outgoing>rechazar</bpmn:outgoing>" +
                    "      <bpmn:outgoing>aceptar</bpmn:outgoing>" +
                    "    </bpmn:exclusiveGateway>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_1mk2slu\" sourceRef=\"enEjecucion\" targetRef=\"ExclusiveGateway_0qmslys\" />" +
                    "    <bpmn:endEvent id=\"aceptado\" name=\"Aceptado\">" +
                    "      <bpmn:incoming>SequenceFlow_1efkp7d</bpmn:incoming>" +
                    "    </bpmn:endEvent>" +
                    "    <bpmn:endEvent id=\"rechazado\" name=\"Rechazado\">" +
                    "      <bpmn:incoming>SequenceFlow_05mvt8h</bpmn:incoming>" +
                    "      <bpmn:errorEventDefinition errorRef=\"Error_07g0lf5\" />" +
                    "    </bpmn:endEvent>" +
                    "    <bpmn:sequenceFlow id=\"rechazar\" name=\"Rechazar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0knb9w2\">" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''rechazar''}]]></bpmn:conditionExpression>" +
                    "    </bpmn:sequenceFlow>" +
                    "    <bpmn:sequenceFlow id=\"aceptar\" name=\"Aceptar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0xnr1oc\">" +
                    "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\"><![CDATA[${action == ''aceptar''}]]></bpmn:conditionExpression>" +
                    "    </bpmn:sequenceFlow>" +
                    "    <bpmn:serviceTask id=\"Task_0xnr1oc\" name=\"Aceptando\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.FinishTransaction\">" +
                    "      <bpmn:incoming>aceptar</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_12ktbww</bpmn:outgoing>" +
                    "    </bpmn:serviceTask>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_12ktbww\" sourceRef=\"Task_0xnr1oc\" targetRef=\"Task_1l4zkyo\" />" +
                    "    <bpmn:serviceTask id=\"Task_0knb9w2\" name=\"Rechazando\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.CancelTransaction\">" +
                    "      <bpmn:incoming>rechazar</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_0gd6rpt</bpmn:outgoing>" +
                    "    </bpmn:serviceTask>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_0gd6rpt\" sourceRef=\"Task_0knb9w2\" targetRef=\"SendTask_0hn6yj8\" />" +
                    "    <bpmn:sendTask id=\"Task_1l4zkyo\" name=\"Notificando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.SendNotifications\">" +
                    "      <bpmn:incoming>SequenceFlow_12ktbww</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_1efkp7d</bpmn:outgoing>" +
                    "    </bpmn:sendTask>" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_1efkp7d\" sourceRef=\"Task_1l4zkyo\" targetRef=\"aceptado\" />" +
                    "    <bpmn:sequenceFlow id=\"SequenceFlow_05mvt8h\" sourceRef=\"SendTask_0hn6yj8\" targetRef=\"rechazado\" />" +
                    "    <bpmn:sendTask id=\"SendTask_0hn6yj8\" name=\"Notificando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.SendNotifications\">" +
                    "      <bpmn:incoming>SequenceFlow_0gd6rpt</bpmn:incoming>" +
                    "      <bpmn:outgoing>SequenceFlow_05mvt8h</bpmn:outgoing>" +
                    "    </bpmn:sendTask>" +
                    "  </bpmn:process>" +
                    "  <bpmn:error id=\"Error_07g0lf5\" name=\"Rechazado\" errorCode=\"rechazado\" />" +
                    "  <bpmndi:BPMNDiagram id=\"BPMNDiagram_1\">" +
                    "    <bpmndi:BPMNPlane id=\"BPMNPlane_1\" bpmnElement=\"demo\">" +
                    "      <bpmndi:BPMNShape id=\"_BPMNShape_StartEvent_2\" bpmnElement=\"inicio\">" +
                    "        <dc:Bounds x=\"167\" y=\"110\" width=\"36\" height=\"36\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"172\" y=\"146\" width=\"26\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNShape id=\"UserTask_1vng2m0_di\" bpmnElement=\"enEjecucion\">" +
                    "        <dc:Bounds x=\"284\" y=\"88\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_18t0i75_di\" bpmnElement=\"SequenceFlow_18t0i75\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"203\" y=\"128\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"243\" y=\"128\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"243\" y=\"128\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"284\" y=\"128\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"213\" y=\"121.5\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"ExclusiveGateway_0qmslys_di\" bpmnElement=\"ExclusiveGateway_0qmslys\" isMarkerVisible=\"true\">" +
                    "        <dc:Bounds x=\"453\" y=\"103\" width=\"50\" height=\"50\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"433\" y=\"156\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_1mk2slu_di\" bpmnElement=\"SequenceFlow_1mk2slu\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"384\" y=\"128\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"453\" y=\"128\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"373.5\" y=\"106.5\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"EndEvent_0f4uktx_di\" bpmnElement=\"aceptado\">" +
                    "        <dc:Bounds x=\"875\" y=\"44\" width=\"36\" height=\"36\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"870\" y=\"83\" width=\"47\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNShape id=\"EndEvent_0xy6go4_di\" bpmnElement=\"rechazado\">" +
                    "        <dc:Bounds x=\"875\" y=\"191\" width=\"36\" height=\"36\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"865\" y=\"230\" width=\"56\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_18qjkrc_di\" bpmnElement=\"rechazar\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"478\" y=\"153\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"478\" y=\"205\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"565\" y=\"207\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"491.96016186780764\" y=\"215.87684886964172\" width=\"48\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_01odod8_di\" bpmnElement=\"aceptar\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"478\" y=\"103\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"478\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"565\" y=\"62\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"492.6593864824097\" y=\"37.00002784612868\" width=\"39\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"ServiceTask_0rqf8n9_di\" bpmnElement=\"Task_0xnr1oc\">" +
                    "        <dc:Bounds x=\"565\" y=\"22\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_12ktbww_di\" bpmnElement=\"SequenceFlow_12ktbww\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"665\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"687\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"687\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"709\" y=\"62\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"657\" y=\"55.5\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"ServiceTask_044rx2r_di\" bpmnElement=\"Task_0knb9w2\">" +
                    "        <dc:Bounds x=\"565\" y=\"169\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_0gd6rpt_di\" bpmnElement=\"SequenceFlow_0gd6rpt\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"665\" y=\"209\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"707\" y=\"209\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"641\" y=\"187.5\" width=\"90\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"SendTask_003fzlk_di\" bpmnElement=\"Task_1l4zkyo\">" +
                    "        <dc:Bounds x=\"709\" y=\"22\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_1efkp7d_di\" bpmnElement=\"SequenceFlow_1efkp7d\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"809\" y=\"62\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"875\" y=\"62\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"842\" y=\"40.5\" width=\"0\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNEdge id=\"SequenceFlow_05mvt8h_di\" bpmnElement=\"SequenceFlow_05mvt8h\">" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"809\" y=\"209\" />" +
                    "        <di:waypoint xsi:type=\"dc:Point\" x=\"875\" y=\"209\" />" +
                    "        <bpmndi:BPMNLabel>" +
                    "          <dc:Bounds x=\"842\" y=\"187.5\" width=\"0\" height=\"13\" />" +
                    "        </bpmndi:BPMNLabel>" +
                    "      </bpmndi:BPMNEdge>" +
                    "      <bpmndi:BPMNShape id=\"SendTask_141qtum_di\" bpmnElement=\"SendTask_0hn6yj8\">" +
                    "        <dc:Bounds x=\"709\" y=\"169\" width=\"100\" height=\"80\" />" +
                    "      </bpmndi:BPMNShape>" +
                    "    </bpmndi:BPMNPlane>" +
                    "  </bpmndi:BPMNDiagram>" +
                    "</bpmn:definitions>' WHERE ID_= 2");
    }
}
