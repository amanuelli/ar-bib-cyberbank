/*
 *  Copyright 2012 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Guillermo Rosso &lt;grosso@technisys.com&gt;
 */
public class DB201211142257_92 extends DBVSUpdate {

    @Override
    public void up() {
        delete("activities", "id_activity LIKE 'rub.files.%'");
        delete("activities", "id_activity LIKE 'rub.notificationconfig.%'");
        delete("activities", "id_activity LIKE 'rub.notificationinbox.%'");

        delete("permissions", "id_permission='rub.notificationinbox.read'");
        delete("permissions", "id_permission='rub.documentExchange'");
        delete("permissions", "id_permission='rub.files.list'");
    }
}