/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3067
 *
 * @author jmanrique
 */
public class DB20170822_1458_3067 extends DBVSUpdate {

    @Override
    public void up() {
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            update("form_messages", new String[] { "value" }
                    , new String[]{"Levantamiento de discrepancias"}
                    , "id_form = 'surveyOfDiscrepancies' AND lang = 'es' "
                        + "AND id_message = 'forms.surveyOfDiscrepancies.formName'" );
        }
    }
    
}
