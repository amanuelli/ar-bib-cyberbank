/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * 
 * @author msouza
 */
public class DB20171123_1506_2909 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("socialnetworks.facebook.appId", "308134669683487", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.facebook.appSecret", "8722eac2caf608a0cd9344b709459aeb",ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"}, null, true);
        insertOrUpdateConfiguration("socialnetworks.facebook.app.version", "v2.11", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.facebook.notifications.serviceUrl", "https://graph.facebook.com/${cnf:socialnetworks.facebook.app.version}/${FACEBOOK_ID}/notifications", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.facebook.notifications.parameters.href", "notification?idCommunication=${COMMUNICATION_ID}", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.facebook.maxTemplateSize", "180", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='socialnetworks.facebook.appId'");
        updateConfiguration("socialnetworks.twitter.accessToken", "2195145145-3XunAmHRWY9E7iN8u9rrLoQuLlk5itEw5qAkNXJ");
        updateConfiguration("socialnetworks.twitter.accessTokenSecret", "0zUZOOysyWOmWMgZsnOE7c9pQOWzu7ErwC1S7IDmc1aF8");
        updateConfiguration("socialnetworks.twitter.apiKey", "hlJ3VKx9Ph047Vu9B0CPxbPXi");
        updateConfiguration("socialnetworks.twitter.apiSecret", "Jh0aDYQwBOe1D8yD5E6T6KckZMId7ty7h5Yn4ewosTT5HmQEcX");
    }
}