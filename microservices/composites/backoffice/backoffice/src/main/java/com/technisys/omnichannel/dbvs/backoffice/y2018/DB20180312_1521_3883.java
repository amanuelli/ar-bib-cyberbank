/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3822
 *
 * @author midned
 */
public class DB20180312_1521_3883 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("preferences.userData.updateMail.confirm");
        deleteActivity("preferences.userData.modify.preview");
        update("activity_products", new String[]{"id_permission"}, new String[]{"user.preferences"}, "id_activity='preferences.userData.mobilePhone.sendCode'");
    }

}