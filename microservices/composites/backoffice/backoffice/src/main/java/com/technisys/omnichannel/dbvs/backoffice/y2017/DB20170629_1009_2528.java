/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author danireb
 */
public class DB20170629_1009_2528 extends DBVSUpdate {

    @Override
    public void up() {
        
        insert("adm_ui_subcategories", new String[]{"id_subcategory", "id_category", "ordinal"}, new String[]{"frequentDestinations", "transfers", "500"});
        update("adm_ui_permissions", new String[]{"simple_id_subcategory", "medium_id_subcategory", "advanced_id_subcategory"}, new String[]{"frequentDestinations", "frequentDestinations", "frequentDestinations"}, "id_permission = 'frequentDestinations.manage'");
        update("adm_ui_permissions", new String[]{"simple_id_subcategory", "medium_id_subcategory", "advanced_id_subcategory"}, new String[]{"frequentDestinations", "frequentDestinations", "frequentDestinations"}, "id_permission = 'frequentDestinations'");
        
    }
    
    
}
