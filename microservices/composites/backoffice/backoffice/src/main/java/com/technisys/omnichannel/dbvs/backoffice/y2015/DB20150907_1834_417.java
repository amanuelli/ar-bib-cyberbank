/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author diego
 */
public class DB20150907_1834_417 extends DBVSUpdate {

    @Override
    public void up() {        
        //Borrmos el formulario requestCheckbook por si existe, y creamos el nuevo que es Solicitud de chequeras
        delete("form_field_messages", "id_form = 'requestCheckbook'");
        delete("form_messages", "id_form = 'requestCheckbook'");
        delete("form_field_ps", "id_form = 'requestCheckbook'");
        delete("form_field_selector", "id_form = 'requestCheckbook'");
        delete("form_field_branchlist", "id_form = 'requestCheckbook'");
        delete("form_field_amount", "id_form = 'requestCheckbook'");
        delete("form_field_ps_permissions", "id_form = 'requestCheckbook'");
        delete("form_field_ps_product_types", "id_form = 'requestCheckbook'");
        delete("form_field_text", "id_form = 'requestCheckbook'");
        delete("form_field_document", "id_form = 'requestCheckbook'");
        delete("form_field_messages", "id_form = 'requestCheckbook'");
        delete("forms", "id_form = 'requestCheckbook'");
                
        //forms
        String[] formField = new String[]{"id_form", "category", "enabled", "type", "id_jbpm_process", "admin_option", "id_activity", "version", "schedulable"};
        String[] formFieldValues = new String[]{"requestCheckbook", "accounts", "1", "process", "demo-1", "checkbooks", null, "1", "1"};
        insert("forms", formField, formFieldValues);
        
        //form_messages
        formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        formFieldValues = new String[]{"forms.requestCheckbook.formName", "requestCheckbook", "1", "es", "Solicitud de chequeras", "2015-01-01 12:00:00"};
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2]  + "', '" + formFieldValues[3]  + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert ("form_messages", formField, formFieldValues);
        }
        
        //form_fields
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required"};
        
        String[] formFieldsValues = new String[]{"producto", "requestCheckbook", "1", "productselector", "1", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"tipoDeCheque","requestCheckbook", "1","selector","2","TRUE","TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"cuentaDebitoDeCosto","requestCheckbook", "1","productselector","3","TRUE","FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"lugarRetiro","requestCheckbook", "1","branchlist","4","TRUE","TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"domicilio","requestCheckbook", "1","text","5","value(lugarRetiro) == 'other'","TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"cantidadDeCheques","requestCheckbook", "1","text","6","value(tipoDeCheque) == 'fanfold'","TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"cantidadDeChequeras","requestCheckbook", "1","text","7","value(tipoDeCheque) != 'fanfold'","TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"tituloAutorizadoARetirar","requestCheckbook", "1","sectiontitle","8","TRUE","FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"documento","requestCheckbook", "1","document","9","TRUE","FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"nombre","requestCheckbook", "1","text","10","TRUE","FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"primerApellido","requestCheckbook", "1","text","11","TRUE","FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"segundoApellido","requestCheckbook", "1","text","12","TRUE","FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        //form_field_ps
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option"};
        
        formFieldsValues = new String[]{"producto", "requestCheckbook", "1", "field-big", "0"};
        insert("form_field_ps", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"cuentaDebitoDeCosto", "requestCheckbook", "1", "field-big", "0"};
        insert("form_field_ps", formFields, formFieldsValues);
  
        //form_field_ps_permissions
        formFields = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        
        formFieldsValues = new String[]{"producto", "requestCheckbook", "1", "product.read"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"producto", "requestCheckbook", "1", "accounts.requestCheckbook"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"cuentaDebitoDeCosto", "requestCheckbook", "1", "transfer.internal"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        //form_field_ps_product_types
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        
        formFieldsValues = new String[]{"producto", "requestCheckbook", "1", "CC"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"cuentaDebitoDeCosto", "requestCheckbook", "1", "CC"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"cuentaDebitoDeCosto", "requestCheckbook", "1", "CA"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        //form_field_selector
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option"};
        formFieldsValues = new String[]{"tipoDeCheque", "requestCheckbook", "1", "field_big", "normalSinCruzar", "0"};
        insert("form_field_selector", formFields, formFieldsValues);
     
        //form_field_selector_options
        formFields = new String[]{"id_field","id_form", "form_version", "value"};
        
        formFieldsValues = new String[]{"tipoDeCheque","requestCheckbook", "1","diferidoCruzado"};
        insert("form_field_selector_options", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"tipoDeCheque", "requestCheckbook", "1", "diferidoSinCruzar"};
        insert("form_field_selector_options", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"tipoDeCheque", "requestCheckbook", "1", "fanfold"};
        insert("form_field_selector_options", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"tipoDeCheque", "requestCheckbook", "1", "normalCruzado"};
        insert("form_field_selector_options", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"tipoDeCheque", "requestCheckbook", "1", "normalSinCruzar"};
        insert("form_field_selector_options", formFields, formFieldsValues);
        
        //form_field_branchlist
        formFields = new String[]{"id_field","id_form","form_version", "display_type", "show_other_option"};
        formFieldsValues = new String[]{"lugarRetiro","requestCheckbook", "1","field-medium", "1"};
        insert("form_field_branchlist", formFields, formFieldsValues);
        
        //form_field_text
        formFields = new String[]{"id_field","id_form","form_version", "min_length","max_length","display_type","id_validation"};
        
        formFieldsValues = new String[]{"cantidadDeChequeras","requestCheckbook", "1","0","7","field-small","onlyNumbers"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"cantidadDeCheques","requestCheckbook", "1","0","7","field-small","onlyNumbers"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"domicilio","requestCheckbook", "1","0","100","field-big", null};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"nombre","requestCheckbook", "1","0","30","field-medium", null};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"primerApellido","requestCheckbook", "1","0","30","field-medium", null};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"segundoApellido","requestCheckbook", "1","0","30","field-medium", null};
        insert("form_field_text", formFields, formFieldsValues);
        
        //form_field_document
        formFields = new String[]{"id_field","id_form", "form_version"};
        formFieldsValues = new String[]{"documento","requestCheckbook", "1"};
        insert("form_field_document", formFields, formFieldsValues);

        //Solo doy de alta español, y las key que tengan valor
        Map<String, String> messages = new HashMap();
        messages.put("cantidadDeChequeras.help","Indique la cantidad de chequeras que desea.");
        messages.put("cantidadDeChequeras.label","Cantidad de chequeras");
        messages.put("cantidadDeCheques.help","Debe solicitar al menos 500 cheques y la cantidad debe ser múltiplo de 100.");
        messages.put("cantidadDeCheques.label","Cantidad de cheques");
        messages.put("cuentaDebitoDeCosto.help","En este campo se indica de qué cuenta se debitará la comisión por la transferencia, si queda en blanco se toma la cuenta de débito.");
        messages.put("cuentaDebitoDeCosto.label","Cuenta débito de costo");
        messages.put("documento.label","Documento");
        messages.put("domicilio.label","Domicilio");
        messages.put("lugarRetiro.label","Lugar de retiro");
        messages.put("lugarRetiro.showOtherText","Dirección entrega a domicilio");
        messages.put("nombre.label","Nombre");
        messages.put("primerApellido.label","Primer apellido");
        messages.put("producto.label","Cuenta");
        messages.put("segundoApellido.label","Segundo apellido");
        messages.put("tipoDeCheque.label","Tipo de cheque");
        messages.put("tipoDeCheque.option.diferidoCruzado","Diferido cruzado");
        messages.put("tipoDeCheque.option.diferidoSinCruzar","Diferido sin cruzar");
        messages.put("tipoDeCheque.option.fanfold","Fanfold");
        messages.put("tipoDeCheque.option.normalCruzado","Normal cruzado");
        messages.put("tipoDeCheque.option.normalSinCruzar","Normal sin cruzar");
        messages.put("tituloAutorizadoARetirar.label","Autorizado a retirar/recibir chequera");        
        
        formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.requestCheckbook." + key, "es", key.substring(0, key.indexOf(".")), "requestCheckbook", "1", messages.get(key), "2015-01-01 12:00:00"};
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2]  + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "', '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }
}
