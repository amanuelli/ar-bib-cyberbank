/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
/**
 *
 * @author Jhossept Garay
 */
public class DB20190710_1048_7007 extends DBVSUpdate {
     @Override
    public void up() {

         deleteActivity("administration.signatures.create.preview");
         deleteActivity("administration.signatures.modify.preview");
         deleteActivity("administration.signatures.delete.preview");
    }
}
