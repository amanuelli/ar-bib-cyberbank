/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201106171755_10234 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"id_field"}, new String[]{"rubicon.currency.list"}, "id_field = 'rubicon.currency.payCreditCard.list'");
    }
}