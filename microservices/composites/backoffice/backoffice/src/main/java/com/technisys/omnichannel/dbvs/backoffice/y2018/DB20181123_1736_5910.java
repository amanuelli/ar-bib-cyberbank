/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5910
 *
 * @author lpoll
 */
public class DB20181123_1736_5910 extends DBVSUpdate {

    @Override
    public void up() {
        delete("form_field_amount_currencies", "id_form = 'payThirdPartiesLoan' AND id_field='amount'");
    }
}