package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor.AuditLevel;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor.Kind;

/**
 * @author mcheveste
 */

public class DB20190121_1744_6383 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("files.list.transaction.lines", "com.technisys.omnichannel.client.activities.files.ListTransactionLinesActivity", "files", AuditLevel.Header, Kind.Other, "core.authenticated");
    }
}
