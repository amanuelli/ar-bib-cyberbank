/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150916_0952_890 extends DBVSUpdate {

    @Override
    public void up() {
        deleteConfiguration("backoffice.invitationCodes");
        insertOrUpdateConfiguration(
                "backoffice.invitationCodes.unmaskedLength",
                "4",
                ConfigurationGroup.TECNICAS, "others", new String[]{"notEmpty", "integer"});

        insertOrUpdateConfiguration(
                "backoffice.invitationCodes.invalidDocumentTypes",
                "RUT",
                ConfigurationGroup.TECNICAS, "others", new String[]{"notEmpty"});

        delete("backoffice_permissions", "id_backoffice_permission='backoffice.invitationCodes.approve'");
        insertBackofficePermission("backoffice.invitationCodes.approve");

        update("backoffice_activities", new String[]{"id_permission_required", "id_group"}, new String[]{"backoffice.invitationCodes.list", "backoffice.invitationcodes"}, "id_activity='backoffice.invitationCodes.list'");
        update("backoffice_activities", new String[]{"id_permission_required", "id_group"}, new String[]{"backoffice.invitationCodes.list", "backoffice.invitationcodes"}, "id_activity='backoffice.invitationCodes.read'");
        update("backoffice_activities", new String[]{"id_permission_required", "id_group"}, new String[]{"backoffice.invitationCodes.manage", "backoffice.invitationcodes"}, "id_activity='backoffice.invitationCodes.create'");

        deleteBackofficeActivity("backoffice.invitationCodes.cancel");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"backoffice.invitationCodes.cancel", "1", "com.technisys.omnichannel.backoffice.business.invitationcodes.activities.CancelActivity", "backoffice.invitationCodes.manage", null, "backoffice.invitationcodes", "2"});

        deleteBackofficeActivity("backoffice.invitationCodes.resend");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"backoffice.invitationCodes.resend", "1", "com.technisys.omnichannel.backoffice.business.invitationcodes.activities.ResendActivity", "backoffice.invitationCodes.manage", null, "backoffice.invitationcodes", "2"});

        deleteBackofficeActivity("backoffice.invitationCodes.createPre");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"backoffice.invitationCodes.createPre", "1", "com.technisys.omnichannel.backoffice.business.invitationcodes.activities.CreatePreActivity", "backoffice.invitationCodes.manage", null, "backoffice.invitationcodes", "1"});

        deleteBackofficeActivity("backoffice.invitationCodes.create");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"backoffice.invitationCodes.create", "1", "com.technisys.omnichannel.backoffice.business.invitationcodes.activities.CreateActivity", "backoffice.invitationCodes.manage", null, "backoffice.invitationcodes", "2"});

        deleteBackofficeActivity("backoffice.invitationCodes.listDocumentTypes");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"backoffice.invitationCodes.listDocumentTypes", "1", "com.technisys.omnichannel.backoffice.business.invitationcodes.activities.ListDocumentTypesActivity", "backoffice.invitationCodes.list", null, "backoffice.invitationcodes", "0"});

        deleteBackofficeActivity("backoffice.invitationCodes.readUser");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"backoffice.invitationCodes.readUser", "1", "com.technisys.omnichannel.backoffice.business.invitationcodes.activities.ReadUserActivity", "backoffice.invitationCodes.manage", null, "backoffice.invitationcodes", "2"});

        deleteBackofficeActivity("backoffice.invitationCodes.readEnvironment");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"backoffice.invitationCodes.readEnvironment", "1", "com.technisys.omnichannel.backoffice.business.invitationcodes.activities.ReadEnvironmentActivity", "backoffice.invitationCodes.manage", null, "backoffice.invitationcodes", "2"});

    }
}
