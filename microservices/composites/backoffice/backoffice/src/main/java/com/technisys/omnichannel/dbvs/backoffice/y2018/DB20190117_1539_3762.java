/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3762
 *
 * @author isilveira
 */

public class DB20190117_1539_3762 extends DBVSUpdate {

    @Override
    public void up() {
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-503-demo-environments.sql", ";", "UTF-8");
    }

}