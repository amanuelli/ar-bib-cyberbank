/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.util.HashMap;
import java.util.Map;

public class DB20180608_2220_3961 extends DBVSUpdate {

    @Override
    public void up() {
        //Pago otra tarjeta de techbank
        deleteActivity("pay.thirdPartiesCreditCard.preview");
        insertActivity("pay.thirdPartiesCreditCard.preview", "com.technisys.omnichannel.client.activities.pay.thirdpartiescreditcard.PayThirdPartiesCreditCardPreviewActivity", "thirdpartiescreditcard", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        deleteActivity("pay.thirdPartiesCreditCard.send");        
        insertActivity("pay.thirdPartiesCreditCard.send", "com.technisys.omnichannel.client.activities.pay.thirdpartiescreditcard.PayThirdPartiesCreditCardSendActivity", "thirdpartiescreditcard", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, null);
        
        String[] fieldNames = new String[]{"id_activity", "id_field", "id_permission"};
        String[] fieldValues = new String[]{"pay.thirdPartiesCreditCard.send", "debitAccount", "pay.creditCard.thirdParties"};
        insert("activity_products", fieldNames, fieldValues);
        
        fieldNames = new String[]{"id_activity", "id_field_amount", "id_field_product"};
        fieldValues = new String[]{"pay.thirdPartiesCreditCard.send", "amount", "debitAccount"};
        insert("activity_caps", fieldNames, fieldValues);
        
        //Borramos el formulario payThirdPartiesCreditCard por si existe, y creamos el nuevo que es Pago de tarjetas de terceros
        delete("forms", "id_form = 'payThirdPartiesCreditCard'");
        
        String[] formField = new String[]{"id_form", "category", "enabled", "type", "id_activity", "version", "schedulable"};
        String[] formFieldValues = new String[]{"payThirdPartiesCreditCard", "payments", "1", "activity", "pay.thirdPartiesCreditCard.send", "1", "1"};
        insert("forms", formField, formFieldValues);
        
        formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        formFieldValues = new String[]{"forms.thirdPartiesCreditCard.formName", "payThirdPartiesCreditCard", "1", "es", "Pago de tarjeta de terceros", "2018-01-01 12:00:00"};
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2]  + "', '" + formFieldValues[3] + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert ("form_messages", formField, formFieldValues);
        }
        
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "sub_type", "ordinal", "visible", "required"};
        
        String[] formFieldsValues = new String[]{"creditCardNumber", "payThirdPartiesCreditCard", "1", "text", "default", "1", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"amount", "payThirdPartiesCreditCard", "1", "amount", "default", "2", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesCreditCard", "1", "productselector", "default", "3", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
                
        formFieldsValues = new String[]{"notificationEmails", "payThirdPartiesCreditCard", "1", "emaillist", "default", "7", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"notificationBody", "payThirdPartiesCreditCard", "1", "textarea", "default", "8", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "type", "sub_type", "ordinal", "visible", "required", "ticket_only"};
        
        formFieldsValues = new String[]{"debitAmount", "payThirdPartiesCreditCard", "1", "amount", "default", "4", "TRUE", "FALSE", "1"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"rate", "payThirdPartiesCreditCard", "1", "text", "default", "5", "TRUE", "FALSE", "1"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"commission", "payThirdPartiesCreditCard", "1", "amount", "default", "6", "TRUE", "FALSE", "1"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
        formFieldsValues = new String[]{"amount", "payThirdPartiesCreditCard", "1", "field-small", "1"};
        insert("form_field_amount", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type"};
        formFieldsValues = new String[]{"notificationEmails", "payThirdPartiesCreditCard", "1", "field-normal"};
        insert("form_field_emaillist", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option"};
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesCreditCard", "1", "field-normal", "0"};
        insert("form_field_ps", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesCreditCard", "1", "pay.creditCard.thirdParties"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesCreditCard", "1", "CA"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "payThirdPartiesCreditCard", "1", "CC"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        formFieldsValues = new String[]{"creditCardNumber", "payThirdPartiesCreditCard", "1", "16", "16", "field-normal", "onlyNumbers"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"notificationBody", "payThirdPartiesCreditCard", "1", "0", "500", "field-medium"};
        insert("form_field_textarea", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
        formFieldsValues = new String[]{"debitAmount", "payThirdPartiesCreditCard", "1", "field-small", "1"};
        insert("form_field_amount", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_currency"};
        formFieldsValues = new String[]{"amount", "payThirdPartiesCreditCard", "1", "222"};
        insert("form_field_amount_currencies", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
        formFieldsValues = new String[]{"commission", "payThirdPartiesCreditCard", "1", "field-small", "1"};
        insert("form_field_amount", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"rate", "payThirdPartiesCreditCard", "1", "0", "50", "field-normal"};
        insert("form_field_text", formFields, formFieldsValues);
        
        
        //Solo doy de alta español
        Map<String, String> messages = new HashMap();
        messages.put("amount.label","Monto a pagar");
        messages.put("creditCardNumber.label","Tarjeta a pagar");
        messages.put("debitAccount.label","Cuenta de origen");
        messages.put("notificationBody.label","Mensaje");
        messages.put("notificationEmails.label","Correo electrónico para notificaciones");
        messages.put("commission.label","Comisiones");
        messages.put("rate.label","Cotización aplicada");
        messages.put("debitAmount.label","Monto total debitado");
        
        formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.payThirdPartiesCreditCard." + key, "es", key.substring(0, key.indexOf(".")), "payThirdPartiesCreditCard", "1", messages.get(key), "2015-01-01 12:00:00"};
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2]  + "', '" + formFieldsValues[3]  + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }

    }
}
