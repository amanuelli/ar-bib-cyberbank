/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.sql.Types;

/**
 *
 * @author diego
 */
public class DB20150626_1144_432 extends DBVSUpdate {

    @Override
    public void up() {
        dropTable("client_desktop_layouts");
        dropTable("client_desktop_widgets");
        
        ColumnDefinition[] columns = new ColumnDefinition[]{
            new ColumnDefinition("id", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("uri", Types.VARCHAR, 254, 0, false, null)
        };
        createTable("widgets", columns, new String[]{"id"});

        columns = new ColumnDefinition[]{
            new ColumnDefinition("id_user", Types.VARCHAR, 254, 0, false, null),
            new ColumnDefinition("id_environment", Types.INTEGER, 0, 0, false, null),
            new ColumnDefinition("id_widget", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("column_number", Types.INTEGER, 0, 0, false, null),
            new ColumnDefinition("row_number", Types.INTEGER, 0, 0, false, null)
        };
        createTable("desktop_layouts", columns, new String[]{"id_user", "id_environment", "id_widget"});
        createForeignKey("desktop_layouts", "FK_user_environment_widgets", new String[]{"id_user", "id_environment"}, "environment_users", new String[]{"id_user", "id_environment"}, true, true);
        createForeignKey("desktop_layouts", "FK_layouts_widgets", new String[]{"id_widget"}, "widgets", new String[]{"id"}, true, true);
        
         // Widgets por defecto de la columna 1
        String[] columnsNames = new String[]{"id", "uri"};
        insert("widgets", columnsNames, new String[]{"pendingTransactions", "widgets/pending-transactions"});
        insert("widgets", columnsNames, new String[]{"position",            "widgets/products"});
        insert("widgets", columnsNames, new String[]{"accounts",            "widgets/products"});
        insert("widgets", columnsNames, new String[]{"exchangeRates",       "widgets/exchange-rates"});
        
        insertActivity(
            "desktop.saveLayout", "com.technisys.omnichannel.client.activities.desktop.SaveLayoutActivity",
            "desktop",
            ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
    }
}
