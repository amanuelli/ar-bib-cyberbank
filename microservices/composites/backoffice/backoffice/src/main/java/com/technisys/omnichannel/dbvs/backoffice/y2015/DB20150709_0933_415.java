/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150709_0933_415 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.groups.create.pre", "com.technisys.omnichannel.client.activities.administration.groups.CreateGroupsPreActivity", "administration.groups", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.groups.create.preview", "com.technisys.omnichannel.client.activities.administration.groups.CreateGroupsPreviewActivity", "administration.groups", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.groups.create.send", "com.technisys.omnichannel.client.activities.administration.groups.CreateGroupsActivity", "administration.groups", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        insertActivity("administration.groups.modify.pre", "com.technisys.omnichannel.client.activities.administration.groups.ModifyGroupsPreActivity", "administration.groups", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.groups.modify.preview", "com.technisys.omnichannel.client.activities.administration.groups.ModifyGroupsPreviewActivity", "administration.groups", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.groups.modify.send", "com.technisys.omnichannel.client.activities.administration.groups.ModifyGroupsActivity", "administration.groups", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

    }
}
