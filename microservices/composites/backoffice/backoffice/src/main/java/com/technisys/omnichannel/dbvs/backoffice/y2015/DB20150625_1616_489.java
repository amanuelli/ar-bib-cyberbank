/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author ?
 */
public class DB20150625_1616_489 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("breadcrumb.accounts.url", "/accounts/index", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        
        insertOrUpdateConfiguration("breadcrumb.deposits.url", "/deposits/index", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        
        insertOrUpdateConfiguration("breadcrumb.creditcards.url", "/creditcards/index", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        
        insertOrUpdateConfiguration("breadcrumb.loans.url", "/loans/index", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        
        insertOrUpdateConfiguration("breadcrumb.transfers.url", "/accounts/index", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        
        insertOrUpdateConfiguration("breadcrumb.payments.url", "/accounts/index", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        
    }
}
