/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201106291640_10340 extends DBVSUpdate {

    @Override
    public void up() {

        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};

        String[] fieldValues = new String[]{"rub.desktop.listQuotes", "1", "1", "com.technisys.rubicon.business.desktop.activities.ListQuotesActivity"};
        insert("activities", fieldNames, fieldValues);

        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT client_desktop_widgets ON");
        fieldNames = new String[]{"id", "name", "uri", "default_column", "default_row"};
        fieldValues = new String[]{"9", "quotes", "/widgets/quotes.jsp", "2", "1"};
        insert("client_desktop_widgets", fieldNames, fieldValues);
        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT client_desktop_widgets OFF");

        fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};
        fieldValues = new String[]{"rubicon.widget.quotation.list", "USD|EUR", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        update("configuration", new String[]{"value"}, new String[]{"transactions|news|notifications|frequentTasks|banners|productDetails|productSummarized|scheduler|quotes"}, "id_field = 'rubicon.desktop.defaultWidgets.corporate'");


    }
}