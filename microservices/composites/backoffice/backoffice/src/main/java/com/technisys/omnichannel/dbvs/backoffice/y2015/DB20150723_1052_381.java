/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author fpena
 */
public class DB20150723_1052_381 extends DBVSUpdate {

    @Override
    public void up() {
        
        //como tareas frecuentes por defecto dejamos el formulario transferInternal, el unico que existe en este momento
        updateConfiguration("frequentActions.defaults.corporate", "transferInternal");
        updateConfiguration("frequentActions.defaults.retail", "transferInternal");
        //dejamos solo español por el momento
        updateConfiguration("core.languages", "es");
        
        //Borrmos el formulario transferInternal por si existe, y creamos el nuevo que es Transferencia interna
        delete("forms", "id_form = 'transferInternal'");
        
        String[] formField = new String[]{"id_form", "category", "enabled", "type", "id_activity", "version", "schedulable"};
        String[] formFieldValues = new String[]{"transferInternal", "accounts", "1", "activity", "transfers.internal.send", "1", "1"};
        insert("forms", formField, formFieldValues);
        
        formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        formFieldValues = new String[]{"forms.transferInternal.formName", "transferInternal", "1", "es", "Transferencia interna", "2015-01-01 12:00:00"};
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2]  + "', '" + formFieldValues[3] + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert ("form_messages", formField, formFieldValues);
        }
        
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required"};
        String[] formFieldsValues = new String[]{"debitSection", "transferInternal", "1", "sectiontitle", "1", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"debitAccount", "transferInternal", "1", "productselector", "2", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"amount", "transferInternal", "1", "amount", "3", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"debitReference", "transferInternal", "1", "text", "4", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"creditSection", "transferInternal", "1", "sectiontitle", "5", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"creditAccount", "transferInternal", "1", "productselector", "6", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"otherCreditAccount", "transferInternal", "1", "text", "7", "value(creditAccount) == 'other'", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"creditReference", "transferInternal", "1", "text", "8", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"notificationMails", "transferInternal", "1", "emaillist", "9", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"notificationBody", "transferInternal", "1", "textarea", "10", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
        formFieldsValues = new String[]{"amount", "transferInternal", "1", "field-small", "1"};
        insert("form_field_amount", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type"};
        formFieldsValues = new String[]{"notificationMails", "transferInternal", "1", "field-normal"};
        insert("form_field_emaillist", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option", "show_other_permission", "show_other_id_ps"};
        formFieldsValues = new String[]{"creditAccount", "transferInternal", "1", "field-normal", "1", "transfer.internal", "debitAccount"};
        insert("form_field_ps", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option"};
        formFieldsValues = new String[]{"debitAccount", "transferInternal", "1", "field-normal", "0"};
        insert("form_field_ps", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        formFieldsValues = new String[]{"creditAccount", "transferInternal", "1", "product.read"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        formFieldsValues = new String[]{"debitAccount", "transferInternal", "1", "transfer.internal"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"creditAccount", "transferInternal", "1", "CA"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"creditAccount", "transferInternal", "1", "CC"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "transferInternal", "1", "CA"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "transferInternal", "1", "CC"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"creditReference", "transferInternal", "1", "0", "20", "field-normal"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"debitReference", "transferInternal", "1", "0", "20", "field-normal"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"otherCreditAccount", "transferInternal", "1", "0", "14", "field-small"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"notificationBody", "transferInternal", "1", "0", "500", "field-medium"};
        insert("form_field_textarea", formFields, formFieldsValues);
        
        
        //Solo doy de alta español, y las key que tengan valor
        Map<String, String> messages = new HashMap();
        messages.put("amount.label","Monto");
        messages.put("creditAccount.label","Cuenta crédito");
        messages.put("creditAccount.showOtherText","Cuenta de terceros");
        messages.put("creditReference.label","Referencia destino");
        messages.put("creditSection.label","DATOS DE CRÉDITO");
        messages.put("debitAccount.label","Cuenta débito");
        messages.put("debitReference.label","Referencia origen");
        messages.put("debitSection.label","DATOS DE DÉBITO");
        messages.put("notificationBody.label","Cuerpo de la notificación");
        messages.put("notificationBody.placeholder","Ingresa un mensaje a propósito de esta transacción");
        messages.put("notificationMails.help","E-Mails a notificar el envío de la transferencia. Recuerda ingresarlos separados por coma.");
        messages.put("notificationMails.label","Emails de notificación");
        messages.put("otherCreditAccount.label","Número de cuenta crédito");
        
        formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.transferInternal." + key, "es", key.substring(0, key.indexOf(".")), "transferInternal", "1", messages.get(key), "2015-01-01 12:00:00"};
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2]  + "', '" + formFieldsValues[3]  + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }
}
