/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author aelters
 */
public class DB20150706_1406_508 extends DBVSUpdate {

    @Override
    public void up() {
        update("activity_products", new String[]{"id_permission"}, new String[]{"core.authenticated"}, "id_activity = 'preferences.changepassword.send' and id_permission = 'preferences'");
        delete("permissions_credentials_groups", "id_permission = 'preferences' and id_credential_group = 'otp'");
        update("permissions", new String[]{"id_permission"}, new String[]{"user.preferences"}, "id_permission = 'preferences'");
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"user.preferences", "accessToken-otp"});
        update("activity_products", new String[]{"id_permission"}, new String[]{"user.preferences"}, "id_activity = 'preferences.changepassword.send'");
        update("activity_products", new String[]{"id_permission"}, new String[]{"user.preferences"}, "id_activity = 'preferences.notifications.configuration.modify'");
        update("activity_products", new String[]{"id_permission"}, new String[]{"user.preferences"}, "id_activity = 'preferences.environmentandlang.modify.send'");
    }
}