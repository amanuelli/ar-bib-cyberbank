/* 
 * Copyright 2017 Manentia Software. 
 * 
 * This software component is the intellectual property of Manentia Software S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * http://www.manentiasoftware.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-3047
 *
 * @author ldurand
 */
public class DB20170821_1135_3047 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "requestOfManagementCheck";
        String version = "1";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFieldPSProductiTypes = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        String[] formFieldPSPermission = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        Map<String, String> messages = new HashMap();

        update("form_fields", new String[]{"visible", "required"}, new String[]{"value(placeOfRetreat) == 'homeDelivery'", "value(placeOfRetreat) == 'homeDelivery'"}, "id_form = '" + idForm + "' AND id_field = 'address'");
        update("form_field_selector", new String[]{"default_value"}, new String[]{"null"}, "id_form = '" + idForm + "' AND id_field = 'placeOfRetreat'");

        delete("form_field_messages", "id_message = '" + "fields." + idForm + ".placeOfRetreat.option.EntregaADomicilio'");
        delete("form_fields", "id_form = '" + idForm + "' AND id_field = 'disclaimer'");
        delete("form_field_terms_conditions","id_form = '" + idForm + "' AND id_field = 'disclaimer'");
        delete("form_field_messages", "id_form = '" + idForm + "' AND id_field = 'disclaimer'");

        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"placeOfRetreat", idForm, version, "homeDelivery"});

        insert("form_field_ps_product_types", formFieldPSProductiTypes, new String[]{"costDebitAccount", idForm, version, "CA"});
        insert("form_field_ps_product_types", formFieldPSProductiTypes, new String[]{"costDebitAccount", idForm, version, "CC"});
        insert("form_field_ps_product_types", formFieldPSProductiTypes, new String[]{"debitAccount", idForm, version, "CA"});
        insert("form_field_ps_product_types", formFieldPSProductiTypes, new String[]{"debitAccount", idForm, version, "CC"});

        insert("form_field_ps_permissions", formFieldPSPermission, new String[]{"costDebitAccount", idForm, version, "transfer.internal"});
        insert("form_field_ps_permissions", formFieldPSPermission, new String[]{"costDebitAccount", idForm, version, "transfer.thirdParties"});
        
        insert("form_fields", new String[]{"id_field","id_form","form_version","type","ordinal","visible","required","note","visible_in_mobile","read_only","sub_type","ticket_only"}, new String[]{"termsAndConditions",idForm,version,"termsandconditions","11","TRUE","TRUE","null","1","0","default","0"});
        insert("form_field_terms_conditions", new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"}, new String[]{"termsAndConditions", idForm, version, "field-normal", "1", "0"});
        
        messages.put("placeOfRetreat.option.homeDelivery", "Entrega a domicilio");
        messages.put("termsAndConditions.showAcceptOptionText", "Aceptar términos");
        messages.put("termsAndConditions.termsAndConditions", "Las instrucciones enviadas al banco por usted luego de las <hora de corte> serán procesadas al siguiente día hábil bancario. Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.");
        messages.put("termsAndConditions.label",null);
        
         for (String key : messages.keySet()) {
            String[] formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

    }
}
