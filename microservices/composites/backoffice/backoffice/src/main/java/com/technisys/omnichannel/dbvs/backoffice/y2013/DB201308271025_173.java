/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201308271025_173 extends DBVSUpdate {

    @Override
    public void up() {
        delete("form_fields", "id_form=5 and id_field='moneda'");

        insert("form_fields", new String[]{"id_field", "id_form", "description", "field_type", "mandatory", "possible_values", "validation_regexp", "depends_on_id_field", "depends_on_value", "required_permission", "control_cap", "transaction_product"},
                new String[]{"importe", "5", null, "amount", "1", "000|222", "(([1-9][0-9]*)|([1-9]\\d{0,2}(\\.\\d{3})*)|0)(,\\d*)?", null, null, null, "0", "0"});

        update("forms", new String[]{"template_es"}, new String[]{"<form:form>	<form:section>		<form:field idField='importe'/>		<form:field idField='plazo'/>		<form:field idField='destino'/>		<form:field idField='destinoOtros'/>	</form:section><div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='termsAndConditions'/>	</form:section></form:form>"}, "id_form=5");
    }
}