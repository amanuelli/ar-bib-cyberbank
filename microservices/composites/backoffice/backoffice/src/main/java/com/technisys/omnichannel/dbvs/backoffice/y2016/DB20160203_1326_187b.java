/* 
 * Copyright 2016 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2016;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author ?
 */
public class DB20160203_1326_187b extends DBVSUpdate {

    @Override
    public void up() {        
        update("form_field_ps", new String[]{"filter_actives"}, new String[]{"1"}, "id_form = 'transferInternal' AND id_field = 'debitAccount'");

    }
}