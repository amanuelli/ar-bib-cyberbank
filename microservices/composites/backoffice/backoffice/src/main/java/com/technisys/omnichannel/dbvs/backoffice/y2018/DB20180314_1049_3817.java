/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-3817
 *
 * @author isilveira
 */
public class DB20180314_1049_3817 extends DBVSUpdate {

    @Override
    public void up() {
        update("activities", new String[]{"component_fqn"}, new String[]{"com.technisys.omnichannel.client.activities.preferences.securitySeals.ListSecuritySealsActivity"}, "id_activity='preferences.securityseals.list'");
        
        insertActivity("preferences.securityseals.modify", "com.technisys.omnichannel.client.activities.preferences.securitySeals.ModifySecuritySealActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "user.preferences");
    }
}