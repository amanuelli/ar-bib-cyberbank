/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-3669
 *
 * @author idonadini
 */
public class DB20180116_1510_3669 extends DBVSUpdate {

    @Override
    public void up() {

    Map<String, String> formFieldMessages = new HashMap();  
    String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
    String version = "1";

    formFieldMessages.put("fields.requestEndorsement.boardingPass.option.maritime", "Marítimo");
    formFieldMessages.put("fields.requestEndorsement.boardingPass.option.aerial"  , "Aéreo");
    formFieldMessages.put("fields.requestEndorsement.letterCredit.label"    , "Carta de crédito");
    

    formFieldMessages.keySet().forEach((key) -> {

       delete("form_field_messages", "id_message = '" + key + "' and lang = 'es'");

       if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
           customSentence(DBVS.DIALECT_ORACLE, 
                "INSERT INTO form_field_messages (id_message, id_form, id_field, form_version, lang, value, modification_date) "
               + "VALUES ('" + key + "', '" + key.split("\\.")[1] + "', '" + key.split("\\.")[2] + "', '" + version + "', 'es', '" + formFieldMessages.get(key) + "', TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'))");
       } else {
           customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
               "INSERT INTO form_field_messages (id_message, id_form, id_field, form_version, lang, value, modification_date) "
               + "VALUES ('" + key + "', '" + key.split("\\.")[1] + "', '" + key.split("\\.")[2] + "', '" + version + "', 'es', '" + formFieldMessages.get(key) + "', '" + date + "')");                
       }

    });   

        
    }

}