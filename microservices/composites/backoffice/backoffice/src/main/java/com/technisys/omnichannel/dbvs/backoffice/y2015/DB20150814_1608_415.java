/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150814_1608_415 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("invitation.notification.transport", "MAIL", ConfigurationGroup.OTHER, "other", new String[]{"notEmpty"}, "SMS|MAIL");

        insertOrUpdateConfiguration("administration.users.invite.enabled", "true", ConfigurationGroup.OTHER, "other", new String[]{"notEmpty"}, "true|false");
        insertOrUpdateConfiguration("administration.users.invite.notInBackend.enabled", "true", ConfigurationGroup.OTHER, "other", new String[]{"notEmpty"}, "true|false");

        insertOrUpdateConfiguration("core.attempts.invite.lockoutWindow", "24h", ConfigurationGroup.SEGURIDAD, "others", new String[]{"notEmpty", "interval"});
        insertOrUpdateConfiguration("core.attempts.invite.maxAttempts", "10", ConfigurationGroup.SEGURIDAD, "others", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("core.attempts.invite.maxAttemptsTimeout", "24h", ConfigurationGroup.SEGURIDAD, "others", new String[]{"notEmpty", "interval"});
    }
}
