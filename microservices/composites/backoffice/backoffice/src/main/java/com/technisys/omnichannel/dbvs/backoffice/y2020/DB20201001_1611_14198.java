/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author KevinGaray
 */

public class DB20201001_1611_14198 extends DBVSUpdate {

    @Override
    public void up() {
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        String idForm = "accountOpening";
        String idMessage = "fields.accountOpening.termsConditions.termsAndConditions";
        String idField = "termsConditions";
        String version = "1";
        String valueEs = "Lorem Ipsum es simplemente texto de relleno utilizado en la industria de la impresión y la tipografía. Lorem Ipsum ha sido el texto de relleno estándar de la industria desde inicios del 1500, cuando un impresor desconocido tomó una galera de imprenta y la revolvió, para crear una especie de libro. No solo ha sobrevivido cinco siglos, sino también el salto a la tipografía electrónica, permaneciendo esencialmente sin cambios. Se popularizó en la década de 1960 con el lanzamiento de las hojas de Letraset, las cuales contienen pasajes de Lorem Ipsum, y más recientemente con software de autoedición como Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.";
        String valueEn = "Lorem Ipsum is simply dummy text used in the printing and typesetting  industry. It has been the standard dummy text of the industry from the beginnings of the 1500, when an unknown printer took a galley of type and scrambled it to make a type of specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It became popular in the 1960s, with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker, which includes versions of Lorem Ipsum.";
        String valuePt = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.";

        delete("form_field_messages", "id_message = '"+ idMessage +"'");

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('" + idMessage + "', 'es', '" + idField + "', '" + idForm + "', " + version + ", TO_DATE('2019-04-05 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), '" + valueEs + "')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('" + idMessage + "', 'en', '" + idField + "', '" + idForm + "', " + version + ", TO_DATE('2019-04-05 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), '" + valueEn + "')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('" + idMessage + "', 'pt', '" + idField + "', '" + idForm + "', " + version + ", TO_DATE('2019-04-05 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), '" + valuePt + "')");
        } else {
            String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};

            insert("form_field_messages", formFieldsMessages, new String[]{idMessage, "es", idField, idForm, version, date, valueEs});
            insert("form_field_messages", formFieldsMessages, new String[]{idMessage, "en", idField, idForm, version, date, valueEn});
            insert("form_field_messages", formFieldsMessages, new String[]{idMessage, "pt", idField, idForm, version, date, valuePt});
        }

    }

}