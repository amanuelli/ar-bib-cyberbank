/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201308271123_177 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"[0-9]{15}"}, "id_form=16 and id_field='numeroCuentaAntel'");
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"[0-9]{15}"}, "id_form=16 and id_field='numeroCuentaOse'");
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"[0-9]{8}"}, "id_form=16 and id_field='numeroDeCuentaCorrienteSaneamIMMont'");
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"[0-9]{8}"}, "id_form=16 and id_field='numeroDeCuentaCorrienteImpuestosIMMont'");
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"[0-9]{8}"}, "id_form=16 and id_field='numeroDeCuentaCorrientePatenteIMMont'");
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"[0-9]{15}"}, "id_form=16 and id_field='numeroDeClienteMontGas'");
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"[0-9]{7}"}, "id_form=16 and id_field='numeroClienteMovistar'");
    }
}