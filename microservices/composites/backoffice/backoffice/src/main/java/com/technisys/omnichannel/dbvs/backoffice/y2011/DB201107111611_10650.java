/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107111611_10650 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"id_field"}, new String[]{"rubicon.currency.payCreditCard.list"}, "id_field = 'rubicon.currency.list'");
    }
}