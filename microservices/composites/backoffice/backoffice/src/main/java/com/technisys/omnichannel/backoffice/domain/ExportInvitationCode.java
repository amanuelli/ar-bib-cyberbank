/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.domain;

import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.DateUtils;

/**
 *
 * @author Sebastian Barbosa
 */
public class ExportInvitationCode extends InvitationCode {

    private final String exportLang;

    public ExportInvitationCode(InvitationCode invitationCode, String lang) {
        super(invitationCode);

        this.exportLang = lang;
    }

    public String getDocument() {
        I18n i18n = I18nFactory.getHandler();
        return getDocumentNumber() + " (" + i18n.getMessage("documentType.label." + getDocumentType(), exportLang)
                + ", " + i18n.getMessage("country.name." + getDocumentCountry(), exportLang) + ")";
    }

    public String getCreationDateAsString() {
        return DateUtils.formatMediumDate(getCreationDate());
    }

}
