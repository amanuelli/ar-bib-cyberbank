/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150611_1534_419 extends DBVSUpdate {

    @Override
    public void up() {
        // Activity
        insertActivity("preferences.securityseals.list", "com.technisys.omnichannel.client.activities.preferences.password.ListSecuritySealsActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.changepassword.preview", "com.technisys.omnichannel.client.activities.preferences.password.ChangePasswordPreviewActivity", "preferences", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.changepassword.send", "com.technisys.omnichannel.client.activities.preferences.password.ChangePasswordActivity", "preferences", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "preferences");

    }
}
