/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3561
 *
 * @author ncastro
 */
public class DB20171226_1511_3561 extends DBVSUpdate {

    @Override
    public void up() {
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            insertOrUpdateConfiguration("client.emailValidationFormat", "^[a-zA-Z0-9+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty"});
            insertOrUpdateConfiguration("email.validationFormat", "^[a-zA-Z0-9+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", ConfigurationGroup.TECNICAS, null, new String[]{});
        } else {
            insertOrUpdateConfiguration("client.emailValidationFormat", "^[a-zA-Z0-9+&*-]+(?:\\\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\\\.)+[a-zA-Z]{2,7}$",ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty"});
            insertOrUpdateConfiguration("email.validationFormat", "^[a-zA-Z0-9+&*-]+(?:\\\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\\\.)+[a-zA-Z]{2,7}$", ConfigurationGroup.TECNICAS, null, new String[]{});
        }
    }
}