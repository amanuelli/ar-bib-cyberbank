/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 * @author druiz
 */
public class DB201203081500_12074 extends DBVSUpdate {

    @Override
    public void up() {

        String[] fieldNames;
        String[] fieldValues;

        //tabla de cuentas con objetivos
        ColumnDefinition[] columns = new ColumnDefinition[]{
            new ColumnDefinition("id_user", Types.VARCHAR, 254, 0, false, null),
            new ColumnDefinition("id_goal", Types.INTEGER, 10, 0, false, null, true),
            new ColumnDefinition("kind", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("name", Types.VARCHAR, 254, 0, false, null),
            new ColumnDefinition("currency", Types.CHAR, 3, 0, false, null),
            new ColumnDefinition("amount", Types.DECIMAL, 13, 2, false, null),
            new ColumnDefinition("debit_account", Types.CHAR, 20, 0, false, null)
        };

        String[] primaryKey = new String[]{"id_goal"};
        createTable("client_goals", columns, primaryKey);

        //Activities
        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};

        fieldValues = new String[]{"rub.goals.ListGoals", "1", "1", "com.technisys.rubicon.business.goals.activities.ListGoalsActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.goals.CreateGoalPre", "1", "1", "com.technisys.rubicon.business.goals.activities.CreateGoalPreActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.goals.CreateGoal", "1", "1", "com.technisys.rubicon.business.goals.activities.CreateGoalActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.goals.ModifyGoal", "1", "1", "com.technisys.rubicon.business.goals.activities.ModifyGoalActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.goals.ModifyGoalPre", "1", "1", "com.technisys.rubicon.business.goals.activities.ModifyGoalPreActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.goals.DeleteGoal", "1", "1", "com.technisys.rubicon.business.goals.activities.DeleteGoalActivity"};
        insert("activities", fieldNames, fieldValues);

        //Configuration
        fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};

        fieldValues = new String[]{"rubicon.goals.goalTypes", "auto|casa|vacaciones|otro", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.goals.approvalPercentage.auto", "50", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.goals.approvalPercentage.casa", "75", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.goals.approvalPercentage.vacaciones", "35", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.goals.approvalPercentage.otro", "70", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.goals.goalImage.auto", "goal_1.jpg", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.goals.goalImage.casa", "goal_2.jpg", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.goals.goalImage.vacaciones", "goal_3.jpg", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.goals.goalImage.otro", "goal_4.jpg", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        //Widgets
        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT client_desktop_widgets ON");
        fieldNames = new String[]{"id", "name", "uri", "default_column", "default_row"};
        fieldValues = new String[]{"13", "goals", "/widgets/goals.jsp", "1", "2"};
        insert("client_desktop_widgets", fieldNames, fieldValues);
        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT client_desktop_widgets OFF");
    }
}