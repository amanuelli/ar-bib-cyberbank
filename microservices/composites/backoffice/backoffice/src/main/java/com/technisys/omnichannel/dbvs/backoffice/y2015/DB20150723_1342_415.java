/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150723_1342_415 extends DBVSUpdate {

    @Override
    public void up() {

        insertActivity("administration.users.invite.pre", "com.technisys.omnichannel.client.activities.administration.users.InviteUsersPreActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.users.invite.preview", "com.technisys.omnichannel.client.activities.administration.users.InviteUsersPreviewActivity",  "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.users.invite.send", "com.technisys.omnichannel.client.activities.administration.users.InviteUsersActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        update("activities",
                new String[]{"component_fqn", "auditable"},
                new String[]{"com.technisys.omnichannel.client.activities.administration.ListProductsActivity", "0"},
                "id_activity='administration.products.list'");
    }
}
