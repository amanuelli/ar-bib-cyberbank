/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/*
 * @author psuarez
 */
public class DB20150528_1121_413 extends DBVSUpdate {

    @Override
    public void up() {
        
        deleteActivity("creditCards.downloadStatementLine");
        insertActivity("creditCards.downloadStatementLine", "com.technisys.omnichannel.client.activities.creditcards.DownloadStatementLineActivity", "creditCards", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idCreditCard");

        deleteActivity("creditCards.list");
        insertActivity("creditCards.list", "com.technisys.omnichannel.client.activities.creditcards.ListCreditCardsActivity","creditCards", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

        deleteActivity("creditCards.read");
        insertActivity("creditCards.read", "com.technisys.omnichannel.client.activities.creditcards.ReadCreditCardActivity","creditCards", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idCreditCard");

        deleteActivity("creditCards.listStatementLines");
        insertActivity("creditCards.listStatementLines", "com.technisys.omnichannel.client.activities.creditcards.ListStatementLinesActivity", "creditCards", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idCreditCard");

        deleteActivity("creditCards.listStatements");
        insertActivity("creditCards.listStatements", "com.technisys.omnichannel.client.activities.creditcards.ListStatementsActivity", "creditCards", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idCreditCard");

        insertOrUpdateConfiguration("creditCard.statementsPerPage", "10", com.technisys.omnichannel.DBVSUpdate.ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});

    }
}