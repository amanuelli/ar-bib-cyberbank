/* 
 * Copyright 2016 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2016;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20160916_1325_1845 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("mobile.version", "2.0.18", ConfigurationGroup.TECNICAS, "frontend", new String[]{});
    }
}
