/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author danireb
 */
public class DB20170406_1200_2229 extends DBVSUpdate {

    @Override
    public void up() {

        
        
        customSentence(new String[]{DBVS.DIALECT_MSSQL}, "UPDATE E SET product_group_id = CE.product_group_id FROM environments E JOIN client_environments CE ON E.id_environment = CE.id_environment");
        customSentence(new String[]{DBVS.DIALECT_ORACLE}, "UPDATE environments SET environments.product_group_id = (SELECT client_environments.product_group_id FROM client_environments WHERE environments.id_environment = client_environments.id_environment)");
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB}, "UPDATE environments E, client_environments CE SET E.product_group_id = CE.product_group_id WHERE E.id_environment = CE.id_environment");
        
        customSentence(new String[]{DBVS.DIALECT_MSSQL}, "UPDATE U SET document_country = CU.document_country, document_type = CU.document_type, document = CU.document FROM users U JOIN client_users CU ON U.id_user = CU.id_user");
        customSentence(new String[]{DBVS.DIALECT_ORACLE}, "UPDATE users U" +
                                                                " SET (U.document_country, U.document_type, U.document) = (SELECT CU.document_country, CU.document_type, CU.document " +
                                                                "FROM   client_users CU " +
                                                                "WHERE  U.id_user = CU.id_user) " +
                                                                "WHERE EXISTS             (SELECT 1" +
                                                                "                          FROM   client_users CU" +
                                                                "                          WHERE  U.id_user = CU.id_user)");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB}, "UPDATE users U, client_users CU SET U.document_country = CU.document_country, U.document_type = CU.document_type, U.document = CU.document WHERE U.id_user = CU.id_user");
        
        dropTable("client_users");
        dropTable("client_environments");
        
        deleteBackofficeActivity("backoffice.invitationCodes.listDocumentTypes");       
        
    }
    
}
