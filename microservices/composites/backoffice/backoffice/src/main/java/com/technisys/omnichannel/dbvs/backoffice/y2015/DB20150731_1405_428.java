/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150731_1405_428 extends DBVSUpdate {

    @Override
    public void up() {
        update("activities", new String[]{"auditable"}, new String[]{"0"}, "id_activity='communications.latestCommunications'");
        update("activities", new String[]{"auditable"}, new String[]{"0"}, "id_activity='desktop.loadLayout'");
    }
}
