/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.ArrayList;
import java.util.List;

/**
 * Related issue: MANNAZCA-4034
 *
 * @author ncarril
 */
public class DB20180710_1747_4034 extends DBVSUpdate {

    private final String FORM_FIELD_MESSAGES = "form_field_messages";
    private final String FORM_FIELD_SELECTOR_OPTIONS = "form_field_selector_options";
    private final String FORM_FIELD_SELECTOR = "form_field_selector";
    private final String FORM_FIELDS = "form_fields";
    private final String[] FIELD_VALUE = new String[]{"value"};
    private final String[] FIELD_ORDINAL = new String[]{"ordinal"};

    @Override
    public void up() {

        String form_name = "requestCheckbook";

        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type"};
        String[] selector_field = new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as"};
        String[] options_selector_field = new String[]{"id_field", "id_form", "form_version", "value"};
        String[] field_messages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};

        List<String[]> messages = new ArrayList<String[]>();

        //CAMPO FANFOLD
        // Se agrega un combo para cargar FANFOLD -> SI o NO
        String fanfold = "fanfold";

        insert(FORM_FIELDS, formFields, new String[]{fanfold, form_name, "1", "selector", "1", "TRUE", "TRUE", "default"});
        insert(FORM_FIELD_SELECTOR, selector_field, new String[]{fanfold, form_name, "1", "form-normal", "0", "combo"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{fanfold, form_name, "1", "yes"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{fanfold, form_name, "1", "no"});

        String preLoadName = "fields." + form_name + "." + fanfold + ".";
        messages.add(new String[]{preLoadName + "label", "es", fanfold, form_name, "1", "2018-07-10 17:48:00", "Fanfold"});
        messages.add(new String[]{preLoadName + "label", "en", fanfold, form_name, "1", "2018-07-10 17:48:00", "Fanfold"});
        messages.add(new String[]{preLoadName + "label", "pt", fanfold, form_name, "1", "2018-07-10 17:48:00", "Fanfold"});

        messages.add(new String[]{preLoadName + "option.yes", "es", fanfold, form_name, "1", "2018-07-10 17:48:00", "Si"});
        messages.add(new String[]{preLoadName + "option.yes", "en", fanfold, form_name, "1", "2018-07-10 17:48:00", "Yes"});
        messages.add(new String[]{preLoadName + "option.yes", "pt", fanfold, form_name, "1", "2018-07-10 17:48:00", "Sim"});

        messages.add(new String[]{preLoadName + "option.no", "es", fanfold, form_name, "1", "2018-07-10 17:48:00", "No"});
        messages.add(new String[]{preLoadName + "option.no", "en", fanfold, form_name, "1", "2018-07-10 17:48:00", "No"});
        messages.add(new String[]{preLoadName + "option.no", "pt", fanfold, form_name, "1", "2018-07-10 17:48:00", "No"});

        messages.add(new String[]{preLoadName + "requiredError", "es", fanfold, form_name, "1", "2018-07-10 17:48:00", "Debe seleccionar una opción"});
        messages.add(new String[]{preLoadName + "requiredError", "en", fanfold, form_name, "1", "2018-07-10 17:48:00", "You must select an option"});
        messages.add(new String[]{preLoadName + "requiredError", "pt", fanfold, form_name, "1", "2018-07-10 17:48:00", "Você deve selecionar uma opção"});

        //CAMPO TipoCheque
        update(FORM_FIELDS, FIELD_ORDINAL, new String[]{"2"}, "id_form ='requestCheckbook' and id_field='tipoDeCheque'");
        //se elimina opcion fanfold
        delete(FORM_FIELD_SELECTOR_OPTIONS, "id_form ='requestCheckbook' and id_field='tipoDeCheque' and value ='fanfold'");
        delete(FORM_FIELD_MESSAGES, "id_message='fields.requestCheckbook.tipoDeCheque.option.fanfold'");
        //Se agrega los mensajes para requerido
        String preFixMessage_tipoCheque = "fields." + form_name + ".tipoDeCheque.requiredError";
        messages.add(new String[]{preFixMessage_tipoCheque, "es", "tipoDeCheque", form_name, "1", "2018-07-10 17:48:00", "Debe seleccionar un Tipo de cheque"});
        messages.add(new String[]{preFixMessage_tipoCheque, "en", "tipoDeCheque", form_name, "1", "2018-07-10 17:48:00", "You must select a Type of check"});
        messages.add(new String[]{preFixMessage_tipoCheque, "pt", "tipoDeCheque", form_name, "1", "2018-07-10 17:48:00", "Você deve selecionar um tipo de verificação"});

        //CAMPO Cantidad
        //Fanfold 
        //ELimino el campo texto anterior
        delete("form_field_text", "id_form ='requestCheckbook' and id_field='cantidadDeCheques'");
        delete(FORM_FIELDS, "id_form ='requestCheckbook' and id_field='cantidadDeCheques'");

        //Agrego el combo
        String cantFanfold = "cantidadDeCheques";
        insert(FORM_FIELDS, formFields, new String[]{cantFanfold, form_name, "1", "selector", "3", "value(fanfold) == 'yes'", "TRUE", "default"});
        insert(FORM_FIELD_SELECTOR, selector_field, new String[]{cantFanfold, form_name, "1", "form-normal", "0", "combo"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantFanfold, form_name, "1", "1"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantFanfold, form_name, "1", "2"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantFanfold, form_name, "1", "3"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantFanfold, form_name, "1", "4"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantFanfold, form_name, "1", "5"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantFanfold, form_name, "1", "6"});

        String preName_CantFanfold = "fields." + form_name + "." + cantFanfold + ".";
        messages.add(new String[]{preName_CantFanfold + "label", "es", fanfold, form_name, "1", "2018-07-10 17:48:00", "Cantidad de cheques"});
        messages.add(new String[]{preName_CantFanfold + "label", "en", fanfold, form_name, "1", "2018-07-10 17:48:00", "Quantity of checks"});
        messages.add(new String[]{preName_CantFanfold + "label", "pt", fanfold, form_name, "1", "2018-07-10 17:48:00", "Quantidade de cheques"});

        messages.add(new String[]{preName_CantFanfold + "requiredError", "es", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "Debe seleccionar la Cantidad de cheques a solicitar"});
        messages.add(new String[]{preName_CantFanfold + "requiredError", "en", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "You must select the number of checks to request"});
        messages.add(new String[]{preName_CantFanfold + "requiredError", "pt", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "Você deve selecionar o número de cheques para solicitar"});
        //opcion 500
        messages.add(new String[]{preName_CantFanfold + "option.1", "es", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "500"});
        messages.add(new String[]{preName_CantFanfold + "option.1", "en", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "500"});
        messages.add(new String[]{preName_CantFanfold + "option.1", "pt", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "500"});
        //opcion 600
        messages.add(new String[]{preName_CantFanfold + "option.2", "es", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "600"});
        messages.add(new String[]{preName_CantFanfold + "option.2", "en", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "600"});
        messages.add(new String[]{preName_CantFanfold + "option.2", "pt", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "600"});
        //opcion 700
        messages.add(new String[]{preName_CantFanfold + "option.3", "es", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "700"});
        messages.add(new String[]{preName_CantFanfold + "option.3", "en", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "700"});
        messages.add(new String[]{preName_CantFanfold + "option.3", "pt", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "700"});
        //opcion 800
        messages.add(new String[]{preName_CantFanfold + "option.4", "es", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "800"});
        messages.add(new String[]{preName_CantFanfold + "option.4", "en", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "800"});
        messages.add(new String[]{preName_CantFanfold + "option.4", "pt", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "800"});
        //opcion 900
        messages.add(new String[]{preName_CantFanfold + "option.5", "es", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "900"});
        messages.add(new String[]{preName_CantFanfold + "option.5", "en", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "900"});
        messages.add(new String[]{preName_CantFanfold + "option.5", "pt", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "900"});
        //opcion 1000
        messages.add(new String[]{preName_CantFanfold + "option.6", "es", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "1000"});
        messages.add(new String[]{preName_CantFanfold + "option.6", "en", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "1000"});
        messages.add(new String[]{preName_CantFanfold + "option.6", "pt", cantFanfold, form_name, "1", "2018-07-10 17:48:00", "1000"});

        //NoFanfold 
        //elimino el anterior
        delete("form_field_text", "id_form ='requestCheckbook' and id_field='cantidadDeChequeras'");
        delete(FORM_FIELDS, "id_form ='requestCheckbook' and id_field='cantidadDeChequeras'");
        //Creo el nuevo
        String cantComun = "cantidadDeChequeras";
        insert(FORM_FIELDS, formFields, new String[]{cantComun, form_name, "1", "selector", "4", "value(fanfold) == 'no'", "TRUE", "default"});
        insert(FORM_FIELD_SELECTOR, selector_field, new String[]{cantComun, form_name, "1", "form-normal", "0", "combo"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "01"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "02"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "03"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "04"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "05"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "06"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "07"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "08"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "09"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{cantComun, form_name, "1", "10"});

        String preName_CantComun = "fields." + form_name + "." + cantComun + ".";
        messages.add(new String[]{preName_CantComun + "label", "es", fanfold, form_name, "1", "2018-07-10 17:48:00", "Cantidad de chequeras"});
        messages.add(new String[]{preName_CantComun + "label", "en", fanfold, form_name, "1", "2018-07-10 17:48:00", "Quantity of checkbooks"});
        messages.add(new String[]{preName_CantComun + "label", "pt", fanfold, form_name, "1", "2018-07-10 17:48:00", "Quantidade de livros de cheques"});

        messages.add(new String[]{preName_CantComun + "requiredError", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "Debe seleccionar la Cantidad de chequeras a solicitar"});
        messages.add(new String[]{preName_CantComun + "requiredError", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "You must select the number of checkbooks to request"});
        messages.add(new String[]{preName_CantComun + "requiredError", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "Você deve selecionar o número de talões de cheques para solicitar"});
        //opcion 1
        messages.add(new String[]{preName_CantComun + "option.01", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "1"});
        messages.add(new String[]{preName_CantComun + "option.01", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "1"});
        messages.add(new String[]{preName_CantComun + "option.01", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "1"});
        //opcion 2
        messages.add(new String[]{preName_CantComun + "option.02", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "2"});
        messages.add(new String[]{preName_CantComun + "option.02", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "2"});
        messages.add(new String[]{preName_CantComun + "option.02", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "2"});
        //opcion 3
        messages.add(new String[]{preName_CantComun + "option.03", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "3"});
        messages.add(new String[]{preName_CantComun + "option.03", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "3"});
        messages.add(new String[]{preName_CantComun + "option.03", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "3"});
        //opcion 4
        messages.add(new String[]{preName_CantComun + "option.04", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "4"});
        messages.add(new String[]{preName_CantComun + "option.04", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "4"});
        messages.add(new String[]{preName_CantComun + "option.04", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "4"});
        //opcion 5
        messages.add(new String[]{preName_CantComun + "option.05", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "5"});
        messages.add(new String[]{preName_CantComun + "option.05", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "5"});
        messages.add(new String[]{preName_CantComun + "option.05", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "5"});
        //opcion 6
        messages.add(new String[]{preName_CantComun + "option.06", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "6"});
        messages.add(new String[]{preName_CantComun + "option.06", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "6"});
        messages.add(new String[]{preName_CantComun + "option.06", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "6"});
        //opcion 7
        messages.add(new String[]{preName_CantComun + "option.07", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "7"});
        messages.add(new String[]{preName_CantComun + "option.07", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "7"});
        messages.add(new String[]{preName_CantComun + "option.07", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "7"});
        //opcion 8
        messages.add(new String[]{preName_CantComun + "option.08", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "8"});
        messages.add(new String[]{preName_CantComun + "option.08", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "8"});
        messages.add(new String[]{preName_CantComun + "option.08", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "8"});
        //opcion 9
        messages.add(new String[]{preName_CantComun + "option.09", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "9"});
        messages.add(new String[]{preName_CantComun + "option.09", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "9"});
        messages.add(new String[]{preName_CantComun + "option.09", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "9"});
        //opcion 10
        messages.add(new String[]{preName_CantComun + "option.10", "es", cantComun, form_name, "1", "2018-07-10 17:48:00", "10"});
        messages.add(new String[]{preName_CantComun + "option.10", "en", cantComun, form_name, "1", "2018-07-10 17:48:00", "10"});
        messages.add(new String[]{preName_CantComun + "option.10", "pt", cantComun, form_name, "1", "2018-07-10 17:48:00", "10"});

        //CAMPO producto debito
        update(FORM_FIELDS, FIELD_ORDINAL, new String[]{"5"}, "id_form ='requestCheckbook' and id_field='producto'");
        //se agregan los mensajes de error de requerido
        String preFixMessage_producto = "fields." + form_name + ".producto.requiredError";
        messages.add(new String[]{preFixMessage_producto, "es", "producto", form_name, "1", "2018-07-10 17:48:00", "Debe seleccionar una Cuenta"});
        messages.add(new String[]{preFixMessage_producto, "en", "producto", form_name, "1", "2018-07-10 17:48:00", "You must select an Account"});
        messages.add(new String[]{preFixMessage_producto, "pt", "producto", form_name, "1", "2018-07-10 17:48:00", "Você deve selecionar uma conta"});

        //CAMPO producto para gastos
        update(FORM_FIELDS, FIELD_ORDINAL, new String[]{"6"}, "id_form ='requestCheckbook' and id_field='cuentaDebitoDeCosto'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Cuenta de débito de costo"}, "id_form ='requestCheckbook' and id_field='cuentaDebitoDeCosto' and lang ='es' and id_message='fields.requestCheckbook.cuentaDebitoDeCosto.label'");
        delete(FORM_FIELD_MESSAGES, "id_message = 'fields.requestCheckbook.cuentaDebitoDeCosto.help'");
        
        //CAMPO lugar de retiro
        String placeOfRetreat = "placeOfRetreat";

        insert(FORM_FIELDS, formFields, new String[]{placeOfRetreat, form_name, "1", "selector", "7", "TRUE", "TRUE", "default"});
        insert(FORM_FIELD_SELECTOR, selector_field, new String[]{placeOfRetreat, form_name, "1", "form-normal", "0", "combo"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{placeOfRetreat, form_name, "1", "branchOffices"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{placeOfRetreat, form_name, "1", "homeDelivery"});

        String preName_placeOfRetreat = "fields." + form_name + "." + placeOfRetreat + ".";
        messages.add(new String[]{preName_placeOfRetreat + "label", "es", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Lugar de Retiro"});
        messages.add(new String[]{preName_placeOfRetreat + "label", "en", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Withdrawal place"});
        messages.add(new String[]{preName_placeOfRetreat + "label", "pt", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Local de retirada"});

        messages.add(new String[]{preName_placeOfRetreat + "option.branchOffices", "es", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Sucursales"});
        messages.add(new String[]{preName_placeOfRetreat + "option.branchOffices", "en", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Branches"});
        messages.add(new String[]{preName_placeOfRetreat + "option.branchOffices", "pt", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Agências"});

        messages.add(new String[]{preName_placeOfRetreat + "option.homeDelivery", "es", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Entrega a domicilio"});
        messages.add(new String[]{preName_placeOfRetreat + "option.homeDelivery", "en", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Home delivery"});
        messages.add(new String[]{preName_placeOfRetreat + "option.homeDelivery", "pt", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Entrega a domicílio"});

        messages.add(new String[]{preName_placeOfRetreat + "requiredError", "es", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Debe seleccionar un Lugar de Retiro"});
        messages.add(new String[]{preName_placeOfRetreat + "requiredError", "en", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "You must select a Place of Withdrawal"});
        messages.add(new String[]{preName_placeOfRetreat + "requiredError", "pt", placeOfRetreat, form_name, "1", "2018-07-10 17:48:00", "Você deve selecionar um Local de retirada"});

        //CAMPO Sucursales 
        update(FORM_FIELDS, new String[]{"ordinal", "visible"}, new String[]{"8", "value(placeOfRetreat) == 'branchOffices'"}, "id_form = 'requestCheckbook' and id_field='lugarRetiro'");
        update("form_field_branchlist", new String[]{"show_other_option"}, new String[]{"0"}, "id_form ='requestCheckbook' and id_field='lugarRetiro'");

        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Sucursales"}, "id_form = 'requestCheckbook' and id_field ='lugarRetiro' and lang='es' and id_message ='fields.requestCheckbook.lugarRetiro.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Agências"}, "id_form = 'requestCheckbook' and id_field ='lugarRetiro' and lang='pt' and id_message ='fields.requestCheckbook.lugarRetiro.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Branches"}, "id_form = 'requestCheckbook' and id_field ='lugarRetiro' and lang='en' and id_message ='fields.requestCheckbook.lugarRetiro.label'");
        delete(FORM_FIELD_MESSAGES, "id_message ='fields.requestCheckbook.lugarRetiro.showOtherText'");
        //Ser agregan campos para la validacion de requerido
        messages.add(new String[]{"fields.requestCheckbook.lugarRetiro.requiredError", "es", "lugarRetiro", form_name, "1", "2018-07-10 17:48:00", "Debe seleccionar una sucursal de retiro"});
        messages.add(new String[]{"fields.requestCheckbook.lugarRetiro.requiredError", "en", "lugarRetiro", form_name, "1", "2018-07-10 17:48:00", "You must select a retirement branch"});
        messages.add(new String[]{"fields.requestCheckbook.lugarRetiro.requiredError", "pt", "lugarRetiro", form_name, "1", "2018-07-10 17:48:00", "Você deve selecionar um local de retirada"});

        //CAMPO Direccion
        update(FORM_FIELDS, new String[]{"ordinal", "visible"}, new String[]{"9", "value(placeOfRetreat) == 'homeDelivery'"}, "id_form = 'requestCheckbook' and id_field='domicilio'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Dirección"}, "id_message ='fields.requestCheckbook.domicilio.label' and lang='es'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Address"}, "id_message ='fields.requestCheckbook.domicilio.label' and lang='en'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Endereço"}, "id_message ='fields.requestCheckbook.domicilio.label' and lang='pt'");
        //se agregan mensajes para la validacion de requerido
        messages.add(new String[]{"fields.requestCheckbook.domicilio.requiredError", "es", "domicilio", form_name, "1", "2018-07-10 17:48:00", "Debe seleccionar una sucursal de retiro"});
        messages.add(new String[]{"fields.requestCheckbook.domicilio.requiredError", "en", "domicilio", form_name, "1", "2018-07-10 17:48:00", "You must select a retirement branch"});
        messages.add(new String[]{"fields.requestCheckbook.domicilio.requiredError", "pt", "domicilio", form_name, "1", "2018-07-10 17:48:00", "Você deve selecionar um local de retirada"});
        //se modifica el maxLength del campo para llegar a 200
        update("form_field_text", new String[]{"max_length"}, new String[]{"200"}, "id_form ='requestCheckbook' and id_field='domicilio'");

        //CAMPO titulo
        update(FORM_FIELDS, FIELD_ORDINAL, new String[]{"10"}, "id_form ='requestCheckbook' and id_field='tituloAutorizadoARetirar'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Autorizado a Retirar o recibir chequera"}, "id_message ='fields.requestCheckbook.tituloAutorizadoARetirar.label' and lang='es'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Authorized to Withdraw or receive a checkbook"}, "id_message ='fields.requestCheckbook.tituloAutorizadoARetirar.label' and lang='en'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Autorizado para retirar ou receber um talão de cheques"}, "id_message ='fields.requestCheckbook.tituloAutorizadoARetirar.label' and lang='pt'");

        //CAMPO nombre
        update(FORM_FIELDS, new String[]{"ordinal", "required"}, new String[]{"11", "TRUE"}, "id_form ='requestCheckbook' and id_field='nombre'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Name"}, "id_message ='fields.requestCheckbook.nombre.label' and lang='en'");
        update("form_field_text", new String[]{"max_length"}, new String[]{"50"}, "id_form ='requestCheckbook' and id_field='nombre'");
        messages.add(new String[]{"fields.requestCheckbook.nombre.requiredError", "es", "nombre", form_name, "1", "2018-07-10 17:48:00", "El campo Nombre no puede estar vacío"});
        messages.add(new String[]{"fields.requestCheckbook.nombre.requiredError", "en", "nombre", form_name, "1", "2018-07-10 17:48:00", "The Name field can not be empty"});
        messages.add(new String[]{"fields.requestCheckbook.nombre.requiredError", "pt", "nombre", form_name, "1", "2018-07-10 17:48:00", "O campo Nome não pode estar vazio"});

        //CAMPO apellido
        update(FORM_FIELDS, new String[]{"ordinal", "required"}, new String[]{"12", "TRUE"}, "id_form = 'requestCheckbook' and id_field='primerApellido'");
        update("form_field_text", new String[]{"max_length"}, new String[]{"50"}, "id_form ='requestCheckbook' and id_field='primerApellido'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Apellido"}, "lang='es' and id_message ='fields.requestCheckbook.primerApellido.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Sobrenome"}, "lang='pt' and id_message ='fields.requestCheckbook.primerApellido.label'");
        messages.add(new String[]{"fields.requestCheckbook.primerApellido.requiredError", "es", "primerApellido", form_name, "1", "2018-07-10 17:48:00", "El campo Apellido no puede estar vacío"});
        messages.add(new String[]{"fields.requestCheckbook.primerApellido.requiredError", "en", "primerApellido", form_name, "1", "2018-07-10 17:48:00", "The Surname field can not be empty"});
        messages.add(new String[]{"fields.requestCheckbook.primerApellido.requiredError", "pt", "primerApellido", form_name, "1", "2018-07-10 17:48:00", "O campo Sobrenome não pode estar vazio"});

        //CAMPO documento
        update(FORM_FIELDS, new String[]{"ordinal", "required"}, new String[]{"13", "TRUE"}, "id_form = 'requestCheckbook' and id_field='documento'");
        update("form_field_document", new String[]{"default_country", "default_document_type"}, new String[]{"UY", "CI"}, "id_form ='requestCheckbook' and id_field='documento'");
        messages.add(new String[]{"fields.requestCheckbook.documento.requiredError", "es", "documento", form_name, "1", "2018-07-10 17:48:00", "El campo Documento no puede estar vacío"});
        messages.add(new String[]{"fields.requestCheckbook.documento.requiredError", "en", "documento", form_name, "1", "2018-07-10 17:48:00", "The Document field can not be empty"});
        messages.add(new String[]{"fields.requestCheckbook.documento.requiredError", "pt", "documento", form_name, "1", "2018-07-10 17:48:00", "O campo Documento não pode estar vazio"});

        //CAMPO terminos y condiciones
        String terminos = "terminosCondiciones";
        insert(FORM_FIELDS, formFields, new String[]{terminos, form_name, "1", "termsandconditions", "14", "TRUE", "TRUE", "default"});
        insert("form_field_terms_conditions", new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"},
                new String[]{terminos, form_name, "1", "field-normal", "1", "1"});

        String messageNameTC = "fields." + form_name + "." + terminos;

        messages.add(new String[]{messageNameTC + ".label", "es", terminos, form_name, "1", "2018-07-10 17:48:00", "Términos y condiciones"});
        messages.add(new String[]{messageNameTC + ".label", "en", terminos, form_name, "1", "2018-07-10 17:48:00", "Terms and conditions"});
        messages.add(new String[]{messageNameTC + ".label", "pt", terminos, form_name, "1", "2018-07-10 17:48:00", "Termos e condições"});

        messages.add(new String[]{messageNameTC + ".requiredError", "es", terminos, form_name, "1", "2018-07-10 17:48:00", "Debe aceptar los términos y condiciones"});
        messages.add(new String[]{messageNameTC + ".requiredError", "en", terminos, form_name, "1", "2018-07-10 17:48:00", "You must accept the terms and conditions"});
        messages.add(new String[]{messageNameTC + ".requiredError", "pt", terminos, form_name, "1", "2018-07-10 17:48:00", "Você deve aceitar os termos e condições"});

        messages.add(new String[]{messageNameTC + ".showAcceptOptionText", "es", terminos, form_name, "1", "2018-07-10 17:48:00", "Aceptar términos y condiciones"});
        messages.add(new String[]{messageNameTC + ".showAcceptOptionText", "en", terminos, form_name, "1", "2018-07-10 17:48:00", "Accept terms"});
        messages.add(new String[]{messageNameTC + ".showAcceptOptionText", "pt", terminos, form_name, "1", "2018-07-10 17:48:00", "Aceitar termos e condições"});

        messages.add(new String[]{messageNameTC + ".termsAndConditions", "es", terminos, form_name, "1", "2018-07-10 17:48:00", "Las instrucciones enviadas al banco por usted luego de las <hora de corte> serán procesadas al siguiente día hábil bancario. Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar."});
        messages.add(new String[]{messageNameTC + ".termsAndConditions", "en", terminos, form_name, "1", "2018-07-10 17:48:00", "The instructions sent to the bank by you after the <cutoff time> will be processed on the following banking business day. I authorize to debit my account with the commissions accrued by this instruction."});
        messages.add(new String[]{messageNameTC + ".termsAndConditions", "pt", terminos, form_name, "1", "2018-07-10 17:48:00", "As instruções enviadas por você ao banco após as <hora de corte> serão processadas no dia útil bancário seguinte. Autorizo a debitar da minha conta as comissões que esta instrução possa gerar."});

        //Eliminando campo obsoleto
        delete(FORM_FIELD_MESSAGES, "id_form = 'requestCheckbook' and id_field ='segundoApellido'");
        delete("form_field_text", "id_form = 'requestCheckbook' and id_field = 'segundoApellido'");
        delete(FORM_FIELDS, "id_form = 'requestCheckbook' and id_field='segundoApellido'");

        for (String[] key : messages) {
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + key[0] + "', '" + key[1] + "', '" + key[2] + "', '" + key[3] + "', '" + key[4] + "', '" + key[6] + "', TO_DATE('" + key[5] + "', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert(FORM_FIELD_MESSAGES, field_messages, key);
            }
        }

    }
}