/* 
 * Copyright 2017 Manentia Software. 
 * 
 * This software component is the intellectual property of Manentia Software S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * http://www.manentiasoftware.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2944
 *
 * @author ldurand
 */
public class DB20170808_1048_2944 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_fields", new String[]{"visible"}, new String[]{"hasValue(increaseDecreace)"}, "id_form = 'editBondsAndGuarantees' AND id_field = 'amount'");
    }
}
