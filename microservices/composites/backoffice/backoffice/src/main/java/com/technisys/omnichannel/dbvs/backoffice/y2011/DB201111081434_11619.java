/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201111081434_11619 extends DBVSUpdate {

    @Override
    public void up() {
        // rub.payment.payService
        delete("permissions", "id_permission = 'rub.payment.payService'");
        delete("permissions_credentials_groups", "id_permission = 'rub.payment.payService' AND id_credential_group = 'onlyPassword'");
        delete("role_permissions", "id_role = 'client_transfer' AND id_permission = 'rub.payment.payService'");
        delete("role_permissions", "id_role = 'client_operator' AND id_permission = 'rub.payment.payService'");

        // rub.payment.serviceAssociation
        delete("permissions", "id_permission = 'rub.payment.serviceAssociation'");
        delete("permissions_credentials_groups", "id_permission = 'rub.payment.serviceAssociation' AND id_credential_group = 'onlyPassword'");
        delete("role_permissions", "id_role = 'client_transfer' AND id_permission = 'rub.payment.serviceAssociation'");
        delete("role_permissions", "id_role = 'client_operator' AND id_permission = 'rub.payment.serviceAssociation'");

        // rub.payment.checkDiscount
        delete("permissions", "id_permission = 'rub.payment.checkDiscount'");
        delete("permissions_credentials_groups", "id_permission = 'rub.payment.checkDiscount' AND id_credential_group = 'onlyPassword'");
        delete("role_permissions", "id_role = 'client_transfer' AND id_permission = 'rub.payment.checkDiscount'");
        delete("role_permissions", "id_role = 'client_operator' AND id_permission = 'rub.payment.checkDiscount'");
    }
}