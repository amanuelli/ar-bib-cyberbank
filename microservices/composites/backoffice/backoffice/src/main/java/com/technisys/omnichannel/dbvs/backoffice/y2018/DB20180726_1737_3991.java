/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-3991
 *
 * @author mcheveste
 */
public class DB20180726_1737_3991 extends DBVSUpdate {

    @Override
    public void up() {
        final String FORM_ID = "transferForeign";
        final String CURRENT_DATE = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        final String VERSION = "1";
        Map<String, String> messages = new HashMap<>();

        insert("form_field_text",
                new String[]{"id_field", "id_form", "form_version", "min_length", "max_length",
                    "display_type"},
                new String[]{"creditAccount", FORM_ID, VERSION, "1", "30", "field-normal"});

        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                    "required", "visible_in_mobile", "sub_type"},
                new String[]{"shouldDebitFromOriginAccount", FORM_ID, VERSION, "selector", "3",
                    "TRUE", "FALSE", "1", "default"});
        insert("form_field_selector",
                new String[]{"id_field", "id_form", "form_version", "display_type", "default_value",
                    "render_as"},
                new String[]{"shouldDebitFromOriginAccount", FORM_ID, VERSION, "field-normal", "",
                    "check"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"shouldDebitFromOriginAccount", FORM_ID, VERSION, "true"});
        messages.put("shouldDebitFromOriginAccount.label", "Débito de comisiones de la transacción");
        messages.put("shouldDebitFromOriginAccount.option.true",
                "Todas las comisiones se debitarán de la cuenta origen seleccionada. Si lo deseas puedes cargar los gastos a otra cuenta. Hacer click aquí");

        String shouldDebitFromOriginAccountCondition = "value(shouldDebitFromOriginAccount) == 'true'";
        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                    "required", "visible_in_mobile", "sub_type"},
                new String[]{"debitCommissionsFromOptions", FORM_ID, VERSION, "selector", "4",
                    shouldDebitFromOriginAccountCondition, shouldDebitFromOriginAccountCondition, "1", "default"});
        insert("form_field_selector",
                new String[]{"id_field", "id_form", "form_version", "display_type", "default_value",
                    "render_as"},
                new String[]{"debitCommissionsFromOptions", FORM_ID, VERSION, "field-normal", "",
                    "radio"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"debitCommissionsFromOptions", FORM_ID, VERSION,
                    "1stRecipientAccount"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"debitCommissionsFromOptions", FORM_ID, VERSION,
                    "2ndOriginAndRecipientAccounts"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"debitCommissionsFromOptions", FORM_ID, VERSION,
                    "3rdOwnerAndRecipientAccounts"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"debitCommissionsFromOptions", FORM_ID, VERSION, "4thOwnerAccount"});

        messages.put("debitCommissionsFromOptions.label", "Opciones");
        messages.put("debitCommissionsFromOptions.option.1stRecipientAccount", "Al destinatario");
        messages.put("debitCommissionsFromOptions.option.2ndOriginAndRecipientAccounts",
                "Compartido entre el destinatario y la cuenta origen");
        messages.put("debitCommissionsFromOptions.option.3rdOwnerAndRecipientAccounts",
                "Compartido entre el destinatario y otra cuenta propia");
        messages.put("debitCommissionsFromOptions.option.4thOwnerAccount", "A otra cuenta propia");

        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                    "required", "visible_in_mobile", "sub_type"},
                new String[]{"recipientAccountCode", FORM_ID, VERSION, "bankselector", "8", "TRUE",
                    "TRUE", "1", "foreigners"});
        insert("form_field_bankselector",
                new String[]{"id_field", "id_form", "form_version", "display_type"},
                new String[]{"recipientAccountCode", FORM_ID, VERSION, "field-normal"});
        messages.put("recipientAccountCode.label", "Código del banco");

        insert("form_field_bankselector_codes",
                new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"recipientAccountCode", FORM_ID, VERSION, "ABA"});
        insert("form_field_bankselector_codes",
                new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"recipientAccountCode", FORM_ID, VERSION, "BLZ"});
        insert("form_field_bankselector_codes",
                new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"recipientAccountCode", FORM_ID, VERSION, "CHIPS"});
        insert("form_field_bankselector_codes",
                new String[]{"id_field", "id_form", "form_version", "code_type"},
                new String[]{"recipientAccountCode", FORM_ID, VERSION, "SWIFT"});

        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                    "required", "visible_in_mobile", "sub_type"},
                new String[]{"recipientName", FORM_ID, VERSION, "text", "10", "TRUE", "TRUE", "1",
                    "default"});
        insert("form_field_text",
                new String[]{"id_field", "id_form", "form_version", "min_length", "max_length",
                    "display_type"},
                new String[]{"recipientName", FORM_ID, VERSION, "1", "50", "field-normal"});
        messages.put("recipientName.label", "Nombre del destinatario");

        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                    "required", "visible_in_mobile", "sub_type"},
                new String[]{"recipientAddress", FORM_ID, VERSION, "text", "11", "TRUE", "TRUE", "1",
                    "default"});
        insert("form_field_text",
                new String[]{"id_field", "id_form", "form_version", "min_length", "max_length",
                    "display_type"},
                new String[]{"recipientAddress", FORM_ID, VERSION, "1", "50", "field-normal"});
        messages.put("recipientAddress.label", "Dirección del destinatario");

        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                    "required", "visible_in_mobile", "sub_type"},
                new String[]{"isUsingIntermediaryBank", FORM_ID, VERSION, "selector", "16", "TRUE",
                    "FALSE", "1", "default"});
        insert("form_field_selector",
                new String[]{"id_field", "id_form", "form_version", "display_type", "default_value",
                    "render_as"},
                new String[]{"isUsingIntermediaryBank", FORM_ID, VERSION, "field-normal", "",
                    "check"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"isUsingIntermediaryBank", FORM_ID, VERSION, "true"});
        messages.put("isUsingIntermediaryBank.label", "Banco intermediario");
        messages.put("isUsingIntermediaryBank.option.true", "Usar banco intermediario");

        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                    "required", "visible_in_mobile", "sub_type"},
                new String[]{"endOfRecipientInfo", FORM_ID, VERSION, "horizontalrule", "15", "TRUE",
                    "FALSE", "1", "default"});

        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                    "required", "visible_in_mobile", "sub_type"},
                new String[]{"endOfForm", FORM_ID, VERSION, "horizontalrule", "19", "TRUE", "FALSE",
                    "1", "default"});

        messages.put("amount.invalidError", "El campo no puede ser cero");

        messages.put("amount.placeholder", "Ingresa el monto a transferir");
        messages.put("recipientAccountCode.placeholder", "Ingresa el código del banco destino");
        messages.put("creditAccount.placeholder", "Ingresa la cuenta de destino");
        messages.put("recipientName.placeholder", "Ingresa el nombre del destinatario");
        messages.put("recipientAddress.placeholder", "Ingresa la dirección del destinatario");
        messages.put("debitReference.placeholder", "Ingresa una referencia para mostrarle al destinatario");
        messages.put("notificationEmails.placeholder", "Ingresa uno o más correos electrónicos a notificar");
        messages.put("notificationBody.placeholder",
                "Este mensaje se agregará a la información de la transaccioón que se envía por correo electrónico a los destinos listados en el campo anterior");
        messages.put("intermediaryBank.placeholder", "Ingresa el código del banco intermediario");
        messages.put("intermediaryAccountNumber.placeholder", "Ingresa la cuenta del banco intermediario");

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form",
            "form_version", "value", "modification_date"};

        messages.keySet().stream().map((key) -> new String[]{"fields." + FORM_ID + "." + key, "es",
            key.substring(0, key.indexOf(".")), FORM_ID, VERSION, messages.get(key), CURRENT_DATE}).forEachOrdered((formFieldsValues) -> {
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE,
                        "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '"
                        + formFieldsValues[1] + "', '" + formFieldsValues[2]
                        + "', '" + formFieldsValues[3] + "', '"
                        + formFieldsValues[4] + "','" + formFieldsValues[5]
                        + "', TO_DATE('2018-07-27 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        });

        update("form_field_messages", new String[]{"value"}, new String[]{"Monto a transferir"},
                "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.amount.label' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.amount.help' and lang='es'");
        update("form_field_messages", new String[]{"value"},
                new String[]{"El campo no puede estar vacío"}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.amount.requiredError' and lang='es'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Cuenta de origen"}, "id_form='"
                + FORM_ID
                + "' and id_message='fields.transferForeign.debitAccount.label' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.debitAccount.hint' and lang='es'");
        update("form_field_messages", new String[]{"value"},
                new String[]{"Debe seleccionar una cuenta de origen"}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.debitAccount.requiredError' and lang='es'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Destino"}, "id_form='"
                + FORM_ID
                + "' and id_message='fields.transferForeign.sectionCreditData.label' and lang='es'");

        update("form_field_messages", new String[]{"value"},
                new String[]{"El campo no puede estar vacío"}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.debitAccount.requiredError' and lang='es'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Cuenta de destino"},
                "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.creditAccount.label' and lang='es'");
        update("form_field_messages", new String[]{"value"},
                new String[]{"El campo no puede estar vacío"}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.creditAccount.requiredError' and lang='es'");

        update("form_field_messages", new String[]{"value"},
                new String[]{"El campo no puede estar vacío"}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.recipientName.requiredError' and lang='es'");

        update("form_field_messages", new String[]{"value"},
                new String[]{"El campo no puede estar vacío"}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.recipientAddress.requiredError' and lang='es'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Referencia"}, "id_form='"
                + FORM_ID
                + "' and id_message='fields.transferForeign.debitReference.label' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.debitReference.help' and lang='es'");

        update("form_field_messages", new String[]{"value"},
                new String[]{"Correos electrónicos a notificar"}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.notificationEmails.label' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.notificationEmails.help' and lang='es'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Mensaje"}, "id_form='"
                + FORM_ID
                + "' and id_message='fields.transferForeign.notificationBody.label' and lang='es'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Código del banco"}, "id_form='"
                + FORM_ID
                + "' and id_message='fields.transferForeign.intermediaryBank.label' and lang='es'");

        update("form_field_messages", new String[]{"value"},
                new String[]{"Cuenta del banco intermediario"}, "id_form='" + FORM_ID
                + "' and id_message='fields.transferForeign.intermediaryAccountNumber.label' and lang='es'");

        update("form_fields", new String[]{"ordinal"}, new String[]{"1"},
                "id_form='" + FORM_ID + "' and id_field='amount'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"2"},
                "id_form='" + FORM_ID + "' and id_field='debitAccount'");

        update("form_fields", new String[]{"ordinal", "visible", "required"}, new String[]{"5",
            "value(debitCommissionsFromOptions) == '3rdOwnerAndRecipientAccounts|4thOwnerAccount'",
            "value(debitCommissionsFromOptions) == '3rdOwnerAndRecipientAccounts|4thOwnerAccount'"},
                "id_form='" + FORM_ID + "' and id_field='expenseAccount'");
        update("form_fields", new String[]{"type", "ordinal"}, new String[]{"text", "9"},
                "id_form='" + FORM_ID + "' and id_field='creditAccount'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"12"},
                "id_form='" + FORM_ID + "' and id_field='debitReference'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"13"},
                "id_form='" + FORM_ID + "' and id_field='notificationEmails'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"14"},
                "id_form='" + FORM_ID + "' and id_field='notificationBody'");

        String isUsingIntermediaryBankCondition = "value(isUsingIntermediaryBank) == 'true'";
        update("form_fields", new String[]{"ordinal", "required", "visible"}, new String[]{"17", isUsingIntermediaryBankCondition, isUsingIntermediaryBankCondition},
                "id_form='" + FORM_ID + "' and id_field='intermediaryBank'");
        update("form_fields", new String[]{"ordinal", "required", "visible"}, new String[]{"18", isUsingIntermediaryBankCondition, isUsingIntermediaryBankCondition},
                "id_form='" + FORM_ID + "' and id_field='intermediaryAccountNumber'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Transferir a otro banco"},
                "id_form='" + FORM_ID
                + "' and id_message='forms.transferLocal.formName' and lang='es'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Transferir al exterior"},
                "id_form='" + FORM_ID
                + "' and id_message='forms.transferForeign.formName' and lang='es'");

        update("forms", new String[]{"programable"}, new String[]{"1"}, "id_form='" + FORM_ID + "'");

        delete("form_fields", "id_form='" + FORM_ID + "' and id_field = 'sectionNotificationInfo'");
        delete("form_field_messages", "id_form='" + FORM_ID + "' and id_field = 'sectionNotificationInfo'");

        delete("form_fields", "id_form='" + FORM_ID + "' and id_field = 'sectionIntermediaryBank'");
        delete("form_field_messages", "id_form='" + FORM_ID + "' and id_field = 'sectionIntermediaryBank'");

        delete("form_fields", "id_form='" + FORM_ID + "' and id_field LIKE 'beneficiaryInfo_'");
        delete("form_field_text", "id_form='" + FORM_ID + "' and id_field LIKE 'beneficiaryInfo_'");
        delete("form_field_messages", "id_form='" + FORM_ID + "' and id_field LIKE 'beneficiaryInfo_'");

        delete("form_fields", "id_form='" + FORM_ID + "' and id_field='typeOfExpenses'");
        delete("form_field_selector", "id_form='" + FORM_ID + "' and id_field='typeOfExpenses'");
        delete("form_field_messages", "id_form='" + FORM_ID + "' and id_field='typeOfExpenses'");
    }
}
