package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
*
* @author plinkweiler
*/
public class DB20170829_1405_3141 extends DBVSUpdate {

    @Override
    public void up() {
        
    	//inserto una nueva categoria
    	insert("adm_ui_categories", new String[] { "id_category", "ordinal" }, new String[] { "frequentDestinations", "1000" });
    	
    	//"apunto" los permisos de destinos frecuentas hacia esta nueva categoria
    	update("adm_ui_permissions", new String[]{"simple_id_category","simple_id_subcategory", "medium_id_category","medium_id_subcategory", "advanced_id_category","advanced_id_subcategory"}, 
    				new String[]{"frequentDestinations",null, "frequentDestinations", null,"frequentDestinations", null}, 
    				"id_permission = 'frequentDestinations.manage'");
    	
    	//elimino la subcategoria utilizada para destinos frecuentes
    	delete("adm_ui_subcategories", "id_subcategory = 'frequentDestinations'");
          
      }
    
}
