package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5540
 *
 * @author mcheveste
 */
public class DB20180924_1650_5540 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.medium.read.details", "com.technisys.omnichannel.client.activities.administration.medium.ReadDetailsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");

    }
}
