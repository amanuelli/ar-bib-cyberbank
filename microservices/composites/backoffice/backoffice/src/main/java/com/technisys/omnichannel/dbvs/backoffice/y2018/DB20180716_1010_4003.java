/*
 * Copyright 2018 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4003
 *
 * @author aalves
 */
public class DB20180716_1010_4003 extends DBVSUpdate {
    @Override
    public void up() {
        update("form_field_messages", new String[]{"value"}, new String[]{"Teléfono móvil"}, "id_message='fields.creditCardChangeCondition.contactForm.option.phone' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Mobile phone"}, "id_message='fields.creditCardChangeCondition.contactForm.option.phone' AND lang='en'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Telefone móvel"}, "id_message='fields.creditCardChangeCondition.contactForm.option.phone' AND lang='pt'");
    }
}
