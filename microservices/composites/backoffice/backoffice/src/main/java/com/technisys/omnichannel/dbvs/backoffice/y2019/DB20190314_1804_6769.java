/* 
 * Copyright 2019 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6769
 *
 * @author emordezki
 */
public class DB20190314_1804_6769 extends DBVSUpdate {

    @Override
    public void up() {
        // POIs close to Montevideo's Lab
        String[] fieldNames = new String[] { "id_poi_type", "latitude", "longitude", "enabled" };
        String[] fieldValues = new String[] { "1", "-34.88708600", "-56.14869100", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        fieldValues = new String[] { "1", "-34.88708600", "-56.15049100", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        fieldValues = new String[] { "1", "-34.88708600", "-56.15149100", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        
        fieldValues = new String[] { "1", "-34.88888600", "-56.14869100", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        fieldValues = new String[] { "1", "-34.88968600", "-56.14869100", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        fieldValues = new String[] { "1", "-34.89048600", "-56.14869100", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        
        fieldValues = new String[] { "2", "-34.88968600", "-56.15049100", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        fieldValues = new String[] { "2", "-34.89048600", "-56.15149100", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        
        // POIs close to Tandil's lab
        fieldValues = new String[] { "1", "-37.3227235", "-59.1375656", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        fieldValues = new String[] { "1", "-37.3245235", "-59.1375656", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        fieldValues = new String[] { "1", "-37.3253235", "-59.1375656", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        
        fieldValues = new String[] { "1", "-37.3227235", "-59.1393656", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        fieldValues = new String[] { "1", "-37.3227235", "-59.1401256", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        
        fieldValues = new String[] { "1", "-37.3245235", "-59.1393656", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
        fieldValues = new String[] { "1", "-37.3253235", "-59.1401256", "1" };
        insert("points_of_interest", fieldNames, fieldValues);
    }
}