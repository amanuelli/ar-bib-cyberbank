/*
 *  Copyright 2012 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Diego Curbelo &lt;dcurbelo@technisys.com&gt;
 */
public class DB201209171604_119 extends DBVSUpdate {

    @Override
    public void up() {
        insert("configuration", new String[]{"id_field", "value", "possible_values", "id_group", "encrypted", "must_be_encrypted"},
                new String[]{"hmac_hash_function", "HmacSHA1", "HmacSHA1|HmacSHA256", "Core", "0", "0"});

        insert("configuration", new String[]{"id_field", "value", "possible_values", "id_group", "encrypted", "must_be_encrypted"},
                new String[]{"hmac_key", "bb05ae0b2d3347e8074e6ffbc3c86c72", "", "Core", "0", "0"});

        insert("configuration", new String[]{"id_field", "value", "possible_values", "id_group", "encrypted", "must_be_encrypted"},
                new String[]{"rubicon.session.timeout", "30", "", "Rubicon", "0", "0"});

    }
}