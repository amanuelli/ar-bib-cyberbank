/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author lpoll
 */

public class DB20190128_1748_6358 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "requestTransactionCancellation";
        String idField = "date";
        String queryMaxVersionForm = "SELECT MAX(version) FROM forms WHERE id_form = '" + idForm + "'";

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "DELETE FROM form_field_date WHERE  id_form = '" + idForm + "' AND form_version IN ( "
                        + queryMaxVersionForm + " ) ");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "UPDATE form_fields SET type = 'text' WHERE id_form = '" + idForm + "' AND id_field = '" + idField + "' "
                        + " AND form_version IN (" + queryMaxVersionForm + ")");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_text (id_field, id_form, form_version, display_type, min_length, max_length) "
                        + " SELECT '" + idField + "', '" + idForm + "', MAX(version), 'field-small', '0', '10' "
                        + " FROM forms WHERE id_form = '" + idForm + "' ");
    }

}