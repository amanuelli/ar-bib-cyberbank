/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2491
 *
 * @author szuliani
 */
public class DB20170628_1541_2491 extends DBVSUpdate {

    @Override
    public void up() {

        insertOrUpdateConfiguration("backoffice.forms.categories",
                "accounts|deposits|loans|creditcards|payments|transfers|comex|others", ConfigurationGroup.NEGOCIO, null, new String[]{});

        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible",
                "required", "sub_type", "visible_in_mobile" };
        String[] formFieldsReadOnly = new String[] { "id_field", "id_form", "form_version", "type", "ordinal",
                "visible", "required", "sub_type", "visible_in_mobile", "read_only" };
        String[] formFieldsValues;
        String idForm = "openCreditLetter";
        String version = "1";

        int ordinal = 1;

        Map<String, String> messages = new HashMap<String, String>();

        /**** Insert Form - Apertura de carta de crédito ****/
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[] { "id_form", "version", "enabled", "category", "type", "id_bpm_process",
                "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable" };
        String[] formsFieldsValues = new String[] { idForm, version, "1", "comex", "process", "demo:1:3", "comex", null,
                "1", "1", "1" };
        insert("forms", formsFields, formsFieldsValues);

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE,
                    "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                            + " VALUES ('forms." + idForm + ".formName', '" + idForm + "', '" + version
                            + "', 'es', 'Apertura de carta de crédito', TO_DATE('2017-0623 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value",
                    "modification_date" };
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version,
                    "es", "Apertura de carta de crédito", date });
        }

        insert("permissions", new String[] { "id_permission" }, new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[] { "id_permission", "id_credential_group" },
                new String[] { "client.form." + idForm + ".send", "pin" });

        /**** Insert field text - BIC Banco avisador ****/
        formFieldsValues = new String[] { "bicInformantBank", idForm, version, "text", Integer.toString(ordinal),
                "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "bicInformantBank", idForm, version, "0", "30", "field-normal", null });
        messages.put("bicInformantBank.label", "BIC Banco avisador");
        messages.put("bicInformantBank.requiredError", "Debe ingresar el código BIC del banco avisador");
        ordinal++;

        /**** Insert field text - Nombre banco avisador ****/
        formFieldsValues = new String[] { "nameInformantBank", idForm, version, "text", Integer.toString(ordinal),
                "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "nameInformantBank", idForm, version, "0", "30", "field-normal", null });
        messages.put("nameInformantBank.label", "Nombre banco avisador");
        messages.put("nameInformantBank.requiredError", "Debe ingresar el nombre del banco avisador");
        ordinal++;

        /**** Insert field text - Dirección banco avisador ****/
        formFieldsValues = new String[] { "addressInformantBank", idForm, version, "text", Integer.toString(ordinal),
                "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "addressInformantBank", idForm, version, "0", "200", "field-big", null });
        messages.put("addressInformantBank.label", "Dirección banco avisador");
        ordinal++;

        /**** Insert field date - Fecha de vencimiento ****/
        insert("form_fields", formFields, new String[] { "dueDate", idForm, version, "date", Integer.toString(ordinal),
                "TRUE", "TRUE", "default", "1" });
        insert("form_field_date", new String[] { "id_field", "id_form", "form_version", "display_type" },
                new String[] { "dueDate", idForm, version, "field-small" });
        messages.put("dueDate.label", "Fecha de vencimiento");
        ordinal++;

        /**** Insert field text - Lugar de vencimiento ****/
        formFieldsValues = new String[] { "expirationPlace", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "expirationPlace", idForm, version, "0", "29", "field-normal", null });
        messages.put("expirationPlace.label", "Lugar de vencimiento");
        messages.put("expirationPlace.help", "Lugar de presentación de los documentos");
        messages.put("expirationPlace.requiredError", "Debe ingresar un lugar de vencimiento");
        ordinal++;

        /**** Insert field text - Nombre del ordenante ****/
        formFieldsValues = new String[] { "payerName", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "0", "1" };
        insert("form_fields", formFieldsReadOnly, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "payerName", idForm, version, "0", "50", "field-normal", null });
        messages.put("payerName.label", "Nombre del ordenante");
        ordinal++;

        /**** Insert field text - Dirección del ordenante ****/
        formFieldsValues = new String[] { "payerAddress", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "payerAddress", idForm, version, "0", "50", "field-normal", null });
        messages.put("payerAddress.label", "Dirección del ordenante");
        messages.put("payerAddress.requiredError", "Debe ingresar la dirección del ordenante");
        ordinal++;

        /**** Insert field text - Número de cuenta del beneficiario ****/
        formFieldsValues = new String[] { "beneficiaryAccountNumber", idForm, version, "text",
                Integer.toString(ordinal), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "beneficiaryAccountNumber", idForm, version, "0", "30", "field-normal", null });
        messages.put("beneficiaryAccountNumber.label", "Número de cuenta del beneficiario");
        ordinal++;

        /**** Insert field text - Nombre y dirección del beneficiario ****/
        formFieldsValues = new String[] { "beneficiaryData", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "beneficiaryData", idForm, version, "0", "200", "field-big", null });
        messages.put("beneficiaryData.label", "Nombre y dirección del beneficiario");
        messages.put("beneficiaryData.requiredError", "Debe ingresar el nombre y dirección del beneficiario");
        ordinal++;

        /**** Insert field amount - Importe ****/
        formFieldsValues = new String[] { "amount", idForm, version, "amount", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[] { "id_field", "id_form", "form_version", "display_type",
                "control_limits", "use_for_total_amount" },
                new String[] { "amount", idForm, version, "field-small", "0", "0" });
        messages.put("amount.label", "Importe");
        ordinal++;

        /**** Insert field selector - Tolerancia en importe ****/
        formFieldsValues = new String[] { "amountTolerance", idForm, version, "selector", Integer.toString(ordinal),
                "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "amountTolerance", idForm, version, "field-small", "0", "0", "check" });
        messages.put("amountTolerance.label", "Tolerancia en importe");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "amountTolerance", idForm, version, "0" });
        messages.put("amountTolerance.option.0", "Sin Tolerancia");
        ordinal++;

        /**** Insert field text - Min ****/
        formFieldsValues = new String[] { "min", idForm, version, "text", Integer.toString(ordinal),
                "value(amountTolerance) != '0'", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "min", idForm, version, "0", "2", "field-smaller", "onlyNumbers" });
        messages.put("min.label", "Min");
        ordinal++;

        /**** Insert field text - Max ****/
        formFieldsValues = new String[] { "max", idForm, version, "text", Integer.toString(ordinal),
                "value(amountTolerance) != '0'", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "max", idForm, version, "0", "2", "field-smaller", "onlyNumbers" });
        messages.put("max.label", "Max");
        ordinal++;

        /****
         * Insert field selector - Esta tolerancia aplica también a la
         * mercadería
         ****/
        formFieldsValues = new String[] { "merchandiseTolerance", idForm, version, "selector",
                Integer.toString(ordinal), "value(amountTolerance) != '0'", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "merchandiseTolerance", idForm, version, "field-small", "0", "0", "check" });
        messages.put("merchandiseTolerance.label", "Esta tolerancia aplica también a la mercadería");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "merchandiseTolerance", idForm, version, "0" });
        messages.put("merchandiseTolerance.option.0", "Si");
        ordinal++;

        /***** Insert field selector - Disponible como sigue ****/
        formFieldsValues = new String[] { "available", idForm, version, "selector", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "available", idForm, version, "field-small", "1", "0", "combo" });
        messages.put("available.label", "Disponible como sigue");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "available", idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "available", idForm, version, "2" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "available", idForm, version, "3" });
        messages.put("available.option.1", "Pago a la vista");
        messages.put("available.option.2", "Plazo");
        messages.put("available.option.3", "Mixto");
        ordinal++;

        /**** Insert field text - Cantidad de días ****/
        formFieldsValues = new String[] { "numberDays", idForm, version, "text", Integer.toString(ordinal),
                "value(available) == '2'", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "numberDays", idForm, version, "0", "3", "field-smaller", "onlyNumbers" });
        messages.put("numberDays.label", "Cantidad de días");
        messages.put("numberDays.requiredError", "Debe ingresar la cantidad de dias");
        ordinal++;

        /***** Insert field selector - Desde ****/
        formFieldsValues = new String[] { "from", idForm, version, "selector", Integer.toString(ordinal),
                "value(available) == '2'", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "from", idForm, version, "field-small", "1", "0", "combo" });
        messages.put("from.label", "Desde");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "from", idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "from", idForm, version, "2" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "from", idForm, version, "3" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "from", idForm, version, "4" });
        messages.put("from.option.1", "Fecha Embarque");
        messages.put("from.option.2", "Fecha factura");
        messages.put("from.option.3", "Vista");
        messages.put("from.option.4", "Otra");
        ordinal++;

        /**** Insert field text - Plazo ****/
        formFieldsValues = new String[] { "term", idForm, version, "text", Integer.toString(ordinal),
                "value(from) == '4'", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "term", idForm, version, "0", "3", "field-smaller", "onlyNumbers" });
        messages.put("term.label", "Plazo");
        messages.put("term.requiredError", "Debe ingresar un plazo");
        ordinal++;

        /**** Insert field textarea - Descripcion ****/
        insert("form_fields", formFields, new String[] { "description", idForm, version, "textarea",
                Integer.toString(ordinal), "value(available) == '3'", "FALSE", "default", "1" });
        insert("form_field_textarea",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" },
                new String[] { "description", idForm, version, "0", "50", "field-normal" });
        messages.put("description.label", "Descripción");
        ordinal++;

        /**** Insert field selector - Requiere letra de cambio ****/
        formFieldsValues = new String[] { "requiresChange", idForm, version, "selector", Integer.toString(ordinal),
                "value(available) == '3'", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "requiresChange", idForm, version, "field-small", "0", "0", "check" });
        messages.put("requiresChange.label", "Requiere letra de cambio");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "requiresChange", idForm, version, "0" });
        messages.put("requiresChange.option.0", "Si");
        ordinal++;

        /***** Insert field selector - Embarques parciales ****/
        formFieldsValues = new String[] { "partialShipments", idForm, version, "selector", Integer.toString(ordinal),
                "TRUE", "TRUE", "default", "10" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "partialShipments", idForm, version, "field-small", "1", "0", "combo" });
        messages.put("partialShipments.label", "Embarques parciales");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "partialShipments", idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "partialShipments", idForm, version, "2" });
        messages.put("partialShipments.option.1", "Permitidos");
        messages.put("partialShipments.option.2", "Prohibidos");
        ordinal++;

        /***** Insert field selector - Trasbordos ****/
        formFieldsValues = new String[] { "transfers", idForm, version, "selector", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "transfers", idForm, version, "field-small", "1", "0", "combo" });
        messages.put("transfers.label", "Trasbordos");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "transfers", idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "transfers", idForm, version, "2" });
        messages.put("transfers.option.1", "Permitidos");
        messages.put("transfers.option.2", "Prohibidos");
        ordinal++;

        /**** Insert field date - Fecha vto. embarque ****/
        insert("form_fields", formFields, new String[] { "shipmentDate", idForm, version, "date",
                Integer.toString(ordinal), "TRUE", "FALSE", "default", "1" });
        insert("form_field_date", new String[] { "id_field", "id_form", "form_version", "display_type" },
                new String[] { "shipmentDate", idForm, version, "field-small" });
        messages.put("shipmentDate.label", "Fecha vto. embarque");
        ordinal++;

        /**** Insert field text - Período de embarque ****/
        formFieldsValues = new String[] { "boardingPeriod", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "boardingPeriod", idForm, version, "0", "390", "field-big", null });
        messages.put("boardingPeriod.label", "Período de embarque");
        messages.put("boardingPeriod.help", "Ingrese aquí el cronograma de embarque");
        ordinal++;

        /**** Insert field text - Lugar de entrega de la mercadería ****/
        formFieldsValues = new String[] { "deliveryPlace", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "deliveryPlace", idForm, version, "0", "65", "field-normal", null });
        messages.put("deliveryPlace.label", "Lugar de entrega de la mercadería");
        ordinal++;

        /**** Insert field text - Puerto/Aeropuerto de embarque ****/
        formFieldsValues = new String[] { "boardingPort", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "boardingPort", idForm, version, "0", "65", "field-normal", null });
        messages.put("boardingPort.label", "Puerto/Aeropuerto de embarque");
        messages.put("boardingPort.requiredError", "Debe ingresar un Puerto/Aerupoerto de embarque");
        ordinal++;

        /**** Insert field text - Puerto/Aeropuerto de destino ****/
        formFieldsValues = new String[] { "destinationPort", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "destinationPort", idForm, version, "0", "65", "field-normal", null });
        messages.put("destinationPort.label", "Puerto/Aeropuerto de destino");
        messages.put("destinationPort.requiredError", "Debe ingresar un Puerto/Aeropuerto de destino");
        ordinal++;

        /**** Insert field text - Lugar de destino final ****/
        formFieldsValues = new String[] { "destination", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "destination", idForm, version, "0", "65", "field-normal", null });
        messages.put("destination.label", "Lugar de destino final");
        ordinal++;

        /**** Insert field text - Descripción de mercadería y/o servicios ****/
        formFieldsValues = new String[] { "merchandiseDescription", idForm, version, "text", Integer.toString(ordinal),
                "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "merchandiseDescription", idForm, version, "0", "650", "field-big", null });
        messages.put("merchandiseDescription.label", "Descripción de mercadería y/o servicios");
        messages.put("merchandiseDescription.requiredError",
                "Debe ingresar una descripición de mercadería y/o servicios");
        ordinal++;

        /***** Insert field selector - Embarcado vía ****/
        formFieldsValues = new String[] { "shipedBy", idForm, version, "selector", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "shipedBy", idForm, version, "field-small", "1", "0", "combo" });
        messages.put("shipedBy.label", "Embarcado vía");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "shipedBy", idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "shipedBy", idForm, version, "2" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "shipedBy", idForm, version, "3" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "shipedBy", idForm, version, "4" });
        messages.put("shipedBy.option.1", "Aéreo");
        messages.put("shipedBy.option.2", "Marítimo");
        messages.put("shipedBy.option.3", "Terrestre");
        messages.put("shipedBy.option.4", "Combinado");
        ordinal++;

        /***** Insert field selector - Términos Incoterms 2010 ****/
        formFieldsValues = new String[] { "incoterms", idForm, version, "selector", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "incoterms", idForm, version, "field-medium", "EXW", "0", "combo" });
        messages.put("incoterms.label", "Términos Incoterms 2010");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "EXW" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "FCA" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "FAS" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "FOB" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "CFR" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "CIF" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "CPT" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "CIP" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "DAP" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "DAT" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "DDP" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "incoterms", idForm, version, "OTROS" });
        messages.put("incoterms.option.EXW", "EXW: aplica a todos los tipos de transporte");
        messages.put("incoterms.option.FCA", "FCA: aplica a todos los tipos de transporte");
        messages.put("incoterms.option.FAS", "FAS: aplica solo a marítimo y combinado");
        messages.put("incoterms.option.FOB", "FOB: aplica solo a marítimo y combinado");
        messages.put("incoterms.option.CFR", "CFR: aplica solo a marítimo y combinado");
        messages.put("incoterms.option.CIF", "CIF: aplica solo a marítimo y combinado");
        messages.put("incoterms.option.CPT", "CPT: aplica a todos los tipos de transporte");
        messages.put("incoterms.option.CIP", "CIP: aplica a todos los tipos de transporte");
        messages.put("incoterms.option.DAP", "DAP: aplica a todos los tipos de transporte");
        messages.put("incoterms.option.DAT", "DAT: aplica a todos los tipos de transporte");
        messages.put("incoterms.option.DDP", "DDP: aplica a todos los tipos de transporte");
        messages.put("incoterms.option.OTROS", "OTROS: aplica a todos los tipos de transporte");
        ordinal++;

        /**** Insert field text - Texto Incoterms ****/
        formFieldsValues = new String[] { "textIncoterms", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "textIncoterms", idForm, version, "0", "65", "field-normal", null });
        messages.put("textIncoterms.label", "Texto Incoterms");
        messages.put("textIncoterms.requiredError", "Debe ingresar una descripición de mercadería y/o servicios");
        ordinal++;

        /**** Insert field text - Instrucciones adicionales ****/
        formFieldsValues = new String[] { "aditionalInstructions", idForm, version, "text", Integer.toString(ordinal),
                "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "aditionalInstructions", idForm, version, "0", "500", "field-big", null });
        messages.put("aditionalInstructions.label", "Instrucciones adicionales");
        ordinal++;

        /**** Insert field text - Período para presentación de documentos ****/
        formFieldsValues = new String[] { "documentPeriod", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "documentPeriod", idForm, version, "0", "2", "field-smaller", "onlyNumbers" });
        messages.put("documentPeriod.label", "Período para presentación de documentos");
        messages.put("documentPeriod.requiredError", "Debe ingresar un período de presentación de documentos");
        ordinal++;

        /*****
         * Insert field selector - Gastos bancarios (excepto bco. emisor)
         ****/
        formFieldsValues = new String[] { "bankCharges", idForm, version, "selector", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector",
                new String[] { "id_field", "id_form", "form_version", "display_type", "default_value",
                        "show_blank_option", "render_as" },
                new String[] { "bankCharges", idForm, version, "field-small", "1", "0", "combo" });
        messages.put("bankCharges.label", "Gastos bancarios (excepto bco. emisor)");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "bankCharges", idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "bankCharges", idForm, version, "2" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" },
                new String[] { "bankCharges", idForm, version, "3" });
        messages.put("bankCharges.option.1", "Beneficiario");
        messages.put("bankCharges.option.2", "Ordenante");
        messages.put("bankCharges.option.3", "Otros");
        ordinal++;

        /**** Insert field text - Gastos bancarios (otros) ****/
        formFieldsValues = new String[] { "bankChargesOther", idForm, version, "text", Integer.toString(ordinal),
                "value(bankCharges) == '3'", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "bankChargesOther", idForm, version, "0", "150", "field-normal", null });
        messages.put("bankChargesOther.label", "Gastos bancarios (otros)");
        messages.put("bankChargesOther.requiredError", "Debe ingresar un período de presentación de documentos");
        ordinal++;

        /**** Insert field text - Adjuntos ****/
        formFieldsValues = new String[] { "attachments", idForm, version, "text", Integer.toString(ordinal), "TRUE",
                "TRUE", "default", "1", "1",
                "Recuerde adjuntar su factura proforma y póliza de seguros en caso que corresponda. Máximo 2MB" };
        insert("form_fields", new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible",
                "required", "sub_type", "visible_in_mobile", "read_only", "note" }, formFieldsValues);
        insert("form_field_text",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type",
                        "id_validation" },
                new String[] { "attachments", idForm, version, "0", "150", "field-normal", null });
        messages.put("attachments.label", "Adjuntos");
        ordinal++;

        /**** Insert field terms and conditions - Disclaimer ****/
        formFieldsValues = new String[] { "disclaimer", idForm, version, "termsandconditions",
                Integer.toString(ordinal), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field", "id_form", "form_version", "display_type",
                "show_accept_option", "show_label" },
                new String[] { "disclaimer", idForm, version, "field-big", "0", "0" });
        messages.put("disclaimer.label", "Disclaimer");
        messages.put("disclaimer.termsAndConditions",
                "Las instrucciones enviadas al Banco por usted luego de las 17 hrs. serán procesadas al siguiente día hábil bancario. Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.\nSujeto a aprobación crediticia del Banco.\nSu instrucción quedará completada una vez recibidos la totalidad de los documentos correspondientes en nuestras sucursales y se haya enviado la presente debidamente firmada.");
        ordinal++;

        /**** Insert field line - Blank Line ****/
        insert("form_fields", formFields, new String[] { "blank", idForm, version, "horizontalrule",
                String.valueOf(ordinal), "TRUE", "FALSE", "default", "1" });
        messages.put("blank.label", " ");
        ordinal++;

        /**** Insert field emaillist - Emails de notificación ****/
        insert("form_fields", formFields, new String[] { "emails", idForm, version, "emaillist",
                String.valueOf(ordinal), "TRUE", "FALSE", "default", "1" });
        insert("form_field_emaillist", new String[] { "id_field", "id_form", "form_version", "display_type" },
                new String[] { "emails", idForm, version, "field-big" });
        messages.put("emails.help",
                "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");
        messages.put("emails.label", "Emails de notificación");
        ordinal++;

        /**** Insert field textarea - Cuerpo de notificación ****/
        insert("form_fields", formFields, new String[] { "notificationBody", idForm, version, "textarea",
                Integer.toString(ordinal), "TRUE", "FALSE", "default", "1" });
        insert("form_field_textarea",
                new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" },
                new String[] { "notificationBody", idForm, version, "0", "500", "field-big" });
        messages.put("notificationBody.label", "Cuerpo de notificación");
        ordinal++;

        String[] formFieldsMessages = new String[] { "id_message", "lang", "id_field", "id_form", "form_version",
                "value", "modification_date" };

        for (String key : messages.keySet()) {
            formFieldsValues = new String[] { "fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")),
                    idForm, version, messages.get(key), date };

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE,
                        "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                                + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '"
                                + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4]
                                + "','" + formFieldsValues[5]
                                + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

    }

}