/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author danireb
 */
public class DB20170719_1503_2707 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "frequentDestination";
        String field = "accountNumber";
        String idMessage = "fields." + idForm + "." + field + ".help";
        
        delete("form_field_messages", "id_message = '" + idMessage + "'");
        
        field = "externalAccountNumber";
        idMessage = "fields." + idForm + "." + field + ".help";
        
        delete("form_field_messages", "id_message = '" + idMessage + "'");
        
        field = "foreignAccountNumber";
        idMessage = "fields." + idForm + "." + field + ".help";
        
        update("form_field_messages", new String[]{"value"}, new String[]{"Ingrese el número de cuenta o IBAN"}, "id_message = '" + idMessage + "'");
        
    }
    
}
