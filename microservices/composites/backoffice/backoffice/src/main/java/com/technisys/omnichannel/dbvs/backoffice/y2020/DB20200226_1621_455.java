/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author cristobal
 */

public class DB20200226_1621_455 extends DBVSUpdate {

    @Override
    public void up() {
        //delete code type bank no utlizados en core
        delete("form_field_bankselector_codes", "id_form = 'transferForeign' AND form_version = 1 AND id_field = 'recipientAccountCode' AND code_type in ('BLZ','CHIPS')");
        delete("form_field_bankselector_codes", "id_form = 'transferForeign' AND form_version = 1 AND id_field = 'intermediaryBank' AND code_type in ('BLZ','CHIPS')");
        //Insert new code type
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"}, new String[]{"recipientAccountCode", "transferForeign", "1", "IBAN"});
        insert("form_field_bankselector_codes", new String[]{"id_field", "id_form", "form_version", "code_type"}, new String[]{"intermediaryBank", "transferForeign", "1", "IBAN"});
    }

}