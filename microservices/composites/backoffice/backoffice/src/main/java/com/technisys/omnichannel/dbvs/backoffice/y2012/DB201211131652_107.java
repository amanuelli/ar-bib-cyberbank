/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Guillermo Rosso &lt;grosso@technisys.com&gt;
 */
public class DB201211131652_107 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_field", "value", "id_group"};
        String[] fieldValues = new String[]{"rubicon.dateFormat", "dd/MM/yyyy", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required"};
        fieldValues = new String[]{"rub.account.exportListToMulticash", "1", "1", "com.technisys.rubicon.business.accounts.activities.ExportListToMulticashActivity", "rub.product.read"};
        insert("activities", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "value", "id_group"};
        fieldValues = new String[]{"rubicon.multicash.decimalFormat", "##0.00", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "value", "id_group"};
        fieldValues = new String[]{"rubicon.multicash.decimalFormat.separator", ",", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "value", "id_group"};
        fieldValues = new String[]{"rubicon.multicash.bankKey", "Rubicon", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "value", "id_group"};
        fieldValues = new String[]{"rubicon.multicash.dateFormat", "dd.MM.yy", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

    }
}