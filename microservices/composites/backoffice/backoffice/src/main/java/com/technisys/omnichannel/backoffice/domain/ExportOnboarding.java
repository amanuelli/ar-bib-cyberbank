/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.domain;

import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.DateUtils;

/**
 *
 * @author Ignacio Ocampo
 */
public class ExportOnboarding extends Onboarding {

    private final String exportLang;

    public ExportOnboarding(Onboarding onboarding, String lang) {
        super(onboarding);

        this.exportLang = lang;
    }

    public String getDocument() {
        I18n i18n = I18nFactory.getHandler();
        return getDocumentNumber() + " (" + i18n.getMessage("documentType.label." + getDocumentType(), exportLang)
                + ", " + i18n.getMessage("country.name." + getDocumentCountry(), exportLang) + ")";
    }

    public String getCreationDateAsString() {
        return DateUtils.formatMediumDate(getCreationDate());
    }

}
