/*
 * Copyright 2018 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5401
 *
 * @author isilveira
 */
public class DB20181122_0949_5401 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='default_cap_signature'");
        insertOrUpdateConfiguration("administration.signatures.signatureLevels", "A|B|C|D|E|F|G", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"}, "null", "frontend");
        insertOrUpdateConfiguration("administration.signatures.topAmount.defaultFrequency", "daily", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"}, "daily|weekly|monthly|transaction", "frontend");
        insertActivity("administration.signatures.delete.pre", "com.technisys.omnichannel.client.activities.administration.signatures.DeleteSignaturesPreActivity", "administration.signatures", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
    }
}