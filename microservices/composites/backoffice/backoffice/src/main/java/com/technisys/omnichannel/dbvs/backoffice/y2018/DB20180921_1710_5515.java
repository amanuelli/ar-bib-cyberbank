package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5515
 *
 * @author mcheveste
 */
public class DB20180921_1710_5515 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.simple.read.permissions", "com.technisys.omnichannel.client.activities.administration.simple.ReadPermissionsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.simple.modify.permissions.preview", "com.technisys.omnichannel.client.activities.administration.simple.ModifyPermissionsPreviewActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.simple.modify.permissions", "com.technisys.omnichannel.client.activities.administration.simple.ModifyPermissionsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        deleteActivity("administration.products.list");
    }
}
