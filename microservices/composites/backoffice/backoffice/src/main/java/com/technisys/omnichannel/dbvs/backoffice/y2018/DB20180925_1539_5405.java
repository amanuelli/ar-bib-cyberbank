
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

public class DB20180925_1539_5405 extends DBVSUpdate {

    /**
     * Se ajusta el tamaño de la columna donde se guarda el tipo de documento en
     * las diferentes tablas en las que aplica Cuando corresponde se realiza un
     * drop de la foreign key y se vuelve a crear luego de la modificación
     */
    @Override
    public void up() {
            
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "ALTER TABLE onboardings MODIFY (document_type VARCHAR2(50))");
        }else{
            modifyColumn("onboardings", "document_type", new ColumnDefinition("document_type", Types.VARCHAR, 50, 0, true, null));
        }
    }
}
