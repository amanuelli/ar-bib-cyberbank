/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150727_1554_586 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("widget.transactions.maxPending", "100", ConfigurationGroup.NEGOCIO, "widgets", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("widget.transactions.availableStates", "DRAFT|PENDING|SCHEDULED", ConfigurationGroup.NEGOCIO, "widgets", new String[]{"notEmpty"});
        
        insertOrUpdateConfiguration("widget.products.deposits.statementsPerPage", "3", ConfigurationGroup.NEGOCIO, "widgets", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("widget.products.loans.statementsPerPage", "3", ConfigurationGroup.NEGOCIO, "widgets", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("widget.products.creditCards.statementsPerPage", "3", ConfigurationGroup.NEGOCIO, "widgets", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("widget.products.accounts.statementsPerPage", "3", ConfigurationGroup.NEGOCIO, "widgets", new String[]{"notEmpty", "integer"});
        
        insertOrUpdateConfiguration("accounts.export.maxStatements", "9999", ConfigurationGroup.NEGOCIO, "others", new String[]{"notEmpty", "integer"});
        
        insertOrUpdateConfiguration("client.emailValidationFormat", "^[a-zA-ZñÑ0-9._%+-]+@[a-zñÑA-Z0-9.-]+\\.(?:[a-zA-ZñÑ]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("frontend.channels.enabledFreqeuncies", "daily|monthly", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
    }
}
