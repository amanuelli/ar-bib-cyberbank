    /*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.CancelInvitationCodeData;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.TransactionRequest;
import com.opensymphony.xwork2.ActionSupport;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.convention.annotation.Results;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 */
@Results({
    @Result(name = "success", type = "json", params = {"root", "resultMap"})
})
public class DoCancel extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        TransactionRequest request = new TransactionRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.invitationCodes.cancel");

        CancelInvitationCodeData data = new CancelInvitationCodeData();

        data.setIdCode(idCode);
        data.setName(name);
        data.setDocument(document);
        data.setAccount(account);
        data.setComment(comment);

        request.setTransactionData(data);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        if (response != null && (response.getReturnCode().equals(ReturnCodes.OK) || response.getReturnCode().equals(ReturnCodes.BACKOFFICE_REQUIRES_APPROVAL))) {
            resultMap = UIUtils.generateResultMap(response);
            return SUCCESS;
        } else {
            throw new JSONException(response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    protected int idCode;
    protected String name;
    protected String document;
    protected String account;
    protected String comment;

    public void setIdCode(int idCode) {
        this.idCode = idCode;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private transient Map<String, Object> resultMap;

    public Map<String, Object> getResultMap() {
        return resultMap;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
