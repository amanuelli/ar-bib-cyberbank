/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.requests;

import com.technisys.omnichannel.core.domain.TransactionData;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Sebastian Barbosa
 */
public class CreateInvitationCodeData extends TransactionData {

    private String documentCountry;
    private String documentType;
    private String documentNumber;

    private String fullName;
    private String userLang;
    private String accessType;

    private String account;

    private String administrationScheme;
    private Integer signatureQty;

    @Override
    public String calculateAdditionalInfoToHash() {
        StringBuilder additionalInfo = new StringBuilder(super.calculateAdditionalInfoToHash());

        additionalInfo.append("-");
        additionalInfo.append(documentCountry);
        additionalInfo.append("-");
        additionalInfo.append(documentType);
        additionalInfo.append("-");
        additionalInfo.append(documentNumber);
        additionalInfo.append("-");
        additionalInfo.append(account);

        return additionalInfo.toString();
    }

    @Override
    public String getIdentifier() {
        I18n i18n = I18nFactory.getHandler();
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append(documentNumber)
                .append(" (").append(i18n.getMessage("documentType.label." + documentType, getLang()))
                .append(", ").append(i18n.getMessage("country.name." + documentCountry, getLang())).append(")");
        return strBuilder.toString();
    }
    
    @Override
    public String getShortDescription(String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<h5>Acci&oacute;n: <a href=\"javascript:showPendingAction('").append(idTransaction).append("')\"> ").append(activityName).append("</a></h5>");
        strBuilder.append("<p class='element_details'>");
        strBuilder.append("      ").append(i18n.getMessage("backoffice.invitationCodes.create.document", getLang())).append(": <em>")
                .append(documentNumber)
                .append(" (").append(i18n.getMessage("documentType.label." + documentType, getLang()))
                .append(", ").append(i18n.getMessage("country.name." + documentCountry, getLang())).append(")")
                .append("</em><br/>");
        strBuilder.append("      ").append(i18n.getMessage("backoffice.invitationCodes.create.name", getLang())).append(": <em>")
                .append(fullName).append("</em><br/>");
        strBuilder.append("      ").append(i18n.getMessage("backoffice.invitationCodes.create.account", getLang())).append(": <em>")
                .append(account).append("</em><br/>");
        strBuilder.append("      ").append(i18n.getMessage("backoffice.invitationCodes.create.accessType", getLang())).append(": <em>")
                .append(i18n.getMessage("backoffice.invitation.role." + accessType, getLang())).append("</em><br/>");

        return strBuilder.toString();
    }

    @Override
    public String getFullDescription(String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<dl class = 'element_details clearfix'>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.create.document", getLang())).append(":</dt>")
                .append("<dd>")
                .append(documentNumber)
                .append(" (").append(i18n.getMessage("documentType.label." + documentType, getLang())).append(", ")
                .append(i18n.getMessage("country.name." + documentCountry, getLang())).append(")")
                .append("</dd>");

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.create.name", getLang())).append(":</dt>")
                .append("<dd>").append(fullName).append("</dd>");

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.create.lang", getLang())).append(":</dt>")
                .append("<dd>").append(userLang).append("</dd>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.create.accessType", getLang())).append(":</dt>")
                .append("<dd>").append(i18n.getMessage("backoffice.invitation.role." + accessType, getLang())).append("</dd>");

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.create.account", getLang())).append(":</dt>")
                .append("<dd>").append(account).append("</dd>");

        if (StringUtils.isNotBlank(administrationScheme)) {
            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.create.administrationScheme", getLang())).append(":</dt>")
                    .append("<dd>").append(i18n.getMessage("backoffice.environments.schemes." + administrationScheme, getLang())).append("</dd>");
        }
        if (signatureQty != null && signatureQty > 0) {
            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.create.signatureScheme", getLang())).append(":</dt>")
                    .append("<dd>").append(i18n.getMessage("backoffice.invitationCodes.signatureScheme." + (signatureQty > 1 ? "joint" : "indistinct"), getLang())).append("</dd>");
        }
        strBuilder.append("</dl>");

        return strBuilder.toString();
    }

    public String getDocumentCountry() {
        return documentCountry;
    }

    public void setDocumentCountry(String documentCountry) {
        this.documentCountry = documentCountry;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAdministrationScheme() {
        return administrationScheme;
    }

    public void setAdministrationScheme(String administrationScheme) {
        this.administrationScheme = administrationScheme;
    }

    public Integer getSignatureQty() {
        return signatureQty;
    }

    public void setSignatureQty(Integer signatureQty) {
        this.signatureQty = signatureQty;
    }

    public String getUserLang() {
        return userLang;
    }

    public void setUserLang(String userLang) {
        this.userLang = userLang;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

}
