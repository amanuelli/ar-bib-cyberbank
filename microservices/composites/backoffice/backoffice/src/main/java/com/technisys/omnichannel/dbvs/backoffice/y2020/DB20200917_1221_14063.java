/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author cristobal
 */

public class DB20200917_1221_14063 extends DBVSUpdate {

    @Override
    public void up() {


        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        String idForm = "";
        String version = "";
        String idField = "";

        //TRANSFER FOREIGN
        idForm = "transferForeign";
        version = "1";
        idField = "prePaymentDisclosure";

        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsValues = new String[]{idField, idForm, version, "filelink", "20", "TRUE", "FALSE", "default", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_filelink", new String[]{"id_field", "id_form", "form_version", "display_type", "show_label"}, new String[]{idField, idForm, version, "field-normal", "0"});


        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE},
                    "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  " +
                            "VALUES ('fields." + idForm + "." + idField + ".hint', 'en', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Pre Payment disclosure (This is not a Receipt)')");

            customSentence(new String[]{DBVS.DIALECT_ORACLE},
                    "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  " +
                            "VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '" + idField + "','" + idForm + "','" + version
                            + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Divulgación de pago previo (esto no es un recibo)')");

            customSentence(new String[]{DBVS.DIALECT_ORACLE},
                    "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  " +
                            "VALUES ('fields." + idForm + "." + idField + ".hint', 'pt', '" + idField + "','" + idForm + "','" + version
                            + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Divulgação de pré-pagamento (isto não é um recibo)')");

            executeScript(DBVS.DIALECT_ORACLE, "database/oracle-14063-client-transfer-international-field.sql", ";", "UTF-8");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "en", idField, idForm, version, "Pre Payment disclosure (This is not a Receipt)", date});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, "Divulgación de pago previo (esto no es un recibo)')", date});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "pt", idField, idForm, version, "Divulgação de pré-pagamento (isto não é um recibo)'", date});
            executeScript(DBVS.DIALECT_MSSQL, "database/mssql-14063-client-transfer-international-field.sql", ";", "UTF-8");
        }

    }

}