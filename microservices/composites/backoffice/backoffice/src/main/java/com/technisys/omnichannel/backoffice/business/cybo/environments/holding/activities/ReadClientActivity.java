/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.environments.holding.activities;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.cybo.MapResponse;
import com.technisys.omnichannel.backoffice.business.cybo.environments.holding.requests.ReadClientRequest;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

public class ReadClientActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        MapResponse response = new MapResponse(request);
        
        try {
            ReadClientRequest auxRequest = (ReadClientRequest) request;

            ClientEnvironment clientEnv = RubiconCoreConnectorC.readClient(request.getIdTransaction(), auxRequest.getIdClient());

            response.putItem("id", clientEnv.getProductGroupId());
            response.putItem("name", clientEnv.getAccountName());
            response.putItem("segment", clientEnv.getSegment());
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.UNEXPECTED_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        ReadClientRequest auxRequest = (ReadClientRequest) request;

        if (StringUtils.isEmpty(auxRequest.getIdClient())) {
            result.put("@environment", "backoffice.environments.holdings.create.readClient.empty");

            return result;
        }
        if (!StringUtils.isNumeric(auxRequest.getIdClient())) { // in our case, the corebanking simulator just admit numbers, this validation depend projects
            result.put("@environment", "backoffice.environments.holdings.create.readClient.invalidValue");

            return result;
        }

        try {
            ClientEnvironment clientEnv = RubiconCoreConnectorC.readClient(request.getIdTransaction(), auxRequest.getIdClient());

            if (clientEnv == null) {
                result.put("@environment", "backoffice.environments.holdings.create.readClient.notFound");

                return result;
            }
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.UNEXPECTED_ERROR, ex);
        }

        return result;
    }
}
