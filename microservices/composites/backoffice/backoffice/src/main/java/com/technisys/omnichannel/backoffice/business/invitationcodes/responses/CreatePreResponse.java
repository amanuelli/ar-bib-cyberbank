/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.responses;

import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.domain.Country;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sebastian Barbosa
 */
public class CreatePreResponse extends IBResponse {

    private List<Country> countryList;
    private List<String> documentTypeList;
    private String defaultCountry;
    private String defaultDocumentType;
    private List<String> adminSchemeList;

    public CreatePreResponse(IBRequest request) {
        super(request);

        countryList = new ArrayList<>();
        documentTypeList = new ArrayList<>();
        adminSchemeList = new ArrayList<>();
    }

    public List<Country> getCountryList() {
        return countryList;
    }

    public void setCountryList(List<Country> countryList) {
        this.countryList = countryList;
    }

    public void addCountry(Country country) {
        this.countryList.add(country);
    }

    public List<String> getDocumentTypeList() {
        return documentTypeList;
    }

    public void setDocumentTypeList(List<String> documentTypeList) {
        this.documentTypeList = documentTypeList;
    }

    public void addDocumentType(String documentType) {
        this.documentTypeList.add(documentType);
    }

    public String getDefaultCountry() {
        return defaultCountry;
    }

    public void setDefaultCountry(String defaultCountry) {
        this.defaultCountry = defaultCountry;
    }

    public String getDefaultDocumentType() {
        return defaultDocumentType;
    }

    public void setDefaultDocumentType(String defaultDocumentType) {
        this.defaultDocumentType = defaultDocumentType;
    }

    public List<String> getAdminSchemeList() {
        return adminSchemeList;
    }

    public void setAdminSchemeList(List<String> adminSchemeList) {
        this.adminSchemeList = adminSchemeList;
    }

    public void addAdminScheme(String adminScheme) {
        this.adminSchemeList.add(adminScheme);
    }

}
