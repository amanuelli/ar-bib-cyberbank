/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.responses;

import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.domain.Environment;

/**
 *
 * @author Sebastian Barbosa
 */
public class ReadEnvironmentResponse extends IBResponse {

    private Environment environment;
    private ClientEnvironment clientEnvironment;

    public ReadEnvironmentResponse(IBRequest request) {
        super(request);
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public ClientEnvironment getClientEnvironment() {
        return clientEnvironment;
    }

    public void setClientEnvironment(ClientEnvironment clientEnvironment) {
        this.clientEnvironment = clientEnvironment;
    }

    @Override
    public String toString() {
        return "FindEnvironmentResponse{" + "environment=" + environment + ", clientEnvironment=" + clientEnvironment + '}';
    }

}
