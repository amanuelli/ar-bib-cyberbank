/* 
 * Copyright 2021 Technisys.
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities;

import java.util.Arrays;
import java.util.List;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.cybo.Response;
import com.technisys.omnichannel.backoffice.domain.cybo.Environment;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

/**
 * 
 * @author azeballos
 *
 */
public class ListSchemasActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        Response<List<String>> response = new Response<>(request);
        
        List<String> data = Arrays.asList(new String[]{Environment.ADMINISTRATION_SCHEME_SIMPLE, Environment.ADMINISTRATION_SCHEME_MEDIUM, Environment.ADMINISTRATION_SCHEME_ADVANCED});
        response.setData(data);
        response.setReturnCode(ReturnCodes.OK);

        return response;
    }

}
