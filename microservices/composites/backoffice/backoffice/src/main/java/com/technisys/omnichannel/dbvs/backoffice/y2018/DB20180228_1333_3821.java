/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-3821
 *
 * @author isilveira
 */
public class DB20180228_1333_3821 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("preferences.environmentandlang.modify.pre");
        deleteActivity("preferences.environmentandlang.modify.preview");
        deleteActivity("preferences.environmentandlang.modify.send");
        insertActivity("preferences.lang.modify", "com.technisys.omnichannel.client.activities.preferences.lang.ModifyLangActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
    }
}