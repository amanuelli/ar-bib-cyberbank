/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.productrequest.response;

import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import java.util.List;

/**
 *
 * @author iocampo
 */
public class ReadResponse extends IBResponse {

    private Onboarding onboarding;
    private List<String> documents;

    public ReadResponse(IBRequest request) {
        super(request);
    }

    public Onboarding getOnboarding() {
        return onboarding;
    }

    public void setOnboarding(Onboarding onboarding) {
        this.onboarding = onboarding;
    }

    public List<String> getDocuments() {
        return documents;
    }

    public void setDocuments(List<String> documents) {
        this.documents = documents;
    }

    @Override
    public String toString() {
        return "ReadResponse{" + "Onboarding=" + onboarding + '}';
    }
}
