/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2884
 *
 * @author mcommand
 */
public class DB20170803_1040_2884 extends DBVSUpdate {

    @Override
    public void up() {
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        Map<String, String> formMessagesEN = new HashMap();
        Map<String, String> formFieldMessagesEN = new HashMap();
        
        String version = "1";

        formMessagesEN.put("forms.authorizeDiscrepancyDocuments.formName", "Authorization to send documents with discrepancy");
        formMessagesEN.put("forms.buyDocuments.formName", "Purchase of documents");
        formMessagesEN.put("forms.creditLetterDocumentPresentation.formName", "Presentation of documents under letter of credit");
        formMessagesEN.put("forms.frequentDestination.formName", "Frequent destinations");
        formMessagesEN.put("forms.letterCredit.formName", "Submission of Letter of Credit Documents");
        formMessagesEN.put("forms.modifyCreditLetter.formName", "Modification of letter of credit");
        formMessagesEN.put("forms.openCreditLetter.formName", "Opening of letter of credit");
        formMessagesEN.put("forms.presentationOfDocumentsUnderCollection.formName", "Presentation of documents under collection");
        formMessagesEN.put("forms.reissueCreditCard.formName", "Request for credit card reprint");
        formMessagesEN.put("forms.requestEndorsement.formName", "Request for transport document endorsement");
        formMessagesEN.put("forms.requestLoan.formName", "Loan application");
        formMessagesEN.put("forms.requestTransactionCancellation.formName", "Request for cancellation of transaction");
        formMessagesEN.put("forms.surveyOfDiscrepancies.formName", "Survey of discrepancies");
        formMessagesEN.put("forms.transferLocal.formName", "Interbank transfer");

        
        formMessagesEN.keySet().forEach((key) -> {
            delete("form_messages", "id_message = '" + key + "' and lang = 'en'");
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, 
                     "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + "VALUES ('" + key + "', '" + key.split("\\.")[1] + "', '" + version + "', 'en', '" + formMessagesEN.get(key) + "', TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + "VALUES ('" + key + "', '" + key.split("\\.")[1] + "', '" + version + "', 'en', '" + formMessagesEN.get(key) + "', '" + date + "')");                
            }
        });

        formFieldMessagesEN.put("fields.additionalCreditCardRequest.amount.help", "Enter the amount of the transaction, consider using the comma as a decimal separator");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.bill.label", "Invoice Number");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.bill.requiredError", "You must enter an invoice number");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.billAmount.label", "Amount of invoice");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.billAmount.requiredError", "Invalid use amount");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.disclaimer.label", "Disclaimer");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.disclaimer.termsAndConditions", "The instructions sent to the bank by you after the . Will be processed on the next bank business day. I authorize to debit from my account the commissions that this instruction may generate.");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.discrepancies.help", "Enter the discrepancies here");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.discrepancies.label", "Discrepancies");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.discrepancies.requiredError", "You must enter at least one discrepancy");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.emaillist.label", "Notification Emails");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.notification.label", "Notification body");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.operationNumber.label", "No. of operation");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.operationNumber.requiredError", "You must enter an Operation Number");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.operationType.label", "Type of operation");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.operationType.option.aprobation", "Shipping for approval and payment");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.operationType.option.discrepanciesFound", "Shipping with discrepancies found by Uds");
        formFieldMessagesEN.put("fields.authorizeDiscrepancyDocuments.operationType.option.discrepanciesNext", "Shipping with the following discrepancies");
        formFieldMessagesEN.put("fields.buyDocuments.amount.label", "Amount");
        formFieldMessagesEN.put("fields.buyDocuments.amount.requiredError", "The amount is not valid");
        formFieldMessagesEN.put("fields.buyDocuments.attachments.label", "Attachments");
        formFieldMessagesEN.put("fields.buyDocuments.disclaimer.label", "Disclaimer");
        formFieldMessagesEN.put("fields.buyDocuments.disclaimer.showAcceptOptionText", "To accept");
        formFieldMessagesEN.put("fields.buyDocuments.disclaimer.termsAndConditions", "Subject to credit approval from the bank. Instructions sent to the bank by you after the will be processed on the next bank business day. I authorize to debit from my account the commissions that this instruction may generate.");
        formFieldMessagesEN.put("fields.buyDocuments.interestRate.label", "Interest rate");
        formFieldMessagesEN.put("fields.buyDocuments.letterCredit.help", "Select the letter of credit you wish to discount");
        formFieldMessagesEN.put("fields.buyDocuments.letterCredit.label", "Letter of credit");
        formFieldMessagesEN.put("fields.buyDocuments.letterCredit.requiredError", "You must enter a letter of credit");
        formFieldMessagesEN.put("fields.buyDocuments.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.buyDocuments.notificationEmails.help", "E-mails to notify the transfer of the transfer. Remember to enter them separated by commas.");
        formFieldMessagesEN.put("fields.buyDocuments.notificationEmails.label", "Notification Emails");
        formFieldMessagesEN.put("fields.buyDocuments.observations.label", "Observations");
        formFieldMessagesEN.put("fields.buyDocuments.withResource.label", "Means");
        formFieldMessagesEN.put("fields.buyDocuments.withResource.option.resource", "Resource");
        formFieldMessagesEN.put("fields.cashAdvance.amount.help", "Enter the amount of the transaction, consider using the comma as a decimal separator");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.amount.label", "Amount");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.amount.requiredError", "The amount is not valid");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.amountOfUse.help", "This is the amount for which the bank will claim payment of the documents submitted under the reference LC");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.amountOfUse.invalidError", "Invalid use amount");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.amountOfUse.label", "Amount of use");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.attachment.label", "Attachment");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.attachment.option.Adjunto1", "Attachment 1");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.attachment.option.Adjunto2", "Attachment 2");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.attachment.option.Adjunto3", "Attachment 3");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.attachment.requiredError", "You must enter an attachment");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.billNumber.label", "Invoice Number");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.billNumber.requiredError", "You must enter an invoice number");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.courierAccountNumber.help", "Enter your account number");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.courierAccountNumber.label", "Courier account number");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.courierName.help", "Enter the name of the courier with whom you have an account");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.courierName.label", "Courier name");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.creditLetter.label", "Letter of Credit held by the bank");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.creditLetter.option.NO", "No");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.creditLetter.option.SI", "Yes");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.creditLetterNumber.label", "Letter of credit number");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.creditLetterNumber.requiredError", "You must enter a Letter of Credit");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.disclaimer.label", "Disclaimer");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.disclaimer.termsAndConditions", "The instructions sent to the bank by you after the will be processed on the next bank business day. I authorize to debit the commissions that this instruction can generate. Your instruction will be completed once all the corresponding documents have been received in our branches and the present form has been signed.");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.document.label", "Document");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.document.option.Documento1", "Document 1");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.document.option.Documento2", "Document 2");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.document.option.Documento3", "Document 3");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.document.requiredError", "You must enter a document");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.notificationEmails.help", "E-mails to notify the transfer. Remember to enter them separated by commas.");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.notificationEmails.label", "Notification Emails");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.observations.label", "Observations");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.operation.label", "Operation");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.operation.option.Operacion1", "Operation 1");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.operation.option.Operacion2", "Operation 2");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.operation.option.Operacion3", "Operation 3");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.operation.requiredError", "You must enter an Operation");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.shipDiscrepancies.label", "Ship discrepancies");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.shipDiscrepancies.option.NO", "No");
        formFieldMessagesEN.put("fields.creditLetterDocumentPresentation.shipDiscrepancies.option.SI", "Yes");
        formFieldMessagesEN.put("fields.frequentDestination.accountNumber.label", "Account number");
        formFieldMessagesEN.put("fields.frequentDestination.accountNumber.requiredError", "You must enter an account number");
        formFieldMessagesEN.put("fields.frequentDestination.creditCardNumber.label", "Credit card number");
        formFieldMessagesEN.put("fields.frequentDestination.creditCardNumber.requiredError", "You must enter a credit card number");
        formFieldMessagesEN.put("fields.frequentDestination.exteriorBank.label", "Destination foreign bank");
        formFieldMessagesEN.put("fields.frequentDestination.externalAccountNumber.label", "Account number");
        formFieldMessagesEN.put("fields.frequentDestination.externalAccountNumber.requiredError", "You must enter an account number");
        formFieldMessagesEN.put("fields.frequentDestination.foreignAccountNumber.help", "Enter the account number or IBAN");
        formFieldMessagesEN.put("fields.frequentDestination.foreignAccountNumber.label", "Account number");
        formFieldMessagesEN.put("fields.frequentDestination.foreignAccountNumber.requiredError", "You must enter an account number");
        formFieldMessagesEN.put("fields.frequentDestination.loanNumber.label", "Loan number");
        formFieldMessagesEN.put("fields.frequentDestination.loanNumber.requiredError", "You must enter a loan number");
        formFieldMessagesEN.put("fields.frequentDestination.localBank.label", "Bank destination destination");
        formFieldMessagesEN.put("fields.frequentDestination.modal.label", "It''s modal");
        formFieldMessagesEN.put("fields.frequentDestination.name.help", "Text that allows you to identify the target beneficiary");
        formFieldMessagesEN.put("fields.frequentDestination.name.label", "Name");
        formFieldMessagesEN.put("fields.frequentDestination.name.requiredError", "You must enter a name");
        formFieldMessagesEN.put("fields.frequentDestination.productType.label", "Type of product");
        formFieldMessagesEN.put("fields.frequentDestination.productType.option.account", "Account");
        formFieldMessagesEN.put("fields.frequentDestination.productType.option.creditCard", "Credit card");
        formFieldMessagesEN.put("fields.frequentDestination.productType.option.externalAccount", "Another bank account");
        formFieldMessagesEN.put("fields.frequentDestination.productType.option.externalCreditCard", "Credit card other bank");
        formFieldMessagesEN.put("fields.frequentDestination.productType.option.foreignAccount", "Foreign bank account");
        formFieldMessagesEN.put("fields.frequentDestination.productType.option.loan", "Loan");
        formFieldMessagesEN.put("fields.frequentDestination.productType.requiredError", "You must select a product type");
        formFieldMessagesEN.put("fields.frequentDestination.recipientAddress.label", "Beneficiary Address");
        formFieldMessagesEN.put("fields.frequentDestination.recipientName.label", "Name of beneficiary");
        formFieldMessagesEN.put("fields.frequentDestination.recipientName.requiredError", "You must enter the name of the beneficiary person or company");
        formFieldMessagesEN.put("fields.frequentDestination.saveData.label", "Save Frequent Destination");
        formFieldMessagesEN.put("fields.frequentDestination.saveData.option.yes", "YES");
        formFieldMessagesEN.put("fields.frequentDestination.saveData.requiredError", "You must select an option");
        formFieldMessagesEN.put("fields.letterCredit.amountOfUse.help", "This is the amount for which the bank will claim payment of the documents submitted under the reference");
        formFieldMessagesEN.put("fields.letterCredit.amountOfUse.label", "Amount of use");
        formFieldMessagesEN.put("fields.letterCredit.amountOfUse.requiredError", "Invalid amount");
        formFieldMessagesEN.put("fields.letterCredit.attachments.label", "Attachments");
        formFieldMessagesEN.put("fields.letterCredit.billNumber.label", "Invoice Number");
        formFieldMessagesEN.put("fields.letterCredit.billNumber.requiredError", "You must enter an invoice number");
        formFieldMessagesEN.put("fields.letterCredit.disclaimer.label", "Disclaimer");
        formFieldMessagesEN.put("fields.letterCredit.discrepancies.label", "Discrepancies");
        formFieldMessagesEN.put("fields.letterCredit.discrepancies.option.DIS1", "Discrepancies 1");
        formFieldMessagesEN.put("fields.letterCredit.discrepancies.option.DIS2", "Discrepancies 2");
        formFieldMessagesEN.put("fields.letterCredit.discrepancies.option.DIS3", "Discrepancies 3");
        formFieldMessagesEN.put("fields.letterCredit.executionDate.label", "Date of execution");
        formFieldMessagesEN.put("fields.letterCredit.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.letterCredit.notificationEmails.help", "E-mails to notify the transfer. Remember to enter them separated by commas.");
        formFieldMessagesEN.put("fields.letterCredit.notificationEmails.label", "Notification Emails");
        formFieldMessagesEN.put("fields.letterCredit.observations.label", "Observations");
        formFieldMessagesEN.put("fields.letterCredit.operationNumber.help", "Select the operation for which you want to submit documents");
        formFieldMessagesEN.put("fields.letterCredit.operationNumber.label", "No. of operation");
        formFieldMessagesEN.put("fields.letterCredit.operationNumber.option.OP1", "Operation 1");
        formFieldMessagesEN.put("fields.letterCredit.operationNumber.option.OP2", "Operation 2");
        formFieldMessagesEN.put("fields.letterCredit.operationNumber.option.OP3", "Operation 3");
        formFieldMessagesEN.put("fields.letterCredit.operationNumber.requiredError", "You must enter an Operation Number");
        formFieldMessagesEN.put("fields.letterCredit.requiredDocuments.label", "Required Documents");
        formFieldMessagesEN.put("fields.letterCredit.requiredDocuments.requiredError", "You must indicate at least one document");
        formFieldMessagesEN.put("fields.mobilePrepaiment.amount.help", "Enter the amount of the transaction, consider using the comma as a decimal separator");
        formFieldMessagesEN.put("fields.modifyCreditLetter.amount.label", "Purchase Limit");
        formFieldMessagesEN.put("fields.modifyCreditLetter.amount.requiredError", "You must enter an amount greater than zero");
        formFieldMessagesEN.put("fields.modifyCreditLetter.attachments.label", "Attachments");
        formFieldMessagesEN.put("fields.modifyCreditLetter.creditLetter.label", "Letter of credit");
        formFieldMessagesEN.put("fields.modifyCreditLetter.creditLetter.requiredError", "You must enter a letter of credit");
        formFieldMessagesEN.put("fields.modifyCreditLetter.disclaimer.label", "Disclaimer");
        formFieldMessagesEN.put("fields.modifyCreditLetter.disclaimer.requiredError", "You must accept the terms and conditions to continue");
        formFieldMessagesEN.put("fields.modifyCreditLetter.disclaimer.showAcceptOptionText", "Accept");
        formFieldMessagesEN.put("fields.modifyCreditLetter.disclaimer.termsAndConditions", "Instructions sent to the bank by you after 6pm will be processed on the next bank business day. I authorize to debit from my account the commissions that this instruction can generate. Subject to bank credit approval. Your instruction will be completed once all the corresponding documents have been received in our branches and has been signed.");
        formFieldMessagesEN.put("fields.modifyCreditLetter.dueDate.label", "New expiration date");
        formFieldMessagesEN.put("fields.modifyCreditLetter.increaseDecrease.label", "Increase / Decrease");
        formFieldMessagesEN.put("fields.modifyCreditLetter.increaseDecrease.option.disminucion", "Decrease");
        formFieldMessagesEN.put("fields.modifyCreditLetter.increaseDecrease.option.incremento", "Increase");
        formFieldMessagesEN.put("fields.modifyCreditLetter.note.label", "Note");
        formFieldMessagesEN.put("fields.modifyCreditLetter.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.modifyCreditLetter.notificationEmails.help", "E-mails to notify the transfer of the transfer. Remember to enter them separated by commas.");
        formFieldMessagesEN.put("fields.modifyCreditLetter.notificationEmails.label", "Notification Emails");
        formFieldMessagesEN.put("fields.modifyCreditLetter.observation.label", "Observations");
        formFieldMessagesEN.put("fields.modifyCreditLetter.otherModifications.label", "Other modifications");
        formFieldMessagesEN.put("fields.modifyCreditLetter.selectCreditLetter.label", "Letter of credit");
        formFieldMessagesEN.put("fields.modifyCreditLetter.selectCreditLetter.option.carta_1", "Letter 1");
        formFieldMessagesEN.put("fields.modifyCreditLetter.selectCreditLetter.option.carta_2", "Letter 2");
        formFieldMessagesEN.put("fields.modifyCreditLetter.selectCreditLetter.option.carta_3", "Letter 3");
        formFieldMessagesEN.put("fields.modifyCreditLetter.shippingDate.label", "New date of shipment");
        formFieldMessagesEN.put("fields.openCreditLetter.addressInformantBank.label", "Address of teller bank");
        formFieldMessagesEN.put("fields.openCreditLetter.aditionalInstructions.label", "Additional Instructions");
        formFieldMessagesEN.put("fields.openCreditLetter.amount.label", "Amount");
        formFieldMessagesEN.put("fields.openCreditLetter.amountTolerance.label", "Tolerance in amount");
        formFieldMessagesEN.put("fields.openCreditLetter.amountTolerance.option.0", "Without tolerance");
        formFieldMessagesEN.put("fields.openCreditLetter.attachments.label", "Attachments");
        formFieldMessagesEN.put("fields.openCreditLetter.available.label", "Available as follows");
        formFieldMessagesEN.put("fields.openCreditLetter.available.option.1", "Pay per view");
        formFieldMessagesEN.put("fields.openCreditLetter.available.option.2", "Term");
        formFieldMessagesEN.put("fields.openCreditLetter.available.option.3", "Mixed");
        formFieldMessagesEN.put("fields.openCreditLetter.bankCharges.label", "Bank charges (except issuing bank)");
        formFieldMessagesEN.put("fields.openCreditLetter.bankCharges.option.1", "Beneficiary");
        formFieldMessagesEN.put("fields.openCreditLetter.bankCharges.option.2", "Payer");
        formFieldMessagesEN.put("fields.openCreditLetter.bankCharges.option.3", "Others");
        formFieldMessagesEN.put("fields.openCreditLetter.bankChargesOther.label", "Bank charges (other)");
        formFieldMessagesEN.put("fields.openCreditLetter.bankChargesOther.requiredError", "You must enter a document submission period");
        formFieldMessagesEN.put("fields.openCreditLetter.beneficiaryAccountNumber.label", "Beneficiary account number");
        formFieldMessagesEN.put("fields.openCreditLetter.beneficiaryData.label", "Name and address of beneficiary");
        formFieldMessagesEN.put("fields.openCreditLetter.beneficiaryData.requiredError", "You must enter the name and address of the beneficiary");
        formFieldMessagesEN.put("fields.openCreditLetter.bicInformantBank.label", "BIC Teller bank");
        formFieldMessagesEN.put("fields.openCreditLetter.bicInformantBank.requiredError", "You must enter the BIC code of the warning bank");
        formFieldMessagesEN.put("fields.openCreditLetter.boardingPeriod.help", "Enter the shipping schedule here");
        formFieldMessagesEN.put("fields.openCreditLetter.boardingPeriod.label", "Boarding period");
        formFieldMessagesEN.put("fields.openCreditLetter.boardingPort.label", "Port / Boarding Airport");
        formFieldMessagesEN.put("fields.openCreditLetter.boardingPort.requiredError", "You must enter a Port / Aeroteerto of shipment");
        formFieldMessagesEN.put("fields.openCreditLetter.deliveryPlace.label", "Place of delivery of the merchandise");
        formFieldMessagesEN.put("fields.openCreditLetter.description.label", "Description");
        formFieldMessagesEN.put("fields.openCreditLetter.destination.label", "Final destination");
        formFieldMessagesEN.put("fields.openCreditLetter.destinationPort.label", "Port / Destination Airport");
        formFieldMessagesEN.put("fields.openCreditLetter.destinationPort.requiredError", "You must enter a Destination Port / Airport");
        formFieldMessagesEN.put("fields.openCreditLetter.disclaimer.label", "Disclaimer");
        formFieldMessagesEN.put("fields.openCreditLetter.disclaimer.termsAndConditions", "The instructions sent to the Bank by you after 17 hrs. Will be processed the next bank business day. I authorize to debit the commissions that this instruction can generate. Subject to credit approval of the Bank. Your instruction will be completed once all the corresponding documents have been received in our branches and the present document duly signed has been sent.");
        formFieldMessagesEN.put("fields.openCreditLetter.documentPeriod.label", "Period for presentation of documents");
        formFieldMessagesEN.put("fields.openCreditLetter.documentPeriod.requiredError", "You must enter a document submission period");
        formFieldMessagesEN.put("fields.openCreditLetter.dueDate.label", "Due date");
        formFieldMessagesEN.put("fields.openCreditLetter.emails.help", "E-mails to notify the transfer of the transfer. Remember to enter them separated by commas.");
        formFieldMessagesEN.put("fields.openCreditLetter.emails.label", "Notification Emails");
        formFieldMessagesEN.put("fields.openCreditLetter.expirationPlace.help", "Place of presentation of documents");
        formFieldMessagesEN.put("fields.openCreditLetter.expirationPlace.label", "Place of expiration");
        formFieldMessagesEN.put("fields.openCreditLetter.expirationPlace.requiredError", "You must enter a place of expiration");
        formFieldMessagesEN.put("fields.openCreditLetter.from.label", "Since");
        formFieldMessagesEN.put("fields.openCreditLetter.from.option.1", "Shipping date");
        formFieldMessagesEN.put("fields.openCreditLetter.from.option.2", "Invoice date");
        formFieldMessagesEN.put("fields.openCreditLetter.from.option.3", "View");
        formFieldMessagesEN.put("fields.openCreditLetter.from.option.4", "Other");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.label", "Incoterms Terms 2010");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.CFR", "CFR: applies only to marine and combined");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.CIF", "CIF: applies only to maritime and combined");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.CIP", "CIP: applies to all types of transport");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.CPT", "CPT: applies to all types of transportation");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.DAP", "DAP: applies to all types of transport");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.DAT", "DAT: applies to all types of transport");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.DDP", "DDP: applies to all types of transport");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.EXW", "EXW: applies to all types of transport");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.FAS", "FAS: applies only to maritime and combined");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.FCA", "FCA: applies to all types of transport");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.FOB", "FOB: applies only to maritime and combined");
        formFieldMessagesEN.put("fields.openCreditLetter.incoterms.option.OTROS", "OTHERS: applies to all types of transport");
        formFieldMessagesEN.put("fields.openCreditLetter.max.label", "Max");
        formFieldMessagesEN.put("fields.openCreditLetter.merchandiseDescription.label", "Description of merchandise and / or services");
        formFieldMessagesEN.put("fields.openCreditLetter.merchandiseDescription.requiredError", "You must enter a description of merchandise and / or services");
        formFieldMessagesEN.put("fields.openCreditLetter.merchandiseTolerance.label", "This tolerance applies also to the merchandise");
        formFieldMessagesEN.put("fields.openCreditLetter.merchandiseTolerance.option.0", "Yes");
        formFieldMessagesEN.put("fields.openCreditLetter.min.label", "Min");
        formFieldMessagesEN.put("fields.openCreditLetter.nameInformantBank.label", "Bank teller name");
        formFieldMessagesEN.put("fields.openCreditLetter.nameInformantBank.requiredError", "You must enter the name of the teller bank");
        formFieldMessagesEN.put("fields.openCreditLetter.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.openCreditLetter.numberDays.label", "Number of days");
        formFieldMessagesEN.put("fields.openCreditLetter.numberDays.requiredError", "You must enter the number of days");
        formFieldMessagesEN.put("fields.openCreditLetter.partialShipments.label", "Partial shipments");
        formFieldMessagesEN.put("fields.openCreditLetter.partialShipments.option.1", "Allowed");
        formFieldMessagesEN.put("fields.openCreditLetter.partialShipments.option.2", "Forbidden");
        formFieldMessagesEN.put("fields.openCreditLetter.payerAddress.label", "Address of the payer");
        formFieldMessagesEN.put("fields.openCreditLetter.payerAddress.requiredError", "You must enter the originator''s address");
        formFieldMessagesEN.put("fields.openCreditLetter.payerName.label", "Name of the payer");
        formFieldMessagesEN.put("fields.openCreditLetter.requiresChange.label", "Requires bill of exchange");
        formFieldMessagesEN.put("fields.openCreditLetter.requiresChange.option.0", "Yes");
        formFieldMessagesEN.put("fields.openCreditLetter.shipedBy.label", "Shipped via");
        formFieldMessagesEN.put("fields.openCreditLetter.shipedBy.option.1", "by air");
        formFieldMessagesEN.put("fields.openCreditLetter.shipedBy.option.2", "Maritime");
        formFieldMessagesEN.put("fields.openCreditLetter.shipedBy.option.3", "Land");
        formFieldMessagesEN.put("fields.openCreditLetter.shipedBy.option.4", "Combined");
        formFieldMessagesEN.put("fields.openCreditLetter.shipmentDate.label", "Shipment due date");
        formFieldMessagesEN.put("fields.openCreditLetter.term.label", "Term");
        formFieldMessagesEN.put("fields.openCreditLetter.term.requiredError", "You must enter a term");
        formFieldMessagesEN.put("fields.openCreditLetter.textIncoterms.label", "Text Incoterms");
        formFieldMessagesEN.put("fields.openCreditLetter.textIncoterms.requiredError", "You must enter a description of merchandise and / or services");
        formFieldMessagesEN.put("fields.openCreditLetter.transfers.label", "Transfers");
        formFieldMessagesEN.put("fields.openCreditLetter.transfers.option.1", "Allowed");
        formFieldMessagesEN.put("fields.openCreditLetter.transfers.option.2", "Forbidden");
        formFieldMessagesEN.put("fields.payCreditCard.amount.help", "Enter the amount of the transaction, consider using the comma as a decimal separator");
        formFieldMessagesEN.put("fields.payLoan.commission.label", "commissions");
        formFieldMessagesEN.put("fields.payLoan.debitAmount.label", "Total amount charged");
        formFieldMessagesEN.put("fields.payLoan.loan.requiredError", "You must enter a loan number");
        formFieldMessagesEN.put("fields.payLoan.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.payLoan.notificationEmails.help", "E-mails to notify the transfer.");
        formFieldMessagesEN.put("fields.payLoan.notificationEmails.label", "Notification Emails");
        formFieldMessagesEN.put("fields.payLoan.otherLoanAmount.help", "Enter the amount of the transaction, consider using the comma as a decimal separator");
        formFieldMessagesEN.put("fields.payLoan.otherLoanAmount.requiredError", "You must enter an amount");
        formFieldMessagesEN.put("fields.payLoan.rate.label", "Applied quote");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.amountOfCollection.invalidError", "The amount is not valid");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.amountOfCollection.label", "Amount of collection");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.amountOfCollection.placeholder", "Amount of collection");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.conditionsAgainstDelivery.label", "Conditions against delivery");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.conditionsAgainstDelivery.placeholder", "Conditions against delivery");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.conditionsAgainstDelivery.requiredError", "You must enter a value");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.courierAccountNumber.help", "Enter your account number");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.courierAccountNumber.label", "Courier account number");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.courierAccountNumber.placeholder", "Courier account number");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.courierName.help", "Enter the name of the courier with whom you have an account");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.courierName.label", "Courier name");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.courierName.placeholder", "Courier name");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.deliveryDocsAgainst.label", "Delivery docs. against");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.deliveryDocsAgainst.option.acceptanceOfLetter", "Acceptance of letter");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.deliveryDocsAgainst.option.payment", "Payment");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.deliveryDocsAgainst.placeholder", "Delivery docs. against");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.disclaimer.termsAndConditions", "Instructions sent to the bank by you after 18:00. Will be processed the next bank business day. I authorize to debit from my account the commissions that this instruction can generate. Your instruction will be completed once all the corresponding documents have been received in our branches and the present one duly signed has been sent.");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.expensesAbroad.label", "Expenses abroad");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.expensesAbroad.option.onAccountOfTheRotated", "On account of the rotated");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.expensesAbroad.option.onOurBehalf", "On our behalf");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.expensesAbroad.placeholder", "Expenses abroad");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.instructionsProtesto.label", "Protest Instructions");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.instructionsProtesto.option.forNonPaymentAndNonAcceptance", "For non-payment and non-acceptance");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.instructionsProtesto.option.notAccepted", "Not accepting");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.instructionsProtesto.option.notPaid", "Not paid");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.instructionsProtesto.placeholder", "Protest Instructions");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.invoiceNumber.label", "Invoice Number");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.invoiceNumber.placeholder", "Invoice Number");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.invoiceNumber.requiredError", "You must enter an invoice number");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.notificationBody.placeholder", "Notification body");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.notificationEmails.help", "E-mails to notify the transfer of the transfer. Remember to enter them separated by commas.");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.notificationEmails.label", "Notification Emails");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.notificationEmails.placeholder", "Notification Emails");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.observations.label", "Observations");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.observations.placeholder", "Observations");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.sendTheDocumentationToTheBank.help", "Enter address and full bank name");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.sendTheDocumentationToTheBank.label", "Send the documentation to the bank");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.sendTheDocumentationToTheBank.placeholder", "Send the documentation to the bank");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.sendTheDocumentationToTheBank.requiredError", "You must enter the address and full name of the bank.");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.shippingDiscrepancies.label", "Shipping discrepancies");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.shippingDiscrepancies.option.no", "No");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.shippingDiscrepancies.option.si", "Yes");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.shippingDiscrepancies.placeholder", "Shipping discrepancies");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.turnData.help", "Enter address and full name of the rotated");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.turnData.label", "Turn Data");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.turnData.placeholder", "Turn Data");
        formFieldMessagesEN.put("fields.presentationOfDocumentsUnderCollection.turnData.requiredError", "You must enter the data of the rotated");
        formFieldMessagesEN.put("fields.requestEndorsement.attachments.label", "Attachments");
        formFieldMessagesEN.put("fields.requestEndorsement.boardingPass.label", "Type of document boarding");
        formFieldMessagesEN.put("fields.requestEndorsement.boardingPass.option.aerial", "By air");
        formFieldMessagesEN.put("fields.requestEndorsement.boardingPass.option.maritime", "Maritime");
        formFieldMessagesEN.put("fields.requestEndorsement.disclaimer.label", "Disclaimer");
        formFieldMessagesEN.put("fields.requestEndorsement.disclaimer.showAcceptOptionText", "Accept");
        formFieldMessagesEN.put("fields.requestEndorsement.disclaimer.termsAndConditions", "The instructions sent to the bank by you after the . Will be processed on the following bank business day. I authorize to debit from my account the commissions that this instruction may generate.");
        formFieldMessagesEN.put("fields.requestEndorsement.emaillist.help", "E-mails to notify the transfer of the transfer. Remember to enter them separated by commas.");
        formFieldMessagesEN.put("fields.requestEndorsement.emaillist.label", "Notification Emails");
        formFieldMessagesEN.put("fields.requestEndorsement.letterCredit.help", "Select the letter of credit you are about to submit");
        formFieldMessagesEN.put("fields.requestEndorsement.letterCredit.label", "Letter of credit");
        formFieldMessagesEN.put("fields.requestEndorsement.letterCredit.requiredError", "You must enter a letter of credit");
        formFieldMessagesEN.put("fields.requestEndorsement.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.requestEndorsement.observationsOne.label", "Observations");
        formFieldMessagesEN.put("fields.requestEndorsement.observationsTwo.label", "Observations");
        formFieldMessagesEN.put("fields.requestEndorsement.shippingDocumentNumber.label", "Shiping document number");
        formFieldMessagesEN.put("fields.requestLoan.amountOfFees.label", "Installments");
        formFieldMessagesEN.put("fields.requestLoan.amountOfFees.option.12", "12");
        formFieldMessagesEN.put("fields.requestLoan.amountOfFees.option.3", "3");
        formFieldMessagesEN.put("fields.requestLoan.amountOfFees.option.6", "6");
        formFieldMessagesEN.put("fields.requestLoan.amountToRequest.label", "Capital");
        formFieldMessagesEN.put("fields.requestLoan.fundsDestination.label", "Funds destination");
        formFieldMessagesEN.put("fields.requestLoan.fundsDestination.option.0", "Purchase of merchandise");
        formFieldMessagesEN.put("fields.requestLoan.fundsDestination.option.1", "Transfer abroad");
        formFieldMessagesEN.put("fields.requestLoan.fundsDestination.option.2", "Purchase of machinery");
        formFieldMessagesEN.put("fields.requestLoan.fundsDestination.option.3", "Others");
        formFieldMessagesEN.put("fields.requestLoan.iAuthorizeDebitToAccount.label", "Debit account");
        formFieldMessagesEN.put("fields.requestLoan.loanType.label", "Type of loan");
        formFieldMessagesEN.put("fields.requestLoan.loanType.option.0", "Redeemable");
        formFieldMessagesEN.put("fields.requestLoan.loanType.option.1", "Mortgage");
        formFieldMessagesEN.put("fields.requestLoan.loanType.option.2", "Fixed term");
        formFieldMessagesEN.put("fields.requestLoan.loanType.option.3", "Automotive");
        formFieldMessagesEN.put("fields.requestLoan.termsAndCondLoan.label", "Terms and Conditions");
        formFieldMessagesEN.put("fields.requestLoan.termsAndCondLoan.showAcceptOptionText", "Accept");
        formFieldMessagesEN.put("fields.requestLoan.termsAndCondLoan.termsAndConditions", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularized in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.date.help", "Enter the date the transaction was made");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.date.label", "Date of transaction");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.date.requiredError", "You must enter the date of the transaction");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.disclaimer.termsAndConditions", "The receipt of this request does not guarantee the cancellation of the transaction, that process will be subject to the reception schedule, the state or other factors that prevent completing the request.");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.observations.help", "Enter the reason for the cancellation and any additional information to identify the transaction that you consider pertinent");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.observations.label", "Observations");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.reference.help", "Enter the transaction identifier");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.reference.label", "Reference");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.transactionType.help", "Enter the transaction identifier");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.transactionType.label", "Transaction Type");
        formFieldMessagesEN.put("fields.requestTransactionCancellation.transactionType.requiredError", "You must enter the type of transaction");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.authorizedNameWithdrawDocuments.label", "Name authorized to withdraw documents");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.creditLetter.help", "Select the letter of credit to which you want to raise discrepancies");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.creditLetter.label", "Letter of credit");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.creditLetter.requiredError", "You must enter a no. Letter of credit");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.debitAuthorization.label", "Debit authorization");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.debitAuthorization.showAcceptOptionText", "Yes");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.debitAuthorization.termsAndConditions", "I authorize to debit this amount from our account.");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.disclaimer.termsAndConditions", "Instructions sent to the bank by you after the will be processed the next bank business day. I authorize to debit from my account the commissions that this instruction can generate.");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.documentPersonWithdraw.label", "Document of person to withdraw");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.listaEmailsNotificacion.help", "E-mails to notify the transfer. Remember to enter them separated by commas.");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.listaEmailsNotificacion.label", "Notification Emails");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.surveyOfDiscrepancies.observations.label", "Observations");
        formFieldMessagesEN.put("fields.transferForeign.creditAccount.label", "Credit account");
        formFieldMessagesEN.put("fields.transferForeign.creditAccount.requiredError", "You must enter a credit account");
        formFieldMessagesEN.put("fields.transferForeign.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.transferForeign.notificationEmails.help", "E-mails to notify the transfer.");
        formFieldMessagesEN.put("fields.transferForeign.notificationEmails.label", "Notification Emails");
        formFieldMessagesEN.put("fields.transferForeign.sectionNotificationInfo.label", "Notification Information");
        formFieldMessagesEN.put("fields.transferInternal.amount.help", "Enter the amount of the transaction, consider using the comma as a decimal separator");
        formFieldMessagesEN.put("fields.transferInternal.creditReference.help", "Text to help the recipient identify their transfer.");
        formFieldMessagesEN.put("fields.transferInternal.debitReference.help", "Text to help you identify your transfer.");
        formFieldMessagesEN.put("fields.transferLocal.amount.help", "The required currency changes will be made based on the currencies of the debit accounts and the currency of the transfer. Enter the amount of the transaction, consider using the comma as a decimal separator");
        formFieldMessagesEN.put("fields.transferLocal.amount.label", "Amount");
        formFieldMessagesEN.put("fields.transferLocal.amount.requiredError", "You must enter an amount");
        formFieldMessagesEN.put("fields.transferLocal.creditAccount.label", "Credit account");
        formFieldMessagesEN.put("fields.transferLocal.creditAccount.requiredError", "You must enter a credit account");
        formFieldMessagesEN.put("fields.transferLocal.creditReference.help", "Text to help the recipient identify their transfer.");
        formFieldMessagesEN.put("fields.transferLocal.creditReference.label", "Destination reference");
        formFieldMessagesEN.put("fields.transferLocal.debitAccount.hint", "All commissions will be debited from the selected account, if you wish you can upload the expenses to another account");
        formFieldMessagesEN.put("fields.transferLocal.debitAccount.label", "Debit account");
        formFieldMessagesEN.put("fields.transferLocal.debitAccount.requiredError", "You must select a debit account");
        formFieldMessagesEN.put("fields.transferLocal.debitReference.help", "Text to help you identify your transfer.");
        formFieldMessagesEN.put("fields.transferLocal.debitReference.label", "Source reference");
        formFieldMessagesEN.put("fields.transferLocal.expenseAccount.label", "Expense account");
        formFieldMessagesEN.put("fields.transferLocal.notificationBody.label", "Notification body");
        formFieldMessagesEN.put("fields.transferLocal.notificationEmails.help", "E-mails to notify the transfer.");
        formFieldMessagesEN.put("fields.transferLocal.notificationEmails.label", "Notification Emails");
        formFieldMessagesEN.put("fields.transferLocal.sectionCreditData.label", "Credit information");
        formFieldMessagesEN.put("fields.transferLocal.sectionDebitData.label", "Debit data");
        formFieldMessagesEN.put("fields.transferLocal.sectionNotificationInfo.label", "Notification information");

        
        formFieldMessagesEN.keySet().forEach((key) -> {
            delete("form_field_messages", "id_message = '" + key + "' and lang = 'en'");
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, 
                     "INSERT INTO form_field_messages (id_message, id_form, id_field, form_version, lang, value, modification_date) "
                    + "VALUES ('" + key + "', '" + key.split("\\.")[1] + "', '" + key.split("\\.")[2] + "', '" + version + "', 'en', '" + formFieldMessagesEN.get(key) + "', TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO form_field_messages (id_message, id_form, id_field, form_version, lang, value, modification_date) "
                    + "VALUES ('" + key + "', '" + key.split("\\.")[1] + "', '" + key.split("\\.")[2] + "', '" + version + "', 'en', '" + formFieldMessagesEN.get(key) + "', '" + date + "')");                
            }
        });
        
    }
}
