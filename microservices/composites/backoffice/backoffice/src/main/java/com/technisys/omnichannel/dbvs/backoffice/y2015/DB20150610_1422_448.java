/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150610_1422_448 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("preferences.environmentandlang.modify.pre", "com.technisys.omnichannel.client.activities.preferences.environmentandlang.ModifyEnvironmentAndLangPreActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.environmentandlang.modify.send", "com.technisys.omnichannel.client.activities.preferences.environmentandlang.ModifyEnvironmentAndLangActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}
