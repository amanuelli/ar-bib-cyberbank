/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ReadRequest;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.ReadResponse;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.opensymphony.xwork2.ActionSupport;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 * @author Sebastian Barbosa
 */
@Action(results = {
    @Result(location = "/invitationcodes/_resend.jsp")
})
public class Resend extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ReadRequest request = new ReadRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.invitationCodes.read");

        request.setIdInvitationCode(idCode);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);
        if (!ReturnCodes.OK.equals(response.getReturnCode())) {
            throw new JSONException(response);
        }

        ReadResponse readResponse = (ReadResponse) response;
        invitationCode = readResponse.getInvitationCode();

        return SUCCESS;
    }
    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private int idCode;

    public void setIdCode(int idCode) {
        this.idCode = idCode;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private InvitationCode invitationCode;

    public InvitationCode getInvitationCode() {
        return invitationCode;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
