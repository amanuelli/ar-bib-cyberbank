    /*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author MQuerves
 */
public class DB201108291639_10918 extends DBVSUpdate {

    @Override
    public void up() {


        String[] fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value", "possible_values"};

        String[] fieldValues = new String[]{"cuentas", "8", "subtitle", "0", "Cuentas", null};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"condiciones", "8", "subtitle", "0", "Condiciones", null};
        insert("form_fields", fieldNames, fieldValues);

        fieldNames = new String[]{"template_es"};
        fieldValues = new String[]{"<form:form>        <form:section>            <form:field idField=\"cuentas\"/>            <form:field idField=\"cuentaDeudoraA\"/>            <form:field idField=\"cuentaDeudoraB\"/>            <form:field idField=\"cuentaDeudoraC\"/>            <form:field idField=\"cuentaDeudoraD\"/>            <form:field idField=\"cuentaDeudoraE\"/>            <form:field idField=\"cuentaDeudoraF\"/>            <form:field idField=\"cuentaDeudoraG\"/>            "
            + "<form:field idField=\"cuentaDeudoraH\"/>            <form:field idField=\"cuentaAcreedoraA\"/>            <form:field idField=\"cuentaAcreedoraB\"/>            <form:field idField=\"cuentaAcreedoraC\"/>            <form:field idField=\"cuentaAcreedoraD\"/>            <form:field idField=\"cuentaAcreedoraE\"/>            <form:field idField=\"cuentaAcreedoraF\"/>            <form:field idField=\"cuentaAcreedoraG\"/>            <form:field idField=\"cuentaAcreedoraH\"/>        "
            + "</form:section>        <div class=\"dotted_separator\">&nbsp;</div>        <form:section>            <form:field idField=\"condiciones\"/>            <form:field idField=\"momentoDeCompensacion\"/>            <form:field idField=\"tipoDeCompensacion\"/>            <form:field idField=\"compensacionA\"/>            <form:field idField=\"compensacionB\"/>            <form:field idField=\"compensacionC\"/>            <form:field idField=\"compensacionD\"/>            "
            + "<form:field idField=\"compensacionE\"/>            <form:field idField=\"compensacionF\"/>            <form:field idField=\"compensacionG\"/>            <form:field idField=\"compensacionH\"/>        </form:section>    </form:form>"};
        update("forms", fieldNames, fieldValues, "id_form = 8");

    }
}
