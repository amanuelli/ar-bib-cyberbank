/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 * Issue: 9837 (SMS)
 */
public class DB201106131440 extends DBVSUpdate {

    @Override
    public void up() {
        // PERMISSIONS
        String[] fieldNames = new String[]{"id_permission", "permission_type"};
        String[] fieldValues = new String[]{"rub.channels.manage", "client"};
        insert("permissions", fieldNames, fieldValues);

        // PERMISSIONS CREDENTIAL GROUPS
        fieldNames = new String[]{"id_permission", "id_credential_group"};
        fieldValues = new String[]{"rub.channels.manage", "onlyPassword"};
        insert("permissions_credentials_groups", fieldNames, fieldValues);

        // ROLE PERMISSIONS
        fieldNames = new String[]{"id_role", "id_permission"};
        fieldValues = new String[]{"client_administrator", "rub.channels.manage"};
        insert("role_permissions", fieldNames, fieldValues);

        // ACTIVITIES
        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required"};
        fieldValues = new String[]{"rub.channels.modifyPre", "1", "1", "com.technisys.rubicon.business.administration.channels.activities.ModifyPreChannelsActivity", "rub.channels.manage"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.channels.modify", "1", "1", "com.technisys.rubicon.business.administration.channels.activities.ModifyChannelsActivity", "rub.channels.manage"};
        insert("activities", fieldNames, fieldValues);
    }
}
