/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.environments.holding.activities;

import com.technisys.omnichannel.backoffice.business.cybo.Response;
import com.technisys.omnichannel.backoffice.business.cybo.environments.holding.requests.ReadAdminData;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.backoffice.domain.cybo.holdings.AdminUser;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;


public class ReadAdminActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        Response<AdminUser> response = new Response<>(request);

        try {
            ReadAdminData readRequest = (ReadAdminData) request;


            User coreUser = AccessManagementHandlerFactory.getHandler().getUserByDocument(readRequest.getAdminDocumentCountry(), readRequest.getAdminDocumentType(), readRequest.getAdminDocumentNumber());

            Boolean isBackofficeUser = true;

            if (Objects.isNull(coreUser)) {
                isBackofficeUser = false;
                coreUser = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(),
                        readRequest.getAdminDocumentCountry(),
                        readRequest.getAdminDocumentType(),
                        readRequest.getAdminDocumentNumber(),
                        null);
            }

            response.setData(new AdminUser(coreUser, isBackofficeUser));
            response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
        } catch (IOException | BackendConnectorException ex) {
            throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }


    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {

        Map<String, String> result = new HashMap<>();

        ReadAdminData data = (ReadAdminData) request;

        try {
            if (StringUtils.isBlank(data.getAdminDocumentCountry())) {
                result.put("@user", "cybo.environmentsManagement.users.validations.document.empty");
            }

            if (StringUtils.isBlank(data.getAdminDocumentType())) {
                result.put("@user", "backoffice.environments.holdings.error.adminDocumentTypeRequired");
            }

            if (StringUtils.isBlank(data.getAdminDocumentNumber())) {
                result.put("@user", "backoffice.environments.holdings.error.adminDocumentNumberRequired");
            } else if (!data.getAdminDocumentType().equals("PAS") && !data.getAdminDocumentNumber().matches("\\d+")) {
                result.put("@user", "backoffice.environments.holdings.error.adminDocumentNumber.invalid");
            }

            if (result.isEmpty()) {

                User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(data.getAdminDocumentCountry(), data.getAdminDocumentType(), data.getAdminDocumentNumber());

                if (Objects.isNull(user)) {
                    user = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(),
                            data.getAdminDocumentCountry(),
                            data.getAdminDocumentType(),
                            data.getAdminDocumentNumber(),
                            null);
                    if(Objects.isNull(user)){
                        result.put("@documentNumber", "activities.backoffice.invitationCodes.clientNotInBackend");
                    }
                }
            }
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

}
