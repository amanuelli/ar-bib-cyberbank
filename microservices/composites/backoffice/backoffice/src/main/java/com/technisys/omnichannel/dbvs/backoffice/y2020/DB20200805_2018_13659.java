/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author KevinGaray
 */

public class DB20200805_2018_13659 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("session.listRandomSecuritySeal");
        insertActivity("session.listRandomSecuritySeal", "com.technisys.omnichannel.client.activities.session.GetRandomSecuritySealActivity", "login", ActivityDescriptor.AuditLevel.None, ClientActivityDescriptor.Kind.Other,null);

    }

}