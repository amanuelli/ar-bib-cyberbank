/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4820
 *
 * @author dimoda
 */
public class DB20180723_1652_4820 extends DBVSUpdate {

    @Override
    public void up() {

        insertOrUpdateConfiguration("demo.safeMode", "false", ConfigurationGroup.TECNICAS, "other", new String[]{"notEmpty", "boolean"});
        insertOrUpdateConfiguration("demo.safeMode.onboarding.dateOfBirth", "1980-06-01", ConfigurationGroup.TECNICAS, "other", new String[]{""});
        insertOrUpdateConfiguration("demo.safeMode.onboarding.expirationDate", "2030-12-31", ConfigurationGroup.TECNICAS, "other", new String[]{""});
        
        insertOrUpdateConfiguration("demo.safeMode.onboarding.issuingCountry", "CAN", ConfigurationGroup.TECNICAS, "other", new String[]{""});
        insertOrUpdateConfiguration("demo.safeMode.onboarding.documentType", "I", ConfigurationGroup.TECNICAS, "other", new String[]{""});
        insertOrUpdateConfiguration("demo.safeMode.onboarding.documentSubtype", "", ConfigurationGroup.TECNICAS, "other", new String[]{""});
        insertOrUpdateConfiguration("demo.safeMode.onboarding.documentNumber", "12345678", ConfigurationGroup.TECNICAS, "other", new String[]{""});
       
        insertOrUpdateConfiguration("demo.safeMode.onboarding.givenName", "Megan", ConfigurationGroup.TECNICAS, "other", new String[]{""});
        insertOrUpdateConfiguration("demo.safeMode.onboarding.lastName", "Morin", ConfigurationGroup.TECNICAS, "other", new String[]{""});
        insertOrUpdateConfiguration("demo.safeMode.onboarding.sex", "F", ConfigurationGroup.TECNICAS, "other", new String[]{""});
        insertOrUpdateConfiguration("demo.safeMode.onboarding.nationality", "CAN", ConfigurationGroup.TECNICAS, "other", new String[]{""});

    }

}