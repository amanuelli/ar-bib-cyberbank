/*
 *  Copyright 2014 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Resuelve incidencia INCIDENCIA-827
 *
 * @author sbarbosa
 */
public class DB20150410_1124_827 extends DBVSUpdate {

    @Override
    public void up() {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date today = new Date();


        // Agregamos el permiso de usuario logueado
        insert("permissions", new String[]{"id_permission"}, new String[]{"administration.view"});
        insert("permissions_credentials_groups", new String[]{"id_permission","id_credential_group"}, new String[]{"administration.view", "onlyPassword"});

        insert("permissions", new String[]{"id_permission"}, new String[]{"administration.manage"});
        insert("permissions_credentials_groups", new String[]{"id_permission","id_credential_group"}, new String[]{"administration.manage", "onlyPassword"});

        // Usuarios
        String[] fieldNames = new String[]{"id_user", "email", "id_user_status", "deleted", "first_name", "last_name", "password", "lang", "id_seal"};
        insert("users", fieldNames, new String[]{"ystonebraker", "omnichannel-development+ystonebraker@technisys.com", "active", "0", "Yvone", "Stonebraker", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "5"});
        insert("users", fieldNames, new String[]{"khardnett", "omnichannel-development+khardnett@technisys.com", "active", "0", "Karly", "Hardnett", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "1"});
        insert("users", fieldNames, new String[]{"gbuchan", "omnichannel-development+gbuchan@technisys.com", "active", "0", "Gwen", "Buchan", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "2"});
        insert("users", fieldNames, new String[]{"shancock", "omnichannel-development+shancock@technisys.com", "active", "0", "Sunni", "Hancock", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "3"});
        insert("users", fieldNames, new String[]{"cmanderscheid", "omnichannel-development+cmanderscheid@technisys.com", "active", "0", "Charita", "Manderscheid", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "4"});
        insert("users", fieldNames, new String[]{"mcrandell", "omnichannel-development+mcrandell@technisys.com", "active", "0", "Martina", "Crandell", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "5"});
        insert("users", fieldNames, new String[]{"csuiter", "omnichannel-development+csuiter@technisys.com", "active", "0", "Chu", "Suiter", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "6"});
        insert("users", fieldNames, new String[]{"bbrickhouse", "omnichannel-development+bbrickhouse@technisys.com", "active", "0", "Blanche", "Brickhouse", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "7"});
        insert("users", fieldNames, new String[]{"ffite", "omnichannel-development+ffite@technisys.com", "active", "0", "Floyd", "Fite", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "8"});
        insert("users", fieldNames, new String[]{"ggott", "omnichannel-development+ggott@technisys.com", "active", "0", "Gregorio", "Gott", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "9"});
        insert("users", fieldNames, new String[]{"emcquarrie", "omnichannel-development+emcquarrie@technisys.com", "active", "0", "Edward", "Mcquarrie", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "1"});
        insert("users", fieldNames, new String[]{"dburkhardt", "omnichannel-development+dburkhardt@technisys.com", "active", "0", "Delta", "Burkhardt", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "2"});
        insert("users", fieldNames, new String[]{"kdeweese", "omnichannel-development+kdeweese@technisys.com", "active", "0", "Keitha", "Deweese", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "3"});
        insert("users", fieldNames, new String[]{"cheilman", "omnichannel-development+cheilman@technisys.com", "active", "0", "Caroll", "Heilman", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "4"});
        insert("users", fieldNames, new String[]{"kcroke", "omnichannel-development+kcroke@technisys.com", "active", "0", "Karla", "Croke", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "5"});
        insert("users", fieldNames, new String[]{"ischauer", "omnichannel-development+ischauer@technisys.com", "active", "0", "Irwin", "Schauer", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "6"});
        insert("users", fieldNames, new String[]{"rfranceschini", "omnichannel-development+rfranceschini@technisys.com", "active", "0", "Rafaela", "Franceschini", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "7"});
        insert("users", fieldNames, new String[]{"amoshier", "omnichannel-development+amoshier@technisys.com", "active", "0", "Alix", "Moshier", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "8"});
        insert("users", fieldNames, new String[]{"lstahlman", "omnichannel-development+lstahlman@technisys.com", "active", "0", "Luke", "Stahlman", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "9"});
        insert("users", fieldNames, new String[]{"vcurrington", "omnichannel-development+vcurrington@technisys.com", "active", "0", "Vannesa", "Currington", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "1"});
        insert("users", fieldNames, new String[]{"jakin", "omnichannel-development+jakin@technisys.com", "active", "0", "Jaimee", "Akin", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "2"});
        insert("users", fieldNames, new String[]{"ytinner", "omnichannel-development+ytinner@technisys.com", "active", "0", "Yong", "Tinner", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "3"});
        insert("users", fieldNames, new String[]{"khieb", "omnichannel-development+khieb@technisys.com", "active", "0", "Kimber", "Hieb", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "4"});
        insert("users", fieldNames, new String[]{"nmcduff", "omnichannel-development+nmcduff@technisys.com", "active", "0", "Natalie", "Mcduff", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "5"});
        insert("users", fieldNames, new String[]{"sgram", "omnichannel-development+sgram@technisys.com", "active", "0", "Sabrina", "Gram", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "6"});
        insert("users", fieldNames, new String[]{"eyuan", "omnichannel-development+eyuan@technisys.com", "active", "0", "Edris", "Yuan", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "7"});
        insert("users", fieldNames, new String[]{"dsangster", "omnichannel-development+dsangster@technisys.com", "active", "0", "Demetra", "Sangster", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "8"});
        insert("users", fieldNames, new String[]{"mmcneill", "omnichannel-development+mmcneill@technisys.com", "active", "0", "Murray", "Mcneill", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "9"});
        insert("users", fieldNames, new String[]{"bbeall", "omnichannel-development+bbeall@technisys.com", "active", "0", "Brittny", "Beall", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "1"});
        insert("users", fieldNames, new String[]{"pcalcote", "omnichannel-development+pcalcote@technisys.com", "active", "0", "Piedad", "Calcote", "5f4dcc3b5aa765d61d8327deb882cf99", "es", "2"});

        fieldNames = new String[]{"id_user", "document", "document_type", "document_country"};
        insert("client_users", fieldNames, new String[]{"ystonebraker", "11111111", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"khardnett", "11111112", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"gbuchan", "41125788", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"shancock", "11111113", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"cmanderscheid", "11111114", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"mcrandell", "11111115", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"csuiter", "11111116", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"bbrickhouse", "11111117", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"ffite", "24587450", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"ggott", "11111118", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"emcquarrie", "11111119", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"dburkhardt", "11111120", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"kdeweese", "11111121", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"cheilman", "11111122", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"kcroke", "42563214", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"ischauer", "11111123", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"rfranceschini", "11111124", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"amoshier", "11111125", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"lstahlman", "11111126", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"vcurrington", "11111128", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"jakin", "33655894", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"ytinner", "11111129", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"khieb", "11111130", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"nmcduff", "11111131", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"sgram", "19085976", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"eyuan", "11111132", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"dsangster", "11111133", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"mmcneill", "11111134", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"bbeall", "28146325", "CI", "UY"});
        insert("client_users", fieldNames, new String[]{"pcalcote", "11111135", "CI", "UY"});

        // Configuracion de comunicaciones
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "DELETE FROM communications_configuration");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO communications_configuration(id_user, communication_type, subscribed_sms, subscribed_mail, subscribed_default,  subscribed_facebook, subscribed_twitter) "
                + "SELECT id_user, 'accepted', 0, 1, 1, 1, 1 FROM users");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO communications_configuration(id_user, communication_type, subscribed_sms, subscribed_mail, subscribed_default,  subscribed_facebook, subscribed_twitter) "
                + "SELECT id_user, 'rejected', 0, 1, 1, 1, 1 FROM users");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO communications_configuration(id_user, communication_type, subscribed_sms, subscribed_mail, subscribed_default,  subscribed_facebook, subscribed_twitter) "
                + "SELECT id_user, 'warning', 0, 1, 1, 1, 1 FROM users");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO communications_configuration(id_user, communication_type, subscribed_sms, subscribed_mail, subscribed_default,  subscribed_facebook, subscribed_twitter) "
                + "SELECT id_user, 'message', 0, 1, 1, 1, 1 FROM users");


        // ############################################################
        // AMBIENTE Sirio - cliente 10
        String[] fieldNamesEnv = new String[]{"name", "id_environment_status", "environment_type", "deleted", "administration_scheme", "cap_frequency", "sms_enabled"};
        insert("environments", fieldNamesEnv, new String[]{"Sirio S.R.L.", "active", "corporate", "0", "medium", "daily", "1"});

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO client_environments (id_environment, id_client) "
                + "SELECT id_environment, 10 FROM environments WHERE name = 'Sirio S.R.L.'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '017782654', id_environment, 'CA' FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '016512655', id_environment, 'CC' FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '017782655', id_environment, 'CA' FROM client_environments WHERE id_client = 10");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '3322245', id_environment, 'PA' FROM client_environments WHERE id_client = 10");



        // Usuarios del ambiente
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ystonebraker', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ystonebraker', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'khardnett', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'khardnett', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'gbuchan', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'gbuchan', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'shancock', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'shancock', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'cmanderscheid', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 10");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'cmanderscheid', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 10");


        // Grupos
        //Yvone Stonebraker
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Yvone Stonebraker', 0, 0, 'Grupo para la persona Yvone Stonebraker' FROM client_environments WHERE id_client = 10");

        String[] defaultPermissions = new String[]{"core.authenticated", "core.loginWithPassword", "core.loginWithOTP"};
        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 10 AND G.name = 'Yvone Stonebraker'");
        }
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions(id_group, id_permission, target) "
                + "SELECT G.id_group, 'administration.manage', 'NONE' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 10 AND G.name = 'Yvone Stonebraker'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'ystonebraker' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 10 AND G.name = 'Yvone Stonebraker'");


        // Karly Hardnett
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Karly Hardnett', 0, 0, 'Grupo para la persona Karly Hardnett' FROM client_environments WHERE id_client = 10");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 10 AND G.name = 'Karly Hardnett'");
        }
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions(id_group, id_permission, target) "
                + "SELECT G.id_group, 'administration.manage', 'NONE' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 10 AND G.name = 'Karly Hardnett'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'khardnett' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 10 AND G.name = 'Karly Hardnett'");

        // Gwen Buchan
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Gwen Buchan', 0, 0, 'Grupo para la persona Gwen Buchan' FROM client_environments WHERE id_client = 10");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 10 AND G.name = 'Gwen Buchan'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'gbuchan' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 10 AND G.name = 'Gwen Buchan'");

        // Sunni Hancock
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Sunni Hancock', 0, 0, 'Grupo para la persona Sunni Hancock' FROM client_environments WHERE id_client = 10");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 10 AND G.name = 'Sunni Hancock'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'shancock' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 10 AND G.name = 'Sunni Hancock'");

        // Charita Manderscheid
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Charita Manderscheid', 0, 0, 'Grupo para la persona Charita Manderscheid' FROM client_environments WHERE id_client = 10");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 10 AND G.name = 'Charita Manderscheid'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'cmanderscheid' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 10 AND G.name = 'Charita Manderscheid'");

        // ############################################################
        // AMBIENTE Orion - cliente 11
        insert("environments", fieldNamesEnv, new String[]{"Orion L.T.D.A.", "active", "corporate", "0", "advanced", "daily", "1"});

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO client_environments (id_environment, id_client) "
                + "SELECT id_environment, 11 FROM environments WHERE name = 'Orion L.T.D.A.'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '033234982', id_environment, 'CC' FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '022334982', id_environment, 'CA' FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '022334983', id_environment, 'CA' FROM client_environments WHERE id_client = 11");


        // Usuarios del ambiente
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'mcrandell', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'mcrandell', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'csuiter', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'csuiter', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'bbrickhouse', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'bbrickhouse', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ffite', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ffite', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ggott', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ggott', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'emcquarrie', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'emcquarrie', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'dburkhardt', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'dburkhardt', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'kdeweese', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'kdeweese', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'cheilman', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'cheilman', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'kcroke', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'kcroke', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ischauer', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ischauer', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'rfranceschini', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'rfranceschini', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'amoshier', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'amoshier', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'lstahlman', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'lstahlman', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'vcurrington', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 11");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'vcurrington', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 11");

        // Grupos
        //Martina Crandell
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Martina Crandell', 0, 0, 'Grupo para la persona Martina Crandell' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Martina Crandell'");
        }
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions(id_group, id_permission, target) "
                + "SELECT G.id_group, 'administration.manage', 'NONE' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Martina Crandell'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'mcrandell' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Martina Crandell'");

        //Chu Suiter
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Chu Suiter', 0, 0, 'Grupo para la persona Chu Suiter' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Chu Suiter'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'csuiter' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Chu Suiter'");

        //Blanche Brickhouse
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Blanche Brickhouse', 0, 0, 'Grupo para la persona Blanche Brickhouse' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Blanche Brickhouse'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'bbrickhouse' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Blanche Brickhouse'");

        //Floyd Fite
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Floyd Fite', 0, 0, 'Grupo para la persona Floyd Fite' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Floyd Fite'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'ffite' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Floyd Fite'");

        //Gregorio Gott
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Gregorio Gott', 0, 0, 'Grupo para la persona Gregorio Gott' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Gregorio Gott'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'ggott' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Gregorio Gott'");

        //Edward Mcquarrie
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Edward Mcquarrie', 0, 0, 'Grupo para la persona Edward Mcquarrie' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Edward Mcquarrie'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'emcquarrie' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Edward Mcquarrie'");

        //Delta Burkhardt
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Delta Burkhardt', 0, 0, 'Grupo para la persona Delta Burkhardt' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Delta Burkhardt'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'dburkhardt' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Delta Burkhardt'");

        //Keitha Deweese
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Keitha Deweese', 0, 0, 'Grupo para la persona Keitha Deweese' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Keitha Deweese'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'kdeweese' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Keitha Deweese'");

        //Caroll Heilman
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Caroll Heilman', 0, 0, 'Grupo para la persona Caroll Heilman' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Caroll Heilman'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'cheilman' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Caroll Heilman'");

        //Karla Croke
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Karla Croke', 0, 0, 'Grupo para la persona Karla Croke' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Karla Croke'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'kcroke' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Karla Croke'");

        //Irwin Schauer
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Irwin Schauer', 0, 0, 'Grupo para la persona Irwin Schauer' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Irwin Schauer'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'ischauer' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Irwin Schauer'");

        //Rafaela Franceschini
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Rafaela Franceschini', 0, 0, 'Grupo para la persona Rafaela Franceschini' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Rafaela Franceschini'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'rfranceschini' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Rafaela Franceschini'");

        //Alix Moshier
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Alix Moshier', 0, 0, 'Grupo para la persona Alix Moshier' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Alix Moshier'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'amoshier' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Alix Moshier'");

        //Luke Stahlman
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Luke Stahlman', 0, 0, 'Grupo para la persona Luke Stahlman' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Luke Stahlman'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'lstahlman' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Luke Stahlman'");

        //Vannesa Currington
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Vannesa Currington', 0, 0, 'Grupo para la persona Vannesa Currington' FROM client_environments WHERE id_client = 11");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 11 AND G.name = 'Vannesa Currington'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'vcurrington' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 11 AND G.name = 'Vannesa Currington'");


        // ############################################################
        // AMBIENTE Antares - cliente 12
        insert("environments", fieldNamesEnv, new String[]{"Antares S.A.", "active", "corporate", "0", "medium", "daily", "1"});

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO client_environments (id_environment, id_client) "
                + "SELECT id_environment, 12 FROM environments WHERE name = 'Antares S.A.'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '017664901', id_environment, 'CA' FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '017664902', id_environment, 'CA' FROM client_environments WHERE id_client = 12");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '3411741', id_environment, 'PA' FROM client_environments WHERE id_client = 12");


        // Usuarios del ambiente
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'jakin', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'jakin', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ytinner', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'ytinner', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'khieb', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'khieb', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'nmcduff', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'nmcduff', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'sgram', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'sgram', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'eyuan', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 12");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'eyuan', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 12");

        // Grupos
        //Jaimee Akin
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Jaimee Akin', 0, 0, 'Grupo para la persona Jaimee Akin' FROM client_environments WHERE id_client = 12");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 12 AND G.name = 'Jaimee Akin'");
        }
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions(id_group, id_permission, target) "
                + "SELECT G.id_group, 'administration.manage', 'NONE' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 12 AND G.name = 'Jaimee Akin'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'jakin' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 12 AND G.name = 'Jaimee Akin'");

        //Yong Tinner
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Yong Tinner', 0, 0, 'Grupo para la persona Yong Tinner' FROM client_environments WHERE id_client = 12");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 12 AND G.name = 'Yong Tinner'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'ytinner' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 12 AND G.name = 'Yong Tinner'");

        //Kimber Hieb
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Kimber Hieb', 0, 0, 'Grupo para la persona Kimber Hieb' FROM client_environments WHERE id_client = 12");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 12 AND G.name = 'Kimber Hieb'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'khieb' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 12 AND G.name = 'Kimber Hieb'");

        //Natalie Mcduff
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Natalie Mcduff', 0, 0, 'Grupo para la persona Natalie Mcduff' FROM client_environments WHERE id_client = 12");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 12 AND G.name = 'Natalie Mcduff'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'nmcduff' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 12 AND G.name = 'Natalie Mcduff'");

        //Sabrina Gram
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Sabrina Gram', 0, 0, 'Grupo para la persona Sabrina Gram' FROM client_environments WHERE id_client = 12");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 12 AND G.name = 'Sabrina Gram'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'sgram' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 12 AND G.name = 'Sabrina Gram'");

        //Edris Yuan
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Edris Yuan', 0, 0, 'Grupo para la persona Edris Yuan' FROM client_environments WHERE id_client = 12");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 12 AND G.name = 'Edris Yuan'");
        }

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'eyuan' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 12 AND G.name = 'Edris Yuan'");


        // ############################################################
        // AMBIENTE Demetra Sangster - cliente 13
        insert("environments", fieldNamesEnv, new String[]{"Demetra Sangster", "active", "retail", "0", "simple", "daily", "1"});

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO client_environments (id_environment, id_client) "
                + "SELECT id_environment, 13 FROM environments WHERE name = 'Demetra Sangster'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '031665501', id_environment, 'CA' FROM client_environments WHERE id_client = 13");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '031665502', id_environment, 'CA' FROM client_environments WHERE id_client = 13");

        // Usuarios del ambiente
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'dsangster', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 13");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'dsangster', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 13");

        // Grupos
        //Demetra Sangster
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Demetra Sangster', 0, 0, 'Grupo para la persona Demetra Sangster' FROM client_environments WHERE id_client = 13");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 13 AND G.name = 'Demetra Sangster'");
        }
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions(id_group, id_permission, target) "
                + "SELECT G.id_group, 'administration.manage', 'NONE' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 13 AND G.name = 'Jaimee Akin'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'dsangster' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 13 AND G.name = 'Demetra Sangster'");


        // ############################################################
        // AMBIENTE Murray Mcneill - cliente 14
        insert("environments", fieldNamesEnv, new String[]{"Murray Mcneill", "active", "retail", "0", "simple", "daily", "1"});

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO client_environments (id_environment, id_client) "
                + "SELECT id_environment, 14 FROM environments WHERE name = 'Murray Mcneill'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '032775401', id_environment, 'CA' FROM client_environments WHERE id_client = 14");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '032775402', id_environment, 'CA' FROM client_environments WHERE id_client = 14");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '4444122376651109', id_environment, 'VISA' FROM client_environments WHERE id_client = 14");

        // Usuarios del ambiente
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'mmcneill', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 14");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'mmcneill', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 14");

        // Grupos
        //Murray Mcneill
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Murray Mcneill', 0, 0, 'Grupo para la persona Murray Mcneill' FROM client_environments WHERE id_client = 14");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 14 AND G.name = 'Murray Mcneill'");
        }
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions(id_group, id_permission, target) "
                + "SELECT G.id_group, 'administration.manage', 'NONE' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 14 AND G.name = 'Murray Mcneill'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'mmcneill' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 14 AND G.name = 'Murray Mcneill'");


        // ############################################################
        // AMBIENTE Brittny Beall - cliente 15
        insert("environments", fieldNamesEnv, new String[]{"Brittny Beall", "active", "retail", "0", "simple", "daily", "1"});

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO client_environments (id_environment, id_client) "
                + "SELECT id_environment, 15 FROM environments WHERE name = 'Brittny Beall'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '043655401', id_environment, 'CA' FROM client_environments WHERE id_client = 15");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '043655402', id_environment, 'CA' FROM client_environments WHERE id_client = 15");

        // Usuarios del ambiente
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'bbeall', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 15");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'bbeall', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 15");

        // Grupos
        //Brittny Beall
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Brittny Beall', 0, 0, 'Grupo para la persona Brittny Beall' FROM client_environments WHERE id_client = 15");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 15 AND G.name = 'Brittny Beall'");
        }
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions(id_group, id_permission, target) "
                + "SELECT G.id_group, 'administration.manage', 'NONE' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 15 AND G.name = 'Brittny Beall'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'bbeall' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 15 AND G.name = 'Brittny Beall'");


        // ############################################################
        // AMBIENTE Brittny Beall - cliente 16
        insert("environments", fieldNamesEnv, new String[]{"Piedad Calcote", "active", "retail", "0", "simple", "daily", "1"});

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO client_environments (id_environment, id_client) "
                + "SELECT id_environment, 16 FROM environments WHERE name = 'Piedad Calcote'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '035153201', id_environment, 'CA' FROM client_environments WHERE id_client = 16");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '035153202', id_environment, 'CA' FROM client_environments WHERE id_client = 16");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '5155001298550010', id_environment, 'MASTER' FROM client_environments WHERE id_client = 16");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '4444000011220987', id_environment, 'VISA' FROM client_environments WHERE id_client = 16");

        // Usuarios del ambiente
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'pcalcote', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 16");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'pcalcote', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 16");

        // Grupos
        //Brittny Beall
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Piedad Calcote', 0, 0, 'Grupo para la persona Piedad Calcote' FROM client_environments WHERE id_client = 16");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 16 AND G.name = 'Piedad Calcote'");
        }
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions(id_group, id_permission, target) "
                + "SELECT G.id_group, 'administration.manage', 'NONE' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 16 AND G.name = 'Piedad Calcote'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'pcalcote' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 16 AND G.name = 'Piedad Calcote'");


        // ############################################################
        // AMBIENTE Karly Hardnett - cliente 17
        insert("environments", fieldNamesEnv, new String[]{"Karly Hardnett", "active", "retail", "0", "simple", "daily", "1"});

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO client_environments (id_environment, id_client) "
                + "SELECT id_environment, 17 FROM environments WHERE name = 'Karly Hardnett'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '037200111', id_environment, 'CC' FROM client_environments WHERE id_client = 17");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '037200112', id_environment, 'CC' FROM client_environments WHERE id_client = 17");
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_product(id_product, id_environment, product_type) "
                + "SELECT '4444020411339801', id_environment, 'VISA' FROM client_environments WHERE id_client = 17");

        // Usuarios del ambiente
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'khardnett', id_environment, 'active', 'A', '" + sdf.format(today) + "', 1 FROM client_environments WHERE id_client = 17");
        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO environment_users(id_user, id_environment, id_user_status, signature_level, creation_date, can_web) "
                + "SELECT 'khardnett', id_environment, 'active', 'A', to_date('" + sdf.format(today) + "', 'yyyy-mm-dd HH24:MI:SS'), 1 FROM client_environments WHERE id_client = 17");

        // Grupos
        //Brittny Beall
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO groups(id_environment, name, blocked, deleted, description)"
                + "SELECT id_environment, 'Karly Hardnett', 0, 0, 'Grupo para la persona Karly Hardnett' FROM client_environments WHERE id_client = 17");

        for (String permission : defaultPermissions) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO group_permissions(id_group, id_permission, target) "
                    + "SELECT G.id_group, '" + permission + "', 'NONE' "
                    + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                    + "WHERE CE.id_client = 17 AND G.name = 'Karly Hardnett'");
        }
        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_permissions(id_group, id_permission, target) "
                + "SELECT G.id_group, 'administration.manage', 'NONE' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 17 AND G.name = 'Karly Hardnett'");

        customSentence(new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO group_users(id_group, id_user) "
                + "SELECT G.id_group, 'khardnett' "
                + "FROM groups G JOIN client_environments CE ON (G.id_environment = CE.id_environment) "
                + "WHERE CE.id_client = 17 AND G.name = 'Karly Hardnett'");

    }
}