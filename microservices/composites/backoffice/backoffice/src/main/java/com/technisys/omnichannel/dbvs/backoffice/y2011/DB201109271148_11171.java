/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201109271148_11171 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL},
                "UPDATE activities SET id_permission_required=null WHERE id_activity='rub.loans.payLoanPre'");
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL},
                "UPDATE activities SET id_permission_required=null WHERE id_activity='rub.accounts.transferBatchPre'");
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL},
                "UPDATE activities SET id_permission_required=null WHERE id_activity='rub.creditcards.payCreditCardPre'");
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL},
                "UPDATE activities SET id_permission_required=null WHERE id_activity='rub.payment.paySalaryPre'");
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL},
                "UPDATE activities SET id_permission_required=null WHERE id_activity='rub.payment.paySuppliersPre'");
    }
}