/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author jhossept garay
 */

public class DB20190627_1624_7612 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"id_group", "id_sub_group"}, new String[]{ConfigurationGroup.NEGOCIO.toString(), "frontend"}, "id_field='administration.users.invite.notInBackend.enabled'");
    }

}