/* 
 * Copyright 2019 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6923
 *
 * @author emordezki
 */
public class DB20190322_1142_6923 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_activity", "id_field_amount", "id_field_product"};
        String[] fieldValues = new String[]{"pay.multiline.salary.send", "inputManually", "debitAccount"};
        insert("activity_caps", fieldNames, fieldValues);
    }
}
