/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5118
 *
 * @author norbes
 */
public class DB20180806_1614_5118 extends DBVSUpdate {

    @Override
    public void up() {
        
         customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, 
                 "DELETE FROM desktop_layouts WHERE id_environment in (SELECT id_environment FROM environments WHERE name='John Snow')");

        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, 
                "INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number) SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'accounts', '1', '1' FROM environments WHERE name='John Snow'");
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, 
                "INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number) SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'creditCards', '1', '2' FROM environments WHERE name='John Snow'");
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, 
                "INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number) SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'loans', '1', '3' FROM environments WHERE name='John Snow'");
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, 
                "INSERT INTO desktop_layouts (id_user, id_environment, id_widget, column_number, row_number) SELECT 'f6cadabe64fe4fdead71456813d41ab0', id_environment, 'exchangeRates', '1', '4' FROM environments WHERE name='John Snow'");
    }
}
