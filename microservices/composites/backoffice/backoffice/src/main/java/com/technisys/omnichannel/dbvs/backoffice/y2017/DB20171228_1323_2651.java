/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2651
 *
 * @author idonadini
 */
public class DB20171228_1323_2651 extends DBVSUpdate {

    @Override
    public void up() {

        update("forms", new String[]{"category"}, new String[]{"payments"}, "id_form = 'requestTransactionCancellation'");  
        
    }

}