/* 
 * Copyright 2017 Manentia Software. 
 * 
 * This software component is the intellectual property of Manentia Software S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * http://www.manentiasoftware.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-3047
 *
 * @author ldurand
 */
public class DB20170829_1213_3047 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "requestOfManagementCheck";
        String version = "1";
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        Map<String, String> messages = new HashMap();

        update("form_fields", new String[]{"ordinal"}, new String[]{"8"}, "id_form = '" + idForm + "' AND id_field = 'document'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"9"}, "id_form = '" + idForm + "' AND id_field = 'name'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"10"}, "id_form = '" + idForm + "' AND id_field = 'surname'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"11"}, "id_form = '" + idForm + "' AND id_field = 'secondSurname'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"12"}, "id_form = '" + idForm + "' AND id_field = 'termsAndConditions'");

        update("form_field_selector", new String[]{"default_value"}, new String[]{null}, "id_form = '" + idForm + "' AND id_field = 'placeOfRetreat'");
        
        insert("form_fields", new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "visible_in_mobile", "read_only", "sub_type", "ticket_only"}, new String[]{"branchOffices", idForm, version, "branchlist", "7", "value(placeOfRetreat) == 'branchOffices'", "value(placeOfRetreat) == 'branchOffices'", null, "1", "0", "default", "0"});
        insert("form_field_branchlist", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_other_option"}, new String[]{"branchOffices", idForm, version, "field-normal", "01", "0"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"placeOfRetreat", idForm, version, "branchOffices"});

        messages.put("branchOffices.label", "Sucursales");
        messages.put("branchOffices.requiredError", "La sucursal es requerida");
        messages.put("placeOfRetreat.option.branchOffices", "Sucursales");

        for (String key : messages.keySet()) {
            String[] formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

    }

}
