/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201109021443_11103 extends DBVSUpdate {

    @Override
    public void up() {

        update("configuration", new String[]{"value", "possible_values"}, new String[]{"info@technisys.com", ""}, "id_field = 'backoffice.loginbox.mailto'");

    }
}