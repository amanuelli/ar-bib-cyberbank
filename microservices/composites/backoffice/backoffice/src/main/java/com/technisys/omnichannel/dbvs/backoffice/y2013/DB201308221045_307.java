/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201308221045_307 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("rubicon.desktop.defaultLayout.retail", "transactions|accounts|creditCards|news|frequentTasks|banners|quotes|geolocalization|twitter", ConfigurationGroup.TECNICAS, null, new String[]{});

        insertOrUpdateConfiguration("rubicon.desktop.defaultLayout.corporate", "transactions|accounts|creditCards|loans|news|frequentTasks|banners|quotes|pymes|virtualChannelsSavings", ConfigurationGroup.TECNICAS, null, new String[]{});
        insertOrUpdateConfiguration("rubicon.daemon.scheduledTransactions.maxThreads", "5", ConfigurationGroup.NEGOCIO, null, new String[]{});

        insertOrUpdateConfiguration("rubicon.daemon.scheduledTransactions.timeout", "5m", ConfigurationGroup.NEGOCIO, null, new String[]{});

        insertOrUpdateConfiguration("rubicon.daemon.scheduledTransactions.startTime", "6:00", ConfigurationGroup.NEGOCIO, null, new String[]{});

        insertOrUpdateConfiguration("rubicon.daemon.scheduledTransactions.stopTime", "21:00", ConfigurationGroup.NEGOCIO, null, new String[]{});

        insertOrUpdateConfiguration("rubicon.daemon.scheduledTransactions.notification.idTray", "1", ConfigurationGroup.NEGOCIO, null, new String[]{});

    }
}