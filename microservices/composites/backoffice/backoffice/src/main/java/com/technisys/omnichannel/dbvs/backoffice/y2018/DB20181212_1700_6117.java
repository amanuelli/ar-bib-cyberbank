/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author mcheveste
 */

public class DB20181212_1700_6117 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("forms.fields.multilinefile.minimum.invalid.percentage.allowed", "86", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"}, null, "frontend");
    }
}