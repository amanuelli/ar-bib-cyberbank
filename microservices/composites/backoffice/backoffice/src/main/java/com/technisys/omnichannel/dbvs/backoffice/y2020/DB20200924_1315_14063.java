/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jhossept
 */

public class DB20200924_1315_14063 extends DBVSUpdate {

    @Override
    public void up() {
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        //OVERDRAFT
        String idForm = "accountOpening";
        String version = "1";
        String idField = "overdraft";

        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsValues = new String[]{idField, idForm, version, "selector", "40", "TRUE", "FALSE", "default", "1"};
        insert("form_fields", formFields, formFieldsValues);

        Map<String, String> messagesEn = new HashMap();
        Map<String, String> messagesEs = new HashMap();
        Map<String, String> messagesPt = new HashMap();

        /**** Insert field selector - Overdraft ****/
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as" },new String[] { idField, idForm, version, "field-normal", "0", "combo" });

        messagesEn.put("fields.accountOpening.overdraft.label", "Overdraft");
        messagesEs.put("fields.accountOpening.overdraft.label", "Sobregiros");
        messagesPt.put("fields.accountOpening.overdraft.label", "Cheque especial");

        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "Si"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "No"});

        messagesEn.put("fields.accountOpening.overdraft.option.No", "I do not want Techbank to authorize and pay overdrafts on my ATM and everyday debit card transaction");
        messagesEn.put("fields.accountOpening.overdraft.option.Si", "I do want Techbank to authorize and pay overdrafts on my ATM and everyday debit card transaction");
        messagesEs.put("fields.accountOpening.overdraft.option.No", "No quiero que Techbank autorice y pague sobregiros en mi cajero automático y transacciones diarias con tarjeta de débito");
        messagesEs.put("fields.accountOpening.overdraft.option.Si", "Quiero que Techbank autorice y pague sobregiros en mi cajero automático y transacciones diarias con tarjeta de débito  ");
        messagesPt.put("fields.accountOpening.overdraft.option.No", "Não quiero que Techbank autorice y pague sobregiros en mi cajero automático y transacciones diarias con tarjeta de débito  ");
        messagesPt.put("fields.accountOpening.overdraft.option.Si", "Eu quero que o Techbank autorize e pague saques no meu caixa eletrônico e transações diárias com cartão de débito  ");

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messagesEn.keySet()) {
            formFieldsValues = new String[]{key, "en", idField, idForm, version, messagesEn.get(key), date};
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

        for (String key : messagesEs.keySet()) {
            formFieldsValues = new String[]{key, "es", idField, idForm, version, messagesEs.get(key), date};
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

        for (String key : messagesPt.keySet()) {
            formFieldsValues = new String[]{key, "pt", idField, idForm, version, messagesPt.get(key), date};
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

    }

}