/* 
 * Copyright 2019 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.i18n.I18n;

import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-7296, we use the 6996 issue number to prevent the script running twice on a migration scenario
 *
 * @author Marcelo Bruno
 */
public class DB20190530_1055_7341 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "salaryPayment";

        Map<String, String> messages = new HashMap();
        messages.put("inputManually.label", "Líneas de pago");
        insertMessages(idForm, messages, I18n.LANG_SPANISH);

        messages = new HashMap();
        messages.put("inputManually.label", "Linhas de pagamento");
        insertMessages(idForm, messages, I18n.LANG_PORTUGUESE);

        messages = new HashMap();
        messages.put("inputManually.label", "Payment lines");
        insertMessages(idForm, messages, I18n.LANG_ENGLISH);
    }

    private void insertMessages(String idForm, Map<String, String> messages, String lang) {
        String[] formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        String[] formFieldsValues;

        for (Map.Entry<String, String> entry : messages.entrySet()) {
            String key = entry.getKey();
            String message = entry.getValue();

            formFieldsValues = new String[]{"fields." + idForm + "." + key, lang, key.substring(0, key.indexOf('.')), idForm, "1", message, "2018-12-10 12:00:00"};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }

    }

}