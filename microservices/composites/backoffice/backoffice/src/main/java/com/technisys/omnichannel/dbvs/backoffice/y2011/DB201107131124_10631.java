/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201107131124_10631 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};

        String[] fieldValues = new String[]{"rubicon.productTypes.account.list", "CC|CA|PF", "es", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.productTypes.loan.list", "PA|PI", "es", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.productTypes.creditCard.list", "MASTER|VISA", "es", "rubicon"};
        insert("configuration", fieldNames, fieldValues);
    }
}