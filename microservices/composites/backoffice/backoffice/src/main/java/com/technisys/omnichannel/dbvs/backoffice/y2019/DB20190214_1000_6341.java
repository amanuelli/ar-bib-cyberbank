/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import com.technisys.omnichannel.client.domain.Onboarding;
import java.sql.Types;

/**
 *
 * @author iocampo
 */
public class DB20190214_1000_6341 extends DBVSUpdate {

    @Override
    public void up() {

        insertActivity("productrequest.creditcard.preVerifyEmail",
                "com.technisys.omnichannel.client.activities.productrequest.creditcard.PreVerifyEmailActivity",
                "other",
                ActivityDescriptor.AuditLevel.Header,
                ClientActivityDescriptor.Kind.Other,
                null
        );

        insertActivity("productrequest.creditcard.sendVerificationCode",
                "com.technisys.omnichannel.client.activities.productrequest.creditcard.SendEmailVerificationCodeActivity",
                "other",
                ActivityDescriptor.AuditLevel.None,
                ClientActivityDescriptor.Kind.Other,
                null
        );

        insertActivity("productrequest.creditcard.verifyEmail",
                "com.technisys.omnichannel.client.activities.productrequest.creditcard.VerifyEmailActivity",
                "other",
                ActivityDescriptor.AuditLevel.Header,
                ClientActivityDescriptor.Kind.Other,
                null
        );

        insertActivity("productrequest.onboarding.UploadFrontDocument",
                "com.technisys.omnichannel.client.activities.productrequest.onboarding.UploadFrontDocumentActivity",
                "onboarding",
                ActivityDescriptor.AuditLevel.None,
                ClientActivityDescriptor.Kind.Other,
                null
        );

        insertActivity("productrequest.onboarding.UploadBackDocument",
                "com.technisys.omnichannel.client.activities.productrequest.onboarding.UploadBackDocumentActivity",
                "onboarding",
                ActivityDescriptor.AuditLevel.None,
                ClientActivityDescriptor.Kind.Other,
                null
        );

        insertActivity("productrequest.onboarding.UploadSelfie",
                "com.technisys.omnichannel.client.activities.productrequest.onboarding.UploadSelfieActivity",
                "onboarding",
                ActivityDescriptor.AuditLevel.None,
                ClientActivityDescriptor.Kind.Other,
                null
        );

        insertActivity("productrequest.onboarding.Step5",
                "com.technisys.omnichannel.client.activities.productrequest.onboarding.Step5Activity",
                "onboarding",
                ActivityDescriptor.AuditLevel.None,
                ClientActivityDescriptor.Kind.Other,
                null
        );
        
        insert("backoffice_permissions", new String[]{"id_backoffice_permission"}, new String[]{"backoffice.productrequest.read"});
        insert("backoffice_permissions", new String[]{"id_backoffice_permission"}, new String[]{"backoffice.productrequest.list"});
        insert("backoffice_role_permissions", new String[]{"id_backoffice_role", "id_backoffice_permission"}, new String[]{"administrator", "backoffice.productrequest.read"});
        insert("backoffice_role_permissions", new String[]{"id_backoffice_role", "id_backoffice_permission"}, new String[]{"administrator", "backoffice.productrequest.list"});

        insertBackofficeActivity("backoffice.productrequest.read",
                "com.technisys.omnichannel.backoffice.business.productrequest.activities.ReadActivity",
                "backoffice.productrequest.read",
                null,
                "backoffice.productrequest",
                ActivityDescriptor.AuditLevel.None
        );

        insertBackofficeActivity("backoffice.productrequest.list",
                "com.technisys.omnichannel.backoffice.business.productrequest.activities.ListActivity",
                "backoffice.productrequest.list",
                null,
                "backoffice.productrequest",
                ActivityDescriptor.AuditLevel.None
        );

        addColumn("onboardings", new ColumnDefinition("type", Types.VARCHAR, 45, 0, true, Onboarding.ONBOARDING_DEFAULT));

        insertOrUpdateConfiguration("creditCardRequest.minScoreRequired", "50", ConfigurationGroup.NEGOCIO, null, new String[]{"notEmpty", "integer"});
    }
}
