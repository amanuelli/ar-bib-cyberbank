/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2740
 *
 * @author mcommand
 */
public class DB20170725_1439_2740 extends DBVSUpdate {

    @Override
    public void up() {

        String[] formFieldMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        String[] forms = new String[]{"id_form", "version", "enabled", "category", "type", "admin_option", "id_activity", "last", "deleted", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow", "id_bpm_process", "programable"};
        String[] formMessages = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        String[] permissions = new String[]{"id_permission"};
        String[] permissionsCredentialsGroups = new String[]{"id_permission", "id_credential_group"};
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};
        String[] formFieldText = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        String[] formFieldTermsConditions = new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"};
        String[] formFieldTextarea = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        String[] formFieldSelector = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"};
        String[] formFieldSelectorOptions = new String[]{"id_field", "id_form", "form_version", "value"};
        String[] formFieldEmaillist = new String[]{"id_field", "id_form", "form_version", "display_type"};

        String[] values;

        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String formVersion = "1";
        String idForm = "paymentClaim";

        Map<String, String> messagesEs = new HashMap();

        delete("forms", "id_form = '" + idForm + "'");
        delete("permissions", "id_permission='client.form.paymentClaim.send'");

        //Formulario - Reclamo de cobranza        
        values = new String[]{idForm, formVersion, "1", "comex", "process", "comex", null, "1", "0", "1", "1", "1", "0", "1", "demo:1:3", "0"};
        insert("forms", forms, values);

        values = new String[]{"client.form.paymentClaim.send"};
        insert("permissions", permissions, values);

        values = new String[]{"client.form.paymentClaim.send", "accessToken-pin"};
        insert("permissions_credentials_groups", permissionsCredentialsGroups, values);

        values = new String[]{"forms.paymentClaim.formName", idForm, formVersion, "es", "Reclamo de cobranza", date};
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form,version, lang, value, modification_date) "
                    + " VALUES ('" + values[0] + "', '" + values[1] + "', '" + values[2] + "', '" + values[3] + "', '" + values[4] + "', TO_DATE('" + values[5] + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formMessages, values);
        }

        //Campo - Número de cobranza
        values = new String[]{"paymentNumber", idForm, formVersion, "text", "1", "TRUE", "TRUE", null, "1", "0", "default", "0"};
        insert("form_fields", formFields, values);

        values = new String[]{"paymentNumber", idForm, formVersion, "0", "30", "field-medium", "withoutSpecialChars"};
        insert("form_field_text", formFieldText, values);

        messagesEs.put("paymentNumber.invalidError", "Usted no tiene cobranzas para cancelar. En caso de dudas favor diríjase al área de comercio exterior");
        messagesEs.put("paymentNumber.label", "Numero de cobranza");
        messagesEs.put("paymentNumber.placeholder", "Seleccione la cobranza que desea reclamar");
        messagesEs.put("paymentNumber.requiredError", "Debe ingresar un nro. de cobranza");

        //Campo - Buscar
        values = new String[]{"search", idForm, formVersion, "selector", "2", "TRUE", "FALSE", null, "0", "0", "default", "0"};
        insert("form_fields", formFields, values);

        values = new String[]{"search", idForm, formVersion, "field-medium", null, "1", "combo"};
        insert("form_field_selector", formFieldSelector, values);

        values = new String[]{"search", idForm, formVersion, "cobranza1"};
        insert("form_field_selector_options", formFieldSelectorOptions, values);
        values = new String[]{"search", idForm, formVersion, "cobranza2"};
        insert("form_field_selector_options", formFieldSelectorOptions, values);

        messagesEs.put("search.label", "Buscar");
        messagesEs.put("search.option.cobranza1", "valor1");
        messagesEs.put("search.option.cobranza2", "valor2");
        messagesEs.put("search.requiredError", "Debe ingresar el nombre del banco avisador");

        //Campo - Mensaje al banco
        values = new String[]{"messageToTheBank", idForm, formVersion, "textarea", "3", "TRUE", "FALSE", null, "0", "0", "default", "0"};
        insert("form_fields", formFields, values);

        values = new String[]{"messageToTheBank", idForm, formVersion, "0", "500", "field-big"};
        insert("form_field_textarea", formFieldTextarea, values);

        messagesEs.put("messageToTheBank.label", "Mensaje al banco");
        messagesEs.put("messageToTheBank.placeholder", "Indique aquí las instrucciones a ser enviadas");

        //Campo - Adjuntos
        values = new String[]{"attachments", idForm, formVersion, "text", "4", "TRUE", "FALSE", null, "0", "1", "default", "0"};
        insert("form_fields", formFields, values);

        values = new String[]{"attachments", idForm, formVersion, "0", "30", "field-medium", null};
        insert("form_field_text", formFieldText, values);

        messagesEs.put("attachments.label", "Adjuntos");

        //Campo - Adjuntar archivo
        values = new String[]{"attachFile", idForm, formVersion, "text", "5", "TRUE", "FALSE", null, "0", "0", "default", "0"};
        insert("form_fields", formFields, values);

        values = new String[]{"attachFile", idForm, formVersion, "0", "40", "field-big", null};
        insert("form_field_text", formFieldText, values);

        messagesEs.put("attachFile.invalidError", "El archivo seleccionado excede el peso permitido, recuerde que no debe superar los 2MB");
        messagesEs.put("attachFile.label", "Adjuntar archivo");

        //Campo - Términos y condiciones
        values = new String[]{"disclaimer", idForm, formVersion, "termsandconditions", "6", "TRUE", "FALSE", null, "0", "0", "default", "0"};
        insert("form_fields", formFields, values);

        values = new String[]{"disclaimer", idForm, formVersion, "field-big", "0", "0"};
        insert("form_field_terms_conditions", formFieldTermsConditions, values);

        messagesEs.put("disclaimer.label", "Términos y condiciones");
        messagesEs.put("disclaimer.termsAndConditions", "Las instrucciones enviadas al banco por usted luego de las <hora de corte> serán procesadas al siguiente día hábil bancario. Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.");

        //Linea horizontal de separación
        values = new String[]{"horizontalLine", idForm, formVersion, "horizontalrule", "7", "TRUE", "FALSE", null, "1", "0", "default", "0"};
        insert("form_fields", formFields, values);
        messagesEs.put("horizontalLine.label", "");

        //Campo - Emails de notificación
        values = new String[]{"notificationEmails", idForm, formVersion, "emaillist", "8", "TRUE", "FALSE", null, "1", "0", "default", "0"};
        insert("form_fields", formFields, values);

        values = new String[]{"notificationEmails", idForm, formVersion, "field-big"};
        insert("form_field_emaillist", formFieldEmaillist, values);

        messagesEs.put("notificationEmails.label", "Emails de notificacion");
        messagesEs.put("notificationEmails.placeholder", "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");

        //Campo - Cuerpo de notificación
        values = new String[]{"notificationBody", idForm, formVersion, "textarea", "9", "TRUE", "FALSE", null, "1", "0", "default", "0"};
        insert("form_fields", formFields, values);

        values = new String[]{"notificationBody", idForm, formVersion, "0", "500", "field-big"};
        insert("form_field_textarea", formFieldTextarea, values);

        messagesEs.put("notificationBody.label", "Cuerpo de notificacion");

        for (String key : messagesEs.keySet()) {
            values = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, formVersion, date, messagesEs.get(key)};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                        + " VALUES ('" + values[0] + "', '" + values[1] + "', '" + values[2] + "', '" + values[3] + "', '" + values[4] + "', TO_DATE('" + values[5] + "', 'YYYY-MM-DD HH24:MI:SS'),'" + values[6] + "')");
            } else {
                insert("form_field_messages", formFieldMessages, values);
            }
        }
    }
}
