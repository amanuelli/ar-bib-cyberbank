/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201109271255_11201 extends DBVSUpdate {

    @Override
    public void up() {
        // Form 14 - Cambio de condiciones de tarjeta

        // Agregamos campos
        String[] fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value"};

        String[] fieldValues = new String[]{"termsAndConditions", "14", "disclaimer", "1", "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per."};

        insert("form_fields", fieldNames, fieldValues);

        // Actualizamos template
        fieldNames = new String[]{"template_es"};
        fieldValues = new String[]{"<form:form>	<form:section>		<form:field idField=\"numeroTarjeta\"/>		<form:field idField=\"marca\"/>		<form:field idField=\"alcanceMaster\"/>		<form:field idField=\"alcanceVisa\"/>		<form:field idField=\"alcanceAmexCenturion\"/>		<form:field idField=\"alcanceAmexBlueBox\"/>		<form:field idField=\"programaMaster\"/>		<form:field idField=\"programaVisa\"/>		<form:field idField=\"programaAmexCenturion\"/>		<form:field idField=\"programaAmexBlueBox\"/>		<form:field idField=\"cierreVisa\"/>		<form:field idField=\"cierreMaster\"/>		<form:field idField=\"cierreAmexCenturion\"/>		<form:field idField=\"cierreAmexBlueBox\"/>		<form:field idField=\"tipoOperacion\"/>		<form:field idField=\"traspasoLimiteCredito\"/>		<form:field idField=\"montoLimiteTraspasar\"/>		<form:field idField=\"limiteDeCredito\"/>	</form:section>	<div class=\"dotted_separator\">&nbsp;</div>	<form:section>		<form:field idField=\"datosLaborales\"/>		<form:field idField=\"empresa\"/>		<form:field idField=\"rubro\"/>		<form:field idField=\"calle\"/>		<form:field idField=\"numeroLaboral\"/>		<form:field idField=\"codigoPostal\"/>		<form:field idField=\"localidad\"/>		<form:field idField=\"profesion\"/>		<form:field idField=\"cargo\"/>		<form:field idField=\"fechaInicio\"/>		<form:field idField=\"telefono\"/>		<form:field idField=\"email\"/>		<form:field idField=\"ingresosNetos\"/>		<form:field idField=\"ingresosOtros\"/>	</form:section><div class=\"dotted_separator\">&nbsp;</div><form:section><form:field idField=\"termsAndConditions\"/></form:section></form:form>"};
        update("forms", fieldNames, fieldValues, "id_form=14");

        // Form 15 - Solicitud de debito automatico en tarjeta

        // Agregamos campos
        fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value"};

        fieldValues = new String[]{"termsAndConditions", "15", "disclaimer", "1", "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per."};

        insert("form_fields", fieldNames, fieldValues);

        // Actualizamos template
        fieldNames = new String[]{"template_es"};
        fieldValues = new String[]{"<form:form>	<form:section>		<form:field idField=\"tipoOperacion\"/>		<form:field idField=\"tarjetaDebito\"/>		<form:field idField=\"marca\"/>		<form:field idField=\"tipoDebitoVisa\"/>		<form:field idField=\"tipoDebitoMaster\"/>		<form:field idField=\"tipoDebitoVisaIMMMont\"/>		<form:field idField=\"modoPagoPatente\"/>		<form:field idField=\"cuentaCorrientePatente\"/>		<form:field idField=\"cuentaCorrienteSaneamiento\"/>		<form:field idField=\"modoPagoContribucion\"/>		<form:field idField=\"cuentaCorrienteContribucion\"/>		<form:field idField=\"tipoDebitoVisaIMMald\"/>		<form:field idField=\"modoPagoPatenteMald\"/>		<form:field idField=\"matriculaPatenteMald\"/>		<form:field idField=\"tarjetaContribucionMald\"/>		<form:field idField=\"localidadContribucionMald\"/>		<form:field idField=\"padronContribucionMald\"/>		<form:field idField=\"apartamentoContribucionMald\"/>		<form:field idField=\"tipoDebitoVisaBSE\"/>		<form:field idField=\"seccionCarpetaBSE\"/>		<form:field idField=\"carpetaCarpetaBSE\"/>		<form:field idField=\"ramoPolizaBSE\"/>		<form:field idField=\"polizaPolizaBSE\"/>		<form:field idField=\"certificadoPolizaBSE\"/>		<form:field idField=\"verificadorPolizaBSE\"/>		<form:field idField=\"modoPagoANEP\"/>		<form:field idField=\"identificadorANEP\"/>		<form:field idField=\"tipoDebitoVisaCJPPU\"/>		<form:field idField=\"numeroAfiliadoCJPPU\"/>		<form:field idField=\"nombreCJPPU\"/>		<form:field idField=\"referenciaCobroUTE\"/>		<form:field idField=\"numeroCuentaANTEL\"/>		<form:field idField=\"tipoDebitoVisaANCEL\"/>		<form:field idField=\"numeroContratoADSLMovil\"/>		<form:field idField=\"numeroContratoFacAncel\"/>		<form:field idField=\"numeroServicioFacAncel\"/>		<form:field idField=\"tipoDebitoVisaMovistar\"/>		<form:field idField=\"numeroMovilmMovAdsl\"/>		<form:field idField=\"numeroClienteMovEmpresas\"/>		<form:field idField=\"rutMovEmpresas\"/>		<form:field idField=\"numeroMovilMovPersonas\"/>		<form:field idField=\"cedulaClienteMovPersonas\"/>		<form:field idField=\"cedulaSocioClaro\"/>		<form:field idField=\"celularClaro\"/>		<form:field idField=\"numeroCuentaClaro\"/>		<form:field idField=\"contratoMasterAncel\"/>		<form:field idField=\"numeroCelularMasterAncel\"/>		<form:field idField=\"titularMasterAncel\"/>		<form:field idField=\"cuentaMasterAntel\"/>		<form:field idField=\"titularMasterAntel\"/>		<form:field idField=\"identificadorMasterAnep\"/>		<form:field idField=\"seccionMasterBse\"/>		<form:field idField=\"carpetaMasterBse\"/>		<form:field idField=\"monedaMasterBse\"/>		<form:field idField=\"afiliadoMasterCJPPU\"/>		<form:field idField=\"nombreMasterCJPPU\"/>		<form:field idField=\"tipoDebitoMasterIMMont\"/>		<form:field idField=\"cuentaCorrientePatenteMontMaster\"/>		<form:field idField=\"matriculaPatenteMontMaster\"/>		<form:field idField=\"modoDePagoPatenteMontMaster\"/>		<form:field idField=\"cuentaCorrienteContribucionMontMaster\"/>		<form:field idField=\"padronContribucionMontMaster\"/>		<form:field idField=\"unidadOtrosContribucionMontMaster\"/>		<form:field idField=\"modoDePagoContribucionMontMaster\"/>		<form:field idField=\"cuentaCorrienteTributoMontMaster\"/>		<form:field idField=\"cuentaCorrienteSaneamientoMontMaster\"/>		<form:field idField=\"tipoDebitoMasterIMMald\"/>		<form:field idField=\"padronPatenteMaldMaster\"/>		<form:field idField=\"matriculaPatenteMaldMaster\"/>		<form:field idField=\"modoDePagoPatenteMaldMaster\"/>		<form:field idField=\"tipoContribucionMaldMaster\"/>		<form:field idField=\"localidadContribucionMaldMaster\"/>		<form:field idField=\"padronContribucionMaldMaster\"/>		<form:field idField=\"unidadUbicacionContribucionMaldMaster\"/>		<form:field idField=\"departamentoInteriorMaster\"/>		<form:field idField=\"tipoDebitoMasterIMInterior\"/>		<form:field idField=\"tipoContribucionInteriorMaster\"/>		<form:field idField=\"localidadContribucionInteriorMaster\"/>		<form:field idField=\"padronContribucionInteriorMaster\"/>		<form:field idField=\"ubicacionContribucionInteriorMaster\"/>		<form:field idField=\"modoDePagoContribucionInteriorMaster\"/>		<form:field idField=\"padronPatenteInteriorMaster\"/>		<form:field idField=\"matriculaPatenteInteriorMaster\"/>		<form:field idField=\"modoDePagoPatenteInteriorMaster\"/>		<form:field idField=\"nombreApellidoVisa\"/>		<form:field idField=\"vencimientoMes\"/>		<form:field idField=\"vencimientoAno\"/>		<form:field idField=\"domicilio\"/>		<form:field idField=\"localidad\"/>		<form:field idField=\"departamento\"/>		<form:field idField=\"telefonoParticular\"/>		<form:field idField=\"telefonoLaboral\"/>		<form:field idField=\"telefonoCelular\"/>	</form:section><div class='dotted_separator'>&nbsp;</div><form:section>		<form:field idField=\"termsAndConditions\"/>	</form:section></form:form>"};
        update("forms", fieldNames, fieldValues, "id_form=15");

        // Form 13 - Solicitud de reimpresion de tarjeta

        // Agregamos campos
        fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value"};

        fieldValues = new String[]{"termsAndConditions", "13", "disclaimer", "1", "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per."};

        insert("form_fields", fieldNames, fieldValues);

        // Actualizamos template
        fieldNames = new String[]{"template_es"};
        fieldValues = new String[]{"<form:form>	<form:section>		<form:field idField=\"tarjeta\"/>		<form:field idField=\"motivoDeLaSolicitud\"/>		<form:field idField=\"nombres\"/>		<form:field idField=\"apellidos\"/>	</form:section><div class=\"dotted_separator\">&nbsp;</div><form:section>		<form:field idField=\"termsAndConditions\"/>	</form:section>	<div class=\"dotted_separator\">&nbsp;</div>	<form:section>		<form:field idField=\"advertencia1\"/>	</form:section>	<div class=\"dotted_separator\">&nbsp;</div>	<form:section>		<form:field idField=\"advertencia2\"/>	</form:section></form:form>"};
        update("forms", fieldNames, fieldValues, "id_form=13");

        // Form 17 - Solicitud de tarjeta de credito

        // Agregamos campos
        fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value"};

        fieldValues = new String[]{"termsAndConditions", "17", "disclaimer", "1", "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per."};

        insert("form_fields", fieldNames, fieldValues);

        // Actualizamos template
        fieldNames = new String[]{"template_es"};
        fieldValues = new String[]{"<form:form>	<form:section>		<form:field idField=\"marca\"/>		<form:field idField=\"tipoAmericanExpressBlueBox\"/>		<form:field idField=\"tipoAmericanExpressCenturion\"/>		<form:field idField=\"tipoMasterCard\"/>		<form:field idField=\"tipoVisa\"/>		<form:field idField=\"programaAmericanExpressBlueBox\"/>		<form:field idField=\"programaAmericanExpressCenturion\"/>		<form:field idField=\"programaMasterCard\"/>		<form:field idField=\"programaVisa\"/>	</form:section>	<div class=\"dotted_separator\">&nbsp;</div>	<form:section>				<form:field idField=\"subtituloDatosPersonalesDelTitular\"/>		<form:field idField=\"telefonoDeContacto\"/>		<form:field idField=\"celular\"/>		<form:field idField=\"vivienda\"/>		<form:field idField=\"cuotaHipoteca\"/>		<form:field idField=\"alquiler\"/>		<form:field idField=\"personasACargo\"/>		<form:field idField=\"numeroDeHijos\"/>	</form:section>	<div class=\"dotted_separator\">&nbsp;</div>	<form:section>				<form:field idField=\"subtituloDatosPersonalesAdicional\"/>		<form:field idField=\"apellidos\"/>		<form:field idField=\"nombres\"/>		<form:field idField=\"calle\"/>		<form:field idField=\"numero\"/>		<form:field idField=\"apartamento\"/>		<form:field idField=\"codigoPostal\"/>		<form:field idField=\"localidadDepartamento\"/>		<form:field idField=\"pais\"/>		<form:field idField=\"residente\"/>		<form:field idField=\"telefonoAdicional\"/>		<form:field idField=\"celularAdicional\"/>		<form:field idField=\"faxAdicional\"/>		<form:field idField=\"email\"/>		<form:field idField=\"1_fechaNacimiento\"/>		<form:field idField=\"lugarNacimiento\"/>		<form:field idField=\"nacionalidadAdicional\"/>		<form:field idField=\"viviendaAdicional\"/>		<form:field idField=\"cuotaHipotecaAdicional\"/>		<form:field idField=\"alquilerAdicional\"/>		<form:field idField=\"estadoCivilAdicional\"/>		<form:field idField=\"2_fechaEstadoCivilAdicional\"/>		<form:field idField=\"sexoAdicional\"/>		<form:field idField=\"personasACargoAdicional\"/>		<form:field idField=\"numeroHijosAdicional\"/>	</form:section>	<div class=\"dotted_separator\">&nbsp;</div>	<form:section>				<form:field idField=\"subtituloDatosLaboralesDelAdicional\"/>		<form:field idField=\"empresaDatosLaboralesAdicional\"/>		<form:field idField=\"rubroActividadDatosLaboralesAdicional\"/>		<form:field idField=\"domicilioDatosLaboralesAdicional\"/>		<form:field idField=\"numeroDatosLaboralesAdicional\"/>		<form:field idField=\"apartamentoDatosLaboralesAdicional\"/>		<form:field idField=\"codigoPostalDatosLaboralesAdicional\"/>		<form:field idField=\"localidadDepartamentoDatosLaboralesAdicional\"/>		<form:field idField=\"profesionDatosLaboralesAdicional\"/>		<form:field idField=\"cargoFuncionDatosLaboralesAdicional\"/>		<form:field idField=\"3_fechaInicio\"/>		<form:field idField=\"telefonoDatosLaboralesAdicional\"/>		<form:field idField=\"emailDatosLaboralesAdicional\"/>		<form:field idField=\"ingresosNetosMensuales\"/>		<form:field idField=\"otrosIngresos\"/>	</form:section>	<div class=\"dotted_separator\">&nbsp;</div>	<form:section>				<form:field idField=\"subtituloDatosPersonalesDelConyugeDelAdiciona\"/>		<form:field idField=\"apellidosConyugueAdicional\"/>		<form:field idField=\"nombresConyugueAdicional\"/>		<form:field idField=\"tipoYNumeroDocumentoConyugueAdicional\"/>		<form:field idField=\"telefonoConyugueAdicional\"/>		<form:field idField=\"celularConyugueAdicional\"/>		<form:field idField=\"emailConyugueAdicional\"/>		<form:field idField=\"4_fechaDeNacimientoConyugueAdicional\"/>		<form:field idField=\"lugarConyugueAdicional\"/>		<form:field idField=\"nacionalidadConyugueAdicional\"/>	</form:section><div class='dotted_separator'>&nbsp;</div><form:section>		<form:field idField=\"termsAndConditions\"/>	</form:section></form:form>"};
        update("forms", fieldNames, fieldValues, "id_form=17");

    }
}