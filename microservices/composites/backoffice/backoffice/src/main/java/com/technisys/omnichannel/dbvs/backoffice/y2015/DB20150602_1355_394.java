/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150602_1355_394 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("frontend.shortDateFormat", "dd/MM/yyyy", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("frontend.mediumDateFormat", "dd/MM/yyyy HH:mm", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("frontend.timeFormat", "HH:mm", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
    }
}