/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201305241338_200 extends DBVSUpdate {

    private static final String[] MESSAGES_FIELDS = new String[]{"id_message", "value", "lang", "id_group"};

    @Override
    public void up() {
        update("activities", new String[]{"id_permission_required"}, new String[]{"rub.authenticated"}, "id_activity='rub.loans.exportLoansMovementsList'");

    }
}