/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * http://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2741
 *
 * @author fmarichal
 */
public class DB20170725_1439_2741 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "creditLetterTransfer";

        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};
        String[] formFieldsText = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        String[] formFieldsValues;
        String[] formFieldsTextValues;
        String idField;

        final String descForm = "Transferencia de carta de crédito";

        String version = "1";
        final String sTrue = "1";
        int ordinal = 1;

        Map<String, String> messages = new HashMap();

        /**
         * ** Insert Form - Transferencia de carta de crédito ***
         */
        String[] formsFields = new String[]{"id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable"};
        String[] formsFieldsValues = new String[]{idForm, version, sTrue, "comex", "process", "demo:1:3", "comex", null, sTrue, sTrue, sTrue};
        insert("forms", formsFields, formsFieldsValues);

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" + idForm + "', '" + version + "', 'es', '" + descForm + "', TO_DATE('2017-07-27 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            /* es */
            String[] formMessageFields_ES = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields_ES, new String[]{"forms." + idForm + ".formName", idForm, version, "es", descForm, date});
        }

        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "accessToken-pin"});

        /**
         * ** Insert field text - Nro. de operación ***
         */
        formFieldsValues = new String[]{"operationNumber", idForm, version, "text", Integer.toString(ordinal), "TRUE", "TRUE", null, "1", "0", "default", "0"};
        formFieldsTextValues = new String[]{"operationNumber", idForm, version, "0", "30", "field-medium", "onlyNumbers"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field text - Segundo beneficiario ***
         */
        formFieldsValues = new String[]{"secondBeneficiary", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        formFieldsTextValues = new String[]{"secondBeneficiary", idForm, version, "0", "60", "field-big", "withoutSpecialChars"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field text - Dirección completa ***
         */
        formFieldsValues = new String[]{"completeAddress", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        formFieldsTextValues = new String[]{"completeAddress", idForm, version, "0", "150", "field-big", "withoutSpecialChars"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field text - Persona de contacto ***
         */
        formFieldsValues = new String[]{"contactPerson", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        formFieldsTextValues = new String[]{"contactPerson", idForm, version, "0", "35", "field-big", "withoutSpecialChars"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field text - Teléfono ***
         */
        formFieldsValues = new String[]{"phone", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        formFieldsTextValues = new String[]{"phone", idForm, version, "0", "35", "field-big", "onlyNumbers"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field text - Swift del banco del 2do beneficiario ***
         */
        formFieldsValues = new String[]{"swiftFromSecondBeneficiaryBank", idForm, version, "text", Integer.toString(ordinal), "TRUE", "TRUE", null, "1", "0", "default", "0"};
        formFieldsTextValues = new String[]{"swiftFromSecondBeneficiaryBank", idForm, version, "0", "50", "field-big", "withoutSpecialChars"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field bankSelector - Buscar banco ***
         */
        idField = "searchBank";
        formFieldsValues = new String[]{idField, idForm, version, "bankselector", Integer.toString(ordinal), "TRUE", "TRUE", null, "1", "0", "foreigners", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_bankselector", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{idField, idForm, version, "field-big"});
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Buscar banco')");

        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Buscar banco"});
        }
        ordinal++;

        /**
         * ** Insert field text - Nombre del banco del 2do beneficiario ***
         */
        formFieldsValues = new String[]{"nameSecondBeneficiaryBank", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        formFieldsTextValues = new String[]{"nameSecondBeneficiaryBank", idForm, version, "0", "50", "field-big", "withoutSpecialChars"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field text - Dirección del banco del 2do beneficiario ***
         */
        formFieldsValues = new String[]{"addressBeneficiaryBank", idForm, version, "text", Integer.toString(ordinal), "TRUE", "TRUE", null, "1", "0", "default", "0"};
        formFieldsTextValues = new String[]{"addressBeneficiaryBank", idForm, version, "0", "200", "field-big", "withoutSpecialChars"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field text - Cuenta del 2do beneficiario ***
         */
        formFieldsValues = new String[]{"secondBeneficiaryAccount", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        formFieldsTextValues = new String[]{"secondBeneficiaryAccount", idForm, version, "0", "50", "field-big", "withoutSpecialChars"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field amount - Importe a transferir ***
         */
        formFieldsValues = new String[]{"amountToTransfer", idForm, version, "amount", Integer.toString(ordinal), "TRUE", "TRUE", null, "1", "0", "default", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "id_ps_currency_check", "control_limits", "control_limit_over_product", "use_for_total_amount"}, new String[]{"amountToTransfer", idForm, version, "field-normal", null, "0", null, "1"});
        ordinal++;

        /**
         * ** Insert field date - Nueva fecha de vencimiento ***
         */
        insert("form_fields", formFields, new String[]{"newExpirationDate", idForm, version, "date",
            Integer.toString(ordinal), "TRUE", "TRUE", null, "1", "0", "default", "0"});
        insert("form_field_date", new String[]{"id_field", "id_form", "form_version", "display_type"},
                new String[]{"newExpirationDate", idForm, version, "field-normal"});
        ordinal++;

        /**
         * ** Insert field date - Fecha vto. embarque ***
         */
        insert("form_fields", formFields, new String[]{"newBoardingDate", idForm, version, "date",
            Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"});
        insert("form_field_date", new String[]{"id_field", "id_form", "form_version", "display_type"},
                new String[]{"newBoardingDate", idForm, version, "field-normal"});
        ordinal++;

        /**
         * ** Insert field text - Plazo presentación de documentos ***
         */
        formFieldsValues = new String[]{"deadlineSubmissionDocuments", idForm, version, "text", Integer.toString(ordinal), "TRUE", "TRUE", null, "1", "0", "default", "0"};
        formFieldsTextValues = new String[]{"deadlineSubmissionDocuments", idForm, version, "0", "2", "field-smaller", "onlyNumbers"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field textarea - Cant. mercadería y precio unitario ***
         */
        insert("form_fields", formFields, new String[]{"quantityMerchandiseAndUnitPrice", idForm, version, "textarea",
            Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"});
        insert("form_field_textarea",
                new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"},
                new String[]{"quantityMerchandiseAndUnitPrice", idForm, version, "0", "500", "field-big"});
        ordinal++;

        /**
         * ** Insert field text - Archivos adjuntos ***
         */
        formFieldsValues = new String[]{"attachedFiles", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        formFieldsTextValues = new String[]{"attachedFiles", idForm, version, "0", "50", "field-normal", null};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field text - Adjuntar archivo link ***
         */
        formFieldsValues = new String[]{"attachFileLink", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        formFieldsTextValues = new String[]{"attachFileLink", idForm, version, "0", "50", "field-normal", null};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        /**
         * ** Insert field product selector - Gastos de transferencia del
         * crédito ***
         */
        formFieldsValues = new String[]{"creditTransferExpenses", idForm, version, "selector", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"}, new String[]{"creditTransferExpenses", idForm, version, "field-normal", "creditTransferExpenses1", "0", "combo"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"creditTransferExpenses", idForm, version, "creditTransferExpenses1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"creditTransferExpenses", idForm, version, "creditTransferExpenses2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"creditTransferExpenses", idForm, version, "creditTransferExpenses3"});
        ordinal++;

        /**
         * ** Insert field product selector - Gastos de confirmación ***
         */
        formFieldsValues = new String[]{"confirmationExpenses", idForm, version, "selector", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"}, new String[]{"confirmationExpenses", idForm, version, "field-normal", "confirmationExpenses1", "0", "combo"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"confirmationExpenses", idForm, version, "confirmationExpenses1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"confirmationExpenses", idForm, version, "confirmationExpenses2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"confirmationExpenses", idForm, version, "confirmationExpenses3"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"confirmationExpenses", idForm, version, "confirmationExpenses4"});
        ordinal++;

        /**
         * ** Insert field product selector - Vuestros otros gastos ***
         */
        formFieldsValues = new String[]{"yourOtherExpenses", idForm, version, "selector", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"}, new String[]{"yourOtherExpenses", idForm, version, "field-normal", "yourOtherExpenses1", "0", "combo"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"yourOtherExpenses", idForm, version, "yourOtherExpenses1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"yourOtherExpenses", idForm, version, "yourOtherExpenses2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"yourOtherExpenses", idForm, version, "yourOtherExpenses3"});
        ordinal++;

        /**
         * ** Insert field product selector - Gastos del banco emisor ***
         */
        formFieldsValues = new String[]{"expensesIssuingBank", idForm, version, "selector", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"}, new String[]{"expensesIssuingBank", idForm, version, "field-normal", "expensesIssuingBank1", "0", "combo"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"expensesIssuingBank", idForm, version, "expensesIssuingBank1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"expensesIssuingBank", idForm, version, "expensesIssuingBank2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"expensesIssuingBank", idForm, version, "expensesIssuingBank3"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"expensesIssuingBank", idForm, version, "expensesIssuingBank4"});
        ordinal++;

        /**
         * ** Insert field product selector - Gastos de otros bancos ***
         */
        formFieldsValues = new String[]{"banksExpenses", idForm, version, "selector", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"}, new String[]{"banksExpenses", idForm, version, "field-normal", "banksExpenses1", "0", "combo"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"banksExpenses", idForm, version, "banksExpenses1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"banksExpenses", idForm, version, "banksExpenses2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"banksExpenses", idForm, version, "banksExpenses3"});
        ordinal++;

        /**
         * ** Insert field textarea - Instrucciones especiales ***
         */
        insert("form_fields", formFields, new String[]{"specialInstructions", idForm, version, "textarea",
            Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "0", "default", "0"});
        insert("form_field_textarea",
                new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"},
                new String[]{"specialInstructions", idForm, version, "0", "500", "field-big"});
        ordinal++;

        /**
         * ** Insert field text - Disclaimer ***
         */
        formFieldsValues = new String[]{"disclaimer", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", null, "0", "1", "default", "0"};
        formFieldsTextValues = new String[]{"disclaimer", idForm, version, "0", "200", "field-big", null};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        ordinal++;

        messages.put("addressBeneficiaryBank.help", null);
        messages.put("addressBeneficiaryBank.hint", null);
        messages.put("addressBeneficiaryBank.label", "Dirección del banco del 2do beneficiario");
        messages.put("addressBeneficiaryBank.requiredError", "Debe ingresar el swift y/o la dirección del banco");
        messages.put("amountToTransfer.help", null);
        messages.put("amountToTransfer.hint", null);
        messages.put("amountToTransfer.invalidError", "Debe mantener la misma moneda que el importe original de la carta de crédito y debe ser menor al importe de la misma.");
        messages.put("amountToTransfer.label", "Importe a transferir");
        messages.put("amountToTransfer.requiredError", "Debe ingresar un importe a transferir");
        messages.put("attachedFiles.help", null);
        messages.put("attachedFiles.hint", null);
        messages.put("attachedFiles.label", "Archivos adjuntos");
        messages.put("attachedFiles.placeholder", "No ha adjuntado ningún archivo. Máximo 2 MB");
        messages.put("attachFileLink.help", null);
        messages.put("attachFileLink.hint", null);
        messages.put("attachFileLink.label", "Adjuntar archivo link");
        messages.put("banksExpenses.help", null);
        messages.put("banksExpenses.hint", null);
        messages.put("banksExpenses.label", "Gastos de otros bancos");
        messages.put("banksExpenses.option.banksExpenses1", "Primer beneficiario");
        messages.put("banksExpenses.option.banksExpenses2", "Segundo beneficiario");
        messages.put("banksExpenses.option.banksExpenses3", "Otro");
        messages.put("completeAddress.help", null);
        messages.put("completeAddress.hint", null);
        messages.put("completeAddress.invalidError", "Debe ingresar un texto alfanumérico de largo máximo 150 caracteres");
        messages.put("completeAddress.label", "Dirección completa");
        messages.put("completeAddress.requiredError", "Debe ingresar una dirección");
        messages.put("confirmationExpenses.help", null);
        messages.put("confirmationExpenses.hint", null);
        messages.put("confirmationExpenses.label", "Gastos de confirmación");
        messages.put("confirmationExpenses.option.confirmationExpenses1", "Primer beneficiario");
        messages.put("confirmationExpenses.option.confirmationExpenses2", "Segundo beneficiario");
        messages.put("confirmationExpenses.option.confirmationExpenses3", "Ordenante");
        messages.put("confirmationExpenses.option.confirmationExpenses4", "Otro");
        messages.put("contactPerson.help", null);
        messages.put("contactPerson.hint", null);
        messages.put("contactPerson.invalidError", "Debe ingresar un texto alfanumérico de largo máximo 35 caracteres");
        messages.put("contactPerson.label", "Persona de contacto");
        messages.put("creditTransferExpenses.help", null);
        messages.put("creditTransferExpenses.hint", null);
        messages.put("creditTransferExpenses.label", "Gastos de transferencia del crédito");
        messages.put("creditTransferExpenses.option.creditTransferExpenses1", "Primer beneficiario");
        messages.put("creditTransferExpenses.option.creditTransferExpenses2", "Segundo beneficiario");
        messages.put("creditTransferExpenses.option.creditTransferExpenses3", "Otro");
        messages.put("deadlineSubmissionDocuments.help", null);
        messages.put("deadlineSubmissionDocuments.hint", null);
        messages.put("deadlineSubmissionDocuments.invalidError", "El largo máximo es de 2 dígitos");
        messages.put("deadlineSubmissionDocuments.label", "Plazo presentación de documentos");
        messages.put("deadlineSubmissionDocuments.requiredError", "Debe ingresar el plazo de presentación de documentos");
        messages.put("disclaimer.help", null);
        messages.put("disclaimer.hint", "Las instrucciones enviadas al banco por usted luego de las . serán procesadas al siguiente día hábil bancario.\nAutorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.");
        messages.put("disclaimer.label", "Disclaimer");
        messages.put("expensesIssuingBank.help", null);
        messages.put("expensesIssuingBank.hint", null);
        messages.put("expensesIssuingBank.label", "Gastos del banco emisor");
        messages.put("expensesIssuingBank.option.expensesIssuingBank1", "Primer beneficiario");
        messages.put("expensesIssuingBank.option.expensesIssuingBank2", "Segundo beneficiario");
        messages.put("expensesIssuingBank.option.expensesIssuingBank3", "Ordenante");
        messages.put("expensesIssuingBank.option.expensesIssuingBank4", "Otro");
        messages.put("nameSecondBeneficiaryBank.help", null);
        messages.put("nameSecondBeneficiaryBank.hint", null);
        messages.put("nameSecondBeneficiaryBank.invalidError", "Debe ingresar un texto alfanumérico de largo máximo 160 caracteres");
        messages.put("nameSecondBeneficiaryBank.label", "Nombre del banco del 2do beneficiario");
        messages.put("newBoardingDate.help", null);
        messages.put("newBoardingDate.hint", null);
        messages.put("newBoardingDate.label", "Nueva fecha embarque");
        messages.put("newExpirationDate.help", null);
        messages.put("newExpirationDate.hint", null);
        messages.put("newExpirationDate.label", "Nueva fecha de vencimiento");
        messages.put("newExpirationDate.requiredError", "La fecha no es válida");
        messages.put("operationNumber.help", "Seleccione la operación para la que desea presentar documentos");
        messages.put("operationNumber.hint", null);
        messages.put("operationNumber.invalidError", "Debe ingresar un número con menos de 30 dígitos");
        messages.put("operationNumber.label", "Nro. de operación");
        messages.put("operationNumber.requiredError", "Este campo es obligatorio");
        messages.put("phone.help", null);
        messages.put("phone.hint", null);
        messages.put("phone.invalidError", "Debe ingresar un número de largo máximo 35 dígitos");
        messages.put("phone.label", "Teléfono");
        messages.put("quantityMerchandiseAndUnitPrice.help", null);
        messages.put("quantityMerchandiseAndUnitPrice.hint", null);
        messages.put("quantityMerchandiseAndUnitPrice.invalidError", "Alfanumérico de largo máximo 500 caracteres.");
        messages.put("quantityMerchandiseAndUnitPrice.label", "Cant. mercadería y precio unitario");
        messages.put("secondBeneficiary.help", "Ingrese el nombre completo");
        messages.put("secondBeneficiary.hint", null);
        messages.put("secondBeneficiary.invalidError", "Debe ingresar un texto alfanumérico de largo máximo 60 caracteres");
        messages.put("secondBeneficiary.label", "Segundo beneficiario");
        messages.put("secondBeneficiary.requiredError", "Debe ingresar un segundo beneficiario");
        messages.put("secondBeneficiaryAccount.help", null);
        messages.put("secondBeneficiaryAccount.hint", null);
        messages.put("secondBeneficiaryAccount.invalidError", "Debe ingresar un texto alfanumérico de largo máximo 50 caracteres");
        messages.put("secondBeneficiaryAccount.label", "Cuenta del 2do beneficiario");
        messages.put("specialInstructions.help", null);
        messages.put("specialInstructions.hint", null);
        messages.put("specialInstructions.invalidError", "Debe ingresar un texto alfanumérico de largo máximo 500 caracteres");
        messages.put("specialInstructions.label", "Instrucciones especiales");
        messages.put("swiftFromSecondBeneficiaryBank.help", null);
        messages.put("swiftFromSecondBeneficiaryBank.hint", null);
        messages.put("swiftFromSecondBeneficiaryBank.label", "Swift del banco del 2do beneficiario");
        messages.put("swiftFromSecondBeneficiaryBank.requiredError", "Debe ingresar el swift y/o la dirección del banco");
        messages.put("yourOtherExpenses.help", null);
        messages.put("yourOtherExpenses.hint", null);
        messages.put("yourOtherExpenses.label", "Vuestros otros gastos");
        messages.put("yourOtherExpenses.option.yourOtherExpenses1", "Primer beneficiario");
        messages.put("yourOtherExpenses.option.yourOtherExpenses2", "Segundo beneficiario");
        messages.put("yourOtherExpenses.option.yourOtherExpenses3", "Otro");

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, date, messages.get(key)};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }
}
