/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150807_1704_665 extends DBVSUpdate {

    @Override
    public void up() {
       
        insertOrUpdateConfiguration("changeEmail.mail.validity", "24h", ConfigurationGroup.TECNICAS, "others", new String[]{"notEmpty"});
        
        insertActivity("preferences.userData.updateMail.preview", "com.technisys.omnichannel.client.activities.preferences.userdata.UpdateMailPreviewActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.userData.updateMail.send", "com.technisys.omnichannel.client.activities.preferences.userdata.UpdateMailActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "user.preferences");
        insertActivity("preferences.userData.updateMail.confirm", "com.technisys.omnichannel.client.activities.preferences.userdata.ConfirmUpdateMailActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}
