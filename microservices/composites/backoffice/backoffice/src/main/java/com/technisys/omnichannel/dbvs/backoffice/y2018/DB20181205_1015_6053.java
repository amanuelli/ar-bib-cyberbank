package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

public class DB20181205_1015_6053 extends DBVSUpdate {

    @Override
    public void up() {
        String idActivity = "pay.multiline.salary.send";

        insert("permissions", new String[]{"id_permission"}, new String[]{"pay.multiline.salaryPayment"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"pay.multiline.salaryPayment", "accessToken-pin"});

        insertActivity(idActivity, "com.technisys.omnichannel.client.activities.pay.multiline.PaySalarySendActivity", "multilinePayments", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, "pay.multiline.salaryPayment", "debitAccount");
        insertActivity("pay.multiline.salary.preview", "com.technisys.omnichannel.client.activities.pay.multiline.PaySalaryPreviewActivity", "multilinePayments", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

        String[] formFields = new String[]{"id_activity", "id_field_amount", "id_field_product"};
        String[] formFieldsValues = new String[]{idActivity, "file", "debitAccount"};
        insert("activity_caps", formFields, formFieldsValues);
    }
}