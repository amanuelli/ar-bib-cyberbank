/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4020
 *
 * @author rdosantos
 */
public class DB20180711_1640_4020 extends DBVSUpdate {

    @Override
    public void up() {
        
        String idForm = "cashAdvance";
        String[] formFields = new String[] {"ordinal"};
        String[] formFieldsValues;
        String where;
        /**
         * ** Field product selector - Tarjeta ***
         */
        formFieldsValues = new String[] {"1"};
        where = String.format("id_form='%s' and id_field='%s'", idForm, "creditcard" );
        update ("form_fields", formFields, formFieldsValues, where);
        
        /**
         * ** Field product selector - cuenta Destino ***
         */
        formFieldsValues = new String[] {"2"};
        where = String.format("id_form='%s' and id_field='%s'", idForm, "creditAccount" );
        update ("form_fields", formFields, formFieldsValues, where);
        
        /**
         * ** Field amount - Monto ***
         */
        formFieldsValues = new String[] {"3"};
        where = String.format("id_form='%s' and id_field='%s'", idForm, "amount" );
        update ("form_fields", formFields, formFieldsValues, where);
        
        /**
         * ** Field selector - cuotas ***
         */
        formFieldsValues = new String[] {"4"};
        where = String.format("id_form='%s' and id_field='%s'", idForm, "installments" );
        update ("form_fields", formFields, formFieldsValues, where);
        
        /**
         * ** Delete field text - referencia ***
         */
        where = String.format("id_form='%s' and id_field='%s'", idForm, "reference" );
        delete ("form_fields", where);
        delete ("form_field_text", where);
        where = String.format("id_form='%s' and id_field='%s'", idForm, "reference" );
        delete ("form_field_messages",where);
    }

}