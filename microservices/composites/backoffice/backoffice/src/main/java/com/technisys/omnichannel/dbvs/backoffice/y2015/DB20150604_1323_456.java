/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author alfredo
 */
public class DB20150604_1323_456 extends DBVSUpdate {

    @Override
    public void up() {
       insertActivity("loans.read", "com.technisys.omnichannel.client.activities.loans.ReadLoanActivity", "loans", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idLoan");
       insertActivity("loans.listStatements", "com.technisys.omnichannel.client.activities.loans.ListStatementsActivity", "loans", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idLoan");
    }
}
