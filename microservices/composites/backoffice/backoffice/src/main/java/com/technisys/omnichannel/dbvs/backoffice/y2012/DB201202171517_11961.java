/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201202171517_11961 extends DBVSUpdate {

    @Override
    public void up() {

        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT client_desktop_widgets ON");
        String[] fieldNames = new String[]{"id", "name", "uri", "default_column", "default_row"};
        String[] fieldValues = new String[]{"12", "pymes", "/widgets/pymes.jsp", "1", "7"};
        insert("client_desktop_widgets", fieldNames, fieldValues);
        customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT client_desktop_widgets OFF");

    }
}