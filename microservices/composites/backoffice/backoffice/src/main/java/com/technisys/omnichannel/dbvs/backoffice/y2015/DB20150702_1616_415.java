/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150702_1616_415 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("core.masterCurrency", "000");
        insertOrUpdateConfiguration("frontend.channels.enabledFrequencies", "daily|monthly", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});

    }
}
