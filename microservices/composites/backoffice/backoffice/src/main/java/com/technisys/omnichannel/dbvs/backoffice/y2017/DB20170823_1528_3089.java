/* 
 * Copyright 2017 Manentia Software. 
 * 
 * This software component is the intellectual property of Manentia Software S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * http://www.manentiasoftware.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-3089
 *
 * @author ldurand
 */
public class DB20170823_1528_3089 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "requestOfManagementCheck";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());//secondSurname.requiredError

        update("forms", new String[]{"schedulable", "templates_enabled", "drafts_enabled"}, new String[]{"0", "1", "1"}, "id_form = '" + idForm + "'");
        
        String [] formFieldsValues = new String[]{"fields.requestOfManagementCheck.nameOfBeneficiary.requiredError", "es", "nameOfBeneficiary", idForm, "1", "Debe ingresar nombre del beneficiario", date};
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('"+ formFieldsValues[6] + "', 'YYYY-MM-DD HH24:MI:SS'))");
            customSentence(DBVS.DIALECT_ORACLE, "UPDATE form_field_messages SET modification_date = TO_DATE('"+ date + "', 'YYYY-MM-DD HH24:MI:SS') , value = 'Debe ingresar un nombre' WHERE id_message = '" + "fields." + idForm + ".name.requiredError' AND lang = 'es'");
            customSentence(DBVS.DIALECT_ORACLE, "UPDATE form_field_messages SET modification_date = TO_DATE('"+ date + "', 'YYYY-MM-DD HH24:MI:SS') , value = 'Debe ingresar un apellido' WHERE id_message = '" + "fields." + idForm + ".surname.requiredError' AND lang = 'es'");
        }else{
            insert("form_field_messages", new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"}, formFieldsValues);
            update("form_field_messages", new String[]{"modification_date", "value"}, new String[]{date, "Debe ingresar un nombre"}, "id_message = '" + "fields." + idForm + ".name.requiredError' AND lang = 'es'");
            update("form_field_messages", new String[]{"modification_date", "value"}, new String[]{date, "Debe ingresar un apellido"}, "id_message = '" + "fields." + idForm + ".surname.requiredError' AND lang = 'es'");
        }
        
        update("form_fields", new String[]{"required"}, new String[]{"FALSE"}, "id_form = '" + idForm + "' AND id_field = 'secondSurname'");

        delete("form_field_messages", "id_message = '" + "fields." + idForm + ".secondSurname.requiredError' AND lang = 'es'");

    }

}
