/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201109091607_11191 extends DBVSUpdate {

    @Override
    public void up() {

        update("forms", new String[]{"template_es"}, new String[]{"<form:form>	<form:section>		<form:field idField='valorNetoDelBien'/>		<form:field idField='incluyeSeguro'/>		<form:field idField='capitalAFinanciar'/>		<form:field idField='plazo'/>		<form:field idField='periodicidadDePago'/>		<form:field idField='informacionAdicional'/>	</form:section></form:form>"}, "id_form = 4");

    }
}