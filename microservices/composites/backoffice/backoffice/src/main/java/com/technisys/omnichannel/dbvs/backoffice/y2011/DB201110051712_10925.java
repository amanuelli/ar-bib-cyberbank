/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Agrega el mensaje de ocultar teclado virtual en el login.
 *
 * @author grosso
 */
public class DB201110051712_10925 extends DBVSUpdate {

    @Override
    public void up() {
       
        update("configuration", new String[]{"value"}, new String[]{"[A-Za-z0-9ñÑ_]*"}, "id_field = 'rubicon.userconfiguration.password.pattern'");
    }
}
