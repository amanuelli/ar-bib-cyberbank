/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150904_1231_860 extends DBVSUpdate {

    @Override
    public void up() {
       
        insertActivity("session.recoverPinWithSecondFactor.step1", "com.technisys.omnichannel.client.activities.recoverpin.RecoverPinStep1Activity", "recoverPin", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.loginWithPassword");
        insertActivity("session.recoverPin.step2", "com.technisys.omnichannel.client.activities.recoverpin.RecoverPinStep2Activity", "recoverPin", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("session.recoverPin.step3", "com.technisys.omnichannel.client.activities.recoverpin.RecoverPinStep3Activity", "recoverPin", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        
        insertOrUpdateConfiguration("core.activities.withSecretFields", "preferences.changepassword.preview|preferences.changepassword.send|preferences.changepin.preview|preferences.changepin.send", ConfigurationGroup.OTHER, "other", new String[]{});
    }
}
