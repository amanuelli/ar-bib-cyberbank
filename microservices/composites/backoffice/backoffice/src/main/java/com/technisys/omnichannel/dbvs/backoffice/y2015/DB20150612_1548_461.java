/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150612_1548_461 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("messages.communicationsPerPage", "10", ConfigurationGroup.NEGOCIO, "communications", new String[]{"notEmpty", "integer"});
        
        insertActivity("communications.list", "com.technisys.omnichannel.client.activities.communications.ListCommunicationsActivity", "communications", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("communications.listPre", "com.technisys.omnichannel.client.activities.communications.ListCommunicationsPreActivity", "communications", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}
