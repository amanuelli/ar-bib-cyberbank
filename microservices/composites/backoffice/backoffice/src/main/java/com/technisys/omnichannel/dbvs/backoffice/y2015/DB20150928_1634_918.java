/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150928_1634_918 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_messages", new String[]{"value"}, new String[]{"Teléfono fijo"}, "id_message = 'fields.modifyUserData.phoneNumber.label'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Ingrese su teléfono fijo"}, "id_message = 'fields.modifyUserData.phoneNumber.placeholder'");
        
        deleteActivity("preferences.userData.updateMail.preview");
        update("activity_products", new String[]{"id_permission"}, new String[]{"core.authenticated"}, "id_activity = 'preferences.userData.updateMail.send'");
    
        insertOrUpdateConfiguration("changeMobilePhone.sms.validity", "24h", ConfigurationGroup.TECNICAS, "others", new String[]{"notEmpty"});
        insertActivity("preferences.userData.updateMobilePhone.send", "com.technisys.omnichannel.client.activities.preferences.userdata.UpdateMobilePhoneActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}
