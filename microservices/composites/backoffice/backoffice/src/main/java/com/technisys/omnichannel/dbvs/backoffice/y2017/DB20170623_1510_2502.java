/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author rmedina
 */
public class DB20170623_1510_2502 extends DBVSUpdate{

    @Override
    public void up() {
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String idForm = "requestEndorsement";
        String version = "1";
        Map<String, String> messages = new HashMap();
        
        delete("forms", "id_form='" + idForm + "'");
        delete("form_messages", "id_form='" + idForm + "'");
        delete("form_fields", "id_form='" + idForm + "'");
        delete("form_field_text", "id_form='" + idForm + "'");
        delete("form_field_messages", "id_form='" + idForm + "'");
        delete("form_field_textarea", "id_form='" + idForm + "'");
        delete("form_field_selector", "id_form='" + idForm + "'");
        delete("form_field_selector_options", "id_form='" + idForm + "'");
        delete("form_field_emaillist", "id_form='" + idForm + "'");
        delete("form_field_date", "id_form='" + idForm + "'");
        delete("form_field_terms_conditions", "id_form='" + idForm + "'");
        
        
        String[] forms = new String[]{"id_form", "version", "enabled", "category", "type", "admin_option", "id_activity", "last", "deleted", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow", "id_bpm_process"};
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};
        String[] formFieldsText = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        String[] formFieldsTextArea = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        String[] formFieldsSelector = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"};
        String[] formFieldsSelectorOption = new String[]{"id_field", "id_form", "form_version", "value"};
        String[] formFieldsEmailList = new String[]{"id_field", "id_form", "form_version", "display_type"};
        String[] formFieldsTermsConditions = new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"};
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        String[] formsValues, formFieldsValues, formFieldsTextValues, formFieldsTextAreaValues, formFieldsSelectorValues, 
                formFieldsSelectorOptionValues, formFieldsEmailListValues, formFieldsDateValues, formFieldsTermsConditionsValues;
        
        formsValues = new String[]{idForm, version, "1", "comex", "process", null, null, "1", "0", "1", "1", "1", "1", "1", "demo:1:3"};
        insert("forms", forms, formsValues);
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', 'Solicitud de endoso de documento de transporte', TO_DATE('2017-04-03 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Solicitud de endoso de documento de transporte", date});    
        }
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});
        
        formFieldsValues = new String[]{"letterCredit", idForm, version , "text", "1", "TRUE", "TRUE",null, "1", "0", "default", "0"};
        formFieldsTextValues = new String[]{"letterCredit", idForm, version, "0", "50", "field-normal", null};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        messages.put("letterCredit.label", "Carta de credito");
        messages.put("letterCredit.requiredError", "Debe ingresar una carta de credito");        
        messages.put("letterCredit.help", "Seleccione la carta de crédito sobre la cual va a presentar documento");        
        
        //BOTON BUSCAR
        
        formFieldsValues = new String[]{"boardingPass", idForm, version, "selector", "2", "TRUE", "TRUE",null, "1", "0", "default", "0"};
        formFieldsSelectorValues = new String[]{"boardingPass", idForm, version, "field-normal", "aerial", "0", "combo"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", formFieldsSelector, formFieldsSelectorValues);
        messages.put("boardingPass.label", "Tipo de documento embarque");        
        
        formFieldsSelectorOptionValues = new String[]{"boardingPass", idForm, version, "aerial"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("boardingPass.option.aerial", "Aereo");        
        
        formFieldsSelectorOptionValues = new String[]{"boardingPass", idForm, version, "maritime"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("boardingPass.option.maritime", "Maritimo");
        
        formFieldsValues = new String[]{"shippingDocumentNumber", idForm, version, "text", "3", "TRUE", "TRUE",null, "1", "0", "default", "0"};
        formFieldsTextValues = new String[]{"shippingDocumentNumber", idForm, version, "0", "40", "field-normal", "withoutSpecialChars"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        messages.put("shippingDocumentNumber.label", "Nro. de documento de embarque");

        formFieldsValues = new String[]{"observationsOne", idForm, version, "textarea", "4", "TRUE", "FALSE",null, "1", "0", "default", "0"};
        formFieldsTextAreaValues = new String[]{"observationsOne", idForm, version, "0", "500", "field-normal"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", formFieldsTextArea, formFieldsTextAreaValues);
        messages.put("observationsOne.help", null);
        messages.put("observationsOne.hint", null);
        messages.put("observationsOne.label", "Observaciones");
        
        formFieldsValues = new String[]{"attachments", idForm, version, "text", "5", "TRUE", "FALSE",null, "1", "0", "default", "0"};
        formFieldsTextValues = new String[]{"attachments", idForm, version, "0", "50", "field-normal", "withoutSpecialChars"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        messages.put("attachments.label", "Adjuntos");
        
        //ADJUNTAR ARCHIVO TIPO LINK
        
        formFieldsValues = new String[]{"observationsTwo", idForm, version, "textarea", "7", "TRUE", "FALSE",null, "1", "0", "default", "0"};
        formFieldsTextAreaValues = new String[]{"observationsTwo", idForm, version, "0", "500", "field-normal"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", formFieldsTextArea, formFieldsTextAreaValues);
        messages.put("observationsTwo.help", null);
        messages.put("observationsTwo.hint", null);
        messages.put("observationsTwo.label", "Observaciones");
        
        formFieldsValues = new String[]{"disclaimer", idForm, version, "termsandconditions", "8", "TRUE", "FALSE", null, "1", "0", "default", "0"};
        formFieldsTermsConditionsValues = new String[]{"disclaimer", idForm, version, "field-normal", "0", "0"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", formFieldsTermsConditions, formFieldsTermsConditionsValues);
        messages.put("disclaimer.label", "Disclaimer");
        messages.put("disclaimer.requiredError", "");
        messages.put("disclaimer.showAcceptOptionText", "Aceptar");
        messages.put("disclaimer.termsAndConditions", "Las instrucciones enviadas al banco por usted luego de las <hora de corte>." + 
                " serán procesadas al siguiente día hábil bancario." +
                "Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.");
        
        formFieldsValues = new String[]{"emaillist", idForm, version, "emaillist", "9", "TRUE", "TRUE", null, "1", "0", "default", "0"};
        formFieldsEmailListValues = new String[]{"emaillist", idForm, version, "field-normal"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_emaillist", formFieldsEmailList, formFieldsEmailListValues);
        messages.put("emaillist.label", "Emails de notificación");
        messages.put("emaillist.help", "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");
        
        formFieldsValues = new String[]{"notificationBody", idForm, version, "textarea", "10", "TRUE", "FALSE",null, "1", "0", "default", "0"};
        formFieldsTextAreaValues = new String[]{"notificationBody", idForm, version, "0", "500", "field-normal"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", formFieldsTextArea, formFieldsTextAreaValues);
        messages.put("notificationBody.help", null);
        messages.put("notificationBody.hint", null);
        messages.put("notificationBody.label", "Cuerpo de notificacion");
        
        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }
    
}


