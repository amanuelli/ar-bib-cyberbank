/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author Diego Curbelo
 */
public class DB20150622_1321_432 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("desktop.loadLayout");
        insertActivity(
                "desktop.loadLayout", "com.technisys.omnichannel.client.activities.desktop.LoadLayoutActivity",
                "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        delete("client_desktop_layouts", null);
        delete("client_desktop_widgets", null);
        
        // Widgets por defecto de la columna 1
        String[] columns = new String[]{"name", "uri", "default_column", "default_row"};
        insert("client_desktop_widgets", columns, new String[]{"pendingTransactions", "/widgets/pending-transactions", "1", "1"});
        
        insert("client_desktop_widgets", columns, new String[]{"position", "/widgets/products", "1", "2"});
        
        insert("client_desktop_widgets", columns, new String[]{"accounts", "/widgets/products", "1", "3"});
        
        // Widgets por defecto de la columna 2
        insert("client_desktop_widgets", columns, new String[]{"exchangeRates", "/widgets/exchange-rates", "2", "1"});
        
        deleteActivity("widgets.exchangeRates");
        insertActivity(
                "widgets.exchangeRates", "com.technisys.omnichannel.client.activities.widgets.ExchangeRatesActivity",
                "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        deleteActivity("widgets.pendingTransactions");
        insertActivity(
                "widgets.pendingTransactions", "com.technisys.omnichannel.client.activities.widgets.PendingTransactionsActivity",
                "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        deleteActivity("widgets.products");
        insertActivity(
                "widgets.products", "com.technisys.omnichannel.client.activities.widgets.ProductsActivity",
                "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}
