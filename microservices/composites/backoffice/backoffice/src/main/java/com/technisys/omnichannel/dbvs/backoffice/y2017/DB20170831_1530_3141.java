/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * 
 * @author
 */
public class DB20170831_1530_3141 extends DBVSUpdate {

    @Override
    public void up() {

        update("adm_ui_permissions",
                new String[]{"simple_id_category", "medium_id_category", "advanced_id_category", "advanced_allow_prod_selection", "simple_ordinal", "medium_ordinal", "advanced_ordinal"},
                new String[]{null, null, null, "0", "810", "810", "810"},
                "id_permission='frequentDestinations.manage'");
    }
}
