/*
 * Copyright 2018 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor.AuditLevel;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor.Kind;

/**
 * Related issue: MANNAZCA-6155
 *
 * @author isilveira
 */
public class DB20181220_1808_6155 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("pay.multiline.salary.status", "com.technisys.omnichannel.client.activities.pay.multiline.PaySalaryStatusActivity", "multilinePayments", AuditLevel.Header, Kind.Other, "core.authenticated");
    }
}