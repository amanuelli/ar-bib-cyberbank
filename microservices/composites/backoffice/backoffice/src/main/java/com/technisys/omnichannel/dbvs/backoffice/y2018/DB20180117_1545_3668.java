/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3668
 *
 * @author msouza
 */
public class DB20180117_1545_3668 extends DBVSUpdate {

    @Override
    public void up() {
        
        /***** Setea TyC de constitucion de prefinanciacion en comex como obligatorio *****/
        
        update("form_fields", new String[] {"required"}, new String[] {"TRUE"}, "id_field = 'prefinancingType' AND id_form = 'preFinancingConstitution'");
    }
}