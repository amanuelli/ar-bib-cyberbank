/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150727_1041_415 extends DBVSUpdate {

    @Override
    public void up() {
        
        // Configuracion
        insertOrUpdateConfiguration("administration.signatures.maxNeeded", "3", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
    }
}
