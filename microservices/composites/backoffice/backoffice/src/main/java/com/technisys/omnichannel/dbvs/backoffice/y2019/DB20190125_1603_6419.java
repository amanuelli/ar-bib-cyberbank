/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author pbanales
 */

public class DB20190125_1603_6419 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("cellPhone.code.URY.format",
                "^(\\\\+?\\\\d{3}[- ]?)?([0])?([9])\\\\d{1}([- ])?\\\\d{3}([- ])?\\\\d{3}$",
                ConfigurationGroup.TECNICAS,
                "frontend",
                new String[]{},
                null,
                false,
                null,
                null,
                "frontend");
        insertOrUpdateConfiguration("cellPhone.code.URY.placeholder",
                "9X XXX XXX",
                ConfigurationGroup.TECNICAS,
                "frontend",
                new String[]{},
                null,
                false,
                null,
                null,
                "frontend");
        insertOrUpdateConfiguration("cellPhone.code.ARG.format",
                "",
                ConfigurationGroup.TECNICAS,
                "frontend",
                new String[]{},
                null,
                false,
                null,
                null,
                "frontend");
        insertOrUpdateConfiguration("cellPhone.code.ARG.placeholder",
                "",
                ConfigurationGroup.TECNICAS,
                "frontend",
                new String[]{},
                null,
                false,
                null,
                null,
                "frontend");
        insertOrUpdateConfiguration("cellPhone.code.BRA.format",
                "^(\\\\?(55\\\\d{2})|\\\\d{2})[6-9]\\\\d{8}$",
                ConfigurationGroup.TECNICAS,
                "frontend",
                new String[]{},
                null,
                false,
                null,
                null,
                "frontend");
        insertOrUpdateConfiguration("cellPhone.code.BRA.placeholder",
                "xx 9yyyy-yyyy",
                ConfigurationGroup.TECNICAS,
                "frontend",
                new String[]{},
                null,
                false,
                null,
                null,
                "frontend");
        insertOrUpdateConfiguration("cellPhone.code.USA.format",
                "^(\\\\([0-9]{3}\\\\) |[0-9]{3}-)[0-9]{3}-[0-9]{4}$",
                ConfigurationGroup.TECNICAS,
                "frontend",
                new String[]{},
                null,
                false,
                null,
                null,
                "frontend");
        insertOrUpdateConfiguration("cellPhone.code.USA.placeholder",
                "XXX-XXX-XXXX",
                ConfigurationGroup.TECNICAS,
                "frontend",
                new String[]{},
                null,
                false,
                null,
                null,
                "frontend");
    }


}