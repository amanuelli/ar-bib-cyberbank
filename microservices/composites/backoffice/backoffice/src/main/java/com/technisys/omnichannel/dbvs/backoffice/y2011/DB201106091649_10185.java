/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201106091649_10185 extends DBVSUpdate {

    @Override
    public void up() {

        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};
        String[] fieldValues = new String[]{"rub.account.accountMovementsDetails", "1", "1", "com.technisys.rubicon.business.accounts.activities.ListMovementsAccountsActivity"};
        insert("activities", fieldNames, fieldValues);
    }
}