/*
 *  Copyright 2012 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201211241612_117 extends DBVSUpdate {

    @Override
    public void up() {
        delete("environment_product", "id_product='RUBICON'");

        modifyColumn("environment_users", "signature_level", new ColumnDefinition("signature_level", Types.VARCHAR, 1, 0, true, null));
//
        String[] fieldsMessages = new String[]{"id_message", "value", "lang", "id_group"};

        //Activities
        delete("activities", "id_activity like 'rub.authorizations.%'");

        String[] fields = new String[]{"administrative"};
        update("activities", fields, new String[]{"1"}, "id_permission_required='rub.administration'");

        fields = new String[]{"component_fqn"};
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.common.users.activities.BlockUnblockUsersActivity"}, "id_activity='rub.users.blockUnblock'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.common.users.activities.DeleteUsersActivity"}, "id_activity='rub.users.delete'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.common.users.activities.GeneratePasswordActivity"}, "id_activity='rub.users.generatePassword'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.common.users.activities.ListUsersActivity"}, "id_activity='rub.users.list'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.common.users.activities.ExportListUsersActivity"}, "id_activity='rub.users.exportListUsers'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.common.users.activities.LoadInvitationActivity"}, "id_activity='rub.users.loadInvitation'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.common.users.activities.RejectInvitationActivity"}, "id_activity='rub.users.rejectInvitation'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.common.users.activities.AutocompleteUsersActivity"}, "id_activity='rub.users.autocomplete'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.advanced.groups.activities.ListGroupsActivity"}, "id_activity='rub.groups.list'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.advanced.groups.activities.ExportListGroupsActivity"}, "id_activity='rub.groups.exportListGroups'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.advanced.groups.activities.DeleteGroupsActivity"}, "id_activity='rub.groups.delete'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.advanced.groups.activities.BlockUnblockGroupsActivity"}, "id_activity='rub.groups.blockUnblock'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.advanced.groups.activities.CreatePreGroupsActivity"}, "id_activity='rub.groups.createPre'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.advanced.groups.activities.CreateGroupsActivity"}, "id_activity='rub.groups.create'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.advanced.groups.activities.ModifyPreGroupsActivity"}, "id_activity='rub.groups.modifyPre'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.advanced.groups.activities.ModifyPreGroupsActivity"}, "id_activity='rub.groups.modifyPre'");
        update("activities", fields, new String[]{"com.technisys.rubicon.business.administration.advanced.groups.activities.ModifyGroupsActivity"}, "id_activity='rub.groups.modify'");

        fields = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required", "id_group", "auditable", "administrative"};
        insert("activities", fields, new String[]{"rub.signatures.list", "1", "1", "com.technisys.rubicon.business.administration.advanced.signatures.activities.ListSignaturesActivity", "rub.administration", "rub.selfManagement", "0", "1"});

        insert("activities", fields, new String[]{"rub.signatures.delete", "1", "1", "com.technisys.rubicon.business.administration.advanced.signatures.activities.DeleteSignaturesActivity", "rub.administration", "rub.selfManagement", "2", "1"});

        insert("activities", fields, new String[]{"rub.signatures.exportListSignatures", "1", "1", "com.technisys.rubicon.business.administration.advanced.signatures.activities.ExportListSignaturesActivity", "rub.administration", "rub.selfManagement", "1", "1"});

        insert("activities", fields, new String[]{"rub.signatures.createPre", "1", "1", "com.technisys.rubicon.business.administration.advanced.signatures.activities.CreatePreSignatureActivity", "rub.administration", "rub.selfManagement", "0", "1"});

        insert("activities", fields, new String[]{"rub.signatures.create", "1", "1", "com.technisys.rubicon.business.administration.advanced.signatures.activities.CreateSignatureActivity", "rub.administration", "rub.selfManagement", "2", "1"});

        insert("activities", fields, new String[]{"rub.signatures.modifyPre", "1", "1", "com.technisys.rubicon.business.administration.advanced.signatures.activities.ModifyPreSignatureActivity", "rub.administration", "rub.selfManagement", "0", "1"});

        insert("activities", fields, new String[]{"rub.signatures.modify", "1", "1", "com.technisys.rubicon.business.administration.advanced.signatures.activities.ModifySignatureActivity", "rub.administration", "rub.selfManagement", "2", "1"});

        insert("activities", fields, new String[]{"rub.users.verifyInvitationStep1", "1", "1", "com.technisys.rubicon.business.administration.common.users.activities.VerifyInvitationStep1Activity", "rub.administration", "rub.selfManagement", "2", "1"});

        insert("activities", fields, new String[]{"rub.users.invitePre", "1", "1", "com.technisys.rubicon.business.administration.common.users.activities.InvitePreUsersActivity", "rub.administration", "rub.selfManagement", "0", "1"});

        insert("activities", fields, new String[]{"rub.users.invite", "1", "1", "com.technisys.rubicon.business.administration.common.users.activities.InviteUsersActivity", "rub.administration", "rub.selfManagement", "1", "1"});

        insert("activities", fields, new String[]{"rub.users.acceptInvitation", "1", "1", "com.technisys.rubicon.business.administration.common.users.activities.AcceptInvitationUserActivity", null, "rub.selfManagement", "2", "1"});

        insert("activities", fields, new String[]{"rub.users.advancedModifyPre", "1", "1", "com.technisys.rubicon.business.administration.advanced.users.activities.ModifyPreUsersActivity", "rub.administration", "rub.selfManagement", "0", "1"});

        insert("activities", fields, new String[]{"rub.users.advancedModify", "1", "1", "com.technisys.rubicon.business.administration.advanced.users.activities.ModifyUsersActivity", "rub.administration", "rub.selfManagement", "2", "1"});

        insert("activities", fields, new String[]{"rub.groups.listProducts", "1", "1", "com.technisys.rubicon.business.administration.advanced.groups.activities.ListProductsActivity", "rub.administration", "rub.selfManagement", "0", "1"});

        insert("activities", fields, new String[]{"rub.users.simpleModifyPre", "1", "1", "com.technisys.rubicon.business.administration.simple.users.activities.ModifyPreUsersActivity", "rub.administration", "rub.selfManagement", "0", "1"});

        insert("activities", fields, new String[]{"rub.users.simpleModify", "1", "1", "com.technisys.rubicon.business.administration.simple.users.activities.ModifyUsersActivity", "rub.administration", "rub.selfManagement", "2", "1"});

        insert("activities", fields, new String[]{"rub.users.mediumModifyPre", "1", "1", "com.technisys.rubicon.business.administration.medium.users.activities.ModifyPreUsersActivity", "rub.administration", "rub.selfManagement", "0", "1"});

        insert("activities", fields, new String[]{"rub.users.mediumModify", "1", "1", "com.technisys.rubicon.business.administration.medium.users.activities.ModifyUsersActivity", "rub.administration", "rub.selfManagement", "2", "1"});

        insert("activities", fields, new String[]{"rub.environment.modifyPre", "1", "1", "com.technisys.rubicon.business.administration.medium.environment.activities.ModifyPreEnvironmentActivity", "rub.administration", "rub.selfManagement", "0", "1"});

        insert("activities", fields, new String[]{"rub.environment.modify", "1", "1", "com.technisys.rubicon.business.administration.medium.environment.activities.ModifyEnvironmentActivity", "rub.administration", "rub.selfManagement", "2", "1"});

        delete("activities", "id_activity in ('rub.users.createPre', 'rub.users.create', 'rub.users.acceptInvitationExistingUser', 'rub.users.acceptInvitationNewUser', 'rub.users.modifyPre', 'rub.users.userDetails', 'rub.groups.groupDetails', 'rub.channels.modify', 'rub.channels.modifyPre')");
    }
}