/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * @author Marcelo Bruno
 */

public class DB20200506_1238_11190 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("session.listAllGeneralConditionDocuments");
        insertActivity("session.listAllGeneralConditionDocuments", "com.technisys.omnichannel.client.activities.session.ListAllGeneralConditionDocuments", "acceptGeneralConditions", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);

        Date startDate = new Date();
        String startDateStr = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(startDate);
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.YEAR, 10);
        Date endDate = calendar.getTime();
        String endDateStr = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(endDate);
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO general_conditions (term, date_start, date_final) "
                    + " VALUES ('Esign', TO_DATE('"+ startDateStr +"', 'YYYY-MM-DD HH24:MI:SS'), TO_DATE('"+ startDateStr +"', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("general_conditions", new String[]{"term", "date_start", "date_final"}, new String[]{"Esign", startDateStr, endDateStr});
        }

        update("environment_users", new String[]{"condition_accepted"}, new String[]{"1"}, "1=1");
    }

}