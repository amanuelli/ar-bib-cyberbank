/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3883
 *
 * @author midned
 */
public class DB20180309_1753_3883 extends DBVSUpdate {

    @Override
    public void up() {
        {
            String[] keys = new String[]{"id_permission"};
            String[] values = new String[]{"user.preferences"};

            update("activity_products", keys, values, "id_activity='preferences.userData.updateMobilePhone.send'");
        }
        {
            String[] keys = new String[]{"id_permission"};
            String[] values = new String[]{"core.authenticated"};

            update("activity_products", keys, values, "id_activity='preferences.userData.modify.send'");
        }
    }
    
}