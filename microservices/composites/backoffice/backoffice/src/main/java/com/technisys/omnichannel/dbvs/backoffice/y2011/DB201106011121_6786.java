/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201106011121_6786 extends DBVSUpdate {

    @Override
    public void up() {

        String[] fieldNames = new String[]{"id_permission", "permission_type"};
        String[] fieldValues = new String[]{"rub.swift.read", "client"};
        insert("permissions", fieldNames, fieldValues);

        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required"};
        fieldValues = new String[]{"rub.swift.list", "1", "1", "com.technisys.rubicon.business.swift.activities.ListSwiftActivity", "rub.swift.read"};
        insert("activities", fieldNames, fieldValues);

        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required"};
        fieldValues = new String[]{"rub.swift.export", "1", "1", "com.technisys.rubicon.business.swift.activities.ExportSwiftActivity", "rub.swift.read"};
        insert("activities", fieldNames, fieldValues);

        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required"};
        fieldValues = new String[]{"rub.swift.read", "1", "1", "com.technisys.rubicon.business.swift.activities.ReadSwiftActivity", "rub.swift.read"};
        insert("activities", fieldNames, fieldValues);

        fieldNames = new String[]{"id_role", "id_permission"};
        fieldValues = new String[]{"client_administrator", "rub.swift.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldNames = new String[]{"id_role", "id_permission"};
        fieldValues = new String[]{"client_operator", "rub.swift.read"};
        insert("role_permissions", fieldNames, fieldValues);


    }
}