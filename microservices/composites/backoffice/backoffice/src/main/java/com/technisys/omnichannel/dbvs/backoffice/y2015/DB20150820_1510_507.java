/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150820_1510_507 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("enrollment.invitationCode.verify", "com.technisys.omnichannel.client.activities.enrollment.InvitationCodeVerifyActivity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);

        insertActivity("enrollment.wizzard.pre", "com.technisys.omnichannel.client.activities.enrollment.WizzardPreActivity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);

        deleteActivity("enrollment.wizzard.verificationCode");
        deleteActivity("enrollment.wizzard.personalData");
        
        insertActivity("enrollment.wizzard.resendVerificationCode", "com.technisys.omnichannel.client.activities.enrollment.WizzardResendVerificationCodeActivity", "enrollment", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("enrollment.wizzard.verificationCode", "com.technisys.omnichannel.client.activities.enrollment.WizzardVerificationCodeActivity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("enrollment.wizzard.personalData", "com.technisys.omnichannel.client.activities.enrollment.WizzardPersonalDataActivity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);

        insertActivity("enrollment.securitySeals.list", "com.technisys.omnichannel.client.activities.enrollment.ListSecuritySealsActivity", "enrollment", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);

        insertActivity("enrollment.wizzard.finish", "com.technisys.omnichannel.client.activities.enrollment.WizzardFinishActivity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);

    }
}
