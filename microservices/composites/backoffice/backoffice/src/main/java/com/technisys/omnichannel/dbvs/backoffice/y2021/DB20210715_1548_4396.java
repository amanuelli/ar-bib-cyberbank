/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2021;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;

import java.sql.Types;

/**
 * Related issue: TECCDPBO-4396
 *
 * @author Mauricio Uribe
 */
public class DB20210715_1548_4396 extends DBVSUpdate {

    @Override
    public void up() {

        // List Activity
        delete("backoffice_activities", "id_activity = 'cybo.invitationcodes.list'");
        insertBackofficeActivity("cybo.invitationcodes.list",
                "com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities.ListActivity",
                "backoffice.invitationCodes.list", null, "backoffice.invitationcodes",
                ActivityDescriptor.AuditLevel.Header);

        // List Statuses Activity
        delete("backoffice_activities", "id_activity = 'cybo.invitationcodes.listStatuses'");
        insertBackofficeActivity("cybo.invitationcodes.listStatuses",
                "com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities.ListStatusesActivity",
                "backoffice.invitationCodes.list", null, "backoffice.invitationcodes",
                ActivityDescriptor.AuditLevel.None);

        // Invitation Codes Feature Flag
        insertOrUpdateConfiguration(
                "cybo.features.invitationcodes.enabled",
                "false",
                ConfigurationGroup.TECNICAS,
                "others",
                new String[]{"notEmpty", "boolean"}, "true|false", "backoffice");

        // New field product_group_name
        addColumn("invitation_codes", new ColumnDefinition("product_group_name", Types.VARCHAR, 255, 0, true, null ));
    }
}
