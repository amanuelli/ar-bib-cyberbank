/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author salva
 */
public class DB20150819_1629_514 extends DBVSUpdate {

    @Override
    public void up() {
        
        delete("activity_products", "id_activity = 'pay.loan.preview'");
        delete("activity_products", "id_activity = 'pay.loan.send'");
        insert("activity_products", new String[]{"id_activity", "id_field", "id_permission"}, new String[]{"pay.loan.send", "debitAccount", "pay.loan"});
        
        update("activities", new String[]{"kind"}, new String[]{"Other"}, "id_activity = 'pay.loan.preview'");
        update("activities", new String[]{"kind"}, new String[]{"Transactional"}, "id_activity = 'pay.loan.send'");
    
        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"pay.loan", "payments", null, null, "0", "10", "payments", null, null, "0", "10", "payments", null, null, "1", "10", "CA,CC", null, null});

        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"pay.loan.thirdParties", "payments", null, null, "0", "20", "payments", null, null, "0", "10", "payments", null, null, "1", "10", "CA,CC", "pay.loan", null});

    }
}
