/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107271426_10700 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_role", "role_type", "name", "deleted"};

        String[] fieldValues = new String[]{"client_transfer", "client", "Transferencias", "0"};
        insert("roles", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "client", "Consultas", "0"};
        insert("roles", fieldNames, fieldValues);

        fieldNames = new String[]{"id_role", "id_permission"};

        fieldValues = new String[]{"client_transfer", "core.cancelTransaction"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "core.forms.send"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "core.listTransactions"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "core.product.alias"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "core.moveToDraftTransaction"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "core.readTransaction"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "core.readForm"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.accounts.checkDiscount"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.account.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.accounts.requestCheckbook"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.accounts.requestAccountOpening"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.accounts.sendCheck"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.accounts.requestDebit"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.accounts.transferBatch"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.accounts.transferInternal"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.certificate.download"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.authorizations.manage"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.creditcard.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.creditcard.pay"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.creditcards.rePrint"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.creditcards.request"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.files.upload"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.files.list"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.exportCreditLetterTransfer"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.exportCreditLetterPurchase"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.exportCreditLetterApproval"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.importCreditLetterDiscrepancies"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.importCreditLetterCanceling"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.exportPreFinancing"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.importCreditLetterOpening"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.importCreditLetterModify"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.foreigntrade.importCreditLetterRejection"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.loan.payDebit"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.loan.pay"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.loan.request"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.loan.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.notificationinbox.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.login"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.checkDiscount"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.billDiscount"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.payBPS"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.payDGI"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.payIMF"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.payIMC"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.payService"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.paySalary"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.paySuppliers"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.swift.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.payment.serviceAssociation"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.userconfiguration.manage"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_transfer", "rub.transactionhistory.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "core.listTransactions"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "core.readForm"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "core.readTransaction"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.creditcard.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.certificate.download"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.account.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.loan.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.files.list"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.swift.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.notificationinbox.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.transactionhistory.read"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.login"};
        insert("role_permissions", fieldNames, fieldValues);

        fieldValues = new String[]{"client_readonly", "rub.userconfiguration.manage"};
        insert("role_permissions", fieldNames, fieldValues);

        update("user_roles", new String[]{"id_role"}, new String[]{"client_readonly"}, "id_role='client_operator'");
        update("configuration", new String[]{"value"}, new String[]{"client_readonly"}, "id_field='rubicon.defaultUserRole'");

        delete("role_permissions", "id_role='client_operator'");
        delete("roles", "id_role='client_operator'");

        update("roles", new String[]{"name"}, new String[]{"Administración"}, "id_role='client_administrator'");
        update("roles", new String[]{"name"}, new String[]{"Backoffice - Administración"}, "id_role='administrator'");

    }
}