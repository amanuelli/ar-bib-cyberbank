/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150623_1513_421 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("session.recoverPasswordWithOTP.step1", "com.technisys.omnichannel.client.activities.recoverpassword.RecoverPasswordStep1Activity","recoverPassword", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.loginWithOTP");
        insertActivity("session.recoverPassword.step2", "com.technisys.omnichannel.client.activities.recoverpassword.RecoverPasswordStep2Activity", "recoverPassword", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("session.recoverPassword.step3", "com.technisys.omnichannel.client.activities.recoverpassword.RecoverPasswordStep3Activity", "recoverPassword", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        
        insertOrUpdateConfiguration("recoverPassword.mail.validity", "48h", ConfigurationGroup.TECNICAS, "others", new String[]{"notEmpty"});
        
    }
}
