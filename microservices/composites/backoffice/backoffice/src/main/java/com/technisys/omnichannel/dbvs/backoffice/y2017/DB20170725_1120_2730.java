/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author danireb
 */
public class DB20170725_1120_2730 extends DBVSUpdate {

    @Override
    public void up() {
        
        delete("form_fields", "id_field = 'recipientDocumentType' and id_form = 'frequentDestination'");
        delete("form_fields", "id_field = 'recipientDocumentNumber' and id_form = 'frequentDestination'");
        
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        String[] formFieldsValues;
        String idForm = "frequentDestination";
        String version = "1";       
        String idField = "recipientDocument";
        
        formFieldsValues = new String[]{idField, idForm, version, "document", "9", "value(productType) == 'externalAccount'", "value(productType) == 'externalAccount'", "default", "1"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_document", new String[]{"id_field", "id_form", "form_version", "default_country", "default_document_type"}, new String[]{idField, idForm, version, "UY", "CI"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".help', 'es', '"+idField+"','" + idForm + "','" + version
                  + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".hint', 'es', '"+idField+"','" + idForm + "','" + version
                  + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '"+idField+"','" + idForm + "','" + version
                  + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Documento del beneficiario')");
            customSentence(new String[] { DBVS.DIALECT_ORACLE }, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '"+idField+"','" + idForm + "','" + version
                  + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar el documento del beneficiario')");          
        } else {
          insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null});
          insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null});
          insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Documento del beneficiario"});
          insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "Debe ingresar el documento del beneficiario"});
        }
        
    }
    
}
