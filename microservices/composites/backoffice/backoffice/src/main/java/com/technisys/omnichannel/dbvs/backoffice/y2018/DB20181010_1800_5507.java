package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5507
 *
 * @author mcheveste
 */
public class DB20181010_1800_5507 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.medium.read.channels", "com.technisys.omnichannel.client.activities.administration.medium.ReadChannelsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.medium.modify.channels.preview", "com.technisys.omnichannel.client.activities.administration.medium.ModifyChannelsPreviewActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.medium.modify.channels", "com.technisys.omnichannel.client.activities.administration.medium.ModifyChannelsActivity", "administration", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");
    }
}
