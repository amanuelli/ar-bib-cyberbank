/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201112011624_11529 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};
        String[] fieldValues = new String[]{"rubicon.producttype.checkDiscount.checkDiscountRate", "90", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

    }
}