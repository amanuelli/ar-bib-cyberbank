/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author salva
 */
public class DB20150626_1647_434 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("widget.transactions.maxPending", "100", ConfigurationGroup.NEGOCIO, "transactions", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("widget.transactions.availableStates", "DRAFT|PENDING|SCHEDULED", ConfigurationGroup.NEGOCIO, "transactions", new String[]{"notEmpty"});
    
    }
}
