/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

import java.text.SimpleDateFormat;
import java.util.Date;


/**
 * @author dimoda
 */

public class DB20200329_1604_958 extends DBVSUpdate {

    @Override
    public void up() {
        /* This is a fix for dbvs DB20200219_2100_455, because this dbvs was executed in some servers, but it was empty */
        String whereClause = "id_form='transferForeign' and id_field='document'";
        delete("form_fields", whereClause);
        delete("form_field_document", whereClause);
        delete("form_field_messages", whereClause);

        String idForm = "transferForeign";
        String version = "1";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String idField = "document";

        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsValues = new String[]{idField, idForm, version, "document", "10", "TRUE", "TRUE", "default", "0"};

        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_document", new String[]{"id_field", "id_form", "form_version", "default_country", "default_document_type"}, new String[]{idField, idForm, version, "UY", "CI"});

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'es', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Documento del Destinatario')");
            customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'es', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar el documento del destinatario')");
            customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'en', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Recipient Document')");
            customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'en', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'You must enter the recipient document')");
            customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".label', 'pt', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Documento Destinatário')");
            customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value)  VALUES ('fields." + idForm + "." + idField + ".requiredError', 'pt', '" + idField + "','" + idForm + "','" + version
                    + "',TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Você deve inserir o documento do destinatário')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, "Documento del Destinatario", date});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, "Debe ingresar el documento del destinatario", date});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, "Recipient Document", date});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "en", idField, idForm, version, "You must enter the recipient document", date});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, "Documento Destinatário", date});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + "." + idField + ".requiredError", "pt", idField, idForm, version, "Você deve inserir o documento do destinatário", date});
        }

    }

}