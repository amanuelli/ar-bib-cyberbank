/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2895
 *
 * @author rmedina
 */
public class DB20170803_1143_2895 extends DBVSUpdate {

    @Override
    public void up() {
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFieldsMessages = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value" };
        String idForm = "presentationOfDocumentsUnderCollection";
        String version = "1";

        delete("form_field_messages", "id_message = '" + "fields." + idForm + ".amountOfCollection.help'");
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) 
             customSentence(DBVS.DIALECT_ORACLE, "UPDATE form_field_messages "
                     + "SET modification_date = TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),"
                     + "value = 'Introduzca el nombre del courier con el que tiene una cuenta' "
                     + "WHERE id_message = 'fields." + idForm + ".courierName.help' AND lang = 'es'"); 
        else
            update("form_field_messages", new String[] { "modification_date", "value" }, new String[]{date, "Introduzca el nombre del courier con el que tiene una cuenta"}, "id_message = '" + "fields." + idForm + ".courierName.help' AND lang = 'es'" );
         
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".amountOfCollection.help','es','amountOfCollection', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Ingrese el importe por el cual será solicitado el pago')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".amountOfCollection.help','pt','amountOfCollection', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Digite o valor para o qual será solicitado o pagamento')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".amountOfCollection.help','en','amountOfCollection', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Enter the amount for which the payment will be requested')");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".amountOfCollection.help", "es", "amountOfCollection", idForm, version, date, "Ingrese el importe por el cual será solicitado el pago" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".amountOfCollection.help", "pt", "amountOfCollection", idForm, version, date, "Digite o valor para o qual será solicitado o pagamento" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".amountOfCollection.help", "en", "amountOfCollection", idForm, version, date, "Enter the amount for which the payment will be requested" });
        }  
    }
}