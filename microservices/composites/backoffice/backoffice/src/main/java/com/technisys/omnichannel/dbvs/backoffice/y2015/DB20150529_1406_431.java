/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author Diego Curbelo
 */
public class DB20150529_1406_431 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("frontend.google.api.key",
                "AIzaSyDXzPrBIdIZ23cVPcB3qVN3FpyOjTYlXM0",
                com.technisys.omnichannel.DBVSUpdate.ConfigurationGroup.TECNICAS, "frontend", new String[]{});
    }
}