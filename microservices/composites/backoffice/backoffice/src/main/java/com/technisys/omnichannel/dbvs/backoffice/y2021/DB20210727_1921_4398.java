/*
 * Copyright 2021 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2021;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;

/**
 * Related issue: TECCDPBO-4398
 *
 * @author azeballos
 */
public class DB20210727_1921_4398 extends DBVSUpdate {
    @Override
    public void up() {
        delete("backoffice_activities", "id_activity = 'cybo.invitationcodes.create'");
        insertBackofficeActivity("cybo.invitationcodes.create", "com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities.CreateActivity", "backoffice.invitationCodes.manage", null, "backoffice.invitationcodes", ActivityDescriptor.AuditLevel.Full);

        delete("backoffice_activities", "id_activity = 'cybo.invitationcodes.readUser'");
        insertBackofficeActivity("cybo.invitationcodes.readUser", "com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities.ReadUserActivity", "backoffice.invitationCodes.list", null, "backoffice.invitationcodes", ActivityDescriptor.AuditLevel.Header);

        delete("backoffice_activities", "id_activity = 'cybo.invitationcodes.listSchemas'");
        insertBackofficeActivity("cybo.invitationcodes.listSchemas", "com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities.ListSchemasActivity", "backoffice.invitationCodes.list", null, "backoffice.invitationcodes", ActivityDescriptor.AuditLevel.None);
    }
}
