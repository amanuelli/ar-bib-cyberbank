/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3453
 *
 * @author msouza
 */
public class DB20171130_1017_3453 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("frontend.chat.zendesk.bubbleEnable", "true", ConfigurationGroup.TECNICAS, "zendesk", new String[]{"notEmpty"});
    }
}
