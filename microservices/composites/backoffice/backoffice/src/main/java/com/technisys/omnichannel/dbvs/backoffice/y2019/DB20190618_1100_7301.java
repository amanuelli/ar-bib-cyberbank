/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.dbvs.DBVS;

/**
 * Related issue: MANNAZCA-7301
 *
 * @author pbregonzio
 */

public class DB20190618_1100_7301 extends DBVSUpdate {

    @Override
    public void up() {

        if (DBVS.DIALECT_MSSQL.equals(getDialect())) {

            delete("points_of_interest", "1 = 1");
            delete("messages", "id_message LIKE 'pointsofinterest%'" );

            customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT points_of_interest ON");

            String[] fields = new String[]{"id_poi", "id_poi_type", "latitude", "longitude", "enabled"};

            /* United States */
            String[] fieldValues = new String[]{"1", "1", "25.766889", "-80.189056", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"2", "1", "25.779695", "-80.194153", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"3", "2", "25.769162", "-80.205670", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"4", "1", "25.801762", "-80.229898", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"5", "1", "25.758274", "-80.230194", "1"};
            insert("points_of_interest", fields, fieldValues);

            /* Brazil */
            fieldValues = new String[]{"6", "1", "-23.605889", "-46.693322", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"7", "1", "-23.612149", "-46.723756", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"8", "2", "-23.639471", "-46.698104", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"9", "1", "-23.604491", "-46.662549", "1"};
            insert("points_of_interest", fields, fieldValues);

            /* Mexico */
            fieldValues = new String[]{"10", "1", "19.425763", "-99.193742", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"11", "2", "19.435632", "-99.165511", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"12", "1", "19.413759", "-99.176553", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"13", "1", "19.436767", "-99.208546", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"14", "2", "19.454228", "-99.208546", "1"};
            insert("points_of_interest", fields, fieldValues);

            /* Colombia */
            fieldValues = new String[]{"15", "1", "4.691565", "-74.034951", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"16", "1", "4.736094", "-74.030526", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"17", "2", "4.732749", "-74.091579", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"18", "1", "4.743138", "-74.089098", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"19", "1", "4.667003", "-74.050320", "1"};
            insert("points_of_interest", fields, fieldValues);

            /* Chile */
            fieldValues = new String[]{"20", "1", "-33.441793", "-70.650422", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"21", "1", "-33.469766", "-70.650522", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"22", "1", "-33.432511", "-70.673002", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"23", "2", "-33.424628", "-70.662038", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"24", "1", "-33.449067", "-70.639324", "1"};
            insert("points_of_interest", fields, fieldValues);

            /* Uruguay */
            fieldValues = new String[]{"25", "1", "-34.886415", "-56.147877", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"26", "1", "-34.904384", "-56.176184", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"27", "2", "-34.907903", "-56.164691", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"28", "1", "-34.909507", "-56.149056", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"29", "1", "-34.905446", "-56.204902", "1"};
            insert("points_of_interest", fields, fieldValues);

            /* Argentina */
            fieldValues = new String[]{"30", "1", "-34.597317", "-58.370095", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"31", "1", "-34.612630", "-58.419533", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"32", "1", "-34.616062", "-58.443596", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"33", "2", "-34.590547", "-58.399371", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"34", "1", "-34.579075", "-58.429022", "1"};
            insert("points_of_interest", fields, fieldValues);


            fieldValues = new String[]{"35", "1", "-37.322761", "-59.135346", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"36", "2", "-37.312676", "-59.126929", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"37", "1", "-37.329914", "-59.118033", "1"};
            insert("points_of_interest", fields, fieldValues);

            /* Costa Rica */
            fieldValues = new String[]{"38", "1", "9.941085", "-84.075203", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"39", "1", "9.937431", "-84.077776", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"40", "1", "9.938332", "-84.080414", "1"};
            insert("points_of_interest", fields, fieldValues);

            fieldValues = new String[]{"41", "2", "9.931521", "-84.079383", "1"};
            insert("points_of_interest", fields, fieldValues);

            customSentence(DBVS.DIALECT_MSSQL, "SET IDENTITY_INSERT points_of_interest OFF");
        }
    }
}
