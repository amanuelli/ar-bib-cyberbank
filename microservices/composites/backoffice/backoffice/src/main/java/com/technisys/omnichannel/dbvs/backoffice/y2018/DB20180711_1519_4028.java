/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-4028
 *
 * @author dimoda
 */
public class DB20180711_1519_4028 extends DBVSUpdate {

    @Override
    public void up() {
        
        String idForm = "accountOpening";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsValues;
        String version = "1";
        Map<String, String> messages = new HashMap();
        
        update("form_field_messages", new String[]{"value"}, new String[]{"Debes seleccionar un tipo de cuenta"}, "id_form='" + idForm + "' and id_message='fields.accountOpening.accountType.requiredError' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Moneda de la cuenta"}, "id_form='" + idForm + "' and id_message='fields.accountOpening.currencies.label' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Debes seleccionar una moneda"}, "id_form='" + idForm + "' and id_message='fields.accountOpening.currencies.requiredError' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Debes seleccionar la sucursal de la cuenta"}, "id_form='" + idForm + "' and id_message='fields.accountOpening.accountBranch.requiredError' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Sucursales"}, "id_form='" + idForm + "' and id_message='fields.accountOpening.placeOfRetreat.label' and lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"La sucursal de retiro es requerido"}, "id_form='" + idForm + "' and id_message='fields.accountOpening.placeOfRetreat.requiredError' and lang='es'");
        
        
        update("form_fields", new String[]{"ordinal", "visible", "required"}, new String[]{"15", "value(placeSelector) == 'retreatBranch'", "value(placeSelector) == 'retreatBranch'"}, "id_form='" + idForm + "' and id_field='placeOfRetreat'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"20"}, "id_form='" + idForm + "' and id_field='horizontalrule'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"30"}, "id_form='" + idForm + "' and id_field='termsConditions'");
        
        update("form_field_selector", new String[]{"default_value"}, new String[]{"No"}, "id_form='" + idForm + "' and id_field='debitCard'");
        
        delete("form_field_messages", "id_form='accountOpening' and id_message='fields.accountOpening.placeOfRetreat.showOtherText'");        
        
        /**** Insert field selector - Lugar de retiro ****/
        formFieldsValues = new String[]{"placeSelector", idForm, version, "selector", "10", "value(debitCard) == 'Si'", "value(debitCard) == 'Si'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value","show_blank_option"}, new String[]{"placeSelector", idForm, version, "field-normal", null, "0"});
        messages.put("placeSelector.label", "Lugar de retiro");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"placeSelector", idForm, version, "retreatBranch"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"placeSelector", idForm, version, "homeDelivery"});     
        messages.put("placeSelector.option.retreatBranch", "Sucursales");
        messages.put("placeSelector.option.homeDelivery", "Entrega a domicilio");
        
        
        /**** Insert field text - address ****/
        formFieldsValues = new String[]{"address", idForm, version, "text", "16", "value(placeSelector) == 'homeDelivery'", "value(placeSelector) == 'homeDelivery'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"}, new String[] {"address", idForm, version, "1", "200", "field-normal"});
        messages.put("address.label", "Dirección");
        messages.put("address.requiredError", "El campo dirección no puede estar vacío");
        

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
        
    }

}