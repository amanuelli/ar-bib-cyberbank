/*
 * Copyright 2018 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

public class DB20180720_1110_4879 extends DBVSUpdate {
    @Override
    public void up() {
        //Formulario de pago de prestamos
        //se puede agendar
        //no se puede programar
        //no se puede guardar plantilla
        //no se puede guardar draft 
        update("forms", new String[]{"schedulable", "programable", "drafts_enabled", "templates_enabled"}, new String[]{"1", "0", "0", "0"}, "id_form='payLoan'");
    }
}
