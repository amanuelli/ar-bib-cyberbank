/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6194
 *
 * @author Marcelo Bruno
 */
public class DB20181220_1159_6194 extends DBVSUpdate {

    @Override
    public void up() {
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-502-demo-environments.sql", ";", "UTF-8");
    }

}