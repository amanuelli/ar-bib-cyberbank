/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ahernandez
 */

public class DB20190930_1034_75 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("connector.cyberbank.webservices.url", "http://172.23.0.9:8080/engine/json/restapi", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("connector.cyberbank.core.dateFormat", "yyyyMMddhhmmssSSS", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("connector.cyberbank.connections.maxTotal", "100", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("connector.cyberbank.connections.maxPerRoute", "20", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("connector.cyberbank.core.project", "Tech", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("connector.cyberbank.core.channelId", "8", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("connector.cyberbank.core.workflowId", "massiveSelectGeneralTableQueryApoyo", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("connector.cyberbank.core.branchId", "1", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("connector.cyberbank.core.institutionId", "1111", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("connector.cyberbank.core.userId", "TECHNISYS", ConfigurationGroup.TECNICAS, "connector.cyberbank.core", new String[]{"notEmpty"}, "", true);
    }
}