/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Jhossept G
 */

public class DB20200512_1013_11187 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration(
                "frontend.regions",
                "US|AR|CL|CA|UY|MX|PE|CO|EC|PY|PA|CR|LATAM",
                null, "frontend", new String[]{"notEmpty"},null,"frontend");
    }

}