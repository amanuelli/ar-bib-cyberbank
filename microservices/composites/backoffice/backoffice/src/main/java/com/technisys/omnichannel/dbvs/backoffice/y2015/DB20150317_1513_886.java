/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150317_1513_886 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("core.cache.bankHandler.listBanks.maximumSize", "1000", ConfigurationGroup.TECNICAS, null, new String[]{});
        insertOrUpdateConfiguration("core.cache.bankHandler.listBanks.expireAfter", "1m", ConfigurationGroup.TECNICAS, null, new String[]{});
    }
}
