/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * 
 * @author fpena
 */
public class DB20170713_1402_2666 extends DBVSUpdate {

    @Override
    public void up() {
        String[] dialects = new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE};

        customSentence(dialects,
                "INSERT INTO group_permissions "
                + "SELECT 29, 'administration.view', 'NONE' "
                + "FROM groups g "
                + "WHERE g.id_group = 29 "
                + "AND NOT EXISTS (SELECT 1 "
                + "FROM group_permissions gp "
                + "WHERE gp.id_group = g.id_group AND gp.id_permission = 'administration.view')");

        updateConfiguration("global.displayMessageKeys", "false");
        updateConfiguration("frontend.forms.refresh.interval", "0m");
    }
}
