/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author iocampo
 */
public class DB20190107_1710_4387 extends DBVSUpdate {

    @Override
    public void up() {
        update("backoffice_activities", new String[]{ "id_permission_required" }, new String[]{ "backoffice.clientAudit" }, "id_activity='backoffice.clientAudit.detail'");
    }
    
}
