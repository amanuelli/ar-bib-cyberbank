/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author salva
 */
public class DB20151012_1256_959 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("services.notification.communicationType.1", "message", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("services.notification.communicationType.2", "creditCardPayment", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("services.notification.communicationType.3", "creditCardExpiration", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("services.notification.communicationType.4", "loanExpiration", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("services.notification.communicationType.5", "creditCardPayment", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("services.notification.communicationType.6", "transferReceived", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("services.notification.communicationType.7", "promos", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty"});
        
        insertOrUpdateConfiguration("services.notification.communicationTray.1", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("services.notification.communicationTray.2", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("services.notification.communicationTray.3", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("services.notification.communicationTray.4", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("services.notification.communicationTray.5", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("services.notification.communicationTray.6", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("services.notification.communicationTray.7", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        
        insertOrUpdateConfiguration("notifications.channels", "DEFAULT|SMS|MAIL|FACEBOOK|TWITTER", ConfigurationGroup.OTHER, "notification", new String[]{"notEmpty"});
   
    }
}
