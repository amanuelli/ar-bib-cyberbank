/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.List;

/**
 * @author AndresCasas
 */

public class DB20200603_1707_11367 extends DBVSUpdate {

    @Override
    public void up() {
        try {
            PaginatedList<User> list = AccessManagementHandlerFactory.getHandler().getUsers(-1, -1, -1, "id_user ASC");
            List<User> users = list.getElementList();
            for (int i = 0; i < users.size(); i++) {
                if (StringUtils.isNotBlank(users.get(i).getFirstName())) {
                    String firstName =  SecurityUtils.encrypt(users.get(i).getFirstName().substring(0, 1).toUpperCase() + users.get(i).getFirstName().substring(1).toLowerCase());
                    String firstNameHashed =  SecurityUtils.hash(users.get(i).getFirstName().toUpperCase());
                    update("users", new String[]{"first_name"}, new String[]{firstName}, "id_user = '" + users.get(i).getIdUser() + "'");
                    update("users", new String[]{"first_nameHashed"}, new String[]{firstNameHashed}, "id_user = '" + users.get(i).getIdUser() + "'");
                }
                if (StringUtils.isNotBlank(users.get(i).getLastName())){
                    String lastName = SecurityUtils.encrypt(users.get(i).getLastName().substring(0, 1).toUpperCase() + users.get(i).getLastName().substring(1).toLowerCase());
                    String lastNameHashed = SecurityUtils.hash(users.get(i).getLastName().toUpperCase());
                    update("users", new String[]{"last_name"}, new String[]{lastName}, "id_user = '" + users.get(i).getIdUser() + "'");
                    update("users", new String[]{"last_nameHashed"}, new String[]{lastNameHashed}, "id_user = '" + users.get(i).getIdUser() + "'");
                }
                if (StringUtils.isNotBlank(users.get(i).getDocumentNumber())) {
                    String documentNumber = SecurityUtils.encrypt(users.get(i).getDocumentNumber());
                    String documentNumberHashed = SecurityUtils.hash(users.get(i).getDocumentNumber());
                    update("users", new String[]{"document"}, new String[]{documentNumber}, "id_user = '" + users.get(i).getIdUser() + "'");
                    update("users", new String[]{"documentHashed"}, new String[]{documentNumberHashed}, "id_user = '" + users.get(i).getIdUser() + "'");
                }
                if (StringUtils.isNotBlank(users.get(i).getMobileNumber())) {
                    String mobileNumber = SecurityUtils.encrypt(users.get(i).getMobileNumber());
                    String mobileNumberHashed = SecurityUtils.hash(users.get(i).getMobileNumber());
                    update("users", new String[]{"mobile_number"}, new String[]{mobileNumber}, "id_user = '" + users.get(i).getIdUser() + "'");
                    update("users", new String[]{"mobile_numberHashed"}, new String[]{mobileNumberHashed}, "id_user = '" + users.get(i).getIdUser() + "'");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}