/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150903_1025_507 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration(
                "enrollment.digital.automatic",
                "true", null, "enrollment", new String[]{"notEmpty"}, "true|false");
        insertOrUpdateConfiguration(
                "enrollment.digital.userRole",
                "administrator", null, "enrollment", new String[]{"notEmpty"}, "administrator|transactions|readonly|login");

        insertOrUpdateConfiguration("core.attempts.digitalEnrollment.lockoutWindow", "8m", null, "enrollment", new String[]{"notEmpty", "interval"});
        insertOrUpdateConfiguration("core.attempts.digitalEnrollment.maxAttempts", "3", null, "enrollment", new String[]{"notEmpty", "integer"});
    }
}
