/*
 *  Copyright 2014 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2014;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB201408201834_429 extends DBVSUpdate{
    @Override
    public void up() {
        // FOREIGN KEY
        String[] dialects = new String[]{DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB};
        customSentence(dialects, "ALTER TABLE communications ADD CONSTRAINT fk_comm_client FOREIGN KEY (id_client_user) REFERENCES client_users(id_user)");
    }
}
