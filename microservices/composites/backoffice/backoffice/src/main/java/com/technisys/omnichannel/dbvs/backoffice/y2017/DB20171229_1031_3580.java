/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3580
 *
 * @author isilveira
 */
public class DB20171229_1031_3580 extends DBVSUpdate {

    @Override
    public void up() {
        String[] dialects = new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB, DBVS.DIALECT_ORACLE};
        String sentence = "UPDATE configuration "
                        + "SET value = REPLACE(value, 'com.technisys.omnichannel.credentials.development', 'com.technisys.omnichannel.client.credentials.development') "
                        + "WHERE value LIKE 'com.technisys.omnichannel.credentials.development%'";
        
        customSentence(dialects, sentence);
        
        sentence = "UPDATE configuration " 
                 + "SET possible_values = REPLACE(possible_values, 'com.technisys.omnichannel.credentials.development', 'com.technisys.omnichannel.client.credentials.development') "
                 + "WHERE possible_values LIKE '%com.technisys.omnichannel.credentials.development%'";
        
        customSentence(dialects, sentence);
    }
}