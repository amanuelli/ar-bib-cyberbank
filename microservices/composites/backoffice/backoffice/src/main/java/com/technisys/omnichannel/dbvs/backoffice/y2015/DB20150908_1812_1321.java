/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author diego
 */
public class DB20150908_1812_1321 extends DBVSUpdate {

    @Override
    public void up() {        
        insertOrUpdateConfiguration("form.updateJBPMStatus.aceptar", "FINISHED", null, null, new String[]{});
        insertOrUpdateConfiguration("form.updateJBPMStatus.rechazar", "CANCELLED", null, null, new String[]{});
    }
}
