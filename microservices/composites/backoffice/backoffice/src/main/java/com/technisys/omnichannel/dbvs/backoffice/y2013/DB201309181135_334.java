/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201309181135_334 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("backoffice.emailValidationFormat", "^[a-zA-ZñÑ0-9._%+-]+@[a-zñÑA-Z0-9.-]+\\.(?:[a-zA-ZñÑ]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$");
        updateConfiguration("rubicon.emailValidationFormat", "^[a-zA-ZñÑ0-9._%+-]+@[a-zñÑA-Z0-9.-]+\\.(?:[a-zA-ZñÑ]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$");
    }
}