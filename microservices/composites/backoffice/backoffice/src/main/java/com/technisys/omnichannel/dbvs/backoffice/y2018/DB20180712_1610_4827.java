package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4827
 *
 * @author arameau
 */
public class DB20180712_1610_4827 extends DBVSUpdate {

    @Override
    public void up() {
    	//Se actualizan algunos form_message para uniformizar el criterio (que tenga una sola mayúscula y que sea al inicio)
    	
    	update("form_messages", new String[]{"value"}, new String[]{"Cash advance"}, "id_form = 'cashAdvance' and lang ='en'");
    	update("form_messages", new String[]{"value"}, new String[]{"Avance en efectivo"}, "id_form = 'cashAdvance' and lang ='es'");
    	
    	update("form_messages", new String[]{"value"}, new String[]{"Credit card application"}, "id_form = 'creditCardRequest' and lang ='en'");
    	
    	update("form_messages", new String[]{"value"}, new String[]{"Submission of letter of credit documents"}, "id_form = 'letterCredit' and lang ='en'");
    	update("form_messages", new String[]{"value"}, new String[]{"Apresentação de documentos de carta de crédito"}, "id_form = 'letterCredit' and lang ='pt'");

    	update("form_messages", new String[]{"value"}, new String[]{"Solicitud de préstamo"}, "id_form = 'requestLoan' and lang ='es'");
    	update("form_messages", new String[]{"value"}, new String[]{"Solicitação de empréstimo"}, "id_form = 'requestLoan' and lang ='pt'");
    	
    	update("form_messages", new String[]{"value"}, new String[]{"Solicitud de cheque gerencial"}, "id_form = 'requestOfManagementCheck' and lang ='es'");
    	update("form_messages", new String[]{"value"}, new String[]{"Pedido de cheque de gerência"}, "id_form = 'requestOfManagementCheck' and lang ='pt'");
    	
    	update("form_messages", new String[]{"value"}, new String[]{"International money transfer"}, "id_form = 'transferForeign' and lang ='en'");
    	
    	update("form_messages", new String[]{"value"}, new String[]{"Transferência interna"}, "id_form = 'transferInternal' and lang ='pt'");
    }

}