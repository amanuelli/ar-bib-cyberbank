/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.i18n.I18n;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-3234
 *
 * @author mjoseph
 */
public class DB20170907_1555_3234 extends DBVSUpdate {

    @Override
    public void up() {
        String fecha = "'"+(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date())+"'";
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            fecha = "TO_DATE("+fecha+", 'YYYY-MM-DD HH24:MI:SS')";
        } 
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                 "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                + " VALUES ('fields.creditCardChangeCondition.modifySelector.requiredError', '" + I18n.LANG_SPANISH + "', "
                + "'modifySelector', 'creditCardChangeCondition', '1', 'Debe al menos seleccionar una condición a modificar', " + fecha + ")");

        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                 "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                + " VALUES ('fields.creditCardChangeCondition.modifySelector.requiredError', '" + I18n.LANG_PORTUGUESE + "', "
                + "'modifySelector', 'creditCardChangeCondition', '1', 'Você deve pelo menos selecionar uma condição a ser modificada', " + fecha + ")");
        
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                 "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                + " VALUES ('fields.creditCardChangeCondition.modifySelector.requiredError', '" + I18n.LANG_ENGLISH + "', "
                + "'modifySelector', 'creditCardChangeCondition', '1', 'You must at least select a condition to be modified', " + fecha + ")");
    }

}
