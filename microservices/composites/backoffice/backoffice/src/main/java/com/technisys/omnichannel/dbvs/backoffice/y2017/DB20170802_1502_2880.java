/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2880
 *
 * @author jmanrique
 */
public class DB20170802_1502_2880 extends DBVSUpdate {

    @Override
    public void up() {
        insert("form_field_amount_currencies", new String[]{"id_field", "id_form", "form_version", "id_currency"}, new String[]{"amountOfCollection", "presentationOfDocumentsUnderCollection", "1", "222"});
    }
    
}
