/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-3837
 *
 * @author isilveira
 */
public class DB20180411_1523_3837 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("session.recoverPinAndPassword.step2");
        deleteActivity("session.recoverPinAndPassword.step3");
        
        insertActivity("session.recoverPinAndPassword.step1", "com.technisys.omnichannel.client.activities.recoverpinandpassword.RecoverPinAndPasswordStep1Activity", "recoverPinAndPassword", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("session.recoverPinAndPassword.step2", "com.technisys.omnichannel.client.activities.recoverpinandpassword.RecoverPinAndPasswordStep2Activity", "recoverPinAndPassword", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("session.recoverPinAndPassword.step3", "com.technisys.omnichannel.client.activities.recoverpinandpassword.RecoverPinAndPasswordStep3Activity", "recoverPinAndPassword", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
    }
}