/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Marcelo Bruno
 */

public class DB20200615_1743_10401 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("email.validationFormat", "^[a-zA-Z0-9+&*-_]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$", ConfigurationGroup.TECNICAS, null, new String[]{});
    }

}