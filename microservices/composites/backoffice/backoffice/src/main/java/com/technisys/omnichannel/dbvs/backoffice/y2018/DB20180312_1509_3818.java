/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-3818
 *
 * @author isilveira
 */
public class DB20180312_1509_3818 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "UPDATE configuration SET channels='frontend' WHERE id_field='core.communications.communicationTypes'");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.message.default', 'default', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.accepted.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.rejected.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.pending.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.transferReceived.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.creditCardExpiration.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.loanExpiration.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.creditCardPayment.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.loanPayment.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.promos.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.userBlock.default', '', 'communications', 'frontend')");
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "INSERT INTO configuration (id_field, value, id_sub_group, channels) VALUES ('core.communications.communicationTypes.transactionReminders.default', '', 'communications', 'frontend')");
        
        deleteActivity("preferences.notifications.configuration.modify.preview");
        insertActivity("preferences.notifications.configuration.pre", "com.technisys.omnichannel.client.activities.preferences.notificationsconfiguration.NotificationsConfigurationPreActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        customSentence(new String[] { DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL }, "UPDATE activity_products SET id_permission='core.authenticated' WHERE id_activity='preferences.notifications.configuration.modify'");
    }
}