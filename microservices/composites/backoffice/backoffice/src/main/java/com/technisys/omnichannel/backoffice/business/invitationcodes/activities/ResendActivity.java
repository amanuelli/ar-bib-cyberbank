/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.activities;

import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ResendInvitationCodeData;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.CreateResponse;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.onboarding.Step5Activity;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.TransactionRequest;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sebastian Barbosa
 */
public class ResendActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        CreateResponse response = new CreateResponse(request);

        try {
            TransactionRequest tRequest = (TransactionRequest) request;
            ResendInvitationCodeData data = (ResendInvitationCodeData) tRequest.getTransactionData();

            InvitationCode oldInvitationCode = InvitationCodesHandler.readInvitationCode(data.getIdCode());
            InvitationCode newInvitationCode = oldInvitationCode;
            InvitationCodesHandler.updateForResesedInvitationCode(newInvitationCode.getId());

            newInvitationCode = InvitationCodesHandler.readInvitationCode(oldInvitationCode.getId());

            String invitationCodePlain = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
            InvitationCodesHandler.cancelInvitationCode(oldInvitationCode.getId());
            InvitationCodesHandler.createInvitationCode(newInvitationCode, invitationCodePlain);

            Step5Activity.sendInvitationEmail(newInvitationCode, request, invitationCodePlain, null);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        TransactionRequest tRequest = (TransactionRequest) request;
        ResendInvitationCodeData data = (ResendInvitationCodeData) tRequest.getTransactionData();
        Map<String, String> result = new HashMap<>();

        try {
            InvitationCode code = InvitationCodesHandler.readInvitationCode(data.getIdCode());
            if (code == null || !InvitationCode.STATUS_NOT_USED.equals(code.getStatus())) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED, "Se esta intentando reenviar un código de invitación inexistente o ya utilizado");
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }
}
