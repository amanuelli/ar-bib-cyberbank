/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107121049_10647 extends DBVSUpdate {

    @Override
    public void up() {
        update("forms", new String[]{"description"}, new String[]{"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."}, "id_form = 41");
        update("forms", new String[]{"description"}, new String[]{"Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."}, "id_form = 67");
        update("forms", new String[]{"description"}, new String[]{"Obtenga para su empresa la l&iacute;nea de pre-financiaci&oacute;n necesaria para financiar sus exportaciones. Esta financiaci&oacute;n puede otorgarse dentro o fuera de la circular 1456 del BCU obteniendo as&iacute; los beneficios que el Estado otorga a exportadores."}, "id_form = 68");
        update("forms", new String[]{"description"}, new String[]{"Le asesoramos y le brindamos servicio integral para la administraci&oacute;n de la cobranza de exportaciones de su empresa. Avisamos y acreditamos en el d&iacute;a la recepci&oacute;n de los fondos provenientes de sus cobranzas."}, "id_form = 69");
    }
}