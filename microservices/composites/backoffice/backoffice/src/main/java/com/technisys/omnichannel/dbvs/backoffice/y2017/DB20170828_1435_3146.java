/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author mcommand
 */
public class DB20170828_1435_3146 extends DBVSUpdate {

    @Override
    public void up() {
        
        update("forms", new String[]{"category"}, new String[]{null}, "id_form = 'requestTransactionCancellation'");        
        
    }
}
