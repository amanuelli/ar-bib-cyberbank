/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ?
 */
public class DB201201091550_11728 extends DBVSUpdate {

    @Override
    public void up() {

        //keys de configuracion
        String[] fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};
        String[] fieldValues = new String[]{"rubicon.securitySeal.path", "c:/tmp/seals", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};
        fieldValues = new String[]{"rubicon.securitySeal.maxSize", "300", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        //activities
        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};
        fieldValues = new String[]{"rub.userconfiguration.loadSeals", "1", "1", "com.technisys.rubicon.business.userconfiguration.activities.LoadSealsActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};
        fieldValues = new String[]{"rub.userconfiguration.uploadSeal", "1", "1", "com.technisys.rubicon.business.userconfiguration.activities.UploadSealActivity"};
        insert("activities", fieldNames, fieldValues);

    }
}