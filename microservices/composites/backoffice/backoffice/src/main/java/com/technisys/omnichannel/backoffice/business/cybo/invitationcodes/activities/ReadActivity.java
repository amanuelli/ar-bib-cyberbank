/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.cybo.Response;
import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.ReadRequest;
import com.technisys.omnichannel.backoffice.domain.cybo.invitationcodes.InvitationCode;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author azeballos
 */
public class ReadActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        Response<InvitationCode> response = new Response<>(request);

        try {
            ReadRequest req = (ReadRequest) request;
            com.technisys.omnichannel.core.domain.InvitationCode code = InvitationCodesHandler.readInvitationCode(req.getInvitationCodeID());

            // Enmascaro el código de invitación
            code.setInvitationCodeMasked(maskInvitationCode(code.getInvitationCode()));

            response.setData(new InvitationCode(code));
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            ReadRequest req = (ReadRequest) request;
            if (InvitationCodesHandler.readInvitationCode(req.getInvitationCodeID()) == null) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }

    private static String maskInvitationCode(String code) {
        int maskLength = StringUtils.length(code) - ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "backoffice.invitationCodes.unmaskedLength", 4);
        return maskLength > 0 ? "****" + code.substring(maskLength) : code;
    }
}
