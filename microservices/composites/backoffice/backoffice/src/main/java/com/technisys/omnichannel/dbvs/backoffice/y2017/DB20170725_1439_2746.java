/* 
 * Copyright 2017 Manentia Software. 
 * 
 * This software component is the intellectual property of Manentia Software S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * http://www.manentiasoftware.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2746
 *
 * @author ldurand
 */
public class DB20170725_1439_2746 extends DBVSUpdate {

    String idForm = "requestOfManagementCheck";
    String version = "1";
    String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
    String clientFormId = "client.form." + idForm + ".send";
    String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};
    String[] formFieldText = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
    String[] formFieldAmount = new String[]{"id_field", "id_form", "form_version", "display_type", "id_ps_currency_check", "control_limits", "control_limit_over_product", "use_for_total_amount"};
    String[] formFieldSelector = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"};
    String[] formFieldPS = new String[]{"id_field", "id_form", "form_version", "display_type", "id_ps_currency_check", "show_other_option", "show_other_permission", "show_other_id_ps", "show_other_pfnovc", "filter_actives", "show_other_option_in_mobile", "show_other_by", "apply_frequent_destinations"};
    String[] formFieldDocument = new String[]{"id_field", "id_form", "form_version", "default_country", "default_document_type"};
    String[] formMessagesFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
    String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
    String[] formFieldTermsConditions = new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"};
    String[] formFieldValues;
    Map<String, String> messages = new HashMap();

    @Override
    public void up() {

        //FORMULARIO
        String[] formsFields = new String[]{"id_form", "version", "enabled", "category", "type", "admin_option", "id_activity", "last", "deleted", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow", "id_bpm_process", "programable"};
        String[] formsFieldsValues = new String[]{idForm, version, "1", "accounts", "process", "checkbooks", null, "1", "0", "1", "0", "0", "1", "1", "demo:1:3", "0"};

        insert("forms", formsFields, formsFieldsValues);

        //INTERNACIONALIZACION FORM
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" + idForm + "', '" + version + "', 'es', 'Solicitud de Cheque Gerencia', TO_DATE('2017-04-03 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessagesFieldsValuesES = new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Solicitud de Cheque Gerencia", date};
            insert("form_messages", formMessagesFields, formMessagesFieldsValuesES);
        }

        //PERMISOS
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "accessToken-pin"});

        //CAMPOS
        //Address
        formFieldValues = new String[]{"address", idForm, version, "text", "6", "value(placeOfRetreat) == 'entregaADomicilio'", "value(placeOfRetreat) == 'entregaADomicilio'", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_text", formFieldText, new String[]{"address", idForm, version, "1", "200", "field-medium", null});

        //Amount
        formFieldValues = new String[]{"amount", idForm, version, "amount", "2", "TRUE", "TRUE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_amount", formFieldAmount, new String[]{"amount", idForm, version, "field-small", null, "0", null, "0"});

        //CostDebitAccount
        formFieldValues = new String[]{"costDebitAccount", idForm, version, "productselector", "3", "TRUE", "TRUE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_ps", formFieldPS, new String[]{"costDebitAccount", idForm, version, "field-medium", null, "0", null, null, null, "0", "0", null, "0"});

        //debitAccount
        formFieldValues = new String[]{"debitAccount", idForm, version, "productselector", "1", "TRUE", "TRUE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_ps", formFieldPS, new String[]{"debitAccount", idForm, version, "field-normal", null, "0", null, null, null, "0", "0", null, "0"});

        //Document
        formFieldValues = new String[]{"document", idForm, version, "document", "7", "TRUE", "TRUE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_document", formFieldDocument, new String[]{"document", idForm, version, "UY", "CI"});

        //Name
        formFieldValues = new String[]{"name", idForm, version, "text", "8", "TRUE", "TRUE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_text", formFieldText, new String[]{"name", idForm, version, "1", "50", "field-normal", null});

        //NameOfBeneficiary
        formFieldValues = new String[]{"nameOfBeneficiary", idForm, version, "text", "4", "TRUE", "TRUE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_text", formFieldText, new String[]{"nameOfBeneficiary", idForm, version, "1", "100", "field-normal", null});

        //PlaceOfRetreat
        formFieldValues = new String[]{"placeOfRetreat", idForm, version, "selector", "5", "TRUE", "TRUE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_selector", formFieldSelector, new String[]{"placeOfRetreat", idForm, version, "field-normal", "entregaADomicilio", "0", "combo"});

        //SecondSurname
        formFieldValues = new String[]{"secondSurname", idForm, version, "text", "10", "TRUE", "TRUE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_text", formFieldText, new String[]{"secondSurname", idForm, version, "1", "50", "field-normal", null});

        //Surname
        formFieldValues = new String[]{"surname", idForm, version, "text", "9", "TRUE", "TRUE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_text", formFieldText, new String[]{"surname", idForm, version, "1", "50", "field-normal", null});

        //termsandconditions - Disclaimer
        formFieldValues = new String[]{"disclaimer", idForm, version, "termsandconditions", "11", "TRUE", "FALSE", null, "1", "0", "default", "0"};

        insert("form_fields", formFields, formFieldValues);
        insert("form_field_terms_conditions", formFieldTermsConditions, new String[]{"disclaimer", idForm, version, "field-big", "1", "0"});

        //MENSAJES
        messages.put("address.label", "Domicilio");
        messages.put("address.requiredError", "El domicilio es requerido");
        messages.put("amount.label", "Importe");
        messages.put("amount.requiredError", "El importe es requerido");
        messages.put("costDebitAccount.label", "Cuenta débito de costo");
        messages.put("costDebitAccount.requiredError", "El campo cuenta de débito es requerido");
        messages.put("debitAccount.label", "Cuenta débito");
        messages.put("debitAccount.requiredError", "El campo cuenta es requerido");
        messages.put("document.label", "Documento");
        messages.put("name.label", "Nombre");
        messages.put("name.requiredError", "El campo nombre es requerido");
        messages.put("nameOfBeneficiary.label", "Nombre del Beneficiario");
        messages.put("placeOfRetreat.label", "Lugar de Retiro");
        messages.put("placeOfRetreat.option.EntregaADomicilio", "Entrega a domicilio");
        messages.put("placeOfRetreat.requiredError", "El lugar de retiro es requerido");
        messages.put("secondSurname.label", "Segundo Apellido");
        messages.put("secondSurname.requiredError", "El campo segundo apellido es requerido");
        messages.put("surname.label", "Primer Apellido");
        messages.put("surname.requiredError", "El campo primer apellido es requerido");
        messages.put("disclaimer.label", null);
        messages.put("disclaimer.termsAndConditions", "Las instrucciones enviadas al banco por usted luego de las <hora de corte> serán procesadas al siguiente día hábil bancario. Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.");

        for (String key : messages.keySet()) {
            String[] formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }
}
