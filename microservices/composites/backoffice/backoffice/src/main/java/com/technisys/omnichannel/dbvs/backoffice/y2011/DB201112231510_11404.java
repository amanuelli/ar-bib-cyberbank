/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 * @author ?
 */
public class DB201112231510_11404 extends DBVSUpdate {

    @Override
    public void up() {

        //keys de configuracion
        String[] fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};
        String[] fieldValues = new String[]{"rubicon.enrollment.emailVerification.url", "http://localhost:8080/rubicon/enrollment/emailVerification", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.enrollment.invitationCode.validity", "48", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.enrollment.affiliation.jbpmProcessId", "demo-1", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.enrollment.environmentType", "retail", "retail" + "|" + "corporate", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.administratorRole", "client_administrator", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.defaultLanguage", "es", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        //activities
        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};
        fieldValues = new String[]{"rub.enrollment.verifyInvitationCode", "1", "1", "com.technisys.rubicon.business.enrollment.activities.VerifyInvitationCodeActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.enrollment.verifyEmailVerificationLink", "1", "1", "com.technisys.rubicon.business.enrollment.activities.VerifyEmailVerificationLinkActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.enrollment.validateUserAndPassword", "1", "1", "com.technisys.rubicon.business.enrollment.activities.ValidateUserAndPasswordActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.enrollment.readClientInformation", "1", "1", "com.technisys.rubicon.business.enrollment.activities.ReadClientInformationActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.enrollment.finishEnrollment", "1", "1", "com.technisys.rubicon.business.enrollment.activities.FinishEnrollmentActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.enrollment.checkAffiliationForm", "1", "1", "com.technisys.rubicon.business.enrollment.activities.CheckAffiliationFormActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.enrollment.affiliation", "1", "1", "com.technisys.rubicon.business.enrollment.activities.AffiliationActivity"};
        insert("activities", fieldNames, fieldValues);

        //tabla de asociacion de clientes y ambientes
        ColumnDefinition[] columns = new ColumnDefinition[]{
            new ColumnDefinition("id_client", Types.INTEGER, 10, 0, false, null),
            new ColumnDefinition("id_environment", Types.INTEGER, 10, 0, false, null)
        };

        String[] primaryKey = new String[]{"id_client", "id_environment"};
        createTable("client_environments", columns, primaryKey);

        //inserto en client_environments los ambientes que hay hoy en dia
        delete("client_environments", null);
        fieldNames = new String[]{"id_client", "id_environment"};
        fieldValues = new String[]{"2", "2"};
        insert("client_environments", fieldNames, fieldValues);
    }
}