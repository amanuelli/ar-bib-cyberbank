/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3473
 *
 * @author ahernandez
 */
public class DB20171226_1409_3473 extends DBVSUpdate {

    @Override
    public void up() {
        insert("widgets", new String[]{"id", "uri"}, new String[]{"fingerprint", "widgets/fingerprint"});
        
        customSentence(new String[] {DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB}, "update desktop_layouts set row_number = row_number+1");
        customSentence(new String[] {DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB}, "insert into desktop_layouts (id_user, id_environment, id_widget, column_number, row_number) select distinct id_user, id_environment, 'fingerprint', 1, 1 from desktop_layouts");
        customSentence(new String[] {DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB}, "update configuration set value = concat('fingerprint|', value) where id_field='environments.desktopLayout.corporate' or id_field='environments.desktopLayout.retail'");
    }

}