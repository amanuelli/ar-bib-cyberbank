/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2021;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;

/**
 * Related issue: TECCDPBO-4400
 *
 * @author azeballos
 */
public class DB20210820_1913_4400 extends DBVSUpdate {

    @Override
    public void up() {
        update("backoffice_activities", new String[]{"id_permission_approval"}, new String[]{"backoffice.invitationCodes.approve"}, "id_activity = 'cybo.invitationcodes.create'");

        delete("backoffice_activities", "id_activity = 'cybo.invitationcodes.cancel'");
        insertBackofficeActivity("cybo.invitationcodes.cancel",
                "com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities.CancelActivity",
                "backoffice.invitationCodes.manage", "backoffice.invitationCodes.approve", "backoffice.invitationcodes",
                ActivityDescriptor.AuditLevel.Full);
    }
}
