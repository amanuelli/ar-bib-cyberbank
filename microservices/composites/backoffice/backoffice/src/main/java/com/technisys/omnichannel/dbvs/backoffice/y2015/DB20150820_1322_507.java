/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150820_1322_507 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration(
                "frontend.i18n.languages",
                "es|en|pt",
                null, "frontend", new String[]{"notEmpty"});

        insertOrUpdateConfiguration(
                "enrollment.personalData.enabled",
                "true", null, "enrollment", new String[]{"notEmpty"}, "true|false");

        insertOrUpdateConfiguration(
                "enrollment.newUser.status",
                "active", null, "enrollment", new String[]{"notEmpty"}, "active|pending|blocked");

        insertOrUpdateConfiguration(
                "enrollment.verificationCode.length",
                "4",
                null, "enrollment", new String[]{"notEmpty", "integer"});

        insertOrUpdateConfiguration(
                "enrollment.verificationCode.maxFailedAttempts",
                "5",
                null, "enrollment", new String[]{"notEmpty", "integer"});

        insertOrUpdateConfiguration(
                "username.pattern",
                "[a-zA-Z0-9@_\\.-]*",
                null, "frontend", new String[]{"notEmpty"});

        insertOrUpdateConfiguration(
                "username.minLength",
                "5",
                null, "frontend", new String[]{"notEmpty", "integer"});

        insertOrUpdateConfiguration(
                "username.maxLength",
                "50",
                null, "frontend", new String[]{"notEmpty", "integer"});

        // Comunicaciones por defecto
        insertOrUpdateConfiguration(
                "core.communications.communicationTypes.accepted",
                "WEB",
                ConfigurationGroup.NEGOCIO, "other", new String[]{"notEmpty"});
        insertOrUpdateConfiguration(
                "core.communications.communicationTypes.message",
                "WEB",
                ConfigurationGroup.NEGOCIO, "other", new String[]{"notEmpty"});
        insertOrUpdateConfiguration(
                "core.communications.communicationTypes.rejected",
                "WEB",
                ConfigurationGroup.NEGOCIO, "other", new String[]{"notEmpty"});
        insertOrUpdateConfiguration(
                "core.communications.communicationTypes.warning",
                "WEB",
                ConfigurationGroup.NEGOCIO, "other", new String[]{"notEmpty"});

        // Permisos por defecto
        updateConfiguration("client.permissions.defaults", "core.authenticated|core.loginWithOTP|core.loginWithPassword|core.loginWithPIN|user.preferences");

        delete("configuration", "id_field like 'client.permissions.%.none'");

        //Roles -- administrator|transactions|readonly|login
        // Target NONE
        insertOrUpdateConfiguration(
                "client.permissions.administrator.none",
                "position|administration.manage|administration.view",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.transactions.none",
                "position",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.readonly.none",
                "position",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.login.none",
                "",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});

        // Target Generic
        // CA, CC, PF, PI, PA, TC
        insertOrUpdateConfiguration(
                "client.permissions.administrator.allCA",
                "product.read|pay.loan|pay.loan.thirdParties|transfer.internal|transfer.thirdParties",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.administrator.allCC",
                "product.read|pay.loan|pay.loan.thirdParties|transfer.internal|transfer.thirdParties", 
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.administrator.allPF",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.administrator.allPI",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.administrator.allPA",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.administrator.allTC",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});

        //-
        insertOrUpdateConfiguration(
                "client.permissions.transactions.allCA",
                "product.read|pay.loan|pay.loan.thirdParties|transfer.internal|transfer.thirdParties",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.transactions.allCC",
                "product.read|pay.loan|pay.loan.thirdParties|transfer.internal|transfer.thirdParties",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.transactions.allPF",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.transactions.allPI",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.transactions.allPA",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.transactions.allTC",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});

        //-
        insertOrUpdateConfiguration(
                "client.permissions.readonly.allCA",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.readonly.allCC",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.readonly.allPF",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.readonly.allPI",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.readonly.allPA",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.readonly.allTC",
                "product.read",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});

        //-
        insertOrUpdateConfiguration(
                "client.permissions.login.allCA",
                "",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.login.allCC",
                "",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.login.allPF",
                "",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.login.allPI",
                "", 
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.login.allPA",
                "",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
        insertOrUpdateConfiguration(
                "client.permissions.login.allTC",
                "",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});

        // URL de enrollment
        updateConfiguration("administration.users.invite.enrollmentUrl", "${BASE_URL}/enrollment/start-code");

    }
}
