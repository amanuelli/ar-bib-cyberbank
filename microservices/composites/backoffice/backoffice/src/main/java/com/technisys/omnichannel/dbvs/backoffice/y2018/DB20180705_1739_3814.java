/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor.AuditLevel;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor.Kind;

/**
 * Related issue: MANNAZCA-3814
 *
 * @author isilveira
 */
public class DB20180705_1739_3814 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[] { "channels" }, new String[] { "frontend" }, "id_field = 'enrollment.digital.automatic' or id_field = 'enrollment.verificationCode.length' or id_field = 'enrollment.verificationCode.maxFailedAttempts' or id_field = 'backoffice.invitationCodes.unmaskedLength'");
        updateConfiguration("core.userconfiguration.password.pattern", "^((?=.*[0-9])(?=.*[A-Za-zñÑ])(?=.*[_!@#$%&*()]).*)$");
    }
}