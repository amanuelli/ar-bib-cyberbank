/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ahernandez
 */

public class DB20191121_1151_357 extends DBVSUpdate {

    @Override
    public void up() {
        insert("ACT_RE_DEPLOYMENT", new String[]{"ID_"}, new String[]{"2"});
        String bpmnFlow = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" +
                "<bpmn:definitions xmlns:bpmn=\"http://www.omg.org/spec/BPMN/20100524/MODEL\" xmlns:bpmndi=\"http://www.omg.org/spec/BPMN/20100524/DI\" xmlns:di=\"http://www.omg.org/spec/DD/20100524/DI\" xmlns:dc=\"http://www.omg.org/spec/DD/20100524/DC\" xmlns:camunda=\"http://camunda.org/schema/1.0/bpmn\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" id=\"Definitions_1\" targetNamespace=\"http://bpmn.io/schema/bpmn\" exporter=\"Camunda Modeler\" exporterVersion=\"3.4.1\">\n" +
                "  <bpmn:process id=\"request-account\" name=\"Request Account\" isClosed=\"false\" isExecutable=\"true\" camunda:versionTag=\"1\">\n" +
                "    <bpmn:startEvent id=\"inicio\" name=\"Inicio\">\n" +
                "      <bpmn:outgoing>SequenceFlow_18t0i75</bpmn:outgoing>\n" +
                "    </bpmn:startEvent>\n" +
                "    <bpmn:userTask id=\"enEjecucion\" name=\"En ejecución\" camunda:candidateGroups=\"10000,10001\">\n" +
                "      <bpmn:incoming>SequenceFlow_18t0i75</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_1mk2slu</bpmn:outgoing>\n" +
                "    </bpmn:userTask>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_18t0i75\" sourceRef=\"inicio\" targetRef=\"enEjecucion\" />\n" +
                "    <bpmn:exclusiveGateway id=\"ExclusiveGateway_0qmslys\">\n" +
                "      <bpmn:incoming>SequenceFlow_1mk2slu</bpmn:incoming>\n" +
                "      <bpmn:outgoing>rechazar</bpmn:outgoing>\n" +
                "      <bpmn:outgoing>aceptar</bpmn:outgoing>\n" +
                "    </bpmn:exclusiveGateway>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_1mk2slu\" sourceRef=\"enEjecucion\" targetRef=\"ExclusiveGateway_0qmslys\" />\n" +
                "    <bpmn:endEvent id=\"aceptado\" name=\"Aceptado\">\n" +
                "      <bpmn:incoming>SequenceFlow_1efkp7d</bpmn:incoming>\n" +
                "    </bpmn:endEvent>\n" +
                "    <bpmn:endEvent id=\"rechazado\" name=\"Rechazado\">\n" +
                "      <bpmn:incoming>SequenceFlow_05mvt8h</bpmn:incoming>\n" +
                "      <bpmn:errorEventDefinition errorRef=\"Error_07g0lf5\" />\n" +
                "    </bpmn:endEvent>\n" +
                "    <bpmn:sequenceFlow id=\"rechazar\" name=\"Rechazar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0knb9w2\">\n" +
                "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\">${action == ''rechazar''}</bpmn:conditionExpression>\n" +
                "    </bpmn:sequenceFlow>\n" +
                "    <bpmn:sequenceFlow id=\"aceptar\" name=\"Aceptar\" sourceRef=\"ExclusiveGateway_0qmslys\" targetRef=\"Task_0xnr1oc\">\n" +
                "      <bpmn:conditionExpression xsi:type=\"bpmn:tFormalExpression\">${action == ''aceptar''}</bpmn:conditionExpression>\n" +
                "    </bpmn:sequenceFlow>\n" +
                "    <bpmn:serviceTask id=\"Task_0xnr1oc\" name=\"Aceptando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.RequestAccount\">\n" +
                "      <bpmn:incoming>aceptar</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_12ktbww</bpmn:outgoing>\n" +
                "    </bpmn:serviceTask>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_12ktbww\" sourceRef=\"Task_0xnr1oc\" targetRef=\"Task_1l4zkyo\" />\n" +
                "    <bpmn:serviceTask id=\"Task_0knb9w2\" name=\"Rechazando\" camunda:class=\"com.technisys.omnichannel.core.bpm.servicetasks.CancelTransaction\">\n" +
                "      <bpmn:incoming>rechazar</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_0gd6rpt</bpmn:outgoing>\n" +
                "    </bpmn:serviceTask>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_0gd6rpt\" sourceRef=\"Task_0knb9w2\" targetRef=\"SendTask_0hn6yj8\" />\n" +
                "    <bpmn:sendTask id=\"Task_1l4zkyo\" name=\"Notificando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.SendNotifications\">\n" +
                "      <bpmn:incoming>SequenceFlow_12ktbww</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_1efkp7d</bpmn:outgoing>\n" +
                "    </bpmn:sendTask>\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_1efkp7d\" sourceRef=\"Task_1l4zkyo\" targetRef=\"aceptado\" />\n" +
                "    <bpmn:sequenceFlow id=\"SequenceFlow_05mvt8h\" sourceRef=\"SendTask_0hn6yj8\" targetRef=\"rechazado\" />\n" +
                "    <bpmn:sendTask id=\"SendTask_0hn6yj8\" name=\"Notificando\" camunda:class=\"com.technisys.omnichannel.client.bpm.servicetasks.SendNotifications\">\n" +
                "      <bpmn:incoming>SequenceFlow_0gd6rpt</bpmn:incoming>\n" +
                "      <bpmn:outgoing>SequenceFlow_05mvt8h</bpmn:outgoing>\n" +
                "    </bpmn:sendTask>\n" +
                "  </bpmn:process>\n" +
                "  <bpmn:error id=\"Error_07g0lf5\" name=\"Rechazado\" errorCode=\"rechazado\" />\n" +
                "</bpmn:definitions>";

        customSentence(DBVS.DIALECT_MYSQL, "INSERT INTO ACT_GE_BYTEARRAY (ID_, REV_, NAME_, DEPLOYMENT_ID_, BYTES_, GENERATED_) VALUES ('3', '1', 'request-account.bpmn', '2', '" + bpmnFlow + "', '0')");
        customSentence(DBVS.DIALECT_MSSQL, "INSERT INTO ACT_GE_BYTEARRAY (ID_, REV_, NAME_, DEPLOYMENT_ID_, BYTES_, GENERATED_) VALUES ('3', '1', 'request-account.bpmn', '2', CAST('" + bpmnFlow + "' AS IMAGE), '0')");
        customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO ACT_GE_BYTEARRAY (ID_, REV_, NAME_, DEPLOYMENT_ID_, BYTES_, GENERATED_) VALUES ('3', '1', 'request-account.bpmn', '2', xmltype('" + bpmnFlow + "').getblobval(nls_charset_id('UTF8')), '0')");
        insert("ACT_RE_PROCDEF", new String[]{"ID_", "REV_", "CATEGORY_", "NAME_", "KEY_", "VERSION_", "DEPLOYMENT_ID_", "RESOURCE_NAME_", "HAS_START_FORM_KEY_", "SUSPENSION_STATE_", "VERSION_TAG_"},
                new String[]{"request-account:1:3", "1", "http://bpmn.io/schema/bpmn", "Request Account", "request-account", "1", "2", "request-account.bpmn", "0", "1", "1"});
        update("ACT_GE_PROPERTY", new String[]{"VALUE_", "REV_"}, new String[]{"101", "2"}, "NAME_='next.dbid'");
        update("forms", new String[]{"id_bpm_process"}, new String[]{"request-account:1:3"}, "id_form = 'accountOpening'");
    }

}