/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author aelters
 */
public class DB20150701_1337_509 extends DBVSUpdate {

    @Override
    public void up() {
        insert("widgets", new String[]{"id", "uri"}, new String[]{"nextExpirations", "widgets/next-expirations"});
        insertActivity("widgets.nextExpirations", "com.technisys.omnichannel.client.activities.widgets.NextExpirationsActivity", "desktop",ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        insertOrUpdateConfiguration("desktop.widgets.nextExpirations.limit", "99", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("nextExpirations.threshold.warning", "7", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("nextExpirations.threshold.error", "0", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
        
    }
}
