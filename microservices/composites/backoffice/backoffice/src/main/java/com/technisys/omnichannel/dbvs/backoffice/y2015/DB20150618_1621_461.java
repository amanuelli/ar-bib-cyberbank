/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150618_1621_461 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("files.allowedFileExtensions", "txt|doc|xls|ppt|docx|xlsx|pptx|jpg|bmp|gif|png", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("files.maxFileSize", "10485760", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty", "integer"});
    
        insertActivity("files.list", "com.technisys.omnichannel.client.activities.files.ListFileActivity", "files", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("files.delete", "com.technisys.omnichannel.client.activities.files.DeleteFileActivity", "files", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("files.save", "com.technisys.omnichannel.client.activities.files.SaveFileActivity", "files", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    
    }
}
