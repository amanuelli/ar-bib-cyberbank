/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

public class DB20180911_1409_5436 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("cellPhone.code.ARG", "+54", ConfigurationGroup.OTHER, null, new String[]{});
        insertOrUpdateConfiguration("cellPhone.code.BRA", "+55", ConfigurationGroup.OTHER, null, new String[]{});
        insertOrUpdateConfiguration("cellPhone.code.USA", "+1", ConfigurationGroup.OTHER, null, new String[]{});
        insertOrUpdateConfiguration("cellPhone.code.URY", "+598", ConfigurationGroup.OTHER, null, new String[]{});
        insertOrUpdateConfiguration("cellPhone.code.default", "URY", ConfigurationGroup.OTHER, null, new String[]{});
        insertOrUpdateConfiguration("country.codes", "ARG|BRA|USA|URY", ConfigurationGroup.OTHER, null, new String[]{});
        
        update("configuration", new String[] { "channels" }, new String[] { "frontend" }, "id_field like 'cellPhone.code.%' OR id_field='country.codes'");
        
    }
}