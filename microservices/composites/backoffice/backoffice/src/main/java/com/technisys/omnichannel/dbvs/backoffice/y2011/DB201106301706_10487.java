/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201106301706_10487 extends DBVSUpdate {

    @Override
    public void up() {
        // CONFIGURATION
        delete("configuration", "id_field = 'rubicon.sms.operations.T'");
        delete("configuration", "id_field = 'rubicon.sms.operations.CC'");
        delete("configuration", "id_field = 'rubicon.sms.operations.CT'");
        delete("configuration", "id_field = 'rubicon.sms.operations.CP'");

        String[] fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};
        String[] fieldValues = new String[]{"rubicon.sms.operations.T.idActivity", "rub.accounts.transferInternal", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);
        fieldValues = new String[]{"rubicon.sms.operations.T.activityVersion", "1", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.sms.operations.CC.idActivity", "rub.account.readAccountDetails", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);
        fieldValues = new String[]{"rubicon.sms.operations.CC.activityVersion", "1", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.sms.operations.CT.idActivity", "rub.creditcards.readCreditCard", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);
        fieldValues = new String[]{"rubicon.sms.operations.CT.activityVersion", "1", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.sms.operations.CP.idActivity", "rub.loans.readLoanDetails", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);
        fieldValues = new String[]{"rubicon.sms.operations.CP.activityVersion", "1", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);
    }
}