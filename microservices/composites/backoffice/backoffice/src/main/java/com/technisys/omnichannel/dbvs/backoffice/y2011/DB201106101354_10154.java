/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201106101354_10154 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, ';', 'go') where id_form = 1");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, ';', 'go') where id_form = 4");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, ';', 'go') where id_form = 14");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, ';', 'go') where id_form = 15");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(description, ';', 'go') where id_form = 16");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, ';', 'go') where id_form = 41");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(description, ';', 'go') where id_form = 64");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, 'Códi;', 'Código') where id_form = 68");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, 'Codi;', 'Codigo') where id_form = 68");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, ';', 'go') where id_form = 70");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, 'acutego', 'acute;') where id_form = 70");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, ';', 'go') where id_form = 81");
        customSentence(DBVS.DIALECT_MYSQL, "update forms set template_es = replace(template_es, 'acutego', 'acute;') where id_form = 81");
    }
}
