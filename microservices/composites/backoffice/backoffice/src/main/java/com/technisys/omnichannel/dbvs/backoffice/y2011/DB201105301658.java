/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author rzuasti
 */
public class DB201105301658 extends DBVSUpdate {

    @Override
    public void up() {
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-301-client-database.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-302-client-core-data.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-303-client-data.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-400-demo-users.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-401-demo-forms.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-402-demo-fields.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-404-demo-configuration.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-405-demo-banks.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-406-demo-jbpm.sql", ";", "UTF-8");

        executeScript(DBVS.DIALECT_HSQLDB, "database/hsqldb-301-client-database.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_HSQLDB, "database/hsqldb-302-client-core-data.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_HSQLDB, "database/hsqldb-303-client-data.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_HSQLDB, "database/hsqldb-400-demo-users.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_HSQLDB, "database/hsqldb-401-demo-forms.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_HSQLDB, "database/hsqldb-402-demo-fields.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_HSQLDB, "database/hsqldb-404-demo-configuration.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_HSQLDB, "database/hsqldb-405-demo-banks.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_HSQLDB, "database/hsqldb-406-demo-jbpm.sql", ";", "UTF-8");

        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-301-client-database.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-302-client-core-data.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-303-client-data.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-400-demo-users.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-401-demo-forms.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-402-demo-fields.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-404-demo-configuration.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-405-demo-banks.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_MSSQL, "database/mssql-406-demo-jbpm.sql", ";", "UTF-8");

        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-301-client-database.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-302-client-core-data.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-303-client-data.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-400-demo-users.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-401-demo-forms.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-402-demo-fields.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-404-demo-configuration.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-405-demo-banks.sql", ";", "UTF-8");
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-406-demo-jbpm.sql", ";", "UTF-8");
    }
}