/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.requests;

import com.technisys.omnichannel.core.IBRequest;

/**
 *
 * @author Sebastian Barbosa
 */
public class ReadRequest extends IBRequest {

    private int idInvitationCode;

    public int getIdInvitationCode() {
        return idInvitationCode;
    }

    public void setIdInvitationCode(int idInvitationCode) {
        this.idInvitationCode = idInvitationCode;
    }

    @Override
    public String toString() {
        return "ReadRequest{" + "idInvitationCode=" + idInvitationCode + '}';
    }
}
