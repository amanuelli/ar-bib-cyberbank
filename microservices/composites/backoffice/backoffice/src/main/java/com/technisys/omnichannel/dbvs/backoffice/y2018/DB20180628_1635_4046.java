package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author marcelobruno
 */
public class DB20180628_1635_4046 extends DBVSUpdate {

    @Override
    public void up() {
        update ("forms", new String[]{"templates_enabled"}, new String[]{"0"}, "id_form='reissueCreditCard'");
        
        String [] fieldNames = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value" };
        String [] fieldValues = new String[] { "fields.reissueCreditCard.reason.placeholder", "es", "reason", "reissueCreditCard", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un motivo de la lista" };
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            String insertSentence = String.format("INSERT INTO form_field_messages (%1$s, %2$s, %3$s, %4$s, %5$s, %6$s, %7$s) "
                                                + "VALUES ('%8$s', '%9$s', '%10$s', '%11$s', '%12$s', TO_DATE('%13$s', 'YYYY-MM-DD HH24:MI:SS'), '%14$s')",
                                                fieldNames[0], fieldNames[1], fieldNames[2], fieldNames[3], fieldNames[4], fieldNames[5], fieldNames[6],
                                                fieldValues[0], fieldValues[1], fieldValues[2], fieldValues[3], fieldValues[4], fieldValues[5], fieldValues[6]);
            
            customSentence(DBVS.DIALECT_ORACLE, insertSentence);
        } else {
            insert("form_field_messages", fieldNames, fieldValues);
        }
        
    }
    
}
