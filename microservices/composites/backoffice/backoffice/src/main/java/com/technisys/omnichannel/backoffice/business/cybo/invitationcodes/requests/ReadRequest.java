package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests;

import com.technisys.omnichannel.core.IBRequest;

public class ReadRequest extends IBRequest {
    
    private int invitationCodeID;

    public int getInvitationCodeID() {
        return invitationCodeID;
    }

    public void setInvitationCodeID(int invitationCodeID) {
        this.invitationCodeID = invitationCodeID;
    }

}
