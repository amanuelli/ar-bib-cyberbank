/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.activities;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ReadEnvironmentRequest;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.ReadEnvironmentResponse;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;

/**
 *
 * @author Sebastian Barbosa
 */
public class ReadEnvironmentActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        ReadEnvironmentResponse response = new ReadEnvironmentResponse(request);

        try {
            ReadEnvironmentRequest auxRequest = (ReadEnvironmentRequest) request;

            ClientEnvironment clientEnv = RubiconCoreConnectorC.readClient(request.getIdTransaction(), auxRequest.getAccount());
            response.setClientEnvironment(clientEnv);

            Environment environment = Administration.getInstance().readEnvironmentByProductGroupId(auxRequest.getAccount());
            if (environment != null) {
                response.setEnvironment(Administration.getInstance().readEnvironment(environment.getIdEnvironment()));
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            ReadEnvironmentRequest auxRequest = (ReadEnvironmentRequest) request;
            if (StringUtils.isBlank(auxRequest.getAccount())) {
                result.put("account", "activities.backoffice.invitationCodes.accountEmpty");
            } else if (auxRequest.getAccount().length() > 9) {
                result.put("account", "activities.backoffice.invitationCodes.accountMaxLengthExceeded");
            } else {
                ClientEnvironment clientEnv = RubiconCoreConnectorC.readClient(request.getIdTransaction(), auxRequest.getAccount());
                if (clientEnv == null) {
                    result.put("account", "activities.backoffice.invitationCodes.accountNotInBackend");
                }
            }

        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        }
        return result;
    }
}
