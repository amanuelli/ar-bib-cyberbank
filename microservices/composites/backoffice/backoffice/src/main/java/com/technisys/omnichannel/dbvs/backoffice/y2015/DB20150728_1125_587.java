/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150728_1125_587 extends DBVSUpdate {

    @Override
    public void up() {
        
        insertOrUpdateConfiguration("socialnetworks.facebook.appId", "1089018964447799", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.facebook.appSecret", "460c7b8468300b102a205232ce445f9a", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.twitter.apiKey", "hgRF0uZBxFGSFl1w3WBUpHDPk", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.twitter.apiSecret", "3vmjPVMJcV9r0pQOor5ii83pCeerZazKSxNjUvfldOikwrZyKI", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.twitter.accessToken", "3364350520-auV2f9DmDuE8DcISZc79OwYxNz6vKIykzB1H9El", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.twitter.accessTokenSecret", "Bb8rkQ86iP1KAaji5umit6ETS2XaUn99kBfXE3GQzntVC", ConfigurationGroup.TECNICAS, "socialnetworks", new String[]{"notEmpty"});
        
    }
}
