/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.activities;

import java.io.IOException;
import java.util.List;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.invitationcodes.responses.ListPreResponse;
import com.technisys.omnichannel.core.domain.InvitationCodesStatus;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;

/**
 * 
 * @author szuliani
 *
 */
public class ListPreActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        ListPreResponse response = new ListPreResponse(request);
        try {
            List<InvitationCodesStatus> statusList = InvitationCodesHandler.listInvitationCodesStatus();
            response.setStatusList(statusList);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
