/*
 * Copyright 2018 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3973
 *
 * @author aalves
 */
public class DB20180712_1730_3973 extends DBVSUpdate {
    @Override
    public void up() {
        update("form_fields", new String[]{"ordinal"}, new String[]{"1"}, "id_form='requestLoan' AND id_field = 'loanType'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"2"}, "id_form='requestLoan' AND id_field = 'fundsDestination'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"3"}, "id_form='requestLoan' AND id_field = 'amountToRequest'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"4"}, "id_form='requestLoan' AND id_field = 'amountOfFees'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"5"}, "id_form='requestLoan' AND id_field = 'iAuthorizeDebitToAccount'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"6"}, "id_form='requestLoan' AND id_field = 'lineaSeparadora'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"7"}, "id_form='requestLoan' AND id_field = 'termsAndCondLoan'");

        update("form_fields", new String[]{"required"}, new String[]{"TRUE"}, "id_form='requestLoan' AND id_field = 'fundsDestination'");
        update("form_fields", new String[]{"required"}, new String[]{"TRUE"}, "id_form='requestLoan' AND id_field = 'iAuthorizeDebitToAccount'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Monto"}, "id_message='fields.requestLoan.amountToRequest.label' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Amount"}, "id_message='fields.requestLoan.amountToRequest.label' AND lang='en'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Valor"}, "id_message='fields.requestLoan.amountToRequest.label' AND lang='pt'");

        update("form_field_messages", new String[]{"value"}, new String[]{"Cuenta de débito"}, "id_message='fields.requestLoan.iAuthorizeDebitToAccount.label' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Conta de débito"}, "id_message='fields.requestLoan.iAuthorizeDebitToAccount.label' AND lang='pt'");
    }
}
