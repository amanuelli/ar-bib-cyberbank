/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2021;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author dmazzuco
 */

public class DB20210205_1126_14604 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"JOB_CLASS_NAME"};
        String[] fieldValues = new String[]{"com.technisys.omnichannel.core.notifications.legacy.CommunicationsDaemonJob"};
        update("QRTZ_JOB_DETAILS",fieldNames,fieldValues,"JOB_NAME='communications'");
    }

}