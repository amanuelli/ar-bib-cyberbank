/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author lmcoussirat
 */
public class DB20170914_0928_3144 extends DBVSUpdate {

	public void up() {

		String[] fieldsNames = { "id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date" };
		String[] fieldsValues = { "fields.preFinancingConstitution.disclaimer.showAcceptOptionText", "es", "disclaimer", "preFinancingConstitution", "1", "Aceptar términos", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) };
		if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
			customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
					+ " VALUES ('" + fieldsValues[0] + "', '" + fieldsValues[1] + "', '" + fieldsValues[2] + "', '" + fieldsValues[3] + "', '" + fieldsValues[4] + "','" + fieldsValues[5] + "', TO_DATE('2017-09-14 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
		} else {
			insert("form_field_messages", fieldsNames, fieldsValues);
		}
		
	}
}