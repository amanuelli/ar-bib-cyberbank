/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor.Kind;

/**
 *
 * @author salva
 */
public class DB20150513_1350_382 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("accounts.list", "com.technisys.omnichannel.client.activities.accounts.ListAccountsActivity", "accounts", ActivityDescriptor.AuditLevel.Header, Kind.Other, "core.authenticated");
    }
}