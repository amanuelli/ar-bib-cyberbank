/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201204091251_11886 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames;
        String[] fieldValues;

        fieldNames = new String[]{"name", "uri", "default_column", "default_row"};
        fieldValues = new String[]{"virtualChannelsSavings", "/widgets/virtualChannelsSavings.jsp", "1", "1"};
        insert("client_desktop_widgets", fieldNames, fieldValues);

        update("configuration", new String[]{"value"}, new String[]{"transactions|news|notifications|frequentTasks|banners|virtualChannelsSavings"}, "id_field = 'rubicon.desktop.defaultWidgets'");
        update("configuration", new String[]{"value"}, new String[]{"transactions|news|notifications|frequentTasks|banners|pymes|goals|geolocalization|twitter|virtualChannelsSavings"}, "id_field = 'rubicon.desktop.defaultWidgets.corporate'");
        update("configuration", new String[]{"value"}, new String[]{"transactions|news|notifications|frequentTasks|banners|pymes|goals|geolocalization|twitter|virtualChannelsSavings"}, "id_field = 'rubicon.desktop.defaultWidgets.retail'");

        update("client_desktop_widgets", new String[]{"default_row"}, new String[]{"2"}, "name='pymes'");
        update("client_desktop_widgets", new String[]{"default_row"}, new String[]{"3"}, "name='goals'");
        update("client_desktop_widgets", new String[]{"default_row"}, new String[]{"4"}, "name='accounts'");
        update("client_desktop_widgets", new String[]{"default_row"}, new String[]{"5"}, "name='creditCards'");
        update("client_desktop_widgets", new String[]{"default_row"}, new String[]{"6"}, "name='loans'");
        update("client_desktop_widgets", new String[]{"default_row"}, new String[]{"7"}, "name='transactions'");
        update("client_desktop_widgets", new String[]{"default_row"}, new String[]{"8"}, "name='news'");
    }
}