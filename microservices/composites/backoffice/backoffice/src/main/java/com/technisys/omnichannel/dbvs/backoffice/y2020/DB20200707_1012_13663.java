/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

public class DB20200707_1012_13663 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='core.enabledChannels'");
    }

}