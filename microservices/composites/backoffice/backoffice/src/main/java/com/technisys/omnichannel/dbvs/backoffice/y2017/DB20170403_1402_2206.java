/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2206
 *
 * @author dimoda
 */
public class DB20170403_1402_2206 extends DBVSUpdate {

    @Override
    public void up() {
       
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsValues;
        String idForm = "additionalCreditCardRequest";
        String version = "1";
 
        Map<String, String> messages = new HashMap();
        
        /**** Insert Form - Solicitud de tarjeta de crédito ****/    
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[]{"id_form", "version", "enabled", "category", "type", "id_jbpm_process", "admin_option","id_activity", "templates_enabled", "drafts_enabled", "schedulable"};
        String[] formsFieldsValues = new String[]{idForm, version, "1", "creditcards", "process", "demo-1", "creditCards", null, "1", "1", "1"};
        insert("forms", formsFields, formsFieldsValues);
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', 'Solicitud de tarjeta de crédito adicional', TO_DATE('2017-04-03 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Solicitud de tarjeta de crédito adicional", date});
        }
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

     
        /**** Insert field product selector - Tarjeta ****/
        formFieldsValues = new String[]{"creditCard", idForm, version, "productselector", "1", "TRUE", "TRUE", "creditCardSelector", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{"creditCard", idForm, version, "field-normal"});
        insert("form_field_ps_product_types", new String[]{"id_field","id_form", "form_version", "id_product_type"}, new String[]{"creditCard", idForm, version, "TC"});
        insert("form_field_ps_permissions", new String[]{"id_field","id_form", "form_version", "id_permission"}, new String[]{"creditCard", idForm, version, "product.read"});  

        messages.put("creditCard.label", "Tarjeta");
        messages.put("creditCard.requiredError", "Debe seleccionar una tarjeta");
        
        /**** Insert field text - Nombre ****/
        formFieldsValues = new String[]{"name", idForm, version, "text", "2", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"}, new String[] {"name", idForm, version, "1", "100", "field-normal"});
  
        messages.put("name.label", "Nombre");
        messages.put("name.requiredError", "Debe ingresar el nombre del adicional");
        
               /**** Insert field text - Nombre y apellido ****/
        formFieldsValues = new String[]{"lastName", idForm, version, "text", "3", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"}, new String[] {"lastName", idForm, version, "1", "100", "field-normal"});

        messages.put("lastName.label", "Apellido");
        messages.put("lastName.requiredError", "Debe ingresar el apellido del adicional");

        
        /**** Insert field document - Documento ****/
        formFieldsValues = new String[]{"document", idForm, version, "document", "4", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_document",new String[]{"id_field", "id_form", "form_version", "default_country", "default_document_type"}, new String[]{"document", idForm, version, "UY", "CI"});

        messages.put("document.label", "Documento");
        messages.put("document.requiredError", "Debe ingresar un documento");

     
        /**** Insert field amount - Monto ****/
        formFieldsValues = new String[]{"amount", idForm, version, "amount", "5", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount"}, new String[]{"amount", idForm, version, "field-normal", "1", "1"});

        messages.put("amount.label", "Límite de compra");
        messages.put("amount.requiredError", "El límite debe ser mayor a cero y no superar el límite de la tarjeta");

        
        /****  Insert fied termsandcondition - Terminos y condiciones ****/
        formFieldsValues = new String[]{"termsandconditions", idForm, version, "termsandconditions", "6", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"}, new String[]{"termsandconditions", idForm, version, "field-normal", "1", "1"});

        messages.put("termsandconditions.label", "Términos y condiciones");
        messages.put("termsandconditions.requiredError", "Debe aceptar los términos y condiciones para continuar");
        messages.put("termsandconditions.showAcceptOptionText", "Aceptar");

        messages.put("termsandconditions.termsAndConditions", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industrys standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }

}
