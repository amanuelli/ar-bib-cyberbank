/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201107151509_10673 extends DBVSUpdate {

    @Override
    public void up() {
        // CONFIGURATION
        String[] fieldNames = new String[]{"id_field", "value", "id_group"};

        String[] fieldValues = new String[]{"rubicon.productAlias.regexPattern", "[a-zA-Z0-9]*", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.sms.regexPattern", "[a-zA-Z0-9 $]*", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

    }
}
