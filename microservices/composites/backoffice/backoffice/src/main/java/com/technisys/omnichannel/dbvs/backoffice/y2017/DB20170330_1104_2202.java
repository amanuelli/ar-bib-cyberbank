/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2202
 *
 * @author dimoda
 */
public class DB20170330_1104_2202 extends DBVSUpdate {

    @Override
    public void up() {
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        String[] formFieldsValues;
        String idForm ="lostOrStolenCreditCard";
        String version="1";
        
        
        insert("adm_ui_categories", new String[]{"id_category", "ordinal"}, new String[]{"creditCards", "800"});

        /**** Insert Form - Denuncia de robo o extravío de tarjeta ****/    
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[]{"id_form", "version","enabled", "category", "type", "id_jbpm_process", "admin_option","id_activity", "templates_enabled", "drafts_enabled", "schedulable"};
        String[] formsFieldsValues = new String[]{idForm, version, "1", "creditcards", "process", "demo-1", "creditCards", null, "1", "1", "0"};
        insert("forms", formsFields, formsFieldsValues);
       
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', 'Denuncia de robo o extravío de tarjeta', TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version,"es", "Denuncia de robo o extravío de tarjeta", date});
        }
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

        
        /**** Insert field product selector - Tarjeta ****/
        formFieldsValues = new String[]{"creditCardSelector", idForm, version, "productselector", "1", "TRUE", "TRUE", "creditCardSelector", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{"creditCardSelector", idForm, version, "field-normal"});
        insert("form_field_ps_product_types", new String[]{"id_field","id_form", "form_version", "id_product_type"}, new String[]{"creditCardSelector", idForm, version, "TC"});
        insert("form_field_ps_permissions", new String[]{"id_field","id_form", "form_version", "id_permission"}, new String[]{"creditCardSelector", idForm, version, "product.read"});  
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".creditCardSelector.label', 'es', 'creditCardSelector', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Tarjeta')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".creditCardSelector.requiredError', 'es', 'creditCardSelector', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Debe seleccionar una tarjeta')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".creditCardSelector.label", "es", "creditCardSelector", idForm, version, date, "Tarjeta"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".creditCardSelector.requiredError", "es", "creditCardSelector", idForm, version, date, "Debe seleccionar una tarjeta"});
        }  
        
        /**** Insert field text - Concepto ****/
        formFieldsValues = new String[]{"concept", idForm, version, "text", "2", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"}, new String[] {"concept", idForm, version, "1", "50", "field-normal"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".concept.label', 'es', 'concept', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Concepto')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".concept.requiredError', 'es', 'concept', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Debe ingresar un concepto')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".concept.label", "es", "concept", idForm, version, date, "Concepto"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".concept.requiredError", "es", "concept", idForm, version, date, "Debe ingresar un concepto"});    
        }  
        
        
    }

}
