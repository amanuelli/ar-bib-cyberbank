/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2492
 *
 * @author mricotta
 */
public class DB20170627_1746_2492 extends DBVSUpdate {

    @Override
    public void up() {
        /**** Insert category - "Comercio Exterior" ****/
        insertOrUpdateConfiguration("backoffice.forms.categories", "accounts|deposits|loans|creditcards|payments|transfers|comex|others", ConfigurationGroup.NEGOCIO, null, new String[]{});
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile", "read_only", "ticket_only" };
        String[] formFieldsMessages = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value" };
        String[] formFieldsValues;
        String idForm = "cancelCreditLetter";
        String version = "1";

        /**** Insert Form - Destinos frecuentes ****/
        String[] formsFields = new String[] { "id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable", "editable_in_mobile", "editable_in_narrow" };
        String[] formsFieldsValues = new String[] { idForm, version, "1", "comex", "process", "demo:1:3", "comex", null, "1", "1", "1", "0", "1" };
        insert("forms", formsFields, formsFieldsValues);

        String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value", "modification_date" };

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version,lang,value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "','" + version + "','es',  'Cancelación de carta de crédito',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'))");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version,lang,value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "','" + version + "','en',  'Credit letter cancellation',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'))");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version,lang,value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "','" + version + "','pt',  'Cancelamento do cartão de crédito',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "es", "Cancelación de carta de crédito", date });
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "en", "Credit letter cancellation", date });
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "pt", "Cancelamento do cartão de crédito", date });
        }
        insert("permissions", new String[] { "id_permission" }, new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[] { "id_permission", "id_credential_group" }, new String[] { "client.form." + idForm + ".send", "pin" });

        String idField;

        /**** Insert field text - creditLetter ****/
        idField = "creditLetter";
        formFieldsValues = new String[] { idField, idForm, version, "text", "1", "TRUE", "FALSE", "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation" }, new String[] { idField, idForm, version, "field-medium", "0", "30", null });
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Seleccione la cuenta de crédito que desea cancelar')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Carta de crédito')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Account Type')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Cartão de crédito')");
        }else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, "Seleccione la cuenta de crédito que desea cancelar" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Carta de crédito" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Credit letter" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Cartão de crédito" });
        }    
        /**** Insert field amount - cancellationAmount ****/
        idField = "cancellationAmount";
        formFieldsValues = new String[] { idField, idForm, version, "amount", "2", "TRUE", "FALSE", "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[] { "id_field", "id_form", "form_version", "display_type", "id_ps_currency_check", "control_limits", "control_limit_over_product", "use_for_total_amount" }, new String[] { idField, idForm, version, "field-normal", null, "0", null, "1" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Monto de la cancelación')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".invalidError','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'El importe no es válido')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Cancellation amount')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".invalidError','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'The amount is not valid')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Quantidade de cancelamente')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".invalidError','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'O valor é inválido')");
        }else{    
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Monto de la cancelación" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".invalidError", "es", idField, idForm, version, date, "El importe no es válido" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Cancellation amount" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".invalidError", "en", idField, idForm, version, date, "The amount is not valid" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Quantidade de cancelamente" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".invalidError", "pt", idField, idForm, version, date, "O valor é inválido" });
        }    
        /**** Insert field textarea - observation ****/
        idField = "observation";
        formFieldsValues = new String[] { idField, idForm, version, "textarea", "3", "TRUE", "FALSE", "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length" }, new String[] { idField, idForm, version, "field-big", "0", "500" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Observaciones')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Observations')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Observações')");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Observaciones" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Observations" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Observações" });
        }    
        /**** Insert field document - document ****/
        idField = "document";
        formFieldsValues = new String[] { idField, idForm, version, "document", "4", "TRUE", "FALSE", "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_document", new String[] { "id_field", "id_form", "form_version", "default_country", "default_document_type" }, new String[] { idField, idForm, version, "UY", "CI" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Documento')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Document')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Documento')");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Documento" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Document" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Documento" });
        }    
        /**** Insert field text - authorizedName ****/
        idField = "authorizedName";
        formFieldsValues = new String[] { idField, idForm, version, "text", "5", "TRUE", "FALSE", "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation" }, new String[] { idField, idForm, version, "field-big", "0", "140", null });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'No es necesario completar si la persona ya está autorizada frente al banco')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Nombre autorizado a retirar los documentos')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'It is not necessary to complete if the person is already authorized against the bank')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Authorized name to withdraw documents')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Não há necessidade de ser preenchido se a pessoa já autorizado na frente do banco')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Nome permitido para retirar documentos')");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, "No es necesario completar si la persona ya está autorizada frente al banco" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Nombre autorizado a retirar los documentos" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "en", idField, idForm, version, date, "It is not necessary to complete if the person is already authorized against the bank" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Authorized name to withdraw documents" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "pt", idField, idForm, version, date, "Não há necessidade de ser preenchido se a pessoa já autorizado na frente do banco" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Nome permitido para retirar documentos" });
        }    
        /**** Insert field termsandconditions - disclaimer ****/
        idField = "disclaimer";
        formFieldsValues = new String[] { idField, idForm, version, "termsandconditions", "6", "TRUE", "FALSE", "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label" }, new String[] { idField, idForm, version, "field-big", "1", "0" });
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".showAcceptOptionText','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Aceptp')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".termsAndConditions','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Las instrucciones enviadas al banco por usted luego de las 18:00 serán procesadas al siguiente día hábil.\nAutorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.')");
            
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".showAcceptOptionText','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'I agreee')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".termsAndConditions','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'I authorize to charge from my account the commissions that this instruction can generate.')");
            
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".showAcceptOptionText','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Eu aceito')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".termsAndConditions','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Autorizo a debitar os meus taxas de conta que esta instrução pode gerar.')"); 
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".showAcceptOptionText", "es", idField, idForm, version, date, "Acepto" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".termsAndConditions", "es", idField, idForm, version, date,
                "Las instrucciones enviadas al banco por usted luego de las 18:00 serán procesadas al siguiente día hábil.\nAutorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar." });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".showAcceptOptionText", "en", idField, idForm, version, date, "I agree" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".termsAndConditions", "en", idField, idForm, version, date, "I authorize to charge from my account the commissions that this instruction can generate." });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".showAcceptOptionText", "pt", idField, idForm, version, date, "Eu aceito" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".termsAndConditions", "pt", idField, idForm, version, date, "Autorizo a debitar os meus taxas de conta que esta instrução pode gerar." });
        }   
        /**** Insert field horizontalrule ****/
        idField = "horizontalrule";
        formFieldsValues = new String[] { idField, idForm, version, "horizontalrule", "7", "TRUE", "FALSE", "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'')");
        }else{   
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "" });
        }    
        /**** Insert field emaillist - notificationEmails ****/
        idField = "notificationEmails";
        formFieldsValues = new String[] { idField, idForm, version, "emaillist", "8", "TRUE", "FALSE", "default", "1", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_emaillist", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { idField, idForm, version, "field-big" });
       
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Emails de notificación')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Notification emails')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'E-mail a notificar o transporte de transferência. Recuerde ingresarlos separados por coma.')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Emails de notificação')");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma." });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Emails de notificación" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Notification emails" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "pt", idField, idForm, version, date, "E-mail a notificar o transporte de transferência. Recuerde ingresarlos separados por coma." });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Emails de notificação" });
        }    
        /**** Insert field textarea - notificationBody ****/
        idField = "notificationBody";
        formFieldsValues = new String[] { idField, idForm, version, "textarea", "9", "TRUE", "FALSE", "default", "1", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length" }, new String[] { idField, idForm, version, "field-big", "0", "500" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Cuerpo de notificación')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Notification body')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Notificação corpo')");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Cuerpo de notificación" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "en", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Notification body" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "pt", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Notificação corpo" });
        }    
    }
}
