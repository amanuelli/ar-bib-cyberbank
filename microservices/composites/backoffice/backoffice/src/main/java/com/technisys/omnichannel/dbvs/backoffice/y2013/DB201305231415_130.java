/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201305231415_130 extends DBVSUpdate {

    private static final String[] CONFIGURATION_FIELDS = new String[]{"id_field", "value", "possible_values", "id_group", "encrypted", "must_be_encrypted"};

    @Override
    public void up() {
        delete("configuration", "id_field='backoffice.invitation.new.acceptUrl'");
        delete("configuration", "id_field='client.baseURL'");

        insert("configuration", CONFIGURATION_FIELDS, new String[]{"backoffice.invitation.new.acceptUrl", "#{BASE_URL}/enrollment/enterCode?data=#{CODE}", "", "Client", "0", "0"});
        insert("configuration", CONFIGURATION_FIELDS, new String[]{"client.baseURL", "http://localhost:3000", "", "Client", "0", "0"});
    }
}