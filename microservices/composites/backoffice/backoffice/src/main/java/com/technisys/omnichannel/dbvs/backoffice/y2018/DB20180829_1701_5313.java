/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5313
 *
 * @author marcelobruno
 */
public class DB20180829_1701_5313 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_selector_options", new String[]{"value"}, new String[]{"selected"}, "id_form = 'transferForeign' and id_field = 'shouldDebitFromOriginAccount'");
        update("form_field_selector_options", new String[]{"value"}, new String[]{"selected"}, "id_form = 'transferForeign' and id_field = 'isUsingIntermediaryBank'");
        
        update("form_fields", new String[]{"visible"}, new String[]{"value(shouldDebitFromOriginAccount) == 'selected'"}, "id_form = 'transferForeign' and id_field = 'debitCommissionsFromOptions'");
        update("form_fields", new String[]{"required"}, new String[]{"value(shouldDebitFromOriginAccount) == 'selected'"}, "id_form = 'transferForeign' and id_field = 'debitCommissionsFromOptions'");
        
        update("form_fields", new String[]{"visible"}, new String[]{"value(isUsingIntermediaryBank) == 'selected'"}, "id_form = 'transferForeign' and id_field = 'intermediaryAccountNumber'");
        update("form_fields", new String[]{"required"}, new String[]{"value(isUsingIntermediaryBank) == 'selected'"}, "id_form = 'transferForeign' and id_field = 'intermediaryAccountNumber'");
        
        update("form_fields", new String[]{"visible"}, new String[]{"value(isUsingIntermediaryBank) == 'selected'"}, "id_form = 'transferForeign' and id_field = 'intermediaryBank'");
        update("form_fields", new String[]{"required"}, new String[]{"value(isUsingIntermediaryBank) == 'selected'"}, "id_form = 'transferForeign' and id_field = 'intermediaryBank'");
        
        update("form_field_messages", new String[]{"id_message"}, new String[]{"fields.transferForeign.isUsingIntermediaryBank.option.selected"}, "id_form = 'transferForeign' and id_message = 'fields.transferForeign.isUsingIntermediaryBank.option.true'");
        update("form_field_messages", new String[]{"id_message"}, new String[]{"fields.transferForeign.shouldDebitFromOriginAccount.option.selected"}, "id_form = 'transferForeign' and id_message = 'fields.transferForeign.shouldDebitFromOriginAccount.option.true'");
    }

}