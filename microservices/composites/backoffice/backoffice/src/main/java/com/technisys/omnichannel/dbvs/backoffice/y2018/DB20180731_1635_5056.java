/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5056
 *
 * @author dimoda
 */
public class DB20180731_1635_5056 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"id_sub_group"}, new String[]{"others"}, "id_field = 'forms.bankselector.maxRecords'");
    }

}