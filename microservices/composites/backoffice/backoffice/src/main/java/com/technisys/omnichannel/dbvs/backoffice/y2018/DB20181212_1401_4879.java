/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-4879
 *
 * @author emordezki
 */
public class DB20181212_1401_4879 extends DBVSUpdate {

    @Override
    public void up() {
        delete("form_field_messages", "id_message='fields.payLoan.loanPayment.label'");
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        Map<String, String> formFieldMessages = new HashMap<>();
        formFieldMessages.put("es", "Listado de préstamos");
        formFieldMessages.put("en", "List of loans");
        formFieldMessages.put("pt", "Lista de prestações");
        
        String[] formFieldMessagesColumns = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};

        for (String key : formFieldMessages.keySet()) {
            String[] formFieldsValues = new String[]{"fields.payLoan.loanPayment.label", key, "loanPayment", "payLoan", "1", date, formFieldMessages.get(key)};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', " + formFieldsValues[4] + ", TO_DATE('"+date+"', 'YYYY-MM-DD HH24:MI:SS'), '" + formFieldsValues[6] + "')");
            } else {
                insert("form_field_messages", formFieldMessagesColumns, formFieldsValues);
            }
        }
    }
}