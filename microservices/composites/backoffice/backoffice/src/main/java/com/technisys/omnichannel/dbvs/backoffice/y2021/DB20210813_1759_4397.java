/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2021;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;

/**
 * Related issue: TECCDPBO-4397
 *
 * @author azeballos
 */
public class DB20210813_1759_4397 extends DBVSUpdate {

    @Override
    public void up() {
        delete("backoffice_activities", "id_activity = 'cybo.invitationcodes.read'");
        insertBackofficeActivity("cybo.invitationcodes.read",
                "com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities.ReadActivity",
                "backoffice.invitationCodes.list", null, "backoffice.invitationcodes",
                ActivityDescriptor.AuditLevel.Header);
    }
}
