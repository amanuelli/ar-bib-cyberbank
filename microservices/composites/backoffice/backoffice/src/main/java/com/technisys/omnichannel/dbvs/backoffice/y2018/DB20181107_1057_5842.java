/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;


import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5842
 *
 * @author Marcelo Bruno
 */
public class DB20181107_1057_5842 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("administration.medium.modify.channels");
        insertActivity("administration.medium.modify.channels.send", "com.technisys.omnichannel.client.activities.administration.medium.ModifyChannelsActivity", "administration", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");
        
        deleteActivity("administration.simple.modify.permissions");
        insertActivity("administration.simple.modify.permissions.send", "com.technisys.omnichannel.client.activities.administration.simple.ModifyPermissionsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Admin, "administration.manage");
        
        deleteActivity("administration.medium.modify.permissions");
        insertActivity("administration.medium.modify.permissions.send", "com.technisys.omnichannel.client.activities.administration.medium.ModifyPermissionsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Admin, "administration.manage");
        
        deleteActivity("administration.medium.modify.signature");
        insertActivity("administration.medium.modify.signature.send", "com.technisys.omnichannel.client.activities.administration.medium.ModifySignatureActivity", "administration", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");
        
    }

}