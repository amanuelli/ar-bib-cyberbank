/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2021;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-14765
 *
 * @author sbarbosa
 */
public class DB20210616_1030_14765 extends DBVSUpdate {

    @Override
    public void up() {
        delete("QRTZ_SIMPLE_TRIGGERS", "TRIGGER_NAME = 'auditHist'");
        delete("QRTZ_TRIGGERS", "TRIGGER_NAME = 'auditHist'");
        delete("QRTZ_JOB_DETAILS", "JOB_NAME = 'auditHist'");
    }
}
