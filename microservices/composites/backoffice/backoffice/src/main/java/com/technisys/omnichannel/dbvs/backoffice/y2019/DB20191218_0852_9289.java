/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Jhossept
 */

public class DB20191218_0852_9289 extends DBVSUpdate {

    @Override
    public void up() {
        delete("group_permissions", "id_permission IN ('frequentDestinations.manage','position')");
        delete("permissions", "id_permission IN ('frequentDestinations.manage','position')");
    }

}