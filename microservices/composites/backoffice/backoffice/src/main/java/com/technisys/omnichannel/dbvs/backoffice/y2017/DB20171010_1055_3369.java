/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-3369
 *
 * @author dimoda
 */
public class DB20171010_1055_3369 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("communications.pushnotifications.listUserDevices", "com.technisys.omnichannel.client.activities.communications.pushnotifications.ListUserDevicesActivity", "communications", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("communications.pushnotifications.deleteUserDevice", "com.technisys.omnichannel.client.activities.communications.pushnotifications.DeleteUserDeviceActivity", "communications", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}