/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.responses;

import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Sebastian Barbosa
 */
public class ReadUserResponse extends IBResponse {

    private User user;
    private ClientUser clientUser;
    private List<String> accountList;

    private Environment selectedEnvironment;
    private ClientEnvironment selectedClientEnvironment;

    public ReadUserResponse(IBRequest request) {
        super(request);

        accountList = new ArrayList<>();
    }

    public ClientUser getClientUser() {
        return clientUser;
    }

    public void setClientUser(ClientUser clientUser) {
        this.clientUser = clientUser;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<String> getAccountList() {
        return accountList;
    }

    public void setAccountList(List<String> accountList) {
        this.accountList = accountList;
    }

    public void addAccount(String account) {
        this.accountList.add(account);
    }

    public Environment getSelectedEnvironment() {
        return selectedEnvironment;
    }

    public void setSelectedEnvironment(Environment selectedEnvironment) {
        this.selectedEnvironment = selectedEnvironment;
    }

    public ClientEnvironment getSelectedClientEnvironment() {
        return selectedClientEnvironment;
    }

    public void setSelectedClientEnvironment(ClientEnvironment selectedClientEnvironment) {
        this.selectedClientEnvironment = selectedClientEnvironment;
    }

    @Override
    public String toString() {
        return "ReadUserResponse{" + "user=" + user + ", clientUser=" + clientUser + ", accountList=" + accountList + ", selectedEnvironment=" + selectedEnvironment + ", selectedClientEnvironment=" + selectedClientEnvironment + '}';
    }

}
