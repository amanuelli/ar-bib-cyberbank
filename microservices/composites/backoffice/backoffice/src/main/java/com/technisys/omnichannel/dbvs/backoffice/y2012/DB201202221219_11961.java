/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201202221219_11961 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};
        String[] fieldValues = new String[]{"rub.desktop.loadPymesWidget", "1", "1", "com.technisys.rubicon.business.desktop.activities.LoadPymesWidgetActivity"};
        insert("activities", fieldNames, fieldValues);

        update("configuration", new String[]{"value"}, new String[]{"Visa"}, "id_field = 'rubicon.productTypes.VISA'");
        update("configuration", new String[]{"value"}, new String[]{"MasterCard"}, "id_field = 'rubicon.productTypes.MASTER'");
        update("configuration", new String[]{"value"}, new String[]{"AmEx"}, "id_field = 'rubicon.productTypes.AMEX'");

        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_group"};
        fieldValues = new String[]{"rub.check.list", "1", "1", "com.technisys.rubicon.business.checks.activities.ListChecksActivity", "rub.checks"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.check.exportList", "1", "1", "com.technisys.rubicon.business.checks.activities.ExportListActivity", "rub.checks"};
        insert("activities", fieldNames, fieldValues);

    }
}