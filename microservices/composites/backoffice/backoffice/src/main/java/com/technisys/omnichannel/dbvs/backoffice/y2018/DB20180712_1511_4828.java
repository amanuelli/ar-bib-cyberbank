package com.technisys.omnichannel.dbvs.backoffice.y2018;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4828
 *
 * @author arameau
 */
public class DB20180712_1511_4828 extends DBVSUpdate {

    @Override
    public void up() {
    	String [] fieldNames = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value" };
    	ArrayList<String[]> fieldValuesList = new ArrayList<>();
    	
    	//		REQUEST LOAN
    	//Placeholder for loanType selector
    	fieldValuesList.add(new String[] {"fields.requestLoan.loanType.placeholder", "es", "loanType", "requestLoan", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de préstamo" });
    	fieldValuesList.add(new String[] {"fields.requestLoan.loanType.placeholder", "en", "loanType", "requestLoan", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de préstamo" });
    	fieldValuesList.add(new String[] {"fields.requestLoan.loanType.placeholder", "pt", "loanType", "requestLoan", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de préstamo" });
    	//Placeholder for amountOfFees selector
    	fieldValuesList.add(new String[] {"fields.requestLoan.amountOfFees.placeholder", "es", "amountOfFees", "requestLoan", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la cantidad de cuotas" });
    	fieldValuesList.add(new String[] {"fields.requestLoan.amountOfFees.placeholder", "en", "amountOfFees", "requestLoan", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la cantidad de cuotas" });
    	fieldValuesList.add(new String[] {"fields.requestLoan.amountOfFees.placeholder", "pt", "amountOfFees", "requestLoan", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la cantidad de cuotas" });
    	//Placeholder for fundsDestination selector
    	fieldValuesList.add(new String[] {"fields.requestLoan.fundsDestination.placeholder", "es", "fundsDestination", "requestLoan", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el destino" });
    	fieldValuesList.add(new String[] {"fields.requestLoan.fundsDestination.placeholder", "en", "fundsDestination", "requestLoan", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el destino" });
    	fieldValuesList.add(new String[] {"fields.requestLoan.fundsDestination.placeholder", "pt", "fundsDestination", "requestLoan", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el destino" });
    	
    	//		ACCOUNT OPENING 
    	//Placeholder for accountType selector
    	fieldValuesList.add(new String[] {"fields.accountOpening.accountType.placeholder", "es", "accountType", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de cuenta" });
    	fieldValuesList.add(new String[] {"fields.accountOpening.accountType.placeholder", "en", "accountType", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de cuenta" });
    	fieldValuesList.add(new String[] {"fields.accountOpening.accountType.placeholder", "pt", "accountType", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de cuenta" });
    	//Placeholder for currencies selector
    	fieldValuesList.add(new String[] {"fields.accountOpening.currencies.placeholder", "es", "currencies", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la moneda" });
    	fieldValuesList.add(new String[] {"fields.accountOpening.currencies.placeholder", "en", "currencies", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la moneda" });
    	fieldValuesList.add(new String[] {"fields.accountOpening.currencies.placeholder", "pt", "currencies", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la moneda" });
    	//Placeholder for accountBranch selector
    	fieldValuesList.add(new String[] {"fields.accountOpening.accountBranch.placeholder", "es", "accountBranch", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la sucursal" });
    	fieldValuesList.add(new String[] {"fields.accountOpening.accountBranch.placeholder", "en", "accountBranch", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la sucursal" });
    	fieldValuesList.add(new String[] {"fields.accountOpening.accountBranch.placeholder", "pt", "accountBranch", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la sucursal" });
    	//Placeholder for placeOfRetreat selector
    	fieldValuesList.add(new String[] {"fields.accountOpening.placeOfRetreat.placeholder", "es", "placeOfRetreat", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el lugar de retiro" });
    	fieldValuesList.add(new String[] {"fields.accountOpening.placeOfRetreat.placeholder", "en", "placeOfRetreat", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el lugar de retiro" });
    	fieldValuesList.add(new String[] {"fields.accountOpening.placeOfRetreat.placeholder", "pt", "placeOfRetreat", "accountOpening", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el lugar de retiro" });
    	
    	//		REQUEST MANAGEMENT CHECK
    	//Placeholder for type selector
    	fieldValuesList.add(new String[] {"fields.requestOfManagementCheck.type.placeholder", "es", "type", "requestOfManagementCheck", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo" });
    	fieldValuesList.add(new String[] {"fields.requestOfManagementCheck.type.placeholder", "en", "type", "requestOfManagementCheck", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo" });
    	fieldValuesList.add(new String[] {"fields.requestOfManagementCheck.type.placeholder", "pt", "type", "requestOfManagementCheck", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo" });
    	//Placeholder for placeOfRetreat selector
    	fieldValuesList.add(new String[] {"fields.requestOfManagementCheck.placeOfRetreat.placeholder", "es", "placeOfRetreat", "requestOfManagementCheck", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el lugar de retiro" });
    	fieldValuesList.add(new String[] {"fields.requestOfManagementCheck.placeOfRetreat.placeholder", "en", "placeOfRetreat", "requestOfManagementCheck", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el lugar de retiro" });
    	fieldValuesList.add(new String[] {"fields.requestOfManagementCheck.placeOfRetreat.placeholder", "pt", "placeOfRetreat", "requestOfManagementCheck", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el lugar de retiro" });
    	
    	//		AUTHORIZE DISCREPANCY DOCUMENTS
    	//Placeholder for operationType selector
    	fieldValuesList.add(new String[] {"fields.authorizeDiscrepancyDocuments.operationType.placeholder", "es", "operationType", "authorizeDiscrepancyDocuments", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de operación" });
    	fieldValuesList.add(new String[] {"fields.authorizeDiscrepancyDocuments.operationType.placeholder", "en", "operationType", "authorizeDiscrepancyDocuments", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de operación" });
    	fieldValuesList.add(new String[] {"fields.authorizeDiscrepancyDocuments.operationType.placeholder", "pt", "operationType", "authorizeDiscrepancyDocuments", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de operación" });
    	
    	//		CASH ADVANCE
    	//Placeholder for installments selector
    	fieldValuesList.add(new String[] {"fields.cashAdvance.installments.placeholder", "es", "installments", "cashAdvance", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la cantidad de cuotas" });
    	fieldValuesList.add(new String[] {"fields.cashAdvance.installments.placeholder", "en", "installments", "cashAdvance", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la cantidad de cuotas" });
    	fieldValuesList.add(new String[] {"fields.cashAdvance.installments.placeholder", "pt", "installments", "cashAdvance", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione la cantidad de cuotas" });
    	
    	//		CREDIT CARD CHANGE CONDITION				
    	//Placeholder for installments selector	
    	fieldValuesList.add(new String[] {"fields.creditCardChangeCondition.modifySelector.placeholder", "es", "modifySelector", "creditCardChangeCondition", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione una opción" });
    	fieldValuesList.add(new String[] {"fields.creditCardChangeCondition.modifySelector.placeholder", "en", "modifySelector", "creditCardChangeCondition", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione una opción" });
    	fieldValuesList.add(new String[] {"fields.creditCardChangeCondition.modifySelector.placeholder", "pt", "modifySelector", "creditCardChangeCondition", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione una opción" });
    	//Placeholder for americanExpressAwardsProgram selector	
    	fieldValuesList.add(new String[] {"fields.creditCardChangeCondition.americanExpressAwardsProgram.placeholder", "es", "americanExpressAwardsProgram", "creditCardChangeCondition", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el programa de premios" });
    	fieldValuesList.add(new String[] {"fields.creditCardChangeCondition.americanExpressAwardsProgram.placeholder", "en", "americanExpressAwardsProgram", "creditCardChangeCondition", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el programa de premios" });
    	fieldValuesList.add(new String[] {"fields.creditCardChangeCondition.americanExpressAwardsProgram.placeholder", "pt", "americanExpressAwardsProgram", "creditCardChangeCondition", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el programa de premios" });
    	//Placeholder for americanExpressTypeCard selector	
    	fieldValuesList.add(new String[] {"fields.creditCardChangeCondition.americanExpressTypeCard.placeholder", "es", "americanExpressTypeCard", "creditCardChangeCondition", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de tarjeta" });
    	fieldValuesList.add(new String[] {"fields.creditCardChangeCondition.americanExpressTypeCard.placeholder", "en", "americanExpressTypeCard", "creditCardChangeCondition", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de tarjeta" });
    	fieldValuesList.add(new String[] {"fields.creditCardChangeCondition.americanExpressTypeCard.placeholder", "pt", "americanExpressTypeCard", "creditCardChangeCondition", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione el tipo de tarjeta" });
    	
    	//		TRANSFER LOCAL
    	//Placeholder for bankSelector selector	
    	fieldValuesList.add(new String[] {"fields.transferLocal.bankSelector.placeholder", "es", "bankSelector", "transferLocal", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un banco" });
    	fieldValuesList.add(new String[] {"fields.transferLocal.bankSelector.placeholder", "en", "bankSelector", "transferLocal", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un banco" });
    	fieldValuesList.add(new String[] {"fields.transferLocal.bankSelector.placeholder", "pt", "bankSelector", "transferLocal", "1", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()), "Seleccione un banco" });
    	
    	
    	String [] fieldValues;
        for(int i = 0; i < fieldValuesList.size(); i++){
            fieldValues = fieldValuesList.get(i);
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                String insertSentence = String.format("INSERT INTO form_field_messages (%1$s, %2$s, %3$s, %4$s, %5$s, %6$s, %7$s) "
                                                    + "VALUES ('%8$s', '%9$s', '%10$s', '%11$s', '%12$s', TO_DATE('%13$s', 'YYYY-MM-DD HH24:MI:SS'), '%14$s')",
                                                    fieldNames[0], fieldNames[1], fieldNames[2], fieldNames[3], fieldNames[4], fieldNames[5], fieldNames[6],
                                                    fieldValues[0], fieldValues[1], fieldValues[2], fieldValues[3], fieldValues[4], fieldValues[5], fieldValues[6]);

                customSentence(DBVS.DIALECT_ORACLE, insertSentence);
            } else {
                insert("form_field_messages", fieldNames, fieldValues);
            }
        } 
    }

}