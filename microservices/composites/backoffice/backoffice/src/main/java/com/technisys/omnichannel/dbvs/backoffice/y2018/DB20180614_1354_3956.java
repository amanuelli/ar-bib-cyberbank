package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20180614_1354_3956 extends DBVSUpdate {

    @Override
    public void up() {
        //Borrar titulos de sección, mail de notificación, etc.
        delete("form_fields", "id_form='payLoan' AND id_field in ('reference', 'otherLoanAmount', 'notificationEmails', 'notificationBody', 'notificationMails', 'otherCreditAccount', 'horizontalrule')");
        
        //Reordenar el campo monto y cuenta débito
        update("form_fields", new String[]{"ordinal", "read_only"}, new String[]{"1", "1"}, "id_form='payLoan' AND id_field = 'loan'");
        update("form_fields", new String[]{"ordinal", "visible", "required"}, new String[]{"2", "TRUE", "TRUE"}, "id_form='payLoan' AND id_field = 'loanPayment'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"3"}, "id_form='payLoan' AND id_field = 'debitAccount'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"4"}, "id_form='payLoan' AND id_field = 'debitAmount'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"5"}, "id_form='payLoan' AND id_field = 'rate'");
        update("form_fields", new String[]{"ordinal"}, new String[]{"6"}, "id_form='payLoan' AND id_field = 'commision'");

        //Ajusto labels
        update("form_field_messages", new String[]{"value"}, new String[]{"Cuenta de origen"}, "id_message='fields.payLoan.debitAccount.label' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Importe total"}, "id_message='fields.payLoan.loanPayment.labelTotalPay' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Importe total"}, "id_message='fields.payLoan.loanPayment.label' AND lang='es'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Selecciona las cuotas que quieras pagar usando el casillero de la derecha"}, "id_message='fields.payLoan.loanPayment.labelIntro' AND lang='es'");
        
        delete("form_field_messages", "id_form='payLoan' AND id_field='loanPayment' AND id_message='fields.payLoan.loanPayment.labelPartialPay'");
        delete("form_field_messages", "id_form='payLoan' AND id_field='loanPayment' AND id_message='fields.payLoan.loanPayment.partialPay'");
        delete("form_field_messages", "id_form='payLoan' AND id_field='loanPayment' AND id_message='fields.payLoan.loanPayment.totalPay'");
                
        delete("activity_caps", "id_activity='pay.loan.send' AND id_field_amount='otherLoanAmount'");
    }
}
