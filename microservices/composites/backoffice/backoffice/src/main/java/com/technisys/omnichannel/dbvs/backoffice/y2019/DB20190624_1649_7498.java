package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-7498
 *
 * @author Marcelo Bruno
 */

public class DB20190624_1649_7498 extends DBVSUpdate {

    @Override
    public void up() {

        customSentence(
                DBVS.DIALECT_MSSQL,
                "UPDATE configuration SET value = CASE " +
                        "WHEN LEN(value) = 0 " +
                        "THEN 'image/png|image/jpeg|text/plain|application/pdf|application/msword|application/zip|application/x-tika-ooxml|application/x-tika-msoffice' " +
                        "ELSE CONCAT(value, '|application/x-tika-ooxml|application/x-tika-msoffice') " +
                        "END " +
                        "WHERE id_field = 'form.inputFile.acceptedFileTypes'"
        );
        customSentence(
                new String[] { DBVS.DIALECT_MYSQL, DBVS.DIALECT_HSQLDB, DBVS.DIALECT_ORACLE },
                "UPDATE configuration SET value = CASE " +
                        "WHEN LENGTH(value) = 0 " +
                        "THEN 'image/png|image/jpeg|text/plain|application/pdf|application/msword|application/zip|application/x-tika-ooxml|application/x-tika-msoffice' " +
                        "ELSE CONCAT(value, '|application/x-tika-ooxml|application/x-tika-msoffice') " +
                        "END " +
                        "WHERE id_field = 'form.inputFile.acceptedFileTypes'"
        );

    }

}
