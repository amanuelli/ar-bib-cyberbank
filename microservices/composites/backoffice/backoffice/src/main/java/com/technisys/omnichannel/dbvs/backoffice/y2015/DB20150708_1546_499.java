/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author aelters
 */
public class DB20150708_1546_499 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("preferences.socialnetworks.configuration.modify.pre", "com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration.ModifySocialNetworksConfigurationPreActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.socialnetworks.configuration.modify.preview", "com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration.ModifySocialNetworksConfigurationPreviewActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.socialnetworks.configuration.modify", "com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration.ModifySocialNetworksConfigurationActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "user.preferences");
        insertActivity("preferences.socialnetworks.twitter.firstStep", "com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration.TwitterLoginFirstStepActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.socialnetworks.twitter.secondStep", "com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration.TwitterLoginSecondStepActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertOrUpdateConfiguration("socialnetworks.facebook.appId", "1089018964447799", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.facebook.appSecret", "460c7b8468300b102a205232ce445f9a", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.twitter.apiKey", "hgRF0uZBxFGSFl1w3WBUpHDPk", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.twitter.apiSecret", "3vmjPVMJcV9r0pQOor5ii83pCeerZazKSxNjUvfldOikwrZyKI", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.twitter.accessToken", "3364350520-auV2f9DmDuE8DcISZc79OwYxNz6vKIykzB1H9El", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("socialnetworks.twitter.accessTokenSecret", "Bb8rkQ86iP1KAaji5umit6ETS2XaUn99kBfXE3GQzntVC", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"});
    }
}