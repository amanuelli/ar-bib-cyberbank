/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150708_1444_415 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.groups.list", "com.technisys.omnichannel.client.activities.administration.groups.ListGroupsActivity", "administration.groups", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.groups.export", "com.technisys.omnichannel.client.activities.administration.groups.ExportGroupsActivity", "administration.groups", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");

        insertActivity("administration.groups.blockunblock.preview", "com.technisys.omnichannel.client.activities.administration.groups.BlockUnblockGroupsPreviewActivity", "administration.groups", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.groups.blockunblock.send", "com.technisys.omnichannel.client.activities.administration.groups.BlockUnblockGroupsActivity", "administration.groups", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        insertActivity("administration.groups.delete.preview", "com.technisys.omnichannel.client.activities.administration.groups.DeleteGroupsPreviewActivity", "administration.groups", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.groups.delete.send", "com.technisys.omnichannel.client.activities.administration.groups.DeleteGroupsActivity", "administration.groups", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

    }
}
