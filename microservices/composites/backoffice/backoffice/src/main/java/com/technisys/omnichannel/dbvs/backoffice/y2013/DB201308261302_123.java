/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201308261302_123 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("rubicon.userconfiguration.password.pattern", "[A-Za-z0-9ñÑ_!@#$%&\\*\\(\\)]*");

    }
}