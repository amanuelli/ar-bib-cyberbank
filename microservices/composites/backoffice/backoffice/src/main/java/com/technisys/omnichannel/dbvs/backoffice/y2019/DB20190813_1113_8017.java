/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author pbanales
 */

public class DB20190813_1113_8017 extends DBVSUpdate {

    protected static final String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
            "required", "visible_in_mobile", "read_only", "sub_type", "ticket_only", "note"};

    @Override
    public void up() {
        Map<String, String> messagesEN = new HashMap<>();
        Map<String, String> messagesES = new HashMap<>();
        Map<String, String> messagesPT = new HashMap<>();

        String formId = "transferInternal";
        String formVersion = "1";

        String[] formFieldsValues = new String[]{"tax", formId, formVersion, "amount", "3", "TRUE", "FALSE", "1", "1", "default", "1", null};
        insert("form_fields", formFields, formFieldsValues);

        insert("form_field_amount",
                new String[]{"id_field", "id_form", "form_version", "display_type", "id_ps_currency_check",
                        "control_limits", "control_limit_over_product", "use_for_total_amount"},
                new String[]{"tax", "transferInternal", "1", "field-normal", null, "0", null, "0"});

        messagesEN = new HashMap<>();
        messagesES = new HashMap<>();
        messagesPT = new HashMap<>();

        messagesEN.put("help", "");
        messagesES.put("help", "");
        messagesPT.put("help", "");

        messagesEN.put("hint", "");
        messagesES.put("hint", "");
        messagesPT.put("hint", "");

        messagesEN.put("label", "Tax/commission");
        messagesES.put("label", "Comisión");
        messagesPT.put("label", "Comissão");

        messagesEN.put("requiredError", "");
        messagesES.put("requiredError", "");
        messagesPT.put("requiredError", "");

        insertFormFieldMessages(formId, "tax", formVersion, messagesEN, messagesES, messagesPT);

    }

}
