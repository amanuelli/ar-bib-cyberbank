/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2201
 *
 * @author dimoda
 */
public class DB20170330_1512_2201 extends DBVSUpdate {

    @Override
    public void up() {
            
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        String[] formFieldsValues;
        String idForm ="mobilePrepaiment";
        String version ="1";
       
        /**** Insert Form - Recarga de celular ****/    
        delete("forms", "id_form = '" + idForm + "'");
        
        String[] formsFields = new String[]{"id_form", "version", "enabled", "category", "type", "id_jbpm_process", "admin_option","id_activity", "templates_enabled", "drafts_enabled", "schedulable"};
        String[] formsFieldsValues = new String[]{idForm, version,"1", "payments", "process", "demo-1", "payments", null, "1", "1", "0"};
        insert("forms", formsFields, formsFieldsValues);
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', 'Recarga de celular', TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Recarga de celular", date});
        }
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

        
        /**** Insert field product selector - Cuenta débito ****/
        formFieldsValues = new String[]{"debitAccount", idForm, version, "productselector", "1", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{"debitAccount", idForm, version, "field-normal"});

        insert("form_field_ps_product_types", new String[]{"id_field","id_form", "form_version", "id_product_type"}, new String[]{"debitAccount", idForm, version, "CA"});
        insert("form_field_ps_product_types", new String[]{"id_field","id_form", "form_version", "id_product_type"}, new String[]{"debitAccount", idForm, version, "CC"});
        insert("form_field_ps_permissions", new String[]{"id_field","id_form", "form_version", "id_permission"}, new String[]{"debitAccount", idForm, version, "product.read"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".debitAccount.label', 'es', 'debitAccount', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Cuenta débito')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".debitAccount.label", "es", "debitAccount", idForm, version, date, "Cuenta débito"});  
        } 
        
        /**** Insert field text - Teléfono celular ****/
        formFieldsValues = new String[]{"phone", idForm, version, "text", "2", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type","id_validation"}, new String[] {"phone", idForm, version, "9", "9", "field-normal","onlyNumbers"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".phone.label', 'es', 'phone', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Teléfono celular')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".phone.requiredError', 'es', 'phone', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Debe ingresar un número de celular')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".phone.label", "es", "phone", idForm, version, date, "Teléfono celular"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".phone.requiredError", "es", "phone", idForm, version, date, "Debe ingresar un número de celular"});   
        }
        
        
        /**** Insert field amount - Monto ****/
        formFieldsValues = new String[]{"amount", idForm, version, "amount", "3", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount"}, new String[]{"amount", idForm, version, "field-normal", "1", "1"});
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".amount.label', 'es', 'amount', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Monto')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                    + " VALUES ('fields." + idForm + ".amount.requiredError', 'es', 'amount', '" + idForm + "'," + version  + ", TO_DATE('2017-03-30 00:00:01', 'YYYY-MM-DD HH24:MI:SS'), 'Debe ingresar un importe mayor a cero')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".amount.label", "es", "amount", idForm, version, date, "Monto"});
            insert("form_field_messages", formFieldsMessages, new String[]{"fields." + idForm + ".amount.requiredError", "es", "amount", idForm, version, date, "Debe ingresar un importe mayor a cero"});
        }
        
    }

}
