/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.productrequest;

import com.opensymphony.xwork2.ActionSupport;
import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.PaginatedListResponse;
import com.technisys.omnichannel.backoffice.business.productrequest.requests.ListRequest;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.HTMLException;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.technisys.omnichannel.core.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 * @author Marcelo Bruno
 */
@Action(results = {
        @Result(name = "success", type = "stream",
                params = {
                        "contentDisposition", "attachment;filename=\"${filename}\"", "contentType", "${contentType}", "inputName", "inputStream", "bufferSize", "1024"
                })
})
public class Export extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ListRequest request = new ListRequest();

        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.productrequest.list");

        request.setDocumentNumber(documentNumber);
        request.setEmail(email);
        request.setCreationDateFrom(creationDateFrom);
        request.setCreationDateTo(creationDateTo);
        request.setStatus(status);
        request.setType(type);

        request.setPageNumber(-1);
        request.setOrderBy(orderBy);
        request.setExport(true);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        if (response.getReturnCode().equals(ReturnCodes.OK)) {
            PaginatedListResponse<Onboarding> listResponse = (PaginatedListResponse<Onboarding>) response;
            String lang = request.getLang();
            String baseColumnKey = "backoffice.productRequest.list.column.";
            String[] columns = {
                    I18nFactory.getHandler().getMessage(baseColumnKey + "documentNumber", lang),
                    I18nFactory.getHandler().getMessage(baseColumnKey + "email", lang),
                    I18nFactory.getHandler().getMessage(baseColumnKey + "creationDate", lang),
                    I18nFactory.getHandler().getMessage(baseColumnKey + "status", lang),
                    I18nFactory.getHandler().getMessage(baseColumnKey + "type", lang),
            };

            String[] attributes = {"documentNumber", "email", "creationDate", "status", "type"};

            byte[] file = UIUtils.toExcelFormat(lang, listResponse.getItemList(), columns, attributes, I18nFactory.getHandler().getMessage("backoffice.export.xls.productrequest.title", lang));
            inputStream = new ByteArrayInputStream(file);
            contentType = "application/xls";

            return SUCCESS;
        } else {
            throw new HTMLException(response);
        }

    }
    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private String filename;
    private String orderBy = null;

    private String documentNumber;
    private String email;
    private String status;
    private Date creationDateFrom;
    private Date creationDateTo;
    private String type;

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    transient InputStream inputStream;
    private String contentType;

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getFilename() {
        return filename;
    }

    public String getContentType() {
        return contentType;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setCreationDateFrom(String creationDateFrom) {
        try {
            if (StringUtils.isNotEmpty(creationDateFrom)) {
                this.creationDateFrom = DateUtils.parseShortDate(creationDateFrom);
            }
        } catch (ParseException e) {
            this.creationDateFrom = null;
        }
    }

    public void setCreationDateTo(String creationDateTo) {
        try {
            if (StringUtils.isNotEmpty(creationDateTo)) {
                this.creationDateTo = DateUtils.parseShortDate(creationDateTo);
            }
        } catch (ParseException e) {
            this.creationDateTo = null;
        }
    }

    public void setType(String type) {
        this.type = type;
    }

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    transient HttpServletRequest httpRequest;
    transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }

    // </editor-fold>
}
