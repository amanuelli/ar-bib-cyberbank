/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2021;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;

/**
 * Related issue: TECCDPBO-4399
 *
 * @author azeballos
 */
public class DB20210907_1533_4399 extends DBVSUpdate {

    @Override
    public void up() {
        delete("backoffice_activities", "id_activity = 'cybo.invitationcodes.resend'");
        insertBackofficeActivity("cybo.invitationcodes.resend",
                "com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities.ResendActivity",
                "backoffice.invitationCodes.manage", "backoffice.invitationCodes.approve", "backoffice.invitationcodes",
                ActivityDescriptor.AuditLevel.Full);
    }
}
