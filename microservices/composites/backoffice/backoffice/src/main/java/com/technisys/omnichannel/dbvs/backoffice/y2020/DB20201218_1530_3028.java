package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;

public class DB20201218_1530_3028 extends DBVSUpdate {

    @Override
    public void up() {

        delete("backoffice_activities", "id_activity = 'cybo.environments.holdings.create'");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"cybo.environments.holdings.create", "1", "com.technisys.omnichannel.backoffice.business.cybo.environments.holding.activities.CreateHoldingActivity", "backoffice.environments.manage", "backoffice.environments.approve", "backoffice.environments", ActivityDescriptor.AuditLevel.Full.toString()});

        delete("backoffice_activities", "id_activity = 'cybo.environments.holdings.users.find'");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"cybo.environments.holdings.users.find", "1", "com.technisys.omnichannel.backoffice.business.cybo.environments.holding.activities.ReadAdminActivity", "backoffice.environments.manage", "backoffice.environments.approve", "backoffice.environments", ActivityDescriptor.AuditLevel.Full.toString()});

        delete("backoffice_activities", "id_activity = 'cybo.environments.holdings.readClient'");
        insert("backoffice_activities",
                new String[]{"id_activity", "enabled", "component_fqn", "id_permission_required", "id_permission_approval", "id_group", "auditable"},
                new String[]{"cybo.environments.holdings.readClient", "1", "com.technisys.omnichannel.backoffice.business.cybo.environments.holding.activities.ReadClientActivity", "backoffice.environments.manage", "backoffice.environments.approve", "backoffice.environments", ActivityDescriptor.AuditLevel.Full.toString()});

    }
}
