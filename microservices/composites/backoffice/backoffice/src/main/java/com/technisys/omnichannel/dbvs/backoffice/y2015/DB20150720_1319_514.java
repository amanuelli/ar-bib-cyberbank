/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150720_1319_514 extends DBVSUpdate {

    @Override
    public void up() {
        
        delete("permissions", "id_permission='pay.loan'");
        insert("permissions", new String[]{"id_permission"}, new String[]{"pay.loan"});
        
        delete("permissions_credentials_groups", "id_permission='pay.loan'");
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"pay.loan", "accessToken-otp"});
        
        delete("permissions", "id_permission='pay.loan.thirdParties'");
        insert("permissions", new String[]{"id_permission"}, new String[]{"pay.loan.thirdParties"});
        
        delete("permissions_credentials_groups", "id_permission='pay.loan.thirdParties'");
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"pay.loan.thirdParties", "accessToken-otp"});
        
        insertActivity("pay.loan.preview", "com.technisys.omnichannel.client.activities.pay.loan.PayLoanPreviewActivity", "payLoan", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Transactional, "pay.loan");
        insertActivity("pay.loan.send", "com.technisys.omnichannel.client.activities.pay.loan.PayLoanSendActivity", "payLoan", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "core.authenticated", "debitAccount");
    
        updateConfiguration("core.permissionsForProducts", "product.read|transfer.internal|pay.loan|pay.loan.thirdParties");
    }
}
