/* 
 * Copyright 2016 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2016;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20160704_1113_1595 extends DBVSUpdate {

    @Override
    public void up() {
        insert("campaing_indicator_types", new String[]{"id_type", "ordinal"}, new String[]{"assets", "5"});
        
        insertOrUpdateConfiguration("campaigns.indicators.assets.cache.maximumSize", "5000", ConfigurationGroup.TECNICAS, null, new String[]{});
        insertOrUpdateConfiguration("campaigns.indicators.assets.cache.expireAfter", "24h", ConfigurationGroup.TECNICAS, null, new String[]{});
    }
}
