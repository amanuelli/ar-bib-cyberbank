/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author iocampo
 */

public class DB20190405_0917_6825 extends DBVSUpdate {

    @Override
    public void up() {
        delete("form_field_messages", "lang = 'en' and id_message = 'fields.cashAdvance.installments.help'");
        delete("form_field_messages", "lang = 'en' and id_message = 'fields.cashAdvance.installments.requiredError'");
        delete("form_field_messages", "lang = 'en' and id_message = 'fields.requestLoan.amountOfFees.label'");
        delete("form_field_messages", "lang = 'en' and id_message = 'fields.cashAdvance.amount.help'");

        Map<String, String> formFieldMessages = new HashMap();
        formFieldMessages.put("fields.cashAdvance.installments.help", "It corresponds to the number of installments to which the cash advance of the credit card will be charged.");
        formFieldMessages.put("fields.cashAdvance.installments.requiredError", "You must select a number of installments");
        formFieldMessages.put("fields.requestLoan.amountOfFees.label", "number of installments");
        formFieldMessages.put("fields.cashAdvance.amount.help", "Enter the transaction amount; remember to use the dot to separate decimals.");

        String[] formFieldMessagesColumns = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};

        for (String key : formFieldMessages.keySet()) {
            String[] formFieldsValues = new String[]{key, "en", key.split(Pattern.quote("."))[2], key.split(Pattern.quote("."))[1], "1", "2019-04-05 10:00:00", formFieldMessages.get(key)};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', " + formFieldsValues[4] + ", TO_DATE('2019-04-05 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), '" + formFieldsValues[6] + "')");
            } else {
                insert("form_field_messages", formFieldMessagesColumns, formFieldsValues);
            }
        }
    }

}