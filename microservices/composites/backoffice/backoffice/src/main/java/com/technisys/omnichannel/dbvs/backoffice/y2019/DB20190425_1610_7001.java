/* 
 * Copyright 2019 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 * Related issue: MANNAZCA-7001
 *
 * @author Marcelo Bruno
 */
public class DB20190425_1610_7001 extends DBVSUpdate {

    @Override
    public void up() {
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "ALTER TABLE \"TRANSACTION_LINES\" MODIFY \"CREDIT_AMOUNT_CURRENCY\" VARCHAR(4)");
        } else {
            modifyColumn("transaction_lines", "credit_amount_currency", new ColumnDefinition("credit_amount_currency", Types.VARCHAR, 4, 0, true, null));
        }

        String sqlString = "UPDATE transaction_lines set credit_amount_currency = TRIM(credit_amount_currency)";
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB}, sqlString);
    }
    
}