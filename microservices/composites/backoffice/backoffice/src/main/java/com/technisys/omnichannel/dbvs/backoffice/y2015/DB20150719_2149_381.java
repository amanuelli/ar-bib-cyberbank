/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150719_2149_381 extends DBVSUpdate {

    @Override
    public void up() {
        delete("permissions", "id_permission='transfer.thirdParties'");
        insert("permissions", new String[]{"id_permission"}, new String[]{"transfer.thirdParties"});

        delete("permissions_credentials_groups", "id_permission='transfer.thirdParties'");
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"transfer.thirdParties", "accessToken-otp"});
        
        updateConfiguration("core.permissionsForProducts", "product.read|transfer.internal|transfer.thirdParties");
    }
}
