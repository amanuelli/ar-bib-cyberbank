/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-3985
 *
 * @author rdosantos
 */
public class DB20180626_1448_3985 extends DBVSUpdate {

    @Override
    public void up() {
        
        Map<String, String> messages = new HashMap();
        String idForm = "transferLocal";
        String version = "1";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        
        //borrar sectionDebitData
        delete("form_fields", "id_form='transferLocal' AND id_field = 'sectionDebitData'");
        delete("form_fields", "id_form='transferLocal' AND id_field = 'sectionCreditData'");
        delete("form_fields", "id_form='transferLocal' AND id_field = 'creditAccount'");
        delete("form_fields", "id_form='transferLocal' AND id_field = 'sectionNotificationInfo'");
        delete("form_fields", "id_form='transferLocal' AND id_field = 'debitReference'");
        delete("form_field_messages","id_form='transferLocal' AND id_field='amount'");
        delete("form_field_messages","id_form='transferLocal' AND id_field='creditReference'");
        delete("form_field_messages","id_form='transferLocal' AND id_field='notificationEmails'");
        delete("form_field_messages","id_form='transferLocal' AND id_field='notificationBody'");
        delete("form_field_messages","id_form='transferLocal' AND id_message='fields.transferLocal.debitAccount.label'");
        delete("form_field_messages","id_form='transferLocal' AND id_message='fields.transferLocal.debitAccount.hint'");     
       
        update ("form_fields", new String[]{"ordinal"}, new String[]{"1"}, "id_form='transferLocal' and id_field='amount'");
        messages.put("amount.label","Monto");
        messages.put("amount.requiredError","Debe ingresar un monto");
        
        update ("form_fields", new String[]{"ordinal"}, new String[]{"2"}, "id_form='transferLocal' and id_field='debitAccount'");
        messages.put("debitAccount.label","Cuenta de origen");
        
        update ("form_fields", new String[]{"ordinal"}, new String[]{"3"}, "id_form='transferLocal' and id_field='expenseAccount'");
        
        /**** Insert field selector - Bancos de plaza ****/
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        
        String[] formFieldsValues = new String[]{"bankSelector", idForm, version, "selector", "4", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"bankSelector", idForm, version, "field-normal", "0"});
        
        messages.put("bankSelector.label","Banco de plaza");
        messages.put("bankSelector.requiredError","Debe seleccionar un banco");
        
        // Opciones del selector de bancos
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"bankSelector", idForm, version, "bbva"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"bankSelector", idForm, version, "brou"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version" ,"value"}, new String[]{"bankSelector", idForm, version, "santander"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"bankSelector", idForm, version, "scotiabank"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"bankSelector", idForm, version, "hsbc"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"bankSelector", idForm, version, "bandes"});
        
        messages.put("bankSelector.option.bbva", "BBVA");
        messages.put("bankSelector.option.brou", "BROU");
        messages.put("bankSelector.option.santander", "Santander");
        messages.put("bankSelector.option.scotiabank", "Scotiabank");
        messages.put("bankSelector.option.hsbc", "HSBC");
        messages.put("bankSelector.option.bandes", "Bandes");
        
        
        /**** Insert field text - Numero cuenta destino ****/
        formFieldsValues = new String[]{"creditAccountNumber", idForm, version, "text", "5", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"}, new String[] {"creditAccountNumber", idForm, version, "1", "20", "field-normal"});
        messages.put("creditAccountNumber.label", "Cuenta de destino");
        messages.put("creditAccountNumber.requiredError", "Debe ingresar una cuenta de destino");
        
        
        /**** Insert field text - Nombre y apellido ****/
        formFieldsValues = new String[]{"creditAccountName", idForm, version, "text", "6", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"}, new String[] {"creditAccountName", idForm, version, "1", "100", "field-normal"});
        messages.put("creditAccountName.label", "Nombre del destinatario");
        messages.put("creditAccountName.requiredError", "Debe ingresar una cuenta de destino");
        

        /**** Insert field document - Documento ****/
        formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        formFieldsValues = new String[]{"document", "transferLocal", "1", "document", "7", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_document",new String[]{"id_field", "id_form", "form_version", "default_country", "default_document_type"}, new String[]{"document", "transferLocal", "1", "UY", "CI"});    
        messages.put("document.label", "Documento del destinatario");
        messages.put("document.requiredError", "Debe ingresar un documento");
        
        update("form_fields", new String[]{"ordinal"}, new String[]{"8"}, "id_form='transferLocal' and id_field='creditReference'");
        update("form_field_text", new String[]{"max_length"},new String[]{"20"},"id_form='transferLocal' and id_field='creditReference'");
        messages.put("creditReference.label", "Referencia");
        
        update("form_fields", new String[]{"ordinal"}, new String[]{"9"}, "id_form='transferLocal' and id_field='notificationEmails'");
        messages.put("notificationEmails.label", "Correo electrónico para notificaciones");
        
        update("form_fields", new String[]{"ordinal"}, new String[]{"10"}, "id_form='transferLocal' and id_field='notificationBody'");
        update("form_field_textarea", new String[]{"max_length"},new String[]{"500"},"id_form='transferLocal' and id_field='notificationBody'");
        messages.put("notificationBody.label", "Mensaje");
        
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
        
    }
}