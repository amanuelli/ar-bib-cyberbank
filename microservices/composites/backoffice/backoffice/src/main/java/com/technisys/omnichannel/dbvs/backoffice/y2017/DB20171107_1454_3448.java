/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3448
 *
 * @author dimoda
 */
public class DB20171107_1454_3448 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("notifications.channels", "DEFAULT|SMS|MAIL|FACEBOOK|TWITTER|PUSH");
    }

}