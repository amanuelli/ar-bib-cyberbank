/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author Divier
 */
public class DB20190924_0817_75 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("connectors.core.default", "rubicon", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, "rubicon|cyberbank");

        insertOrUpdateConfiguration("cyberbank.client.contactPhone.areaCode.default", "500002", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.contactPhone.areaCountry.default", "500050", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");

        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.maritalStatus.single", "SOLTERO", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.maritalStatus.married", "CASADO", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.maritalStatus.divorced", "DIVORCIADO", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.maritalStatus.separateFromFact", "SEPARADO DE HECHO", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.maritalStatus.concubine", "CONCUBINO", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.maritalStatus.widower", "VIUDO", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.nationality.default", "Argentina", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.nationality.default", "Argentina", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.countryOfBirth.default", "Argentina", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.numberOfChildren.default", "0", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.basicIndividualData.birthDate.default", "19820522000000000", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        
        insertOrUpdateConfiguration("cyberbank.client.address.street.default", "Sarmiento", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.streetNumber.default", "1234", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.country.default", "Argentina", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.localityZipCode.default", "440820", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.provinceCode.default", "500002", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.countryZipCode.default", "1778", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.townZone.default", "RETIRO", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.floor.default", "19", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.sector.default", "", ConfigurationGroup.TECNICAS, "backend", null, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.apartment.default", "1778", ConfigurationGroup.TECNICAS, "backend", null, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.address.comments.default", "", ConfigurationGroup.TECNICAS, "backend", null, null, "backend");

        insertOrUpdateConfiguration("cyberbank.client.electronicsContact.emailType.default", "De Contacto", ConfigurationGroup.TECNICAS, "backend", null, null, "backend");

        insertOrUpdateConfiguration("cyberbank.client.customer.institutionId.default", "1111", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.customer.confidentialFlag.default", "false", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, "true|false", "backend");
        insertOrUpdateConfiguration("cyberbank.client.customer.channel.default", "BRANCH", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.customer.customerId.default", "1", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");

        insertOrUpdateConfiguration("cyberbank.client.employmentDetails.employmenteStartDate.default", "", ConfigurationGroup.TECNICAS, "backend", null, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.employmentDetails.profession.default", "FUNCIONARIO", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.employmentDetails.employerIdentification.default", "", ConfigurationGroup.TECNICAS, "backend", null, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.employmentDetails.fixedIncome.default", "2000", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");
        insertOrUpdateConfiguration("cyberbank.client.employmentDetails.employmentSituation.default", "10001", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty"}, null, "backend");

    }

}
