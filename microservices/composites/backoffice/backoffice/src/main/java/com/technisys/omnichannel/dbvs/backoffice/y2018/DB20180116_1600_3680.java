/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-3680
 *
 * @author idonadini
 */
public class DB20180116_1600_3680 extends DBVSUpdate {

    @Override
    public void up() {

        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        delete("form_field_messages", "id_message = '" + "fields.paymentClaim.paymentNumber.label" + "' and lang = 'es'");
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, 
                 "INSERT INTO form_field_messages (id_message, id_form, id_field, form_version, lang, value, modification_date) "
                + "VALUES ('" + "fields.paymentClaim.paymentNumber.label" + "', '" + "paymentClaim" + "', '" + "paymentNumber" + "', 1, 'es', '" + "Nro. de cobranza" + "', TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO form_field_messages (id_message, id_form, id_field, form_version, lang, value, modification_date) "
                + "VALUES ('" + "fields.paymentClaim.paymentNumber.label" + "', '" + "paymentClaim" + "', '" + "paymentNumber" + "', 1, 'es', '" + "Nro. de cobranza" + "', '" + date + "')");                
        }

    
    }

}