/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.util.HashMap;
import java.util.Map;

public class DB20180529_1150_3979 extends DBVSUpdate {

    @Override
    public void up() {
        //Transferir a otra cuenta Techbank
        deleteActivity("transfers.thirdParties.preview");
        insertActivity("transfers.thirdParties.preview", "com.technisys.omnichannel.client.activities.transfers.thirdparties.TransfersThirdPartiesPreviewActivity", "transferThirdParties", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        deleteActivity("transfers.thirdParties.send");        
        insertActivity("transfers.thirdParties.send", "com.technisys.omnichannel.client.activities.transfers.thirdparties.TransfersThirdPartiesSendActivity", "transferThirdParties", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, null);
        
        String[] fieldNames = new String[]{"id_activity", "id_field", "id_permission"};
        String[] fieldValues = new String[]{"transfers.thirdParties.send", "debitAccount", "transfer.thirdParties"};
        insert("activity_products", fieldNames, fieldValues);
        
        fieldNames = new String[]{"id_activity", "id_field_amount", "id_field_product"};
        fieldValues = new String[]{"transfers.thirdParties.send", "amount", "debitAccount"};
        insert("activity_caps", fieldNames, fieldValues);
        
        //Borramos el formulario transferThirdParties por si existe, y creamos el nuevo que es Transferencia a terceros
        delete("forms", "id_form = 'transferThirdParties'");
        
        String[] formField = new String[]{"id_form", "category", "enabled", "type", "id_activity", "version", "schedulable"};
        String[] formFieldValues = new String[]{"transferThirdParties", "accounts", "1", "activity", "transfers.thirdParties.send", "1", "1"};
        insert("forms", formField, formFieldValues);
        
        formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        formFieldValues = new String[]{"forms.transferThirdParties.formName", "transferThirdParties", "1", "es", "Transferir a otra cuenta Techbank", "2015-01-01 12:00:00"};
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2]  + "', '" + formFieldValues[3] + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert ("form_messages", formField, formFieldValues);
        }
        
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "sub_type", "ordinal", "visible", "required"};
        
        String[] formFieldsValues = new String[]{"amount", "transferThirdParties", "1", "amount", "default", "1", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"debitAccount", "transferThirdParties", "1", "productselector", "default", "2", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
                
        formFieldsValues = new String[]{"creditAccount", "transferThirdParties", "1", "text", "default", "3", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"reference", "transferThirdParties", "1", "text", "default", "4", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"notificationMails", "transferThirdParties", "1", "emaillist", "default", "5", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"notificationBody", "transferThirdParties", "1", "textarea", "default", "6", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
        formFieldsValues = new String[]{"amount", "transferThirdParties", "1", "field-small", "1"};
        insert("form_field_amount", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type"};
        formFieldsValues = new String[]{"notificationMails", "transferThirdParties", "1", "field-normal"};
        insert("form_field_emaillist", formFields, formFieldsValues);

        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option"};
        formFieldsValues = new String[]{"debitAccount", "transferThirdParties", "1", "field-normal", "0"};
        insert("form_field_ps", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        formFieldsValues = new String[]{"debitAccount", "transferThirdParties", "1", "transfer.thirdParties"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "transferThirdParties", "1", "CA"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "transferThirdParties", "1", "CC"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"reference", "transferThirdParties", "1", "0", "20", "field-normal"};
        insert("form_field_text", formFields, formFieldsValues);
        
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"creditAccount", "transferThirdParties", "1", "0", "14", "field-small"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"notificationBody", "transferThirdParties", "1", "0", "500", "field-medium"};
        insert("form_field_textarea", formFields, formFieldsValues);
        
        
        //Solo doy de alta español
        Map<String, String> messages = new HashMap();
        messages.put("amount.label","Monto");
        messages.put("reference.label","Referencia");
        messages.put("debitAccount.label","Cuenta de origen");
        messages.put("notificationBody.label","Mensaje");
        messages.put("notificationMails.label","Correo electrónico para notificaciones");
        messages.put("creditAccount.label","Cuenta de destino");
        
        formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.transferThirdParties." + key, "es", key.substring(0, key.indexOf(".")), "transferThirdParties", "1", messages.get(key), "2015-01-01 12:00:00"};
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2]  + "', '" + formFieldsValues[3]  + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }
}
