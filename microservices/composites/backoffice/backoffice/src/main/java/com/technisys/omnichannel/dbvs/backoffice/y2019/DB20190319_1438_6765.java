/* 
 * Copyright 2019 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 * Related issue: MANNAZCA-6765
 *
 * @author Marcelo Bruno
 */
public class DB20190319_1438_6765 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("massive.payments.allowed.banks", "bandes|bbva|brou|hsbc|santander|scotiabank", ConfigurationGroup.NEGOCIO, "forms", new String[]{"notEmpty"});
        
        addColumn("transaction_lines", new ColumnDefinition("bank_identifier", Types.VARCHAR, 20, 0, true, null));
    }

}