/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201308271406_185 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"^\\w+([\\.+]?\\w+)@\\w+([\\.]?\\w+)\\.(\\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum))$"}, "id_form=17 and id_field='email'");
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"^\\w+([\\.+]?\\w+)@\\w+([\\.]?\\w+)\\.(\\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum))$"}, "id_form=17 and id_field='emailConyugueAdicional'");
        update("form_fields", new String[]{"validation_regexp"}, new String[]{"^\\w+([\\.+]?\\w+)@\\w+([\\.]?\\w+)\\.(\\w{2}|(com|net|org|edu|int|mil|gov|arpa|biz|aero|name|coop|info|pro|museum))$"}, "id_form=17 and id_field='emailDatosLaboralesAdicional'");
    }
}