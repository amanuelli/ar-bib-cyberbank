/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20151105_0916_983 extends DBVSUpdate {

    @Override
    public void up() {
        delete("sections", "id_section='desktop-widget'");
        insert("sections", new String[]{"id_section", "name"}, new String[]{"desktop-widget", "Escritorio - widget"});

        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, "INSERT INTO sections_images (id_section, id_image_size) SELECT 'desktop-widget', id_image_size FROM image_sizes WHERE image_width=1536 AND image_height=350");
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, "INSERT INTO sections_images (id_section, id_image_size) SELECT 'desktop-widget', id_image_size FROM image_sizes WHERE image_width=2160 AND image_height=240");
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, "INSERT INTO sections_images (id_section, id_image_size) SELECT 'desktop-widget', id_image_size FROM image_sizes WHERE image_width=3160 AND image_height=260");

        // Widget
        insert("widgets", new String[]{"id", "uri"}, new String[]{"campaigns", "widgets/campaigns"});
    }
}
