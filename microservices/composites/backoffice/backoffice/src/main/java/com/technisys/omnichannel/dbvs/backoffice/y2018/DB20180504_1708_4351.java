/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4351
 *
 * @author rdosantos
 */
public class DB20180504_1708_4351 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(new String[]{DBVS.DIALECT_MYSQL,DBVS.DIALECT_MSSQL,DBVS.DIALECT_ORACLE,DBVS.DIALECT_HSQLDB},"UPDATE configuration SET channels = 'frontend' WHERE id_field='core.password.maxLength'");
        customSentence(new String[]{DBVS.DIALECT_MYSQL,DBVS.DIALECT_MSSQL,DBVS.DIALECT_ORACLE,DBVS.DIALECT_HSQLDB},"UPDATE configuration SET channels = 'frontend' WHERE id_field='core.password.minLength'");
    }

}