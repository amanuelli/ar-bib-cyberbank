/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201106161423_10254 extends DBVSUpdate {

    @Override
    public void up() {

        delete("configuration", "id_field='rubicon.productTypes.PCF'");
        delete("configuration", "id_field='rubicon.productTypes.PSI'");
        delete("configuration", "id_field='rubicon.productTypes.PSIA'");
        delete("configuration", "id_field='rubicon.productTypes.PSII'");

        String[] fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};

        String[] fieldValues = new String[]{"rubicon.productTypes.PI", "PI", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.productTypes.PA", "PA", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);
    }
}