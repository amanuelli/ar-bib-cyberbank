/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author pbanales
 */

public class DB20190416_1721_6568 extends DBVSUpdate {

    @Override
    public void up() {
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms.transferThirdParties.formName', 'transferThirdParties', 1, 'pt', 'Transferir para outra conta do Techbank', TO_DATE('2019-04-16 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL}, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms.transferThirdParties.formName', 'transferThirdParties', 1, 'pt', 'Transferir para outra conta do Techbank', '2019-04-16 00:00:00')");
        }
    }
}
