/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2781
 *
 * @author fpena
 */
public class DB20170726_1456_2781 extends DBVSUpdate {

    @Override
    public void up() {

        Map<String, String> formFieldMessages = new HashMap();

        formFieldMessages.put("fields.payCreditCard.amount.label", "Amount payable");
        formFieldMessages.put("fields.payCreditCard.amount.requiredError", "You must enter an amount");

        formFieldMessages.put("fields.payCreditCard.creditCard.label", "Credit card");
        formFieldMessages.put("fields.payCreditCard.creditCard.requiredError", "You must select a credit card");
        formFieldMessages.put("fields.payCreditCard.creditCard.showOtherText", "Another credit card");
        formFieldMessages.put("fields.payCreditCard.creditCardBank.label", "Bank Card");
        formFieldMessages.put("fields.payCreditCard.creditCardBank.requiredError", "You must select a bank");
        formFieldMessages.put("fields.payCreditCard.otherAmount.label", "Amount payable");
        formFieldMessages.put("fields.payCreditCard.otherAmount.requiredError", "You must enter an amount");
        formFieldMessages.put("fields.payCreditCard.otherCreditCard.label", "Another credit card");
        formFieldMessages.put("fields.payCreditCard.otherCreditCardNumber.help", "You must enter here the credit card number to pay");
        formFieldMessages.put("fields.payCreditCard.otherCreditCardNumber.label", "Credit card number");
        formFieldMessages.put("fields.payCreditCard.otherCreditCardNumber.requiredError", "You must enter a credit card number");

        formFieldMessages.put("fields.payLoan.loan.label", "Loan");
        formFieldMessages.put("fields.payLoan.loan.showOtherText", "Another loan");
        formFieldMessages.put("fields.payLoan.loanPayment.label", "Payment type");
        formFieldMessages.put("fields.payLoan.loanPayment.labelPartialPay", "Amount to pay");
        formFieldMessages.put("fields.payLoan.loanPayment.labelTotalPay", "Total amount payable");
        formFieldMessages.put("fields.payLoan.loanPayment.partialPay", "Partial payment");
        formFieldMessages.put("fields.payLoan.loanPayment.totalPay", "Total payment");
        formFieldMessages.put("fields.payLoan.otherLoanAmount.label", "Amount to pay");
        formFieldMessages.put("fields.payLoan.otherLoanNumber.label", "Loan number");

        formFieldMessages.keySet().forEach((key) -> {
            customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO form_field_messages (id_message, id_form, form_version, lang, id_field, value, modification_date) "
                    + "SELECT '" + key + "', id_form, form_version, 'en', id_field, '" + formFieldMessages.get(key) + "', modification_date "
                    + "FROM form_field_messages "
                    + "WHERE id_message = '" + key + "' AND lang='es' ");
        });
    }

}
