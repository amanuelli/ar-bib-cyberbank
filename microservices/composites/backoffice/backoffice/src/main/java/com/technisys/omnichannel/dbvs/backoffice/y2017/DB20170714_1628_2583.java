/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-2583
 *
 * @author rschiappapietra
 *
 */
public class DB20170714_1628_2583 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("widgets.notifications");
        insertActivity("widgets.notifications", "com.technisys.omnichannel.client.activities.widgets.NotificationsActivity",
                "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        delete("widgets", "id='notifications'");
        insert("widgets", new String[]{"id", "uri"}, new String[]{"notifications", "widgets/notifications"});
    }
}

