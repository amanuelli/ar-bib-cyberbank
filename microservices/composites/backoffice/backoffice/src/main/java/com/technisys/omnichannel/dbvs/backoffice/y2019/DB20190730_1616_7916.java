/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * @author Marcelo Bruno
 */

public class DB20190730_1616_7916 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "accountFund";

        deleteActivity("account.fund.send");
        deleteActivity("account.fund.preview");
        delete("permissions",  "id_permission='account.fund'");
        delete("form_field_types", "id_type = 'accountfundplaid'");
        delete("forms", "id_form = 'accountFund'");

        insertOrUpdateConfiguration("plaid.integration.balance.get.url", "https://sandbox.plaid.com/accounts/balance/get", ConfigurationGroup.TECNICAS, "plaidIntegration", new String[]{"notEmpty", "url"});
        insertOrUpdateConfiguration("plaid.integration.public.token.exchange.url", "https://sandbox.plaid.com/item/public_token/exchange", ConfigurationGroup.TECNICAS, "plaidIntegration", new String[]{"notEmpty", "url"});

        insertOrUpdateConfiguration("plaid.integration.clientId",
                "5d4079370a5f8300172143c1",
                 ConfigurationGroup.TECNICAS,
                "plaidIntegration",
                 new String[]{"notEmpty"},
                null,
                true
        );

        insertOrUpdateConfiguration("plaid.integration.secret",
                "0ab0cb1904acc7b23c2c84e69fcc2d",
                 ConfigurationGroup.TECNICAS,
                "plaidIntegration",
                 new String[]{"notEmpty"},
                null,
                true
        );

        insertOrUpdateConfiguration("plaid.integration.publicKey",
                "d3d43ab510fe0cec31f78a6d756735",
                 ConfigurationGroup.TECNICAS,
                "plaidIntegration",
                 new String[]{"notEmpty"},
                null,
                true,
                null,
                null,
                "frontend"
        );

        insert("permissions", new String[] { "id_permission" }, new String[] { "account.fund" });
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"account.fund", "accessToken-pin"});

        insertActivity("account.fund.send", "com.technisys.omnichannel.client.activities.accounts.FundAccountSendActivity", "accounts", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, "account.fund", "creditAccount");
        insertActivity("account.fund.preview", "com.technisys.omnichannel.client.activities.accounts.FundAccountPreviewActivity", "accounts", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        String[] fieldNames = new String[] { "id_permission", "simple_id_category", "simple_ordinal", "simple_allow_prod_selection", "medium_id_category", "medium_ordinal", "medium_allow_prod_selection", "advanced_id_category", "advanced_ordinal", "advanced_allow_prod_selection", "product_types" };
        String[] fieldValues = new String[] { "account.fund", "accounts", "2", "0", "accounts", "2", "0", "accounts", "2", "1", "CA,CC" };
        insert("adm_ui_permissions", fieldNames, fieldValues);

        // form
        insert("form_field_types", new String[]{"id_type"}, new String[]{"accountfundplaid"});

        fieldNames = new String[] { "id_form", "version", "enabled", "category", "type", "admin_option", "id_activity", "last", "deleted", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow", "id_bpm_process", "programable" };
        fieldValues = new String[] { idForm, "1", "1", "accounts", "activity", null, "account.fund.send", "1", "0", "0", "0", "0", "1", "1", null, "0" };
        insert("forms", fieldNames, fieldValues);

        // fields
        fieldNames = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};

        fieldValues = new String[]{"creditAccount", idForm, "1", "productselector", "1", "TRUE", "TRUE", "1", "0", "default", "0"};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"amount", idForm, "1", "amount", "2", "TRUE", "TRUE", "1", "0", "default", "0"};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"debitAccount", idForm, "1", "accountfundplaid", "3", "TRUE", "TRUE", "1", "0", "default", "0"};
        insert("form_fields", fieldNames, fieldValues);

        // product selector
        fieldNames = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option", "filter_actives", "show_other_option_in_mobile", "apply_frequent_destinations"};
        fieldValues = new String[]{"creditAccount", idForm, "1", "field-normal", "0", "1", "0", "0"};
        insert("form_field_ps", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        fieldValues = new String[]{"creditAccount", idForm, "1", "product.read"};
        insert("form_field_ps_permissions", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        fieldValues = new String[]{"creditAccount", idForm, "1", "CA"};
        insert("form_field_ps_product_types", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        fieldValues = new String[]{"creditAccount", idForm, "1", "CC"};
        insert("form_field_ps_product_types", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount"};
        fieldValues = new String[]{"amount", idForm, "1", "field-normal", "0", "1"};
        insert("form_field_amount", fieldNames, fieldValues);

        Map<String, String> formFieldMessagesEn = new HashMap();
        formFieldMessagesEn.put("fields.accountFund.amount.label", "Amount");
        formFieldMessagesEn.put("fields.accountFund.amount.requiredError", "Amount is required");
        formFieldMessagesEn.put("fields.accountFund.debitAccount.label", "Debit account");
        formFieldMessagesEn.put("fields.accountFund.debitAccount.requiredError", "Debit account is required");
        formFieldMessagesEn.put("fields.accountFund.creditAccount.label", "Credit account");
        formFieldMessagesEn.put("fields.accountFund.creditAccount.requiredError", "Credit account is required");

        Map<String, String> formFieldMessagesEs = new HashMap();
        formFieldMessagesEs.put("fields.accountFund.amount.label", "Monto");
        formFieldMessagesEs.put("fields.accountFund.amount.requiredError", "El monto es requerido");
        formFieldMessagesEs.put("fields.accountFund.debitAccount.label", "Cuenta débito");
        formFieldMessagesEs.put("fields.accountFund.debitAccount.requiredError", "Cuenta débito es requerida");
        formFieldMessagesEs.put("fields.accountFund.creditAccount.label", "Cuenta crédito");
        formFieldMessagesEs.put("fields.accountFund.creditAccount.requiredError", "Cuenta crédito es requerida");

        Map<String, String> formFieldMessagesPt = new HashMap();
        formFieldMessagesPt.put("fields.accountFund.amount.label", "Monto");
        formFieldMessagesPt.put("fields.accountFund.amount.requiredError", "Amount is required");
        formFieldMessagesPt.put("fields.accountFund.debitAccount.label", "Debit account");
        formFieldMessagesPt.put("fields.accountFund.debitAccount.requiredError", "Debit account is required");
        formFieldMessagesPt.put("fields.accountFund.creditAccount.label", "Credit account");
        formFieldMessagesPt.put("fields.accountFund.creditAccount.requiredError", "Credit account is required");

        insertFormMessages(idForm, "Fund my account", "en");
        insertFormMessages(idForm, "Fondear mi cuenta", "es");
        insertFormMessages(idForm, "financiar minha conta", "pt");

        insertFormFieldMessages(formFieldMessagesEn, "en");
        insertFormFieldMessages(formFieldMessagesPt, "pt");
        insertFormFieldMessages(formFieldMessagesEs, "es");

    }

    private void insertFormFieldMessages(Map<String, String> formFieldMessages, String lang) {
        String[] formFieldMessagesColumns = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        for (String key : formFieldMessages.keySet()) {
            String[] formFieldsValues = new String[]{key, lang, key.split(Pattern.quote("."))[2], key.split(Pattern.quote("."))[1], "1", "2019-08-06 10:00:00", formFieldMessages.get(key)};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', " + formFieldsValues[4] + ", TO_DATE('2019-04-05 10:00:00', 'YYYY-MM-DD HH24:MI:SS'), '" + formFieldsValues[6] + "')");
            } else {
                insert("form_field_messages", formFieldMessagesColumns, formFieldsValues);
            }
        }
    }

    private void insertFormMessages(String idForm, String message, String lang) {
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] values = new String[]{"forms.accountFundTest.formName", idForm, "1", lang, message, date};
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form,version, lang, value, modification_date) "
                    + " VALUES ('" + values[0] + "', '" + values[1] + "', '" + values[2] + "', '" + values[3] + "', '" + values[4] + "', TO_DATE('" + values[5] + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, "1", lang, message, date});
        }
    }

}