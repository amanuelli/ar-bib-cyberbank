/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author salva
 */
public class DB20150811_1214_678 extends DBVSUpdate {

    @Override
    public void up() {
        //Borrmos el formulario modifyUserData por si existe, y creamos el nuevo que es Modificar datos personales
        delete("form_field_messages", "id_form = 'modifyUserData'");
        delete("form_messages", "id_form = 'modifyUserData'");
        delete("form_field_countrylist", "id_form = 'modifyUserData'");
        delete("forms", "id_form = 'modifyUserData'");
        
        String[] formField = new String[]{"id_form", "category", "enabled", "type", "id_activity", "version", "schedulable"};
        String[] formFieldValues = new String[]{"modifyUserData", "others", "1", "activity", "preferences.userData.modify.send", "1", "0"};
        insert("forms", formField, formFieldValues);
        
        formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        formFieldValues = new String[]{"forms.modifyUserData.formName", "modifyUserData", "1" , "es", "Modificar datos del usuario", "2015-01-01 12:00:00"};
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2]  + "', '" + formFieldValues[3]  + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert ("form_messages", formField, formFieldValues);
        }
        
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required"};
        String[] formFieldsValues = new String[]{"phoneNumber", "modifyUserData", "1", "text", "6", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        formFieldsValues = new String[]{"phoneNumber", "modifyUserData", "1", "0", "50", "field-big", "onlyNumbers"};
        insert("form_field_text", formFields, formFieldsValues);
        
        //Solo doy de alta español, y las key que tengan valor
        Map<String, String> messages = new HashMap();
        messages.put("phoneNumber.help","Ingrese el código de pais y número de teléfono fijo o celular.");
        messages.put("phoneNumber.label","Teléfono");
        messages.put("phoneNumber.placeholder","Ingrese su teléfono fijo o celular");
        
        formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.modifyUserData." + key, "es", key.substring(0, key.indexOf(".")), "modifyUserData", "1", messages.get(key), "2015-01-01 12:00:00"};
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3]  + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }
}
