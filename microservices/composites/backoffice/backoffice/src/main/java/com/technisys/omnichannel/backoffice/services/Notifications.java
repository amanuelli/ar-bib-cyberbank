/*
 * Copyright 2016 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.services;

import com.technisys.omnichannel.backoffice.domain.NotificationServiceVariable;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.domain.CommunicationConfiguration;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

/**
 *
 * @author salva
 */
@WebService(serviceName = "WSNotifications")
@SOAPBinding(style = SOAPBinding.Style.RPC)
public class Notifications {

    private static final Logger log = LoggerFactory.getLogger(Notifications.class);

    private static final String RETURN_CODE_OK = "0";
    private static final String RETURN_CODE_USER_DOES_NOT_EXIST = "1";
    private static final String RETURN_CODE_TYPE_EMPTY = "2";
    private static final String RETURN_CODE_INVALID_TYPE = "3";
    private static final String RETURN_CODE_DOCUMENT_TYPE_EMPTY = "4";
    private static final String RETURN_CODE_DOCUMENT_EMPTY = "5";
    private static final String RETURN_CODE_DOCUMENT_COUNTRY_EMPTY = "6";
    private static final String RETURN_CODE_NO_CHANNEL_TO_SEND = "7";
    private static final String RETURN_CODE_NO_TRAY_CONFIGURED = "8";
    private static final String RETURN_CODE_UNEXPED_ERROR = "99";

    @WebMethod(operationName = "send")
    public String send(@WebParam(name = "documentCountry") String documentCountry,
            @WebParam(name = "documentType") String documentType,
            @WebParam(name = "document") String documentNumber,
            @WebParam(name = "userId") String userId,
            @WebParam(name = "title") String title,
            @WebParam(name = "message") String message,
            @WebParam(name = "type") Integer type,
            @WebParam(name = "channels") String[] channels,
            @WebParam(name = "variables") NotificationServiceVariable[] variables) {

        try {
            String logMessage;
            //se validan campos obligatorios

            if (StringUtils.isBlank(userId)) {
                if (StringUtils.isBlank(documentCountry)) {
                    log.warn("Document country was not specified. ReturnCode " + RETURN_CODE_DOCUMENT_COUNTRY_EMPTY);
                    return (RETURN_CODE_DOCUMENT_COUNTRY_EMPTY);
                } else if (StringUtils.isBlank(documentType)) {
                    log.warn("Document type was not specified. ReturnCode " + RETURN_CODE_DOCUMENT_TYPE_EMPTY);
                    return (RETURN_CODE_DOCUMENT_TYPE_EMPTY);
                } else if (StringUtils.isBlank(documentNumber)) {
                    log.warn("Document number was not specified. ReturnCode " + RETURN_CODE_DOCUMENT_EMPTY);
                    return (RETURN_CODE_DOCUMENT_EMPTY);
                }
            }if (type == null || type == 0) {
                log.warn("Communication type was not specified. ReturnCode " + RETURN_CODE_TYPE_EMPTY);
                return (RETURN_CODE_TYPE_EMPTY);
            }

            User user;
            if (StringUtils.isBlank(userId)) {
                user = AccessManagementHandlerFactory.getHandler().getUserByDocument(documentCountry, documentType, documentNumber);
            } else {
                user = AccessManagementHandlerFactory.getHandler().getUser(userId);
            }
            //el usuario existe
            if (user == null) {
                logMessage= MessageFormat.format("There is not registered user for {0}-{1}-{2}. ReturnCode {3}", documentCountry, documentType, documentNumber, RETURN_CODE_USER_DOES_NOT_EXIST);
                log.warn(logMessage);
                return RETURN_CODE_USER_DOES_NOT_EXIST;
            }
            
            String communicationType = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "services.notification.communicationType." + String.valueOf(type));
            //el tipo ingresado exite
            if (StringUtils.isBlank(communicationType)) {
                logMessage= MessageFormat.format("There is not communication type configured for {0}. ReturnCode {1}", type, RETURN_CODE_INVALID_TYPE);
                log.error(logMessage);
                return RETURN_CODE_INVALID_TYPE;
            }

            int communicationTray = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "services.notification.communicationTray." + communicationType, -1);
            if (communicationTray == -1) {
                logMessage= MessageFormat.format("There is not communication tray configured for {0}. ReturnCode {1}", communicationType, RETURN_CODE_NO_TRAY_CONFIGURED);
                log.error(logMessage);
                return RETURN_CODE_NO_TRAY_CONFIGURED;
            }

            // Cargo los canales habilitados
            if (channels != null) {
                for (int i = 0; i < channels.length; i++) {
                    channels[i] = (channels[i] == null) ? "" : channels[i].toUpperCase();
                }
            }
            Set<String> channelSet = getChannelsToNotificate(user.getIdUser(), communicationType, channels);
            if (channelSet.isEmpty()) {
                logMessage= MessageFormat.format("The user {0} is not subscribed to at least one channel. ReturnCode {1}", user.getIdUser(), RETURN_CODE_NO_CHANNEL_TO_SEND);
                log.warn(logMessage);
                return RETURN_CODE_NO_CHANNEL_TO_SEND;
            }

            String subject = "";
            if (StringUtils.isNotBlank(title)) {
                subject = replaceTextFillers(title, Arrays.asList(variables));
            } else {
                // Como quiero crear una única comunicación solo tomo el subject DEFAULT...
                subject = I18nFactory.getHandler().getMessage("services.notification." + communicationType + "." + Communication.TRANSPORT_DEFAULT + ".subject", user.getLang());
            }
            
            String body = "";
            if (StringUtils.isNotBlank(message)) {
                body = replaceTextFillers(message, Arrays.asList(variables));
            }

            Map<String, String> bodyByTransportMap = new HashMap<>();
            
            for (String channel : channelSet) {
                if (StringUtils.isBlank(message)) {
                    body = I18nFactory.getHandler().getMessage("services.notification." + communicationType + "." + channel + ".body", true, user.getLang(), null);
                    if (StringUtils.isBlank(body)) {
                        body = I18nFactory.getHandler().getMessage("services.notification." + communicationType + ".DEFAULT.body", user.getLang());
                    }
                }
                
                bodyByTransportMap.put(channel, replaceTextFillers(body, Arrays.asList(variables)));
            }
            
            
            NotificationsHandlerFactory.getHandler().create(null, communicationTray, subject, bodyByTransportMap, null, communicationType, false, new String [] { user.getIdUser() }, null, null, -1, Communication.PRIORITY_LOW);
            logMessage= MessageFormat.format("Communication for {0} ({1}-{2}-{3}) has been successfully created. ReturnCode {4}", user.getIdUser(), documentCountry, documentType, documentNumber, RETURN_CODE_OK);
            log.info(logMessage);
            return RETURN_CODE_OK;
        } catch (IOException ex) {
            log.error("Unexpected error creating communication for " + documentCountry
                        + "-" + documentType + "-" + documentNumber +
                        ". ReturnCode " + RETURN_CODE_UNEXPED_ERROR, ex);
            return RETURN_CODE_UNEXPED_ERROR;
        }
    }

    private static Set getChannelsToNotificate(String idUser, String communicationType, String[] requestedChannels) throws IOException {
        // Cargo los canales habilitados
        List<String> enabledChannels = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "notifications.channels");

        // Cargo la configuracion de comunicaciones para el usuario y el tipo
        CommunicationConfiguration comunicationConfiguration = NotificationsHandlerFactory.getHandler().readCommunicationConfiguration(idUser, communicationType);

        // Busco la intersección o todo si no vino nada
        Set<String> userChannels = new HashSet<>();
        for (String enabledChannel : enabledChannels) {
            if (comunicationConfiguration.isChannelEnabled(enabledChannel)) {
                userChannels.add(enabledChannel);
            }
        }
        
        if ((requestedChannels != null) && (requestedChannels.length > 0)) {
            for (int i = 0; i < requestedChannels.length; i++) {
                requestedChannels[i] = (requestedChannels[i] == null) ? null : requestedChannels[i].toUpperCase();
            }
            
            // Me quedo con la intersección entre lo pedido y lo configurado
            userChannels.retainAll(Arrays.asList(requestedChannels));
        }

        return userChannels;
    }

    public static String replaceTextFillers(String text, List<NotificationServiceVariable> fillers) {
        for (NotificationServiceVariable filler : fillers) {
            //falta agregar para los distintos tipos de campo la forma de desplegarlos
            switch (filler.getType()) {
                default:
                    text = text.replace(filler.getKey(), filler.getValue());
            }
        }
        return text;
    }
}
