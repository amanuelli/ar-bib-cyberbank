/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author iocampo
 */

public class DB20190423_1502_7035 extends DBVSUpdate {

    @Override
    public void up() {
        updateActivity("administration.users.update", "com.technisys.omnichannel.client.activities.administration.users.UpdateUserActivity", "administration.manage", "administration.users", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin);
    }

}