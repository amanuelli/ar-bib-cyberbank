/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2665
 *
 * @author rmedina
 */
public class DB20170713_1140_2665 extends DBVSUpdate {

    @Override
    public void up() {
        update("permissions_credentials_groups", new String[]{"id_credential_group"}, new String[]{"accessToken-pin"}, "id_credential_group='pin'");
        delete("permissions_credentials_groups", "id_permission ='client.form.4.send'" );
    }
}