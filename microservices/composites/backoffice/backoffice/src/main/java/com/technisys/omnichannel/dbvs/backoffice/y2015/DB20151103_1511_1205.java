/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20151103_1511_1205 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("session.acceptGeneralConditions", "com.technisys.omnichannel.client.activities.session.AcceptGeneralConditionsActivity", "acceptGeneralConditions", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("session.downloadGeneralConditions", "com.technisys.omnichannel.client.activities.session.DownloadGeneralConditionsActivity", "acceptGeneralConditions", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
    }
}
