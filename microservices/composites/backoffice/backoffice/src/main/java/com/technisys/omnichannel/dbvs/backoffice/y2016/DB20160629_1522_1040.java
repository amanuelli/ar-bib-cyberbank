/*
 *  Copyright 2016 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2016;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Homologaci&oacute;n de mensajes de notificaciones
 *
 * @since 2.0.10
 * 
 * @author <a href="mailto:nicolas.orbes@technisys.com">Nicol&aacute;s Orbes</a>
 */
public class DB20160629_1522_1040 extends DBVSUpdate {

    @Override
    public void up() {
       
        
        updateConfiguration("core.communications.communicationTypes", "message|accepted|rejected|pending|transferReceived|creditCardExpiration|loanExpiration|creditCardPayment|loanPayment|promos|userBlock");
        
        insertOrUpdateConfiguration("core.communications.communicationTypes.userBlock", "WEB|MAIL", ConfigurationGroup.NEGOCIO, null, new String[]{});
        
        insertOrUpdateConfiguration("services.notification.communicationTray.userBlock", "1", ConfigurationGroup.NEGOCIO, null, new String[]{});
    }
}
