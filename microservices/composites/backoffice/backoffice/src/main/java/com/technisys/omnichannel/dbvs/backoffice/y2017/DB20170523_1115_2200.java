/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2200
 *
 * @author sbarbosa
 */
public class DB20170523_1115_2200 extends DBVSUpdate {

    @Override
    public void up() {
        // Form
        String idForm = "requestTransactionCancellation";
        String version = "1";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

        delete("forms", "id_form='" + idForm + "' and version=" + version);

        delete("permissions", "id_permission = 'client.form." + idForm + ".send'");
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

        //forms
        insert("forms", new String[]{"id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable"},
                new String[]{idForm, version, "1", "payments", "process", "demo:1:3", null, null, "0", "0", "0"});

        //form_fields
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};

        int ordinal = 1;
        Map<String, String> messages = new HashMap();

        // Referencia (reference)
        insert("form_fields", formFields, new String[]{"reference", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"reference", idForm, version, "1", "200", "field-normal", null});

        messages.put("reference.help", "Ingrese el identificador de la transacción");
        messages.put("reference.hint", null);
        messages.put("reference.label", "Referencia");

        // Tipo de transacción (transactionType)
        insert("form_fields", formFields, new String[]{"transactionType", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"},
                new String[]{"transactionType", idForm, version, "0", "20", "field-normal", null});

        messages.put("transactionType.help", "Ingrese el identificador de la transacción");
        messages.put("transactionType.hint", null);
        messages.put("transactionType.label", "Tipo de transacción");
        messages.put("transactionType.requiredError", "Debe ingresar el tipo de transacción");

        // Fecha de la transacción (date)
        insert("form_fields", formFields, new String[]{"date", idForm, version, "date", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_date", new String[]{"id_field", "id_form", "form_version", "display_type"},
                new String[]{"date", idForm, version, "field-small"});

        messages.put("date.help", "Ingrese la fecha en que fue realizada la transacción");
        messages.put("date.hint", null);
        messages.put("date.label", "Fecha de la transacción");
        messages.put("date.requiredError", "Debe ingresar la fecha de la transacción");

        // Observaciones (observations)
        insert("form_fields", formFields, new String[]{"observations", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_textarea", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"},
                new String[]{"observations", idForm, version, "0", "1000", "field-normal"});

        messages.put("observations.help", "Ingrese el motivo para la cancelación y cualquier información adicional para identificar la transacción que considere pertinente");
        messages.put("observations.hint", null);
        messages.put("observations.label", "Observaciones");

        // Disclaimer (disclaimer)
        insert("form_fields", formFields, new String[]{"disclaimer", idForm, version, "termsandconditions", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1"});
        insert("form_field_terms_conditions", new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"},
                new String[]{"disclaimer", idForm, version, "field-medium", "0", "0"});

        messages.put("disclaimer.termsAndConditions", "La recepción de esta solicitud no garantiza la cancelación de la transacción, dicho proceso estará sujeto al horario de recepción, el estado u otros factores que impidan completar la solicitud.");
        messages.put("disclaimer.hint", null);
        messages.put("disclaimer.help", null);
        messages.put("disclaimer.label", " ");

        //Insertado de Mensajes
        //*********************************************************************
        String[] formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        String[] formFieldsValues = new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Solicitud de cancelacion de transaccion", date};

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form,version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formField, formFieldsValues);
        }

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

        insertOrUpdateConfiguration("requestTransactionCancellation.validityWindow", "7d", ConfigurationGroup.SEGURIDAD, null, new String[]{});
    }

}
