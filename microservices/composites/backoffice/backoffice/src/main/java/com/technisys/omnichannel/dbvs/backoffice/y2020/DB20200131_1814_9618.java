/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author Marcelo Bruno
 */

public class DB20200131_1814_9618 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("administration.environment.restrictions.send");
        deleteActivity("administration.environment.user.restrictions.send");

        insertActivity("administration.environment.restrictions.send", "com.technisys.omnichannel.client.activities.administration.common.PersistEnvironmentRestrictionsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Admin, "administration.manage");
        insertActivity("administration.users.restrictions.send", "com.technisys.omnichannel.client.activities.administration.users.PersistEnvironmentUserRestrictionsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Admin, "administration.manage");
    }

}