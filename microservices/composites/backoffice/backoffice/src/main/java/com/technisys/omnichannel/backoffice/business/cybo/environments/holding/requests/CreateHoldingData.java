/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

/*
 * @author pbanales
 */

package com.technisys.omnichannel.backoffice.business.cybo.environments.holding.requests;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.core.domain.TransactionData;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import org.apache.commons.lang3.StringUtils;


public class CreateHoldingData extends TransactionData {

    private static final String SPACE = "      ";
    private static final String EM = ": <em>";
    private static final String EMBR = "</em><br/>";
    private static final String DT = "<dt>";
    private static final String DT_CLOSE = ":</dt>";
    private static final String DD = "<dd>";
    private static final String DD_CLOSE = "</dd>";

    private String adminDocumentCountry;
    private String adminLanguage;
    private String adminDocumentType;
    private String adminDocumentNumber;
    private String adminEmail;
    private String adminLastName;
    private String adminFirstName;
    private String adminAccessType;
    private String administrationScheme;
    private Integer signatureQty;
    private String holdingName;
    private String holdingMembers;
    private String adminMobileNumber;
    private Boolean sendAdminInvite;

    @Override
    public String calculateAdditionalInfoToHash() {
        StringBuilder additionalInfo = new StringBuilder(super.calculateAdditionalInfoToHash());

        additionalInfo.append("-");
        additionalInfo.append(adminDocumentCountry);
        additionalInfo.append("-");
        additionalInfo.append(adminDocumentType);
        additionalInfo.append("-");
        additionalInfo.append(adminDocumentNumber);
        additionalInfo.append("-");
        additionalInfo.append(holdingName);
        additionalInfo.append("-");
        additionalInfo.append(holdingMembers);

        return additionalInfo.toString();
    }

    @Override
    public String getIdentifier() {
        return getHoldingName();
    }
    
    @Override
    public String getShortDescription(String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();
        String lang = getLang();

        strBuilder.append("<h5>Acci&oacute;n: <a href=\"javascript:showPendingAction('").append(idTransaction).append("')\"> ").append(activityName).append("</a></h5>");
        strBuilder.append("<p class='element_details'>");
        strBuilder.append(SPACE).append(i18n.getMessage("backoffice.environments.holdings.create.type", lang))
                .append(EM)
                .append(i18n.getMessage("backoffice.environments.holdings.create.typeToDisplay", lang))
                .append(EMBR);
        strBuilder.append(SPACE).append(i18n.getMessage("backoffice.environments.holdings.create.name", lang))
                .append(EM)
                .append(getHoldingName())
                .append(EMBR);
        strBuilder.append(SPACE).append(i18n.getMessage("backoffice.environments.holdings.create.members", lang))
                .append(EM).append("<br/>");
        String[] lMembers = getHoldingMembers().split(", ");

        for (String lMember : lMembers) {
            strBuilder.append(SPACE).append(SPACE).append(getMemberName(idTransaction, lMember)).append("<br/>");
        }

        strBuilder.append(EMBR);

        if (getSendAdminInvite()) {
            strBuilder.append(SPACE).append(i18n.getMessage("backoffice.environments.holdings.create.adminData", lang)).append(EMBR);
            strBuilder.append(SPACE).append(i18n.getMessage("backoffice.environments.holdings.create.adminFullName", lang)).append(EM)
                    .append(adminFirstName).append(" ").append(adminLastName).append(EMBR);
            strBuilder.append(SPACE).append(i18n.getMessage("backoffice.environments.holdings.create.adminDocumentNumber", lang)).append(EM)
                    .append(adminDocumentNumber)
                    .append(" (").append(i18n.getMessage("documentType.label." + adminDocumentType, lang))
                    .append(", ").append(i18n.getMessage("country.name." + adminDocumentCountry, lang)).append(")")
                    .append(EMBR);
        }

        return strBuilder.toString();
    }

    @Override
    public String getFullDescription(String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<dl class = 'element_details clearfix'>");

        strBuilder.append(DT).append(i18n.getMessage("backoffice.environments.holdings.create.type", getLang())).append(DT_CLOSE)
                .append(DD).append("Holding").append(DD_CLOSE);

        strBuilder.append(DT).append(i18n.getMessage("backoffice.environments.holdings.create.name", getLang())).append(DT_CLOSE)
                .append(DD).append(getHoldingName()).append(DD_CLOSE);
        strBuilder.append(DT).append(i18n.getMessage("backoffice.environments.holdings.create.members", getLang())).append(DT_CLOSE)
                .append(DD);
        String[] lMembers = getHoldingMembers().split(", ");

        for (String lMember : lMembers) {
            strBuilder.append(SPACE).append(SPACE).append(getMemberName(idTransaction, lMember)).append("<br/>");
        }
        strBuilder.append(DD_CLOSE);

        if (StringUtils.isNotBlank(administrationScheme)) {
            strBuilder.append(DT).append(i18n.getMessage("backoffice.environments.holdings.create.administrationScheme", getLang())).append(DT_CLOSE)
                    .append(DD).append(i18n.getMessage("backoffice.environments.schemes." + administrationScheme, getLang())).append(DD_CLOSE);
        }
        if (signatureQty != null && signatureQty > 0) {
            strBuilder.append(DT).append(i18n.getMessage("backoffice.environments.holdings.create.signatureScheme", getLang())).append(DT_CLOSE)
                    .append(DD).append(i18n.getMessage("backoffice.environments.holdings.create.signatureScheme." + (signatureQty > 1 ? "joint" : "indistinct"), getLang())).append(DD_CLOSE);
        }

        if (getSendAdminInvite()) {
            strBuilder.append(DT).append(i18n.getMessage("backoffice.environments.holdings.create.adminFullName", getLang())).append(DT_CLOSE)
                    .append(DD).append(adminFirstName).append(" ").append(adminLastName).append(DD_CLOSE);
            strBuilder.append(DT).append(i18n.getMessage("backoffice.environments.holdings.create.adminDocumentNumber", getLang())).append(DT_CLOSE)
                    .append(DD)
                    .append(adminDocumentNumber)
                    .append(" (").append(i18n.getMessage("documentType.label." + adminDocumentType, getLang())).append(", ")
                    .append(i18n.getMessage("country.name." + adminDocumentCountry, getLang())).append(")")
                    .append(DD_CLOSE);
        }

        strBuilder.append("</dl>");

        return strBuilder.toString();
    }

    private String getMemberName(String transactionID, String memberID) {
        String result;

        try {
            result = RubiconCoreConnectorC.readClient(transactionID, memberID).getAccountName();
        } catch (Exception ex) {
            result = "";
        }

        return result;
    }

    public String getAdminLanguage() {
        return adminLanguage;
    }

    public void setAdminLanguage(String adminLanguage) {
        this.adminLanguage = adminLanguage;
    }

    public Boolean getSendAdminInvite() {
        return sendAdminInvite != null ? sendAdminInvite : false;
    }

    public void setSendAdminInvite(Boolean sendAdminInvite) {
        this.sendAdminInvite = sendAdminInvite;
    }

    public String getAdminMobileNumber() {
        return adminMobileNumber;
    }

    public void setAdminMobileNumber(String mobile) {
        adminMobileNumber = mobile;
    }

    public String getAdminEmail() {
        return adminEmail;
    }

    public void setAdminEmail(String email) {
        adminEmail = email;
    }

    public String getAdminDocumentNumber() {
        return adminDocumentNumber;
    }

    public void setAdminDocumentNumber(String adminDocumentNumber) {
        this.adminDocumentNumber = adminDocumentNumber;
    }

    public String getAdminDocumentType() {
        return adminDocumentType;
    }

    public void setAdminDocumentType(String documentType) {
        adminDocumentType = documentType;
    }

    public String getAdminDocumentCountry() {
        return adminDocumentCountry;
    }

    public void setAdminDocumentCountry(String documentNumberCountry) {
        adminDocumentCountry = documentNumberCountry;
    }

    public String getAdminAccessType() {
        return adminAccessType;
    }

    public void setAdminAccessType(String accessType) {
        adminAccessType = accessType;
    }

    public String getAdministrationScheme() {
        return administrationScheme;
    }

    public void setAdministrationScheme(String administrationScheme) {
        this.administrationScheme = administrationScheme;
    }

    public Integer getSignatureQty() {
        return signatureQty;
    }

    public void setSignatureQty(Integer signatureQty) {
        this.signatureQty = signatureQty;
    }

    public String getAdminFirstName() {
        return adminFirstName;
    }

    public void setAdminFirstName(String adminFirstName) {
        this.adminFirstName = adminFirstName;
    }

    public String getAdminLastName() {
        return adminLastName;
    }

    public void setAdminLastName(String adminLastName) {
        this.adminLastName = adminLastName;
    }

    public String getHoldingName() {
        return holdingName;
    }

    public void setHoldingName(String name) {
        holdingName = name;
    }

    public String getHoldingMembers() {
        return holdingMembers;
    }

    public void setHoldingMembers(String holdingMembers) {
        this.holdingMembers = holdingMembers;
    }
}
