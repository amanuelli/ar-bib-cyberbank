/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

public class DB20180605_1317_3961 extends DBVSUpdate {

    @Override
    public void up() {
        //Borrar campos que ya no corresponden al pago de tarjeta
        delete("form_fields", "id_form='payCreditCard' AND id_field in ('otherAmount', 'notificationBody', 'notificationEmails', 'line', 'reference')");
        delete("activity_caps", "id_activity='pay.creditcard.send' AND id_field_amount = 'otherAmount'");
        
        update ("form_fields", new String[]{"read_only"}, new String[]{"1"}, "id_form='payCreditCard' and id_field='creditCard'");
        update ("form_fields", new String[]{"visible"}, new String[]{"TRUE"}, "id_form='payCreditCard' and id_field='amount'");
        update ("form_fields", new String[]{"required"}, new String[]{"TRUE"}, "id_form='payCreditCard' and id_field='amount'");
        update ("form_fields", new String[]{"ordinal"}, new String[]{"1"}, "id_form='payCreditCard' and id_field='creditCard'");
        update ("form_fields", new String[]{"ordinal"}, new String[]{"2"}, "id_form='payCreditCard' and id_field='amount'");
        update ("form_fields", new String[]{"ordinal"}, new String[]{"3"}, "id_form='payCreditCard' and id_field='debitAccount'");
        
        update ("form_fields", new String[]{"ordinal"}, new String[]{"4"}, "id_form='payCreditCard' and id_field='debitAmount'");
        update ("form_fields", new String[]{"ordinal"}, new String[]{"5"}, "id_form='payCreditCard' and id_field='rate'");
        update ("form_fields", new String[]{"ordinal"}, new String[]{"6"}, "id_form='payCreditCard' and id_field='commission'");
        
        update ("form_field_cc_amount_curr", new String[]{"id_currency"}, new String[]{"222"}, "id_form='payCreditCard' and id_field='amount'");
        
        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_message='fields.payCreditCard.amount.help'");
        update("form_field_messages", new String[]{"value"}, new String[]{"TARJETA DE CRÉDITO"}, "id_message='fields.payCreditCard.creditCard.label'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Cuenta de origen"}, "id_message='fields.payCreditCard.debitAccount.label'");
        update("form_field_messages", new String[]{"value"}, new String[]{"Pagar"}, "id_message='fields.payCreditCard.amount.label'");
        
        update("form_messages", new String[]{"value"}, new String[]{"Pago de tarjeta de crédito"}, "id_form='payCreditCard'");
    }
}
