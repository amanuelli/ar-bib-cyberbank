/*
 * Copyright 2018 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6128
 *
 * @author Marcelo Bruno
 */
public class DB20181214_1452_6128 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("administration.signatures.signatureLevels", "A|B|C|D|E|F|G");
    }

}