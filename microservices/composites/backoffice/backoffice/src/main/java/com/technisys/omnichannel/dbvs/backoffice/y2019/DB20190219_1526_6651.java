/*
 * Copyright 2019 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author dimoda
 */
public class DB20190219_1526_6651 extends DBVSUpdate {

    @Override
    public void up() {
        insert("document_types", new String[]{"id_document_type"}, new String[]{"RG"});
        insert("document_types_country_codes",
                new String[]{"id_country_code", "id_document_type", "mrtd_document_type"},
                new String[]{"BR", "RG", "I"});

    }
    
}
