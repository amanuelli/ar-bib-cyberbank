/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author salva
 */
public class DB20150728_1158_446 extends DBVSUpdate {

    @Override
    public void up() {
        //Borrmos el formulario modifyUserData por si existe, y creamos el nuevo que es Modificar datos personales
        delete("forms", "id_form = 'modifyUserData'");
        
        String[] formField = new String[]{"id_form", "category", "enabled", "type", "id_activity", "version", "schedulable"};
        String[] formFieldValues = new String[]{"modifyUserData", "others", "1", "activity", "preferences.userData.modify.send", "1", "0"};
        insert("forms", formField, formFieldValues);
        
        formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        formFieldValues = new String[]{"forms.modifyUserData.formName", "modifyUserData", "1" , "es", "Modificar datos del usuario", "2015-01-01 12:00:00"};
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2] + "', '" + formFieldValues[3]  + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert ("form_messages", formField, formFieldValues);
        }
        
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required"};
        String[] formFieldsValues = new String[]{"mobileNumber", "modifyUserData", "1", "text", "7", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"district", "modifyUserData", "1", "text", "2", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"department", "modifyUserData", "1", "text", "4", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"address", "modifyUserData", "1", "text", "1", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"email", "modifyUserData", "1", "text", "8", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"phoneNumber", "modifyUserData", "1", "text", "6", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"province", "modifyUserData", "1", "text", "3", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"residenceCountry", "modifyUserData", "1", "countrylist", "5", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value"};
        formFieldsValues = new String[]{"residenceCountry", "modifyUserData", "1", "field-big", "UY"};
        insert("form_field_countrylist", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"district", "modifyUserData", "1", "0", "50", "field-big"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"address", "modifyUserData", "1", "0", "50", "field-big"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"province", "modifyUserData", "1", "0", "50", "field-big"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"department", "modifyUserData", "1", "0", "50", "field-big"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        formFieldsValues = new String[]{"email", "modifyUserData", "1", "0", "50", "field-big", "email"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"mobileNumber", "modifyUserData", "1", "0", "50", "field-big", "onlyNumbers"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"phoneNumber", "modifyUserData", "1", "0", "50", "field-big", "onlyNumbers"};
        insert("form_field_text", formFields, formFieldsValues);
        
        //Solo doy de alta español, y las key que tengan valor
        Map<String, String> messages = new HashMap();
        messages.put("district.label","Distrito");
        messages.put("district.placeholder","Ingrese su distrito");
        messages.put("department.label","Departamento");
        messages.put("department.placeholder","Ingrese su departamento");
        messages.put("address.label","Dirección de residencia");
        messages.put("address.placeholder","Ingrese su dirección");
        messages.put("email.label","Email");
        messages.put("email.placeholder","Ingrese su email");
        messages.put("mobileNumber.help","Ingrese el código de país y número de teléfono móvil.");
        messages.put("mobileNumber.label","Teléfono móvil");
        messages.put("mobileNumber.placeholder","Ingrese su teléfono móvil");
        messages.put("phoneNumber.help","Ingrese el código de país y número de teléfono fijo.");
        messages.put("phoneNumber.label","Teléfono fijo");
        messages.put("phoneNumber.placeholder","Ingrese su teléfono fijo");
        messages.put("province.label","Provincia");
        messages.put("province.placeholder","Ingrese su provincia");
        messages.put("residenceCountry.label","País de residencia");
        messages.put("residenceCountry.placeholder","Ingrese su país de residencia");
        
        formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.modifyUserData." + key, "es", key.substring(0, key.indexOf(".")), "modifyUserData", "1", messages.get(key), "2015-01-01 12:00:00"};
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3]  + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }
}