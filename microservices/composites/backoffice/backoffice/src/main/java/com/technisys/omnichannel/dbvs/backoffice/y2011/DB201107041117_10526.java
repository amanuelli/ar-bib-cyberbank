/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107041117_10526 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};

        String[] fieldValues = new String[]{"rub.userconfiguration.sendForgotPasswordEmail", "1", "1", "com.technisys.rubicon.business.userconfiguration.activities.SendForgotPasswordEmailActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.userconfiguration.changePassword", "1", "1", "com.technisys.rubicon.business.userconfiguration.activities.ChangePasswordActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};

        fieldValues = new String[]{"rubicon.userconfiguration.forgotpasswordmail.url", "http://localhost:8080/rubicon/forgotPassword/changepassword", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"core.changepassword.link.codeExpiration.hours", "1", "6|12|24", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

    }
}