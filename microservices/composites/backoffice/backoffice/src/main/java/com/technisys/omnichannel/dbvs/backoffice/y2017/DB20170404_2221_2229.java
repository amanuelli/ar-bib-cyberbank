/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author fpena
 */
public class DB20170404_2221_2229 extends DBVSUpdate {

    @Override
    public void up() {
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB}, "UPDATE communications SET id_user=id_client_user");
        
        dropForeignKey("communications", "fk_comm_client");
        dropColumn("communications", "id_client_user");
        
        delete("form_fields", "id_form = 'modifyUserData'");
        delete("form_field_text", "id_form = 'modifyUserData'");
        delete("form_field_messages", "id_form = 'modifyUserData'");
        
        Map<String, String> messages = new HashMap();
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile", "read_only", "form_version"};
        String[] formFieldsValues;
        String idForm = "modifyUserData";
        String version = "1";

        formFieldsValues = new String[]{"residenceCountry", idForm, "text", "1", "TRUE", "FALSE", "default", "1", "1", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "min_length", "max_length", "display_type", "form_version"}, new String[] {"residenceCountry", idForm, "1", "50", "field-normal", "1"});
        messages.put("residenceCountry.label", "País residencia");
        messages.put("residenceCountry.requiredError", "Debe ingresar un país");
        
        formFieldsValues = new String[]{"address", idForm, "text", "2", "TRUE", "FALSE", "default", "1", "1", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "min_length", "max_length", "display_type", "form_version"}, new String[] {"address", idForm, "1", "50", "field-normal", "1"});
        
        messages.put("address.label", "Dirección");
        messages.put("address.requiredError", "Debe ingresar una dirección");
        
        formFieldsValues = new String[]{"gender", idForm, "text", "3", "TRUE", "FALSE", "default", "1", "1", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "min_length", "max_length", "display_type", "form_version"}, new String[] {"gender", idForm, "1", "50", "field-normal", "1"});
        
        messages.put("gender.label", "Sexo");
        messages.put("gender.requiredError", "Debe ingresar el sexo");
        
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, date, messages.get(key)};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }        

    }
}
