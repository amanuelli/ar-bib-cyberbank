/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150903_1131_507 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("enrollment.digital.pre", "com.technisys.omnichannel.client.activities.enrollment.DigitalPreActivity", "enrollment", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("enrollment.digital.documentTypes", "com.technisys.omnichannel.client.activities.misc.ListDocumentTypesActivity", "enrollment", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("enrollment.digital.finish", "com.technisys.omnichannel.client.activities.enrollment.DigitalFinishActivity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);
    }
}
