/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

import java.util.StringJoiner;

/**
 * @author dimoda
 */

public class DB20190124_1450_5584 extends DBVSUpdate {

    @Override
    public void up() {

        update("activities", new String[]{"id_group"}, new String[]{"administration.signatures"},
                "id_activity='administration.medium.modify.signature.send'");

        StringJoiner groupsActivities = new StringJoiner("','", "'", "'");
        groupsActivities.add("administration.advanced.group.create.pre");
        groupsActivities.add("administration.advanced.group.create.preview");
        groupsActivities.add("administration.advanced.group.create.send");
        groupsActivities.add("administration.advanced.group.modify.pre");
        groupsActivities.add("administration.advanced.group.modify.preview");
        groupsActivities.add("administration.advanced.group.modify.send");
        groupsActivities.add("administration.advanced.group.read");
        groupsActivities.add("administration.user.detail.groups.modify.send");
        groupsActivities.add("administration.user.detail.groups.modify.preview");
        update("activities", new String[]{"id_group"}, new String[]{"administration.groups"},
                "id_activity in (" + groupsActivities.toString() + ")");

        StringJoiner usersActivities = new StringJoiner("','", "'", "'");
        usersActivities.add("administration.advanced.user.details.read");
        usersActivities.add("administration.medium.user.details.read");
        usersActivities.add("administration.medium.read.channels");
        update("activities", new String[]{"id_group"}, new String[]{"administration.users"},
                "id_activity in (" + usersActivities + ")");

        StringJoiner permissionsActivities = new StringJoiner("','", "'", "'");
        permissionsActivities.add("administration.medium.modify.permissions.send");
        permissionsActivities.add("administration.simple.modify.permissions.send");
        permissionsActivities.add("administration.medium.modify.channels.send");
        permissionsActivities.add("administration.simple.read.permissions");
        permissionsActivities.add("administration.simple.modify.permissions.preview");
        permissionsActivities.add("administration.medium.read.permissions");
        permissionsActivities.add("administration.medium.modify.permissions.preview");
        permissionsActivities.add("administration.medium.modify.channels.preview");
        update("activities", new String[]{"id_group"}, new String[]{"administration.permissions"},
                "id_activity in (" + permissionsActivities+ ")");

    }

}