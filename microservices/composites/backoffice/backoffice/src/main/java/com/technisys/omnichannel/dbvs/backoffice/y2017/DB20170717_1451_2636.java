/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author danireb
 */
public class DB20170717_1451_2636 extends DBVSUpdate {

    @Override
    public void up() {
        update("forms", new String[]{"id_activity"}, new String[]{"frequentdestinations.create"}, "id_form='frequentDestination'");
    }
}
