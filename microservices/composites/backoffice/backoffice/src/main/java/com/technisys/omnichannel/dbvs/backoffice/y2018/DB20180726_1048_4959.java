/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4889
 *
 * @author ncarril
 */
public class DB20180726_1048_4959 extends DBVSUpdate {

    @Override
    public void up() {
        update("activity_products", new String[]{"id_permission"}, new String[]{"user.preferences.withSecondFactor"}, "id_activity='preferences.userData.mail.sendCode'");
        update("activity_products", new String[]{"id_permission"}, new String[]{"user.preferences.withSecondFactor"}, "id_activity='preferences.userData.mobilePhone.sendCode'");
    }


}