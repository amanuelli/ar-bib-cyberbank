/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-3822
 *
 * @author midned
 */
public class DB20180312_1521_3822 extends DBVSUpdate {

    @Override
    public void up() {
        String[] keys;
        String[] values;

        //
        // send mail code activity
        //
        keys = new String[]{"component_fqn"};
        values = new String[]{"com.technisys.omnichannel.client.activities.preferences.userdata.SendMailCodeActivity"};

        update("activities", keys, values, "id_activity='preferences.userData.updateMail.send'");
        
        //
        // send mobile phone code
        //
        keys = new String[]{"component_fqn"};
        values = new String[]{"com.technisys.omnichannel.client.activities.preferences.userdata.SendMobilePhoneCodeActivity"};

        update("activities", keys, values, "id_activity='preferences.userData.updateMobilePhone.send'");
        
        // remove old version
        deleteActivity("preferences.userData.modify.send");
        
        //
        // update mail activity
        //
        insertActivity("preferences.userData.mail.update", "com.technisys.omnichannel.client.activities.preferences.userdata.UpdateMailActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        //
        // update mobile phone activity
        //
        insertActivity("preferences.userData.mobilePhone.update", "com.technisys.omnichannel.client.activities.preferences.userdata.UpdateMobilePhoneActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }

}