/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author aelters
 */
public class DB20150626_1648_496 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("desktop.loadFrequentActions", "com.technisys.omnichannel.client.activities.desktop.LoadFrequentActionsActivity", "desktop", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertOrUpdateConfiguration("desktop.frequentActions.limit", "3", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        insertOrUpdateConfiguration("desktop.frequentActions.maxToShow", "3", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        insertOrUpdateConfiguration("client.desktop.frequentActions.supportedActivityList", "accounts.requestCheckBook|accounts.internalTranference|accounts.vacancyTransference|creditCards.payment|accounts.foreignTransfer", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        insertOrUpdateConfiguration("client.desktop.frequentActions.frequentActions.defaults.corporate", "${BASE_URL}/accounts/index:activities.accounts.list|${BASE_URL}/loans/index:activities.loans.list", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
        insertOrUpdateConfiguration("client.desktop.frequentActions.frequentActions.defaults.retail", "${BASE_URL}/accounts/index:activities.accounts.list|${BASE_URL}/loans/index:activities.loans.list", ConfigurationGroup.NEGOCIO, "frontend", new String[]{});
       
    }
}
