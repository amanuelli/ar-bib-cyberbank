/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150720_1146_496 extends DBVSUpdate {

    @Override
    public void up() {
                
        deleteConfiguration("accounts.list.url");
        deleteConfiguration("loans.list.url");
        deleteConfiguration("frequentActions.supportedActivityList");

        updateConfiguration("frequentActions.defaults.retail", "2");
        updateConfiguration("frequentActions.defaults.corporate", "2");
    }
}
