/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author amauro
 */
public class DB20191113_1556_8489 extends DBVSUpdate {

    private final String FORM_FIELD_MESSAGES = "form_field_messages";
    private final String[] FIELD_VALUE = new String[]{"value"};

    @Override
    public void up() {

        Map<String, String> messagesEN = new HashMap<>();
        Map<String, String> messagesES = new HashMap<>();
        Map<String, String> messagesPT = new HashMap<>();

        String formVersion = "1";
        messagesPT.put("label", "Correio/s eletrônicos para notificar");

        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Correo/s electrónicos a notificar"}, "id_field ='notificationMails' AND lang ='es' AND id_message ='fields.transferThirdParties.notificationMails.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Email/s to be notified"}, "id_field ='notificationMails' AND lang ='en' AND id_message ='fields.transferThirdParties.notificationMails.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Correio/s eletrônicos para notificar"}, "id_field ='notificationMails' AND lang ='pt' AND id_message ='fields.transferThirdParties.notificationMails.label'");

        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Correo/s electrónicos a notificar"}, "id_field ='notificationEmails' AND lang ='es' AND id_message ='fields.payThirdPartiesLoan.notificationEmails.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Email/s to be notified"}, "id_field ='notificationEmails' AND lang ='en' AND id_message ='fields.payThirdPartiesLoan.notificationEmails.label'");
        insertFormFieldMessages("payThirdPartiesLoan", "notificationEmails", formVersion, messagesEN, messagesES, messagesPT);

        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Correo/s electrónicos a notificar"}, "id_field ='notificationEmails' AND lang ='es' AND id_message ='fields.payThirdPartiesCreditCard.notificationEmails.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Email/s to be notified"}, "id_field ='notificationEmails' AND lang ='en' AND id_message ='fields.payThirdPartiesCreditCard.notificationEmails.label'");
        insertFormFieldMessages("payThirdPartiesCreditCard", "notificationEmails", formVersion, messagesEN, messagesES, messagesPT);

        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Correo/s electrónicos a notificar"}, "id_field ='notificationEmails' AND lang ='es' AND id_message ='fields.transferLocal.notificationEmails.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Email/s to be notified"}, "id_field ='notificationEmails' AND lang ='en' AND id_message ='fields.transferLocal.notificationEmails.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Correio/s eletrônicos para notificar"}, "id_field ='notificationEmails' AND lang ='pt' AND id_message ='fields.transferLocal.notificationEmails.label'");
    }
}