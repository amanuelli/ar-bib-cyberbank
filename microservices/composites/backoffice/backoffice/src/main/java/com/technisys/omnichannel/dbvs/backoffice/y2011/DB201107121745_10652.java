/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107121745_10652 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn"};

        String[] fieldValues = new String[]{"rub.creditcards.payCreditCardTransferQuery", "1", "1", "com.technisys.rubicon.business.creditcards.activities.PayCreditCardTransferQueryActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.loans.payLoanTransferQuery", "1", "1", "com.technisys.rubicon.business.loans.activities.PayLoanTransferQueryActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.accounts.transferAbroadTransferQuery", "1", "1", "com.technisys.rubicon.business.accounts.activities.TransferAbroadTransferQueryActivity"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.accounts.transferLocalTransferQuery", "1", "1", "com.technisys.rubicon.business.accounts.activities.TransferLocalTransferQueryActivity"};
        insert("activities", fieldNames, fieldValues);

    }
}