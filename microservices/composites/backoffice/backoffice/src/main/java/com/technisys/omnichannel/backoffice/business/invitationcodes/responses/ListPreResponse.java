/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.responses;

import java.util.ArrayList;
import java.util.List;

import com.technisys.omnichannel.core.domain.InvitationCodesStatus;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;

/**
 * 
 * @author szuliani
 *
 */
public class ListPreResponse extends IBResponse {

    private static final long serialVersionUID = 1L;
    private List<InvitationCodesStatus> statusList;

    public ListPreResponse(IBRequest request) {
        super(request);
        statusList = new ArrayList<InvitationCodesStatus>();
    }

    public List<InvitationCodesStatus> getStatusList() {
        return statusList;
    }

    public void setStatusList(List<InvitationCodesStatus> statusList) {
        this.statusList = statusList;
    }

}
