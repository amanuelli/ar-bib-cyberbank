/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author ncarril
 */
public class DB20180813_1312_5209 extends DBVSUpdate {

    private final String FORM_FIELD_MESSAGES = "form_field_messages";
    
    @Override
    public void up() {
        
        delete(FORM_FIELD_MESSAGES, "id_message = 'fields.transferForeign.shouldDebitFromOriginAccount.option.true'");
        String[] field_messages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};


        List<String[]> messages = new ArrayList<String[]>();
        
        messages.add(new String[]{"fields.transferForeign.shouldDebitFromOriginAccount.option.true", "es", "shouldDebitFromOriginAccount", "transferForeign", "1", "2018-08-13 13:12:00", "Todas las comisiones se debitarán de la cuenta origen seleccionada. Si lo deseas puedes cargar los gastos a otra cuenta."});

        for (String[] key : messages) {
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + key[0] + "', '" + key[1] + "', '" + key[2] + "', '" + key[3] + "', '" + key[4] + "', '" + key[6] + "', TO_DATE('" + key[5] + "', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert(FORM_FIELD_MESSAGES, field_messages, key);
            }
        }
    }


}