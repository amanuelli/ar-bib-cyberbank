/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.invitationcodes.requests;

import com.technisys.omnichannel.core.domain.TransactionData;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;

/**
 *
 * @author Sebastian Barbosa
 */
public class ResendInvitationCodeData extends TransactionData {

    private int idCode;
    private String document;
    private String name;
    private String account;

    @Override
    public String calculateAdditionalInfoToHash() {
        StringBuilder additionalInfo = new StringBuilder(super.calculateAdditionalInfoToHash());

        additionalInfo.append("-");
        additionalInfo.append(idCode);

        return additionalInfo.toString();
    }

    @Override
    public String getIdentifier() {
        return getDocument();
    }
    
    @Override
    public String getShortDescription(String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<h5>Acci&oacute;n: <a href=\"javascript:showPendingAction('").append(idTransaction).append("')\"> ").append(activityName).append("</a></h5>");
        strBuilder.append("<p class='element_details'>");
        strBuilder.append("      ").append(i18n.getMessage("backoffice.invitationCodes.resend.document", getLang())).append(": <em>").append(document).append("</em><br/>");
        strBuilder.append("      ").append(i18n.getMessage("backoffice.invitationCodes.resend.comments", getLang())).append(": <em>").append(this.getComment()).append("</em><br/>");

        return strBuilder.toString();
    }

    @Override
    public String getFullDescription(String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<dl class = 'element_details clearfix'>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.resend.name", getLang())).append(":</dt>")
                .append("<dd>").append(name).append("</dd>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.resend.document", getLang())).append(":</dt>")
                .append("<dd>").append(document).append("</dd>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.invitationCodes.resend.account", getLang())).append(":</dt>")
                .append("<dd>").append(account).append("</dd>");
        strBuilder.append("</dl>");

        return strBuilder.toString();
    }

    public int getIdCode() {
        return idCode;
    }

    public void setIdCode(int idCode) {
        this.idCode = idCode;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    @Override
    public String toString() {
        return "ResendInvitationCodeData{" + "idCode=" + idCode + ", document=" + document + ", name=" + name + ", account=" + account + '}';
    }

}
