/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2896
 *
 * @author ivitale
 */
public class DB20170822_1116_2896 extends DBVSUpdate {

    @Override
    public void up() {
         update("form_field_selector", new String[] { "render_as" }, new String[] { "radio" }, "id_field='shippingDiscrepancies' AND id_form='presentationOfDocumentsUnderCollection'");
        }


}