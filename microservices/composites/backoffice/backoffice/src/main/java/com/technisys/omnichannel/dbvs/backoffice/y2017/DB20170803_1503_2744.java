/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class DB20170803_1503_2744 extends DBVSUpdate {

    @Override
    public void up() {
        
        /** Datos del FORMULARIO ***/
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "sub_type", "visible_in_mobile", "read_only", "ticket_only" };
        String[] formFieldsValues;
        String idForm = "preFinancingConstitution";
        String version = "1";
        Map<String, String> messages = new HashMap();
        
        /**** Insert Form - Constitución prefinanciación ****/
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[] { "id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable", "editable_in_mobile", "editable_in_narrow" };
        String[] formsFieldsValues = new String[] { idForm, version, "1", "comex", "process", "demo:1:3", "comex", null, "1", "1", "1", "0", "1" };
        insert("forms", formsFields, formsFieldsValues);

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version,lang,value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "','" + version + "','es',  'Constitución de prefinanciación',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value", "modification_date" };
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "es", "Constitución de prefinanciación", date });
        }

        String clientFormId = "client.form." + idForm + ".send";
        delete("permissions", "id_permission = '" + clientFormId + "'");
        delete("permissions_credentials_groups", "id_permission = '" + clientFormId + "'");
        delete("group_permissions", "id_permission = '" + clientFormId + "'");
        insert("permissions", new String[] { "id_permission" }, new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[] { "id_permission", "id_credential_group" }, new String[] { "client.form." + idForm + ".send", "accessToken-pin" });

        String idField;
        
        /**** Insert field selector - Tipo de prefinanciación ****/
        idField = "prefinancingType";        
        formFieldsValues = new String[]{idField, idForm, version, "selector", "1", "TRUE", "TRUE",null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as" },
        new String[] { idField, idForm, version, "field-big","compraSinRecursoDeposito", "1", "combo" });
        messages.put("prefinancingType.label", "Tipo de prefinanciación");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "compraSinRecursoDeposito"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "financiacionSintetica"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "financiacionOperación"});
        messages.put("prefinancingType.option.compraSinRecursoDeposito","Compra sin recurso del depósito");
        messages.put("prefinancingType.option.financiacionSintetica", "Financiación sintética - financiera");
        messages.put("prefinancingType.option.financiacionOperación","Financiación 100% de la operación");
        
        /**** Insert field amount - Tasa acordada para la operación ****/
        idField = "rateAgreedOperation";
        formFieldsValues = new String[]{idField, idForm, version, "amount", "2", "TRUE", "TRUE",null ,"default", "0", "0", "0"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount"}, new String[]{idField, idForm, version, "field-normal", "1", "0"});
        // falta controla "Máximo 3 dígitos para la parte decimal y 2 para la parte entera. Debe ser mayor a cero.")
        messages.put("rateAgreedOperation.label", "Tasa acordada para la operación");
        messages.put("rateAgreedOperation.requiredError","La tasa acordada no es válida.");
        
        /**** Insert field amount - Importe ****/
        idField = "import";
        formFieldsValues = new String[]{idField, idForm, version, "amount", "3", "TRUE", "TRUE","Luego de ingresar un importe, se debe desplegar el valor en lenguaje natural." ,"default", "0", "0", "0"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount"}, new String[]{idField, idForm, version, "field-normal", "1", "0"});
        insert("form_field_amount_currencies", new String[]{"id_field", "id_form", "form_version", "id_currency"}, new String[]{idField, idForm, version, "222"});
        messages.put("import.label", "Importe");
        messages.put("import.requiredError","Debe ingresar un importe.");
        
        /**** Insert field text - Rubro NCM ****/
        idField = "rubro";
        formFieldsValues = new String[] { idField, idForm, version, "text", "4", "TRUE", "TRUE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation" }, new String[] { idField, idForm, version, "field-normal", "1", "10", "onlyNumbers"});
        messages.put("rubro.label","Rubro NCM");
        messages.put("rubro.help","Cargue aquí el NCM más representativo de las exportaciones afectadas");
        messages.put("rubro.requiredError","Debe ingresar el rubro NCM");
        
        /**** Insert field selector - Calidad del exportador ****/
        idField = "exporterQuality";        
        formFieldsValues = new String[]{idField, idForm, version, "selector", "5", "TRUE", "TRUE",null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type","default_value", "show_blank_option", "render_as" },
                new String[] { idField, idForm, version, "field-normal","directo", "0", "combo" });
        messages.put("exporterQuality.label", "Calidad del exportador");
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "directo"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "indirecto"});
        messages.put("exporterQuality.option.directo","Directo");
        messages.put("exporterQuality.option.indirecto", "Indirecto");
        
        /**** Insert field text - RUT del exportador ****/
        idField = "exporterRUT";
        formFieldsValues = new String[] { idField, idForm, version, "text", "6", "TRUE", "TRUE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation" }, new String[] { idField, idForm, version, "field-normal", "0", "12", "onlyNumbers"});
        messages.put("exporterRUT.label","RUT del exportador");

        /**** Insert field text - Nombre del exportador ****/
        idField = "exporterName";
        formFieldsValues = new String[] { idField, idForm, version, "text", "7", "TRUE", "TRUE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation" }, new String[] { idField, idForm, version, "field-big", "0", "200", null});
        messages.put("exporterName.label","Nombre del exportador");
        
        /**** Insert field text - Código de referencia ****/
        idField = "referenceCode";
        formFieldsValues = new String[] { idField, idForm, version, "text", "8", "TRUE", "TRUE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation" }, new String[] { idField, idForm, version, "field-smaller", "0", "3", "onlyNumbers"});
        messages.put("referenceCode.label","Código de referencia");
        messages.put("referenceCode.requiredError","Si está en blanco: Debe ingresar el código de referencia");
        
        /**** Insert field selector - Tipo de empresa ****/
        idField = "typeCompany";        
        formFieldsValues = new String[]{idField, idForm, version, "selector", "9", "TRUE", "TRUE",null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as" },new String[] { idField, idForm, version, "field-normal", "0", "combo" });
        messages.put("typeCompany.label", "Tipo de empresa");
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "circularx"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "circularxx"});
        messages.put("typeCompany.option.circularx","Circular X");
        messages.put("typeCompany.option.circularxx", "Circular XX");
        
        /**** Insert field selector - Plazo ****/
        idField = "term";        
        formFieldsValues = new String[]{idField, idForm, version, "selector", "10", "TRUE", "TRUE",null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as" },
                new String[] { idField, idForm, version, "field-normal", "0", "combo" });
        messages.put("term.label", "Plazo");
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "plazo1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "plazo2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "plazo3"});
        messages.put("term.option.plazo1","180");
        messages.put("term.option.plazo2","270");
        messages.put("term.option.plazo3","360");

        /**** Insert field selector - Depósito ****/
        idField = "deposit";        
        formFieldsValues = new String[]{idField, idForm, version, "selector", "11", "TRUE", "TRUE",null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as" },
                new String[] { idField, idForm, version, "field-normal", "0", "combo" });
        messages.put("deposit.label", "Depósito");
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "valor1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{idField, idForm, version, "valor2"});
        messages.put("deposit.option.valor1","10%");
        messages.put("deposit.option.valor2","30%");
        
        /**** Insert field text - Tasa ****/
        idField = "rate";
        formFieldsValues = new String[] { idField, idForm, version, "text", "12", "TRUE", "TRUE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "display_type", "min_length", "max_length", "id_validation" }, new String[] { idField, idForm, version, "field-smaller", "0", "200", null});
        messages.put("rate.label","Tasa");
        
        /**** Insert field text - Pais destino ****/
        idField = "destinationCountry";
        formFieldsValues = new String[] { idField, idForm, version, "countrylist", "13", "TRUE", "TRUE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_countrylist",  new String[]{"id_field", "id_form", "form_version", "display_type", "default_value"}, new String[]{idField, idForm, version, "field-normal", "UY"});
        messages.put("destinationCountry.label","País destino");
        
        /**** Insert field termsandconditions - Disclaimer ****/
        idField = "disclaimer";
        formFieldsValues = new String[] { idField, idForm, version, "termsandconditions", "14", "TRUE", "FALSE", null, "default", "0", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label" }, new String[] { idField, idForm, version, "field-medium", "0", "1" });
        messages.put("disclaimer.label","Disclaimer");
        messages.put("disclaimer.termsAndConditions", "Sujeto a aprobación crediticia del banco.");
        
        /**** Insert field emaillist - Emails de notificación ****/
        idField = "emailsListNotification";
        formFieldsValues = new String[] { idField, idForm, version, "emaillist", "15", "TRUE", "FALSE", null, "default", "1", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_emaillist", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { idField, idForm, version, "field-normal" });
        messages.put("emailsListNotification.help","E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");
        messages.put("emailsListNotification.label","Emails de notificación");
        
        /**** Insert field textarea - Cuerpo de notificación ****/
        idField = "notificationBody";
        formFieldsValues = new String[] { idField, idForm, version, "textarea", "16", "TRUE", "FALSE", null, "default", "1", "0", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { idField, idForm, version, "0", "500", "field-medium" });
        messages.put("notificationBody.label", "Cuerpo de notificación");

        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }  
    }
}