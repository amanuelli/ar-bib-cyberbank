/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 * Issue 9838: Plataforma de procesamiento de SMS
 */
public class DB201106211647 extends DBVSUpdate {

    @Override
    public void up() {
        // MESSAGES
        
        // permissions_credentials_groups
        String[] fieldNames = new String[]{"id_permission", "id_credential_group"};
        String[] fieldValues = new String[]{"rub.accounts.transferInternal", "sms"};
        insert("permissions_credentials_groups", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.account.read", "sms"};
        insert("permissions_credentials_groups", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.creditcard.read", "sms"};
        insert("permissions_credentials_groups", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.loan.read", "sms"};
        insert("permissions_credentials_groups", fieldNames, fieldValues);

        // ACTIVITIES
        fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required"};
        fieldValues = new String[]{"rub.account.readAccountDetails", "1", "1", "com.technisys.rubicon.business.accounts.activities.ReadAccountDetailsActivity", "rub.account.read"};
        insert("activities", fieldNames, fieldValues);

        fieldValues = new String[]{"rub.loans.readLoanDetails", "1", "1", "com.technisys.rubicon.business.loans.activities.ReadLoanDetailsActivity", "rub.loan.read"};
        insert("activities", fieldNames, fieldValues);

        // CONFIGURATION
        fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};
        fieldValues = new String[]{"rubicon.sms.operations.T", "rub.accounts.transferInternal", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.sms.operations.CC", "rub.account.readAccountDetails", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.sms.operations.CT", "rub.creditcards.readCreditCard", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.sms.operations.CP", "rub.loans.readLoanDetails", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);
    }
}
