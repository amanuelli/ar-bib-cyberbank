/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author salva
 */
public class DB20150528_1139_380 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("accounts.details.statementsPerPage", "10", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
    }
}