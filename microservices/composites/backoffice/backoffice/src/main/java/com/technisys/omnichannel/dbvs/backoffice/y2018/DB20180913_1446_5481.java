/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5481
 *
 * @author dimoda
 */
public class DB20180913_1446_5481 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("environments.desktopLayout.corporate", "investments");
        updateConfiguration("environments.desktopLayout.retail", "accounts|creditCards|loans");
    }

}