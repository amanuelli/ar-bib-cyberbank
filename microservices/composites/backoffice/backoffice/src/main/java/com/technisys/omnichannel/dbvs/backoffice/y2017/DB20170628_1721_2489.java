/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author lcabero
 */
public class DB20170628_1721_2489 extends DBVSUpdate {

    @Override
    public void up() {

        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile" };
        String[] formFieldsMessages = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value" };
        String[] formFieldsValues;
        String idForm = "presentationOfDocumentsUnderCollection";
        String version = "1";

        /**** Insert Form - Presentación de documentos bajo cobranza ****/
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[] { "id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable" };
        String[] formsFieldsValues = new String[] { idForm, version, "1", "comex", "process", "demo:1:3", "comex", null, "1", "1", "1" };
        insert("forms", formsFields, formsFieldsValues);

        String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value", "modification_date" };
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version,lang,value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "','" + version + "','es',  'Presentación de documentos bajo cobranza',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "es", "Presentación de documentos bajo cobranza", date });
        }    
        delete("permissions", "id_permission = 'client.form." + idForm + ".send'");
        insert("permissions", new String[] { "id_permission" }, new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[] { "id_permission", "id_credential_group" }, new String[] { "client.form." + idForm + ".send", "accessToken-pin" });

        int ordinal = 1;

        /**** Insert field text - Nro. de factura ****/
        formFieldsValues = new String[] { "invoiceNumber", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "invoiceNumber", idForm, version, "0", "80", "field-big", null });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".invoiceNumber.label','es','invoiceNumber', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Nro. de factura')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".invoiceNumber.placeholder','es','invoiceNumber', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Nro. de factura')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".invoiceNumber.requiredError','es','invoiceNumber', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar un Nro. de factura')");
        }else{    
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".invoiceNumber.label", "es", "invoiceNumber", idForm, version, date, "Nro. de factura" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".invoiceNumber.placeholder", "es", "invoiceNumber", idForm, version, date, "Nro. de factura" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".invoiceNumber.requiredError", "es", "invoiceNumber", idForm, version, date, "Debe ingresar un Nro. de factura" });
        }    

        /**** Insert field amount - Importe de la cobranza ****/
        formFieldsValues = new String[] { "amountOfCollection", idForm, version, "amount", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[] { "id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount" }, new String[] { "amountOfCollection", idForm, version, "field-small", "0", "0" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".amountOfCollection.label','es','amountOfCollection', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Importe de la cobranza')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".amountOfCollection.placeholder','es','amountOfCollection', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Importe de la cobranza')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".amountOfCollection.invalidError','es','amountOfCollection', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'El importe no es válido')");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".amountOfCollection.label", "es", "amountOfCollection", idForm, version, date, "Importe de la cobranza" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".amountOfCollection.placeholder", "es", "amountOfCollection", idForm, version, date, "Importe de la cobranza" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".amountOfCollection.invalidError", "es", "amountOfCollection", idForm, version, date, "El importe no es válido" });
        }    
        /**** Insert field text - Nombre courier ****/
        formFieldsValues = new String[] { "courierName", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "courierName", idForm, version, "0", "30", "field-medium", null });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".courierName.label','es','courierName', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Nombre courier')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".courierName.help','es','courierName', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Enter the name of the courier with whom you have an account')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".courierName.placeholder','es','courierName', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Nombre courier')");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".courierName.label", "es", "courierName", idForm, version, date, "Nombre courier" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".courierName.help", "es", "courierName", idForm, version, date, "Enter the name of the courier with whom you have an account" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".courierName.placeholder", "es", "courierName", idForm, version, date, "Nombre courier" });
        }   
        /**** Insert field text - Nro. de cuenta de courier ****/
        formFieldsValues = new String[] { "courierAccountNumber", idForm, version, "text", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "0" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "courierAccountNumber", idForm, version, "0", "20", "field-normal", null });
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".courierAccountNumber.label','es','courierAccountNumber', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Nombre courier')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".courierAccountNumber.help','es','courierAccountNumber', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Enter the name of the courier with whom you have an account')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".courierAccountNumber.placeholder','es','courierAccountNumber', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Nombre courier')");
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".courierAccountNumber.label", "es", "courierAccountNumber", idForm, version, date, "Nro. de cuenta de courier" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".courierAccountNumber.help", "es", "courierAccountNumber", idForm, version, date, "Ingrese su número de cuenta" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".courierAccountNumber.placeholder", "es", "courierAccountNumber", idForm, version, date, "Nro. de cuenta de courier" });
        }   
        /**** Insert field textarea - Enviar la documentación al banco ****/
        formFieldsValues = new String[] { "sendTheDocumentationToTheBank", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { "sendTheDocumentationToTheBank", idForm, version, "0", "350", "field-big" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".sendTheDocumentationToTheBank.label','es','sendTheDocumentationToTheBank', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Enviar la documentación al banco')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".sendTheDocumentationToTheBank.placeholder','es','sendTheDocumentationToTheBank', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Enviar la documentación al banco')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".sendTheDocumentationToTheBank.requiredError','es','sendTheDocumentationToTheBank', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresarla dirección y el nombre completo del banco')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".sendTheDocumentationToTheBank.help','es','sendTheDocumentationToTheBank', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Ingrese dirección y nombre completo del banco')"); 
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".sendTheDocumentationToTheBank.label", "es", "sendTheDocumentationToTheBank", idForm, version, date, "Enviar la documentación al banco" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".sendTheDocumentationToTheBank.placeholder", "es", "sendTheDocumentationToTheBank", idForm, version, date, "Enviar la documentación al banco" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".sendTheDocumentationToTheBank.requiredError", "es", "sendTheDocumentationToTheBank", idForm, version, date, "Debe ingresarla dirección y el nombre completo del banco." });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".sendTheDocumentationToTheBank.help", "es", "sendTheDocumentationToTheBank", idForm, version, date, "Ingrese dirección y nombre completo del banco" });
        }   
        /**** Insert field textarea - Datos del girado ****/
        formFieldsValues = new String[] { "turnData", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { "turnData", idForm, version, "0", "350", "field-big" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".turnData.label','es','turnData', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Datos del girado')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".turnData.placeholder','es','turnData', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Datos del girado')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".turnData.requiredError','es','turnData', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar los datos del girado')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".turnData.help','es','turnData', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Ingrese dirección y nombre completo del girado')"); 
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".turnData.label", "es", "turnData", idForm, version, date, "Datos del girado" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".turnData.placeholder", "es", "turnData", idForm, version, date, "Datos del girado" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".turnData.requiredError", "es", "turnData", idForm, version, date, "Debe ingresar los datos del girado" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".turnData.help", "es", "turnData", idForm, version, date, "Ingrese dirección y nombre completo del girado" });
        }    
        /**** Insert field selector - Entrega docs. contra ****/
        formFieldsValues = new String[] { "deliveryDocsAgainst", idForm, version, "selector", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as" }, new String[] { "deliveryDocsAgainst", idForm, version, "field-normal", "payment", "0", "combo" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "deliveryDocsAgainst", idForm, version, "payment" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "deliveryDocsAgainst", idForm, version, "acceptanceOfLetter" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".deliveryDocsAgainst.option.acceptanceOfLetter','es','deliveryDocsAgainst', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Aceptación de letra')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".deliveryDocsAgainst.option.payment','es','deliveryDocsAgainst', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Pago')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".deliveryDocsAgainst.label','es','deliveryDocsAgainst', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Entrega docs. contra')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".deliveryDocsAgainst.placeholder','es','deliveryDocsAgainst', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Entrega docs. contra')"); 
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".deliveryDocsAgainst.option.acceptanceOfLetter", "es", "deliveryDocsAgainst", idForm, version, date, "Aceptación de letra" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".deliveryDocsAgainst.option.payment", "es", "deliveryDocsAgainst", idForm, version, date, "Pago" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".deliveryDocsAgainst.label", "es", "deliveryDocsAgainst", idForm, version, date, "Entrega docs. contra" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".deliveryDocsAgainst.placeholder", "es", "deliveryDocsAgainst", idForm, version, date, "Entrega docs. contra" });
        }   
        /**** Insert field text - Condiciones contra entrega ****/
        formFieldsValues = new String[] { "conditionsAgainstDelivery", idForm, version, "text", String.valueOf(ordinal++), "value(deliveryDocsAgainst) == 'acceptanceOfLetter'", "value(deliveryDocsAgainst) == 'acceptanceOfLetter'", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "conditionsAgainstDelivery", idForm, version, "0", "50", "field-big", null });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".conditionsAgainstDelivery.label','es','conditionsAgainstDelivery', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Condiciones contra entrega')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".conditionsAgainstDelivery.placeholder','es','conditionsAgainstDelivery', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Condiciones contra entrega')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".conditionsAgainstDelivery.requiredError','es','conditionsAgainstDelivery', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Debe ingresar un valor')");           
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".conditionsAgainstDelivery.label", "es", "conditionsAgainstDelivery", idForm, version, date, "Condiciones contra entrega" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".conditionsAgainstDelivery.placeholder", "es", "conditionsAgainstDelivery", idForm, version, date, "Condiciones contra entrega" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".conditionsAgainstDelivery.requiredError", "es", "conditionsAgainstDelivery", idForm, version, date, "Debe ingresar un valor" });
        }   
        /**** Insert field selector - Gastos en el exterior ****/
        formFieldsValues = new String[] { "expensesAbroad", idForm, version, "selector", String.valueOf(ordinal++), "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as" }, new String[] { "expensesAbroad", idForm, version, "field-normal", "onAccountOfTheRotated", "0", "combo" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "expensesAbroad", idForm, version, "onAccountOfTheRotated" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "expensesAbroad", idForm, version, "onOurBehalf" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".expensesAbroad.option.onAccountOfTheRotated','es','expensesAbroad', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Por cuenta del girado')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".expensesAbroad.option.onOurBehalf','es','expensesAbroad', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Por nuestra cuenta')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".expensesAbroad.label','es','expensesAbroad', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Gastos en el exterior')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".expensesAbroad.placeholder','es','expensesAbroad', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Gastos en el exterior')"); 
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".expensesAbroad.option.onAccountOfTheRotated", "es", "expensesAbroad", idForm, version, date, "Por cuenta del girado" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".expensesAbroad.option.onOurBehalf", "es", "expensesAbroad", idForm, version, date, "Por nuestra cuenta" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".expensesAbroad.label", "es", "expensesAbroad", idForm, version, date, "Gastos en el exterior" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".expensesAbroad.placeholder", "es", "expensesAbroad", idForm, version, date, "Gastos en el exterior" });
        }   
        /**** Insert field selector - Instrucciones de protesto ****/
        formFieldsValues = new String[] { "instructionsProtesto", idForm, version, "selector", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as" }, new String[] { "instructionsProtesto", idForm, version, "field-normal", null, "1", "combo" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "instructionsProtesto", idForm, version, "forNonPaymentAndNonAcceptance" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "instructionsProtesto", idForm, version, "notPaid" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "instructionsProtesto", idForm, version, "notAccepted" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".instructionsProtesto.option.forNonPaymentAndNonAcceptance','es','instructionsProtesto', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Por no pago y por no aceptación')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".instructionsProtesto.option.notAccepted','es','instructionsProtesto', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Por no aceptación')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".instructionsProtesto.option.notPaid','es','instructionsProtesto', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Por no pago')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".instructionsProtesto.label','es','instructionsProtesto', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Instrucciones de protesto')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".instructionsProtesto.placeholder','es','instructionsProtesto', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Instrucciones de protesto')"); 
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".instructionsProtesto.option.forNonPaymentAndNonAcceptance", "es", "instructionsProtesto", idForm, version, date, "Por no pago y por no aceptación" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".instructionsProtesto.option.notAccepted", "es", "instructionsProtesto", idForm, version, date, "Por no aceptación" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".instructionsProtesto.option.notPaid", "es", "instructionsProtesto", idForm, version, date, "Por no pago" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".instructionsProtesto.label", "es", "instructionsProtesto", idForm, version, date, "Instrucciones de protesto" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".instructionsProtesto.placeholder", "es", "instructionsProtesto", idForm, version, date, "Instrucciones de protesto" });
        }    
        /**** Insert field selector - Envío discrepancias ****/
        formFieldsValues = new String[] { "shippingDiscrepancies", idForm, version, "selector", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as" }, new String[] { "shippingDiscrepancies", idForm, version, "field-normal", null, "0", "check" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "shippingDiscrepancies", idForm, version, "si" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "shippingDiscrepancies", idForm, version, "no" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".shippingDiscrepancies.option.si','es','shippingDiscrepancies', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Si')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".shippingDiscrepancies.option.no','es','shippingDiscrepancies', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'No')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".shippingDiscrepancies.label','es','shippingDiscrepancies', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Envío discrepancias')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".shippingDiscrepancies.placeholder','es','shippingDiscrepancies', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Envío discrepancias')"); 
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".shippingDiscrepancies.option.si", "es", "shippingDiscrepancies", idForm, version, date, "Si" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".shippingDiscrepancies.option.no", "es", "shippingDiscrepancies", idForm, version, date, "No" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".shippingDiscrepancies.label", "es", "shippingDiscrepancies", idForm, version, date, "Envío discrepancias" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".shippingDiscrepancies.placeholder", "es", "shippingDiscrepancies", idForm, version, date, "Envío discrepancias" });
        }   
        /**** Insert field textarea - Observaciones ****/
        formFieldsValues = new String[] { "observations", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { "observations", idForm, version, "0", "350", "field-big" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".observations.label','es','observations', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Observaciones')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".observations.placeholder','es','observations', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Observaciones')"); 
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".observations.label", "es", "observations", idForm, version, date, "Observaciones" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".observations.placeholder", "es", "observations", idForm, version, date, "Observaciones" });
        }    
        /**** Insert field selector - Disclaimer ****/
        formFieldsValues = new String[] { "disclaimer", idForm, version, "termsandconditions", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label" }, new String[] { "disclaimer", idForm, version, "field-big", "0", "0" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".disclaimer.label','es','disclaimer', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),' ')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".disclaimer.termsAndConditions','es','disclaimer', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Las instrucciones enviadas al banco por usted luego de las 18:00hs. serán procesadas al siguiente día hábil bancario. Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar. Su instrucción quedará completada una vez recibidos la totalidad de los documentos correspondientes en nuestras sucursales y se haya enviado la presente debidamente firmada.')"); 
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".disclaimer.label", "es", "disclaimer", idForm, version, date, " " });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".disclaimer.termsAndConditions", "es", "disclaimer", idForm, version, date,
                "Las instrucciones enviadas al banco por usted luego de las 18:00hs. serán procesadas al siguiente día hábil bancario. Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar. Su instrucción quedará completada una vez recibidos la totalidad de los documentos correspondientes en nuestras sucursales y se haya enviado la presente debidamente firmada." });
        }    
        /**** Insert field horizontalrule - line ****/
        formFieldsValues = new String[] { "line", idForm, version, "horizontalrule", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".line.label','es','disclaimer', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Emails de notificación')");
        }else{    
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".line.label", "es", "disclaimer", idForm, version, date, " " });
        }    
        /**** Insert field emaillist - Condiciones contra entrega ****/
        formFieldsValues = new String[] { "notificationEmails", idForm, version, "emaillist", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_emaillist", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { "notificationEmails", idForm, version, "field-medium" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".notificationEmails.label','es','notificationEmails', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Emails de notificación')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".notificationEmails.placeholder','es','notificationEmails', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Emails de notificación')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".notificationEmails.help','es','notificationEmails', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.')");           
        }else{
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".notificationEmails.label", "es", "notificationEmails", idForm, version, date, "Emails de notificación" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".notificationEmails.placeholder", "es", "notificationEmails", idForm, version, date, "Emails de notificación" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".notificationEmails.help", "es", "notificationEmails", idForm, version, date, "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma." });
        }   
        /**** Insert field text - Cuerpo de notificación ****/
        formFieldsValues = new String[] { "notificationBody", idForm, version, "textarea", String.valueOf(ordinal++), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { "notificationBody", idForm, version, "0", "350", "field-big" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".notificationBody.label','es','notificationBody', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Cuerpo de notificación')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".notificationBody.placeholder','es','notificationBody', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Cuerpo de notificación')");
                       
        }else{
            
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".notificationBody.label", "es", "notificationBody", idForm, version, date, "Cuerpo de notificación" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + ".notificationBody.placeholder", "es", "notificationBody", idForm, version, date, "Cuerpo de notificación" });
        }    
    }
}