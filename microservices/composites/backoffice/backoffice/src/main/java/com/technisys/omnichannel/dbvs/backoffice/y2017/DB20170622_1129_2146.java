/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.domain.InvitationCode;
import java.sql.Types;

/**
 * Related issue: MANNAZCA-2146
 *
 * @author fpena
 */
public class DB20170622_1129_2146 extends DBVSUpdate {

    @Override
    public void up() {

        createTable("onboardings", new ColumnDefinition[]{
            new ColumnDefinition("id_onboarding", Types.INTEGER, 0, 0, false, null, true),
            new ColumnDefinition("creation_date", Types.DATE, 0, 0, false, null),
            new ColumnDefinition("modification_date", Types.DATE, 0, 0, false, null),
            new ColumnDefinition("status", Types.VARCHAR, 50, 0, false, "STARTED"),
            new ColumnDefinition("id_invitation_code", Types.INTEGER, 0, 0, true, null),
            new ColumnDefinition("extra_info", Types.VARCHAR, 3000, 0, true, null),
            new ColumnDefinition("document_country", Types.VARCHAR, 2, 0, true, null),
            new ColumnDefinition("document_type", Types.VARCHAR, 10, 0, true, null),
            new ColumnDefinition("document_number", Types.VARCHAR, 45, 0, true, null),
            new ColumnDefinition("mobile_number", Types.BIGINT, 0, 0, true, null),
            new ColumnDefinition("email", Types.VARCHAR, InvitationCode.EMAIL_MAX_LENGTH, 0, true, null)
        }, new String[]{"id_onboarding"});
        
        createForeignKey("onboardings", "fk_onboard_inv_codes", new String[]{"id_invitation_code"}, "invitation_codes", new String[]{"id"}, false, false);
        
        createTable("onboarding_documents", new ColumnDefinition[]{
            new ColumnDefinition("id_document", Types.INTEGER, 0, 0, false, null, true),
            new ColumnDefinition("id_onboarding", Types.INTEGER, 0, 0, false, null),
            new ColumnDefinition("content", Types.LONGVARBINARY, 0, 0, false, null),
            new ColumnDefinition("document_type", Types.VARCHAR, 50, 0, false, null),
            
        }, new String[]{"id_document"});
        
        createForeignKey("onboarding_documents", "fk_onboard_doc_onboard", new String[]{"id_onboarding"}, "onboardings", new String[]{"id_onboarding"}, true, true);
        
    }

}
