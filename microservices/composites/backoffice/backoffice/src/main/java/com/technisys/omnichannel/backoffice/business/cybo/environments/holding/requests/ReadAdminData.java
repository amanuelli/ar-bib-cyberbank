/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.environments.holding.requests;

import com.technisys.omnichannel.core.IBRequest;

public class ReadAdminData extends IBRequest {

    private String adminDocumentCountry;
    private String adminDocumentType;
    private String adminDocumentNumber;

    public String getAdminDocumentCountry() {
        return adminDocumentCountry;
    }

    public void setAdminDocumentCountry(String adminDocumentCountry) {
        this.adminDocumentCountry = adminDocumentCountry;
    }

    public String getAdminDocumentType() {
        return adminDocumentType;
    }

    public void setAdminDocumentType(String adminDocumentType) {
        this.adminDocumentType = adminDocumentType;
    }

    public String getAdminDocumentNumber() {
        return adminDocumentNumber;
    }

    public void setAdminDocumentNumber(String adminDocumentNumber) {
        this.adminDocumentNumber = adminDocumentNumber;
    }

}
