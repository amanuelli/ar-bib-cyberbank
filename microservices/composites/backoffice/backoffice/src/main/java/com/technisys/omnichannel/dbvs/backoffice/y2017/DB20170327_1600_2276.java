/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 *
 * @author jbaccino
 */
public class DB20170327_1600_2276 extends DBVSUpdate {

    @Override
    public void up() {
        insert("form_field_types", new String[]{"id_type"}, new String[]{"paycreditcardamount"});

        //creo la tabla form_field_cc_amount (para el campo de tipo monto para pago de tarjeta)
        ColumnDefinition[] columns = new ColumnDefinition[]{
            new ColumnDefinition("id_field", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("id_form", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("form_version", Types.INTEGER, 11, 0, false, null),
            new ColumnDefinition("display_type", Types.VARCHAR, 50, 0, true, null),
            new ColumnDefinition("id_cc_selector", Types.VARCHAR, 50, 0, true, null),
            new ColumnDefinition("control_limits", Types.BOOLEAN, 0, 0, false, "0"),
            new ColumnDefinition("control_limit_over_product", Types.VARCHAR, 50, 0, true, null)
        };
        createTable("form_field_cc_amount", columns, new String[]{"id_field", "id_form", "form_version"});

        //creo foreign key form_field_cc_amount(id_field, id_form) - form_fields(id_field, id_form)
        createForeignKey("form_field_cc_amount", "fk_ffcca_ff", new String[]{"id_field", "id_form", "form_version"}, "form_fields", new String[]{"id_field", "id_form", "form_version"}, true, true);

        //creo la tabla para monedas
        columns = new ColumnDefinition[]{
            new ColumnDefinition("id_field", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("id_form", Types.VARCHAR, 50, 0, false, null),
            new ColumnDefinition("form_version", Types.INTEGER, 11, 0, false, null),
            new ColumnDefinition("id_currency", Types.VARCHAR, 100, 0, false, null)
        };
        createTable("form_field_cc_amount_curr", columns, new String[]{"id_field", "id_form", "form_version", "id_currency"});

        //creo foreign key form_field_amount_currencies(id_field, id_form) - form_fields_amount(id_field, id_form)
        createForeignKey("form_field_cc_amount_curr", "fk_ffccac_ffcca", new String[]{"id_field", "id_form", "form_version"}, "form_field_cc_amount", new String[]{"id_field", "id_form", "form_version"}, true, true);

    }
}
