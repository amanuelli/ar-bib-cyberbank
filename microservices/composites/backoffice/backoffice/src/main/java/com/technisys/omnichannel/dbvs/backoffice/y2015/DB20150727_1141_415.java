/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150727_1141_415 extends DBVSUpdate {

    @Override
    public void up() {

        insertActivity("administration.environment.modify.pre", "com.technisys.omnichannel.client.activities.administration.environment.ModifyEnvironmentPreActivity", "administration.environment", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.environment.modify.preview", "com.technisys.omnichannel.client.activities.administration.environment.ModifyEnvironmentPreviewActivity", "administration.environment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.environment.modify.send", "com.technisys.omnichannel.client.activities.administration.environment.ModifyEnvironmentActivity", "administration.environment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

    }
}
