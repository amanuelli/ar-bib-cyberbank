package com.technisys.omnichannel.backoffice.domain.cybo.invitationcodes;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import com.technisys.omnichannel.annotations.docs.DocumentedType;
import com.technisys.omnichannel.annotations.docs.DocumentedTypeParam;

import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;

/**
 * Represents a CyBO invitation code.
 */
@DocumentedType
public class InvitationCode implements Serializable {
    
    @DocumentedTypeParam(description = "Invitation code ID")
    private int id;

    @DocumentedTypeParam(description = "Firstname")
    private String firstName;

    @DocumentedTypeParam(description = "Last name")
    private String lastName;

    @DocumentedTypeParam(description = "Document country")
    private String documentCountry;

    @DocumentedTypeParam(description = "Document type")
    private String documentType;

    @DocumentedTypeParam(description = "Document number")
    private String documentNumber;

    @DocumentedTypeParam(description = "Email")
    private String email;

    @DocumentedTypeParam(description = "Mobile number")
    private String mobileNumber;

    @DocumentedTypeParam(description = "Language")
    private String language;

    @DocumentedTypeParam(description = "Creation date")
    private Date creationDate;

    @DocumentedTypeParam(description = "Status")
    private String status;

    @DocumentedTypeParam(description = "Expired")
    private Boolean expired;

    @DocumentedTypeParam(description = "Account ID")
    private String productGroupId;

    @DocumentedTypeParam(description = "Account Name")
    private String productGroupName;

    @DocumentedTypeParam(description = "Invitation code masked")
    private String invitationCodeMasked;

    @DocumentedTypeParam(description = "Access type")
    private String accessType;

    @DocumentedTypeParam(description = "Account")
    private String account;

    @DocumentedTypeParam(description = "Account name")
    private String accountName;

    @DocumentedTypeParam(description = "Schema")
    private String schema;

    @DocumentedTypeParam(description = "Signature level")
    private Integer signatureLevel;

    public InvitationCode(com.technisys.omnichannel.core.domain.InvitationCode invitationCode) {
        this.id = invitationCode.getId();
        this.firstName = invitationCode.getFirstName();
        this.lastName = invitationCode.getLastName();
        this.documentCountry = invitationCode.getDocumentCountry();
        this.documentType = invitationCode.getDocumentType();
        this.documentNumber = invitationCode.getDocumentNumber();
        this.email = invitationCode.getEmail();
        this.mobileNumber = invitationCode.getMobileNumber();
        this.language = invitationCode.getLang();
        this.creationDate = invitationCode.getCreationDate();
        this.status = invitationCode.getStatus();
        this.expired = invitationCode.isExpired();
        this.productGroupId = invitationCode.getProductGroupId();
        this.productGroupName = invitationCode.getProductGroupName();
        this.invitationCodeMasked = invitationCode.getInvitationCodeMasked();
        
        this.accessType = invitationCode.getAccessType();

        this.account = invitationCode.getProductGroupId();
        this.accountName = invitationCode.getProductGroupName();

        this.schema = invitationCode.getAdministrationScheme();
        this.signatureLevel = invitationCode.getSignatureQty();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getDocumentCountry() {
        return documentCountry;
    }

    public void setDocumentCountry(String documentCountry) {
        this.documentCountry = documentCountry;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getExpirationDate() {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(creationDate);
        calendar.add(Calendar.MILLISECOND, ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "client.enrollment.invitationCode.validity").intValue());

        return calendar.getTime();
    }

    public String getStatus() {
        return status;
    }

    public Boolean getExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductGroupId() {
        return productGroupId;
    }

    public void setProductGroupId(String productGroupId) {
        this.productGroupId = productGroupId;
    }

    public String getProductGroupName() {
        return productGroupName;
    }

    public void setProductGroupName(String productGroupName) {
        this.productGroupName = productGroupName;
    }

    public String getInvitationCodeMasked() {
        return invitationCodeMasked;
    }

    public void setInvitationCodeMasked(String invitationCodeMasked) {
        this.invitationCodeMasked = invitationCodeMasked;
    }

    public String getAccessType() {
        return accessType;
    }

    public void setAccessType(String accessType) {
        this.accessType = accessType;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public Integer getSignatureLevel() {
        return signatureLevel;
    }

    public void setSignatureLevel(Integer signatureLevel) {
        this.signatureLevel = signatureLevel;
    }

    public String getSchema() {
        return schema;
    }

    public void setSchema(String schema) {
        this.schema = schema;
    }
}
