/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author fpena
 */
public class DB20151006_1343_952 extends DBVSUpdate {

    @Override
    public void up() {
        //Cambio del formulario de solicitud de chequera para que sea de actividad
        delete("permissions", "id_permission='client.form.requestCheckbook.send'");
        
        deleteActivity("request.checkbook.preview");
        insertActivity("request.checkbook.preview", "com.technisys.omnichannel.activities.other.PreviewFormActivity", "requestCheckbook", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        deleteActivity("request.checkbook.send");
        insertActivity("request.checkbook.send", "com.technisys.omnichannel.client.activities.requests.checkbook.RequestCheckbookSendActivity", "requestCheckbook", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Transactional, null);
        
        String[] fieldNames = new String[]{"id_activity", "id_field", "id_permission"};
        String[] fieldValues = new String[]{"request.checkbook.send", "producto", "accounts.requestCheckbook"};
        insert("activity_products", fieldNames, fieldValues);
        
        update("forms", new String[]{"type", "id_jbpm_process", "id_activity"}, new String[]{"activity", null, "request.checkbook.send"}, "id_form='requestCheckbook'");
        
        insertOrUpdateConfiguration("request.checkbook.jbpmProcessKey", "demo-1", null, "requestCheckboook", new String[]{"notEmpty"});
    }
}
