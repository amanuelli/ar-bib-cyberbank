/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5531
 *
 * @author marcelobruno
 */
public class DB20180924_0952_5531 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"value"}, new String[]{"portfolio"}, "id_field='environments.desktopLayout.corporate'");
        update("configuration", new String[]{"value"}, new String[]{"${BASE_URL}/enrollment"}, "id_field='administration.users.invite.enrollmentUrl'");
    }

}