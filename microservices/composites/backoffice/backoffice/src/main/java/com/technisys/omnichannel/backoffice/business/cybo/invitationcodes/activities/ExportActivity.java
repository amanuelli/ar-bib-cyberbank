/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.cybo.ExportRequest;
import com.technisys.omnichannel.backoffice.business.cybo.Response;
import com.technisys.omnichannel.backoffice.domain.cybo.PdfExporter;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.function.Function;

/**
 *
 * @author Mauricio Uribe
 */
public class ExportActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        Response<String> response = new Response<>(request);
        String lang = StringUtils.isNotBlank(request.getLang()) ? request.getLang() : ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM, "core.default.lang", "en");

        try {
            ExportRequest exportRequest = (ExportRequest) request;
            String fullOrderBy = Objects.nonNull(exportRequest.getOrderBy()) ? exportRequest.getOrderBy().concat(" ").concat(exportRequest.getOrderDirection()) : null;
            int maxRowsExport = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "backoffice.maxRowsExport");

            Map<String, Object> filters = new ObjectMapper().readValue(exportRequest.getFilters(), Map.class);

            Date _creationDateFrom = (RequestParamsUtils.getValue(filters, "creationDateFrom", Date.class));
            Date _creationDateTo = (RequestParamsUtils.getValue(filters, "creationDateTo", Date.class));
            Date creationDateFrom = (_creationDateFrom != null) ? DateUtils.trunc(_creationDateFrom, ChronoUnit.DAYS) : null;
            Date creationDateTo = (_creationDateTo != null) ? DateUtils.changeTime(_creationDateTo, 23, 59, 59, 998) : null;
            String productGroupId = (RequestParamsUtils.getValue(filters, "productGroupId", String.class));
            String email = (RequestParamsUtils.getValue(filters, "email", String.class));
            String documentNumber = (RequestParamsUtils.getValue(filters, "documentNumber", String.class));
            String status = (RequestParamsUtils.getValue(filters, "status", String.class));
            String mobileNumber = (RequestParamsUtils.getValue(filters, "mobileNumber", String.class));
            String productGroupName = (RequestParamsUtils.getValue(filters, "productGroupName", String.class));

            PaginatedList<InvitationCode> list = InvitationCodesHandler.listInvitationCodes(null,
                    productGroupId, null, null, documentNumber, email,
                    creationDateFrom, creationDateTo, status,
                    mobileNumber, 0, maxRowsExport, fullOrderBy, productGroupName);

            if(!list.getElementList().isEmpty()) {
                byte[] file;
                I18n i18n = I18nFactory.getHandler();
                Map<String, Function<InvitationCode, String>> valuesMap = new LinkedHashMap<>();
                valuesMap.put("Workspace", InvitationCode::getProductGroupName);
                valuesMap.put("Name", (invitationCode -> (StringUtils.defaultIfBlank(invitationCode.getFirstName(), "")  + " " + StringUtils.defaultIfBlank(invitationCode.getLastName(), "")).trim()));
                valuesMap.put("CustomerID", (invitationCode ->
                        invitationCode.getDocumentNumber() + " "
                        + i18n.getMessageRaw("documentType.label." + invitationCode.getDocumentType(), lang)));
                valuesMap.put("Email", InvitationCode::getEmail);
                valuesMap.put("MobileNumber", InvitationCode::getMobileNumber);
                valuesMap.put("CreationDate", invitationCode -> DateUtils.formatShortDate(invitationCode.getCreationDate(), lang));
                valuesMap.put("Status", InvitationCode::getStatusLabel);

                if (exportRequest.getFormat().equalsIgnoreCase("xlsx")) {
                    String[] columns = {
                            i18n.getMessageRaw("cybo.invitationCodesManagement.list.columns.productGroupName", lang),
                            i18n.getMessageRaw("cybo.invitationCodesManagement.list.columns.name", lang),
                            i18n.getMessageRaw("cybo.invitationCodesManagement.list.columns.document", lang),
                            i18n.getMessageRaw("cybo.invitationCodesManagement.list.columns.email", lang),
                            i18n.getMessageRaw("cybo.invitationCodesManagement.list.columns.mobileNumber", lang),
                            i18n.getMessageRaw("cybo.invitationCodesManagement.list.columns.creationDate", lang),
                            i18n.getMessageRaw("cybo.invitationCodesManagement.list.columns.status", lang),
                    };
                    String[] attributes = {"Workspace", "Name", "CustomerID", "Email", "MobileNumber", "CreationDate", "Status"};

                    file = UIUtils.toExcelFormat(lang, list.getElementList(), columns, attributes, "cybo.invitationCodesManagement.list.export.title", valuesMap);
                } else {
                    Map<String, String> headersMap = new LinkedHashMap<>();
                    headersMap.put("Workspace", "cybo.invitationCodesManagement.list.columns.productGroupName");
                    headersMap.put("Name", "cybo.invitationCodesManagement.list.columns.name");
                    headersMap.put("CustomerID", "cybo.invitationCodesManagement.list.columns.document");
                    headersMap.put("Email", "cybo.invitationCodesManagement.list.columns.email");
                    headersMap.put("MobileNumber", "cybo.invitationCodesManagement.list.columns.mobileNumber");
                    headersMap.put("CreationDate", "cybo.invitationCodesManagement.list.columns.creationDate");
                    headersMap.put("Status", "cybo.invitationCodesManagement.list.columns.status");

                    PdfExporter pdfExporter = new PdfExporter<>(headersMap, valuesMap, lang, "cybo.invitationCodesManagement.list.export.title");
                    file = pdfExporter.export(list.getElementList());
                }
                response.setData(Base64.getEncoder().encodeToString(file));
            }
            response.setReturnCode(ReturnCodes.OK);
        } catch (Exception ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            // To export
            if (!Authorization.hasBackofficePermission(request.getIdUser(), "backoffice.massiveData.allow")) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED, "User not authorized to export data");
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }
}
