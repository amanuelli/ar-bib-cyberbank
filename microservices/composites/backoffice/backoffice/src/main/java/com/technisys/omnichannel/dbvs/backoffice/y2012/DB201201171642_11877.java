/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2012;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ?
 */
public class DB201201171642_11877 extends DBVSUpdate {

    @Override
    public void up() {
        //keys de configuracion
        String[] fieldNames = new String[]{"id_field", "value", "possible_values", "id_group"};
        String[] fieldValues = new String[]{"rub.administration.invitation.validity", "48", "", "Rubicon"};
        insert("configuration", fieldNames, fieldValues);

    }
}