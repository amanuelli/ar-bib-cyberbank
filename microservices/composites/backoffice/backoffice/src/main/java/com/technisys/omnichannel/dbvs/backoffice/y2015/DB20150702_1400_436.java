/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author salva
 */
public class DB20150702_1400_436 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("widget.products.deposits.statementsPerPage", "3", ConfigurationGroup.NEGOCIO, "products", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("widget.products.loans.statementsPerPage", "3", ConfigurationGroup.NEGOCIO, "products", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("widget.products.creditCards.statementsPerPage", "3", ConfigurationGroup.NEGOCIO, "products", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("widget.products.accounts.statementsPerPage", "3", ConfigurationGroup.NEGOCIO, "products", new String[]{"notEmpty", "integer"});
    }
}
