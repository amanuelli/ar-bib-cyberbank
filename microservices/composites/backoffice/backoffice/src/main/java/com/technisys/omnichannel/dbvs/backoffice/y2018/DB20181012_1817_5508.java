package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5508
 *
 * @author mcheveste
 */
public class DB20181012_1817_5508 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.medium.modify.signature", "com.technisys.omnichannel.client.activities.administration.medium.ModifySignatureActivity", "administration", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

    }
}
