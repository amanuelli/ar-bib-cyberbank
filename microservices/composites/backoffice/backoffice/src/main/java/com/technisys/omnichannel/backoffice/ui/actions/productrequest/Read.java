/*
 *  Copyright 2019 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.productrequest;

import com.opensymphony.xwork2.ActionSupport;
import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.backoffice.business.productrequest.requests.ReadRequest;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.JSONException;
import com.technisys.omnichannel.backoffice.business.productrequest.response.ReadResponse;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.utils.JsonUtils;
import com.technisys.omnichannel.ReturnCodes;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 * @author iocampo
 */
@Action(results = {
    @Result(location = "/productrequest/_detail.jsp")
})
public class Read extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ReadRequest request = new ReadRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.productrequest.read");

        request.setIdOnboarding(id);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);
        if (!ReturnCodes.OK.equals(response.getReturnCode())) {
            throw new JSONException(response);
        }

        ReadResponse readResponse = (ReadResponse) response;
        onboarding = readResponse.getOnboarding();
        documents = readResponse.getDocuments();

        ExtraInfo extraInfo = JsonUtils.fromJson(onboarding.getExtraInfo(), ExtraInfo.class);

        if (extraInfo != null) {
            if(extraInfo.firstName != null){
                fullName = extraInfo.getFullName();
            }
            creditCard = extraInfo.getCreditCardId();
        }

        return SUCCESS;
    }
    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private int id;
    private int currentPage;
    private int rowsPerPage;
    private int totalRows;
    private int position;

    public void setId(int id) {
        this.id = id;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public void setRowsPerPage(int rowsPerPage) {
        this.rowsPerPage = rowsPerPage;
    }

    public void setTotalRows(int totalRows) {
        this.totalRows = totalRows;
    }

    public void setPosition(int position) {
        this.position = position;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private Onboarding onboarding;
    private java.util.List<String> documents;
    private String fullName;
    private String creditCard;

    public Onboarding getOnboarding() {
        return onboarding;
    }

    public java.util.List<String> getDocuments() {
        return documents;
    }

    public String getFullName() {
        return fullName;
    }

    public String getCreditCard() {
        return creditCard;
    }

    public int getId() {
        return id;
    }

    public int getCurrentPage() {
        return currentPage;
    }

    public int getRowsPerPage() {
        return rowsPerPage;
    }

    public int getTotalRows() {
        return totalRows;
    }

    public int getPosition() {
        return position;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>

    static class ExtraInfo {

        private String firstName;
        private String lastName;
        private String creditCardId;

        public String getCreditCardId() {
            return creditCardId;
        }

        public String getFullName() {
            return firstName + " " + lastName;
        }
    }
}
