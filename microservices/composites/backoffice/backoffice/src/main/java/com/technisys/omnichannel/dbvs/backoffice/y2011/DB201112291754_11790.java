/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Diego Curbelo
 */
public class DB201112291754_11790 extends DBVSUpdate {

    @Override
    public void up() {

        update("forms", new String[]{"description"},
                new String[]{"Este formulario sirve de inicio del tramite de solicitud de préstamo de una persona física. La lista de préstamos disponibles se obtiene de una tabla del sistema que el Banco deberá mantener a través del Backoffice de Omnichannel. No se incluyen productos especiales o formularios especiales para persona jurídica. El formulario se recibe en el Backoffice de Omnichannel a través de un flujo de trabajo centralizado."},
                "id_form = 12");

        update("forms", new String[]{"description"},
                new String[]{"Obtenga para su empresa la línea de pre-financiación necesaria para financiar sus exportaciones. Esta financiación puede otorgarse dentro o fuera de la circular 1456 del BCU obteniendo así los beneficios que el Estado otorga a exportadores."},
                "id_form = 68");

        update("forms", new String[]{"description"},
                new String[]{"Le asesoramos y le brindamos servicio integral para la administración de la cobranza de exportaciones de su empresa. Avisamos y acreditamos en el día la recepción de los fondos provenientes de sus cobranzas."},
                "id_form = 69");
    }
}