/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4918
 *
 * @author marcelobruno
 */
public class DB20180726_1501_4918 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_selector", new String[]{"default_value"}, new String[]{"lost"}, "id_form='lostOrStolenCreditCard' AND id_field='reason'");
    }

}