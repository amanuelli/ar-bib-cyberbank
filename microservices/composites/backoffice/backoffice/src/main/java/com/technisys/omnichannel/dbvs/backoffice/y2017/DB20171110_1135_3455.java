/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3448
 *
 * @author dimoda
 */
public class DB20171110_1135_3455 extends DBVSUpdate {

    @Override
    public void up() {
	
        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_message = 'fields.requestTransactionCancellation.date.help'");
        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_message = 'fields.requestTransactionCancellation.transactionType.help'");
        update("form_field_messages", new String[]{"value"}, new String[]{""}, "id_message = 'fields.requestTransactionCancellation.reference.help'");

        update("form_fields", new String[]{"read_only"}, new String[]{"1"}, "id_field = 'date'");
        update("form_fields", new String[]{"read_only"}, new String[]{"1"}, "id_field = 'reference'");
        update("form_fields", new String[]{"read_only"}, new String[]{"1"}, "id_field = 'transactionType'");
    
    }

}