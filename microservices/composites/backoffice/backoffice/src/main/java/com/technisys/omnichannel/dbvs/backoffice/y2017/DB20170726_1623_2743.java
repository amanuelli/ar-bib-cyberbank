/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 
 * @author szuliani
 *
 */
public class DB20170726_1623_2743 extends DBVSUpdate {

    @Override
    public void up() {

        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile" };
        String[] formFieldsReadOnly = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile", "read_only" };
        String[] formFieldsValues;
        String idForm = "editBondsAndGuarantees";
        String version = "1";

        int ordinal = 1;

        Map<String, String> messages = new HashMap();

        /**** Insert Form - Modificación de garantía y aval ****/
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[] { "id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable" };
        String[] formsFieldsValues = new String[] { idForm, version, "1", "comex", "process", "demo:1:3", "comex", null, "1", "1", "1" };
        insert("forms", formsFields, formsFieldsValues);

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE,
                    "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "', '" + version + "', 'es', 'Modificación de garantía y aval', TO_DATE('2017-07-25 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value", "modification_date" };
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "es", "Modificación de garantía y aval", date });
        }

        insert("permissions", new String[] { "id_permission" }, new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[] { "id_permission", "id_credential_group" }, new String[] { "client.form." + idForm + ".send", "accessToken-pin" });

        /**** Insert field text - Nro. de operación ****/
        formFieldsValues = new String[] { "operationNumber", idForm, version, "text", Integer.toString(ordinal), "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "operationNumber", idForm, version, "0", "30", "field-normal", null });
        messages.put("operationNumber.label", "Nro. de operación");
        messages.put("operationNumber.help", "Seleccione la operación para la que desea modificar");
        messages.put("operationNumber.requiredError", "Debe ingresar un Nro. de operación");
        ordinal++;

        /**** Insert field selector - Tipo de emisión ****/
        formFieldsValues = new String[] { "emissionType", idForm, version, "selector", Integer.toString(ordinal), "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as" }, new String[] { "emissionType", idForm, version, "field-normal", "1", "0", "combo" });
        messages.put("emissionType.label", "Tipo de emisión");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "emissionType", idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "emissionType", idForm, version, "2" });
        messages.put("emissionType.option.1", "Contra línea de crédito");
        messages.put("emissionType.option.2", "Contra prenda de depósito");
        ordinal++;

        /**** Insert field date - Nueva fecha de vencimiento ****/
        insert("form_fields", formFields, new String[] { "newDueDate", idForm, version, "date", Integer.toString(ordinal), "TRUE", "FALSE", "default", "1" });
        insert("form_field_date", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { "newDueDate", idForm, version, "field-small" });
        messages.put("newDueDate.label", "Nueva fecha de vencimiento");
        ordinal++;

        /**** Insert field selector - Incremento /Disminución ****/
        formFieldsValues = new String[] { "increaseDecreace", idForm, version, "selector", Integer.toString(ordinal), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as" }, new String[] { "increaseDecreace", idForm, version, "field-small", null, "1", "combo" });
        messages.put("increaseDecreace.label", "Incremento /Disminución");
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "increaseDecreace", idForm, version, "1" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { "increaseDecreace", idForm, version, "2" });
        messages.put("increaseDecreace.option.1", "Incremento");
        messages.put("increaseDecreace.option.2", "Disminución");
        ordinal++;

        /**** Insert field amount - Importe ****/
        formFieldsValues = new String[] { "amount", idForm, version, "amount", Integer.toString(ordinal), "value(increaseDecreace) != ''", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[] { "id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount" }, new String[] { "amount", idForm, version, "field-small", "0", "0" });
        messages.put("amount.label", "Importe");
        ordinal++;

        /**** Insert field textarea - Observaciones ****/
        insert("form_fields", formFields, new String[] { "observations", idForm, version, "textarea", Integer.toString(ordinal), "TRUE", "FALSE", "default", "1" });
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { "observations", idForm, version, "0", "1500", "field-big" });
        messages.put("observations.label", "Observaciones");
        ordinal++;

        /**** Insert field text - Adjuntos ****/
        formFieldsValues = new String[] { "attachments", idForm, version, "text", Integer.toString(ordinal), "TRUE", "FALSE", "default", "1", "1" };
        insert("form_fields", formFieldsReadOnly, formFieldsValues);
        insert("form_field_text", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation" }, new String[] { "attachments", idForm, version, "0", "500", "field-medium", null });
        messages.put("attachments.label", "Adjuntos");
        ordinal++;

        /**** Insert field terms and conditions - Disclaimer ****/
        formFieldsValues = new String[] { "disclaimer", idForm, version, "termsandconditions", Integer.toString(ordinal), "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label" }, new String[] { "disclaimer", idForm, version, "field-big", "0", "0" });
        messages.put("disclaimer.label", "Disclaimer");
        messages.put("disclaimer.termsAndConditions",
                "Las instrucciones enviadas al Banco por usted luego de las 17 hrs. serán procesadas al siguiente día hábil bancario. Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.\nSujeto a aprobación crediticia del Banco.\nSu instrucción quedará completada una vez recibidos la totalidad de los documentos correspondientes en nuestras sucursales y se haya enviado la presente debidamente firmada.");
        ordinal++;

        /**** Insert field line - Blank Line ****/
        insert("form_fields", formFields, new String[] { "blank", idForm, version, "horizontalrule", String.valueOf(ordinal), "TRUE", "FALSE", "default", "1" });
        messages.put("blank.label", " ");
        ordinal++;

        /**** Insert field emaillist - Emails de notificación ****/
        insert("form_fields", formFields, new String[] { "notificationEmails", idForm, version, "emaillist", String.valueOf(ordinal), "TRUE", "FALSE", "default", "1" });
        insert("form_field_emaillist", new String[] { "id_field", "id_form", "form_version", "display_type" }, new String[] { "notificationEmails", idForm, version, "field-big" });
        messages.put("notificationEmails.help", "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");
        messages.put("notificationEmails.label", "Emails de notificación");
        ordinal++;

        /**** Insert field textarea - Cuerpo de notificación ****/
        insert("form_fields", formFields, new String[] { "notificationBody", idForm, version, "textarea", Integer.toString(ordinal), "TRUE", "FALSE", "default", "1" });
        insert("form_field_textarea", new String[] { "id_field", "id_form", "form_version", "min_length", "max_length", "display_type" }, new String[] { "notificationBody", idForm, version, "0", "500", "field-big" });
        messages.put("notificationBody.label", "Cuerpo de notificación");
        ordinal++;

        String[] formFieldsMessages = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date" };

        for (String key : messages.keySet()) {
            formFieldsValues = new String[] { "fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date };

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) " + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3]
                        + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }

    }

}
