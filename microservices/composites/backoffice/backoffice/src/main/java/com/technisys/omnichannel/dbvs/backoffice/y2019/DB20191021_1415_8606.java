/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author Marcelo Bruno
 */

public class DB20191021_1415_8606 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("desktop.corporateGroup.accounts");
        insertActivity(
                "desktop.corporateGroup.accounts", "com.technisys.omnichannel.client.activities.desktop.CorporateGroupDesktopActivity",
                "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

    }

}