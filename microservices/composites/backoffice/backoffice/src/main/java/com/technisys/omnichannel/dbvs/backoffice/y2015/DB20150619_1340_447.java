/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author aelters
 */
public class DB20150619_1340_447 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("preferences.notifications.configuration.modify.pre", "com.technisys.omnichannel.client.activities.preferences.notificationsconfiguration.ModifyNotificationsConfigurationPreActivity","preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.notifications.configuration.modify.preview", "com.technisys.omnichannel.client.activities.preferences.notificationsconfiguration.ModifyNotificationsConfigurationPreviewActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.notifications.configuration.modify", "com.technisys.omnichannel.client.activities.preferences.notificationsconfiguration.ModifyNotificationsConfigurationActivity","preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
    }
}
