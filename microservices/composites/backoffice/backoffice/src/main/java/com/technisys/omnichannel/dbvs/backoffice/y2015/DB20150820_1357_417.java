/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author diegor
 */
public class DB20150820_1357_417 extends DBVSUpdate {

    @Override
    public void up() {
        insert("permissions", new String[]{"id_permission"}, new String[]{"accounts.requestCheckbook"});
        
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"accounts.requestCheckbook", "accessToken-otp"});
        
        insert("adm_ui_permissions",
                new String[]{"id_permission", "simple_id_category", "simple_id_subcategory", "simple_group", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_id_subcategory", "medium_group", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_id_subcategory", "advanced_group", "advanced_allow_prod_selection", "advanced_ordinal", "product_types", "auto_add_permissions", "environment_types"},
                new String[]{"accounts.requestCheckbook", "checkbooks", null, null, "0", "10", "checkbooks", null, null, "0", "10", "checkbooks", null, null, "1", "10", "CC", null, null});
        
        updateConfiguration("core.permissionsForProducts", "product.read|transfer.internal|transfer.thirdParties|pay.loan|pay.loan.thirdParties|accounts.requestCheckbook");

    }
}
