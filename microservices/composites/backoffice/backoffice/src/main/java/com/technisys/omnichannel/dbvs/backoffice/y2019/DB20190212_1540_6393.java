/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6393
 *
 * @author pbregonzio
 */
public class DB20190212_1540_6393 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fields = new String[] {"value"};

        String[] fieldValues = new String[] {"You\'ll need to upload a file, following a preset format with individual payment lines. Or you can input the payment list manually"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.introSection.hint' AND lang = 'en'");

        fieldValues = new String[] {"Necesitarás subir un archivo que respete alguno de los formatos en las líneas de pago individuales. O puedes ingresarlas manualmente"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.introSection.hint' AND lang = 'es'");

        fieldValues = new String[] {"Comment"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.reference.label' AND lang = 'en'");

        fieldValues = new String[] {"Comentario"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.reference.label' AND lang = 'es'");

        fieldValues = new String[] {"Payments list management"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.file.label' AND lang = 'en'");

        fieldValues = new String[] {"Manejo de líneas de pago"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.file.label' AND lang = 'es'");

        fieldValues = new String[] {"Your payment list file should have one of the following preset formats"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.sampleFile.label' AND lang = 'en'");

        fieldValues = new String[] {"Tu archivo para lista de pagos debe respetar uno de los siguientes formatos predeterminados"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.sampleFile.label' AND lang = 'es'");

        fieldValues = new String[] {"Write a comment for your reference"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.reference.placeholder' AND lang = 'en'");

        fieldValues = new String[] {"Escribe un comentario para tu referencia"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.reference.placeholder' AND lang = 'es'");
    }
}
