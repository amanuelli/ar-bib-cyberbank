/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.productrequest.requests;

import com.technisys.omnichannel.core.IBRequest;

/**
 *
 * @author iocampo
 */
public class ReadRequest extends IBRequest {

    private int idOnboarding;

    public int getIdOnboarding() {
        return idOnboarding;
    }

    public void setIdOnboarding(int idOnboarding) {
        this.idOnboarding = idOnboarding;
    }

    @Override
    public String toString() {
        return "ReadRequest{" + "idOnboarding=" + idOnboarding + '}';
    }
}
