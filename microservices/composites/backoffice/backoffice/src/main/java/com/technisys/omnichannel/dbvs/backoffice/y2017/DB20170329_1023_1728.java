/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * 
 * @author fpena
 */
public class DB20170329_1023_1728 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("backoffice.forms.categories", "accounts|deposits|loans|creditcards|payments|transfers|others", ConfigurationGroup.NEGOCIO, null, new String[]{});
        
    }
}
