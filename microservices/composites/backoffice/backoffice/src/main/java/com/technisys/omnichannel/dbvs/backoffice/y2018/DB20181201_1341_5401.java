/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5401
 *
 * @author isilveira
 */
public class DB20181201_1341_5401 extends DBVSUpdate {

    @Override
    public void up() {
        // Functional groups' data
        insert("feature_activities", new String[] { "id_feature", "id_activity" }, new String[] { "pay.own", "pay.creditcard.send" });
        insert("feature_activities", new String[] { "id_feature", "id_activity" }, new String[] { "pay.own", "pay.loan.send" });
        insert("feature_activities", new String[] { "id_feature", "id_activity" }, new String[] { "pay.thirdParties", "pay.thirdPartiesCreditCard.send" });
        insert("feature_activities", new String[] { "id_feature", "id_activity" }, new String[] { "pay.thirdParties", "pay.thirdPartiesLoan.send" });
        insert("feature_activities", new String[] { "id_feature", "id_activity" }, new String[] { "request", "request.checkbook.send" });
        insert("feature_activities", new String[] { "id_feature", "id_activity" }, new String[] { "transfers.foreign", "transfers.foreign.send" });
        insert("feature_activities", new String[] { "id_feature", "id_activity" }, new String[] { "transfers.local", "transfers.internal.send" });
        insert("feature_activities", new String[] { "id_feature", "id_activity" }, new String[] { "transfers.local", "transfers.local.send" });
        insert("feature_activities", new String[] { "id_feature", "id_activity" }, new String[] { "transfers.local", "transfers.thirdParties.send" });
        insert("feature_forms", new String[] { "id_feature", "id_form" }, new String[] { "request", "accountOpening" });
        insert("feature_forms", new String[] { "id_feature", "id_form" }, new String[] { "request", "additionalCreditCardRequest" });
        insert("feature_forms", new String[] { "id_feature", "id_form" }, new String[] { "request", "creditCardChangeCondition" });
        insert("feature_forms", new String[] { "id_feature", "id_form" }, new String[] { "request", "creditCardRequest" });
        insert("feature_forms", new String[] { "id_feature", "id_form" }, new String[] { "request", "requestLoan" });
        insert("feature_forms", new String[] { "id_feature", "id_form" }, new String[] { "request", "requestOfManagementCheck" });
    }
}