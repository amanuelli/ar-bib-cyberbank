/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Marcelo Bruno
 */

public class DB20200603_1850_11333 extends DBVSUpdate {

    @Override
    public void up() {
        executeScript(DBVS.DIALECT_ORACLE, "database/oracle-505-demo-general_condition_pdf_documents.sql", ";", "UTF-8");
    }

}