/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5042
 *
 * @author marcelobruno
 */
public class DB20180731_1547_5042 extends DBVSUpdate {

    @Override
    public void up() {
        
        //Cuentas
        update("form_messages", new String[]{"value"}, new String[]{"Solicitar apertura de cuenta"}, "id_form='accountOpening' AND id_message='forms.accountOpening.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Solicitar cheque gerencial"}, "id_form='requestOfManagementCheck' AND id_message='forms.requestOfManagementCheck.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Solicitar chequera"}, "id_form='requestCheckbook' AND id_message='forms.requestCheckbook.formName' AND lang='es'");
        
        //Tarjetas
        update("form_messages", new String[]{"value"}, new String[]{"Solicitar tarjeta de crédito adicional"}, "id_form='additionalCreditCardRequest' AND id_message='forms.additionalCreditCardRequest.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Solicitar cambio de condiciones"}, "id_form='creditCardChangeCondition' AND id_message='forms.creditCardChangeCondition.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Solicitar tarjeta de crédito"}, "id_form='creditCardRequest' AND id_message='forms.creditCardRequest.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Denunciar robo o extravío de tarjeta"}, "id_form='lostOrStolenCreditCard' AND id_message='forms.lostOrStolenCreditCard.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Solicitar reimpresión de tarjeta de crédito"}, "id_form='reissueCreditCard' AND id_message='forms.reissueCreditCard.formName' AND lang='es'");
        
        //Prestamos
        update("form_messages", new String[]{"value"}, new String[]{"Solicitar préstamo"}, "id_form='requestLoan' AND id_message='forms.requestLoan.formName' AND lang='es'");
        
        //Pagos
        update("form_messages", new String[]{"value"}, new String[]{"Pagar préstamo"}, "id_form='payLoan' AND id_message='forms.payLoan.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Pagar préstamo a un tercero"}, "id_form='payThirdPartiesLoan' AND id_message='forms.thirdPartiesLoan.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Pagar tarjeta"}, "id_form='payCreditCard' AND id_message='forms.payCreditCard.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Pagar tarjeta a un tercero"}, "id_form='payThirdPartiesCreditCard' AND id_message='forms.thirdPartiesCreditCard.formName' AND lang='es'");
        update("form_messages", new String[]{"value"}, new String[]{"Recargar celular"}, "id_form='mobilePrepaiment' AND id_message='forms.mobilePrepaiment.formName' AND lang='es'");
      
    }
    
}