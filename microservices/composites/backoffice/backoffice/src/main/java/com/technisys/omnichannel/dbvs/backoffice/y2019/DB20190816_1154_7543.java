/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author Jhossept Garay
 */

public class DB20190816_1154_7543 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("client.permissions.defaults", "core.authenticated|user.preferences|user.preferences.withSecondFactor|core.cancelTransaction|core.forms.send|core.listTransactions|core.loginWithOTP|core.loginWithPassword|core.loginWithPIN|core.moveToDraftTransaction|core.readForm|core.readTransaction");
    }

}