/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.productrequest.activities;

import com.technisys.omnichannel.backoffice.business.productrequest.requests.ReadRequest;
import com.technisys.omnichannel.backoffice.business.productrequest.response.ReadResponse;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.domain.OnboardingDocument;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.ReturnCodes;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author iocampo
 */
public class ReadActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        ReadResponse response = new ReadResponse(request);

        try {
            ReadRequest req = (ReadRequest) request;
            Onboarding onboarding = OnboardingHandlerFactory.getInstance().read(req.getIdOnboarding());
            List<String> documents = new ArrayList<>();
            OnboardingDocument front = OnboardingHandlerFactory.getInstance().readDocument(req.getIdOnboarding(), "DOCUMENT_FRONT");
            OnboardingDocument back = OnboardingHandlerFactory.getInstance().readDocument(req.getIdOnboarding(), "DOCUMENT_BACK");
            if (front != null) {
                documents.add(Base64.getEncoder().encodeToString(front.getContent()));
            }
            if (back != null) {
                documents.add(Base64.getEncoder().encodeToString(back.getContent()));
            }
            response.setOnboarding(onboarding);
            response.setDocuments(documents);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            ReadRequest req = (ReadRequest) request;
            if (OnboardingHandlerFactory.getInstance().read(req.getIdOnboarding()) == null) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }

}
