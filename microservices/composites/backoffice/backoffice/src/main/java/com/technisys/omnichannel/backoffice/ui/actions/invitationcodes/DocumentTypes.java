/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.ListResponse;
import com.technisys.omnichannel.backoffice.business.misc.request.ListDocumentTypesRequest;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.HTMLException;
import com.technisys.omnichannel.core.IBResponse;
import com.opensymphony.xwork2.ActionSupport;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 * @author Sebastian Barbosa
 */
@Action(results = {
    @Result(location = "/invitationcodes/_documentTypes.jsp")
})
public class DocumentTypes extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ListDocumentTypesRequest request = new ListDocumentTypesRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.misc.listDocumentTypes");

        request.setCountry(country);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        if (ReturnCodes.OK.equals(response.getReturnCode())) {
            ListResponse<String> auxResponse = (ListResponse<String>)response;
            documentTypeList = auxResponse.getItemList();

            return SUCCESS;
        } else {
            throw new HTMLException(response);
        }
    }

    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private String country;

    public void setCountry(String country) {
        this.country = country;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private java.util.List<String> documentTypeList;

    public java.util.List<String> getDocumentTypeList() {
        return documentTypeList;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
