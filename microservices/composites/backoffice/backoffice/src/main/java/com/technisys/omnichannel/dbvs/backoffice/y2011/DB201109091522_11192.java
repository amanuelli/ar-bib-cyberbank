/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201109091522_11192 extends DBVSUpdate {

    @Override
    public void up() {

        update("forms", new String[]{"template_es"}, new String[]{"<form:form>	<form:section>		<form:field idField='liquido'/>		<form:field idField='cantidadDeCuotas'/>		<form:field idField='cuentaDeDebitoDeCuota'/>		<form:field idField='destinoDeFondos'/>		<form:field idField='destinoDeFondosOtros'/>	</form:section><div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='termsAndConditions'/>	</form:section></form:form>"}, "id_form = 7");

        String[] fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value"};

        String[] fieldValues = new String[]{"termsAndConditions", "7", "disclaimer", "1", "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per."};
        insert("form_fields", fieldNames, fieldValues);

        update("forms", new String[]{"template_es"}, new String[]{"<form:form>	<form:section>		<form:field idField='moneda'/>		<form:field idField='plazo'/>		<form:field idField='destino'/>		<form:field idField='destinoOtros'/>	</form:section><div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='termsAndConditions'/>	</form:section></form:form>"}, "id_form = 5");

        fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value"};

        fieldValues = new String[]{"termsAndConditions", "5", "disclaimer", "1", "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per."};
        insert("form_fields", fieldNames, fieldValues);

        update("forms", new String[]{"template_es"}, new String[]{"<form:form>	<form:section>		<form:field idField='prestamoTipo'/>		<form:field idField='importe'/>		<form:field idField='cantidadDeCuotas'/>		<form:field idField='cuentaAutorizadaADebitar'/>		<form:field idField='destinoDeFondos'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>				<form:field idField='separadorHipotecario'/>		<form:field idField='afiliadoAFAP'/>		<form:field idField='tvCable'/>		<form:field idField='empresaTVCable'/>		<form:field idField='otrasFuentesDeIngreso'/>		<form:field idField='actividadesAnteriores'/>		<form:field idField='otrosTelefonosDeContacto'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>						<form:field idField='separadorDatosDelInmueble'/>		<form:field idField='padronNumero'/>		<form:field idField='tipoDeVivienda'/>		<form:field idField='areaDelTerreno'/>		<form:field idField='areaEdificada'/>		<form:field idField='departamento'/>		<form:field idField='localidad'/>		<form:field idField='barrio'/>		<form:field idField='seccionJudicial'/>		<form:field idField='calle'/>		<form:field idField='numeroDePuerta'/>		<form:field idField='entre'/>		<form:field idField='piso'/>		<form:field idField='apto'/>		<form:field idField='antiguedadConstruccion'/>		<form:field idField='valorProbableInteresado'/>		<form:field idField='descripcionDeComodidades'/>		<form:field idField='destinoDelInmueble'/>		<form:field idField='linderosAumentanRiesgo'/>		<form:field idField='ocupacionLinderos'/>		<form:field idField='techosParedesCombustibles'/>		<form:field idField='detalle'/>		<form:field idField='otrasDeudas'/>	</form:section><div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='termsAndConditions'/>	</form:section></form:form>"}, "id_form = 12");

        fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value"};

        fieldValues = new String[]{"termsAndConditions", "12", "disclaimer", "1", "Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per."};
        insert("form_fields", fieldNames, fieldValues);

    }
}