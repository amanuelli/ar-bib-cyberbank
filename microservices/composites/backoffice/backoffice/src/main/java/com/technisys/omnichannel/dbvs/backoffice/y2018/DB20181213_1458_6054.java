/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author isilveira
 */

public class DB20181213_1458_6054 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("multilinePayments.transactionLinesBatch.count", "100", ConfigurationGroup.TECNICAS, "multilinePayments", new String[] { "notEmpty", "integer" });
    }

}