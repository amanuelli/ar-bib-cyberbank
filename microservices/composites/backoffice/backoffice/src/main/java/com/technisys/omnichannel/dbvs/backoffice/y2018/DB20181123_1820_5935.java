package com.technisys.omnichannel.dbvs.backoffice.y2018;


import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5935
 *
 * @author Manuel Cheveste
 */
public class DB20181123_1820_5935 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("administration.groups.modify.send");
        deleteActivity("administration.groups.modify.pre");
        deleteActivity("administration.groups.modify.preview");

        deleteActivity("administration.groups.create.send");
        deleteActivity("administration.groups.create.pre");
        deleteActivity("administration.groups.create.preview");
    }
}