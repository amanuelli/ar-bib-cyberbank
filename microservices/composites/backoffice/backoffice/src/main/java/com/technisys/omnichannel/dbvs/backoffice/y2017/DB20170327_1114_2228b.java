package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;


/**
*
* @author danireb
*/
public class DB20170327_1114_2228b extends DBVSUpdate {
    
    
    @Override
    public void up() {
    
        ColumnDefinition column = new ColumnDefinition("product_group_id", Types.VARCHAR, 70, 0, true, null);
        
        addColumn("client_environments", column);
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB}, "UPDATE client_environments SET product_group_id=id_client");
        
        dropIndex("client_environments", "idx_ce_id_client");
        dropColumn("client_environments", "id_client");
        
        createIndex("client_environments", "idx_ce_product_group_id", new String[]{"product_group_id"});
        
    }

}
