/* 
 * Copyright 2017 Manentia Software. 
 * 
 * This software component is the intellectual property of Manentia Software S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * http://www.manentiasoftware.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3051
 *
 * @author ldurand
 */
public class DB20170905_1031_3051 extends DBVSUpdate {

    @Override
    public void up() {
        String badIdForm = "RequestOfManagementCheck";
        String idForm = "requestOfManagementCheck";

        update("form_fields", new String[]{"id_form"}, new String[]{idForm}, "id_form = '" + badIdForm + "' AND id_field = 'termsAndConditions'");
        update("form_field_ps_product_types", new String[]{"id_form"}, new String[]{idForm}, "id_form = '" + badIdForm + "' AND id_field = 'debitAccount'");
        update("form_field_selector_options", new String[]{"id_form"}, new String[]{idForm}, "id_form = '" + badIdForm + "' AND value='homeDelivery'");
        update("form_field_terms_conditions", new String[]{"show_label"}, new String[]{"1"}, "id_form = '" + idForm + "' AND id_field='termsAndConditions'");

        update("form_field_messages", new String[]{"id_message", "id_form"}, new String[]{"fields.requestOfManagementCheck.placeOfRetreat.option.homeDelivery", idForm}, "id_message = 'fields.RequestOfManagementCheck.placeOfRetreat.option.homeDelivery'");
        update("form_field_messages", new String[]{"id_message", "id_form"}, new String[]{"fields.requestOfManagementCheck.termsAndConditions.label", idForm}, "id_message = 'fields.RequestOfManagementCheck.termsAndConditions.label'");
        update("form_field_messages", new String[]{"id_message", "id_form"}, new String[]{"fields.requestOfManagementCheck.termsAndConditions.showAcceptOptionText", idForm}, "id_message = 'fields.RequestOfManagementCheck.termsAndConditions.showAcceptOptionText'");
        update("form_field_messages", new String[]{"id_message", "id_form"}, new String[]{"fields.requestOfManagementCheck.termsAndConditions.termsAndConditions", idForm}, "id_message = 'fields.RequestOfManagementCheck.termsAndConditions.termsAndConditions'");

    }

}
