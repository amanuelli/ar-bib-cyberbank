/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author diego
 */
public class DB20150914_1140_876 extends DBVSUpdate {

    @Override
    public void up() {
        StringBuilder configurationFields = new StringBuilder();
        configurationFields.append("'loans.statementsPerPage',");
        configurationFields.append("'creditCard.statementsPerPage',");
        configurationFields.append("'accounts.statementsPerPage',");
        configurationFields.append("'deposits.statementsPerPage',");
        configurationFields.append("'transactions.rowsPerPage'");
        
        update("configuration", new String[]{"id_sub_group"}, new String[]{""}, "id_field in (" + configurationFields + ")");
    }
}
