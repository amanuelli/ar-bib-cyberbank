/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6017
 *
 * @author lpoll
 */
public class DB20181127_1610_6017 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_text", new String[]{"id_validation"}, new String[]{"withoutSpecialChars"}, "id_field = 'creditReference' and id_form = 'transferInternal'");
        update("form_field_text", new String[]{"id_validation"}, new String[]{"withoutSpecialChars"}, "id_field = 'creditReference' and id_form = 'transferLocal'");
        update("form_field_text", new String[]{"id_validation"}, new String[]{"withoutSpecialChars"}, "id_field = 'debitReference' and id_form = 'transferForeign'");
        update("form_field_text", new String[]{"id_validation"}, new String[]{"withoutSpecialChars"}, "id_field = 'reference' and id_form = 'transferThirdParties'");
    }
}