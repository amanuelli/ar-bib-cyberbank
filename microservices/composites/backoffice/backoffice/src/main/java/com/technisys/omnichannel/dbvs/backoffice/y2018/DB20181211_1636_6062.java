/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6062
 *
 * @author Marcelo Bruno
 */
public class DB20181211_1636_6062 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("administration.environment.modify.send");
        deleteActivity("administration.environment.modify.pre");
        deleteActivity("administration.environment.modify.preview");
        deleteActivity("administration.users.modify.send");
        deleteActivity("administration.users.modify.pre");
        deleteActivity("administration.users.modify.preview");
    }

}