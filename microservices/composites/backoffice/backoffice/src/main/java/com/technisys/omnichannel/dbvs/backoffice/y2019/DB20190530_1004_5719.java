/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5719
 *
 * @author Mauricio Uribe
 */
public class DB20190530_1004_5719 extends DBVSUpdate {

    @Override
    public void up() {

        insertOrUpdateConfiguration("session.duration",
                "5m",
                DBVSUpdate.ConfigurationGroup.SEGURIDAD,
                "frontend",
                new String[]{"interval"});

        insertOrUpdateConfiguration("session.duration.ASSISTANT",
                "20d",
                DBVSUpdate.ConfigurationGroup.SEGURIDAD,
                "frontend",
                new String[]{"interval"});

        insertOrUpdateConfiguration("session.duration.FINGER",
                "10d",
                DBVSUpdate.ConfigurationGroup.SEGURIDAD,
                "frontend",
                new String[]{"interval"});

        insertOrUpdateConfiguration("session.duration.NORMAL.max",
                "8h",
                DBVSUpdate.ConfigurationGroup.SEGURIDAD,
                "frontend",
                new String[]{"notEmpty", "interval"});

    }
}
