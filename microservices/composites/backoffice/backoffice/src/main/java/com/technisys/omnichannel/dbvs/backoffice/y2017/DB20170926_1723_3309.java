/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3309
 *
 * @author dimoda
 */
public class DB20170926_1723_3309 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("nextExpirations.threshold.show", "90", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
    }

}