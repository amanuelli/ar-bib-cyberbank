/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor.AuditLevel;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor.Kind;

/**
 *
 * @author Diego Curbelo
 */
public class DB20151023_1250_1426 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("session.loginWithSecondFactorAndPassword.step1", "com.technisys.omnichannel.client.activities.session.LoginWithSecondFactorAndPasswordStep1Activity",
                "login", AuditLevel.Full, Kind.Other, null);
        
        insertActivity("session.loginWithSecondFactorAndPassword.step2", "com.technisys.omnichannel.client.activities.session.LoginWithSecondFactorAndPasswordStep2Activity",
                "login", AuditLevel.Full, Kind.Other, null);
        
        updateConfiguration("client.permissions.defaults", "core.authenticated|user.preferences|user.preferences.pin");

    }
}
