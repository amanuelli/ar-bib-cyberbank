/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author user
 */

public class DB20201021_1452_14318 extends DBVSUpdate {

    private final String FORM_FIELD_MESSAGES = "form_field_messages";
    private final String FORM_FIELD_BRANCHLIST = "form_field_branchlist";
    private final String[] FIELD_VALUE = new String[]{"value"};
    private final String[] DEFAULT_VALUE = new String[]{"default_value"};

    @Override
    public void up() {
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"El campo Domicilio no puede estar vacío"}, "id_form = 'requestOfManagementCheck' and id_field ='address' and lang='es' and id_message ='fields.requestOfManagementCheck.address.requiredError'");
        update(FORM_FIELD_BRANCHLIST, DEFAULT_VALUE, new String[]{null}, "id_form = 'requestOfManagementCheck' and id_field ='branchOffices'");
    }

}