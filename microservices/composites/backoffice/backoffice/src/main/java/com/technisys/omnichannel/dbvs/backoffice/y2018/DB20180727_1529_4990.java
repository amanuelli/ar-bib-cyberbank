/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4990
 *
 * @author dimoda
 */
public class DB20180727_1529_4990 extends DBVSUpdate {

    @Override
    public void up() {

        update("forms", new String[]{"category"}, new String[]{"comex"}, "id_form = 'authorizeDiscrepancyDocuments'");
        update("forms", new String[]{"deleted"}, new String[]{"1"}, "category = 'comex'");
        
    }

}