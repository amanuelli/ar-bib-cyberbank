package com.technisys.omnichannel.backoffice.domain.cybo.invitationcodes;

import java.io.Serializable;

import com.technisys.omnichannel.annotations.docs.DocumentedType;
import com.technisys.omnichannel.annotations.docs.DocumentedTypeParam;
import com.technisys.omnichannel.backoffice.domain.cybo.ReducedEnvironment;
import com.technisys.omnichannel.core.domain.Environment;

/**
 * Represents a CyBO client environment.
 */
@DocumentedType
public class ClientEnvironment implements Serializable {
    
    @DocumentedTypeParam(description = "Product group ID")
    private String id;
  
    @DocumentedTypeParam(description = "Account name")
    private String name;
    
    @DocumentedTypeParam(description = "Segment")
    private String segment;

    @DocumentedTypeParam(description = "Environment basic data")
    private ReducedEnvironment environment;

    public ClientEnvironment(com.technisys.omnichannel.client.domain.ClientEnvironment clientEnvironment, Environment environment) {
        this.id = clientEnvironment.getProductGroupId();
        this.name = clientEnvironment.getAccountName();
        this.segment = clientEnvironment.getSegment();
        if (environment != null) {
            this.environment = new ReducedEnvironment(environment);
        }
    }

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSegment() {
		return segment;
	}

	public void setSegment(String segment) {
		this.segment = segment;
	}

	public ReducedEnvironment getEnvironment() {
		return environment;
	}

	public void setEnvironment(ReducedEnvironment environment) {
		this.environment = environment;
	}

    
}
