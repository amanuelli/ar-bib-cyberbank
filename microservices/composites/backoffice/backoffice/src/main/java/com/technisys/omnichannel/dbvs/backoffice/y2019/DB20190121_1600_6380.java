/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author iocampo
 */
public class DB20190121_1600_6380 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("loans.downloadPayment", "com.technisys.omnichannel.client.activities.loans.DownloadPaymentActivity", "loans", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idLoan");
        insertOrUpdateConfiguration("loans.export.maxStatements", "100", ConfigurationGroup.NEGOCIO, null, new String[]{"notEmpty", "integer"});
    }
}
