/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5261
 *
 * @author lpoll
 */
public class DB20181105_1219_5261 extends DBVSUpdate {
    
    private void insertDocumentType(String documentType){
        customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                "INSERT INTO document_types (id_document_type) "
                + "SELECT * FROM (SELECT '" + documentType + "' as id_document_type) AS tmp WHERE NOT EXISTS "
                + "(SELECT id_document_type FROM document_types WHERE id_document_type = '" + documentType + "')");

        customSentence(new String[]{DBVS.DIALECT_ORACLE},
                "INSERT INTO document_types (id_document_type) "
                + "SELECT '" + documentType + "' FROM dual WHERE NOT EXISTS "
                + "(SELECT id_document_type FROM document_types WHERE id_document_type = '" + documentType + "')");
        
    }
    
    private void insertDocumentTypeCountryCode(String countryCode, String documentType, String mrtdDocumentType){
        insert("document_types_country_codes", new String[]{"id_country_code", "id_document_type", "mrtd_document_type"}, new String[]{countryCode, documentType, mrtdDocumentType});
    }
    
    private void insertDocumentTypeCountryCode(String countryCode, String documentType){
        insertDocumentTypeCountryCode(countryCode, documentType, null);
    }

    @Override
    public void up() {
        insertDocumentType("CI");
        insertDocumentType("DNI");
        insertDocumentType("PAS");
        insertDocumentType("RUT");
        
        delete("document_types_country_codes", null);
        customSentence(new String[]{DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_HSQLDB},
                " INSERT INTO document_types_country_codes (id_country_code, id_document_type, mrtd_document_type) "
                + " SELECT id_country_code, 'PAS', 'P' "
                + " FROM country_codes");
        
        insertDocumentTypeCountryCode("UY", "CI", "I");
        insertDocumentTypeCountryCode("UY", "RUT");
        insertDocumentTypeCountryCode("CL", "CI", "I");
        insertDocumentTypeCountryCode("CL", "RUT", "I");
        insertDocumentTypeCountryCode("AR", "DNI", "I");
        insertDocumentTypeCountryCode("CA", "DNI", "I");
    }

}