/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150907_1204_507 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("enrollment.associate.pre", "com.technisys.omnichannel.client.activities.enrollment.AssociatePreActivity", "enrollment", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("enrollment.associate.verifyStep1", "com.technisys.omnichannel.client.activities.enrollment.AssociateVerifyStep1Activity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("enrollment.associate.verifyStep2", "com.technisys.omnichannel.client.activities.enrollment.AssociateVerifyStep2Activity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);
        insertActivity("enrollment.associate.finish", "com.technisys.omnichannel.client.activities.enrollment.AssociateFinishActivity", "enrollment", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);
    }
}
