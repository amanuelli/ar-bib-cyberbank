/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author ?
 */
public class DB20150930_1512_1364 extends DBVSUpdate {

    @Override
    public void up() {
      
        deleteActivity("preferences.smsconfiguration.modify.pre");
        insertActivity("preferences.smsconfiguration.modify.pre", "com.technisys.omnichannel.client.activities.preferences.smsconfiguration.ModifySmsConfigurationPreActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        insertOrUpdateConfiguration("preferences.sms.configuration.productTypes", "CA|CC|PA|PI", ConfigurationGroup.NEGOCIO, null, new String[]{});
        insertOrUpdateConfiguration("preferences.sms.configuration.productPermissions", "product.read|transfer.internal|transfer.thirdParties|pay.loan|pay.loan.thirdParties", ConfigurationGroup.NEGOCIO, null, new String[]{});
        
        deleteActivity("preferences.smsconfiguration.products.list");
        insertActivity("preferences.smsconfiguration.products.list", "com.technisys.omnichannel.client.activities.preferences.smsconfiguration.ListProductsActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");

        insertOrUpdateConfiguration("preferences.sms.configuration.aliasPrefix.CA", "CA", ConfigurationGroup.NEGOCIO, null, new String[]{});
        insertOrUpdateConfiguration("preferences.sms.configuration.aliasPrefix.CC", "CA", ConfigurationGroup.NEGOCIO, null, new String[]{});
        insertOrUpdateConfiguration("preferences.sms.configuration.aliasPrefix.PA", "PR", ConfigurationGroup.NEGOCIO, null, new String[]{});
        insertOrUpdateConfiguration("preferences.sms.configuration.aliasPrefix.PI", "PR", ConfigurationGroup.NEGOCIO, null, new String[]{});
        
        deleteActivity("preferences.smsconfiguration.modify.preview");
        insertActivity("preferences.smsconfiguration.modify.preview", "com.technisys.omnichannel.client.activities.preferences.smsconfiguration.ModifySmsConfigurationPreviewActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        deleteActivity("preferences.smsconfiguration.modify.send");
        insertActivity("preferences.smsconfiguration.modify.send", "com.technisys.omnichannel.client.activities.preferences.smsconfiguration.ModifySmsConfigurationActivity", "preferences", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "user.preferences");
        
    }
}
