/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-3816
 *
 * @author mcheveste
 */
public class DB20180531_1636_3816 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("accounts.downloadMovements", "com.technisys.omnichannel.client.activities.accounts.DownloadMovementsActivity", "accounts", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idAccount");
    }
}