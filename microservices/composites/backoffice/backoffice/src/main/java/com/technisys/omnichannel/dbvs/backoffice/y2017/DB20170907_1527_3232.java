/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3232
 *
 * @author mjoseph
 */
public class DB20170907_1527_3232 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_text", new String[]{"min_length", "max_length"}, new String[]{"1","20"}, "id_form = 'creditCardChangeCondition' AND id_field = 'phone'");
    }
    
}
