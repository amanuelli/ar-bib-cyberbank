/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author rschiappapietra
 */
public class DB20170523_1100_2357 extends DBVSUpdate {

    @Override
    public void up() {
        
        update("sections", new String[]{"name"}, new String[]{"Login - 1"}, "id_section = 'login'");
        update("sections", new String[]{"name"}, new String[]{"Login - 2"}, "id_section = 'login2'");
        insert("sections", new String[]{"id_section","name"}, new String[]{"login3","Login - 3"});
        insert("sections_images", new String[]{"id_section","id_image_size"}, new String[]{"login3","1"});
        
    }
}