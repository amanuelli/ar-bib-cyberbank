/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.activities;

import com.technisys.omnichannel.backoffice.business.cybo.invitationcodes.requests.CreateInvitationCodeData;
import com.technisys.omnichannel.backoffice.business.cybo.Response;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.onboarding.Step5Activity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.TransactionRequest;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author azeballos
 */
public class CreateActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        Response<String> response = new Response<>(request);

        try {
            TransactionRequest tRequest = (TransactionRequest) request;
            CreateInvitationCodeData data = (CreateInvitationCodeData) tRequest.getTransactionData();

            ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), data.getDocumentCountry(), data.getDocumentType(), data.getDocumentNumber(), data.getAccount());
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(data.getDocumentCountry(), data.getDocumentType(), data.getDocumentNumber());

            // Creo el registro invitation code
            InvitationCode invitationCode = new InvitationCode();
            invitationCode.setStatus(InvitationCode.STATUS_NOT_USED);

            User auxClient = user != null ? user : client;
            if (auxClient != null) { // Nunca va a ser null ya que si el cliente es null nunca pasa las validaciones
                invitationCode.setEmail(auxClient.getEmail());
                invitationCode.setMobileNumber(auxClient.getMobileNumber());
                invitationCode.setFirstName(auxClient.getFirstName());
                invitationCode.setLastName(auxClient.getLastName());
            }
            invitationCode.setProductGroupId(data.getAccount());
            invitationCode.setProductGroupName(data.getAccountName());

            invitationCode.setDocumentCountry(data.getDocumentCountry());
            invitationCode.setDocumentType(data.getDocumentType());
            invitationCode.setDocumentNumber(data.getDocumentNumber());

            invitationCode.setBackendUser(true);
            invitationCode.setLang(user != null ? user.getLang() : data.getUserLang());
            invitationCode.setAccessType(data.getAccessType());

            String sendChannel = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "invitation.notification.transport");
            invitationCode.setChannelSent(sendChannel);

            invitationCode.setSignatureLevel("A");

            Environment environment = Administration.getInstance().readEnvironmentByProductGroupId(data.getAccount());
            if (environment == null) {
                invitationCode.setAdministrationScheme(data.getAdministrationScheme());
                invitationCode.setSignatureQty(data.getSignatureQty());
            }

            String invitationCodePlain = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
            InvitationCodesHandler.createInvitationCode(invitationCode, invitationCodePlain);

            response.setData(invitationCode.getInvitationCode());

            Step5Activity.sendInvitationEmail(invitationCode, request, invitationCodePlain, null);

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        TransactionRequest tRequest = (TransactionRequest) request;
        Map<String, String> result = new HashMap<>();

        CreateInvitationCodeData data = (CreateInvitationCodeData) tRequest.getTransactionData();

        try {
            ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), data.getDocumentCountry(), data.getDocumentType(), data.getDocumentNumber(), data.getAccount());
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(data.getDocumentCountry(), data.getDocumentType(), data.getDocumentNumber());

            if (StringUtils.isBlank(data.getFullName()) || client == null) {
                result.put("@documentNumber", "cybo.invitationCodesManagement.validations.documentNumber.mustReadUserFirst");
            } else {
                // Account
                List<ClientEnvironment> accounts = RubiconCoreConnectorC.listClients(request.getIdTransaction(), data.getDocumentCountry(), data.getDocumentType(), data.getDocumentNumber());
                if (accounts.isEmpty()) {
                    result.put("@documentNumber", "cybo.invitationCodesManagement.validations.documentNumber.clientDontHaveAccounts");
                } else {
                    ClientEnvironment clientEnv = new ClientEnvironment();
                    clientEnv.setProductGroupId(data.getAccount());

                    if (!StringUtils.isNumeric(data.getAccount()) || !accounts.contains(clientEnv)) {
                        result.put("@account", "cybo.invitationCodesManagement.validations.account.invalid");
                    }
                }

                // Lang
                if (user == null) {
                    List<String> validLangs = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "core.languages");
                    if (StringUtils.isBlank(data.getUserLang())) {
                        result.put("@language", "cybo.general.validations.empty");
                    } else if (!validLangs.contains(data.getUserLang())) {
                        result.put("@language", "cybo.invitationCodesManagement.validations.language.invalid");
                    }
                }

                // Access Type
                List<String> validAccessTypes = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "invitation.permissions.roleList.backoffice");
                if (StringUtils.isBlank(data.getAccessType())) {
                    result.put("@accessType", "cybo.general.validations.empty");
                } else if (!validAccessTypes.contains(data.getAccessType())) {
                    result.put("@accessType", "cybo.invitationCodesManagement.validations.accessType.invalid");
                }

                Environment environment = Administration.getInstance().readEnvironmentByProductGroupId(data.getAccount());
                if (environment == null) {
                    // Administration Scheme
                    List<String> validAdminSchemes = Arrays.asList(new String[]{Environment.ADMINISTRATION_SCHEME_SIMPLE, Environment.ADMINISTRATION_SCHEME_MEDIUM, Environment.ADMINISTRATION_SCHEME_ADVANCED});
                    if (StringUtils.isBlank(data.getAdministrationScheme())) {
                        result.put("@schema", "cybo.general.validations.empty");
                    } else if (!validAdminSchemes.contains(data.getAdministrationScheme())) {
                        result.put("@schema", "cybo.invitationCodesManagement.validations.schema.invalid");
                    }

                    // Signature Scheme
                    List<Integer> validSignatureSchemes = Arrays.asList(1, 2);
                    if (data.getSignatureQty() == null) {
                        result.put("@signatureLevel", "cybo.general.validations.empty");
                    } else if (!validSignatureSchemes.contains(data.getSignatureQty())) {
                        result.put("@signatureLevel", "cybo.invitationCodesManagement.validations.signatureLevel.invalid");
                    }
                } else {
                    //Si el ambiente existe, hay que ver que el usuario no esté ya asociado al mismo
                    if (user != null && Administration.getInstance().readEnvironmentUserInfo(user.getIdUser(), environment.getIdEnvironment()) != null) {
                        result.put("@account", "cybo.invitationCodesManagement.validations.account.userAlreadyLinked");
                    }
                }

                // Datos del backend
                if (client.getMobileNumber() == null) {
                    result.put("@mobileNumber", "cybo.invitationCodesManagement.validations.mobileNumber.invalidBackendMobilePhone");
                }
                else if (StringUtils.isEmpty(client.getEmail())
                        || !UIUtils.validateEmailPattern(client.getEmail())
                        || client.getEmail().length() > InvitationCode.EMAIL_MAX_LENGTH) {
                    result.put("@email", "cybo.invitationCodesManagement.validations.email.invalidBackendEmail");
                }
            }
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }

}
