/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.i18n.I18n;

import java.util.HashMap;
import java.util.Map;

/**
 * @author fpena
 */

public class DB20181211_1457_6123 extends DBVSUpdate {

    @Override
    public void up() {

        String idForm = "salaryPayment";
        String idActivity = "pay.multiline.salary.send";

        //Borramos el formulario transferThirdParties por si existe, y creamos el nuevo que es Transferencia a terceros
        delete("forms", "id_form = '" + idForm + "'");

        String[] formField = new String[]{"id_form", "category", "enabled", "type", "id_activity", "version", "schedulable", "drafts_enabled", "editable_in_narrow"};
        String[] formFieldValues = new String[]{idForm, "payments", "1", "activity", idActivity, "1", "1", "1", "1"};
        insert("forms", formField, formFieldValues);

        formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        formFieldValues = new String[]{"forms." + idForm + ".formName", idForm, "1", "es", "Pagar sueldos", "2018-12-10 12:00:00"};

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2] + "', '" + formFieldValues[3] + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formField, formFieldValues);
        }

        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "sub_type", "ordinal", "visible", "required"};

        String[] formFieldsValues = new String[]{"introSection", idForm, "1", "intro", "default", "1", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);

        formFieldsValues = new String[]{"debitAccount", idForm, "1", "productselector", "default", "2", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);

        formFieldsValues = new String[]{"reference", idForm, "1", "text", "default", "3", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);

        formFieldsValues = new String[]{"horizontalLine", idForm, "1", "horizontalrule", "default", "4", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);

        formFieldsValues = new String[]{"fileSection", idForm, "1", "sectiontitle", "default", "5", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);

        formFieldsValues = new String[]{"sampleFile", idForm, "1", "filelink", "default", "6", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);

        formFieldsValues = new String[]{"file", idForm, "1", "multilinefile", "salaryPayment", "7", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);


        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option"};
        formFieldsValues = new String[]{"debitAccount", idForm, "1", "field-normal", "0"};
        insert("form_field_ps", formFields, formFieldsValues);

        formFields = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        formFieldsValues = new String[]{"debitAccount", idForm, "1", "pay.multiline.salaryPayment"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);

        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", idForm, "1", "CA"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);

        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", idForm, "1", "CC"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);

        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"reference", idForm, "1", "0", "50", "field-normal"};
        insert("form_field_text", formFields, formFieldsValues);

        formFields = new String[]{"id_field", "id_form", "form_version", "data"};
        formFieldsValues = new String[]{"file", idForm, "1", "{\"allowMultiple\":false,\"acceptedFileTypes\":[\"text/plain\"],\"useForTotalAmount\":true,\"maxFileSizeMB\":4}"};
        insert("form_field_data", formFields, formFieldsValues);

        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_label"};
        formFieldsValues = new String[]{"sampleFile", idForm, "1", "field-normal", "0"};
        insert("form_field_filelink", formFields, formFieldsValues);


        customSentence(new String[]{DBVS.DIALECT_MSSQL}, "INSERT INTO form_field_fl_file (id_field, id_form, form_version, lang, file_name, contents, creation_date) " +
                "values ('sampleFile', '" + idForm + "', 1, 'es', 'archivoEjemplo.txt', convert(varbinary(max), '7487563622839,222' + CHAR(13) + CHAR(10) + '12345679,222,500.00,Name LastName'), getDate())");

        customSentence(new String[]{DBVS.DIALECT_MSSQL}, "INSERT INTO form_field_fl_file (id_field, id_form, form_version, lang, file_name, contents, creation_date) " +
                "values ('sampleFile', '" + idForm + "', 1, 'en', 'sampleFile.txt', convert(varbinary(max), '7487563622839,222' + CHAR(13) + CHAR(10) + '12345679,222,500.00,Name LastName'), getDate())");

        customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_fl_file (id_field, id_form, form_version, lang, file_name, contents, creation_date) " +
                "values ('sampleFile', '" + idForm + "', 1, 'es', 'archivoEjemplo.txt', utl_raw.cast_to_raw('7487563622839,222\\n12345679,222,500.00,Name LastName'), CURRENT_DATE)");

        customSentence(new String[]{DBVS.DIALECT_ORACLE}, "INSERT INTO form_field_fl_file (id_field, id_form, form_version, lang, file_name, contents, creation_date) " +
                "values ('sampleFile', '" + idForm + "', 1, 'en', 'sampleFile.txt', utl_raw.cast_to_raw('7487563622839,222\\n12345679,222,500.00,Name LastName'), CURRENT_DATE)");

        customSentence(new String[]{DBVS.DIALECT_MYSQL}, "INSERT INTO form_field_fl_file (id_field, id_form, form_version, lang, file_name, contents, creation_date) " +
                "values ('sampleFile', '" + idForm + "', 1, 'es', 'archivoEjemplo.txt', convert('7487563622839,222\\n12345679,222,500.00,Name LastName', binary), NOW())");

        customSentence(new String[]{DBVS.DIALECT_MYSQL}, "INSERT INTO form_field_fl_file (id_field, id_form, form_version, lang, file_name, contents, creation_date) " +
                "values ('sampleFile', '" + idForm + "', 1, 'en', 'sampleFile.txt', convert('7487563622839,222\\n12345679,222,500.00,Name LastName', binary), NOW())");

        Map<String, String> messages = new HashMap();
        messages.put("debitAccount.label", "Cuenta de origen");
        messages.put("file.label", "Subir archivo");
        messages.put("reference.label", "Referencia");
        messages.put("reference.placeholder", "Ingresa una referencia para mostrar en cada movimiento");
        messages.put("fileSection.label", "Archivo a procesar");
        messages.put("introSection.label", "introducción");
        messages.put("introSection.hint", "Necesitarás subir un archivo con un formato predeterminado con las líneas de pago individuales.");
        messages.put("sampleFile.label", "Tu archivo de pagos debe respetar una estructura predeterminada");
        messages.put("sampleFile.hint", "Descargar archivo de ejemplo");

        insertMessages(idForm, messages, I18n.LANG_SPANISH);

        messages = new HashMap();
        messages.put("debitAccount.label", "Source account");
        messages.put("file.label", "Upload file");
        messages.put("reference.label", "Reference");
        messages.put("reference.placeholder", "Enter a reference to show in each movement");
        messages.put("fileSection.label", "File to process");
        messages.put("introSection.label", "Introduction");
        messages.put("introSection.hint", "You will need to upload a file with a specific format containing the individual payment lines.");
        messages.put("sampleFile.label", "Your payment file must comply a predetermined structure");
        messages.put("sampleFile.hint", "Download sample file");

        insertMessages(idForm, messages, I18n.LANG_ENGLISH);

        formFields = new String[]{"id_permission", "simple_id_category", "simple_allow_prod_selection", "simple_ordinal", "medium_id_category", "medium_allow_prod_selection", "medium_ordinal", "advanced_id_category", "advanced_ordinal", "advanced_allow_prod_selection", "product_types"};
        formFieldsValues = new String[]{"pay.multiline.salaryPayment", "payments", "0", "50", "payments", "0", "50", "payments", "50", "1", "CA,CC"};
        insert("adm_ui_permissions", formFields, formFieldsValues);

    }

    private void insertMessages(String idForm, Map<String, String> messages, String lang) {
        String[] formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        String[] formFieldsValues;

        for (Map.Entry<String, String> entry : messages.entrySet()) {
            String key = entry.getKey();
            String message = entry.getValue();

            formFieldsValues = new String[]{"fields." + idForm + "." + key, lang, key.substring(0, key.indexOf('.')), idForm, "1", message, "2018-12-10 12:00:00"};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }
}