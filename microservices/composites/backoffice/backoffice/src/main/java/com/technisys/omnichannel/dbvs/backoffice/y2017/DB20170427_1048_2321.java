/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2321
 *
 * @author dimoda
 */
public class DB20170427_1048_2321 extends DBVSUpdate {

    @Override
    public void up() {
        //MANNAZCA-2321 - Mensaje de error por tope diario para una transaccion sin importe
        update("form_field_amount", new String[]{"control_limits"}, new String[]{"0"}, "id_form='additionalCreditCardRequest' and id_field='amount'");
    }
  
}