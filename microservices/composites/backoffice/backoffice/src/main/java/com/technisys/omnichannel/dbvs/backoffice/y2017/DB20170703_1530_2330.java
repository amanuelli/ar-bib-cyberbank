/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 

package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author danny
 */
public class DB20170703_1530_2330 extends DBVSUpdate {

    @Override
    public void up() {        

        deleteActivity("transactions.sendTransactionReminders");
        insertActivity("transactions.sendTransactionReminders",
                "com.technisys.omnichannel.client.activities.transactions.SendTransactionRemindersActivity",
                "core.saveCancelSignTransaction", ActivityDescriptor.AuditLevel.None,
                ClientActivityDescriptor.Kind.Other, "core.authenticated");
        

        update("configuration", new String[]{"value"},
                new String[]{"message|accepted|rejected|pending|transferReceived|creditCardExpiration|loanExpiration|creditCardPayment|loanPayment|promos|userBlock|transactionReminders"},
                "id_field='core.communications.communicationTypes'");

    }

}
