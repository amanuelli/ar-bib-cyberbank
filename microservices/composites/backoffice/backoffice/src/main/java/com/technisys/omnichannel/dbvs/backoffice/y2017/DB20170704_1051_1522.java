/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import java.sql.Types;

/**
 *
 * @author szuliani
 */
public class DB20170704_1051_1522 extends DBVSUpdate {

    @Override
    public void up() {

        ColumnDefinition[] columns = new ColumnDefinition[] {
                new ColumnDefinition("id_status", Types.VARCHAR, 20, 0, false, null),
                new ColumnDefinition("key_status", Types.VARCHAR, 100, 0, false, null) };
        createTable("invitation_codes_status", columns, new String[] { "id_status" });

        String[] fieldNames = new String[] { "id_status", "key_status" };
        insert("invitation_codes_status", fieldNames,
                new String[] { "NOT_USED", "backoffice.invitationCodes.filter.status.notUsed" });
        insert("invitation_codes_status", fieldNames,
                new String[] { "USED", "backoffice.invitationCodes.filter.status.used" });
        insert("invitation_codes_status", fieldNames,
                new String[] { "CANCELED", "backoffice.invitationCodes.filter.status.canceled" });
        insert("invitation_codes_status", fieldNames,
                new String[] { "EXPIRED", "backoffice.invitationCodes.filter.status.expired" });

        insertBackofficeActivity("backoffice.invitationCodes.listpre",
                "com.technisys.omnichannel.backoffice.business.invitationcodes.activities.ListPreActivity",
                "backoffice.invitationCodes.list", null, "backoffice.invitationcodes",
                ActivityDescriptor.AuditLevel.None);

    }
}
