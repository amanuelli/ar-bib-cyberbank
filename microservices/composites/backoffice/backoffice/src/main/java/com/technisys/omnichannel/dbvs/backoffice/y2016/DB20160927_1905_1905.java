/* 
 * Copyright 2016 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2016;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.sql.Types;

/**
 *
 * @author fpena
 */
public class DB20160927_1905_1905 extends DBVSUpdate {

    @Override
    public void up() {
        //la clave de la tabla client_environments debia ser solo el id_environment, por algun motivo tenia tambien el id_cliente, la recreamos
        ColumnDefinition[] columns = new ColumnDefinition[]{
            new ColumnDefinition("id_environment", Types.INTEGER, 10, 0, false, null),
            new ColumnDefinition("id_client", Types.INTEGER, 10, 0, false, null),
            new ColumnDefinition("last_synchronization", Types.DATE, 0, 0, true, null)
        };

        String[] primaryKey = new String[]{"id_environment"};
        
        createTable("client_environments2", columns, primaryKey);
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, 
                "INSERT INTO client_environments2 (id_client, id_environment) SELECT id_client, id_environment FROM client_environments");
        dropTable("client_environments");
        
        
        createTable("client_environments", columns, primaryKey);
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, 
                "INSERT INTO client_environments (id_client, id_environment) SELECT id_client, id_environment FROM client_environments2");
        dropTable("client_environments2");
        
        createIndex("client_environments", "idx_ce_id_client", new String[]{"id_client"});
        
        //por defecto sinchronizamos productos cada 12 horas o más
        insertOrUpdateConfiguration("backend.synchronization.interval", "12h", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty","interval"});
        insertOrUpdateConfiguration("backend.webservices.concurrentRequest", "100", ConfigurationGroup.TECNICAS, "backend", new String[]{"notEmpty","integer"});
        
        update ("configuration", new String[]{"id_sub_group","id_group"}, new String[]{"backend",ConfigurationGroup.TECNICAS.name()}, "id_field='backend.webservices.dateFormat'");
        update ("configuration", new String[]{"id_sub_group","id_group"}, new String[]{"backend",ConfigurationGroup.TECNICAS.name()}, "id_field='backend.webservices.decimalFormat'");
        update ("configuration", new String[]{"id_sub_group","id_group"}, new String[]{"backend",ConfigurationGroup.TECNICAS.name()}, "id_field='backend.webservices.url'");
        
    }
}
