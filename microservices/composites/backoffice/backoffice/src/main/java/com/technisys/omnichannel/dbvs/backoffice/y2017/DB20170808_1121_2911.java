/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2911
 *
 * @author drebolo
 */
public class DB20170808_1121_2911 extends DBVSUpdate {

    @Override
    public void up() {
        
        String fields[] = new String[] { "code_type", "code_number", "bank_name", "bank_address", "bank_country_code"};
        String tableName = "client_banks";
        
        insert(tableName, fields, new String[]{"SWIFT", "AACMUS41", "AAM COMPANY", "SUITE 3500:", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "AACOUS31", "AMBAC ASSURANCE CORPORATION", "ONE STATE STREET PLAZA", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "AAMCUS41", "ATTUCKS ASSET MANAGEMENT LLC", "SUITE 1940:", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "AAMSUS31", "ARNOLD AND S. BLEICHROEDER ADVISORS LLC", "OF THE AMERICAS:", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "AANCUS31", "XARAF MANAGEMENT LLC", "2 AMERICAN LANE", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "ABAIUS31", "ABERDEEN AMERICA INC.", "10 UNION WHARF", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "ABCOUS33", "ARAB BANKING CORPORATION", "27TH FLOOR:", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "ABCTUS31", "ABACO INTERNATIONAL CORPORATION", "", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "ABEEUS31", "ABG SECURITIES INC.", "FLOOR 11:", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "ABENUS31", "ABEL NOSER", "90 BROAD STREET", "US"});
        insert(tableName, fields, new String[]{"SWIFT", "ABFGUS41", "ABACUS FINANCIAL GROUP INC.", "SUITE 2175:", "US"});
        
        insert(tableName, fields, new String[]{"ABA", "82900872", "Arvest Bank", "403 TOWN CENTER BELLA VISTA AR72714", "US"});
        insert(tableName, fields, new String[]{"ABA", "82906517", "BancorpSouth Bank", "2006 S MAIN STUTTGART AR72160", "US"});
        insert(tableName, fields, new String[]{"ABA", "81302633", "German American Bancorp d/b/a German American", "RTE 1 MONTGOMERY IN47558", "US"});
        insert(tableName, fields, new String[]{"ABA", "82901570", "BancorpSouth Bank", "131 N CHESTER ST HAZEN AR72064", "US"});
        insert(tableName, fields, new String[]{"ABA", "82906892", "BancorpSouth Bank", "118 N COURT ST CARLISLE AR72024", "US"});
        insert(tableName, fields, new String[]{"ABA", "82901075", "BancorpSouth Bank", "800 N MAIN ST BRINKLEY AR72021", "US"});
        insert(tableName, fields, new String[]{"ABA", "211371120", "Cambridge Savings Bank", "214 CAMBRIDGE ST BURLINGTON MA01803", "US"});
        insert(tableName, fields, new String[]{"ABA", "211372378", "UniBank for Savings", "10 CHENEY ST BLACKSTONE MA01504", "US"});
        insert(tableName, fields, new String[]{"ABA", "11301798", "Eastern Bank", "108 MAIN ST KINGSTON MA02364", "US"});
        insert(tableName, fields, new String[]{"ABA", "211370370", "Hingham Institution for Savings", "401 NANTASKET AVE HULL MA02045", "US"});
        insert(tableName, fields, new String[]{"ABA", "211370396", "Avidia Bank", "1 LYMAN ST WESTBOROUGH MA01581", "US"});
        insert(tableName, fields, new String[]{"ABA", "62201999", "Traders and Farmers Bank", "HWY 278 EAST ADDISON AL35540", "US"});
        
        insert(tableName, fields, new String[]{"SWIFT", "ABNAARBA", "ROYAL BANK OF SCOTLAND NV (SUC ARGENTINA)", "361 FLORIDA", "AR"});
        insert(tableName, fields, new String[]{"SWIFT", "ACIAARB1", "ABACO CIA. FINANCIERA S.A.", "1172 SARMIENTO", "AR"});
        insert(tableName, fields, new String[]{"SWIFT", "AEIBARB1", "AMERICAN EXPRESS BANK LTD.", "FLOOR 17:", "AR"});
        insert(tableName, fields, new String[]{"SWIFT", "BACHARBA", "NUEVO BANCO DEL CHACO S.A.", "", "AR"});
        insert(tableName, fields, new String[]{"SWIFT", "BACIARBA", "BANCO DE LA CIUDAD DE BUENOS AIRES", "302 FLORIDA", "AR"});
        insert(tableName, fields, new String[]{"SWIFT", "BACOARBA", "HSBC BANK ARGENTINA SA", "", "AR"});
        insert(tableName, fields, new String[]{"SWIFT", "BACOARB1", "HSBC BANK ARGENTINA SA", "", "AR"});
        insert(tableName, fields, new String[]{"SWIFT", "BAPIARBA", "BANCO PIANO S.A.", "345-347 SAN MARTIN", "AR"});
        insert(tableName, fields, new String[]{"SWIFT", "BAVAARBA", "BANCO DE VALORES", "", "AR"});
        insert(tableName, fields, new String[]{"SWIFT", "BBDEARBA", "BANCO BRADESCO ARGENTINA S/A", "", "AR"});
        
        insert(tableName, fields, new String[]{"CHIPS", "1", "BANK OF NEW YORK", "6023 AIRPORT ROAD", "US"});
        insert(tableName, fields, new String[]{"CHIPS", "2", "JPMORGAN CHASE BANK", "10410 HIGHLAND MANOR DRIVE", "US"});
        insert(tableName, fields, new String[]{"CHIPS", "8", "CITIBANK NA", "", "US"});
        insert(tableName, fields, new String[]{"CHIPS", "103", "DEUTSCHE BANK TRUST CO AMERICAS", "60 WALL STREET MAIL SUITE NYC60-0501", "US"});
        insert(tableName, fields, new String[]{"CHIPS", "108", "HSBC BANK USA N.A.", "90 CHRISTIANA ROAD", "US"});
        insert(tableName, fields, new String[]{"CHIPS", "112", "THE NORTHERN TRUST COMPANY", "40 BROAD STREET 10TH FLOOR", "US"});
        insert(tableName, fields, new String[]{"CHIPS", "160", "BB T", "2501 WOOTEN BLVD", "US"});
        insert(tableName, fields, new String[]{"CHIPS", "174", "MASHREQ BANK", "255 FIFTH AVENUE - 1ST. FLOOR", "US"});
        insert(tableName, fields, new String[]{"CHIPS", "184", "BANCO BILBAO VIZCAYA S.A.", "1345 AVE OF THE AMERICAS 45TH FL", "US"});
        insert(tableName, fields, new String[]{"CHIPS", "217", "THE NATIONAL BANK OF KUWAIT SAK", "299 PARK AVENUE 17TH FLOOR", "US"});
       
    }
}