/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-4781
 *
 * @author dimoda
 */
public class DB20180711_1054_4781 extends DBVSUpdate {

    @Override
    public void up() {
        
        deleteActivity("preferences.userData.updateMail.send");
        deleteActivity("preferences.userData.updateMobilePhone.send");
        
        insertActivity("preferences.userData.mail.sendCode", "com.technisys.omnichannel.client.activities.preferences.userdata.SendMailCodeActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "user.preferences");
        insertActivity("preferences.userData.mobilePhone.sendCode", "com.technisys.omnichannel.client.activities.preferences.userdata.SendMobilePhoneCodeActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "user.preferences");

    }

}