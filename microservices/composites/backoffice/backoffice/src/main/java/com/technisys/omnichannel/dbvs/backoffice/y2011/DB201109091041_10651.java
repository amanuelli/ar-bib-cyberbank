/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201109091041_10651 extends DBVSUpdate {

    @Override
    public void up() {

        update("form_fields", new String[]{"field_type"}, new String[]{"currency"}, "id_field='moneda' and id_form = 83");
        update("form_fields", new String[]{"field_type"}, new String[]{"currency"}, "id_field='moneda' and id_form = 84");
        update("form_fields", new String[]{"field_type"}, new String[]{"currency"}, "id_field='moneda' and id_form = 5");
        update("form_fields", new String[]{"field_type"}, new String[]{"currency"}, "id_field='monedaCarta' and id_form = 18");

    }
}