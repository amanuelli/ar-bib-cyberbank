/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201109271331_11200 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"mandatory"};
        String[] fieldValues = new String[]{"1"};

        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='numeroTarjeta'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='marca'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='alcanceAmexBlueBox'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='alcanceAmexCenturion'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='alcanceMaster'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='alcanceVisa'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='programaAmexBlueBox'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='programaAmexCenturion'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='programaMaster'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='programaVisa'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='cierreAmexBlueBox'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='cierreAmexCenturion'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='cierreMaster'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='cierreVisa'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='traspasoLimiteCredito'");

        fieldNames = new String[]{"possible_values"};
        fieldValues = new String[]{"1|10|20"};

        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='cierreAmexBlueBox'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='cierreAmexCenturion'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='cierreMaster'");
        update("form_fields", fieldNames, fieldValues, "id_form=14 AND id_field='cierreVisa'");
    }
}