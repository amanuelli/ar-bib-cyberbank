/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;


import com.technisys.omnichannel.DBVSUpdate;

/**
 * 
 * @author diegofabra
 */
public class DB20170907_1551_3227 extends DBVSUpdate {

    @Override
    public void up() {
        
        
        update("form_messages",new String[]{"value"},new String[]{"Solicitud de cambio de condiciones"},
                "id_message = 'forms.creditCardChangeCondition.formName' and id_form = 'creditCardChangeCondition'"
                        + " and lang = 'es' ");
        
        update("form_messages",new String[]{"value"},new String[]{"Request for change of conditions"},
                "id_message = 'forms.creditCardChangeCondition.formName' and id_form = 'creditCardChangeCondition'"
                        + " and lang = 'en' ");
        
        update("form_messages",new String[]{"value"},new String[]{"Solicitação de alteração das condições"},
                "id_message = 'forms.creditCardChangeCondition.formName' and id_form = 'creditCardChangeCondition'"
                        + " and lang = 'pt' ");
        
    }
}