package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.ArrayList;
import java.util.List;

/**
 * MANNAZCA-4040
 *
 * @author ncarril
 */
public class DB20180705_1039_4040 extends DBVSUpdate {

    private final String FORM_FIELD_MESSAGES = "form_field_messages";
    private final String FORM_FIELD_SELECTOR_OPTIONS = "form_field_selector_options";
    private final String FORM_FIELD_SELECTOR = "form_field_selector";
    private final String FORM_FIELDS = "form_fields";
    private final String[] FIELD_VALUE = new String[]{"value"};

    @Override
    public void up() {

        String form_name = "requestOfManagementCheck";

        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type"};
        String[] selector_field = new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as"};
        String[] options_selector_field = new String[]{"id_field", "id_form", "form_version", "value"};
        String[] field_messages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};

        //Se cambia el titulo para agregar la L al final de gerencia
        update("form_messages", FIELD_VALUE, new String[]{"Solicitud de Cheque Gerencial"}, "id_form = 'requestOfManagementCheck' and lang ='es'");

        //CAMPO TIPO
        // Se agrega un combo para cargar tipo -> cheque o letra
        String type = "type";

        insert(FORM_FIELDS, formFields, new String[]{type, form_name, "1", "selector", "1", "TRUE", "TRUE", "default"});
        insert(FORM_FIELD_SELECTOR, selector_field, new String[]{type, form_name, "1", "form-normal", "0", "combo"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{type, form_name, "1", "checkbook"});
        insert(FORM_FIELD_SELECTOR_OPTIONS, options_selector_field, new String[]{type, form_name, "1", "billOfExchange"});

        List<String[]> messages = new ArrayList<String[]>();

        String preLoadName = "fields." + form_name + "." + type + ".";
        messages.add(new String[]{preLoadName + "label", "es", type, form_name, "1", "2018-07-06 09:48:00", "Tipo"});
        messages.add(new String[]{preLoadName + "label", "en", type, form_name, "1", "2018-07-06 09:48:00", "Type"});
        messages.add(new String[]{preLoadName + "label", "pt", type, form_name, "1", "2018-07-06 09:48:00", "Tipo"});

        messages.add(new String[]{preLoadName + "option.checkbook", "es", type, form_name, "1", "2018-07-06 09:48:00", "Cheque"});
        messages.add(new String[]{preLoadName + "option.checkbook", "en", type, form_name, "1", "2018-07-06 09:48:00", "Check"});
        messages.add(new String[]{preLoadName + "option.checkbook", "pt", type, form_name, "1", "2018-07-06 09:48:00", "Cheque"});

        messages.add(new String[]{preLoadName + "option.billOfExchange", "es", type, form_name, "1", "2018-07-06 09:48:00", "Letra de Cambio"});
        messages.add(new String[]{preLoadName + "option.billOfExchange", "en", type, form_name, "1", "2018-07-06 09:48:00", "Bill of Exchange"});
        messages.add(new String[]{preLoadName + "option.billOfExchange", "pt", type, form_name, "1", "2018-07-06 09:48:00", "Letra de câmbio"});

        messages.add(new String[]{preLoadName + "requiredError", "es", type, form_name, "1", "2018-07-06 09:48:00", "Debe seleccionar un Tipo"});
        messages.add(new String[]{preLoadName + "requiredError", "en", type, form_name, "1", "2018-07-06 09:48:00", "You must select a Type"});
        messages.add(new String[]{preLoadName + "requiredError", "pt", type, form_name, "1", "2018-07-06 09:48:00", "Você deve selecionar um tipo"});

        //CAMPO importe
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"2"}, "id_form = 'requestOfManagementCheck' and id_field='amount'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"El campo Importe no puede estar vacío"}, "id_form = 'requestOfManagementCheck' and id_field ='amount' and lang ='es' and id_message ='fields.requestOfManagementCheck.amount.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"The Amount field can not be empty"}, "id_form = 'requestOfManagementCheck' and id_field ='amount' and lang ='en' and id_message ='fields.requestOfManagementCheck.amount.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"O campo Importância não pode estar vazio"}, "id_form = 'requestOfManagementCheck' and id_field ='amount' and lang ='pt' and id_message ='fields.requestOfManagementCheck.amount.requiredError'");

        //CAMPO Cuenta
        //Se cambia la etiqueta de 'Cuenta Debito' a 'Cuenta'
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Cuenta"}, "id_form = 'requestOfManagementCheck' and id_field ='debitAccount' and lang ='es' and id_message ='fields.requestOfManagementCheck.debitAccount.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Account"}, "id_form = 'requestOfManagementCheck' and id_field ='debitAccount' and lang ='en' and id_message ='fields.requestOfManagementCheck.debitAccount.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Conta"}, "id_form = 'requestOfManagementCheck' and id_field ='debitAccount' and lang ='pt' and id_message ='fields.requestOfManagementCheck.debitAccount.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Debe seleccionar una Cuenta"}, "id_form = 'requestOfManagementCheck' and id_field ='debitAccount' and lang ='es' and id_message ='fields.requestOfManagementCheck.debitAccount.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"You must select an Account"}, "id_form = 'requestOfManagementCheck' and id_field ='debitAccount' and lang ='en' and id_message ='fields.requestOfManagementCheck.debitAccount.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Você deve selecionar uma conta"}, "id_form = 'requestOfManagementCheck' and id_field ='debitAccount' and lang ='pt' and id_message ='fields.requestOfManagementCheck.debitAccount.requiredError'");
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"3"}, "id_form = 'requestOfManagementCheck' and id_field='debitAccount'");

        //CAMPO Cuenta debito Costo
        //Se modifica la etiqueta 
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Cuenta de débito de costo"}, "id_form = 'requestOfManagementCheck' and id_field ='costDebitAccount' and lang ='es' and id_message ='fields.requestOfManagementCheck.costDebitAccount.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Conta de débito de custo"}, "id_form = 'requestOfManagementCheck' and id_field ='costDebitAccount' and lang ='pt' and id_message ='fields.requestOfManagementCheck.costDebitAccount.label'");
        //Se eliminan los mensajes de validacion de requerido del campo Cuenta débito de costo ya que no es obligatorio
        delete(FORM_FIELD_MESSAGES, "id_message = 'fields.requestOfManagementCheck.costDebitAccount.requiredError'");
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"4"}, "id_form = 'requestOfManagementCheck' and id_field='costDebitAccount'");

        //CAMPO nombre Beneficiario
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"5"}, "id_form = 'requestOfManagementCheck' and id_field='nameOfBeneficiary'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"El Nombre de beneficiario no puede estar vacío"}, "id_form = 'requestOfManagementCheck' and id_field ='nameOfBeneficiary' and lang='es' and id_message ='fields.requestOfManagementCheck.nameOfBeneficiary.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"O nome do beneficiário não pode estar vazio"}, "id_form = 'requestOfManagementCheck' and id_field ='nameOfBeneficiary' and lang='pt' and id_message ='fields.requestOfManagementCheck.nameOfBeneficiary.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"The Beneficiary Name can not be empty"}, "id_form = 'requestOfManagementCheck' and id_field ='nameOfBeneficiary' and lang='en' and id_message ='fields.requestOfManagementCheck.nameOfBeneficiary.requiredError'");

        //CAMPO lugar de retiro
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"6"}, "id_form = 'requestOfManagementCheck' and id_field='placeOfRetreat'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Debe seleccionar un Lugar de Retiro"}, "id_form = 'requestOfManagementCheck' and id_field ='placeOfRetreat' and lang='es' and id_message ='fields.requestOfManagementCheck.placeOfRetreat.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"You must select a Place of Withdrawal"}, "id_form = 'requestOfManagementCheck' and id_field ='placeOfRetreat' and lang='en' and id_message ='fields.requestOfManagementCheck.placeOfRetreat.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Você deve selecionar um Local de retirada"}, "id_form = 'requestOfManagementCheck' and id_field ='placeOfRetreat' and lang='pt' and id_message ='fields.requestOfManagementCheck.placeOfRetreat.requiredError'");

        //CAMPO Sucursales 
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"7"}, "id_form = 'requestOfManagementCheck' and id_field='branchOffices'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Debe seleccionar una sucursal de retiro"}, "id_form = 'requestOfManagementCheck' and id_field ='branchOffices' and lang='es' and id_message ='fields.requestOfManagementCheck.branchOffices.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Você deve selecionar um local de retirada"}, "id_form = 'requestOfManagementCheck' and id_field ='branchOffices' and lang='pt' and id_message ='fields.requestOfManagementCheck.branchOffices.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"You must select a retirement branch"}, "id_form = 'requestOfManagementCheck' and id_field ='branchOffices' and lang='en' and id_message ='fields.requestOfManagementCheck.branchOffices.requiredError'");

        //CAMPO direccion
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"8"}, "id_form = 'requestOfManagementCheck' and id_field='address'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"El campo Dirección no puede estar vacío"}, "id_form = 'requestOfManagementCheck' and id_field ='address' and lang='es' and id_message ='fields.requestOfManagementCheck.address.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"The Address field can not be empty"}, "id_form = 'requestOfManagementCheck' and id_field ='address' and lang='en' and id_message ='fields.requestOfManagementCheck.address.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"O campo Domicílio não pode estar vazio"}, "id_form = 'requestOfManagementCheck' and id_field ='address' and lang='pt' and id_message ='fields.requestOfManagementCheck.address.requiredError'");

        //Titulo seccion
        String titleData = "personalDataTitle";
        String[] title = new String[]{titleData, form_name, "1", "sectiontitle", "9", "TRUE", "FALSE", "default"};
        String tagTitle = "fields." + form_name + "." + titleData + ".label";
        messages.add(new String[]{tagTitle, "es", titleData, form_name, "1", "2018-07-06 09:48:00", "Autorizado a Retirar o recibir chequera"});
        messages.add(new String[]{tagTitle, "pt", titleData, form_name, "1", "2018-07-06 09:48:00", "Autorizado para retirar ou receber um talão de cheques"});
        messages.add(new String[]{tagTitle, "en", titleData, form_name, "1", "2018-07-06 09:48:00", "Authorized to Withdraw or receive a checkbook"});
        insert(FORM_FIELDS, formFields, title);

        //CAMPO nombre
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"10"}, "id_form = 'requestOfManagementCheck' and id_field='name'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"El campo Nombre no puede estar vacío"}, "id_form = 'requestOfManagementCheck' and id_field ='name' and lang='es' and id_message ='fields.requestOfManagementCheck.name.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"The Name field can not be empty"}, "id_form = 'requestOfManagementCheck' and id_field ='name' and lang='en' and id_message ='fields.requestOfManagementCheck.name.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"O campo Nome não pode estar vazio"}, "id_form = 'requestOfManagementCheck' and id_field ='name' and lang='pt' and id_message ='fields.requestOfManagementCheck.name.requiredError'");

        //CAMPO apellido 
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"11"}, "id_form = 'requestOfManagementCheck' and id_field='surname'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Apellido"}, "id_form = 'requestOfManagementCheck' and id_field ='surname' and lang='es' and id_message ='fields.requestOfManagementCheck.surname.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Surname"}, "id_form = 'requestOfManagementCheck' and id_field ='surname' and lang='en' and id_message ='fields.requestOfManagementCheck.surname.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Sobrenome"}, "id_form = 'requestOfManagementCheck' and id_field ='surname' and lang='pt' and id_message ='fields.requestOfManagementCheck.surname.label'");

        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"El campo Apellido no puede estar vacío"}, "id_form = 'requestOfManagementCheck' and id_field ='surname' and lang='es' and id_message ='fields.requestOfManagementCheck.surname.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"The Surname field can not be empty"}, "id_form = 'requestOfManagementCheck' and id_field ='surname' and lang='en' and id_message ='fields.requestOfManagementCheck.surname.requiredError'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"O campo Sobrenome não pode estar vazio"}, "id_form = 'requestOfManagementCheck' and id_field ='surname' and lang='pt' and id_message ='fields.requestOfManagementCheck.surname.requiredError'");

        //CAMPO Documento
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"12"}, "id_form = 'requestOfManagementCheck' and id_field='document'");

        //CAMPO terminos y condiciones
        update(FORM_FIELDS, new String[]{"ordinal"}, new String[]{"13"}, "id_form = 'requestOfManagementCheck' and id_field='termsAndConditions'");

        String messID = "fields." + form_name + ".termsAndConditions.requiredError";
        messages.add(new String[]{messID, "es", "termsAndConditions", form_name, "1", "2018-07-06 09:48:00", "Debe aceptar los términos y condiciones"});
        messages.add(new String[]{messID, "en", "termsAndConditions", form_name, "1", "2018-07-06 09:48:00", "You must accept the terms and conditions"});
        messages.add(new String[]{messID, "pt", "termsAndConditions", form_name, "1", "2018-07-06 09:48:00", "Você deve aceitar os termos e condições"});

        delete(FORM_FIELD_MESSAGES, "id_form = 'requestOfManagementCheck' and id_field ='secondSurname'");
        delete("form_field_text", "id_form = 'requestOfManagementCheck' and id_field = 'secondSurname'");
        delete(FORM_FIELDS, "id_form = 'requestOfManagementCheck' and id_field='secondSurname'");

        for (String[] key : messages) {
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + key[0] + "', '" + key[1] + "', '" + key[2] + "', '" + key[3] + "', '" + key[4] + "', '" + key[6] + "', TO_DATE('" + key[5] + "', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert(FORM_FIELD_MESSAGES, field_messages, key);
            }
        }

    }

}
