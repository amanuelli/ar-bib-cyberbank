/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

/**
 * Related issue: MANNAZCA-5348
 *
 * @author marcelobruno
 */
public class DB20180917_1500_5348 extends DBVSUpdate {

    @Override
    public void up() {
        
        String formVersion = "1";
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
     
        update("form_field_messages", new String[] {"value"}, new String[] {""}, "id_message='fields.transferForeign.debitAccount.hint' AND id_form='transferForeign'");
        
        String[] formFieldMessagesColumns = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        
        Map<String, String> formFieldMessages = new HashMap();
        
        formFieldMessages.put("fields.transferForeign.shouldDebitFromOriginAccount.option.selected", "All the commissions will be debited from the selected account; if you wish you can charge the expenses to other account.");
        formFieldMessages.put("fields.transferForeign.amount.placeholder", "Enter the amount to be transferred");
        formFieldMessages.put("fields.transferForeign.shouldDebitFromOriginAccount.label", "Debit of transaction fees");
        formFieldMessages.put("fields.transferForeign.debitCommissionsFromOptions.option.1stRecipientAccount", "For the recipient");
        formFieldMessages.put("fields.transferForeign.debitCommissionsFromOptions.option.2ndOriginAndRecipientAccounts", "Shared between the recipient and the origin account");
        formFieldMessages.put("fields.transferForeign.debitCommissionsFromOptions.option.3rdOwnerAndRecipientAccounts", "Shared between the recipient and another own account");
        formFieldMessages.put("fields.transferForeign.debitCommissionsFromOptions.option.4thOwnerAccount", "To another own account");
        formFieldMessages.put("fields.transferForeign.recipientAccountCode.placeholder", "Enter the destination bank code");
        formFieldMessages.put("fields.transferForeign.creditAccount.placeholder", "Enter the destination account");
        formFieldMessages.put("fields.transferForeign.recipientName.label", "Recipient''s name");
        formFieldMessages.put("fields.transferForeign.recipientName.placeholder", "Enter the recipient''s name");
        formFieldMessages.put("fields.transferForeign.recipientAddress.label", "Recipient address");
        formFieldMessages.put("fields.transferForeign.recipientAddress.placeholder", "Enter the recipient''s address");
        formFieldMessages.put("fields.transferForeign.debitReference.placeholder", "Enter a reference to show the recipient");
        formFieldMessages.put("fields.transferForeign.notificationEmails.placeholder", "Enter one or more emails to notify");
        formFieldMessages.put("fields.transferForeign.isUsingIntermediaryBank.label", "Intermediary bank");
        formFieldMessages.put("fields.transferForeign.isUsingIntermediaryBank.option.selected", "Use intermediary bank");
        formFieldMessages.put("fields.transferForeign.intermediaryAccountNumber.placeholder", "Enter the intermediary bank account");
        formFieldMessages.put("fields.transferForeign.intermediaryBank.placeholder", "Enter the intermediary bank code");
        formFieldMessages.put("fields.transferForeign.notificationBody.placeholder", "This message will be added to the transaction information that is sent by email to the destinations listed in the previous field");
        this.insertFormFieldMessages(formFieldMessagesColumns, formFieldMessages, "en", formVersion, date);

        formFieldMessages = new HashMap();
        formFieldMessages.put("fields.transferForeign.shouldDebitFromOriginAccount.option.selected", "Todas as comissões serão debitadas da conta selecionada. Se desejar, você poderá carregar as despesas a outra conta.");
        formFieldMessages.put("fields.transferForeign.amount.placeholder", "Insira o valor");
        formFieldMessages.put("fields.transferForeign.shouldDebitFromOriginAccount.label", "Comissões da transação");
        formFieldMessages.put("fields.transferForeign.debitCommissionsFromOptions.option.1stRecipientAccount", "Para o destinatário");
        formFieldMessages.put("fields.transferForeign.debitCommissionsFromOptions.option.2ndOriginAndRecipientAccounts", "Compartilhado entre o destinatário e a conta de origem");
        formFieldMessages.put("fields.transferForeign.debitCommissionsFromOptions.option.3rdOwnerAndRecipientAccounts", "Compartilhado entre o destinatário e outra conta própria");
        formFieldMessages.put("fields.transferForeign.debitCommissionsFromOptions.option.4thOwnerAccount", "Para outra conta própria");
        formFieldMessages.put("fields.transferForeign.recipientAccountCode.placeholder", "Digite o código do banco de destino");
        formFieldMessages.put("fields.transferForeign.creditAccount.placeholder", "Digite a conta de destino");
        formFieldMessages.put("fields.transferForeign.recipientName.label", "Nome do destinatário");
        formFieldMessages.put("fields.transferForeign.recipientName.placeholder", "Digite o nome do destinatário");
        formFieldMessages.put("fields.transferForeign.recipientAddress.label", "Endereço do destinatário");
        formFieldMessages.put("fields.transferForeign.recipientAddress.placeholder", "Digite o endereço do destinatário");
        formFieldMessages.put("fields.transferForeign.debitReference.placeholder", "Digite uma referência para mostrar ao destinatário");
        formFieldMessages.put("fields.transferForeign.notificationEmails.placeholder", "Insira um ou mais emails para notificar");
        formFieldMessages.put("fields.transferForeign.isUsingIntermediaryBank.label", "Banco intermediário");
        formFieldMessages.put("fields.transferForeign.isUsingIntermediaryBank.option.selected", "Usar Banco intermediário");
        formFieldMessages.put("fields.transferForeign.intermediaryAccountNumber.placeholder", "Digite a conta bancária intermediária");
        formFieldMessages.put("fields.transferForeign.intermediaryBank.placeholder", "Digite o código do banco intermediário");
        formFieldMessages.put("fields.transferForeign.notificationBody.placeholder", "Esta mensagem será adicionada às informações de transação enviadas por email para os destinos listados no campo anterior");
        this.insertFormFieldMessages(formFieldMessagesColumns, formFieldMessages, "pt", formVersion, date);
        
    }
    
    private void insertFormFieldMessages(String[] formFieldMessagesColumns, Map<String, String> formFieldMessages, String lang, String formVersion, String date){
        for (String key : formFieldMessages.keySet()) {
            String[] formFieldsValues = new String[]{key, lang, key.split(Pattern.quote("."))[2], key.split(Pattern.quote("."))[1], formVersion, date, formFieldMessages.get(key)};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', " + formFieldsValues[4] + ", TO_DATE('2018-06-26 15:03:07', 'YYYY-MM-DD HH24:MI:SS'), '" + formFieldsValues[6] + "')");
            } else {
                insert("form_field_messages", formFieldMessagesColumns, formFieldsValues);
            }
        }
    }
    
}
