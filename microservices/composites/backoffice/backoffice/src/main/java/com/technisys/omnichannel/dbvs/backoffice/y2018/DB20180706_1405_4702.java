/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-4702
 *
 * @author norbes
 */
public class DB20180706_1405_4702 extends DBVSUpdate {

    @Override
    public void up() {
        
        deleteActivity("widgets.investments");
        
        insertActivity("widgets.portfolio", "com.technisys.omnichannel.client.activities.widgets.PortfolioActivity", 
                "desktop", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        
        insert("widgets", new String[]{"id", "uri"}, new String[]{"portfolio", "widgets/portfolio"});
        
        update("desktop_layouts", new String[]{"id_widget"}, new String[]{"portfolio"}, "id_widget = 'investments'");
        
        delete("widgets", "id = 'investments'");
    }
}
