/* 
 * Copyright 2016 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2016;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20160924_1705_1889 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("core.scheduler.request.checkbook.send.saturdayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
        insertOrUpdateConfiguration("core.scheduler.request.checkbook.send.sundayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
        insertOrUpdateConfiguration("core.scheduler.request.checkbook.preview.saturdayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
        insertOrUpdateConfiguration("core.scheduler.request.checkbook.preview.sundayIsWorkingDay", "true", ConfigurationGroup.TECNICAS, "scheduler", new String[]{"notEmpty","boolean"});
    }
}
