package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5403
 *
 * @author mcheveste
 */
public class DB20181001_1800_5403 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.medium.read.permissions", "com.technisys.omnichannel.client.activities.administration.medium.ReadPermissionsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.medium.modify.permissions.preview", "com.technisys.omnichannel.client.activities.administration.medium.ModifyPermissionsPreviewActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.medium.modify.permissions", "com.technisys.omnichannel.client.activities.administration.medium.ModifyPermissionsActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Admin, "administration.manage");
    }
}
