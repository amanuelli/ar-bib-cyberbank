/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import java.sql.Types;

/**
 *
 * @author salva
 */
public class DB20150710_1620_446 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("preferences.userData.modify.pre", "com.technisys.omnichannel.client.activities.preferences.userdata.ModifyUserDataPreActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.userData.modify.preview", "com.technisys.omnichannel.client.activities.preferences.userdata.ModifyUserDataPreviewActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.userData.modify.send", "com.technisys.omnichannel.client.activities.preferences.userdata.ModifyUserDataActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "user.preferences");
    
        dropColumn("client_users", "phone");
        dropColumn("client_users", "mobile_phone");
        
        ColumnDefinition column = new ColumnDefinition("district", Types.VARCHAR, 50, 0, true, null);
        addColumn("client_users", column);
        column = new ColumnDefinition("province", Types.VARCHAR, 50, 0, true, null);
        addColumn("client_users", column);
        column = new ColumnDefinition("department", Types.VARCHAR, 50, 0, true, null);
        addColumn("client_users", column);
        column = new ColumnDefinition("residence_country", Types.VARCHAR, 50, 0, true, null);
        addColumn("client_users", column);
    
    }
}
