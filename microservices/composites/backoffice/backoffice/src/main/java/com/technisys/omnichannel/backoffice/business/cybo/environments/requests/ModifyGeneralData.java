/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.cybo.environments.requests;

import com.technisys.omnichannel.core.domain.TransactionData;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;

import org.apache.commons.lang3.StringUtils;

import static com.technisys.omnichannel.core.domain.Environment.ENVIRONMENT_TYPE_CORPORATE_GROUP;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;

/**
 *
 * @author azeballos
 */
public class ModifyGeneralData extends TransactionData {
    private int id;
    private String name;
    private String type;
    private String scheme;
    private String holdingClientsToAdd;
    private String holdingClientsToRemove;

    @Override
    public String calculateAdditionalInfoToHash(){
        StringBuilder additionalInfo = new StringBuilder(super.calculateAdditionalInfoToHash());

        additionalInfo.append(id);
        if (!StringUtils.isBlank(holdingClientsToAdd)) {
            additionalInfo.append("-");
            additionalInfo.append(holdingClientsToAdd);
        }
        if (!StringUtils.isBlank(holdingClientsToRemove)) {
            additionalInfo.append("-");
            additionalInfo.append(holdingClientsToRemove);
        }

        return additionalInfo.toString();
    }
    
    @Override
    public String getIdentifier() {
        return getName();
    }
    
    @Override
    public String getShortDescription (String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();
        strBuilder.append("<h5>").append(i18n.getMessage("backoffice.transaction.action", getLang())).append(": <a href=\"javascript:showPendingAction('").append(idTransaction).append("')\"> ").append(activityName).append("</a></h5>");
        strBuilder.append("<p class='element_details'>");
        strBuilder.append("      ").append(i18n.getMessage("backoffice.transaction.name", getLang())).append(": <em>").append(name).append("</em><br/>");
        strBuilder.append("      ").append(i18n.getMessage("cybo.environmentsManagement.general.type", getLang())).append(": <em>").append(i18n.getMessage("cybo.environmentsManagement.general.type." + type, getLang())).append("</em><br/>");
        if (!ENVIRONMENT_TYPE_CORPORATE_GROUP.equals(type)) {
            strBuilder.append("      ").append(i18n.getMessage("cybo.environmentsManagement.general.schema", getLang())).append(": <em>").append(i18n.getMessage("cybo.environmentsManagement.general.schema." + scheme, getLang())).append("</em><br/>");
        }
        strBuilder.append("      ").append(i18n.getMessage("backoffice.transaction.comments", getLang())).append(": <em>").append(this.getComment()).append("</em><br/>");
        return strBuilder.toString();
    }

    @Override
    public String getFullDescription (String idTransaction, String activityName) {
        StringBuilder strBuilder = new StringBuilder();
        I18n i18n = I18nFactory.getHandler();
        strBuilder.append("<dl class = 'element_details clearfix'>");
        strBuilder.append("<dt>").append(i18n.getMessage("cybo.environmentsManagement.title.singular", getLang())).append(":</dt>")
                  .append("<dd>").append(name).append("</dd>");
        strBuilder.append("<dt>").append(i18n.getMessage("cybo.environmentsManagement.general.type", getLang())).append("</dt>")
                  .append("<dd>").append(i18n.getMessage("cybo.environmentsManagement.general.type." + type, getLang())).append("</dd>");
        if (!ENVIRONMENT_TYPE_CORPORATE_GROUP.equals(type)) {
            strBuilder.append("<dt>").append(i18n.getMessage("cybo.environmentsManagement.general.schema", getLang())).append("</dt>")
                      .append("<dd>").append(i18n.getMessage("cybo.environmentsManagement.general.schema." + scheme, getLang())).append("</dd>");
        }
        if (!StringUtils.isBlank(holdingClientsToAdd)) {
            String[] lMembers = holdingClientsToAdd.split(", ");

            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.environments.holdings.modify.members.add", getLang())).append("</dt>")
                      .append("<dd>");
            for (String lMember : lMembers) {
                strBuilder.append(getMemberName(idTransaction, lMember)).append("<br/>");
            }
            strBuilder.append("</dd>");
        }

        if (!StringUtils.isBlank(holdingClientsToRemove)) {
            String[] lMembers = holdingClientsToRemove.split(", ");

            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.environments.holdings.modify.members.remove", getLang())).append("</dt>")
                      .append("<dd>");
            for (String lMember : lMembers) {
                strBuilder.append(getMemberName(idTransaction, lMember)).append("<br/>");
            }
            strBuilder.append("</dd>");
        }
        
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.transaction.comments", getLang())).append(":</dt>")
                  .append("<dd>").append(this.getComment()).append("</dd>");

        strBuilder.append("</dl>");

        return strBuilder.toString();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getHoldingClientsToAdd() {
        return StringUtils.isBlank(holdingClientsToAdd) ? "" : holdingClientsToAdd;
    }

    public void setHoldingClientsToAdd(String newHoldingMembers) {
        holdingClientsToAdd = newHoldingMembers;
    }

    public void setHoldingClientsToRemove(String toRemove) {
        this.holdingClientsToRemove = toRemove;
    }

    public String getHoldingClientsToRemove() {
        return StringUtils.isBlank(holdingClientsToRemove) ? "" : holdingClientsToRemove;
    }

    private String getMemberName(String transactionID, String memberID) {
        String result;

        try {
            result = RubiconCoreConnectorC.readClient(transactionID, memberID).getAccountName();
        } catch (Exception ex) {
            result = "N/A";
        }

        return result;
    }

}
