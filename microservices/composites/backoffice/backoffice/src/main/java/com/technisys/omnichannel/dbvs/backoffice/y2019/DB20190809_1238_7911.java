/*
 * Copyright (c) 2019 Technisys.
 * 
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 * 
 * https://www.technisys.com.
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-7911
 *
 * @author divie
 */
public class DB20190809_1238_7911 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("transactions.downloadTicket");
        insertActivity("transactions.downloadTicket", "com.technisys.omnichannel.client.activities.transactions.DownloadTicketActivity", "transactions", ActivityDescriptor.AuditLevel.None, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}
