/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author dimoda
 */

public class DB20190312_1530_6832 extends DBVSUpdate {

    @Override
    public void up() {
        delete("backoffice_transactions", "id_activity='backoffice.invitationCodesHolding.readEntity'");
        delete("backoffice_activities", "id_activity='backoffice.invitationCodesHolding.readEntity'");
        delete("backoffice_role_permissions", "id_backoffice_permission='backoffice.invitationCodesHolding.manage'");
        delete("backoffice_permissions_cred_gr", "id_backoffice_permission='backoffice.invitationCodesHolding.manage'");
        delete("backoffice_permissions", "id_backoffice_permission='backoffice.invitationCodesHolding.manage'");
    }
}