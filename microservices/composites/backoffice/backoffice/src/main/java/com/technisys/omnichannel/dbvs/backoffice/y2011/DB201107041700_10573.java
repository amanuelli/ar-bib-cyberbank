/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107041700_10573 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_field", "value", "possible_values", "id_group", "encrypted", "must_be_encrypted"};
        String[] fieldValues = new String[]{"rubicon.password.minLength", "6", "", "Rubicon", "0", "0"};
        insert("configuration", fieldNames, fieldValues);
    }
}