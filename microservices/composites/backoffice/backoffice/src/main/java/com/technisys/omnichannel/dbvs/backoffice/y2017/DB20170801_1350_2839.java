/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author rschiappapietra
 */
public class DB20170801_1350_2839 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("widgets.notifications.maxCommunications", "10", ConfigurationGroup.NEGOCIO, "widgetNotifications",
                new String[]{"notEmpty", "integer"});

        insertOrUpdateConfiguration("widgets.notifications.maxExpirations", "10", ConfigurationGroup.NEGOCIO, "widgetNotifications", new String[] { "notEmpty", "integer" });

        insertOrUpdateConfiguration("widgets.notifications.maxPending", "10", ConfigurationGroup.NEGOCIO, "widgetNotifications", new String[] { "notEmpty", "integer" });

        update("configuration", new String[] { "id_sub_group" }, new String[] { "widgetNotifications" }, "id_field='widgets.notifications.max.simple.cards'");
        update("configuration", new String[] { "id_sub_group" }, new String[] { "widgetProducts" }, "id_field='widget.products.accounts.statementsPerPage'");
        update("configuration", new String[] { "id_sub_group" }, new String[] { "widgetProducts" }, "id_field='widget.products.creditCards.statementsPerPage'");
        update("configuration", new String[] { "id_sub_group" }, new String[] { "widgetProducts" }, "id_field='widget.products.deposits.statementsPerPage'");
        update("configuration", new String[] { "id_sub_group" }, new String[] { "widgetProducts" }, "id_field='widget.products.loans.statementsPerPage'");
        update("configuration", new String[] { "id_sub_group" }, new String[] { "widgetTransactions" }, "id_field='widget.transactions.availableStates'");
        update("configuration", new String[] { "id_sub_group" }, new String[] { "widgetTransactions" }, "id_field='widget.transactions.maxPending'");
    }
}
