/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150730_1057_381 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("core.permissionsForProducts","product.read|transfer.internal|transfer.thirdParties|pay.loan|pay.loan.thirdParties");
        update("form_field_ps", new String[]{"show_other_permission"}, new String[]{"transfer.thirdParties"}, "id_field='creditAccount' AND id_form='transferInternal'");
    }
}
