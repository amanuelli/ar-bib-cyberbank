/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author KevinGaray
 */

public class DB20200728_1923_13659 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("auth.login.credentialRequested", "otp");

        //update the permissions_credentials_groups table to set the accessToken-pin value to accessToken-otp
        update("permissions_credentials_groups", new String[]{"id_credential_group"}, new String[]{"accessToken-otp"}, "id_credential_group = 'accessToken-pin'");

    }

}