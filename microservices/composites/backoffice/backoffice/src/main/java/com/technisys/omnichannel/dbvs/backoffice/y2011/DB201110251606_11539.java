/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author mquerves
 */
public class DB201110251606_11539 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_field", "value", "id_group"};

        String[] fieldValues = new String[]{"rubicon.widgets.geolocalization.atmList", "-34.008717,-56.130719#Cajero 1|-34.906850,-56.163381#Cajero 2|-34.806287,-56.163853#Cajero 3", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.widgets.geolocalization.offices", "-34.000017,-56.100009#Sucursal 1|-34.926850,-56.163481#Sucursal Uno|-34.906687,-56.163853#Sucursal Pocitos", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.widgets.geolocalization.promotions", "-34.908717,-56.160769#Technisys|-32.906750,-52.163381#Promocion|-33.806687,-56.143853#Promocion otra", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.widgets.geolocalization.areas", "-34.908333,-56.151389#Pocitos|-34.905678,-56.208243#Ciudad Vieja|-34.899131,-56.180692#Cordon", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.widgets.geolocalization.defaultLocation", "-34.908717,-56.160769#Technisys", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldValues = new String[]{"rubicon.widgets.geolocalization.googleClientID", "", "rubicon"};
        insert("configuration", fieldNames, fieldValues);

        fieldNames = new String[]{"name", "uri", "default_column", "default_row"};

        fieldValues = new String[]{"geolocalization", "/widgets/geolocalization.jsp", "1", "6"};
        insert("client_desktop_widgets", fieldNames, fieldValues);

    }
}