/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5009
 *
 * @author dimoda
 */
public class DB20180727_1618_5009 extends DBVSUpdate {

    @Override
    public void up() {

        update("forms", new String[]{"programable"}, new String[]{"1"}, "id_form = 'transferInternal'");
        update("forms", new String[]{"programable"}, new String[]{"1"}, "id_form = 'transferLocal'");
        update("forms", new String[]{"programable"}, new String[]{"1"}, "id_form = 'transferThirdParties'");
        
    }

}