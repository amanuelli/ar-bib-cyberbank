/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author ?
 */
public class DB20151013_1616_1416 extends DBVSUpdate {

    @Override
    public void up() {
        deleteConfiguration("services.notification.communicationTray.1");
        deleteConfiguration("services.notification.communicationTray.2");
        deleteConfiguration("services.notification.communicationTray.3");
        deleteConfiguration("services.notification.communicationTray.4");
        deleteConfiguration("services.notification.communicationTray.5");
        deleteConfiguration("services.notification.communicationTray.6");
        deleteConfiguration("services.notification.communicationTray.7");

        insertOrUpdateConfiguration("services.notification.communicationTray.accepted", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("services.notification.communicationTray.rejected", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("services.notification.communicationTray.warning", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("services.notification.communicationTray.message", "1", ConfigurationGroup.NEGOCIO, "notificationService", new String[]{"notEmpty", "integer"});

        insertOrUpdateConfiguration("core.notifier.plugin", "com.technisys.omnichannel.client.utils.ClientTransactionsNotifier", null, "notifications", new String[]{"notEmpty", "class"});
        
    }
}
