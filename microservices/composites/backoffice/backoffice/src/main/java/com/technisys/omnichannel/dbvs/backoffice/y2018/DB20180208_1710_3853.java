/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3853
 *
 * @author isilveira
 */
public class DB20180208_1710_3853 extends DBVSUpdate {

    @Override
    public void up() {
        String text = "Lorem Ipsum es simplemente texto de relleno utilizado en la industria de la impresión y la tipografía. Lorem Ipsum ha sido el texto de relleno estándar de la industria desde inicios del 1500, cuando un impresor desconocido tomó una galera de imprenta y la revolvió, para crear una especie de libro. No solo ha sobrevivido cinco siglos, sino también el salto a la tipografía electrónica, permaneciendo esencialmente sin cambios. Se popularizó en la década de 1960 con el lanzamiento de las hojas de Letraset, las cuales contienen pasajes de Lorem Ipsum, y más recientemente con software de autoedición como Aldus PageMaker, el cual incluye versiones de Lorem Ipsum.";
        String whereClause = "(id_message='fields.accountOpening.termsConditions.termsAndConditions' OR id_message='fields.additionalCreditCardRequest.termsandconditions.termsAndConditions' OR id_message='fields.creditCardRequest.termsandconditions.termsAndConditions' OR id_message='fields.requestLoan.termsAndCondLoan.termsAndConditions') AND lang='es'";
        
        update("form_field_messages", new String[] { "value" }, new String[] { text }, whereClause);
    }
}