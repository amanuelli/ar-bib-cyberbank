/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5819
 *
 * @author rdosantos
 */
public class DB20181119_1051_5819 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("product.list.maxBoxDisplay", "5", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='product.list.maxBoxDisplay'");
    }
}