/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 *
 * Issue 10166: Solicitud de activacion de chequera - campos
 */
public class DB201106301207_10166 extends DBVSUpdate {

    @Override
    public void up() {
        // FORM FIELDS
        delete("form_fields", "id_form = 83 AND id_field='cantidadNumero'");
        delete("form_fields", "id_form = 83 AND id_field='cantidadSerie'");

        String[] fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "ordinal", "default_value", "possible_values"};
        String[] fieldValues = new String[]{"quieroIndicar", "83", "radio_button", "1", "8", "", "Cantidad de cheques|Último cheque"};
        insert("form_fields", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "ordinal", "default_value", "possible_values", "validation_regexp", "depends_on_id_field", "depends_on_value"};
        fieldValues = new String[]{"cantidadCheques", "83", "short_text", "1", "9", "", "", "[\\\\d][\\\\d]*", "quieroIndicar", "Cantidad de cheques"};
        insert("form_fields", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "ordinal", "default_value", "possible_values", "validation_regexp", "depends_on_id_field", "depends_on_value"};
        fieldValues = new String[]{"ultimoChequeNumero", "83", "short_text", "1", "7", "", "", "[\\\\d][\\\\d]*", "quieroIndicar", "Último cheque"};
        insert("form_fields", fieldNames, fieldValues);

        fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "ordinal", "default_value", "possible_values", "validation_regexp", "depends_on_id_field", "depends_on_value"};
        fieldValues = new String[]{"ultimoChequeSerie", "83", "short_text", "1", "6", "", "", "[0-9a-zA-Z][0-9a-zA-Z]*", "quieroIndicar", "Último cheque"};
        insert("form_fields", fieldNames, fieldValues);

        // FORMS
        StringBuilder template = new StringBuilder("");
        template.append("<fieldset>\n");
        template.append("<form:field idField='cuenta'/>\n");
        template.append("<form:field idField='tipoDeChequera'/>\n");
        template.append("<form:field idField='moneda'/>\n");
        template.append("<form:field idField='primerChequeSerie'/>\n");
        template.append("<form:field idField='primerChequeNumero'/>\n");
        template.append("<form:field idField='quieroIndicar'/>\n");
        template.append("<form:field idField='ultimoChequeSerie'/>\n");
        template.append("<form:field idField='ultimoChequeNumero'/>\n");
        template.append("<form:field idField='cantidadCheques'/>\n");
        template.append("</fieldset>");

        update("forms", new String[]{"template_es"}, new String[]{template.toString()}, "id_form = 83");
    }
}