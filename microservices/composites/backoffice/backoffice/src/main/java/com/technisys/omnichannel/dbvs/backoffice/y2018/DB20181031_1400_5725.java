package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-5725
 *
 * @author mcheveste
 */
public class DB20181031_1400_5725 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.advanced.group.create.pre", "com.technisys.omnichannel.client.activities.administration.advanced.group.create.GroupCreatePreActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.advanced.group.create.preview", "com.technisys.omnichannel.client.activities.administration.advanced.group.create.GroupCreatePreviewActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.advanced.group.create.send", "com.technisys.omnichannel.client.activities.administration.advanced.group.create.GroupCreateActivity", "administration", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        deleteActivity("administration.advanced.group.read");
        insertActivity("administration.advanced.group.read", "com.technisys.omnichannel.client.activities.administration.advanced.group.GroupReadActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");

        insertActivity("administration.advanced.group.modify.pre", "com.technisys.omnichannel.client.activities.administration.advanced.group.modify.GroupModifyPreActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.advanced.group.modify.preview", "com.technisys.omnichannel.client.activities.administration.advanced.group.modify.GroupModifyPreviewActivity", "administration", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
        insertActivity("administration.advanced.group.modify.send", "com.technisys.omnichannel.client.activities.administration.advanced.group.modify.GroupModifyActivity", "administration", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");
    }
}
