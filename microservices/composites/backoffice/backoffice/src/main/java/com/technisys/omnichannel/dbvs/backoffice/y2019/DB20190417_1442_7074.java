/* 
 * Copyright 2019 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-7074
 *
 * @author Marcelo Bruno
 */
public class DB20190417_1442_7074 extends DBVSUpdate {

    @Override
    public void up() {       
        Map<String, String> messages = new HashMap();
        
        messages.put("debitAccount.label","Conta de origem");
        messages.put("file.label","Manuseio de linhas de pagamento");
        messages.put("fileSection.label","Arquivo para processar");        
        messages.put("introSection.hint","Você precisará fazer upload de um arquivo que respeite qualquer um dos formato nas linhas de pagamento individuais. Ou você pode inserilos manulamente");        
        messages.put("introSection.label","Introdução");
        messages.put("reference.label","Comentário");
        messages.put("reference.placeholder","Escreva um comentário para sua referência");
        messages.put("sampleFile.hint","Baixar arquivo de amostra");
        messages.put("sampleFile.label","Seu arquivo para a lista de pagamentos deve respeitar um dos seguintes formatos padrão");
        messages.put("uploadBy.help","Lembre-se de que alterar isso excluirá os dados da transação");
        messages.put("uploadBy.label","Selecione a maneira de fazer o upload dos dados");
        messages.put("uploadBy.option.file","Através de arquivo");
        messages.put("uploadBy.option.inputManually","Manualmente");
        
        String[] formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        String[] formFieldsValues;
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.salaryPayment." + key, "pt", key.substring(0, key.indexOf(".")), "salaryPayment", "1", messages.get(key), "2019-04-17 15:00:00"};           
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2]  + "', '" + formFieldsValues[3]  + "', " + formFieldsValues[4] + ", '" + formFieldsValues[5] + "', TO_DATE('2019-04-17 15:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }

}