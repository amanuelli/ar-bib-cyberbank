/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6256
 *
 * @author iocampo
 */
public class DB20190103_1700_6256 extends DBVSUpdate {
    @Override
    public void up() {
        update("form_field_text", new String[]{ "max_length" }, new String[]{ "20" }, "id_form='salaryPayment'");
    }
}
