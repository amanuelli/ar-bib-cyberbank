/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author KevinGaray
 */

public class DB20200218_1200_9501 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("administration.environment.restrictions.send");
        deleteActivity("administration.users.restrictions.send");
        deleteActivity("administration.restrictions.user.delete.send");
        deleteActivity("administration.restrictions.manage.preview");

        insertActivity(
                "administration.restrictions.user.delete.send", "com.technisys.omnichannel.client.activities.administration.restrictions.DeleteAllUserRestrictionsActivity",
                "administration",
                ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Admin, "administration.manage");

        insertActivity(
                "administration.restrictions.manage.preview", "com.technisys.omnichannel.client.activities.administration.restrictions.ManageAccessRestrictionsPreviewActivity",
                "administration",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");

    }

}