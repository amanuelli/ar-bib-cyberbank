/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author pdominguez
 */
public class DB20170626_1130_2501 extends DBVSUpdate {

    String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());

    String[] forms = new String[]{"id_form", "version", "enabled", "category", "type", "admin_option", "id_activity", "last", "deleted", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow", "id_bpm_process"};
    String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};
    String[] formFieldText = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
    String[] formFieldAmount = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
    String[] formFieldSelector = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"};
    String[] formFieldSelectorOptions = new String[]{"id_field", "id_form", "form_version", "value"};
    String[] formFieldMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
    String[] formMessages = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
    String[] formFieldTextArea = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
    String[] formFieldEmailList = new String[]{"id_field", "id_form", "form_version", "display_type"};
    String[] formFieldDate = new String[]{"id_field", "id_form", "form_version", "display_type"};
    String[] formFieldsValues;

    String idForm = "letterCredit";
    String version = "1";

    Map<String, String> messages = new HashMap();

    @Override
    public void up() {

        /* Delete Form - Presentación de documentos de carta de crédito */
        delete("forms", "id_form = '" + idForm + "'");
        delete("form_messages", "id_form = '" + idForm + "'");

        delete("permissions", "id_permission = 'client.form." + idForm + ".send'");
        delete("permissions_credentials_groups", "id_permission = 'client.form." + idForm + ".send'");
        delete("group_permissions", "id_permission = 'client.form." + idForm + ".send'");

        delete("form_fields", "id_form = '" + idForm + "'");
        delete("form_field_text", "id_form = '" + idForm + "'");
        delete("form_field_messages", "id_form = '" + idForm + "'");

        delete("form_field_selector", "id_form = '" + idForm + "'");
        delete("form_field_selector_options", "id_form = '" + idForm + "'");

        /* Insert Form - Presentación de documentos de carta de crédito */
        String[] forms = new String[]{"id_form", "version", "enabled", "category", "type", "admin_option", "id_activity", "last", "deleted", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow", "id_bpm_process"};
        insert("forms", forms, new String[]{idForm, version, "1", "comex", "process", "comex", null, "1", "0", "1", "1", "1", "1", "1", "demo:1:3"});

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" + idForm + "', '" + version + "', 'es', 'Presentación de documentos de carta de crédito', TO_DATE('2017-06-26 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formMessages, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Presentación de documentos de carta de crédito", date});
        }

        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

        /* Insert field operationNumber - NroOperacion */
        insert("form_fields", formFields, new String[]{"operationNumber", idForm, version, "selector", "1", "TRUE", "TRUE", null, "0", "0", "default", "0"});
        insert("form_field_selector", formFieldSelector, new String[]{"operationNumber", idForm, version, "field-normal", null, "0", "combo"});
        insert("form_field_selector_options", formFieldSelectorOptions, new String[]{"operationNumber", idForm, version, "OP1"});
        insert("form_field_selector_options", formFieldSelectorOptions, new String[]{"operationNumber", idForm, version, "OP2"});
        insert("form_field_selector_options", formFieldSelectorOptions, new String[]{"operationNumber", idForm, version, "OP3"});
        messages.put("operationNumber.label", "Nro. de operación");
        messages.put("operationNumber.requiredError", "Debe ingresar un Nro. de operación");
        messages.put("operationNumber.help", "Seleccione la operación para la que desea presentar documentos");
        messages.put("operationNumber.option.OP1", "Operación 1");
        messages.put("operationNumber.option.OP2", "Operación 2");
        messages.put("operationNumber.option.OP3", "Operación 3");

        /* Insert field billNumber - NroFactura */
        insert("form_fields", formFields, new String[]{"billNumber", idForm, version, "text", "2", "TRUE", "TRUE", null, "0", "0", "default", "0"});
        insert("form_field_text", formFieldText, new String[]{"billNumber", idForm, version, "1", "80", "field-normal", null});
        messages.put("billNumber.label", "Nro. de factura");
        messages.put("billNumber.requiredError", "Debe ingresar un Nro. de factura");

        /* Insert field amountOfUse - Importe de la utilización(Monto) */
        insert("form_fields", formFields, new String[]{"amountOfUse", idForm, version, "amount", "3", "TRUE", "TRUE", null, "0", "0", "default", "0"});
        insert("form_field_amount", formFieldAmount, new String[]{"amountOfUse", idForm, version, "field-normal", "0"});
        messages.put("amountOfUse.label", "Importe de la utilización");
        messages.put("amountOfUse.requiredError", "El importe de la utilización no es válido");
        messages.put("amountOfUse.help", "Este es el importe por el cual el banco reclamará el pago de los documentos presentados bajo la LC de referencia");

        /* Insert field discrepancies - Discrepancias(selector) */
        insert("form_fields", formFields, new String[]{"discrepancies", idForm, version, "selector", "4", "TRUE", "TRUE", null, "0", "0", "default", "0"});
        insert("form_field_selector", formFieldSelector, new String[]{"discrepancies", idForm, version, "field-normal", null, "0", "check"});
        insert("form_field_selector_options", formFieldSelectorOptions, new String[]{"discrepancies", idForm, version, "DIS1"});
        insert("form_field_selector_options", formFieldSelectorOptions, new String[]{"discrepancies", idForm, version, "DIS2"});
        insert("form_field_selector_options", formFieldSelectorOptions, new String[]{"discrepancies", idForm, version, "DIS3"});
        messages.put("discrepancies.label", "Discrepancias");
        messages.put("discrepancies.option.DIS1", "Discrepancias 1");
        messages.put("discrepancies.option.DIS2", "Discrepancias 2");
        messages.put("discrepancies.option.DIS3", "Discrepancias 3");

        /* Insert field requiredDocuments - Documentos exigidos */
        insert("form_fields", formFields, new String[]{"requiredDocuments", idForm, version, "text", "5", "TRUE", "TRUE", null, "0", "0", "default", "0"});
        insert("form_field_text", formFieldText, new String[]{"requiredDocuments", idForm, version, "1", "80", "field-normal", null});
        messages.put("requiredDocuments.label", "Documentos exigidos");
        messages.put("requiredDocuments.requiredError", "Debe indicar al menos un documento");

        /* Insert field attachments - Adjuntos */
        insert("form_fields", formFields, new String[]{"attachments", idForm, version, "text", "6", "TRUE", "FALSE", null, "0", "0", "default", "0"});
        insert("form_field_text", formFieldText, new String[]{"attachments", idForm, version, "1", "80", "field-normal", null});
        messages.put("attachments.label", "Adjuntos");

        /* Insert field observations - Observaciones */
        insert("form_fields", formFields, new String[]{"observations", idForm, version, "textarea", "7", "TRUE", "FALSE", null, "0", "0", "default", "0"});
        insert("form_field_textarea", formFieldTextArea, new String[]{"observations", idForm, version, "1", "500", "field-normal"});
        messages.put("observations.label", "Observaciones");

        /* Insert field disclaimer - Disclaimer */
        insert("form_fields", formFields, new String[]{"disclaimer", idForm, version, "text", "8", "TRUE", "FALSE", null, "0", "0", "default", "0"});
        insert("form_field_text", formFieldText, new String[]{"disclaimer", idForm, version, "1", "80", "field-normal", null});
        messages.put("disclaimer.label", "Disclaimer");

        /* Insert field notificationEmails - Emails de notificación */
        insert("form_fields", formFields, new String[]{"notificationEmails", idForm, version, "emaillist", "9", "TRUE", "FALSE", null, "1", "0", "default", "0"});
        insert("form_field_emaillist", formFieldEmailList, new String[]{"notificationEmails", idForm, version, "field-normal"});
        messages.put("notificationEmails.label", "Emails de notificación");
        messages.put("notificationEmails.help", "E-mails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma.");

        /* Insert field notificationBody - Cuerpo de notificación */
        insert("form_fields", formFields, new String[]{"notificationBody", idForm, version, "textarea", "10", "TRUE", "FALSE", null, "1", "0", "default", "0"});
        insert("form_field_textarea", formFieldTextArea, new String[]{"notificationBody", idForm, version, "1", "500", "field-normal"});
        messages.put("notificationBody.label", "Cuerpo de notificación");

        /* Insert field executionDate - Fecha de ejecución */
        insert("form_fields", formFields, new String[]{"executionDate", idForm, version, "date", "11", "TRUE", "FALSE", null, "1", "0", "default", "0"});
        insert("form_field_date", formFieldDate, new String[]{"executionDate", idForm, version, "field-normal"});
        messages.put("executionDate.label", "Fecha de ejecución");

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','"+formFieldsValues[5] +"', TO_DATE('"+date+"', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldMessages, formFieldsValues);
            }
        }

    }

}
