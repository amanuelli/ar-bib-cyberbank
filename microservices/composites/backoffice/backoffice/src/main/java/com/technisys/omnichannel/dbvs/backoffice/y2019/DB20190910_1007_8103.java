/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Jhossept Garay
 */

public class DB20190910_1007_8103 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("frontend.timeoutforWeatherCall",
                "2000",
                com.technisys.omnichannel.DBVSUpdate.ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty", "integer"},
                "2000", "frontend");
    }

}