/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Jhrodriguez
 */

public class DB20191218_1457_9289 extends DBVSUpdate {

    @Override
    public void up() {
        delete("configuration", "id_field IN ('client.permissions.readonly.none','client.permissions.transactions.none')");
        insertOrUpdateConfiguration(
                "client.permissions.administrator.none",
                "administration.manage|administration.view",
                ConfigurationGroup.SEGURIDAD, "permissions", new String[]{});
    }

}