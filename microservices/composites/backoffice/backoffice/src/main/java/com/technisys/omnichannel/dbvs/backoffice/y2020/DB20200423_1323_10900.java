/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Jhossept Garay
 */

public class DB20200423_1323_10900 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration(
                "frontend.regions",
                "USA|LATAM",
                null, "frontend", new String[]{"notEmpty"},null,"frontend");
    }

}