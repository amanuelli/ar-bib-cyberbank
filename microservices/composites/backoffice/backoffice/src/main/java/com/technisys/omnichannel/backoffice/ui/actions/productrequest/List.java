/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.productrequest;

import com.opensymphony.xwork2.ActionSupport;
import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.backoffice.business.PaginatedListResponse;
import com.technisys.omnichannel.backoffice.business.productrequest.requests.ListRequest;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.utils.DateUtils;
import java.text.ParseException;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

/**
 *
 * @author iocampo
 */
@Action(results = {
    @Result(location = "/productrequest/_list.jsp")
})
public class List extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ListRequest request = new ListRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.productrequest.list");

        request.setId(id);
        request.setCreationDateFrom(creationDateFrom);
        request.setCreationDateTo(creationDateTo);
        request.setEmail(email);
        request.setDocumentNumber(documentNumber);
        request.setStatus(status);
        request.setMobileNumber(mobileNumber);
        request.setType(type);
        request.setExport(false);

        request.setPageNumber(pageNumber);
        request.setOrderBy(orderBy);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        PaginatedListResponse<Onboarding> listResponse = (PaginatedListResponse<Onboarding>) response;
        returnCode = response.getReturnCode().toString();

        list = listResponse.getItemList();
        totalPages = listResponse.getTotalPages();
        totalRows = listResponse.getTotalRows();
        rowsPerPage = listResponse.getRowsPerPage();
        startIndex = Math.max(1, pageNumber - 5);
        endIndex = Math.min(totalPages, pageNumber + 5);

        return SUCCESS;
    }
    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private long id;
    private String documentNumber;
    private String email;
    private Date creationDateFrom;
    private Date creationDateTo;
    private String status;
    private String mobileNumber;
    private String type;
    private int pageNumber = -1;
    private String orderBy = null;
    private String recordToShow = null;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCreationDateFrom(String creationDateFrom) {
        try {
            if (StringUtils.isNotEmpty(creationDateFrom)) {
                this.creationDateFrom = DateUtils.parseShortDate(creationDateFrom);
            }
        } catch (ParseException e) {
            this.creationDateFrom = null;
        }
    }

    public void setCreationDateTo(String creationDateTo) {
        try {
            if (StringUtils.isNotEmpty(creationDateTo)) {
                this.creationDateTo = DateUtils.parseShortDate(creationDateTo);
            }
        } catch (ParseException e) {
            this.creationDateTo = null;
        }
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setPageNumber(int pageNumber) {
        this.pageNumber = pageNumber;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setRecordToShow(String recordToShow) {
        this.recordToShow = recordToShow;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private java.util.List<Onboarding> list;
    private String returnCode;
    private long totalPages;
    private long totalRows;
    private long rowsPerPage;
    private long startIndex;
    private long endIndex;

    public java.util.List<Onboarding> getList() {
        return list;
    }

    public long getTotalPages() {
        return totalPages;
    }

    public long getTotalRows() {
        return totalRows;
    }

    public long getRowsPerPage() {
        return rowsPerPage;
    }

    public String getReturnCode() {
        return returnCode;
    }

    public int getPageNumber() {
        return pageNumber;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public String getRecordToShow() {
        return recordToShow;
    }

    public long getStartIndex() {
        return startIndex;
    }

    public long getEndIndex() {
        return endIndex;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
