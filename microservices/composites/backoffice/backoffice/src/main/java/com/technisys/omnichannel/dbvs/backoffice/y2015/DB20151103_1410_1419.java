/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author ?
 */
public class DB20151103_1410_1419 extends DBVSUpdate {

    @Override
    public void up() {
        //creo las secciones para el login
        insert("sections", new String[]{"id_section", "name"}, new String[]{"login", "Login - Banner superior"});
        insert("sections", new String[]{"id_section", "name"}, new String[]{"login2", "Login - Banner inferior"});

        //tamanio de imagenes
        insert("image_sizes", new String[]{"display", "image_width", "image_height"}, new String[]{"NORMAL", "300", "235"});

        //asociacion de imagenes a secciones
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, "INSERT INTO sections_images (id_section, id_image_size) SELECT 'login', id_image_size FROM image_sizes");
        customSentence(new String[]{DBVS.DIALECT_HSQLDB, DBVS.DIALECT_MSSQL, DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE}, "INSERT INTO sections_images (id_section, id_image_size) SELECT 'login2', id_image_size FROM image_sizes");
        
    
    }
}
