/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author ahernandez
 */

public class DB20200204_1516_CDPCORE_776 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_fields",
                new String[]{"sub_type"},
                new String[]{"coreLocalBanks"},
                "id_form = 'transferLocal' AND id_field = 'bankSelector'");
    }

}