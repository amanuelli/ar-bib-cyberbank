/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.ui.actions.invitationcodes;

import com.opensymphony.xwork2.ActionSupport;
import com.technisys.omnichannel.BackofficeDispatcher;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.PaginatedListResponse;
import com.technisys.omnichannel.backoffice.business.invitationcodes.requests.ListRequest;
import com.technisys.omnichannel.backoffice.ui.UIUtils;
import com.technisys.omnichannel.backoffice.ui.exceptions.HTMLException;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.struts2.convention.annotation.Action;
import org.apache.struts2.convention.annotation.Result;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.ParseException;
import java.util.Date;

/**
 *
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
@Action(results = {
    @Result(name = "success", type = "stream", params = {
        "contentDisposition", "attachment;filename=\"${filename}\"", "contentType", "${contentType}", "inputName", "inputStream", "bufferSize", "1024"
    })
})
public class Export extends ActionSupport implements ServletRequestAware, ServletResponseAware {

    @Override
    public String execute() throws Exception {
        ListRequest request = new ListRequest();
        UIUtils.prepareRequest(request, httpRequest);

        request.setIdActivity("backoffice.invitationCodes.list");

        request.setInvitationCode(invitationCode);
        request.setCreationDateFrom(creationDateFrom);
        request.setCreationDateTo(creationDateTo);
        request.setProductGroupId(clientId);
        request.setEmail(email);
        request.setDocumentNumber(documentNumber);
        request.setStatus(status);

        request.setPageNumber(-1);
        request.setOrderBy(orderBy);

        IBResponse response = BackofficeDispatcher.getInstance().execute(request);

        if (ReturnCodes.OK.equals(response.getReturnCode())) {
            PaginatedListResponse<InvitationCode> listResponse = (PaginatedListResponse<InvitationCode>) response;

            String[] columns = {
                I18nFactory.getHandler().getMessage("backoffice.export.invitationCodes.column.document", request.getLang()),
                I18nFactory.getHandler().getMessage("backoffice.export.invitationCodes.column.email", request.getLang()),
                I18nFactory.getHandler().getMessage("backoffice.export.invitationCodes.column.mobileNumber", request.getLang()),
                I18nFactory.getHandler().getMessage("backoffice.export.invitationCodes.column.creationDate", request.getLang()),
                I18nFactory.getHandler().getMessage("backoffice.export.invitationCodes.column.account", request.getLang()),
                I18nFactory.getHandler().getMessage("backoffice.export.invitationCodes.column.status", request.getLang())
            };

            String[] attributes = {"document", "email", "mobileNumber", "creationDateAsString", "idClient", "statusLabel"};

            byte[] file = UIUtils.toExcelFormat(request.getLang(), listResponse.getItemList(), columns, attributes, I18nFactory.getHandler().getMessageForBackofficeUser("backoffice.export.xls.environments.title", request.getIdUser()));
            inputStream = new ByteArrayInputStream(file);
            contentType = "application/xls";

            return SUCCESS;
        } else {
            throw new HTMLException(response);
        }

    }
    // <editor-fold defaultstate="collapsed" desc="INPUT Parameters">
    private String filename;
    private String orderBy = null;

    private String invitationCode;
    private String documentNumber;
    private String email;
    private Date creationDateFrom;
    private Date creationDateTo;
    private String status;
    private String clientId;

    public void setInvitationCode(String invitationCode) {
        this.invitationCode = invitationCode;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setCreationDateFrom(String creationDateFrom) {
        try {
            if (StringUtils.isNotEmpty(creationDateFrom)) {
                this.creationDateFrom = DateUtils.parseShortDate(creationDateFrom);
            }
        } catch (ParseException e) {
            this.creationDateFrom = null;
        }
    }

    public void setCreationDateTo(String creationDateTo) {
        try {
            if (StringUtils.isNotEmpty(creationDateTo)) {
                this.creationDateTo = DateUtils.parseShortDate(creationDateTo);
            }
        } catch (ParseException e) {
            this.creationDateTo = null;
        }
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="OUTPUT Parameters">
    private transient InputStream inputStream;
    private String contentType;

    public InputStream getInputStream() {
        return inputStream;
    }

    public String getFilename() {
        return filename;
    }

    public String getContentType() {
        return contentType;
    }
    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="HTTPServlet Aware">
    protected transient HttpServletRequest httpRequest;
    protected transient HttpServletResponse httpResponse;

    @Override
    public void setServletRequest(HttpServletRequest hsr) {
        this.httpRequest = hsr;
    }

    @Override
    public void setServletResponse(HttpServletResponse hsr) {
        httpResponse = hsr;
    }
    // </editor-fold>
}
