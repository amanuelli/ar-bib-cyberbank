/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author Marcelo Bruno
 */

public class DB20200608_1425_12562 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("preferences.userData.getUserAddressInformation");
        deleteActivity("preferences.userData.setUserAddressInformation");
        insertActivity("preferences.userData.getUserAddressInformation", "com.technisys.omnichannel.client.activities.preferences.userdata.GetAddressInformation", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("preferences.userData.setUserAddressInformation", "com.technisys.omnichannel.client.activities.preferences.userdata.SetAddressInformation", "preferences", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, "user.preferences.withSecondFactor");
    }

}