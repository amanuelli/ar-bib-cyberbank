/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author salva
 */
public class DB20150611_1422_448 extends DBVSUpdate {

    @Override
    public void up() {
        insert("permissions", new String[]{"id_permission"}, new String[]{"preferences"});
        insert("permissions_credentials_groups", new String[]{"id_permission","id_credential_group"}, new String[]{"preferences", "otp"});
        insertActivity("preferences.environmentandlang.modify.preview", "com.technisys.omnichannel.client.activities.preferences.environmentandlang.ModifyEnvironmentAndLangPreviewActivity", "preferences", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        update("activity_products", new String[]{"id_permission"}, new String[]{"preferences"}, "id_activity = 'preferences.modifyEnvironmentAndLang'");
    }
}
