/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Marcelo Bruno
 */

public class DB20190903_1150_7969 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "payCreditCard";
        insert("form_field_types", new String[]{"id_type"}, new String[]{"coordinates"});

        delete("form_fields",  "id_form='" + idForm + "' and id_field='latitude'");
        delete("form_fields",  "id_form='" + idForm + "' and id_field='longitude'");

        String[] fieldNames = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};
        String[] fieldValues = new String[]{"localization", idForm, "1", "coordinates", "7", "TRUE", "TRUE", "1", "0", "default", "0"};
        insert("form_fields", fieldNames, fieldValues);

        Map<String, String> formFieldMessagesEn = new HashMap();
        formFieldMessagesEn.put("label", "Location");

        Map<String, String> formFieldMessagesEs = new HashMap();
        formFieldMessagesEs.put("label", "Localización");

        Map<String, String> formFieldMessagesPt = new HashMap();
        formFieldMessagesPt.put("label", "localização");

        insertFormFieldMessages(idForm, "localization", formFieldMessagesEn, formFieldMessagesEs, formFieldMessagesPt);

    }

}