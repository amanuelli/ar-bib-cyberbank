/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3008
 *
 * @author fpena
 */
public class DB20171215_1643_3008 extends DBVSUpdate {

    @Override
    public void up() {
        executeScript(DBVS.DIALECT_MYSQL, "database/mysql-501-demo-environments.sql", ";", "UTF-8");
    }

}