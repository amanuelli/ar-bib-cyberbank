/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4927
 *
 * @author marcelobruno
 */
public class DB20180730_1510_4927 extends DBVSUpdate {

    @Override
    public void up() {
        insert("activity_products", new String[]{"id_activity", "id_field", "id_permission"}, new String[]{"core.product.changeAlias", "idProduct", "product.read"});
    }

}