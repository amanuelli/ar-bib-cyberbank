/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * MANNAZCA-5247
 * @author aalves
 */
public class DB20180824_1440_5247 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_text", new String[]{"id_validation"}, new String[]{"withoutSpecialChars"}, "id_form = 'transferForeign' and id_field = 'creditAccount'");
    }
    
}
