/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author ahernandez
 */
public class DB20180710_1203_4169 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("communications.mark.unread");
        insertActivity("communications.mark.unread", "com.technisys.omnichannel.client.activities.communications.MarkAsUnReadActivity", 
                "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
    
}
