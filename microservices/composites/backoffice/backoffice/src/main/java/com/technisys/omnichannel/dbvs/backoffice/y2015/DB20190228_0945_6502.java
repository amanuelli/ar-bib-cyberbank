/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20190228_0945_6502 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.users.update", "com.technisys.omnichannel.client.activities.administration.users.UpdateUserActivity", "administration.users", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.manage");
    }
}
