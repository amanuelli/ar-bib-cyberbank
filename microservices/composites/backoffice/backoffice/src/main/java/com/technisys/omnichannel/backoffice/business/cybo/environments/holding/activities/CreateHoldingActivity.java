/*
 *  Copyright 2021 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.backoffice.business.cybo.environments.holding.activities;

import com.technisys.omnichannel.backoffice.business.cybo.Response;
import com.technisys.omnichannel.backoffice.business.cybo.environments.holding.requests.CreateHoldingData;
import com.technisys.omnichannel.backoffice.domain.cybo.holdings.AdminUser;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.handlers.environment.EnvironmentHandler;
import com.technisys.omnichannel.core.*;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import static com.technisys.omnichannel.core.domain.Environment.ADMINISTRATION_SCHEME_MEDIUM;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import java.util.Arrays;


public class CreateHoldingActivity extends BOActivity {
    private static final Logger log = LoggerFactory.getLogger(CreateHoldingActivity.class);

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        Response<AdminUser> response = new Response<>(request);

        try {
            TransactionRequest tRequest = (TransactionRequest) request;
            CreateHoldingData data = (CreateHoldingData) tRequest.getTransactionData();
            String transactionID = request.getIdTransaction();
            String[] clientsID = data.getHoldingMembers().split(", ");

            Environment env = EnvironmentHandler.createHoldingEnvironment(transactionID, clientsID, data.getHoldingName(),
                    ADMINISTRATION_SCHEME_MEDIUM, 1);

            if (data.getSendAdminInvite()) {
                sendAdministratorInvitationCode(request, data, env.getIdEnvironment());
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (Exception ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        TransactionRequest tRequest = (TransactionRequest) request;
        Map<String, String> result = new HashMap<>();

        CreateHoldingData data = (CreateHoldingData) tRequest.getTransactionData();

        try {
            //validate holding name
            validateHoldingName(result, data);

            //validate holding members
            if (StringUtils.isBlank(data.getHoldingMembers()) || data.getHoldingMembers().split(",").length < 2) {
                result.put("@environment", "backoffice.environments.holdings.error.membersRequired");
            }

            // Administrator data
            if (data.getSendAdminInvite()) {
                validateAdminData(result, data);
            }
        } catch (IOException | BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    private void validateAdminData(Map<String, String> result, CreateHoldingData data)
            throws IOException, BackendConnectorException {

        if (StringUtils.isBlank(data.getAdminEmail())) {
            result.put("@user", "cybo.environmentsManagement.users.validations.document.empty");
        }

        if (StringUtils.isBlank(data.getAdminDocumentCountry())) {
            result.put("@user", "backoffice.environments.holdings.error.adminDocumentCountryRequired");
        }

        if (StringUtils.isBlank(data.getAdminDocumentType())) {
            result.put("@user", "backoffice.environments.holdings.error.adminDocumentTypeRequired");
        }

        if (StringUtils.isBlank(data.getAdminDocumentNumber())) {
            result.put("@user", "backoffice.environments.holdings.error.adminDocumentNumberRequired");
        } else if (!data.getAdminDocumentType().equals("PAS") && !data.getAdminDocumentNumber().matches("\\d+")){
            result.put("@user", "backoffice.environments.holdings.error.adminDocumentNumber.invalid");
        }

        if (result.isEmpty()){
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(data.getAdminDocumentCountry(), data.getAdminDocumentType(), data.getAdminDocumentNumber());

            if(Objects.isNull(user)) {
                user = CoreCustomerConnectorOrchestrator.read(null,
                        data.getAdminDocumentCountry(),
                        data.getAdminDocumentType(),
                        data.getAdminDocumentNumber(),
                        null);
            }

            if(!Objects.isNull(user)) {
                boolean docCountry = user.getDocumentCountry().equalsIgnoreCase(data.getAdminDocumentCountry());
                boolean docType = user.getDocumentType().equalsIgnoreCase(data.getAdminDocumentType());
                boolean docNumber = user.getDocumentNumber().equalsIgnoreCase(data.getAdminDocumentNumber());

                if (!(docCountry && docType && docNumber)){
                    result.put("@documentNumber", "backoffice.environments.holdings.error.emailDocumentNotMatch");
                }
            }
        }
    }

    private void sendAdministratorInvitationCode(IBRequest request, CreateHoldingData data, int environmentId)
            throws BackendConnectorException, IOException, ClassNotFoundException, InstantiationException,
            IllegalAccessException, MessagingException, ActivityException {

        String logMessage;
        InvitationCode invitationCode = new InvitationCode();
        invitationCode.setEmail(data.getAdminEmail());
        invitationCode.setFirstName(data.getAdminFirstName());
        invitationCode.setLastName(data.getAdminLastName());
        invitationCode.setMobileNumber(data.getAdminMobileNumber());
        invitationCode.setAccessType("administrator");
        invitationCode.setDocumentCountry(data.getAdminDocumentCountry());
        invitationCode.setDocumentNumber(data.getAdminDocumentNumber());
        invitationCode.setDocumentType(data.getAdminDocumentType());
        invitationCode.setIdEnvironment(environmentId);
        invitationCode.setAdministrationScheme(data.getAdministrationScheme());
        invitationCode.setSignatureQty(data.getSignatureQty());
        invitationCode.setStatus(InvitationCode.STATUS_NOT_USED);

        ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(),
                data.getAdminDocumentCountry(),
                data.getAdminDocumentType(),
                data.getAdminDocumentNumber(),
                null);

        invitationCode.setBackendUser(client != null);
        String sendChannel = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "invitation.notification.transport");
        invitationCode.setChannelSent(sendChannel);

        String invitationCodePlain = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
        InvitationCodesHandler.createInvitationCode(invitationCode, invitationCodePlain);

        // Send the invitation
        HashMap<String, String> fillers = new HashMap<>();
        String holdingName = ((CreateHoldingData) ((TransactionRequest) request).getTransactionData()).getHoldingName();
        fillers.put("ENVIRONMENT", holdingName);
        String subject = I18nFactory.getHandler().getMessage("administration.users.invite." + sendChannel + ".subject", request.getLang(), fillers);

        fillers.put("BASE_URL", ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, Constants.BASE_URL_KEY));

        String enrollmentUrl = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "administration.users.invite.enrollmentUrl", fillers);
        logMessage= MessageFormat.format("URL de inicio de invitacion: {0}", enrollmentUrl);
        log.info(logMessage);

        fillers.put("ENROLLMENT_URL", enrollmentUrl);

        fillers.put("FULL_NAME", invitationCode.getFirstName() + " " + invitationCode.getLastName());
        fillers.put("INVITATION_CODE", invitationCodePlain);
        fillers.put("EXPIRATION_DATE", invitationCode.getExpiration());
        String body = I18nFactory.getHandler().getMessage("administration.users.invite." + sendChannel + ".body", request.getLang(), fillers);

        switch (sendChannel) {
            case Constants.TRANSPORT_SMS:
                NotificationsHandlerFactory.getHandler().sendSMS(invitationCode.getMobileNumber(), body, true);
                break;
            case Constants.TRANSPORT_MAIL:
                NotificationsHandlerFactory.getHandler().sendEmails(subject, body, Arrays.asList(invitationCode.getEmail()), null, request.getLang(), true);
                break;
            default:
                throw new ActivityException(ReturnCodes.ERROR_SENDING_INVITATION, "No se encuentra bien configurado el transporte de la notificacion para invitaciones");
        }
    }

    private void validateHoldingName(Map<String, String> result, CreateHoldingData data) throws IOException {
        if (StringUtils.isBlank(data.getHoldingName())) {
            result.put("holdingName", "backoffice.environments.holdings.error.requiredName");
        } else {
            //look for an environment with the same name
            Environment environment;
            environment = Administration.getInstance().getEnvironment(data.getHoldingName());

            if (environment != null) {
                result.put("holdingName", "backoffice.environments.holdings.error.duplicatedName");
            }
        }
    }
}
