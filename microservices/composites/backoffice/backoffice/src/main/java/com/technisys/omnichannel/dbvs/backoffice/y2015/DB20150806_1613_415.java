/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150806_1613_415 extends DBVSUpdate {

    @Override
    public void up() {
        // Configuracion
        insertOrUpdateConfiguration("administration.users.invite.enabled", "false", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"}, "true|false");
        insertOrUpdateConfiguration("administration.users.invite.notInBackend.enabled", "true", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty"}, "true|false");
        insertOrUpdateConfiguration("invitation.permissions.roleList.backoffice", "administrator|transactions|readonly|login", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "string"});
        insertOrUpdateConfiguration("invitation.permissions.roleList.frontend", "transactions|readonly|login", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "string"});

    }
}
