/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author rschiappapietra
 */
public class DB20170725_1300_2765 extends DBVSUpdate {

    @Override
    public void up() {
        
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='widgets.notifications.max.simple.cards'");
        
        insertOrUpdateConfiguration("widgets.notifications.maxExpirations", 
                "5",
                ConfigurationGroup.NEGOCIO, "widgets",
                new String[]{"notEmpty", "integer"});
        update("configuration", new String[]{"channels"}, new String[]{"frontend"}, "id_field='widgets.notifications.maxExpirations'");
        
        deleteActivity("communications.mark.read");
        insertActivity("communications.mark.read", "com.technisys.omnichannel.client.activities.communications.MarkAsReadActivity",
                "desktop",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}
