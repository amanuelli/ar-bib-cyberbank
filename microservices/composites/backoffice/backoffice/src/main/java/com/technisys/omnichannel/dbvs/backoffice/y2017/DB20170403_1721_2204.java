/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: aMANNAZCA-2204
 *
 * @author dimoda
 */
public class DB20170403_1721_2204 extends DBVSUpdate {

    @Override
    public void up() {
    
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsValues;
        String idForm = "creditCardChangeCondition";
        String version = "1";
       
        Map<String, String> messages = new HashMap();
        
        /**** Insert Form - Solicitud de cambio de condiciones de tarjeta de crédito ****/    
        delete("forms", "id_form = '" + idForm + "'");
        String[] formsFields = new String[]{"id_form", "version", "enabled", "category", "type", "id_jbpm_process", "admin_option","id_activity", "templates_enabled", "drafts_enabled", "schedulable"};
        String[] formsFieldsValues = new String[]{idForm, version, "1", "creditcards", "process", "demo-1", "creditCards", null, "1", "1", "1"};
        insert("forms", formsFields, formsFieldsValues);
    
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', 'Solicitud de cambio de condiciones de tarjeta de crédito', TO_DATE('2017-04-03 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Solicitud de cambio de condiciones de tarjeta de crédito", date});    
        }
        
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

        /**** Insert field product selector - Tarjeta ****/
        formFieldsValues = new String[]{"creditCard", idForm, version, "productselector", "1", "TRUE", "TRUE", "creditCardSelector", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_ps", new String[]{"id_field", "id_form", "form_version", "display_type"}, new String[]{"creditCard", idForm, version, "field-normal"});
        insert("form_field_ps_product_types", new String[]{"id_field","id_form", "form_version", "id_product_type"}, new String[]{"creditCard", idForm, version, "TC"});
        insert("form_field_ps_permissions", new String[]{"id_field","id_form", "form_version", "id_permission"}, new String[]{"creditCard", idForm, version, "product.read"});  

        messages.put("creditCard.label", "Tarjeta");
        messages.put("creditCard.requiredError", "Debe seleccionar una tarjeta");
        
        /**** Insert field selector - Modificar ****/
        formFieldsValues = new String[]{"modifySelector", idForm, version, "selector", "2", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"modifySelector", idForm, version, "field-normal", "1"});
        messages.put("modifySelector.label", "Modificar");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"modifySelector", idForm, version, "lineaCredito"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"modifySelector", idForm, version, "programaPremios"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"modifySelector", idForm, version, "tipoTarjeta"});

        messages.put("modifySelector.option.lineaCredito", "Línea de crédito");
        messages.put("modifySelector.option.programaPremios", "Programa de premios");
        messages.put("modifySelector.option.tipoTarjeta", "Tipo de tarjeta");
        
        /**** Insert field amount - Monto ****/
        formFieldsValues = new String[]{"amount", idForm, version, "amount", "3", "value(modifySelector) == 'lineaCredito'", "value(modifySelector) == 'lineaCredito'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_amount", new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits", "use_for_total_amount"}, new String[]{"amount", idForm, version, "field-normal", "0", "0"});

        messages.put("amount.label", "Límite de compra");
        messages.put("amount.requiredError", "Debe ingresar un monto mayor a cero");
           
        /**** Insert field selector - Programa de Premios American Express ****/
        formFieldsValues = new String[]{"americanExpressAwardsProgram", idForm, version, "selector", "4", "value(modifySelector) == 'programaPremios'", "value(modifySelector) == 'programaPremios'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"americanExpressAwardsProgram", idForm, version, "field-normal", "1"});
        
        messages.put("americanExpressAwardsProgram.label", "Programa de Premios");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressAwardsProgram", idForm, version, "programa1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressAwardsProgram", idForm, version, "programa2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressAwardsProgram", idForm, version, "programa3"});

        messages.put("americanExpressAwardsProgram.option.programa1", "Programa 1");
        messages.put("americanExpressAwardsProgram.option.programa2", "Programa 2");
        messages.put("americanExpressAwardsProgram.option.programa3", "Programa 3");
     
    
        /**** Insert field selector - Tipo de Tarjeta American Express ****/
        formFieldsValues = new String[]{"americanExpressTypeCard", idForm, version, "selector", "5", "value(modifySelector) == 'tipoTarjeta'", "value(modifySelector) == 'tipoTarjeta'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"americanExpressTypeCard", idForm, version, "field-normal", "1"});
        messages.put("americanExpressTypeCard.label", "Tipo de Tarjeta");

        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporateBusinessExtra"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporateGold"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporateGreen"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporateMeeting"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporatePlatinum"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporatePurchasing"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "greenCard"});
        
        messages.put("americanExpressTypeCard.option.corporateBusinessExtra", "Corporate Business Extra");
        messages.put("americanExpressTypeCard.option.corporateGold", "Corporate Gold");
        messages.put("americanExpressTypeCard.option.corporateGreen", "Corporate Green");
        messages.put("americanExpressTypeCard.option.corporateMeeting", "Corporate Meeting");
        messages.put("americanExpressTypeCard.option.corporatePlatinum", "Corporate Platinum");
        messages.put("americanExpressTypeCard.option.corporatePurchasing", "Corporate Purchasing");
        messages.put("americanExpressTypeCard.option.greenCard", "Green Card");
        
        /**** Insert field selector - Forma de contacto ****/
        formFieldsValues = new String[]{"contactForm", idForm, version, "selector", "6", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value","show_blank_option"}, new String[]{"contactForm", idForm, version, "field-normal", "phone", "0"});
        messages.put("contactForm.label", "Forma de contacto");

        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"contactForm", idForm, version, "phone"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"contactForm", idForm, version, "email"});
             
        messages.put("contactForm.option.phone", "Teléfono");
        messages.put("contactForm.option.email", "E-mail");
         
        /**** Insert field text - Teléfono celular ****/
        formFieldsValues = new String[]{"phone", idForm, version, "text", "7", "value(contactForm) == 'phone'", "value(contactForm) == 'phone'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type","id_validation"}, new String[] {"phone", idForm, version, "9", "9", "field-normal","onlyNumbers"});
        messages.put("phone.label", "Teléfono");
        messages.put("phone.requiredError", "Debe ingresar un teléfono o dirección de e-mail");
       
       
        /**** Insert field text - E-mail ****/
        formFieldsValues = new String[]{"email", idForm, version, "text", "8", "value(contactForm) == 'email'", "value(contactForm) == 'email'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type","id_validation"}, new String[] {"email", idForm, version, "0", "50", "field-normal","email"});
        messages.put("email.label", "E-mail");
        messages.put("email.requiredError", "Debe ingresar un teléfono o dirección de e-mail");        
        
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
    }

}
