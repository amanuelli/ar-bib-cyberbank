/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

public class DB20181022_1326_5575 extends DBVSUpdate {

    @Override
    public void up() {
        insert("form_field_types", new String[]{"id_type"}, new String[]{"inputfile"});
        insertActivity("files.downloadStream", "com.technisys.omnichannel.client.activities.files.DownloadFileAsStreamActivity","", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");  

        insertOrUpdateConfiguration("form.inputFile.acceptedFileTypes", "image/png|image/jpeg|text/plain|application/pdf|application/msword|application/zip", ConfigurationGroup.NEGOCIO, "others", new String[]{});
        
    }

}
 