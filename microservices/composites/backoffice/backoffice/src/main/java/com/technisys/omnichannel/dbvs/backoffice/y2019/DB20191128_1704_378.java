/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dimoda
 */

public class DB20191128_1704_378 extends DBVSUpdate {

    @Override
    public void up() {

        Map<String, String> currencies = new HashMap<>();
        currencies.put("000", "UYU");
        currencies.put("111", "EUR");
        currencies.put("222", "USD");
        currencies.put("333", "GBP");
        currencies.put("444", "JYP");
        currencies.put("555", "UYI");
        currencies.put("666", "ARS");
        currencies.put("777", "BRL");
        currencies.put("888", "CLP");
        currencies.put("999", "CAD");

        List<String> tables = new ArrayList<>();
        tables.add("form_field_cc_amount_curr");
        tables.add("form_field_amount_currencies");
        tables.add("form_field_ps_currencies");
        tables.add("form_field_lpa_currencies");

        for (Map.Entry<String,String> currency: currencies.entrySet()) {
            for(String tableName : tables) {
                update(tableName, new String[]{"id_currency"}, new String[]{currency.getValue()}, "id_currency='" + currency.getKey() + "'");
            }
        }

    }

}