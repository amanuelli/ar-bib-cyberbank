/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 
 * @author hcastillo
 *
 */
public class DB20170627_1207_2490 extends DBVSUpdate {

    @Override
    public void up() {

        /** Mensajes para Frontend/Backoficce **/
        insertOrUpdateConfiguration("backoffice.forms.categories", "accounts|deposits|loans|creditcards|payments|transfers|request|others", ConfigurationGroup.NEGOCIO, null, new String[]{});

        /*** Se agrega nueva categoria */
        insert("adm_ui_categories", new String[] { "id_category", "ordinal" }, new String[] { "accounts", "900" });

        /** Datos del FORMULARIO ***/
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[] { "id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile" };
        String[] formFieldsMessages = new String[] { "id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value" };
        String[] formFieldsValues;
        String idForm = "accountOpening";
        String version = "1";

        /**** Insert Form - Apertura de Cuenta ****/
        String[] formsFields = new String[] { "id_form", "version", "enabled", "category", "type", "id_bpm_process", "admin_option", "id_activity", "templates_enabled", "drafts_enabled", "schedulable" };
        String[] formsFieldsValues = new String[] { idForm, version, "1", "request", "process", "demo:1:3", "accounts", null, "1", "1", "0" };
        insert("forms", formsFields, formsFieldsValues);

        String[] formMessageFields = new String[] { "id_message", "id_form", "version", "lang", "value", "modification_date" };

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version,lang,value, modification_date) " + " VALUES ('forms." + idForm + ".formName', '" + idForm + "','" + version + "','es',  'Solicitud de apertura de cuenta',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "es", "Solicitud de apertura de cuenta", date });
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "pt", "Aplicação de abertura de conta", date });
            insert("form_messages", formMessageFields, new String[] { "forms." + idForm + ".formName", idForm, version, "en", "Account opening request", date });
        }
        /*** ASIGNAMOS PERMISOS AL FORMULARIO ***/
        insert("permissions", new String[] { "id_permission" }, new String[] { "client.form." + idForm + ".send" });
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});

        String idField;

        /**** Insert field selector - tipoDeCuenta ****/
        idField = "accountType";
        formFieldsValues = new String[] { idField, idForm, version, "selector", "1", "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as" }, new String[] { idField, idForm, version, "field-big", "0", "combo" });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Tipo de Cuenta')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Account Type')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Tipo de Conta')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'El tipo de cuenta es requerido')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Account type is required')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'O tipo de contabilidade requerida')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Tipo de Cuenta" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Account Type" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Tipo de Conta" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "El tipo de cuenta es requerido" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "en", idField, idForm, version, date, "Account type is required" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "pt", idField, idForm, version, date, "O tipo de contabilidade requerida" });
        }
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { idField, idForm, version, "CA" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { idField, idForm, version, "CC" });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.CA','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Caja de Ahorros')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.CA','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Savings bank')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.CA','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Poupança')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.CC','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Cuenta Corriente')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.CC','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Current account')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.CC','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Conta corrente')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.CA", "es", idField, idForm, version, date, "Caja de Ahorros" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.CA", "en", idField, idForm, version, date, "Savings bank" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.CA", "pt", idField, idForm, version, date, "Poupança" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.CC", "es", idField, idForm, version, date, "Cuenta Corriente" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.CC", "en", idField, idForm, version, date, "Current account" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.CC", "pt", idField, idForm, version, date, "Conta corrente" });
        }
        /**** Insert field selector - monedas ****/
        idField = "currencies";
        formFieldsValues = new String[] { idField, idForm, version, "selector", "2", "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as" }, new String[] { idField, idForm, version, "field-big", "0", "combo" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Monedas')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Currencies')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'moedas')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'El campo moneda es requerido')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'The currency field is required')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'O campo de moeda é necessária')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Monedas" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Currencies" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "moedas" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "El campo moneda es requerido" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "en", idField, idForm, version, date, "The currency field is required" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "pt", idField, idForm, version, date, "O campo de moeda é necessária" });
        }
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { idField, idForm, version, "Euros" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { idField, idForm, version, "Pesos" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { idField, idForm, version, "Dolar" });
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Pesos','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Pesos')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Pesos','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Pesos')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Pesos','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Pesos')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Euros','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Euros')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Euros','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Euros')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Euros','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Euros')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Dolar','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Dolar')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Dolar','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Dolar')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Dolar','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Dolar')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Pesos", "es", idField, idForm, version, date, "Pesos" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Pesos", "en", idField, idForm, version, date, "Pesos" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Pesos", "pt", idField, idForm, version, date, "Pesos" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Euros", "es", idField, idForm, version, date, "Euros" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Euros", "en", idField, idForm, version, date, "Euros" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Euros", "pt", idField, idForm, version, date, "Euros" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Dolar", "es", idField, idForm, version, date, "Dolar" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Dolar", "en", idField, idForm, version, date, "Dolar" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Dolar", "pt", idField, idForm, version, date, "Dolar" });
        }

        /**** Insert field selector - sucursalCuenta ****/
        idField = "accountBranch";
        formFieldsValues = new String[] { idField, idForm, version, "branchlist", "3", "TRUE", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_branchlist", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value" }, new String[] { idField, idForm, version, "field-big", null });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Sucursal de la cuenta')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Account branch')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Conta ramo')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'La sucursal es requerida')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'The branch is required')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'O ramo é necessária')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Sucursal de la cuenta" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Account branch" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Conta ramo" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "La sucursal es requerida" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "en", idField, idForm, version, date, "The branch is required" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "pt", idField, idForm, version, date, "O ramo é necessária" });
        }
        /**** Insert field selector - tarjetaDebito ****/
        idField = "debitCard";
        formFieldsValues = new String[] { idField, idForm, version, "selector", "4", "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[] { "id_field", "id_form", "form_version", "display_type", "show_blank_option", "render_as" }, new String[] { idField, idForm, version, "field-smaller", "0", "combo" });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Tarjeta de débito')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Debit Card')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Cartão de débito')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Tarjeta de débito" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Debit Card" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Cartão de débito" });
        }
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { idField, idForm, version, "No" });
        insert("form_field_selector_options", new String[] { "id_field", "id_form", "form_version", "value" }, new String[] { idField, idForm, version, "Si" });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Si','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Si')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Si','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Yes')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.Si','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'SeSe')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.No','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'No')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.No','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'No')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".option.No','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Não')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Si", "es", idField, idForm, version, date, "Si" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Si", "en", idField, idForm, version, date, "Yes" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.Si", "pt", idField, idForm, version, date, "SeSe" });

            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.No", "es", idField, idForm, version, date, "No" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.No", "en", idField, idForm, version, date, "No" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".option.No", "pt", idField, idForm, version, date, "Não" });
        }
        /**** Insert field selector - lugarDeRetiro ****/
        idField = "placeOfRetreat";
        formFieldsValues = new String[] { idField, idForm, version, "branchlist", "5", "value(tarjetaDebito) == 'Si'", "TRUE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_branchlist", new String[] { "id_field", "id_form", "form_version", "display_type", "default_value", "show_other_option" }, new String[] { idField, idForm, version, "field-big", "", "1" });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".help','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".hint','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),null)");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Lugar de retiro')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Place of retreat')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Retiro')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'El lugar de retiro es requerido')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'The place of withdrawal is required')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".requiredError','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'O retiro é necessária')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".showOtherText','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Entrega a domicilio')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".showOtherText','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Home delivery')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".showOtherText','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('"
                    + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Entrega a domicilio')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".help", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".hint", "es", idField, idForm, version, date, null });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Lugar de retiro" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Place of retreat" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Retiro" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "es", idField, idForm, version, date, "El lugar de retiro es requerido" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "en", idField, idForm, version, date, "The place of withdrawal is required" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".requiredError", "pt", idField, idForm, version, date, "O retiro é necessária" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".showOtherText", "es", idField, idForm, version, date, "Entrega a domicilio" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".showOtherText", "en", idField, idForm, version, date, "Home delivery" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".showOtherText", "pt", idField, idForm, version, date, "Entrega a domicilio" });
        }
        /**** Insert fied horizontalrule - Linea separadora ****/
        formFieldsValues = new String[] { "horizontalrule", idForm, version, "horizontalrule", "6", "TRUE", "FALSE", "default", "1" };
        insert("form_fields", formFields, formFieldsValues);
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + ".horizontalrule.label','es','horizontalrule','" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + "horizontalrule.label", "es", "horizontalrule", idForm, version, date, "" });
        }

        /**** Insert field selector - terminosCondiciones ****/
        idField = "termsConditions";
        formFieldsValues = new String[] { idField, idForm, version, "termsandconditions", "7", "TRUE", "TRUE", "default", "1" };

        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[] { "id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label" }, new String[] { "termsConditions", idForm, version, "field-big", "1", "1" });

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','es','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Términos y condiciones')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','en','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Terms and Conditions')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".label','pt','" + idField + "', '" + idForm + "','" + version + "',  TO_DATE('" + date
                    + "', 'YYYY-MM-DD HH24:MI:SS'),'Termos e Condições')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".showAcceptOptionText','es','" + idField + "', '" + idForm + "','" + version
                    + "',  TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Términos y condiciones')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".showAcceptOptionText','en','" + idField + "', '" + idForm + "','" + version
                    + "',  TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Terms and Conditions')");
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) " + " VALUES ('fields." + idForm + "." + idField + ".showAcceptOptionText','pt','" + idField + "', '" + idForm + "','" + version
                    + "',  TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),'Termos e Condições')");

            customSentence(
                    DBVS.DIALECT_ORACLE,
                    "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) "
                            + " VALUES ('fields."
                            + idForm
                            + "."
                            + idField
                            + "..termsAndConditions','es','"
                            + idField
                            + "', '"
                            + idForm
                            + "','"
                            + version
                            + "',  TO_DATE('"
                            + date
                            + "', 'YYYY-MM-DD HH24:MI:SS'),'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.')");
            customSentence(
                    DBVS.DIALECT_ORACLE,
                    "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) "
                            + " VALUES ('fields."
                            + idForm
                            + "."
                            + idField
                            + "..termsAndConditions','en','"
                            + idField
                            + "', '"
                            + idForm
                            + "','"
                            + version
                            + "',  TO_DATE('"
                            + date
                            + "', 'YYYY-MM-DD HH24:MI:SS'),'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.')");
            customSentence(
                    DBVS.DIALECT_ORACLE,
                    "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, modification_date, value ) "
                            + " VALUES ('fields."
                            + idForm
                            + "."
                            + idField
                            + "..termsAndConditions','pt','"
                            + idField
                            + "', '"
                            + idForm
                            + "','"
                            + version
                            + "',  TO_DATE('"
                            + date
                            + "', 'YYYY-MM-DD HH24:MI:SS'),'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "es", idField, idForm, version, date, "Términos y condiciones" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "en", idField, idForm, version, date, "Terms and Conditions" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".label", "pt", idField, idForm, version, date, "Termos e Condições" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".showAcceptOptionText", "es", idField, idForm, version, date, "Términos y condiciones" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".showAcceptOptionText", "en", idField, idForm, version, date, "Terms and Conditions" });
            insert("form_field_messages", formFieldsMessages, new String[] { "fields." + idForm + "." + idField + ".showAcceptOptionText", "pt", idField, idForm, version, date, "Termos e Condições" });
            insert("form_field_messages",
                    formFieldsMessages,
                    new String[] {
                            "fields." + idForm + "." + idField + ".termsAndConditions",
                            "es",
                            idField,
                            idForm,
                            version,
                            date,
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." });
            insert("form_field_messages",
                    formFieldsMessages,
                    new String[] {
                            "fields." + idForm + "." + idField + ".termsAndConditions",
                            "en",
                            idField,
                            idForm,
                            version,
                            date,
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." });
            insert("form_field_messages",
                    formFieldsMessages,
                    new String[] {
                            "fields." + idForm + "." + idField + ".termsAndConditions",
                            "pt",
                            idField,
                            idForm,
                            version,
                            date,
                            "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum." });
        }
    }

}
