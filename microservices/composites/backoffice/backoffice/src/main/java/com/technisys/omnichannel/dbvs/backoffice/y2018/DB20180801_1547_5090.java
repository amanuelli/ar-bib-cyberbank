/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-5090
 *
 * @author fpena
 */
public class DB20180801_1547_5090 extends DBVSUpdate {

    @Override
    public void up() {
        delete("widgets", "id='notifications'");
    }
}