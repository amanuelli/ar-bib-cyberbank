/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150612_1052_419 extends DBVSUpdate {

    @Override
    public void up() {

        // Sasfeway Config Keys
        insertOrUpdateConfiguration("safeway.dateFormat", "dd/MM/yyyy HH:mm:ss", ConfigurationGroup.SEGURIDAD, null, new String[]{});

        insertOrUpdateConfiguration("safeway.secretKeys.encryptionAlgorithmForCipher", "AES/ECB/PKCS5Padding", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.secretKeys.encryptionAlgorithm", "AES", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.secretKeys.encryptionKey", "ENCRYPTION_KEY", ConfigurationGroup.SEGURIDAD, null, new String[]{});

        insertOrUpdateConfiguration("safeway.clientSystem", "CLIENT_SYSTEM", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.clientSystemPassword", "CLIENT_SYSTEM_PASSWORD", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.domainType", "DOMAIN_TYPE", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.keyType", "KEY_TYPE", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        
        insertOrUpdateConfiguration("safeway.secretKeys.url", "http://safeway-hostname:port/safeway/com/technisys/safeway/SecretKeys.jws?wsdl", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.secretKeys.validForXYears", "50", ConfigurationGroup.SEGURIDAD, null, new String[]{});

        insertOrUpdateConfiguration("safeway.otps.url", "http://safeway-hostname:port/safeway/com/technisys/safeway/OTPs.jws?wsdl", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.otps.applicationName", "", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.otps.info", "", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.otps.softTokenValidationType", "OTPMOBILE", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.otps.tokenValidationType", "VASCO", ConfigurationGroup.SEGURIDAD, null, new String[]{});
        insertOrUpdateConfiguration("safeway.otps.validForXYears", "50", ConfigurationGroup.SEGURIDAD, null, new String[]{});

    }
}
