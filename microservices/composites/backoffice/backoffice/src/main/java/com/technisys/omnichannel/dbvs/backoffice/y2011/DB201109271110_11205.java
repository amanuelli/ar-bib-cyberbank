/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201109271110_11205 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"mandatory"};

        String[] fieldValues = new String[]{"0"};

        update("form_fields", fieldNames, fieldValues, "id_field='apartamentoDatosLaboralesAdicional' AND id_form=17");
        update("form_fields", fieldNames, fieldValues, "id_field='localidadDepartamentoDatosLaboralesAdicional' AND id_form=17");
        update("form_fields", fieldNames, fieldValues, "id_field='profesionDatosLaboralesAdicional' AND id_form=17");
        update("form_fields", fieldNames, fieldValues, "id_field='cargoFuncionDatosLaboralesAdicional' AND id_form=17");
        update("form_fields", fieldNames, fieldValues, "id_field='faxAdicional' AND id_form=17");
        update("form_fields", fieldNames, fieldValues, "id_field='sexoAdicional' AND id_form=17");

    }
}