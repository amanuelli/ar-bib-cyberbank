/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Related issue: MANNAZCA-2885
 *
 * @author rmedina
 */
public class DB20170803_1211_2885 extends DBVSUpdate {

    @Override
    public void up() {
        
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFieldsMessages = new String[] {"modification_date", "value" };
        String idForm = "presentationOfDocumentsUnderCollection";        
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "UPDATE form_field_messages "
                    + "SET modification_date = TO_DATE('" + date + "', 'YYYY-MM-DD HH24:MI:SS'),"
                    + " value = 'Debe ingresar la dirección y el nombre completo del banco'"
                    + "WHERE id_message = 'fields." + idForm + ".sendTheDocumentationToTheBank.requiredError' AND lang = 'es'"); 
        }else{
            update("form_field_messages", formFieldsMessages, 
                    new String[]{ date , "Debe ingresar la dirección y el nombre completo del banco."}, 
                    "id_message = 'fields." + idForm + ".sendTheDocumentationToTheBank.requiredError' AND lang = 'es'");
        
        }
    }
}