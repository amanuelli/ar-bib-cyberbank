/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author salva
 */
public class DB20150820_1557_514 extends DBVSUpdate {

    @Override
    public void up() {
        //Borrmos el formulario payLoan por si existe, y creamos el nuevo que es Pago de prestamo
        delete("form_field_messages", "id_form = 'payLoan'");
        delete("form_messages", "id_form = 'payLoan'");
        delete("form_field_ps", "id_form = 'payLoan'");
        delete("form_field_loanpaymentamount", "id_form = 'payLoan'");
        delete("form_field_amount", "id_form = 'payLoan'");
        delete("form_field_ps_permissions", "id_form = 'payLoan'");
        delete("form_field_ps_product_types", "id_form = 'payLoan'");
        delete("form_field_text", "id_form = 'payLoan'");
        delete("forms", "id_form = 'payLoan'");
        
        String[] formField = new String[]{"id_form", "category", "enabled", "type", "id_activity", "version", "schedulable"};
        String[] formFieldValues = new String[]{"payLoan", "loans", "1", "activity", "pay.loan.send", "1", "1"};
        insert("forms", formField, formFieldValues);
                
        formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        formFieldValues = new String[]{"forms.payLoan.formName", "payLoan", "1", "es", "Pago de préstamo", "2015-01-01 12:00:00"};
        
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2]  + "', '" + formFieldValues[3]  + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert ("form_messages", formField, formFieldValues);
        }
        
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required"};
        String[] formFieldsValues = new String[]{"debitAccount", "payLoan", "1", "productselector", "1", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"loan", "payLoan", "1", "productselector", "2", "TRUE", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"loanPayment", "payLoan", "1", "loanpaymentamount", "4", "hidden(otherLoanNumber)", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"otherLoanAmount", "payLoan", "1", "amount", "5", "shown(otherLoanNumber)", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"otherLoanNumber", "payLoan", "1", "text", "3", "value(loan) == 'other'", "TRUE"};
        insert("form_fields", formFields, formFieldsValues);
        
        formFieldsValues = new String[]{"reference", "payLoan", "1", "text", "6", "TRUE", "FALSE"};
        insert("form_fields", formFields, formFieldsValues);
        
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option"};
        formFieldsValues = new String[]{"debitAccount", "payLoan", "1", "field-big", "0"};
        insert("form_field_ps", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "show_other_option", "show_other_permission", "show_other_id_ps"};
        formFieldsValues = new String[]{"loan", "payLoan", "1", "field-big", "1", "pay.loan.thirdParties", "debitAccount"};
        insert("form_field_ps", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        formFieldsValues = new String[]{"debitAccount", "payLoan", "1", "pay.loan"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_permission"};
        formFieldsValues = new String[]{"loan", "payLoan", "1", "product.read"};
        insert("form_field_ps_permissions", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "payLoan", "1", "CA"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"debitAccount", "payLoan", "1", "CC"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"loan", "payLoan", "1", "PA"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "id_product_type"};
        formFieldsValues = new String[]{"loan", "payLoan", "1", "PI"};
        insert("form_field_ps_product_types", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits", "depends_on_product"};
        formFieldsValues = new String[]{"loanPayment", "payLoan", "1", "field-small", "1", "loan"};
        insert("form_field_loanpaymentamount", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "display_type", "control_limits"};
        formFieldsValues = new String[]{"otherLoanAmount", "payLoan", "1", "field-small", "1"};
        insert("form_field_amount", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        formFieldsValues = new String[]{"otherLoanNumber", "payLoan", "1", "0", "30", "field-normal", "onlyNumbers"};
        insert("form_field_text", formFields, formFieldsValues);
        
        formFields = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        formFieldsValues = new String[]{"reference", "payLoan", "1", "0", "50", "field-big"};
        insert("form_field_text", formFields, formFieldsValues);
        
        //Solo doy de alta español, y las key que tengan valor
        Map<String, String> messages = new HashMap();
        messages.put("debitAccount.label","Cuenta débito");
        messages.put("loan.label","Préstamo");
        messages.put("loan.showOtherText","Otro préstamo");
        messages.put("loanPayment.label","Tipo de pago");
        messages.put("loanPayment.labelPartialPay","Importe a pagar");
        messages.put("loanPayment.labelTotalPay","Monto total a pagar");
        messages.put("loanPayment.partialPay","Pago parcial");
        messages.put("loanPayment.totalPay","Pago total");
        messages.put("otherLoanAmount.label","Importe a pagar");
        messages.put("otherLoanNumber.label","Número de préstamo");
        messages.put("reference.label","Referencia");
        
        formFields = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        for (String key : messages.keySet()){
            formFieldsValues = new String[]{"fields.payLoan." + key, "es", key.substring(0, key.indexOf(".")), "payLoan", "1", messages.get(key), "2015-01-01 12:00:00"};
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3]  + "', '" + formFieldsValues[4] + "', '" + formFieldsValues[5] + "', TO_DATE('2012-01-01 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFields, formFieldsValues);
            }
        }
    }
}
