/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author agonzalez
 */

public class DB20190913_1756_7934 extends DBVSUpdate {

    private final String FORM_FIELD_MESSAGES = "form_field_messages";
    private final String[] FIELD_VALUE = new String[]{"value"};


    @Override
    public void up() {

        //PT UPDATE
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Quantidade de cotas"}, "id_field ='amountOfFees' and lang ='pt' and id_message ='fields.requestLoan.amountOfFees.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Termos e condições"}, "value like 'Termos e Condições'");//UPDATES ALL OF "Termos e Condições" as it was found that there were more than one message with the same issue (Bug -MANNAZCA-7934).
        //ES UPDATE
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Autorizado a retirar o recibir chequera"}, "id_field ='tituloAutorizadoARetirar' and lang ='es' and id_message ='fields.requestCheckbook.tituloAutorizadoARetirar.label'");
        update(FORM_FIELD_MESSAGES, FIELD_VALUE, new String[]{"Autorizado a retirar o recibir chequera"}, "id_field ='personalDataTitle' and lang ='es' and id_message ='fields.requestOfManagementCheck.personalDataTitle.label'");




    }

}