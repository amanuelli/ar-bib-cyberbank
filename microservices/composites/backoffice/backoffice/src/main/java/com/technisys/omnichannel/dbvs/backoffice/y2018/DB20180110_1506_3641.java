/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3641
 *
 * @author isilveira
 */
public class DB20180110_1506_3641 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_messages", new String[] { "value" }, new String[] { "Emails to notify the transfer. Remember to enter them separated by commas." }, "id_form='surveyOfDiscrepancies' AND id_field='listaEmailsNotificacion' AND id_message='fields.surveyOfDiscrepancies.listaEmailsNotificacion.help' AND lang='en'");
        update("form_field_messages", new String[] { "value" }, new String[] { "Emails a notificar el envío de la transferencia. Recuerde ingresarlos separados por coma." }, "id_form='surveyOfDiscrepancies' AND id_field='listaEmailsNotificacion' AND id_message='fields.surveyOfDiscrepancies.listaEmailsNotificacion.help' AND lang='es'");
        update("form_field_messages", new String[] { "value" }, new String[] { "Emails para notificar o envio da transferência. Lembre-se de inseri-los separados por vírgulas." }, "id_form='surveyOfDiscrepancies' AND id_field='listaEmailsNotificacion' AND id_message='fields.surveyOfDiscrepancies.listaEmailsNotificacion.help' AND lang='pt'");
    }
}