/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-4142
 *
 * @author tech-uy
 */
public class DB20180706_1337_4142 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("forms.bankselector.maxRecords", "10", ConfigurationGroup.TECNICAS, "bankselector", new String[]{"notEmpty"});
    }
}