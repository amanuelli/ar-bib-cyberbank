/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author diego
 */
public class DB20150916_1409_893 extends DBVSUpdate {

    @Override
    public void up() {
        //Borrmos el permiso por si ya existía
        delete("permissions", "id_permission = 'client.form.4.send'");
        delete("permissions_credentials_groups", "id_permission = 'client.form.4.send'");
        
        String[] formFields = new String[]{"id_permission"};
        String[] formFieldsValues = new String[]{"client.form.4.send"};
        insert("permissions", formFields, formFieldsValues);
        
        formFields = new String[]{"id_permission", "id_credential_group"};
        formFieldsValues = new String[]{"client.form.4.send", "accessToken-pin"};
        insert("permissions_credentials_groups", formFields, formFieldsValues);
    }
}
