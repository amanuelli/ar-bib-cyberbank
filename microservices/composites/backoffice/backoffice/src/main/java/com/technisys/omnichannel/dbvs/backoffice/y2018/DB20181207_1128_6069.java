/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author fpena
 */

public class DB20181207_1128_6069 extends DBVSUpdate {

    @Override
    public void up() {
        insert("form_field_types", new String[]{"id_type"}, new String[]{"multilinefile"});
        insertOrUpdateConfiguration("form.multilinefile.acceptedFileTypes", "text/plain", ConfigurationGroup.NEGOCIO, "others", new String[]{});
        insertOrUpdateConfiguration("field.subType.multilinefile", "salaryPayment", ConfigurationGroup.OTHER, "others", new String[]{});
    }

}