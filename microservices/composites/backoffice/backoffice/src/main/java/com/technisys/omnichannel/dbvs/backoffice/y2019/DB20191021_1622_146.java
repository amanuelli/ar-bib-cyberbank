/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

import java.util.HashMap;
import java.util.Map;

/**
 * @author dimoda
 */

public class DB20191021_1622_146 extends DBVSUpdate {

    @Override
    public void up() {
        Map<String,String> countryMap = new HashMap<>();

        countryMap.put("ALB", "Albania");
        countryMap.put("DEU", "Alemanha");
        countryMap.put("AND", "Andorra");
        countryMap.put("AIA", "Anguilla");
        countryMap.put("ATG", "Antigua e Barbuda");
        countryMap.put("ANT", "Antilhas Neerlandesas");
        countryMap.put("SAU", "Arábia Saudita");
        countryMap.put("DZA", "Argélia");
        countryMap.put("ARG", "Argentina");
        countryMap.put("AUS", "Austrália");
        countryMap.put("AUT", "Áustria");
        countryMap.put("BHS", "Bahamas");
        countryMap.put("BHR", "Bahrein");
        countryMap.put("BEL", "Bélgica");
        countryMap.put("BOL", "Bolívia");
        countryMap.put("BIH", "Bósnia e Herzegovina");
        countryMap.put("BRA", "BRAZIL");
        countryMap.put("BRN", "Brunei");
        countryMap.put("BGR", "Bulgária");
        countryMap.put("BFA", "Burkina Faso");
        countryMap.put("BDI", "Burundi");
        countryMap.put("BTN", "Butão");
        countryMap.put("CAN", "Canadá");
        countryMap.put("CHL", "Chile");
        countryMap.put("CHN", "China");
        countryMap.put("COL", "Colômbia");
        countryMap.put("PRK", "Coreia do Norte");
        countryMap.put("KOR", "Coreia do Sul");
        countryMap.put("CRI", "Costa Rica");
        countryMap.put("CUB", "Cuba");
        countryMap.put("DNK", "Dinamarca");
        countryMap.put("DMA", "Dominica, Ilha");
        countryMap.put("ECU", "Equador");
        countryMap.put("EGY", "Egito");
        countryMap.put("SLV", "El Salvador");
        countryMap.put("ARE", "Emirados Árabes Unidos");
        countryMap.put("ESP", "Espanha");
        countryMap.put("USA", "Estados Unidos");
        countryMap.put("PHL", "Filipinas");
        countryMap.put("FIN", "Finlândia");
        countryMap.put("FRA", "França");
        countryMap.put("GEO", "Geórgia");
        countryMap.put("GRD", "Granada");
        countryMap.put("GRC", "Grécia");
        countryMap.put("GRL", "Groenlândia");
        countryMap.put("GTM", "Guatemala");
        countryMap.put("HTI", "Haiti");
        countryMap.put("HND", "Honduras");
        countryMap.put("HKG", "Hong Kong");
        countryMap.put("HUN", "Hungria");
        countryMap.put("IND", "Índia");
        countryMap.put("IDN", "Indonésia");
        countryMap.put("IRL", "Irlanda");
        countryMap.put("ISL", "Islândia");
        countryMap.put("CYM", "Cayman");
        countryMap.put("FRO", "Feroe");
        countryMap.put("MNP", "Marianas Setentrionais");
        countryMap.put("MHL", "Marshall");
        countryMap.put("ISR", "Israel");
        countryMap.put("ITA", "Itália");
        countryMap.put("JAM", "Jamaica");
        countryMap.put("JPN", "Japão");
        countryMap.put("JOR", "Jordânia");
        countryMap.put("KWT", "Coveite (Kuwait)");
        countryMap.put("LVA", "Letônia");
        countryMap.put("LBR", "Libéria");
        countryMap.put("LBY", "Líbia");
        countryMap.put("LTU", "Lituânia");
        countryMap.put("LUX", "Luxemburgo");
        countryMap.put("MYS", "Malásia");
        countryMap.put("MDV", "Maldivas");
        countryMap.put("MLT", "Malta");
        countryMap.put("MUS", "Mauricio");
        countryMap.put("MEX", "México");
        countryMap.put("MCO", "Mônaco");
        countryMap.put("MNG", "Mongólia");
        countryMap.put("NPL", "Nepal");
        countryMap.put("NIC", "Nicarágua");
        countryMap.put("NFK", "Norfolk");
        countryMap.put("NOR", "Noruega");
        countryMap.put("NZL", "Nova Zelândia");
        countryMap.put("OMN", "Omã");
        countryMap.put("NLD", "Países Baixos (Holanda)");
        countryMap.put("PSE", "Palestina");
        countryMap.put("PAN", "Panamá");
        countryMap.put("PRY", "Paraguai");
        countryMap.put("PER", "Peru");
        countryMap.put("POL", "Polônia");
        countryMap.put("PRT", "Portugal");
        countryMap.put("PRI", "Puerto Rico");
        countryMap.put("GRB", "Reino Unido");
        countryMap.put("DOM", "República Dominicana");
        countryMap.put("ROU", "Romênia");
        countryMap.put("RUS", "Rússia");
        countryMap.put("WSM", "Samoa");
        countryMap.put("SMR", "San Marino");
        countryMap.put("SHN", "Santa Helena");
        countryMap.put("SYR", "Síria");
        countryMap.put("LKA", "Sri Lanka");
        countryMap.put("SWZ", "Suazilândia");
        countryMap.put("ZAF", "África do Sul");
        countryMap.put("SWE", "Suécia");
        countryMap.put("CHE", "Suíça");
        countryMap.put("SUR", "Suriname");
        countryMap.put("THA", "Tailândia");
        countryMap.put("TWN", "Formosa (Taiwan)");
        countryMap.put("TUN", "Tunísia");
        countryMap.put("TUR", "Turquia");
        countryMap.put("UKR", "Ucrânia");
        countryMap.put("URY", "Uruguai");
        countryMap.put("VEN", "Venezuela");
        countryMap.put("VNM", "Vietnã");
        countryMap.put("ZMB", "Zâmbia");

        for (Map.Entry<String, String> entry : countryMap.entrySet()){
            update("country_codes",
                    new String[]{"core_code"} ,
                    new String[]{entry.getValue()},
                    "alpha_3='" + entry.getKey() + "'");
        }

    }

}