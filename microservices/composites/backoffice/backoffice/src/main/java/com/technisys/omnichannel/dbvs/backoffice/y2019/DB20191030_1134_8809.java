/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author mgiglio
 */

public class DB20191030_1134_8809 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("session.login.step2b",
                "com.technisys.omnichannel.client.activities.session.ListEnvironmentActivity",
                "login",
                ActivityDescriptor.AuditLevel.Full,
                ClientActivityDescriptor.Kind.Other,
                null
        );
        insertActivity("session.login.step3.oauth",
                "com.technisys.omnichannel.client.activities.session.OauthSelectEnvironmentActivity",
                "login",
                ActivityDescriptor.AuditLevel.Full,
                ClientActivityDescriptor.Kind.Other,
                null
        );
        insertActivity("session.login.step4.oauth",
                "com.technisys.omnichannel.client.activities.session.OauthAcceptGeneralConditionsActivity",
                "login",
                ActivityDescriptor.AuditLevel.Full,
                ClientActivityDescriptor.Kind.Other,
                null
        );
    }

}