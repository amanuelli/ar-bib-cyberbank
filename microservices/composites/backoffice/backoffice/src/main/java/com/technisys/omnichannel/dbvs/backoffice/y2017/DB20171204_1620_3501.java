/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3453
 *
 * @author msouza
 */
public class DB20171204_1620_3501 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("frontend.chat.zendesk.bubbleEnable", "false", ConfigurationGroup.TECNICAS, "zendesk", new String[]{"notEmpty"});
        update("configuration", new String[]{"channels","possible_values"}, new String[]{"frontend","true|false"}, "id_field='frontend.chat.zendesk.bubbleEnable'");
       
    }
}
