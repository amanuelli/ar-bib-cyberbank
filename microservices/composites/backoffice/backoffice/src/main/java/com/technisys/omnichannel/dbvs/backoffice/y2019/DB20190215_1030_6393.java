/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-6393
 *
 * @author mcheveste
 */
public class DB20190215_1030_6393 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "salaryPayment";
        String VERSION = "1";
        String CURRENT_DATE = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form",
                "form_version", "value", "modification_date"};
        Map<String, String> messages = new HashMap<>();
        Map<String, String> messagesEs = new HashMap<>();

        insert("form_field_types", new String[]{"id_type"}, new String[]{"transactionlines"});

        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                        "required", "visible_in_mobile", "sub_type"},
                new String[]{"uploadBy", idForm, VERSION, "selector", "5",
                        "TRUE", "TRUE", "1", "default"});
        insert("form_field_selector",
                new String[]{"id_field", "id_form", "form_version", "display_type", "default_value",
                        "render_as"},
                new String[]{"uploadBy", idForm, VERSION, "field-normal", "file",
                        "radio"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"uploadBy", idForm, VERSION, "file"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"},
                new String[]{"uploadBy", idForm, VERSION, "inputManually"});
        messages.put("uploadBy.label", "Payments list management");
        messages.put("uploadBy.option.file", "Upload a file");
        messages.put("uploadBy.option.inputManually", "Input manually");
        messagesEs.put("uploadBy.label", "Administración de lista de pagos");
        messagesEs.put("uploadBy.option.file", "Subir un archivo");
        messagesEs.put("uploadBy.option.inputManually", "Escribir manualmente");

        insert("form_fields",
                new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible",
                        "required", "visible_in_mobile", "sub_type"},
                new String[]{"inputManually", idForm, VERSION, "transactionlines", "6",
                        "value(uploadBy) == 'inputManually'", "value(uploadBy) == 'inputManually'", "1", "default"});

        update("form_fields", new String[]{"ordinal", "visible"}, new String[]{"6", "value(uploadBy) == 'file'"},
                "id_form='" + idForm + "' and id_field='fileSection'");
        update("form_fields", new String[]{"ordinal", "visible"}, new String[]{"7", "value(uploadBy) == 'file'"},
                "id_form='" + idForm + "' and id_field='sampleFile'");
        update("form_fields", new String[]{"ordinal", "visible", "required"}, new String[]{"8", "value(uploadBy) == 'file'", "value(uploadBy) == 'file'"},
                "id_form='" + idForm + "' and id_field='file'");

        messages.keySet().stream().map((key) -> new String[]{"fields." + idForm + "." + key, "en",
                key.substring(0, key.indexOf(".")), idForm, VERSION, messages.get(key), CURRENT_DATE}).forEachOrdered((formFieldsValues) -> {
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE,
                        "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                                + " VALUES ('" + formFieldsValues[0] + "', '"
                                + formFieldsValues[1] + "', '" + formFieldsValues[2]
                                + "', '" + formFieldsValues[3] + "', '"
                                + formFieldsValues[4] + "','" + formFieldsValues[5]
                                + "', TO_DATE('2018-07-27 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        });

        messagesEs.keySet().stream().map((key) -> new String[]{"fields." + idForm + "." + key, "es",
                key.substring(0, key.indexOf(".")), idForm, VERSION, messagesEs.get(key), CURRENT_DATE}).forEachOrdered((formFieldsValues) -> {
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE,
                        "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                                + " VALUES ('" + formFieldsValues[0] + "', '"
                                + formFieldsValues[1] + "', '" + formFieldsValues[2]
                                + "', '" + formFieldsValues[3] + "', '"
                                + formFieldsValues[4] + "','" + formFieldsValues[5]
                                + "', TO_DATE('2018-07-27 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        });


        String[] fields = new String[]{"value"};

        String[] fieldValues = new String[]{"You\'ll need to upload a file, following a pre-set format with individual payment lines. Or you can input the payment list manually"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.introSection.hint' AND lang = 'en'");

        fieldValues = new String[]{"Necesitaras subir un archivo que respete alguno de los formatos en las lineas de pago individuales. O puedes ingresarlas manualmente"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.introSection.hint' AND lang = 'es'");

        fieldValues = new String[]{"Comment"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.reference.label' AND lang = 'en'");

        fieldValues = new String[]{"Comentario"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.reference.label' AND lang = 'es'");

        fieldValues = new String[]{"Payments list management"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.file.label' AND lang = 'en'");

        fieldValues = new String[]{"Manejo de lineas de pago"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.file.label' AND lang = 'es'");

        fieldValues = new String[]{"Your payment list file should have one of the following preset formats"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.sampleFile.label' AND lang = 'en'");

        fieldValues = new String[]{"Tu archivo para lista de pagos debe respetar uno de los siguientes formatos predeterminados"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.sampleFile.label' AND lang = 'es'");

        fieldValues = new String[]{"Write a comment for your reference"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.reference.placeholder' AND lang = 'en'");

        fieldValues = new String[]{"Escribe un comentario para tu referencia"};
        update("form_field_messages", fields, fieldValues, "id_message = 'fields.salaryPayment.reference.placeholder' AND lang = 'es'");
    }
}
