/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3019
 *
 * @author fmarichal
 */
public class DB20170818_1118_3019 extends DBVSUpdate {

    @Override
    public void up() {
        String[] formFields = new String[] { "visible", "required", "sub_type", "visible_in_mobile" };
        String[] formFieldsValues = new String[] { "value(debitCard) == 'Si'", "TRUE", "default", "1" };
        update("form_fields", formFields, formFieldsValues, "id_field='placeOfRetreat' AND id_form='accountOpening'");
        
        String[] formFieldsBranchlist = new String[] { "display_type", "default_value", "show_other_option" };
        String[] formFieldsBranchlistValues = new String[] { "field-big", null, "1" };
        update("form_field_branchlist", formFieldsBranchlist, formFieldsBranchlistValues, "id_field='placeOfRetreat'");
    }
}
