/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor.AuditLevel;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor.Kind;

/**
 * @author isilveira
 */

public class DB20181221_1124_6147 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("files.download.multiline.errors", "com.technisys.omnichannel.client.activities.files.DownloadMultilinePaymentErrorsActivity", "files", AuditLevel.Header, Kind.Other, "core.authenticated");
    }

}