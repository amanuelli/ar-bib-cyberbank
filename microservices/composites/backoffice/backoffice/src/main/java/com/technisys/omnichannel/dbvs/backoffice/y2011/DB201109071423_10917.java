/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201109071423_10917 extends DBVSUpdate {

    @Override
    public void up() {
        delete("form_fields", "id_form=61 AND id_field='subtituloReferencia1'");
        delete("form_fields", "id_form=61 AND id_field='subtituloReferencia2'");
        delete("form_fields", "id_form=61 AND id_field='subtituloReferencia3'");

        update("forms", new String[]{"template_es"}, new String[]{"<form:form>	<form:section>	<form:field idField='introReferences1'/><form:field idField='referencia1Nombre'/>		<form:field idField='referencia1Apellido'/>		<form:field idField='referencia1Telefono'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='introReferences2'/>		<form:field idField='referencia2Nombre'/>		<form:field idField='referencia2Apellido'/>		<form:field idField='referencia2Telefono'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='introReferences3'/>		<form:field idField='referencia3Nombre'/>		<form:field idField='referencia3Apellido'/>		<form:field idField='referencia3Telefono'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='subtituloTarjetaDebito'/>		<form:field idField='tarjetaDebito'/>		<form:field idField='tarjetaDebitoNumeroDeCuenta'/>		<form:field idField='tarjetaDebitoTipoTarjeta'/>		<form:field idField='tarjetaDebitoProgramaVisa'/>		<form:field idField='tarjetaDebitoCuentaDebitoCostos'/>		<form:field idField='tarjetaDebitoDependenciaRetiro'/>		<form:field idField='tarjetaDebitoLimiteDiarioRetiro'/>		<form:field idField='tarjetaDebitoLimiteDiarioPago'/>		<form:field idField='tarjetaDebitoVisaLimiteDiarioCompra'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='tarjetaDebitoSubtituloAdicional'/>		<form:field idField='tarjetaDebitoAdicionalTipoDocumento'/>		<form:field idField='tarjetaDebitoAdicionalNumeroDocumento'/>		<form:field idField='tarjetaDebitoAdicionalPrimerNombre'/>		<form:field idField='tarjetaDebitoAdicional'/>		<form:field idField='tarjetaDebitoAdicionalPrimerApellido'/>		<form:field idField='tarjetaDebitoAdicionalSegundoApellido'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='subtituloFirmas'/>		<form:field idField='usoDeFirmas'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='subtituloDPCotitular'/>		<form:field idField='dPCotitularEsConyugue'/>		<form:field idField='dPCotitularApellidos'/>		<form:field idField='dPCotitularNombres'/>		<form:field idField='dPCotitularCalle'/>		<form:field idField='dPCotitularNumero'/>		<form:field idField='dPCotitularApartamento'/>		<form:field idField='dPCotitularCodigoPostal'/>		<form:field idField='dPCotitularLocalidad'/>		<form:field idField='dPCotitularPais'/>		<form:field idField='dPCotitularResidente'/>		<form:field idField='dpCotitularTelefono'/>		<form:field idField='dPCotitularCelular'/>		<form:field idField='dPCotitularFax'/>		<form:field idField='dPCotitularEmail'/>		<form:field idField='dPCotitularFechaNacimiento'/>		<form:field idField='dPCotitularLugarNacimiento'/>		<form:field idField='dPCotitularNacionalidad'/>		<form:field idField='dPCotitularTipoVivienda'/>		<form:field idField='dPCotitularCuotaHipoteca'/>		<form:field idField='dPCotitularAlquiler'/>		<form:field idField='dPCotitularEstadoCivil'/>		<form:field idField='dPCotitularFechaEstadoCivil'/>		<form:field idField='dPCotitularSexo'/>		<form:field idField='dPCotitularPersonasACargo'/>		<form:field idField='dPCotitularNumeroDeHijos'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='subtituloDLCotitular'/>		<form:field idField='dLCotitularEmpresa'/>		<form:field idField='dLCotitularRubro'/>		<form:field idField='dLCotitularCalle'/>		<form:field idField='dLCotitularNumero'/>		<form:field idField='dLCotitularApartamento'/>		<form:field idField='dLCotitularCodigoPostal'/>		<form:field idField='dLCotitularLocalidad'/>		<form:field idField='dLCotitularProfesion'/>		<form:field idField='dLCotitularCargo'/>		<form:field idField='dLCotitularFechaInicio'/>		<form:field idField='dLCotitularTelefono'/>		<form:field idField='dLCotitularEmail'/>		<form:field idField='dLCotitularIngresosNetosMensuales'/>		<form:field idField='dLCotitularOtrosIngresos'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='subtituloDPCCotitular'/>		<form:field idField='dPCCotitularApellidos'/>		<form:field idField='dPCCotitularNombres'/>		<form:field idField='dPCCotitularTipoDocumento'/>		<form:field idField='dPCCotitularNumeroDocumento'/>		<form:field idField='dPCCotitularTelefono'/>		<form:field idField='dPCCotitularCelular'/>		<form:field idField='dPCCotitularEmail'/>		<form:field idField='dPCCotitularFechaNacimiento'/>		<form:field idField='dPCCotitularLugar'/>		<form:field idField='dPCCotitularNacionalidad'/>	</form:section>	<div class='dotted_separator'>&nbsp;</div>	<form:section>		<form:field idField='terminosYCondiciones'/>	</form:section></form:form>"}, "id_form = 61");

        String[] fieldNames = new String[]{"id_field", "id_form", "field_type", "mandatory", "default_value"};

        String[] fieldValues = new String[]{"introReferences1", "61", "note", "1", "Ingrese 3 referencias comerciales, las mismas serán verificadas por el banco previo a la apertura de la nueva cuenta."};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"introReferences2", "61", "note", "1", "Ingrese una segunda referencia."};
        insert("form_fields", fieldNames, fieldValues);

        fieldValues = new String[]{"introReferences3", "61", "note", "1", "Ingrese una última referencia."};
        insert("form_fields", fieldNames, fieldValues);

    }
}