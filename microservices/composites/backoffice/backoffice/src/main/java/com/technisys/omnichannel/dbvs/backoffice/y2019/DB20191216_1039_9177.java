/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author jhossept
 */

public class DB20191216_1039_9177 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration(
                "frontend.apps.ios.downloadUrl",
                "https://install.appcenter.ms/sign-in?original_url=install:/%2Fapps",
                ConfigurationGroup.TECNICAS, "frontend", new String[]{},"","frontend");

    }

}