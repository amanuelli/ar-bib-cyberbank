/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-4610
 *
 * @author mcheveste
 */
public class DB20180627_1615_4610 extends DBVSUpdate {

    @Override
    public void up() {
        insert("widgets", new String[]{"id", "uri"}, new String[]{"pendingTransactions", "widgets/pendingTransactions"});
        insert("widgets", new String[]{"id", "uri"}, new String[]{"scheduledTransactions", "widgets/scheduledTransactions"});
        insertActivity("widgets.pendingTransactions", "com.technisys.omnichannel.client.activities.widgets.PendingTransactionsActivity",
                "desktop", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertActivity("widgets.scheduledTransactions", "com.technisys.omnichannel.client.activities.widgets.ScheduledTransactionsActivity", 
                "desktop", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
        insertOrUpdateConfiguration("desktop.widgets.pendingTransactions.status", "DRAFT|PENDING", ConfigurationGroup.TECNICAS, "widgetTransactions", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("desktop.widgets.scheduledTransactions.status", "SCHEDULED", ConfigurationGroup.TECNICAS, "widgetTransactions", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("desktop.widgets.pendingTransactions.max", "6", ConfigurationGroup.TECNICAS, "widgetTransactions", new String[]{"notEmpty"});
        insertOrUpdateConfiguration("desktop.widgets.scheduledTransactions.max", "6", ConfigurationGroup.TECNICAS, "widgetTransactions", new String[]{"notEmpty"});
    }
}
