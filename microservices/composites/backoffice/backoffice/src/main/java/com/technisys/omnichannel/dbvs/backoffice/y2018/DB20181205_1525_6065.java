package com.technisys.omnichannel.dbvs.backoffice.y2018;

import com.technisys.dbvs.ColumnDefinition;
import com.technisys.omnichannel.DBVSUpdate;

import java.sql.Types;

public class DB20181205_1525_6065 extends DBVSUpdate {

    @Override
    public void up() {
        ColumnDefinition[] columns = new ColumnDefinition[]{
                new ColumnDefinition("id_transaction", Types.VARCHAR, 100, 0, false, null),
                new ColumnDefinition("line_number", Types.INTEGER, 6, 0, false, null),
                new ColumnDefinition("credit_account_number", Types.VARCHAR, 50, 0, true, null),
                new ColumnDefinition("credit_account_name", Types.VARCHAR, 50, 0, true, null),
                new ColumnDefinition("credit_amount_quantity", Types.NUMERIC, 15, 3, true, null),
                new ColumnDefinition("credit_amount_currency", Types.CHAR, 4, 0, true, null),
                new ColumnDefinition("reference", Types.VARCHAR, 50, 0, true, null),
                new ColumnDefinition("status", Types.VARCHAR, 50, 0, false, null),
                new ColumnDefinition("status_date", Types.DATE, 0, 0, false, null),
                new ColumnDefinition("error_code", Types.VARCHAR, 50, 0, true, null)
        };
        createTable("transaction_lines", columns, new String[] { "id_transaction", "line_number" });
        createForeignKey("transaction_lines", "fk_tl_t_id_transaction", new String[] { "id_transaction" }, "transactions", new String[] { "id_transaction" }, true, true);
    }
}