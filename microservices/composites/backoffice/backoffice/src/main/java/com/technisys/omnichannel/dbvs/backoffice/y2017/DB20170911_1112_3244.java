/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-3244
 *
 * @author mjoseph
 */
public class DB20170911_1112_3244 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_messages",new String[]{"value"},new String[]{"Línea de crédito"},
                "id_message = 'fields.creditCardChangeCondition.amount.label' and id_form = 'creditCardChangeCondition'"
                        + " and id_field = 'amount' and lang = 'es' ");
        
        update("form_field_messages",new String[]{"value"},new String[]{"Credit line"},
                "id_message = 'fields.creditCardChangeCondition.amount.label' and id_form = 'creditCardChangeCondition'"
                        + " and id_field = 'amount' and lang = 'en' ");
        
        update("form_field_messages",new String[]{"value"},new String[]{"Linha de crédito"},
                "id_message = 'fields.creditCardChangeCondition.amount.label' and id_form = 'creditCardChangeCondition'"
                        + " and id_field = 'amount' and lang = 'pt' ");
        
        update("forms",new String[]{"schedulable"},new String[]{"0"}, "id_form = 'creditCardChangeCondition'");
    }
    
}
