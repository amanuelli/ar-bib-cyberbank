/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author mquerves
 */
public class DB201110251600_11533 extends DBVSUpdate {

    @Override
    public void up() {
        delete("form_fields", "id_field='numeroCelularAncel' AND id_form=16");

        update("form_fields", new String[]{"id_field"}, new String[]{"numeroContrato"}, "id_field='numeroContratoAncel' AND id_form=16");
        update("form_fields", new String[]{"default_value"}, new String[]{"Lorem ipsum ad his scripta blandit partiendo, eum fastidii accumsan euripidis in, eum liber hendrerit an. Qui ut wisi vocibus suscipiantur, quo dicit ridens inciderint id. Quo mundi lobortis reformidans eu, legimus senserit definiebas an eos. Eu sit tincidunt incorrupte definitionem, vis mutat affert percipit cu, eirmod consectetuer signiferumque eu per. In usu latine equidem dolores. Quo no falli viris intellegam, ut fugit veritus placerat per."}, "id_field='aceptacionDeSuscripcion' AND id_form=16");

    }
}