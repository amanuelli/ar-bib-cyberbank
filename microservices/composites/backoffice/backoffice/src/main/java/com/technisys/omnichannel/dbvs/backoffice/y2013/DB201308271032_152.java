/*
 *  Copyright 2013 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2013;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Sebastian Barbosa &lt;sbarbosa@technisys.com&gt;
 */
public class DB201308271032_152 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("rubicon.widgets.geolocalization.offices", "-34.909221,-56.153725#Sucursal Pocitos|-34.906687,-56.163853#Sucursal Parque Rodó|-34.905291,-56.206397#Casa Central|-34.905853,-56.195069#Sucursal Entrevero|-34.905361,-56.184383#Sucursal Gaucho");

    }
}