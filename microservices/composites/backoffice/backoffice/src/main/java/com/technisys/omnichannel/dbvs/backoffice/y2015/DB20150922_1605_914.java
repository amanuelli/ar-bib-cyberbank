/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author salva
 */
public class DB20150922_1605_914 extends DBVSUpdate {

    @Override
    public void up() {
        update("configuration", new String[]{"id_sub_group"}, new String[]{"frontend"}, "id_field = 'core.auth.login.maxAttemptsBeforeCaptcha'");
        updateConfiguration("core.attempts.features", "session.loginWithPasswordAndSecondFactor.step1=login|session.loginWithPasswordAndSecondFactor.step2=login|session.recoverPasswordWithSecondFactor.step1=login|session.recoverPinAndPassword.step2=recoverCredential|session.incrementCaptchaAttempts=login|session.recoverPinWithSecondFactor.step1=login");
    }
}
