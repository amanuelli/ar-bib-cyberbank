/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author npavlotzky
 */
public class DB201107261014 extends DBVSUpdate {

    @Override
    public void up() {

        update("activities", new String[]{"id_activity", "component_fqn"}, new String[]{"rub.accounts.transferInternalQuery", "com.technisys.rubicon.business.accounts.activities.TransferInternalQueryActivity"}, "id_activity = 'rub.accounts.transferInternalTransferQuery' AND version=1");
        update("activities", new String[]{"id_activity", "component_fqn"}, new String[]{"rub.creditcards.payCreditCardQuery", "com.technisys.rubicon.business.creditcards.activities.PayCreditCardQueryActivity"}, "id_activity = 'rub.creditcards.payCreditCardTransferQuery' AND version=1");
        update("activities", new String[]{"id_activity", "component_fqn"}, new String[]{"rub.loans.payLoanQuery", "com.technisys.rubicon.business.loans.activities.PayLoanQueryActivity"}, "id_activity = 'rub.loans.payLoanTransferQuery' AND version=1");

        delete("activities", "id_activity='rub.accounts.transferAbroadTransferQuery' AND version=1");
        delete("activities", "id_activity='rub.accounts.transferLocalTransferQuery' AND version=1");

    }
}