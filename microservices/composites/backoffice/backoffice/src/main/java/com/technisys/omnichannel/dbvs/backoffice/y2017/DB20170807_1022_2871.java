/* 
 * Copyright 2017 Manentia Software. 
 * 
 * This software component is the intellectual property of Manentia Software S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * http://www.manentiasoftware.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DatabaseUpdate;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2871
 *
 * @author ldurand
 */
public class DB20170807_1022_2871 extends DBVSUpdate {

    @Override
    public void up() {
        update("forms", new String[]{"category"}, new String[]{"accounts"}, "id_form = 'accountOpening'");
    }
}