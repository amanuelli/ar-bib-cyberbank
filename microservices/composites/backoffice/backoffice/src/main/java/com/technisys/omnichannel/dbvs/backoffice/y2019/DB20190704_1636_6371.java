/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * Related issue: MANNAZCA-6371
 *
 * @author dlondono
 */
public class DB20190704_1636_6371 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("creditCards.downloadMovements", "com.technisys.omnichannel.client.activities.creditcards.DownloadMovementsActivity", "creditCards", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "product.read", "idCreditCard");
        insertOrUpdateConfiguration("creditCards.export.maxStatements", "100", ConfigurationGroup.NEGOCIO, null, new String[]{"notEmpty", "integer"});
    }
}
