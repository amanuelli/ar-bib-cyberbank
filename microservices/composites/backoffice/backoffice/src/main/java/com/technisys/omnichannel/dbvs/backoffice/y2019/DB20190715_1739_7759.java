/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author Marcelo Bruno
 */

public class DB20190715_1739_7759 extends DBVSUpdate {

    @Override
    public void up() {
        update("form_field_messages", new String[]{"value"}, new String[]{"Number of installments"}, "id_message='fields.requestLoan.amountOfFees.label' and lang='en'");
    }

}