/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author mcommand
 */
public class DB20170828_1630_3144 extends DBVSUpdate {

    @Override
    public void up() {
        
        update("form_field_terms_conditions", new String[]{"show_label", "show_accept_option"}, new String[]{"0", "1"}, "id_form = 'preFinancingConstitution' and id_field = 'disclaimer'");
        
        String newMessage = "Las instrucciones enviadas al banco por usted luego de las <hora de corte>. serán procesadas al siguiente día hábil bancario.\n" +
            "Autorizo a debitar de mi cuenta las comisiones que la presente instrucción pueda generar.\nSujeto a aprobación crediticia del banco.\n" +
            "Su instrucción quedará completada una vez recibidos la totalidad de los documentos correspondientes en nuestras sucursales y se haya enviado la presente debidamente firmada.";
        
        update("form_field_messages", new String[]{"value"}, new String[]{newMessage}, "id_message = 'fields.preFinancingConstitution.disclaimer.termsAndConditions' and lang = 'es'");
    }
}