/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author ?
 */
public class DB20150702_1743_498 extends DBVSUpdate {

    @Override
    public void up() {
        delete("permissions", "id_permission='position'");
        insert("permissions", new String[]{"id_permission"}, new String[]{"position"});
        
        delete("permissions_credentials_groups", "id_permission='position'");
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"position", "accessToken"});
        
        deleteActivity("products.list");
        insertActivity("products.list", "com.technisys.omnichannel.client.activities.position.ListProductsActivity", "position", ActivityDescriptor.AuditLevel.None, ClientActivityDescriptor.Kind.Other, "position");
        
        insertOrUpdateConfiguration("defaultDecimal.minimum", "2", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});
        insertOrUpdateConfiguration("defaultDecimal.maximum", "2", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty", "integer"});

        update("configuration", new String[]{"id_sub_group"}, new String[]{"frontend"}, "id_field='core.decimalFormat'");
        
    }
}
