/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author Pablo
 */
public class DB20171009_1405_3375 extends DBVSUpdate {

    @Override
    public void up() {
        // Actividad para registrar un nuevo dispositivo para las push notifications
        insertActivity("communications.pushnotifications.registerUserDevice", "com.technisys.omnichannel.client.activities.communications.pushnotifications.RegisterUserDeviceActivity", "communications", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "core.authenticated");
    }
}
