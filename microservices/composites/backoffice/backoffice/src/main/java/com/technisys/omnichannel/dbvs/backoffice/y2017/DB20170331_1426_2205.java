/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2205
 *
 * @author dimoda
 */
public class DB20170331_1426_2205 extends DBVSUpdate {

    @Override
    public void up() {
    
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "sub_type", "visible_in_mobile"};
        String[] formFieldsValues;
        String idForm = "creditCardRequest";
        String version = "1";
       
        /**** Insert Form - Solicitud de tarjeta de crédito ****/    
        delete("forms", "id_form = '" + idForm + "'");        
        String[] formsFields = new String[]{"id_form", "version", "enabled", "category", "type", "id_jbpm_process", "admin_option","id_activity", "templates_enabled", "drafts_enabled", "schedulable"};
        String[] formsFieldsValues = new String[]{idForm, version, "1", "creditcards", "process", "demo-1", "creditCards", null, "1", "1", "1"};
        insert("forms", formsFields, formsFieldsValues);
    
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', 'Solicitud de tarjeta de crédito', TO_DATE('2017-03-31 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Solicitud de tarjeta de crédito", date});
        }
        
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "pin"});


        Map<String, String> messages = new HashMap();
    
        /****  Insert fied sectiontitle - Seccion Datos Tarjeta ****/
        formFieldsValues = new String[]{"seccionCard", idForm, version, "sectiontitle", "1", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("seccionCard.label", "Datos Tarjeta");
     
        /**** Insert field selector - Tarjeta solicitada ****/
        formFieldsValues = new String[]{"creditCard", idForm, version, "selector", "2", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"creditCard", idForm, version, "field-normal", "1"});
        messages.put("creditCard.label", "Tarjeta solicitada");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"creditCard", idForm, version, "americanExpress"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"creditCard", idForm, version, "masterCard"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"creditCard", idForm, version, "visa"});
    
        messages.put("creditCard.option.americanExpress", "American Express");
        messages.put("creditCard.option.masterCard", "MasterCard");
        messages.put("creditCard.option.visa", "Visa");
        
        /**** Insert field selector - Tipo de Tarjeta American Express ****/
        formFieldsValues = new String[]{"americanExpressTypeCard", idForm, version, "selector", "3", "value(creditCard) == 'americanExpress'", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"americanExpressTypeCard", idForm, version, "field-normal", "1"});
        messages.put("americanExpressTypeCard.label", "Tipo de Tarjeta");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporateBusinessExtra"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporateGold"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporateGreen"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporateMeeting"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporatePlatinum"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "corporatePurchasing"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressTypeCard", idForm, version, "greenCard"});
                
        messages.put("americanExpressTypeCard.option.corporateBusinessExtra", "Corporate Business Extra");
        messages.put("americanExpressTypeCard.option.corporateGold", "Corporate Gold");
        messages.put("americanExpressTypeCard.option.corporateGreen", "Corporate Green");
        messages.put("americanExpressTypeCard.option.corporateMeeting", "Corporate Meeting");
        messages.put("americanExpressTypeCard.option.corporatePlatinum", "Corporate Platinum");
        messages.put("americanExpressTypeCard.option.corporatePurchasing", "Corporate Purchasing");
        messages.put("americanExpressTypeCard.option.greenCard", "Green Card");
        
        
        /**** Insert field selector - Tipo de Tarjeta MasterCard ****/
        formFieldsValues = new String[]{"masterCardTypeCard", idForm, version, "selector", "4", "value(creditCard) == 'masterCard'", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"masterCardTypeCard", idForm, version, "field-normal", "1"});
        messages.put("masterCardTypeCard.label", "Tipo de Tarjeta");
        
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"masterCardTypeCard", idForm, version, "black"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"masterCardTypeCard", idForm, version, "internacional"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"masterCardTypeCard", idForm, version, "oro"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"masterCardTypeCard", idForm, version, "platinum"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"masterCardTypeCard", idForm, version, "regional"});
        
        messages.put("masterCardTypeCard.option.black", "Black");
        messages.put("masterCardTypeCard.option.internacional", "Internacional");
        messages.put("masterCardTypeCard.option.oro", "Oro");
        messages.put("masterCardTypeCard.option.platinum", "Platinum");
        messages.put("masterCardTypeCard.option.regional", "Regional");
        
        /**** Insert field selector - Tipo de Tarjeta Visa ****/
        formFieldsValues = new String[]{"visaTypeCard", idForm, version, "selector", "5", "value(creditCard) == 'visa'", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"visaTypeCard", idForm, version, "field-normal", "1"});
        messages.put("visaTypeCard.label", "Tipo de Tarjeta");

        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaTypeCard", idForm, version, "black"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaTypeCard", idForm, version, "internacional"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaTypeCard", idForm, version, "oro"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaTypeCard", idForm, version, "platinum"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaTypeCard", idForm, version, "regional"});
        
        messages.put("visaTypeCard.option.black", "Black");
        messages.put("visaTypeCard.option.internacional", "Internacional");
        messages.put("visaTypeCard.option.oro", "Oro");
        messages.put("visaTypeCard.option.platinum", "Platinum");
        messages.put("visaTypeCard.option.regional", "Regional");
        
        /**** Insert field selector - Programa de Premios American Express ****/
        formFieldsValues = new String[]{"americanExpressAwardsProgram", idForm, version, "selector", "6", "value(creditCard) == 'americanExpress'", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"americanExpressAwardsProgram", idForm, version, "field-normal", "1"});
        
        messages.put("americanExpressAwardsProgram.label", "Programa de Premios");

        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressAwardsProgram", idForm, version, "programa1"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressAwardsProgram", idForm, version, "programa2"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"americanExpressAwardsProgram", idForm, version, "programa3"});
        
        messages.put("americanExpressAwardsProgram.option.programa1", "Programa 1");
        messages.put("americanExpressAwardsProgram.option.programa2", "Programa 2");
        messages.put("americanExpressAwardsProgram.option.programa3", "Programa 3");
        
        /**** Insert field selector - Programa de Premios MasterCard ****/
        formFieldsValues = new String[]{"masterCardAwardsProgram", idForm, version, "selector", "7", "value(creditCard) == 'masterCard'", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"masterCardAwardsProgram", idForm, version, "field-normal", "1"});
        
        messages.put("masterCardAwardsProgram.label", "Programa de Premios");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"masterCardAwardsProgram", idForm, version, "aAdvantage"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"masterCardAwardsProgram", idForm, version, "classic"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"masterCardAwardsProgram", idForm, version, "smiles"});
        
        messages.put("masterCardAwardsProgram.option.aAdvantage", "AAdvantage");
        messages.put("masterCardAwardsProgram.option.classic", "Classic");
        messages.put("masterCardAwardsProgram.option.smiles", "Smiles");
        
        /**** Insert field selector - Programa de Premios Visa ****/
        formFieldsValues = new String[]{"visaAwardsProgram", idForm, version, "selector", "8", "value(creditCard) == 'visa'", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "show_blank_option"}, new String[]{"visaAwardsProgram", idForm, version, "field-normal", "1"});

        messages.put("visaAwardsProgram.label", "Programa de Premios");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaAwardsProgram", idForm, version, "aAdvantage"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaAwardsProgram", idForm, version, "clubCard"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaAwardsProgram", idForm, version, "latamPass"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaAwardsProgram", idForm, version, "mileagePlus"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"visaAwardsProgram", idForm, version, "smiles"});
                
        messages.put("visaAwardsProgram.option.aAdvantage", "AAdvantage");
        messages.put("visaAwardsProgram.option.clubCard", "Club Card");
        messages.put("visaAwardsProgram.option.latamPass", "LATAM Pass");
        messages.put("visaAwardsProgram.option.mileagePlus", "Mileage plus");
        messages.put("visaAwardsProgram.option.smiles", "Smiles");
        
        /****  Insert fied sectiontitle - Seccion Datos Titular ****/
        formFieldsValues = new String[]{"ownerSection", idForm, version, "sectiontitle", "9", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("ownerSection.label", "Datos del titular");
         
        /**** Insert field selector - Forma de contacto ****/
        formFieldsValues = new String[]{"contactForm", idForm, version, "selector", "10", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value","show_blank_option"}, new String[]{"contactForm", idForm, version, "field-normal", "phone", "0"});
        messages.put("contactForm.label", "Forma de contacto");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"contactForm", idForm, version, "phone"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"contactForm", idForm, version, "email"});     
        messages.put("contactForm.option.phone", "Teléfono");
        messages.put("contactForm.option.email", "E-mail");
         
        /**** Insert field text - Teléfono celular ****/
        formFieldsValues = new String[]{"phone", idForm, version, "text", "11", "value(contactForm) == 'phone'", "value(contactForm) == 'phone'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type","id_validation"}, new String[] {"phone", idForm, version, "9", "9", "field-normal","onlyNumbers"});
        messages.put("phone.label", "Teléfono");
        messages.put("phone.requiredError", "Debe ingresar un teléfono o dirección de e-mail");
       
        /**** Insert field text - E-mail ****/
        formFieldsValues = new String[]{"email", idForm, version, "text", "12", "value(contactForm) == 'email'", "value(contactForm) == 'email'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type","id_validation"}, new String[] {"email", idForm, version, "0", "50", "field-normal","email"});
        messages.put("email.label", "E-mail");
        messages.put("email.requiredError", "Debe ingresar un teléfono o dirección de e-mail");
        
        /**** Insert field selector - Solicitar adicional ****/
        formFieldsValues = new String[]{"requestAdditional", idForm, version, "selector", "13", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", new String[]{"id_field", "id_form", "form_version", "display_type", "default_value","show_blank_option"}, new String[]{"requestAdditional", idForm, version, "field-normal", "no", "0"});
        messages.put("requestAdditional.label", "Solicitar adicional");
        
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"requestAdditional", idForm, version, "no"});
        insert("form_field_selector_options", new String[]{"id_field", "id_form", "form_version", "value"}, new String[]{"requestAdditional", idForm, version, "si"});
             
        messages.put("requestAdditional.option.no", "No");
        messages.put("requestAdditional.option.si", "Si");
        
        /****  Insert fied sectiontitle - Seccion Datos del adicional ****/
        formFieldsValues = new String[]{"additionalSection", idForm, version, "sectiontitle", "14", "value(requestAdditional) == 'si'", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("additionalSection.label", "Datos del adicional");
        
        
        /**** Insert field text - Nombre y apellido ****/
        formFieldsValues = new String[]{"name", idForm, version, "text", "15", "value(requestAdditional) == 'si'", "value(requestAdditional) == 'si'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"}, new String[] {"name", idForm, version, "1", "100", "field-normal"});
        messages.put("name.label", "Nombre y apellido");
        messages.put("name.requiredError", "Debe ingresar el nombre y apellido del adicional");
        
        /**** Insert field document - Documento ****/
        formFieldsValues = new String[]{"document", idForm, version, "document", "16", "value(requestAdditional) == 'si'", "value(requestAdditional) == 'si'", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_document",new String[]{"id_field", "id_form", "form_version", "default_country", "default_document_type"}, new String[]{"document", idForm, version, "UY", "CI"});    
        messages.put("document.label", "Documento");
        messages.put("document.requiredError", "Debe ingresar un documento");
 
        /****  Insert fied horizontalrule - Linea separadora ****/
        formFieldsValues = new String[]{"horizontalrule", idForm, version, "horizontalrule", "17", "TRUE", "FALSE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        messages.put("horizontalrule.label", "");
    
        
        /****  Insert fied termsandcondition - Terminos y condiciones ****/
        formFieldsValues = new String[]{"termsandconditions", idForm, version, "termsandconditions", "18", "TRUE", "TRUE", "default", "1"};        
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_terms_conditions", new String[]{"id_field", "id_form", "form_version", "display_type", "show_accept_option", "show_label"}, new String[]{"termsandconditions", idForm, version, "field-normal", "1", "1"});

        messages.put("termsandconditions.label", "Términos y condiciones");
        messages.put("termsandconditions.requiredError", "Debe aceptar los términos y condiciones para continuar");
        messages.put("termsandconditions.showAcceptOptionText", "Aceptar");
        messages.put("termsandconditions.termsAndConditions", "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");
        
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};

        for (String key : messages.keySet()) {
            formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                        + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
            } else {
                insert("form_field_messages", formFieldsMessages, formFieldsValues);
            }
        }
           
    }

}
