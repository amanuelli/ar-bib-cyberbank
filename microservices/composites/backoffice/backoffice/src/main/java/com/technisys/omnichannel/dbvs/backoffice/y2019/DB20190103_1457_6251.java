/*
 * Copyright 2018 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-6251
 *
 * @author mcheveste
 */
public class DB20190103_1457_6251 extends DBVSUpdate {

    @Override
    public void up() {
        String idForm = "salaryPayment";
        String[] formField = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
        String[] formFieldValues = new String[]{"forms." + idForm + ".formName", idForm, "1", "en", "Salary payment", "2019-01-03 12:00:00"};

        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2] + "', '" + formFieldValues[3] + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");

            formFieldValues = new String[]{"forms." + idForm + ".formName", idForm, "1", "pt", "Pagar salários", "2019-01-03 12:00:00"};
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('" + formFieldValues[0] + "', '" + formFieldValues[1] + "', '" + formFieldValues[2] + "', '" + formFieldValues[3] + "', '" + formFieldValues[4] + "', TO_DATE('2015-01-01 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            insert("form_messages", formField, formFieldValues);

            formFieldValues = new String[]{"forms." + idForm + ".formName", idForm, "1", "pt", "Pagar salários", "2019-01-03 12:00:00"};
            insert("form_messages", formField, formFieldValues);
        }
    }
}
