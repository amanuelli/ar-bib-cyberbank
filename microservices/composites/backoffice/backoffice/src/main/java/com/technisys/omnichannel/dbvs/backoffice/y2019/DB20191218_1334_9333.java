/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author pbanales
 */

public class DB20191218_1334_9333 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("safeway.apirest.baseurl",
                "http://safeway.digital-test/safeway/api",
                ConfigurationGroup.SEGURIDAD,
                "others",
                new String[]{"notEmpty", "url", "safewayRESTConfiguration"});
    }

}
