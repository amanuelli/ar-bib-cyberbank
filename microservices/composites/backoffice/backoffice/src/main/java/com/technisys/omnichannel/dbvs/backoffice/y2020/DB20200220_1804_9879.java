/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 * @author KevinGaray
 */

public class DB20200220_1804_9879 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("administration.restrictions.availability.get");

        insertActivity(
                "administration.restrictions.availability.get", "com.technisys.omnichannel.client.activities.administration.restrictions.GetRestrictionsAvailabilityActivity",
                "administration",
                ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");
    }

}