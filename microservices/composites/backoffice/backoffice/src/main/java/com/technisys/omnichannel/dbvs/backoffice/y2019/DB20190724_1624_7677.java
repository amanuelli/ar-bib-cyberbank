/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Jhossept Garay
 */
public class DB20190724_1624_7677 extends DBVSUpdate {
    
    @Override
    public void up() {
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "modification_date", "value"};
        String formFieldsMessagesString = "id_message, lang, id_field, id_form, form_version, modification_date, value";
        String idForm ="lostOrStolenCreditCard";
        String idField = "reason";
        String version="1";
        
        Map<String, String> formFieldMessagesEs = new HashMap();
        formFieldMessagesEs.put("fields." + idForm + ".reason.option.lost","Pérdida");
        formFieldMessagesEs.put("fields." + idForm + ".reason.option.stolen","Robo");
        
        formFieldMessagesEs.keySet().forEach((key) -> {
            delete("form_field_messages", "id_message = '" + key + "' and lang = 'es'");
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages ("+ formFieldsMessagesString + ") "
                        + " VALUES ('"+ key + "', 'es', '" + idField + "', '" + idForm + "'," + version  + ", TO_DATE('"+date+"', 'YYYY-MM-DD HH24:MI:SS'), '" + formFieldMessagesEs.get(key) + "')");
            } else {
                insert("form_field_messages", formFieldsMessages, new String[]{key, "es", idField, idForm, version, date, formFieldMessagesEs.get(key)});
            }  
        });
        
        Map<String, String> formFieldMessagesPt = new HashMap();
        formFieldMessagesPt.put("fields." + idForm + ".reason.option.lost","Perda");
        formFieldMessagesPt.put("fields." + idForm + ".reason.option.stolen","Roubo");
        
        formFieldMessagesPt.keySet().forEach((key) -> {
            delete("form_field_messages", "id_message = '" + key + "' and lang = 'pt'");
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages ("+ formFieldsMessagesString + ") "
                        + " VALUES ('"+ key + "', 'pt', '" + idField + "', '" + idForm + "'," + version  + ", TO_DATE('"+date+"', 'YYYY-MM-DD HH24:MI:SS'), '" + formFieldMessagesPt.get(key) + "')");
            } else {
                insert("form_field_messages", formFieldsMessages, new String[]{key, "pt", idField, idForm, version, date, formFieldMessagesPt.get(key)});
            }  
        });

        /*fields.lostOrStolenCreditCard.reason.label message is added too*/
        delete("form_field_messages", "id_message = 'fields.lostOrStolenCreditCard.reason.label' and lang = 'pt'");
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages ("+ formFieldsMessagesString + ") "
                    + " VALUES ('fields.lostOrStolenCreditCard.reason.label', 'pt', '" + idField + "', '" + idForm + "'," + version  + ", TO_DATE('"+date+"', 'YYYY-MM-DD HH24:MI:SS'), 'Razão')");
        } else {
            insert("form_field_messages", formFieldsMessages, new String[]{"fields.lostOrStolenCreditCard.reason.label", "pt", idField, idForm, version, date, "Razão"});
        }

        
        Map<String, String> formFieldMessagesEn = new HashMap();
        formFieldMessagesEn.put("fields." + idForm + ".reason.option.lost","Loss");
        formFieldMessagesEn.put("fields." + idForm + ".reason.option.stolen","Theft");
        
        formFieldMessagesEn.keySet().forEach((key) -> {
            delete("form_field_messages", "id_message = '" + key + "' and lang = 'en'");
            
            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages ("+ formFieldsMessagesString + ") "
                        + " VALUES ('"+ key + "', 'en', '" + idField + "', '" + idForm + "'," + version  + ", TO_DATE('"+date+"', 'YYYY-MM-DD HH24:MI:SS'), '" + formFieldMessagesEn.get(key) + "')");
            } else {
                insert("form_field_messages", formFieldsMessages, new String[]{key, "en", idField, idForm, version, date, formFieldMessagesEn.get(key)});
            }  
        });
        
    }
}
