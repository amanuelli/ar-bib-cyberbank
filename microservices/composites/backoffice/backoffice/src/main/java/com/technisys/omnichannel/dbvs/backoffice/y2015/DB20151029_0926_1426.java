/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20151029_0926_1426 extends DBVSUpdate {

    @Override
    public void up() {
        deleteActivity("session.loginWithPasswordAndSecondFactor.step1");
        deleteActivity("session.loginWithPasswordAndSecondFactor.step2");

        insertActivity("session.selectEnvironment", "com.technisys.omnichannel.client.activities.session.SelectEnvironmentActivity",
                "login", ActivityDescriptor.AuditLevel.Full, ClientActivityDescriptor.Kind.Other, null);

    }
}
