/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.backoffice.business.productrequest.activities;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.backoffice.business.PaginatedListResponse;
import com.technisys.omnichannel.backoffice.business.productrequest.requests.ListRequest;
import com.technisys.omnichannel.backoffice.domain.ExportOnboarding;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.activities.BOActivity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import org.apache.commons.collections.CollectionUtils;

import java.io.IOException;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author iocampo
 */
public class ListActivity extends BOActivity {

    @Override
    public IBResponse execute(IBRequest request) throws ActivityException {
        PaginatedListResponse response = new PaginatedListResponse(request);

        try {
            ListRequest listRequest = (ListRequest) request;
            int rowsPerPage = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "backoffice.rowsPerPage");

            PaginatedList<Onboarding> list;

            String orderBy = listRequest.getOrderBy();

            Date creationDateFrom = (listRequest.getCreationDateFrom() != null) ? com.technisys.omnichannel.core.utils.DateUtils.trunc(listRequest.getCreationDateFrom(), ChronoUnit.DAYS) : null;
            Date creationDateTo = (listRequest.getCreationDateTo() != null) ? com.technisys.omnichannel.core.utils.DateUtils.changeTime(listRequest.getCreationDateTo(), 23, 59, 59, 998) : null;

            if (listRequest.getPageNumber() > 0) {
                int offset = (listRequest.getPageNumber() - 1) * rowsPerPage;
                int limit = rowsPerPage;

                list = OnboardingHandlerFactory.getInstance().listOnboardings(null, null, listRequest.getDocumentNumber(), listRequest.getEmail(),
                        creationDateFrom, creationDateTo, listRequest.getStatus(),
                        listRequest.getMobileNumber(), listRequest.getType(), offset, limit, orderBy);
            } else {
                int maxRowsExport = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "backoffice.maxRowsExport");

                list = OnboardingHandlerFactory.getInstance().listOnboardings(null, null, listRequest.getDocumentNumber(), listRequest.getEmail(),
                        creationDateFrom, creationDateTo, listRequest.getStatus(),
                        listRequest.getMobileNumber(), listRequest.getType(), 0, maxRowsExport, orderBy);

                final String lang = request.getLang();
                CollectionUtils.transform(list.getElementList(), (Object o) -> new ExportOnboarding((Onboarding) o, lang));
            }

            response.setCurrentPage(listRequest.getPageNumber());
            response.setTotalPages(list.getTotalPages());
            response.setTotalRows(list.getTotalRows());
            response.setRowsPerPage(rowsPerPage);
            response.setItemList(list.getElementList());
            response.setOrderBy(listRequest.getOrderBy());

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(IBRequest request) throws ActivityException {
        ListRequest listRequest = (ListRequest) request;
        Map<String, String> result = new HashMap<>();
        try {
            if (listRequest.getPageNumber() <= 0
                && (!Authorization.hasBackofficePermission(request.getIdUser(), "backoffice.massiveData.allow"))) {
                    throw new ActivityException(ReturnCodes.NOT_AUTHORIZED,
                            "The user (" + request.getIdUser() + ") is not authorized to export");
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
