/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.i18n.I18n;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author fpena
 */
public class DB20170908_1255_3007 extends DBVSUpdate {

    @Override
    public void up() {
        Map<String, String> messages = new HashMap();

        String dateAsString = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            dateAsString = "TO_DATE('" + dateAsString + "', 'YYYY-MM-DD HH24:MI:SS')";
        }
        String version = "1";

        messages.clear();
        messages.put("broadcastRequest", "Broadcast request");
        messages.put("creditLetterTransfer", "Credit letter transfer");
        messages.put("editBondsAndGuarantees", "Bonds and guarantees modification");
        messages.put("paymentClaim", "Payment claim");
        messages.put("preFinancingConstitution", "Pre-finance registration");
        messages.put("requestOfManagementCheck", "Management check request");

        for (String idForm : messages.keySet()) {

            if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
                customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                        + " VALUES ('forms." + idForm + ".formName', '" + idForm + "', '" + version + "', 'en', '" + messages.get(idForm) + "'," + dateAsString + ")");
            } else {
                String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
                insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "en", messages.get(idForm), dateAsString});
            }
        }

        messages.clear();

        messages.put("fields.accountOpening.horizontalrule.label", "");
        messages.put("fields.broadcastRequest.broadcastRequestSelector.label", "Broadcast request");
        messages.put("fields.broadcastRequest.broadcastRequestSelector.option.creditLine", "Against credit line");
        messages.put("fields.broadcastRequest.broadcastRequestSelector.option.depositBond", "Against deposit bond");
        messages.put("fields.broadcastRequest.broadcastRequestSelector.requiredError", "Choose an option");
        messages.put("fields.broadcastRequest.depositBondNotHave.label", "Doesn''t have");
        messages.put("fields.broadcastRequest.depositBondNotHave.requiredError", "Complete this information for the deposit bond");
        messages.put("fields.broadcastRequest.expenditureAbroadCharged.label", "Abroad expenses charged to");
        messages.put("fields.broadcastRequest.expenditureAbroadCharged.option.beneficiary", "Beneficiary");
        messages.put("fields.broadcastRequest.expenditureAbroadCharged.option.other", "Other");
        messages.put("fields.broadcastRequest.expenditureAbroadCharged.option.payer", "Payer");
        messages.put("fields.broadcastRequest.expenditureAbroadChargedNotHave.label", "Doesn''t have");
        messages.put("fields.broadcastRequest.expenditureAbroadChargedNotHave.requiredError", "Enter a value");
        messages.put("fields.broadcastRequest.formatType.label", "Format type");
        messages.put("fields.broadcastRequest.formatType.option.specificFormat", "Specific format");
        messages.put("fields.broadcastRequest.formatType.option.standardFormat", "Standard format");
        messages.put("fields.broadcastRequest.issueIn.label", "Issued in");
        messages.put("fields.broadcastRequest.issueIn.option.international", "International");
        messages.put("fields.broadcastRequest.issueIn.option.local", "Local");
        messages.put("fields.broadcastRequest.issueInNotHave.label", "Doesn''t have");
        messages.put("fields.broadcastRequest.issueInNotHave.option.directGuarantee", "Direct guarantee for the beneficiary");
        messages.put("fields.broadcastRequest.issueInNotHave.option.localGuaranteeCountry", "Local guarantee in the beneficiarys country");
        messages.put("fields.broadcastRequest.warningBankDetails.help", "Enter the banks full name and address");
        messages.put("fields.broadcastRequest.warningBankDetails.label", "Warning bank data");
        messages.put("fields.buyDocuments.disclaimer.requiredError", "");
        messages.put("fields.cancelCreditLetter.creditLetter.help", "Select the credit letter you wish to cancel");
        messages.put("fields.cancelCreditLetter.horizontalrule.label", "");
        messages.put("fields.cancelCreditLetter.notificationEmails.help", "This e-mails will be notified of the transfer. Remember to separate them using a comma.");
        messages.put("fields.creditLetterTransfer.addressBeneficiaryBank.label", "Second beneficiary banks address");
        messages.put("fields.creditLetterTransfer.addressBeneficiaryBank.requiredError", "Enter the banks SWIFT code and/or address");
        messages.put("fields.creditLetterTransfer.amountToTransfer.invalidError", "You must use the same currency as the original credit letter and the amount must be lower.");
        messages.put("fields.creditLetterTransfer.amountToTransfer.label", "Transfer amount");
        messages.put("fields.creditLetterTransfer.amountToTransfer.requiredError", "Enter the transfer amount");
        messages.put("fields.creditLetterTransfer.attachedFiles.label", "Attachments");
        messages.put("fields.creditLetterTransfer.attachedFiles.placeholder", "No attachments. Maximum 2MB");
        messages.put("fields.creditLetterTransfer.attachFileLink.label", "Attach file link");
        messages.put("fields.creditLetterTransfer.banksExpenses.label", "Other banks expenses");
        messages.put("fields.creditLetterTransfer.banksExpenses.option.banksExpenses1", "First beneficiary");
        messages.put("fields.creditLetterTransfer.banksExpenses.option.banksExpenses2", "Second beneficiary");
        messages.put("fields.creditLetterTransfer.banksExpenses.option.banksExpenses3", "Other");
        messages.put("fields.creditLetterTransfer.completeAddress.invalidError", "Enter an alphanumeric text shorter than 150 characters");
        messages.put("fields.creditLetterTransfer.completeAddress.label", "Complete address");
        messages.put("fields.creditLetterTransfer.completeAddress.requiredError", "Enter the address");
        messages.put("fields.creditLetterTransfer.confirmationExpenses.label", "Confirmation expenses");
        messages.put("fields.creditLetterTransfer.confirmationExpenses.option.confirmationExpenses1", "First beneficiary");
        messages.put("fields.creditLetterTransfer.confirmationExpenses.option.confirmationExpenses2", "Second beneficiary");
        messages.put("fields.creditLetterTransfer.confirmationExpenses.option.confirmationExpenses3", "Payer");
        messages.put("fields.creditLetterTransfer.confirmationExpenses.option.confirmationExpenses4", "Other");
        messages.put("fields.creditLetterTransfer.contactPerson.invalidError", "Enter an alphanumeric text shorter than 35 characters");
        messages.put("fields.creditLetterTransfer.contactPerson.label", "Contact");
        messages.put("fields.creditLetterTransfer.creditTransferExpenses.label", "Credit transfer expenses");
        messages.put("fields.creditLetterTransfer.creditTransferExpenses.option.creditTransferExpenses1", "First beneficiary");
        messages.put("fields.creditLetterTransfer.creditTransferExpenses.option.creditTransferExpenses2", "Second beneficiary");
        messages.put("fields.creditLetterTransfer.creditTransferExpenses.option.creditTransferExpenses3", "Other");
        messages.put("fields.creditLetterTransfer.deadlineSubmissionDocuments.invalidError", "Maximum length is 2 digits");
        messages.put("fields.creditLetterTransfer.deadlineSubmissionDocuments.label", "Documents submission period (days)");
        messages.put("fields.creditLetterTransfer.deadlineSubmissionDocuments.requiredError", "Enter the documents submission period");
        messages.put("fields.creditLetterTransfer.disclaimer.hint", "Instructions sent by you after business hours will be processed on the next working day. You are hereby authorizing that comissions generated by this operation be deducted from your account.");
        messages.put("fields.creditLetterTransfer.disclaimer.label", "Disclaimer");
        messages.put("fields.creditLetterTransfer.expensesIssuingBank.label", "Issuing bank expenses");
        messages.put("fields.creditLetterTransfer.expensesIssuingBank.option.expensesIssuingBank1", "First beneficiary");
        messages.put("fields.creditLetterTransfer.expensesIssuingBank.option.expensesIssuingBank2", "Second beneficiary");
        messages.put("fields.creditLetterTransfer.expensesIssuingBank.option.expensesIssuingBank3", "Payer");
        messages.put("fields.creditLetterTransfer.expensesIssuingBank.option.expensesIssuingBank4", "Other");
        messages.put("fields.creditLetterTransfer.nameSecondBeneficiaryBank.invalidError", "Enter an alphanumeric text shorter than 160 characters");
        messages.put("fields.creditLetterTransfer.nameSecondBeneficiaryBank.label", "Second beneficiary banks name");
        messages.put("fields.creditLetterTransfer.newBoardingDate.label", "New shipping date");
        messages.put("fields.creditLetterTransfer.newExpirationDate.label", "New due date");
        messages.put("fields.creditLetterTransfer.newExpirationDate.requiredError", "Invalid date");
        messages.put("fields.creditLetterTransfer.operationNumber.help", "Select the operation for which documents will be presented");
        messages.put("fields.creditLetterTransfer.operationNumber.invalidError", "Enter a numeric text shorter than 30 characters");
        messages.put("fields.creditLetterTransfer.operationNumber.label", "Operation number");
        messages.put("fields.creditLetterTransfer.operationNumber.requiredError", "This field is mandatory");
        messages.put("fields.creditLetterTransfer.phone.invalidError", "Enter a numeric text shorter than 35 characters");
        messages.put("fields.creditLetterTransfer.phone.label", "Phone");
        messages.put("fields.creditLetterTransfer.quantityMerchandiseAndUnitPrice.invalidError", "Enter an alphanumeric text shorter than 500 characters");
        messages.put("fields.creditLetterTransfer.quantityMerchandiseAndUnitPrice.label", "Merchandise quantity and unit price");
        messages.put("fields.creditLetterTransfer.searchBank.label", "Find bank");
        messages.put("fields.creditLetterTransfer.secondBeneficiary.help", "Enter the full name");
        messages.put("fields.creditLetterTransfer.secondBeneficiary.invalidError", "Enter an alphanumeric text shorter than 60 characters");
        messages.put("fields.creditLetterTransfer.secondBeneficiary.label", "Second beneficiary");
        messages.put("fields.creditLetterTransfer.secondBeneficiary.requiredError", "Enter a second beneficiary");
        messages.put("fields.creditLetterTransfer.secondBeneficiaryAccount.invalidError", "Enter an alphanumeric text shorter than 50 characters");
        messages.put("fields.creditLetterTransfer.secondBeneficiaryAccount.label", "Second beneficiary account");
        messages.put("fields.creditLetterTransfer.specialInstructions.invalidError", "Enter an alphanumeric text shorter than 500 characters");
        messages.put("fields.creditLetterTransfer.specialInstructions.label", "Special instructions");
        messages.put("fields.creditLetterTransfer.swiftFromSecondBeneficiaryBank.label", "Second beneficiary SWIFT code");
        messages.put("fields.creditLetterTransfer.swiftFromSecondBeneficiaryBank.requiredError", "Enter the banks SWIFT code and/or address");
        messages.put("fields.creditLetterTransfer.yourOtherExpenses.label", "Other expenses");
        messages.put("fields.creditLetterTransfer.yourOtherExpenses.option.yourOtherExpenses1", "First beneficiary");
        messages.put("fields.creditLetterTransfer.yourOtherExpenses.option.yourOtherExpenses2", "Second beneficiary");
        messages.put("fields.creditLetterTransfer.yourOtherExpenses.option.yourOtherExpenses3", "Other");
        messages.put("fields.editBondsAndGuarantees.amount.label", "Amount");
        messages.put("fields.editBondsAndGuarantees.attachments.label", "Attachments");
        messages.put("fields.editBondsAndGuarantees.blank.label", "");
        messages.put("fields.editBondsAndGuarantees.disclaimer.label", "Disclaimer");
        messages.put("fields.editBondsAndGuarantees.disclaimer.termsAndConditions", "Instructions sent by you after business hours will be processed on the next working day. You are hereby authorizing that comissions generated by this operation be deducted from your account. Subject to banks credit approval. The transaction will be completed once all required documents are received and the authorization form signed.");
        messages.put("fields.editBondsAndGuarantees.emissionType.label", "Submission type");
        messages.put("fields.editBondsAndGuarantees.emissionType.option.1", "Against credit line");
        messages.put("fields.editBondsAndGuarantees.emissionType.option.2", "Against deposit bond");
        messages.put("fields.editBondsAndGuarantees.increaseDecreace.label", "Increase / decrease");
        messages.put("fields.editBondsAndGuarantees.increaseDecreace.option.1", "Increase");
        messages.put("fields.editBondsAndGuarantees.increaseDecreace.option.2", "Decrease");
        messages.put("fields.editBondsAndGuarantees.newDueDate.label", "New due date");
        messages.put("fields.editBondsAndGuarantees.notificationBody.label", "Notification body");
        messages.put("fields.editBondsAndGuarantees.notificationEmails.help", "This e-mails will be notified of the transfer. Remember to separate them using a comma.");
        messages.put("fields.editBondsAndGuarantees.notificationEmails.label", "Notification e-mails");
        messages.put("fields.editBondsAndGuarantees.observations.label", "Notes");
        messages.put("fields.editBondsAndGuarantees.operationNumber.help", "Select the operation you wish to modify");
        messages.put("fields.editBondsAndGuarantees.operationNumber.label", "Operation number");
        messages.put("fields.editBondsAndGuarantees.operationNumber.requiredError", "Enter an operation number");
        messages.put("fields.frequentDestination.recipientDocument.label", "Beneficiary document");
        messages.put("fields.frequentDestination.recipientDocument.requiredError", "Enter the beneficiary document");
        messages.put("fields.modifyCreditLetter.line.label", "");
        messages.put("fields.openCreditLetter.blank.label", "");
        messages.put("fields.paymentClaim.attachFile.invalidError", "The selected file is too big, maximum allowed is 2MB");
        messages.put("fields.paymentClaim.attachFile.label", "Attach file");
        messages.put("fields.paymentClaim.attachments.label", "Attachments");
        messages.put("fields.paymentClaim.disclaimer.label", "Terms and conditions");
        messages.put("fields.paymentClaim.disclaimer.termsAndConditions", "Instructions sent by you after business hours will be processed on the next working day. You are hereby authorizing that comissions generated by this operation be deducted from your account.");
        messages.put("fields.paymentClaim.horizontalLine.label", "");
        messages.put("fields.paymentClaim.messageToTheBank.label", "Message to the bank");
        messages.put("fields.paymentClaim.messageToTheBank.placeholder", "Describe your instructions here");
        messages.put("fields.paymentClaim.notificationBody.label", "Notification body");
        messages.put("fields.paymentClaim.notificationEmails.label", "Notification e-mails");
        messages.put("fields.paymentClaim.notificationEmails.placeholder", "This e-mails will be notified of the transfer. Remember to separate them using a comma.");
        messages.put("fields.paymentClaim.paymentNumber.invalidError", "You don''t have any payments to cancel.");
        messages.put("fields.paymentClaim.paymentNumber.label", "Payment number");
        messages.put("fields.paymentClaim.paymentNumber.placeholder", "Select the payment you wish to claim");
        messages.put("fields.paymentClaim.paymentNumber.requiredError", "Enter a payment number");
        messages.put("fields.paymentClaim.search.label", "Search");
        messages.put("fields.paymentClaim.search.option.cobranza1", "value1");
        messages.put("fields.paymentClaim.search.option.cobranza2", "value2");
        messages.put("fields.paymentClaim.search.requiredError", "Enter the banks name");
        messages.put("fields.preFinancingConstitution.deposit.label", "Deposit");
        messages.put("fields.preFinancingConstitution.deposit.option.valor1", "10%");
        messages.put("fields.preFinancingConstitution.deposit.option.valor2", "30%");
        messages.put("fields.preFinancingConstitution.destinationCountry.label", "Destination country");
        messages.put("fields.preFinancingConstitution.disclaimer.label", "Disclaimer");
        messages.put("fields.preFinancingConstitution.disclaimer.termsAndConditions", "Instructions sent by you after business hours will be processed on the next working day. You are hereby authorizing that comissions generated by this operation be deducted from your account. Subject to banks credit approval. The transaction will be completed once all required documents are received and the authorization form signed.");
        messages.put("fields.preFinancingConstitution.emailsListNotification.help", "This e-mails will be notified of the transfer. Remember to separate them using a comma.");
        messages.put("fields.preFinancingConstitution.emailsListNotification.label", "Notification e-mails");
        messages.put("fields.preFinancingConstitution.exporterName.label", "Exporters name");
        messages.put("fields.preFinancingConstitution.exporterQuality.label", "Exporters quality");
        messages.put("fields.preFinancingConstitution.exporterQuality.option.directo", "Direct");
        messages.put("fields.preFinancingConstitution.exporterQuality.option.indirecto", "Indirect");
        messages.put("fields.preFinancingConstitution.exporterRUT.label", "Exporter company ID");
        messages.put("fields.preFinancingConstitution.import.label", "Amount");
        messages.put("fields.preFinancingConstitution.import.requiredError", "Enter an amount");
        messages.put("fields.preFinancingConstitution.notificationBody.label", "Notification body");
        messages.put("fields.preFinancingConstitution.prefinancingType.label", "Pre-finance type");
        messages.put("fields.preFinancingConstitution.prefinancingType.option.compraSinRecursoDeposito", "Purchase without deposit resource");
        messages.put("fields.preFinancingConstitution.prefinancingType.option.financiacionOperación", "100% operation financing");
        messages.put("fields.preFinancingConstitution.prefinancingType.option.financiacionSintetica", "Synthetic financing");
        messages.put("fields.preFinancingConstitution.rate.label", "Rate");
        messages.put("fields.preFinancingConstitution.rateAgreedOperation.label", "Rate agreed for this operation");
        messages.put("fields.preFinancingConstitution.rateAgreedOperation.requiredError", "Agreed rate is invalid");
        messages.put("fields.preFinancingConstitution.referenceCode.label", "Reference code");
        messages.put("fields.preFinancingConstitution.referenceCode.requiredError", "Enter a reference code");
        messages.put("fields.preFinancingConstitution.rubro.help", "Enter the most representative NCM for the affected exports");
        messages.put("fields.preFinancingConstitution.rubro.label", "NCM type");
        messages.put("fields.preFinancingConstitution.rubro.requiredError", "Enter the NCM type");
        messages.put("fields.preFinancingConstitution.term.label", "Term");
        messages.put("fields.preFinancingConstitution.term.option.plazo1", "180");
        messages.put("fields.preFinancingConstitution.term.option.plazo2", "270");
        messages.put("fields.preFinancingConstitution.term.option.plazo3", "360");
        messages.put("fields.preFinancingConstitution.typeCompany.label", "Company type");
        messages.put("fields.preFinancingConstitution.typeCompany.option.circularx", "Circular X");
        messages.put("fields.preFinancingConstitution.typeCompany.option.circularxx", "Circular XX");
        messages.put("fields.presentationOfDocumentsUnderCollection.disclaimer.label", "");
        messages.put("fields.presentationOfDocumentsUnderCollection.line.label", "");
        messages.put("fields.requestEndorsement.disclaimer.requiredError", "");
        messages.put("fields.requestLoan.lineaSeparadora.label", "");
        messages.put("fields.requestOfManagementCheck.address.label", "Address");
        messages.put("fields.requestOfManagementCheck.address.requiredError", "Enter the address");
        messages.put("fields.requestOfManagementCheck.amount.label", "Amount");
        messages.put("fields.requestOfManagementCheck.amount.requiredError", "Enter the amount");
        messages.put("fields.requestOfManagementCheck.branchOffices.label", "Branch");
        messages.put("fields.requestOfManagementCheck.branchOffices.requiredError", "Select a branch");
        messages.put("fields.requestOfManagementCheck.costDebitAccount.label", "Cost debit account");
        messages.put("fields.requestOfManagementCheck.costDebitAccount.requiredError", "Select a cost debit account");
        messages.put("fields.requestOfManagementCheck.debitAccount.label", "Debit account");
        messages.put("fields.requestOfManagementCheck.debitAccount.requiredError", "Select a debit account");
        messages.put("fields.requestOfManagementCheck.document.label", "Document");
        messages.put("fields.requestOfManagementCheck.name.label", "Name");
        messages.put("fields.requestOfManagementCheck.name.requiredError", "Enter a name");
        messages.put("fields.requestOfManagementCheck.nameOfBeneficiary.label", "Beneficiary");
        messages.put("fields.requestOfManagementCheck.nameOfBeneficiary.requiredError", "Enter a beneficiary");
        messages.put("fields.requestOfManagementCheck.placeOfRetreat.label", "Pick-up location");
        messages.put("fields.requestOfManagementCheck.placeOfRetreat.option.branchOffices", "Branches");
        messages.put("fields.requestOfManagementCheck.placeOfRetreat.option.homeDelivery", "Home delivery");
        messages.put("fields.requestOfManagementCheck.placeOfRetreat.requiredError", "Enter a pick-up location");
        messages.put("fields.requestOfManagementCheck.secondSurname.label", "Second last name");
        messages.put("fields.requestOfManagementCheck.surname.label", "First last name");
        messages.put("fields.requestOfManagementCheck.surname.requiredError", "Enter the last name");
        messages.put("fields.requestOfManagementCheck.termsAndConditions.showAcceptOptionText", "Accept terms");
        messages.put("fields.requestOfManagementCheck.termsAndConditions.termsAndConditions", "Instructions sent by you after business hours will be processed on the next working day. You are hereby authorizing that comissions generated by this operation be deducted from your account.");
        messages.put("fields.requestTransactionCancellation.disclaimer.label", "");

        dateAsString = "'" + (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date()) + "'";
        if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            dateAsString = "TO_DATE(" + dateAsString + ", 'YYYY-MM-DD HH24:MI:SS')";
        }

        String idForm;
        String idField;
        String[] parts;
        for (String idMessage : messages.keySet()) {

            parts = idMessage.split("\\.");
            idForm = parts[1];
            idField = parts[2];

            delete("form_field_messages", "id_form='" + idForm + "' AND form_version = 1 AND id_field='" + idField + "' AND lang='en'");
            customSentence(new String[]{DBVS.DIALECT_MYSQL, DBVS.DIALECT_ORACLE, DBVS.DIALECT_MSSQL, DBVS.DIALECT_HSQLDB},
                    "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                    + " VALUES ('" + idMessage + "', '" + I18n.LANG_ENGLISH + "', "
                    + "'" + idField + "', '" + idForm + "', '1', '" + messages.get(idMessage) + "', " + dateAsString + ")");
        }
    }
}
