/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */

package com.technisys.omnichannel.dbvs.backoffice.y2020;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author cristobal
 */

public class DB20201001_1008_14203 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("core.auth.captcha.google.keepAlive", "5", ConfigurationGroup.SEGURIDAD, "others", new String[]{"notEmpty", "integer"});;
    }
}