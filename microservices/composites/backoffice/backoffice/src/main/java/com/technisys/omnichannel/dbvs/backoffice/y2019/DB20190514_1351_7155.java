/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice.y2019;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author mgiglio
 */

public class DB20190514_1351_7155 extends DBVSUpdate {

    @Override
    public void up() {
        delete("form_field_selector_options","id_field='currencies' AND id_form='accountOpening' AND value='Pesos'");
    }

}