/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author sbarbosa
 */
public class DB20150714_0954_415 extends DBVSUpdate {

    @Override
    public void up() {
        insertActivity("administration.products.list", "com.technisys.omnichannel.client.activities.administration.common.ListProductsActivity", "administration.products", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other, "administration.view");

    }
}
