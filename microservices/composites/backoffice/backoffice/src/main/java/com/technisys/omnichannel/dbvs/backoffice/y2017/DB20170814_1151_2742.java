/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */ 
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.dbvs.DBVS;
import com.technisys.omnichannel.DBVSUpdate;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Related issue: MANNAZCA-2742
 *
 * @author rmedina
 */
public class DB20170814_1151_2742 extends DBVSUpdate {

    @Override
    public void up() {
    
        String date = (new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")).format(new Date());
        String idForm = "broadcastRequest";
        String version = "1";
        Map<String, String> messages = new HashMap();
        
        String[] forms = new String[]{"id_form", "version", "enabled", "category", "type", "admin_option", "id_activity", "last", "deleted", "schedulable", "templates_enabled", "drafts_enabled", "editable_in_mobile", "editable_in_narrow", "id_bpm_process"};
        String[] formFields = new String[]{"id_field", "id_form", "form_version", "type", "ordinal", "visible", "required", "note", "visible_in_mobile", "read_only", "sub_type", "ticket_only"};
        String[] formFieldsText = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type", "id_validation"};
        String[] formFieldsTextArea = new String[]{"id_field", "id_form", "form_version", "min_length", "max_length", "display_type"};
        String[] formFieldsSelector = new String[]{"id_field", "id_form", "form_version", "display_type", "default_value", "show_blank_option", "render_as"};
        String[] formFieldsSelectorOption = new String[]{"id_field", "id_form", "form_version", "value"};
        String[] formFieldsMessages = new String[]{"id_message", "lang", "id_field", "id_form", "form_version", "value", "modification_date"};
        String[] formsValues, formFieldsValues, formFieldsTextValues, formFieldsTextAreaValues, formFieldsSelectorValues, formFieldsSelectorOptionValues; 
        
        formsValues = new String[]{idForm, version, "1", "comex", "process", null, null, "1", "0", "1", "1", "1", "1", "1", "demo:1:3"};
        insert("forms", forms, formsValues);
        
         if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
            customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_messages (id_message, id_form, version, lang, value, modification_date) "
                    + " VALUES ('forms." + idForm + ".formName', '" +idForm + "', '" + version  + "', 'es', 'Solicitud de emisión', TO_DATE('2017-04-03 00:00:01', 'YYYY-MM-DD HH24:MI:SS'))");
        } else {
            String[] formMessageFields = new String[]{"id_message", "id_form", "version", "lang", "value", "modification_date"};
            insert("form_messages", formMessageFields, new String[]{"forms." + idForm + ".formName", idForm, version, "es", "Solicitud de emisión", date});    
        }
         
        insert("permissions", new String[]{"id_permission"}, new String[]{"client.form." + idForm + ".send"});
        insert("permissions_credentials_groups", new String[]{"id_permission", "id_credential_group"}, new String[]{"client.form." + idForm + ".send", "accessToken-pin"});
        
        formFieldsValues = new String[]{"formatType", idForm, version, "selector", "1", "TRUE", "TRUE",null, "0", "0", "default", "0"};
        formFieldsSelectorValues = new String[]{"formatType", idForm, version, "field-normal", "standardFormat", "0", "combo"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", formFieldsSelector, formFieldsSelectorValues);
        messages.put("formatType.label", "Tipo de formato");        
        
        formFieldsSelectorOptionValues = new String[]{"formatType", idForm, version, "standardFormat"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("formatType.option.standardFormat", "Vuestro formato estándar");

        formFieldsSelectorOptionValues = new String[]{"formatType", idForm, version, "specificFormat"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("formatType.option.specificFormat", "Formato específico");
        
        //CAMPO TIPO LINK
        
        formFieldsValues = new String[]{"broadcastRequestSelector", idForm, version, "selector", "2", "TRUE", "TRUE",null, "0", "0", "default", "0"};
        formFieldsSelectorValues = new String[]{"broadcastRequestSelector", idForm, version, "field-normal", null, "1", "combo"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", formFieldsSelector, formFieldsSelectorValues);
        messages.put("broadcastRequestSelector.label", "Solicitud de emisión");        
        messages.put("broadcastRequestSelector.requiredError", "Debe seleccionar una opción");        
        
        formFieldsSelectorOptionValues = new String[]{"broadcastRequestSelector", idForm, version, "creditLine"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("broadcastRequestSelector.option.creditLine", "Contra línea de crédito");

        formFieldsSelectorOptionValues = new String[]{"broadcastRequestSelector", idForm, version, "depositBond"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("broadcastRequestSelector.option.depositBond", "Contra prenda de depósito");
        
        formFieldsValues = new String[]{"depositBondNotHave", idForm, version, "textarea", "3", "value(broadcastRequestSelector) == 'depositBond'", "value(broadcastRequestSelector) == 'depositBond'",null, "0", "0", "default", "0"};
        formFieldsTextAreaValues = new String[]{"depositBondNotHave", idForm, version, "1", "1500", "field-normal"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", formFieldsTextArea, formFieldsTextAreaValues);
        messages.put("depositBondNotHave.help", null);
        messages.put("depositBondNotHave.hint", null);
        messages.put("depositBondNotHave.requiredError", "Complete está información para prenda de depósito");
        messages.put("depositBondNotHave.label", "No tiene");
        
        formFieldsValues = new String[]{"issueIn", idForm, version, "selector", "4", "TRUE", "TRUE",null, "0", "0", "default", "0"};
        formFieldsSelectorValues = new String[]{"issueIn", idForm, version, "field-normal", "local", "1", "combo"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", formFieldsSelector, formFieldsSelectorValues);
        messages.put("issueIn.label", "Emita en");        
        
        formFieldsSelectorOptionValues = new String[]{"issueIn", idForm, version, "local"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("issueIn.option.local", "Local");

        formFieldsSelectorOptionValues = new String[]{"issueIn", idForm, version, "international"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("issueIn.option.international", "Internacional");
        
        formFieldsValues = new String[]{"issueInNotHave", idForm, version, "selector", "5", "value(issueIn) == 'international'", "value(issueIn) == 'international'",null, "0", "0", "default", "0"};
        formFieldsSelectorValues = new String[]{"issueInNotHave", idForm, version, "field-normal", null, "1", "combo"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", formFieldsSelector, formFieldsSelectorValues);
        messages.put("issueInNotHave.label", "No tiene");        
        
        formFieldsSelectorOptionValues = new String[]{"issueInNotHave", idForm, version, "localGuaranteeCountry"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("issueInNotHave.option.localGuaranteeCountry", "Emisión garantía local en el país del beneficiario");

        formFieldsSelectorOptionValues = new String[]{"issueInNotHave", idForm, version, "directGuarantee"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("issueInNotHave.option.directGuarantee", "Emisión de garantía directa a favor del beneficiario");
        
        formFieldsValues = new String[]{"warningBankDetails", idForm, version, "textarea", "6", "value(issueIn) == 'international'", "value(issueIn) == 'international'",null, "0", "0", "default", "0"};
        formFieldsTextAreaValues = new String[]{"warningBankDetails", idForm, version, "0", "150", "field-normal"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_textarea", formFieldsTextArea, formFieldsTextAreaValues);
        messages.put("warningBankDetails.help", "Ingrese nombre y dirección completa del banco");
        messages.put("warningBankDetails.label", "Datos del banco avisador");
        
        formFieldsValues = new String[]{"expenditureAbroadCharged", idForm, version, "selector", "7", "value(issueIn) == 'international'", "value(issueIn) == 'international'",null, "0", "0", "default", "0"};
        formFieldsSelectorValues = new String[]{"expenditureAbroadCharged", idForm, version, "field-normal", "payer", "0", "combo"};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_selector", formFieldsSelector, formFieldsSelectorValues);
        messages.put("expenditureAbroadCharged.label", "Gastos en el exterior a cargo de");        
        
        formFieldsSelectorOptionValues = new String[]{"expenditureAbroadCharged", idForm, version, "payer"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("expenditureAbroadCharged.option.payer", "Ordenante");

        formFieldsSelectorOptionValues = new String[]{"expenditureAbroadCharged", idForm, version, "beneficiary"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("expenditureAbroadCharged.option.beneficiary", "Beneficiario");
        
        formFieldsSelectorOptionValues = new String[]{"expenditureAbroadCharged", idForm, version, "other"};
        insert("form_field_selector_options", formFieldsSelectorOption, formFieldsSelectorOptionValues);
        messages.put("expenditureAbroadCharged.option.other", "Otro");
        
        formFieldsValues = new String[]{"expenditureAbroadChargedNotHave", idForm, version , "text", "8", "value(expenditureAbroadCharged) == 'other'", "value(expenditureAbroadCharged) == 'other'",null, "1", "0", "default", "0"};
        formFieldsTextValues = new String[]{"expenditureAbroadChargedNotHave", idForm, version, "0", "20", "field-normal", null};
        insert("form_fields", formFields, formFieldsValues);
        insert("form_field_text", formFieldsText, formFieldsTextValues);
        messages.put("expenditureAbroadChargedNotHave.label", "No tiene");
        messages.put("expenditureAbroadChargedNotHave.requiredError", "Debe ingresar un valor");        
        
        
        for (String key : messages.keySet()) {
           formFieldsValues = new String[]{"fields." + idForm + "." + key, "es", key.substring(0, key.indexOf(".")), idForm, version, messages.get(key), date};
           if (DBVS.DIALECT_ORACLE.equals(getDialect())) {
               customSentence(DBVS.DIALECT_ORACLE, "INSERT INTO form_field_messages (id_message, lang, id_field, id_form, form_version, value, modification_date) "
                       + " VALUES ('" + formFieldsValues[0] + "', '" + formFieldsValues[1] + "', '" + formFieldsValues[2] + "', '" + formFieldsValues[3] + "', '" + formFieldsValues[4] + "','" + formFieldsValues[5] + "', TO_DATE('2017-05-11 00:00:00', 'YYYY-MM-DD HH24:MI:SS'))");
           } else {
               insert("form_field_messages", formFieldsMessages, formFieldsValues);
           }
       }
        
    }
}