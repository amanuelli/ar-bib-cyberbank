/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150723_1123_415 extends DBVSUpdate {

    @Override
    public void up() {
      
        insertOrUpdateConfiguration("client.emailValidationFormat", "^[a-zA-ZñÑ0-9._%+-]+@[a-zñÑA-Z0-9.-]+\\.(?:[a-zA-ZñÑ]{2}|com|org|net|edu|gov|mil|biz|info|mobi|name|aero|asia|jobs|museum)$", ConfigurationGroup.TECNICAS, "frontend", new String[]{"notEmpty", "string"});

    }
}
