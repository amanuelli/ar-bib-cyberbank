/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author sbarbosa
 */
public class DB20150907_1329_507 extends DBVSUpdate {

    @Override
    public void up() {
        updateConfiguration("core.activities.withSecretFields", "enrollment.wizzard.accessData|enrollment.wizzard.finish");

        // Permisos por defecto
        updateConfiguration("client.permissions.defaults", "core.authenticated|core.loginWithOTP|core.loginWithPassword|core.loginWithPIN|user.preferences|user.preferences.pin");

        insertOrUpdateConfiguration("core.attempts.associateEnrollment.lockoutWindow", "8m", null, "enrollment", new String[]{"notEmpty", "interval"});
        insertOrUpdateConfiguration("core.attempts.associateEnrollment.maxAttempts", "3", null, "enrollment", new String[]{"notEmpty", "integer"});

    }
}
