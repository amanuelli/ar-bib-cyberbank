/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author salva
 */
public class DB20151013_1327_960 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration(
                "frontend.availabelFeatures.refresh.interval",
                "1h", ConfigurationGroup.NEGOCIO, "frontend", new String[]{"notEmpty","interval"});
    }
}
