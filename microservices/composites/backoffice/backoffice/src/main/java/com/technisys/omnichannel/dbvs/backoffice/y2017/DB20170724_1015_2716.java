/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;
import com.technisys.omnichannel.core.activities.ActivityDescriptor;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;

/**
 *
 * @author danireb
 */
public class DB20170724_1015_2716 extends DBVSUpdate {

    @Override
    public void up() {
        
        delete("adm_ui_permissions", "id_permission = 'frequentDestinations'");
        delete("permissions", "id_permission = 'frequentDestinations'");
        delete("activity_products", "id_activity = 'frequentdestinations.list'");
        updateActivity("frequentdestinations.create.validate", "com.technisys.omnichannel.client.activities.frequentdestinations.CreateFrequentDestinationActivity", "frequentDestinations.manage", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other);
        updateActivity("frequentdestinations.create.preview", "com.technisys.omnichannel.client.activities.frequentdestinations.CreateFrequentDestinationPreviewActivity", "frequentDestinations.manage", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other);
        updateActivity("frequentdestinations.modify.pre", "com.technisys.omnichannel.client.activities.frequentdestinations.ModifyFrequentDestinationPreActivity", "frequentDestinations.manage", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other);
        updateActivity("frequentdestinations.modify.preview", "com.technisys.omnichannel.client.activities.frequentdestinations.ModifyFrequentDestinationPreviewActivity", "frequentDestinations.manage", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other);        
        updateActivity("frequentdestinations.delete.preview", "com.technisys.omnichannel.client.activities.frequentdestinations.DeleteFrequentDestinationPreviewActivity", "frequentDestinations.manage", "frequentDestinations", ActivityDescriptor.AuditLevel.Header, ClientActivityDescriptor.Kind.Other);
        
    }
    
}
