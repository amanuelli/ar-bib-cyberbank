/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.dbvs.backoffice;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author pbanales
 */

public class DB20191007_1104_8522 extends DBVSUpdate {

    @Override
    public void up() {
        insertOrUpdateConfiguration("safeway.apirest.timeout.connect",
                "5000",
                ConfigurationGroup.TECNICAS,
                "others",
                new String[]{"notEmpty"});
        insertOrUpdateConfiguration("safeway.apirest.timeout.read",
                "5000",
                ConfigurationGroup.TECNICAS,
                "others",
                new String[]{"notEmpty"});
        insertOrUpdateConfiguration("safeway.apirest.timeout.write",
                "5000",
                ConfigurationGroup.TECNICAS,
                "others",
                new String[]{"notEmpty"});
    }

}
