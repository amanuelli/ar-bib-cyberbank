/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2017;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * Related issue: MANNAZCA-2401
 *
 * @author sbarbosa
 */
public class DB20170529_0930_2401 extends DBVSUpdate {

    @Override
    public void up() {
        
        insert("activity_caps", new String[]{"id_activity", "id_field_amount", "id_field_product"},
                new String[]{"pay.creditcard.send", "amount", "debitAccount"});
        insert("activity_caps", new String[]{"id_activity", "id_field_amount", "id_field_product"},
                new String[]{"pay.creditcard.send", "otherAmount", "debitAccount"});

        insert("activity_caps", new String[]{"id_activity", "id_field_amount", "id_field_product"},
                new String[]{"pay.loan.send", "loanPayment", "debitAccount"});
        insert("activity_caps", new String[]{"id_activity", "id_field_amount", "id_field_product"},
                new String[]{"pay.loan.send", "otherLoanAmount", "debitAccount"});

        insert("activity_caps", new String[]{"id_activity", "id_field_amount", "id_field_product"},
                new String[]{"transfers.foreign.send", "amount", "debitAccount"});

    }

}
