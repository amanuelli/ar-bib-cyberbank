/*
 *  Copyright 2011 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.dbvs.backoffice.y2011;

import com.technisys.omnichannel.DBVSUpdate;

/**
 * @author grosso
 */
public class DB201109271215_11333 extends DBVSUpdate {

    @Override
    public void up() {
        String[] fieldNames = new String[]{"id_activity", "version", "enabled", "component_fqn", "id_permission_required", "id_group"};
        String[] fieldValues = new String[]{"rub.accounts.sendCheckElement", "1", "1", "com.technisys.rubicon.business.accounts.activities.SendCheckElementActivity", "rub.accounts.sendCheck", "rub.discountSendChecks"};
        insert("activities", fieldNames, fieldValues);

    }
}