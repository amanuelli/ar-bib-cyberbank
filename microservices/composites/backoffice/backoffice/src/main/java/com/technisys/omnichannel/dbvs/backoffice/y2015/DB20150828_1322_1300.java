/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.dbvs.backoffice.y2015;

import com.technisys.omnichannel.DBVSUpdate;

/**
 *
 * @author fpena
 */
public class DB20150828_1322_1300 extends DBVSUpdate {

    @Override
    public void up() {

        StringBuilder configurationFields = new StringBuilder();
        configurationFields.append("'administration.users.invite.enabled',");
        configurationFields.append("'backoffice.environments.productsTypes',");
        configurationFields.append("'backoffice.environments.schemes',");
        configurationFields.append("'backoffice.environments.types',");
        configurationFields.append("'backoffice.forms.categories',");
        configurationFields.append("'backoffice.forms.credentials.exclude',");
        configurationFields.append("'backoffice.forms.displayTypes',");
        configurationFields.append("'backoffice.invitation.new.acceptUrl',");
        configurationFields.append("'backoffice.users.filter.environmentType.options',");
        configurationFields.append("'breadcrumb.accounts.url',");
        configurationFields.append("'breadcrumb.creditcards.url',");
        configurationFields.append("'breadcrumb.deposits.url',");
        configurationFields.append("'breadcrumb.loans.url',");
        configurationFields.append("'breadcrumb.payments.url',");
        configurationFields.append("'breadcrumb.transfers.url',");
        configurationFields.append("'client.defaultUserRole',");
        configurationFields.append("'client.permissions.defaults',");
        configurationFields.append("'client.permissions.full.allCA',");
        configurationFields.append("'client.permissions.full.allCC',");
        configurationFields.append("'client.permissions.full.allPA',");
        configurationFields.append("'client.permissions.full.allPF',");
        configurationFields.append("'client.permissions.full.allPI',");
        configurationFields.append("'client.permissions.full.allTC',");
        configurationFields.append("'client.permissions.full.none',");
        configurationFields.append("'client.permissions.read.allCA',");
        configurationFields.append("'client.permissions.read.allCC',");
        configurationFields.append("'client.permissions.read.allPA',");
        configurationFields.append("'client.permissions.read.allPF',");
        configurationFields.append("'client.permissions.read.allPI',");
        configurationFields.append("'client.permissions.read.allTC',");
        configurationFields.append("'client.permissions.read.none',");
        configurationFields.append("'client.userconfiguration.forgotpasswordmail.url',");
        configurationFields.append("'communications.dispatcher.MAIL',");
        configurationFields.append("'communications.dispatcher.SMS',");
        configurationFields.append("'core.activities.dontExtendSession',");
        configurationFields.append("'core.activitiesNotLogged',");
        configurationFields.append("'core.capFrequency.list',");
        configurationFields.append("'core.communications.communicationTypes',");
        configurationFields.append("'core.communications.defaultCommunicationType',");
        configurationFields.append("'core.communications.productType.perrmissionsToCheck',");
        configurationFields.append("'core.currencyConverter',");
        configurationFields.append("'core.dependencyReader',");
        configurationFields.append("'core.enabledChannels',");
        configurationFields.append("'core.formVariablesReader',");
        configurationFields.append("'core.notifications.defaultConfiguration',");
        configurationFields.append("'core.permissionsForProducts',");
        configurationFields.append("'core.productLabeler',");
        configurationFields.append("'core.scheduler.nonWorkingDays.plugin',");
        configurationFields.append("'core.transactions.status',");
        configurationFields.append("'credential.accessToken.componentFQN',");
        configurationFields.append("'credential.backofficePassword.componentFQN',");
        configurationFields.append("'credential.captcha.componentFQN',");
        configurationFields.append("'credential.certificate.componentFQN',");
        configurationFields.append("'credential.otp.componentFQN',");
        configurationFields.append("'credential.password.componentFQN',");
        configurationFields.append("'credential.sms.componentFQN',");
        configurationFields.append("'frontend.channels.enabledFrequencies',");
        configurationFields.append("'widget.transactions.availableStates',");
        configurationFields.append("'core.encryption.algorithm',");
        configurationFields.append("'core.encryption.key',");
        configurationFields.append("'hmac_hash_function',");
        configurationFields.append("'hmac_key',");
        configurationFields.append("'session.token.secretKey',");
        configurationFields.append("'core.languages',");
        configurationFields.append("'core.allowedFileExtensions',");
        configurationFields.append("'files.allowedFileExtensions',");
        configurationFields.append("'core.transaction.fullDescription.template.data',");
        configurationFields.append("'core.transaction.fullDescription.template.signatures'");
        
        update("configuration", new String[]{"id_group"}, new String[]{""}, "id_field in (" + configurationFields + ")");
        
        deleteConfiguration("client.administration.signatures.maxNeeded");
        
    }
}
