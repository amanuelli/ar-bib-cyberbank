# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]
### Added
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)
### Changed
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-649](https://technisys.atlassian.net/browse/TECENG-649). 

### Improve
- add APM config to all environments [MANNAZCA-14554](https://technisys.atlassian.net/browse/MANNAZCA-14554)

### Fixed
- Declare REDIS_HOST as variable on ConfigMap [TECENG-563](https://technisys.atlassian.net/browse/TECENG-563)
