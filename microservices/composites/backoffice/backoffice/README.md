# Digital BACKOFFICE


This repository contains the BACKOFFICE code. 

## Configuration

Env                                         | Description                                       
--------------------------------------------|---------------------------------------------------
database.username                           | Database username Ex: sa
database.password                           | Database password Ex: password
database.driver                             | Database Driver Ex: com.microsoft.sqlserver.jdbc.SQLServerDriver
database.url                                | Database URL Ex: jdbc:sqlserver://localhost:1433;database=databaseName
database.poolPingQuery                      | Database ping query Ex: SELECT GETDATE()
database.dbms                               | Database dbms Ex: mssql
database.maxActiveConnections               | Database Maximum Active Connections Ex: 200
database.bpm.maxActiveConnections           | Database BPM Maximum Active Connections Ex: 200
database.poolMaximumCheckoutTime            | Database Pool Maximum Checkout Time Ex: 30000
database.maxIdleConnections                 | Database Maximum Idle Connections Ex: 20
database.bpm.maxIdleConnections             | Database BPM Maximum Idle ConnectionsEx: 5
database.bpm.poolMaximumCheckoutTime        | Database BPM Pool Maximum Checkout Time Ex: 20000
database.runDBVS                            | Database auto run scripts to update database Ex: true
i18n.url                                    | I18N address Ex: http://localhost:8077, http://i18n.digital-local
REDIS.PORT                                  | Redis Cache Service address Ex: 6379
REDIS.HOST                                  | Redis Cache Service port Ex: localhost
SES_URL                                     | SES (Safeway Encryption Service) address Ex: http://localhost:8099, http://ses.digital-local
SES_USAGE_KEY                               | SES (Safeway Encryption Service) usage key Ex: 66925f1da83c54354da73d81e013974d
AUDIT_URL                                   | Audit address Ex: http://localhost:8080, http://audit.digital-local
AUDIT_ACTIVE_SIGNED:                        | Sign each audit message before to sent it to the Broker Ex: false, true
AUDIT_BROKER:                               | Audit broker implementation Ex: RabbitMQ, KafkaMQ
AUDIT_MAXIMUM_NUMBER_THREAD:                | Minimum number of thread in the pool, default value = 8
AUDIT_MINIMUM_OBJECT:                       | Minimum number of special ExportingProcess instances residing in the pool, default value = 4
AUDIT_MAXIMUM_OBJECTS:                      | Maximum number of special ExportingProcess instances residing in the pool, default value = 10
AUDIT_VALIDATION_INTERVAL:                  | Time in seconds for periodical checking of minObjects / maxObjects conditions, default value = 5
configuration.mapper.cache.flushInterval    | Configuration Mapper cache flush interval Ex: 0
forms.mapper.cache.flushInterval            | Forms Mapper Cache flush interval Ex: 0
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_OPTS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"


### Kafka Configuration
Env                                         | Description
--------------------------------------------|---------------------------------------------------
AUDIT_BROKER                                | Audit broker implementation Ex: KafkaMQ
AUDIT_SPRING_KAFKAMQ_ADDRESS                | Kafka broker address. Ex: "http://localhost", "http://host.docker.internal", "tec-confluent-oss-cp-kafka-headless"
AUDIT_SPRING_KAFKAMQ_CLIENTID               | Kafka message id. Ex: "digital"
AUDIT_SPRING_KAFKAMQ_PORT                   | Kafka broker port. Ex: "9092"
AUDIT_SPRING_KAFKAMQ_SCHEMA_REGISTRY_URL    | Kafka Schema Registry URL. Ex: "http://localhost:8081", http://tec-confluent-oss-cp-schema-registry:8081"
AUDIT_SPRING_KAFKAMQ_TOPICNAME              | Kafka Topic Name to store the messages. Ex: "queue.audit.logs"

### Rabbit Configuration
Env                                         | Description
--------------------------------------------|---------------------------------------------------
AUDIT_BROKER                                | Audit broker implementation Ex: RabbitMQ
AUDIT_SPRING_RABBITMQ_ADDRESSES:            | Rabbit broker address Ex: localhost, 0.0.0.0, tec-digital-rabbit.local.svc.cluster.local
AUDIT_SPRING_RABBITMQ_PORT:                 | Rabbit broker port Ex: 5672
AUDIT_SPRING_RABBITMQ_QUEUE:                | Rabbit Queue name Ex: audit.queue, my_queue
AUDIT_SPRING_RABBITMQ_EXCHANGE:             | Rabbit exchange Ex: audit.exchange, my_exchange
AUDIT_SPRING_RABBITMQ_ROUTING:              | Rabbit Routing Key Ex: audit.rountingkey, my_routing
AUDIT_SPRING_RABBITMQ_VIRTUALHOST:          | Rabbit Virtualhost Ex: /, root

### Database Engine

In file **values.yaml** on env folders:

deployment.configMap.dbserver.engine: SQLServer | Oracle

Case SQLServer ex:

```

  dbserver:

    engine: SQLServer

    name: tec-digital.database.windows.net

    digitalDBName: tec-digital-test-aks

    driver: com.microsoft.sqlserver.jdbc.SQLServerDriver

```

Case Oracle ex:

```

  dbserver:

    engine: Oracle

    name: 10.12.22.4

    sid: DB12CR2

    driver: oracle.jdbc.OracleDriver

```

