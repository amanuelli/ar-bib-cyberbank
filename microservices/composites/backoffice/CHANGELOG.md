# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [3.2.7]

### Added
- Added register-properties file to Backoffice API [TECDIGSK-645](https://technisys.atlassian.net/browse/TECDIGSK-645)

### Changed

- Update platform backoffice component to 3.2.47 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Update api dependency to version 3.2.5-SNAPSHOT [TECDIGSK-643](https://technisys.atlassian.net/browse/TECDIGSK-643)
- Invitation code cancellation implemented [TECDIGSK-555](https://technisys.atlassian.net/browse/TECDIGSK-555)
- Invitation code resend implemented [TECDIGSK-596](https://technisys.atlassian.net/browse/TECDIGSK-596)
- Invitation codes pop-up window message adjusted when cancelling [TECDIGSK-575](https://technisys.atlassian.net/browse/TECDIGSK-575)
- Invitation codes validations on filters added [TECCDPBO-4834](https://technisys.atlassian.net/browse/TECCDPBO-4834)
- Update version shown in Backoffice and Backoffice API [TECDIGSK-593](https://technisys.atlassian.net/browse/TECDIGSK-593)
- Remove itext dependency from Backoffice [TECDIGSK-633](https://technisys.atlassian.net/browse/TECDIGSK-633)
- Switch backoffice component to use Kafka [TECDIGSK-536](https://technisys.atlassian.net/browse/TECDIGSK-536)
- Cleanup Backoffice legacy Environment module [TECDIGSK-641](https://technisys.atlassian.net/browse/TECDIGSK-641)

## [3.2.6] - 2021-08-23

### Added

- Control Health implemented for Backoffice API [TECDIGSK-514](https://technisys.atlassian.net/browse/TECDIGSK-514)

### Changed

- Update platform backoffice component to 3.2.30 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Switch Backoffice to use RabbitMQ [TECDIGSK-517](https://technisys.atlassian.net/browse/TECDIGSK-517)
- Fix paging error at Invitation Codes list [TECDIGSK-526](https://technisys.atlassian.net/browse/TECDIGSK-526)
- Invitation Codes Send feature implemented [TECDIGSK-485](https://technisys.atlassian.net/browse/TECDIGSK-485)
- Fix status filter not working on Invitation Codes list [TECDIGSK-542](https://technisys.atlassian.net/browse/TECDIGSK-542)
- Invitation Codes Read details feature implemented [TECDIGSK-532](https://technisys.atlassian.net/browse/TECDIGSK-532)

## [3.2.5] - 2021-08-06

### Changed

- Update platform backoffice component to 3.2.26 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Fix StarterKit settings to switch Platform to Kafka [TECDIGSK-504](https://technisys.atlassian.net/browse/TECDIGSK-504)

## [3.2.4] - 2021-08-02

### Added

- Export feature implemented for the Invitation codes list [TECDIGSK-488](https://technisys.atlassian.net/browse/TECDIGSK-488)

### Changed

- Update platform backoffice component to 3.2.24 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)

## [3.2.3] - 2021-06-18

### Changed

- Update platform backoffice component to 3.2.11 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Fix Backoffice Api APM configuration inside charts [TECDIGSK-435](https://technisys.atlassian.net/browse/TECDIGSK-435)
- Fix error in Audit Hist cleanup daemon [TECDIGSK-437](https://technisys.atlassian.net/browse/TECDIGSK-437)

## [3.2.2] - 2021-06-03

### Added

- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
- Added starterkit version of backoffice [TECDIGSK-378](https://technisys.atlassian.net/browse/TECDIGSK-378)
- Dependency check analysis in API

### Changed

- Update platform backoffice component to 3.2.8 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)

### Security

- Update poi and poi-ooxml in API to fix security issue [TECDIGSK-75](https://technisys.atlassian.net/browse/TECDIGSK-75).
