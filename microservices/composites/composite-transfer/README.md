# MS Composite transfer

Cyberbank StarterKit composite transfer microservice.

## Code analysis

[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-transfer&metric=alert_status&token=0b7333062988728efbe79868ae029b628bc0ad15)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-transfer)
[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-transfer&metric=bugs&token=0b7333062988728efbe79868ae029b628bc0ad15)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-transfer)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-transfer&metric=vulnerabilities&token=0b7333062988728efbe79868ae029b628bc0ad15)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-transfer)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-transfer&metric=code_smells&token=0b7333062988728efbe79868ae029b628bc0ad15)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-transfer)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-transfer&metric=coverage&token=0b7333062988728efbe79868ae029b628bc0ad15)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-transfer)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-composite-transfer&metric=duplicated_lines_density&token=0b7333062988728efbe79868ae029b628bc0ad15)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-composite-transfer)

### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"

## Pipelines Status

 CICD     | DEV         | TEST         | DEMO 
----------|-------------|---------------------|---------------
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20Composites/Composite%20Transfer/Master%20-%20Composite%20Transfer?branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=775&branchName=master)   | ![Deploy DEV](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/98/261) | ![Deploy TEST](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/98/262) | ![Deploy DEMO](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/98/263)