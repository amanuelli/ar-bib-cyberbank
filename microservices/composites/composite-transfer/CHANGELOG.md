# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]
## [1.1.5]
### Added
 - Endpoint to list transaction details
### Changed
 - Fixed sorting in transfers list
## [1.1.4]
### Added
 - Validation for invalid accounts and unify same accounts validation
### Changed
 - Updated Swagger
 - Updated Pagination
## [1.1.3]
### Added
 - Validation of cross dates for scheduled transfers

## [1.1.2] - 2021-09-23
### Added
 - Endpoint to cancel transfers without OTP.

## [1.1.1] - 2021-09-15
### Added
- Endpoints to crate and edit transfers without OTP.

## [1.1.0] - 2021-08-02
### Changed
- Moved all code to P2 structure

### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
- Updated fastrack-server to 3.2.14 final version, and flows to 1.0.11  [TECHBANK20-637](https://technisys.atlassian.net/browse/TECHBANK20-637)

## [1.0.7] - 2021-03-30
### Fixed
- Open Api v3 Documentations and execute funcionality from apicatalog [TECDIGSK-327](https://technisys.atlassian.net/browse/TECDIGSK-327)

### Changed
- Update flows to 1.0.8 [TECDIGSK-318](https://technisys.atlassian.net/browse/TECDIGSK-318)
- Update fastarck-api version to final 3.2.0 [TECDIGSK-315](https://technisys.atlassian.net/browse/TECDIGSK-315)
- Updated fastarck-api version [TECDIGSK-296](https://technisys.atlassian.net/browse/TECDIGSK-296)

### Added
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-656](https://technisys.atlassian.net/browse/TECENG-656).
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)


## [1.0.6] - 2021-01-18
### Added
- Scheduled transfers execution hour and timezone from configuration [TECDIGSK-250](https://technisys.atlassian.net/browse/TECDIGSK-250)
- MS Scheduler return trasnactionId  when schedule transaction [TECDIGSK-229](https://technisys.atlassian.net/browse/TECDIGSK-229)
- MS Scheduler integration for transferInternal [TECDIGSK-219](https://technisys.atlassian.net/browse/TECDIGSK-219)

### Changed
- Updated Dockerfile to avoid non-ELK compatible logs

## [1.0.5] - 2020-12-02
### Fixed
- Update helm values: pullPolicy, docker resgistry [TECDISK-192](https://technisys.atlassian.net/browse/TECDIGSK-192)
- Helm, update chart & app versions [TECDIGSK-167](https://technisys.atlassian.net/browse/TECDIGSK-167)

### Changed
- Apply trivy's security container scanning [TECENG-422](https://technisys.atlassian.net/browse/TECENG-422)
- Changes for MS Config integration on tec_digitl_flows [TECDIGSK-129](https://technisys.atlassian.net/browse/TECDIGSK-129)
- Update version of flow to 1.0.5 [TECDIGSK-155](https://technisys.atlassian.net/browse/TECDIGSK-155)

### Security
- Dockerfile base images updated to fix security issue [TECDIGSK-177](https://technisys.atlassian.net/browse/TECDIGSK-177)

## [1.0.4] - 2020-10-07
### Changed
- Update digital.starterkit.api.version 3.0.1-SNAPSHOT -> 3.0.1, digital.platform.api.version 3.0.1-SNAPSHOT -> 3.0.1, flows version to 1.0.4 [TECDIGSK-115](https://technisys.atlassian.net/browse/TECDIGSK-115)
- Refactor release pipeline to use profile template [TECDIGSK-104](https://technisys.atlassian.net/browse/TECDIGSK-104)
- Update version of director-spring-boot-parent 3.0.0  [TECDIGSK-89](https://technisys.atlassian.net/browse/TECDIGSK-89)
- ECS Logback Encoder dependency updated to `0.5.1` [TECDIGSK-57](https://technisys.atlassian.net/browse/TECDIGSK-57)
- Added new configuration value to configure forkJoinPool size used by director [TECDIGSK-101](https://technisys.atlassian.net/browse/TECDIGSK-101)

### Security
- Update version of director-spring-boot-parent 3.0.0  [TECDIGSK-89](https://technisys.atlassian.net/browse/TECDIGSK-89)
- Exclude guava dependency to fix security issue [TECDIGSK-90](https://technisys.atlassian.net/browse/TECDIGSK-90).

## [1.0.3] - Missing in action
## [1.0.2] - 2020-09-14
### Changed
- Update values.yaml for each env

## [1.0.1] - 2020-09-09
### Changed
- Refactor package name (com.technisys.digital.composite). Update flow version [TECDIGSK-60](https://technisys.atlassian.net/browse/TECDIGSK-60) 
- Update profile template path TECDIGSK-51
- Refactor CICD pipelines to use profile templates TECENG-250
- Authorization header was removed from controller methods [TECDIGSK-48](https://technisys.atlassian.net/browse/TECDIGSK-48) 

### Added
- Tests refactor.
- README.md updated with deploy badges

## [1.0.0] - 2020-08-21
### Added
- First transfer migrated to composie. Transfer internal service was migrated using nonTransactionalProcessingFlow and interactiveTransactionalProcessingFlow flows.  [TECDIGSK-24]((https://technisys.atlassian.net/browse/TECDIGSK-24) )
