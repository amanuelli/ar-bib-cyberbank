# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

### Fix
- Fix the changelog. [TECDIGSK-439](https://technisys.atlassian.net/browse/TECDIGSK-439)
- Fix composite backoffice integration reference. [TECDIGSK-562](https://technisys.atlassian.net/browse/TECDIGSK-562)

### Added
- Add configuration to map apm secret how operative system variable [TECDIGSK-682](https://technisys.atlassian.net/browse/TECDIGSK-682)

- The composite backoffice integration were added in the paths of externalgateway. [TECHBANK20-978](https://technisys.atlassian.net/browse/TECHBANK20-978)
- Exposes only authserver public endpoints through the externalgateway. [TECCOSERV-3467] (https://technisys.atlassian.net/browse/TECCOSERV-3467)

## [1.0.5] - 2021-08-18
### Added
- Added the integration with the keycloak. [TECDIGSK-439](https://technisys.atlassian.net/browse/TECDIGSK-439)
- Expose the authserver based in keycloak. [TECDIGSK-420](https://technisys.atlassian.net/browse/TECDIGSK-420)

## [1.0.4] - 2021-07-30
### Added
- Routes to channel support component were added  [TECDIGSK-395](https://technisys.atlassian.net/browse/TECDIGSK-395)

### Changed
- Rename frontend to web in all values [TECDIGSK-547](https://technisys.atlassian.net/browse/TECDIGSK-547)

## [1.0.3] - 2021-04-01
### Added
- Add apm config [TECDIGSK-234](https://technisys.atlassian.net/browse/TECDIGSK-234).

### Changed
- Update external gateway docker files to use apigateway.jar without ms-config dependency [TECDIGSK-207](https://technisys.atlassian.net/browse/TECDIGSK-207).
- Changed routes composite [TECDIGSK-220](https://technisys.atlassian.net/browse/TECDIGSK-220).
- Swagger routes for open API V3 [TECDIGSK-342](https://technisys.atlassian.net/browse/TECDIGSK-342).
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-651](https://technisys.atlassian.net/browse/TECENG-651).
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)
- Update to Profile H v2.0.0 with invoke [TECENG-277](https://technisys.atlassian.net/browse/TECENG-277)
- Update to Profile C v2.0.0 with invoke [TECENG-277](https://technisys.atlassian.net/browse/TECENG-277)


### Fixed
- Fix redirect backoffice [TECDIGSK-357](https://technisys.atlassian.net/browse/TECDIGSK-357).

## [1.0.2] - 2020-12-01
### Fixed
- Fix release externalgateway [TECDIGSK-188](https://technisys.atlassian.net/browse/TECDIGSK-188)
- Fixes release 3.1 [TECDIGSK-178](https://technisys.atlassian.net/browse/TECDIGSK-178)
- Helm, update chart and app versions [TECDIGSK-162](https://technisys.atlassian.net/browse/TECDIGSK-162) 

### Changed
- Apply trivy's security container scanning [TECENG-422](https://technisys.atlassian.net/browse/TECENG-422)
- Updated product image version to `1.1.0` [TECDIGSK-172](https://technisys.atlassian.net/browse/TECDIGSK-172)

## [1.0.1] - 2020-10-01
### Changed
- Updated product image version to `1.0.5` [TECDIGSK-109](https://technisys.atlassian.net/browse/TECDIGSK-109)
- Refactor release pipeline to use profile template [TECDIGSK-97](https://technisys.atlassian.net/browse/TECDIGSK-97)
- Update profile template path [TECDIGSK-53](https://technisys.atlassian.net/browse/TECDIGSK-53)
- Refactor CICD pipelines to use profile templates [TECENG-250](https://technisys.atlassian.net/browse/TECENG-250)

## [1.0.0] - 2020-08-29
### Added
- Added Jira project link to Readme file [TECDIGSK-21](https://technisys.atlassian.net/browse/TECDIGSK-21)

### Changed
- Composite account deployment URL changed [TECDIGSK-35](https://technisys.atlassian.net/browse/TECDIGSK-35)
- Refactor project & CICD pipelines to download APIGATEWAY Jar artifact [TECDIGSK-41](https://technisys.atlassian.net/browse/TECDIGSK-41)

### Fixed
- Removed old composite account route (/v1/accounts) [TECDIGSK-34](https://technisys.atlassian.net/browse/TECDIGSK-34)
- Bash task on continuous integration pipeline fixed [TECDIGSK-33](https://technisys.atlassian.net/browse/TECDIGSK-33)