## Internal Gateway

Use to expose routes to the bank LAN, for example Safeway administrative interface, Backoffice, etc

[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20Internal%20Gateway/Master%20-%20Techbank%20Internal%20Gateway?repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=90&repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)

In order to add HTTPS support, you will have 3 options

#### Option 1: Using a certificate of your own (Docker)
1. Add a .p12 in the path referenced in server.ssl.key-store, in application.yml 
2. Modify the Dockerfile to paste the certificate in the container

```
RUN mkdir -p /opt/externalgateway
COPY your/certificate/path/cert.p12 /opt/externalgateway
```

#### Option 2: Using a certificate inside an external volume (k8s)
1. Add a .p12 in the external volume
2. Modify the externalgateway.yaml to include the external volume with your certificate
    ```
     In externalgateway.yaml, add the following lines inside the container configurations
     volumeMounts:
       - mountPath: /opt/externalgateway
         name: volume
     volumes:
       - name: volume
         persistentVolumeClaim:
           claimName: <volume-name> 
    ```
#### Option 3: Use Let's Encrypt
1. Modify the Dockerfile to allow usage of Let's Encrypt
    ```
        ENV LETSENCRYPT_ENABLED=true
        ENV LETSENCRYPT_EMAIL=email@email.com
        ENV LETSENCRYPT_DOMAIN=<Subdomain.Domain.TLD>
        ENV LETSENCRYPT_PASSWORD=1234567890
    ```

After choosing one of these options, enable SSL in the application.yml
```
server:
  port : 443
  ssl:
    enabled: true
    key-store-password: your_password
    key-store: /opt/externalgateway/cert.p12
    key-store-type: PKCS12
security:
  require-ssl: true
```



### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
BRIDGE.TRACING.ENABLED | To enable elastic apm bridge. Default: true
OPENTRACING.SPRING.CLOUD.GATEWAY.ENABLED | Enable tracing to routes filter. Default:true
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"


#### URL Grouping and Filtered paths
To avoid seeing transactions in kibana where the url has path params (eg: "/account/12345678/statements") it is necessary to define the variable tracingUrlGroups.
In this way we can group all these spans under the same operation name such as "/accounts/*/statements".

We also want to avoid the creation of transactions such as static resources (images, js, etc). For this we need to define the variable tracingFilteredPaths.

Both variables are defined at the path level so we can have many definitions. The syntax used is the same as that used when defining Paths (predicates). You can see more information in this [link](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/util/pattern/PathPattern.html).

```
spring:
  main:
    banner-mode: 'off'
  cloud:
    gateway:
      routes:
        - id: frontend
          uri: http://frontend.local.domain
          predicates:
            - Path=/**
          metadata:
            tracingFilteredPaths: "/static/**, /images/**, /config.js"
            tracingUrlGroups: "/v1/accounts/{accountId}/statements, /v1/accounts/{accountId}"
```

# Documentation

All the documentation for the microservice can be found in this [Confluence.](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/1136427768/API+Gateway)

