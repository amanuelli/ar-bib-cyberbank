if $DEBUG_MODE; then
  java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -jar -Djava.security.egd=file:/dev/./urandom -Dreactor.netty.http.server.accessLogEnabled=true $TECHNISYS_HOME/apigateway.jar
else
  java -jar -Djava.security.egd=file:/dev/./urandom -Dreactor.netty.http.server.accessLogEnabled=true $TECHNISYS_HOME/apigateway.jar
fi
