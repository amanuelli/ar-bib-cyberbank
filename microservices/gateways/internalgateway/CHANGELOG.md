# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [1.0.6]
### Added
- Add configuration to map apm secret how operative system variable [TECDIGSK-682](https://technisys.atlassian.net/browse/TECDIGSK-682)
- added foundation composite product route [TECDIGSK-496](https://technisys.atlassian.net/browse/TECDIGSK-496)
### Changed
- Change Backoffice API routing [TECDIGSK-514](https://technisys.atlassian.net/browse/TECDIGSK-514)

## [1.0.5] - 2021-04-14
### Changed
- fix jar filename used in the Dockerfile [TECDIGSK-363](https://technisys.atlassian.net/browse/TECDIGSK-363)
- remove the unused pipeline release-azure-build.yml

## [1.0.4] - 2021-04-12
### Added
- Update to Profile H v2.0.0 with invoke [TECENG-277](https://technisys.atlassian.net/browse/TECENG-277)
- Update to Profile C v2.0.0 with invoke [TECENG-277](https://technisys.atlassian.net/browse/TECENG-277)
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)
- Add configMap on Helm Chart to define application.yaml by environment
### Changed
- Update product image to version `1.0.4` using platform version `1.1.1` [TECDIGSK-359](https://technisys.atlassian.net/browse/TECDIGSK-359)
- Default route pointing to kibana. Remove jaeger route [TECDIGSK-293](https://technisys.atlassian.net/browse/TECDIGSK-293)


## [1.0.3] - 2020-12-01
### Fixed
- Fix release externalgateway [TECDIGSK-188](https://technisys.atlassian.net/browse/TECDIGSK-188)
- Fixes release 3.1 [TECDIGSK-178](https://technisys.atlassian.net/browse/TECDIGSK-178)
- Helm, update chart and app versions [TECDIGSK-163](https://technisys.atlassian.net/browse/TECDIGSK-163) 
### Changed
- Apply trivy's security container scanning [TECENG-422](https://technisys.atlassian.net/browse/TECENG-422)
- Updated product image version to `1.1.0` [TECDIGSK-172](https://technisys.atlassian.net/browse/TECDIGSK-172)


## [1.0.2] - 2020-10-01
### Changed
- Updated product image version to `1.0.5` [TECDIGSK-23](https://technisys.atlassian.net/browse/TECDIGSK-23)
- Refactor release pipeline to use profile template [TECDIGSK-98](https://technisys.atlassian.net/browse/TECDIGSK-98)
- Update profile template path [TECDIGSK-52](https://technisys.atlassian.net/browse/TECDIGSK-52)
- Refactor CICD pipelines to use profile templates [TECENG-250](https://technisys.atlassian.net/browse/TECENG-250)

## [1.0.0] - 2020-08-29
### Changed
- Refactor project & CICD pipelines to download APIGATEWAY Jar artifact [TECDIGSK-42](https://technisys.atlassian.net/browse/TECDIGSK-42)
