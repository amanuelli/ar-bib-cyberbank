#!/bin/sh

if [ $LETSENCRYPT_ENABLED = true ]; then
    # TODO: Check if certificate already exists (could happen with external volumes) and check mutex

    echo "Installing certbot..."
    apk add certbot

    echo "Creating certificate..."
    echo "A" | certbot register --email $LETSENCRYPT_EMAIL
    certbot certonly -a standalone -d $LETSENCRYPT_DOMAIN

    echo "Installing openssl..."
    apk add openssl
    echo "Converting certificate to .p12..."
    openssl pkcs12 -export -out /opt/externalgateway/cert.p12 -inkey /etc/letsencrypt/live/$LETSENCRYPT_DOMAIN/privkey.pem -in /etc/letsencrypt/live/$LETSENCRYPT_DOMAIN/fullchain.pem -password pass:$LETSENCRYPT_PASSWORD

    echo "Registering cron..."
    echo "0 0,12 * * * root python -c 'import random; import time; time.sleep(86400)' && certbot renew && openssl pkcs12 -export -out /opt/externalgateway/cert.p12 -inkey /etc/letsencrypt/live/$LETSENCRYPT_DOMAIN/privkey.pem -in /etc/letsencrypt/live/$LETSENCRYPT_DOMAIN/fullchain.pem" -password pass:$LETSENCRYPT_PASSWORD | tee -a /etc/crontab > /dev/null
fi

java -Djava.security.egd=file:/dev/./urandom -jar /apigateway.jar

# CMD
exec "$@"
