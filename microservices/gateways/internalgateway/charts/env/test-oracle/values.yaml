# Default values for api.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

name: internalgateway

platform: AKS

externalURL: digital-test-oracle-int.technisys.net

ocp:
  routerName: default

labels:
  app: internalgateway
  lang: java
  platform: digital

selectorLabels:
  app: internalgateway

deployment:

  image:
    repository: tecdigitalacr.azurecr.io/techbank/internalgateway
    version: #{MS_VERSION}#
    pullPolicy: IfNotPresent

  replicaCount: 1

  port: 8080

  envFrom:
    - configMapRef:
        name: internalgateway-config
    - secretRef:
        name: internalgateway-secrets

  volumeMounts:
    - mountPath: /usr/local/application.yaml
      name: spring-application-yaml
      subPath: application.yaml
  volumes:
    - name: spring-application-yaml
      configMap:
        name: internalgateway-spring-config
        items:
          - key: spring_application_yaml
            path: application.yaml

  env:
    - name: DIGITAL_NAMESPACE
      valueFrom:
        fieldRef:
          fieldPath: metadata.namespace

  livenessProbe:
    httpGet:
      path: /actuator/health
      port: 8080
    initialDelaySeconds: 60
    periodSeconds: 10

  readinessProbe:
    httpGet:
      path: /actuator/health
      port: 8080
    initialDelaySeconds: 30
    periodSeconds: 15



  imagePullSecrets:
    - name: regcred

  serviceAccountName: tecdigital-sa

  resources:
    limits:
      cpu: 750m
      memory: 750Mi
    requests:
      cpu: 100m
      memory: 256Mi


service:
  type: LoadBalancer
  port: 80
  targetPort: 8080
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-internal: "true"
    service.beta.kubernetes.io/azure-load-balancer-internal-subnet: "subnet-aks-ilb"
  loadBalancerIP: 10.12.34.13


configPodReset:
  - configmap.yaml
  - appyamlconfigmap.yaml

configMap:
  name: internalgateway-config
  JAVA_TOOL_OPTIONS: "-javaagent:/home/technisys/elastic-apm-agent.jar"
  ELASTIC_APM_SERVER_URLS: "#{apmserver.url}#"
  ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
  ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
  ELASTIC_APM_APPLICATION_PACKAGES: "com.technisys"
  ELASTIC_APM_CENTRAL_CONFIG: "true"
  ELASTIC_APM_SERVICE_NAME: "internalgateway"
  ELASTIC_APM_LOG_LEVEL: "INFO"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "true"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: "com.technisys.digital.apigateway.*"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: "(?-i)java.*, (?-i)javax.*, (?-i)sun.*, (?-i)com.sun.*,
                                                          (?-i)jdk.*, (?-i)org.apache.tomcat.*,
                                                          (?-i)org.apache.catalina.*, (?-i)org.apache.coyote.*,
                                                          (?-i)org.eclipse.jetty.*"
  ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc, redis"
  SPRING_CONFIG_LOCATION: /usr/local/application.yaml
  WHITE_LISTED_HEADERS: "Accept,Authorization,Cookie,User-Agent,X-Forwarded-For"

springConfig:
  name: internalgateway-spring-config
  spring:
    applicationYaml: |-
      server:
        port: 8080
      #  ssl:
      #    enabled: true
      #    key-store-password: 1234567890
      #    key-store: /opt/internalgateway/cert.p12
      #    key-store-type: PKCS12
      #security:
      #  require-ssl: true

      management:
        endpoints:
          web:
            exposure:
              include: 'health'
      opentracing:
        jaeger:
          service-name: internalgateway
          log-spans: false

      spring:
        main:
          banner-mode: 'off'
        cloud:
          gateway:
            #      httpclient:
            #        ssl:
            #          useInsecureTrustManager: true # Accepts any certificate coming from the routed servers.
            # Don't use in production
            routes:
              - id: backoffice-api
                uri: http://backoffice-api.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/backoffice/api/**
                filters:
                  - RewritePath=/backoffice/api/(?<segment>.*), /$\{segment}
                metadata:
                  tracingUrlGroups: "/backoffice/api/v1/environments/{environmentId}/**"
              
              - id: cybo-login
                uri: http://cyboweb.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/backoffice/,/backoffice/login
                filters:
                  - RedirectWithParamsTo=302, /backoffice/cybo/login

              - id: cybo-error
                uri: http://cyboweb.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/backoffice/error
                filters:
                  - RedirectWithParamsTo=302, /backoffice/cybo/error

              - id: cybo
                uri: http://cyboweb.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/backoffice/cybo/**
                filters:
                  - RewritePath=/backoffice/cybo/(?<segment>.*), /$\{segment}

              - id: backoffice
                uri: http://backoffice.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/backoffice/**

              - id: safeway
                uri: http://safeway.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/safeway/**

              - id: safewaysamples
                uri: http://safewaysamples.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/safewaysamples/**

              - id: misc
                uri: http://misc.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/misc/**
                metadata:
                  tracingFilteredPaths: "/misc/**"

              - id: kiali
                uri: http://kiali.istio-system.svc.cluster.local:20001
                predicates:
                  - Path=/kiali/**

              - id: foundationCompositeProduct
                uri: http://foundation-composite-product.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/foundation/product/**
                filters:
                  - RewritePath=/foundation/product/(?<segment>.*), /$\{segment}

              - id: foundationCompositeTransfer
                uri: http://foundation-composite-transfer.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/foundation/transfer/**
                filters:
                  - RewritePath=/foundation/transfer/(?<segment>.*), /$\{segment}

              - id: foundationCompositeCustomer
                uri: http://foundation-composite-customer.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/foundation/customer/**
                filters:
                  - RewritePath=/foundation/customer/(?<segment>.*), /$\{segment}

              - id: foundationCompositeTransaction
                uri: http://foundation-composite-transaction.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/foundation/transaction/**
                filters:
                  - RewritePath=/foundation/transaction/(?<segment>.*), /$\{segment}

              - id: foundationCompositeAccount
                uri: http://foundation-composite-account.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/foundation/account/**
                filters:
                  - RewritePath=/foundation/account/(?<segment>.*), /$\{segment}

              - id: foundationCompositeCard
                uri: http://foundation-composite-card.${DIGITAL_NAMESPACE}
                predicates:
                  - Path=/foundation/card/**
                filters:
                  - RewritePath=/foundation/card/(?<segment>.*), /$\{segment}

              # The filter is necessary as kibana fails. Kibana use cookies
              #- id: kibana
              #  uri: http://10.12.16.8:5601
              #  predicates:
              #    - Path=/**
              #  filters:
              #    - RemoveRequestHeader=Authorization

      #        - id: redirect_root
      #          uri: http://www.technisys.com
      #          predicates:
      #            - Path=/**
      #          filters:
      #           - RedirectTo=302, /backoffice

secrets:
  name: internalgateway-secrets
  elastic:
    apmtoken: "#{elastic.apmtoken}#" 