#!/bin/bash
BASE_DIR=`realpath $0`
BASE_DIR=`dirname $BASE_DIR`
cd $BASE_DIR/

show_general_help() {
    echo "Usage dev_environment_manager.sh command"
    echo ""
    echo "  build_api:              Builds the api"
    echo "  build_backoffice:       Builds the backoffice"
    echo "  build_backoffice_api:   Builds the backoffice_api"
    echo "  build_externalgate:     Builds the external gateway microservice"
    echo "  build_internalgate:     Builds the internal gateway microservice"
    echo "  build_account:          Builds the account microservice"
    echo "  build_transfer:         Builds the transfer microservice"
    echo "  build_audit:            Builds the audit microservice"
    echo "  build_api_catalog:      Builds the api catalog microservice"
    echo "  build_authserver:       Builds the auth server microservice"
    echo "  build_configuration:    Builds the configuration microservice"
    echo "  build_i18n:             Builds the i18n microservice"
    echo "  build_limits            Builds limits microservice"
    echo "  build_notifications     Builds the notification microservice"
    echo "  build_onb:              Builds the onb microservice"
    echo "  build_safeway           Builds safeway microservice"
    echo "  build_scheduler:        Builds the scheduler"
    echo "  build_ses:              Builds ses microservice"
    echo "  build_staff             Builds staff management microservice"
    echo "  stop:       Stop environment"
    echo "  help:       Show this menu"
}

build_jar() {
    actual_dir=$(pwd)
    cd $1
    mvn clean -Dmaven.test.skip=true && mvn clean install -am -Dmaven.test.skip=true
    cd $actual_dir
}

build_docker_image() {
    actual_dir=$(pwd)
    cd $1
    version="SNAPSHOT"
    #repository="tecdigitalacr.azurecr.io/digital"
    repository=$3
    component=$2
    os=$(uname)
    if [ $os != "Darwin" ]; then
      eval $(minikube docker-env)
    fi
    az acr login -n tecdigitalacr
    docker build --build-arg curl_opts='-u omni-services-read:OmniServicesRead' \
    --build-arg ARTIFACTORY_USER=omni-services-read \
    --build-arg ARTIFACTORY_PASSWORD=OmniServicesRead \
    --build-arg MVN_LOCAL_REPO=/Users/jsalame/.m2/repository \
    --tag $repository/$component:$version .
    cd $actual_dir
    echo "Shutting down pod:" $component
    kubectl scale deployment $component --replicas=0 -n digital-local
    echo "Starting pod:" $component
    kubectl scale deployment $component --replicas=1 -n digital-local
}

build_api() {
    build_jar capabilities/api
    build_docker_image capabilities/api api tecdigitalacr.azurecr.io/digital
}

build_backoffice() {
    build_jar composites/backoffice/backoffice
    build_docker_image composites/backoffice/backoffice backoffice tecdigitalacr.azurecr.io/techbank
}

build_backoffice_api() {
    build_jar composites/backoffice/backoffice-api
    build_docker_image composites/backoffice/backoffice-api backoffice-api tecdigitalacr.azurecr.io/digital
}

build_externalgate() {
    build_docker_image gateways/externalgateway externalgateway tecdigitalacr.azurecr.io/techbank
}

build_internalgate() {
    build_docker_image gateways/internalgateway internalgateway tecdigitalacr.azurecr.io/techbank
}

build_account() {
    build_jar composites/composite-account
    build_docker_image composites/composite-account composite-account tecdigitalacr.azurecr.io/techbank
}

build_transfer() {
    build_jar composites/composite-transfer
    build_docker_image composites/composite-account composite-transfer tecdigitalacr.azurecr.io/techbank
}

build_api_catalog() {
    build_docker_image cross/apicatalog apicatalog tecdigitalacr.azurecr.io/techbank
}

build_audit() {
  build_jar capabilities/audit
    build_docker_image capabilities/audit audit tecdigitalacr.azurecr.io/digital
}

build_authserver() {
    build_docker_image capabilities/authserver authserver tecdigitalacr.azurecr.io/techbank
}

build_configuration() {
    build_jar capabilities/configuration
    build_docker_image capabilities/configuration configuration tecdigitalacr.azurecr.io/techbank
}

build_i18n() {
    build_jar capabilities/i18n
    build_docker_image capabilities/i18n i18n tecdigitalacr.azurecr.io/techbank
}

build_limits() {
    build_docker_image capabilities/limits limits tecdigitalacr.azurecr.io/techbank
}

build_notifications() {
    build_docker_image capabilities/notifications notifications tecdigitalacr.azurecr.io/techbank
}

build_onb() {
    build_docker_image capabilities/onboarding onboarding tecdigitalacr.azurecr.io/techbank
}

build_safeway() {
    build_docker_image capabilities/safeway safeway tecdigitalacr.azurecr.io/digital
}


build_scheduler() {
    build_docker_image capabilities/scheduler scheduler tecdigitalacr.azurecr.io/techbank
}

build_ses() {
    build_docker_image capabilities/ses ses tecdigitalacr.azurecr.io/digital
}

build_staff() {
    build_docker_image capabilities/staffmanagement staffmanagement tecdigitalacr.azurecr.io/digital
}

build_transactions() {
    build_docker_image capabilities/transactions transactions tecdigitalacr.azurecr.io/techbank
}

case $# in
    0)
        show_general_help
    ;;
    *)
        case $1 in
            build_api)
                build_api
            ;;
            build_backoffice)
                build_backoffice
            ;;
            build_backoffice_api)
                build_backoffice_api
            ;;
            build_externalgate)
                build_externalgate
            ;;
            build_internalgate)
                build_internalgate
            ;;
            build_account)
                build_account
            ;;
            build_transfer)
                build_transfer
            ;;
            build_audit)
                build_audit
            ;;
            build_api_catalog)
                build_api_catalog
            ;;
            build_authserver)
                build_authserver
            ;;
            build_configuration)
                build_configuration
            ;;
            build_i18n)
                build_i18n
            ;;
            build_limits)
                build_limits
            ;;
            build_notifications)
                build_notifications
            ;;
            build_onb)
                build_onb
            ;;
            build_safeway)
                build_safeway
            ;;
            build_scheduler)
                build_scheduler
            ;;
            build_ses)
                build_ses
            ;;
            build_staff)
                build_staff
            ;;
            build_transactions)
                build_transactions
            ;;
            *)
                show_general_help
            ;;
        esac
    ;;
esac
