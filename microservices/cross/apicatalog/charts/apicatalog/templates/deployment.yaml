apiVersion: apps/v1
kind: Deployment
metadata:
  name: {{ .Values.name }}
  labels:
    {{- toYaml .Values.labels | nindent 4 }}
spec:
  replicas: {{ .Values.deployment.replicaCount }}
  {{- if .Values.deployment.strategy }}
  strategy:
    {{- toYaml .Values.deployment.strategy | nindent 4 }}
  {{- end }}
  selector:
    matchLabels:
      {{- toYaml .Values.selectorLabels | nindent 6 }}
  template:
    metadata:
      labels:
        {{- toYaml .Values.labels | nindent 8 }}
      annotations:
        {{- if .Values.deployment.annotations }}
          {{- toYaml .Values.deployment.annotations | nindent 8 }}
          {{ end }}
          {{- range $config := .Values.configPodReset }}
          checksum/{{$config}}: {{ include (print $.Template.BasePath "/" $config ) . | sha256sum }}
          {{ end }}
    spec:
      {{- with .Values.deployment.imagePullSecrets }}
      imagePullSecrets:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      serviceAccountName: {{ .Values.deployment.serviceAccountName }}
      {{- if .Values.deployment.securityContext }}
      securityContext:
        {{- toYaml .Values.deployment.securityContext | nindent 8 }}
      {{ end }}
      {{- if .Values.deployment.volumes }}
      {{- with .Values.deployment.volumes }}
      volumes:
        {{- toYaml . | nindent 8 }}
      {{- end }}
      {{- end }}
      {{- if .Values.deployment.tolerations }}
      tolerations:
        {{- toYaml .Values.deployment.tolerations | nindent 8}}
      {{ end }}
      {{- if .Values.deployment.affinity }}
      affinity:
        {{- toYaml .Values.deployment.affinity | nindent 8 }}
      {{ end }}
      {{- if .Values.deployment.initContainers }}
      initContainers:
        {{- toYaml .Values.deployment.initContainers | nindent 8 }}
      {{ end }}
      {{- if .Values.deployment.nodeSelector }}
      nodeSelector:
        {{- toYaml .Values.deployment.nodeSelector | nindent 8 }}
      {{ end }}
      containers:
        - name: {{ .Chart.Name }}
          image: "{{ .Values.deployment.image.repository }}:{{ .Values.deployment.image.version }}"
          imagePullPolicy: {{ .Values.deployment.image.pullPolicy }}
          ports:
            - name: http
              containerPort: {{ .Values.deployment.port }}
              protocol: {{ .Values.deployment.protocol | default "TCP" }}
          {{- if .Values.deployment.livenessProbe }}
          livenessProbe:
            {{- toYaml .Values.deployment.livenessProbe | nindent 12 }}
          {{- end }}
          {{- if .Values.deployment.readinessProbe }}
          readinessProbe:
            {{- toYaml .Values.deployment.readinessProbe | nindent 12 }}
          {{- end }}
          resources:
            {{- toYaml .Values.deployment.resources | nindent 12 }}
          {{- if .Values.deployment.envFrom }}
          envFrom:
            {{- toYaml .Values.deployment.envFrom | nindent 12 }}
          {{- end }}
          {{- if .Values.deployment.env }}
          env:
            {{- toYaml .Values.deployment.env | nindent 12 }}
          {{- end }}
          {{- if .Values.deployment.volumeMounts }}
          volumeMounts:
            {{- toYaml .Values.deployment.volumeMounts | nindent 12 }}
          {{- end }}