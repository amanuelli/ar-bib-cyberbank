    # Continuous Integration
    
    The CI process generate two artifacts as result:
    
    - Docker image
    - Helm chart
    
    The Docker image version follow the pattern: **(BuildNumber)_$(LastCommit)**: 
     - BuildNumber: Azure pipeline build number as autoincremental values.
     - LastCommit: Last commit truncate by first 8 characters that identify last changes.
    
    ## Docker image
    
    Based on base or platform images the project compile the source code and generate the jar artifact on new Docker image 
    to be deployed.
    
    ## Helm Chart
    
    The Helm chart define the way of MS deployment on K8S platform. The chart definition will be validated, packaged and 
    published on Azure Container Registry **tecdigitalacr** with defined appversion and version.
    
    -  **appVersion**: Docker app version. Example: stable latest version: 3.0
    -  **version**: Chart version that represent latest deployment definition. Example to upgrade this version: updates of K8S templates. 
    
    **NOTE**: the appversion will be updated after new release change based on pom file definition.
    
    # Continuous Deployment
    
    After CI process the new procces will be triggered by Azure DevOps release pipeline definition to promote the 
    latest changes on **Dev**, **Test** and **Demo** environment.
      
[Azure DevOps pipeline](https://dev.azure.com/technisys/TEC%20-%20Digital/_release?definitionId=52)
