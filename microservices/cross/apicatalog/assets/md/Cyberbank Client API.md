# Cyberbank Client API

### Components
- Traditional API
- Accounts
- Transfers
- DAS

#### Traditional API
This solution represets the full actions availables on cyberbank client system.

#### Accounts
This microservice contains all actions related with accounts

#### Transfers
This microservice contains all actions related with transfers

#### DAS
This microservice contains all actions related with assistants

### Error list

#### General
|Return Code|Code|Error message|Category|
|-|-|-|-|
| returnCode.BOF001E | BOF001E | Bad username/password combination | Backoffice |
| returnCode.BOF002W | BOF002W | Validation error | Backoffice |
| returnCode.BOF007E | BOF007E | This operation is not enabled. | Backoffice |
| returnCode.BOF009W | BOF009W | Selected filter parameters are not correct | Backoffice |
| returnCode.BOF010E | BOF010E | Domain user blocked or disabled | Backoffice |
| returnCode.BOF011E | BOF011E | Domain user requires a password change | Backoffice |
| returnCode.BOF012E | BOF012E | Unexpected error authenticating domain user | Backoffice |
| returnCode.BOF999E | BOF999E | Internal error | Backoffice  
| returnCode.API003E | API003E | The session activity (idActivity) is not valid | Client |
| returnCode.API006E | API006E | Session is not valid (exchange token) | Client |
| returnCode.API007E | API007E | Waiting time to complete login expired | Client |
| returnCode.API010E | API010E | You have logged in in other device | Client |
| returnCode.API010W | API010W | The amount of attempts has been exceeded, to continue please re-login | Client |
| returnCode.API019W | API019W | User and/or password not valid | Client |
| returnCode.API020W | API020W | User and/or password not valid | Client |
| returnCode.API021W | API021W | User and/or OTP not valid | Client |
| returnCode.API022E | API022E | The key is blocked | Client |
| returnCode.API024E | API024E |  | Client |
| returnCode.API025E | API025E |  | Client |
| returnCode.API026W | API026W | User and/or OTP not valid | Client |
| returnCode.API034E | API034E | The campaign activity (idActivity) is not valid | Client |
| returnCode.API035E | API035E | The onboarding activity (idActivity) is not valid | Client |
| returnCode.CLI001E | CLI001E | The HTTP status code is invalid. | Client |
| returnCode.CLI002E | CLI002E | Internal error | Client |
| returnCode.CLI003E | CLI003E | Error in the URL of connection with the API | Client |
| returnCode.CLI999E | CLI999E | Yikes! An error prevented your request from processing. We're truly sorry | Client |
| returnCode.COR019E | COR019E | Blocked user | Client |
| returnCode.COR048W | COR048W | Wrong user and/or key | Client |
| returnCode.COR080W | COR080W | The size of the selected file surpasses the limit allowed | Client |
| returnCode.COR087E | COR087E | The form identifier specified is not valid | Client |
| returnCode.COR088W | COR088W | You have not submitted a valid OTP | Client |
| returnCode.COR092W | COR092W | Please, validate the captcha | Client |
| returnCode.API002E | API002E | The method is not supported by this resource; OPTIONS returns the supported methods | Core |
| returnCode.API004E | API004E | Access token expired | Core |
| returnCode.API004W | API004W | Access token expired | Core |
| returnCode.API005E | API005E | Access token is invalid | Core |
| returnCode.API005W | API005W | Access token is invalid | Core |
| returnCode.API007W | API007W | Waiting time to complete login expired | Core |
| returnCode.API008E | API008E | The IP address does not match the one used to start the login | Core |
| returnCode.API009E | API009E | The User-Agent does not match the one used to start the login | Core |
| returnCode.API016W | API016W | Data entered invalid or user not enabled | Core |
| returnCode.API017W | API017W | Invalid user and / or password | Core |
| returnCode.API018W | API018W | Invalid user and / or password | Core |
| returnCode.API028E | API028E | The enrollment activity (idActivity) is invalid | Core |
| returnCode.API033E | API033E | Facebook activity (idActivity) is invalid | Core |
| returnCode.API041E | API041E | This app version is not supported, please download the latest one from the store | Core |
| returnCode.API044I | API044I | Meaningful HTTP status code | Core |
| returnCode.COR001E | COR001E | Internal error | Core |
| returnCode.COR002E | COR002E | Internal error | Core |
| returnCode.COR003E | COR003E | Internal error | Core |
| returnCode.COR006E | COR006E | Internal error | Core |
| returnCode.COR007E | COR007E | Internal error | Core |
| returnCode.COR011E | COR011E | Internal error | Core |
| returnCode.COR015E | COR015E | Internal error | Core |
| returnCode.COR018E | COR018E | Incorrect user or password | Core |
| returnCode.COR020W | COR020W | Validation error | Core |
| returnCode.COR026E | COR026E | Internal error | Core |
| returnCode.COR027W | COR027W | You have not submitted a valid OTP | Core |
| returnCode.COR038E | COR038E | Invalid phone number | Core |
| returnCode.COR039E | COR039E | Invalid channel | Core |
| returnCode.COR042E | COR042E | Internal error | Core |
| returnCode.COR050W | COR050W | Incorrect captcha | Core |
| returnCode.COR051E | COR051E | Failed to verify Captcha (communication error with reCaptcha) | Core |
| returnCode.COR074E | COR074E | Configuration file not found | Core |
| returnCode.COR075E | COR075E | Java Cryptography Extension (JCE) was not detected in the Java version used by the system | Core |
| returnCode.COR076E | COR076E | Error with database | Core |
| returnCode.COR077E | COR077E | Internal error | Core |
| returnCode.COR078E | COR078E | Internal error | Core |
| returnCode.COR079E | COR079E | Invocation parameter error | Core |
| returnCode.COR082E | COR082E | Validation code has expired | Core |
| returnCode.COR083E | COR083E | Validation code is invalid | Core |
| returnCode.COR085E | COR085E | Data exchange token with Backoffice is invalid | Backoffice |
| returnCode.COR086E | COR086E | Data exchange token with Backoffice has expired | Backoffice |
| returnCode.COR097E | COR097E | The system is not available | Core |
| returnCode.COR102W | COR102W | Access token expired | Core |
| returnCode.COR103W | COR103W | Access token is invalid | Core |
| returnCode.COR104W | COR104W | Access token is invalid | Core |
| returnCode.COR105W | COR105W | Access token is invalid | Core |
| returnCode.COR107E | COR107E | Can not modify already executed scheduled transaction | Core |
| returnCode.COR999E | COR999E | Error processing request | Core |

#### Authenticated
|Return Code|Code|Error message|Category|
|-|-|-|-|
| returnCode.BOF005E | BOF005E | Environment data is not suitable for the current management type | Backoffice |
| returnCode.BOF006E | BOF006E | The environment is not configured to use this type of administration | Backoffice |
| returnCode.API023E | API023E | It was not possible to change the password | Client |
| returnCode.API029E | API029E | It was not possible to assign the OTP | Client |
| returnCode.API030E | API030E | It was not possible to assign the password | Client |
| returnCode.API031E | API031E | It was not possible to assign the password | Client |
| returnCode.API041W | API041W | Your biometric credential session has expired; log in normally and configure its use again. | Client |
| returnCode.API503E | API503E | The environment data are not fit for the current type of administration | Client |
| returnCode.API504E | API504E | Error in sending notifications | Client |
| returnCode.API505E | API505E | The environment is not configured to use this type of administration | Client |
| returnCode.API506E | API506E | The email address does not match the one registered in your personal data. | Client |
| returnCode.API507E | API507E | The invitation could not be sent. Try again later. | Client |
| returnCode.API508E | API508E | The invitation code is not correct | Client |
| returnCode.API508W | API508W | The invitation code is not correct | Client |
| returnCode.API509E | API509E | The invitation code generated has expired. Request for a new invitation. | Client |
| returnCode.API509W | API509W | The invitation code generated has expired. loans.list.emptyRequest for a new invitation. | Client |
| returnCode.API510E | API510E | The invitation code that you are trying to use has already been used. Request for a new invitation. | Client |
| returnCode.API510W | API510W | The invitation code that you are trying to use has already been used. Request for a new invitation. | Client |
| returnCode.API511E | API511E | The invitation code that you are trying to use has been annulled. Request for a new invitation. | Client |
| returnCode.API511W | API511W | The invitation code that you are trying to use has been annulled. Request for a new invitation. | Client |
| returnCode.API512E | API512E | The data associated to the invitation code is not correct | Client |
| returnCode.API513E | API513E | There is already a registered user with the same information | Client |
| returnCode.COR090E | COR090E | Configuration error in Safeway plugin. | Client |
| returnCode.API001E | API001E | The requested resource does not exist; See the documentation | Core |
| returnCode.API027W | API027W | User is locked | Core |
| returnCode.API042W | API042W | Session waiting for verification | Core |
| returnCode.API043W | API043W | Existing account | Core |
| returnCode.BAK008E | BAK008E | Error invoking service | Core |
| returnCode.COR004E | COR004E | This option is momentarily out of order | Core |
| returnCode.COR005E | COR005E | You are not authorized to perform this operation | Core |
| returnCode.COR016E | COR016E | Not all credentials required to perform this operation were received | Core |
| returnCode.COR017E | COR017E | The user is locked in the environment | Core |
| returnCode.COR029E | COR029E | The user is not linked to the requested environment | Core |
| returnCode.COR030E | COR030E | Error sending message | Core |
| returnCode.COR031E | COR031E | Error reading form | Core |
| returnCode.COR032E | COR032E | Error reading form | Core |
| returnCode.COR040E | COR040E | Channel not enabled for the user in the environment | Core |
| returnCode.COR041E | COR041E | Unauthorized PC | Core |
| returnCode.COR047E | COR047E | The user is not active in any environment | Core |
| returnCode.COR052E | COR052E | There is no valid Omnichannel license registered in the system | Core |
| returnCode.COR091W | COR091W | Invalid PIN | Core |
| returnCode.COR092E | COR092E | Data exchange token with Backoffice is invalid | Backoffice |

#### Transactions
|Return Code|Code|Error message|Category|
|-|-|-|-|
| returnCode.CLI000I | CLI000I | The operation has been completed | Client |
| returnCode.COR043E | COR043E | The transaction does not match this activity identifier | Client |
| returnCode.API040E | API040E | You do not have the type of product needed to perform this operation | Core |
| returnCode.COR000I | COR000I | Operation executed successfully | Core |
| returnCode.COR013I | COR013I | The transaction has been scheduled to be sent to the bank | Core |
| returnCode.COR014W | COR014W | The scheduled date is not valid | Core |
| returnCode.COR023I | COR023I | The transaction requires additional signatures to be sent to the bank | Core |
| returnCode.COR024W | COR024W | This transaction requires the approval of another authorized user | Core |
| returnCode.COR025E | COR025E | The request could not be completed. | Core |
| returnCode.COR035E | COR035E | Invalid transaction | Core |
| returnCode.COR036E | COR036E | The request could not be taken, verify that it was not taken or terminated by another user. | Core |
| returnCode.COR037E | COR037E | The request could not be queried, verify that it was not taken or terminated by another user. | Core |
| returnCode.COR044E | COR044E | Your request has already been processed in the last few seconds, please retry later. | Core |
| returnCode.COR059I | COR059I | This operation is pending submission of documents. | Core |
| returnCode.COR060I | COR060I | This operation is being processed by the bank. | Core |
| returnCode.COR061E | COR061E | It is not possible to save a draft. The transaction is not in a valid state. | Core |
| returnCode.COR062E | COR062E | The transaction is already being processed at this time | Core |
| returnCode.COR064E | COR064E | Unfortunately it is not possible to process the request, duplicate signature schemes have been detected in the environment. | Core |
| returnCode.COR094I | COR094I | The transaction has been scheduled | Core |
| returnCode.COR095E | COR095E | Error handling calendar | Core |
| returnCode.COR096E | COR096E | Scheduling program is invalid | Core |
| returnCode.COR096I | COR096I | The transaction has been scheduled | Core |

#### Transactions with amount
|Return Code|Code|Error message|Category|
|-|-|-|-|
| returnCode.API514W | API514W | Total amount can not be zero | Client |
| returnCode.COR093E | COR093E | Invalid execution mode for limit control | Client |
| returnCode.COR094E | COR094E | Action not valid for limit control | Client |
| returnCode.COR098W | COR098W | This transaction exceeds your weekly user limit authorized. | Client |
| returnCode.COR099W | COR099W | This transaction exceeds your weekly product limit authorized. | Client |
| returnCode.COR100W | COR100W | This transaction exceeds your weekly environment limit authorized. | Client |
| returnCode.FLO0002W | FLO0002W | This transaction exceeds your authorized per signature limit | Client |
| returnCode.FLO0003W | FLO0003W | No signature for product | Client |
| returnCode.FLO0004W | FLO0004W | This transaction exceeds your authorized per environment daily limit | Client |
| returnCode.FLO0005W | FLO0005W | This transaction exceeds your authorized per environment weekly limit | Client |
| returnCode.FLO0006W | FLO0006W | This transaction exceeds your authorized per environment montlhy limit | Client |
| returnCode.FLO0007W | FLO0007W | This transaction exceeds your authorized per environment transaction limit | Client |
| returnCode.FLO0008W | FLO0008W | This transaction exceeds your authorized per user daily limit | Client |
| returnCode.FLO0009W | FLO0009W | This transaction exceeds your authorized per user weeky limit | Client |
| returnCode.FLO0010W | FLO0010W | This transaction exceeds your authorized per transaction limit | Client |
| returnCode.FLO0015W | FLO0015W | This transaction exceeds your authorized per user montlhy limit | Client |
| returnCode.COR054W | COR054W | Either no signature was found, or none of the available ones have enough cap to authorize this transaction | Core |
| returnCode.COR055W | COR055W | There is no sign scheme with sufficient limit to perform this transaction | Core |
| returnCode.COR065W | COR065W | This transaction exceeds your authorized per-user daily limit | Core |
| returnCode.COR066W | COR066W | This transaction exceeds your authorized monthly user limit | Core |
| returnCode.COR067W | COR067W | This transaction exceeds your authorized limit per user | Core |
| returnCode.COR068W | COR068W | This transaction exceeds your authorized daily limit per product | Core |
| returnCode.COR069W | COR069W | This transaction exceeds your authorized monthly limit per product | Core |
| returnCode.COR070W | COR070W | This transaction exceeds your limit per product per authorized transaction | Core |
| returnCode.COR071W | COR071W | This transaction exceeds your authorized daily environment limit | Core |
| returnCode.COR072W | COR072W | This transaction exceeds your authorized monthly limit per environment | Core |
| returnCode.COR073W | COR073W | This transaction exceeds your limit per environment per authorized transaction | Core |