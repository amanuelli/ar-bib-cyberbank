# Cyberbank CPG 2.0 API

The **Cyberbank CPG 2.0 API** is a set of endpoints that will usually be invoked from a client application such as a Backoffice.