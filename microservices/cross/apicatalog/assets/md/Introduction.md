# API CATALOG
## What is an API Rest?
The API REST can be considered as a software layer which enables the different software development stacks to communicate to each other, talking a common data information language.

<img src="/catalog/images/Software%20Stack%20representation.png" alt="Different Software Stacks" width="800" style="margin: auto; display: block;"/>

> **Figure 1:** Representation of different Customers (internal and/or external) using a variety of Software Stacks.

An API gives you the power to bring and save data information using a data structure based on a standard format (usually JSON). This way, you don’t need to know which data store engine there is, even to know how the information providing software works.

<img src="/catalog/images/Software%20Stack%20representation%20and%20API%20Rest.png" alt="API Restfull" width="800" style="margin: auto; display: block;"/>

> **Figure 2:** API Rest enables communication of different stack developments between them using a standard way for the data exchange.

By following a basic parameters structure defined by the API documentation, you can get the accurate information you are looking for and also save any collected information.
For more information: *maybe to add a link*

## What are Microservices?
Microservices are basically an approach used to build software applications. Unlike traditional software architecture, known as Monolith, microservices-based software development allows building an end-to-end application separated into small blocks called microservices.

<img src="/catalog/images/Monolithic%20app%20versus%20Microservices%20App.png" alt="Monolithic vs MS Application" width="800" style="margin: auto; display: block;"/>

> **Figure 3:** A graphic representation of a monolithic application versus a microservices application.

Every microservice is distributed and freely coupled. This means that the final software app is builded with many microservices conforming a whole software solution. To conform to the Final Product, every microservice works as a specific component of the whole software product (Figure 3, B image), sharing information (talking) with the rest of the microservices through the common language: API Rest.

### Advantages to use this architecture
- Implementing microservices enables the different software engineering teams to build a specific part of the software using the most appropriate technology or stack
- Maintaining is also another great benefit. Every microservice can be updated by bug fixing or by increasing its functionality without affecting the rest of software product
- Even the software stack used to code all the microservice can be changed or migrated to another software stack without affecting the rest of the Product
- And, of course, it is possible to reuse a part of microservices to integrate it into another software product

## What types of Microservices do we have?
We have the Microservices segmented under the following three families or categories:
1. Composite services
2. Capability services
3. Cross services

**Capatiliby services** execute actions such as control limits and audits. These components are responsible for the interaction between them and the repositories as well as to provide a solution to get the integration with other baking systems and vendors with the most abstraction as possible to avoid an impact over the Composite services business logic.

<img src="/catalog/images/Composite%20services%20interaction.png" alt="Capability Services" width="600" style="margin: auto; display: block;"/>

> **Figure 4:** The interaction between API Gateway, Component and Capability Services.

**Cross services** brings you services ready to be invoked for any component of the platform as well as can be used by third-party components. Any sample available to mention under the hook of Cross services can be (*Discovery Service, Authorization and Configuration*).

<img src="/catalog/images/Cross%20Services%20interacting%20with%20Capabilites%20Services.png" alt="Cross Services" width="600" style="margin: auto; display: block;"/>

> **Figure 5:** A graphic representation of the Interaction between Cross services and Capabilities services.

**Composite services** are components that contain the business logic of every use case. They’re grouped using a business perspective (ex.: Account, Credit Card, Loans, etcetera). Each one of them contains multiple use cases that are translated as different functionalities. You can see in a graphic example below:

<img src="/catalog/images/Account%20MS%20-%20use%20cases.png" alt="Composite Services" width="600" style="margin: auto; display: block;"/>

> **Figure 6:** A high level representation of a Composite services and its use cases.

## Architecture
In the following picture you can see the whole Technisys platform moved to a full microservices-oriented architecture, including Gateway, Backoffice and also the Safeway microservices.

<img src="/catalog/images/Microservices%20Layer.png" alt="Architecture Components" width="800" style="margin: auto; display: block;"/>

> **Figure 7:** A quick look at the Technisys architecture components.

Throughout the solution proposed is to expose a Restful API through a one API Gateway and, behind it, the cross services also discovery, authentication, authorization services and many others. In addition to that there are two different service layers connected through the API Gateway as the unique entry point for exposing services from the first or second layer when applicable.

The first layer is related to the **Composite Services** which contains the responsibility for carrying out the cross-logic of every request plus to be in charge of the orchestration. The second one invokes the **Capability services** which has the services grouped into a business perspective.
The Capability services also provide the integration with other banking systems w/o impacting into the Composite business logic.

## Microservices accessibility
The Gateway behaves as the entry point between the external world (*a.k.a. Channels*) and the Cyberbank Microservices, routing requests, applying security filters as well as authentication via the OAuth2 framework, among other things, to be able to make use of the modules corresponding to each microservice.

<img src="/catalog/images/microservices%20accesibility%20-%20Gateway.png" alt="API Gateways" width="700" style="margin: auto; display: block;"/>

> **Figure 8:** A quick look at the Microservices accessibility through API Gateways.

Some of an API Gateway responsibilities are:

| | |
| ----------- | ----------- |
| Routing | API composition |
| Authentication | Monitoring |
| Rate limiting | Rate limiting  |
| Request logging | Response caching |
