const apis = [
    {
        urls: [
            {
                title: "Traditional API v3",
                url: "/api/docs/reference/docs.yml"
            },
            {
                title: "Accounts",
                url: "/api/accounts/api-docs"
            },
            {
                title: "Channel Support",
                url: "/api/channelsupport/api-docs"
            },
            {
                title: "Transfers",
                url: "/api/transfers/api-docs"
            },
            {
                title: "DAS",
                url: "/api/das/v2/api-doc"
            },
            {
                title: "Payments",
                url: "/api/payments/api-docs"
            }
        ],
        specs: [],
        title: 'Cyberbank Client API'
    },
];
