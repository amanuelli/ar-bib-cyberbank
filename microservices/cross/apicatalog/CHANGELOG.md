# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

### Changed
- Update MS Onboarding doc url. TECDIGSK-546](https://technisys.atlassian.net/browse/TECDIGSK-546)
- Added Payments Doc url [TECHBANK20-748](https://technisys.atlassian.net/browse/TECHBANK20-748)
- Added REE url [TECDIGSK-512](https://technisys.atlassian.net/browse/TECDIGSK-512)
- Update SES url [TECDIGSK-445](https://technisys.atlassian.net/browse/TECDIGSK-445)
- Channel support MS is added in the external Api Docs [TECDIGSK-396](https://technisys.atlassian.net/browse/TECDIGSK-396)
- Update to Profile H v2.0.0 with invoke [TECENG-1021](https://technisys.atlassian.net/browse/TECENG-1021)
- Added Foundation Composite Product documentation [TECDIGSK-497](https://technisys.atlassian.net/browse/TECDIGSK-497)
- Added Staff Management documentation. Backoffice Api documentation url fixed. [TECDIGSK-589](https://technisys.atlassian.net/browse/TECDIGSK-589)

## [1.0.6] - 2021-03-16
- Delete the api file when creating it [TECAPICAT-20](https://technisys.atlassian.net/browse/TECAPICAT-20)
- Update to Profile C v2.0.0 with invoke [TECENG-277](https://technisys.atlassian.net/browse/TECENG-277)
- MS Access Management to internal api catalog [TECDIGSK-297](https://technisys.atlassian.net/browse/TECDIGSK-297)

## [1.0.5] - 2021-03-01
### Added
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)

### Changed
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-718](https://technisys.atlassian.net/browse/TECENG-718).

## [1.0.4] - 2020-12-18
- No issues

## [1.0.3] - 2020-12-02
### Fixed
- Remove unnecessary configMap declarations [https://technisys.atlassian.net/browse/TECDIGSK-193](https://technisys.atlassian.net/browse/TECDIGSK-193)

## [1.0.2] - 2020-11-26
- Fix appversion in Helm. [TECDIGSK-178] (https://technisys.atlassian.net/browse/TECDIGSK-178)
### Fixed
- Helm, update chart and app versions [TECDIGSK-165](https://technisys.atlassian.net/browse/TECDIGSK-165)
### Added
- MS Scheduler [TECENG-514](https://technisys.atlassian.net/browse/TECENG-514)

### Changed
- Apply trivy's security container scanning [TECENG-422](https://technisys.atlassian.net/browse/TECENG-422)
- Add audit microservice to api catalog [TECDIGSK-150](https://technisys.atlassian.net/browse/TECDIGSK-150)

## [1.0.1] - 2020-09-30
### Added
- Variable to fix apicatalog 404 and select if frontend override or not [TECAPICAT-11](https://technisys.atlassian.net/browse/TECAPICAT-11)
- Added Jira project link to Readme file [TECDIGSK-21](https://technisys.atlassian.net/browse/TECDIGSK-21)

### Changed
- Refactor release pipeline to use profile template [TECDIGSK-87](https://technisys.atlassian.net/browse/TECDIGSK-87)
- Refactor CICD pipelines to use profile templates [TECDIGSK-47](https://technisys.atlassian.net/browse/TECDIGSK-47)

### Fixed
