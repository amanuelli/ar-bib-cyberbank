# Content

This service is based on the Apicatalog image and adds new APIs to the catalog. The inner logic remains unchanged so refer to https://bitbucket.org/technisys/tec-cyberbank-apicatalog/src/master/README.md for details on how to use and customize. 

## Code analysis

N/A. This is not a code project.

## Pipeline Status

 CICD     | CDeployment DEV | CDeployment TEST-ORACLE | CDeployment DEMO |
----------|-----------------|-------------------------|------------------|
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20ApiCatalog/Master%20-%20Techbank%20API%20Catalog?branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=677&branchName=master) | [![Deploy to DEV](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/80/221)](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/80/221) | [![Deploy to TET-ORACLE](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/80/223)](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/80/223) | [![Deploy to DEMO](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/80/224)](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/80/224)

## Jira project

TEC - Digital Starterkit: [TECDIGSK](https://technisys.atlassian.net/projects/TECDIGSK/issues?filter=allissues)

## APIs added

### External APIs
- API 3
- Accounts
- Transfers

### Internal APIs
- Cyberbank Core
  - Accounts - Register an account
  - Accounts - Customer accounts list
  - Accounts - Register an account
  - Accounts - Customer accounts list
  - Accounts - Consultation locks an account
  - Accounts - Query the account balance
  - Accounts - Account movements
  - Accounts - Detail of a movement of an account
  - Accounts - Transfer between accounts same bco
  - Accounts - Checkbook order
  - Customers - Customer high
  - Customers - Customer maintenance
  - Customers - Customer Consultation
  - Customers - Consolidated position
  - Customers - Consultation Customer Preferences
  - Customers - High Customer Preference
  - Customers - Modified Preferred Customer
  - Customers - Low Preferred Customer
  - Insert Warranty
  - Insert Loan
- Internal
  - Limits 1.0
  - I18N 1.0
  - BI Client 1.0
  - Customer Insights 1.0
