
# MS Authorization-server 

Cyberbank StarterKit Authorization Server Microservice.

## Code analysis

[![Quality gate](https://sonarcloud.io/api/project_badges/quality_gate?project=technisys_tec-cyberbank-starterkit-authserver&token=14a82b7fb33c6cf3d0ce8e98171c26f8fb1603d3)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-authserver)

[![Bugs](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-authserver&metric=bugs&token=14a82b7fb33c6cf3d0ce8e98171c26f8fb1603d3)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-authserver)
[![Vulnerabilities](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-authserver&metric=vulnerabilities&token=14a82b7fb33c6cf3d0ce8e98171c26f8fb1603d3)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-authserver)
[![Code Smells](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-authserver&metric=code_smells&token=14a82b7fb33c6cf3d0ce8e98171c26f8fb1603d3)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-authserver)
[![Coverage](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-authserver&metric=coverage&token=14a82b7fb33c6cf3d0ce8e98171c26f8fb1603d3)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-authserver)
[![Duplicated Lines (%)](https://sonarcloud.io/api/project_badges/measure?project=technisys_tec-cyberbank-starterkit-authserver&metric=duplicated_lines_density&token=14a82b7fb33c6cf3d0ce8e98171c26f8fb1603d3)](https://sonarcloud.io/dashboard?id=technisys_tec-cyberbank-starterkit-authserver)


## Pipelines Status

 CICD     | DEV         | TEST-ORACLE         | DEMO 
----------|-------------|---------------------|---------------
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20AuthServer/Master%20-%20AuthServer?repoName=technisys%2Ftec-omnichannel&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=119&repoName=technisys%2Ftec-omnichannel&branchName=master) | ![Deploy DEV](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/63/123) | ![Deploy TEST-ORACLE](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/63/180) | ![Deploy DEMO](https://vsrm.dev.azure.com/technisys/_apis/public/Release/badge/1e96a6ee-c8fb-448c-a57b-fac1daaebc90/63/125)


### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"
API_VALIDATION_TIME | The time that the session is valid must be the same that the session.duration in the configuration
## How to run

1- Having Docker downloaded
2- Being logged into a Docker account with read access to the registry
```
docker login tecdigitalacr.azurecr.io -u=6b801393-1f48-4305-90c8-28948df8a713 -p=b1236605-6b95-4772-845f-4bc9ee4195ad
```
3- Build and run the docker image with a name of your choice by running the following script in your terminal (Remove the '\' and line breaks if you're running this in Windows)
```
docker build --build-arg ARTIFACTORY_USER=omni-services-read --build-arg ARTIFACTORY_PASSWORD=OmniServicesRead -t techbank/authserver .
docker run -p 9091:8080 \
-e AUTH_SERVER_OAUTH2_LOGINVALIDATIONSERVICE_URL=http://<Your_API's_IP:PORT>/api/v1/execute/oauth.password.validate \
-e AUTH_SERVER_OAUTH2_JSONINSTEADOFREDIRECTRESPONSE=true \
-e AUTH_SERVER_DECRYPTUSERCREDENTIALS=true \
-e AUTH_SERVER_SECURITY_RSA_TRANSFORM=RSA/NONE/PKCS1Padding \
techbank/authserver
```

