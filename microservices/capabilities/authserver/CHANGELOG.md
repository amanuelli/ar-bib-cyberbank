# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

## 3.5.2 - 2021-08-30
### Added
- Added API_VALIDATION_TIME to set the seconds that the session is valid in the authserver [TECDIGSK-558](https://technisys.atlassian.net/browse/TECDIGSK-558)

## 3.5.1 - 2021-04-07
### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
## 3.5.0 - 2021-04-07
### Changed
- Updates to increment replicaCount (2 instances) and use Redis in DEMO environment [TECCOSERV-3100](https://technisys.atlassian.net/browse/TECCOSERV-3100)
- Update to use the last authserver product version 1.7.0 (jar artifact based)

### Added
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)
- Agnostic pipeline for build [TECCOSERV-3062](https://technisys.atlassian.net/browse/TECCOSERV-3062)

## [3.4.0] - 2020-12-09
### Added
- Updates to use the last authserver product version 1.6.0 (jar artifact based) [TECCOSERV-2976](https://technisys.atlassian.net/browse/TECCOSERV-2976)
- New properties for feature/TECCOSERV-2963 (Lettuce with Redis Cluster support) and feature/TECCOSERV-2993 (User Activity Control)

### Changed
- Apply trivy's security container scanning [TECENG-422](https://technisys.atlassian.net/browse/TECENG-422)
- Refactor CICD pipelines to use profile templates [TECDIGSK-83](https://technisys.atlassian.net/browse/TECDIGSK-83)

## [3.3.0] - 2020-10-09
### Added
- First version of authorization code (new authentication / authorization pages). [AIDA-606](https://technisys.atlassian.net/browse/AIDA-606)

### Changed
- Updates to use the last authserver product version 1.5.0 (jar artifact based) [TECCOSERV-2868](https://technisys.atlassian.net/browse/TECCOSERV-2868)

## [3.2.0] - 2020-09-15
### Added
- Fix AUTHSERVER release pipeline. [TECDIGSK-59](https://technisys.atlassian.net/browse/TECDIGSK-59)
- Redis pool configurable (via env vars AUTH_SERVER_REDIS_POOL_MAXIDLE / AUTH_SERVER_REDIS_POOL_MAXTOTAL / AUTH_SERVER_REDIS_POOL_MAXWAIT)

## [3.1.0] - 2020-09-03
### Added
- Add default RSA public/private keys in starterkit repository, update login page to use the public key with a GET request, and define new properties to still using current RSA padding / transform (retrocompatibility). [TECCOSERV-2752](https://technisys.atlassian.net/browse/TECCOSERV-2752)

### Changed
- Define a new path for default login page ("/oauth/authorize" endpoint). [TECCOSERV-2768](https://technisys.atlassian.net/browse/TECCOSERV-2768)
- Update Dockerfile to use "technisys" user and build the customization using the last product jar artifact. [TECCOSERV-2770](https://technisys.atlassian.net/browse/TECCOSERV-2770)
- Updates to use the last authserver product version 1.3.0 (jar artifact based) [TECCOSERV-2778](https://technisys.atlassian.net/browse/TECCOSERV-2778)  

## [3.0.0] - 2020-07-20
### Changed
- Updates to define a new major version in starterkit 3.0.0 and use the last authserver product version 1.2.0 (docker image based).
