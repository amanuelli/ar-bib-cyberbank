//-------------------
//Utils

let publicRSAKey;

function loadPublicKey (){
	$.ajax({ url: "/oauth/key",
		async: false,
		type: "GET",
		error: function(jqXHR, textStatus, errorThrown) {
			console.log('Error. Status: ' + jqXHR.status + '. Detail: ' + jqXHR.responseText);
		},
		success: function(data, textStatus, jqXHR) {
			publicRSAKey = data;
		}
	});
}

function encryptText(text){
	loadPublicKey();
	const encrypt = new JSEncrypt();
	encrypt.setPublicKey(publicRSAKey);
	return encrypt.encrypt(text);
}

function validateEmail(email) {
	const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	return re.test(String(email).toLowerCase());
}

function getURLParam(name,step1) {
	//In the step 1 the params are in the URL in the next are in the session storage
	const params = step1? location.search : sessionStorage.getItem('authParams');
	const value = (new RegExp('[?&]' + encodeURIComponent(name) + '=([^&]*)')).exec(params);
	if (value) {
		return decodeURIComponent(value[1]);
	} else {
		return null;
	}
}

//-------------------
//The next action function by Step

function nextStep1(){
	sessionStorage.clear();
	sessionStorage.setItem('authParams', window.location.search);
	location.assign('authentication.html');
}

function nextStep2 () {
	const user = loginForm.elements["login-user"].value;
	const pass = loginForm.elements["login-pass"].value;
	if (user.length === 0 || pass.length === 0) {
		showError ("Please enter username and password");
	}else if(!validateEmail(user)){
		showError ("Please enter a valid username");
	} else{
		sessionStorage.setItem('credentials',btoa(encryptText(user) + ":" + encryptText(pass)));
		location.assign('authorization.html');
	}
}

function nextStep3 (){
	const credentials = sessionStorage.getItem('credentials');
	const params =sessionStorage.getItem('authParams');
	const url = "/oauth/authorize" + params;
	$.ajax({ url: url,
		beforeSend: function (xhr) {
			xhr.setRequestHeader ("Authorization", "Basic " + credentials);
			xhr.setRequestHeader ("channel", "assistant");
		},
		async: false,
		type: "GET",
		error: function(jqXHR, textStatus, errorThrown) {
			console.log('Error. Status: ' + jqXHR.status + '. Detail: ' + jqXHR.responseText);
			const response = $.parseJSON(jqXHR.responseText);
			if (jqXHR.status === 401){
				sessionStorage.setItem('loginError',response.error_description);
				location.assign('authentication.html');
			} else{
				deny();
			}
		},
		success: function(data) {
			sessionStorage.setItem('redirectUrl',data.redirectUrl);
			location.assign('result.html');
		}
	});
}

function nextStep4() {
	const client_number = getURLParam('client_number');
	const account_linking_token= getURLParam('account_linking_token');
	const custom_token = getURLParam('custom_token');
	let redirectUrl;
	if(client_number){
		//This case is for twilio
		redirectUrl=getURLParam('redirect_uri')+'?client_number='+client_number;
	}else if(account_linking_token && custom_token) {
		//This case is for facebook messenger
		redirectUrl = sessionStorage.getItem('redirectUrl')
			.replace('#access_token','&access_token')
			+ "&authorization_code=" + encodeURIComponent(custom_token);
	}else{
		redirectUrl=sessionStorage.getItem('redirectUrl');
	}

	sessionStorage.clear();
	location.assign(redirectUrl); // Redirect OK
}

//------------------
//The render content for the pages by the assistant

function renderStep1(){
	const text= "In order to offer this service, we need your approval for <em>"+
				getAssistant(true)+"</em> to view the information shared in this channel.";
	document.getElementById("title").innerHTML = "Chat with us via "+getAssistant(true);
	document.getElementById("headText").innerHTML = text;
}

function renderStep2(){
	document.getElementById("leadText").innerHTML =
		"Your login information is not visible to <em>"+getAssistant()+"</em>";
}

function renderStep3(){
	const text ="You can stop this service at any time, just typing “log out” on the\n" +
				"conversation with <em>Techbank</em> on <em>"+getAssistant()+"</em>.";
	document.getElementById("assistant").innerHTML = getAssistant();
	document.getElementById("footer").innerHTML = text;
}

function renderStep4(){
	document.getElementById("assistant-logo").src=getAssistant().toLowerCase()
		.replace(' ','-')+"-techbank.svg";
	document.getElementById("leadText").innerHTML =
		"You have successfully authorized Techbank to interact with you through "+getAssistant();
}

function getAssistant(step1){
	const redirect_uri = getURLParam('redirect_uri',step1);
	const client_number = getURLParam('client_number',step1);
	if(client_number)
		return "WhatsApp";
	if(redirect_uri.indexOf("google") >= 0)
		return "Google Assistant";
	if(redirect_uri.indexOf("messenger") >= 0)
		return "Facebook Messenger";
	if(redirect_uri.indexOf("amazon") >= 0)
		return "Alexa";
	return "the Assistant"
}

//-------------------
//handler errors functions
function deny(step1){
	const url = getURLParam("redirect_uri",step1) + "?error=" + "authorization_denied" + "&error_description="
		+ "user_denied_the_access" + ((getURLParam("state",step1)!==undefined)?"&state="
			+ getURLParam("state",step1):"");
	sessionStorage.clear();
	location.assign(url);
}

function showError(msg){
	document.getElementById("errorBox").innerHTML=msg;
}
function watchErrors(){
	const error = sessionStorage.getItem('loginError')
	if(error) {
		if (error === "API019W")
			showError("Invalid user and/or password");
		else {
			showError("The user is either blocked, or missing Digital Assistant permissions. Please contact your bank.");
		}
	}
}


