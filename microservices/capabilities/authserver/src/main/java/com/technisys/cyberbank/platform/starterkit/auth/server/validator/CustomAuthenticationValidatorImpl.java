/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.cyberbank.platform.starterkit.auth.server.validator;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.technisys.digital.auth.server.bean.AuthServerErrorBean;
import com.technisys.digital.auth.server.bean.AuthenticationResponse;
import com.technisys.digital.auth.server.util.*;
import com.technisys.digital.auth.server.validator.IAuthenticationValidator;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Custom implementation to validate username and password.
 *
 * @author lpicareli
 */
@Component(value = "customAuthenticationValidator")
public class CustomAuthenticationValidatorImpl implements IAuthenticationValidator {
	@Autowired
	HttpClientComponent httpClient;
	@Autowired
	PrivateUserContextUtil privateUserContextUtil;
	@Autowired
	PublicUserContextUtil publicUserContextUtil;
	@Autowired
	AuthUtil authUtil;
	@Autowired
	JsonUtil jsonUtil;

	@Value("${auth.server.oauth2.loginStep1Service.url:}")
	private String loginStep1ServiceUrl;

	@Value("${auth.server.oauth2.loginValidationService.url:}")
	private String loginValidationServiceUrl;

	private final String RESPONSE_CODE_OK = "COR000I";

	private static final Logger logger = LoggerFactory.getLogger(CustomAuthenticationValidatorImpl.class);

	@Override
	public AuthenticationResponse validate(String username, String password, HttpServletRequest request) {
		try {
			return getDefaultResponse(username, password, request);
		} catch (Exception e) {
			logger.error("Error calling login validation service.", e);
		}
		return new AuthenticationResponse(false);
	}

	private AuthenticationResponse getDefaultResponse(String username, String password, HttpServletRequest request) throws Exception {
		String pass = password;
		if(authUtil.getChannelId(request).equals("assistant")) {
			String exchangeToken = getExchangeToken(username, authUtil.getChannelId(request));
			pass = password +'#'+ exchangeToken;
		}
		String jsonResponse = callService(username, pass, authUtil.getChannelId(request));
		if (RESPONSE_CODE_OK.equals(jsonUtil.getValueFromJSON(jsonResponse, "code"))) {
			publicUserContextUtil.setOAuth2UserContextValue("userId",
					jsonUtil.getValueFromJSON(jsonResponse, "data.userId"));
			privateUserContextUtil.setOAuth2UserContextValue("sessionKind",
					jsonUtil.getValueFromJSON(jsonResponse, "data.sessionKind"));
			privateUserContextUtil.setOAuth2UserContextValue("userId",
					jsonUtil.getValueFromJSON(jsonResponse, "data.userId"));
			return new AuthenticationResponse(true);
		} else {
			logger.info("Login validation failed. Username: {}  .Code: {} .Message: {}",
					username,
					jsonUtil.getValueFromJSON(jsonResponse, "code"),
					jsonUtil.getValueFromJSON(jsonResponse, "message"));
			String errorDescription = jsonUtil.getValueFromJSON(jsonResponse, "code");
			return new AuthenticationResponse(new AuthServerErrorBean(errorDescription));
		}
	}

	private String getExchangeToken(String username, String channel)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode body = mapper.createObjectNode();
		body.put("channel", channel);
		body.put("userEmail", username);
		HttpPost method = new HttpPost(loginStep1ServiceUrl);
		method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		method.setHeader("Authorization", null);
		method.setEntity(new StringEntity(body.toString()));
		HttpResponse httpResponse = httpClient.getHttpClient().execute(method);
		return jsonUtil.getValueFromJSON(EntityUtils.toString(httpResponse.getEntity()),"data._exchangeToken");
	}


	private String callService(String username, String password, String channel)
			throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		ObjectNode body = mapper.createObjectNode();
		body.put("channel", channel);
		body.put("_usernameToValidate", username);
		body.put("_passwordToValidate", password);
		HttpPost method = new HttpPost(loginValidationServiceUrl);
		method.setHeader(HttpHeaders.CONTENT_TYPE, "application/json");
		method.setHeader("Authorization", null);
		method.setEntity(new StringEntity(body.toString()));
		HttpResponse httpResponse = httpClient.getHttpClient().execute(method);
		return EntityUtils.toString(httpResponse.getEntity());
	}
}
