package com.technisys.cyberbank.platform;

import static java.util.Objects.requireNonNull;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.util.ReflectionTestUtils;

import com.technisys.cyberbank.platform.starterkit.auth.server.validator.CustomAuthenticationValidatorImpl;
import com.technisys.digital.auth.server.bean.AuthenticationResponse;
import com.technisys.digital.auth.server.util.AuthUtil;
import com.technisys.digital.auth.server.util.HttpClientComponent;
import com.technisys.digital.auth.server.util.JsonUtil;
import com.technisys.digital.auth.server.util.PrivateUserContextUtil;
import com.technisys.digital.auth.server.util.PublicUserContextUtil;

import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;

@RunWith(SpringJUnit4ClassRunner.class) 
public class CustomAuthenticationValidatorTest {
	
	private MockWebServer mockWebServer = new MockWebServer();
	
	@Spy
	@InjectMocks
	CustomAuthenticationValidatorImpl customAuthenticationValidatorImpl;
	
	HttpClientComponent httpClient = new HttpClientComponent();
	JsonUtil jsonUtil = new JsonUtil();
	
	@Mock
	AuthUtil authUtil = Mockito.mock(AuthUtil.class);
	
	@Mock
	PrivateUserContextUtil privateUserContextUtil = Mockito.mock(PrivateUserContextUtil.class);
	
	@Mock
	PublicUserContextUtil publicUserContextUtil = Mockito.mock(PublicUserContextUtil.class);
	
	HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
	
	@Before
	public void setUp() {
		String baseUrl=mockWebServer.url("/").toString();
		ReflectionTestUtils.setField(customAuthenticationValidatorImpl, "loginValidationServiceUrl", baseUrl);
		ReflectionTestUtils.setField(customAuthenticationValidatorImpl, "loginStep1ServiceUrl", baseUrl);
		ReflectionTestUtils.setField(customAuthenticationValidatorImpl, "httpClient", httpClient);
		ReflectionTestUtils.setField(customAuthenticationValidatorImpl, "jsonUtil", jsonUtil);
	}
	
	
	@Test
	public void testValidator() throws IOException {
		String VALID_TOKEN_RESPONSE = new String(requireNonNull(CustomAuthenticationValidatorTest.class
		        .getClassLoader()
		        .getResourceAsStream("responses/token_success.json"))
		        .readAllBytes());
		mockWebServer.enqueue(new MockResponse()
			      .addHeader("Content-Type", "application/json; charset=utf-8")
			      .setBody(VALID_TOKEN_RESPONSE)
			      .setResponseCode(200));
		String VALID_RESPONSE = new String(requireNonNull(CustomAuthenticationValidatorTest.class
		        .getClassLoader()
		        .getResourceAsStream("responses/success.json"))
		        .readAllBytes());
		mockWebServer.enqueue(new MockResponse()
			      .addHeader("Content-Type", "application/json; charset=utf-8")
			      .setBody(VALID_RESPONSE)
			      .setResponseCode(200));		
		Mockito.when(authUtil.getChannelId(Mockito.any(HttpServletRequest.class))).thenReturn("assistant");		
		AuthenticationResponse response = customAuthenticationValidatorImpl.validate("demo", "demo", request);
		Assert.assertNotNull(response);
		Assert.assertTrue(response.isValidationOk());
	}
	
	@Test
	public void testValidatorError() throws IOException {
		String VALID_RESPONSE = new String(requireNonNull(CustomAuthenticationValidatorTest.class
		        .getClassLoader()
		        .getResourceAsStream("responses/error.json"))
		        .readAllBytes());
		mockWebServer.enqueue(new MockResponse()
			      .addHeader("Content-Type", "application/json; charset=utf-8")
			      .setBody(VALID_RESPONSE)
			      .setResponseCode(200));
		Mockito.when(authUtil.getChannelId(Mockito.any(HttpServletRequest.class))).thenReturn("IN");		
		AuthenticationResponse response = customAuthenticationValidatorImpl.validate("demo", "demo", request);
		Assert.assertNotNull(response);
		Assert.assertFalse(response.isValidationOk());
	}
	
	@Test
	public void testValidatorException() throws IOException {
		ReflectionTestUtils.setField(customAuthenticationValidatorImpl, "httpClient", null);
		String VALID_RESPONSE = new String(requireNonNull(CustomAuthenticationValidatorTest.class
		        .getClassLoader()
		        .getResourceAsStream("responses/error.json"))
		        .readAllBytes());
		mockWebServer.enqueue(new MockResponse()
			      .addHeader("Content-Type", "application/json; charset=utf-8")
			      .setBody(VALID_RESPONSE)
			      .setResponseCode(200));
		Mockito.when(authUtil.getChannelId(Mockito.any(HttpServletRequest.class))).thenReturn("IN");		
		AuthenticationResponse response = customAuthenticationValidatorImpl.validate("demo", "demo", request);
		Assert.assertNotNull(response);
		Assert.assertFalse(response.isValidationOk());
	}
	
	
}
