# Limits Microservice 

[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Platform/MS%20-%20Notifications/Master%20-%20Notifications?repoName=technisys%2Ftec-digital-notifications&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=371&repoName=technisys%2Ftec-digital-limits&branchName=master)

# Table of Contents

- [Project Overview](#markdown-header-project-overview)
  - [Microservice](#markdown-header-microservice)
- [Getting Started](#markdown-header-getting-started)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Installation](#markdown-header-installation)
  - [Configuration](#markdown-header-configuration)
    - [DB Configuration](#markdown-header-db-configuration)
    - [Cache Configuration](#markdown-header-cache-configuration)
    - [Notifications Configuration](#markdown-header-limits-configuration)
- [Documentation](#markdown-header-documentation)
    

# Project Overview

This Microservice manages the notifications in the platform using the asyncronus services called kafka.

## Microservice 

### Technology stack

* Maven
* Java 11
* Spring boot 2
* Kafka


# Getting Started


## Prerequisites

- Java 11
- Maven 3.x
- Docker (Optional)
- Kafka
- A database on one of the supported DB engines.

## Installation

Let's boot up a local Kafka cluster with the Schema Registry. Run the following command 
```
docker-compose up -d
```

Build the image of the microservice
```
docker build -t my-notifications-image:latest .
```

Then you can run it (for example on port 9000) using the following command:
```
docker run -d --name notifications -p 9000:8080 -e 'spring.datasource.password=yourpassword' -t my-notifications-image:latest 
```
By naming the container, we can execute actions using it, for example see logs:
```
docker logs -f notifications
```

## New providers
The notification-delivery-provider.jar library contains the channel's providers/dispatchers that are compatibles with notification delivery ms. The idea is to reuse these providers, saving work on new projects. Let them choose which provider full-field the project requirements.
The providers for each channel are defined through the following env variables in the notification delivery in the Starterkit's Configmap.

SMS_DISPATCHER_FQN  
EMAIL_DISPATCHER_FQN  
PUSH_DISPATCHER_FQN  
TWITTER_DISPATCHER_FQN

The value assigned to each variable **MUST BE** the fully qualified name (**FQN**) of the class to be used, for example:

**SMS_DISPATCHER_FQN**=com.technisys.cyberbank.platform.notification.delivery.providers.BulkSmsDispatcher

## Configuration
Default configuration values are defined in application.yml.

### Notifications Configuration

Default caps values can be overwritten acording to the environment variables in the following table:

Environment variable | Default value
--- | ---
KAFKA_BROKER_URL                      | http://kafka.digital-local
SCHEMA_REGISTRY_URL_URL               | http://schema.digital-local
CONFIGURATION_URL                     | http://configuration.digital-local
SES_CLIENT_NAME                       | digitalapi
SES_USAGE_KEY                         | notnil
SES_URL                               | http://ses.digital-local
AUDIT_BROKER                          | RabbitMQ
EMAIL_DISPATCHER_FQN                  | com.technisys.digital.notifications.dispatcher.DefaultEmailDispatcher
PUSH_DISPATCHER_FQN                   | com.technisys.digital.notifications.dispatcher.DefaultPushDispatcher
SMS_DISPATCHER_FQN                    | com.technisys.digital.notifications.dispatcher.DefaultSMSDispatcher
TWITTER_DISPATCHER_FQN                | com.technisys.digital.notifications.dispatcher.DefaultTwitterDispatcher
SES_CLIENT_NAME                       | digitalapi
PUSH_FCM_URL                          | https://fcm.googleapis.com/fcm/send
PUSH_FCM_AUTHKEY                      | ""
TWITTER_SEND_URL                      | https://api.twitter.com/1.1/direct_messages/events/new.json
TWITTER_CONSUMER_KEY                  | ""
TWITTER_CONSUMER_SECRET               | ""
TWITTER_TOKEN                         | ""
TWITTER_TOKEN_SECRET                  | ""
EMAIL_HOST                            | smtp.gmail.com
EMAIL_PORT                            | 587
EMAIL_CLASS                           | com.sun.mail.smtp.SMTPTransport
EMAIL_STARTTLS_ENABLE                 | false
EMAIL_CHARSET                         | UTF8
EMAIL_DEBUG                           | true
EMAIL_EHLO                            | ""
EMAIL_USER                            | omnichannel.fastrack@gmail.com
EMAIL_FROM                            | omnichannel.fastrack@gmail.com
EMAIL_PASSWORD                        | ""
KAFKA_BROKER_URL                      | tec-digital-kafka-cp-kafka-headless:9092
SCHEMA_REGISTRY_URL                   | http://tec-digital-kafka-cp-schema-registry:8081
EMAIL_THREADS_HIGH_PRIORITY           | 2
EMAIL_THREADS_NORMAL_PRIORITY         | 1
NOTIFICATION_THREADS_HIGH_PRIORITY    | 2
NOTIFICATION_THREADS_NORMAL_PRIORITY  | 1
PUSH_THREADS_HIGH_PRIORITY            | 2
PUSH_THREADS_NORMAL_PRIORITY          | 1
SMS_THREADS_HIGH_PRIORITY             | 2
SMS_THREADS_NORMAL_PRIORITY           | 1
TWITTER_THREADS_HIGH_PRIORITY         | 1
TWITTER_THREADS_NORMAL_PRIORITY       | 1

# Documentation

All the documentation for the microservice can be found in this [Confluence.](https://technisys.atlassian.net/wiki/spaces/TECMSNOTIF/overview)
