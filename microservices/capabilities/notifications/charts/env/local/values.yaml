# Default values for notifications.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

name: notification-delivery

labels:
  app: notification-delivery
  lang: java
  platform: digital

selectorLabels:
  app: notification-delivery

deployment:

  image:
    repository: tecdigitalacr.azurecr.io/techbank/notifications
    version: #{MS_VERSION}#
    pullPolicy: IfNotPresent

  envFrom:
    - configMapRef:
        name: api-config
    - secretRef:
        name: api-secrets
    - configMapRef:
        name: notification-delivery-config
    - configMapRef:
        name: notification-delivery-provider-config
    - secretRef:
        name: notification-delivery-secrets

  livenessProbe:
    httpGet:
      path: /actuator/health
      port: 8080
    initialDelaySeconds: 60
    periodSeconds: 10

  readinessProbe:
    httpGet:
      path: /actuator/health
      port: 8080
    initialDelaySeconds: 30
    periodSeconds: 15


  replicaCount: 1

  security:
    uid:

  port: 8080

  imagePullSecrets:
    - name: regcred

  serviceAccountName: tecdigital-sa

  annotations:

  resources:
    limits:
      cpu: 1000m
      memory: 1000Mi
    requests:
      cpu: 100m
      memory: 256Mi

  volumeMounts:
    - mountPath: /var/notifications
      name: volume

  volumes:
    - name: volume
      persistentVolumeClaim:
        claimName: volume-notifications

service:
  type: ClusterIP
  port: 80
  targetPort: 8080

configPodReset:
  - configmap.yaml
  - providersConfigMap.yaml
  - secrets.yaml

providersConfigMap:
  name: notification-delivery-provider-config
  EMAIL_DISPATCHER_FQN: com.technisys.digital.notifications.dispatcher.DefaultEmailDispatcher
  PUSH_DISPATCHER_FQN: com.technisys.digital.notifications.dispatcher.DefaultPushDispatcher
  SMS_DISPATCHER_FQN: com.technisys.digital.notifications.dispatcher.DefaultSMSDispatcher
  TWITTER_DISPATCHER_FQN: com.technisys.digital.notifications.dispatcher.DefaultTwitterDispatcher
  PUSH_FCM_URL: "https://fcm.googleapis.com/fcm/send"
  TWITTER_SEND_URL: "https://api.twitter.com/1.1/direct_messages/events/new.json"
  EMAIL_HOST: "smtp.gmail.com"
  EMAIL_PORT: 587
  EMAIL_CLASS: "com.sun.mail.smtp.SMTPTransport"
  EMAIL_STARTTLS_ENABLE: "true"
  EMAIL_CHARSET: "UTF8"
  EMAIL_DEBUG: "false"
  EMAIL_EHLO: ""
  EMAIL_USER: "omnichannel.fastrack@gmail.com"
  EMAIL_FROM: "omnichannel.fastrack@gmail.com"

configMap:
  name:  notification-delivery-config
  BRIDGE_TRACING_ENABLED: "false"
  ELASTIC_APM_SERVER_URLS: "#{apmserver.url}#"
  ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
  ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
  ELASTIC_APM_APPLICATION_PACKAGES: "com.technisys"
  ELASTIC_APM_CENTRAL_CONFIG: "true"
  ELASTIC_APM_SERVICE_NAME: "limits"
  ELASTIC_APM_LOG_LEVEL: "INFO"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "true"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: "com.technisys.digital.limits.controller.*"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: "(?-i)java.*, (?-i)javax.*, (?-i)sun.*, (?-i)com.sun.*,
                                                          (?-i)jdk.*, (?-i)org.apache.tomcat.*,
                                                          (?-i)org.apache.catalina.*, (?-i)org.apache.coyote.*,
                                                          (?-i)org.eclipse.jetty.*, (?-i)org.springframework.*"
  ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc, redis"
  JAVA_TOOL_OPTIONS: ""
  SES_CLIENT_NAME: "digitalapi"
  KAFKA_BROKER_URL: "tec-confluent-oss-cp-kafka-headless:9092"
  SCHEMA_REGISTRY_URL: "http://tec-confluent-oss-cp-schema-registry:8081"
  EMAIL_THREADS_HIGH_PRIORITY: 2
  EMAIL_THREADS_NORMAL_PRIORITY: 1
  NOTIFICATION_THREADS_HIGH_PRIORITY: 2
  NOTIFICATION_THREADS_NORMAL_PRIORITY: 1
  PUSH_THREADS_HIGH_PRIORITY: 2
  PUSH_THREADS_NORMAL_PRIORITY: 1
  SMS_THREADS_HIGH_PRIORITY: 2
  SMS_THREADS_NORMAL_PRIORITY: 1
  TWITTER_THREADS_HIGH_PRIORITY: 2
  TWITTER_THREADS_NORMAL_PRIORITY: 1
  dbserver:
    url: jdbc:sqlserver://host.docker.internal:1433;databaseName=tec-digital-notification-delivery
  SPRING_FLYWAY_LOCATIONS: "classpath:db/migration/mssql"
  STORAGE_DEFAULT: local
  STORAGE_LOCAL_PATH: /var/notifications

secrets:
  name: notification-delivery-secrets
  sesUsageKey: "#{ses.usageKey}#"
  push:
    fcm:
      authkey: "#{push.fcm.authkey}#"
  twitter:
    consumer:
      key: "#{twitter.consumer.key}#"
      secret: "#{twitter.consumer.secret}#"
    token:
      name: "#{twitter.token}#"
      secret: "#{twitter.token.secret}#"
  email:
    password: "#{email.password}#"
  elastic:
    apmtoken: "#{elastic.apmtoken}#"
  dbserver:
    password: 'password'
    username: 'sa'

volume:
  name: volume-notifications
  accessModes:
    - ReadWriteMany
  persistentvolumes:
    local: "true"
    storageClassName: "standard"