if $DEBUG_MODE; then
  java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -cp $TECHNISYS_HOME/notification-delivery.jar -Dloader.path=$TECHNISYS_HOME/notification-delivery-providers.jar -Djava.security.egd=file:/dev/./urandom org.springframework.boot.loader.PropertiesLauncher
else
  java -cp $TECHNISYS_HOME/notification-delivery.jar -Dloader.path=$TECHNISYS_HOME/notification-delivery-providers.jar -Djava.security.egd=file:/dev/./urandom org.springframework.boot.loader.PropertiesLauncher
fi