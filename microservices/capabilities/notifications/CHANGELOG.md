# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]
### Changed
- Update platform notification-delivery component to 1.0.2. [TECDIGSK-614](https://technisys.atlassian.net/browse/TECDIGSK-614)
- Added file sending and database configurations. [TECDIGSK-588](https://technisys.atlassian.net/browse/TECDIGSK-588)

## [1.0.0](https://technisys.atlassian.net/projects/TECMSNOTIF/versions/54500/tab/release-report-in-progress) - 2020-10-07
### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
  
- Added 4 types of notifications using kafka [TECMSNOTIF-15](https://technisys.atlassian.net/browse/TECDIGSK-116)
- The microservice was connected with kafka

