FROM acrbibank.azurecr.io/digital/openjdk-jdk:11

ENV TECHNISYS_HOME /home/technisys

RUN groupadd -r technisys && \
    useradd -g technisys -d $TECHNISYS_HOME -s /sbin/nologin -c "Technisys user" technisys && \
    chown -R technisys:technisys $TECHNISYS_HOME

USER technisys

WORKDIR $TECHNISYS_HOME

COPY --chown=technisys start.sh .

ENTRYPOINT sh start.sh

ARG curl_opts
ARG artifact='transactions'
ARG base_version='1.0.2'
ARG version=''
ARG repository='https://artifactory.technisys.com/artifactory/list/libs-release/com/technisys/cyberbank/platform'
ARG sha1_version='dd0514b4f90120330f892d8c31bb90cd7cf34fb4'
ARG artifact_version=''
ARG file_name=$artifact-$base_version$artifact_version.jar

ARG apm_agent_jar_name='elastic-apm-agent'
ARG apm_agent_repository='https://repo1.maven.org/maven2/co/elastic/apm/elastic-apm-agent/1.18.1/elastic-apm-agent-1.18.1.jar'
ARG apm_agent_sha1_version='b779afe69763958ac7de7d0c7cd37657da068513'

RUN curl $curl_opts -o $file_name $repository/$artifact/$base_version$version/$file_name && \
    sha1sum $file_name | grep $sha1_version && \
    mv $file_name $artifact.jar && \
    curl -o $apm_agent_jar_name.jar $apm_agent_repository && \
    sha1sum $apm_agent_jar_name.jar | grep $apm_agent_sha1_version