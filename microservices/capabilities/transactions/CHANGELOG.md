# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

## [[1.0.1]] - 2021-10-15
- Use transactions-1.0.2.jar

## [[1.0.0]] - 2021-09-24
### Changed
- Use transactions-1.0.1.jar
