# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).


## [1.0.9] - 2021-11-18

### Fixed
- Initialize default values for Boolean classes [TECDIGSK-707](https://technisys.atlassian.net/browse/TECDIGSK-707)
- Update spring boot version and io.netty for dependency check [TECDIGSK-707](https://technisys.atlassian.net/browse/TECDIGSK-707)
- Add missing prop core.session.validateIPAddress.uncheckedUsers. Change prop core.communications.communicationPriorities, communications.sendToAllUsers.enabled, core.communications.communicationTypes, backoffice.campaigns.indicators.rfm.enabledOptions, notifications.channels, communications.bodySocialNetworks.maxLength and communications.bodySMS.maxLength to access it from CyBO [TECDIGSK-695](https://technisys.atlassian.net/browse/TECDIGSK-695)
- Fixed the cache type and javadoc in the microservice and fix a property [TECDIGSK-724](https://technisys.atlassian.net/browse/TECDIGSK-724)

### Changed
- New Google Maps APIKEY value [TECDIGSK-661](https://technisys.atlassian.net/browse/TECDIGSK-661)
- Update platform configuration component to 2.0.1  [TECDIGSK-670](https://technisys.atlassian.net/browse/TECDIGSK-670)
- Change props backoffice.profile.password.minLength, core.password.maxLength and backoffice.backofficeusers.sendPasswordEmail to access them from CyBO [TECDIGSK-691](https://technisys.atlassian.net/browse/TECDIGSK-691)

### Added 
- Add FQN for Factory in the monolith [TECDIGSK-579](https://technisys.atlassian.net/browse/TECDIGSK-579)
- Add invitation codes feature flag variable [TECDIGSK-452](https://technisys.atlassian.net/browse/TECDIGSK-452)
- Add Communications Trays feature flag variable [TECDIGSK-486](https://technisys.atlassian.net/browse/TECDIGSK-486)
- Change prop client.enrollment.invitationCode.validity to access it from CyBO [TECDIGSK-485](https://technisys.atlassian.net/browse/TECDIGSK-485)
- Change prop core.default.lang to access it from CyBO [TECDIGSK-609](https://technisys.atlassian.net/browse/TECDIGSK-609)
- Remove Backoffice Legacy environment feature flag [TECCDPBO-5146](https://technisys.atlassian.net/browse/TECDIGSK-5146)

## [1.0.8]- 2021-08-30
### Changed
- Update platform configuration component to 1.1.3 [TECDIGSK-568](https://technisys.atlassian.net/browse/TECDIGSK-568)

## [[1.0.7]](https://technisys.atlassian.net/projects/TECDIGSK/versions/55584/tab/release-report-all-issues) - 2021-08-02
### Changed
- Update platform configuration component to 1.1.2 [TECDIGSK-478](https://technisys.atlassian.net/browse/TECDIGSK-478)
### Added
- Added time to live and prefix as Redis properties to modify default values [TECDIGSK-478](https://technisys.atlassian.net/browse/TECDIGSK-478)

## [1.0.6] - 2021-07-22
### Changed
- Update platform configuration component to 1.1.1 [TECDIGSK-471](https://technisys.atlassian.net/browse/TECDIGSK-471)
### Added
- Add operation-safeway configuration Variables [MANSAFEWAY-6068](https://technisys.atlassian.net/browse/MANSAFEWAY-6068)
- Add general condition file validation variables [TECDIGSK-472](https://technisys.atlassian.net/browse/TECDIGSK-472)

## [[1.0.5]](https://technisys.atlassian.net/projects/TECDIGSK/versions/55250/tab/release-report-all-issues) - 2021-06-10
### Changed
- Update platform configuration component to 1.0.6 [TECDIGSK-428](https://technisys.atlassian.net/browse/TECDIGSK-428)

## [[1.0.4]](https://technisys.atlassian.net/projects/TECDIGSK/versions/54976/tab/release-report-all-issues) - 2021-06-02
### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)

### Changed
- Update platform configuration component to 1.0.5 [TECDIGSK-407](https://technisys.atlassian.net/browse/TECDIGSK-407)

## [[1.0.3]](https://technisys.atlassian.net/projects/TECDIGSK/versions/54885/tab/release-report-all-issues) - 2021-04-28
### Changed
- Update platform configuration component to 1.0.2 final version [TECDIGSK-379](https://technisys.atlassian.net/browse/TECDIGSK-379)
- Use configuration 1.0.4 

## [[1.0.2]](https://technisys.atlassian.net/projects/TECDIGSK/versions/54276/tab/release-report-all-issues) - 2021-04-16
### Changed
- Update to profile C v2.0.0 and profile H v2.0.0 with invoke [TECMSCONFIG-103](https://technisys.atlassian.net/browse/TECMSCONFIG-103)
- Fix default widget configuration [TECCDPBO-3813](https://technisys.atlassian.net/browse/TECCDPBO-3813)

### Added 
- Properties not present in demo environment added [TECDIGSK-303](https://technisys.atlassian.net/browse/TECDIGSK-303).
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-842](https://technisys.atlassian.net/browse/TECENG-842).
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)
- Remove duplicate safeway variables [TECDIGSK-329](https://technisys.atlassian.net/browse/TECDIGSK-329)

### Removed
- Safeway variables [TECDIGSK-329](https://technisys.atlassian.net/browse/TECDIGSK-329)

## [[1.0.1]](https://technisys.atlassian.net/projects/TECDIGSK/versions/53942/tab/release-report-all-issues) - 2021-01-18
### Changed
- Use configuration-1.0.1.jar

## [[1.0.0]](https://technisys.atlassian.net/projects/TECDIGSK/versions/53788/tab/release-report-all-issues) - 2020-11-12
### Changed
- Apply trivy's security container scanning [TECENG-422](https://technisys.atlassian.net/browse/TECENG-422)
- Remove JAEGER variables and set new variable BRIDGE_TRACING_ENABLED [TECDIGSK-249](https://technisys.atlassian.net/browse/TECDIGSK-249)
- Add APM configuration Variables [TECDIGSK-233](https://technisys.atlassian.net/browse/TECDIGSK-233)

### Added
- First commit, added (changelog, readme, etc) [TECMSCONFIG-20](https://technisys.atlassian.net/browse/TECMSCONFIG-20)
