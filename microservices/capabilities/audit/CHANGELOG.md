# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).


## [1.0.10]
### Fixed
- Fix broken Chart that was overrides by previous versions 1.04 [TECDIGSK-552](https://technisys.atlassian.net/browse/TECDIGSK-552)
- Fix Audit limits resources to improve the performance in dev [TECDIGSK-516](https://technisys.atlassian.net/browse/TECDIGSK-516)
- Fix Kafka to evict process messages without protobuf [TECDIGSK-561](https://technisys.atlassian.net/browse/TECDIGSK-561)

### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
- Added Kafka with protobuf and supporting Rabbit with Json [TECDIGSK-466](https://technisys.atlassian.net/browse/TECDIGSK-466)
- Adding layered spring boot config to Docker file. [TECDIGSK-479](https://technisys.atlassian.net/browse/TECDIGSK-479)
- Chart configuration [TECDIGSK-524](https://technisys.atlassian.net/browse/TECDIGSK-524)

### Changed
- Change version MS-Audit in the dockerfile to 1.0.15 [TECDIGSK-479](https://technisys.atlassian.net/browse/TECDIGSK-479)
- Changed values.yaml and upgraded audit platform version. [TECDIGSK-479](https://technisys.atlassian.net/browse/TECDIGSK-479)
- Turn off Syslog in the environments [TECDIGSK-506](https://technisys.atlassian.net/browse/TECDIGSK-506)

## [1.0.9] - 2021-05-25
### Changed
- Change the value of the Techlog migration location (flyway) [TECDIGSK-411](https://technisys.atlassian.net/browse/TECDIGSK-411)
- Change version MS-Audit in the dockerfile to 1.0.11 [TECDIGSK-409](https://technisys.atlassian.net/browse/TECDIGSK-409)
- Change version MS-Audit in the dockerfil to 1.0.9 [TECDIGSK-377](https://technisys.atlassian.net/browse/TECDIGSK-377)

## [1.0.8] - 2021-04-8
### Changed
- First Version. The code come from tec-digital-auditconsumer repository [TECENG-1054](https://technisys.atlassian.net/browse/TECENG-1054)
- Add APM MS-Audit [TECDIGSK-355](https://technisys.atlassian.net/browse/TECDIGSK-355)
- Change version MS-Audit in the dockerfile [TECDIGSK-356](https://technisys.atlassian.net/browse/TECDIGSK-356)

