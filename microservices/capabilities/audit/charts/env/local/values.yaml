# Default values for api.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

name: audit

labels:
  app: audit
  lang: java
  platform: digital

selectorLabels:
  app: audit

deployment:

  image:
    repository: tecdigitalacr.azurecr.io/digital/audit
    version: #{MS_VERSION}#
    pullPolicy: Always

  replicaCount: 1

  strategy:
    type: Recreate

  security:
    uid:

  port: 8080

  initContainers:
    - name: check-rabbitmq
      image: busybox:1.28
      command: ['sh', '-c', 'until nc -zv tec-digital-rabbit.digital-local.svc.cluster.local 5672; do echo waiting for rabbitmq; sleep 2; done']
    - name: check-configuration
      image: busybox:1.28
      command: [ 'sh', '-c', 'until nc -zv configuration.digital-local 80; do echo waiting for ses; sleep 2; done' ]
  envFrom:
    - configMapRef:
        name: audit-config
    - secretRef:
        name: audit-secrets

  livenessProbe:
    httpGet:
      path: /actuator/health
      port: 8080
    initialDelaySeconds: 60
    periodSeconds: 15

  readinessProbe:
    httpGet:
      path: /actuator/health
      port: 8080
    initialDelaySeconds: 30
    periodSeconds: 15



  imagePullSecrets:
    - name: regcred

  serviceAccountName: tecdigital-sa


  resources:
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    # limits:
    #   cpu: 100m
    #   memory: 128Mi
    # requests:
    #   cpu: 100m
    #   memory: 128Mi


service:
  type: ClusterIP
  port: 80
  targetPort: 8080

configPodReset:
  - configmap.yaml
  - secrets.yaml

configMap:
  name: "audit-config"
  datasource:
    url: "jdbc:sqlserver://host.docker.internal:1433;databaseName=tec_digital_audit"
    driver: "com.microsoft.sqlserver.jdbc.SQLServerDriver"
    hikari:
      minimumIdle: 5
      maximumPoolSize: 20
      connectionTimeout: 10000
  flyway:
    baselineOnMigrate: "true"
    locations: "classpath:db/migration/audit/mssql,
                classpath:db/migration/writer/digital/mssql,
                classpath:db/migration/writer/safeway/mssql,
                classpath:db/migration/writer/services/mssql,
                classpath:db/migration/writer/aida/mssql"
  cache:
    type: "redis"
    redis:
      use-key-prefix: "true"
      key-prefix: 'audit'
      time-to-live: 300000
  redis:
    enabled: "false"
    host: "redis.digital-local"
    port: "6379"
  cloud:
    stream:
      bindings:
        input:
          binder: "rabbit"
          destination: "local.audit.exchange"
          group: "local.audit.queue"
          consumer:
            concurrency: "3"
            autoStartup: "true"
          producer:
            requiredGroups: "local.audit.queue"
      binders:
        rabbit:
          type: "rabbit"
          environment:
            spring:
              rabbitmq:
                host: "tec-digital-rabbit.digital-local.svc.cluster.local"
                port: "5672"
                virtualHost: "/"
      rabbit:
        bindings:
          input:
            consumer:
              exchangeType: "direct"
              bindingRoutingKey: "local.audit.routing"
              exchangeName: "local.audit.exchange"
              queueNameGroupOnly: "true"
              bindQueue: "true"
              exchangeDurable: "true"
              acknowledgeMode: "AUTO"
              maxConcurrency: "10"  # Maximum 2047
              prefetch: "250"
  management:
    health:
      rabbit:
        enabled: false
  jobs:
    safeway:
      timeToRun: "0 */20 * * * ?"
    safewayAdmin:
      timeToRun: "0 */20 * * * ?"
    digital:
      timeToRun: "0 */1 * * * ?"
  writers:
    aida:
      enable: true
      jdbc: 
        enable: true
      rabbit:
        enable: false
        exchange-name: aida-exchange
        routing-key: aida
    syslog:
      enable: false
      url: syslog-ng.digital-local.svc.cluster.local
      port: 514
      hostName: local
    services:
      enable: true
  ses:
    url: "http://ses.digital-local"
  configuration:
    url: "http://configuration.digital-local"
  generalAudit:
    persist:
      enabled: false
  rabbit:
    enable: true
  kafka:
    enable: true
    topics: queue.audit.logs
    group:
      id: audit
    broker:
      url: "tec-confluent-oss-cp-kafka-headless:9092"
    schema:
      registry:
        url: "http://tec-confluent-oss-cp-schema-registry:8081"
  ELASTIC_APM_SERVER_URLS: "#{apmserver.url}#"
  ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
  ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
  ELASTIC_APM_APPLICATION_PACKAGES: "com.technisys"
  ELASTIC_APM_CENTRAL_CONFIG: "true"
  ELASTIC_APM_SERVICE_NAME: "audit"
  ELASTIC_APM_LOG_LEVEL: "INFO"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "true"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: "com.technisys.digital.audit.controller.*"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: "(?-i)java.*, (?-i)javax.*, (?-i)sun.*, (?-i)com.sun.*,
                                                          (?-i)jdk.*, (?-i)org.apache.tomcat.*,
                                                          (?-i)org.apache.catalina.*, (?-i)org.apache.coyote.*,
                                                          (?-i)org.eclipse.jetty.*, (?-i)org.springframework.*"
  ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc, redis"
  JAVA_TOOL_OPTIONS: ""
  SPRING_CACHE_TYPE: "redis"
  SPRING_CACHE_REDIS_USEKEYPREFIX: true
  SPRING_CACHE_REDIS_KEYPREFIX: "Audit::"
  SPRING_CACHE_REDIS_TIMETOLIVE: "100s"
  SPRING_REDIS_HOST: "redis.digital-local"
  SPRING_REDIS_PORT: 6379
  I18N_URL: http://i18n.digital-local

secrets:
  name: audit-secrets
  dbserver:
    username: "sa"
    password: "password"
  rabbitmq:
    username: "username"
    password: "password"
  elastic:
    apmtoken: "#{elastic.apmtoken}#"