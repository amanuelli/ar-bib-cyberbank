if $TRACE_ENABLED; then
  java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -Djava.security.egd=file:/dev/./urandom org.springframework.boot.loader.JarLauncher
else
  java -Djava.security.egd=file:/dev/./urandom org.springframework.boot.loader.JarLauncher
fi