# Audit Microservice 

# Content

By default, this services is based on Audit .jar and builds a Docker image.

## Code analysis

N/A. This is not a code project.

## Pipeline Status

 CICD     | 
----------|
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20Audit/AUDIT%20-%20Techbank%20-%20CI-CD?branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=1946&branchName=master)|

## Jira project

TEC - Digital Starterkit: [TECDIGSK](https://technisys.atlassian.net/projects/TECDIGSK/issues?filter=allissues)

### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

 Environment variable            | Description
|------------------------------- | ---------------------------------
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG       | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT      | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL            | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS          | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME         | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS                | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"
BRIDGE_TRACING_ENABLED           | Boolean value to indicate id bridge tracing is enabled https://bitbucket.org/technisys/tec-commons-tracing/src/master/

### Environment variables

Environment variable                           | Description
|--------------------------------------------- | ---------------------------------
SPRING_CLOUD_STREAM_BINDINGS_INPUT_BINDER      | 
SPRING_CLOUD_STREAM_BINDINGS_INPUT_DESTINATION |
SPRING_CLOUD_STREAM_BINDINGS_INPUT_GROUP       |
SPRING_CLOUD_STREAM_BINDINGS_INPUT_CONSUMER_CONCURRENCY  | 
SPRING_CLOUD_STREAM_BINDINGS_INPUT_CONSUMER_AUTO_STARTUP |
SPRING_CLOUD_STREAM_BINDINGS_INPUT_PRODUCER_REQUIRED_GROUPS | 
SPRING_CLOUD_STREAM_BINDERS_RABBIT_TYPE        |
SPRING_CLOUD_STREAM_BINDERS_RABBIT_ENVIRONMENT_SPRING_RABBITMQ_HOST |
SPRING_CLOUD_STREAM_BINDERS_RABBIT_ENVIRONMENT_SPRING_RABBITMQ_PORT |
SPRING_CLOUD_STREAM_BINDERS_RABBIT_ENVIRONMENT_SPRING_RABBITMQ_VIRTUAL_HOST |
SPRING_CLOUD_STREAM_RABBIT_BINDINGS_INPUT_CONSUMER_EXCHANGE_TYPE |
SPRING_CLOUD_STREAM_RABBIT_BINDINGS_INPUT_CONSUMER_BINDING_ROUTING_KEY |
SPRING_CLOUD_STREAM_RABBIT_BINDINGS_INPUT_CONSUMER_EXCHANGE_NAME |
SPRING_CLOUD_STREAM_RABBIT_BINDINGS_INPUT_CONSUMER_QUEUE_NAME_GROUP_ONLY |
SPRING_CLOUD_STREAM_RABBIT_BINDINGS_INPUT_CONSUMER_BIND_QUEUE |
SPRING_CLOUD_STREAM_RABBIT_BINDINGS_INPUT_CONSUMER_ACKNOWLEDGE-MODE |
SPRING_CLOUD_STREAM_RABBIT_BINDINGS_INPUT_CONSUMER_MAX_CONCURRENCY |
SPRING_CLOUD_STREAM_RABBIT_BINDINGS_INPUT_CONSUMER_PREFETCH |
spring.cloud.stream.binders.rabbit.environment.spring.rabbitmq.username |
spring.cloud.stream.binders.rabbit.environment.spring.password          |
SPRING_DATASOURCE_URL                          |
SPRING_DATASOURCE_DRIVER-CLASS-NAME            |
SPRING_DATASOURCE_HIKARI_MINIMUMIDLE           |
SPRING_DATASOURCE_HIKARI_MAXIMUMPOOLSIZE       |
SPRING_DATASOURCE_HIKARI_CONNECTIONTIMEOUT     |
SPRING_FLYWAY_LOCATIONS                        |
SPRING_FLYWAY_BASELINE-ON-MIGRATE              |
SPRING_CACHE_TYPE                              |
SPRING_CACHE_REDIS_USEKEYPREFIX                |
SPRING_CACHE_REDIS_KEYPREFIX                   |
SPRING_CACHE_REDIS_TIMETOLIVE                  |
SPRING_REDIS_HOST                              | 
SPRING_REDIS_PORT                              | 
JOBS_SAFEWAY_TIME-TO-RUN                       | 
JOBS_SAFEWAYADMIN_TIME-TO-RUN                  | 
JOBS_DIGITAL_TIME-TO-RUN                       |
WRITERS_AIDA_ENABLE                            |
WRITERS_SYSLOG_ENABLE                          |
WRITERS_SYSLOG_URL                             | 
WRITERS_SYSLOG_PORT                            |
WRITERS_SYSLOG_HOSTNAME                        |
WRITERS_SERVICES_ENABLE                        |
MANAGEMENT_HEALTH_RABBIT_ENABLED               |
SES_URL                                        | 
CONFIGURATION_URL                              |
GENERALAUDIT_PERSIST_ENABLED                   |
RABBIT_ENABLE                                  | Enable Rabbit Broker. Ex: true or false
KAFKA_ENABLE                                   | Enable Kafka Broker. Ex: true or false
KAFKA_TOPICS                                   | Kafka topic names: Ex: queue.audit.logs
KAFKA_BROKER_URL                               | Kafka broker url. Ex: localhost:9092
KAFKA_SCHEMA_REGISTRY_URL                      | Kafka Schema Registry url. Ex: http://localhost:8081
KAFKA_GROUP_ID                                 | Kafka Group Id. Ex: audit


## Kafka local installation

To test and run a local kafka you can install it following these steps and using docker-compose: [Apache Kafka Quickstart](https://docs.confluent.io/platform/current/quickstart/ce-docker-quickstart.html)