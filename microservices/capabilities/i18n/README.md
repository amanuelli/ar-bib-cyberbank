# Content

By default, this services is based on I18n image and add new resources.

## Code analysis

N/A. This is not a code project.

## Pipeline Status

 CICD     | 
----------|
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20I18n/Master%20-%20Techbank%20I18n?repoName=technisys%2Ftec-omnichannel&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=88&branchName=master)|

## Jira project

TEC - Digital Starterkit: [TECDIGSK](https://technisys.atlassian.net/projects/TECDIGSK/issues?filter=allissues)

## How to add new messages

Just like in the previous version, adding, modifying and removing messages on resources is done by editing .properties files.
The format of these .properties files is:

Category_yyymmdd_mmss_issue_lang.properties

For example:

Backoffice_20190905_1234_6574_es.properties
Client_20191012_1225_7876_es.properties

With content as such:

key1=value1
key2=value2
...
...

The messages that can be modified by projects are in the root of messages folder. "messages/platform" folder properties file must be changed only by product team.

## How to run

### Preconditions:

1. Having Docker downloaded
2. Being logged into a Docker account with read access to the registry
    ```bash
    $ docker login tecdigitalacr.azurecr.io \
        -u=6b801393-1f48-4305-90c8-28948df8a713 \
        -p=b1236605-6b95-4772-845f-4bc9ee4195ad
    ```
3. Having your terminal open in this folder

Follow the next steps to run this image with Docker:

1. Make sure the configuration variable `core.i18n.componentFQN` is set to `com.technisys.omnichannel.core.i18n.msplatform.I18nMS` by running the following script in your database
    ```sql
    UPDATE configuration SET value = 'com.technisys.omnichannel.core.i18n.msplatform.I18nMS' WHERE id_field = 'core.i18n.componentFQN';
    ```
2. Build and run the docker image with a name of your choice by running the following script in your terminal (Remove the '\' and line breaks if you're running this in Windows)
    ```bash
    $ cd microservices/i18n
    $ docker build -t i18n .
    $ docker run --name i18n \
          -p 9090:8080 \
          -e spring.datasource.username=sa \
          -e spring.datasource.password=<Your_DB_Password> \
          -e spring.datasource.url="jdbc:sqlserver://<Your_DB's_IP>:1433;database=digital_i18n" \
          i18n
    ```

    This script assumes you have a database called `digital_i18n` and that it will be running on SQL Server. You may also use Oracle instead, by setting the environment variable `spring.datasource.driver` to your oracle driver and setting `spring.flyway.locations="classpath:db/migration/oracle"`

3. Run Digital with the environment variable `i18n.url=http://localhost:9090/` using Digital's plugin for IntelliJ

## Configuration

Env                        | Description                                       
---------------------------|---------------------------------------------------
`SPRING_DATASOURCE_URL`      | Database url, e.g.: `jdbc:sqlserver://localhost:1433;database=tec_digital_i18n`
`SPRING_DATASOURCE_DRIVER`   | Database connection driver, e.g.: `com.microsoft.sqlserver.jdbc.SQLServerDriver`
`SPRING_DATASOURCE_USERNAME` | Database username
`SPRING_DATASOURCE_PASSWORD` | Database password
`SPRING_DATASOURCE_HIKARI_MINIMUMIDLE` | Minimum number of idle connections maintained in a connection pool
`SPRING_DATASOURCE_HIKARI_MAXIMUMPOOLSIZE` | Maximum connection pool size
`SPRING_DATASOURCE_HIKARI_CONNECTIONTIMEOUT` | Maximum milliseconds that a client will wait for a connection
`SPRING_FLYWAY_LOCATIONS` | To choose migrations according to the db engine. Options: `classpath:db/migration/mssql` or `classpath:db/migration/oracle`
`SPRING_CACHE_TYPE` | Cache type. Options: `none` or `redis`
`SPRING_CACHE_REDIS_KEY-PREFIX` | Key prefix, e.g.: `I18n::`
`SPRING_CACHE_REDIS_TIME-TO-LIVE` | Entry expiration, e.g.: `5m`
`SPRING_CACHE_REDIS_USE-KEY-PREFIX` | Whether to use the key prefix when writing to Redis
`SPRING_REDIS_HOST` | Redis server host
`SPRING_REDIS_PORT` | Redis server port
`BRIDGE_TRACING_ENABLED`     | Boolean value to indicate whether bridge tracing is enabled. See [https://bitbucket.org/technisys/tec-commons-tracing/src/master/](https://bitbucket.org/technisys/tec-commons-tracing/src/master/)

### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable             | Description
---------------------------------|----------------------------------------
`ELASTIC_APM_APPLICATION_PACKAGES` | String value to indicate name package to tracking example: "com.technisys"
`ELASTIC_APM_CENTRAL_CONFIG`       | Boolean value to say if config is centralize
`ELASTIC_APM_DISABLE_INSTRUMENTATIONS` | Array string value to disable instrumentation example> "jdbc, redis"
`ELASTIC_APM_ENABLE_LOG_CORRELATION` | Boolean value to enable correlation log 
`ELASTIC_APM_LOG_FORMAT_SOUT`      | String value to indicate format output log example: "JSON"
`ELASTIC_APM_LOG_LEVEL`            | String value to indicate level log example: "INFO"
`ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED` | Boolean value to enable inferred spans 
`ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES` | String value to indicate what class will be exclude in the span
`ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES` | String value to indicate what class will be include in the span
`ELASTIC_APM_SERVER_URLS`         | String value to indicate URL of apm-server
`ELASTIC_APM_SERVICE_NAME`         | String value to indicate name of application who implement APM
`ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME` | Boolean value to indicate if path will be use as transaction name
`JAVA_TOOL_OPTIONS`               | String value to run java with the apm param and indicate where is apm-agent.jar example: `-javaagent:/PATH/elastic-apm-agent.jar`

### Database Engine

In file `values.yaml` on env folders set the variable `deployment.configMap.dbserver.engine` to `SQLServer` or `Oracle`

Case SQLServer ex:

```yaml
  dbserver:
    url: jdbc:sqlserver://<HOST>;databaseName=<DBNAME>
    driver: com.microsoft.sqlserver.jdbc.SQLServerDriver
  SPRING_FLYWAY_LOCATIONS: "classpath:db/migration/mssql"
```
Change `<HOST>` and `<DBNAME>` with correct values, ex: `jdbc:sqlserver://tec-digital.database.windows.net;databaseName=tec-digital-i18n-dev-aks`


Case Oracle ex:

```yaml
  dbserver:
    url: jdbc:oracle:thin:@<HOST>:1521:<SID>
    driver: com.oracle.jdbc.OracleDriver
  SPRING_FLYWAY_LOCATIONS: "classpath:db/migration/oracle"
```
Change `<HOST>` and `<SID>` with correct values, ex: `jdbc:oracle:thin:@host.docker.internal:1521:ORCLCDB`