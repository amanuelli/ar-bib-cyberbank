# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [1.0.13] - 2021-10-20
### Changed
- Remove messages from the legacy modules backofficeusers/groups/backofficeroles [TECDIGSK-550](https://technisys.atlassian.net/browse/TECDIGSK-550)
- Created audit permission message to view Customer authentication audit, User authentication audit. [TECMSAAU-259](https://technisys.atlassian.net/browse/TECMSAAU-259)
- Add permission message for communications trays messages. [TECDIGSK-566](https://technisys.atlassian.net/browse/TECDIGSK-566)
- Invitation codes, transaction limits and general conditions permissions label adjusted [TECDIGSK-569](https://technisys.atlassian.net/browse/TECDIGSK-569)
- Add permission for List communications trays messages. [TECDIGSK-581](https://technisys.atlassian.net/browse/TECDIGSK-581) 
- Add dual approve for communications trays. [TECDIGSK-586](https://technisys.atlassian.net/browse/TECDIGSK-586)
- Change timezones message keys replacing "/" with "_". [TECDIGSK-604](https://technisys.atlassian.net/browse/TECDIGSK-604)
- Add messages general conditions public categories [TECDIGSK-562](https://technisys.atlassian.net/browse/TECDIGSK-562)
- Message Trays permissions texts modified [TECDIGSK-615](https://technisys.atlassian.net/browse/TECDIGSK-615)
- Remove Backoffice Legacy environment messages [TECCDPBO-5146](https://technisys.atlassian.net/browse/TECDIGSK-5146)
- Update platform i18n component to 1.1.5 [TECDIGSK-669](https://technisys.atlassian.net/browse/TECDIGSK-669)

## [[1.0.12]](https://technisys.atlassian.net/projects/TECDIGSK/versions/55396/tab/release-report-all-issues) - 2021-08-06
### Changed
- Update platform i18n component to 1.1.3 . [TECDIGSK-495](https://technisys.atlassian.net/browse/TECDIGSK-495)
- Added time to live and prefix as Redis properties to modify default values [TECDIGSK-477](https://technisys.atlassian.net/browse/TECDIGSK-477)
- Other Spring Boot cache options can be used now by adding its dependencies and configuration via properties or even code. [TECDIGSK-450](https://technisys.atlassian.net/browse/TECDIGSK-450)
- Added messages for general conditions configurations [TECDIGSK-472](https://technisys.atlassian.net/browse/TECDIGSK-472)
- Added messages for invitation codes configuration. Message modified for invitation codes expired status [TECDIGSK-452](https://technisys.atlassian.net/browse/TECDIGSK-452)
- Added messages for Communications Trays configurations [TECDIGSK-486](https://technisys.atlassian.net/browse/TECDIGSK-486)

## [1.0.11] - 2021-07-01
### Changed
- Update platform i18n component to 1.1.0

## [1.0.10] - 2021-06-09
## Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
### Changed
- Update platform i18n component to 1.0.9

## [1.0.9] - 2021-05-03
### Changed
- Update platform i18n component to 1.0.8

## [1.0.8] - 2021-04-15
### Changed
- Update to Profile H v2.0. with invoke [TECENG-1027] (https://technisys.atlassian.net/browse/TECENG-1027)
- Update to Profile C v2.0.0 with invoke [TECENG-277](https://technisys.atlassian.net/browse/TECENG-277)

## [1.0.7] - 2021-03-04
### Fixed
- List filter fixed [TECMSI18N-92](https://technisys.atlassian.net/browse/TECMSI18N-92)

### Added
- Add list application endpoint [TECMSI18N-90](https://technisys.atlassian.net/browse/TECMSI18N-90)

## [1.0.6] - 2021-03-01
### Added
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)

### Changed
- Implement v3 alternative of delivery workflow [TECENG-845](https://technisys.atlassian.net/browse/TECENG-845)
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-654](https://technisys.atlassian.net/browse/TECENG-654).
- Remove JAEGER variables and set new variable BRIDGE_TRACING_ENABLED [TECDIGSK-253](https://technisys.atlassian.net/browse/TECDIGSK-253)

## [1.0.5] - 2020-10-21
### Fixed
- Helm, chart and app versions updated [TECDIGSK-178](https://technisys.atlassian.net/browse/TECDIGSK-178)
- Helm, chart and app versions updated [TECDIGSK-164](https://technisys.atlassian.net/browse/TECDIGSK-164)

### Changed
- Apply trivy's security container scanning [TECENG-422](https://technisys.atlassian.net/browse/TECENG-422)

## [1.0.4] - 2020-10-21
### Changed
- Refactor release pipeline to use profile template [TECDIGSK-82](https://technisys.atlassian.net/browse/TECDIGSK-82)

### Fixed
- Updated version, move platform resource files to platform folder [TECDIGSK-118](https://technisys.atlassian.net/browse/TECDIGSK-118)

### 
### Versions 1.0.1, 1.0.2 and 1.0.3 were not registered in this file
### 

## [1.0.0] - 2020-09-14
### Changed
- Update profile template path [TECDIGSK-54](https://technisys.atlassian.net/browse/TECDIGSK-54)
- Refactor CICD pipelines to use profile templates [TECDIGSK-250](https://technisys.atlassian.net/browse/TECDIGSK-250)

### Added
- Added Jira project link to Readme file [TECDIGSK-21](https://technisys.atlassian.net/browse/TECDIGSK-21)

