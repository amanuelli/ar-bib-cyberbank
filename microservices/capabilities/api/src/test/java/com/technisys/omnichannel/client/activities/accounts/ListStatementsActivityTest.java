package com.technisys.omnichannel.client.activities.accounts;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import utils.TestUtils;

import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, ConfigurationFactory.class, RubiconCoreConnectorC.class, CoreAccountConnectorOrchestrator.class})
public class ListStatementsActivityTest {
    private Request request;

    @Mock
    private Administration administrationMock;

    @Mock
    private Configuration configurationMock;


    private ListStatementsActivity activity;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString()))
                .thenReturn("http://WEBSERVICES_URL:PORT");

        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(10);

        PowerMockito.mockStatic(CoreAccountConnectorOrchestrator.class);
        PowerMockito.mockStatic(RubiconCoreConnectorC.class);

        request = new Request();
        activity = new ListStatementsActivity();
    }

    @Test
    public void executeReturnOkTest() throws IOException, ActivityException, BackendConnectorException {
        Mockito.when(administrationMock.readProduct(
                Mockito.any(),
                Mockito.anyInt()
        )).thenReturn(TestUtils.getValidProduct());

        Mockito.when(CoreAccountConnectorOrchestrator.readAccount(
                Mockito.any(),
                Mockito.any()
        )).thenReturn(TestUtils.getValidAccount());

        Mockito.when(RubiconCoreConnectorC.listAccountStatementsTotalCount(
                Mockito.any()
        )).thenReturn(1);

        Mockito.when(CoreAccountConnectorOrchestrator.statements(
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any(),
                Mockito.any()
        )).thenReturn(TestUtils.getStatementResult());

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }
}