/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.TransferForeignBankDetail;
import com.technisys.omnichannel.client.domain.TransferInternalDetail;
import com.technisys.omnichannel.client.domain.TransferOtherBankDetails;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.utils.CacheUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.ArgumentMatchers.eq;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigurationFactory.class, CyberbankCoreConnector.class, CyberbankCoreRequest.class, CacheUtils.class, ProductUtils.class})
public class CyberbankTransferConnectorTest {

    @Mock
    private Configuration configurationMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM),eq("connector.cyberbank.core.template.request"), eq("/templates/connectors/cyberbank/request/"))).thenReturn("/templates/connectors/cyberbank/request/");

        PowerMockito.mockStatic(CyberbankCoreRequest.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CacheUtils.class);
        PowerMockito.mockStatic(ProductUtils.class);
    }

    @Test
    public void testInternal() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{}"));
        Account accountFrom = new Account();
        accountFrom.setProductType("CA");
        accountFrom.setExtraInfo("CA|1234|USD|01000|10|10001|1|1");

        Account accountTo = new Account();
        accountTo.setProductType("CA");
        accountTo.setExtraInfo("CA|3334|USD|01000|10|10001|1|1");
        CyberbankCoreConnectorResponse<TransferInternalDetail> response = CyberbankTransferConnector.internal(accountFrom, accountTo, 100, "description");

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
    }

    @Test
    public void toLocalBanksOK() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{}"));
        Account accountFrom = new Account();
        accountFrom.setProductType("CA");
        accountFrom.setExtraInfo("CA|1234|USD|01000|10|10001|1|1");

        CyberbankCoreConnectorResponse<TransferOtherBankDetails> response = CyberbankTransferConnector.toLocalBanks("1234", accountFrom, "1234", "Juan", "1234", new Amount("1", 10.0), "1", "reference", "299");
        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
    }

    @Test
    public void toLocalBanksWithError() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"processEft_TransferInsert_Other_Bank_Channel\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": 1,\n" +
                "                \"description\": \"[in.message]\",\n" +
                "                \"id\": 1,\n" +
                "                \"codigoerror\": 1179\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}"));
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);
        Account accountFrom = new Account();
        accountFrom.setProductType("CA");
        accountFrom.setExtraInfo("CA|1234|USD|01000|10|10001|1|1");

        CyberbankCoreConnectorResponse<TransferOtherBankDetails> response = CyberbankTransferConnector.toLocalBanks("1234", accountFrom, "1234", "Juan", "1234", new Amount("1", 10.0), "1", "reference", "299");
        Assert.assertNotNull(response);
    }

    @Test
    public void toForeignlBank() throws CyberbankCoreConnectorException{
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{}"));
        Account accountFrom = new Account();
        accountFrom.setProductType("CA");
        accountFrom.setExtraInfo("CA|1234|USD|01000|10|10001|1|1");

        CyberbankCoreConnectorResponse<TransferForeignBankDetail> response = CyberbankTransferConnector.toForeignlBank("1234", accountFrom, "1234", "Juan", "1234", new Amount("1", 10.0), "1", "reference", "299", "SWIFT", "PRBAARBA", "mensaje de test");
        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
    }

    @Test
    public void toForeignlBankWithError() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"processEft_TransferInsert_Other_Bank_Channel\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": 1,\n" +
                "                \"description\": \"[in.message]\",\n" +
                "                \"id\": 1,\n" +
                "                \"codigoerror\": 1179\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}"));
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);
        Account accountFrom = new Account();
        accountFrom.setProductType("CA");
        accountFrom.setExtraInfo("CA|1234|USD|01000|10|10001|1|1");

        CyberbankCoreConnectorResponse<TransferForeignBankDetail> response = CyberbankTransferConnector.toForeignlBank("1234", accountFrom, "1234", "Juan", "1234", new Amount("1", 10.0), "1", "reference", "299", "SWIFT", "PRBAARBA", "mensaje de test");
        Assert.assertNotNull(response);
    }
}
