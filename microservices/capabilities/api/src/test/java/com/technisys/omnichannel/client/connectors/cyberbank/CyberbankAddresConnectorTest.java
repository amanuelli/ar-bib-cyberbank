package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.Province;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.utils.CacheUtils;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({
        ConfigurationFactory.class,
        SecurityUtils.class,
        CyberbankCoreConnector.class,
        CyberbankCoreRequest.class,
        CacheUtils.class
})
public class CyberbankAddresConnectorTest {
    @Mock
    private Configuration configurationMock;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), eq("connector.cyberbank.core.template.request"), eq("/templates/connectors/cyberbank/request/"))).thenReturn("/templates/connectors/cyberbank/request/");


        PowerMockito.mockStatic(CyberbankCoreRequest.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CacheUtils.class);

    }

    @Test
    public void testListProvincesOk() throws IOException, CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListProvincesOK());
        Mockito.when(CacheUtils.get("cyberbank.core.quotationType")).thenReturn(null);
        CyberbankCoreConnectorResponse<List<Province>> response = CyberbankAddresConnector.listProvince(null);

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());

    }

    @Test(expected = NullPointerException.class)
    public void testListProvincesNoOk() throws BackendConnectorException {
        Mockito.when(CyberbankAddresConnector.listProvince(null )).thenThrow(NullPointerException.class);
    }

    public JSONObject getResponseListProvincesOK(){
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectProvinceBy_Country\",\n" +
                "    \"transactionId\": \"massiveSelectProvinceBy_Country\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.count\": 24,\n" +
                "    \"out.province_code_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"1\",\n" +
                "                \"provinceCodeId\": 1,\n" +
                "                \"shortDesc\": \"Autonomous City of Buenos Aires\",\n" +
                "                \"id\": 500001,\n" +
                "                \"longDesc\": \"\\tCiudad Autónoma de Buenos Aires\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"2\",\n" +
                "                \"provinceCodeId\": 2,\n" +
                "                \"shortDesc\": \"Buenos Aires province\",\n" +
                "                \"id\": 500002,\n" +
                "                \"longDesc\": \"\\tProvincia de Buenos Aires\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"3\",\n" +
                "                \"provinceCodeId\": 3,\n" +
                "                \"shortDesc\": \"Catamarca Province\",\n" +
                "                \"id\": 500003,\n" +
                "                \"longDesc\": \"\\tProvincia de Catamarca\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"6\",\n" +
                "                \"provinceCodeId\": 6,\n" +
                "                \"shortDesc\": \"Chaco Province\",\n" +
                "                \"id\": 500006,\n" +
                "                \"longDesc\": \"\\tProvincia de Chaco\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"7\",\n" +
                "                \"provinceCodeId\": 7,\n" +
                "                \"shortDesc\": \"Province of Chubut\",\n" +
                "                \"id\": 500007,\n" +
                "                \"longDesc\": \"\\tProvincia de Chubut\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"5\",\n" +
                "                \"provinceCodeId\": 5,\n" +
                "                \"shortDesc\": \"Corrientes Province\",\n" +
                "                \"id\": 500005,\n" +
                "                \"longDesc\": \"\\tProvincia de Corrientes\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"8\",\n" +
                "                \"provinceCodeId\": 8,\n" +
                "                \"shortDesc\": \"Province of Entre Rios\",\n" +
                "                \"id\": 500008,\n" +
                "                \"longDesc\": \"\\tProvincia de Entre Ríos\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"9\",\n" +
                "                \"provinceCodeId\": 9,\n" +
                "                \"shortDesc\": \"Formosa Province\",\n" +
                "                \"id\": 500009,\n" +
                "                \"longDesc\": \"\\tProvincia de Formosa\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"10\",\n" +
                "                \"provinceCodeId\": 10,\n" +
                "                \"shortDesc\": \"Jujuy Province\",\n" +
                "                \"id\": 500010,\n" +
                "                \"longDesc\": \"\\tProvincia de Jujuy\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"11\",\n" +
                "                \"provinceCodeId\": 11,\n" +
                "                \"shortDesc\": \"Province of La Pampa\",\n" +
                "                \"id\": 500011,\n" +
                "                \"longDesc\": \"\\tProvincia de La Pampa\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"12\",\n" +
                "                \"provinceCodeId\": 12,\n" +
                "                \"shortDesc\": \"Province of La Rioja\",\n" +
                "                \"id\": 500012,\n" +
                "                \"longDesc\": \"\\tProvincia de La Rioja\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"14\",\n" +
                "                \"provinceCodeId\": 14,\n" +
                "                \"shortDesc\": \"Province of Misiones\",\n" +
                "                \"id\": 500014,\n" +
                "                \"longDesc\": \"\\tProvincia de Misiones\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"15\",\n" +
                "                \"provinceCodeId\": 15,\n" +
                "                \"shortDesc\": \"Province of Neuquen\",\n" +
                "                \"id\": 500015,\n" +
                "                \"longDesc\": \"\\tProvincia de Neuquén\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"16\",\n" +
                "                \"provinceCodeId\": 16,\n" +
                "                \"shortDesc\": \"\\tProvincia de Río Negro\\t\",\n" +
                "                \"id\": 500016,\n" +
                "                \"longDesc\": \"\\tProvincia de Río Negro\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"17\",\n" +
                "                \"provinceCodeId\": 17,\n" +
                "                \"shortDesc\": \"Salta Province\",\n" +
                "                \"id\": 500017,\n" +
                "                \"longDesc\": \"\\tProvincia de Salta\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"18\",\n" +
                "                \"provinceCodeId\": 18,\n" +
                "                \"shortDesc\": \"San Juan Province\",\n" +
                "                \"id\": 500018,\n" +
                "                \"longDesc\": \"\\tProvincia de San Juan\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"19\\n\",\n" +
                "                \"provinceCodeId\": 19,\n" +
                "                \"shortDesc\": \"San Luis Province\",\n" +
                "                \"id\": 500019,\n" +
                "                \"longDesc\": \"\\tProvincia de San Luis\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"20\",\n" +
                "                \"provinceCodeId\": 20,\n" +
                "                \"shortDesc\": \"Province of Santa Cruz\",\n" +
                "                \"id\": 500020,\n" +
                "                \"longDesc\": \"\\tProvincia de Santa Cruz\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"21\",\n" +
                "                \"provinceCodeId\": 21,\n" +
                "                \"shortDesc\": \"Province of Santa Fe\",\n" +
                "                \"id\": 500021,\n" +
                "                \"longDesc\": \"\\tProvincia de Santa Fe\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"22\",\n" +
                "                \"provinceCodeId\": 22,\n" +
                "                \"shortDesc\": \"Santiago del Estero Province\",\n" +
                "                \"id\": 500022,\n" +
                "                \"longDesc\": \"\\tProvincia de Santiago del Estero\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"23\",\n" +
                "                \"provinceCodeId\": 23,\n" +
                "                \"shortDesc\": \"Tierra del Fuego Province\",\n" +
                "                \"id\": 500023,\n" +
                "                \"longDesc\": \"\\tProvincia de Tierra del Fuego\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20151224000000000\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"24\",\n" +
                "                \"provinceCodeId\": 24,\n" +
                "                \"shortDesc\": \"Province of Tucuman\",\n" +
                "                \"id\": 500024,\n" +
                "                \"longDesc\": \"\\tProvincia de Tucumán\\t\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323154911823\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"4\",\n" +
                "                \"provinceCodeId\": 4,\n" +
                "                \"shortDesc\": \"Province of Cordoba\",\n" +
                "                \"id\": 500004,\n" +
                "                \"longDesc\": \"Provincia de Córdoba\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323112112859\",\n" +
                "                \"country\": {\n" +
                "                    \"nemotecnico\": \"Argentina\",\n" +
                "                    \"shortDesc\": \"Argentina\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"countryId\": 1,\n" +
                "                    \"longDesc\": \"Argentina\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"13\",\n" +
                "                \"provinceCodeId\": 13,\n" +
                "                \"shortDesc\": \"Province of Mendoza\",\n" +
                "                \"id\": 500013,\n" +
                "                \"longDesc\": \"Provincia de Mendoza\",\n" +
                "                \"status\": {\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"ProvinceCode\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}");
    }

}
