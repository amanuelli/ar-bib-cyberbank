package com.technisys.omnichannel.client.activities.desktop;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreProductConnectorOrchestrator;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.client.connectors.orchestrator.CoreConnectorOrchestrator.RUBICON_CONNECTOR;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, ConfigurationFactory.class, CoreProductConnectorOrchestrator.class})
public class CorporateGroupDesktopActivityTest {

    @Mock
    private Administration administrationMock;

    @Mock
    private Configuration configurationMock;

    private Request request;
    private CorporateGroupDesktopActivity activity;

    private String idUserForTest = "idUserForTest";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        PowerMockito.mockStatic(CoreProductConnectorOrchestrator.class);

        request = new Request();

        Map<String, Object> parameters = new HashMap<>();

        request.setIdTransaction("idTransaction");
        request.setParameters(parameters);
        request.setIdUser(idUserForTest);
        request.setIdEnvironment(11);

        activity = new CorporateGroupDesktopActivity();
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionTest() throws IOException, ActivityException {
        Mockito
                .when(administrationMock.readEnvironment(Mockito.anyInt()))
                .thenThrow(IOException.class);

        activity.execute(request);
    }

    @Test
    public void executeTest1() throws IOException, ActivityException {

        Environment environment = new Environment();
        environment.setClients(new ArrayList<>());
        environment.setEnvironmentType(Environment.ENVIRONMENT_TYPE_CORPORATE_GROUP);
        Mockito
                .when(administrationMock.readEnvironment(Mockito.anyInt()))
                .thenReturn(environment);

        Mockito
                .when(configurationMock.getString(eq(Configuration.PLATFORM), Mockito.anyString()))
                .thenReturn("USD");

        Mockito
                .when(administrationMock.listEnvironments(Mockito.anyString()))
                .thenReturn(new ArrayList<Environment>() {
                });

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
        assertEquals(1, response.getData().size());
    }

    @Test
    public void executeTest2() throws IOException, BackendConnectorException, ActivityException {
        String idClientTest = "1";
        String nameClientTest = "clientName";
        Environment environment = new Environment();
        List<Client> clients = new ArrayList<>();
        Client client = new Client();
        client.setIdClient(idClientTest);
        client.setName(nameClientTest);
        clients.add(client);

        List<Environment> userEnvs = new ArrayList<>();
        Environment userEnv = new Environment();
        userEnv.setIdEnvironment(1);
        userEnv.setName("name");
        userEnvs.add(userEnv);

        environment.setClients(clients);
        environment.setEnvironmentType(Environment.ENVIRONMENT_TYPE_CORPORATE_GROUP);

        Mockito
                .when(CoreProductConnectorOrchestrator.list(Mockito.anyString(), Mockito.anyList(), Mockito.anyList()))
                .thenReturn(new ArrayList<>());

        Mockito
                .when(administrationMock.readEnvironment(Mockito.anyInt()))
                .thenReturn(environment);

        Mockito
                .when(configurationMock.getString(eq(Configuration.PLATFORM), Mockito.anyString()))
                .thenReturn("USD");

        Mockito
                .when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyString()))
                .thenReturn(RUBICON_CONNECTOR);

        Mockito
                .when(administrationMock.listEnvironments(Mockito.anyString()))
                .thenReturn(userEnvs);

        Response response = activity.execute(request);

        Map<String, Object> responseData = response.getData();
        Map<String, Object> clientResponseDataMap = (Map<String, Object>)responseData.get("clientsData");
        Map<String, Object> clientResponseData = (Map<String, Object>)clientResponseDataMap.get(idClientTest);

        assertEquals(1, response.getData().size());
        assertEquals(false, clientResponseData.get("canLogin"));
        assertEquals(new Long(0), clientResponseData.get("total"));
        assertEquals(-1, clientResponseData.get("idEnvironment"));
        assertEquals(nameClientTest, clientResponseData.get("name"));
        assertEquals(0, ((List)clientResponseData.get("accounts")).size());
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

}