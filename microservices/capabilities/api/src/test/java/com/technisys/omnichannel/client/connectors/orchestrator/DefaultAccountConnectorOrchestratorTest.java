/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.StatementResult;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Product;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class})

public class DefaultAccountConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);

    }

    @Test(expected = BackendConnectorException.class)
    public void testAddAccountTypeCyberbankOk() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreAccountConnectorOrchestrator.addAccount("212", "3131");
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadAccountOK() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        Product product = new Product();
        product.setExtraInfo("CA|3334|USD|01000|10|10001|1|1");
        CoreAccountConnectorOrchestrator.readAccount("323", product);
    }

    @Test(expected = BackendConnectorException.class)
    public void testStatementsOK() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        Date startDate = new Date();
        Date endDate = new Date();
        CoreAccountConnectorOrchestrator.statements("21",
               "323",
                "232",
                startDate,
                endDate,
                0.2,
               0.5,
               "ref",
                new String[0],
               0,
                10,
                1);

    }

    @Test(expected = BackendConnectorException.class)
    public void testCountMovements() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CyberbankCoreConnectorResponse<StatementResult> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new StatementResult(0, new ArrayList<>()));
        CoreAccountConnectorOrchestrator.countMovements("21", response.getData());
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadStatement() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreAccountConnectorOrchestrator.readStatement(0, "31");
    }

    @Test(expected = BackendConnectorException.class)
    public void testListAccount() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreAccountConnectorOrchestrator.list("212", new ArrayList<>(), new ArrayList<>());
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadNoteBoucher() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreAccountConnectorOrchestrator.readNoteBoucher(212);
    }
}