package com.technisys.omnichannel.client.activities.preferences.environment;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.advanced.ModifyGroupsOfUserActivity;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.*;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Administration.class, AccessManagementHandlerFactory.class })
public class ModifyDefaultEnvironmentActivityTest {

    @Mock
    private Administration administrationMock;
    @Mock
    private AccessManagementHandler accessManagementHandlerMock;
    private static Request request;
    private ModifyDefaultEnvironmentActivity activity;

    private String idEnvironment = "14";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        activity = new ModifyDefaultEnvironmentActivity();
        request = new Request();
        request.setChannel("frontend");

        request.setClientIP("127.0.0.1");
        request.setDateTime(new Date());

        request.setIdEnvironment(9998);
        request.setIdUser("coreuser1");

        request.addCredential(new Credential("password", "password"));
    }

    @Test
    public void executeValid() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put(ModifyDefaultEnvironmentActivity.InParams.ID_ENVIRONMENT_TO_SET_DEFAULT, idEnvironment);
        request.setParameters(params);

        Mockito
                .doNothing()
                .when(accessManagementHandlerMock)
                .updateUserDefaultEnvironment(
                        Mockito.anyString(),
                        Mockito.anyInt());

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    @Test(expected = ActivityException.class)
    public void executeIOError() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put(ModifyDefaultEnvironmentActivity.InParams.ID_ENVIRONMENT_TO_SET_DEFAULT, idEnvironment);
        request.setParameters(params);

        Mockito
                .doThrow(IOException.class)
                .when(accessManagementHandlerMock)
                .updateUserDefaultEnvironment(
                        Mockito.anyString(),
                        Mockito.anyInt());

        activity.execute(request);
    }

    @Test
    public void validateValidRequestParameters() throws ActivityException, IOException {
        Map<String, Object> params = new HashMap<>();
        params.put(ModifyDefaultEnvironmentActivity.InParams.ID_ENVIRONMENT_TO_SET_DEFAULT, idEnvironment);
        request.setParameters(params);

        Map<String, String> result = activity.validate(request);

        assertEquals(0, result.size());
    }

    @Test
    public void validateInvalidRequestParameters() throws ActivityException, IOException {
        Map<String, Object> params = new HashMap<>();
        params.put(ModifyDefaultEnvironmentActivity.InParams.ID_ENVIRONMENT_TO_SET_DEFAULT, null);
        request.setParameters(params);

        Map<String, String> result = activity.validate(request);

        assertEquals(1, result.size());
    }
}