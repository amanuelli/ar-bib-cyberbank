package com.technisys.omnichannel.client.activities.session;


/*
 *
 * @author Marcelo Bruno
 */
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.domain.GeneralConditionDocument;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;

import static org.junit.Assert.assertEquals;

@PowerMockIgnore({ "org.apache.logging.log4j.*", "javax.management.*" })
@RunWith(PowerMockRunner.class)
@PrepareForTest({GeneralConditions.class})
public class ListAllGeneralConditionDocumentsTest {

    @Mock
    private GeneralConditions generalConditionsMock;
    private static Request request;
    private ListAllGeneralConditionDocuments activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        activity = new ListAllGeneralConditionDocuments();
        request = new Request();

        PowerMockito.mockStatic(GeneralConditions.class);
        Mockito.when(GeneralConditions.getInstance()).thenReturn(generalConditionsMock);

        request.setIdActivity("session.downloadGeneralConditions");
        request.setChannel("frontend");
    }

    @Test(expected = ActivityException.class)
    public void executeException() throws IOException, ActivityException {
        Mockito
                .when(generalConditionsMock.actualCondition(
                ))
                .thenReturn(1);
        Mockito
                .when(generalConditionsMock.listAllGeneralConditionDocuments(
                        Mockito.anyInt()
                ))
                .thenThrow(IOException.class);

        activity.execute(request);
    }

    @Test
    public void executeOk() throws IOException, ActivityException {
        Mockito
                .when(generalConditionsMock.actualCondition(
                ))
                .thenReturn(1);
        Mockito
                .when(generalConditionsMock.listAllGeneralConditionDocuments(
                        Mockito.anyInt()
                ))
                .thenReturn(new ArrayList<GeneralConditionDocument>());

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }
}
