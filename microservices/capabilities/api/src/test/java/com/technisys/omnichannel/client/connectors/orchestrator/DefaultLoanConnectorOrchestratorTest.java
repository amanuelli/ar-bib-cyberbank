/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankLoanConnector;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.StatementLoan;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Client;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import utils.TestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, CyberbankCoreConnector.class, RubiconCoreConnectorC.class, CyberbankLoanConnector.class})

public class DefaultLoanConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyString())).thenReturn("other");

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(RubiconCoreConnectorC.class);
        PowerMockito.mockStatic(CyberbankLoanConnector.class);

    }

    @Test(expected = BackendConnectorException.class)
    public void testListLoansCyberbankException() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        Mockito.when(CoreLoanConnectorOrchestrator.list("1",
                new ArrayList<Client>() {{add(new Client("1450", ""));}},
                Arrays.asList(new String[]{Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY})
        )).thenThrow(new BackendConnectorException("exception"));

    }

    @Test(expected = BackendConnectorException.class)
    public void testListInstallments() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CyberbankCoreConnectorResponse<List<StatementLoan>> installmentsResponse = new CyberbankCoreConnectorResponse<>(1);
        installmentsResponse.setData(new ArrayList<StatementLoan>() {{
            add(new StatementLoan());
            add(new StatementLoan());
        }});
        Mockito.when(CyberbankLoanConnector.listInstallments(any(Loan.class))).thenReturn(installmentsResponse);

        List<StatementLoan> statementLoans = CoreLoanConnectorOrchestrator.listInstallments("kjdfh374w384uefh84738wfue83", TestUtils.getValidProduct(), "a", 1, 10);

        Assert.assertTrue(statementLoans.size() > 0);
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadLoansCyberbankException() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        Mockito.when(CoreLoanConnectorOrchestrator.read("1", TestUtils.getValidLoan()
        )).thenThrow(new BackendConnectorException("exception"));

    }

    @Test(expected = BackendConnectorException.class)
    public void testLoanTypeCyberbankException() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        Mockito.when(CoreLoanConnectorOrchestrator.listLoanType()).thenThrow(new BackendConnectorException("exception"));
    }

    @Test(expected = BackendConnectorException.class)
    public void testRequestLoanCyberbankException() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        Account account = new Account();
        Amount amount = new Amount();
        Mockito.when(CoreLoanConnectorOrchestrator.request(account,"12", "12", amount, 121)).thenThrow(new BackendConnectorException("exception"));
    }

}