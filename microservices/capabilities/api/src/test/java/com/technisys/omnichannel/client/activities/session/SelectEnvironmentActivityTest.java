package com.technisys.omnichannel.client.activities.session;

import com.google.gson.Gson;
import com.technisys.omnichannel.client.activities.session.legacy.SelectEnvironmentActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.configuration.legacy.ConfigurationLegacy;
import com.technisys.omnichannel.core.domain.CalendarLoginRestriction;
import com.technisys.omnichannel.core.domain.IAccessRestriction;
import com.technisys.omnichannel.core.domain.IPAccessEnvironment;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.environments.EnvironmentHandler;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.DateUtils;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.*;

import static org.junit.Assert.*;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@Ignore("test temporarily ignored")
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigurationLegacy.class, EnvironmentHandler.class})
public class SelectEnvironmentActivityTest {

    CalendarLoginRestriction calendarLoginRest;
    SelectEnvironmentActivity activity;
    PaginatedList<CalendarLoginRestriction> restrictions;
    Request request;
    private int idEnvironment = 14;

    @Mock
    private Configuration configurationMock;

    @Mock
    private EnvironmentHandler environmentHandlerMock;

    @Before
    public void setUp() throws Exception {
        calendarLoginRest = new CalendarLoginRestriction();
        calendarLoginRest.setDays(21);
        calendarLoginRest.setStart(480); //8h = 8 * 60
        calendarLoginRest.setEnd(1020); //17h = 17 * 60
        calendarLoginRest.setTimeZone("America/Santiago");
        calendarLoginRest.setIdEnvironment(idEnvironment);

        restrictions = new PaginatedList<>();
        restrictions.addElement(calendarLoginRest);


        MockitoAnnotations.initMocks(this);

        mockStatic(ConfigurationFactory.class);
        Mockito
                .when(ConfigurationFactory.getInstance())
                .thenReturn(configurationMock);
        Mockito.when(configurationMock.getString(Configuration.PLATFORM, "core.default.lang")).thenReturn("en");

        activity = new SelectEnvironmentActivity();
        request = new Request();
        request.setIdUser("test");
        request.getParameters().put(SelectEnvironmentActivity.InParams.ENVIRONMENT, idEnvironment);
    }

    @Test
    public void ChangeNowDateToChileDate() {
        Date requestDate = new Date();
        ZoneId requestDateZoneId = ZoneId.systemDefault();
        String ZoneIdToChange = "America/Santiago";
        ZonedDateTime result = DateUtils.getDateInOtherTimeZone(ZoneIdToChange, requestDate, requestDateZoneId);

        assertEquals(result.getZone().getId(), ZoneIdToChange);
    }

    @Ignore("test temporarily ignored")
    @Test
    public void correctDayWrongTimeValidateCalendarAccess() throws ActivityException, IOException {
        //lunes, 6:30am(CO)/8:30am(CH)
        GregorianCalendar calendar = new GregorianCalendar(2020, Calendar.JANUARY, 27); // Monday
        calendar.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
        Date requestDate = calendar.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(requestDate);
        cal.set(Calendar.HOUR_OF_DAY, 5);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 25);
        cal.set(Calendar.MILLISECOND, 0);
        requestDate = cal.getTime();

        request.setValueDate(requestDate);

        mockStatic(EnvironmentHandler.class);
        Mockito.when(EnvironmentHandler.listCalendarLoginRestrictions(  Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyInt(),
                Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(restrictions);

        Map<String, String> validateResult = activity.validate(request);

        assertTrue(validateResult.containsKey(SelectEnvironmentActivity.InParams.ENVIRONMENT));
        assertEquals("core.calendarRestrictions.NotAuthorized", validateResult.get(SelectEnvironmentActivity.InParams.ENVIRONMENT));
    }

    @Ignore("test temporarily ignored")
    @Test
    public void wrongDayAndTimeValidateCalendarAccess() throws IOException, ActivityException{
        //martes, 2:30am(CO)/4:30am(CH)
        Date requestDate = new GregorianCalendar(2020, Calendar.JANUARY, 28).getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(requestDate);
        cal.set(Calendar.HOUR_OF_DAY, 2);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 25);
        cal.set(Calendar.MILLISECOND, 0);
        requestDate = cal.getTime();

        request.setValueDate(requestDate);

        mockStatic(EnvironmentHandler.class);
        Mockito.when(EnvironmentHandler.listCalendarLoginRestrictions(  Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyInt(),
                Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(restrictions);

        Map<String, String> validateResult = activity.validate(request);

        assertTrue(validateResult.containsKey(SelectEnvironmentActivity.InParams.ENVIRONMENT));
        assertEquals("core.calendarRestrictions.NotAuthorized", validateResult.get(SelectEnvironmentActivity.InParams.ENVIRONMENT));
    }

    @Test
    public void wrongDayCorrectTimeValidateCalendarAccess() throws IOException, ActivityException{
        //martes, 2:30am(CO)/4:30am(CH)
        Date requestDate = new GregorianCalendar(2020, Calendar.JANUARY, 28).getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(requestDate);
        cal.set(Calendar.HOUR_OF_DAY, 11);
        cal.set(Calendar.MINUTE, 30);
        cal.set(Calendar.SECOND, 25);
        cal.set(Calendar.MILLISECOND, 0);
        requestDate = cal.getTime();

        request.setValueDate(requestDate);

        mockStatic(EnvironmentHandler.class);
        Mockito.when(EnvironmentHandler.listCalendarLoginRestrictions(  Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyInt(),
                Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(restrictions);

        Map<String, String> validateResult = activity.validate(request);

        assertTrue(validateResult.containsKey(SelectEnvironmentActivity.InParams.ENVIRONMENT));
        assertEquals("core.calendarRestrictions.NotAuthorized", validateResult.get(SelectEnvironmentActivity.InParams.ENVIRONMENT));
    }

    @Test
    @Ignore("test temporarily ignored")
    public void correctDayWrongTimeByTimeZoneValidateCalendarAccess() throws IOException, ActivityException {
        //miercoles, 6:00am(CO)/8:00am(CH)
        GregorianCalendar calendar = new GregorianCalendar(2020, Calendar.JANUARY, 29); //Wednesday
        calendar.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
        Date requestDate = calendar.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(requestDate);
        cal.set(Calendar.HOUR_OF_DAY, 17);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 0);
        requestDate = cal.getTime();

        request.setValueDate(requestDate);

        mockStatic(EnvironmentHandler.class);
        Mockito.when(EnvironmentHandler.listCalendarLoginRestrictions(  Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyInt(),
                Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(restrictions);

        Map<String, String> validateResult = activity.validate(request);

        assertTrue(validateResult.containsKey(SelectEnvironmentActivity.InParams.ENVIRONMENT));
        assertEquals("core.calendarRestrictions.NotAuthorized", validateResult.get(SelectEnvironmentActivity.InParams.ENVIRONMENT));
    }

    @Test
    public void correctDayAndTimeValidateCalendarAccess() throws IOException, ActivityException {
        //miercoles, 6:00am(CO)/8:00am(CH)
        GregorianCalendar calendar = new GregorianCalendar(2020, Calendar.JANUARY, 29); //Wednesday
        calendar.setTimeZone(TimeZone.getTimeZone("America/Bogota"));
        Date requestDate = calendar.getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(requestDate);
        cal.set(Calendar.HOUR_OF_DAY, 13);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 0);
        requestDate = cal.getTime();

        request.setValueDate(requestDate);

        mockStatic(EnvironmentHandler.class);
        Mockito.when(EnvironmentHandler.listCalendarLoginRestrictions(  Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyInt(),
                Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(restrictions);

        Map<String, String> validateResult = activity.validate(request);

        assertFalse(validateResult.containsKey(SelectEnvironmentActivity.InParams.ENVIRONMENT));
    }

    @Test
    @Ignore("test temporarily ignored")
    public void correctTimeToEnvironmentButWrongToUserValidateCalendarAccess() throws IOException, ActivityException{

        CalendarLoginRestriction newcalendarLoginRest = new CalendarLoginRestriction();
        newcalendarLoginRest.setDays(21);
        newcalendarLoginRest.setStart(480); //8h = 8 * 60
        newcalendarLoginRest.setEnd(720); //12h = 12 * 60
        newcalendarLoginRest.setTimeZone("America/Santiago");
        newcalendarLoginRest.setIdEnvironment(idEnvironment);
        newcalendarLoginRest.setIdUser("123");

        PaginatedList<CalendarLoginRestriction> userRestrictions = new PaginatedList<>();
        userRestrictions.addElement(newcalendarLoginRest);

        Date requestDate = new GregorianCalendar(2020, Calendar.JANUARY, 27).getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(requestDate);
        cal.set(Calendar.HOUR_OF_DAY, 14);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 0);
        requestDate = cal.getTime();

        request.setValueDate(requestDate);

        mockStatic(EnvironmentHandler.class);
        Mockito.when(EnvironmentHandler.listCalendarLoginRestrictions(Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyInt(),
                Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(userRestrictions);

        Mockito.when(EnvironmentHandler.listCalendarLoginRestrictions(Mockito.anyInt(),
                Mockito.isNull(),
                Mockito.anyInt(),
                Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(restrictions);

        Map<String, String> validateResult = activity.validate(request);

        assertTrue(validateResult.containsKey(SelectEnvironmentActivity.InParams.ENVIRONMENT));
        assertEquals("core.calendarRestrictions.NotAuthorized", validateResult.get(SelectEnvironmentActivity.InParams.ENVIRONMENT));
    }

    @Test
    public void NoRestrictionsTestValidation() throws IOException, ActivityException{
        Date requestDate = new GregorianCalendar(2020, Calendar.JANUARY, 27).getTime();
        Calendar cal = Calendar.getInstance();
        cal.setTime(requestDate);
        cal.set(Calendar.HOUR_OF_DAY, 14);
        cal.set(Calendar.MINUTE, 00);
        cal.set(Calendar.SECOND, 00);
        cal.set(Calendar.MILLISECOND, 0);
        requestDate = cal.getTime();

        request.setValueDate(requestDate);

        mockStatic(EnvironmentHandler.class);
        Mockito.when(EnvironmentHandler.listCalendarLoginRestrictions(  Mockito.anyInt(),
                Mockito.anyString(),
                Mockito.anyInt(),
                Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(new PaginatedList<>());

        Mockito.when(EnvironmentHandler.listCalendarLoginRestrictions(  Mockito.anyInt(),
                Mockito.isNull(),
                Mockito.anyInt(),
                Mockito.anyInt(), Mockito.anyBoolean()))
                .thenReturn(new PaginatedList<>());

        Map<String, String> validateResult = activity.validate(request);

        assertFalse(validateResult.containsKey(SelectEnvironmentActivity.InParams.ENVIRONMENT));
    }

    @Test
    public void TestToSeeAllOfThePropertiesOfAParsedJsonWithAnInterface(){
        List<IAccessRestriction> listToTest = new ArrayList<>();
        CalendarLoginRestriction calTest = new CalendarLoginRestriction();
        calTest.setDays(21);
        calTest.setStart(480); //8h = 8 * 60
        calTest.setEnd(1020); //17h = 17 * 60
        calTest.setTimeZone("America/Santiago");
        calTest.setIdEnvironment(idEnvironment);
        listToTest.add(calTest);

        IPAccessEnvironment ipTest = new IPAccessEnvironment();
        ipTest.setIdEnvironment(idEnvironment);
        ipTest.setIdUser("123");
        ipTest.setIp("123456");
        listToTest.add(ipTest);

        String json = new Gson().toJson(listToTest);
        String expectedJson = "[{\"id\":0,\"idRestriction\":0,\"idEnvironment\":14,\"days\":21,\"start\":480,\"end\":1020,\"timeZone\":\"America/Santiago\",\"type\":\"Calendar\"},{\"ip\":\"123456\",\"idEnvironment\":14,\"idUser\":\"123\",\"type\":\"IP\"}]";

        assertEquals(expectedJson,json);
    }
}
