/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorO;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.domain.Address;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.domain.JobInformation;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.PhoneNumber;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, CyberbankCoreConnector.class, RubiconCoreConnectorO.class, RubiconCoreConnectorC.class})

public class RubiconCustomerConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(RubiconCoreConnectorO.class);
        PowerMockito.mockStatic(RubiconCoreConnectorC.class);

    }

    @Test
    public void testAdd() throws Exception {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorO.addClient("1",
                1,
                "Test",
                "Tes",
                "test@technisys.com",
                "777",
                "64645",
                "1111111",
                "CI",
                "CL",
                "Chilena",
                "CA",
                "M",
                "Test",
                "test",
                "28",
                "ggdfd 6vd",
                false
                )).thenReturn(0);

        JobInformation jobInformation = new JobInformation();
        Amount amount = new Amount();
        jobInformation.setIncome(amount);
        Address address = new Address();
        PhoneNumber mobilePhone = new PhoneNumber();

        String id = CoreCustomerConnectorOrchestrator.add("1123",
                "test",
                "Test",
                "Tes@hotmail.com",
                "777777",
                mobilePhone,
                "1111111",
                "CI",
                "CL",
                "Chilena",
                "CA",
                "M",
                "Test",
                false,
                "28",
                address,
                jobInformation
        );
        Assert.assertEquals("0", id);
    }


    @Test
    public void  read() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorC.readPerson("1", "CL", "CI", "1111111", "21")).thenReturn(new ClientUser());
        ClientUser clientUser =  CoreCustomerConnectorOrchestrator.read("1", "CL", "CI", "1111111", "21");
        Assert.assertNotNull(clientUser);
    }

    @Test
    public void readClientEnvironment() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorC.readPerson("1", "21", "CI", "2122", "21")).thenReturn(new ClientUser());
        ClientEnvironment clientEnvironment = CoreCustomerConnectorOrchestrator.readClientEnvironment("1", "21", "CI", "2122");
        Assert.assertNull(clientEnvironment);
    }

    @Test
    public void listClients() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorC.listClients("1212", "CL", "CI", "131332")).thenReturn(new ArrayList<>());
        List<ClientEnvironment> clientEnvironmentList  = CoreCustomerConnectorOrchestrator.listClients("1", "CL","CI", "131332");
        Assert.assertNotNull(clientEnvironmentList);
    }


}