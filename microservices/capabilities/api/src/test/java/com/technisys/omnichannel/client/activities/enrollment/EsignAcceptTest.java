/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;

/*
 *
 * @author Marcelo Bruno
 */

@PowerMockIgnore({ "org.apache.logging.log4j.*", "javax.management.*" })
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, GeneralConditions.class, ExchangeTokenHandler.class, AccessManagementHandlerFactory.class })
public class EsignAcceptTest {

    @Mock
    private Administration administrationMock;
    @Mock
    private AccessManagementHandler accessManagementHandlerMock;
    @Mock
    private GeneralConditions generalConditionsMock;
    private static Request request;
    private EsignAccept activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        activity = new EsignAccept();
        request = new Request();

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        PowerMockito.mockStatic(GeneralConditions.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);
        Mockito.when(GeneralConditions.getInstance()).thenReturn(generalConditionsMock);

        request.setIdActivity("enrollment.esignAccept");
        request.setChannel("frontend");
    }

    @Test
    public void executeValidateMail() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put(EsignAccept.InParams.USERMAIL, "");
        request.setParameters(params);
        Map<String, String> result = activity.validate(request);
        assertEquals("enrollment.esignAccept.usermail.empty", result.get(EsignAccept.InParams.USERMAIL));
    }

    @Test
    public void executeValidateEnv() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put(EsignAccept.InParams.USERMAIL, "a@a.com");
        params.put(EsignAccept.InParams.ID_ENVVIRONMENT, null);
        request.setParameters(params);
        Map<String, String> result = activity.validate(request);
        assertEquals("enrollment.esignAccept.idenvironment.empty", result.get(EsignAccept.InParams.ID_ENVVIRONMENT));
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionUserNotFound() throws IOException, ActivityException {
        Mockito
                .when(generalConditionsMock.actualCondition(
                ))
                .thenReturn(1);
        Mockito
                .when(accessManagementHandlerMock.getUserByEmail(
                        Mockito.anyString()
                ))
                .thenReturn(null);

        activity.execute(request);
    }

    @Test
    public void executeOk() throws Exception {
        String validEmail = "test@test.com";
        User user = new User();
        user.setIdUser("12345");
        user.setEmail(validEmail);
        EnvironmentUser envUser = new EnvironmentUser();
        envUser.setEmail(validEmail);
        Map<String, Object> params = new HashMap<>();
        params.put(EsignAccept.InParams.USERMAIL, validEmail);
        params.put(EsignAccept.InParams.ID_ENVVIRONMENT, 1);
        request.setParameters(params);
        Mockito
                .when(generalConditionsMock.actualCondition(
                ))
                .thenReturn(1);
        Mockito
                .when(accessManagementHandlerMock.getUserByEmail(
                        Mockito.anyString()
                ))
                .thenReturn(user);
        Mockito
                .when(administrationMock.readEnvironmentUserInfo(
                        Mockito.anyString(),
                        Mockito.anyInt()
                ))
                .thenReturn(envUser);
        Mockito
                .doNothing()
                .when(generalConditionsMock)
                .acceptGeneralConditions(
                        Mockito.anyInt(),
                        Mockito.anyString(),
                        Mockito.anyList());


        PowerMockito.mockStatic(ExchangeTokenHandler.class);
        PowerMockito.doNothing().when(ExchangeTokenHandler.class, "delete", anyString());

        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }
}
