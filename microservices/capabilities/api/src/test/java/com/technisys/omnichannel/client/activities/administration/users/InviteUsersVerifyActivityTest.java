package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.eq;


@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, ConfigurationFactory.class, RubiconCoreConnectorC.class})
public class InviteUsersVerifyActivityTest {

    @Mock
    private Administration administrationMock;

    @Mock
    private Configuration configurationMock;

    private Request request;
    private InviteUsersVerifyActivity activity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito
                .when(Administration.getInstance())
                .thenReturn(administrationMock);

        Mockito
                .when(ConfigurationFactory.getInstance())
                .thenReturn(configurationMock);


        request = new Request();
        activity = new InviteUsersVerifyActivity();
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionInvalidSchemeTest() throws IOException, ActivityException {
        request.setEnvironmentAdminScheme(Environment.ADMINISTRATION_SCHEME_SIMPLE);
        activity.execute(request);
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionNotAuthorizedTest() throws IOException, ActivityException {
        request.setEnvironmentAdminScheme(Environment.ADMINISTRATION_SCHEME_MEDIUM);
        Mockito.when(configurationMock.getBoolean(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn(false);

        activity.execute(request);
    }

}