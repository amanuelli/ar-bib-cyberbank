package com.technisys.omnichannel.client.activities.productrequest.creditcard;

import com.technisys.omnichannel.client.activities.productrequest.onboarding.Step5Activity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.EnvironmentSettings;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.technisys.omnichannel.client.activities.productrequest.creditcard.SendEmailVerificationCodeActivity.InParams.*;
import static com.technisys.omnichannel.core.configuration.Configuration.PLATFORM;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;

/*
 *
 * @author Marcelo Bruno
 */

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*", "jdk.internal.reflect.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({EnvironmentSettings.class, ConfigurationFactory.class})
public class SendEmailVerificationCodeActivityTest {

    private static Request request;
    private SendEmailVerificationCodeActivity activity;

    @Mock
    private EnvironmentSettings environmentSettingsMock;

    @Mock
    Configuration configurationMock;

    @Before
    public void setup() throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(EnvironmentSettings.class);
        PowerMockito.mockStatic(ConfigurationFactory.class);

        activity = new SendEmailVerificationCodeActivity();
        request = new Request();

        request.setIdActivity("productrequest.creditcard.sendVerificationCode");
        request.setChannel("frontend");


        when(EnvironmentSettings.getInstance()).thenReturn(environmentSettingsMock);

        when(environmentSettingsMock.getString(eq("core.configuration.componentFQN"), eq("com.technisys.omnichannel.core.configuration.legacy.ConfigurationLegacy")))
                .thenReturn("com.technisys.omnichannel.core.configuration.legacy.ConfigurationLegacy");

        when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        when(configurationMock.getDefaultString(eq(PLATFORM), eq("email.validationFormat"), anyString()))
                .thenReturn("^[a-zA-Z0-9+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$");
    }

    @Test
    public void testRequiredParams() throws ActivityException {
        String requiredString = "global.activity.required.param";

        Map<String, String> result = activity.validate(request);

        assertEquals(requiredString, result.get(EMAIL));
        assertEquals(requiredString, result.get(USERNAME));
        assertEquals(requiredString, result.get(CREDIT_CARD_ID));
    }

    @Test
    public void testInvalidParams() throws ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put(USERNAME, "User name test");
        params.put(EMAIL, "invalidEmail");
        params.put(CREDIT_CARD_ID, "invalidCard");
        request.setParameters(params);

        Map<String, String> result = activity.validate(request);

        assertEquals("creditCardRequest.emailForm.email.invalid", result.get(EMAIL));
        assertEquals("creditCardRequest.emailForm.creditCardId.invalid", result.get(CREDIT_CARD_ID));
    }

    @Test
    public void testValidateOk() throws IOException, ActivityException {
        String validCreditCard = (String) Step5Activity.getCreditCards().keySet().toArray()[0];
        Map<String, Object> params = new HashMap<>();
        params.put(USERNAME, "User name test");
        params.put(EMAIL, "a@a.com");
        params.put(CREDIT_CARD_ID, validCreditCard);
        request.setParameters(params);

        Map<String, String> result = activity.validate(request);

        assertEquals(0, result.size());
    }

}