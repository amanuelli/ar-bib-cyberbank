/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.cyberbank.domain.FundDestiny;
import com.technisys.omnichannel.client.domain.Bank;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Currency;
import com.technisys.omnichannel.core.utils.CacheUtils;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({
        ConfigurationFactory.class,
        SecurityUtils.class,
        CyberbankCoreConnector.class,
        CyberbankCoreRequest.class,
        CacheUtils.class
})
public class CyberbankGeneralServicesConnectorTest {

    @Mock
    private Configuration configurationMock;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM),eq("connector.cyberbank.core.template.request"), eq("/templates/connectors/cyberbank/request/"))).thenReturn("/templates/connectors/cyberbank/request/");


        PowerMockito.mockStatic(CyberbankCoreRequest.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CacheUtils.class);

    }

    @Test
    public void testListBanksOk() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListBanksOK());
        CyberbankCoreConnectorResponse<List<Bank>> response = CyberbankGeneralServicesConnector.listCorrespondentBanks(false);

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
    }

    @Test
    public void testListBankEmpty() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListBanksEmpty());
        CyberbankCoreConnectorResponse<List<Bank>> response = CyberbankGeneralServicesConnector.listCorrespondentBanks(false);

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
        Assert.assertEquals(0, response.getData().size());
    }

    @Test
    public void testListBankError() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListBanksWithError());
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);
        CyberbankCoreConnectorResponse<List<Bank>> response = CyberbankGeneralServicesConnector.listCorrespondentBanks(false);

        Assert.assertNotNull(response);
        Assert.assertNull(response.getData());
    }

    @Test
    public void testListCurrenciesOK() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListCurrenciesOK());
        CyberbankCoreConnectorResponse<List<Currency>> response = CyberbankGeneralServicesConnector.listCurrencies();

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
    }

    @Test
    public void testListCurrenciesWithCacheOK() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListCurrenciesOK());
        Mockito.when(CacheUtils.get(Mockito.anyString())).thenReturn(new ArrayList<>());
        CyberbankCoreConnectorResponse<List<Currency>> response = CyberbankGeneralServicesConnector.listCurrencies();

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
    }

    @Test(expected = CyberbankCoreConnectorException.class)
    public void testListCurrenciesEmpty() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectCurrency_Active\",\n" +
                "    \"transactionId\": \"massiveSelectCurrency_Active\",\n" +
                "    \"transactionVersion\": \"0.0\"\n" +
                "}"));
        CyberbankCoreConnectorResponse<List<Currency>> response = CyberbankGeneralServicesConnector.listCurrencies();

    }

    @Test
    public void testListCurrenciesWithError() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"massiveSelectCurrency_Active\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": null,\n" +
                "                \"description\": \"Parse error: NumberFormatException: Invalid Integer '${_branchId_}' for parameter ${_branchId_}\",\n" +
                "                \"id\": 0,\n" +
                "                \"codigoerror\": null\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}"));
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);
        CyberbankCoreConnectorResponse<List<Currency>> response = CyberbankGeneralServicesConnector.listCurrencies();

        Assert.assertNotNull(response);
        Assert.assertNull(response.getData());
    }

    @Test
    public void testGetCurrencieOK() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListCurrenciesOK());
        CyberbankCoreConnectorResponse<Currency> response = CyberbankGeneralServicesConnector.getCurrency("USD");

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
    }

    @Test(expected = CyberbankCoreConnectorException.class)
    public void testGetCurrencieError() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListCurrenciesOK());
        CyberbankCoreConnectorResponse<Currency> response = CyberbankGeneralServicesConnector.getCurrency("XXX");
    }

    @Test
    public void testlistFundDestiny() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListFundDestinyOK());
        CyberbankCoreConnectorResponse<List<FundDestiny>>  response = CyberbankGeneralServicesConnector.listFundDestiny("111");
        Assert.assertNotNull(response);
        Assert.assertNotNull(response.getData());
    }

    @Test(expected = NullPointerException.class)
    public void testlistFundDestinyError() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankGeneralServicesConnector.listFundDestiny("1111")).thenThrow(NullPointerException.class);

    }

    @Test
    public void testlistFundDestinyWithCacheOK() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListCurrenciesOK());
        Mockito.when(CacheUtils.get(Mockito.anyString())).thenReturn(new ArrayList<>());
        CyberbankCoreConnectorResponse<List<FundDestiny>> response = CyberbankGeneralServicesConnector.listFundDestiny("111");

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
    }

    @Test
    public void testlistFundDestinyWithError() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListFundDestinyWithError());
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);
        CyberbankCoreConnectorResponse<List<FundDestiny>> response = CyberbankGeneralServicesConnector.listFundDestiny("111");

        Assert.assertNotNull(response);
        Assert.assertNull(response.getData());
    }

    @Test
    public void testFundDestiny(){
        FundDestiny fundDestiny = new FundDestiny();
        Integer fundDestinyId = 31;
        String fundDestinyCode = "TEST";
        String fundDestinyDescription = "Description test";
        fundDestiny.setFundDestinyCode(fundDestinyCode);
        fundDestiny.setFundDestinyDescription(fundDestinyDescription);
        fundDestiny.setFundDestinyId(fundDestinyId);

        int hashCodeValue = fundDestiny.hashCode();

        FundDestiny fundDestiny2 = new FundDestiny();
        fundDestiny2.setFundDestinyId(2121);

        Assert.assertEquals(fundDestinyId, fundDestiny.getFundDestinyId() );
        Assert.assertEquals(fundDestinyCode, fundDestiny.getFundDestinyCode() );
        Assert.assertEquals(fundDestinyDescription, fundDestiny.getFundDestinyDescription() );
        Assert.assertEquals(hashCodeValue, fundDestiny.hashCode());
        Assert.assertFalse(fundDestiny.equals(null));
        Assert.assertFalse(fundDestiny.equals(fundDestiny2));



    }

    @Test
    public void testListFundDestinyEmpty() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListFundDestinyEmpty());
        CyberbankCoreConnectorResponse<List<FundDestiny>> response = CyberbankGeneralServicesConnector.listFundDestiny("111");

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
        Assert.assertEquals(0, response.getData().size());
    }

    private JSONObject getResponseListCurrenciesOK() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectCurrency_Active\",\n" +
                "    \"transactionId\": \"massiveSelectCurrency_Active\",\n" +
                "    \"transactionVersion\": \"0.0\",\n" +
                "    \"out.currency_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323175010003\",\n" +
                "                \"officialId\": null,\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"epayCode\": 2,\n" +
                "                \"currAcronym\": \"USD\",\n" +
                "                \"institution\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": \"20060101000000000\",\n" +
                "                    \"institutionId\": 1111,\n" +
                "                    \"shortDesc\": \"Technisys - Baseline\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"centralBankCode\": 1,\n" +
                "                    \"longDesc\": \"TECHNISYS - Linea Base\"\n" +
                "                },\n" +
                "                \"expressionUnit\": 3,\n" +
                "                \"currencySwift\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20080304000000000\",\n" +
                "                    \"stampDateTime\": \"20080304000000000\",\n" +
                "                    \"nemotecnico\": \"USD\",\n" +
                "                    \"currencySwiftId\": \"USD\",\n" +
                "                    \"shortDesc\": \"US DOLLAR\",\n" +
                "                    \"id\": 21,\n" +
                "                    \"longDesc\": \"US DOLLAR\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"USD\",\n" +
                "                \"currencyCodeId\": 2,\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"shortDesc\": \"Dollar\",\n" +
                "                \"id\": 21,\n" +
                "                \"stampUser\": {\n" +
                "                    \"statusDate\": \"20160512000000000\",\n" +
                "                    \"validityTerm\": null,\n" +
                "                    \"passwordExpireDated\": \"20190205000000000\",\n" +
                "                    \"userId\": \"ARODRIGUEZ\",\n" +
                "                    \"stampAdditional\": \"724\",\n" +
                "                    \"passwordAttemps\": 0,\n" +
                "                    \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "                    \"stampDateTime\": \"20160512151711762\",\n" +
                "                    \"lastAccessDate\": \"20200120160541000\",\n" +
                "                    \"logged\": false,\n" +
                "                    \"lastTrxDate\": \"20200120160541501\",\n" +
                "                    \"name\": \"RODRIGUEZ ALICIA\",\n" +
                "                    \"id\": 301190,\n" +
                "                    \"activeFlag\": true\n" +
                "                },\n" +
                "                \"longDesc\": \"Dolar\",\n" +
                "                \"status\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": null,\n" +
                "                    \"officialId\": null,\n" +
                "                    \"stampDateTime\": \"20180601102648000\",\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323163211189\",\n" +
                "                \"officialId\": null,\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"epayCode\": null,\n" +
                "                \"currAcronym\": \"EUR\",\n" +
                "                \"institution\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": \"20060101000000000\",\n" +
                "                    \"institutionId\": 1111,\n" +
                "                    \"shortDesc\": \"Technisys - Baseline\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"centralBankCode\": 1,\n" +
                "                    \"longDesc\": \"TECHNISYS - Linea Base\"\n" +
                "                },\n" +
                "                \"expressionUnit\": 1,\n" +
                "                \"currencySwift\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20080304000000000\",\n" +
                "                    \"stampDateTime\": \"20080304000000000\",\n" +
                "                    \"nemotecnico\": \"EUR\",\n" +
                "                    \"currencySwiftId\": \"EUR\",\n" +
                "                    \"shortDesc\": \"EURO\",\n" +
                "                    \"id\": 51,\n" +
                "                    \"longDesc\": \"EURO\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"EUR\",\n" +
                "                \"currencyCodeId\": 3,\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"shortDesc\": \"Euro\",\n" +
                "                \"id\": 22,\n" +
                "                \"stampUser\": {\n" +
                "                    \"statusDate\": \"20190821000000000\",\n" +
                "                    \"validityTerm\": null,\n" +
                "                    \"passwordExpireDated\": \"20220516000000000\",\n" +
                "                    \"userId\": \"NORTIZ\",\n" +
                "                    \"stampAdditional\": \"724\",\n" +
                "                    \"passwordAttemps\": 0,\n" +
                "                    \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "                    \"stampDateTime\": \"20190821152532473\",\n" +
                "                    \"lastAccessDate\": \"20200205100809000\",\n" +
                "                    \"logged\": true,\n" +
                "                    \"lastTrxDate\": \"20200205100809895\",\n" +
                "                    \"name\": \"NATALIA ORTIZ\",\n" +
                "                    \"id\": 516452,\n" +
                "                    \"activeFlag\": true\n" +
                "                },\n" +
                "                \"longDesc\": \"Euro\",\n" +
                "                \"status\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": null,\n" +
                "                    \"officialId\": null,\n" +
                "                    \"stampDateTime\": \"20180601102648000\",\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323175746034\",\n" +
                "                \"officialId\": null,\n" +
                "                \"dateFrom\": null,\n" +
                "                \"epayCode\": null,\n" +
                "                \"currAcronym\": \"LAK\",\n" +
                "                \"institution\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": \"20060101000000000\",\n" +
                "                    \"institutionId\": 1111,\n" +
                "                    \"shortDesc\": \"Technisys - Baseline\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"centralBankCode\": 1,\n" +
                "                    \"longDesc\": \"TECHNISYS - Linea Base\"\n" +
                "                },\n" +
                "                \"expressionUnit\": 1,\n" +
                "                \"currencySwift\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20080304000000000\",\n" +
                "                    \"stampDateTime\": \"20080304000000000\",\n" +
                "                    \"nemotecnico\": \"LAK\",\n" +
                "                    \"currencySwiftId\": \"LAK\",\n" +
                "                    \"shortDesc\": \"KIP\",\n" +
                "                    \"id\": 6,\n" +
                "                    \"longDesc\": \"KIP\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"LAK\",\n" +
                "                \"currencyCodeId\": 11,\n" +
                "                \"dateTo\": null,\n" +
                "                \"shortDesc\": \"LIBRA\",\n" +
                "                \"id\": 542518,\n" +
                "                \"stampUser\": {\n" +
                "                    \"statusDate\": \"20160512000000000\",\n" +
                "                    \"validityTerm\": null,\n" +
                "                    \"passwordExpireDated\": \"20190205000000000\",\n" +
                "                    \"userId\": \"ARODRIGUEZ\",\n" +
                "                    \"stampAdditional\": \"724\",\n" +
                "                    \"passwordAttemps\": 0,\n" +
                "                    \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "                    \"stampDateTime\": \"20160512151711762\",\n" +
                "                    \"lastAccessDate\": \"20200120160541000\",\n" +
                "                    \"logged\": false,\n" +
                "                    \"lastTrxDate\": \"20200120160541501\",\n" +
                "                    \"name\": \"RODRIGUEZ ALICIA\",\n" +
                "                    \"id\": 301190,\n" +
                "                    \"activeFlag\": true\n" +
                "                },\n" +
                "                \"longDesc\": \"LIBRA\",\n" +
                "                \"status\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": null,\n" +
                "                    \"officialId\": null,\n" +
                "                    \"stampDateTime\": \"20180601102648000\",\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323175644575\",\n" +
                "                \"officialId\": null,\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"epayCode\": null,\n" +
                "                \"currAcronym\": \"MNL\",\n" +
                "                \"institution\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": \"20060101000000000\",\n" +
                "                    \"institutionId\": 1111,\n" +
                "                    \"shortDesc\": \"Technisys - Baseline\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"centralBankCode\": 1,\n" +
                "                    \"longDesc\": \"TECHNISYS - Linea Base\"\n" +
                "                },\n" +
                "                \"expressionUnit\": 1,\n" +
                "                \"currencySwift\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20080304000000000\",\n" +
                "                    \"stampDateTime\": \"20080304000000000\",\n" +
                "                    \"nemotecnico\": \"LRD\",\n" +
                "                    \"currencySwiftId\": \"LRD\",\n" +
                "                    \"shortDesc\": \"LIBERIAN DOLLAR\",\n" +
                "                    \"id\": 99,\n" +
                "                    \"longDesc\": \"LIBERIAN DOLLAR\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"MNL\",\n" +
                "                \"currencyCodeId\": 1,\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"shortDesc\": \"Local currency\",\n" +
                "                \"id\": 1,\n" +
                "                \"stampUser\": {\n" +
                "                    \"statusDate\": \"20160512000000000\",\n" +
                "                    \"validityTerm\": null,\n" +
                "                    \"passwordExpireDated\": \"20190205000000000\",\n" +
                "                    \"userId\": \"ARODRIGUEZ\",\n" +
                "                    \"stampAdditional\": \"724\",\n" +
                "                    \"passwordAttemps\": 0,\n" +
                "                    \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "                    \"stampDateTime\": \"20160512151711762\",\n" +
                "                    \"lastAccessDate\": \"20200120160541000\",\n" +
                "                    \"logged\": false,\n" +
                "                    \"lastTrxDate\": \"20200120160541501\",\n" +
                "                    \"name\": \"RODRIGUEZ ALICIA\",\n" +
                "                    \"id\": 301190,\n" +
                "                    \"activeFlag\": true\n" +
                "                },\n" +
                "                \"longDesc\": \"Moneda Local\",\n" +
                "                \"status\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": null,\n" +
                "                    \"officialId\": null,\n" +
                "                    \"stampDateTime\": \"20180601102648000\",\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323020052504\",\n" +
                "                \"officialId\": null,\n" +
                "                \"dateFrom\": \"20190520000000000\",\n" +
                "                \"epayCode\": 123,\n" +
                "                \"currAcronym\": \"ADP\",\n" +
                "                \"institution\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": \"20060101000000000\",\n" +
                "                    \"institutionId\": 1111,\n" +
                "                    \"shortDesc\": \"Technisys - Baseline\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"centralBankCode\": 1,\n" +
                "                    \"longDesc\": \"TECHNISYS - Linea Base\"\n" +
                "                },\n" +
                "                \"expressionUnit\": 2,\n" +
                "                \"currencySwift\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20080304000000000\",\n" +
                "                    \"stampDateTime\": \"20080304000000000\",\n" +
                "                    \"nemotecnico\": \"ADP\",\n" +
                "                    \"currencySwiftId\": \"ADP\",\n" +
                "                    \"shortDesc\": \"ANDORRAN PESETA\",\n" +
                "                    \"id\": 126,\n" +
                "                    \"longDesc\": \"ANDORRAN PESETA\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"ADP\",\n" +
                "                \"currencyCodeId\": 10,\n" +
                "                \"dateTo\": \"20190522000000000\",\n" +
                "                \"shortDesc\": \"PESOS\",\n" +
                "                \"id\": 477658,\n" +
                "                \"stampUser\": {\n" +
                "                    \"statusDate\": \"20140225000000000\",\n" +
                "                    \"validityTerm\": 0,\n" +
                "                    \"passwordExpireDated\": \"20781231000000000\",\n" +
                "                    \"userId\": \"TECHNISYS\",\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"passwordAttemps\": 0,\n" +
                "                    \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "                    \"stampDateTime\": \"20140225135546197\",\n" +
                "                    \"lastAccessDate\": \"20200205120028000\",\n" +
                "                    \"logged\": true,\n" +
                "                    \"lastTrxDate\": \"20200205120028067\",\n" +
                "                    \"name\": \"TECHNISYS\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"activeFlag\": true\n" +
                "                },\n" +
                "                \"longDesc\": \"PESOS ARG\",\n" +
                "                \"status\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": null,\n" +
                "                    \"officialId\": null,\n" +
                "                    \"stampDateTime\": \"20180601102648000\",\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323191050562\",\n" +
                "                \"officialId\": null,\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"epayCode\": 5000,\n" +
                "                \"currAcronym\": \"PTS\",\n" +
                "                \"institution\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": \"20060101000000000\",\n" +
                "                    \"institutionId\": 1111,\n" +
                "                    \"shortDesc\": \"Technisys - Baseline\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"centralBankCode\": 1,\n" +
                "                    \"longDesc\": \"TECHNISYS - Linea Base\"\n" +
                "                },\n" +
                "                \"expressionUnit\": 1,\n" +
                "                \"currencySwift\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20080304000000000\",\n" +
                "                    \"stampDateTime\": \"20080304000000000\",\n" +
                "                    \"nemotecnico\": \"PTS\",\n" +
                "                    \"currencySwiftId\": \"PTS\",\n" +
                "                    \"shortDesc\": \"PUNTOS\",\n" +
                "                    \"id\": 196,\n" +
                "                    \"longDesc\": \"PUNTOS\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"PTS\",\n" +
                "                \"currencyCodeId\": 8,\n" +
                "                \"dateTo\": \"20090323000000000\",\n" +
                "                \"shortDesc\": \"points\",\n" +
                "                \"id\": 402601,\n" +
                "                \"stampUser\": {\n" +
                "                    \"statusDate\": \"20140225000000000\",\n" +
                "                    \"validityTerm\": 0,\n" +
                "                    \"passwordExpireDated\": \"20781231000000000\",\n" +
                "                    \"userId\": \"TECHNISYS\",\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"passwordAttemps\": 0,\n" +
                "                    \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "                    \"stampDateTime\": \"20140225135546197\",\n" +
                "                    \"lastAccessDate\": \"20200205120028000\",\n" +
                "                    \"logged\": true,\n" +
                "                    \"lastTrxDate\": \"20200205120028067\",\n" +
                "                    \"name\": \"TECHNISYS\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"activeFlag\": true\n" +
                "                },\n" +
                "                \"longDesc\": \"PUNTITOS\",\n" +
                "                \"status\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": null,\n" +
                "                    \"officialId\": null,\n" +
                "                    \"stampDateTime\": \"20180601102648000\",\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323145651188\",\n" +
                "                \"officialId\": null,\n" +
                "                \"dateFrom\": \"20090323000000000\",\n" +
                "                \"epayCode\": null,\n" +
                "                \"currAcronym\": \"BRL\",\n" +
                "                \"institution\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": \"20060101000000000\",\n" +
                "                    \"institutionId\": 1111,\n" +
                "                    \"shortDesc\": \"Technisys - Baseline\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"centralBankCode\": 1,\n" +
                "                    \"longDesc\": \"TECHNISYS - Linea Base\"\n" +
                "                },\n" +
                "                \"expressionUnit\": 1,\n" +
                "                \"currencySwift\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20080304000000000\",\n" +
                "                    \"stampDateTime\": \"20080304000000000\",\n" +
                "                    \"nemotecnico\": \"BRL\",\n" +
                "                    \"currencySwiftId\": \"BRL\",\n" +
                "                    \"shortDesc\": \"BRAZILIAN REAL\",\n" +
                "                    \"id\": 146,\n" +
                "                    \"longDesc\": \"BRAZILIAN REAL\"\n" +
                "                },\n" +
                "                \"nemotecnico\": \"BRL1\",\n" +
                "                \"currencyCodeId\": 9,\n" +
                "                \"dateTo\": \"20500323000000000\",\n" +
                "                \"shortDesc\": \"Real\",\n" +
                "                \"id\": 405209,\n" +
                "                \"stampUser\": {\n" +
                "                    \"statusDate\": \"20010101000000000\",\n" +
                "                    \"validityTerm\": 0,\n" +
                "                    \"passwordExpireDated\": \"20781231000000000\",\n" +
                "                    \"userId\": \"BASECERO\",\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"passwordAttemps\": 0,\n" +
                "                    \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "                    \"stampDateTime\": \"20010101000000000\",\n" +
                "                    \"lastAccessDate\": \"20190523100607000\",\n" +
                "                    \"logged\": false,\n" +
                "                    \"lastTrxDate\": \"20190523100607625\",\n" +
                "                    \"name\": \"BASECERO\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"activeFlag\": true\n" +
                "                },\n" +
                "                \"longDesc\": \"Real\",\n" +
                "                \"status\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": null,\n" +
                "                    \"officialId\": null,\n" +
                "                    \"stampDateTime\": \"20180601102648000\",\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Currency\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}");
    }

    public JSONObject getResponseListBanksWithError() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"massiveSelectBankCorrespond1ent\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": null,\n" +
                "                \"description\": \"Parse error: ParserConfigurationException: Service not found: massiveSelectBankCorrespond1ent[Tech]\",\n" +
                "                \"id\": 0,\n" +
                "                \"codigoerror\": null\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}");
    }

    public JSONObject getResponseListBanksEmpty() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectBankCorrespondent\",\n" +
                "    \"transactionId\": \"massiveSelectBankCorrespondent\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.bank_list\": {\n" +
                "        \"collectionEntityId\": \"Bank\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}");
    }

    public JSONObject getResponseListBanksOK() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectBankCorrespondent\",\n" +
                "    \"transactionId\": \"massiveSelectBankCorrespondent\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.bank_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"bankId\": 299,\n" +
                "                \"shortDesc\": \"Banco Comafi\",\n" +
                "                \"longDesc\": \"Banco Comafi S.A.\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"bankId\": 7,\n" +
                "                \"shortDesc\": \"Banco Galicia\",\n" +
                "                \"longDesc\": \"Banco de Galicia y Buenos Aires S.A.\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"bankId\": 256,\n" +
                "                \"shortDesc\": \"BANCO ITAU ARGENTINA S.A.\",\n" +
                "                \"longDesc\": \"BANCO ITAU ARGENTINA S.A.\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"bankId\": 111,\n" +
                "                \"shortDesc\": \"Banco Patagónia\",\n" +
                "                \"longDesc\": \"Banco Patagónia\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"bankId\": 500,\n" +
                "                \"shortDesc\": \"BANKITO\",\n" +
                "                \"longDesc\": \"BANKITO\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"bankId\": 499,\n" +
                "                \"shortDesc\": \"TechBank\",\n" +
                "                \"longDesc\": \"Tech Bank\"\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Bank\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}");
    }

    private JSONObject getResponseListFundDestinyOK(){
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectSubproduct_Loan_Fund_Destiny\",\n" +
                "    \"transactionId\": \"massiveSelectSubproduct_Loan_Fund_Destiny\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.subproduct_loan_fund_destiny_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"stampAdditional\": null,\n" +
                "                \"statusDate\": \"20090330113252210\",\n" +
                "                \"stampDateTime\": \"20190118194747992\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5102,\n" +
                "                    \"nemotecnico\": \"CONSTRUCCION\",\n" +
                "                    \"shortDesc\": \"Renovation or construction of buildings\",\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"subproduct\": {\n" +
                "                    \"product\": {\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10\n" +
                "                    },\n" +
                "                    \"subproductId\": \"09014\",\n" +
                "                    \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "                    \"id\": 453495\n" +
                "                },\n" +
                "                \"id\": 453559,\n" +
                "                \"stampUser\": {\n" +
                "                    \"name\": \"MAX ELLIOT TOVAR\"\n" +
                "                },\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"stampAdditional\": null,\n" +
                "                \"statusDate\": \"20090330113240051\",\n" +
                "                \"stampDateTime\": \"20190118194747994\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5101,\n" +
                "                    \"nemotecnico\": \"COMPRA DE BIENES\",\n" +
                "                    \"shortDesc\": \"ADQ. property\",\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"subproduct\": {\n" +
                "                    \"product\": {\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10\n" +
                "                    },\n" +
                "                    \"subproductId\": \"09014\",\n" +
                "                    \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "                    \"id\": 453495\n" +
                "                },\n" +
                "                \"id\": 453560,\n" +
                "                \"stampUser\": {\n" +
                "                    \"name\": \"MAX ELLIOT TOVAR\"\n" +
                "                },\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"stampAdditional\": null,\n" +
                "                \"statusDate\": \"20090330113226042\",\n" +
                "                \"stampDateTime\": \"20190118194747996\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5103,\n" +
                "                    \"nemotecnico\": \"AUTOMOTOR\",\n" +
                "                    \"shortDesc\": \"ADQ. motor for personal and / or family\",\n" +
                "                    \"id\": 4\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"subproduct\": {\n" +
                "                    \"product\": {\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10\n" +
                "                    },\n" +
                "                    \"subproductId\": \"09014\",\n" +
                "                    \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "                    \"id\": 453495\n" +
                "                },\n" +
                "                \"id\": 453561,\n" +
                "                \"stampUser\": {\n" +
                "                    \"name\": \"MAX ELLIOT TOVAR\"\n" +
                "                },\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"stampAdditional\": null,\n" +
                "                \"statusDate\": \"20090330113149909\",\n" +
                "                \"stampDateTime\": \"20190118194747998\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5104,\n" +
                "                    \"nemotecnico\": \"AUTOOTRO\",\n" +
                "                    \"shortDesc\": \"ADQ. motor for other uses\",\n" +
                "                    \"id\": 5\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"subproduct\": {\n" +
                "                    \"product\": {\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10\n" +
                "                    },\n" +
                "                    \"subproductId\": \"09014\",\n" +
                "                    \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "                    \"id\": 453495\n" +
                "                },\n" +
                "                \"id\": 453562,\n" +
                "                \"stampUser\": {\n" +
                "                    \"name\": \"MAX ELLIOT TOVAR\"\n" +
                "                },\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"stampAdditional\": null,\n" +
                "                \"statusDate\": \"20090330113232882\",\n" +
                "                \"stampDateTime\": \"20190118194748000\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5105,\n" +
                "                    \"nemotecnico\": \"ELECTRODOMESTICO\",\n" +
                "                    \"shortDesc\": \"ADQ. appliances and household items\",\n" +
                "                    \"id\": 6\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"subproduct\": {\n" +
                "                    \"product\": {\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10\n" +
                "                    },\n" +
                "                    \"subproductId\": \"09014\",\n" +
                "                    \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "                    \"id\": 453495\n" +
                "                },\n" +
                "                \"id\": 453563,\n" +
                "                \"stampUser\": {\n" +
                "                    \"name\": \"MAX ELLIOT TOVAR\"\n" +
                "                },\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"stampAdditional\": null,\n" +
                "                \"statusDate\": \"20090330113246074\",\n" +
                "                \"stampDateTime\": \"20190118194748004\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5106,\n" +
                "                    \"nemotecnico\": \"MAQUINARIAS\",\n" +
                "                    \"shortDesc\": \"Adq. maquinarias, equipos y herramientas\",\n" +
                "                    \"id\": 7\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"subproduct\": {\n" +
                "                    \"product\": {\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10\n" +
                "                    },\n" +
                "                    \"subproductId\": \"09014\",\n" +
                "                    \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "                    \"id\": 453495\n" +
                "                },\n" +
                "                \"id\": 453564,\n" +
                "                \"stampUser\": {\n" +
                "                    \"name\": \"MAX ELLIOT TOVAR\"\n" +
                "                },\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"stampAdditional\": null,\n" +
                "                \"statusDate\": \"20090330113304116\",\n" +
                "                \"stampDateTime\": \"20190118194748005\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5107,\n" +
                "                    \"nemotecnico\": \"TURISMO\",\n" +
                "                    \"shortDesc\": \"tourism\",\n" +
                "                    \"id\": 8\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"subproduct\": {\n" +
                "                    \"product\": {\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10\n" +
                "                    },\n" +
                "                    \"subproductId\": \"09014\",\n" +
                "                    \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "                    \"id\": 453495\n" +
                "                },\n" +
                "                \"id\": 453565,\n" +
                "                \"stampUser\": {\n" +
                "                    \"name\": \"MAX ELLIOT TOVAR\"\n" +
                "                },\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"stampAdditional\": null,\n" +
                "                \"statusDate\": \"20090330113257962\",\n" +
                "                \"stampDateTime\": \"20190118194748007\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5108,\n" +
                "                    \"nemotecnico\": \"REFINANCIACION\",\n" +
                "                    \"shortDesc\": \"Refinancing or cancellation of liabilities\",\n" +
                "                    \"id\": 9\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"subproduct\": {\n" +
                "                    \"product\": {\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10\n" +
                "                    },\n" +
                "                    \"subproductId\": \"09014\",\n" +
                "                    \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "                    \"id\": 453495\n" +
                "                },\n" +
                "                \"id\": 453566,\n" +
                "                \"stampUser\": {\n" +
                "                    \"name\": \"MAX ELLIOT TOVAR\"\n" +
                "                },\n" +
                "                \"dateFrom\": \"20010101000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"SubproductLoanFundDestiny\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}");
    }

    public JSONObject getResponseListFundDestinyWithError() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"massiveSelectSubproduct_Loan_Fund_Destiny\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": null,\n" +
                "                \"description\": \"Parse error: ParserConfigurationException: Service not found: massiveSelectSubproduct_Loan_Fund_Destiny[Tech]\",\n" +
                "                \"id\": 0,\n" +
                "                \"codigoerror\": null\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}");
    }

    public JSONObject getResponseListFundDestinyEmpty() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectSubproduct_Loan_Fund_Destiny\",\n" +
                "    \"transactionId\": \"massiveSelectSubproduct_Loan_Fund_Destiny\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.bank_list\": {\n" +
                "        \"collectionEntityId\": \"Bank\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}");
    }

}
