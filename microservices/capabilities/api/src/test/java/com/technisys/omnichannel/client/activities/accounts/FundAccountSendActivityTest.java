package com.technisys.omnichannel.client.activities.accounts;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.rubicon.core.accounts.WsAccountFundResponse;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Administration.class, ConfigurationFactory.class, RubiconCoreConnectorC.class })
public class FundAccountSendActivityTest {

    @Mock
    private Administration administrationMock;

    @Mock
    private Configuration configurationMock;

    private Request request;
    private FundAccountSendActivity activity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString()))
                .thenReturn("http://WEBSERVICES_URL:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(50);

        PowerMockito.mockStatic(RubiconCoreConnectorC.class);

        request = new Request();
        Map parameters = new HashMap();
        Map creditAccount = new HashMap();
        Amount amount = new Amount();
        amount.setQuantity(new Double(100));
        creditAccount.put("value", "dummyAccount");
        parameters.put(FundAccountSendActivity.InParams.CREDIT_ACCOUNT, creditAccount);
        parameters.put(FundAccountSendActivity.InParams.AMOUNT, amount);
        request.setParameters(parameters);

        activity = new FundAccountSendActivity();
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionTest() throws IOException, ActivityException {
        Mockito
                .when(administrationMock.readProduct(
                        Mockito.anyString(),
                        Mockito.anyInt()
                ))
                .thenThrow(IOException.class);

        activity.execute(request);
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionBackendTest() throws IOException, ActivityException, BackendConnectorException {
        Mockito
                .when(administrationMock.readProduct(
                        Mockito.anyString(),
                        Mockito.anyInt()
                ))
                .thenReturn(getValidProduct());

        Mockito
                .when(RubiconCoreConnectorC.fundAccount(
                        Mockito.anyString(),
                        Mockito.anyDouble(),
                        Mockito.anyString(),
                        Mockito.anyString()
                ))
                .thenThrow(BackendConnectorException.class);

        activity.execute(request);
    }

    @Test
    public void executeReturnOkTest() throws IOException, ActivityException, BackendConnectorException {
        Mockito
                .when(administrationMock.readProduct(
                        Mockito.anyString(),
                        Mockito.anyInt()
                ))
                .thenReturn(getValidProduct());

        Mockito
                .when(RubiconCoreConnectorC.fundAccount(
                        Mockito.anyString(),
                        Mockito.anyDouble(),
                        Mockito.anyString(),
                        Mockito.anyString()
                ))
                .thenReturn(new WsAccountFundResponse());

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    private Product getValidProduct() {
        Product product = new Product();
        product.setIdEnvironment(1);
        product.setIdProduct("123");
        product.setProductType(Constants.PRODUCT_CA_KEY);
        product.setLabel("qwer");
        product.setProductAlias("qwer");
        product.setExtraInfo(Constants.PRODUCT_CA_KEY+"|qwer|1234|4321");

        return product;
    }

}