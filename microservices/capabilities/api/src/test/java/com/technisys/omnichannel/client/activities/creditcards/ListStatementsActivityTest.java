package com.technisys.omnichannel.client.activities.creditcards;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.client.domain.StatementCreditCard;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Administration.class, ConfigurationFactory.class, RubiconCoreConnectorTC.class })
public class ListStatementsActivityTest {

    @Mock
    private Administration administrationMock;

    @Mock
    private Configuration configurationMock;

    private static Request request;
    private ListStatementsActivity activity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Product product = new Product();
        product.setIdProduct("12");
        product.setExtraInfo("extraInfo");
        Mockito.when(administrationMock.readProduct(Mockito.anyString(),Mockito.anyInt())).thenReturn(product);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM),eq("backend.webservices.url"),eq(""))).thenReturn("http://WEBSERVICES_URL:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(50);

        PowerMockito.mockStatic(RubiconCoreConnectorTC.class);

        activity = new ListStatementsActivity();
        request = new Request();
        request.setChannel("frontend");

        request.setClientIP("127.0.0.1");
        request.setDateTime(new Date());

        request.setIdEnvironment(9998);
        request.setIdUser("coreuser1");
        request.setIdTransaction("123456789");

        request.addCredential(new Credential("password", "password"));


    }

    @Test
    public void executeValid() throws IOException, ActivityException, BackendConnectorException {
        Map<String, Object> params = new HashMap<>();
        params.put(ListStatementsActivity.InParams.ID_CREDITCARD, "123");
        request.setParameters(params);

        CreditCard creditCard = new CreditCard();
        Mockito.when(RubiconCoreConnectorTC.readCreditCardDetails(Mockito.anyString(), Mockito.anyString(), Mockito.anyString())).thenReturn(creditCard);

        List<StatementCreditCard> results = new ArrayList<>();

        Mockito
                .when(RubiconCoreConnectorTC.listCreditCardStatements(Mockito.anyString(), Mockito.anyString(), Mockito.anyString(), Mockito.any(), Mockito.any(), Mockito.anyDouble(), Mockito.anyDouble(), Mockito.anyString(), Mockito.anyString(), Mockito.anyInt(), Mockito.anyInt()))
                .thenReturn(results);


        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionBackendTest() throws IOException, ActivityException, BackendConnectorException {
        Map<String, Object> params = new HashMap<>();
        params.put(ListStatementsActivity.InParams.ID_CREDITCARD, "123");
        request.setParameters(params);

        Mockito.when(RubiconCoreConnectorTC.readCreditCardDetails(Mockito.anyString(), Mockito.anyString(), Mockito.anyString()))
                .thenThrow(BackendConnectorException.class);

        activity.execute(request);
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionTest() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put(ListStatementsActivity.InParams.ID_CREDITCARD, "123");
        request.setParameters(params);
        Mockito.when(administrationMock.readProduct(Mockito.anyString(), Mockito.anyInt())).thenThrow(IOException.class);

        activity.execute(request);
    }
}