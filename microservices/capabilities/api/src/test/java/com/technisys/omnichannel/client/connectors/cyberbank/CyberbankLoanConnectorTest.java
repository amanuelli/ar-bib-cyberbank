/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.cyberbank.domain.ISOProduct;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.LoanType;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.ProductType;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.utils.CacheUtils;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;
import utils.TestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({"com.technisys.omnichannel.core.utils.SecurityUtils"})
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CyberbankCoreConnector.class, CyberbankCoreRequest.class, CacheUtils.class, SecurityUtils.class, ProductUtils.class, CyberbankConsolidateConnector.class})

public class CyberbankLoanConnectorTest {

    @Mock
    private Configuration configurationMock;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM),eq("connector.cyberbank.core.template.request"), eq("/templates/connectors/cyberbank/request/"))).thenReturn("/templates/connectors/cyberbank/request/");

        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CacheUtils.class);
        PowerMockito.mockStatic(ProductUtils.class);
        PowerMockito.mockStatic(CyberbankConsolidateConnector.class);
    }

    @Test
    public void testListLoans() throws CyberbankCoreConnectorException {

        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListLoansOk());
        CyberbankCoreConnectorResponse<List<Loan>> loans = CyberbankLoanConnector.list("1", new ArrayList<Client>() {{
            add(new Client("1450", ""));
        }});

        Assert.assertTrue(loans != null);
        Assert.assertTrue(loans.getData() != null);
        Assert.assertTrue(loans.getData().size() > 0);
    }

    @Test
    public void testListLoansWithNulls() throws CyberbankCoreConnectorException {

        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListLoansWithNulls());
        CyberbankCoreConnectorResponse<List<Loan>> loans = CyberbankLoanConnector.list("1", new ArrayList<Client>() {{
            add(new Client("1450", ""));
        }});

        Assert.assertTrue(loans != null);
        Assert.assertTrue(loans.getData() != null);
        Assert.assertTrue(loans.getData().size() > 0);
    }

    @Test
    public void testListLoansEmpty() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListLoansEmpty());
        CyberbankCoreConnectorResponse<List<Loan>> loans = CyberbankLoanConnector.list("1", new ArrayList<Client>() {{
            add(new Client("1450", ""));
        }});

        Assert.assertNotNull(loans);
        Assert.assertEquals(0, loans.getCode());
        Assert.assertNotNull(loans.getData());
        Assert.assertTrue(loans.getData().size() == 0);
    }

    @Test(expected = NullPointerException.class)
    public void testListLoansException() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankLoanConnector.list("1", null)).thenThrow(CyberbankCoreConnectorException.class);
    }

    @Test
    public void testRequest() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseRequestLoanOK());
        Mockito.when(CyberbankConsolidateConnector.getProductType(Mockito.anyString(), Mockito.anyString())).thenReturn(new ProductType("01000", 127, "10001", 4));
        Mockito.when(ProductUtils.getBackendID(Mockito.anyString())).thenReturn("121332");
        Account debitAccount = new Account();
        debitAccount.setExtraInfo("PA|1350|USD|09001|90|09000|1|1|1|0|0");

        CyberbankCoreConnectorResponse<Loan> cyberbankCoreConnectorResponse = CyberbankLoanConnector.request(debitAccount, "00100", "00001", "1000", new Amount("USD", 123.23), 12);
        Assert.assertTrue(cyberbankCoreConnectorResponse != null);
        Assert.assertTrue(cyberbankCoreConnectorResponse.getData() != null);
    }

    @Test(expected = NullPointerException.class)
    public void testRequestNoOk() throws CyberbankCoreConnectorException {
        Account debitAccount = new Account();
        Mockito.when(CyberbankLoanConnector.request(debitAccount, "00100", "00001", "1000", new Amount("USD", 123.23), 12)).thenThrow(NullPointerException.class);
    }

    @Test
    public void testListLoansWithError() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseRequestWithError());
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);

        CyberbankCoreConnectorResponse<List<Loan>> response = CyberbankLoanConnector.list("1", new ArrayList<Client>() {{
            add(new Client("1450", ""));
        }});

        Assert.assertNotNull(response);
        Assert.assertNull(response.getData());
    }

    @Test
    public void testRequestWithError() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseRequestWithError());
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);
        Mockito.when(CyberbankConsolidateConnector.getProductType(Mockito.anyString(), Mockito.anyString())).thenReturn(new ProductType("01000", 127, "10001", 4));
        Mockito.when(ProductUtils.getBackendID(Mockito.anyString())).thenReturn("121332");
        Account debitAccount = new Account();
        debitAccount.setExtraInfo("PA|1350|USD|09001|90|09000|1|1|1|0|0");
        CyberbankCoreConnectorResponse<Loan> response = CyberbankLoanConnector.request(debitAccount, "00100", "00001", "1000", new Amount("USD", 123.23), 12);

        Assert.assertNotNull(response);
        Assert.assertNull(response.getData());
    }

    @Test
    public void testListLoanType() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"massiveSelectProductBy_Iso_Product_Loan_Prestamo\",\"transactionId\":\"massiveSelectProductBy_Iso_Product_Loan_Prestamo\",\"transactionVersion\":\"1.0\",\"out.product_list\":{\"collection\":[{\"statusDate\":\"20090203123819187\",\"officialId\":\"1\",\"productId\":\"09001\",\"systemUser\":\"1\",\"dateFrom\":\"20010101000000000\",\"isoProduct\":{\"nemotecnico\":\"LOAN\",\"shortDesc\":\"Loans\",\"id\":11},\"productIsOwn\":true,\"stampAdditional\":null,\"institution\":{\"institutionId\":1111,\"shortDesc\":\"Technisys - Baseline\",\"id\":2},\"stampDateTime\":\"20091201123819203\",\"crmInfo\":null,\"dateTo\":\"20781231000000000\",\"shortDesc\":\"Loans\",\"id\":10,\"stampUser\":{\"name\":\"BASECERO\",\"id\":1,\"userId\":\"BASECERO\"},\"detailTableName\":\"LOAN\",\"longDesc\":\"Prestamos\",\"crmInfoExpiryDate\":null,\"status\":{\"statusId\":7,\"nemotecnico\":\"VIGENTE\",\"shortDesc\":\"Valid\",\"id\":7}}],\"collectionEntityId\":\"Product\",\"collectionEntityVersion\":\"1.0\",\"collectionEntityDataModel\":\"product.financials\"}}"));
        List<ISOProduct> loanISOProducts = CyberbankConsolidateConnector.listLoanISOProducts();
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListLoanTypeOk());
        CyberbankCoreConnectorResponse<List<LoanType>> cyberbankCoreConnectorResponse = CyberbankLoanConnector.listLoanType(loanISOProducts);
        Assert.assertTrue(cyberbankCoreConnectorResponse != null);
        Assert.assertTrue(cyberbankCoreConnectorResponse.getData() != null);
    }

    @Test
    public void testListLoanTypeWithError() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"massiveSelectProductBy_Iso_Product_Loan_Prestamo\",\"transactionId\":\"massiveSelectProductBy_Iso_Product_Loan_Prestamo\",\"transactionVersion\":\"1.0\",\"out.product_list\":{\"collection\":[{\"statusDate\":\"20090203123819187\",\"officialId\":\"1\",\"productId\":\"09001\",\"systemUser\":\"1\",\"dateFrom\":\"20010101000000000\",\"isoProduct\":{\"nemotecnico\":\"LOAN\",\"shortDesc\":\"Loans\",\"id\":11},\"productIsOwn\":true,\"stampAdditional\":null,\"institution\":{\"institutionId\":1111,\"shortDesc\":\"Technisys - Baseline\",\"id\":2},\"stampDateTime\":\"20091201123819203\",\"crmInfo\":null,\"dateTo\":\"20781231000000000\",\"shortDesc\":\"Loans\",\"id\":10,\"stampUser\":{\"name\":\"BASECERO\",\"id\":1,\"userId\":\"BASECERO\"},\"detailTableName\":\"LOAN\",\"longDesc\":\"Prestamos\",\"crmInfoExpiryDate\":null,\"status\":{\"statusId\":7,\"nemotecnico\":\"VIGENTE\",\"shortDesc\":\"Valid\",\"id\":7}}],\"collectionEntityId\":\"Product\",\"collectionEntityVersion\":\"1.0\",\"collectionEntityDataModel\":\"product.financials\"}}"));
        List<ISOProduct> loanISOProducts = CyberbankConsolidateConnector.listLoanISOProducts();
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseLoanTypetWithError());
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);
        CyberbankCoreConnectorResponse<List<LoanType>> cyberbankCoreConnectorResponse = CyberbankLoanConnector.listLoanType(loanISOProducts);

        Assert.assertNotNull(cyberbankCoreConnectorResponse);
        Assert.assertNull(cyberbankCoreConnectorResponse.getData());
    }

    @Test
    public void testListLoanTypeListEmpty() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"massiveSelectProductBy_Iso_Product_Loan_Prestamo\",\"transactionId\":\"massiveSelectProductBy_Iso_Product_Loan_Prestamo\",\"transactionVersion\":\"1.0\",\"out.product_list\":{\"collection\":[{\"statusDate\":\"20090203123819187\",\"officialId\":\"1\",\"productId\":\"09001\",\"systemUser\":\"1\",\"dateFrom\":\"20010101000000000\",\"isoProduct\":{\"nemotecnico\":\"LOAN\",\"shortDesc\":\"Loans\",\"id\":11},\"productIsOwn\":true,\"stampAdditional\":null,\"institution\":{\"institutionId\":1111,\"shortDesc\":\"Technisys - Baseline\",\"id\":2},\"stampDateTime\":\"20091201123819203\",\"crmInfo\":null,\"dateTo\":\"20781231000000000\",\"shortDesc\":\"Loans\",\"id\":10,\"stampUser\":{\"name\":\"BASECERO\",\"id\":1,\"userId\":\"BASECERO\"},\"detailTableName\":\"LOAN\",\"longDesc\":\"Prestamos\",\"crmInfoExpiryDate\":null,\"status\":{\"statusId\":7,\"nemotecnico\":\"VIGENTE\",\"shortDesc\":\"Valid\",\"id\":7}}],\"collectionEntityId\":\"Product\",\"collectionEntityVersion\":\"1.0\",\"collectionEntityDataModel\":\"product.financials\"}}"));
        List<ISOProduct> loanISOProducts = CyberbankConsolidateConnector.listLoanISOProducts();
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListLoanTypeListEmpty());
        CyberbankCoreConnectorResponse<List<LoanType>> cyberbankCoreConnectorResponse = CyberbankLoanConnector.listLoanType(loanISOProducts);

        Assert.assertNotNull(cyberbankCoreConnectorResponse);
        Assert.assertEquals(0, cyberbankCoreConnectorResponse.getCode());
        Assert.assertNotNull(cyberbankCoreConnectorResponse.getData());
        Assert.assertEquals(0, cyberbankCoreConnectorResponse.getData().size());
    }

    @Test
    public void testLoanType() {
        LoanType loanType = new LoanType();
        String loanTypeId = "31";
        String loanTypeDescription = "TEST";

        loanType.setLoanTypeId(loanTypeId);
        loanType.setLoanTypeDescription(loanTypeDescription);

        int hashCodeValue = loanType.hashCode();

        LoanType loanType2 = new LoanType();
        loanType2.setLoanTypeId("2121");

        Assert.assertEquals(loanTypeId, loanType.getLoanTypeId());
        Assert.assertEquals(loanTypeDescription, loanType.getLoanTypeDescription());
        Assert.assertEquals(hashCodeValue, loanType.hashCode());
        Assert.assertFalse(loanType.equals(null));
        Assert.assertFalse(loanType.equals(loanType2));
    }

    @Test
    public void testReadLoanOK() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseReadLoanOk());

        Product product = new Product();
        product.setExtraInfo("PA|1350|USD|09001|90|09000|1|1|1|0|0");
        CyberbankCoreConnectorResponse<Loan> cyberbankCoreConnectorResponse = CyberbankLoanConnector.read(product);
        Assert.assertTrue(cyberbankCoreConnectorResponse != null);
        Assert.assertTrue(cyberbankCoreConnectorResponse.getData() != null);

    }

    @Test(expected = CyberbankCoreConnectorException.class)
    public void testReadLoanWithException() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenThrow(CyberbankCoreConnectorException.class);

        Product product = new Product();
        product.setExtraInfo("PA|1350|USD|09001|90|09000|1|1|1|0|0");
        CyberbankCoreConnectorResponse<Loan> cyberbankCoreConnectorResponse = CyberbankLoanConnector.read(product);
    }

    @Test
    public void testReadLoansEmpty() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseReadLoanEmpty());

        Product product = new Product();
        product.setExtraInfo("PA|1350|USD|09001|90|09000|1|1|1|0|0");
        CyberbankCoreConnectorResponse<Loan> cyberbankCoreConnectorResponse = CyberbankLoanConnector.read(product);


        Assert.assertNotNull(cyberbankCoreConnectorResponse.getData());
    }

    @Test
    public void testReadLoanWithError() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseReadLoanWithError());
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);

        Product product = new Product();
        product.setExtraInfo("PA|1350|USD|09001|90|09000|1|1|1|0|0");
        CyberbankCoreConnectorResponse<Loan> cyberbankCoreConnectorResponse = CyberbankLoanConnector.read(product);
        Assert.assertNotNull(cyberbankCoreConnectorResponse);
        Assert.assertNull(cyberbankCoreConnectorResponse.getData());
    }


    @Test
    public void testListLoanInstallments() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListInstallments());
        CyberbankLoanConnector.listInstallments(new Loan(TestUtils.getValidProduct()));
    }

    @Test
    public void testListLoanInstallmentsEmpty() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListInstallmentsEmpty());
        CyberbankLoanConnector.listInstallments(new Loan(TestUtils.getValidProduct()));
    }

    @Test
    public void testListLoanInstallmentsWithoutInstallments() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListInstallmentsWithoutInstallments());
        CyberbankLoanConnector.listInstallments(new Loan(TestUtils.getValidProduct()));
    }

    @Test(expected = CyberbankCoreConnectorException.class)
    public void testListLoanInstallmentsWithException() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenThrow(CyberbankCoreConnectorException.class);
        CyberbankLoanConnector.listInstallments(new Loan(TestUtils.getValidProduct()));
    }

    @Test
    public void testListLoanInstallmentsWithError() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListInstallmentsWithError());
        Mockito.when(CyberbankLoanConnector.callHasError(Mockito.any(JSONObject.class))).thenReturn(true);
        CyberbankLoanConnector.listInstallments(new Loan(TestUtils.getValidProduct()));
    }

    private JSONObject getResponseListInstallmentsWithError() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"massiveSelectLoan_Installment\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": null,\n" +
                "                \"description\": \"Parse error: NumberFormatException: Invalid Integer '59328411223' for parameter 59328411223\",\n" +
                "                \"id\": 0,\n" +
                "                \"codigoerror\": null\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}");
    }

    private JSONObject getResponseListInstallments() {
        return new JSONObject("{\n" +
                "  \"project\": \"Tech\",\n" +
                "  \"responseCode\": \"massiveSelectLoan_Installment\",\n" +
                "  \"transactionId\": \"massiveSelectLoan_Installment\",\n" +
                "  \"transactionVersion\": \"1.0\",\n" +
                "  \"out.loan_installment_list\": {\n" +
                "    \"collection\": [\n" +
                "      {\n" +
                "        \"statusDate\": \"20090323144641978\",\n" +
                "        \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "        \"loan\": {\n" +
                "          \"accrualIntadjNextMonth\": \"0.00000\",\n" +
                "          \"statusDate\": \"20090323114240857\",\n" +
                "          \"totalCapSubsidyDue\": \"0.00000\",\n" +
                "          \"agreementActiveFlag\": false,\n" +
                "          \"automaticDbFlag\": true,\n" +
                "          \"accrualIntNextMonth\": \"0.00000\",\n" +
                "          \"firstCapitalInstallDate\": \"20090423000000000\",\n" +
                "          \"customerNumberDeclared\": 0,\n" +
                "          \"initialDisbursedAmount\": \"2000.00000\",\n" +
                "          \"currentIndex\": \"1.00000\",\n" +
                "          \"loCurrentPaymentAmount\": \"236.41000\",\n" +
                "          \"loOpeningDate\": null,\n" +
                "          \"loCrmInfoExpiryDate\": null,\n" +
                "          \"stampDateTime\": \"20200220114310973\",\n" +
                "          \"loDescription\": \"BIRCHE JUNIOR, ADEMIR \",\n" +
                "          \"loSettlementAmount\": \"0.00000\",\n" +
                "          \"accrualSubsidyIntNotAcco\": \"0.00000\",\n" +
                "          \"id\": 593284,\n" +
                "          \"fixedInstallmentDay\": 23,\n" +
                "          \"firstInterestInstallDate\": \"20090423000000000\",\n" +
                "          \"totalInterestAdjustement\": \"0.00000\",\n" +
                "          \"accrualIntNotAccount\": \"0.00000\",\n" +
                "          \"totalCapitalTeoricAdjust\": \"0.00000\",\n" +
                "          \"maxArrearsDaysUnpInstSubs\": 0,\n" +
                "          \"accrualAdjNextMonth\": \"99.26000\",\n" +
                "          \"dueInterestBalance\": \"0.00000\",\n" +
                "          \"originIndex\": \"7.17000\",\n" +
                "          \"pendingDisbursementFlag\": false,\n" +
                "          \"termGraceInstallment\": 0,\n" +
                "          \"subproduct\": {\n" +
                "            \"product\": {\n" +
                "              \"statusDate\": \"20090203123819187\",\n" +
                "              \"productId\": \"09001\",\n" +
                "              \"systemUser\": \"1\",\n" +
                "              \"dateFrom\": \"20010101000000000\",\n" +
                "              \"productIsOwn\": true,\n" +
                "              \"stampAdditional\": null,\n" +
                "              \"stampDateTime\": \"20091201123819203\",\n" +
                "              \"crmInfo\": null,\n" +
                "              \"dateTo\": \"20781231000000000\",\n" +
                "              \"shortDesc\": \"Loans\",\n" +
                "              \"id\": 10,\n" +
                "              \"detailTableName\": \"LOAN\",\n" +
                "              \"longDesc\": \"Prestamos\",\n" +
                "              \"crmInfoExpiryDate\": null\n" +
                "            },\n" +
                "            \"subproductId\": \"09000\",\n" +
                "            \"shortDesc\": \"1 LOAN RATE VAR FRANCES\",\n" +
                "            \"id\": 23\n" +
                "          },\n" +
                "          \"inputDate\": \"20090323000000000\",\n" +
                "          \"qtyGraceInstallment\": 0,\n" +
                "          \"qtyInterestInstallment\": 12,\n" +
                "          \"subsidyFlag\": false,\n" +
                "          \"lo2ndPaymentDebt\": \"71.00000\",\n" +
                "          \"loOtherAmountDueDate\": \"19691231210000000\",\n" +
                "          \"unpaidQuotSubsidyFlag\": false,\n" +
                "          \"specialRates\": true,\n" +
                "          \"interestSpreadValue\": \"5.00000\",\n" +
                "          \"linkToPackage\": false,\n" +
                "          \"status\": {\n" +
                "            \"stampAdditional\": null,\n" +
                "            \"statusDate\": null,\n" +
                "            \"stampDateTime\": \"20180601102648000\",\n" +
                "            \"statusId\": 7,\n" +
                "            \"nemotecnico\": \"VIGENTE\",\n" +
                "            \"shortDesc\": \"Valid\",\n" +
                "            \"id\": 7,\n" +
                "            \"longDesc\": \"Vigente\"\n" +
                "          },\n" +
                "          \"loPrincipalAmount\": \"0.00000\",\n" +
                "          \"updateDate\": \"20090322000000000\",\n" +
                "          \"approvalDate\": \"20090323000000000\",\n" +
                "          \"lo1stPaymentDebt\": \"0.00000\",\n" +
                "          \"loCrmInfo\": null,\n" +
                "          \"agreementFlag\": false,\n" +
                "          \"systemUser\": null,\n" +
                "          \"totalIntSubsidyDue\": \"0.00000\",\n" +
                "          \"totalOperationAmount\": \"2000.00000\",\n" +
                "          \"maxAmountInstallAuthorized\": \"0.00000\",\n" +
                "          \"subsidyRateValue\": null,\n" +
                "          \"qtyCapitalInstallment\": 12,\n" +
                "          \"interestPeriodVariable\": 1,\n" +
                "          \"accrualSuspTotalIntadj\": \"0.00000\",\n" +
                "          \"expiryDate\": \"20100323000000000\",\n" +
                "          \"stampAdditional\": \"409\",\n" +
                "          \"accrualAdjNotAccount\": \"0.00000\",\n" +
                "          \"accrualSuspendedTotalInt\": \"0.00000\",\n" +
                "          \"nextNewRateDate\": \"20090324000000000\",\n" +
                "          \"lastTxnDate\": \"20090323000000000\",\n" +
                "          \"totalCapitalizatedInterest\": \"0.00000\",\n" +
                "          \"operationId\": \"1350\",\n" +
                "          \"customerNumberReached\": 0,\n" +
                "          \"totalTermDays\": 365,\n" +
                "          \"loPaymentDueDate\": \"20090323000000000\",\n" +
                "          \"accrualSuspendedDate\": null,\n" +
                "          \"lo3thPaymentDebt\": \"100.00000\",\n" +
                "          \"totalDisbursedAmount\": \"2000.00000\",\n" +
                "          \"accrualIntadjNotAccount\": \"0.00000\",\n" +
                "          \"teoricDueCapitalBalance\": \"1880.30000\",\n" +
                "          \"dueCapitalBalance\": \"1880.30000\",\n" +
                "          \"crDate\": \"20090323000000000\",\n" +
                "          \"interestRateValue\": \"66.00000\",\n" +
                "          \"lastCapIntDate\": null,\n" +
                "          \"maxUnpaidSubsidyInstallment\": null,\n" +
                "          \"accrualSubsidyIntNextMonth\": \"0.00000\",\n" +
                "          \"beginDate\": \"20090323000000000\",\n" +
                "          \"nextInstallmentDate\": \"20090523000000000\",\n" +
                "          \"priceListProfile\": 11,\n" +
                "          \"loOtherAmount\": \"2000.00000\",\n" +
                "          \"closedDate\": \"20100323000000000\",\n" +
                "          \"capitalTerm\": 1,\n" +
                "          \"maxQtyArrearDays\": 0,\n" +
                "          \"totalCapitalAdjustement\": \"0.00000\",\n" +
                "          \"amountInstallment\": \"236.40889\",\n" +
                "          \"interestTerm\": 1,\n" +
                "          \"loFeesNotCharged\": \"2600.51000\",\n" +
                "          \"specialFees\": true\n" +
                "        },\n" +
                "        \"accrualSuspendedIntAjust\": \"0.00000\",\n" +
                "        \"installmentInterestDue\": \"116.71000\",\n" +
                "        \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "        \"installmentCapitalDue\": \"119.70000\",\n" +
                "        \"liInterest1\": \"0.00000\",\n" +
                "        \"systemUser\": null,\n" +
                "        \"liTax\": \"2000.00000\",\n" +
                "        \"currentIndex\": \"1.00000\",\n" +
                "        \"installmentType\": {\n" +
                "          \"instalmentTypeId\": null,\n" +
                "          \"stampAdditional\": null,\n" +
                "          \"statusDate\": \"20060101000000000\",\n" +
                "          \"stampDateTime\": null,\n" +
                "          \"nemotecnico\": \"AMBOS\",\n" +
                "          \"shortDesc\": \"Both of them\",\n" +
                "          \"id\": 3,\n" +
                "          \"longDesc\": \"Ambos\"\n" +
                "        },\n" +
                "        \"stampAdditional\": \"379\",\n" +
                "        \"refinancingInstallmentNmbr\": 0,\n" +
                "        \"liIndex\": null,\n" +
                "        \"stampDateTime\": \"20200220114240666\",\n" +
                "        \"paymentOrder\": 0,\n" +
                "        \"adjustAmortCap\": \"0.00000\",\n" +
                "        \"liAdditionalCosts\": \"0.00000\",\n" +
                "        \"id\": 593293,\n" +
                "        \"liAddData\": \"NORMAL\",\n" +
                "        \"refinancingFlag\": false,\n" +
                "        \"liInterest\": \"6.03000\",\n" +
                "        \"termDays\": 31,\n" +
                "        \"accrualIntSuspended\": \"0.00000\",\n" +
                "        \"liInstallmentNumber\": 1,\n" +
                "        \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "        \"paymentDueDate\": \"20090423000000000\",\n" +
                "        \"installmentCapitalBalance\": \"0.00000\",\n" +
                "        \"liStamps\": null,\n" +
                "        \"amountCapitalPercent\": \"0.00000\",\n" +
                "        \"capitalizedInterest\": \"0.00000\",\n" +
                "        \"liCapital\": \"99.26000\",\n" +
                "        \"liAmount\": \"71.00000\",\n" +
                "        \"liTax1\": \"116.71000\",\n" +
                "        \"stampUser\": {\n" +
                "          \"stampAdditional\": \"724\",\n" +
                "          \"statusDate\": \"20190102000000000\",\n" +
                "          \"stampDateTime\": \"20190102154150738\",\n" +
                "          \"lastTrxDate\": \"20200324164319455\",\n" +
                "          \"name\": \"PATRICIO BUTTA\",\n" +
                "          \"id\": 451214,\n" +
                "          \"userId\": \"PBUTTA\"\n" +
                "        },\n" +
                "        \"installmentInterestBalance\": \"116.71000\",\n" +
                "        \"paymentDate\": \"20090323000000000\",\n" +
                "        \"liInsurances\": \"0.00000\",\n" +
                "        \"liFees\": \"236.41000\",\n" +
                "        \"liTax2\": \"256.41000\",\n" +
                "        \"status\": {\n" +
                "          \"stampAdditional\": null,\n" +
                "          \"statusDate\": null,\n" +
                "          \"stampDateTime\": \"20180601102648000\",\n" +
                "          \"statusId\": 20,\n" +
                "          \"nemotecnico\": \"COBRADO\",\n" +
                "          \"shortDesc\": \"Paid\",\n" +
                "          \"id\": 20,\n" +
                "          \"longDesc\": \"Cobrada\"\n" +
                "        }\n" +
                "      },\n" +
                "      {\n" +
                "        \"statusDate\": \"20090323144641984\",\n" +
                "        \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "        \"loan\": {\n" +
                "          \"accrualIntadjNextMonth\": \"0.00000\",\n" +
                "          \"statusDate\": \"20090323114240857\",\n" +
                "          \"totalCapSubsidyDue\": \"0.00000\",\n" +
                "          \"agreementActiveFlag\": false,\n" +
                "          \"automaticDbFlag\": true,\n" +
                "          \"accrualIntNextMonth\": \"0.00000\",\n" +
                "          \"firstCapitalInstallDate\": \"20090423000000000\",\n" +
                "          \"customerNumberDeclared\": 0,\n" +
                "          \"initialDisbursedAmount\": \"2000.00000\",\n" +
                "          \"currentIndex\": \"1.00000\",\n" +
                "          \"loCurrentPaymentAmount\": \"236.41000\",\n" +
                "          \"loOpeningDate\": null,\n" +
                "          \"loCrmInfoExpiryDate\": null,\n" +
                "          \"stampDateTime\": \"20200220114310973\",\n" +
                "          \"loDescription\": \"BIRCHE JUNIOR, ADEMIR \",\n" +
                "          \"loSettlementAmount\": \"0.00000\",\n" +
                "          \"accrualSubsidyIntNotAcco\": \"0.00000\",\n" +
                "          \"id\": 593284,\n" +
                "          \"fixedInstallmentDay\": 23,\n" +
                "          \"firstInterestInstallDate\": \"20090423000000000\",\n" +
                "          \"totalInterestAdjustement\": \"0.00000\",\n" +
                "          \"accrualIntNotAccount\": \"0.00000\",\n" +
                "          \"totalCapitalTeoricAdjust\": \"0.00000\",\n" +
                "          \"maxArrearsDaysUnpInstSubs\": 0,\n" +
                "          \"accrualAdjNextMonth\": \"99.26000\",\n" +
                "          \"dueInterestBalance\": \"0.00000\",\n" +
                "          \"originIndex\": \"7.17000\",\n" +
                "          \"pendingDisbursementFlag\": false,\n" +
                "          \"termGraceInstallment\": 0,\n" +
                "          \"subproduct\": {\n" +
                "            \"product\": {\n" +
                "              \"statusDate\": \"20090203123819187\",\n" +
                "              \"productId\": \"09001\",\n" +
                "              \"systemUser\": \"1\",\n" +
                "              \"dateFrom\": \"20010101000000000\",\n" +
                "              \"productIsOwn\": true,\n" +
                "              \"stampAdditional\": null,\n" +
                "              \"stampDateTime\": \"20091201123819203\",\n" +
                "              \"crmInfo\": null,\n" +
                "              \"dateTo\": \"20781231000000000\",\n" +
                "              \"shortDesc\": \"Loans\",\n" +
                "              \"id\": 10,\n" +
                "              \"detailTableName\": \"LOAN\",\n" +
                "              \"longDesc\": \"Prestamos\",\n" +
                "              \"crmInfoExpiryDate\": null\n" +
                "            },\n" +
                "            \"subproductId\": \"09000\",\n" +
                "            \"shortDesc\": \"1 LOAN RATE VAR FRANCES\",\n" +
                "            \"id\": 23\n" +
                "          },\n" +
                "          \"inputDate\": \"20090323000000000\",\n" +
                "          \"qtyGraceInstallment\": 0,\n" +
                "          \"qtyInterestInstallment\": 12,\n" +
                "          \"subsidyFlag\": false,\n" +
                "          \"lo2ndPaymentDebt\": \"71.00000\",\n" +
                "          \"loOtherAmountDueDate\": \"19691231210000000\",\n" +
                "          \"unpaidQuotSubsidyFlag\": false,\n" +
                "          \"specialRates\": true,\n" +
                "          \"interestSpreadValue\": \"5.00000\",\n" +
                "          \"linkToPackage\": false,\n" +
                "          \"status\": {\n" +
                "            \"stampAdditional\": null,\n" +
                "            \"statusDate\": null,\n" +
                "            \"stampDateTime\": \"20180601102648000\",\n" +
                "            \"statusId\": 7,\n" +
                "            \"nemotecnico\": \"VIGENTE\",\n" +
                "            \"shortDesc\": \"Valid\",\n" +
                "            \"id\": 7,\n" +
                "            \"longDesc\": \"Vigente\"\n" +
                "          },\n" +
                "          \"loPrincipalAmount\": \"0.00000\",\n" +
                "          \"updateDate\": \"20090322000000000\",\n" +
                "          \"approvalDate\": \"20090323000000000\",\n" +
                "          \"lo1stPaymentDebt\": \"0.00000\",\n" +
                "          \"loCrmInfo\": null,\n" +
                "          \"agreementFlag\": false,\n" +
                "          \"systemUser\": null,\n" +
                "          \"totalIntSubsidyDue\": \"0.00000\",\n" +
                "          \"totalOperationAmount\": \"2000.00000\",\n" +
                "          \"maxAmountInstallAuthorized\": \"0.00000\",\n" +
                "          \"subsidyRateValue\": null,\n" +
                "          \"qtyCapitalInstallment\": 12,\n" +
                "          \"interestPeriodVariable\": 1,\n" +
                "          \"accrualSuspTotalIntadj\": \"0.00000\",\n" +
                "          \"expiryDate\": \"20100323000000000\",\n" +
                "          \"stampAdditional\": \"409\",\n" +
                "          \"accrualAdjNotAccount\": \"0.00000\",\n" +
                "          \"accrualSuspendedTotalInt\": \"0.00000\",\n" +
                "          \"nextNewRateDate\": \"20090324000000000\",\n" +
                "          \"lastTxnDate\": \"20090323000000000\",\n" +
                "          \"totalCapitalizatedInterest\": \"0.00000\",\n" +
                "          \"operationId\": \"1350\",\n" +
                "          \"customerNumberReached\": 0,\n" +
                "          \"totalTermDays\": 365,\n" +
                "          \"loPaymentDueDate\": \"20090323000000000\",\n" +
                "          \"accrualSuspendedDate\": null,\n" +
                "          \"lo3thPaymentDebt\": \"100.00000\",\n" +
                "          \"totalDisbursedAmount\": \"2000.00000\",\n" +
                "          \"accrualIntadjNotAccount\": \"0.00000\",\n" +
                "          \"teoricDueCapitalBalance\": \"1880.30000\",\n" +
                "          \"dueCapitalBalance\": \"1880.30000\",\n" +
                "          \"crDate\": \"20090323000000000\",\n" +
                "          \"interestRateValue\": \"66.00000\",\n" +
                "          \"lastCapIntDate\": null,\n" +
                "          \"maxUnpaidSubsidyInstallment\": null,\n" +
                "          \"accrualSubsidyIntNextMonth\": \"0.00000\",\n" +
                "          \"beginDate\": \"20090323000000000\",\n" +
                "          \"nextInstallmentDate\": \"20090523000000000\",\n" +
                "          \"priceListProfile\": 11,\n" +
                "          \"loOtherAmount\": \"2000.00000\",\n" +
                "          \"closedDate\": \"20100323000000000\",\n" +
                "          \"capitalTerm\": 1,\n" +
                "          \"maxQtyArrearDays\": 0,\n" +
                "          \"totalCapitalAdjustement\": \"0.00000\",\n" +
                "          \"amountInstallment\": \"236.40889\",\n" +
                "          \"interestTerm\": 1,\n" +
                "          \"loFeesNotCharged\": \"2600.51000\",\n" +
                "          \"specialFees\": true\n" +
                "        },\n" +
                "        \"accrualSuspendedIntAjust\": \"236.41000\",\n" +
                "        \"installmentInterestDue\": \"109.73000\",\n" +
                "        \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "        \"installmentCapitalDue\": \"126.68000\",\n" +
                "        \"liInterest1\": \"20.00000\",\n" +
                "        \"systemUser\": null,\n" +
                "        \"liTax\": \"1880.30000\",\n" +
                "        \"currentIndex\": \"1.00000\",\n" +
                "        \"installmentType\": {\n" +
                "          \"instalmentTypeId\": null,\n" +
                "          \"stampAdditional\": null,\n" +
                "          \"statusDate\": \"20060101000000000\",\n" +
                "          \"stampDateTime\": null,\n" +
                "          \"nemotecnico\": \"AMBOS\",\n" +
                "          \"shortDesc\": \"Both of them\",\n" +
                "          \"id\": 3,\n" +
                "          \"longDesc\": \"Ambos\"\n" +
                "        },\n" +
                "        \"stampAdditional\": \"406\",\n" +
                "        \"refinancingInstallmentNmbr\": 0,\n" +
                "        \"liIndex\": null,\n" +
                "        \"stampDateTime\": \"20200122144641985\",\n" +
                "        \"paymentOrder\": 0,\n" +
                "        \"adjustAmortCap\": \"0.00000\",\n" +
                "        \"liAdditionalCosts\": \"0.00000\",\n" +
                "        \"id\": 593294,\n" +
                "        \"liAddData\": \"NORMAL\",\n" +
                "        \"refinancingFlag\": false,\n" +
                "        \"liInterest\": \"6.03000\",\n" +
                "        \"termDays\": 30,\n" +
                "        \"accrualIntSuspended\": \"0.00000\",\n" +
                "        \"liInstallmentNumber\": 2,\n" +
                "        \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "        \"paymentDueDate\": \"20090523000000000\",\n" +
                "        \"installmentCapitalBalance\": \"126.68000\",\n" +
                "        \"liStamps\": null,\n" +
                "        \"amountCapitalPercent\": \"0.00000\",\n" +
                "        \"capitalizedInterest\": \"0.00000\",\n" +
                "        \"liCapital\": \"99.26000\",\n" +
                "        \"liAmount\": \"71.00000\",\n" +
                "        \"liTax1\": \"0.00000\",\n" +
                "        \"stampUser\": {\n" +
                "          \"stampAdditional\": null,\n" +
                "          \"statusDate\": \"20140225000000000\",\n" +
                "          \"stampDateTime\": \"20140225135546197\",\n" +
                "          \"lastTrxDate\": \"20200324172730280\",\n" +
                "          \"name\": \"TECHNISYS\",\n" +
                "          \"id\": 2,\n" +
                "          \"userId\": \"TECHNISYS\"\n" +
                "        },\n" +
                "        \"installmentInterestBalance\": \"0.00000\",\n" +
                "        \"paymentDate\": null,\n" +
                "        \"liInsurances\": \"0.00000\",\n" +
                "        \"liFees\": \"236.41000\",\n" +
                "        \"liTax2\": \"256.41000\",\n" +
                "        \"status\": {\n" +
                "          \"stampAdditional\": null,\n" +
                "          \"statusDate\": null,\n" +
                "          \"stampDateTime\": \"20180601102648000\",\n" +
                "          \"statusId\": 7,\n" +
                "          \"nemotecnico\": \"VIGENTE\",\n" +
                "          \"shortDesc\": \"Valid\",\n" +
                "          \"id\": 7,\n" +
                "          \"longDesc\": \"Vigente\"\n" +
                "        }\n" +
                "      }\n" +
                "    ],\n" +
                "    \"collectionEntityId\": \"LoanInstallment\",\n" +
                "    \"collectionEntityVersion\": \"1.0\",\n" +
                "    \"collectionEntityDataModel\": \"product.financials\"\n" +
                "  }\n" +
                "}");
    }

    private JSONObject getResponseListInstallmentsEmpty() {
        return new JSONObject("{\n" +
                "  \"project\": \"Tech\",\n" +
                "  \"responseCode\": \"massiveSelectLoan_Installment\",\n" +
                "  \"transactionId\": \"massiveSelectLoan_Installment\",\n" +
                "  \"transactionVersion\": \"1.0\",\n" +
                "  \"out.loan_installment_list\": {\n" +
                "    \"collectionEntityId\": \"LoanInstallment\",\n" +
                "    \"collectionEntityVersion\": \"1.0\",\n" +
                "    \"collectionEntityDataModel\": \"product.financials\"\n" +
                "  }\n" +
                "}");
    }

    private JSONObject getResponseListInstallmentsWithoutInstallments() {
        return new JSONObject("{\n" +
                "  \"project\": \"Tech\",\n" +
                "  \"responseCode\": \"massiveSelectLoan_Installment\",\n" +
                "  \"transactionId\": \"massiveSelectLoan_Installment\",\n" +
                "  \"transactionVersion\": \"1.0\"" +
                "}");
    }

    public JSONObject getResponseListLoansOk() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectLoanFrom_Customer_Operation\",\n" +
                "    \"transactionId\": \"massiveSelectLoanFrom_Customer_Operation\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.loan_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"accrualIntadjNextMonth\": \"0.00000\",\n" +
                "                \"statusDate\": \"20090323114240857\",\n" +
                "                \"crAuthorizedCustomer\": {\n" +
                "                    \"statusDate\": \"20090323172353141\",\n" +
                "                    \"firstName1\": \"ADEMIR\",\n" +
                "                    \"confidentialFlag\": false,\n" +
                "                    \"firstName2\": null,\n" +
                "                    \"purgeDate\": null,\n" +
                "                    \"seniorityBank\": \"20090323000000000\",\n" +
                "                    \"nameShort\": null,\n" +
                "                    \"expiryDateCrmInfo\": \"20090323000000000\",\n" +
                "                    \"stampAdditional\": \"993305451\",\n" +
                "                    \"stampDateTime\": \"20191007173018246\",\n" +
                "                    \"crmInfo\": null,\n" +
                "                    \"customerId\": \"1450\",\n" +
                "                    \"lastName1\": \"BIRCHE\",\n" +
                "                    \"name\": \"BIRCHE JUNIOR, ADEMIR \",\n" +
                "                    \"lastName2\": \"JUNIOR\",\n" +
                "                    \"id\": 537611,\n" +
                "                    \"additionDate\": \"20090323000000000\"\n" +
                "                },\n" +
                "                \"totalCapSubsidyDue\": \"0.00000\",\n" +
                "                \"automaticDbFlag\": true,\n" +
                "                \"accrualIntNextMonth\": \"0.00000\",\n" +
                "                \"firstCapitalInstallDate\": \"20090423000000000\",\n" +
                "                \"customerNumberDeclared\": 0,\n" +
                "                \"branch\": {\n" +
                "                    \"attentionPointQuantity\": null,\n" +
                "                    \"stampAdditional\": \"655\",\n" +
                "                    \"statusDate\": \"20090323152351117\",\n" +
                "                    \"branchId\": 1,\n" +
                "                    \"meanTime\": null,\n" +
                "                    \"stampDateTime\": \"20190808152351117\",\n" +
                "                    \"shortDesc\": \"Main Branch\",\n" +
                "                    \"id\": 0,\n" +
                "                    \"longDesc\": \"Main Branch\"\n" +
                "                },\n" +
                "                \"loOpeningDate\": null,\n" +
                "                \"loSettlementAmount\": \"0.00000\",\n" +
                "                \"accrualSubsidyIntNotAcco\": \"0.00000\",\n" +
                "                \"id\": 593284,\n" +
                "                \"termTypeGraceInstallment\": {\n" +
                "                    \"stampAdditional\": \"1011\",\n" +
                "                    \"statusDate\": \"20181112000000000\",\n" +
                "                    \"stampDateTime\": \"20181112120546743\",\n" +
                "                    \"nemotecnico\": \"DAYS\",\n" +
                "                    \"termTypeId\": 1,\n" +
                "                    \"shortDesc\": \"Day\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"Dia\"\n" +
                "                },\n" +
                "                \"fixedInstallmentDay\": 23,\n" +
                "                \"algorithm\": {\n" +
                "                    \"nemotecnico\": \"ALGORITHM1\",\n" +
                "                    \"shortDesc\": \"Cap + Int\",\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"totalInterestAdjustement\": \"0.00000\",\n" +
                "                \"accrualDailyFlag\": true,\n" +
                "                \"maxArrearsDaysUnpInstSubs\": 0,\n" +
                "                \"pendingDisbursementFlag\": false,\n" +
                "                \"termGraceInstallment\": 0,\n" +
                "                \"accrualType\": {\n" +
                "                    \"nemotecnico\": \"LINEAL\",\n" +
                "                    \"shortDesc\": \"Linear\",\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"interestRateType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"rateTypeId\": 1,\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"TNAV\",\n" +
                "                    \"shortDesc\": \"TNAV\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"Tasa nominal anual vencida\"\n" +
                "                },\n" +
                "                \"inputDate\": \"20090323000000000\",\n" +
                "                \"qtyInterestInstallment\": 12,\n" +
                "                \"subsidyFlag\": false,\n" +
                "                \"lo2ndPaymentDebt\": \"71.00000\",\n" +
                "                \"interestSpreadValue\": \"5.00000\",\n" +
                "                \"linkToPackage\": false,\n" +
                "                \"status\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": null,\n" +
                "                    \"stampDateTime\": \"20180601102648000\",\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                },\n" +
                "                \"loPrincipalAmount\": \"0.00000\",\n" +
                "                \"updateDate\": \"20090322000000000\",\n" +
                "                \"lo1stPaymentDebt\": \"0.00000\",\n" +
                "                \"agreementFlag\": false,\n" +
                "                \"termType\": {\n" +
                "                    \"stampAdditional\": \"1011\",\n" +
                "                    \"statusDate\": \"20181112000000000\",\n" +
                "                    \"stampDateTime\": \"20181112120555667\",\n" +
                "                    \"nemotecnico\": \"MONTHS\",\n" +
                "                    \"termTypeId\": 2,\n" +
                "                    \"shortDesc\": \"Month\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"longDesc\": \"Mes\"\n" +
                "                },\n" +
                "                \"maxAmountInstallAuthorized\": \"0.00000\",\n" +
                "                \"subsidyRateValue\": null,\n" +
                "                \"qtyCapitalInstallment\": 12,\n" +
                "                \"accrualSuspTotalIntadj\": \"0.00000\",\n" +
                "                \"stampAdditional\": \"409\",\n" +
                "                \"accrualAdjNotAccount\": \"0.00000\",\n" +
                "                \"accrualSuspendedTotalInt\": \"0.00000\",\n" +
                "                \"nextNewRateDate\": \"20090324000000000\",\n" +
                "                \"lastTxnDate\": \"20090323000000000\",\n" +
                "                \"totalCapitalizatedInterest\": \"0.00000\",\n" +
                "                \"operationId\": \"1350\",\n" +
                "                \"vatCategory\": {\n" +
                "                    \"stampAdditional\": \"FUERA DE USO\",\n" +
                "                    \"statusDate\": \"20170310000000000\",\n" +
                "                    \"stampDateTime\": \"20170310000000000\",\n" +
                "                    \"nemotecnico\": \"NO APLICA\",\n" +
                "                    \"shortDesc\": \"VAT no charge\",\n" +
                "                    \"id\": 0,\n" +
                "                    \"vatCategoryId\": 0,\n" +
                "                    \"longDesc\": \"IVA no Responsable\"\n" +
                "                },\n" +
                "                \"totalTermDays\": 365,\n" +
                "                \"interestRate\": {\n" +
                "                    \"stampAdditional\": \"816\",\n" +
                "                    \"statusDate\": \"20090323130029948\",\n" +
                "                    \"stampDateTime\": \"20191011130029948\",\n" +
                "                    \"dateTo\": \"20781231000000000\",\n" +
                "                    \"rateCodeId\": 8492,\n" +
                "                    \"systemUser\": null,\n" +
                "                    \"shortDesc\": \"PIZARRA LOANS\",\n" +
                "                    \"id\": 453613,\n" +
                "                    \"dateFrom\": \"19691231000000000\",\n" +
                "                    \"longDesc\": \"PIZARRA LOANS MAX\"\n" +
                "                },\n" +
                "                \"accrualSuspendedDate\": null,\n" +
                "                \"lo3thPaymentDebt\": \"100.00000\",\n" +
                "                \"totalDisbursedAmount\": \"2000.00000\",\n" +
                "                \"teoricDueCapitalBalance\": \"1880.30000\",\n" +
                "                \"dueCapitalBalance\": \"1880.30000\",\n" +
                "                \"accountingBranch\": {\n" +
                "                    \"attentionPointQuantity\": null,\n" +
                "                    \"stampAdditional\": \"655\",\n" +
                "                    \"statusDate\": \"20090323091156163\",\n" +
                "                    \"branchId\": 502,\n" +
                "                    \"meanTime\": null,\n" +
                "                    \"stampDateTime\": \"20170511091156163\",\n" +
                "                    \"shortDesc\": \"MICROCENTRO\",\n" +
                "                    \"id\": 24,\n" +
                "                    \"longDesc\": \"MICROCENTRO\"\n" +
                "                },\n" +
                "                \"lastCapIntDate\": null,\n" +
                "                \"maxUnpaidSubsidyInstallment\": null,\n" +
                "                \"accrualSubsidyIntNextMonth\": \"0.00000\",\n" +
                "                \"nextInstallmentDate\": \"20090523000000000\",\n" +
                "                \"totalPaymentDecress\": \"0.00000\",\n" +
                "                \"loOtherAmount\": \"2000.00000\",\n" +
                "                \"closedDate\": \"20100323000000000\",\n" +
                "                \"capitalTerm\": 1,\n" +
                "                \"stampUser\": {\n" +
                "                    \"stampAdditional\": \"724\",\n" +
                "                    \"statusDate\": \"20190102000000000\",\n" +
                "                    \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "                    \"stampDateTime\": \"20190102154150738\",\n" +
                "                    \"lastTrxDate\": \"20200312122927647\",\n" +
                "                    \"name\": \"PATRICIO BUTTA\",\n" +
                "                    \"id\": 451214,\n" +
                "                    \"userId\": \"PBUTTA\"\n" +
                "                },\n" +
                "                \"totalCapitalAdjustement\": \"0.00000\",\n" +
                "                \"interestSpreadType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"PUNTOS\",\n" +
                "                    \"shortDesc\": \"points\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"interestSpreadTypeId\": 1,\n" +
                "                    \"longDesc\": \"Suma puntos\"\n" +
                "                },\n" +
                "                \"currencyCode\": {\n" +
                "                    \"stampAdditional\": \"1806\",\n" +
                "                    \"statusDate\": \"20090323175644575\",\n" +
                "                    \"currAcronym\": \"MNL\",\n" +
                "                    \"stampDateTime\": \"20191022175644575\",\n" +
                "                    \"expressionUnit\": 1,\n" +
                "                    \"currencyCodeId\": 1,\n" +
                "                    \"dateTo\": \"20781231000000000\",\n" +
                "                    \"shortDesc\": \"Local currency\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"dateFrom\": \"20010101000000000\",\n" +
                "                    \"longDesc\": \"Moneda Local\"\n" +
                "                },\n" +
                "                \"interestPeriodType\": {\n" +
                "                    \"stampAdditional\": \"1011\",\n" +
                "                    \"statusDate\": \"20181112000000000\",\n" +
                "                    \"stampDateTime\": \"20181112120546743\",\n" +
                "                    \"nemotecnico\": \"DAYS\",\n" +
                "                    \"termTypeId\": 1,\n" +
                "                    \"shortDesc\": \"Day\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"Dia\"\n" +
                "                },\n" +
                "                \"agreementActiveFlag\": false,\n" +
                "                \"accrualBaseCalc\": {\n" +
                "                    \"nemotecnico\": \"365_DIAS\",\n" +
                "                    \"shortDesc\": \"365 days\",\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"initialDisbursedAmount\": \"2000.00000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"loCurrentPaymentAmount\": \"236.41000\",\n" +
                "                \"loCrmInfoExpiryDate\": null,\n" +
                "                \"stampDateTime\": \"20200220114310973\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5104,\n" +
                "                    \"stampAdditional\": \"145\",\n" +
                "                    \"statusDate\": \"20160503000000000\",\n" +
                "                    \"stampDateTime\": \"20160503090406188\",\n" +
                "                    \"nemotecnico\": \"AUTOOTRO\",\n" +
                "                    \"shortDesc\": \"ADQ. motor for other uses\",\n" +
                "                    \"id\": 5,\n" +
                "                    \"longDesc\": \"Adquisición de automotores para otros usos\"\n" +
                "                },\n" +
                "                \"loDescription\": \"BIRCHE JUNIOR, ADEMIR \",\n" +
                "                \"firstInterestInstallDate\": \"20090423000000000\",\n" +
                "                \"businessSector\": {\n" +
                "                    \"stampAdditional\": \"1011\",\n" +
                "                    \"statusDate\": \"20180126000000000\",\n" +
                "                    \"stampDateTime\": \"20180126091639250\",\n" +
                "                    \"businessSectorId\": 1003,\n" +
                "                    \"nemotecnico\": \"PRIV NO FINANC\",\n" +
                "                    \"shortDesc\": \"In the country Priv No Financ\",\n" +
                "                    \"id\": 500003,\n" +
                "                    \"longDesc\": \"\\tResidentes en el país - Sector privado no financiero\\t\"\n" +
                "                },\n" +
                "                \"accrualIntNotAccount\": \"0.00000\",\n" +
                "                \"totalCapitalTeoricAdjust\": \"0.00000\",\n" +
                "                \"accrualAdjNextMonth\": \"99.26000\",\n" +
                "                \"dueInterestBalance\": \"0.00000\",\n" +
                "                \"originIndex\": \"7.17000\",\n" +
                "                \"subproduct\": {\n" +
                "                    \"specialCrInterestFlag\": false,\n" +
                "                    \"statusDate\": \"20090323171053815\",\n" +
                "                    \"isConditionUpdateAllowed\": false,\n" +
                "                    \"systemUser\": null,\n" +
                "                    \"isPackagesAllowed\": false,\n" +
                "                    \"isForcedBusinessSector\": false,\n" +
                "                    \"stampAdditional\": \"930\",\n" +
                "                    \"automNbrAcctFlag\": true,\n" +
                "                    \"stampDateTime\": \"20200304171053815\",\n" +
                "                    \"subproductId\": \"09000\",\n" +
                "                    \"isInterbranchAllowed\": true,\n" +
                "                    \"sttIssueFlag\": false,\n" +
                "                    \"crmInfo\": null,\n" +
                "                    \"id\": 23,\n" +
                "                    \"warrantyReqFlag\": true,\n" +
                "                    \"valuedDateTxnMaxDays\": null,\n" +
                "                    \"product\": {\n" +
                "                        \"productId\": \"09001\",\n" +
                "                        \"crmInfo\": null,\n" +
                "                        \"dateTo\": \"20781231000000000\",\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10,\n" +
                "                        \"dateFrom\": \"20010101000000000\",\n" +
                "                        \"detailTableName\": \"LOAN\",\n" +
                "                        \"longDesc\": \"Prestamos\",\n" +
                "                        \"crmInfoExpiryDate\": null,\n" +
                "                        \"productIsOwn\": true\n" +
                "                    },\n" +
                "                    \"batchProcessFlag\": false,\n" +
                "                    \"sttDayNbr\": null,\n" +
                "                    \"isForAllBranches\": false,\n" +
                "                    \"specialDbInterestFlag\": false,\n" +
                "                    \"valuedDateTxnAllowed\": true,\n" +
                "                    \"dateFrom\": \"20080102000000000\",\n" +
                "                    \"sttMaxLines\": 5,\n" +
                "                    \"sttMonthNbr\": 6,\n" +
                "                    \"valuedDateIntMaxDays\": null,\n" +
                "                    \"specialComissionFlag\": false,\n" +
                "                    \"dateTo\": \"20781231000000000\",\n" +
                "                    \"sttInterval\": null,\n" +
                "                    \"shortDesc\": \"1 LOAN RATE VAR FRANCES\",\n" +
                "                    \"acceptOtherCurrency\": false,\n" +
                "                    \"currencyCode\": {\n" +
                "                        \"currencyCodeId\": 1,\n" +
                "                        \"shortDesc\": \"Local currency\",\n" +
                "                        \"id\": 1\n" +
                "                    },\n" +
                "                    \"longDesc\": \"PRESTAMO TASA VDA FRANCES\",\n" +
                "                    \"crmInfoExpiryDate\": null\n" +
                "                },\n" +
                "                \"qtyGraceInstallment\": 0,\n" +
                "                \"loOtherAmountDueDate\": \"19691231210000000\",\n" +
                "                \"unpaidQuotSubsidyFlag\": false,\n" +
                "                \"specialRates\": true,\n" +
                "                \"approvalDate\": \"20090323000000000\",\n" +
                "                \"loanType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"PRESTAMO\",\n" +
                "                    \"shortDesc\": \"Loan\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"loanTypeId\": 1,\n" +
                "                    \"longDesc\": \"Prestamo\"\n" +
                "                },\n" +
                "                \"loCrmInfo\": null,\n" +
                "                \"systemUser\": null,\n" +
                "                \"totalIntSubsidyDue\": \"0.00000\",\n" +
                "                \"totalOperationAmount\": \"2000.00000\",\n" +
                "                \"fundOrigin\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20010101000000000\",\n" +
                "                    \"fundOriginId\": 3,\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"GENERICO\",\n" +
                "                    \"shortDesc\": \"Generic\",\n" +
                "                    \"id\": 3,\n" +
                "                    \"longDesc\": \"Generico\"\n" +
                "                },\n" +
                "                \"interestPeriodVariable\": 1,\n" +
                "                \"expiryDate\": \"20100323000000000\",\n" +
                "                \"amortizationType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"FRANCES\",\n" +
                "                    \"shortDesc\": \"French\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"amortizationTypeId\": 1,\n" +
                "                    \"longDesc\": \"Frances\"\n" +
                "                },\n" +
                "                \"customerNumberReached\": 0,\n" +
                "                \"loPaymentDueDate\": \"20090323000000000\",\n" +
                "                \"accrualIntadjNotAccount\": \"0.00000\",\n" +
                "                \"crDate\": \"20090323000000000\",\n" +
                "                \"interestRateValue\": \"66.00000\",\n" +
                "                \"situationCode\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"NORMAL\",\n" +
                "                    \"shortDesc\": \"Normal\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"SITUACION Y CUMPLIMIENTO NORMAL\",\n" +
                "                    \"situationCodeId\": 1\n" +
                "                },\n" +
                "                \"interestVariableType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"interestVariableTypeId\": 2,\n" +
                "                    \"nemotecnico\": \"VARIABLE\",\n" +
                "                    \"shortDesc\": \"Variable\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"longDesc\": \"Variable\"\n" +
                "                },\n" +
                "                \"beginDate\": \"20090323000000000\",\n" +
                "                \"priceListProfile\": 11,\n" +
                "                \"maxQtyArrearDays\": 0,\n" +
                "                \"amountInstallment\": \"236.40889\",\n" +
                "                \"interestTerm\": 1,\n" +
                "                \"loFeesNotCharged\": \"2600.51000\",\n" +
                "                \"specialFees\": true\n" +
                "            },\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Loan\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}"
        );
    }

    public JSONObject getResponseListLoansWithNulls() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectLoanFrom_Customer_Operation\",\n" +
                "    \"transactionId\": \"massiveSelectLoanFrom_Customer_Operation\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.loan_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"accrualIntadjNextMonth\": \"0.00000\",\n" +
                "                \"statusDate\": \"20090323114240857\",\n" +
                "                \"crAuthorizedCustomer\": {\n" +
                "                    \"statusDate\": \"20090323172353141\",\n" +
                "                    \"firstName1\": \"ADEMIR\",\n" +
                "                    \"confidentialFlag\": false,\n" +
                "                    \"firstName2\": null,\n" +
                "                    \"purgeDate\": null,\n" +
                "                    \"seniorityBank\": \"20090323000000000\",\n" +
                "                    \"nameShort\": null,\n" +
                "                    \"expiryDateCrmInfo\": \"20090323000000000\",\n" +
                "                    \"stampAdditional\": \"993305451\",\n" +
                "                    \"stampDateTime\": \"20191007173018246\",\n" +
                "                    \"crmInfo\": null,\n" +
                "                    \"customerId\": \"1450\",\n" +
                "                    \"lastName1\": \"BIRCHE\",\n" +
                "                    \"name\": \"BIRCHE JUNIOR, ADEMIR \",\n" +
                "                    \"lastName2\": \"JUNIOR\",\n" +
                "                    \"id\": 537611,\n" +
                "                    \"additionDate\": \"20090323000000000\"\n" +
                "                },\n" +
                "                \"totalCapSubsidyDue\": \"0.00000\",\n" +
                "                \"automaticDbFlag\": true,\n" +
                "                \"accrualIntNextMonth\": \"0.00000\",\n" +
                "                \"firstCapitalInstallDate\": \"20090423000000000\",\n" +
                "                \"customerNumberDeclared\": 0,\n" +
                "                \"branch\": {\n" +
                "                    \"attentionPointQuantity\": null,\n" +
                "                    \"stampAdditional\": \"655\",\n" +
                "                    \"statusDate\": \"20090323152351117\",\n" +
                "                    \"branchId\": 1,\n" +
                "                    \"meanTime\": null,\n" +
                "                    \"stampDateTime\": \"20190808152351117\",\n" +
                "                    \"shortDesc\": \"Main Branch\",\n" +
                "                    \"id\": 0,\n" +
                "                    \"longDesc\": \"Main Branch\"\n" +
                "                },\n" +
                "                \"loOpeningDate\": null,\n" +
                "                \"loSettlementAmount\": \"0.00000\",\n" +
                "                \"accrualSubsidyIntNotAcco\": \"0.00000\",\n" +
                "                \"id\": 593284,\n" +
                "                \"termTypeGraceInstallment\": {\n" +
                "                    \"stampAdditional\": \"1011\",\n" +
                "                    \"statusDate\": \"20181112000000000\",\n" +
                "                    \"stampDateTime\": \"20181112120546743\",\n" +
                "                    \"nemotecnico\": \"DAYS\",\n" +
                "                    \"termTypeId\": 1,\n" +
                "                    \"shortDesc\": \"Day\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"Dia\"\n" +
                "                },\n" +
                "                \"fixedInstallmentDay\": 23,\n" +
                "                \"algorithm\": {\n" +
                "                    \"nemotecnico\": \"ALGORITHM1\",\n" +
                "                    \"shortDesc\": \"Cap + Int\",\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"totalInterestAdjustement\": \"0.00000\",\n" +
                "                \"accrualDailyFlag\": true,\n" +
                "                \"maxArrearsDaysUnpInstSubs\": 0,\n" +
                "                \"pendingDisbursementFlag\": false,\n" +
                "                \"termGraceInstallment\": 0,\n" +
                "                \"accrualType\": {\n" +
                "                    \"nemotecnico\": \"LINEAL\",\n" +
                "                    \"shortDesc\": \"Linear\",\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"interestRateType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"rateTypeId\": 1,\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"TNAV\",\n" +
                "                    \"shortDesc\": \"TNAV\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"Tasa nominal anual vencida\"\n" +
                "                },\n" +
                "                \"qtyInterestInstallment\": 12,\n" +
                "                \"subsidyFlag\": false,\n" +
                "                \"lo2ndPaymentDebt\": \"71.00000\",\n" +
                "                \"interestSpreadValue\": \"5.00000\",\n" +
                "                \"linkToPackage\": false,\n" +
                "                \"status\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": null,\n" +
                "                    \"stampDateTime\": \"20180601102648000\",\n" +
                "                    \"statusId\": 7,\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                },\n" +
                "                \"loPrincipalAmount\": \"0.00000\",\n" +
                "                \"updateDate\": \"20090322000000000\",\n" +
                "                \"lo1stPaymentDebt\": \"0.00000\",\n" +
                "                \"agreementFlag\": false,\n" +
                "                \"termType\": {\n" +
                "                    \"stampAdditional\": \"1011\",\n" +
                "                    \"statusDate\": \"20181112000000000\",\n" +
                "                    \"stampDateTime\": \"20181112120555667\",\n" +
                "                    \"nemotecnico\": \"MONTHS\",\n" +
                "                    \"termTypeId\": 2,\n" +
                "                    \"shortDesc\": \"Month\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"longDesc\": \"Mes\"\n" +
                "                },\n" +
                "                \"maxAmountInstallAuthorized\": \"0.00000\",\n" +
                "                \"subsidyRateValue\": null,\n" +
                "                \"accrualSuspTotalIntadj\": \"0.00000\",\n" +
                "                \"stampAdditional\": \"409\",\n" +
                "                \"accrualAdjNotAccount\": \"0.00000\",\n" +
                "                \"accrualSuspendedTotalInt\": \"0.00000\",\n" +
                "                \"nextNewRateDate\": \"20090324000000000\",\n" +
                "                \"lastTxnDate\": \"20090323000000000\",\n" +
                "                \"totalCapitalizatedInterest\": \"0.00000\",\n" +
                "                \"operationId\": \"1350\",\n" +
                "                \"vatCategory\": {\n" +
                "                    \"stampAdditional\": \"FUERA DE USO\",\n" +
                "                    \"statusDate\": \"20170310000000000\",\n" +
                "                    \"stampDateTime\": \"20170310000000000\",\n" +
                "                    \"nemotecnico\": \"NO APLICA\",\n" +
                "                    \"shortDesc\": \"VAT no charge\",\n" +
                "                    \"id\": 0,\n" +
                "                    \"vatCategoryId\": 0,\n" +
                "                    \"longDesc\": \"IVA no Responsable\"\n" +
                "                },\n" +
                "                \"totalTermDays\": 365,\n" +
                "                \"interestRate\": {\n" +
                "                    \"stampAdditional\": \"816\",\n" +
                "                    \"statusDate\": \"20090323130029948\",\n" +
                "                    \"stampDateTime\": \"20191011130029948\",\n" +
                "                    \"dateTo\": \"20781231000000000\",\n" +
                "                    \"rateCodeId\": 8492,\n" +
                "                    \"systemUser\": null,\n" +
                "                    \"shortDesc\": \"PIZARRA LOANS\",\n" +
                "                    \"id\": 453613,\n" +
                "                    \"dateFrom\": \"19691231000000000\",\n" +
                "                    \"longDesc\": \"PIZARRA LOANS MAX\"\n" +
                "                },\n" +
                "                \"accrualSuspendedDate\": null,\n" +
                "                \"lo3thPaymentDebt\": \"100.00000\",\n" +
                "                \"totalDisbursedAmount\": \"2000.00000\",\n" +
                "                \"teoricDueCapitalBalance\": \"1880.30000\",\n" +
                "                \"accountingBranch\": {\n" +
                "                    \"attentionPointQuantity\": null,\n" +
                "                    \"stampAdditional\": \"655\",\n" +
                "                    \"statusDate\": \"20090323091156163\",\n" +
                "                    \"branchId\": 502,\n" +
                "                    \"meanTime\": null,\n" +
                "                    \"stampDateTime\": \"20170511091156163\",\n" +
                "                    \"shortDesc\": \"MICROCENTRO\",\n" +
                "                    \"id\": 24,\n" +
                "                    \"longDesc\": \"MICROCENTRO\"\n" +
                "                },\n" +
                "                \"lastCapIntDate\": null,\n" +
                "                \"maxUnpaidSubsidyInstallment\": null,\n" +
                "                \"accrualSubsidyIntNextMonth\": \"0.00000\",\n" +
                "                \"nextInstallmentDate\": \"20090523000000000\",\n" +
                "                \"totalPaymentDecress\": \"0.00000\",\n" +
                "                \"loOtherAmount\": \"2000.00000\",\n" +
                "                \"closedDate\": \"20100323000000000\",\n" +
                "                \"capitalTerm\": 1,\n" +
                "                \"stampUser\": {\n" +
                "                    \"stampAdditional\": \"724\",\n" +
                "                    \"statusDate\": \"20190102000000000\",\n" +
                "                    \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "                    \"stampDateTime\": \"20190102154150738\",\n" +
                "                    \"lastTrxDate\": \"20200312122927647\",\n" +
                "                    \"name\": \"PATRICIO BUTTA\",\n" +
                "                    \"id\": 451214,\n" +
                "                    \"userId\": \"PBUTTA\"\n" +
                "                },\n" +
                "                \"totalCapitalAdjustement\": \"0.00000\",\n" +
                "                \"interestSpreadType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"PUNTOS\",\n" +
                "                    \"shortDesc\": \"points\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"interestSpreadTypeId\": 1,\n" +
                "                    \"longDesc\": \"Suma puntos\"\n" +
                "                },\n" +
                "                \"currencyCode\": {\n" +
                "                    \"stampAdditional\": \"1806\",\n" +
                "                    \"statusDate\": \"20090323175644575\",\n" +
                "                    \"currAcronym\": \"MNL\",\n" +
                "                    \"stampDateTime\": \"20191022175644575\",\n" +
                "                    \"expressionUnit\": 1,\n" +
                "                    \"currencyCodeId\": 1,\n" +
                "                    \"dateTo\": \"20781231000000000\",\n" +
                "                    \"shortDesc\": \"Local currency\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"dateFrom\": \"20010101000000000\",\n" +
                "                    \"longDesc\": \"Moneda Local\"\n" +
                "                },\n" +
                "                \"interestPeriodType\": {\n" +
                "                    \"stampAdditional\": \"1011\",\n" +
                "                    \"statusDate\": \"20181112000000000\",\n" +
                "                    \"stampDateTime\": \"20181112120546743\",\n" +
                "                    \"nemotecnico\": \"DAYS\",\n" +
                "                    \"termTypeId\": 1,\n" +
                "                    \"shortDesc\": \"Day\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"Dia\"\n" +
                "                },\n" +
                "                \"agreementActiveFlag\": false,\n" +
                "                \"accrualBaseCalc\": {\n" +
                "                    \"nemotecnico\": \"365_DIAS\",\n" +
                "                    \"shortDesc\": \"365 days\",\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"initialDisbursedAmount\": \"2000.00000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"loCurrentPaymentAmount\": \"236.41000\",\n" +
                "                \"loCrmInfoExpiryDate\": null,\n" +
                "                \"stampDateTime\": \"20200220114310973\",\n" +
                "                \"fundDestiny\": {\n" +
                "                    \"fundDestinyId\": 5104,\n" +
                "                    \"stampAdditional\": \"145\",\n" +
                "                    \"statusDate\": \"20160503000000000\",\n" +
                "                    \"stampDateTime\": \"20160503090406188\",\n" +
                "                    \"nemotecnico\": \"AUTOOTRO\",\n" +
                "                    \"shortDesc\": \"ADQ. motor for other uses\",\n" +
                "                    \"id\": 5,\n" +
                "                    \"longDesc\": \"Adquisición de automotores para otros usos\"\n" +
                "                },\n" +
                "                \"loDescription\": \"BIRCHE JUNIOR, ADEMIR \",\n" +
                "                \"firstInterestInstallDate\": \"20090423000000000\",\n" +
                "                \"businessSector\": {\n" +
                "                    \"stampAdditional\": \"1011\",\n" +
                "                    \"statusDate\": \"20180126000000000\",\n" +
                "                    \"stampDateTime\": \"20180126091639250\",\n" +
                "                    \"businessSectorId\": 1003,\n" +
                "                    \"nemotecnico\": \"PRIV NO FINANC\",\n" +
                "                    \"shortDesc\": \"In the country Priv No Financ\",\n" +
                "                    \"id\": 500003,\n" +
                "                    \"longDesc\": \"\\tResidentes en el país - Sector privado no financiero\\t\"\n" +
                "                },\n" +
                "                \"accrualIntNotAccount\": \"0.00000\",\n" +
                "                \"totalCapitalTeoricAdjust\": \"0.00000\",\n" +
                "                \"accrualAdjNextMonth\": \"99.26000\",\n" +
                "                \"dueInterestBalance\": \"0.00000\",\n" +
                "                \"originIndex\": \"7.17000\",\n" +
                "                \"subproduct\": {\n" +
                "                    \"specialCrInterestFlag\": false,\n" +
                "                    \"statusDate\": \"20090323171053815\",\n" +
                "                    \"isConditionUpdateAllowed\": false,\n" +
                "                    \"systemUser\": null,\n" +
                "                    \"isPackagesAllowed\": false,\n" +
                "                    \"isForcedBusinessSector\": false,\n" +
                "                    \"stampAdditional\": \"930\",\n" +
                "                    \"automNbrAcctFlag\": true,\n" +
                "                    \"stampDateTime\": \"20200304171053815\",\n" +
                "                    \"subproductId\": \"09000\",\n" +
                "                    \"isInterbranchAllowed\": true,\n" +
                "                    \"sttIssueFlag\": false,\n" +
                "                    \"crmInfo\": null,\n" +
                "                    \"id\": 23,\n" +
                "                    \"warrantyReqFlag\": true,\n" +
                "                    \"valuedDateTxnMaxDays\": null,\n" +
                "                    \"product\": {\n" +
                "                        \"productId\": \"09001\",\n" +
                "                        \"crmInfo\": null,\n" +
                "                        \"dateTo\": \"20781231000000000\",\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10,\n" +
                "                        \"dateFrom\": \"20010101000000000\",\n" +
                "                        \"detailTableName\": \"LOAN\",\n" +
                "                        \"longDesc\": \"Prestamos\",\n" +
                "                        \"crmInfoExpiryDate\": null,\n" +
                "                        \"productIsOwn\": true\n" +
                "                    },\n" +
                "                    \"batchProcessFlag\": false,\n" +
                "                    \"sttDayNbr\": null,\n" +
                "                    \"isForAllBranches\": false,\n" +
                "                    \"specialDbInterestFlag\": false,\n" +
                "                    \"valuedDateTxnAllowed\": true,\n" +
                "                    \"dateFrom\": \"20080102000000000\",\n" +
                "                    \"sttMaxLines\": 5,\n" +
                "                    \"sttMonthNbr\": 6,\n" +
                "                    \"valuedDateIntMaxDays\": null,\n" +
                "                    \"specialComissionFlag\": false,\n" +
                "                    \"dateTo\": \"20781231000000000\",\n" +
                "                    \"sttInterval\": null,\n" +
                "                    \"shortDesc\": \"1 LOAN RATE VAR FRANCES\",\n" +
                "                    \"acceptOtherCurrency\": false,\n" +
                "                    \"currencyCode\": {\n" +
                "                        \"currencyCodeId\": 1,\n" +
                "                        \"shortDesc\": \"Local currency\",\n" +
                "                        \"id\": 1\n" +
                "                    },\n" +
                "                    \"longDesc\": \"PRESTAMO TASA VDA FRANCES\",\n" +
                "                    \"crmInfoExpiryDate\": null\n" +
                "                },\n" +
                "                \"qtyGraceInstallment\": 0,\n" +
                "                \"loOtherAmountDueDate\": \"19691231210000000\",\n" +
                "                \"unpaidQuotSubsidyFlag\": false,\n" +
                "                \"specialRates\": true,\n" +
                "                \"approvalDate\": \"20090323000000000\",\n" +
                "                \"loanType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"PRESTAMO\",\n" +
                "                    \"shortDesc\": \"Loan\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"loanTypeId\": 1,\n" +
                "                    \"longDesc\": \"Prestamo\"\n" +
                "                },\n" +
                "                \"loCrmInfo\": null,\n" +
                "                \"systemUser\": null,\n" +
                "                \"totalIntSubsidyDue\": \"0.00000\",\n" +
                "                \"fundOrigin\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20010101000000000\",\n" +
                "                    \"fundOriginId\": 3,\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"GENERICO\",\n" +
                "                    \"shortDesc\": \"Generic\",\n" +
                "                    \"id\": 3,\n" +
                "                    \"longDesc\": \"Generico\"\n" +
                "                },\n" +
                "                \"interestPeriodVariable\": 1,\n" +
                "                \"expiryDate\": \"20100323000000000\",\n" +
                "                \"amortizationType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"FRANCES\",\n" +
                "                    \"shortDesc\": \"French\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"amortizationTypeId\": 1,\n" +
                "                    \"longDesc\": \"Frances\"\n" +
                "                },\n" +
                "                \"customerNumberReached\": 0,\n" +
                "                \"loPaymentDueDate\": \"20090323000000000\",\n" +
                "                \"accrualIntadjNotAccount\": \"0.00000\",\n" +
                "                \"crDate\": \"20090323000000000\",\n" +
                "                \"interestRateValue\": \"66.00000\",\n" +
                "                \"situationCode\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"nemotecnico\": \"NORMAL\",\n" +
                "                    \"shortDesc\": \"Normal\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"SITUACION Y CUMPLIMIENTO NORMAL\",\n" +
                "                    \"situationCodeId\": 1\n" +
                "                },\n" +
                "                \"interestVariableType\": {\n" +
                "                    \"stampAdditional\": null,\n" +
                "                    \"statusDate\": \"20060101000000000\",\n" +
                "                    \"stampDateTime\": null,\n" +
                "                    \"interestVariableTypeId\": 2,\n" +
                "                    \"nemotecnico\": \"VARIABLE\",\n" +
                "                    \"shortDesc\": \"Variable\",\n" +
                "                    \"id\": 2,\n" +
                "                    \"longDesc\": \"Variable\"\n" +
                "                },\n" +
                "                \"beginDate\": \"20090323000000000\",\n" +
                "                \"priceListProfile\": 11,\n" +
                "                \"maxQtyArrearDays\": 0,\n" +
                "                \"interestTerm\": 1,\n" +
                "                \"loFeesNotCharged\": \"2600.51000\",\n" +
                "                \"specialFees\": true\n" +
                "            },\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Loan\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}"
        );
    }

    public JSONObject getResponseListLoansEmpty() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectLoanFrom_Customer_Operation\",\n" +
                "    \"transactionId\": \"massiveSelectLoanFrom_Customer_Operation\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.loan_list\": {\n" +
                "        \"collectionEntityId\": \"Loan\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}"
        );
    }

    private JSONObject getResponseRequestLoanOK() {
        return new JSONObject("{\n" +
                "   \"project\": \"Tech\",\n" +
                "   \"responseCode\": \"insertLoanNormal_CMM\",\n" +
                "   \"transactionId\": \"insertLoanNormal_CMM\",\n" +
                "   \"transactionVersion\": \"1.0\",\n" +
                "   \"out.loan_installment_payment_list\": {\n" +
                "      \"collection\": [\n" +
                "         {}\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"LoanInstallmentPayment\",\n" +
                "      \"collectionEntityVersion\": \"1.0\",\n" +
                "      \"collectionEntityDataModel\": \"product.financials\"\n" +
                "   },\n" +
                "   \"out.loan_charge_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"amountType\": {\n" +
                "               \"stampAdditional\": \"1011\",\n" +
                "               \"statusDate\": \"20120602000000000\",\n" +
                "               \"officialId\": \"CARGO\",\n" +
                "               \"stampDateTime\": \"20120602093229772\",\n" +
                "               \"nemotecnico\": \"COM_AJE\",\n" +
                "               \"amountTypeId\": 25,\n" +
                "               \"shortDesc\": \"Gtos.Vrbles s / iva\",\n" +
                "               \"id\": 18,\n" +
                "               \"longDesc\": \"Comisiones Ajenas\"\n" +
                "            },\n" +
                "            \"statusDate\": \"23032009\",\n" +
                "            \"currencyChargeAmount\": \"0.00000\",\n" +
                "            \"currencyChargePurchasePrice\": \"0.00000\",\n" +
                "            \"isManual\": false,\n" +
                "            \"loanTxn\": {\n" +
                "               \"statusDate\": \"23032009\",\n" +
                "               \"loan\": {\n" +
                "                  \"accrualIntadjNextMonth\": \"0.00000\",\n" +
                "                  \"statusDate\": \"23032009\",\n" +
                "                  \"crAuthorizedCustomer\": {\n" +
                "                     \"statusDate\": \"20090323132734128\",\n" +
                "                     \"isEmployee\": null,\n" +
                "                     \"creditStatusDate\": null,\n" +
                "                     \"firstName1\": \"Ariel\",\n" +
                "                     \"confidentialFlag\": false,\n" +
                "                     \"firstName2\": null,\n" +
                "                     \"description\": null,\n" +
                "                     \"purgeDate\": null,\n" +
                "                     \"seniorityBank\": \"20090323000000000\",\n" +
                "                     \"nameShort\": null,\n" +
                "                     \"expiryDateCrmInfo\": \"20090323000000000\",\n" +
                "                     \"stampAdditional\": \"449797\",\n" +
                "                     \"dependentsM\": null,\n" +
                "                     \"stampDateTime\": \"20191028132734128\",\n" +
                "                     \"crmInfo\": null,\n" +
                "                     \"customerId\": \"1596\",\n" +
                "                     \"lastName1\": \"Hernandez\",\n" +
                "                     \"name\": \"Hernandez , Ariel \",\n" +
                "                     \"dependentsF\": null,\n" +
                "                     \"identificationNumber\": \"999999\",\n" +
                "                     \"lastName2\": null,\n" +
                "                     \"pep\": null,\n" +
                "                     \"id\": 545004,\n" +
                "                     \"additionDate\": \"20090323000000000\"\n" +
                "                  },\n" +
                "                  \"totalCapSubsidyDue\": \"0.00000\",\n" +
                "                  \"automaticDbFlag\": true,\n" +
                "                  \"accrualIntNextMonth\": \"0.00000\",\n" +
                "                  \"firstCapitalInstallDate\": \"23042009\",\n" +
                "                  \"customerNumberDeclared\": 0,\n" +
                "                  \"branch\": {\n" +
                "                     \"statusDate\": \"20090323152351117\",\n" +
                "                     \"branchId\": 1,\n" +
                "                     \"meanTime\": null,\n" +
                "                     \"workOnSaturday\": false,\n" +
                "                     \"dateFrom\": \"20010101000000000\",\n" +
                "                     \"attentionPointQuantity\": null,\n" +
                "                     \"stampAdditional\": \"655\",\n" +
                "                     \"stampDateTime\": \"20190808152351117\",\n" +
                "                     \"handleCash\": true,\n" +
                "                     \"dateTo\": \"20781231000000000\",\n" +
                "                     \"shortDesc\": \"Main Branch\",\n" +
                "                     \"id\": 0,\n" +
                "                     \"longDesc\": \"Main Branch\"\n" +
                "                  },\n" +
                "                  \"loOpeningDate\": null,\n" +
                "                  \"loSettlementAmount\": \"0.00000\",\n" +
                "                  \"accrualSubsidyIntNotAcco\": \"0.00000\",\n" +
                "                  \"id\": 11866763,\n" +
                "                  \"termTypeGraceInstallment\": {\n" +
                "                     \"stampAdditional\": \"1011\",\n" +
                "                     \"statusDate\": \"20181112000000000\",\n" +
                "                     \"officialId\": \"1\",\n" +
                "                     \"stampDateTime\": \"20181112120546743\",\n" +
                "                     \"nemotecnico\": \"DAYS\",\n" +
                "                     \"termTypeId\": 1,\n" +
                "                     \"shortDesc\": \"Day\",\n" +
                "                     \"id\": 1,\n" +
                "                     \"longDesc\": \"Dia\"\n" +
                "                  },\n" +
                "                  \"fixedInstallmentDay\": 23,\n" +
                "                  \"totalInterestAdjustement\": \"0.00000\",\n" +
                "                  \"maxArrearsDaysUnpInstSubs\": 0,\n" +
                "                  \"pendingDisbursementFlag\": false,\n" +
                "                  \"termGraceInstallment\": 0,\n" +
                "                  \"interestRateType\": {\n" +
                "                     \"stampAdditional\": null,\n" +
                "                     \"statusDate\": \"20060101000000000\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"rateTypeId\": 1,\n" +
                "                     \"stampDateTime\": null,\n" +
                "                     \"nemotecnico\": \"TNAV\",\n" +
                "                     \"shortDesc\": \"TNAV\",\n" +
                "                     \"id\": 1,\n" +
                "                     \"longDesc\": \"Tasa nominal anual vencida\"\n" +
                "                  },\n" +
                "                  \"inputDate\": \"23032009\",\n" +
                "                  \"qtyInterestInstallment\": 10,\n" +
                "                  \"subsidyFlag\": false,\n" +
                "                  \"lo2ndPaymentDebt\": \"0.00000\",\n" +
                "                  \"interestSpreadValue\": \"0.00000\",\n" +
                "                  \"linkToPackage\": false,\n" +
                "                  \"loPrincipalAmount\": \"0.00000\",\n" +
                "                  \"updateDate\": \"22032009\",\n" +
                "                  \"lo1stPaymentDebt\": \"0.00000\",\n" +
                "                  \"agreementFlag\": false,\n" +
                "                  \"termType\": {\n" +
                "                     \"stampAdditional\": \"1011\",\n" +
                "                     \"statusDate\": \"20181112000000000\",\n" +
                "                     \"officialId\": \"1\",\n" +
                "                     \"stampDateTime\": \"20181112120555667\",\n" +
                "                     \"nemotecnico\": \"MONTHS\",\n" +
                "                     \"termTypeId\": 2,\n" +
                "                     \"shortDesc\": \"Month\",\n" +
                "                     \"id\": 2,\n" +
                "                     \"longDesc\": \"Mes\"\n" +
                "                  },\n" +
                "                  \"maxAmountInstallAuthorized\": \"0.00000\",\n" +
                "                  \"subsidyRateValue\": null,\n" +
                "                  \"qtyCapitalInstallment\": 10,\n" +
                "                  \"accrualSuspTotalIntadj\": \"0.00000\",\n" +
                "                  \"stampAdditional\": \"409\",\n" +
                "                  \"accrualAdjNotAccount\": \"0.00000\",\n" +
                "                  \"accrualSuspendedTotalInt\": \"0.00000\",\n" +
                "                  \"nextNewRateDate\": \"23032009\",\n" +
                "                  \"lastTxnDate\": \"23032009\",\n" +
                "                  \"totalCapitalizatedInterest\": \"0.00000\",\n" +
                "                  \"operationId\": \"1275\",\n" +
                "                  \"vatCategory\": {\n" +
                "                     \"stampAdditional\": null,\n" +
                "                     \"statusDate\": \"20060101000000000\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"stampDateTime\": null,\n" +
                "                     \"nemotecnico\": \"RESP. INSCRIPTO\",\n" +
                "                     \"shortDesc\": \"Responsible VAT Enrolled\",\n" +
                "                     \"id\": 1,\n" +
                "                     \"vatCategoryId\": 1,\n" +
                "                     \"longDesc\": \"IVA Responsable Inscripto\"\n" +
                "                  },\n" +
                "                  \"totalTermDays\": 306,\n" +
                "                  \"accrualSuspendedDate\": null,\n" +
                "                  \"lo3thPaymentDebt\": \"100.00000\",\n" +
                "                  \"totalDisbursedAmount\": \"39000.00000\",\n" +
                "                  \"teoricDueCapitalBalance\": \"39000.00000\",\n" +
                "                  \"dueCapitalBalance\": \"39000.00000\",\n" +
                "                  \"accountingBranch\": {\n" +
                "                     \"statusDate\": \"20090323091156163\",\n" +
                "                     \"branchId\": 502,\n" +
                "                     \"meanTime\": null,\n" +
                "                     \"workOnSaturday\": false,\n" +
                "                     \"dateFrom\": \"20010101000000000\",\n" +
                "                     \"attentionPointQuantity\": null,\n" +
                "                     \"stampAdditional\": \"655\",\n" +
                "                     \"stampDateTime\": \"20170511091156163\",\n" +
                "                     \"handleCash\": true,\n" +
                "                     \"dateTo\": \"20481231000000000\",\n" +
                "                     \"shortDesc\": \"MICROCENTRO\",\n" +
                "                     \"id\": 24,\n" +
                "                     \"longDesc\": \"MICROCENTRO\"\n" +
                "                  },\n" +
                "                  \"lastCapIntDate\": null,\n" +
                "                  \"maxUnpaidSubsidyInstallment\": null,\n" +
                "                  \"accrualSubsidyIntNextMonth\": \"0.00000\",\n" +
                "                  \"nextInstallmentDate\": \"23042009\",\n" +
                "                  \"totalPaymentDecress\": \"0.00000\",\n" +
                "                  \"loOtherAmount\": \"39000.00000\",\n" +
                "                  \"closedDate\": \"23012010\",\n" +
                "                  \"capitalTerm\": 1,\n" +
                "                  \"totalCapitalAdjustement\": \"0.00000\",\n" +
                "                  \"interestSpreadType\": {\n" +
                "                     \"stampAdditional\": null,\n" +
                "                     \"statusDate\": \"20060101000000000\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"stampDateTime\": null,\n" +
                "                     \"nemotecnico\": \"PUNTOS\",\n" +
                "                     \"shortDesc\": \"points\",\n" +
                "                     \"id\": 2,\n" +
                "                     \"interestSpreadTypeId\": 1,\n" +
                "                     \"longDesc\": \"Suma puntos\"\n" +
                "                  },\n" +
                "                  \"currencyCode\": {\n" +
                "                     \"statusDate\": \"20090323175644575\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"dateFrom\": \"20010101000000000\",\n" +
                "                     \"epayCode\": null,\n" +
                "                     \"stampAdditional\": \"1806\",\n" +
                "                     \"currAcronym\": \"MNL\",\n" +
                "                     \"stampDateTime\": \"20191022175644575\",\n" +
                "                     \"expressionUnit\": 1,\n" +
                "                     \"nemotecnico\": \"MNL\",\n" +
                "                     \"currencyCodeId\": 1,\n" +
                "                     \"dateTo\": \"20781231000000000\",\n" +
                "                     \"shortDesc\": \"Local currency\",\n" +
                "                     \"id\": 1,\n" +
                "                     \"longDesc\": \"Moneda Local\"\n" +
                "                  },\n" +
                "                  \"interestPeriodType\": {\n" +
                "                     \"stampAdditional\": \"1011\",\n" +
                "                     \"statusDate\": \"20181112000000000\",\n" +
                "                     \"officialId\": \"1\",\n" +
                "                     \"stampDateTime\": \"20181112120546743\",\n" +
                "                     \"nemotecnico\": \"DAYS\",\n" +
                "                     \"termTypeId\": 1,\n" +
                "                     \"shortDesc\": \"Day\",\n" +
                "                     \"id\": 1,\n" +
                "                     \"longDesc\": \"Dia\"\n" +
                "                  },\n" +
                "                  \"agreementActiveFlag\": false,\n" +
                "                  \"initialDisbursedAmount\": \"39000.00000\",\n" +
                "                  \"currentIndex\": \"1.00000\",\n" +
                "                  \"loCurrentPaymentAmount\": \"0.00000\",\n" +
                "                  \"loCrmInfoExpiryDate\": null,\n" +
                "                  \"stampDateTime\": \"16032020\",\n" +
                "                  \"loDescription\": \"Hernandez , Ariel \",\n" +
                "                  \"firstInterestInstallDate\": \"23042009\",\n" +
                "                  \"businessSector\": {\n" +
                "                     \"stampAdditional\": \"1011\",\n" +
                "                     \"statusDate\": \"20160422000000000\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"stampDateTime\": \"20160422112203041\",\n" +
                "                     \"businessSectorId\": 1007,\n" +
                "                     \"nemotecnico\": \"No Informada\",\n" +
                "                     \"shortDesc\": \"uninformed\",\n" +
                "                     \"id\": 92,\n" +
                "                     \"longDesc\": \"No Informada\"\n" +
                "                  },\n" +
                "                  \"accrualIntNotAccount\": \"0.00000\",\n" +
                "                  \"totalCapitalTeoricAdjust\": \"0.00000\",\n" +
                "                  \"accrualAdjNextMonth\": \"0.00000\",\n" +
                "                  \"dueInterestBalance\": \"0.00000\",\n" +
                "                  \"originIndex\": \"0.34000\",\n" +
                "                  \"subproduct\": {\n" +
                "                     \"specialCrInterestFlag\": false,\n" +
                "                     \"statusDate\": \"20090323152613232\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"isConditionUpdateAllowed\": false,\n" +
                "                     \"isFundsSettlementAllowed\": false,\n" +
                "                     \"systemUser\": null,\n" +
                "                     \"isPackagesAllowed\": false,\n" +
                "                     \"isForcedBusinessSector\": false,\n" +
                "                     \"stampAdditional\": \"930\",\n" +
                "                     \"automNbrAcctFlag\": true,\n" +
                "                     \"stampDateTime\": \"20200227152613232\",\n" +
                "                     \"subproductId\": \"09014\",\n" +
                "                     \"isInterbranchAllowed\": true,\n" +
                "                     \"sttIssueFlag\": false,\n" +
                "                     \"nemotecnico\": \"PRESTAMO\",\n" +
                "                     \"crmInfo\": null,\n" +
                "                     \"id\": 453495,\n" +
                "                     \"warrantyReqFlag\": false,\n" +
                "                     \"valuedDateTxnMaxDays\": null,\n" +
                "                     \"product\": {\n" +
                "                        \"statusDate\": \"20090203123819187\",\n" +
                "                        \"officialId\": \"1\",\n" +
                "                        \"productId\": \"09001\",\n" +
                "                        \"systemUser\": \"1\",\n" +
                "                        \"dateFrom\": \"20010101000000000\",\n" +
                "                        \"productIsOwn\": true,\n" +
                "                        \"stampAdditional\": null,\n" +
                "                        \"stampDateTime\": \"20091201123819203\",\n" +
                "                        \"crmInfo\": null,\n" +
                "                        \"dateTo\": \"20781231000000000\",\n" +
                "                        \"shortDesc\": \"Loans\",\n" +
                "                        \"id\": 10,\n" +
                "                        \"detailTableName\": \"LOAN\",\n" +
                "                        \"longDesc\": \"Prestamos\",\n" +
                "                        \"crmInfoExpiryDate\": null\n" +
                "                     },\n" +
                "                     \"batchProcessFlag\": false,\n" +
                "                     \"sttDayNbr\": null,\n" +
                "                     \"isForAllBranches\": true,\n" +
                "                     \"specialDbInterestFlag\": true,\n" +
                "                     \"valuedDateTxnAllowed\": true,\n" +
                "                     \"dateFrom\": \"20080102000000000\",\n" +
                "                     \"sttMaxLines\": 20,\n" +
                "                     \"sttMonthNbr\": 6,\n" +
                "                     \"valuedDateIntMaxDays\": null,\n" +
                "                     \"specialComissionFlag\": false,\n" +
                "                     \"dateTo\": \"20781231000000000\",\n" +
                "                     \"sttInterval\": null,\n" +
                "                     \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "                     \"acceptOtherCurrency\": true,\n" +
                "                     \"longDesc\": \"ATB TEST FRANCESS\",\n" +
                "                     \"crmInfoExpiryDate\": null\n" +
                "                  },\n" +
                "                  \"qtyGraceInstallment\": 0,\n" +
                "                  \"loOtherAmountDueDate\": \"31121969\",\n" +
                "                  \"unpaidQuotSubsidyFlag\": false,\n" +
                "                  \"specialRates\": true,\n" +
                "                  \"approvalDate\": \"23032009\",\n" +
                "                  \"loanType\": {\n" +
                "                     \"stampAdditional\": null,\n" +
                "                     \"statusDate\": \"20060101000000000\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"stampDateTime\": null,\n" +
                "                     \"nemotecnico\": \"PRESTAMO\",\n" +
                "                     \"shortDesc\": \"Loan\",\n" +
                "                     \"id\": 1,\n" +
                "                     \"loanTypeId\": 1,\n" +
                "                     \"longDesc\": \"Prestamo\"\n" +
                "                  },\n" +
                "                  \"loCrmInfo\": null,\n" +
                "                  \"systemUser\": null,\n" +
                "                  \"totalIntSubsidyDue\": \"0.00000\",\n" +
                "                  \"totalOperationAmount\": \"39000.00000\",\n" +
                "                  \"fundOrigin\": {\n" +
                "                     \"stampAdditional\": null,\n" +
                "                     \"statusDate\": \"20010101000000000\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"fundOriginId\": 3,\n" +
                "                     \"stampDateTime\": null,\n" +
                "                     \"nemotecnico\": \"GENERICO\",\n" +
                "                     \"shortDesc\": \"Generic\",\n" +
                "                     \"id\": 3,\n" +
                "                     \"longDesc\": \"Generico\"\n" +
                "                  },\n" +
                "                  \"interestPeriodVariable\": 0,\n" +
                "                  \"expiryDate\": \"23012010\",\n" +
                "                  \"amortizationType\": {\n" +
                "                     \"stampAdditional\": null,\n" +
                "                     \"statusDate\": \"20060101000000000\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"stampDateTime\": null,\n" +
                "                     \"nemotecnico\": \"FRANCES\",\n" +
                "                     \"shortDesc\": \"French\",\n" +
                "                     \"id\": 7,\n" +
                "                     \"amortizationTypeId\": 1,\n" +
                "                     \"longDesc\": \"Frances\"\n" +
                "                  },\n" +
                "                  \"customerNumberReached\": 0,\n" +
                "                  \"loPaymentDueDate\": \"23032009\",\n" +
                "                  \"accrualIntadjNotAccount\": \"0.00000\",\n" +
                "                  \"crDate\": \"23032009\",\n" +
                "                  \"interestRateValue\": \"4.00000\",\n" +
                "                  \"situationCode\": {\n" +
                "                     \"stampAdditional\": null,\n" +
                "                     \"statusDate\": \"20060101000000000\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"stampDateTime\": null,\n" +
                "                     \"nemotecnico\": \"NORMAL\",\n" +
                "                     \"shortDesc\": \"Normal\",\n" +
                "                     \"id\": 1,\n" +
                "                     \"longDesc\": \"SITUACION Y CUMPLIMIENTO NORMAL\",\n" +
                "                     \"situationCodeId\": 1\n" +
                "                  },\n" +
                "                  \"interestVariableType\": {\n" +
                "                     \"stampAdditional\": \"1011\",\n" +
                "                     \"statusDate\": \"20180704000000000\",\n" +
                "                     \"officialId\": null,\n" +
                "                     \"stampDateTime\": \"20180704154936182\",\n" +
                "                     \"interestVariableTypeId\": 1,\n" +
                "                     \"nemotecnico\": \"FIXED\",\n" +
                "                     \"shortDesc\": \"fixed\",\n" +
                "                     \"id\": 1,\n" +
                "                     \"longDesc\": \"Fija\"\n" +
                "                  },\n" +
                "                  \"beginDate\": \"23032009\",\n" +
                "                  \"priceListProfile\": 300,\n" +
                "                  \"maxQtyArrearDays\": null,\n" +
                "                  \"amountInstallment\": \"3971.85690\",\n" +
                "                  \"interestTerm\": 1,\n" +
                "                  \"loFeesNotCharged\": null,\n" +
                "                  \"specialFees\": true\n" +
                "               },\n" +
                "               \"totalSubsidy\": \"0.00000\",\n" +
                "               \"prorateInstallmentFlag\": false,\n" +
                "               \"transactionNumber\": 1,\n" +
                "               \"totalBonification\": \"0.00000\",\n" +
                "               \"stampAdditional\": \"376\",\n" +
                "               \"totalAmount\": \"39000.00000\",\n" +
                "               \"advanceTermFlag\": false,\n" +
                "               \"totalCharge\": \"1950.00000\",\n" +
                "               \"stampDateTime\": \"16032020\",\n" +
                "               \"id\": 11867323,\n" +
                "               \"txnValueDate\": \"23032009\",\n" +
                "               \"txnDate\": \"23032009\"\n" +
                "            },\n" +
                "            \"stampAdditional\": \"376\",\n" +
                "            \"currencyChargeSellingPrice\": \"0.00000\",\n" +
                "            \"loanInstallmentPayment\": {\n" +
                "               \"isCurrentInstallment\": null,\n" +
                "               \"adjustInterestPaid\": \"0.00000\",\n" +
                "               \"arrearAdjustmentPaid\": \"0.00000\",\n" +
                "               \"arrearInterestPaid\": \"0.00000\",\n" +
                "               \"orderNumber\": 0,\n" +
                "               \"paymentIsFull\": false,\n" +
                "               \"accruedIntSubsidy\": \"0.00000\",\n" +
                "               \"accruedInterest\": \"0.00000\",\n" +
                "               \"subsidyInterestPaid\": \"0.00000\",\n" +
                "               \"totalBonification\": \"0.00000\",\n" +
                "               \"adjustCapitalPaid\": \"0.00000\",\n" +
                "               \"subsidyCapitalPaid\": \"0.00000\",\n" +
                "               \"totalChargePaid\": \"1950.00000\",\n" +
                "               \"intDifferenceCode\": 0,\n" +
                "               \"capitalPaid\": \"39000.00000\",\n" +
                "               \"arrearIntAdjustmentPaid\": \"0.00000\",\n" +
                "               \"accruedAdjust\": \"0.00000\",\n" +
                "               \"punitiveInterestPaid\": \"0.00000\",\n" +
                "               \"accruedIntAdj\": \"0.00000\",\n" +
                "               \"id\": 11867283,\n" +
                "               \"punitiveIntAdjustmentPaid\": \"0.00000\",\n" +
                "               \"interestPaid\": \"0.00000\",\n" +
                "               \"interestPaidDifference\": \"0.00000\"\n" +
                "            },\n" +
                "            \"currencyChargeDate\": null,\n" +
                "            \"stampDateTime\": \"16032020\",\n" +
                "            \"currencyChargePriceUnity\": null,\n" +
                "            \"currencyCharge\": {\n" +
                "               \"statusDate\": \"20090323175644575\",\n" +
                "               \"officialId\": null,\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"epayCode\": null,\n" +
                "               \"stampAdditional\": \"1806\",\n" +
                "               \"currAcronym\": \"MNL\",\n" +
                "               \"stampDateTime\": \"20191022175644575\",\n" +
                "               \"expressionUnit\": 1,\n" +
                "               \"nemotecnico\": \"MNL\",\n" +
                "               \"currencyCodeId\": 1,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1,\n" +
                "               \"longDesc\": \"Moneda Local\"\n" +
                "            },\n" +
                "            \"pureAction\": {\n" +
                "               \"stampAdditional\": \"ISO 8583\",\n" +
                "               \"statusDate\": \"20161012000000000\",\n" +
                "               \"officialId\": \"CR\",\n" +
                "               \"stampDateTime\": \"20161012000000000\",\n" +
                "               \"nemotecnico\": \"ACC_CREDITO\",\n" +
                "               \"shortDesc\": \"Credito\",\n" +
                "               \"id\": 2,\n" +
                "               \"pureActionId\": 2,\n" +
                "               \"longDesc\": \"Credito (Suma importe)\"\n" +
                "            },\n" +
                "            \"chargeAmount\": \"1950.00000\",\n" +
                "            \"id\": 11867363,\n" +
                "            \"stampUser\": {\n" +
                "               \"statusDate\": \"20140225000000000\",\n" +
                "               \"validityTerm\": 0,\n" +
                "               \"passwordExpireDated\": \"20781231000000000\",\n" +
                "               \"sessionId\": null,\n" +
                "               \"userId\": \"TECHNISYS\",\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"passwordAttemps\": 0,\n" +
                "               \"password\": \"cRDtpNCeBiql5KOQsKVyrA0sAiA=\",\n" +
                "               \"stampDateTime\": \"20140225135546197\",\n" +
                "               \"lastAccessDate\": \"20200316111353000\",\n" +
                "               \"logged\": false,\n" +
                "               \"lastTrxDate\": \"20200316111353026\",\n" +
                "               \"name\": \"TECHNISYS\",\n" +
                "               \"id\": 2,\n" +
                "               \"activeFlag\": true\n" +
                "            },\n" +
                "            \"chargeCode\": {\n" +
                "               \"stampAdditional\": \"122\",\n" +
                "               \"statusDate\": \"20090323161934252\",\n" +
                "               \"stampDateTime\": \"20191105161934252\",\n" +
                "               \"chargeId\": 321,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"systemUser\": null,\n" +
                "               \"shortDesc\": \"CAC\",\n" +
                "               \"id\": 548655,\n" +
                "               \"dateFrom\": \"19691231210000000\",\n" +
                "               \"longDesc\": \"CAC\"\n" +
                "            },\n" +
                "            \"status\": {\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"statusDate\": null,\n" +
                "               \"officialId\": null,\n" +
                "               \"stampDateTime\": \"20180601102648000\",\n" +
                "               \"statusId\": 20,\n" +
                "               \"nemotecnico\": \"COBRADO\",\n" +
                "               \"shortDesc\": \"Paid\",\n" +
                "               \"id\": 20,\n" +
                "               \"longDesc\": \"Cobrada\"\n" +
                "            }\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"LoanCharge\",\n" +
                "      \"collectionEntityVersion\": \"1.0\",\n" +
                "      \"collectionEntityDataModel\": \"product.financials\"\n" +
                "   },\n" +
                "   out.loan:  {" +
                "      \"id\": 11866763\n" +
                "    }" +
                "}");
    }

    public JSONObject getResponseReadAccountOK() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"singleSelectAccount\",\n" +
                "    \"transactionId\": \"singleSelectAccount\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.account\": {\n" +
                "        \"lastPageGenerated\": 0,\n" +
                "        \"statusDate\": \"20090323112904093\",\n" +
                "        \"totalCrAccruedInterest\": \"0.00000\",\n" +
                "        \"subCurrencyIsObligatory\": false,\n" +
                "        \"todayCashWithdrawal\": \"0.00000\",\n" +
                "        \"dbBalanceToAccrual\": \"0.00000\",\n" +
                "        \"customerNumberDeclared\": 1,\n" +
                "        \"branch\": {\n" +
                "            \"branchId\": 1,\n" +
                "            \"shortDesc\": \"Main Branch\",\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"useOfSignature\": {\n" +
                "            \"useOfSignatureId\": 4,\n" +
                "            \"nemotecnico\": \"UNIPERSONAL\",\n" +
                "            \"shortDesc\": \"UNIPERSONAL\",\n" +
                "            \"id\": 4\n" +
                "        },\n" +
                "        \"today24HrsDb\": \"0.00000\",\n" +
                "        \"packOperationId\": \" \",\n" +
                "        \"bank\": {\n" +
                "            \"bankId\": 499,\n" +
                "            \"nemotecnico\": \"TECHBANK\",\n" +
                "            \"shortDesc\": \"TechBank\",\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"checkDigitOut\": \"0\",\n" +
                "        \"bankActivityCode\": {\n" +
                "            \"bankActivityCodeId\": 21,\n" +
                "            \"nemotecnico\": \"No Informada\",\n" +
                "            \"shortDesc\": \"uninformed\",\n" +
                "            \"id\": 21\n" +
                "        },\n" +
                "        \"id\": 544164,\n" +
                "        \"lastStatProcess\": \"20090320000000000\",\n" +
                "        \"tomorrowBlocked\": \"0.00000\",\n" +
                "        \"blockedBalance\": \"0.00000\",\n" +
                "        \"todayCashDeposits\": \"0.00000\",\n" +
                "        \"otherCr72Hrs\": \"0.00000\",\n" +
                "        \"packMainAccount\": false,\n" +
                "        \"inputDate\": \"20090323000000000\",\n" +
                "        \"lastProcessDate\": \"20090320000000000\",\n" +
                "        \"depositsOtherTerms\": \"0.00000\",\n" +
                "        \"checks48Hrs\": \"0.00000\",\n" +
                "        \"lastDateEmitted\": null,\n" +
                "        \"otherCrOtherTerms\": \"0.00000\",\n" +
                "        \"verifHistBalFlag\": true,\n" +
                "        \"cntChkRejectYtd\": 0,\n" +
                "        \"cntChkRejectMtd\": 0,\n" +
                "        \"otherDb96Hrs\": \"0.00000\",\n" +
                "        \"shortName\": \"Morin , Megan \",\n" +
                "        \"linkToPackage\": false,\n" +
                "        \"accumPendingTxn\": \"0.00000\",\n" +
                "        \"deposits24Hrs\": \"0.00000\",\n" +
                "        \"status\": {\n" +
                "            \"statusId\": 7,\n" +
                "            \"nemotecnico\": \"VIGENTE\",\n" +
                "            \"shortDesc\": \"Valid\",\n" +
                "            \"id\": 7\n" +
                "        },\n" +
                "        \"lastWithdrawalDate\": null,\n" +
                "        \"updateDate\": \"20090323000000000\",\n" +
                "        \"yesterdayDb\": \"0.00000\",\n" +
                "        \"dbBalAllowedFlag\": false,\n" +
                "        \"balanceToday\": \"280.00000\",\n" +
                "        \"stampAdditional\": \"971\",\n" +
                "        \"todayDeposits48Hrs\": \"0.00000\",\n" +
                "        \"debitModeType\": {\n" +
                "            \"nemotecnico\": \"COBRO_NORMAL\",\n" +
                "            \"shortDesc\": \"Normal collection\",\n" +
                "            \"debitModeTypeId\": 1,\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"lastTxnDate\": \"20191104000000000\",\n" +
                "        \"operationId\": \"4570\",\n" +
                "        \"currency\": {\n" +
                "            \"currAcronym\": \"MNL\",\n" +
                "            \"currencyCodeId\": 1,\n" +
                "            \"shortDesc\": \"Local currency\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"vatCategory\": {\n" +
                "            \"nemotecnico\": \"RESP. INSCRIPTO\",\n" +
                "            \"shortDesc\": \"Responsible VAT Enrolled\",\n" +
                "            \"id\": 1,\n" +
                "            \"vatCategoryId\": 1\n" +
                "        },\n" +
                "        \"todayDepositsOtherTerms\": \"0.00000\",\n" +
                "        \"lastTxn2date\": null,\n" +
                "        \"accrualSuspendedDate\": null,\n" +
                "        \"lineSttNumber\": 0,\n" +
                "        \"product\": {\n" +
                "            \"officialId\": null,\n" +
                "            \"productId\": \"01000\",\n" +
                "            \"shortDesc\": \"Caja de Ahorros\",\n" +
                "            \"id\": 4,\n" +
                "            \"isoProduct\": {\n" +
                "                \"officialId\": \"PASIVAS\",\n" +
                "                \"isoProductTypeId\": 10,\n" +
                "                \"nemotecnico\": \"C_AHORRO\",\n" +
                "                \"shortDesc\": \"Savings bank\",\n" +
                "                \"id\": 3,\n" +
                "                \"longDesc\": \"Caja de Ahorros\"\n" +
                "            },\n" +
                "            \"productIsOwn\": true\n" +
                "        },\n" +
                "        \"cntStopPaymentsYtd\": 0,\n" +
                "        \"dbDaysWithoutAgreement\": 0,\n" +
                "        \"dbBalanceTotalDays\": 0,\n" +
                "        \"lastTxnIntDbDate\": null,\n" +
                "        \"otherDbOtherTerms\": \"0.00000\",\n" +
                "        \"availableBalProcessType\": {\n" +
                "            \"availableBalProcessTypeId\": null,\n" +
                "            \"nemotecnico\": \"NO_APLICAR\",\n" +
                "            \"shortDesc\": \"DO NOT APPLY\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"dbBalanceStartDate\": null,\n" +
                "        \"lastTxnExtCrDate\": null,\n" +
                "        \"oldestValuedDateTxn\": null,\n" +
                "        \"todayDeposits96Hrs\": \"0.00000\",\n" +
                "        \"yesterdayCr\": \"0.00000\",\n" +
                "        \"closedDate\": null,\n" +
                "        \"checkTemporaryReceivership\": null,\n" +
                "        \"stampUser\": {\n" +
                "            \"name\": \"TECHNISYS\",\n" +
                "            \"id\": 2,\n" +
                "            \"userId\": \"TECHNISYS\"\n" +
                "        },\n" +
                "        \"lastAccrualDate\": \"20090320000000000\",\n" +
                "        \"minDaysForChecks\": 5,\n" +
                "        \"depositRestriction\": {\n" +
                "            \"nemotecnico\": \"NOT_RESTRICTED\",\n" +
                "            \"restrictionId\": 1,\n" +
                "            \"shortDesc\": \"Without restrictions\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"todayOtherCr\": \"0.00000\",\n" +
                "        \"otherDb48Hrs\": \"0.00000\",\n" +
                "        \"cntChkJustifiedYtd\": 0,\n" +
                "        \"totalDbAccruedInterest\": \"0.00000\",\n" +
                "        \"checksOthersTerms\": \"0.00000\",\n" +
                "        \"stampDateTime\": \"20191104112904093\",\n" +
                "        \"openingCodeMotive\": {\n" +
                "            \"nemotecnico\": \"AUTOMATICA\",\n" +
                "            \"openingCodeMotiveId\": 4,\n" +
                "            \"shortDesc\": \"AUTOMATICA\",\n" +
                "            \"id\": 4\n" +
                "        },\n" +
                "        \"lastHistoryProcess\": \"20090320000000000\",\n" +
                "        \"totalAgreements\": \"0.00000\",\n" +
                "        \"businessSector\": {\n" +
                "            \"businessSectorId\": 1001,\n" +
                "            \"nemotecnico\": \"PUB NO FINANC\",\n" +
                "            \"shortDesc\": \"In the country Publ No Financ\",\n" +
                "            \"id\": 500001\n" +
                "        },\n" +
                "        \"checksAllowedFlag\": false,\n" +
                "        \"lastTxnExtDbDate\": null,\n" +
                "        \"otherDb72Hrs\": \"0.00000\",\n" +
                "        \"withdrawalRestriction\": {\n" +
                "            \"nemotecnico\": \"NOT_RESTRICTED\",\n" +
                "            \"restrictionId\": 1,\n" +
                "            \"shortDesc\": \"Without restrictions\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"nickName\": \"Morin , Megan \",\n" +
                "        \"cntNormalStt\": 0,\n" +
                "        \"deposits72Hrs\": \"0.00000\",\n" +
                "        \"subproduct\": {\n" +
                "            \"product\": {\n" +
                "                \"productId\": \"01000\",\n" +
                "                \"shortDesc\": \"Caja de Ahorros\",\n" +
                "                \"id\": 4,\n" +
                "                \"isoProduct\": {\n" +
                "                    \"officialId\": \"PASIVAS\",\n" +
                "                    \"isoProductTypeId\": 10,\n" +
                "                    \"nemotecnico\": \"C_AHORRO\",\n" +
                "                    \"shortDesc\": \"Savings bank\",\n" +
                "                    \"id\": 3,\n" +
                "                    \"longDesc\": \"Caja de Ahorros\"\n" +
                "                },\n" +
                "                \"longDesc\": \"Caja de Ahorros\",\n" +
                "                \"productIsOwn\": true\n" +
                "            },\n" +
                "            \"subproductId\": \"10001\",\n" +
                "            \"nemotecnico\": null,\n" +
                "            \"shortDesc\": \"SAVINGS BANK\",\n" +
                "            \"id\": 127,\n" +
                "            \"longDesc\": \"CAJA DE AHORROS\"\n" +
                "        },\n" +
                "        \"forcedSttFlag\": false,\n" +
                "        \"otherCr24Hrs\": \"0.00000\",\n" +
                "        \"cntWithdrawal\": 0,\n" +
                "        \"checks72Hrs\": \"0.00000\",\n" +
                "        \"todayOtherDb\": \"0.00000\",\n" +
                "        \"balanceYesterday\": \"0.00000\",\n" +
                "        \"name\": \"Morin , Megan \",\n" +
                "        \"donotDormantFlag\": true,\n" +
                "        \"specialRates\": false,\n" +
                "        \"dataCompleteCustomer\": null,\n" +
                "        \"otherCr48Hrs\": \"0.00000\",\n" +
                "        \"payrollBalance\": \"0.00000\",\n" +
                "        \"deposits96Hrs\": \"0.00000\",\n" +
                "        \"otherCr96Hrs\": \"0.00000\",\n" +
                "        \"systemUser\": \" \",\n" +
                "        \"tomorrowDb\": \"0.00000\",\n" +
                "        \"tomorrowAgreements\": \"0.00000\",\n" +
                "        \"checks24Hrs\": \"0.00000\",\n" +
                "        \"checks96Hrs\": \"0.00000\",\n" +
                "        \"todayDeposits24Hrs\": \"0.00000\",\n" +
                "        \"inhibitCheckbookFlag\": true,\n" +
                "        \"interbranchAllowed\": true,\n" +
                "        \"deposits48Hrs\": \"0.00000\",\n" +
                "        \"customerNumberReached\": 1,\n" +
                "        \"cntOnpYtd\": 0,\n" +
                "        \"openingDate\": \"20090323000000000\",\n" +
                "        \"todayDeposits72Hrs\": \"0.00000\",\n" +
                "        \"lastSttBalance\": \"0.00000\",\n" +
                "        \"dbRestriction\": {\n" +
                "            \"nemotecnico\": \"NOT_RESTRICTED\",\n" +
                "            \"restrictionId\": 1,\n" +
                "            \"shortDesc\": \"Without restrictions\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"sttType\": {\n" +
                "            \"nemotecnico\": \"NO APLICA\",\n" +
                "            \"summaryTypeId\": 4,\n" +
                "            \"shortDesc\": \"NO PRINTS\",\n" +
                "            \"id\": 4\n" +
                "        },\n" +
                "        \"otherDb24Hrs\": \"0.00000\",\n" +
                "        \"isoProduct\": {\n" +
                "            \"officialId\": \"PASIVAS\",\n" +
                "            \"isoProductTypeId\": 10,\n" +
                "            \"nemotecnico\": \"C_AHORRO\",\n" +
                "            \"shortDesc\": \"Savings bank\",\n" +
                "            \"id\": 3\n" +
                "        },\n" +
                "        \"situationCode\": {\n" +
                "            \"nemotecnico\": \"NORMAL\",\n" +
                "            \"shortDesc\": \"Normal\",\n" +
                "            \"id\": 1,\n" +
                "            \"situationCodeId\": 1\n" +
                "        },\n" +
                "        \"cntChkFormalRejectYtd\": 0,\n" +
                "        \"cntSpecialStt\": 0,\n" +
                "        \"bukNumber\": \"49900013110000000457010\",\n" +
                "        \"lastTxnIntCrDate\": \"20090323000000000\",\n" +
                "        \"availableBalProcessTime\": \"19691231210000000\",\n" +
                "        \"lastPageEmitted\": 0,\n" +
                "        \"crRestriction\": {\n" +
                "            \"nemotecnico\": \"NOT_RESTRICTED\",\n" +
                "            \"restrictionId\": 1,\n" +
                "            \"shortDesc\": \"Without restrictions\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"centralBankActivityCode\": {\n" +
                "            \"centralBankActivityCodeId\": 0,\n" +
                "            \"nemotecnico\": \"No Informada\",\n" +
                "            \"shortDesc\": \"uninformed\",\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"cntChkFormalRejectMtd\": 0,\n" +
                "        \"tomorrowCr\": \"0.00000\",\n" +
                "        \"specialFees\": false\n" +
                "    }\n" +
                "}"
        );
    }

    private JSONObject getResponseListLoanTypeOk() {
        return new JSONObject("{\n" +
                "   \"project\": \"Tech\",\n" +
                "   \"responseCode\": \"massiveSelectSubproductFor_Loan_Standard\",\n" +
                "   \"transactionId\": \"massiveSelectSubproductFor_Loan_Standard\",\n" +
                "   \"transactionVersion\": \"1.0\",\n" +
                "   \"out.subproduct_list\":    {\n" +
                "      \"collection\":       [\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323152613232\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"930\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20200227152613232\",\n" +
                "            \"subproductId\": \"09014\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"POSPONE\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"id\": 453495,\n" +
                "            \"warrantyReqFlag\": false,\n" +
                "            \"valuedDateTxnMaxDays\": null,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1002,\n" +
                "               \"nemotecnico\": \"PUB FINANCIERO\",\n" +
                "               \"shortDesc\": \"In the Publ Country Financ\",\n" +
                "               \"id\": 500002,\n" +
                "               \"longDesc\": \"\\tResidentes en el paÃs - Sector pÃºblico financiero\\t\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": true,\n" +
                "            \"valuedDateTxnAllowed\": true,\n" +
                "            \"dateFrom\": \"20080102000000000\",\n" +
                "            \"sttMaxLines\": 20,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"1 ATB TEST FRANCES\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"ESTEFANIA\",\n" +
                "               \"id\": 300870\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": true,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"ATB TEST FRANCESS\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323171053815\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"930\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20200304171053815\",\n" +
                "            \"subproductId\": \"09000\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 23,\n" +
                "            \"warrantyReqFlag\": true,\n" +
                "            \"valuedDateTxnMaxDays\": null,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1002,\n" +
                "               \"nemotecnico\": \"PUB FINANCIERO\",\n" +
                "               \"shortDesc\": \"In the Publ Country Financ\",\n" +
                "               \"id\": 500002,\n" +
                "               \"longDesc\": \"\\tResidentes en el paÃs - Sector pÃºblico financiero\\t\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": false,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": true,\n" +
                "            \"dateFrom\": \"20080102000000000\",\n" +
                "            \"sttMaxLines\": 5,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"1 LOAN RATE VAR FRANCES\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"PATRICIO BUTTA\",\n" +
                "               \"id\": 451214\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"PRESTAMO TASA VDA FRANCES\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323181857768\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"valuedDateTxnMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"930\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20200219181857768\",\n" +
                "            \"subproductId\": \"09004\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 403,\n" +
                "            \"warrantyReqFlag\": false,\n" +
                "            \"valuedDateTxnMaxDays\": 999,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1002,\n" +
                "               \"nemotecnico\": \"PUB FINANCIERO\",\n" +
                "               \"shortDesc\": \"In the Publ Country Financ\",\n" +
                "               \"id\": 500002,\n" +
                "               \"longDesc\": \"\\tResidentes en el paÃs - Sector pÃºblico financiero\\t\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": true,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 10,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"1 PRESTAMO TASA VDA FRANC\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"PATRICIO BUTTA\",\n" +
                "               \"id\": 451214\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"PRESTAMO TASA VDA FRANCES CFT\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323170017095\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"valuedDateTxnMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"834\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20200309170017096\",\n" +
                "            \"subproductId\": \"09009\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 443,\n" +
                "            \"warrantyReqFlag\": true,\n" +
                "            \"valuedDateTxnMaxDays\": 999,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1002,\n" +
                "               \"nemotecnico\": \"PUB FINANCIERO\",\n" +
                "               \"shortDesc\": \"In the Publ Country Financ\",\n" +
                "               \"id\": 500002,\n" +
                "               \"longDesc\": \"\\tResidentes en el paÃs - Sector pÃºblico financiero\\t\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": true,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 3,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"1 REF RATE VDA FRANCES\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"HERRERA RODOLFO\",\n" +
                "               \"id\": 300990\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"PRESTAMOS DE REFINANCIACION TASA VDA FRANCES\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323101653844\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"valuedDateTxnMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"834\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20191113101653844\",\n" +
                "            \"subproductId\": \"09016\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 453421,\n" +
                "            \"warrantyReqFlag\": true,\n" +
                "            \"valuedDateTxnMaxDays\": 999,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1001,\n" +
                "               \"nemotecnico\": \"PUB NO FINANC\",\n" +
                "               \"shortDesc\": \"In the country Publ No Financ\",\n" +
                "               \"id\": 500001,\n" +
                "               \"longDesc\": \"Residentes en el paÃs - Sector pÃºblico no financiero\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": true,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 2,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"2 ATB TEST ALEMAN\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"HERRERA RODOLFO\",\n" +
                "               \"id\": 300990\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"ATB TEST ALEMAN\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323103814149\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"834\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20191113103814149\",\n" +
                "            \"subproductId\": \"09002\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 168,\n" +
                "            \"warrantyReqFlag\": false,\n" +
                "            \"valuedDateTxnMaxDays\": null,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1001,\n" +
                "               \"nemotecnico\": \"PUB NO FINANC\",\n" +
                "               \"shortDesc\": \"In the country Publ No Financ\",\n" +
                "               \"id\": 500001,\n" +
                "               \"longDesc\": \"Residentes en el paÃs - Sector pÃºblico no financiero\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": false,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 2,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"2 LOAN RATE VDA ALEMAN\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"HERRERA RODOLFO\",\n" +
                "               \"id\": 300990\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"PRESTAMO TASA VENCIDA ALEMAN\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323102136705\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"valuedDateTxnMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"834\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20191111102136705\",\n" +
                "            \"subproductId\": \"09015\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"POSPONE\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"id\": 453461,\n" +
                "            \"warrantyReqFlag\": true,\n" +
                "            \"valuedDateTxnMaxDays\": 999,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1001,\n" +
                "               \"nemotecnico\": \"PUB NO FINANC\",\n" +
                "               \"shortDesc\": \"In the country Publ No Financ\",\n" +
                "               \"id\": 500001,\n" +
                "               \"longDesc\": \"Residentes en el paÃs - Sector pÃºblico no financiero\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": true,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 2,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"3 ATB TEST AMERICANO\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"PATRICIO BUTTA\",\n" +
                "               \"id\": 451214\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"ATB TEST AMERICANO\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": true,\n" +
                "            \"statusDate\": \"20090323110734616\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"valuedDateTxnMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"834\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20200131110734616\",\n" +
                "            \"subproductId\": \"09001\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": true,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 143,\n" +
                "            \"warrantyReqFlag\": false,\n" +
                "            \"valuedDateTxnMaxDays\": 999,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1001,\n" +
                "               \"nemotecnico\": \"PUB NO FINANC\",\n" +
                "               \"shortDesc\": \"In the country Publ No Financ\",\n" +
                "               \"id\": 500001,\n" +
                "               \"longDesc\": \"Residentes en el paÃs - Sector pÃºblico no financiero\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": true,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 0,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"5 DIRECT VDA\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"HERRERA RODOLFO\",\n" +
                "               \"id\": 300990\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"DIRECTO VDA\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": true,\n" +
                "            \"statusDate\": \"20090323091628967\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"834\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20191111091628967\",\n" +
                "            \"subproductId\": \"09025\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": true,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 371905,\n" +
                "            \"warrantyReqFlag\": false,\n" +
                "            \"valuedDateTxnMaxDays\": null,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1001,\n" +
                "               \"nemotecnico\": \"PUB NO FINANC\",\n" +
                "               \"shortDesc\": \"In the country Publ No Financ\",\n" +
                "               \"id\": 500001,\n" +
                "               \"longDesc\": \"Residentes en el paÃs - Sector pÃºblico no financiero\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": false,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 0,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"5 DIRECTO ESPECIAL\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"HERRERA RODOLFO\",\n" +
                "               \"id\": 300990\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"ALICIA DIRECTO\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323134733355\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"valuedDateTxnMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"993305684\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20191114134733356\",\n" +
                "            \"subproductId\": \"09010\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"POSPONE\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"id\": 183,\n" +
                "            \"warrantyReqFlag\": true,\n" +
                "            \"valuedDateTxnMaxDays\": 999,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1002,\n" +
                "               \"nemotecnico\": \"PUB FINANCIERO\",\n" +
                "               \"shortDesc\": \"In the Publ Country Financ\",\n" +
                "               \"id\": 500002,\n" +
                "               \"longDesc\": \"\\tResidentes en el paÃs - Sector pÃºblico financiero\\t\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": true,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 99,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20090322000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"90 FRANCES TARIFARIO\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"MARYBEL GISELA LÃ“PEZ\",\n" +
                "               \"id\": 531634\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": true,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"PRUEBAS TARIFARIO\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323102607094\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"valuedDateTxnMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"993305684\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20190902102607094\",\n" +
                "            \"subproductId\": \"09011\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 376194,\n" +
                "            \"warrantyReqFlag\": false,\n" +
                "            \"valuedDateTxnMaxDays\": 999,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1002,\n" +
                "               \"nemotecnico\": \"PUB FINANCIERO\",\n" +
                "               \"shortDesc\": \"In the Publ Country Financ\",\n" +
                "               \"id\": 500002,\n" +
                "               \"longDesc\": \"\\tResidentes en el paÃs - Sector pÃºblico financiero\\t\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": true,\n" +
                "            \"valuedDateTxnAllowed\": true,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 0,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"ATB TEST CUOTA FIJA\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"HERRERA RODOLFO\",\n" +
                "               \"id\": 300990\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"ATB TEST CUOTA FIJA\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": false,\n" +
                "            \"statusDate\": \"20090323201329601\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNICA\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"993305684\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20200103201329601\",\n" +
                "            \"subproductId\": \"99913\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": false,\n" +
                "            \"nemotecnico\": null,\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 516038,\n" +
                "            \"warrantyReqFlag\": false,\n" +
                "            \"valuedDateTxnMaxDays\": null,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1009,\n" +
                "               \"nemotecnico\": \"NoInformada\",\n" +
                "               \"shortDesc\": \"NoInformada\",\n" +
                "               \"id\": 405284,\n" +
                "               \"longDesc\": \"NoInformada\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": false,\n" +
                "            \"dateFrom\": \"20100105000000000\",\n" +
                "            \"sttMaxLines\": 0,\n" +
                "            \"sttMonthNbr\": 0,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_IN_MONTH\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"dateTo\": \"20501231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"AUTOMATIC\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"PATRICIO BUTTA\",\n" +
                "               \"id\": 451214\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": false,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"AUTOMATIC\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         },\n" +
                "                  {\n" +
                "            \"specialCrInterestFlag\": true,\n" +
                "            \"statusDate\": \"20090323160849825\",\n" +
                "            \"officialId\": null,\n" +
                "            \"isConditionUpdateAllowed\": false,\n" +
                "            \"accountingStrip\": null,\n" +
                "            \"automNbrAcctMode\":             {\n" +
                "               \"nemotecnico\": \"UNO\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"isFundsSettlementAllowed\": false,\n" +
                "            \"systemUser\": null,\n" +
                "            \"isPackagesAllowed\": false,\n" +
                "            \"isForcedBusinessSector\": false,\n" +
                "            \"stampAdditional\": \"993305684\",\n" +
                "            \"automNbrAcctFlag\": true,\n" +
                "            \"stampDateTime\": \"20190905160849825\",\n" +
                "            \"subproductId\": \"1212\",\n" +
                "            \"isInterbranchAllowed\": true,\n" +
                "            \"sttIssueFlag\": true,\n" +
                "            \"nemotecnico\": \"PRESTAMO\",\n" +
                "            \"crmInfo\": null,\n" +
                "            \"holidayAnalisysType\":             {\n" +
                "               \"nemotecnico\": \"ANTEPONE\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"id\": 521579,\n" +
                "            \"warrantyReqFlag\": false,\n" +
                "            \"valuedDateTxnMaxDays\": null,\n" +
                "            \"businessSector\":             {\n" +
                "               \"businessSectorId\": 1001,\n" +
                "               \"nemotecnico\": \"PUB NO FINANC\",\n" +
                "               \"shortDesc\": \"In the country Publ No Financ\",\n" +
                "               \"id\": 500001,\n" +
                "               \"longDesc\": \"Residentes en el paÃs - Sector pÃºblico no financiero\"\n" +
                "            },\n" +
                "            \"product\":             {\n" +
                "               \"statusDate\": \"20090203123819187\",\n" +
                "               \"productId\": \"09001\",\n" +
                "               \"systemUser\": \"1\",\n" +
                "               \"dateFrom\": \"20010101000000000\",\n" +
                "               \"isoProduct\": {\"id\": 11},\n" +
                "               \"productIsOwn\": true,\n" +
                "               \"stampAdditional\": null,\n" +
                "               \"institution\": {\"id\": 2},\n" +
                "               \"stampDateTime\": \"20091201123819203\",\n" +
                "               \"crmInfo\": null,\n" +
                "               \"dateTo\": \"20781231000000000\",\n" +
                "               \"shortDesc\": \"Loans\",\n" +
                "               \"id\": 10,\n" +
                "               \"stampUser\": {\"id\": 1},\n" +
                "               \"detailTableName\": \"LOAN\",\n" +
                "               \"longDesc\": \"Prestamos\",\n" +
                "               \"crmInfoExpiryDate\": null,\n" +
                "               \"status\": {\"id\": 7}\n" +
                "            },\n" +
                "            \"batchProcessFlag\": false,\n" +
                "            \"sttDayNbr\": null,\n" +
                "            \"isForAllBranches\": true,\n" +
                "            \"specialDbInterestFlag\": false,\n" +
                "            \"valuedDateTxnAllowed\": false,\n" +
                "            \"dateFrom\": \"20010101000000000\",\n" +
                "            \"sttMaxLines\": 0,\n" +
                "            \"sttMonthNbr\": 6,\n" +
                "            \"valuedDateIntMaxDays\": null,\n" +
                "            \"specialComissionFlag\": false,\n" +
                "            \"valuedDateIntMode\":             {\n" +
                "               \"nemotecnico\": \"VD_UNLIMIT\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"dateTo\": \"20781231000000000\",\n" +
                "            \"sttInterval\": null,\n" +
                "            \"shortDesc\": \"DIRECT VDA CLONE TEST\",\n" +
                "            \"stampUser\":             {\n" +
                "               \"name\": \"TECHNISYS\",\n" +
                "               \"id\": 2\n" +
                "            },\n" +
                "            \"acceptOtherCurrency\": true,\n" +
                "            \"currencyCode\":             {\n" +
                "               \"shortDesc\": \"Local currency\",\n" +
                "               \"id\": 1\n" +
                "            },\n" +
                "            \"longDesc\": \"DIRECT VDA CLONE TEST\",\n" +
                "            \"crmInfoExpiryDate\": null,\n" +
                "            \"status\":             {\n" +
                "               \"nemotecnico\": \"VIGENTE\",\n" +
                "               \"id\": 7\n" +
                "            }\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"Subproduct\",\n" +
                "      \"collectionEntityVersion\": \"1.0\",\n" +
                "      \"collectionEntityDataModel\": \"product.financials\"\n" +
                "   }\n" +
                "}");
    }

    public JSONObject getResponseRequestWithError() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"insertLoanNormal_CMM\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": null,\n" +
                "                \"description\": \"Parse error: ParserConfigurationException: Service not found: insertLoanNormal_CMM[Tech]\",\n" +
                "                \"id\": 0,\n" +
                "                \"codigoerror\": null\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}");
    }

    public JSONObject getResponseLoanTypetWithError() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"massiveSelectSubproductFor_Loan_Standard\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": null,\n" +
                "                \"description\": \"Parse error: ParserConfigurationException: Service not found: massiveSelectSubproductFor_Loan_Standard[Tech]\",\n" +
                "                \"id\": 0,\n" +
                "                \"codigoerror\": null\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}");
    }

    private JSONObject getResponseListLoanTypeListEmpty() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectSubproductFor_Loan_Standard\",\n" +
                "    \"transactionId\": \"massiveSelectSubproductFor_Loan_Standard\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.bank_list\": {\n" +
                "        \"collectionEntityId\": \"Bank\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}");
    }

    private JSONObject getResponseReadLoanOk() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"singleSelectLoanCMM\",\n" +
                "    \"transactionId\": \"singleSelectLoanCMM\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.loan_approval_history_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144650251\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"approvalDate\": \"20090323000000000\",\n" +
                "                \"approvalEntity\": \"TXN\",\n" +
                "                \"comment3\": 0,\n" +
                "                \"comment2\": \"GRANT\",\n" +
                "                \"comment1\": \"Aprobacion de la operacion: 1350\",\n" +
                "                \"transactionNumber\": 1,\n" +
                "                \"stampAdditional\": \"376\",\n" +
                "                \"stampDateTime\": \"20200122144650251\",\n" +
                "                \"userLevel\": {\n" +
                "                    \"id\": 0\n" +
                "                },\n" +
                "                \"approvalComment\": null,\n" +
                "                \"id\": 593311,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 43\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323114240869\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"approvalDate\": \"20090323000000000\",\n" +
                "                \"approvalEntity\": \"TXN\",\n" +
                "                \"comment3\": 0,\n" +
                "                \"comment2\": \"PAYMENT\",\n" +
                "                \"comment1\": \"Aprobacion del pago de cuota ope.: 1350\",\n" +
                "                \"transactionNumber\": 2,\n" +
                "                \"stampAdditional\": \"379\",\n" +
                "                \"stampDateTime\": \"20200220114240869\",\n" +
                "                \"userLevel\": {\n" +
                "                    \"id\": 0\n" +
                "                },\n" +
                "                \"approvalComment\": null,\n" +
                "                \"id\": 11866601,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 451214\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 43\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"LoanApprovalHistory\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    },\n" +
                "    \"out.loan_charge_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"amountType\": {\n" +
                "                    \"id\": 16\n" +
                "                },\n" +
                "                \"statusDate\": \"20090323144650191\",\n" +
                "                \"currencyChargeAmount\": \"0.00000\",\n" +
                "                \"currencyChargePurchasePrice\": \"0.00000\",\n" +
                "                \"isManual\": false,\n" +
                "                \"loanTxn\": {\n" +
                "                    \"id\": 593291\n" +
                "                },\n" +
                "                \"stampAdditional\": \"376\",\n" +
                "                \"currencyChargeSellingPrice\": \"0.00000\",\n" +
                "                \"loanInstallmentPayment\": {\n" +
                "                    \"id\": 593292\n" +
                "                },\n" +
                "                \"currencyChargeDate\": null,\n" +
                "                \"stampDateTime\": \"20200122144650191\",\n" +
                "                \"currencyChargePriceUnity\": null,\n" +
                "                \"observations\": null,\n" +
                "                \"currencyCharge\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"pureAction\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"chargeAmount\": \"20.00000\",\n" +
                "                \"id\": 593306,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"chargeCode\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 20\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"amountType\": {\n" +
                "                    \"id\": 17\n" +
                "                },\n" +
                "                \"statusDate\": \"20090323144650172\",\n" +
                "                \"currencyChargeAmount\": \"0.00000\",\n" +
                "                \"currencyChargePurchasePrice\": \"0.00000\",\n" +
                "                \"isManual\": false,\n" +
                "                \"loanTxn\": {\n" +
                "                    \"id\": 593291\n" +
                "                },\n" +
                "                \"stampAdditional\": \"376\",\n" +
                "                \"currencyChargeSellingPrice\": \"0.00000\",\n" +
                "                \"loanInstallmentPayment\": {\n" +
                "                    \"id\": 593292\n" +
                "                },\n" +
                "                \"currencyChargeDate\": null,\n" +
                "                \"stampDateTime\": \"20200122144650172\",\n" +
                "                \"currencyChargePriceUnity\": null,\n" +
                "                \"observations\": null,\n" +
                "                \"currencyCharge\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"pureAction\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"chargeAmount\": \"12.00000\",\n" +
                "                \"id\": 593308,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"chargeCode\": {\n" +
                "                    \"id\": 4\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 20\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"amountType\": {\n" +
                "                    \"id\": 15\n" +
                "                },\n" +
                "                \"statusDate\": \"20090323144650181\",\n" +
                "                \"currencyChargeAmount\": \"0.00000\",\n" +
                "                \"currencyChargePurchasePrice\": \"0.00000\",\n" +
                "                \"isManual\": false,\n" +
                "                \"loanTxn\": {\n" +
                "                    \"id\": 593291\n" +
                "                },\n" +
                "                \"stampAdditional\": \"376\",\n" +
                "                \"currencyChargeSellingPrice\": \"0.00000\",\n" +
                "                \"loanInstallmentPayment\": {\n" +
                "                    \"id\": 593292\n" +
                "                },\n" +
                "                \"currencyChargeDate\": null,\n" +
                "                \"stampDateTime\": \"20200122144650181\",\n" +
                "                \"currencyChargePriceUnity\": null,\n" +
                "                \"observations\": null,\n" +
                "                \"currencyCharge\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"pureAction\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"chargeAmount\": \"20.00000\",\n" +
                "                \"id\": 593307,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"chargeCode\": {\n" +
                "                    \"id\": 37\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 20\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"amountType\": {\n" +
                "                    \"id\": 16\n" +
                "                },\n" +
                "                \"statusDate\": \"20090323114240695\",\n" +
                "                \"currencyChargeAmount\": \"0.00000\",\n" +
                "                \"currencyChargePurchasePrice\": \"0.00000\",\n" +
                "                \"isManual\": false,\n" +
                "                \"loanTxn\": {\n" +
                "                    \"id\": 11866441\n" +
                "                },\n" +
                "                \"stampAdditional\": \"379\",\n" +
                "                \"currencyChargeSellingPrice\": \"0.00000\",\n" +
                "                \"loanInstallmentPayment\": {\n" +
                "                    \"id\": 11866381\n" +
                "                },\n" +
                "                \"currencyChargeDate\": null,\n" +
                "                \"stampDateTime\": \"20200220114240695\",\n" +
                "                \"currencyChargePriceUnity\": null,\n" +
                "                \"observations\": null,\n" +
                "                \"currencyCharge\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"pureAction\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"chargeAmount\": \"20.00000\",\n" +
                "                \"id\": 11866661,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 451214\n" +
                "                },\n" +
                "                \"chargeCode\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 20\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"LoanCharge\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    },\n" +
                "    \"out.cet_tem_siva\": 7.17,\n" +
                "    \"out.total_amount\": 220.0,\n" +
                "    \"out.cet_tem\": 1.0,\n" +
                "    \"out.loan_particular_rate_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144649566\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"rateOrderNumber\": 1,\n" +
                "                \"spreadValue\": \"5.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"accrualBaseCalc\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"dateFrom\": \"20090323000000000\",\n" +
                "                \"accrualType\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"rateValue\": \"66.00000\",\n" +
                "                \"stampAdditional\": \"387\",\n" +
                "                \"rateType\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"stampDateTime\": \"20200122144649566\",\n" +
                "                \"spreadType\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"rate\": {\n" +
                "                    \"id\": 453613\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"rateUse\": {\n" +
                "                    \"id\": 4\n" +
                "                },\n" +
                "                \"id\": 593286,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"algorithm\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144649571\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"rateOrderNumber\": 1,\n" +
                "                \"spreadValue\": \"0.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"accrualBaseCalc\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"dateFrom\": \"20090323000000000\",\n" +
                "                \"accrualType\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"rateValue\": \"40.00000\",\n" +
                "                \"stampAdditional\": \"387\",\n" +
                "                \"rateType\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"stampDateTime\": \"20200122144649571\",\n" +
                "                \"spreadType\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"rateUse\": {\n" +
                "                    \"id\": 5\n" +
                "                },\n" +
                "                \"id\": 593287,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"algorithm\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144649575\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"rateOrderNumber\": 1,\n" +
                "                \"spreadValue\": \"0.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"accrualBaseCalc\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"dateFrom\": \"20090323000000000\",\n" +
                "                \"accrualType\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"rateValue\": \"20.00000\",\n" +
                "                \"stampAdditional\": \"387\",\n" +
                "                \"rateType\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"stampDateTime\": \"20200122144649575\",\n" +
                "                \"spreadType\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"rateUse\": {\n" +
                "                    \"id\": 6\n" +
                "                },\n" +
                "                \"id\": 593288,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"algorithm\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"LoanParticularRate\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    },\n" +
                "    \"out.taxano\": 5.42,\n" +
                "    \"out.cet_tna_siva\": 87.235,\n" +
                "    \"out.iof\": 0.0,\n" +
                "    \"out.loan_txn_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144650200\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"totalSubsidy\": \"0.00000\",\n" +
                "                \"prorateInstallmentFlag\": false,\n" +
                "                \"transactionNumber\": 1,\n" +
                "                \"channel\": {\n" +
                "                    \"id\": 11\n" +
                "                },\n" +
                "                \"currentBranch\": {\n" +
                "                    \"id\": 0\n" +
                "                },\n" +
                "                \"totalBonification\": \"0.00000\",\n" +
                "                \"stampAdditional\": \"376\",\n" +
                "                \"transactionType\": {\n" +
                "                    \"id\": 4\n" +
                "                },\n" +
                "                \"totalAmount\": \"2000.00000\",\n" +
                "                \"advanceTermFlag\": false,\n" +
                "                \"totalCharge\": \"52.00000\",\n" +
                "                \"stampDateTime\": \"20200122144650200\",\n" +
                "                \"id\": 593291,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"txnValueDate\": \"20090323000000000\",\n" +
                "                \"txnDate\": \"20090323000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"id\": 18\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323114240800\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"totalSubsidy\": \"0.00000\",\n" +
                "                \"prorateInstallmentFlag\": false,\n" +
                "                \"transactionNumber\": 2,\n" +
                "                \"channel\": {\n" +
                "                    \"id\": 11\n" +
                "                },\n" +
                "                \"currentBranch\": {\n" +
                "                    \"id\": 0\n" +
                "                },\n" +
                "                \"totalBonification\": \"0.00000\",\n" +
                "                \"stampAdditional\": \"379\",\n" +
                "                \"transactionType\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"totalAmount\": \"236.41000\",\n" +
                "                \"advanceTermFlag\": false,\n" +
                "                \"totalCharge\": \"20.00000\",\n" +
                "                \"stampDateTime\": \"20200220114240800\",\n" +
                "                \"statusReason\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"id\": 11866441,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 451214\n" +
                "                },\n" +
                "                \"txnValueDate\": \"20090323000000000\",\n" +
                "                \"txnDate\": \"20090323000000000\",\n" +
                "                \"status\": {\n" +
                "                    \"id\": 20\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"LoanTxn\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    },\n" +
                "    \"out.operation_related_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144641232\",\n" +
                "                \"subproductRelated\": {\n" +
                "                    \"id\": 306267\n" +
                "                },\n" +
                "                \"activity\": {\n" +
                "                    \"id\": 59\n" +
                "                },\n" +
                "                \"orderId\": 2728,\n" +
                "                \"currencyRelated\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"operationRelated\": \"43\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"branchRelated\": {\n" +
                "                    \"id\": 24\n" +
                "                },\n" +
                "                \"subproduct\": {\n" +
                "                    \"id\": 23\n" +
                "                },\n" +
                "                \"operationIdRelated\": \"Relacion Operacion Cuenta\",\n" +
                "                \"dateFrom\": \"20090323000000000\",\n" +
                "                \"branch\": {\n" +
                "                    \"id\": 0\n" +
                "                },\n" +
                "                \"stampAdditional\": \"1172\",\n" +
                "                \"institution\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"stampDateTime\": \"20200122144641232\",\n" +
                "                \"statusReason\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"operationId\": \"1350\",\n" +
                "                \"currency\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"id\": 593289,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144641650\",\n" +
                "                \"subproductRelated\": {\n" +
                "                    \"id\": 306267\n" +
                "                },\n" +
                "                \"activity\": {\n" +
                "                    \"id\": 156\n" +
                "                },\n" +
                "                \"orderId\": 2729,\n" +
                "                \"currencyRelated\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"operationRelated\": \"43\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"branchRelated\": {\n" +
                "                    \"id\": 24\n" +
                "                },\n" +
                "                \"subproduct\": {\n" +
                "                    \"id\": 23\n" +
                "                },\n" +
                "                \"operationIdRelated\": \"Relacion Operacion Cuenta\",\n" +
                "                \"dateFrom\": \"20090323000000000\",\n" +
                "                \"branch\": {\n" +
                "                    \"id\": 0\n" +
                "                },\n" +
                "                \"stampAdditional\": \"1173\",\n" +
                "                \"institution\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"stampDateTime\": \"20200122144641650\",\n" +
                "                \"statusReason\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"dateTo\": \"20781231000000000\",\n" +
                "                \"operationId\": \"1350\",\n" +
                "                \"currency\": {\n" +
                "                    \"id\": 1\n" +
                "                },\n" +
                "                \"id\": 593290,\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"status\": {\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"OperationRelated\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    },\n" +
                "    \"out.loan\": {\n" +
                "        \"accrualIntadjNextMonth\": \"0.00000\",\n" +
                "        \"statusDate\": \"20090323114240857\",\n" +
                "        \"crAuthorizedCustomer\": {\n" +
                "            \"id\": 537611\n" +
                "        },\n" +
                "        \"totalCapSubsidyDue\": \"0.00000\",\n" +
                "        \"automaticDbFlag\": true,\n" +
                "        \"accrualIntNextMonth\": \"0.00000\",\n" +
                "        \"interestDayNbr\": 0,\n" +
                "        \"firstCapitalInstallDate\": \"20090423000000000\",\n" +
                "        \"taxesInterval\": null,\n" +
                "        \"customerNumberDeclared\": 0,\n" +
                "        \"branch\": {\n" +
                "            \"branchId\": 1,\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"loOpeningDate\": null,\n" +
                "        \"renewalDayNbr\": null,\n" +
                "        \"loSettlementAmount\": \"0.00000\",\n" +
                "        \"accrualSubsidyIntNotAcco\": \"0.00000\",\n" +
                "        \"id\": 593284,\n" +
                "        \"termTypeGraceInstallment\": {\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"fixedInstallmentDay\": 23,\n" +
                "        \"algorithm\": {\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"totalInterestAdjustement\": \"0.00000\",\n" +
                "        \"accrualDailyFlag\": true,\n" +
                "        \"maxArrearsDaysUnpInstSubs\": 0,\n" +
                "        \"taxesMonthNbr\": null,\n" +
                "        \"operationIdReneg\": null,\n" +
                "        \"pendingDisbursementFlag\": false,\n" +
                "        \"termGraceInstallment\": 0,\n" +
                "        \"renewalInterval\": null,\n" +
                "        \"renegotiationFlag\": null,\n" +
                "        \"accrualType\": {\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"interestRateType\": {\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"inputDate\": \"20090323000000000\",\n" +
                "        \"qtyInterestInstallment\": 12,\n" +
                "        \"accountToApplyDbCr\": false,\n" +
                "        \"subsidyFlag\": false,\n" +
                "        \"lo2ndPaymentDebt\": \"71.00000\",\n" +
                "        \"interestSpreadValue\": \"5.00000\",\n" +
                "        \"linkToPackage\": false,\n" +
                "        \"status\": {\n" +
                "            \"id\": 7\n" +
                "        },\n" +
                "        \"loPrincipalAmount\": \"0.00000\",\n" +
                "        \"updateDate\": \"20090322000000000\",\n" +
                "        \"lo1stPaymentDebt\": \"0.00000\",\n" +
                "        \"renewalMonthNbr\": null,\n" +
                "        \"agreementFlag\": false,\n" +
                "        \"termType\": {\n" +
                "            \"id\": 2\n" +
                "        },\n" +
                "        \"maxAmountInstallAuthorized\": \"0.00000\",\n" +
                "        \"subsidyRateValue\": null,\n" +
                "        \"qtyCapitalInstallment\": 12,\n" +
                "        \"accrualSuspTotalIntadj\": \"0.00000\",\n" +
                "        \"stampAdditional\": \"409\",\n" +
                "        \"accrualAdjNotAccount\": \"0.00000\",\n" +
                "        \"accrualSuspendedTotalInt\": \"0.00000\",\n" +
                "        \"nextNewRateDate\": \"20090324000000000\",\n" +
                "        \"lastTxnDate\": \"20090323000000000\",\n" +
                "        \"totalCapitalizatedInterest\": \"0.00000\",\n" +
                "        \"operationId\": \"1350\",\n" +
                "        \"vatCategory\": {\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"totalTermDays\": 365,\n" +
                "        \"debitIsInconditional\": false,\n" +
                "        \"interestRate\": {\n" +
                "            \"id\": 453613\n" +
                "        },\n" +
                "        \"accrualSuspendedDate\": null,\n" +
                "        \"lo3thPaymentDebt\": \"100.00000\",\n" +
                "        \"totalDisbursedAmount\": \"2000.00000\",\n" +
                "        \"teoricDueCapitalBalance\": \"1880.30000\",\n" +
                "        \"dueCapitalBalance\": \"1880.30000\",\n" +
                "        \"accountingBranch\": {\n" +
                "            \"id\": 24\n" +
                "        },\n" +
                "        \"lastCapIntDate\": null,\n" +
                "        \"maxUnpaidSubsidyInstallment\": null,\n" +
                "        \"accrualSubsidyIntNextMonth\": \"0.00000\",\n" +
                "        \"nextInstallmentDate\": \"23052009\",\n" +
                "        \"totalPaymentDecress\": \"0.00000\",\n" +
                "        \"interestInterval\": 2,\n" +
                "        \"loOtherAmount\": \"2000.00000\",\n" +
                "        \"closedDate\": \"20100323000000000\",\n" +
                "        \"capitalTerm\": 1,\n" +
                "        \"stampUser\": {\n" +
                "            \"id\": 451214\n" +
                "        },\n" +
                "        \"totalCapitalAdjustement\": \"0.00000\",\n" +
                "        \"interestSpreadType\": {\n" +
                "            \"id\": 2\n" +
                "        },\n" +
                "        \"currencyCode\": {\n" +
                "            \"currAcronym\": \"MNL\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"interestPeriodType\": {\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"agreementActiveFlag\": false,\n" +
                "        \"accrualBaseCalc\": {\n" +
                "            \"id\": 2\n" +
                "        },\n" +
                "        \"initialDisbursedAmount\": \"2000.00000\",\n" +
                "        \"currentIndex\": \"1.00000\",\n" +
                "        \"loCurrentPaymentAmount\": \"236.41000\",\n" +
                "        \"interestMonthNbr\": null,\n" +
                "        \"loCrmInfoExpiryDate\": null,\n" +
                "        \"stampDateTime\": \"20200220114310973\",\n" +
                "        \"fundDestiny\": {\n" +
                "            \"id\": 5\n" +
                "        },\n" +
                "        \"loDescription\": \"BIRCHE JUNIOR, ADEMIR \",\n" +
                "        \"firstInterestInstallDate\": \"20090423000000000\",\n" +
                "        \"taxesDayNbr\": null,\n" +
                "        \"businessSector\": {\n" +
                "            \"id\": 500003\n" +
                "        },\n" +
                "        \"accrualIntNotAccount\": \"0.00000\",\n" +
                "        \"totalCapitalTeoricAdjust\": \"0.00000\",\n" +
                "        \"accrualAdjNextMonth\": \"99.26000\",\n" +
                "        \"dueInterestBalance\": \"0.00000\",\n" +
                "        \"originIndex\": \"7.17000\",\n" +
                "        \"subproduct\": {\n" +
                "            \"product\": {\n" +
                "                \"productId\": \"09001\"\n" +
                "            },\n" +
                "            \"subproductId\": \"09000\",\n" +
                "            \"id\": 23\n" +
                "        },\n" +
                "        \"qtyGraceInstallment\": 0,\n" +
                "        \"loOtherAmountDueDate\": \"19691231210000000\",\n" +
                "        \"unpaidQuotSubsidyFlag\": false,\n" +
                "        \"specialRates\": true,\n" +
                "        \"approvalDate\": \"20090323000000000\",\n" +
                "        \"loanType\": {\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"loCrmInfo\": null,\n" +
                "        \"systemUser\": null,\n" +
                "        \"totalIntSubsidyDue\": \"0.00000\",\n" +
                "        \"totalOperationAmount\": \"2000.00000\",\n" +
                "        \"fundOrigin\": {\n" +
                "            \"id\": 3\n" +
                "        },\n" +
                "        \"interestPeriodVariable\": 1,\n" +
                "        \"expiryDate\": \"20100323000000000\",\n" +
                "        \"amortizationType\": {\n" +
                "            \"id\": 7\n" +
                "        },\n" +
                "        \"customerNumberReached\": 0,\n" +
                "        \"loPaymentDueDate\": \"23032009\",\n" +
                "        \"accrualIntadjNotAccount\": \"0.00000\",\n" +
                "        \"fundDestinyObservations\": null,\n" +
                "        \"crDate\": \"20090323000000000\",\n" +
                "        \"interestRateValue\": \"66.00000\",\n" +
                "        \"situationCode\": {\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"interestVariableType\": {\n" +
                "            \"id\": 2\n" +
                "        },\n" +
                "        \"beginDate\": \"20090323000000000\",\n" +
                "        \"priceListProfile\": 11,\n" +
                "        \"maxQtyArrearDays\": 0,\n" +
                "        \"renewalPercentMax\": null,\n" +
                "        \"amountInstallment\": \"236.40889\",\n" +
                "        \"interestTerm\": 1,\n" +
                "        \"loFeesNotCharged\": \"2600.51000\",\n" +
                "        \"specialFees\": true\n" +
                "    },\n" +
                "    \"out.iof_adic\": 0.0,\n" +
                "    \"out.total_interest\": 836.92,\n" +
                "    \"out.loan_installment_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144641978\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"0.00000\",\n" +
                "                \"installmentInterestDue\": \"116.71000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"119.70000\",\n" +
                "                \"liInterest1\": \"0.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"2000.00000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"379\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200220114240666\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593293,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 31,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 1,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20090423000000000\",\n" +
                "                \"installmentCapitalBalance\": \"0.00000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"116.71000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 451214\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"116.71000\",\n" +
                "                \"paymentDate\": \"20090323000000000\",\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"COBRADO\",\n" +
                "                    \"id\": 20\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144641984\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"236.41000\",\n" +
                "                \"installmentInterestDue\": \"109.73000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"126.68000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"1880.30000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144641985\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593294,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 30,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 2,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20090523000000000\",\n" +
                "                \"installmentCapitalBalance\": \"126.68000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144641992\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"222.97000\",\n" +
                "                \"installmentInterestDue\": \"102.33000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"134.08000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"1753.62000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144641992\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593295,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 31,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 3,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20090623000000000\",\n" +
                "                \"installmentCapitalBalance\": \"134.08000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144641998\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"210.29000\",\n" +
                "                \"installmentInterestDue\": \"94.51000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"141.90000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"1619.54000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144641998\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593296,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 30,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 4,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20090723000000000\",\n" +
                "                \"installmentCapitalBalance\": \"141.90000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144642009\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"198.33000\",\n" +
                "                \"installmentInterestDue\": \"86.23000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"150.18000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"1477.64000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144642009\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593297,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 31,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 5,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20090823000000000\",\n" +
                "                \"installmentCapitalBalance\": \"150.18000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144642016\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"187.05000\",\n" +
                "                \"installmentInterestDue\": \"77.47000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"158.94000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"1327.46000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144642016\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593298,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 31,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 6,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20090923000000000\",\n" +
                "                \"installmentCapitalBalance\": \"158.94000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144642023\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"176.41000\",\n" +
                "                \"installmentInterestDue\": \"68.19000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"168.22000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"1168.52000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144642023\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593299,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 30,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 7,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20091023000000000\",\n" +
                "                \"installmentCapitalBalance\": \"168.22000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144642039\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"166.38000\",\n" +
                "                \"installmentInterestDue\": \"58.37000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"178.04000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"1000.30000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144642039\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593300,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 31,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 8,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20091123000000000\",\n" +
                "                \"installmentCapitalBalance\": \"178.04000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144642050\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"156.92000\",\n" +
                "                \"installmentInterestDue\": \"47.98000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"188.43000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"822.26000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144642050\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593301,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 30,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 9,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20091223000000000\",\n" +
                "                \"installmentCapitalBalance\": \"188.43000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144642058\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"148.00000\",\n" +
                "                \"installmentInterestDue\": \"36.99000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"199.42000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"633.83000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144642058\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593302,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 31,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 10,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20100123000000000\",\n" +
                "                \"installmentCapitalBalance\": \"199.42000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144642155\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"139.58000\",\n" +
                "                \"installmentInterestDue\": \"25.35000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"211.06000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"434.41000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144642155\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593303,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 31,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 11,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20100223000000000\",\n" +
                "                \"installmentCapitalBalance\": \"211.06000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"statusDate\": \"20090323144643132\",\n" +
                "                \"accrualIntNotAccountant\": \"0.00000\",\n" +
                "                \"loan\": {\n" +
                "                    \"id\": 593284\n" +
                "                },\n" +
                "                \"accrualSuspendedIntAjust\": \"131.64000\",\n" +
                "                \"installmentInterestDue\": \"13.06000\",\n" +
                "                \"accrualIntAdjAmortNotAcc\": \"0.00000\",\n" +
                "                \"installmentCapitalDue\": \"223.35000\",\n" +
                "                \"liInterest1\": \"20.00000\",\n" +
                "                \"systemUser\": null,\n" +
                "                \"liTax\": \"223.35000\",\n" +
                "                \"currentIndex\": \"1.00000\",\n" +
                "                \"installmentType\": {\n" +
                "                    \"id\": 3\n" +
                "                },\n" +
                "                \"stampAdditional\": \"406\",\n" +
                "                \"lastArrearsDate\": null,\n" +
                "                \"refinancingInstallmentNmbr\": 0,\n" +
                "                \"liIndex\": null,\n" +
                "                \"stampDateTime\": \"20200122144643132\",\n" +
                "                \"paymentOrder\": 0,\n" +
                "                \"adjustAmortCap\": \"0.00000\",\n" +
                "                \"liAdditionalCosts\": \"0.00000\",\n" +
                "                \"id\": 593304,\n" +
                "                \"liAddData\": \"NORMAL\",\n" +
                "                \"refinancingFlag\": false,\n" +
                "                \"liInterest\": \"6.03000\",\n" +
                "                \"termDays\": 28,\n" +
                "                \"accrualIntSuspended\": \"0.00000\",\n" +
                "                \"liInstallmentNumber\": 12,\n" +
                "                \"accrualIntAdjAmortCap\": \"0.00000\",\n" +
                "                \"paymentDueDate\": \"20100323000000000\",\n" +
                "                \"installmentCapitalBalance\": \"223.35000\",\n" +
                "                \"liStamps\": null,\n" +
                "                \"amountCapitalPercent\": \"0.00000\",\n" +
                "                \"capitalizedInterest\": \"0.00000\",\n" +
                "                \"liCapital\": \"99.26000\",\n" +
                "                \"liAmount\": \"71.00000\",\n" +
                "                \"liTax1\": \"0.00000\",\n" +
                "                \"stampUser\": {\n" +
                "                    \"id\": 2\n" +
                "                },\n" +
                "                \"installmentInterestBalance\": \"0.00000\",\n" +
                "                \"paymentDate\": null,\n" +
                "                \"liInsurances\": \"0.00000\",\n" +
                "                \"liFees\": \"236.41000\",\n" +
                "                \"liTax2\": \"256.41000\",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"id\": 7\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"LoanInstallment\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    },\n" +
                "    \"out.cet_tna\": 12.1665,\n" +
                "    \"out.encargos\": 52.0\n" +
                "}");
    }

    private JSONObject getResponseReadLoanEmpty() {
        return new JSONObject("{\n" +
                "   \"project\":\"Tech\",\n" +
                "   \"responseCode\":\"singleSelectLoanCMM\",\n" +
                "   \"transactionId\":\"singleSelectLoanCMM\",\n" +
                "   \"transactionVersion\":\"1.0\",\n" +
                "   \"out.loan_approval_history_list\":{\n" +
                "      \"collection\":[],\n" +
                "      \"collectionEntityId\":\"LoanApprovalHistory\",\n" +
                "      \"collectionEntityVersion\":\"1.0\",\n" +
                "      \"collectionEntityDataModel\":\"product.financials\"\n" +
                "   },\n" +
                "   \"out.loan_charge_list\":{},\n" +
                "   \"out.cet_tem_siva\":7.17,\n" +
                "   \"out.total_amount\":220.0,\n" +
                "   \"out.cet_tem\":1.0,\n" +
                "   \"out.loan_particular_rate_list\":{},\n" +
                "   \"out.taxano\":5.42,\n" +
                "   \"out.cet_tna_siva\":87.235,\n" +
                "   \"out.iof\":0.0,\n" +
                "   \"out.loan_txn_list\":{},\n" +
                "   \"out.operation_related_list\":{},\n" +
                "   \"out.loan\":{},\n" +
                "   \"out.iof_adic\":0.0,\n" +
                "   \"out.total_interest\":836.92,\n" +
                "   \"out.loan_installment_list\":{},\n" +
                "   \"out.cet_tna\":12.1665,\n" +
                "   \"out.encargos\":52.0\n" +
                "}");
    }

    public JSONObject getResponseReadLoanWithError() {
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"singleSelectLoanCMM\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": 1,\n" +
                "                \"description\": \"The operation number for the reported sub-product and/or branch does not exist.\",\n" +
                "                \"id\": 1,\n" +
                "                \"codigoerror\": 116572\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}");
    }
}