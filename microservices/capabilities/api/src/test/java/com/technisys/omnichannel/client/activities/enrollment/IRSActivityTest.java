package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.pendingactions.IRSActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.anyString;

/*
 *
 * @author Jhossept Garay
 */
@PowerMockIgnore({ "org.apache.logging.log4j.*", "javax.management.*" })
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, ExchangeTokenHandler.class, AccessManagementHandlerFactory.class })
public class IRSActivityTest {

    @Mock
    private Administration administrationMock;
    @Mock
    private AccessManagementHandler accessManagementHandlerMock;
    private static Request request;
    private IRSActivity activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        activity = new IRSActivity();
        request = new Request();

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        request.setIdActivity("enrollment.irs");
        request.setChannel("frontend");
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionUserNotFound() throws IOException, ActivityException {
        Mockito
                .when(accessManagementHandlerMock.getUserByEmail(
                        Mockito.anyString()
                ))
                .thenReturn(null);

        activity.execute(request);
    }

    @Test
    public void executeOk() throws Exception {
        String validEmail = "test@test.com";
        User user = new User();
        user.setIdUser("12345");
        user.setEmail(validEmail);
        EnvironmentUser envUser = new EnvironmentUser();
        envUser.setEmail(validEmail);
        Map<String, Object> params = new HashMap<>();
        params.put(IRSActivity.InParams.IRS, false);
        request.setParameters(params);
        request.setIdUser("12345");
        List<Environment> listEnv = new ArrayList<>();
        Environment env = new Environment();
        env.setIdEnvironment(10);
        listEnv.add(env);

        Mockito
                .when(administrationMock.readEnvironmentUserInfo(
                        Mockito.anyString(),
                        Mockito.anyInt()
                ))
                .thenReturn(envUser);
        Mockito
                .when(administrationMock.listEnvironments(
                        Mockito.anyString()
                ))
                .thenReturn(listEnv);
        Mockito
                .doNothing()
                .when(accessManagementHandlerMock)
                .updateUserIRS(Mockito.anyBoolean(),Mockito.anyObject(),Mockito.anyInt(),Mockito.anyString());


        PowerMockito.mockStatic(ExchangeTokenHandler.class);
        PowerMockito.doNothing().when(ExchangeTokenHandler.class, "delete", anyString());

        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }
}