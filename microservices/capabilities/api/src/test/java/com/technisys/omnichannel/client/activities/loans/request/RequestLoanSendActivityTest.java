/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */
package com.technisys.omnichannel.client.activities.loans.request;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankLoanConnector;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.utils.SynchronizationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.quartz.SchedulerException;
import utils.TestUtils;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, ConfigurationFactory.class, CoreLoanConnectorOrchestrator.class, CyberbankLoanConnector.class, FormsHandler.class, SynchronizationUtils.class})
public class RequestLoanSendActivityTest {

    private Request request;

    @Mock
    private Administration administrationMock;

    @Mock
    private Configuration configurationMock;

    @Mock
    private RequestLoanSendActivity activity;

    @Mock
    private FormsHandler formsHandlerMock;



    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        PowerMockito.mockStatic(FormsHandler.class);
        Mockito.when(FormsHandler.getInstance()).thenReturn(formsHandlerMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM),  anyString()))
                .thenReturn("http://WEBSERVICES_URL:PORT");

        Mockito.when(configurationMock.getString(eq(Configuration.PLATFORM), anyString()))
                .thenReturn("USD");

        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM),  anyString(), Mockito.anyInt()))
                .thenReturn(50);

        PowerMockito.mockStatic(CoreLoanConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankLoanConnector.class);
        PowerMockito.mockStatic(SynchronizationUtils.class);

        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), anyString(), anyString())).thenReturn("cyberbank");

        request = new Request();
        activity = new RequestLoanSendActivity();
    }

    @Test
    public void executeReturnOk() throws Exception {

        Environment env = new Environment();
        env.setProductGroupId("a1a1a1a1a1a");

        List<Client> clientList = new ArrayList<>();
        Client client = new Client();
        client.setIdClient("1");
        clientList.add(client);
        env.setClients(clientList);

        Account account = new Account();
        Amount amount = new Amount();
        Response responseSendFormActivity = new Response();
        responseSendFormActivity.setReturnCode(com.technisys.omnichannel.ReturnCodes.BACKEND_IS_PROCESSING_THE_TRANSACTION);

        Form form = new Form();
        form.setIdBPMProcess("demo:12");
        form.setVersion(1);

        Mockito.when(administrationMock.readEnvironment( Mockito.nullable(Integer.class))).thenReturn(env);
        Mockito.when(CoreLoanConnectorOrchestrator.request(account,"12", "12", amount, 121)).thenReturn(TestUtils.getValidLoan());
        Mockito.when(administrationMock.readProduct(Mockito.nullable(String.class), Mockito.nullable(Integer.class))).thenReturn(TestUtils.getValidProduct());
        PowerMockito.doNothing().when(SynchronizationUtils.class, "syncEnvironmentProducts", anyString(), Mockito.any(), Mockito.any());

        Map map = new LinkedHashMap();
        map.put("value", "test");
        map.put("value", "test");
        Map parameters = new LinkedHashMap();
        parameters.put("iAuthorizeDebitToAccount", map);

        request.setParameters(parameters);
        request.setIdEnvironment(21);

        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());

    }

    @Test(expected = ActivityException.class)
    public void executeReturnCyberbankException() throws ActivityException, BackendConnectorException, IOException {

        Environment env = new Environment();
        env.setProductGroupId("a1a1a1a1a1a");

        List<Client> clientList = new ArrayList<>();
        Client client = new Client();
        client.setIdClient("1");
        clientList.add(client);
        env.setClients(clientList);

        Account account = new Account();
        Amount amount = new Amount();

        Map map = new LinkedHashMap();
        map.put("value", "test");
        map.put("value", "test");
        Map parameters = new LinkedHashMap();
        parameters.put("iAuthorizeDebitToAccount", map);

        request.setParameters(parameters);
        request.setIdEnvironment(21);

        Mockito.when(administrationMock.readEnvironment( Mockito.nullable(Integer.class))).thenReturn(env);
        Mockito.when(administrationMock.readProduct(Mockito.nullable(String.class), Mockito.nullable(Integer.class))).thenReturn(TestUtils.getValidProduct());
        Mockito.when(CoreLoanConnectorOrchestrator.request(Mockito.any(), Mockito.nullable(String.class),  Mockito.nullable(String.class), Mockito.any(),  Mockito.nullable(Integer.class))).thenThrow(new BackendConnectorException("error"));
        activity.execute(request);
    }

    @Test(expected = ActivityException.class)
    public void executeReturnIOException() throws ActivityException, BackendConnectorException, IOException {
        Mockito.when(administrationMock.readEnvironment( Mockito.nullable(Integer.class))).thenThrow(new IOException());
        activity.execute(request);
    }

    @Test
    public void validateReturnOK() throws IOException, SchedulerException, ActivityException {
        Map result = new HashMap<String, String>();
        Mockito.when(formsHandlerMock.validateRequest(request)).thenReturn(result);
        assertTrue(activity.validate(request).isEmpty());
    }

    @Test(expected = ActivityException.class)
    public void validateReturnIOException() throws IOException, SchedulerException, ActivityException {
        Mockito.when(formsHandlerMock.validateRequest(Mockito.any())).thenThrow(new IOException());
        activity.validate(request);
    }

    @Test(expected = ActivityException.class)
    public void validateReturnSchedulerException() throws IOException, SchedulerException, ActivityException {
        Mockito.when(formsHandlerMock.validateRequest(Mockito.any())).thenThrow(new SchedulerException());
        activity.validate(request);
    }


}
