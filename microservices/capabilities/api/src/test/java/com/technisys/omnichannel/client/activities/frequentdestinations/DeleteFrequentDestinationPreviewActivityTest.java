package com.technisys.omnichannel.client.activities.frequentdestinations;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.junit.Assert.*;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
public class DeleteFrequentDestinationPreviewActivityTest {

    private Request request;
    private DeleteFrequentDestinationPreviewActivity activity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        request = new Request();
        activity = new DeleteFrequentDestinationPreviewActivity();
    }

    @Test
    public void returnsOkTest() throws IOException, ActivityException {
        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }
}