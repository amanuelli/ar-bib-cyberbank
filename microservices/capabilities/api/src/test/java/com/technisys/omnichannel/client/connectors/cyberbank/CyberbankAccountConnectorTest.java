package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.ProductType;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.client.handlers.notes.NotesHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.utils.CacheUtils;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({"com.technisys.omnichannel.core.utils.SecurityUtils"})
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CyberbankCoreConnector.class, CyberbankCoreRequest.class, CacheUtils.class, CyberbankConsolidateConnector.class, SecurityUtils.class, NotesHandler.class})

public class CyberbankAccountConnectorTest {

    @Mock
    private Configuration configurationMock;



    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), eq("connector.cyberbank.core.template.request"), eq("/templates/connectors/cyberbank/request/"))).thenReturn("/templates/connectors/cyberbank/request/");


        PowerMockito.mockStatic(CyberbankCoreRequest.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CyberbankAccountConnector.class);
        PowerMockito.mockStatic(CyberbankConsolidateConnector.class);
        PowerMockito.mockStatic(CacheUtils.class);
        PowerMockito.mockStatic(NotesHandler.class);


    }

    @Test
    public void testCreateAccountOK() throws  CyberbankCoreConnectorException, IOException {

        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), (CyberbankCoreRequest) Mockito.any())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"massiveSelectSubproductBy_Iso_Product\",\"transactionId\":\"massiveSelectSubproductBy_Iso_Product\",\"transactionVersion\":\"1.0\",\"out.subproduct_list\":{\"collection\":[{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00001\",\"shortDesc\":\"BOX\",\"id\":4,\"longDesc\":\"CAJA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00002\",\"shortDesc\":\"TREASURY\",\"id\":5,\"longDesc\":\"TESORERIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00010\",\"shortDesc\":\"REMESA INTERSUCURSAL\",\"id\":363,\"longDesc\":\"REMESA INTERSUCURSAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00011\",\"shortDesc\":\"INTERNAL REMESA\",\"id\":364,\"longDesc\":\"REMESA INTERNA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00112\",\"shortDesc\":\"EXTERNAL REMESA\",\"id\":365,\"longDesc\":\"REMESA EXTERNA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00013\",\"shortDesc\":\"SPEND FUNDS\",\"id\":366,\"longDesc\":\"PASE DE FONDOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"TEST\",\"shortDesc\":\"TEST BORRAR\",\"id\":527110,\"longDesc\":\"BORRAR\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"1460\",\"shortDesc\":\"C. SAVING PROD. 146\",\"id\":743,\"longDesc\":\"C. AHORRO PROD. 146\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"3105\",\"shortDesc\":\"CAJA DE AHORRO P BORRAR\",\"id\":507525,\"longDesc\":\"CAJA DE AHORRO P BORRAR\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"10001\",\"shortDesc\":\"SAVINGS BANK\",\"id\":127,\"longDesc\":\"CAJA DE AHORROS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"12000\",\"shortDesc\":\"TILT BOX\",\"id\":398580,\"longDesc\":\"CAJA GIRE\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"19990\",\"shortDesc\":\"C.AHORROS unmoving.\",\"id\":129,\"longDesc\":\"C.AHORROS INMOVIL.\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"9898\",\"shortDesc\":\"DEMO ACCOUNT\",\"id\":398013,\"longDesc\":\"ACCOUNT DEMO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"1\",\"shortDesc\":\"C. AHORRO PRUEBA\",\"id\":452891,\"longDesc\":\"CAJA DE AHORRO DE PRUEBA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"11000\",\"shortDesc\":\"CREDIT ACCOUNT\",\"id\":306267,\"longDesc\":\"CUENTA CRÉDITO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"SVACC\",\"shortDesc\":\"SVACC 1\",\"id\":418810,\"longDesc\":\"SVACC1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"1998\",\"shortDesc\":\"COIN PURSE\",\"id\":444431,\"longDesc\":\"MONEDERO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"001\",\"shortDesc\":\"DIEGO CUBA\",\"id\":462992,\"longDesc\":\"DIEGO CUBA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"278797\",\"shortDesc\":\"DIEGO_CUBA\",\"id\":463075,\"longDesc\":\"DIEGO_CUBA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"00193\",\"shortDesc\":\"C AHORRO YBRAHIM\",\"id\":487910,\"longDesc\":\"TEST YBRAHIM\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"1234\",\"shortDesc\":\"SUBPRODAHO\",\"id\":535543,\"longDesc\":\"SUPRODAHO\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"002\",\"shortDesc\":\"CA_MJ\",\"id\":534510,\"longDesc\":\"CA MAJO\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"003\",\"shortDesc\":\"CAMJ2\",\"id\":536504,\"longDesc\":\"CAMJ2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"99998\",\"shortDesc\":\"TEST\",\"id\":535939,\"longDesc\":\"TEST\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20999\",\"shortDesc\":\"ACCOUNTS CBU\",\"id\":11,\"longDesc\":\"CUENTAS POR CBU\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20004\",\"shortDesc\":\"ACCOUNTS II\",\"id\":458301,\"longDesc\":\"CUENTAS CORRIENTES II\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"200015\",\"shortDesc\":\"ACCOUNTS III\",\"id\":459284,\"longDesc\":\"CUENTAS CORRIENTES III\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"29999\",\"shortDesc\":\"VISTA Recurrence\",\"id\":408640,\"longDesc\":\"VISTA RECURRENCIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20001\",\"shortDesc\":\"CURRENT ACCOUNTS\",\"id\":128,\"longDesc\":\"CUENTAS CORRIENTES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"9980\",\"shortDesc\":\"CONTA CORRENTE TRZ\",\"id\":425853,\"longDesc\":\"CONTA CORRENTE TRZ\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"29990\",\"shortDesc\":\"Cta.cte. STILL.\",\"id\":130,\"longDesc\":\"CTA.CTE. INMOVIL.\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20003\",\"shortDesc\":\"VIRTUAL\",\"id\":447824,\"longDesc\":\"MONEDERO VIRTUAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20002\",\"shortDesc\":\"CV ROTATE\",\"id\":446798,\"longDesc\":\"CUENTA VIRTUAL GIRE\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"789\",\"shortDesc\":\"ALE\",\"id\":536334,\"longDesc\":\"ALE1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"88888\",\"shortDesc\":\"TEST2\",\"id\":536210,\"longDesc\":\"TEST2\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"888\",\"shortDesc\":\"PRUEBA3\",\"id\":536594,\"longDesc\":\"PRUEBA3MA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"99998\",\"shortDesc\":\"CC\",\"id\":535985,\"longDesc\":\"TESTMARY\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"8888888\",\"shortDesc\":\"TEST1\",\"id\":536173,\"longDesc\":\"TEST1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"55555\",\"shortDesc\":\"T11\",\"id\":536622,\"longDesc\":\"T111\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"888888\",\"shortDesc\":\"TESTM\",\"id\":536032,\"longDesc\":\"TESTM\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"9989\",\"shortDesc\":\"ASDUEBA\",\"id\":536329,\"longDesc\":\"AHJSJKASDF\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"77777\",\"shortDesc\":\"TEST3\",\"id\":536301,\"longDesc\":\"TEST3\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05000\",\"id\":1},\"subproductId\":\"84922\",\"shortDesc\":\"SECURITY\",\"id\":1,\"longDesc\":\"SEGURIDAD\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"58002\",\"shortDesc\":\"PLAZO FIJO CLONADO\",\"id\":461413,\"longDesc\":\"PLAZO FIJO CLONADO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"3232432423\",\"shortDesc\":\"FSFDFDSF\",\"id\":511031,\"longDesc\":\"FDSFDSFSD\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"58001\",\"shortDesc\":\"CDB\",\"id\":803,\"longDesc\":\"CDB\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"111132132132131\",\"shortDesc\":\"111111\",\"id\":511025,\"longDesc\":\"21212132312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"7744\",\"shortDesc\":\"ASDADASDASDASD\",\"id\":512016,\"longDesc\":\"AAASSSS\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"123467\",\"shortDesc\":\"PLAZO FIJO DK2\",\"id\":491776,\"longDesc\":\"PLAZO FIJO DK2\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"22222\",\"shortDesc\":\"CDB AUTOMATICO OK\",\"id\":496636,\"longDesc\":\"CDB AUTOMATICO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"2020\",\"shortDesc\":\"PLAZO FIJO DK\",\"id\":491760,\"longDesc\":\"PLAZO FIJO DK\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"222\",\"shortDesc\":\"BORRAR12\",\"id\":510998,\"longDesc\":\"BORRAR1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"11\",\"shortDesc\":\"BORRAR\",\"id\":510993,\"longDesc\":\"BORRAR1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"666\",\"shortDesc\":\"CODIGO DE TASAS\",\"id\":502381,\"longDesc\":\"CODIGO DE TASAS1\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"58003\",\"shortDesc\":\"PLAZO FIJO COPIA\",\"id\":462375,\"longDesc\":\"PLAZO FIJO COPIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"222222222\",\"shortDesc\":\"AA\",\"id\":509647,\"longDesc\":\"BB\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"10007\",\"shortDesc\":\"CDB AUTOMATICO\",\"id\":496632,\"longDesc\":\"CDB AUTOMATICO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"12332\",\"shortDesc\":\"PLAZO FIJO BR\",\"id\":491826,\"longDesc\":\"PLAZO FIJO BR\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"3050\",\"shortDesc\":\"PF MAJO\",\"id\":527850,\"longDesc\":\"PFIJO MAJO\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"78445\",\"shortDesc\":\"NO SIRVE\",\"id\":520665,\"longDesc\":\"BORRAR NO SIRVE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"72777\",\"shortDesc\":\"BORRARRR\",\"id\":535335,\"longDesc\":\"BORRAME\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"06000\",\"id\":13},\"subproductId\":\"00100\",\"shortDesc\":\"ACCOUNTANT\",\"id\":523,\"longDesc\":\"CONTABLE\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"08004\",\"shortDesc\":\"GSS INSURANCE\",\"id\":684,\"longDesc\":\"SEGURO DE VIDA SALDO DEUDOR PÓL. COL. GSS SEGUROS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"598945455\",\"shortDesc\":\"GARGA43\",\"id\":454422,\"longDesc\":\"GARGA 434\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"895445545\",\"shortDesc\":\"GARAGARA5\",\"id\":454436,\"longDesc\":\"GARAGARA 5545\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"49889984\",\"shortDesc\":\"GARGA565\",\"id\":454462,\"longDesc\":\"GARGA 565\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"55989888\",\"shortDesc\":\"GARGAME02\",\"id\":454468,\"longDesc\":\"GARGAME 03\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"56892267\",\"shortDesc\":\"GARGAR011\",\"id\":456989,\"longDesc\":\"GARGAR 0101\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"298298989\",\"shortDesc\":\"WARAWARANEW\",\"id\":459791,\"longDesc\":\"WARAWARA NEW\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5665612\",\"shortDesc\":\"REAL WARA\",\"id\":459797,\"longDesc\":\"WARA REAL\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"65568989\",\"shortDesc\":\"WARREAL01\",\"id\":460028,\"longDesc\":\"WAR REAL0111\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"08001\",\"shortDesc\":\"MONETARY GUARANTEE\",\"id\":146,\"longDesc\":\"GARANTIA MONETARIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"08002\",\"shortDesc\":\"WARRANTY DOCUMENTARY\",\"id\":147,\"longDesc\":\"GARANTIA DOCUMENTARIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"08003\",\"shortDesc\":\"REAL GUARANTEE\",\"id\":148,\"longDesc\":\"GARANTIA REAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"4545545\",\"shortDesc\":\"GARAGA\",\"id\":453389,\"longDesc\":\"GARAGA 456\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"69552444\",\"shortDesc\":\"GAR045\",\"id\":452680,\"longDesc\":\"GARAN045\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"56685877\",\"shortDesc\":\"OTRAGAR5\",\"id\":453387,\"longDesc\":\"OTRA GAR45\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"45455456\",\"shortDesc\":\"GARAGA2\",\"id\":453392,\"longDesc\":\"GARAGA 4562\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"58865599\",\"shortDesc\":\"GAR454\",\"id\":453403,\"longDesc\":\"GAR 4545\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"588655990\",\"shortDesc\":\"GAR4546\",\"id\":453406,\"longDesc\":\"GAR 4545\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"588655991\",\"shortDesc\":\"GAR45468\",\"id\":453408,\"longDesc\":\"GAR 45456\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"45874555\",\"shortDesc\":\"SHORTGAR\",\"id\":453412,\"longDesc\":\"SHORT GARA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"458745557\",\"shortDesc\":\"SHORTGAR2\",\"id\":453414,\"longDesc\":\"SHORT GARA2\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"598945458\",\"shortDesc\":\"GARGA434\",\"id\":454428,\"longDesc\":\"GARGA 4345\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5989454589\",\"shortDesc\":\"GARGA4347\",\"id\":454430,\"longDesc\":\"GARGA 43456\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5698898\",\"shortDesc\":\"GARDESCSHO\",\"id\":454453,\"longDesc\":\"GARDESC LARGE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"65566565\",\"shortDesc\":\"GAR04\",\"id\":452797,\"longDesc\":\"GARANITIA04\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"6598\",\"shortDesc\":\"GARA02\",\"id\":452668,\"longDesc\":\"GARANTIA 02\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"589898989\",\"shortDesc\":\"GARA0022\",\"id\":452850,\"longDesc\":\"GARA 0022\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"56685878\",\"shortDesc\":\"OTRAGAR4\",\"id\":453382,\"longDesc\":\"OTRA GAR448\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"59894545890\",\"shortDesc\":\"GARGA43478\",\"id\":454432,\"longDesc\":\"GARGA 434560\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"956565645\",\"shortDesc\":\"WARRA02155\",\"id\":455020,\"longDesc\":\"WARRA 02156\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5566\",\"shortDesc\":\"WAR1235666\",\"id\":455078,\"longDesc\":\"WAR123\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"95656564\",\"shortDesc\":\"WARRA0215\",\"id\":455015,\"longDesc\":\"WARRA 02156\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"55667\",\"shortDesc\":\"WAR12356667\",\"id\":455081,\"longDesc\":\"WAR123\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"556678\",\"shortDesc\":\"WAR123566678\",\"id\":455084,\"longDesc\":\"WAR123\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5566789\",\"shortDesc\":\"WAR1235666789\",\"id\":455087,\"longDesc\":\"WAR1239\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5566789012\",\"shortDesc\":\"WAR12356667800\",\"id\":455089,\"longDesc\":\"WAR12390\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"568998984\",\"shortDesc\":\"WARWAR02\",\"id\":455096,\"longDesc\":\"WAR WAR03\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"565454554\",\"shortDesc\":\"GAR02\",\"id\":455112,\"longDesc\":\"GAR 022\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5654545549\",\"shortDesc\":\"9GAR02\",\"id\":455114,\"longDesc\":\"9GAR 022\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"565445\",\"shortDesc\":\"OTRAGAR1\",\"id\":455116,\"longDesc\":\"OTRAGAR12\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5654456\",\"shortDesc\":\"OTRAGAR12\",\"id\":455118,\"longDesc\":\"OTRAGAR122\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"45545499\",\"shortDesc\":\"MONETARIATEST\",\"id\":455425,\"longDesc\":\"MONETARIA PRUEBA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"455454990\",\"shortDesc\":\"TESTMONETARIA\",\"id\":455427,\"longDesc\":\"MONETARIA PRUEBA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5656265\",\"shortDesc\":\"DOCUMENTARIA1\",\"id\":456116,\"longDesc\":\"DOCUMENTARIA 01\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"56562657\",\"shortDesc\":\"DOCUMENTARIA12\",\"id\":456120,\"longDesc\":\"DOCUMENTARIA 012\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"12345789\",\"shortDesc\":\"GARANCOR01\",\"id\":456956,\"longDesc\":\"GARAN CORTA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5689226\",\"shortDesc\":\"GARGAR01\",\"id\":456984,\"longDesc\":\"GARGAR 0101\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"90432\",\"shortDesc\":\"MONETARIAS BASELINE\",\"id\":468271,\"longDesc\":\"MONETARIAS BASELINE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"54435\",\"shortDesc\":\"MONETARIA BASELINE\",\"id\":468430,\"longDesc\":\"MONETARIA BASELINE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"30003\",\"shortDesc\":\"EXCESS OF BALANCE\",\"id\":445958,\"longDesc\":\"EXCESO DE SALDO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"0900\",\"shortDesc\":\"TX03\",\"id\":425821,\"longDesc\":\"TEST X03\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"00112\",\"shortDesc\":\"Basic Agreement\",\"id\":70,\"longDesc\":\"ACUERDO BASICO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"00003\",\"shortDesc\":\"OVERDRAFT\",\"id\":68,\"longDesc\":\"SOBREGIRO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"00011\",\"shortDesc\":\"INT CREDITORS\",\"id\":69,\"longDesc\":\"INT ACREEDORES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"1122\",\"shortDesc\":\"TEST AGREEMENTS\",\"id\":420822,\"longDesc\":\"PRUEBA ACUERDOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"2100\",\"shortDesc\":\"PRUEBA 2100\",\"id\":468296,\"longDesc\":\"PRUEBA 2100\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"00113\",\"shortDesc\":\"AAA\",\"id\":478791,\"longDesc\":\"AAA\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09000\",\"shortDesc\":\"LOAN RATE VDA FRANCES\",\"id\":23,\"longDesc\":\"PRESTAMO TASA VDA FRANCES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09008\",\"shortDesc\":\"EARLY RATE LOAN\",\"id\":43,\"longDesc\":\"PRESTAMO TASA ADELANTADA\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09009\",\"shortDesc\":\"REFI RATE VDA FRANCES\",\"id\":443,\"longDesc\":\"PRESTAMOS DE REFINANCIACION TASA VDA FRANCES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09011\",\"shortDesc\":\"ATB TEST CUOTA FIJA\",\"id\":376194,\"longDesc\":\"ATB TEST CUOTA FIJA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09007\",\"shortDesc\":\"Pledging LOAN\",\"id\":744,\"longDesc\":\"PRESTAMO PRENDARIO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09021\",\"shortDesc\":\"VDA DIRECT COPY\",\"id\":397455,\"longDesc\":\"DIRECTO VDA COPIA\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09026\",\"shortDesc\":\"TEST CELE\",\"id\":406339,\"longDesc\":\"PRUEBA CELE\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09002\",\"shortDesc\":\"LOAN RATE VDA ALEMAN\",\"id\":168,\"longDesc\":\"PRESTAMO TASA VENCIDA ALEMAN\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09010\",\"shortDesc\":\"TESTS TARIFARIO\",\"id\":183,\"longDesc\":\"PRUEBAS TARIFARIO\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09027\",\"shortDesc\":\"CLEAR TEST\",\"id\":309729,\"longDesc\":\"PRUEBA BORRAR\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09005\",\"shortDesc\":\"LOAN RATE VDA AMRCANO\",\"id\":704,\"longDesc\":\"PRESTAMO TASA VDA AMERICANO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09022\",\"shortDesc\":\"TESTING AÍDAÁÁA 12345\",\"id\":317514,\"longDesc\":\"TESTING BORRAR\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09006\",\"shortDesc\":\"LOAN RATE VDA espcial\",\"id\":705,\"longDesc\":\"PRESTAMO TASA VDA ESPECIAL\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09003\",\"shortDesc\":\"MORTGAGE LOAN\",\"id\":623,\"longDesc\":\"PRESTAMO HIPOTECARIO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09025\",\"shortDesc\":\"ALICIA DIRECT\",\"id\":371905,\"longDesc\":\"ALICIA DIRECTO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09013\",\"shortDesc\":\"DISCOUNT OF DOCUMENTS\",\"id\":440857,\"longDesc\":\"DESCUENTO DE DOCUMENTOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09016\",\"shortDesc\":\"ATB TEST ALEMAN\",\"id\":453421,\"longDesc\":\"ATB TEST ALEMAN\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09015\",\"shortDesc\":\"ATB TEST AMERICANO\",\"id\":453461,\"longDesc\":\"ATB TEST AMERICANO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09014\",\"shortDesc\":\"ATB TEST FRANCES\",\"id\":453495,\"longDesc\":\"ATB TEST FRANCES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09001\",\"shortDesc\":\"DIRECT VDA\",\"id\":143,\"longDesc\":\"DIRECTO VDA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09028\",\"shortDesc\":\"REQ WITH A RAG.\",\"id\":326507,\"longDesc\":\"A CON REQ GAR\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09004\",\"shortDesc\":\"PRESTAMO TASA VDA FRANCF\",\"id\":403,\"longDesc\":\"PRESTAMO TASA VDA FRANCES CFT\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09023\",\"shortDesc\":\"Loan 1\",\"id\":375243,\"longDesc\":\"PRESTAMO 1\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"07000\",\"shortDesc\":\"AUTOMATIZACION 1\",\"id\":514909,\"longDesc\":\"AUTOMATIZACION 1\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"1000\",\"shortDesc\":\"FRANCES CLONADO\",\"id\":516607,\"longDesc\":\"FRANCES CLONADO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09012\",\"shortDesc\":\"EMPRESTIMO FRANCES\",\"id\":463470,\"longDesc\":\"EMPRESTIMO FRANCES\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"99913\",\"shortDesc\":\"AUTOMATIC\",\"id\":516038,\"longDesc\":\"AUTOMATIC\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"1212\",\"shortDesc\":\"DIRECT VDA CLONE TEST\",\"id\":521579,\"longDesc\":\"DIRECT VDA CLONE TEST\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"32432423423\",\"shortDesc\":\"31231232131\",\"id\":518831,\"longDesc\":\"321312312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"323123123\",\"shortDesc\":\"321321312\",\"id\":518837,\"longDesc\":\"312312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"32321321\",\"shortDesc\":\"32131231231233\",\"id\":518843,\"longDesc\":\"321321312312312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"3213123123213\",\"shortDesc\":\"31231231\",\"id\":518849,\"longDesc\":\"312312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"9995\",\"shortDesc\":\"PERSONAL\",\"id\":526258,\"longDesc\":\"PERSONAL\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"01234\",\"shortDesc\":\"DIRECT SMOKE TEST\",\"id\":527363,\"longDesc\":\"DIRECT SMOKE TEST\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"11111111\",\"shortDesc\":\"21212121\",\"id\":518821,\"longDesc\":\"212121\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"1111111\",\"shortDesc\":\"RWEREREW\",\"id\":518826,\"longDesc\":\"REWREWRWE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"111\",\"shortDesc\":\"PRUEBA RODO\",\"id\":536863,\"longDesc\":\"PRUEBA RODO\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"08208\",\"shortDesc\":\"FACT01\",\"id\":430541,\"longDesc\":\"FACT01\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"0204\",\"shortDesc\":\"FACT 02\",\"id\":430546,\"longDesc\":\"FACT 02\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"0002\",\"shortDesc\":\"TEST SP 0002\",\"id\":440650,\"longDesc\":\"SP PRUEBA 0002\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"8080\",\"shortDesc\":\"INVOICES\",\"id\":430500,\"longDesc\":\"FACTURAS\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"9090\",\"shortDesc\":\"CHECKS\",\"id\":430533,\"longDesc\":\"CHEQUES\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT04\",\"shortDesc\":\"FACTORING 04\",\"id\":433346,\"longDesc\":\"FACTORING 04\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT111\",\"shortDesc\":\"PRUEBAFACT111\",\"id\":443094,\"longDesc\":\"PRUEBAFACT111\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"0206\",\"shortDesc\":\"FACT 03\",\"id\":430549,\"longDesc\":\"FACT 03\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT05\",\"shortDesc\":\"FACTORING 5\",\"id\":433376,\"longDesc\":\"FACTORING 5\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"0909\",\"shortDesc\":\"TEST 1 SHORT\",\"id\":442390,\"longDesc\":\"PRUEBA 1 LONG DESC\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT06\",\"shortDesc\":\"FACTORING 06\",\"id\":433544,\"longDesc\":\"FACTORING 06\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT07\",\"shortDesc\":\"FACTORING 07\",\"id\":433666,\"longDesc\":\"FACTORING 07\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"9999\",\"shortDesc\":\"SHORT TEST 2\",\"id\":442501,\"longDesc\":\"PRUEBA 2 LONG\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"222522\",\"shortDesc\":\"SUBPRODUCTO RWD\",\"id\":465747,\"longDesc\":\"SUBPRODUCTO REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"111\",\"shortDesc\":\"RWD Test11\",\"id\":460316,\"longDesc\":\"REWARDS PRUEBA 11\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"112\",\"shortDesc\":\"REW BORR\",\"id\":460356,\"longDesc\":\"REW BORRAR1\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"7885455\",\"shortDesc\":\"NUEVO SUBPROD REWARDS\",\"id\":518208,\"longDesc\":\"NUEVO SUBPRODUCTO REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"99\",\"shortDesc\":\"DONT USE\",\"id\":406221,\"longDesc\":\"DONT USE\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"02\",\"shortDesc\":\"PJ REWARDS\",\"id\":403729,\"longDesc\":\"REWARDS PJ\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"03\",\"shortDesc\":\"REWARDS NP\",\"id\":405057,\"longDesc\":\"REWARDS NP\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"50\",\"shortDesc\":\"NEW_REWARDS\",\"id\":406194,\"longDesc\":\"REWARDS_NEW\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"5001\",\"shortDesc\":\"PF REWARDS\",\"id\":400646,\"longDesc\":\"REWARDS PF\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"5455\",\"shortDesc\":\"DESC CORTA 25\",\"id\":494639,\"longDesc\":\"DESC LARGA 100\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"545454\",\"shortDesc\":\"DELETE\",\"id\":477092,\"longDesc\":\"DEL\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"0000001\",\"shortDesc\":\"PRODUCT-TEST-1\",\"id\":494837,\"longDesc\":\"PRODUCT-TEST-1\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"12345\",\"shortDesc\":\"REWARDS-TEST\",\"id\":494744,\"longDesc\":\"REWARDS-TEST\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"4040\",\"shortDesc\":\"RWD BR\",\"id\":480185,\"longDesc\":\"REWARDS BR\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"1234\",\"shortDesc\":\"WWWWW\",\"id\":494796,\"longDesc\":\"WWWWW\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"G8UGHU\",\"shortDesc\":\"SHORTDESC\",\"id\":491662,\"longDesc\":\"LONG DESC\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"1234567\",\"shortDesc\":\"SUPPROD REWARDS\",\"id\":495546,\"longDesc\":\"SUBPPROD REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"123456\",\"shortDesc\":\"SUPPROD REWARDS\",\"id\":495526,\"longDesc\":\"SUPPROD REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"5050\",\"shortDesc\":\"REWARDS 5050\",\"id\":480047,\"longDesc\":\"REWARDS 5050\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"00003\",\"shortDesc\":\"SUBPRODUCTO-REWARDS\",\"id\":495202,\"longDesc\":\"ESTE ES UN SUB DE REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"100\",\"shortDesc\":\"100 - REWARDS\",\"id\":487797,\"longDesc\":\"100 - REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"45575754\",\"shortDesc\":\"DFDFADFAFAFAFAFDFDFFDFDFD\",\"id\":489809,\"longDesc\":\"FFDFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"212121\",\"shortDesc\":\"DSADSDSADSA\",\"id\":512543,\"longDesc\":\"DSADASDASDAS\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"00004\",\"shortDesc\":\"SUB_REWARDS\",\"id\":495226,\"longDesc\":\"ESTE ES UN SUB DE REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"000002\",\"shortDesc\":\"PRODUCT-TEST-2\",\"id\":494868,\"longDesc\":\"PRODUCT-TEST-2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"1\",\"shortDesc\":\"1\",\"id\":478859,\"longDesc\":\"1\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"11111\",\"shortDesc\":\"1\",\"id\":478879,\"longDesc\":\"1\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"0123456\",\"shortDesc\":\"SUB REWARDS\",\"id\":495618,\"longDesc\":\"SUB REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"1\",\"shortDesc\":\"CREDIT CARD PAYMENT\",\"id\":12,\"longDesc\":\"PAGO EN TARJETA CREDITO\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"2222222\",\"shortDesc\":\"2\",\"id\":485750,\"longDesc\":\"2\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"03\",\"shortDesc\":\"MC PREPAID CARD\",\"id\":419688,\"longDesc\":\"MASTERCARD PREPAID CARD\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"02\",\"shortDesc\":\"VISA ELECTRON\",\"id\":303348,\"longDesc\":\"VISA ELECTRON\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"01011\",\"shortDesc\":\"VISA GOLD\",\"id\":963,\"longDesc\":\"VISA ORO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"9923\",\"shortDesc\":\"VISAMJ\",\"id\":535928,\"longDesc\":\"VISAMJ\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"9926\",\"shortDesc\":\"VISAMJ3\",\"id\":536726,\"longDesc\":\"VISAMJ3\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"1978\",\"shortDesc\":\"BHSA\",\"id\":535510,\"longDesc\":\"HIPOTECARIO SA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"333\",\"shortDesc\":\"333\",\"id\":535723,\"longDesc\":\"333\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"5\",\"shortDesc\":\"MASTERCARD\",\"id\":534237,\"longDesc\":\"TARJETA DE DÉBITO MASTERCARD\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"9924\",\"shortDesc\":\"VISAMJ2\",\"id\":536264,\"longDesc\":\"VISAMJ2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"4\",\"shortDesc\":\"MASTER_PRU\",\"id\":534714,\"longDesc\":\"MASTER_PRU\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"5\",\"shortDesc\":\"ADJUSTMENTS\",\"id\":823,\"longDesc\":\"COMPENSACION OTROS VALORES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"21\",\"shortDesc\":\"SUB1\",\"id\":438393,\"longDesc\":\"SUB2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"22\",\"shortDesc\":\"SUB2\",\"id\":438399,\"longDesc\":\"SUB2\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"20\",\"shortDesc\":\"NEW SUBPROD\",\"id\":438346,\"longDesc\":\"NUEVO SUBPROD LONG\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"13\",\"shortDesc\":\"CLC NEW 3\",\"id\":409990,\"longDesc\":\"CLC NUEVO LARGA 2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"18\",\"shortDesc\":\"SHORT DESCRIPTION\",\"id\":437778,\"longDesc\":\"DESCRIPCION LARGA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"23\",\"shortDesc\":\"SUB3\",\"id\":438406,\"longDesc\":\"SUB3\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"4\",\"shortDesc\":\"CHECKS\",\"id\":663,\"longDesc\":\"COMPENSACION Y LIQUIDACION DE CHEQUES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92000\",\"id\":3},\"subproductId\":\"35222\",\"shortDesc\":\"CONSUMPTION\",\"id\":46666666,\"longDesc\":\"CONSUMO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92000\",\"id\":3},\"subproductId\":\"30000\",\"shortDesc\":\"TAXES\",\"id\":10,\"longDesc\":\"IMPUESTOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"10001\",\"shortDesc\":\"DIRECT DEBIT COMP\",\"id\":3,\"longDesc\":\"COMPENSACION DE DEBITOS DIRECTOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"14\",\"shortDesc\":\"REAL TIME DEBIT\",\"id\":424197,\"longDesc\":\"DEBITO EN TIEMPO REAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"7\",\"shortDesc\":\"COMP DIRECT CREDIT\",\"id\":923,\"longDesc\":\"COMPENSACION DE CREDITOS DIRECTOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"16\",\"shortDesc\":\"TRANSF. INTERBANK\",\"id\":424542,\"longDesc\":\"TRANSF. INTERBANCARIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"9\",\"shortDesc\":\"TRANF BETWEEN CUSTOMERS\",\"id\":303182,\"longDesc\":\"TRANSFERENCIAS ENTRE CLIENTES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"10\",\"shortDesc\":\"TRANF between third\",\"id\":303364,\"longDesc\":\"TRANSFERENCIAS ENTRE TERCEROS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"17\",\"shortDesc\":\"DEPOSITS FROM CUSTOMERS\",\"id\":404724,\"longDesc\":\"DÉBITOS ENTRE CLIENTES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92300\",\"id\":209809},\"subproductId\":\"84922\",\"shortDesc\":\"MONEX\",\"id\":2,\"longDesc\":\"MONEX\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"66565656\",\"shortDesc\":\"SUBP NO PROPIO3\",\"id\":460776,\"longDesc\":\"PRUEBA NO PROPIO3\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10011\",\"shortDesc\":\"MASTERCARD\",\"id\":6,\"longDesc\":\"MASTERCARD\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10020\",\"shortDesc\":\"VISA\",\"id\":7,\"longDesc\":\"VISA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10030\",\"shortDesc\":\"AMEX\",\"id\":8,\"longDesc\":\"AMEX\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10040\",\"shortDesc\":\"THOROUGH\",\"id\":9,\"longDesc\":\"CABAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10031\",\"shortDesc\":\"AMEX24\",\"id\":480529,\"longDesc\":\"AMEX25\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"565645445\",\"shortDesc\":\"SUBPROD_NOTOWN5\",\"id\":470248,\"longDesc\":\"SUBPROD_NOTOWN5\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"66565657\",\"shortDesc\":\"SUB_NOPROP4\",\"id\":470229,\"longDesc\":\"SUB_NOPROP4\",\"status\":{\"id\":6}}],\"collectionEntityId\":\"Subproduct\",\"collectionEntityVersion\":\"1.0\",\"collectionEntityDataModel\":\"product.financials\"}}"));
        Mockito.when(CacheUtils.get("cyberbank.core.productTypes")).thenReturn(null);
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseCreateAccountOK());
        Mockito.when(CyberbankConsolidateConnector.getProductType(Mockito.anyString(), Mockito.anyString())).thenReturn(new ProductType("01000", 127, "10001", 4));
        CyberbankCoreConnectorResponse<String> cyberbankCoreConnectorResponse = CyberbankAccountConnector.create("1000", "00100", "00001", "11" );
        Assert.assertTrue(cyberbankCoreConnectorResponse != null);
        Assert.assertTrue(cyberbankCoreConnectorResponse.getData() != null);

    }


    @Test(expected = NullPointerException.class)
    public void testCreateAccountNOOK() throws BackendConnectorException, CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankConsolidateConnector.getProductType(Mockito.anyString(), Mockito.anyString())).thenReturn(new ProductType("01000", 127, "10001", 4));
        Mockito.when(CyberbankAccountConnector.create("1000","00","1", "11" )).thenThrow(NullPointerException.class);
    }

    @Test
    public void testReadStatementOK() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(gerResponseStatementOK());
        Mockito.when(NotesHandler.readNote(Mockito.anyInt(),Mockito.anyString())).thenReturn("Test Note");
        CyberbankCoreConnectorResponse<Statement> cyberbankCoreConnectorResponse = CyberbankAccountConnector.readStatement(Mockito.anyInt(), Mockito.anyString() );

        Assert.assertTrue(cyberbankCoreConnectorResponse != null);
        Assert.assertTrue(cyberbankCoreConnectorResponse.getData() != null);
    }

    @Test
    public void testReadAccountOK() throws  CyberbankCoreConnectorException, IOException {

        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), (CyberbankCoreRequest) Mockito.any())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"massiveSelectSubproductBy_Iso_Product\",\"transactionId\":\"massiveSelectSubproductBy_Iso_Product\",\"transactionVersion\":\"1.0\",\"out.subproduct_list\":{\"collection\":[{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00001\",\"shortDesc\":\"BOX\",\"id\":4,\"longDesc\":\"CAJA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00002\",\"shortDesc\":\"TREASURY\",\"id\":5,\"longDesc\":\"TESORERIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00010\",\"shortDesc\":\"REMESA INTERSUCURSAL\",\"id\":363,\"longDesc\":\"REMESA INTERSUCURSAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00011\",\"shortDesc\":\"INTERNAL REMESA\",\"id\":364,\"longDesc\":\"REMESA INTERNA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00112\",\"shortDesc\":\"EXTERNAL REMESA\",\"id\":365,\"longDesc\":\"REMESA EXTERNA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"00013\",\"shortDesc\":\"SPEND FUNDS\",\"id\":366,\"longDesc\":\"PASE DE FONDOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"00100\",\"id\":12},\"subproductId\":\"TEST\",\"shortDesc\":\"TEST BORRAR\",\"id\":527110,\"longDesc\":\"BORRAR\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"1460\",\"shortDesc\":\"C. SAVING PROD. 146\",\"id\":743,\"longDesc\":\"C. AHORRO PROD. 146\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"3105\",\"shortDesc\":\"CAJA DE AHORRO P BORRAR\",\"id\":507525,\"longDesc\":\"CAJA DE AHORRO P BORRAR\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"10001\",\"shortDesc\":\"SAVINGS BANK\",\"id\":127,\"longDesc\":\"CAJA DE AHORROS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"12000\",\"shortDesc\":\"TILT BOX\",\"id\":398580,\"longDesc\":\"CAJA GIRE\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"19990\",\"shortDesc\":\"C.AHORROS unmoving.\",\"id\":129,\"longDesc\":\"C.AHORROS INMOVIL.\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"9898\",\"shortDesc\":\"DEMO ACCOUNT\",\"id\":398013,\"longDesc\":\"ACCOUNT DEMO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"1\",\"shortDesc\":\"C. AHORRO PRUEBA\",\"id\":452891,\"longDesc\":\"CAJA DE AHORRO DE PRUEBA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"11000\",\"shortDesc\":\"CREDIT ACCOUNT\",\"id\":306267,\"longDesc\":\"CUENTA CRÉDITO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"SVACC\",\"shortDesc\":\"SVACC 1\",\"id\":418810,\"longDesc\":\"SVACC1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"1998\",\"shortDesc\":\"COIN PURSE\",\"id\":444431,\"longDesc\":\"MONEDERO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"001\",\"shortDesc\":\"DIEGO CUBA\",\"id\":462992,\"longDesc\":\"DIEGO CUBA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"278797\",\"shortDesc\":\"DIEGO_CUBA\",\"id\":463075,\"longDesc\":\"DIEGO_CUBA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"00193\",\"shortDesc\":\"C AHORRO YBRAHIM\",\"id\":487910,\"longDesc\":\"TEST YBRAHIM\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"1234\",\"shortDesc\":\"SUBPRODAHO\",\"id\":535543,\"longDesc\":\"SUPRODAHO\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"002\",\"shortDesc\":\"CA_MJ\",\"id\":534510,\"longDesc\":\"CA MAJO\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"003\",\"shortDesc\":\"CAMJ2\",\"id\":536504,\"longDesc\":\"CAMJ2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"01000\",\"id\":4},\"subproductId\":\"99998\",\"shortDesc\":\"TEST\",\"id\":535939,\"longDesc\":\"TEST\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20999\",\"shortDesc\":\"ACCOUNTS CBU\",\"id\":11,\"longDesc\":\"CUENTAS POR CBU\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20004\",\"shortDesc\":\"ACCOUNTS II\",\"id\":458301,\"longDesc\":\"CUENTAS CORRIENTES II\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"200015\",\"shortDesc\":\"ACCOUNTS III\",\"id\":459284,\"longDesc\":\"CUENTAS CORRIENTES III\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"29999\",\"shortDesc\":\"VISTA Recurrence\",\"id\":408640,\"longDesc\":\"VISTA RECURRENCIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20001\",\"shortDesc\":\"CURRENT ACCOUNTS\",\"id\":128,\"longDesc\":\"CUENTAS CORRIENTES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"9980\",\"shortDesc\":\"CONTA CORRENTE TRZ\",\"id\":425853,\"longDesc\":\"CONTA CORRENTE TRZ\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"29990\",\"shortDesc\":\"Cta.cte. STILL.\",\"id\":130,\"longDesc\":\"CTA.CTE. INMOVIL.\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20003\",\"shortDesc\":\"VIRTUAL\",\"id\":447824,\"longDesc\":\"MONEDERO VIRTUAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"20002\",\"shortDesc\":\"CV ROTATE\",\"id\":446798,\"longDesc\":\"CUENTA VIRTUAL GIRE\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"789\",\"shortDesc\":\"ALE\",\"id\":536334,\"longDesc\":\"ALE1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"88888\",\"shortDesc\":\"TEST2\",\"id\":536210,\"longDesc\":\"TEST2\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"888\",\"shortDesc\":\"PRUEBA3\",\"id\":536594,\"longDesc\":\"PRUEBA3MA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"99998\",\"shortDesc\":\"CC\",\"id\":535985,\"longDesc\":\"TESTMARY\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"8888888\",\"shortDesc\":\"TEST1\",\"id\":536173,\"longDesc\":\"TEST1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"55555\",\"shortDesc\":\"T11\",\"id\":536622,\"longDesc\":\"T111\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"888888\",\"shortDesc\":\"TESTM\",\"id\":536032,\"longDesc\":\"TESTM\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"9989\",\"shortDesc\":\"ASDUEBA\",\"id\":536329,\"longDesc\":\"AHJSJKASDF\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"02000\",\"id\":5},\"subproductId\":\"77777\",\"shortDesc\":\"TEST3\",\"id\":536301,\"longDesc\":\"TEST3\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05000\",\"id\":1},\"subproductId\":\"84922\",\"shortDesc\":\"SECURITY\",\"id\":1,\"longDesc\":\"SEGURIDAD\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"58002\",\"shortDesc\":\"PLAZO FIJO CLONADO\",\"id\":461413,\"longDesc\":\"PLAZO FIJO CLONADO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"3232432423\",\"shortDesc\":\"FSFDFDSF\",\"id\":511031,\"longDesc\":\"FDSFDSFSD\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"58001\",\"shortDesc\":\"CDB\",\"id\":803,\"longDesc\":\"CDB\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"111132132132131\",\"shortDesc\":\"111111\",\"id\":511025,\"longDesc\":\"21212132312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"7744\",\"shortDesc\":\"ASDADASDASDASD\",\"id\":512016,\"longDesc\":\"AAASSSS\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"123467\",\"shortDesc\":\"PLAZO FIJO DK2\",\"id\":491776,\"longDesc\":\"PLAZO FIJO DK2\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"22222\",\"shortDesc\":\"CDB AUTOMATICO OK\",\"id\":496636,\"longDesc\":\"CDB AUTOMATICO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"2020\",\"shortDesc\":\"PLAZO FIJO DK\",\"id\":491760,\"longDesc\":\"PLAZO FIJO DK\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"222\",\"shortDesc\":\"BORRAR12\",\"id\":510998,\"longDesc\":\"BORRAR1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"11\",\"shortDesc\":\"BORRAR\",\"id\":510993,\"longDesc\":\"BORRAR1\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"666\",\"shortDesc\":\"CODIGO DE TASAS\",\"id\":502381,\"longDesc\":\"CODIGO DE TASAS1\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"58003\",\"shortDesc\":\"PLAZO FIJO COPIA\",\"id\":462375,\"longDesc\":\"PLAZO FIJO COPIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"222222222\",\"shortDesc\":\"AA\",\"id\":509647,\"longDesc\":\"BB\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"10007\",\"shortDesc\":\"CDB AUTOMATICO\",\"id\":496632,\"longDesc\":\"CDB AUTOMATICO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"12332\",\"shortDesc\":\"PLAZO FIJO BR\",\"id\":491826,\"longDesc\":\"PLAZO FIJO BR\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"3050\",\"shortDesc\":\"PF MAJO\",\"id\":527850,\"longDesc\":\"PFIJO MAJO\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"78445\",\"shortDesc\":\"NO SIRVE\",\"id\":520665,\"longDesc\":\"BORRAR NO SIRVE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"05800\",\"id\":8},\"subproductId\":\"72777\",\"shortDesc\":\"BORRARRR\",\"id\":535335,\"longDesc\":\"BORRAME\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"06000\",\"id\":13},\"subproductId\":\"00100\",\"shortDesc\":\"ACCOUNTANT\",\"id\":523,\"longDesc\":\"CONTABLE\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"08004\",\"shortDesc\":\"GSS INSURANCE\",\"id\":684,\"longDesc\":\"SEGURO DE VIDA SALDO DEUDOR PÓL. COL. GSS SEGUROS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"598945455\",\"shortDesc\":\"GARGA43\",\"id\":454422,\"longDesc\":\"GARGA 434\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"895445545\",\"shortDesc\":\"GARAGARA5\",\"id\":454436,\"longDesc\":\"GARAGARA 5545\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"49889984\",\"shortDesc\":\"GARGA565\",\"id\":454462,\"longDesc\":\"GARGA 565\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"55989888\",\"shortDesc\":\"GARGAME02\",\"id\":454468,\"longDesc\":\"GARGAME 03\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"56892267\",\"shortDesc\":\"GARGAR011\",\"id\":456989,\"longDesc\":\"GARGAR 0101\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"298298989\",\"shortDesc\":\"WARAWARANEW\",\"id\":459791,\"longDesc\":\"WARAWARA NEW\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5665612\",\"shortDesc\":\"REAL WARA\",\"id\":459797,\"longDesc\":\"WARA REAL\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"65568989\",\"shortDesc\":\"WARREAL01\",\"id\":460028,\"longDesc\":\"WAR REAL0111\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"08001\",\"shortDesc\":\"MONETARY GUARANTEE\",\"id\":146,\"longDesc\":\"GARANTIA MONETARIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"08002\",\"shortDesc\":\"WARRANTY DOCUMENTARY\",\"id\":147,\"longDesc\":\"GARANTIA DOCUMENTARIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"08003\",\"shortDesc\":\"REAL GUARANTEE\",\"id\":148,\"longDesc\":\"GARANTIA REAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"4545545\",\"shortDesc\":\"GARAGA\",\"id\":453389,\"longDesc\":\"GARAGA 456\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"69552444\",\"shortDesc\":\"GAR045\",\"id\":452680,\"longDesc\":\"GARAN045\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"56685877\",\"shortDesc\":\"OTRAGAR5\",\"id\":453387,\"longDesc\":\"OTRA GAR45\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"45455456\",\"shortDesc\":\"GARAGA2\",\"id\":453392,\"longDesc\":\"GARAGA 4562\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"58865599\",\"shortDesc\":\"GAR454\",\"id\":453403,\"longDesc\":\"GAR 4545\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"588655990\",\"shortDesc\":\"GAR4546\",\"id\":453406,\"longDesc\":\"GAR 4545\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"588655991\",\"shortDesc\":\"GAR45468\",\"id\":453408,\"longDesc\":\"GAR 45456\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"45874555\",\"shortDesc\":\"SHORTGAR\",\"id\":453412,\"longDesc\":\"SHORT GARA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"458745557\",\"shortDesc\":\"SHORTGAR2\",\"id\":453414,\"longDesc\":\"SHORT GARA2\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"598945458\",\"shortDesc\":\"GARGA434\",\"id\":454428,\"longDesc\":\"GARGA 4345\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5989454589\",\"shortDesc\":\"GARGA4347\",\"id\":454430,\"longDesc\":\"GARGA 43456\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5698898\",\"shortDesc\":\"GARDESCSHO\",\"id\":454453,\"longDesc\":\"GARDESC LARGE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"65566565\",\"shortDesc\":\"GAR04\",\"id\":452797,\"longDesc\":\"GARANITIA04\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"6598\",\"shortDesc\":\"GARA02\",\"id\":452668,\"longDesc\":\"GARANTIA 02\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"589898989\",\"shortDesc\":\"GARA0022\",\"id\":452850,\"longDesc\":\"GARA 0022\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"56685878\",\"shortDesc\":\"OTRAGAR4\",\"id\":453382,\"longDesc\":\"OTRA GAR448\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"59894545890\",\"shortDesc\":\"GARGA43478\",\"id\":454432,\"longDesc\":\"GARGA 434560\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"956565645\",\"shortDesc\":\"WARRA02155\",\"id\":455020,\"longDesc\":\"WARRA 02156\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5566\",\"shortDesc\":\"WAR1235666\",\"id\":455078,\"longDesc\":\"WAR123\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"95656564\",\"shortDesc\":\"WARRA0215\",\"id\":455015,\"longDesc\":\"WARRA 02156\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"55667\",\"shortDesc\":\"WAR12356667\",\"id\":455081,\"longDesc\":\"WAR123\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"556678\",\"shortDesc\":\"WAR123566678\",\"id\":455084,\"longDesc\":\"WAR123\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5566789\",\"shortDesc\":\"WAR1235666789\",\"id\":455087,\"longDesc\":\"WAR1239\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5566789012\",\"shortDesc\":\"WAR12356667800\",\"id\":455089,\"longDesc\":\"WAR12390\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"568998984\",\"shortDesc\":\"WARWAR02\",\"id\":455096,\"longDesc\":\"WAR WAR03\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"565454554\",\"shortDesc\":\"GAR02\",\"id\":455112,\"longDesc\":\"GAR 022\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5654545549\",\"shortDesc\":\"9GAR02\",\"id\":455114,\"longDesc\":\"9GAR 022\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"565445\",\"shortDesc\":\"OTRAGAR1\",\"id\":455116,\"longDesc\":\"OTRAGAR12\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5654456\",\"shortDesc\":\"OTRAGAR12\",\"id\":455118,\"longDesc\":\"OTRAGAR122\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"45545499\",\"shortDesc\":\"MONETARIATEST\",\"id\":455425,\"longDesc\":\"MONETARIA PRUEBA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"455454990\",\"shortDesc\":\"TESTMONETARIA\",\"id\":455427,\"longDesc\":\"MONETARIA PRUEBA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5656265\",\"shortDesc\":\"DOCUMENTARIA1\",\"id\":456116,\"longDesc\":\"DOCUMENTARIA 01\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"56562657\",\"shortDesc\":\"DOCUMENTARIA12\",\"id\":456120,\"longDesc\":\"DOCUMENTARIA 012\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"12345789\",\"shortDesc\":\"GARANCOR01\",\"id\":456956,\"longDesc\":\"GARAN CORTA\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"5689226\",\"shortDesc\":\"GARGAR01\",\"id\":456984,\"longDesc\":\"GARGAR 0101\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"90432\",\"shortDesc\":\"MONETARIAS BASELINE\",\"id\":468271,\"longDesc\":\"MONETARIAS BASELINE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"08000\",\"id\":9},\"subproductId\":\"54435\",\"shortDesc\":\"MONETARIA BASELINE\",\"id\":468430,\"longDesc\":\"MONETARIA BASELINE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"30003\",\"shortDesc\":\"EXCESS OF BALANCE\",\"id\":445958,\"longDesc\":\"EXCESO DE SALDO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"0900\",\"shortDesc\":\"TX03\",\"id\":425821,\"longDesc\":\"TEST X03\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"00112\",\"shortDesc\":\"Basic Agreement\",\"id\":70,\"longDesc\":\"ACUERDO BASICO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"00003\",\"shortDesc\":\"OVERDRAFT\",\"id\":68,\"longDesc\":\"SOBREGIRO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"00011\",\"shortDesc\":\"INT CREDITORS\",\"id\":69,\"longDesc\":\"INT ACREEDORES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"1122\",\"shortDesc\":\"TEST AGREEMENTS\",\"id\":420822,\"longDesc\":\"PRUEBA ACUERDOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"2100\",\"shortDesc\":\"PRUEBA 2100\",\"id\":468296,\"longDesc\":\"PRUEBA 2100\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09000\",\"id\":15},\"subproductId\":\"00113\",\"shortDesc\":\"AAA\",\"id\":478791,\"longDesc\":\"AAA\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09000\",\"shortDesc\":\"LOAN RATE VDA FRANCES\",\"id\":23,\"longDesc\":\"PRESTAMO TASA VDA FRANCES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09008\",\"shortDesc\":\"EARLY RATE LOAN\",\"id\":43,\"longDesc\":\"PRESTAMO TASA ADELANTADA\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09009\",\"shortDesc\":\"REFI RATE VDA FRANCES\",\"id\":443,\"longDesc\":\"PRESTAMOS DE REFINANCIACION TASA VDA FRANCES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09011\",\"shortDesc\":\"ATB TEST CUOTA FIJA\",\"id\":376194,\"longDesc\":\"ATB TEST CUOTA FIJA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09007\",\"shortDesc\":\"Pledging LOAN\",\"id\":744,\"longDesc\":\"PRESTAMO PRENDARIO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09021\",\"shortDesc\":\"VDA DIRECT COPY\",\"id\":397455,\"longDesc\":\"DIRECTO VDA COPIA\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09026\",\"shortDesc\":\"TEST CELE\",\"id\":406339,\"longDesc\":\"PRUEBA CELE\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09002\",\"shortDesc\":\"LOAN RATE VDA ALEMAN\",\"id\":168,\"longDesc\":\"PRESTAMO TASA VENCIDA ALEMAN\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09010\",\"shortDesc\":\"TESTS TARIFARIO\",\"id\":183,\"longDesc\":\"PRUEBAS TARIFARIO\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09027\",\"shortDesc\":\"CLEAR TEST\",\"id\":309729,\"longDesc\":\"PRUEBA BORRAR\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09005\",\"shortDesc\":\"LOAN RATE VDA AMRCANO\",\"id\":704,\"longDesc\":\"PRESTAMO TASA VDA AMERICANO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09022\",\"shortDesc\":\"TESTING AÍDAÁÁA 12345\",\"id\":317514,\"longDesc\":\"TESTING BORRAR\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09006\",\"shortDesc\":\"LOAN RATE VDA espcial\",\"id\":705,\"longDesc\":\"PRESTAMO TASA VDA ESPECIAL\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09003\",\"shortDesc\":\"MORTGAGE LOAN\",\"id\":623,\"longDesc\":\"PRESTAMO HIPOTECARIO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09025\",\"shortDesc\":\"ALICIA DIRECT\",\"id\":371905,\"longDesc\":\"ALICIA DIRECTO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09013\",\"shortDesc\":\"DISCOUNT OF DOCUMENTS\",\"id\":440857,\"longDesc\":\"DESCUENTO DE DOCUMENTOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09016\",\"shortDesc\":\"ATB TEST ALEMAN\",\"id\":453421,\"longDesc\":\"ATB TEST ALEMAN\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09015\",\"shortDesc\":\"ATB TEST AMERICANO\",\"id\":453461,\"longDesc\":\"ATB TEST AMERICANO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09014\",\"shortDesc\":\"ATB TEST FRANCES\",\"id\":453495,\"longDesc\":\"ATB TEST FRANCES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09001\",\"shortDesc\":\"DIRECT VDA\",\"id\":143,\"longDesc\":\"DIRECTO VDA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09028\",\"shortDesc\":\"REQ WITH A RAG.\",\"id\":326507,\"longDesc\":\"A CON REQ GAR\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09004\",\"shortDesc\":\"PRESTAMO TASA VDA FRANCF\",\"id\":403,\"longDesc\":\"PRESTAMO TASA VDA FRANCES CFT\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09023\",\"shortDesc\":\"Loan 1\",\"id\":375243,\"longDesc\":\"PRESTAMO 1\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"07000\",\"shortDesc\":\"AUTOMATIZACION 1\",\"id\":514909,\"longDesc\":\"AUTOMATIZACION 1\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"1000\",\"shortDesc\":\"FRANCES CLONADO\",\"id\":516607,\"longDesc\":\"FRANCES CLONADO\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"09012\",\"shortDesc\":\"EMPRESTIMO FRANCES\",\"id\":463470,\"longDesc\":\"EMPRESTIMO FRANCES\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"99913\",\"shortDesc\":\"AUTOMATIC\",\"id\":516038,\"longDesc\":\"AUTOMATIC\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"1212\",\"shortDesc\":\"DIRECT VDA CLONE TEST\",\"id\":521579,\"longDesc\":\"DIRECT VDA CLONE TEST\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"32432423423\",\"shortDesc\":\"31231232131\",\"id\":518831,\"longDesc\":\"321312312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"323123123\",\"shortDesc\":\"321321312\",\"id\":518837,\"longDesc\":\"312312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"32321321\",\"shortDesc\":\"32131231231233\",\"id\":518843,\"longDesc\":\"321321312312312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"3213123123213\",\"shortDesc\":\"31231231\",\"id\":518849,\"longDesc\":\"312312\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"9995\",\"shortDesc\":\"PERSONAL\",\"id\":526258,\"longDesc\":\"PERSONAL\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"01234\",\"shortDesc\":\"DIRECT SMOKE TEST\",\"id\":527363,\"longDesc\":\"DIRECT SMOKE TEST\",\"status\":{\"id\":24}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"11111111\",\"shortDesc\":\"21212121\",\"id\":518821,\"longDesc\":\"212121\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"1111111\",\"shortDesc\":\"RWEREREW\",\"id\":518826,\"longDesc\":\"REWREWRWE\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09001\",\"id\":10},\"subproductId\":\"111\",\"shortDesc\":\"PRUEBA RODO\",\"id\":536863,\"longDesc\":\"PRUEBA RODO\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"08208\",\"shortDesc\":\"FACT01\",\"id\":430541,\"longDesc\":\"FACT01\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"0204\",\"shortDesc\":\"FACT 02\",\"id\":430546,\"longDesc\":\"FACT 02\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"0002\",\"shortDesc\":\"TEST SP 0002\",\"id\":440650,\"longDesc\":\"SP PRUEBA 0002\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"8080\",\"shortDesc\":\"INVOICES\",\"id\":430500,\"longDesc\":\"FACTURAS\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"9090\",\"shortDesc\":\"CHECKS\",\"id\":430533,\"longDesc\":\"CHEQUES\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT04\",\"shortDesc\":\"FACTORING 04\",\"id\":433346,\"longDesc\":\"FACTORING 04\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT111\",\"shortDesc\":\"PRUEBAFACT111\",\"id\":443094,\"longDesc\":\"PRUEBAFACT111\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"0206\",\"shortDesc\":\"FACT 03\",\"id\":430549,\"longDesc\":\"FACT 03\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT05\",\"shortDesc\":\"FACTORING 5\",\"id\":433376,\"longDesc\":\"FACTORING 5\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"0909\",\"shortDesc\":\"TEST 1 SHORT\",\"id\":442390,\"longDesc\":\"PRUEBA 1 LONG DESC\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT06\",\"shortDesc\":\"FACTORING 06\",\"id\":433544,\"longDesc\":\"FACTORING 06\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"FACT07\",\"shortDesc\":\"FACTORING 07\",\"id\":433666,\"longDesc\":\"FACTORING 07\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"09200\",\"id\":20},\"subproductId\":\"9999\",\"shortDesc\":\"SHORT TEST 2\",\"id\":442501,\"longDesc\":\"PRUEBA 2 LONG\",\"status\":{\"id\":54}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"222522\",\"shortDesc\":\"SUBPRODUCTO RWD\",\"id\":465747,\"longDesc\":\"SUBPRODUCTO REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"111\",\"shortDesc\":\"RWD Test11\",\"id\":460316,\"longDesc\":\"REWARDS PRUEBA 11\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"112\",\"shortDesc\":\"REW BORR\",\"id\":460356,\"longDesc\":\"REW BORRAR1\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"7885455\",\"shortDesc\":\"NUEVO SUBPROD REWARDS\",\"id\":518208,\"longDesc\":\"NUEVO SUBPRODUCTO REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"99\",\"shortDesc\":\"DONT USE\",\"id\":406221,\"longDesc\":\"DONT USE\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"02\",\"shortDesc\":\"PJ REWARDS\",\"id\":403729,\"longDesc\":\"REWARDS PJ\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"03\",\"shortDesc\":\"REWARDS NP\",\"id\":405057,\"longDesc\":\"REWARDS NP\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"50\",\"shortDesc\":\"NEW_REWARDS\",\"id\":406194,\"longDesc\":\"REWARDS_NEW\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"5001\",\"shortDesc\":\"PF REWARDS\",\"id\":400646,\"longDesc\":\"REWARDS PF\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"5455\",\"shortDesc\":\"DESC CORTA 25\",\"id\":494639,\"longDesc\":\"DESC LARGA 100\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"545454\",\"shortDesc\":\"DELETE\",\"id\":477092,\"longDesc\":\"DEL\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"0000001\",\"shortDesc\":\"PRODUCT-TEST-1\",\"id\":494837,\"longDesc\":\"PRODUCT-TEST-1\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"12345\",\"shortDesc\":\"REWARDS-TEST\",\"id\":494744,\"longDesc\":\"REWARDS-TEST\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"4040\",\"shortDesc\":\"RWD BR\",\"id\":480185,\"longDesc\":\"REWARDS BR\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"1234\",\"shortDesc\":\"WWWWW\",\"id\":494796,\"longDesc\":\"WWWWW\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"G8UGHU\",\"shortDesc\":\"SHORTDESC\",\"id\":491662,\"longDesc\":\"LONG DESC\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"1234567\",\"shortDesc\":\"SUPPROD REWARDS\",\"id\":495546,\"longDesc\":\"SUBPPROD REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"123456\",\"shortDesc\":\"SUPPROD REWARDS\",\"id\":495526,\"longDesc\":\"SUPPROD REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"5050\",\"shortDesc\":\"REWARDS 5050\",\"id\":480047,\"longDesc\":\"REWARDS 5050\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"00003\",\"shortDesc\":\"SUBPRODUCTO-REWARDS\",\"id\":495202,\"longDesc\":\"ESTE ES UN SUB DE REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"100\",\"shortDesc\":\"100 - REWARDS\",\"id\":487797,\"longDesc\":\"100 - REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"45575754\",\"shortDesc\":\"DFDFADFAFAFAFAFDFDFFDFDFD\",\"id\":489809,\"longDesc\":\"FFDFAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAFDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDDD\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"212121\",\"shortDesc\":\"DSADSDSADSA\",\"id\":512543,\"longDesc\":\"DSADASDASDAS\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"00004\",\"shortDesc\":\"SUB_REWARDS\",\"id\":495226,\"longDesc\":\"ESTE ES UN SUB DE REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"000002\",\"shortDesc\":\"PRODUCT-TEST-2\",\"id\":494868,\"longDesc\":\"PRODUCT-TEST-2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"1\",\"shortDesc\":\"1\",\"id\":478859,\"longDesc\":\"1\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"11111\",\"shortDesc\":\"1\",\"id\":478879,\"longDesc\":\"1\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"5000\",\"id\":402609},\"subproductId\":\"0123456\",\"shortDesc\":\"SUB REWARDS\",\"id\":495618,\"longDesc\":\"SUB REWARDS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"1\",\"shortDesc\":\"CREDIT CARD PAYMENT\",\"id\":12,\"longDesc\":\"PAGO EN TARJETA CREDITO\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"2222222\",\"shortDesc\":\"2\",\"id\":485750,\"longDesc\":\"2\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"03\",\"shortDesc\":\"MC PREPAID CARD\",\"id\":419688,\"longDesc\":\"MASTERCARD PREPAID CARD\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"02\",\"shortDesc\":\"VISA ELECTRON\",\"id\":303348,\"longDesc\":\"VISA ELECTRON\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"01011\",\"shortDesc\":\"VISA GOLD\",\"id\":963,\"longDesc\":\"VISA ORO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"9923\",\"shortDesc\":\"VISAMJ\",\"id\":535928,\"longDesc\":\"VISAMJ\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"9926\",\"shortDesc\":\"VISAMJ3\",\"id\":536726,\"longDesc\":\"VISAMJ3\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"1978\",\"shortDesc\":\"BHSA\",\"id\":535510,\"longDesc\":\"HIPOTECARIO SA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"333\",\"shortDesc\":\"333\",\"id\":535723,\"longDesc\":\"333\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"5\",\"shortDesc\":\"MASTERCARD\",\"id\":534237,\"longDesc\":\"TARJETA DE DÉBITO MASTERCARD\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"9924\",\"shortDesc\":\"VISAMJ2\",\"id\":536264,\"longDesc\":\"VISAMJ2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"80000\",\"id\":209569},\"subproductId\":\"4\",\"shortDesc\":\"MASTER_PRU\",\"id\":534714,\"longDesc\":\"MASTER_PRU\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"5\",\"shortDesc\":\"ADJUSTMENTS\",\"id\":823,\"longDesc\":\"COMPENSACION OTROS VALORES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"21\",\"shortDesc\":\"SUB1\",\"id\":438393,\"longDesc\":\"SUB2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"22\",\"shortDesc\":\"SUB2\",\"id\":438399,\"longDesc\":\"SUB2\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"20\",\"shortDesc\":\"NEW SUBPROD\",\"id\":438346,\"longDesc\":\"NUEVO SUBPROD LONG\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"13\",\"shortDesc\":\"CLC NEW 3\",\"id\":409990,\"longDesc\":\"CLC NUEVO LARGA 2\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"18\",\"shortDesc\":\"SHORT DESCRIPTION\",\"id\":437778,\"longDesc\":\"DESCRIPCION LARGA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"23\",\"shortDesc\":\"SUB3\",\"id\":438406,\"longDesc\":\"SUB3\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"90000\",\"id\":11},\"subproductId\":\"4\",\"shortDesc\":\"CHECKS\",\"id\":663,\"longDesc\":\"COMPENSACION Y LIQUIDACION DE CHEQUES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92000\",\"id\":3},\"subproductId\":\"35222\",\"shortDesc\":\"CONSUMPTION\",\"id\":46666666,\"longDesc\":\"CONSUMO\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92000\",\"id\":3},\"subproductId\":\"30000\",\"shortDesc\":\"TAXES\",\"id\":10,\"longDesc\":\"IMPUESTOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"10001\",\"shortDesc\":\"DIRECT DEBIT COMP\",\"id\":3,\"longDesc\":\"COMPENSACION DE DEBITOS DIRECTOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"14\",\"shortDesc\":\"REAL TIME DEBIT\",\"id\":424197,\"longDesc\":\"DEBITO EN TIEMPO REAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"7\",\"shortDesc\":\"COMP DIRECT CREDIT\",\"id\":923,\"longDesc\":\"COMPENSACION DE CREDITOS DIRECTOS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"16\",\"shortDesc\":\"TRANSF. INTERBANK\",\"id\":424542,\"longDesc\":\"TRANSF. INTERBANCARIA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"9\",\"shortDesc\":\"TRANF BETWEEN CUSTOMERS\",\"id\":303182,\"longDesc\":\"TRANSFERENCIAS ENTRE CLIENTES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"10\",\"shortDesc\":\"TRANF between third\",\"id\":303364,\"longDesc\":\"TRANSFERENCIAS ENTRE TERCEROS\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92100\",\"id\":21},\"subproductId\":\"17\",\"shortDesc\":\"DEPOSITS FROM CUSTOMERS\",\"id\":404724,\"longDesc\":\"DÉBITOS ENTRE CLIENTES\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92300\",\"id\":209809},\"subproductId\":\"84922\",\"shortDesc\":\"MONEX\",\"id\":2,\"longDesc\":\"MONEX\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"66565656\",\"shortDesc\":\"SUBP NO PROPIO3\",\"id\":460776,\"longDesc\":\"PRUEBA NO PROPIO3\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10011\",\"shortDesc\":\"MASTERCARD\",\"id\":6,\"longDesc\":\"MASTERCARD\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10020\",\"shortDesc\":\"VISA\",\"id\":7,\"longDesc\":\"VISA\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10030\",\"shortDesc\":\"AMEX\",\"id\":8,\"longDesc\":\"AMEX\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10040\",\"shortDesc\":\"THOROUGH\",\"id\":9,\"longDesc\":\"CABAL\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"10031\",\"shortDesc\":\"AMEX24\",\"id\":480529,\"longDesc\":\"AMEX25\",\"status\":{\"id\":7}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"565645445\",\"shortDesc\":\"SUBPROD_NOTOWN5\",\"id\":470248,\"longDesc\":\"SUBPROD_NOTOWN5\",\"status\":{\"id\":6}},{\"product\":{\"productId\":\"92401\",\"id\":24},\"subproductId\":\"66565657\",\"shortDesc\":\"SUB_NOPROP4\",\"id\":470229,\"longDesc\":\"SUB_NOPROP4\",\"status\":{\"id\":6}}],\"collectionEntityId\":\"Subproduct\",\"collectionEntityVersion\":\"1.0\",\"collectionEntityDataModel\":\"product.financials\"}}"));
        Mockito.when(CacheUtils.get("cyberbank.core.productTypes")).thenReturn(null);
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseReadAccountOK());
        Mockito.when(CyberbankConsolidateConnector.getProductType(Mockito.anyString(), Mockito.anyString())).thenReturn(new ProductType("01000", 127, "10001", 4));

        Product product = new Product();
        product.setExtraInfo("CA|3334|USD|01000|10|10001|1|1");
        CyberbankCoreConnectorResponse<Account> cyberbankCoreConnectorResponse = CyberbankAccountConnector.read(product);
        Assert.assertTrue(cyberbankCoreConnectorResponse != null);
        Assert.assertTrue(cyberbankCoreConnectorResponse.getData() != null);

    }

    @Test(expected = NullPointerException.class)
    public void testReadAccountNOOK() throws CyberbankCoreConnectorException {
        Mockito.when(CyberbankAccountConnector.read(new Product())).thenThrow(NullPointerException.class);
    }

    @Test
    public void testListAccounts() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), (CyberbankCoreRequest) Mockito.any())).thenReturn(new JSONObject(("{\n" +
                "  \"requestParameters\":{\n" +
                "     \"glb.credential\":{\n" +
                "        \"branchId\":\"1\",\n" +
                "        \"institutionId\":\"1111\",\n" +
                "        \"sourceTime\":\"20170704124255005-0300\",\n" +
                "        \"parityQuotationTypeNemotecnic\":\"BOLETOS_INTERNOS\",\n" +
                "        \"sessionId\":\"C521B16C2DDFC7C8330193328E174683\",\n" +
                "        \"locale\":\"es_AR\",\n" +
                "        \"userId\":\"TECHNISYS\",\n" +
                "        \"localCountryId\":\"1\",\n" +
                "        \"sourceDate\":\"20170704124255005-0300\",\n" +
                "        \"businessDate\":\"20090323000000000\",\n" +
                "        \"parityCurrencyCodeId\":\"2\",\n" +
                "        \"localCurrencyCodeId\":\"1\",\n" +
                "        \"msgTypeId\":\"200\",\n" +
                "        \"channelId\":\"8\",\n" +
                "        \"workflowModule\":\"Apoyo\",\n" +
                "        \"workflowId\":\"massiveSelectGeneralTableQueryApoyo\"\n" +
                "     },\n" +
                "     \"in.customer\":{\n" +
                "        \"customerId\": \"1613\"\n" +
                "     }\n" +
                "  },\n" +
                "  \"project\":\"Tech\",\n" +
                "  \"transactionId\":\"massiveSelectAccountAllFrom_Customer\"\n" +
                "}")));

        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListAccountsOk());


        CyberbankCoreConnectorResponse<List<Account>> accounts = CyberbankAccountConnector.list("1", new ArrayList<Client>() {{
            add(new Client("1630", ""));
        }});

        Assert.assertTrue(accounts != null);
        Assert.assertTrue(accounts.getData() != null);
        Assert.assertTrue(accounts.getData().size() > 0);
    }

    @Test
    public void testReadNoteBoucher() throws IOException {
        Statement statement = CyberbankAccountConnector.readNoteBoucher(21);
        Assert.assertTrue(statement != null);
    }

    public JSONObject gerResponseStatementOK(){
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"singleSelectLog_Mov\",\n" +
                "    \"transactionId\": \"singleSelectLog_Mov\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.log_mov\": {\n" +
                "        \"statusDate\": \"20090323144743581\",\n" +
                "        \"movTracki\": null,\n" +
                "        \"reversalNumber\": null,\n" +
                "        \"movTmstOutAuth\": null,\n" +
                "        \"traceNumber\": 15731488634530004,\n" +
                "        \"movAddDataMsg\": null,\n" +
                "        \"channel\": {\n" +
                "            \"shortDesc\": \"SUCURSAL\",\n" +
                "            \"id\": 11,\n" +
                "            \"channelId\": 8\n" +
                "        },\n" +
                "        \"movResponder\": null,\n" +
                "        \"branch\": {\n" +
                "            \"branchId\": 1,\n" +
                "            \"shortDesc\": \"Main Branch\",\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"sourceSubproduct\": {\n" +
                "            \"product\": {\n" +
                "                \"productId\": \"02000\",\n" +
                "                \"shortDesc\": \"Current Accounts\",\n" +
                "                \"id\": 5\n" +
                "            },\n" +
                "            \"subproductId\": \"20001\",\n" +
                "            \"shortDesc\": \"CURRENT ACCOUNTS\",\n" +
                "            \"id\": 128\n" +
                "        },\n" +
                "        \"productIsOwn\": true,\n" +
                "        \"offLineFormId\": null,\n" +
                "        \"reversalReason\": null,\n" +
                "        \"referenceNumber\": 7777777,\n" +
                "        \"movLocationId\": 127,\n" +
                "        \"movAffectedBalance\": null,\n" +
                "        \"id\": 549760,\n" +
                "        \"quotFixed\": null,\n" +
                "        \"imputationTime\": \"20191107144743578\",\n" +
                "        \"quotApplied\": \"0.00000\",\n" +
                "        \"checkNumber\": null,\n" +
                "        \"movBankSupervisorProfile\": null,\n" +
                "        \"movAttentionNum\": null,\n" +
                "        \"originalReferenceNumber\": null,\n" +
                "        \"merchantIdentifierId\": null,\n" +
                "        \"historyPass\": true,\n" +
                "        \"status\": {\n" +
                "            \"id\": 7\n" +
                "        },\n" +
                "        \"sourceIsoProduct\": {\n" +
                "            \"shortDesc\": \"Current accounts\",\n" +
                "            \"id\": 4\n" +
                "        },\n" +
                "        \"sourceTime\": null,\n" +
                "        \"operationCurrencyCode\": {\n" +
                "            \"currencyCodeId\": 1,\n" +
                "            \"shortDesc\": \"Local currency\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"transactionReason\": {\n" +
                "            \"statusDate\": \"20160211000000000\",\n" +
                "            \"updateExtCrDate\": false,\n" +
                "            \"payrollBalanceFlag\": false,\n" +
                "            \"reason\": {\n" +
                "                \"shortDesc\": \"Transf.Enviada\",\n" +
                "                \"id\": 64,\n" +
                "                \"reasonCodeId\": 57\n" +
                "            },\n" +
                "            \"panelId\": null,\n" +
                "            \"atmDesc\": \"Rech.Trnf.o/Bco\",\n" +
                "            \"cmc7Id\": null,\n" +
                "            \"originMode\": {\n" +
                "                \"id\": 4\n" +
                "            },\n" +
                "            \"updateExtDbDate\": false,\n" +
                "            \"updateWithdrawalDate\": false,\n" +
                "            \"stampAdditional\": \"PASIVAS\",\n" +
                "            \"otherCurrency\": {\n" +
                "                \"id\": 1\n" +
                "            },\n" +
                "            \"txnAction\": {\n" +
                "                \"id\": 9\n" +
                "            },\n" +
                "            \"networkDesc\": \"Rech.Trnf.o/Bco\",\n" +
                "            \"allowedDate\": {\n" +
                "                \"id\": 3\n" +
                "            },\n" +
                "            \"offLineFormId\": null,\n" +
                "            \"pinPadMode\": null,\n" +
                "            \"stampDateTime\": \"20160211000000000\",\n" +
                "            \"onLineFormId\": null,\n" +
                "            \"statementDesc\": \"Rech.Trnf.o/Bco\",\n" +
                "            \"interbranchAllowed\": true,\n" +
                "            \"pureAction\": {\n" +
                "                \"id\": 2\n" +
                "            },\n" +
                "            \"id\": 271,\n" +
                "            \"isCash\": false,\n" +
                "            \"updateIntDbDate\": false,\n" +
                "            \"amountType\": {\n" +
                "                \"id\": 3\n" +
                "            },\n" +
                "            \"updateTodayOtherDb\": false,\n" +
                "            \"overdraftMode\": {\n" +
                "                \"id\": 3\n" +
                "            },\n" +
                "            \"genuineBalance\": {\n" +
                "                \"id\": 3\n" +
                "            },\n" +
                "            \"opportunity\": {\n" +
                "                \"id\": 1\n" +
                "            },\n" +
                "            \"updateTodayCashWithdrawal\": false,\n" +
                "            \"impDebCode\": {\n" +
                "                \"id\": 5\n" +
                "            },\n" +
                "            \"isSiter\": false,\n" +
                "            \"updateTxn2Update\": false,\n" +
                "            \"accountRequired\": true,\n" +
                "            \"updateTodayCashDeposits\": false,\n" +
                "            \"updateTodayOtherCr\": false,\n" +
                "            \"maxIteration\": 0,\n" +
                "            \"updateIntCrDate\": true,\n" +
                "            \"shortDesc\": \"Rech.Trnf.o/Bco\",\n" +
                "            \"stampUser\": {\n" +
                "                \"id\": 300970\n" +
                "            },\n" +
                "            \"historyPass\": true,\n" +
                "            \"longDesc\": \"CR por rech. de Transf. enviada a otro Banco\",\n" +
                "            \"transaction\": {\n" +
                "                \"shortDesc\": \"CR por Transf\",\n" +
                "                \"id\": 127031,\n" +
                "                \"txnCodeId\": 7\n" +
                "            },\n" +
                "            \"status\": {\n" +
                "                \"id\": 7\n" +
                "            }\n" +
                "        },\n" +
                "        \"stampAdditional\": \"212\",\n" +
                "        \"businessDate\": \"20090323000000000\",\n" +
                "        \"exchangeTicketId\": 0,\n" +
                "        \"movRespCode\": null,\n" +
                "        \"operationId\": \"4847\",\n" +
                "        \"pureAction\": {\n" +
                "            \"id\": 2\n" +
                "        },\n" +
                "        \"txnActionCode\": {\n" +
                "            \"txnActionId\": 9,\n" +
                "            \"shortDesc\": \"Transfer\",\n" +
                "            \"id\": 9\n" +
                "        },\n" +
                "        \"keyOtherSystem\": null,\n" +
                "        \"movAuthNum\": null,\n" +
                "        \"sourceBranch\": {\n" +
                "            \"branchId\": 1,\n" +
                "            \"shortDesc\": \"Main Branch\",\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"currencyCodeOut\": {\n" +
                "            \"currencyCodeId\": 1,\n" +
                "            \"shortDesc\": \"Local currency\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"movPayments\": null,\n" +
                "        \"reversalDate\": null,\n" +
                "        \"transactionNumber\": null,\n" +
                "        \"movCountableBranch\": {\n" +
                "            \"branchId\": 502,\n" +
                "            \"shortDesc\": \"MICROCENTRO\",\n" +
                "            \"id\": 24\n" +
                "        },\n" +
                "        \"movDispensedAmount\": null,\n" +
                "        \"movTmstInAuth\": null,\n" +
                "        \"userId\": {\n" +
                "            \"name\": \"TECHNISYS\",\n" +
                "            \"id\": 2,\n" +
                "            \"userId\": \"TECHNISYS\"\n" +
                "        },\n" +
                "        \"isSiter\": false,\n" +
                "        \"movNewStatus\": null,\n" +
                "        \"movAddAmount2\": null,\n" +
                "        \"movOriginator\": null,\n" +
                "        \"movAddAmount1\": null,\n" +
                "        \"movTransmitionTime\": null,\n" +
                "        \"imputationDate\": \"20191107000000000\",\n" +
                "        \"issueBusinessDate\": \"20090323000000000\",\n" +
                "        \"stampUser\": {\n" +
                "            \"id\": 2\n" +
                "        },\n" +
                "        \"currencyCode\": {\n" +
                "            \"currencyCodeId\": 1,\n" +
                "            \"shortDesc\": \"Local currency\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"movAccessId\": null,\n" +
                "        \"movDetAvailBalance\": null,\n" +
                "        \"movTmstInPre\": null,\n" +
                "        \"convertedAmount\": null,\n" +
                "        \"movTmstOutPre\": null,\n" +
                "        \"originBranch\": {\n" +
                "            \"branchId\": 1,\n" +
                "            \"shortDesc\": \"Main Branch\",\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"movTransmitionDate\": null,\n" +
                "        \"stampDateTime\": \"20191107144743581\",\n" +
                "        \"movAddDataDb\": null,\n" +
                "        \"childSequenceNbr\": 0,\n" +
                "        \"valuedDate\": null,\n" +
                "        \"amountType\": {\n" +
                "            \"shortDesc\": \"Capital\",\n" +
                "            \"id\": 3\n" +
                "        },\n" +
                "        \"businessSector\": {\n" +
                "            \"businessSectorId\": 1001,\n" +
                "            \"shortDesc\": \"In the country Publ No Financ\",\n" +
                "            \"id\": 500001\n" +
                "        },\n" +
                "        \"movTrackii\": null,\n" +
                "        \"movOldStatus\": null,\n" +
                "        \"reversalFlag\": false,\n" +
                "        \"terminalUnitId\": 145,\n" +
                "        \"subproduct\": {\n" +
                "            \"product\": {\n" +
                "                \"productId\": \"01000\",\n" +
                "                \"shortDesc\": \"Caja de Ahorros\"\n" +
                "            },\n" +
                "            \"subproductId\": \"10001\",\n" +
                "            \"shortDesc\": \"SAVINGS BANK\",\n" +
                "            \"id\": 127\n" +
                "        },\n" +
                "        \"movCustomer\": {\n" +
                "            \"customerId\": \"1606\",\n" +
                "            \"id\": 548124,\n" +
                "            \"nameShort\": null\n" +
                "        },\n" +
                "        \"terminalType\": {\n" +
                "            \"id\": 2\n" +
                "        },\n" +
                "        \"sourceOperationId\": \"10324\",\n" +
                "        \"movCategoryId\": null,\n" +
                "        \"movCrmInfoRecv\": null,\n" +
                "        \"reversalTime\": null,\n" +
                "        \"customerSessionNumber\": null,\n" +
                "        \"sourceDate\": \"20170704124255000\",\n" +
                "        \"momentCode\": {\n" +
                "            \"shortDesc\": \"Normal\",\n" +
                "            \"id\": 1,\n" +
                "            \"momentCodeId\": 1038\n" +
                "        },\n" +
                "        \"externalTransactionId\": null,\n" +
                "        \"isCash\": false,\n" +
                "        \"amount\": \"1.00000\",\n" +
                "        \"postPending\": false,\n" +
                "        \"comments\": null,\n" +
                "        \"movBankSupervisor\": null,\n" +
                "        \"movCrmInfoSend\": null,\n" +
                "        \"cashierMachine\": null,\n" +
                "        \"terminal\": {\n" +
                "            \"terminalUnitId\": 145,\n" +
                "            \"id\": 1,\n" +
                "            \"terminalId\": \"127.0.0.1\"\n" +
                "        },\n" +
                "        \"isoProduct\": {\n" +
                "            \"isoProductTypeId\": 10,\n" +
                "            \"shortDesc\": \"Savings bank\",\n" +
                "            \"id\": 3\n" +
                "        },\n" +
                "        \"movBankUserProfile\": null,\n" +
                "        \"movIssuer\": null,\n" +
                "        \"sourceCurrencyCode\": {\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"txnDescription\": \"test cmeneses 2\",\n" +
                "        \"originalAmount\": \"1.00000\",\n" +
                "        \"movAcqBusinessDate\": null,\n" +
                "        \"msgTypeId\": null,\n" +
                "        \"service\": {\n" +
                "            \"id\": 212\n" +
                "        },\n" +
                "        \"movInternalSequenceNum\": null,\n" +
                "        \"movAcquirer\": null,\n" +
                "        \"termCode\": {\n" +
                "            \"termCodeId\": 0,\n" +
                "            \"shortDesc\": \"Does not apply\",\n" +
                "            \"id\": 1\n" +
                "        }\n" +
                "    }\n" +
                "}");
    }

    public JSONObject getResponseCreateAccountOK(){
        return new JSONObject("{\n" +
                "   \"project\": \"Tech\",\n" +
                "   \"transactionId\": \"insertAccountAutomatic\",\n" +
                "   \"transactionVersion\": \"value\",\n" +
                "   \"out.account\": {\n" +
                "      \"statusDate\": \"value\",\n" +
                "      \"lastPageGenerated\": \"value\",\n" +
                "      \"totalCrAccruedInterest\": \"value\",\n" +
                "      \"todayCashWithdrawal\": \"value\",\n" +
                "      \"subCurrencyIsObligatory\": \"value\",\n" +
                "      \"dbBalanceToAccrual\": \"value\",\n" +
                "      \"customerNumberDeclared\": \"value\",\n" +
                "      \"branch\": {\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"useOfSignature\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"today24HrsDb\": \"value\",\n" +
                "      \"packOperationId\": \"value\",\n" +
                "      \"bank\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"checkDigitOut\": \"value\",\n" +
                "      \"bankActivityCode\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"id\": \"value\",\n" +
                "      \"lastStatProcess\": \"value\",\n" +
                "      \"tomorrowBlocked\": \"value\",\n" +
                "      \"blockedBalance\": \"value\",\n" +
                "      \"todayCashDeposits\": \"value\",\n" +
                "      \"otherCr72Hrs\": \"value\",\n" +
                "      \"packMainAccount\": \"value\",\n" +
                "      \"inputDate\": \"value\",\n" +
                "      \"lastProcessDate\": \"value\",\n" +
                "      \"depositsOtherTerms\": \"value\",\n" +
                "      \"checks48Hrs\": \"value\",\n" +
                "      \"lastDateEmitted\": \"value\",\n" +
                "      \"otherCrOtherTerms\": \"value\",\n" +
                "      \"verifHistBalFlag\": \"value\",\n" +
                "      \"cntChkRejectYtd\": \"value\",\n" +
                "      \"closedCode\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"nettingGroup\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"subproductInsurance\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"cntChkRejectMtd\": \"value\",\n" +
                "      \"shortName\": \"value\",\n" +
                "      \"otherDb96Hrs\": \"value\",\n" +
                "      \"linkToPackage\": \"value\",\n" +
                "      \"status\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"deposits24Hrs\": \"value\",\n" +
                "      \"accumPendingTxn\": \"value\",\n" +
                "      \"lastWithdrawalDate\": \"value\",\n" +
                "      \"updateDate\": \"value\",\n" +
                "      \"yesterdayDb\": \"value\",\n" +
                "      \"dbBalAllowedFlag\": \"value\",\n" +
                "      \"balanceToday\": \"value\",\n" +
                "      \"stampAdditional\": \"value\",\n" +
                "      \"todayDeposits48Hrs\": \"value\",\n" +
                "      \"debitModeType\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"lastTxnDate\": \"value\",\n" +
                "      \"operationId\": \"value\",\n" +
                "      \"currency\": {\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"vatCategory\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"todayDepositsOtherTerms\": \"value\",\n" +
                "      \"lastTxn2date\": \"value\",\n" +
                "      \"product\": {\n" +
                "         \"statusDate\": \"value\",\n" +
                "         \"officialId\": \"value\",\n" +
                "         \"productId\": \"value\",\n" +
                "         \"systemUser\": \"value\",\n" +
                "         \"isoProduct\": {\n" +
                "            \"id\": \"value\"\n" +
                "         },\n" +
                "         \"dateFrom\": \"value\",\n" +
                "         \"productIsOwn\": \"value\",\n" +
                "         \"stampAdditional\": \"value\",\n" +
                "         \"institution\": {\n" +
                "            \"id\": \"value\"\n" +
                "         },\n" +
                "         \"statusReason\": {\n" +
                "            \"id\": \"value\"\n" +
                "         },\n" +
                "         \"stampDateTime\": \"value\",\n" +
                "         \"crmInfo\": \"value\",\n" +
                "         \"dateTo\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"stampUser\": {\n" +
                "            \"id\": \"value\"\n" +
                "         },\n" +
                "         \"id\": \"value\",\n" +
                "         \"longDesc\": \"value\",\n" +
                "         \"detailTableName\": \"value\",\n" +
                "         \"status\": {\n" +
                "            \"id\": \"value\"\n" +
                "         },\n" +
                "         \"crmInfoExpiryDate\": \"value\"\n" +
                "      },\n" +
                "      \"lineSttNumber\": \"value\",\n" +
                "      \"accrualSuspendedDate\": \"value\",\n" +
                "      \"cntStopPaymentsYtd\": \"value\",\n" +
                "      \"dbDaysWithoutAgreement\": \"value\",\n" +
                "      \"packSubproduct\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"lastTxnIntDbDate\": \"value\",\n" +
                "      \"dbBalanceTotalDays\": \"value\",\n" +
                "      \"otherDbOtherTerms\": \"value\",\n" +
                "      \"availableBalProcessType\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"dbBalanceStartDate\": \"value\",\n" +
                "      \"lastTxnExtCrDate\": \"value\",\n" +
                "      \"todayDeposits96Hrs\": \"value\",\n" +
                "      \"oldestValuedDateTxn\": \"value\",\n" +
                "      \"yesterdayCr\": \"value\",\n" +
                "      \"closedDate\": \"value\",\n" +
                "      \"checkTemporaryReceivership\": \"value\",\n" +
                "      \"stampUser\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"lastAccrualDate\": \"value\",\n" +
                "      \"minDaysForChecks\": \"value\",\n" +
                "      \"depositRestriction\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"todayOtherCr\": \"value\",\n" +
                "      \"otherDb48Hrs\": \"value\",\n" +
                "      \"cntChkJustifiedYtd\": \"value\",\n" +
                "      \"totalDbAccruedInterest\": \"value\",\n" +
                "      \"checksOthersTerms\": \"value\",\n" +
                "      \"statusReason\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"stampDateTime\": \"value\",\n" +
                "      \"openingCodeMotive\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"lastHistoryProcess\": \"value\",\n" +
                "      \"totalAgreements\": \"value\",\n" +
                "      \"businessSector\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"checksAllowedFlag\": \"value\",\n" +
                "      \"lastTxnExtDbDate\": \"value\",\n" +
                "      \"withdrawalRestriction\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"otherDb72Hrs\": \"value\",\n" +
                "      \"nickName\": \"value\",\n" +
                "      \"cntNormalStt\": \"value\",\n" +
                "      \"subproduct\": {\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"deposits72Hrs\": \"value\",\n" +
                "      \"forcedSttFlag\": \"value\",\n" +
                "      \"otherCr24Hrs\": \"value\",\n" +
                "      \"cntWithdrawal\": \"value\",\n" +
                "      \"checks72Hrs\": \"value\",\n" +
                "      \"todayOtherDb\": \"value\",\n" +
                "      \"balanceYesterday\": \"value\",\n" +
                "      \"sttPeriod\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"name\": \"value\",\n" +
                "      \"compensationGroup\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"specialRates\": \"value\",\n" +
                "      \"donotDormantFlag\": \"value\",\n" +
                "      \"dataCompleteCustomer\": \"value\",\n" +
                "      \"otherCr48Hrs\": \"value\",\n" +
                "      \"payrollBalance\": \"value\",\n" +
                "      \"deposits96Hrs\": \"value\",\n" +
                "      \"otherCr96Hrs\": \"value\",\n" +
                "      \"systemUser\": \"value\",\n" +
                "      \"tomorrowDb\": \"value\",\n" +
                "      \"tomorrowAgreements\": \"value\",\n" +
                "      \"checks24Hrs\": \"value\",\n" +
                "      \"checks96Hrs\": \"value\",\n" +
                "      \"todayDeposits24Hrs\": \"value\",\n" +
                "      \"inhibitCheckbookFlag\": \"value\",\n" +
                "      \"interbranchAllowed\": \"value\",\n" +
                "      \"deposits48Hrs\": \"value\",\n" +
                "      \"customerNumberReached\": \"value\",\n" +
                "      \"cntOnpYtd\": \"value\",\n" +
                "      \"openingDate\": \"value\",\n" +
                "      \"todayDeposits72Hrs\": \"value\",\n" +
                "      \"lastSttBalance\": \"value\",\n" +
                "      \"dbRestriction\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"sttType\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"otherDb24Hrs\": \"value\",\n" +
                "      \"situationCode\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"isoProduct\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"cntChkFormalRejectYtd\": \"value\",\n" +
                "      \"positioningGroup\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"priceListProfile\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"cntSpecialStt\": \"value\",\n" +
                "      \"bukNumber\": \"value\",\n" +
                "      \"lastTxnIntCrDate\": \"value\",\n" +
                "      \"availableBalProcessTime\": \"value\",\n" +
                "      \"lastPageEmitted\": \"value\",\n" +
                "      \"crRestriction\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"centralBankActivityCode\": {\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"cntChkFormalRejectMtd\": \"value\",\n" +
                "      \"tomorrowCr\": \"value\",\n" +
                "      \"specialFees\": \"value\"\n" +
                "   }\n" +
                "}");
    }



    public JSONObject getResponseReadAccountOK(){
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"singleSelectAccount\",\n" +
                "    \"transactionId\": \"singleSelectAccount\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.account\": {\n" +
                "        \"lastPageGenerated\": 0,\n" +
                "        \"statusDate\": \"20090323112904093\",\n" +
                "        \"totalCrAccruedInterest\": \"0.00000\",\n" +
                "        \"subCurrencyIsObligatory\": false,\n" +
                "        \"todayCashWithdrawal\": \"0.00000\",\n" +
                "        \"dbBalanceToAccrual\": \"0.00000\",\n" +
                "        \"customerNumberDeclared\": 1,\n" +
                "        \"branch\": {\n" +
                "            \"branchId\": 1,\n" +
                "            \"shortDesc\": \"Main Branch\",\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"useOfSignature\": {\n" +
                "            \"useOfSignatureId\": 4,\n" +
                "            \"nemotecnico\": \"UNIPERSONAL\",\n" +
                "            \"shortDesc\": \"UNIPERSONAL\",\n" +
                "            \"id\": 4\n" +
                "        },\n" +
                "        \"today24HrsDb\": \"0.00000\",\n" +
                "        \"packOperationId\": \" \",\n" +
                "        \"bank\": {\n" +
                "            \"bankId\": 499,\n" +
                "            \"nemotecnico\": \"TECHBANK\",\n" +
                "            \"shortDesc\": \"TechBank\",\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"checkDigitOut\": \"0\",\n" +
                "        \"bankActivityCode\": {\n" +
                "            \"bankActivityCodeId\": 21,\n" +
                "            \"nemotecnico\": \"No Informada\",\n" +
                "            \"shortDesc\": \"uninformed\",\n" +
                "            \"id\": 21\n" +
                "        },\n" +
                "        \"id\": 544164,\n" +
                "        \"lastStatProcess\": \"20090320000000000\",\n" +
                "        \"tomorrowBlocked\": \"0.00000\",\n" +
                "        \"blockedBalance\": \"0.00000\",\n" +
                "        \"todayCashDeposits\": \"0.00000\",\n" +
                "        \"otherCr72Hrs\": \"0.00000\",\n" +
                "        \"packMainAccount\": false,\n" +
                "        \"inputDate\": \"20090323000000000\",\n" +
                "        \"lastProcessDate\": \"20090320000000000\",\n" +
                "        \"depositsOtherTerms\": \"0.00000\",\n" +
                "        \"checks48Hrs\": \"0.00000\",\n" +
                "        \"lastDateEmitted\": null,\n" +
                "        \"otherCrOtherTerms\": \"0.00000\",\n" +
                "        \"verifHistBalFlag\": true,\n" +
                "        \"cntChkRejectYtd\": 0,\n" +
                "        \"cntChkRejectMtd\": 0,\n" +
                "        \"otherDb96Hrs\": \"0.00000\",\n" +
                "        \"shortName\": \"Morin , Megan \",\n" +
                "        \"linkToPackage\": false,\n" +
                "        \"accumPendingTxn\": \"0.00000\",\n" +
                "        \"deposits24Hrs\": \"0.00000\",\n" +
                "        \"status\": {\n" +
                "            \"statusId\": 7,\n" +
                "            \"nemotecnico\": \"VIGENTE\",\n" +
                "            \"shortDesc\": \"Valid\",\n" +
                "            \"id\": 7\n" +
                "        },\n" +
                "        \"lastWithdrawalDate\": null,\n" +
                "        \"updateDate\": \"20090323000000000\",\n" +
                "        \"yesterdayDb\": \"0.00000\",\n" +
                "        \"dbBalAllowedFlag\": false,\n" +
                "        \"balanceToday\": \"280.00000\",\n" +
                "        \"stampAdditional\": \"971\",\n" +
                "        \"todayDeposits48Hrs\": \"0.00000\",\n" +
                "        \"debitModeType\": {\n" +
                "            \"nemotecnico\": \"COBRO_NORMAL\",\n" +
                "            \"shortDesc\": \"Normal collection\",\n" +
                "            \"debitModeTypeId\": 1,\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"lastTxnDate\": \"20191104000000000\",\n" +
                "        \"operationId\": \"4570\",\n" +
                "        \"currency\": {\n" +
                "            \"currAcronym\": \"MNL\",\n" +
                "            \"currencyCodeId\": 1,\n" +
                "            \"shortDesc\": \"Local currency\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"vatCategory\": {\n" +
                "            \"nemotecnico\": \"RESP. INSCRIPTO\",\n" +
                "            \"shortDesc\": \"Responsible VAT Enrolled\",\n" +
                "            \"id\": 1,\n" +
                "            \"vatCategoryId\": 1\n" +
                "        },\n" +
                "        \"todayDepositsOtherTerms\": \"0.00000\",\n" +
                "        \"lastTxn2date\": null,\n" +
                "        \"accrualSuspendedDate\": null,\n" +
                "        \"lineSttNumber\": 0,\n" +
                "        \"product\": {\n" +
                "            \"officialId\": null,\n" +
                "            \"productId\": \"01000\",\n" +
                "            \"shortDesc\": \"Caja de Ahorros\",\n" +
                "            \"id\": 4,\n" +
                "            \"isoProduct\": {\n" +
                "                \"officialId\": \"PASIVAS\",\n" +
                "                \"isoProductTypeId\": 10,\n" +
                "                \"nemotecnico\": \"C_AHORRO\",\n" +
                "                \"shortDesc\": \"Savings bank\",\n" +
                "                \"id\": 3,\n" +
                "                \"longDesc\": \"Caja de Ahorros\"\n" +
                "            },\n" +
                "            \"productIsOwn\": true\n" +
                "        },\n" +
                "        \"cntStopPaymentsYtd\": 0,\n" +
                "        \"dbDaysWithoutAgreement\": 0,\n" +
                "        \"dbBalanceTotalDays\": 0,\n" +
                "        \"lastTxnIntDbDate\": null,\n" +
                "        \"otherDbOtherTerms\": \"0.00000\",\n" +
                "        \"availableBalProcessType\": {\n" +
                "            \"availableBalProcessTypeId\": null,\n" +
                "            \"nemotecnico\": \"NO_APLICAR\",\n" +
                "            \"shortDesc\": \"DO NOT APPLY\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"dbBalanceStartDate\": null,\n" +
                "        \"lastTxnExtCrDate\": null,\n" +
                "        \"oldestValuedDateTxn\": null,\n" +
                "        \"todayDeposits96Hrs\": \"0.00000\",\n" +
                "        \"yesterdayCr\": \"0.00000\",\n" +
                "        \"closedDate\": null,\n" +
                "        \"checkTemporaryReceivership\": null,\n" +
                "        \"stampUser\": {\n" +
                "            \"name\": \"TECHNISYS\",\n" +
                "            \"id\": 2,\n" +
                "            \"userId\": \"TECHNISYS\"\n" +
                "        },\n" +
                "        \"lastAccrualDate\": \"20090320000000000\",\n" +
                "        \"minDaysForChecks\": 5,\n" +
                "        \"depositRestriction\": {\n" +
                "            \"nemotecnico\": \"NOT_RESTRICTED\",\n" +
                "            \"restrictionId\": 1,\n" +
                "            \"shortDesc\": \"Without restrictions\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"todayOtherCr\": \"0.00000\",\n" +
                "        \"otherDb48Hrs\": \"0.00000\",\n" +
                "        \"cntChkJustifiedYtd\": 0,\n" +
                "        \"totalDbAccruedInterest\": \"0.00000\",\n" +
                "        \"checksOthersTerms\": \"0.00000\",\n" +
                "        \"stampDateTime\": \"20191104112904093\",\n" +
                "        \"openingCodeMotive\": {\n" +
                "            \"nemotecnico\": \"AUTOMATICA\",\n" +
                "            \"openingCodeMotiveId\": 4,\n" +
                "            \"shortDesc\": \"AUTOMATICA\",\n" +
                "            \"id\": 4\n" +
                "        },\n" +
                "        \"lastHistoryProcess\": \"20090320000000000\",\n" +
                "        \"totalAgreements\": \"0.00000\",\n" +
                "        \"businessSector\": {\n" +
                "            \"businessSectorId\": 1001,\n" +
                "            \"nemotecnico\": \"PUB NO FINANC\",\n" +
                "            \"shortDesc\": \"In the country Publ No Financ\",\n" +
                "            \"id\": 500001\n" +
                "        },\n" +
                "        \"checksAllowedFlag\": false,\n" +
                "        \"lastTxnExtDbDate\": null,\n" +
                "        \"otherDb72Hrs\": \"0.00000\",\n" +
                "        \"withdrawalRestriction\": {\n" +
                "            \"nemotecnico\": \"NOT_RESTRICTED\",\n" +
                "            \"restrictionId\": 1,\n" +
                "            \"shortDesc\": \"Without restrictions\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"nickName\": \"Morin , Megan \",\n" +
                "        \"cntNormalStt\": 0,\n" +
                "        \"deposits72Hrs\": \"0.00000\",\n" +
                "        \"subproduct\": {\n" +
                "            \"product\": {\n" +
                "                \"productId\": \"01000\",\n" +
                "                \"shortDesc\": \"Caja de Ahorros\",\n" +
                "                \"id\": 4,\n" +
                "                \"isoProduct\": {\n" +
                "                    \"officialId\": \"PASIVAS\",\n" +
                "                    \"isoProductTypeId\": 10,\n" +
                "                    \"nemotecnico\": \"C_AHORRO\",\n" +
                "                    \"shortDesc\": \"Savings bank\",\n" +
                "                    \"id\": 3,\n" +
                "                    \"longDesc\": \"Caja de Ahorros\"\n" +
                "                },\n" +
                "                \"longDesc\": \"Caja de Ahorros\",\n" +
                "                \"productIsOwn\": true\n" +
                "            },\n" +
                "            \"subproductId\": \"10001\",\n" +
                "            \"nemotecnico\": null,\n" +
                "            \"shortDesc\": \"SAVINGS BANK\",\n" +
                "            \"id\": 127,\n" +
                "            \"longDesc\": \"CAJA DE AHORROS\"\n" +
                "        },\n" +
                "        \"forcedSttFlag\": false,\n" +
                "        \"otherCr24Hrs\": \"0.00000\",\n" +
                "        \"cntWithdrawal\": 0,\n" +
                "        \"checks72Hrs\": \"0.00000\",\n" +
                "        \"todayOtherDb\": \"0.00000\",\n" +
                "        \"balanceYesterday\": \"0.00000\",\n" +
                "        \"name\": \"Morin , Megan \",\n" +
                "        \"donotDormantFlag\": true,\n" +
                "        \"specialRates\": false,\n" +
                "        \"dataCompleteCustomer\": null,\n" +
                "        \"otherCr48Hrs\": \"0.00000\",\n" +
                "        \"payrollBalance\": \"0.00000\",\n" +
                "        \"deposits96Hrs\": \"0.00000\",\n" +
                "        \"otherCr96Hrs\": \"0.00000\",\n" +
                "        \"systemUser\": \" \",\n" +
                "        \"tomorrowDb\": \"0.00000\",\n" +
                "        \"tomorrowAgreements\": \"0.00000\",\n" +
                "        \"checks24Hrs\": \"0.00000\",\n" +
                "        \"checks96Hrs\": \"0.00000\",\n" +
                "        \"todayDeposits24Hrs\": \"0.00000\",\n" +
                "        \"inhibitCheckbookFlag\": true,\n" +
                "        \"interbranchAllowed\": true,\n" +
                "        \"deposits48Hrs\": \"0.00000\",\n" +
                "        \"customerNumberReached\": 1,\n" +
                "        \"cntOnpYtd\": 0,\n" +
                "        \"openingDate\": \"20090323000000000\",\n" +
                "        \"todayDeposits72Hrs\": \"0.00000\",\n" +
                "        \"lastSttBalance\": \"0.00000\",\n" +
                "        \"dbRestriction\": {\n" +
                "            \"nemotecnico\": \"NOT_RESTRICTED\",\n" +
                "            \"restrictionId\": 1,\n" +
                "            \"shortDesc\": \"Without restrictions\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"sttType\": {\n" +
                "            \"nemotecnico\": \"NO APLICA\",\n" +
                "            \"summaryTypeId\": 4,\n" +
                "            \"shortDesc\": \"NO PRINTS\",\n" +
                "            \"id\": 4\n" +
                "        },\n" +
                "        \"otherDb24Hrs\": \"0.00000\",\n" +
                "        \"isoProduct\": {\n" +
                "            \"officialId\": \"PASIVAS\",\n" +
                "            \"isoProductTypeId\": 10,\n" +
                "            \"nemotecnico\": \"C_AHORRO\",\n" +
                "            \"shortDesc\": \"Savings bank\",\n" +
                "            \"id\": 3\n" +
                "        },\n" +
                "        \"situationCode\": {\n" +
                "            \"nemotecnico\": \"NORMAL\",\n" +
                "            \"shortDesc\": \"Normal\",\n" +
                "            \"id\": 1,\n" +
                "            \"situationCodeId\": 1\n" +
                "        },\n" +
                "        \"cntChkFormalRejectYtd\": 0,\n" +
                "        \"cntSpecialStt\": 0,\n" +
                "        \"bukNumber\": \"49900013110000000457010\",\n" +
                "        \"lastTxnIntCrDate\": \"20090323000000000\",\n" +
                "        \"availableBalProcessTime\": \"19691231210000000\",\n" +
                "        \"lastPageEmitted\": 0,\n" +
                "        \"crRestriction\": {\n" +
                "            \"nemotecnico\": \"NOT_RESTRICTED\",\n" +
                "            \"restrictionId\": 1,\n" +
                "            \"shortDesc\": \"Without restrictions\",\n" +
                "            \"id\": 1\n" +
                "        },\n" +
                "        \"centralBankActivityCode\": {\n" +
                "            \"centralBankActivityCodeId\": 0,\n" +
                "            \"nemotecnico\": \"No Informada\",\n" +
                "            \"shortDesc\": \"uninformed\",\n" +
                "            \"id\": 0\n" +
                "        },\n" +
                "        \"cntChkFormalRejectMtd\": 0,\n" +
                "        \"tomorrowCr\": \"0.00000\",\n" +
                "        \"specialFees\": false\n" +
                "    }\n" +
                "}"
        );
    }




//    @Test
//    public void testClientWithNoProducts() throws BackendConnectorException, CyberbankCoreConnectorException {
//        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\n" +
//                "  \"requestParameters\":{\n" +
//                "     \"glb.credential\":{\n" +
//                "        \"branchId\":\"1\",\n" +
//                "        \"institutionId\":\"1111\",\n" +
//                "        \"sourceTime\":\"20170704124255005-0300\",\n" +
//                "        \"parityQuotationTypeNemotecnic\":\"BOLETOS_INTERNOS\",\n" +
//                "        \"sessionId\":\"C521B16C2DDFC7C8330193328E174683\",\n" +
//                "        \"locale\":\"es_AR\",\n" +
//                "        \"userId\":\"TECHNISYS\",\n" +
//                "        \"localCountryId\":\"1\",\n" +
//                "        \"sourceDate\":\"20170704124255005-0300\",\n" +
//                "        \"businessDate\":\"20090323000000000\",\n" +
//                "        \"parityCurrencyCodeId\":\"2\",\n" +
//                "        \"localCurrencyCodeId\":\"1\",\n" +
//                "        \"msgTypeId\":\"200\",\n" +
//                "        \"channelId\":\"8\",\n" +
//                "        \"workflowModule\":\"Apoyo\",\n" +
//                "        \"workflowId\":\"massiveSelectGeneralTableQueryApoyo\"\n" +
//                "     },\n" +
//                "     \"in.customer\":{\n" +
//                "        \"customerId\": \"1613\"\n" +
//                "     }\n" +
//                "  },\n" +
//                "  \"project\":\"Tech\",\n" +
//                "  \"transactionId\":\"massiveSelectAccountAllFrom_Customer\"\n" +
//                "}"));
//
////        Mockito.when(ProductUtils.generateIdProduct(Mockito.anyString())).thenReturn("7e536ceeb8f722e8c3f4a55a045a153febe492ed");
//        CyberbankCoreConnectorResponse<List<Account>> accounts = CyberbankAccountConnector.list("1", new ArrayList<Client>() {{
//            add(new Client("1613", ""));
//        }});
//
//        Assert.assertTrue(accounts != null);
//        Assert.assertTrue(accounts.getData() != null);
//        Assert.assertTrue(accounts.getData().isEmpty());
//    }

    public JSONObject getResponseListAccountsOk(){
        return  new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectAccountAllFrom_Customer\",\n" +
                "    \"transactionId\": \"massiveSelectAccountAllFrom_Customer\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.count\": 2,\n" +
                "    \"out.account_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"product\": {\n" +
                "                    \"productId\": \"01000\",\n" +
                "                    \"shortDesc\": \"Caja de Ahorros\",\n" +
                "                    \"id\": 4,\n" +
                "                    \"longDesc\": \"Caja de Ahorros\"\n" +
                "                },\n" +
                "                \"nickName\": \"Morin , Megan \",\n" +
                "                \"subproduct\": {\n" +
                "                    \"subproductId\": \"10001\",\n" +
                "                    \"shortDesc\": \"SAVINGS BANK\",\n" +
                "                    \"id\": 127,\n" +
                "                    \"longDesc\": \"CAJA DE AHORROS\"\n" +
                "                },\n" +
                "                \"branch\": {\n" +
                "                    \"branchId\": 1,\n" +
                "                    \"shortDesc\": \"Main Branch\",\n" +
                "                    \"id\": 0,\n" +
                "                    \"longDesc\": \"Main Branch\"\n" +
                "                },\n" +
                "                \"inputDate\": \"20090323000000000\",\n" +
                "                \"balanceToday\": \"1990.00000\",\n" +
                "                \"bukNumber\": \"4990001311000000049795\",\n" +
                "                \"name\": \"Morin , Megan \",\n" +
                "                \"operationId\": \"4979\",\n" +
                "                \"currency\": {\n" +
                "                    \"currencyCodeId\": 1,\n" +
                "                    \"shortDesc\": \"Local currency\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"Moneda Local\"\n" +
                "                },\n" +
                "                \"id\": 550345,\n" +
                "                \"shortName\": \"Morin , Megan \",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            },\n" +
                "            {\n" +
                "                \"product\": {\n" +
                "                    \"productId\": \"01000\",\n" +
                "                    \"shortDesc\": \"Caja de Ahorros\",\n" +
                "                    \"id\": 4,\n" +
                "                    \"longDesc\": \"Caja de Ahorros\"\n" +
                "                },\n" +
                "                \"nickName\": \"Morin , Megan \",\n" +
                "                \"subproduct\": {\n" +
                "                    \"subproductId\": \"10001\",\n" +
                "                    \"shortDesc\": \"SAVINGS BANK\",\n" +
                "                    \"id\": 127,\n" +
                "                    \"longDesc\": \"CAJA DE AHORROS\"\n" +
                "                },\n" +
                "                \"branch\": {\n" +
                "                    \"branchId\": 1,\n" +
                "                    \"shortDesc\": \"Main Branch\",\n" +
                "                    \"id\": 0,\n" +
                "                    \"longDesc\": \"Main Branch\"\n" +
                "                },\n" +
                "                \"inputDate\": \"20090323000000000\",\n" +
                "                \"balanceToday\": \"10.00000\",\n" +
                "                \"bukNumber\": \"49900013110000000498710\",\n" +
                "                \"name\": \"Morin , Megan \",\n" +
                "                \"operationId\": \"4987\",\n" +
                "                \"currency\": {\n" +
                "                    \"currencyCodeId\": 1,\n" +
                "                    \"shortDesc\": \"Local currency\",\n" +
                "                    \"id\": 1,\n" +
                "                    \"longDesc\": \"Moneda Local\"\n" +
                "                },\n" +
                "                \"id\": 550354,\n" +
                "                \"shortName\": \"Morin , Megan \",\n" +
                "                \"status\": {\n" +
                "                    \"nemotecnico\": \"VIGENTE\",\n" +
                "                    \"shortDesc\": \"Valid\",\n" +
                "                    \"id\": 7,\n" +
                "                    \"longDesc\": \"Vigente\"\n" +
                "                }\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Account\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}"
        );
    }

}
