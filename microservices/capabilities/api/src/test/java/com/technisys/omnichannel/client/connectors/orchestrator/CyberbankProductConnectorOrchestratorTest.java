/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankConsolidateConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, CyberbankCoreConnector.class, CyberbankConsolidateConnector.class})

public class CyberbankProductConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CyberbankConsolidateConnector.class);
    }

    @Test
    public void testlistProduct() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<List<Product>> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new ArrayList<>());
        Mockito.when(CyberbankConsolidateConnector.products(Mockito.anyString(),Mockito.anyList())).thenReturn(response);
        List<Client> clienList = new ArrayList<>();
        Client client = new Client();
        client.setIdClient("121");
        clienList.add(client);
        List<Product> productList = CoreProductConnectorOrchestrator.list("121", clienList, new ArrayList<>());
        Assert.assertNotNull(productList);
    }

    @Test (expected = BackendConnectorException.class)
    public void testlistProductException() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankConsolidateConnector.products(Mockito.anyString(),Mockito.anyList())).thenThrow(new CyberbankCoreConnectorException("Error"));
        List<Client> clienList = new ArrayList<>();
        Client client = new Client();
        client.setIdClient("121");
        clienList.add(client);
        List<Product> productList = CoreProductConnectorOrchestrator.list("121", clienList, new ArrayList<>());
        Assert.assertNotNull(productList);
    }


}