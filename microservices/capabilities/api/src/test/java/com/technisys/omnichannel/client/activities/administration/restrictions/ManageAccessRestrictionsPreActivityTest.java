package com.technisys.omnichannel.client.activities.administration.restrictions;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;
import static org.powermock.api.mockito.PowerMockito.mockStatic;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigurationFactory.class})
public class ManageAccessRestrictionsPreActivityTest {

    Request request;

    ManageAccessRestrictionsPreActivity activity;

    private List<String> timezoneValues = new ArrayList<>();

    @Mock
    private Configuration configurationMock;

    @Before
    public void setUp() throws Exception {
        request = new Request();
        activity = new ManageAccessRestrictionsPreActivity();
        timezoneValues.add("Pacific/Midway");
        timezoneValues.add("Asia/Kuala_Lumpur");
        timezoneValues.add("Australia/Brisbane");
        MockitoAnnotations.initMocks(this);
        mockStatic(ConfigurationFactory.class);
        Mockito
                .when(ConfigurationFactory.getInstance())
                .thenReturn(configurationMock);
        Mockito.when(configurationMock.getList(Configuration.PLATFORM, "administration.timezone.values")).thenReturn(timezoneValues);
    }

    @Test(expected = ActivityException.class)
    public void testWhenEnvironmentSchemeSimpleIsRequesting() throws ActivityException {
        request.setEnvironmentAdminScheme(Environment.ADMINISTRATION_SCHEME_SIMPLE);

        String x = ZoneId.systemDefault().getId();

        activity.validate(request);
    }

    @Test
    public void testWhenEnvironmentSchemeMediumIsRequesting() throws ActivityException {
        request.setEnvironmentAdminScheme(Environment.ADMINISTRATION_SCHEME_MEDIUM);

        assertTrue(activity.validate(request).size() == 0);
    }

    @Test
    public void testWhenEnvironmentSchemeAdvancedIsRequesting() throws ActivityException {
        request.setEnvironmentAdminScheme(Environment.ADMINISTRATION_SCHEME_ADVANCED);

        assertTrue(activity.validate(request).size() == 0);
    }

    @Test
    public void testWhenActivityIsRequested() throws ActivityException {
        request.setEnvironmentAdminScheme(Environment.ADMINISTRATION_SCHEME_ADVANCED);

        Response res = activity.execute(request);

        assertTrue(res.getData().containsKey(ManageAccessRestrictionsPreActivity.OutParams.TIME_ZONES));
    }



}