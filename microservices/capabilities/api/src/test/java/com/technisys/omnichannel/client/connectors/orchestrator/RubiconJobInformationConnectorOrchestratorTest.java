/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.OccupationJob;
import com.technisys.omnichannel.client.domain.RoleJob;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class})

public class RubiconJobInformationConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);

    }

    @Test
    public void testListRolesJob() throws  BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        List<RoleJob> roleJobList = CoreJobInformationConnectorOrchestator.listRolesJob();
        Assert.assertNotNull(roleJobList);
    }

    @Test
    public void testListOccupationJobs() throws  BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        List<OccupationJob> occupationJobList = CoreJobInformationConnectorOrchestator.listOccupationJobs();
        Assert.assertNotNull(occupationJobList);
    }
}