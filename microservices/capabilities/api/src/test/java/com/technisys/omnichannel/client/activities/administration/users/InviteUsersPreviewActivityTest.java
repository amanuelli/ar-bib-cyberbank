package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.junit.Assert.*;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Administration.class })
public class InviteUsersPreviewActivityTest {

    @Mock
    private Administration administrationMock;

    private Request request;
    private InviteUsersPreviewActivity activity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        Mockito
                .when(Administration.getInstance())
                .thenReturn(administrationMock);

        request = new Request();
        activity = new InviteUsersPreviewActivity();
    }

    @Test
    public void returnsOkTest() throws IOException, ActivityException {
        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

}