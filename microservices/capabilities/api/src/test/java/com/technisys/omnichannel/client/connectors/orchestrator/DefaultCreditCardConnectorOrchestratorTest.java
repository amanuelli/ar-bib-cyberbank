/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.StatementCreditCard;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class})

public class DefaultCreditCardConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadCreditCardDetails() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreCreditCardConnectorOrchestator.readCreditCardDetails("21", "21", "21");
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadStatementDetails() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreCreditCardConnectorOrchestator.readStatementDetails("21", "21",0, "21");
    }

    @Test(expected = BackendConnectorException.class)
    public void testListCreditCardStatementLines() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreCreditCardConnectorOrchestator.listCreditCardStatementLines("21");
    }



    @Test(expected = BackendConnectorException.class)
    public void testReadCreditCardStatementLines() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreCreditCardConnectorOrchestator.readCreditCardStatementLine("21");
    }


    @Test(expected = BackendConnectorException.class)
    public void testGetTotalCreditCardStatements() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreCreditCardConnectorOrchestator.getTotalCreditCardStatements("21");
    }


    @Test(expected = BackendConnectorException.class)
    public void testGetCurrentPeriodCreditCardStatements() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreCreditCardConnectorOrchestator.getCurrentPeriodCreditCardStatements("21");
    }


    @Test(expected = BackendConnectorException.class)
    public void testListCreditCardStatements() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        List<StatementCreditCard> list = CoreCreditCardConnectorOrchestator.listCreditCardStatements("21", "2", "1", new Date(), new Date(), 0.1, 0.3, "test", "UDS", 0, 0 );
    }


    @Test(expected = BackendConnectorException.class)
    public void testPayCreditCardValidate() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        Account account = new Account();
        account.setIdProduct("23");
        CoreCreditCardConnectorOrchestator.payCreditCardValidate("21", "2", account, "21", "creditcardBank", "USD", 100.0, "test", 0.0, 0.1);
    }


    @Test(expected = BackendConnectorException.class)
    public void testPayCreditCardSendException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        Account account = new Account();
        account.setIdProduct("23");
        CoreCreditCardConnectorOrchestator.payCreditCardSend("21", "2", account, "21", "creditcardBank", "USD", 100.0, "test", 0.0, 0.1);
    }

    @Test(expected = BackendConnectorException.class)
    public void testCreateCreditCardException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreCreditCardConnectorOrchestator.createCreditCard("21", "2", "test");
    }

    @Test(expected = IllegalStateException.class)
    public void testCoreCreditCardConnectorOrchestatorException()  {
        new CoreCreditCardConnectorOrchestator();
    }

}