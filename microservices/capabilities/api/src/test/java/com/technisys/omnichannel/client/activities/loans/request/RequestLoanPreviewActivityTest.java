/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */
package com.technisys.omnichannel.client.activities.loans.request;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.quartz.SchedulerException;
import java.io.IOException;
import java.util.*;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, FormsHandler.class})
public class RequestLoanPreviewActivityTest {

    private Request request;

    @Mock
    private Administration administrationMock;

    @Mock
    private RequestLoanPreviewActivity activity;

    @Mock
    private FormsHandler formsHandlerMock;



    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);

        PowerMockito.mockStatic(FormsHandler.class);
        Mockito.when(FormsHandler.getInstance()).thenReturn(formsHandlerMock);

        request = new Request();
        activity = new RequestLoanPreviewActivity();
    }

    @Test
    public void executeReturnOk() throws Exception {
        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    @Test
    public void validateReturnOK() throws IOException, SchedulerException, ActivityException {
        Map<String, String> result = new HashMap<>();
        Mockito.when(formsHandlerMock.validateRequest(request)).thenReturn(result);
        assertTrue(activity.validate(request).isEmpty());
    }

    @Test(expected = ActivityException.class)
    public void validateReturnIOException() throws IOException, SchedulerException, ActivityException {
        Mockito.when(formsHandlerMock.validateRequest(Mockito.any())).thenThrow(new IOException());
        activity.validate(request);
    }

    @Test(expected = ActivityException.class)
    public void validateReturnSchedulerException() throws IOException, SchedulerException, ActivityException {
        Mockito.when(formsHandlerMock.validateRequest(Mockito.any())).thenThrow(new SchedulerException());
        activity.validate(request);
    }


}
