/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.userdata;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Address;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static com.technisys.omnichannel.client.activities.preferences.userdata.GetAddressInformation.OutParams.ADDRESS;
import static org.junit.Assert.assertEquals;

/*
 *
 * @author Marcelo Bruno
 */

@PowerMockIgnore({ "org.apache.logging.log4j.*", "javax.management.*" })
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Administration.class, CountryCodesHandler.class, AccessManagementHandlerFactory.class, CoreCustomerConnectorOrchestrator.class})
public class GetAddressInformationTest {

    @Mock
    private Administration administrationMock;
    @Mock
    private AccessManagementHandler accessManagementHandlerMock;

    private static Request request;
    private GetAddressInformation activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        activity = new GetAddressInformation();
        request = new Request();

        PowerMockito.mockStatic(CountryCodesHandler.class);
        PowerMockito.mockStatic(CoreCustomerConnectorOrchestrator.class);
        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);

        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        request.setIdActivity("preferences.userData.getUserAddressInformation");
        request.setChannel("frontend");
        request.setIdUser("idUserTest");
        request.setIdTransaction("idTransactionTest");
    }

    @Test(expected = ActivityException.class)
    public void executeThrowException() throws IOException, ActivityException {
        Mockito
                .when(CountryCodesHandler.getCountryList(
                        Mockito.anyString()
                ))
                .thenReturn(null);
        Mockito
                .when(administrationMock.readEnvironment(
                        Mockito.anyInt()
                ))
                .thenThrow(IOException.class);


        activity.execute(request);
    }

    @Test(expected = ActivityException.class)
    public void executeThrowException2() throws IOException, ActivityException, BackendConnectorException {
        Mockito
                .when(CountryCodesHandler.getCountryList(
                        Mockito.anyString()
                ))
                .thenReturn(null);
        Mockito
                .when(administrationMock.readEnvironment(
                        Mockito.anyInt()
                ))
                .thenReturn(new Environment());
        Mockito
                .when(accessManagementHandlerMock.getUser(
                        Mockito.anyString()
                ))
                .thenReturn(new User());
        Mockito
                .when(CoreCustomerConnectorOrchestrator.read(
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString()
                ))
                .thenReturn(null);

        activity.execute(request);
    }

    @Test
    public void testOk() throws IOException, ActivityException, BackendConnectorException {
        Environment environment = new Environment();
        environment.setProductGroupId("1234");
        User user = new User();
        user.setDocumentCountry("US");
        user.setDocumentType("PAS");
        user.setDocumentNumber("1234");
        ClientUser backendClient = new ClientUser();
        backendClient.setAddress("{\"zipcode\":\"33131\",\"country\":\"US\",\"city\":\"Miami\",\"addressLine1\":\"701 Brickell Ave\",\"addressLine2\":\"1550\",\"federalState\":\"FL\"}");
        Mockito
                .when(CountryCodesHandler.getCountryList(
                        Mockito.anyString()
                ))
                .thenReturn(null);
        Mockito
                .when(administrationMock.readEnvironment(
                        Mockito.anyInt()
                ))
                .thenReturn(environment);
        Mockito
                .when(accessManagementHandlerMock.getUser(
                        Mockito.anyString()
                ))
                .thenReturn(user);
        Mockito
                .when(CoreCustomerConnectorOrchestrator.read(
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString()
                ))
                .thenReturn(backendClient);

        Response response = activity.execute(request);

        Address resultAddreess = (Address) response.getData().get(ADDRESS);
        assertEquals("US", resultAddreess.getCountry());
        assertEquals("FL", resultAddreess.getFederalState());
        assertEquals("Miami", resultAddreess.getCity());
        assertEquals("33131", resultAddreess.getZipcode());
        assertEquals("701 Brickell Ave", resultAddreess.getAddressLine1());
        assertEquals("1550", resultAddreess.getAddressLine2());
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

}
