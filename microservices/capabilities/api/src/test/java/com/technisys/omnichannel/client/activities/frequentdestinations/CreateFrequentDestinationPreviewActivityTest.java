package com.technisys.omnichannel.client.activities.frequentdestinations;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.accounts.FundAccountPreviewActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.io.IOException;

import static org.junit.Assert.*;

public class CreateFrequentDestinationPreviewActivityTest {
    private Request request;
    private FundAccountPreviewActivity activity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        request = new Request();
        activity = new FundAccountPreviewActivity();
    }

    @Test
    public void returnsOkTest() throws IOException, ActivityException {
        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

}