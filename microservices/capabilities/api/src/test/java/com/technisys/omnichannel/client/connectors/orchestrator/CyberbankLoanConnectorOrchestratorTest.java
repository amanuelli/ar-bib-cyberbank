/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.*;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.ISOProduct;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.LoanType;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.StatementLoan;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import utils.TestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, CyberbankCoreConnector.class, CyberbankLoanConnector.class, CyberbankConsolidateConnector.class})

public class CyberbankLoanConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyString())).thenReturn("cyberbank");

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CyberbankLoanConnector.class);
        PowerMockito.mockStatic(CyberbankConsolidateConnector.class);

    }

    @Test
    public void testListLoansCyberbankOk() throws BackendConnectorException, CyberbankCoreConnectorException {


        CyberbankCoreConnectorResponse<List<Loan>>  loansDummy = new CyberbankCoreConnectorResponse<>();
        loansDummy.setData(new ArrayList<>());

        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankLoanConnector.list("1",
                new ArrayList<Client>() {{
                    add(new Client("1450", ""));
                }})).thenReturn(loansDummy);


       List<Loan> loans = CoreLoanConnectorOrchestrator.list("1",
                new ArrayList<Client>() {{
                    add(new Client("1450", ""));
                }},
                Arrays.asList(Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY)
        );

        Assert.assertNotNull(loans);
        Assert.assertEquals(0, loans.size());
    }

    @Test(expected = BackendConnectorException.class)
    public void testListLoansCyberbankException() throws CyberbankCoreConnectorException, BackendConnectorException {

        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");

        Mockito.when(CyberbankLoanConnector.list(
                Mockito.any(),
                Mockito.anyList()
        ))
                .thenThrow(new CyberbankCoreConnectorException("exception"));

        Mockito.when(CoreLoanConnectorOrchestrator.list("1",
                new ArrayList<Client>() {{
                    add(new Client("1450", ""));
                }},
                Arrays.asList(new String[]{Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY})
        )).thenThrow(new CyberbankCoreConnectorException("exception"));

    }

    @Test
    public void testListInstallments() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<List<StatementLoan>> installmentsResponse = new CyberbankCoreConnectorResponse<>();
        installmentsResponse.setData(new ArrayList<StatementLoan>() {{
            add(new StatementLoan());
            add(new StatementLoan());
        }});

        Mockito.when(CyberbankLoanConnector.listInstallments(Mockito.any(Product.class))).thenReturn(installmentsResponse);

        List<StatementLoan> statementLoans = CoreLoanConnectorOrchestrator.listInstallments("kjdfh374w384uefh84738wfue83", TestUtils.getValidProduct(), "a", 1, 10);

        Assert.assertNotNull(statementLoans);

    }

    @Test(expected = BackendConnectorException.class)
    public void testListInstallmentsError() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<List<StatementLoan>> installmentsResponse = new CyberbankCoreConnectorResponse<>(1);
        installmentsResponse.setData(new ArrayList<StatementLoan>() {{
            add(new StatementLoan());
            add(new StatementLoan());
        }});

        Mockito.when(CyberbankLoanConnector.listInstallments(Mockito.any(Product.class))).thenReturn(installmentsResponse);

        List<StatementLoan> statementLoans = CoreLoanConnectorOrchestrator.listInstallments("kjdfh374w384uefh84738wfue83", TestUtils.getValidProduct(), "a", 1, 10);

    }



    public void testReadLoansCyberbankOk() throws BackendConnectorException, CyberbankCoreConnectorException {

        CyberbankCoreConnectorResponse<Loan> response = new CyberbankCoreConnectorResponse<>();
        response.setData(TestUtils.getValidLoan());
        Mockito.when(CyberbankLoanConnector.read(TestUtils.getValidProduct())).thenReturn(response);

        Loan loans = CoreLoanConnectorOrchestrator.read("1", TestUtils.getValidProduct());

        Assert.assertNotNull(loans);
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadLoansCyberbankException() throws CyberbankCoreConnectorException, BackendConnectorException {

        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankLoanConnector.read(Mockito.any()))
                .thenThrow(new CyberbankCoreConnectorException("exception"));

        Mockito.when(CoreLoanConnectorOrchestrator.read("1",TestUtils.getValidLoan())).
                thenThrow(new CyberbankCoreConnectorException("exception"));

    }

    @Test
    public void testListLoanTypeCyberbankOk() throws CyberbankCoreConnectorException, BackendConnectorException, IOException {

        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<List<ISOProduct>> isoProductsResponse = new CyberbankCoreConnectorResponse<>(1);
        isoProductsResponse.setData(new ArrayList<ISOProduct>(){{
            add(new ISOProduct());
            add(new ISOProduct());
            add(new ISOProduct());
        }});

        CyberbankCoreConnectorResponse<List<LoanType>> loanTypesResponse = new CyberbankCoreConnectorResponse<>(1);
        loanTypesResponse.setData(new ArrayList<LoanType>(){{
            add(new LoanType());
            add(new LoanType());
            add(new LoanType());
        }});
        Mockito.when(CyberbankConsolidateConnector.listLoanISOProducts()).thenReturn(isoProductsResponse.getData());
        Mockito.when(CyberbankLoanConnector.listLoanType(Mockito.anyList())).thenReturn(loanTypesResponse);

        List<LoanType> loanTypeList = CoreLoanConnectorOrchestrator.listLoanType();
        Assert.assertNotNull(loanTypeList);
    }

    @Test(expected = BackendConnectorException.class)
    public void testListLoanTypeCyberbankException() throws CyberbankCoreConnectorException, BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<List<ISOProduct>> isoProductsResponse = new CyberbankCoreConnectorResponse<>(1);
        isoProductsResponse.setData(new ArrayList<ISOProduct>(){{
            add(new ISOProduct());
            add(new ISOProduct());
            add(new ISOProduct());
        }});
        Mockito.when(CyberbankConsolidateConnector.listLoanISOProducts()).thenReturn(isoProductsResponse.getData());
        Mockito.when(CyberbankLoanConnector.listLoanType(Mockito.anyList())).thenThrow(CyberbankCoreConnectorException.class);
        CoreLoanConnectorOrchestrator.listLoanType();
    }

    @Test
    public void testRequestLoanCyberbank() throws IOException, CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<List<ISOProduct>> isoProductsResponse = new CyberbankCoreConnectorResponse<>(1);
        isoProductsResponse.setData(new ArrayList<ISOProduct>(){{
            add(new ISOProduct("00076", 1, "Test", "Test"));
        }});
        Mockito.when(CyberbankConsolidateConnector.listLoanISOProducts()).thenReturn(isoProductsResponse.getData());

        CyberbankCoreConnectorResponse<Loan> response = new CyberbankCoreConnectorResponse<>();
        response.setData(TestUtils.getValidLoan());

        Account account = new Account();
        Amount amount = new Amount();
        Mockito.when(CyberbankLoanConnector.request(account,isoProductsResponse.getData().get(0).getProductId(), "12", "12", amount, 121)).thenReturn(response);
        Loan loan = CoreLoanConnectorOrchestrator.request(account,"12", "12", amount, 121);
        Assert.assertNotNull(loan);
    }

    @Test(expected = BackendConnectorException.class)
    public void testRequestLoanCyberbankException() throws IOException, CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<List<ISOProduct>> isoProductsResponse = new CyberbankCoreConnectorResponse<>(1);
        isoProductsResponse.setData(new ArrayList<ISOProduct>(){{
            add(new ISOProduct("00076", 1, "Test", "Test"));
        }});

        Account account = new Account();
        Amount amount = new Amount();
        Mockito.when(CyberbankConsolidateConnector.listLoanISOProducts()).thenReturn(isoProductsResponse.getData());
        Mockito.when(CyberbankLoanConnector.request(account,isoProductsResponse.getData().get(0).getProductId(), "12", "12", amount, 121)).thenThrow(CyberbankCoreConnectorException.class);
        CoreLoanConnectorOrchestrator.request(account,"12", "12", amount, 121);
    }

}