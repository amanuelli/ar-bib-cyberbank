package com.technisys.omnichannel.client.activities.loans;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankLoanConnector;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import utils.TestUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;


@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, ConfigurationFactory.class, CoreLoanConnectorOrchestrator.class, CyberbankLoanConnector.class, I18nFactory.class})
public class ReadLoanActivityTest {
    private Request request;

    @Mock
    private Administration administrationMock;

    @Mock
    private Configuration configurationMock;

    @Mock
    private I18nFactory i18nFactoryMock;

    @Mock
    private I18n i18nMock;

    private ReadLoanActivity activity;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        PowerMockito.mockStatic(I18nFactory.class);
        Mockito.when(I18nFactory.getHandler()).thenReturn(i18nMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString()))
                .thenReturn("http://WEBSERVICES_URL:PORT");

        Mockito.when(configurationMock.getString(eq(Configuration.PLATFORM), Mockito.anyString()))
                .thenReturn("USD");

        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(50);

        PowerMockito.mockStatic(CoreLoanConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankLoanConnector.class);

        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyString())).thenReturn("cyberbank");

        request = new Request();
        activity = new ReadLoanActivity();
    }

    @Test
    public void executeReturnOk() throws IOException, ActivityException, BackendConnectorException {
        Environment env = new Environment();
        env.setProductGroupId("a1a1a1a1a1a");

        Mockito.when(administrationMock.readProduct(
                Mockito.anyString(),
                Mockito.anyInt()
        ))
                .thenReturn(TestUtils.getValidProduct());

        Mockito.when(administrationMock.readEnvironment(Mockito.anyInt()))
                .thenReturn(env);

        Mockito.when(i18nMock.getMessage(Mockito.nullable(String.class), Mockito.nullable(String.class))).thenReturn("");

        Mockito.when(CoreLoanConnectorOrchestrator.read(
                Mockito.nullable(String.class),
                Mockito.any()
        ))
                .thenReturn(TestUtils.getValidLoan());

        Map<String, Object> params = new HashMap<>();
        params.put(ReadLoanActivity.InParams.ID_LOAN, "123");
        request.setParameters(params);
        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    @Test(expected = ActivityException.class)
    public void testReadLoansCyberbankException() throws ActivityException, BackendConnectorException {

        Mockito.when(CoreLoanConnectorOrchestrator.read(Mockito.anyString(), Mockito.any()))
                .thenThrow(new BackendConnectorException("exception"));

        Response response = activity.execute(request);

    }

    @Test(expected = ActivityException.class)
    public void executeReturnIOException() throws ActivityException, IOException {
        Mockito.doThrow(new IOException()).when(administrationMock).readEnvironment(Mockito.anyInt());

        Response response = activity.execute(request);
    }

}
