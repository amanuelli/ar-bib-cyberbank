/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.*;
import com.technisys.omnichannel.client.domain.CurrencyExchange;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, CyberbankCoreConnector.class, CyberbankCustomerConnector.class, CyberbankExchangeRateConnector.class})

public class CyberbankExchangeRateConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CyberbankCustomerConnector.class);
        PowerMockito.mockStatic(CyberbankExchangeRateConnector.class);
    }

    @Test
    public void testlistExchangeRates() throws BackendConnectorException, CyberbankCoreConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");

        CyberbankCoreConnectorResponse<Integer> response =  new CyberbankCoreConnectorResponse<>();
        response.setData(1);

        CyberbankCoreConnectorResponse<List<CurrencyExchange>> responseExchange = new CyberbankCoreConnectorResponse<>();
        responseExchange.setData(new ArrayList<>());

        Mockito.when(CyberbankCustomerConnector.getClassification(Mockito.anyString())).thenReturn(response);

        Mockito.when(CyberbankExchangeRateConnector.listExchangeRates(Mockito.anyString(), Mockito.anyInt())).thenReturn(responseExchange);

        List<CurrencyExchange> exchangeList = CoreExchangeRateConnectorOrchestrator.listExchangeRates("121", "121", "21");
        Assert.assertNotNull(exchangeList);
    }

    @Test
    public void testlistExchangeRatesEmpty() throws BackendConnectorException, CyberbankCoreConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");

        CyberbankCoreConnectorResponse<Integer> response =  new CyberbankCoreConnectorResponse<>();
        response.setData(1);

        CyberbankCoreConnectorResponse<List<CurrencyExchange>> responseExchange = new CyberbankCoreConnectorResponse<>();
        responseExchange.setData(new ArrayList<>());
        responseExchange.setCode(1);

        Mockito.when(CyberbankCustomerConnector.getClassification(Mockito.anyString())).thenReturn(response);

        Mockito.when(CyberbankExchangeRateConnector.listExchangeRates(Mockito.anyString(), Mockito.anyInt())).thenReturn(responseExchange);

        List<CurrencyExchange> exchangeList = CoreExchangeRateConnectorOrchestrator.listExchangeRates("121", "121", "21");
        Assert.assertNotNull(exchangeList);
    }

    @Test(expected = BackendConnectorException.class)
    public void testlistExchangeRatesException() throws BackendConnectorException, IOException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankCustomerConnector.getClassification(Mockito.anyString())).thenThrow(new CyberbankCoreConnectorException("Error"));
        CoreExchangeRateConnectorOrchestrator.listExchangeRates("121", "121", "21");
    }


}