package com.technisys.omnichannel.client.activities.session;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Country;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.session.legacy.RegisterUserDeviceActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.postloginchecks.geoip.PostLoginCheckGeoIPDataAccess;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ExchangeTokenHandler.class, ConfigurationFactory.class, PostLoginCheckGeoIPDataAccess.class, CityResponse.class, AccessManagementHandlerFactory.class,})
public class RegisterUserDeviceActivityTest {

    @Mock
    private Configuration configurationMock;

    @Mock
    private PostLoginCheckGeoIPDataAccess postLoginCheckIpDataAccessMock;

    @Mock
    private AccessManagementHandler accessManagementHandlerMock;

    private Request request;
    private RegisterUserDeviceActivity activity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        PowerMockito.mockStatic(PostLoginCheckGeoIPDataAccess.class);
        PowerMockito.mockStatic(ExchangeTokenHandler.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);

        Mockito
                .when(ConfigurationFactory.getInstance())
                .thenReturn(configurationMock);
        Mockito
                .when(PostLoginCheckGeoIPDataAccess.getInstance())
                .thenReturn(postLoginCheckIpDataAccessMock);
        Mockito
                .when(AccessManagementHandlerFactory.getHandler())
                .thenReturn(accessManagementHandlerMock);
        Mockito
                .doNothing()
                .when(accessManagementHandlerMock)
                .createOrUpdateUserDevice(Mockito.any());

        request = new Request();
        activity = new RegisterUserDeviceActivity();
    }

    @Test(expected = ActivityException.class)
    public void executeIdDeviceEmptyError() throws IOException, ActivityException {
        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    @Test
    public void executeOk() throws IOException, ActivityException, GeoIp2Exception {
        String testIP = "190.0.247.226";
        InetAddress inetAddress = InetAddress.getByName(testIP);
        request.setClientIP(testIP);
        Map<String, Object> params = new HashMap<>();
        params.put(RegisterUserDeviceActivity.InParams.ID_DEVICE, "123456789");
        params.put(RegisterUserDeviceActivity.InParams.EXTRA_INFO, "{}");
        params.put(RegisterUserDeviceActivity.InParams.EXCHANGE_TOKEN, "Mokaajkuyd8a68dyasASADjaksjd9ahVk87ejhasjdh71yjashd");
        request.setParameters(params);

        //Mock CityResponse
        CityResponse cityResponseMock = PowerMockito.mock(CityResponse.class);
        Mockito.when(postLoginCheckIpDataAccessMock.getCity(inetAddress)).thenReturn(cityResponseMock);

        //Mock Country in CityResponse
        Country countryMock = new Country(null, null, null, null, null);
        Mockito.when(cityResponseMock.getCountry()).thenReturn(countryMock);


        //Mock Country in CityResponse
        City cityMock = new City();
        Mockito.when(cityResponseMock.getCity()).thenReturn(cityMock);

        //mock retrieveIdUser
        ExchangeToken exchangeTokenMock = new ExchangeToken();
        exchangeTokenMock.setIdUser("123");

        //mock ExchangeTokenHandler
        Mockito.when(ExchangeTokenHandler.read(Mockito.anyString())).thenReturn(exchangeTokenMock);

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }
}