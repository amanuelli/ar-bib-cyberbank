package com.technisys.omnichannel.client.activities.dummy;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.NotificableActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;

public class MANNAZCA8759InvalidTransProdActivity extends Activity {
    public static final String ID = "MANNAZCA8759.invalidTransactionProduct";

    public interface InParams {

        String ID_DEBIT_ACCOUNT = "debitAccount";
        String ID_CREDIT_ACCOUNT = "creditAccount";
        String AMOUNT = "amount";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        response.setReturnCode(ReturnCodes.OK);

        return response;
    }

}
