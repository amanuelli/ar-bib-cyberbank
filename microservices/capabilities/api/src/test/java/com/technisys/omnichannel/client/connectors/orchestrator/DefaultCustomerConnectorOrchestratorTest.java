/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.domain.Address;
import com.technisys.omnichannel.client.domain.JobInformation;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.PhoneNumber;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, CyberbankCoreConnector.class})

public class DefaultCustomerConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM),  Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM),  Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);

    }

    @Test(expected = BackendConnectorException.class)
    public void testAdd() throws Exception {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        JobInformation jobInformation = new JobInformation();
        Amount amount = new Amount();
        jobInformation.setIncome(amount);
        Address address = new Address();
        PhoneNumber mobilePhone = new PhoneNumber();

        CoreCustomerConnectorOrchestrator.add("1123",
                "test",
                "Test",
                "Tes@hotmail.com",
                "777777",
                mobilePhone,
                "1111111",
                "CI",
                "CL",
                "Chilena",
                "CA",
                "M",
                "Test",
                false,
                "28",
                address,
                jobInformation
        );
    }

    @Test(expected = BackendConnectorException.class)
    public void  read() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        CoreCustomerConnectorOrchestrator.read("1", "CL", "CI", "1111111", "21");
    }

    @Test(expected = BackendConnectorException.class)
    public void updateClientAddress() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("other");
        Address address = new Address();
        CoreCustomerConnectorOrchestrator.updateClientAddress("121", 0, address, address);
    }

}