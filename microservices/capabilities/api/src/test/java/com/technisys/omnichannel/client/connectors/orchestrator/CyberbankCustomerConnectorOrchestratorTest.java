/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCustomerConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.CyberbankCoreCustomer;
import com.technisys.omnichannel.client.domain.Address;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.domain.JobInformation;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.PhoneNumber;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, CyberbankCoreConnector.class, CyberbankCustomerConnector.class, CountryCodesHandler.class, CyberbankCustomerConnector.class, CoreCustomerConnectorOrchestrator.class})

public class CyberbankCustomerConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CyberbankCustomerConnector.class);
        PowerMockito.mockStatic(CountryCodesHandler.class);
        PowerMockito.mockStatic(CyberbankCustomerConnector.class);
    }

    @Test
    public void testAdd() throws Exception {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CountryCodesHandler.getCoreCodeByAlpha3("chilena")).thenReturn("chilena");
        CyberbankCoreConnectorResponse<String> response = new CyberbankCoreConnectorResponse<>();
        response.setData("121");
        response.setCode(0);
        Mockito.when(CyberbankCustomerConnector.create(Mockito.any(CyberbankCoreCustomer.class))).thenReturn(response);

        JobInformation jobInformation = new JobInformation();
        Amount amount = new Amount();
        jobInformation.setIncome(amount);
        Address address = new Address();
        PhoneNumber mobilePhone = new PhoneNumber();

        String id = CoreCustomerConnectorOrchestrator.add("1123",
                "test",
                "Test",
                "Tes@hotmail.com",
                "777777",
                mobilePhone,
                "1111111",
                "CI",
                "CL",
                "Chilena",
                "CA",
                "M",
                "Test",
                false,
                "28",
                address,
                jobInformation
        );
        Assert.assertEquals("121", id);
    }


    @Test(expected = BackendConnectorException.class)
    public void testAddException() throws Exception {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CountryCodesHandler.getCoreCodeByAlpha3("chilena")).thenReturn("chilena");
        CyberbankCoreConnectorResponse<String> response = new CyberbankCoreConnectorResponse<>();
        response.setData("121");
        response.setCode(0);
        Mockito.when(CyberbankCustomerConnector.create(Mockito.any(CyberbankCoreCustomer.class))).thenThrow(new CyberbankCoreConnectorException("Error"));

        JobInformation jobInformation = new JobInformation();
        Amount amount = new Amount();
        jobInformation.setIncome(amount);
        Address address = new Address();
        PhoneNumber mobilePhone = new PhoneNumber();

        CoreCustomerConnectorOrchestrator.add("1123",
                "test",
                "Test",
                "Tes@hotmail.com",
                "777777",
                mobilePhone,
                "1111111",
                "CI",
                "CL",
                "Chilena",
                "CA",
                "M",
                "Test",
                false,
                "28",
                address,
                jobInformation
        );

    }

    @Test
    public void  read() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when((CyberbankCustomerConnector.read("CI", "1111111"))).thenReturn(new CyberbankCoreConnectorResponse<>());
        ClientUser clientUser =  CoreCustomerConnectorOrchestrator.read("1", "CL", "CI", "1111111", "21");
        Assert.assertNull(clientUser);
    }

    @Test(expected = BackendConnectorException.class)
    public void  readException() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when((CyberbankCustomerConnector.read("CI", "1111111"))).thenThrow(new BackendConnectorException("Error"));
        CoreCustomerConnectorOrchestrator.read("1", "CL", "CI", "1111111", "21");
    }

    @Test
    public void readClientEnvironment() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<ClientUser> response = new CyberbankCoreConnectorResponse<>();
        response.setCode(0);
        response.setData(new ClientUser());
        Mockito.when(CyberbankCustomerConnector.read("CI", "2122")).thenReturn(response);
        ClientEnvironment clientEnvironment = CoreCustomerConnectorOrchestrator.readClientEnvironment("1", "21", "CI", "2122");
        Assert.assertNotNull(clientEnvironment);
    }

    @Test
    public void readClientEnvironmentError() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankCustomerConnector.read("CI", "2122")).thenThrow(new CyberbankCoreConnectorException("Error"));
        ClientEnvironment clientEnvironment = CoreCustomerConnectorOrchestrator.readClientEnvironment("1", "21", "CI", "2122");
        Assert.assertNull(clientEnvironment);
    }

    @Test
    public void listClients() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<ClientUser> response = new CyberbankCoreConnectorResponse<>();
        response.setCode(0);
        response.setData(new ClientUser());
        Mockito.when(CyberbankCustomerConnector.read("CI", "131332")).thenReturn(response);
        List<ClientEnvironment> clientEnvironmentList  = CoreCustomerConnectorOrchestrator.listClients("1", "CL","CI", "131332");
        Assert.assertNotNull(clientEnvironmentList);
    }

    @Test(expected = BackendConnectorException.class)
    public void listClientException() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankCustomerConnector.read("CI", "131332")).thenThrow(new CyberbankCoreConnectorException("Error"));
        CoreCustomerConnectorOrchestrator.listClients("1", "CL","CI", "131332");
    }

    @Test(expected = BackendConnectorException.class)
    public void updateClientAddress() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Address address = new Address();
        CoreCustomerConnectorOrchestrator.updateClientAddress("121", 0, address, address);
    }

}