package com.technisys.omnichannel.client;

import com.technisys.omnichannel.Dispatcher;
import com.technisys.omnichannel.client.activities.dummy.MANNAZCA8759InvalidTransProdActivity;
import com.technisys.omnichannel.core.IBResponse;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.credentials.Credential;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.*;

/*
Data to reproduce it:

    insert into activities (id_activity, ENABLED, component_fqn, id_group, auditable, kind)
        VALUES ('MANNAZCA8759.invalidTransactionProduct', 1, 'com.technisys.omnichannel.client.activities.dummy.MANNAZCA8759InvalidTransProdActivity', 'tests', 2, 'Transactional')

    insert into activity_products (id_activity, id_field, id_permission)
        VALUES ('MANNAZCA8759.invalidTransactionProduct', 'debitAccount', 'product.read')

    Then, add breakpoints in Dispatcher line 137, and in Request line 242
    To reproduce it:

    Run  rirst thread until line 242
    Then finish the second thread
    Finally finish the first thread

 */

public class MANNAZCA8759InvalidTransactionProduct {

    public static final Logger LOG = LoggerFactory.getLogger(MANNAZCA8759InvalidTransactionProduct.class);

    private Request getRequest(String debitAccount) {
        Request request = new Request();
        request.setIdActivity(MANNAZCA8759InvalidTransProdActivity.ID);
        request.setClientIP("192.168.0.17");
        request.setDateTime(new Date());
        request.setIdEnvironment(14);
        request.setChannel("frontend");
        request.setIdUser("f6cadaap64fe4hjead71456813d41afo");

        List<Credential> credentials = new ArrayList<>();
        credentials.add(new Credential(Credential.ACCESS_TOKEN_CREDENTIAL, "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJlbnZpcm9ubWVudCI6MTQsImFnZW50IjoiMzczMDgyNjMyMDVjNDJmMDQ0ZjI3ZTIzMWY2Y2I5M2E0MDA4NzI2NiIsImlwIjoiMDowOjA6MDowOjA6MDoxIiwiY2hhbm5lbCI6ImZyb250ZW5kIiwiaXNzdWVkIjoxNTcyMjcyNDQ0LCJ1c2VyIjoiZjZjYWRhYXA2NGZlNGhqZWFkNzE0NTY4MTNkNDFhZm8ifQ.PcCq_281gE8RwCeL43Q4FxTNbzo3PfYcNVu2ie5crAc"));

        request.setCredentials(credentials);

        request.putParam(MANNAZCA8759InvalidTransProdActivity.InParams.ID_CREDIT_ACCOUNT, "102531e958a742f86abd895e8542188e619b188b");
        request.putParam(MANNAZCA8759InvalidTransProdActivity.InParams.ID_DEBIT_ACCOUNT, debitAccount);
        request.putParam(MANNAZCA8759InvalidTransProdActivity.InParams.AMOUNT, 100);

        request.setCredentials(credentials);

        return request;
    }

//    @Test
    public void testInvalidTransactionProduct() throws IOException {
        Request request1 = getRequest("1358187bf520d81c385067eef540e1537eeb8366");
        Request request2 = getRequest("45d097ce1122c973305fe883c9741abac7e08cf7");

        ExecutorService executor = Executors.newFixedThreadPool(5);

        List<Future> futures = new ArrayList<>();

        futures.add(executor.submit(new RunTransactionCallable(request1)));
        futures.add(executor.submit(new RunTransactionCallable(request2)));

        for (Future f : futures) {
            try {
                Object object = f.get();
                if (object instanceof Response) {
                    IBResponse response = (IBResponse) object;

                    LOG.info("Return code: " + response.getReturnCode());

                } else {
                    LOG.error("Response");
                    System.out.println("Response is not valid");
                }
            } catch (InterruptedException ie) {
                ie.printStackTrace();
            } catch (ExecutionException ee) {
                ee.printStackTrace();
            }
        }

        executor.shutdown();

    }
}

class RunTransactionCallable implements Callable {

    private Request request;

    public RunTransactionCallable(Request request) {
        this.request = request;
    }

    @Override
    public Object call() throws Exception {
        Response response = Dispatcher.execute(request);
        return response;
    }
}