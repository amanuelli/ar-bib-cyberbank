package com.technisys.omnichannel.client.activities.session;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.maxmind.geoip2.record.City;
import com.maxmind.geoip2.record.Country;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.postloginchecks.geoip.PostLoginCheckGeoIPDataAccess;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.net.InetAddress;

import static org.junit.Assert.assertEquals;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigurationFactory.class, PostLoginCheckGeoIPDataAccess.class, CityResponse.class})
public class GetUserCountryActivityTest {
    @Mock
    private Configuration configurationMock;

    @Mock
    private PostLoginCheckGeoIPDataAccess postLoginCheckIpDataAccessMock;

    private Request request;
    private GetUserCountryActivity activity;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        PowerMockito.mockStatic(PostLoginCheckGeoIPDataAccess.class);

        Mockito
                .when(ConfigurationFactory.getInstance())
                .thenReturn(configurationMock);
        Mockito
                .when(PostLoginCheckGeoIPDataAccess.getInstance())
                .thenReturn(postLoginCheckIpDataAccessMock);

        request = new Request();
        activity = new GetUserCountryActivity();
    }

    @Test
    public void executeOk() throws IOException, ActivityException, GeoIp2Exception {
        String testIP = "190.0.247.226";
        InetAddress inetAddress = InetAddress.getByName(testIP);
        request.setClientIP(testIP);

        //Mock CityResponse
        CityResponse cityResponseMock = PowerMockito.mock(CityResponse.class);
        Mockito.when(postLoginCheckIpDataAccessMock.getCity(inetAddress)).thenReturn(cityResponseMock);

        //Mock Country in CityResponse
        Country countryMock = new Country(null, null, null, "CO", null);
        Mockito.when(cityResponseMock.getCountry()).thenReturn(countryMock);


        //Mock Country in CityResponse
        City cityMock = new City();
        Mockito.when(cityResponseMock.getCity()).thenReturn(cityMock);

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }
}