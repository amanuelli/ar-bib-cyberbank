/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.activities.administration.advanced;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.*;

import static org.junit.Assert.assertEquals;

/*
 *
 * @author emordezki
 */

@PowerMockIgnore({ "org.apache.logging.log4j.*", "javax.management.*" })
@RunWith(PowerMockRunner.class)
@PrepareForTest({ ValidationUtils.class, Administration.class, Authorization.class, ModifyGroupsOfUserActivity.class, AccessManagementHandlerFactory.class })
public class ModifyGroupsOfUserActivityTest {

    @Mock
    private Administration administrationMock;

    @Mock
    private AccessManagementHandler accessManagementHandlerMock;

    private static Request request;
    private ModifyGroupsOfUserActivity activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        activity = new ModifyGroupsOfUserActivity();
        request = new Request();

        request.setIdActivity("transfers.etransfers.registerAsSender");
        request.setChannel("frontend");

        request.setClientIP("127.0.0.1");
        request.setDateTime(new Date());

        request.setIdEnvironment(9998);
        request.setIdUser("coreuser1");

        request.addCredential(new Credential("password", "password"));
    }

    @Test
    public void executeValid() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        int groupId = 1;
        params.put("idUser", request.getIdUser());
        params.put("groups", new Integer[] { groupId });
        request.setParameters(params);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        Mockito.doNothing().when(accessManagementHandlerMock).updateUserGroups(Mockito.anyString(), Mockito.anyInt(), Mockito.anyList());

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    @Test(expected = ActivityException.class)
    public void executeIOError() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        int groupId = 1;
        params.put("idUser", request.getIdUser());
        params.put("groups", new Integer[] { groupId });
        request.setParameters(params);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        Mockito.doThrow(IOException.class).when(accessManagementHandlerMock).updateUserGroups(Mockito.anyString(), Mockito.anyInt(), Mockito.anyList());

        activity.execute(request);
    }

    @Test
    public void validateValid() throws ActivityException, IOException {
        Map<String, Object> params = new HashMap<>();
        int groupId = 1;
        params.put("idUser", request.getIdUser());
        params.put("groups", new Integer[] { groupId });
        request.setParameters(params);

        PowerMockito.mockStatic(ModifyGroupsOfUserActivity.class);
        Mockito.when(ModifyGroupsOfUserActivity.validateFields(request)).thenReturn(new HashMap<>());

        PowerMockito.mockStatic(ValidationUtils.class);
        Mockito.when(ValidationUtils.validateEmptyCredentials(request)).thenReturn(new HashMap<>());

        Map<String, String> result = activity.validate(request);

        assertEquals(0, result.size());
    }

    @Test(expected = ActivityException.class)
    public void validateInvalidCredentials() throws IOException, ActivityException {
        PowerMockito.mockStatic(ValidationUtils.class);
        Mockito.when(ValidationUtils.validateEmptyCredentials(request)).thenThrow(IOException.class);

        activity.validate(request);
    }

    @Test
    public void validateFieldsValid() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        int groupId = 1;
        params.put("idUser", request.getIdUser());
        params.put("groups", new Integer[] { groupId });
        request.setParameters(params);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        List<Integer> groupIdsMock = new ArrayList<>();
        groupIdsMock.add(groupId);
        Mockito.when(administrationMock.listEnvironmentGroupIds(Mockito.anyInt())).thenReturn(groupIdsMock);

        List<GroupPermission> groupPermissionsMock = new ArrayList<>();
        GroupPermission gp = new GroupPermission(groupId, Authorization.TARGET_NONE, Constants.ADMINISTRATION_VIEW_PERMISSION);
        groupPermissionsMock.add(gp);
        Mockito.when(accessManagementHandlerMock.getGroupPermissions(Mockito.anyInt())).thenReturn(groupPermissionsMock);

        PowerMockito.mockStatic(Authorization.class);
        Mockito.when(Authorization.hasPermission(Mockito.anyString(), Mockito.anyInt(), Mockito.any(), Mockito.anyString())).thenReturn(true);

        Map<String, String> result = ModifyGroupsOfUserActivity.validateFields(request);

        assertEquals(0, result.size());
    }

    @Test
    public void validateFieldsEmptyUser() throws ActivityException {
        Map<String, String> result = ModifyGroupsOfUserActivity.validateFields(request);

        assertEquals("administration.users.mustSelectUser", result.get("groups"));
    }

    @Test
    public void validateFieldsEmptyGroup() throws ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put("idUser", request.getIdUser());
        request.setParameters(params);

        Map<String, String> result = ModifyGroupsOfUserActivity.validateFields(request);

        assertEquals("administration.users.mustSelectGroup", result.get("groups"));
    }

    @Test(expected = ActivityException.class)
    public void validateFieldsEmptyGroupIds() throws ActivityException, IOException {
        Map<String, Object> params = new HashMap<>();
        int groupId = 1;
        params.put("idUser", request.getIdUser());
        params.put("groups", new Integer[] { groupId });
        request.setParameters(params);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        Mockito.when(administrationMock.listEnvironmentGroupIds(Mockito.anyInt())).thenReturn(null);

        ModifyGroupsOfUserActivity.validateFields(request);
    }

    @Test(expected = ActivityException.class)
    public void validateFieldsNotAuthorized() throws ActivityException, IOException {
        Map<String, Object> params = new HashMap<>();
        int groupId = 1;
        params.put("idUser", request.getIdUser());
        params.put("groups", new Integer[] { groupId });
        request.setParameters(params);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        List<Integer> groupIdsMock = new ArrayList<>();
        groupIdsMock.add(2);
        Mockito.when(administrationMock.listEnvironmentGroupIds(Mockito.anyInt())).thenReturn(groupIdsMock);

        ModifyGroupsOfUserActivity.validateFields(request);
    }

    @Test
    public void validateFieldsNoRemoveAdminPermission() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        int groupId = 1;
        params.put("idUser", request.getIdUser());
        params.put("groups", new Integer[] { groupId });
        request.setParameters(params);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        List<Integer> groupIdsMock = new ArrayList<>();
        groupIdsMock.add(groupId);
        Mockito.when(administrationMock.listEnvironmentGroupIds(Mockito.anyInt())).thenReturn(groupIdsMock);

        List<GroupPermission> groupPermissionsMock = new ArrayList<>();
        GroupPermission gp = new GroupPermission(groupId + 1, Authorization.TARGET_NONE, Constants.ADMINISTRATION_VIEW_PERMISSION);
        groupPermissionsMock.add(gp);
        Mockito.when(accessManagementHandlerMock.getGroupPermissions(Mockito.anyInt())).thenReturn(groupPermissionsMock);

        PowerMockito.mockStatic(Authorization.class);
        Mockito.when(Authorization.hasPermission(Mockito.anyString(), Mockito.anyInt(), Mockito.any(), Mockito.anyString())).thenReturn(true);

        Map<String, String> result = ModifyGroupsOfUserActivity.validateFields(request);

        assertEquals("administration.users.cantRemoveAdminPermission", result.get("groups"));
    }

    @Test
    public void validateFieldsNoAddAdminPermission() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        int groupId = 1;
        params.put("idUser", request.getIdUser());
        params.put("groups", new Integer[] { groupId });
        request.setParameters(params);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        List<Integer> groupIdsMock = new ArrayList<>();
        groupIdsMock.add(groupId);
        Mockito.when(administrationMock.listEnvironmentGroupIds(Mockito.anyInt())).thenReturn(groupIdsMock);

        List<GroupPermission> groupPermissionsMock = new ArrayList<>();
        GroupPermission gp = new GroupPermission(groupId, Authorization.TARGET_NONE, Constants.ADMINISTRATION_VIEW_PERMISSION);
        groupPermissionsMock.add(gp);
        Mockito.when(accessManagementHandlerMock.getGroupPermissions(Mockito.anyInt())).thenReturn(groupPermissionsMock);

        PowerMockito.mockStatic(Authorization.class);
        Mockito.when(Authorization.hasPermission(Mockito.anyString(), Mockito.anyInt(), Mockito.any(), Mockito.anyString())).thenReturn(false);

        Map<String, String> result = ModifyGroupsOfUserActivity.validateFields(request);

        assertEquals("administration.users.cantAddAdminPermission", result.get("groups"));
    }

    @Test(expected = ActivityException.class)
    public void validateFieldsIOError() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        int groupId = 1;
        params.put("idUser", request.getIdUser());
        params.put("groups", new Integer[] { groupId });
        request.setParameters(params);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        Mockito.when(administrationMock.listEnvironmentGroupIds(Mockito.anyInt())).thenThrow(IOException.class);

        ModifyGroupsOfUserActivity.validateFields(request);
    }
}
