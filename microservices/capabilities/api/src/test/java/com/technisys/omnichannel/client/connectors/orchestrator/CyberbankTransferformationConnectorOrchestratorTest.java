/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.*;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.TransferForeignBankDetail;
import com.technisys.omnichannel.client.domain.TransferInternalDetail;
import com.technisys.omnichannel.client.domain.TransferOtherBankDetails;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Currency;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({"com.technisys.omnichannel.core.utils.SecurityUtils"})
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, Administration.class, SecurityUtils.class, CoreConnectorOrchestrator.class, CyberbankCoreConnector.class, CyberbankTransferConnector.class, CyberbankGeneralServicesConnector.class})

public class CyberbankTransferformationConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;

    @Mock
    Administration administrationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);
        PowerMockito.mockStatic(ConfigurationFactory.class);
        PowerMockito.mockStatic(Administration.class);

        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CyberbankTransferConnector.class);
        PowerMockito.mockStatic(CyberbankGeneralServicesConnector.class);
    }

    @Test
    public void testThirdPartyValidate() throws IOException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Product product = new Product();
        product.setExtraInfo("CA|3334|USD|01000|10|10001|1|1");

        Mockito.when(administrationMock.readProduct(Mockito.anyString(), Mockito.anyInt())).thenReturn(product);
        Account account = new Account();
        Amount ammount = new Amount();
        TransferInternalDetail transferInternalDetail = CoreTransferConnectorOrchestrator.thirdPartyValidate(
                "21",
                0,
                "2123",
                account,
                "CA|3334|USD|01000|10|10001|1|1",
                ammount,
                "Reference test",
                "Credit Reference"
        );
        Assert.assertNotNull(transferInternalDetail);
    }

    @Test
    public void thirdParty() throws IOException, CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Product product = new Product();
        product.setExtraInfo("CA|3334|USD|01000|10|10001|1|1");

        CyberbankCoreConnectorResponse<TransferInternalDetail> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new TransferInternalDetail());
        response.setCode(0);

        Mockito.when(administrationMock.readProduct(Mockito.anyString(), Mockito.anyInt())).thenReturn(product);
        Mockito.when(CyberbankTransferConnector.internal(Mockito.any(Account.class), Mockito.any(Account.class), Mockito.anyDouble(), Mockito.anyString() )).thenReturn(response);
        Account account = new Account();
        Amount ammount = new Amount();

        TransferInternalDetail transferInternalDetail = CoreTransferConnectorOrchestrator.thirdParty(
                "21",
                0,
                "2123",
                account,
                "CA|3334|USD|01000|10|10001|1|1",
                ammount,
                "Reference test",
                "Credit Reference"
        );
        Assert.assertNotNull(transferInternalDetail);
    }

    @Test
    public void thirdPartyError() throws IOException, CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Product product = new Product();
        product.setExtraInfo("CA|3334|USD|01000|10|10001|1|1");
        CyberbankCoreError cyberbankCoreError = new CyberbankCoreError(new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"error\",\n" +
                "    \"transactionId\": \"processEft_TransferInsert_Other_Bank_Channel\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.error_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"codigo\": 1,\n" +
                "                \"description\": \"[in.message]\",\n" +
                "                \"id\": 1,\n" +
                "                \"codigoerror\": 1179\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Error\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"null.null\"\n" +
                "    }\n" +
                "}"));


        CyberbankCoreConnectorResponse<TransferInternalDetail> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new TransferInternalDetail());
        response.setCode(1);
        response.setError(cyberbankCoreError);

        Mockito.when(administrationMock.readProduct(Mockito.anyString(), Mockito.anyInt())).thenReturn(product);
        Mockito.when(CyberbankTransferConnector.internal(Mockito.any(Account.class), Mockito.any(Account.class), Mockito.anyDouble(), Mockito.anyString() )).thenReturn(response);
        Account account = new Account();
        Amount ammount = new Amount();

        TransferInternalDetail transferInternalDetail = CoreTransferConnectorOrchestrator.thirdParty(
                "21",
                0,
                "2123",
                account,
                "CA|3334|USD|01000|10|10001|1|1",
                ammount,
                "Reference test",
                "Credit Reference"
        );
        Assert.assertNotNull(transferInternalDetail);
    }

    @Test
    public void localBanks() throws CyberbankCoreConnectorException, IOException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");

        CyberbankCoreConnectorResponse<TransferOtherBankDetails> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new TransferOtherBankDetails());
        response.setCode(0);

        Account account = new Account();
        Amount ammount = new Amount();
        ammount.setCurrency("USD");

        CyberbankCoreConnectorResponse<Currency> cyberbankCoreConnectorResponse = new CyberbankCoreConnectorResponse<> ();
        Currency currency = new Currency();
        currency.setCode("USD");
        currency.setBackendId("USD");
        cyberbankCoreConnectorResponse.setData(currency);
        Mockito.when(CyberbankGeneralServicesConnector.getCurrency(Mockito.anyString())).thenReturn(cyberbankCoreConnectorResponse);

        Mockito.when(CyberbankTransferConnector.toLocalBanks(
                Mockito.anyString(),
                Mockito.any(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString()
                )).thenReturn(response);

        TransferOtherBankDetails transferOtherBankDetails = CoreTransferConnectorOrchestrator.localBanks(
                "21",
                account,
                "2123",
                "Test",
                "CA|3334|USD|01000|10|10001|1|1",
                ammount,
                "Reference test",
                "Credit Reference"
        );
        Assert.assertNotNull(transferOtherBankDetails);
    }

    @Test
    public void foreignBanks() throws CyberbankCoreConnectorException, IOException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");

        CyberbankCoreConnectorResponse<TransferForeignBankDetail> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new TransferForeignBankDetail());
        response.setCode(0);

        Account account = new Account();
        Amount ammount = new Amount();
        ammount.setCurrency("USD");

        CyberbankCoreConnectorResponse<Currency> cyberbankCoreConnectorResponse = new CyberbankCoreConnectorResponse<> ();
        Currency currency = new Currency();
        currency.setCode("USD");
        currency.setBackendId("USD");
        cyberbankCoreConnectorResponse.setData(currency);
        Mockito.when(CyberbankGeneralServicesConnector.getCurrency(Mockito.anyString())).thenReturn(cyberbankCoreConnectorResponse);

        Mockito.when(CyberbankTransferConnector.toForeignlBank(
                Mockito.anyString(),
                Mockito.any(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.any(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString(),
                Mockito.anyString()
        )).thenReturn(response);

        TransferForeignBankDetail transferForeignBankDetail = CoreTransferConnectorOrchestrator.foreignBanks(
                "21",
                account,
                "2123",
                "Test",
                "CA|3334|USD|01000|10|10001|1|1",
                ammount,
                "Reference test",
                "21",
                "Reference test",
                "Credit Reference",
                "Credit Reference"
        );
        Assert.assertNotNull(transferForeignBankDetail);
    }


}