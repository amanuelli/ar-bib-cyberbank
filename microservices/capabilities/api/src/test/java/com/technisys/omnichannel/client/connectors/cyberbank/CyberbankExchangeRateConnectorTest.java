package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.cyberbank.domain.QuotationType;
import com.technisys.omnichannel.client.domain.CurrencyExchange;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.UserData;
import com.technisys.omnichannel.core.utils.CacheUtils;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.core.classloader.annotations.SuppressStaticInitializationFor;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;

@RunWith(PowerMockRunner.class)
@SuppressStaticInitializationFor({"com.technisys.omnichannel.core.utils.SecurityUtils"})
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({
        ConfigurationFactory.class,
        SecurityUtils.class,
        CyberbankCoreConnector.class,
        CyberbankCoreRequest.class,
        AccessManagementHandlerFactory.class,
        CacheUtils.class
})
public class CyberbankExchangeRateConnectorTest {

    @Mock
    private Configuration configurationMock;

    @Mock
    private AccessManagementHandler accessManagementHandlerMock;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), eq("connector.cyberbank.core.template.request"), eq("/templates/connectors/cyberbank/request/"))).thenReturn("/templates/connectors/cyberbank/request/");
        Mockito.when(accessManagementHandlerMock.readUserData(Mockito.anyString())).thenReturn(new UserData("idUser", "{\"stampAdditional\":\"1\", \"quotationTypePhysicId\":\"1\"}"));


        PowerMockito.mockStatic(CyberbankCoreRequest.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CacheUtils.class);

    }

    @Test
    public void testListExchangeCurrenciesOk() throws IOException, CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"massiveSelectQuotationBy_Date\",\"transactionId\":\"massiveSelectQuotationBy_Date\",\"transactionVersion\":\"0.0\",\"out.quotation_list\":{\"collection\":[{\"quotSale\":\"66.00000\",\"quotArbitrage\":\"2.00000\",\"quotBuy\":\"65.00000\",\"systemUser\":null,\"parityUsd\":\"1.00000\",\"dateFrom\":\"19691231210000000\",\"stampAdditional\":\"5\",\"institution\":{\"institutionId\":1111,\"shortDesc\":\"Technisys - Baseline\",\"id\":2},\"stampDateTime\":\"20190927143707254\",\"expressionUnit\":1,\"zone\":{\"zoneQuotationId\":1,\"nemotecnico\":\"BUENOS AIRES\",\"shortDesc\":\"Buenos Aires\",\"id\":1,\"longDesc\":\"Buenos Aires\"},\"dateTo\":\"20781231000000000\",\"currency\":{\"currAcronym\":\"EUR\",\"currencyCodeId\":3,\"shortDesc\":\"Euro\",\"id\":22,\"longDesc\":\"Euro\"},\"customerBehaviourType\":{\"customerBehaviourTypeId\":1,\"nemotecnico\":\"DEFAULT\",\"shortDesc\":\"default\",\"id\":1,\"longDesc\":\"Default\"},\"id\":535231,\"stampUser\":{\"name\":\"NATALIA ORTIZ\",\"id\":516452,\"userId\":\"NORTIZ\"},\"region\":{\"regionQuotationId\":1,\"nemotecnico\":\"PAIS\",\"shortDesc\":\"THE WHOLE COUNTRY\",\"id\":1,\"longDesc\":\"TODAS LAS REGIONES DEL PAIS\"},\"quotationType\":{\"nemotecnico\":\"BOLETOS_INTERNOS\",\"shortDesc\":\"Internal tickets\",\"id\":1,\"quotationTypeId\":1,\"longDesc\":\"Boletos Internos\"}},{\"quotSale\":\"10.00000\",\"quotArbitrage\":\"1.00000\",\"quotBuy\":\"10.00000\",\"systemUser\":null,\"parityUsd\":\"1.00000\",\"dateFrom\":\"19691231210000000\",\"stampAdditional\":\"5\",\"institution\":{\"institutionId\":1111,\"shortDesc\":\"Technisys - Baseline\",\"id\":2},\"stampDateTime\":\"20191226142953818\",\"expressionUnit\":1,\"dateTo\":\"20781231000000000\",\"currency\":{\"currAcronym\":\"PTS\",\"currencyCodeId\":8,\"shortDesc\":\"points\",\"id\":402601,\"longDesc\":\"PUNTITOS\"},\"customerBehaviourType\":{\"customerBehaviourTypeId\":1,\"nemotecnico\":\"DEFAULT\",\"shortDesc\":\"default\",\"id\":1,\"longDesc\":\"Default\"},\"id\":583803,\"stampUser\":{\"name\":\"RICARDO NODA\",\"id\":301151,\"userId\":\"RNODA\"},\"quotationType\":{\"nemotecnico\":\"BOLETOS_INTERNOS\",\"shortDesc\":\"Internal tickets\",\"id\":1,\"quotationTypeId\":1,\"longDesc\":\"Boletos Internos\"}}],\"collectionEntityId\":\"Quotation\",\"collectionEntityVersion\":\"1.0\",\"collectionEntityDataModel\":\"product.financials\"}}"));
        Mockito.when(CacheUtils.get("cyberbank.core.quotationType")).thenReturn(new QuotationType(1,1, "Internal tickets"));

        CyberbankCoreConnectorResponse<List<CurrencyExchange>> response = CyberbankExchangeRateConnector.listExchangeRates("idUser", 1);
        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
        Assert.assertTrue(response.getData().size() > 0);
    }

    @Test
    public void testListExchangeCurrenciesEmpty() throws IOException, CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"massiveSelectQuotationBy_Date\",\"transactionId\":\"massiveSelectQuotationBy_Date\",\"transactionVersion\":\"1.0\",\"out.quotation_list\":{\"collectionEntityId\":\"Quotation\",\"collectionEntityVersion\":\"1.0\",\"collectionEntityDataModel\":\"product.financials\"}}"));
        Mockito.when(CacheUtils.get("cyberbank.core.quotationType")).thenReturn(new QuotationType(1,1, "Internal tickets"));

        CyberbankCoreConnectorResponse<List<CurrencyExchange>> response = CyberbankExchangeRateConnector.listExchangeRates("idUser", 1);

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
        Assert.assertTrue(response.getData().size() == 0);
    }

    @Test
    public void testListExchangeCurrenciesEmpty1() throws IOException, CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"massiveSelectQuotationBy_Date\",\"transactionId\":\"massiveSelectQuotationBy_Date\",\"transactionVersion\":\"1.0\"}"));
        Mockito.when(CacheUtils.get("cyberbank.core.quotationType")).thenReturn(new QuotationType(1,1, "Internal tickets"));

        CyberbankCoreConnectorResponse<List<CurrencyExchange>> response = CyberbankExchangeRateConnector.listExchangeRates("idUser", 1);

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
        Assert.assertTrue(response.getData().size() == 0);
    }

    @Test
    public void testListExchangeCurrenciesResponseWithError() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"error\",\"transactionId\":\"massiveSelectQuotationBy_Date\",\"transactionVersion\":\"1.0\",\"out.error_list\":{\"collection\":[{\"codigo\":-1,\"description\":\"Error not found: 1\",\"id\":1,\"codigoerror\":null}],\"collectionEntityId\":\"Error\",\"collectionEntityVersion\":\"1.0\",\"collectionEntityDataModel\":\"null.null\"}}"));
        Mockito.when(CyberbankCoreConnector.callHasError(Mockito.any())).thenReturn(true);
        Mockito.when(CyberbankCoreConnector.processErrors(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(new CyberbankCoreConnectorResponse(-1));
        Mockito.when(CacheUtils.get("cyberbank.core.quotationType")).thenReturn(new QuotationType(1,1, "Internal tickets"));

        CyberbankCoreConnectorResponse<List<CurrencyExchange>> response = CyberbankExchangeRateConnector.listExchangeRates("idUser", 1);

        Assert.assertNotNull(response);
        Assert.assertNotSame(0, response.getCode());
        Assert.assertNull(response.getData());
    }

    @Test(expected = CyberbankCoreConnectorException.class)
    public void testListExchangeCurrenciesException() throws CyberbankCoreConnectorException, IOException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenThrow(CyberbankCoreConnectorException.class);
        Mockito.when(CacheUtils.get("cyberbank.core.quotationType")).thenReturn(new QuotationType(1,1, "Internal tickets"));

        CyberbankCoreConnectorResponse<List<CurrencyExchange>> response = CyberbankExchangeRateConnector.listExchangeRates("idUser", 1);
        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
    }

    @Test
    public void testQuotationTypeOk() throws IOException, CyberbankCoreConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(new JSONObject("{\"project\":\"Tech\",\"responseCode\":\"processCustomerTo_Obtain_Quotation_Type\",\"transactionId\":\"processCustomerTo_Obtain_Quotation_Type\",\"transactionVersion\":\"1.0\",\"out.quotation_type\":{\"shortDesc\":\"Internal tickets\",\"id\":1,\"quotationTypeId\":1}}"));
        Mockito.when(CacheUtils.get("cyberbank.core.quotationType")).thenReturn(null);

        CyberbankCoreConnectorResponse<QuotationType> response = CyberbankExchangeRateConnector.getQuotationType("CLIENT");
        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
    }



}
