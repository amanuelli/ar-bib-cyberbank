package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.OccupationJob;
import com.technisys.omnichannel.client.domain.RoleJob;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.utils.CacheUtils;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;

@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({
        ConfigurationFactory.class,
        SecurityUtils.class,
        CyberbankCoreConnector.class,
        CyberbankCoreRequest.class,
        CacheUtils.class
})
public class CyberbankJobInformationConnectorTest {
    @Mock
    private Configuration configurationMock;

    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM),  Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM),  Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), eq("connector.cyberbank.core.template.request"), eq("/templates/connectors/cyberbank/request/"))).thenReturn("/templates/connectors/cyberbank/request/");


        PowerMockito.mockStatic(CyberbankCoreRequest.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CacheUtils.class);

    }

    @Test
    public void testListJobInformationOk() throws IOException, CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListRolJobOK());
        Mockito.when(CacheUtils.get("cyberbank.core.roles.job")).thenReturn(null);
        CyberbankCoreConnectorResponse<List<RoleJob>> response = CyberbankJobInformationConnector.listRolesJob();

        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());

    }

    @Test(expected = NullPointerException.class)
    public void testListJobInformationNoOk() throws BackendConnectorException {
        Mockito.when(CyberbankJobInformationConnector.listRolesJob()).thenThrow(NullPointerException.class);
    }

    @Test
    public void testListOccupationJobOK() throws CyberbankCoreConnectorException, IOException, BackendConnectorException {
        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getResponseListOccupationJobOK());
        Mockito.when(CacheUtils.get("cyberbank.core.occupations.job")).thenReturn(null);
        CyberbankCoreConnectorResponse<List<OccupationJob>> response = CyberbankJobInformationConnector.listOccupationJob();
        Assert.assertNotNull(response);
        Assert.assertEquals(0, response.getCode());
        Assert.assertNotNull(response.getData());
    }

    @Test(expected = NullPointerException.class)
    public void testListOccupationJobNoOk() throws BackendConnectorException {
        Mockito.when(CyberbankJobInformationConnector.listOccupationJob()).thenThrow(NullPointerException.class);
    }

    public JSONObject getResponseListOccupationJobOK(){
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectGeneral_TableOccupation_Code\",\n" +
                "    \"transactionId\": \"massiveSelectGeneral_TableOccupation_Code\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.occupation_code_list\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"nemotecnico\": \"\\t3\\t\",\n" +
                "                \"shortDesc\": \"\\tDirectivos de organismos , empresas e instituc\\t\",\n" +
                "                \"id\": 500004,\n" +
                "                \"occupationCodeId\": 10004,\n" +
                "                \"longDesc\": \"\\tDirectivos de organismos , empresas e instituciones estatales\\t\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"\\t2\\t\",\n" +
                "                \"shortDesc\": \"\\tFuncionarios del PJF, nacional, provincial, mu\\t\",\n" +
                "                \"id\": 500003,\n" +
                "                \"occupationCodeId\": 10003,\n" +
                "                \"longDesc\": \"\\tFuncionarios del PJF, nacional, provincial, municipal y/o departamental\\t\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"1\",\n" +
                "                \"shortDesc\": \"\\tFuncionarios del PLN, provincial, municipal y/\\t\",\n" +
                "                \"id\": 500002,\n" +
                "                \"occupationCodeId\": 10002,\n" +
                "                \"longDesc\": \"\\tFuncionarios del PLN, provincial, municipal y/o departamental\\t\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"7\",\n" +
                "                \"shortDesc\": \"Managers of large private enterprises produc\",\n" +
                "                \"id\": 500008,\n" +
                "                \"occupationCodeId\": 10008,\n" +
                "                \"longDesc\": \"Directivos de grandes empresas privadas productoras de bienes y servicios\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"4\",\n" +
                "                \"shortDesc\": \"Managers of social institutions\",\n" +
                "                \"id\": 500005,\n" +
                "                \"occupationCodeId\": 10005,\n" +
                "                \"longDesc\": \"Directivos de instituciones sociales\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"6\",\n" +
                "                \"shortDesc\": \"Managers of medium private enterprises produ\",\n" +
                "                \"id\": 500007,\n" +
                "                \"occupationCodeId\": 10007,\n" +
                "                \"longDesc\": \"Directivos de medianas empresas privadas productoras de bienes y servicios\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"5\",\n" +
                "                \"shortDesc\": \"Managers of small and micro\",\n" +
                "                \"id\": 500006,\n" +
                "                \"occupationCodeId\": 10006,\n" +
                "                \"longDesc\": \"Directivos de pequeñas y microempresas\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"0\",\n" +
                "                \"shortDesc\": \"Funcionarios del PEN, provincial, municipal y\",\n" +
                "                \"id\": 500001,\n" +
                "                \"occupationCodeId\": 10001,\n" +
                "                \"longDesc\": \"Funcionarios del PEN, provincial, municipal y/o departamental\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"63\",\n" +
                "                \"shortDesc\": \"Ocup. Prod. apícola, avícolas y otras\",\n" +
                "                \"id\": 500041,\n" +
                "                \"occupationCodeId\": 10041,\n" +
                "                \"longDesc\": \"Ocup. Prod. apícola, avícolas y otras\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"64\",\n" +
                "                \"shortDesc\": \"Fish production occupations\",\n" +
                "                \"id\": 500042,\n" +
                "                \"occupationCodeId\": 10042,\n" +
                "                \"longDesc\": \"Ocupaciones  de la producción pesquera\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"32\",\n" +
                "                \"shortDesc\": \"Indirect marketing occupations\",\n" +
                "                \"id\": 500014,\n" +
                "                \"occupationCodeId\": 10014,\n" +
                "                \"longDesc\": \"Ocupaciones de comercialización indirecta\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"31\",\n" +
                "                \"shortDesc\": \"Ocupaciones de corretaje comercial, venta domi\",\n" +
                "                \"id\": 500013,\n" +
                "                \"occupationCodeId\": 10013,\n" +
                "                \"longDesc\": \"Ocupaciones de corretaje comercial, venta domiciliaria, viajantes y promotores\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"43\",\n" +
                "                \"shortDesc\": \"Occupations advisory and consulting\",\n" +
                "                \"id\": 500022,\n" +
                "                \"occupationCodeId\": 10022,\n" +
                "                \"longDesc\": \"Ocupaciones de la asesoría y de la consultoría\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"65\",\n" +
                "                \"shortDesc\": \"Occupations of hunting\",\n" +
                "                \"id\": 500043,\n" +
                "                \"occupationCodeId\": 10043,\n" +
                "                \"longDesc\": \"Ocupaciones de la caza\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"33\",\n" +
                "                \"shortDesc\": \"Itinerant occupations and marketing\",\n" +
                "                \"id\": 500015,\n" +
                "                \"occupationCodeId\": 10015,\n" +
                "                \"longDesc\": \"Ocupaciones de la comercialización ambulante y callejera\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"30\",\n" +
                "                \"shortDesc\": \"Direct Marketing occupations\",\n" +
                "                \"id\": 500012,\n" +
                "                \"occupationCodeId\": 10012,\n" +
                "                \"longDesc\": \"Ocupaciones de la comercialización directa\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"45\",\n" +
                "                \"shortDesc\": \"Occupations of mass communication\",\n" +
                "                \"id\": 500024,\n" +
                "                \"occupationCodeId\": 10024,\n" +
                "                \"longDesc\": \"Ocupaciones de la comunicación masiva\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"72\",\n" +
                "                \"shortDesc\": \"Ocupaciones de la construcción edilicia, de ob\",\n" +
                "                \"id\": 500046,\n" +
                "                \"occupationCodeId\": 10046,\n" +
                "                \"longDesc\": \"Ocupaciones de la construcción edilicia, de obras de infraestructura y de redes de distribució\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"41\",\n" +
                "                \"shortDesc\": \"Education occupations\",\n" +
                "                \"id\": 500020,\n" +
                "                \"occupationCodeId\": 10020,\n" +
                "                \"longDesc\": \"Ocupaciones de la educación\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"10\",\n" +
                "                \"shortDesc\": \"Ocupaciones de la gestión administrativa, plan\",\n" +
                "                \"id\": 500009,\n" +
                "                \"occupationCodeId\": 10009,\n" +
                "                \"longDesc\": \"Ocupaciones de la gestión administrativa, planificación y control de gestión\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"11\",\n" +
                "                \"shortDesc\": \"Occupations of legal and legal management\",\n" +
                "                \"id\": 500010,\n" +
                "                \"occupationCodeId\": 10010,\n" +
                "                \"longDesc\": \"Ocupaciones de la gestión jurídico-legal\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"20\",\n" +
                "                \"shortDesc\": \"Ocupaciones de la gestión presupuestaria, cont\",\n" +
                "                \"id\": 500011,\n" +
                "                \"occupationCodeId\": 10011,\n" +
                "                \"longDesc\": \"Ocupaciones de la gestión presupuestaria, contable y financiera\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"92\",\n" +
                "                \"shortDesc\": \"Occupations installation and maintenance\",\n" +
                "                \"id\": 500052,\n" +
                "                \"occupationCodeId\": 10052,\n" +
                "                \"longDesc\": \"Ocupaciones de la instalación y mantenimiento de maquinaria, equipos y sistemas de la prestaci\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"90\",\n" +
                "                \"shortDesc\": \"Occupations installation and maintenance\",\n" +
                "                \"id\": 500050,\n" +
                "                \"occupationCodeId\": 10050,\n" +
                "                \"longDesc\": \"Ocupaciones de la instalación y mantenimiento de maquinaria, equipos y sistemas de la producci\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"42\",\n" +
                "                \"shortDesc\": \"Research occupations\",\n" +
                "                \"id\": 500021,\n" +
                "                \"occupationCodeId\": 10021,\n" +
                "                \"longDesc\": \"Ocupaciones de la investigación\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"44\",\n" +
                "                \"shortDesc\": \"Occupations loss prevention (na\",\n" +
                "                \"id\": 500023,\n" +
                "                \"occupationCodeId\": 10023,\n" +
                "                \"longDesc\": \"Ocupaciones de la prevención de siniestros (naturales, humanos, productivos) y atención del me\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"60\",\n" +
                "                \"shortDesc\": \"Occupations of agricultural production\",\n" +
                "                \"id\": 500038,\n" +
                "                \"occupationCodeId\": 10038,\n" +
                "                \"longDesc\": \"Ocupaciones de la producción agrícola\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"71\",\n" +
                "                \"shortDesc\": \"Ocupaciones de la producción de energía, agua \",\n" +
                "                \"id\": 500045,\n" +
                "                \"occupationCodeId\": 10045,\n" +
                "                \"longDesc\": \"Ocupaciones de la producción de energía, agua y gas\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"81\",\n" +
                "                \"shortDesc\": \"Occupations software production\",\n" +
                "                \"id\": 500048,\n" +
                "                \"occupationCodeId\": 10048,\n" +
                "                \"longDesc\": \"Ocupaciones de la producción de software\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"70\",\n" +
                "                \"shortDesc\": \"Extractive production occupations\",\n" +
                "                \"id\": 500044,\n" +
                "                \"occupationCodeId\": 10044,\n" +
                "                \"longDesc\": \"Ocupaciones de la producción extractiva\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"62\",\n" +
                "                \"shortDesc\": \"Occupations of forestry production\",\n" +
                "                \"id\": 500040,\n" +
                "                \"occupationCodeId\": 10040,\n" +
                "                \"longDesc\": \"Ocupaciones de la producción forestal\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"61\",\n" +
                "                \"shortDesc\": \"Occupations of livestock production\",\n" +
                "                \"id\": 500039,\n" +
                "                \"occupationCodeId\": 10039,\n" +
                "                \"longDesc\": \"Ocupaciones de la producción ganadera\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"80\",\n" +
                "                \"shortDesc\": \"Occupations of industrial production and art\",\n" +
                "                \"id\": 500047,\n" +
                "                \"occupationCodeId\": 10047,\n" +
                "                \"longDesc\": \"Ocupaciones de la producción industrial y artesanal\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"82\",\n" +
                "                \"shortDesc\": \"Repair occupations goods cons\",\n" +
                "                \"id\": 500049,\n" +
                "                \"occupationCodeId\": 10049,\n" +
                "                \"longDesc\": \"Ocupaciones de la reparación de bienes de consumo\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"40\",\n" +
                "                \"shortDesc\": \"Health occupations and health\",\n" +
                "                \"id\": 500019,\n" +
                "                \"occupationCodeId\": 10019,\n" +
                "                \"longDesc\": \"Ocupaciones de la salud y sanidad\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"49\",\n" +
                "                \"shortDesc\": \"Occupations of the Armed Forces Gendarmerie and Prefec\",\n" +
                "                \"id\": 500028,\n" +
                "                \"occupationCodeId\": 10028,\n" +
                "                \"longDesc\": \"Ocupaciones de las FF.AA. Gendarmería y Prefectura\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"35\",\n" +
                "                \"shortDesc\": \"Telecommunications occupations\",\n" +
                "                \"id\": 500017,\n" +
                "                \"occupationCodeId\": 10017,\n" +
                "                \"longDesc\": \"Ocupaciones de las telecomunicaciones\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"54\",\n" +
                "                \"shortDesc\": \"Occupations and accommodation services\",\n" +
                "                \"id\": 500033,\n" +
                "                \"occupationCodeId\": 10033,\n" +
                "                \"longDesc\": \"Ocupaciones de los servicios de alojamiento y turismo\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"47\",\n" +
                "                \"shortDesc\": \"Service occupations surveillance and s\",\n" +
                "                \"id\": 500026,\n" +
                "                \"occupationCodeId\": 10026,\n" +
                "                \"longDesc\": \"Ocupaciones de los servicios de vigilancia y seguridad civil\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"55\",\n" +
                "                \"shortDesc\": \"Domestic service occupations\",\n" +
                "                \"id\": 500034,\n" +
                "                \"occupationCodeId\": 10034,\n" +
                "                \"longDesc\": \"Ocupaciones de los servicios domésticos\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"48\",\n" +
                "                \"shortDesc\": \"Occupations of police services\",\n" +
                "                \"id\": 500027,\n" +
                "                \"occupationCodeId\": 10027,\n" +
                "                \"longDesc\": \"Ocupaciones de los servicios policiales\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"46\",\n" +
                "                \"shortDesc\": \"Ocupaciones de los servicios sociales, comunal\",\n" +
                "                \"id\": 500025,\n" +
                "                \"occupationCodeId\": 10025,\n" +
                "                \"longDesc\": \"Ocupaciones de los servicios sociales, comunales, políticos, gremiales y religiosos\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"58\",\n" +
                "                \"shortDesc\": \"Occupations of various social services\",\n" +
                "                \"id\": 500037,\n" +
                "                \"occupationCodeId\": 10037,\n" +
                "                \"longDesc\": \"Ocupaciones de los servicios sociales varios\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"56\",\n" +
                "                \"shortDesc\": \"Cleaning service occupations not household usage\",\n" +
                "                \"id\": 500035,\n" +
                "                \"occupationCodeId\": 10035,\n" +
                "                \"longDesc\": \"Ocupaciones de servicios de limpieza no domésticos\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"52\",\n" +
                "                \"shortDesc\": \"Recreation service occupations\",\n" +
                "                \"id\": 500031,\n" +
                "                \"occupationCodeId\": 10031,\n" +
                "                \"longDesc\": \"Ocupaciones de servicios de recreación\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"53\",\n" +
                "                \"shortDesc\": \"Food service occupations\",\n" +
                "                \"id\": 500032,\n" +
                "                \"occupationCodeId\": 10032,\n" +
                "                \"longDesc\": \"Ocupaciones de servicios gastronómicos\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"36\",\n" +
                "                \"shortDesc\": \"Ocupaciones del almacenaje de insumos, materia\",\n" +
                "                \"id\": 500018,\n" +
                "                \"occupationCodeId\": 10018,\n" +
                "                \"longDesc\": \"Ocupaciones del almacenaje de insumos, materias primas, mercaderías e instrumentos\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"50\",\n" +
                "                \"shortDesc\": \"Art occupations\",\n" +
                "                \"id\": 500029,\n" +
                "                \"occupationCodeId\": 10029,\n" +
                "                \"longDesc\": \"Ocupaciones del arte\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"57\",\n" +
                "                \"shortDesc\": \"Occupations care and attention perso\",\n" +
                "                \"id\": 500036,\n" +
                "                \"occupationCodeId\": 10036,\n" +
                "                \"longDesc\": \"Ocupaciones del cuidado y la atención de personas\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"51\",\n" +
                "                \"shortDesc\": \"Sports occupations\",\n" +
                "                \"id\": 500030,\n" +
                "                \"occupationCodeId\": 10030,\n" +
                "                \"longDesc\": \"Ocupaciones del deporte\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"91\",\n" +
                "                \"shortDesc\": \"Occupations of technological development product\",\n" +
                "                \"id\": 500051,\n" +
                "                \"occupationCodeId\": 10051,\n" +
                "                \"longDesc\": \"Ocupaciones del desarrollo tecnológico productivo\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"34\",\n" +
                "                \"shortDesc\": \"Transportation occupations\",\n" +
                "                \"id\": 500016,\n" +
                "                \"occupationCodeId\": 10016,\n" +
                "                \"longDesc\": \"Ocupaciones del transporte\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"TO_DELETE\",\n" +
                "                \"shortDesc\": \"TO_DELETE\",\n" +
                "                \"id\": 392830,\n" +
                "                \"occupationCodeId\": 10053,\n" +
                "                \"longDesc\": \"TO_DELETE\"\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": \"TO_DELETE2\",\n" +
                "                \"shortDesc\": \"TO_DELETE2\",\n" +
                "                \"id\": 392831,\n" +
                "                \"occupationCodeId\": 10054,\n" +
                "                \"longDesc\": \"TO_DELETE2\"\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"OccupationCode\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}");
    }

    public JSONObject getResponseListRolJobOK(){
        return new JSONObject("{\n" +
                "    \"project\": \"Tech\",\n" +
                "    \"responseCode\": \"massiveSelectGeneral_TableProfession\",\n" +
                "    \"transactionId\": \"massiveSelectGeneral_TableProfession\",\n" +
                "    \"transactionVersion\": \"1.0\",\n" +
                "    \"out.result\": {\n" +
                "        \"collection\": [\n" +
                "            {\n" +
                "                \"nemotecnico\": null,\n" +
                "                \"shortDesc\": \"Agrimensura\",\n" +
                "                \"id\": 500001,\n" +
                "                \"professionId\": 10001\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": null,\n" +
                "                \"shortDesc\": \"Agronomy\",\n" +
                "                \"id\": 500002,\n" +
                "                \"professionId\": 10002\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": null,\n" +
                "                \"shortDesc\": \"Biochemistry\",\n" +
                "                \"id\": 500006,\n" +
                "                \"professionId\": 10006\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": null,\n" +
                "                \"shortDesc\": \"Biological Sciences\",\n" +
                "                \"id\": 500008,\n" +
                "                \"professionId\": 10008\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": null,\n" +
                "                \"shortDesc\": \"Doctorate in Business Administration\",\n" +
                "                \"id\": 500015,\n" +
                "                \"professionId\": 10015\n" +
                "            },\n" +
                "            {\n" +
                "                \"nemotecnico\": null,\n" +
                "                \"shortDesc\": \"PhD in Agrimensura\",\n" +
                "                \"id\": 500017,\n" +
                "                \"professionId\": 10017\n" +
                "            }\n" +
                "        ],\n" +
                "        \"collectionEntityId\": \"Profession\",\n" +
                "        \"collectionEntityVersion\": \"1.0\",\n" +
                "        \"collectionEntityDataModel\": \"product.financials\"\n" +
                "    }\n" +
                "}");
    }

}
