/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankAccountConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.StatementResult;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, CyberbankCoreConnector.class, CyberbankAccountConnector.class})

public class CyberbankAccountConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CyberbankAccountConnector.class);
    }

    @Test
    public void testAddAccountTypeCyberbankOk() throws CyberbankCoreConnectorException, BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<String> response = new CyberbankCoreConnectorResponse<>();
        response.setData("12");
        Mockito.when(CyberbankAccountConnector.create(Mockito.anyString())).thenReturn(response);
        int accountId = CoreAccountConnectorOrchestrator.addAccount("212", "3131");
        Assert.assertEquals(12, accountId);
    }



    @Test
    public void testReadAccountOK() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<Account> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new Account());

        Product product = new Product();
        product.setExtraInfo("CA|3334|USD|01000|10|10001|1|1");
        Mockito.when(CyberbankAccountConnector.read(product)).thenReturn(response);
        Account account = CoreAccountConnectorOrchestrator.readAccount("323", product);
        Assert.assertNotNull(account);
    }

    @Test
    public void testStatementsOK() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<StatementResult> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new StatementResult(0, new ArrayList<Statement>()));
        Date startDate = new Date();
        Date endDate = new Date();
        Mockito.when(CyberbankAccountConnector.statements(
                "323",
               "232",
                startDate,
                endDate,
                10,
                1
        )).thenReturn(response);

       StatementResult result = CoreAccountConnectorOrchestrator.statements("21",
               "323",
                "232",
                startDate,
                endDate,
                0.2,
               0.5,
               "ref",
                new String[0],
               0,
                10,
                1);
        Assert.assertNotNull(result);
    }

    @Test
    public void testCountMovements() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<StatementResult> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new StatementResult(0, new ArrayList<>()));
        int countMovements =  CoreAccountConnectorOrchestrator.countMovements("21", response.getData());
        Assert.assertEquals(0, countMovements);
    }

    @Test
    public void testReadStatement() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<Statement> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new Statement());
        Mockito.when(CyberbankAccountConnector.readStatement(0, "31")).thenReturn(response);
        Statement statement = CoreAccountConnectorOrchestrator.readStatement(0, "31");
        Assert.assertNotNull(statement);
    }

    @Test
    public void testListAccount() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CyberbankCoreConnectorResponse<List<Account>> response = new CyberbankCoreConnectorResponse<>();
        response.setData(new ArrayList<>());

        Mockito.when(CyberbankAccountConnector.list("212", new ArrayList<>())).thenReturn(response);
        List<Account> accountList = CoreAccountConnectorOrchestrator.list("212", new ArrayList<>(), new ArrayList<>());
        Assert.assertNotNull(accountList);
    }

    @Test
    public void testReadNoteBoucher() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Statement response = new Statement();

        Mockito.when(CyberbankAccountConnector.readNoteBoucher(212)).thenReturn(response);
        Statement statement = CoreAccountConnectorOrchestrator.readNoteBoucher(212);
        Assert.assertNotNull(statement);
    }

    @Test(expected = BackendConnectorException.class)
    public void addAccountException() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankAccountConnector.create(Mockito.anyString())).thenThrow(new CyberbankCoreConnectorException("Error"));
        CoreAccountConnectorOrchestrator.addAccount("212", "3131");
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadAccountException() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Product product = new Product();
        product.setExtraInfo("CA|3334|USD|01000|10|10001|1|1");
        Mockito.when(CyberbankAccountConnector.read(product)).thenThrow(new CyberbankCoreConnectorException("Error"));
        CoreAccountConnectorOrchestrator.readAccount("323", product);
    }

    @Test(expected = BackendConnectorException.class)
    public void testStatementsException() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");

        Date startDate = new Date();
        Date endDate = new Date();
        Mockito.when(CyberbankAccountConnector.statements(
                "323",
                "232",
                startDate,
                endDate,
                10,
                1
        )).thenThrow(new CyberbankCoreConnectorException("Error"));

        StatementResult result = CoreAccountConnectorOrchestrator.statements("21",
                "323",
                "232",
                startDate,
                endDate,
                0.2,
                0.5,
                "ref",
                new String[0],
                0,
                10,
                1);
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadStatementException() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankAccountConnector.readStatement(0, "31")).thenThrow(new CyberbankCoreConnectorException("Error"));
        Statement statement = CoreAccountConnectorOrchestrator.readStatement(0, "31");
    }

    @Test(expected = BackendConnectorException.class)
    public void testListAccountException() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankAccountConnector.list("212", new ArrayList<>())).thenThrow(new CyberbankCoreConnectorException("Error"));
        CoreAccountConnectorOrchestrator.list("212", new ArrayList<>(), new ArrayList<>());
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadNoteBoucherException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(CyberbankAccountConnector.readNoteBoucher(212)).thenThrow(new IOException("Error"));
        CoreAccountConnectorOrchestrator.readNoteBoucher(212);
    }








}