package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.utils.CacheUtils;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import static org.mockito.ArgumentMatchers.eq;


@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({ConfigurationFactory.class, CyberbankCoreConnector.class, CyberbankCoreRequest.class, CacheUtils.class})
public class CyberbankCustomerConnectorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);
        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), eq("connector.cyberbank.core.template.request"), eq("/templates/connectors/cyberbank/request/"))).thenReturn("/templates/connectors/cyberbank/request/");

        PowerMockito.mockStatic(CyberbankCoreRequest.class);
        PowerMockito.mockStatic(CyberbankCoreConnector.class);
        PowerMockito.mockStatic(CyberbankCustomerConnector.class);
        PowerMockito.mockStatic(CacheUtils.class);

    }

    @Test
    public void testReadCustomerOK() throws BackendConnectorException, CyberbankCoreConnectorException {

        Mockito.when(CyberbankCoreConnector.call(Mockito.anyString(), Mockito.anyString())).thenReturn(getReponseReadCustomerOK());
        CyberbankCoreConnectorResponse<ClientUser> cyberbankCoreConnectorResponse = CyberbankCustomerConnector.read("DNI","111111");
        Assert.assertTrue(cyberbankCoreConnectorResponse != null);
        Assert.assertTrue(cyberbankCoreConnectorResponse.getData() != null);
    }


    @Test(expected = NullPointerException.class)
    public void testReadCustomerNOOK() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CyberbankCustomerConnector.read("DNI","111111")).thenThrow(NullPointerException.class);

    }

    public JSONObject getReponseReadCustomerOK(){
        return new JSONObject("{\n" +
                "   \"project\": \"Tech\",\n" +
                "   \"transactionId\": \"singleSelectCustomerBy_Identification_Complete\",\n" +
                "   \"transactionVersion\": \"value\",\n" +
                "   \"out.relation_customer_customer_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"id\": \"value\",\n" +
                "            \"customerLink\": {\n" +
                "               \"name\": \"value\",\n" +
                "               \"customerId\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            },\n" +
                "            \"customer\": {\n" +
                "               \"name\": \"value\",\n" +
                "               \"customerId\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            }\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"RelationCustomerCustomer\"\n" +
                "   },\n" +
                "   \"out.employment_details\": {\n" +
                "      \"profession\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"employmenteStartDate\": \"value\",\n" +
                "      \"address\": {\n" +
                "         \"exactAddress\": \"value\"\n" +
                "      },\n" +
                "      \"employmentSituation\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"occupationCode\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\"\n" +
                "      },\n" +
                "      \"employerName\": \"value\",\n" +
                "      \"fixedIncome\": \"value\",\n" +
                "      \"employerIdentification\": \"value\",\n" +
                "      \"id\": \"value\",\n" +
                "      \"contactPhone\": {\n" +
                "         \"phoneNumber\": \"111111\"\n" +
                "      }\n" +
                "   },\n" +
                "   \"out.electronics_contact_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"emailType\": {\n" +
                "               \"nemotecnico\": \"value\"\n" +
                "            },\n" +
                "            \"emailAddressComplete\": \"value\",\n" +
                "            \"id\": \"value\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"ElectronicsContact\"\n" +
                "   },\n" +
                "   \"out.address_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"localityZipCode\": {\n" +
                "               \"shortDesc\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            },\n" +
                "            \"comments\": \"value\",\n" +
                "            \"streetNumber\": \"1\",\n" +
                "            \"addressType\": {\n" +
                "               \"nemotecnico\": \"value\",\n" +
                "               \"shortDesc\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            },\n" +
                "            \"provinceCode\": {\n" +
                "               \"nemotecnico\": \"value\",\n" +
                "               \"shortDesc\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            },\n" +
                "            \"street\": \"value\",\n" +
                "            \"countryZipCode\": \"value\",\n" +
                "            \"townZone\": \"value\",\n" +
                "            \"id\": \"value\",\n" +
                "            \"floor\": \"value\",\n" +
                "            \"apartment\": \"value\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"Address\"\n" +
                "   },\n" +
                "   \"out.contact_phone_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"phoneNumber\": \"1111111\",\n" +
                "            \"telephoneType\": {\n" +
                "               \"nemotecnico\": \"value\"\n" +
                "            },\n" +
                "            \"id\": \"value\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"ContactPhone\"\n" +
                "   },\n" +
                "   \"out.customer_identification_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"identificationNumber\": \"value\",\n" +
                "            \"identificationType\": {\n" +
                "               \"nemotecnico\": \"DNI\",\n" +
                "               \"shortDesc\": \"DNI\"\n" +
                "            },\n" +
                "            \"id\": \"1\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"CustomerIdentification\"\n" +
                "   },\n" +
                "   \"out.basic_individual_data\": {\n" +
                "      \"sexCode\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"numberOfChildren\": \"value\",\n" +
                "      \"nationality\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"countryOfBirth\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"id\": \"value\",\n" +
                "      \"birthDate\": \"value\",\n" +
                "      \"maritalStatus\": {\n" +
                "         \"nemotecnico\": \"SOLTERO\",\n" +
                "         \"shortDesc\": \"SOLTERO\",\n" +
                "         \"id\": \"value\"\n" +
                "      }\n" +
                "   },\n" +
                "   \"out.customer\": {\n" +
                "      \"isEmployee\": \"value\",\n" +
                "      \"firstName1\": \"value\",\n" +
                "      \"firstName2\": \"value\",\n" +
                "      \"channel\": {\n" +
                "         \"nemotecnico\": \"value\"\n" +
                "      },\n" +
                "      \"description\": \"value\",\n" +
                "      \"identificationType\": {\n" +
                "         \"shortDesc\": \"DNI\",\n" +
                "         \"id\": \"1\"\n" +
                "      },\n" +
                "      \"originatingBranch\": {\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"customerType\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\"\n" +
                "      },\n" +
                "      \"dependentsM\": \"value\",\n" +
                "      \"segment\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\"\n" +
                "      },\n" +
                "      \"lastName1\": \"value\",\n" +
                "      \"name\": \"value\",\n" +
                "      \"customerId\": \"value\",\n" +
                "      \"lastName2\": \"value\",\n" +
                "      \"identificationNumber\": \"value\",\n" +
                "      \"dependentsF\": \"value\",\n" +
                "      \"pep\": \"value\",\n" +
                "      \"id\": 123, \n" +
                "      \"personType\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\"\n" +
                "      }\n" +
                "   }\n" +
                "}");
    }


    public JSONObject getReponseReadCustomerNOOK(){
        return new JSONObject("{\n" +
                "   \"project\": \"Tech\",\n" +
                "   \"transactionId\": \"singleSelectCustomerBy_Identification_Complete\",\n" +
                "   \"transactionVersion\": \"value\",\n" +
                "   \"out.relation_customer_customer_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"id\": \"value\",\n" +
                "            \"customerLink\": {\n" +
                "               \"name\": \"value\",\n" +
                "               \"customerId\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            },\n" +
                "            \"customer\": {\n" +
                "               \"name\": \"value\",\n" +
                "               \"customerId\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            }\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"RelationCustomerCustomer\"\n" +
                "   },\n" +
                "   \"out.employment_details\": {\n" +
                "      \"profession\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"employmenteStartDate\": \"value\",\n" +
                "      \"address\": {\n" +
                "         \"exactAddress\": \"value\"\n" +
                "      },\n" +
                "      \"employmentSituation\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"occupationCode\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\"\n" +
                "      },\n" +
                "      \"employerName\": \"value\",\n" +
                "      \"fixedIncome\": \"value\",\n" +
                "      \"employerIdentification\": \"value\",\n" +
                "      \"id\": \"value\",\n" +
                "      \"contactPhone\": {\n" +
                "         \"phoneNumber\": \"111111\"\n" +
                "      }\n" +
                "   },\n" +
                "   \"out.electronics_contact_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"emailType\": {\n" +
                "               \"nemotecnico\": \"value\"\n" +
                "            },\n" +
                "            \"emailAddressComplete\": \"value\",\n" +
                "            \"id\": \"value\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"ElectronicsContact\"\n" +
                "   },\n" +
                "   \"out.address_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"localityZipCode\": {\n" +
                "               \"shortDesc\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            },\n" +
                "            \"comments\": \"value\",\n" +
                "            \"streetNumber\": \"1\",\n" +
                "            \"addressType\": {\n" +
                "               \"nemotecnico\": \"value\",\n" +
                "               \"shortDesc\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            },\n" +
                "            \"provinceCode\": {\n" +
                "               \"nemotecnico\": \"value\",\n" +
                "               \"shortDesc\": \"value\",\n" +
                "               \"id\": \"value\"\n" +
                "            },\n" +
                "            \"street\": \"value\",\n" +
                "            \"countryZipCode\": \"value\",\n" +
                "            \"townZone\": \"value\",\n" +
                "            \"id\": \"value\",\n" +
                "            \"floor\": \"value\",\n" +
                "            \"apartment\": \"value\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"Address\"\n" +
                "   },\n" +
                "   \"out.contact_phone_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"phoneNumber\": \"1111111\",\n" +
                "            \"telephoneType\": {\n" +
                "               \"nemotecnico\": \"value\"\n" +
                "            },\n" +
                "            \"id\": \"value\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"ContactPhone\"\n" +
                "   },\n" +
                "   \"out.customer_identification_list\": {\n" +
                "      \"collection\": [\n" +
                "         {\n" +
                "            \"identificationNumber\": \"value\",\n" +
                "            \"identificationType\": {\n" +
                "               \"nemotecnico\": \"DNI\",\n" +
                "               \"shortDesc\": \"DNI\"\n" +
                "            },\n" +
                "            \"id\": \"1\"\n" +
                "         }\n" +
                "      ],\n" +
                "      \"collectionEntityId\": \"CustomerIdentification\"\n" +
                "   },\n" +
                "   \"out.basic_individual_data\": {\n" +
                "      \"sexCode\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"numberOfChildren\": \"value\",\n" +
                "      \"nationality\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"countryOfBirth\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"id\": \"value\",\n" +
                "      \"birthDate\": \"value\",\n" +
                "      \"maritalStatus\": {\n" +
                "         \"nemotecnico\": \"SOLTERO\",\n" +
                "         \"shortDesc\": \"SOLTERO\",\n" +
                "         \"id\": \"value\"\n" +
                "      }\n" +
                "   },\n" +
                "   \"out.customer\": {\n" +
                "      \"isEmployee\": \"value\",\n" +
                "      \"firstName1\": \"value\",\n" +
                "      \"firstName2\": \"value\",\n" +
                "      \"channel\": {\n" +
                "         \"nemotecnico\": \"value\"\n" +
                "      },\n" +
                "      \"description\": \"value\",\n" +
                "      \"identificationType\": {\n" +
                "         \"shortDesc\": \"DNI\",\n" +
                "         \"id\": \"1\"\n" +
                "      },\n" +
                "      \"originatingBranch\": {\n" +
                "         \"shortDesc\": \"value\",\n" +
                "         \"id\": \"value\"\n" +
                "      },\n" +
                "      \"customerType\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\"\n" +
                "      },\n" +
                "      \"dependentsM\": \"value\",\n" +
                "      \"segment\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\"\n" +
                "      },\n" +
                "      \"lastName1\": \"value\",\n" +
                "      \"name\": \"value\",\n" +
                "      \"customerId\": \"value\",\n" +
                "      \"lastName2\": \"value\",\n" +
                "      \"identificationNumber\": \"value\",\n" +
                "      \"dependentsF\": \"value\",\n" +
                "      \"pep\": \"value\",\n" +
                "      \"id\": \"value\",\n" +
                "      \"personType\": {\n" +
                "         \"nemotecnico\": \"value\",\n" +
                "         \"shortDesc\": \"value\"\n" +
                "      }\n" +
                "   }\n" +
                "}");
    }

}
