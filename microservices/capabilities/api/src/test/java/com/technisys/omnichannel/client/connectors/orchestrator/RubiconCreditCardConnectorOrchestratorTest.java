/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.*;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, RubiconCoreConnectorTC.class})

public class RubiconCreditCardConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(RubiconCoreConnectorTC.class);
    }

    @Test
    public void testReadCreditCardDetails() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorTC.readCreditCardDetails("21", "21", "21")).thenReturn(new CreditCard());
        CreditCard creditCard =CoreCreditCardConnectorOrchestator.readCreditCardDetails("21", "21", "21");
        Assert.assertNotNull(creditCard);
    }

    @Test
    public void testReadStatementDetails() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorTC.readStatementDetails("21", "21",0, "21")).thenReturn(new StatementCreditCard());
        StatementCreditCard statementCreditCard = CoreCreditCardConnectorOrchestator.readStatementDetails("21", "21",0, "21");
        Assert.assertNotNull(statementCreditCard);
    }

    @Test
    public void testListCreditCardStatementLines() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorTC.listCreditCardStatementLines("21")).thenReturn(new ArrayList<>());
        List<StatementLine> statementLines =  CoreCreditCardConnectorOrchestator.listCreditCardStatementLines("21");
        Assert.assertNotNull(statementLines);
    }



    @Test
    public void testReadCreditCardStatementLines() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorTC.readCreditCardStatementLine("21")).thenReturn(new StatementLine());
        StatementLine statementLine = CoreCreditCardConnectorOrchestator.readCreditCardStatementLine("21");
        Assert.assertNotNull(statementLine);
    }


    @Test
    public void testGetTotalCreditCardStatements() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorTC.getTotalCreditCardStatements("21")).thenReturn(0);
        int value = CoreCreditCardConnectorOrchestator.getTotalCreditCardStatements("21");
        Assert.assertEquals(0, value);
    }


    @Test
    public void testGetCurrentPeriodCreditCardStatements() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorTC.getCurrentPeriodCreditCardStatements("21")).thenReturn(0);
        int value = CoreCreditCardConnectorOrchestator.getCurrentPeriodCreditCardStatements("21");
        Assert.assertEquals(0, value);
    }


    @Test
    public void testListCreditCardStatements() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorTC.listCreditCardStatements("21", "2", "1", new Date(), new Date(), 0.1, 0.3, "test", "UDS", 0, 0 )).thenReturn( new ArrayList<>());
        List<StatementCreditCard> list = CoreCreditCardConnectorOrchestator.listCreditCardStatements("21", "2", "1", new Date(), new Date(), 0.1, 0.3, "test", "UDS", 0, 0 );
        Assert.assertNotNull(list);
    }


    @Test
    public void testPayCreditCardValidate() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Account account = new Account();
        account.setIdProduct("23");
        Mockito.when( RubiconCoreConnectorTC.payCreditCardValidate(
                "21",
                "2",
                account,
                "21",
                "creditcardBank",
                "USD",
                100.0,
                "test",
                0.0,
                0.1)).thenReturn(new PayCreditCardDetail());
        PayCreditCardDetail payCreditCardDetail = CoreCreditCardConnectorOrchestator.payCreditCardValidate("21", "2", account, "21", "creditcardBank", "USD", 100.0, "test", 0.0, 0.1);
        Assert.assertNotNull(payCreditCardDetail);
    }



    @Test
    public void testPayCreditCardSendException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Account account = new Account();
        account.setIdProduct("23");
        Mockito.when(RubiconCoreConnectorTC.payCreditCardValidate("21", "2", account, "21", "creditcardBank", "USD", 100.0, "test", 0.0, 0.1)).thenReturn(new PayCreditCardDetail ());
        CoreCreditCardConnectorOrchestator.payCreditCardSend("21", "2", account, "21", "creditcardBank", "USD", 100.0, "test", 0.0, 0.1);
    }

    @Test
    public void testCreateCreditCardException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorTC.createCreditCard("21", "2", "test")).thenReturn(new CreditCard());
        CoreCreditCardConnectorOrchestator.createCreditCard("21", "2", "test");
    }

}