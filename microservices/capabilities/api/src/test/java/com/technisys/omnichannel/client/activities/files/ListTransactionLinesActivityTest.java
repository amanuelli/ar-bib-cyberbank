package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.Map;

import static com.technisys.omnichannel.core.configuration.Configuration.PLATFORM;
import static org.junit.Assert.assertEquals;


@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, ConfigurationFactory.class, ListTransactionLinesActivity.class})
public class ListTransactionLinesActivityTest {

    @Mock
    private Configuration configurationMock;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);

        Mockito
                .when(ConfigurationFactory.getInstance())
                .thenReturn(configurationMock);
        Mockito.when(configurationMock.getStringSafe(PLATFORM,"massive.payments.allowed.banks.sameBank")).thenReturn("Techbank");
        Mockito.when(configurationMock.getDefaultInt(PLATFORM, "transactions.linesPerPage", 10)).thenReturn(10);
    }

    
    @Test
    public void givenFiltersWithBandesBankValue_whenParamsArebuilt_thenBandesBankParamsResult() throws ActivityException {
        String status = "";
        String accountName = "";
        String creditAccount = "";
        Double minAmount = null;
        Double maxAmount = null;
        String accountBank = "bandes";
        
        Map<String, Object> paramsMock1 = ListTransactionLinesActivity.getListTransactionsLinesParams(status, accountName, creditAccount, minAmount, 
                maxAmount, accountBank, "", 1);
        
        assertEquals(paramsMock1.get("accountBank"), accountBank);
    }
    
    @Test
    public void givenFiltersWithTechbankValue_whenParamsArebuilt_thenSamebankParamsResult() throws ActivityException {
        String status = "";
        String accountName = "";
        String creditAccount = "";
        Double minAmount = null;
        Double maxAmount = null;
        String accountBank = "techbank";
        
        Map<String, Object> paramsMock1 = ListTransactionLinesActivity.getListTransactionsLinesParams(status, accountName, creditAccount, minAmount, 
                maxAmount, accountBank, "", 1);
        
        assertEquals(ListTransactionLinesActivity.SAME_BANK, paramsMock1.get("accountBank"));
    }
    
    @Test
    public void givenFiltersWithAccountName_whenParamsArebuilt_thenAccountNameIsInLikeOperatorResult() throws ActivityException {
        String status = "";
        String accountName = "Test Account";
        String creditAccount = "";
        Double minAmount = null;
        Double maxAmount = null;
        String accountBank = "";
        
        Map<String, Object> paramsMock1 = ListTransactionLinesActivity.getListTransactionsLinesParams(status, accountName, creditAccount, minAmount, 
                maxAmount, accountBank, "", 1);
        
        assertEquals(paramsMock1.get("accountName"), "%"+ accountName +"%");
    }
    

}