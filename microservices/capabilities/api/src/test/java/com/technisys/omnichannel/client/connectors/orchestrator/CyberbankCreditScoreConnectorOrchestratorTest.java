/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.CreditScore;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class})

public class CyberbankCreditScoreConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(Mockito.anyString(), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(Mockito.anyString(), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
    }

    @Test(expected = IllegalStateException.class)
    public void testCoreCreditCardConnectorOrchestatorException()  {
        new CoreCreditScoreConnectorOrchestator();
    }


    @Test
    public void testReadCreditCardDetails() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "creditCardRequest.score")).thenReturn("6");
        CreditScore creditScore = CoreCreditScoreConnectorOrchestator.creditScore("6");
        Assert.assertNotNull(creditScore);
    }

    @Test
    public void testReadCreditCardDetails2() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "creditCardRequest.score")).thenReturn("6");
        CreditScore creditScore = CoreCreditScoreConnectorOrchestator.creditScore("10");
        Assert.assertNotNull(creditScore);
    }

    @Test(expected = BackendConnectorException.class)
    public void testReadCreditCardException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        Mockito.when(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "creditCardRequest.score")).thenThrow(new IOException("Error"));
        CoreCreditScoreConnectorOrchestator.creditScore("10");
    }

}