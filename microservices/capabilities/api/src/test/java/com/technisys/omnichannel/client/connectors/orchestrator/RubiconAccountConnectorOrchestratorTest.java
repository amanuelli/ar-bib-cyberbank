/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorO;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.StatementResult;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Product;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class, RubiconCoreConnectorO.class, RubiconCoreConnectorC.class})

public class RubiconAccountConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM),  Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM),  Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
        PowerMockito.mockStatic(RubiconCoreConnectorO.class);
        PowerMockito.mockStatic(RubiconCoreConnectorC.class);

    }

    @Test
    public void testAddAccountTypeCyberbankOk() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorO.addAccount(Mockito.anyString(), Mockito.anyInt())).thenReturn(12);
        int accountId = CoreAccountConnectorOrchestrator.addAccount("212", "3131");
        Assert.assertEquals(12, accountId);
    }

    @Test
    public void testReadAccountOK() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Product product = new Product();
        product.setExtraInfo("CA|3334|USD|01000|10|10001|1|1");

        Mockito.when(RubiconCoreConnectorC.readAccountDetails("323", product.getIdProduct(), product.getExtraInfo())).thenReturn(new Account() );
        Account account = CoreAccountConnectorOrchestrator.readAccount("323", product);
        Assert.assertNotNull(account);
    }

    @Test
    public void testStatementsOK() throws BackendConnectorException, CyberbankCoreConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");


        Date startDate = new Date();
        Date endDate = new Date();
        Mockito.when(RubiconCoreConnectorC.listAccountStatements(
                "323",
               "232",
                "232",
                startDate,
                endDate,
                10.9,
                1.0,
                "test",
                new String[0],
                0,
                10,
                1

        )).thenReturn(new ArrayList<>());

       StatementResult result = CoreAccountConnectorOrchestrator.statements("21",
               "323",
                "232",
                startDate,
                endDate,
                0.2,
               0.5,
               "ref",
                new String[0],
               0,
                10,
                1);
        Assert.assertNotNull(result);
    }


    @Test
    public void testCountMovements() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorC.listAccountStatementsTotalCount(Mockito.anyString())).thenReturn(0);
        int countMovements =  CoreAccountConnectorOrchestrator.countMovements("21", new StatementResult(0, new ArrayList<>()));
        Assert.assertEquals(0, countMovements);
    }


    @Test
    public void testReadStatement() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Mockito.when(RubiconCoreConnectorC.readStatement(Mockito.anyInt(),Mockito.anyString())).thenReturn(new Statement());
        Statement statement = CoreAccountConnectorOrchestrator.readStatement(0, "31");
        Assert.assertNotNull(statement);
    }


    @Test
    public void testListAccount() throws CyberbankCoreConnectorException, BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");

        Mockito.when(RubiconCoreConnectorC.listProducts("212", new ArrayList<>(), new ArrayList<>())).thenReturn(new ArrayList<>());
        List<Account> accountList = CoreAccountConnectorOrchestrator.list("212", new ArrayList<>(), new ArrayList<>());
        Assert.assertNotNull(accountList);
    }


    @Test
    public void testReadNoteBoucher() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("rubicon");
        Statement response = new Statement();
        Mockito.when(RubiconCoreConnectorC.readNoteBoucher(212)).thenReturn(response);
        Statement statement = CoreAccountConnectorOrchestrator.readNoteBoucher(212);
        Assert.assertNotNull(statement);
    }
}