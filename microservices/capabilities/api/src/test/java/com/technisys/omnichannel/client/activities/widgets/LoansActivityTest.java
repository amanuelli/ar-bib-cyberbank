package com.technisys.omnichannel.client.activities.widgets;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankLoanConnector;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import utils.TestUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.eq;


@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, ConfigurationFactory.class, CoreLoanConnectorOrchestrator.class, CyberbankLoanConnector.class, I18nFactory.class})
public class LoansActivityTest {
    private Request request;

    @Mock
    private Administration administrationMock;

    @Mock
    private Configuration configurationMock;

    @Mock
    private I18nFactory i18nFactoryMock;

    @Mock
    private I18n i18nMock;

    private LoansActivity activity;


    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);

        PowerMockito.mockStatic(I18nFactory.class);
        Mockito.when(I18nFactory.getHandler()).thenReturn(i18nMock);

        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString()))
                .thenReturn("http://WEBSERVICES_URL:PORT");

        Mockito.when(configurationMock.getString(eq(Configuration.PLATFORM), Mockito.anyString()))
                .thenReturn("USD");

        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt()))
                .thenReturn(50);

        PowerMockito.mockStatic(CoreLoanConnectorOrchestrator.class);
        PowerMockito.mockStatic(CyberbankLoanConnector.class);

        Mockito.when(configurationMock.getDefaultString(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyString())).thenReturn("cyberbank");


        request = new Request();
        activity = new LoansActivity();
    }

    @Test
    public void executeReturnOkEmptyList() throws IOException, ActivityException, BackendConnectorException {
        Environment env = new Environment();
        env.setProductGroupId("a1a1a1a1a1a");

        Mockito.when(administrationMock.readProduct(
                Mockito.anyString(),
                Mockito.anyInt()
        ))
                .thenReturn(TestUtils.getValidProduct());

        Mockito.when(administrationMock.readEnvironment(Mockito.anyInt()))
                .thenReturn(env);

        Mockito.when(CoreLoanConnectorOrchestrator.list(
                Mockito.any(),
                Mockito.anyList(),
                Mockito.any()
        ))
                .thenReturn(new ArrayList<>());

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    @Test
    public void executeReturnOk() throws IOException, ActivityException, BackendConnectorException {
        Environment env = new Environment();
        env.setProductGroupId("a1a1a1a1a1a");

        List<Product> productList = new ArrayList<Product>() {{
            add(TestUtils.getValidProduct());
        }};

        Mockito.doReturn(productList).when(administrationMock).listAuthorizedProducts(
                Mockito.nullable(String.class), Mockito.anyInt(), Mockito.anyList(), Mockito.anyList()
        );

        Mockito.when(administrationMock.readProduct(
                Mockito.anyString(),
                Mockito.anyInt()
        ))
                .thenReturn(TestUtils.getValidProduct());

        Mockito.when(administrationMock.readEnvironment(Mockito.anyInt()))
                .thenReturn(env);

        Mockito.when(i18nMock.getMessage(Mockito.nullable(String.class), Mockito.nullable(String.class))).thenReturn("");

        List<Loan> list = new ArrayList<>();
        Loan loan = new Loan(TestUtils.getValidProduct());
        list.add(loan);
        Mockito.when(CoreLoanConnectorOrchestrator.list(
                Mockito.nullable(String.class),
                Mockito.nullable(List.class),
                Mockito.any()
        ))
                .thenReturn(list);

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    @Test(expected = ActivityException.class)
    public void executeReturnIOException() throws ActivityException, IOException {
        Mockito.doThrow(new IOException()).when(administrationMock).listAuthorizedProducts(
                Mockito.nullable(String.class), Mockito.anyInt(), Mockito.anyList(), Mockito.anyList()
        );


        Response response = activity.execute(request);
    }

    @Test(expected = ActivityException.class)
    public void executeReturnBackendConnectorException() throws ActivityException, IOException, BackendConnectorException {
        Environment env = new Environment();
        env.setProductGroupId("a1a1a1a1a1a");

        List<Product> productList = new ArrayList<Product>() {{
            add(TestUtils.getValidProduct());
        }};

        Mockito.doReturn(productList).when(administrationMock).listAuthorizedProducts(
                Mockito.nullable(String.class), Mockito.anyInt(), Mockito.anyList(), Mockito.anyList()
        );

        Mockito.when(administrationMock.readProduct(
                Mockito.anyString(),
                Mockito.anyInt()
        ))
                .thenReturn(TestUtils.getValidProduct());

        Mockito.when(administrationMock.readEnvironment(Mockito.anyInt()))
                .thenReturn(env);

        Mockito.when(i18nMock.getMessage(Mockito.nullable(String.class), Mockito.nullable(String.class))).thenReturn("");

        List<Loan> list = new ArrayList<>();
        Loan loan = new Loan(TestUtils.getValidProduct());
        list.add(loan);
        Mockito.when(CoreLoanConnectorOrchestrator.list(
                Mockito.nullable(String.class),
                Mockito.nullable(List.class),
                Mockito.any()
        ))
                .thenThrow(new BackendConnectorException("error"));


        Response response = activity.execute(request);
    }



}
