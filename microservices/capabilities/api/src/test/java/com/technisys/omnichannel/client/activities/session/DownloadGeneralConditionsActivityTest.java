package com.technisys.omnichannel.client.activities.session;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.domain.GeneralConditionDocument;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

/*
 *
 * @author Marcelo Bruno
 */

@PowerMockIgnore({ "org.apache.logging.log4j.*", "javax.management.*" })
@RunWith(PowerMockRunner.class)
@PrepareForTest({GeneralConditions.class})
public class DownloadGeneralConditionsActivityTest {

    @Mock
    private GeneralConditions generalConditionsMock;
    private static Request request;
    private DownloadGeneralConditionsActivity activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        activity = new DownloadGeneralConditionsActivity();
        request = new Request();

        PowerMockito.mockStatic(GeneralConditions.class);
        Mockito.when(GeneralConditions.getInstance()).thenReturn(generalConditionsMock);

        request.setIdActivity("session.downloadGeneralConditions");
        request.setChannel("frontend");
    }

    @Test
    public void executeValidateFileName() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put(DownloadGeneralConditionsActivity.InParams.FILE_NAME_KEY, "");
        request.setParameters(params);
        Map<String, String> result = activity.validate(request);
        assertEquals("session.downloadGeneralConditions.fileNameKey.empty", result.get(DownloadGeneralConditionsActivity.InParams.FILE_NAME_KEY));
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionDocumentNotFound() throws IOException, ActivityException {
        Mockito
                .when(generalConditionsMock.actualCondition(
                ))
                .thenReturn(1);
        Mockito
                .when(generalConditionsMock.readGeneralConditionDocument(
                        Mockito.anyInt(),
                        Mockito.anyString()
                ))
                .thenReturn(null);

        activity.execute(request);
    }

    @Test
    public void executeOk() throws IOException, ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put(DownloadGeneralConditionsActivity.InParams.FILE_NAME_KEY, "fileTest");
        request.setParameters(params);

        GeneralConditionDocument generalConditionDocument = new GeneralConditionDocument();
        generalConditionDocument.setIdCondition(1);
        generalConditionDocument.setFileNameKey("testName");
        generalConditionDocument.setContent("Content test".getBytes());
        Mockito
                .when(generalConditionsMock.actualCondition(
                ))
                .thenReturn(1);
        Mockito
                .when(generalConditionsMock.readGeneralConditionDocument(
                        Mockito.anyInt(),
                        Mockito.anyString()
                ))
                .thenReturn(generalConditionDocument);

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

}
