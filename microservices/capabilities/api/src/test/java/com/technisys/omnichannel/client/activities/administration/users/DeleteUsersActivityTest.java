package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;

@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*"})
@RunWith(PowerMockRunner.class)
@PrepareForTest({Administration.class, ValidationUtils.class, Authorization.class, AccessManagementHandlerFactory.class})
public class DeleteUsersActivityTest {

    @Mock
    private Administration administrationMock;

    @Mock
    private AccessManagementHandler accessManagementHandlerMock;

    @Mock
    private ValidationUtils validationUtilsMock;

    private Request request;
    private DeleteUsersActivity activity;

    private String idUserForTest = "dchbchhdbchiwijed";
    private String nameUserForTest = "dchbchhdbchiwijed";

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);
        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        request = new Request();

        Map<String, Object> parameters = new HashMap<>();
        parameters.put("userIdList", new String[]{idUserForTest});
        parameters.put("userNameList", new String[]{nameUserForTest});
        request.setParameters(parameters);
        request.setIdUser(idUserForTest);
        request.setIdEnvironment(11);

        activity = new DeleteUsersActivity();
    }

    @Test(expected = ActivityException.class)
    public void executeExceptionTest() throws IOException, ActivityException {
        Mockito
                .when(accessManagementHandlerMock.getGroups(
                        Mockito.anyString(),
                        Mockito.isNull(),
                        Mockito.anyInt(),
                        Mockito.anyInt(),
                        Mockito.anyInt(),
                        Mockito.isNull()
                ))
                .thenThrow(IOException.class);

        activity.execute(request);
    }

    @Test
    public void executeOkTest() throws IOException, ActivityException {
        Mockito
                .when(accessManagementHandlerMock.getGroups(
                        Mockito.anyString(),
                        Mockito.isNull(),
                        Mockito.anyInt(),
                        Mockito.anyInt(),
                        Mockito.anyInt(),
                        Mockito.isNull()
                ))
                .thenReturn(new PaginatedList());

        Mockito
                .when(accessManagementHandlerMock.getUser(
                        Mockito.anyString()
                ))
                .thenReturn(new User());

        Response response = activity.execute(request);

        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

    @Test(expected = ActivityException.class)
    public void validateExceptionTest() throws IOException, ActivityException {
        Mockito
                .when(administrationMock.listEnvironmentUserIds(
                        Mockito.anyInt()
                ))
                .thenThrow(IOException.class);

        activity.validate(request);
    }

    @Test
    public void validateMustSelectUserTest() throws IOException, ActivityException {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("userIdList", null);
        request.setParameters(parameters);
        Mockito
                .when(administrationMock.listEnvironmentUserIds(
                        Mockito.anyInt()
                ))
                .thenReturn(new ArrayList<>());

        PowerMockito.mockStatic(ValidationUtils.class);
        Mockito
                .when(ValidationUtils.validateEmptyCredentials(request))
                .thenReturn(new HashMap<>());

        Map<String, String> result = activity.validate(request);

        assertEquals("administration.users.mustSelectUser", result.get("NO_FIELD"));
    }

    @Test
    public void validateUserDoesNotExistTest() throws IOException, ActivityException {
        Mockito
                .when(administrationMock.listEnvironmentUserIds(
                        Mockito.anyInt()
                ))
                .thenReturn(new ArrayList<>());

        Mockito
                .when(accessManagementHandlerMock.getUser(
                        Mockito.anyString()
                ))
                .thenReturn(null);

        PowerMockito.mockStatic(ValidationUtils.class);
        Mockito
                .when(ValidationUtils.validateEmptyCredentials(request))
                .thenReturn(new HashMap<>());

        Map<String, String> result = activity.validate(request);

        assertEquals("administration.users.idUserDoesntExists", result.get("NO_FIELD"));
    }

    @Test(expected = ActivityException.class)
    public void validateNotAuthorizedTest() throws IOException, ActivityException {
        Mockito
                .when(administrationMock.listEnvironmentUserIds(
                        Mockito.anyInt()
                ))
                .thenReturn(null);

        Mockito
                .when(accessManagementHandlerMock.getUser(
                        Mockito.anyString()
                ))
                .thenReturn(new User());

        PowerMockito.mockStatic(ValidationUtils.class);
        Mockito
                .when(ValidationUtils.validateEmptyCredentials(request))
                .thenReturn(new HashMap<>());

        activity.validate(request);

    }

    @Test
    public void validateCanNotDeleteAdministratorsTest() throws IOException, ActivityException {
        List<String> environmentUsers = new ArrayList<>();
        environmentUsers.add(idUserForTest);
        Mockito
                .when(administrationMock.listEnvironmentUserIds(
                        Mockito.anyInt()
                ))
                .thenReturn(environmentUsers);

        Mockito
                .when(accessManagementHandlerMock.getUser(
                        Mockito.anyString()
                ))
                .thenReturn(new User());

        PowerMockito.mockStatic(ValidationUtils.class);
        Mockito
                .when(ValidationUtils.validateEmptyCredentials(request))
                .thenReturn(new HashMap<>());

        PowerMockito.mockStatic(Authorization.class);
        Mockito
                .when(Authorization.hasPermission(
                        Mockito.anyString(),
                        Mockito.anyInt(),
                        Mockito.isNull(),
                        Mockito.anyString()
                ))
                .thenReturn(true);

        Map<String, String> result = activity.validate(request);

        assertEquals("administration.users.cantDeleteAdministrators", result.get("NO_FIELD"));
    }

}