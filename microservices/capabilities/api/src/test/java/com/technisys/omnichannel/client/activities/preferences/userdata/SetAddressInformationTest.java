package com.technisys.omnichannel.client.activities.preferences.userdata;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static com.technisys.omnichannel.client.activities.preferences.userdata.SetAddressInformation.InParams.ADDRESS;
import static com.technisys.omnichannel.client.activities.preferences.userdata.SetAddressInformation.InParams.MAILING_ADDRESS;
import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyString;

/*
 *
 * @author Marcelo Bruno
 */

@PowerMockIgnore({ "org.apache.logging.log4j.*", "javax.management.*" })
@RunWith(PowerMockRunner.class)
@PrepareForTest({ Administration.class, CountryCodesHandler.class, AccessManagementHandlerFactory.class, CoreCustomerConnectorOrchestrator.class})
public class SetAddressInformationTest {

    @Mock
    private Administration administrationMock;
    @Mock
    private AccessManagementHandler accessManagementHandlerMock;

    private static Request request;
    private SetAddressInformation activity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        activity = new SetAddressInformation();
        request = new Request();

        PowerMockito.mockStatic(CountryCodesHandler.class);
        PowerMockito.mockStatic(CoreCustomerConnectorOrchestrator.class);
        PowerMockito.mockStatic(Administration.class);
        PowerMockito.mockStatic(AccessManagementHandlerFactory.class);

        Mockito.when(Administration.getInstance()).thenReturn(administrationMock);
        Mockito.when(AccessManagementHandlerFactory.getHandler()).thenReturn(accessManagementHandlerMock);

        request.setIdActivity("preferences.userData.getUserAddressInformation");
        request.setChannel("frontend");
        request.setIdUser("idUserTest");
        request.setIdTransaction("idTransactionTest");
    }

    @Test
    public void testInvalidAddress() throws ActivityException {
        Map<String, Object> params = new HashMap<>();
        params.put(ADDRESS, "invalid address");
        request.setParameters(params);
        Map<String, String> result = activity.validate(request);
        assertEquals("preferences.userData.setUserAddressInformation.invalidAddressObject", result.get(ADDRESS));
    }

    @Test
    public void testInvalidMailingAddress() throws ActivityException {
        Map<String, Object> params = new HashMap<>();
        Map<String, Object> addressMap = new LinkedHashMap<>();
        addressMap.put("addressLine1", "701 Brickell Ave");
        addressMap.put("addressLine2", "1550");
        addressMap.put("country", "US");
        addressMap.put("city", "Miami");
        addressMap.put("federalState", "FL");
        addressMap.put("zipcode", "33131");
        params.put(ADDRESS, addressMap);

        addressMap = new LinkedHashMap<>();
        addressMap.put("addressLine1", "701 Brickell Ave");
        params.put(MAILING_ADDRESS, addressMap);
        request.setParameters(params);
        Map<String, String> result = activity.validate(request);
        assertEquals("preferences.userData.setUserAddressInformation.invalidAddressObject", result.get(MAILING_ADDRESS));
    }

    @Test(expected = ActivityException.class)
    public void executeThrowException() throws IOException, ActivityException, BackendConnectorException {
        Mockito
                .when(administrationMock.readEnvironment(
                        Mockito.anyInt()
                ))
                .thenReturn(new Environment());
        Mockito
                .when(accessManagementHandlerMock.getUser(
                        Mockito.anyString()
                ))
                .thenReturn(new User());
        Mockito
                .when(CoreCustomerConnectorOrchestrator.read(
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString()
                ))
                .thenReturn(null);

        activity.execute(request);
    }

    @Test
    public void testOk() throws Exception {
        Environment environment = new Environment();
        environment.setProductGroupId("1234");
        User user = new User();
        user.setDocumentCountry("US");
        user.setDocumentType("PAS");
        user.setDocumentNumber("1234");
        ClientUser backendClient = new ClientUser();
        backendClient.setAddress("{\"zipcode\":\"33131\",\"country\":\"US\",\"city\":\"Miami\",\"addressLine1\":\"701 Brickell Ave\",\"addressLine2\":\"1550\",\"federalState\":\"FL\"}");
        Mockito
                .when(CountryCodesHandler.getCountryList(
                        Mockito.anyString()
                ))
                .thenReturn(null);
        Mockito
                .when(administrationMock.readEnvironment(
                        Mockito.anyInt()
                ))
                .thenReturn(environment);
        Mockito
                .when(accessManagementHandlerMock.getUser(
                        Mockito.anyString()
                ))
                .thenReturn(user);
        Mockito
                .when(CoreCustomerConnectorOrchestrator.read(
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString(),
                        Mockito.anyString()
                ))
                .thenReturn(backendClient);

        Response response = activity.execute(request);
        assertEquals(ReturnCodes.OK, response.getReturnCode());
    }

}
