/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.utils;

import org.junit.Assert;
import org.junit.Test;

public class DocumentTypeResolverTest {

    @Test
    public void testDocumentMapper() {
        Assert.assertEquals("DNI", DocumentTypeResolver.resolve("DNI"));
        Assert.assertEquals("DNI", DocumentTypeResolver.resolve("CI"));
        Assert.assertEquals("PA", DocumentTypeResolver.resolve("PAS"));
        Assert.assertEquals("CUIL", DocumentTypeResolver.resolve("CUIL"));
    }
}
