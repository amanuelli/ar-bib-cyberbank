/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import org.apache.commons.lang3.NotImplementedException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.util.Date;

import static org.mockito.ArgumentMatchers.eq;


@RunWith(PowerMockRunner.class)
@PowerMockIgnore({"org.apache.logging.log4j.*", "javax.management.*", "javax.net.ssl.*"})
@PrepareForTest({ConfigurationFactory.class, CoreConnectorOrchestrator.class})

public class CyberbankCreditCardConnectorOrchestratorTest {

    @Mock
    private Configuration configurationMock;


    @Before
    public void setUp() throws IOException {
        MockitoAnnotations.initMocks(this);

        PowerMockito.mockStatic(ConfigurationFactory.class);
        Mockito.when(ConfigurationFactory.getInstance()).thenReturn(configurationMock);
        Mockito.when(configurationMock.getURLSafe(eq(Configuration.PLATFORM), Mockito.anyString())).thenReturn("http://CORE_SERVER:PORT");
        Mockito.when(configurationMock.getDefaultInt(eq(Configuration.PLATFORM), Mockito.anyString(), Mockito.anyInt())).thenReturn(50);

        PowerMockito.mockStatic(CoreConnectorOrchestrator.class);
    }

    @Test(expected = NotImplementedException.class)
    public void testReadCreditCardDetailsException() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.readCreditCardDetails("21", "21", "21");
    }

    @Test(expected = NotImplementedException.class)
    public void testReadStatementDetailsException() throws BackendConnectorException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.readStatementDetails("21", "21",0, "21");
    }

    @Test(expected = NotImplementedException.class)
    public void testListCreditCardStatementLinesException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.listCreditCardStatementLines("21");
    }

    @Test(expected = NotImplementedException.class)
    public void testReadCreditCardStatementLinesException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.readCreditCardStatementLine("21");
    }

    @Test(expected = NotImplementedException.class)
    public void testGetTotalCreditCardStatementsException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.getTotalCreditCardStatements("21");
    }

    @Test(expected = NotImplementedException.class)
    public void testGetCurrentPeriodCreditCardStatementsException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.getCurrentPeriodCreditCardStatements("21");
    }

    @Test(expected = NotImplementedException.class)
    public void testListCreditCardStatementsException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.listCreditCardStatements("21", "2", "1", new Date(), new Date(), 0.1, 0.3, "test", "UDS", 0, 0 );
    }

    @Test(expected = NotImplementedException.class)
    public void testPayCreditCardValidateException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.payCreditCardValidate("21", "2", new Account(), "21", "creditcardBank", "USD", 100.0, "test", 0.0, 0.1);
    }

    @Test(expected = NotImplementedException.class)
    public void testPayCreditCardSendException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.payCreditCardSend("21", "2", new Account(), "21", "creditcardBank", "USD", 100.0, "test", 0.0, 0.1);
    }

    @Test(expected = NotImplementedException.class)
    public void testCreateCreditCardException() throws BackendConnectorException, IOException {
        Mockito.when(CoreConnectorOrchestrator.getCurrentCore()).thenReturn("cyberbank");
        CoreCreditCardConnectorOrchestator.createCreditCard("21", "2", "test");
    }

}