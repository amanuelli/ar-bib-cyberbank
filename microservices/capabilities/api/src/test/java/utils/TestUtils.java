/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package utils;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.StatementResult;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.core.domain.Product;

import java.util.ArrayList;

public class TestUtils {

    public static Product getValidProduct() {
        Product product = new Product();
        product.setIdEnvironment(1);
        product.setIdProduct("123");
        product.setProductType(Constants.PRODUCT_CA_KEY);
        product.setLabel("qwer");
        product.setProductAlias("qwer");
        product.setPaperless(true);
        product.setExtraInfo(Constants.PRODUCT_CA_KEY+"|qwer|1234|4321|123|1233|123|123|234|234");

        return product;
    }

    public static Account getValidAccount() {
        Account account = new Account();
        account.setCurrency("UYU");
        account.setOverdraftLine(10);
        account.setPendingBalance(100);
        account.setBalance(10);
        account.setCountableBalance(100);
        account.setOwnerName("Owner Name");
        account.setProductAlias("Alias");
        account.setTotalCheckAmount(100);
        account.setLabel("LABEL");
        account.setNumber("1");
        account.setIdProduct("CA");
        account.setIdEnvironment(1);

        return account;
    }

    public static StatementResult getStatementResult (){
        return new StatementResult(0, new ArrayList<Statement>());
    }

    public static Loan getValidLoan() {
        Loan loan = new Loan();
        loan.setCurrency("USD");
        loan.setPendingBalance(100);
        loan.setProductAlias("Alias");
        loan.setLabel("LABEL");
        loan.setNumber("1");
        loan.setIdProduct("PA");
        loan.setIdEnvironment(1);
        loan.setNumberOfPaidFees(1);
        loan.setNumberOfFees(12);

        return loan;
    }

}
