/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.client.activities.preferences.securitySeals;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.client.utils.SecuritySealUtils;
import org.apache.commons.lang3.StringUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DocumentedActivity("Modify Security Seals")
public class ModifySecuritySealActivity extends Activity {
    
    public static final String ID = "preferences.securityseals.modify";
    
    public interface InParams {
        @DocumentedParam(type = List.class, description = "Security Seal ID")
        String SECURITY_SEAL = "_securitySeal";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            user.setIdSeal(request.getParam(InParams.SECURITY_SEAL, int.class));

            AccessManagementHandlerFactory.getHandler().updateUser(user);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            String sealString = request.getParam(InParams.SECURITY_SEAL, String.class);
            if (StringUtils.isEmpty(sealString)){
                result.put(InParams.SECURITY_SEAL, "settings.changeSecuritySeal.seal.invalid");
            } else {
                if (StringUtils.isNumeric(sealString)) {
                    int sealValue = request.getParam(InParams.SECURITY_SEAL, int.class);
                    if (SecuritySealUtils.getSealList().get(sealValue) == null) {
                        result.put(InParams.SECURITY_SEAL, "settings.changeSecuritySeal.seal.invalid");
                    }
                } else {
                    result.put(InParams.SECURITY_SEAL, "settings.changeSecuritySeal.seal.invalid");
                }
            }
            return result;
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.VALIDATION_ERROR);
        }
    }
}