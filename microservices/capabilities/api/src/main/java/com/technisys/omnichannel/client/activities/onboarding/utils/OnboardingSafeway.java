/*
 * Copyright 2019 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.onboarding.utils;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.onboarding.*;
import com.technisys.omnichannel.client.connectors.safeway.SafewayRESTConnector;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.domain.OnboardingDocument;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.documenttypes.DocumentTypesHandler;
import com.technisys.omnichannel.core.domain.DocumentTypesCountryCodes;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.onboardings.plugins.DocumentReaderResponse;
import com.technisys.omnichannel.core.utils.ImageUtils;
import com.technisys.safeway.client.ValidateDataDocumentResult;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedHashMap;

import static com.technisys.omnichannel.client.Constants.*;

public class OnboardingSafeway implements OnboardingSteps {

    private static final Logger log = LoggerFactory.getLogger(OnboardingDigital.class);

    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    @Override
    public Response doUploadFrontDocumentExecute(Request request) throws ActivityException {
        Response response = new Response(request);
        boolean isProductRequest = false;
        try {
            String documentToSave = request.getParam(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, String.class);
            if ("phonegap".equals(request.getParameters().get("channel"))) {
                documentToSave = ImageUtils.rotateBase64Image(documentToSave, -90, "JPG");
            }
            //puede que el usuario haya vuelto atras en el wizard y esté repitiendo este paso, o puede que sea la primera vez
            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);

            //si es la primera vez lo creamos
            if (StringUtils.isBlank(exchangeTokenId)) {
                exchangeTokenId = ExchangeTokenHandler.create(request.getChannel(), request.getLang(), request.getIdUser(),
                        -1, request.getUserAgent(), request.getClientIP(), new String[]{UploadFrontDocumentActivity.ID, UploadBackDocumentActivity.ID});
            } else {
                isProductRequest = OnboardingUtils.isProductRequest(OnboardingHandlerFactory.getInstance().read(ExchangeTokenHandler.read(exchangeTokenId).getAttribute("idOnboarding")));
            }

            if (isProductRequest) {
                ExchangeTokenHandler.updateActivities(exchangeTokenId, new String[]{UploadFrontDocumentActivity.ID, UploadBackDocumentActivity.ID});
            }
            ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);

            Long idOnboarding = exchangeToken.getAttribute("idOnboarding");
            if (idOnboarding == null) {
                Onboarding onboarding = new Onboarding();
                onboarding.setStatus(Onboarding.STATUS_STARTED);
                onboarding.setType(Onboarding.ONBOARDING_DEFAULT);
                idOnboarding = OnboardingHandlerFactory.getInstance().create(onboarding);
            }
            Boolean isPassport = request.getParam(UploadFrontDocumentActivity.InParams.IS_PASSPORT, Boolean.class);

            exchangeToken.addAttribute("idOnboarding", idOnboarding);
            exchangeToken.addAttribute("isPassport", isPassport);

            ExchangeTokenHandler.updateData(exchangeTokenId, exchangeToken.getData());

            OnboardingDocument document = new OnboardingDocument(Base64.decodeBase64(documentToSave), DOCUMENT_FRONT, idOnboarding);
            OnboardingHandlerFactory.getInstance().createDocument(document);

            if (isProductRequest) {
                OnboardingHandlerFactory.getInstance().updateStatus(idOnboarding, Onboarding.STATUS_ID_PARTIALLY_LOADED);
            }

            response.putItem(UploadFrontDocumentActivity.OutParams.EXCHANGE_TOKEN, exchangeTokenId);
            response.setReturnCode(ReturnCodes.OK);
            if (isPassport) {
                response = doUploadBackDocumentExecute(request, exchangeTokenId);
            } else {
                OnboardingDocument documentBack = OnboardingHandlerFactory.getInstance().readDocument(idOnboarding, DOCUMENT_BACK);
                if(documentBack != null) {
                    String documentBase64 = new String(java.util.Base64.getEncoder().encode(documentBack.getContent()));
                    request.getParameters().put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, documentBase64);
                    response = doUploadBackDocumentExecute(request, exchangeTokenId);
                    if(ReturnCodes.OK.equals(response.getReturnCode())){
                        response.putItem(UploadFrontDocumentActivity.OutParams.SKIP_BACK_DOCUMENT_STEP, true);
                    }
                }
            }

        } catch (IOException ex) {
            LinkedHashMap<String, Object> errors = new LinkedHashMap<>();
            errors.put(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.faceRecognitionServiceFail");
            response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
            response.setData(errors);
        }

        return response;
    }

    /**
     * Ejecuta el procesamiento para obtener los datos del documento
     *
     * @param request              Request realizado
     * @param requestExchangeToken Si el exchange del request es nulo, se debe
     *                             crear uno y enviarlo como parámetro, como en el primer paso del
     *                             onboarding (UploadFrontDocumentActivity)
     * @throws ActivityException En caso de algún error con el procesamiento del documento
     */
    @Override
    public Response doUploadBackDocumentExecute(Request request, String requestExchangeToken) throws ActivityException {
        Response response = new Response(request);
        LinkedHashMap<String, Object> errors = new LinkedHashMap<>();
        try {
            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);
            if (StringUtils.isBlank(exchangeTokenId)) {
                exchangeTokenId = requestExchangeToken;
            }
            ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);

            String documentToSave = request.getParam(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, String.class);
            if ("phonegap".equals(request.getParameters().get("channel"))) {
                documentToSave = ImageUtils.rotateBase64Image(documentToSave, -90, "JPG");
            }

            long idOnboarding = exchangeToken.getAttribute("idOnboarding");
            OnboardingDocument document = new OnboardingDocument(Base64.decodeBase64(documentToSave), DOCUMENT_BACK, idOnboarding);
            OnboardingHandlerFactory.getInstance().createDocument(document);

            DocumentReaderResponse drResponse = null;
            boolean isPassport = exchangeToken.getAttribute("isPassport");

            if (ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "demo.safeMode")) {
                drResponse = OnboardingUtils.demoSafeModeReadDocument();
            } else {
                drResponse = new SafewayRESTConnector().extractDocumentData(idOnboarding, false, isPassport);
            }

            if (drResponse.isSuccess()) {

                if (!drResponse.getDocumentNumberVerified()) {
                    errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.abbyy.exception");
                    response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
                    response.setData(errors);
                    return response;
                }

                String alpha2DocumentCountry = CountryCodesHandler.getCountryCodyByAlpha3(drResponse.getDocumentCountry());
                String documentTypeCode = "";
                //Con el codigo de documento MRTD obtengo el document-type correspondiente
                DocumentTypesCountryCodes documentTypesCountryCodes = new DocumentTypesCountryCodes(alpha2DocumentCountry, drResponse.getDocumentType());
                documentTypeCode = DocumentTypesHandler.getDocumentTypeFromMrtdDocumentType(documentTypesCountryCodes);
                OnboardingUtils.commonValidations(errors, drResponse, alpha2DocumentCountry, documentTypeCode);

                if (errors.isEmpty()) {
                    OnboardingUtils.extractFullDataResponse(response, drResponse, idOnboarding, documentTypeCode, alpha2DocumentCountry, exchangeToken.getAttribute("creditCardId"), exchangeTokenId);
                    ExchangeTokenHandler.updateActivities(exchangeTokenId, new String[]{UploadFrontDocumentActivity.ID, UploadBackDocumentActivity.ID, UploadSelfieActivity.ID});
                }
            } else if (StringUtils.isBlank(drResponse.getProviderReturnCode())) {
                errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.documentReaderFail");
            } else {
                errors.put("providerErrorCode", drResponse.getProviderReturnCode());
                if (I18nFactory.getHandler().isMessageExists("onboarding.uploadBackDocument.errorCode." + drResponse.getProviderReturnCode(), request.getLang())) {
                    errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.errorCode." + drResponse.getProviderReturnCode());
                } else {
                    errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.documentReaderFail");
                }
            }

            if (errors.size() > 0) {
                response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
                response.setData(errors);
            } else {
                response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
            }

        } catch (Exception ex) {
            log.error("Error parsing document", ex);
            errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.abbyy.exception");
            response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
            response.setData(errors);
        }

        return response;
    }

    @Override
    public Response doUploadSelfieExecute(Request request) throws ActivityException {
        Response response = new Response(request);
        boolean isProductRequest = false;

        try {
            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);

            ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);

            String documentSave = request.getParam(UploadSelfieActivity.InParams.DOCUMENT_TO_SAVE, String.class);
            Long idOnboarding = exchangeToken.getAttribute("idOnboarding");

            OnboardingDocument document = new OnboardingDocument(Base64.decodeBase64(documentSave), DOCUMENT_SELFIE, idOnboarding);
            OnboardingHandlerFactory.getInstance().createDocument(document);

            isProductRequest = OnboardingUtils.isProductRequest(OnboardingHandlerFactory.getInstance().read(ExchangeTokenHandler.read(exchangeTokenId).getAttribute("idOnboarding")));

            if (isProductRequest) {
                OnboardingHandlerFactory.getInstance().updateStatus(idOnboarding, Onboarding.STATUS_ID_VERIFICATION_DONE);
            }

            if (ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "demo.safeMode")) {
                log.info("UploadSelfieActivity. Returns OK because demo.safeMode is on");
                response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
            } else {
                boolean validateResponse = false;
                boolean isPassport = exchangeToken.getAttribute("isPassport");

                ValidateDataDocumentResult validateDataDocumentResult = new ValidateDataDocumentResult();
                if (ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "demo.safeMode")) {
                    validateResponse = true;
                } else {
                    validateDataDocumentResult = new SafewayRESTConnector().validateOnboardingDocument(idOnboarding, isPassport);
                    validateResponse = validateDataDocumentResult.getReturnCode() == ValidateDataDocumentResult.ReturnCodeEnum.NUMBER_0;
                }

                LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

                if (validateDataDocumentResult.getReturnCode().getValue() == null) {
                    errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.faceRecognitionFail");
                } else {
                    if (I18nFactory.getHandler().isMessageExists("onboarding.uploadSelfie.errorCode." + validateDataDocumentResult.getReturnCode(), request.getLang())) {
                        errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.errorCode." + validateDataDocumentResult.getReturnCode());
                    }else{
                        log.warn("onboarding.uploadSelfie return code unkown {}", validateDataDocumentResult.getReturnCode());
                        errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.faceRecognitionFail");
                    }
                }

                if (validateResponse) {
                    response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
                } else {
                    response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
                    response.setData(errors);
                }

            }
            ExchangeTokenHandler.updateActivities(exchangeTokenId, new String[]{UploadFrontDocumentActivity.ID, UploadBackDocumentActivity.ID, UploadSelfieActivity.ID,  Step5Activity.ID});
            response.putItem(UploadSelfieActivity.OutParams.EXCHANGE_TOKEN, exchangeTokenId);

            return response;
        } catch (Exception ex) {
            LinkedHashMap<String, Object> errors = new LinkedHashMap<>();
            errors.put(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.faceRecognitionFail");
            response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
            response.setData(errors);
            return response;
        }
    }

}
