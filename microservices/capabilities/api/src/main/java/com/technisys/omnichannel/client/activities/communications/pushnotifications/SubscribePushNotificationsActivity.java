/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications.pushnotifications;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.UserDevice;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

import org.apache.commons.lang3.StringUtils;

/**
 * Subscribe a device to receive push notifications
 */
@DocumentedActivity("Subscribe to push notifications")
public class SubscribePushNotificationsActivity extends Activity {

    public static final String ID = "communications.pushnotifications.subscribe";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Id device to subscribe")
        String ID_DEVICE = "idDevice";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            String idDevice = request.getParam(SubscribePushNotificationsActivity.InParams.ID_DEVICE, String.class);

            if (StringUtils.isEmpty(idDevice)) {
                throw new ActivityException(com.technisys.omnichannel.ReturnCodes.UNEXPECTED_ERROR);
            }

            UserDevice userDevice = AccessManagementHandlerFactory.getHandler().readUserDevice(idDevice, request.getIdUser());
            if(userDevice == null) {
                throw new ActivityException(com.technisys.omnichannel.ReturnCodes.UNEXPECTED_ERROR, "Device does not exist");
            }

            AccessManagementHandlerFactory.getHandler().subscribeUserDevicePushNotofications(request.getIdUser(), idDevice);

            response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
