/*
 *  Copyright 2011 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.credentials.development;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import java.io.IOException;

/**
 *
 * @author grosso
 */
public class SMSPlugin implements CredentialPlugin {

    @Override
    public void validate(IBRequest request, Credential credential) throws DispatchingException, IOException {
        String inputSmsPhone = credential.getValue() == null ? "" : credential.getValue();
        User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
        if (user == null || !user.getMobileNumber().equals(inputSmsPhone)) {
            throw new DispatchingException(ReturnCodes.INVALID_SMS_NUMBER, "Invalid SMS phone number");
        }
    }

    @Override
    public boolean validateFormat(IBRequest request, String value) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method validateFormat is not implemented for " + this.getClass().getName());
    }

    @Override
    public boolean validateFormat(String commonName, String value) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method validateFormat is not implemented for " + this.getClass().getName());
    }

    @Override
    public void changeStatus(String commonName, String newValue) throws DispatchingException, IOException {
        // No implementation required
    }

    @Override
    public void modify(IBRequest request, String currentValue, String newValue) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method modify is not implemented for " + this.getClass().getName());
    }

    @Override
    public void recover(String idUser, String newValue) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method recover is not implemented for " + this.getClass().getName());
    }

    @Override
    public void assign(String commonName, String value) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method assign is not implemented for " + this.getClass().getName());
    }

    @Override
    public void assign(IBRequest request, String commonName, String newValue) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method assign is not implemented for " + this.getClass().getName());
    }

}
