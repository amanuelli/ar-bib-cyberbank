/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.widgets;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 *
 */
public class AccountsActivity extends Activity {

    public static final String ID = "widgets.accounts";

    public interface InParams {
    }

    public interface OutParams {

        String ACCOUNTS = "accounts";
    }

    @SuppressWarnings("unchecked")
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {

            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            List<String> idPermissions = new ArrayList<>();
            idPermissions.add(PRODUCT_READ_PERMISSION);

            List<String> productTypes = Arrays.asList(Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY);

            List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);

            List<Account> coreAccounts = CoreAccountConnectorOrchestrator.list(request.getIdTransaction(), environment.getClients(), productTypes);

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            List<Account> accounts = new ArrayList<Account>();
            for (Product environmentProduct : environmentProducts) {
                for (Account coreAccount : coreAccounts) {
                    if (coreAccount.getIdProduct().equals(environmentProduct.getIdProduct())) {
                        Account account = new Account(environmentProduct);
                        account.setProductAlias(environmentProduct.getProductAlias());
                        account.setShortLabel(pLabeler.calculateShortLabel(account, false));
                        account.setBalance(coreAccount.getBalance());
                        Map<String, Boolean> permissions = new HashMap<>();
                        if (Constants.PRODUCT_CC_KEY.equals(environmentProduct.getProductType())) {
                            permissions.put(Constants.REQUEST_CHECKBOOK_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(),
                                    account.getIdProduct(), Constants.REQUEST_CHECKBOOK_PERMISSION));
                        }
                        permissions.put(Constants.TRANSFER_INTERNAL_PERMISSION,
                                Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), account.getIdProduct(), Constants.TRANSFER_INTERNAL_PERMISSION));
                        account.setPermissions(permissions);
                        account.setStatus(coreAccount.getStatus());
                        accounts.add(account);
                    }
                }
            }
            response.putItem(OutParams.ACCOUNTS, accounts);
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
