/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.userdata;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Address;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


/**
 * @author Marcelo Bruno
 */

/**
 * Activity to set backend client address information
 */
@DocumentedActivity("Set backend client address information")
public class SetAddressInformation extends Activity {

    public static final String ID = "preferences.userData.setUserAddressInformation";

    public interface InParams {

        @DocumentedParam(type = Map.class, description = "Address information. e.g. {\"zipcode\":\"33131\",\"country\":\"US\",\"city\":\"Miami\",\"addressLine1\":\" 701 Brickell Ave\",\"addressLine2\":\"1550\",\"federalState\":\"FL\"}")
        String ADDRESS = "address";
        @DocumentedParam(type = Map.class, description = "Mailing address information. Same address example", optional = true)
        String MAILING_ADDRESS = "mailingAddress";
    }


    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        Environment environment = null;
        try {
            environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            ClientUser backendClient = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), user.getDocumentCountry(), user.getDocumentType(), user.getDocumentNumber(), environment.getProductGroupId());

            if(backendClient != null) {
                Address address = new Address(request.getParam(InParams.ADDRESS, Map.class));

                Address mailingAddress = null;
                if(request.getParam(InParams.MAILING_ADDRESS, Map.class) != null) {
                    mailingAddress = new Address(request.getParam(InParams.MAILING_ADDRESS, Map.class));
                }
                CoreCustomerConnectorOrchestrator.updateClientAddress(request.getIdTransaction(), Integer.valueOf(environment.getProductGroupId()), address, mailingAddress);
            } else {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

        } catch (IOException | BackendConnectorException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        response.setReturnCode(ReturnCodes.OK);

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        Address address = new Address(request.getParam(InParams.ADDRESS, Map.class));
        if (address!=null && !address.valdiateFields()) {
            result.put(InParams.ADDRESS, "preferences.userData.setUserAddressInformation.invalidAddressObject");
        }

        if(request.getParam(InParams.MAILING_ADDRESS, Map.class) != null) {
            address = new Address(request.getParam(InParams.MAILING_ADDRESS, Map.class));
            if (address !=null && !address.valdiateFields()) {
                result.put(InParams.MAILING_ADDRESS, "preferences.userData.setUserAddressInformation.invalidAddressObject");
            }
        }

        return result;
    }

}
