/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import static com.technisys.omnichannel.client.activities.communications.SendCommunicationPreActivity.OutParams.COMMUNICATION_TRAYS;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.communicationstrays.CommunicationsTraysHandler;
import com.technisys.omnichannel.core.domain.CommunicationTray;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 *
 */
@DocumentedActivity("Communications sendPre")
public class SendCommunicationPreActivity extends Activity {

    public static final String ID = "communications.sendPre";
    
    public interface InParams {
    }

    public interface OutParams {
        @DocumentedParam(type =  CommunicationTray[].class, description =  "Array of communication's trays object details")
        String COMMUNICATION_TRAYS = "communicationTrays";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            
            PaginatedList <CommunicationTray> communicationTrays = CommunicationsTraysHandler.getInstance().list(0, 0, null);

            response.putItem(COMMUNICATION_TRAYS, communicationTrays.getElementList());
            response.setReturnCode(ReturnCodes.OK);
            
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
}