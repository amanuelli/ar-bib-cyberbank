/*
 *  Copyright 2012 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.activities.ActivityHandler;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.domain.ActivityProduct;
import com.technisys.omnichannel.core.preprocessors.authorization.AuthorizationHandler;
import com.technisys.omnichannel.core.preprocessors.authorization.CredentialGroup;

import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Sebastian Barbosa
 */
public class ValidationUtils {

    private ValidationUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static boolean validateEmailPattern(String email) {
        String mask = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "email.validationFormat", "^[a-zA-Z0-9+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$");
        Pattern emailPattern = Pattern.compile(mask, Pattern.CASE_INSENSITIVE);
        Matcher matcher = emailPattern.matcher(email != null ? email : "");
        return matcher.matches();
    }

    public static boolean validateUsername(String text) {
        if (text != null) {
            String pattern = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "username.pattern", "[a-zA-Z0-9@_\\.-]*");
            Pattern emailPattern = Pattern.compile(pattern, Pattern.CASE_INSENSITIVE);
            Matcher matcher = emailPattern.matcher(text);
            boolean valid = matcher.matches();

            int minLengt = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "username.minLength", 5);
            int maxLengt = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "username.maxLength", 50);

            valid = valid && text.length() >= minLengt && text.length() <= maxLengt;

            return valid;
        } else {
            return false;
        }
    }

    /**
     * Valida que las credenciales sean ingresadas.
     *
     * NOTA: Solo utilizar para actividades que no sean formularios de proceso
     *
     * @param request Request de la actividad
     * @return Mapa con los errores de validacion de credenciales vacia
     * @throws IOException Error de base de datos
     */
    public static Map<String, String> validateEmptyCredentials(Request request) throws IOException {
        Map<String, String> result = new HashMap<>();

        Set<String> credentials = new HashSet<>();
        ClientActivityDescriptor descriptor = ActivityHandler.getInstance().readDescriptor(request.getIdActivity());
        for (ActivityProduct product : descriptor.getPermissionRequiredList()) {
            for (CredentialGroup credGroup : AuthorizationHandler.getInstance().listPermissionCredentialsGroups(product.getIdPermission())) {
                if (credGroup.getCredentials() != null && !credGroup.getCredentials().isEmpty()) {
                    credentials.addAll(credGroup.getCredentials());
                }
            }
        }

        for (String cred : credentials) {
            Credential credential = request.getCredential(cred);
            if (credential == null || org.apache.commons.lang.StringUtils.isBlank(credential.getValue())) {
                result.put(cred, "transactions." + cred + ".empty");
            }
        }

        return result;
    }
}
