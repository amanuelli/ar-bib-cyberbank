package com.technisys.omnichannel.client.forms.fields.multilinefile.processors;

import com.technisys.omnichannel.client.activities.files.UploadFileActivity;
import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.client.domain.TransactionLine.ErrorCode;
import com.technisys.omnichannel.client.domain.TransactionLine.Status;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.*;
import java.util.*;

public class SalaryPaymentProcessor implements Processor {

    @Override
    public List<TransactionLine> process(byte[] content, String idTransaction, String reference) throws IOException, InvalidTotalAmountException {
        try (InputStream is = new ByteArrayInputStream(content)) {
            try (BufferedReader bfReader = new BufferedReader(new InputStreamReader(is))) {
                List<TransactionLine> result = new ArrayList<>();

                try {
                    String line = bfReader.readLine();
                    validateHeaderLine(line);

                    result = processLines(bfReader, getHeaderCurrency(line), idTransaction, reference);
                } catch (InvalidLineException ilE) {
                    Map<String, Object> transactionLine = new HashMap<>();

                    transactionLine.put("idTransaction", idTransaction);
                    transactionLine.put("status", Status.FORMAT_ERROR);
                    transactionLine.put("statusDate", new Date());
                    transactionLine.put("errorCode", ilE.getErrorCode());

                    result.add(new TransactionLine(transactionLine));
                }

                return result;
            }
        }
    }

    private List<TransactionLine> processLines(BufferedReader bfReader, String fileCurrency, String idTransaction,
            String reference) throws IOException, InvalidTotalAmountException {
        int lineNumber = 0;
        double totalAmount = 0;
        String line;
        List<TransactionLine> result = new ArrayList<>();

        List<String> allowedBanks = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "massive.payments.allowed.banks");
        while ((line = bfReader.readLine()) != null) {
            Map<String, Object> transactionLine = new HashMap<>();
            transactionLine.put("idTransaction", idTransaction);
            transactionLine.put("lineNumber", ++lineNumber);
            transactionLine.put("reference", reference);
            transactionLine.put("statusDate", new Date());
            transactionLine.put("errorCode", null);

            try {
                Map<String, Object> validatedLine = validateLine(line, fileCurrency, allowedBanks);
                totalAmount += Double.parseDouble((String) validatedLine.get("amount"));
                transactionLine.put("creditAccountNumber", validatedLine.get("account"));
                transactionLine.put("creditAccountName", validatedLine.get("beneficiaryName"));
                transactionLine.put("creditAmountQuantity", validatedLine.get("amount"));
                transactionLine.put("creditAmountCurrency", validatedLine.get("currency"));
                transactionLine.put("bankIdentifier", validatedLine.get("bankIdentifier"));
                transactionLine.put("status", Status.PENDING);
            } catch (InvalidLineException ilE) {
                transactionLine.put("status", Status.FORMAT_ERROR);
                transactionLine.put("errorCode", ilE.getErrorCode());
            }

            result.add(new TransactionLine(transactionLine));
        }

        if(totalAmount == 0){
            throw new InvalidTotalAmountException(ErrorCode.INVALID_TOTAL_AMOUNT); 
        }
        return result;
    }

    @Override
    public Map<String, Object> validate(byte[] content, String lang) throws IOException {
        Map<String, Object> response = new HashMap<>();

        try (InputStream is = new ByteArrayInputStream(content)) {
            try (BufferedReader bfReader = new BufferedReader(new InputStreamReader(is))) {
                String line = bfReader.readLine();

                try {
                    validateHeaderLine(line);
                    response.put("fileIdentifier", getHeaderFileIdentifier(line));
                    validateLines(bfReader, getHeaderCurrency(line), lang, response);
                } catch (InvalidLineException ilE) {
                    response.put("invalidHeader", true);
                }
            }
        }

        return response;
    }

    private void validateHeaderLine(String line) throws InvalidLineException {
        if (StringUtils.isBlank(line)) {
            throw new InvalidLineException(ErrorCode.EMPTY_HEADER);
        }
        String[] fields = line.split(",");
        if (fields.length != 2) {
            throw new InvalidLineException(ErrorCode.INVALID_HEADER_LENGTH);
        }

        validateFileIdentifier(fields[0]);
        validateCurrency(fields[1], null);
    }

    private Map<String, Object> validateLine(String line, String fileCurrency, List<String> allowedBanks) throws InvalidLineException {
        if (StringUtils.isBlank(line)) {
            throw new InvalidLineException(ErrorCode.EMPTY_LINE);
        }

        String[] fields = line.split(",");
        if (fields.length < 4 || fields.length > 5) {
            throw new InvalidLineException(ErrorCode.INVALID_LINE_LENGTH);
        }

        validateAccount(fields[0]);
        validateCurrency(fields[1], fileCurrency);
        validateAmount(fields[2]);
        validateBeneficiaryName(fields[3]);

        Map<String, Object> result = new HashMap<>();
        if(fields.length == 5){
            validateBankIdentifier(fields[4], allowedBanks);
            result.put("bankIdentifier", fields[4]);
        }        
     
        result.put("account", fields[0]);
        result.put("currency", fields[1]);
        result.put("amount", fields[2]);
        result.put("beneficiaryName", fields[3]);

        return result;
    }

    private void validateLines(BufferedReader bfReader, String fileCurrency, String lang, Map<String, Object> response)
            throws IOException {
        String line;
        int validLines = 0;
        int invalidLines = 0;
        double totalAmount = 0;
        double lineAmount;
        int linesWithEmptyAmount = 0;

        List<String> allowedBanks = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "massive.payments.allowed.banks");
        StringBuilder errorLinesContent = new StringBuilder();        
        while ((line = bfReader.readLine()) != null) {
            try {
                validateLine(line, fileCurrency, allowedBanks);
                validLines++;
                lineAmount = getAmount(line);
                totalAmount += lineAmount;
                if (lineAmount == 0) {
                    linesWithEmptyAmount++;
                }
            } catch (InvalidLineException ilE) {
                invalidLines++;
                line += " - " + I18nFactory.getHandler().getMessage("pay.multiline.error." + ilE.getMessage(), lang);
                errorLinesContent.append(line);
                errorLinesContent.append("\n");
            }
        }

        response.put(UploadFileActivity.OutParams.TOTAL_AMOUNT, new Amount(fileCurrency, totalAmount));
        response.put(UploadFileActivity.OutParams.INVALID_LINES, invalidLines);
        response.put(UploadFileActivity.OutParams.INVALID_LINES_CONTENT, errorLinesContent.toString().getBytes());
        response.put(UploadFileActivity.OutParams.VALID_LINES, validLines);
        response.put(UploadFileActivity.OutParams.LINES_WITH_NO_AMOUNT, linesWithEmptyAmount);
    }

    private String getHeaderFileIdentifier(String line) {
        String[] fields = line.split(",");
        return fields[0];
    }

    private String getHeaderCurrency(String line) {
        String[] fields = line.split(",");
        return fields[1];
    }

    private void validateFileIdentifier(String identifier) throws InvalidLineException {
        if (StringUtils.isBlank(identifier)) {
            throw new InvalidLineException(ErrorCode.INVALID_FILE_IDENTIFIER);
        }
    }

    private void validateAccount(String account) throws InvalidLineException {
        if (account.length() < 4 || account.length() > 12) {
            throw new InvalidLineException(ErrorCode.INVALID_ACCOUNT_LENGTH);
        }

        if (!StringUtils.isNumeric(account)) {
            throw new InvalidLineException(ErrorCode.INVALID_ACCOUNT_FORMAT);
        }
    }

    private void validateCurrency(String currency, String fileCurrency) throws InvalidLineException {
        if (StringUtils.isBlank(currency)) {
            throw new InvalidLineException(ErrorCode.EMPTY_CURRENCY);
        }
        if (StringUtils.isBlank(fileCurrency)) {
            if (!"USD".equals(currency) && !"EUR".equals(currency) && !"UYU".equals(currency)) {
                throw new InvalidLineException(ErrorCode.UNKNOWN_CURRENCY);
            }
        } else {
            if (!fileCurrency.equals(currency)) {
                throw new InvalidLineException(ErrorCode.INVALID_CURRENCY);
            }
        }
    }

    private void validateAmount(String amount) throws InvalidLineException {
        try {
            double quantity = Double.parseDouble(amount);
            if (quantity < 0) {
                throw new InvalidLineException(ErrorCode.NEGATIVE_AMOUNT);
            }
        } catch (Exception e) {
            throw new InvalidLineException(ErrorCode.INVALID_AMOUNT);
        }
    }

    private void validateBeneficiaryName(String name) throws InvalidLineException {
        if (StringUtils.isBlank(name)) {
            throw new InvalidLineException(ErrorCode.EMPTY_BENEFICIARY);
        }
    }
    
    private void validateBankIdentifier(String bankIdentifier, List<String> allowedBanks) throws InvalidLineException {
        if (!StringUtils.isBlank(bankIdentifier)
            && (allowedBanks.indexOf(bankIdentifier) == -1)) {
                throw new InvalidLineException(ErrorCode.INVALID_BANK_IDENTIFIER);
        }
    }

    private double getAmount(String line) {
        return Double.parseDouble(line.split(",")[2]);
    }

    @Override
    public byte[] getValidContent(byte[] content) throws IOException {
        List<String> allowedBanks = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "massive.payments.allowed.banks");
        try (InputStream stream = new ByteArrayInputStream(content)) {
            try (BufferedReader bfReader = new BufferedReader(new InputStreamReader(stream))) {
                String header = bfReader.readLine();
                StringBuilder validContent = new StringBuilder();

                try {
                    validateHeaderLine(header);
                    validContent.append(header);
                } catch (InvalidLineException ilE) {
                    // This catch is empty because we only need the valid lines
                }
                String line;
                while ((line = bfReader.readLine()) != null) {
                    try {
                        validateLine(line, getHeaderCurrency(header), allowedBanks);
                        validContent.append("\n").append(line);
                    } catch (InvalidLineException ilE) {
                        // This catch is empty because we only need the valid lines
                    }
                }
                return validContent.toString().getBytes();
            }
        }
    }
}
