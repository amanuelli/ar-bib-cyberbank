/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankConsolidateConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;

import java.util.ArrayList;
import java.util.List;

public class CoreProductConnectorOrchestrator extends CoreConnectorOrchestrator {

    public static List<Product> list(String idTransaction, List<Client> clients, List<String> typeList) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return (List<Product>) RubiconCoreConnectorC.listProducts(idTransaction, clients, typeList);
            case CYBERBANK_CONNECTOR:
                try {
                    List<Product> result = new ArrayList<>();
                    for (Client client: clients) {
                        result.addAll(CyberbankConsolidateConnector.products(client.getIdClient(), typeList).getData());
                    }
                    return result;
                } catch (CyberbankCoreConnectorException e) {
                    throw new BackendConnectorException(e);
                }
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

}
