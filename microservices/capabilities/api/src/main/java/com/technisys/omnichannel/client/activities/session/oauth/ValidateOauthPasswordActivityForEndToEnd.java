/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session.oauth;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.accessmanagement.FingerprintToken;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.attemptscounter.AttemptsCounterHandler;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.credentials.CredentialPluginFactory;
import com.technisys.omnichannel.core.domain.AttemptsCounter;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentStatus;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.exceptions.InvalidCredentialException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.session.SessionKind;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import com.technisys.omnichannel.utils.ReCaptcha;
import com.technisys.omnichannel.utils.UserResolverFromUsername;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.ReturnCodes.API_EXCHANGE_TOKEN_EXPIRED;
import static com.technisys.omnichannel.client.ReturnCodes.*;

/**
 * Endpoint to validate user credentials from Auth Server
 */
@AnonymousActivity
public class ValidateOauthPasswordActivityForEndToEnd extends Activity {

    public static final String ID = "oauth.password.validate";
    public static final Logger LOG = LoggerFactory.getLogger(ValidateOauthPasswordActivityForEndToEnd.class);

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Username who perform the request, e.g. &quot;bbeall&quot;, &quot;32525648&quot;")
        String USERNAME = "_usernameToValidate";

        @DocumentedParam(type = String.class, description = "Password value, e.g. &quot;Password1.&quot; Include the exchange token separated by '#'.")
        String PASSWORD = "_passwordToValidate";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "User id")
        String USER_ID = "userId";
        @DocumentedParam(type = String.class, description = "Kind of the session")
        String SESSION_KIND = "sessionKind";

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try{
            Response response = new Response(request);

            String clientId = request.getParam(InParams.USERNAME, String.class);
            String idUser = UserResolverFromUsername.getIdUser(clientId);

            if (idUser == null) {
                // Registers an invalid user under attempts, increases count and requests captcha if attempts number exceeds the limit
                String feature = AttemptsCounterHandler.getFeature(request.getIdActivity());
                AttemptsCounter attemptsCounter = AttemptsCounterHandler.incrementAttemptsCounter(feature, clientId);

                int maxAttemptsToBlock = AttemptsCounterHandler.getMaxAttemptsToBlock(feature);
                if (maxAttemptsToBlock > 0 && attemptsCounter.getAttempts() >= maxAttemptsToBlock ) {
                    throw new DispatchingException(ReturnCodes.USER_DISABLED);
                }
                boolean isMobile = IBRequest.Channel.phonegap.equals(request.getChannel());
                if (!isMobile && ReCaptcha.mustBeDisplayedForUser(feature, clientId)) {
                    throw new InvalidCredentialException(ReturnCodes.INVALID_PASSWORD_CREDENTIAL_CAPTCHA_REQUIRED, Credential.PWD_CREDENTIAL);
                }
                throw new InvalidCredentialException(ReturnCodes.INVALID_PASSWORD_CREDENTIAL, Credential.PWD_CREDENTIAL);

            } else {
                request.setIdUser(idUser);
                String[] credentialValues = request.getParam(InParams.PASSWORD, String.class).split("#");
                if (credentialValues.length < 2) {
                    throw new ActivityException(API_EXCHANGE_TOKEN_NOT_FOUND);
                }

                if ("fingerprint".equals(credentialValues[0])) {
                    String deviceTokenToValidate = credentialValues[1];
                    String idDevice = credentialValues[2];
                    String deviceModel = credentialValues[3];
                    FingerprintToken deviceToken = AccessManagementHandlerFactory.getHandler().readFingerprintToken(deviceTokenToValidate);
                    if (deviceToken == null
                            || !SecurityUtils.checkHash(deviceTokenToValidate, deviceToken.getId())
                            || !deviceToken.getDeviceId().equals(idDevice)
                            || !deviceToken.getDeviceModel().equals(deviceModel)
                    ) {
                        throw new ActivityException(API_INVALID_DEVICE_TOKEN);
                    }
                    response.putItem(OutParams.SESSION_KIND, SessionKind.FINGER);
                } else {
                    String password = credentialValues[0];
                    String exchangeToken = credentialValues[1];

                    if (credentialValues.length > 3) {
                        String captcha = credentialValues[3];
                        CredentialPlugin cpc = CredentialPluginFactory.getCredentialPlugin(Credential.CAPTCHA_CREDENTIAL);
                        cpc.validate(request, new Credential(Credential.CAPTCHA_CREDENTIAL, captcha));
                    }
                    CredentialPlugin cp = CredentialPluginFactory.getCredentialPlugin(Credential.PWD_CREDENTIAL);
                    cp.validate(request, new Credential(Credential.PWD_CREDENTIAL, password));

                    ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);
                    if (token == null) {
                        throw new ActivityException(API_EXCHANGE_TOKEN_NOT_FOUND, "The exchange token is not valid");
                    }
                    if (token.isExpired()) {
                        throw new ActivityException(API_EXCHANGE_TOKEN_EXPIRED, "The exchange token has expired");
                    }

                    //Checks if user has at least one active environment
                    List<Environment> environments = Administration.getInstance().listEnvironments(idUser);
                    if (environments.isEmpty()) {
                        throw new ActivityException(NO_ACTIVE_ENVIRONMENTS, "Invalid active environment " + request.getIdEnvironment() + " for user " + request.getIdUser());
                    } else {
                        //Returns error if user has only one environment and this one is blocked.
                        if(environments.size() == 1 && environments.get(0).getStatus().getIdEnvironmentStatus().equals(EnvironmentStatus.STATUS_BLOCKED)) {
                            throw new ActivityException(NO_ACTIVE_ENVIRONMENTS, "Invalid active environment " + request.getIdEnvironment() + " for user " + request.getIdUser());
                        }
                    }
                    new Thread(() -> {
                        try {
                            ExchangeTokenHandler.delete(exchangeToken);
                        } catch (IOException ioE) {
                            LOG.warn("Error deleting exchange token", ioE);
                        }
                    }).start();
                    response.putItem(OutParams.SESSION_KIND, SessionKind.NORMAL);
                }

                response.setReturnCode(OK);
                response.putItem(OutParams.USER_ID, idUser);
                return response;
            }
        } catch (DispatchingException e){
            throw new ActivityException(e.getReturnCode());
        }
        catch (IOException e) {
            throw new ActivityException(IO_ERROR, e);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        if (StringUtils.isBlank(request.getParam(InParams.USERNAME, String.class))) {
            result.put(InParams.USERNAME, "oauth.password.validate.username.required");
        }

        if(StringUtils.isBlank(request.getParam(InParams.PASSWORD, String.class))){
            result.put(InParams.PASSWORD, "oauth.password.validate.password.required");
        }

        return result;
    }
}