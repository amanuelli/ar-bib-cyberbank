/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author sbarbosa
 */
public class PermissionsUtils {

    private PermissionsUtils() { }

    private static Map<String, Boolean> permissionsForAccounts(String idUser, int currentEnvironment, boolean hasCreditCard) throws IOException {
        Map<String, Boolean> permissions = new HashMap<>();

        // Accounts module
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.ACCOUNT_FUND)) {
            permissions.put("accountFund", Boolean.TRUE);
        }

        // Transfers module
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_TRANSFER_INTERNAL)) {
            permissions.put("transferInternal", Boolean.TRUE);
        }
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_TRANSFER_THIRD_PARTIES)) {
            permissions.put("transferThirdParties", Boolean.TRUE);
        }
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_TRANSFER_LOCAL)) {
            permissions.put("transferLocal", Boolean.TRUE);
        }
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_TRANSFER_FOREIGN)) {
            permissions.put("transferForeign", Boolean.TRUE);
        }

        // Payments module
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_PAY_LOAN)) {
            permissions.put("payLoan", Boolean.TRUE);
        }
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_PAY_THIRD_PARTIES_LOAN)) {
            permissions.put("payThirdPartiesLoan", Boolean.TRUE);
        }

        if (hasCreditCard && Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_PAY_CREDIT_CARD)) {
            permissions.put("payCreditCard", Boolean.TRUE);
        }
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_PAY_THIRD_PARTIES_CREDIT_CARD)) {
            permissions.put("payThirdPartiesCreditCard", Boolean.TRUE);
        }
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_MULTILINE_SALARY_PAYMENT)) {
            permissions.put("salaryPayment", Boolean.TRUE);
        }

        return permissions;
    }

    /**
     * Retorna la lista de funcionalidades del munú que el usuario tiene
     * permisos
     *
     * @param idUser             Id del usaurio
     * @param currentEnvironment Ambiente actual
     *                           correspondiente
     * @return Mapa con la lista de permisos del menú para ese usuario en ese
     * ambiente
     * @throws java.io.IOException Error de conección con la base de datos
     */
    public static Map<String, Boolean> getPermissions(String idUser, int currentEnvironment) throws IOException {
        Map<String, Boolean> permissions = new HashMap<>();

        //accounts|loans|creditcards|payments|foreigntrade|others
        // Accounts module
        List<String> authorizedProductTypes = Administration.getInstance().listAuthorizedProductTypes(idUser, currentEnvironment, new String[]{"product.read"});

        boolean hasAccount = false;
        boolean hasCreditCard = false;
        if (authorizedProductTypes.contains(Constants.PRODUCT_CA_KEY) || authorizedProductTypes.contains(Constants.PRODUCT_CC_KEY)) {
            permissions.put("accounts", Boolean.TRUE);
            hasAccount = true;
        }
        if (authorizedProductTypes.contains(Constants.PRODUCT_PA_KEY) || authorizedProductTypes.contains(Constants.PRODUCT_PI_KEY)) {
            permissions.put("loans", Boolean.TRUE);
        }
        if (authorizedProductTypes.contains(Constants.PRODUCT_TC_KEY)) {
            permissions.put("creditcards", Boolean.TRUE);
            hasCreditCard = true;
        }

        if (hasAccount) {
            permissions.putAll(permissionsForAccounts(idUser, currentEnvironment, hasCreditCard));
        }
        // Others
        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_REQUEST_TRANSACTION_CANCELLATION)) {
            permissions.put("requestTransactionCancellation", Boolean.TRUE);
        }

        if (Authorization.hasPermissionOverForm(idUser, currentEnvironment, Constants.FORM_REQUEST_LOAN)) {
            permissions.put("requestLoan", Boolean.TRUE);
        }

        return permissions;
    }

    /**
     * Lista los siguientes datos:
     * <ul>
     * <li>identificador</li>
     * <li>categoria</li>
     * <li>nombre (internacionalizado)</li>
     * <li>versi&oacute;n</li>
     * </ul>
     * de los formularios que el usuario tiene permisos.
     *
     * @param userPermissions Permisos del usuario en el ambiente
     *                        correspondiente
     * @param lang            Idioma para los valores internacionalizados
     * @return Lista de formularios para ese usuario en ese ambiente
     * @throws java.io.IOException Error de conección con la base de datos
     */
    public static List<Map<String, Object>> getFormList(List<String> userPermissions, String lang) throws IOException {
        List<Map<String, Object>> result = new ArrayList<>();

        List<String> languages = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "core.languages");

        List<Map<String, Object>> forms = FormsHandler.getInstance().listShrinkedForms(1, Form.FORM_TYPE_PROCESS, lang);
        // Esta forma de verificar permisos funciona siempre y cuando sean de proceso.
        for (Map<String, Object> form : forms) {
            if (userPermissions.contains(Form.getFormIdPermission((String) form.get("idForm")))) {
                Map<String, String> formNames = new HashMap<>();
                // Esto es un parche porque en Oracle se devuelve un BigDecimal... creo que por el tipo de datos lo correcto sería devolver siempre un LONG, incluso usar una bean ShrinkedForm en lugar de un mapa
                int formVersion = form.get("version") instanceof BigDecimal ? ((BigDecimal) form.get("version")).intValue() : (int) form.get("version");

                for (String supportedLang : languages) {
                    String formName = FormMessagesHandler.getInstance().readFormMessage(form.get("idForm").toString(), formVersion, "formName", supportedLang);

                    formNames.put(supportedLang, formName);
                }

                form.put("name", formNames);

                result.add(form);
            }
        }

        return result;
    }
}
