/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client;

import com.technisys.omnichannel.client.activities.recoverpassword.RecoverPasswordStep1Activity;
import com.technisys.omnichannel.client.activities.session.legacy.LoginWithSecondFactorAndPasswordStep1Activity;
import com.technisys.omnichannel.client.activities.session.legacy.LoginWithSecondFactorAndPasswordStep2Activity;

import java.util.List;

/**
 * @author Diego Curbelo
 */
public class Constants extends com.technisys.omnichannel.Constants {

    public static final String TECHNISYS_BANK_ID = "TECH";

    public static final String TRACE_KEY = "uber-trace-id";

    public static final String DOCUMENT_FRONT = "DOCUMENT_FRONT";
    public static final String DOCUMENT_BACK = "DOCUMENT_BACK";
    public static final String DOCUMENT_SELFIE = "SELFIE";


    /* Codigos de producto predefinidos */
    public static final String PRODUCT_CA_KEY = "CA";
    public static final String PRODUCT_CC_KEY = "CC";
    public static final String PRODUCT_PF_KEY = "PF";
    public static final String PRODUCT_PI_KEY = "PI";
    public static final String PRODUCT_PA_KEY = "PA";
    public static final String PRODUCT_TC_KEY = "TC";

    public static final String BASE_URL_KEY = "client.baseURL";

    public static final String ATTEMPTS_INVITE_FEATURE = "invite";
    public static final String ATTEMPTS_DIGITAL_ENROLLMENT = "digitalEnrollment";
    public static final String ATTEMPTS_ASSOCIATE_ENROLLMENT = "associateEnrollment";

    public static final String TRANSPORT_MAIL = "MAIL";
    public static final String TRANSPORT_SMS = "SMS";

    public static final String INVITATION_ROLE_ADMINISTRATOR = "administrator";

    /* Formularios */
    public static final String ACCOUNT_FUND = "accountFund";
    public static final String FORM_TRANSFER_INTERNAL = "transferInternal";
    public static final String FORM_TRANSFER_THIRD_PARTIES = "transferThirdParties";
    public static final String FORM_TRANSFER_LOCAL = "transferLocal";
    public static final String FORM_TRANSFER_FOREIGN = "transferForeign";
    public static final String FORM_PAY_LOAN = "payLoan";
    public static final String FORM_PAY_THIRD_PARTIES_LOAN = "payThirdPartiesLoan";
    public static final String FORM_PAY_CREDIT_CARD = "payCreditCard";
    public static final String FORM_PAY_THIRD_PARTIES_CREDIT_CARD = "payThirdPartiesCreditCard";
    public static final String FORM_REQUEST_CHECKBOOK = "requestCheckbook";
    public static final String FORM_REQUEST_TRANSACTION_CANCELLATION = "requestTransactionCancellation";
    public static final String FORM_MULTILINE_SALARY_PAYMENT = "salaryPayment";
    public static final String FORM_REQUEST_LOAN = "requestLoan";

    /* Codigos de Permisos  */
    public static final String PRODUCT_READ_PERMISSION = "product.read";
    public static final String ADMINISTRATION_VIEW_PERMISSION = "administration.view";
    public static final String ADMINISTRATION_MANAGE_PERMISSION = "administration.manage";

    public static final String DEFAULT_GROUP_PERMISSIONS_KEY = "client.permissions.defaults";

    public static final String POSITION_PERMISSION = "position";
    public static final String REQUEST_CHECKBOOK_PERMISSION = "accounts.requestCheckbook";
    public static final String TRANSFER_FOREIGN_PERMISSION = "transfer.foreign";
    public static final String TRANSFER_INTERNAL_PERMISSION = "transfer.internal";
    public static final String TRANSFER_LOCAL_PERMISSION = "transfer.local";
    public static final String TRANSFER_THIRD_PARTIES_PERMISSION = "transfer.thirdParties";
    public static final String TRANSFERS_PERMISSION = "accounts.transfer";
    public static final String FREQUENT_DESTINATIONS_MANAGE_PERMISSION = "frequentDestinations.manage";


    /**
     * Lista de actividades (dentro de getStartActivities()) que
     * requiere que se incremente el captcha attempts si no existe el usuario
     *
     * @return Lista de identificadores de actividad
     */
    @Override
    public List<String> getIncrementCaptchaActivities() {
        List<String> result = super.getIncrementCaptchaActivities();
        // Session
        result.add(LoginWithSecondFactorAndPasswordStep1Activity.ID);
        result.add(LoginWithSecondFactorAndPasswordStep2Activity.ID);
        result.add(RecoverPasswordStep1Activity.ID);

        return result;
    }
}
