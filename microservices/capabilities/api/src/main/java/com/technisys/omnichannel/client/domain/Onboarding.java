package com.technisys.omnichannel.client.domain;

import java.io.Serializable;
import java.util.Date;

/**
 *
 * @author fpena
 */
public class Onboarding implements Serializable {

    public static final String STATUS_STARTED = "STARTED";
    public static final String STATUS_EMAIL_VERIFICATION_DONE = "EMAIL_VERIFICATION_DONE";
    public static final String STATUS_ID_PARTIALLY_LOADED = "ID_PARTIALLY_LOADED";
    public static final String STATUS_ID_COMPLETELY_LOADED = "ID_COMPLETELY_LOADED";
    public static final String STATUS_ID_VERIFICATION_DONE = "ID_VERIFICATION_DONE";
    public static final String STATUS_ID_VERIFICATION_FAILED = "ID_VERIFICATION_FAILED";
    public static final String STATUS_CREDIT_SCORE_BELLOW_REQUESTED = "CREDIT_SCORE_BELLOW_REQUESTED";
    public static final String STATUS_CREDIT_SCORE_FAILED = "CREDIT_SCORE_FAILED";
    public static final String STATUS_ACCOUNT_EXISTS = "ACCOUNT_EXISTS";
    public static final String STATUS_ENROLLMENT_PENDING = "ENROLLMENT_PENDING";
    public static final String STATUS_FINISHED = "FINISHED";

    public static final String ONBOARDING_DEFAULT = "ONBOARDING";
    public static final String ONBOARDING_CREDITCARD = "CREDITCARD";

    private long idOnboarding;
    private Date creationDate;
    private Date modificationDate;
    private String status;
    private Integer idInvitationCode;
    private String extraInfo;
    private String documentCountry;
    private String documentType;
    private String documentNumber;
    private String mobileNumber;
    private String email;
    private String type;

    public Onboarding() {

    }

    public Onboarding(Onboarding onboarding) {
        this.idOnboarding = onboarding.idOnboarding;
        this.creationDate = onboarding.creationDate;
        this.modificationDate = onboarding.modificationDate;
        this.status = onboarding.status;
        this.idInvitationCode = onboarding.idInvitationCode;
        this.extraInfo = onboarding.extraInfo;
        this.documentCountry = onboarding.documentCountry;
        this.documentType = onboarding.documentType;
        this.documentNumber = onboarding.documentNumber;
        this.mobileNumber = onboarding.mobileNumber;
        this.email = onboarding.email;
        this.type = onboarding.type;
    }

    public long getId() {
        return getIdOnboarding();
    }

    public long getIdOnboarding() {
        return idOnboarding;
    }

    public void setIdOnboarding(long idOnboarding) {
        this.idOnboarding = idOnboarding;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Integer getIdInvitationCode() {
        return idInvitationCode;
    }

    public void setIdInvitationCode(Integer idInvitationCode) {
        this.idInvitationCode = idInvitationCode;
    }

    public String getExtraInfo() {
        return extraInfo;
    }

    public void setExtraInfo(String extraInfo) {
        this.extraInfo = extraInfo;
    }

    public String getDocumentCountry() {
        return documentCountry;
    }

    public void setDocumentCountry(String documentCountry) {
        this.documentCountry = documentCountry;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }

    public String getDocumentNumber() {
        return documentNumber;
    }

    public void setDocumentNumber(String documentNumber) {
        this.documentNumber = documentNumber;
    }

    public String getMobileNumber() {
        return mobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


    public static final class OnboardingBuilder {
        private long idOnboarding;
        private Date creationDate;
        private Date modificationDate;
        private String status;
        private Integer idInvitationCode;
        private String extraInfo;
        private String documentCountry;
        private String documentType;
        private String documentNumber;
        private String mobileNumber;
        private String email;
        private String type;


        public static OnboardingBuilder anOnboarding() {
            return new OnboardingBuilder();
        }

        public OnboardingBuilder setIdOnboarding(long idOnboarding) {
            this.idOnboarding = idOnboarding;
            return this;
        }

        public OnboardingBuilder setCreationDate(Date creationDate) {
            this.creationDate = creationDate;
            return this;
        }

        public OnboardingBuilder setModificationDate(Date modificationDate) {
            this.modificationDate = modificationDate;
            return this;
        }

        public OnboardingBuilder setStatus(String status) {
            this.status = status;
            return this;
        }

        public OnboardingBuilder setIdInvitationCode(Integer idInvitationCode) {
            this.idInvitationCode = idInvitationCode;
            return this;
        }

        public OnboardingBuilder setExtraInfo(String extraInfo) {
            this.extraInfo = extraInfo;
            return this;
        }

        public OnboardingBuilder setDocumentCountry(String documentCountry) {
            this.documentCountry = documentCountry;
            return this;
        }

        public OnboardingBuilder setDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public OnboardingBuilder setDocumentNumber(String documentNumber) {
            this.documentNumber = documentNumber;
            return this;
        }

        public OnboardingBuilder setMobileNumber(String mobileNumber) {
            this.mobileNumber = mobileNumber;
            return this;
        }

        public OnboardingBuilder setEmail(String email) {
            this.email = email;
            return this;
        }

        public OnboardingBuilder setType(String type) {
            this.type = type;
            return this;
        }

        public Onboarding build() {
            Onboarding onboarding = new Onboarding();
            onboarding.setIdOnboarding(idOnboarding);
            onboarding.setCreationDate(creationDate);
            onboarding.setModificationDate(modificationDate);
            onboarding.setStatus(status);
            onboarding.setIdInvitationCode(idInvitationCode);
            onboarding.setExtraInfo(extraInfo);
            onboarding.setDocumentCountry(documentCountry);
            onboarding.setDocumentType(documentType);
            onboarding.setDocumentNumber(documentNumber);
            onboarding.setMobileNumber(mobileNumber);
            onboarding.setEmail(email);
            onboarding.setType(type);
            return onboarding;
        }
    }
}
