/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.i18n.I18nFactory;

/**
 *
 * @author salva
 */
public class StatementLine {
    
    private String idProduct;
    private int month;
    private int year;
    private String idStatementLine;
    private String name;
    private String filename;
    private byte[] content;

    public StatementLine() {
    }

    public StatementLine(String idProduct, int month, int year, String lang, String idStatementLine) {
        this.idProduct = idProduct;
        this.month = month;
        this.year = year;
        this.idStatementLine = idStatementLine;
        
        String monthName = I18nFactory.getHandler().getMessage("core.months." + this.month, lang);
        this.name = monthName + " " + year;
    }
    
    public StatementLine(String idProduct, int month, int year, String lang, String filename, String idStatementLine) {
        this(idProduct, month, year, lang, idStatementLine);
        this.filename = filename;
    }
    
    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getIdStatementLine() {
        return idStatementLine;
    }

    public void setIdStatementLine(String idStatementLine) {
        this.idStatementLine = idStatementLine;
    }
    
}
