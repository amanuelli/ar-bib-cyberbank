/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.desktop;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.campaigns.CampaignsHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.desktop.LayoutHandler;
import com.technisys.omnichannel.core.domain.Campaign;
import com.technisys.omnichannel.core.domain.CampaignContextParameters;
import com.technisys.omnichannel.core.domain.Widget;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

/**
 * Returns list of widgets for a specific user and environment.
 */
@DocumentedActivity("Desktop load layout")
public class LoadLayoutActivity extends Activity {

    public static final String ID = "desktop.loadLayout";

    private static final String CAMPAIGNS_WIDGET_ID = "campaigns";
    private static final String CAMPAIGNS_WIDGET_SECTION = "desktop-widget";

    public interface InParams {
    }

    public interface OutParams {

        @DocumentedParam(type = List.class, description = "List of widgets")
        String WIDGETS = "widgets";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            List<Widget> finalWidgets = new ArrayList<>();
            List<Widget> widgets = LayoutHandler.readLayout(request.getIdUser(), request.getIdEnvironment());
            for (Widget widget : widgets) {
                if (CAMPAIGNS_WIDGET_ID.equals(widget.getId())) {
                    CampaignContextParameters contextParameters = new CampaignContextParameters();
                    contextParameters.setChannel(request.getChannel());
                    contextParameters.setIdUser(request.getIdUser());
                    contextParameters.setIdEnvironment(request.getIdEnvironment());
                    
                    InetAddress inetAddress = InetAddress.getByName(request.getClientIP());
                    // En desarrollo, la IP es 127.0.0.1 o 0:0:0:0:0:0:0:1 y en testing los IPs son de la red local;
                    // las cuales no estan la DB. Por tanto si estamos se utiliza la IP publica del lab de montevideo
                    if (inetAddress.isSiteLocalAddress() || inetAddress.isLoopbackAddress()) {
                        String clientIp = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "cyberbank.localAddressTesting");
                        contextParameters.setIpAddress(clientIp);
                    } else {
                        contextParameters.setIpAddress(request.getClientIP());
                    }
                    Campaign campaign = CampaignsHandler.getInstance().readRandomCampaignBySection(request.getIdUser(), request.getIdEnvironment(), CAMPAIGNS_WIDGET_SECTION, contextParameters);
                    if (campaign != null) {
                        finalWidgets.add(widget);
                    }
                } else {
                    finalWidgets.add(widget);
                }
            }
            response.putItem(OutParams.WIDGETS, finalWidgets);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
}
