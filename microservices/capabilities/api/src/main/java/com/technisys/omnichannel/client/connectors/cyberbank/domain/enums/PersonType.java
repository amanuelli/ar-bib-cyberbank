package com.technisys.omnichannel.client.connectors.cyberbank.domain.enums;

public enum PersonType {
    INDIVIDUAL("INDIVIDUAL"),
    LEGAL_ENTITY("LEGAL_ENTITY");

    private final String value;

    private PersonType(String value) {
        this.value = value;
    }

    public String getValueForService() {
        return value;
    }

}

