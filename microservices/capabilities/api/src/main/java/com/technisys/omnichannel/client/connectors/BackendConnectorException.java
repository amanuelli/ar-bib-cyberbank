/*
 *  Copyright 2014 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

/**
 *
 * @author Diego Curbelo
 */
public class BackendConnectorException extends Exception {

    private int responseCode;

    public BackendConnectorException(String message) {
        super(message);
    }

    public BackendConnectorException(int responseCode) {
        super();

        this.responseCode = responseCode;
    }

    public BackendConnectorException(int responseCode, Throwable cause) {
        super(cause);

        this.responseCode = responseCode;
    }

    public BackendConnectorException(String message, int responseCode) {
        super(message);

        this.responseCode = responseCode;
    }

    public BackendConnectorException(String message, int responseCode, Throwable cause) {
        super(message, cause);

        this.responseCode = responseCode;
    }

    public BackendConnectorException(Throwable cause) {
        super(cause);

        this.responseCode = -1;
    }

    public int getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}