/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.widgets;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreExchangeRateConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.CurrencyExchange;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.List;

/**
 *
 */
public class ExchangeRatesActivity extends Activity {

    public static final String ID = "widgets.exchangeRates";

    public interface InParams {
    }

    public interface OutParams {

        String RATES = "rates";
        String MIDDLE_CURRENCY_RATES_FLOOR = "middleCurrencyRatesFloor";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            if (environment == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }
            List<CurrencyExchange> rates = CoreExchangeRateConnectorOrchestrator.listExchangeRates(request.getIdTransaction(), environment.getClients().get(0).getIdClient(), request.getIdUser());
            //sumamos esta variable para resolver el problema que había cuando se mostraban
            double middleRates = (rates.size() - 1) / 2;
            response.putItem(OutParams.RATES, rates);
            response.putItem(OutParams.MIDDLE_CURRENCY_RATES_FLOOR, middleRates);
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
}
