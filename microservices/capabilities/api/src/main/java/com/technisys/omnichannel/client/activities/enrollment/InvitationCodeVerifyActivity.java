/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.handlers.enrollment.EnrollmentHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.ReturnCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
@AnonymousActivity @DocumentedActivity("Verify invitation code")
public class InvitationCodeVerifyActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(InvitationCodeVerifyActivity.class);

    public static final String ID = "enrollment.invitationCode.verify";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Invitation code")
        String CODE = "_code";
    }

    public interface OutParams {

        @DocumentedParam(type = Boolean.class, description = "Action associate")
        String ASSOCIATE = "associate";
        @DocumentedParam(type = String.class, description = "Exchange token")
        String EXCHANGE_TOKEN = "_exchangeToken";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            String code = request.getParam(InParams.CODE, String.class);
            InvitationCode invitation = InvitationCodesHandler.readInvitationCode(code);
            ReturnCode error = EnrollmentHandler.validateInvitationCode(invitation);
            if (error == null) {
                try {
                    // Chequeo si es un asociar ambiente o si es un enrollment
                    User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber());
                    boolean associate = user != null;
                    response.putItem(OutParams.ASSOCIATE, associate);

                    if (user == null) {
                        // Envío el código de verificación
                        EnrollmentHandler.sendVerificationCode(invitation, request.getLang());
                    }

                    String[] nextActivities = associate ? new String[] { AssociatePreActivity.ID } : new String[] { WizardVerificationCodeActivity.ID, WizardResendVerificationCodeActivity.ID, WizardPreActivity.ID, ListSecuritySealsActivity.ID };
                    String exchangeToken = ExchangeTokenHandler.create(request.getChannel(), request.getLang(), request.getIdUser(), -1, request.getUserAgent(), request.getClientIP(), nextActivities);

                    response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
                } catch (MessagingException ex) {
                    log.error("No fue posible enviar el código de verificación para el enrollment", ex);
                }
            }
            response.setReturnCode(error == null ? ReturnCodes.OK : error);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        String code = request.getParam(InParams.CODE, String.class);
        if (StringUtils.isBlank(code)) {
            result.put(InParams.CODE, "enrollment.index.invitationCode.empty");
        } else {
            try {
                String invitationCodeUnmaskedLength = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "backoffice.invitationCodes.unmaskedLength");
                Pattern pattern = Pattern.compile("^([a-zA-Z\\d]{" + invitationCodeUnmaskedLength + "}(-[a-zA-Z\\d]{" + invitationCodeUnmaskedLength + "}){2})$");
                Matcher matcher = pattern.matcher(code);

                if (!matcher.find()) {
                    result.put(InParams.CODE, "enrollment.index.invitationCode.invalidFormat");
                }
            } catch (IOException ex) {
                throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, ex);
            }
        }

        return result;
    }

}
