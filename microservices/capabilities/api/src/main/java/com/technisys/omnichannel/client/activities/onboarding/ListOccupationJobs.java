/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.onboarding;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreJobInformationConnectorOrchestator;
import com.technisys.omnichannel.client.domain.OccupationJob;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.List;

import static com.technisys.omnichannel.ReturnCodes.OK;
import static com.technisys.omnichannel.client.ReturnCodes.BACKEND_SERVICE_ERROR;

/**
 * @author cmeneses
 */
@AnonymousActivity
@DocumentedActivity("List Onboarding Occupations")
public class ListOccupationJobs extends Activity {

    public static final String ID = "onboarding.occupations.list";

    public interface InParams {

    }

    public interface  OutParams {

        @DocumentedParam(type = OccupationJob[].class, description = "List of Occupations")
        public static final String OCCUPATIONS_LIST = "occupations";

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            List<OccupationJob> occupationJobsList = CoreJobInformationConnectorOrchestator.listOccupationJobs();
            response.putItem(OutParams.OCCUPATIONS_LIST, occupationJobsList);
            response.setReturnCode(OK);
        } catch (BackendConnectorException e) {
            throw new ActivityException(BACKEND_SERVICE_ERROR, e);
        }
        return response;
    }



}
