/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.onboarding;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.Province;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAddresInformationConnectorOrchestator;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.List;


import static com.technisys.omnichannel.client.ReturnCodes.*;

/**
 * @author cmeneses
 */
@AnonymousActivity
@DocumentedActivity("Activity to list the states in the onboarding flow")
public class ListStateActivity extends Activity {

    public static final String ID = "onboarding.states.list";

    public interface InParams {

    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "List of the states")
        String STATE_LIST = "states";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            List<Province> states = CoreAddresInformationConnectorOrchestator.listProvince(null);
            response.putItem(OutParams.STATE_LIST, states);
            response.setReturnCode(OK);
        } catch (BackendConnectorException e) {
            throw new ActivityException(BACKEND_SERVICE_ERROR, e);
        }
        return response;
    }

}