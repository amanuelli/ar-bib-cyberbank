/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class ModifySocialNetworksConfigurationActivity extends Activity {

    public static final String ID = "preferences.socialnetworks.configuration.modify";

    public interface InParams {

        String FACEBOOK_ID = "facebookId";
        String FACEBOOK_NAME = "facebookName";
        String TWITTER_ID = "twitterId";
        String TWITTER_NAME = "twitterName";
    }

    public interface OutParams {

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            String facebookId = request.getParam(ModifySocialNetworksConfigurationActivity.InParams.FACEBOOK_ID, String.class);
            String facebookName = request.getParam(ModifySocialNetworksConfigurationActivity.InParams.FACEBOOK_NAME, String.class);
            String twitterId = request.getParam(ModifySocialNetworksConfigurationActivity.InParams.TWITTER_ID, String.class);
            String twitterName = request.getParam(ModifySocialNetworksConfigurationActivity.InParams.TWITTER_NAME, String.class);
            AccessManagementHandlerFactory.getHandler().updateUserSocialNetworkData(request.getIdUser(), facebookId, facebookName, twitterId, twitterName);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields();

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    public Map<String, String> validateFields() throws ActivityException {
        Map<String, String> result = new HashMap<>();
        return result;
    }
}
