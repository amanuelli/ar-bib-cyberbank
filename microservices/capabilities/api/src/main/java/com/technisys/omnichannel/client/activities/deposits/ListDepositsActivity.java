/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.deposits;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.Deposit;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 *
 */
public class ListDepositsActivity extends Activity {

    public static final String ID = "deposits.list";

    public interface InParams {
    }

    public interface OutParams {

        String DEPOSITS = "deposits";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            List<Deposit> deposits = getDepositList(request, Arrays.asList(Constants.PRODUCT_PF_KEY));

            response.putItem(OutParams.DEPOSITS, deposits);

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    public static List<Deposit> getDepositList(Request request, List<String> productTypes) throws IOException, BackendConnectorException {
        Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

        List<String> idPermissions = new ArrayList<>();
        idPermissions.add(PRODUCT_READ_PERMISSION);

        List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);

        List<Deposit> deposits = (List<Deposit>) RubiconCoreConnectorC.listProducts(request.getIdTransaction(), environment.getClients(), Arrays.asList(new String[]{Constants.PRODUCT_PF_KEY}));

        List<Deposit> authorizedDeposits = new ArrayList<>();

        ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

        for (Deposit deposit : deposits) {
            for (Product environmentProduct : environmentProducts) {
                if (deposit.getExtraInfo().equals(environmentProduct.getExtraInfo())) {
                    deposit.setProductAlias(environmentProduct.getProductAlias());
                    deposit.setShortLabel(pLabeler.calculateShortLabel(deposit));
                    deposit.setLabel(pLabeler.calculateLabel(deposit));
                    deposit.setIdProduct(environmentProduct.getIdProduct());
                    authorizedDeposits.add(deposit);
                    break;
                }
            }
        }

        return authorizedDeposits;
    }

}
