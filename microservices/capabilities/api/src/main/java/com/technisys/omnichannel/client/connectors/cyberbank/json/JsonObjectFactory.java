package com.technisys.omnichannel.client.connectors.cyberbank.json;

public class JsonObjectFactory {

    private JsonObjectFactory() {
    }

    public static JsonObject newJsonObject() {
        return new JsonObject();
    }

    public static JsonArray newJsonArray() {
        return new JsonArray();
    }

}
