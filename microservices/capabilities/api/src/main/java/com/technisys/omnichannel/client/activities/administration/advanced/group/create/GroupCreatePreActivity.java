package com.technisys.omnichannel.client.activities.administration.advanced.group.create;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.common.Permissions;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.List;

@DocumentedActivity("Preload data to be used by the administrator on group form")
public class GroupCreatePreActivity extends Activity {

    public static final String ID = "administration.advanced.group.create.pre";

    public interface InParams {

    }

    public interface OutParams {

        @DocumentedParam(type = List.class, description = "Environment users to be added in the group")
        String AVAILABLE_USERS = "availableUsers";
        @DocumentedParam(type = List.class, description = "Helps to display which user is admin")
        String ADMIN_USERS = "adminUsers";
        @DocumentedParam(type = List.class, description = "Permissions categories to build layout")
        String GROUPS = "groups";
        @DocumentedParam(type = List.class, description = "Environment products")
        String PRODUCTS = "products";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (!Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            int idEnvironment = request.getIdEnvironment();
            String lang = request.getLang();
            List<User> availableUsers = AccessManagementHandlerFactory.getHandler().getUsers(request.getIdEnvironment(), -1, -1, "first_name ASC, last_name ASC").getElementList();

            response.putItem(OutParams.GROUPS, Permissions.listUIPermissions(idEnvironment, lang));
            response.putItem(OutParams.PRODUCTS, Permissions.listProducts(idEnvironment, lang));
            response.putItem(OutParams.AVAILABLE_USERS, availableUsers);
            response.putItem(OutParams.ADMIN_USERS, Permissions.filterAdminUsers(idEnvironment, availableUsers));

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
