package com.technisys.omnichannel.client.forms.fields.multilinefile.processors;

import com.technisys.omnichannel.client.domain.TransactionLine;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public interface Processor {
    List<TransactionLine> process(byte[] content, String idTransaction, String reference) throws IOException, InvalidTotalAmountException;

    Map<String, Object> validate(byte[] content, String lang) throws IOException;

    byte[] getValidContent(byte[] content) throws IOException;
}
