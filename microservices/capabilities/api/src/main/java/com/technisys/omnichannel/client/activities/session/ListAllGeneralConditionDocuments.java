package com.technisys.omnichannel.client.activities.session;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.GeneralConditionDocument;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AnonymousActivity
@DocumentedActivity("List all general condition documents")
public class ListAllGeneralConditionDocuments extends Activity {

    public static final String ID = "session.listAllGeneralConditionDocuments";

    public interface OutParams {

        @DocumentedParam(type = List.class, description = "Flag to indicate if invitation feature is enabled")
        String DOCUMENTS = "documents";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Response response = new Response(request);
            int actualCondition =  GeneralConditions.getInstance().actualCondition();

            List<GeneralConditionDocument> generalConditionDocuments = GeneralConditions.getInstance().listAllGeneralConditionDocuments(actualCondition);

            List<Map<String, String>> resultData = new ArrayList<>();
            Map<String, String> data;
            for(GeneralConditionDocument generalConditionDocument : generalConditionDocuments) {
                data = new HashMap<>();
                data.put("fileNameKey", generalConditionDocument.getFileNameKey());
                data.put("size", String.valueOf(generalConditionDocument.getFileSizeInKB()));
                resultData.add(data);
            }

            response.putItem(OutParams.DOCUMENTS, resultData);
            response.setReturnCode(ReturnCodes.OK);
            return response;

        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

}
