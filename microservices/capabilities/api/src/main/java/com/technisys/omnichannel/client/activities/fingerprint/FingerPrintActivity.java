/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.fingerprint;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.session.legacy.SelectEnvironmentActivity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.utils.CalendarRestrictionsUtils;
import com.technisys.omnichannel.client.utils.SynchronizationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.GeneralCondition;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.preprocessors.ipauthorization.IPAuthorizationHandler;
import com.technisys.omnichannel.core.session.SessionKind;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.session.SessionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Hacemos casi lo mismo que en
 * {@link SelectEnvironmentActivity#execute(Request) SelectEnvironmentActivity.execute(Request)}
 * pero utiliazando un accesToken en lugar del eschangeToken.
 *
 * @see
 * SelectEnvironmentActivity
 */
@DocumentedActivity("Fingerprint Login")
public class FingerPrintActivity extends Activity {
    
    public static final String ID = "login.fingerprint";

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "General conditions")
        String GENERAL_CONDITIONS = "generalConditions";

        @DocumentedParam(type = String.class, description = "General conditions text")
        String GENERAL_CONDITION_TEXT = "generalConditionsText";

        @DocumentedParam(type = String.class, description = "General conditions expiration date")
        String GENERAL_CONDITION_EXPIRATION_DATE = "generalConditionsExpirationDate";

        @DocumentedParam(type = String.class, description = "General conditions show expiration")
        String GENERAL_CONDITIONS_SHOW_EXPIRATION = "generalConditionsShowExpiration";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            // TODO Ver de usar el mismo código que está en SelectEnvironmentActivity, refactorizando lo que tengan en común.

            if (SessionKind.FINGER.equals(request.getSessionKind())) {
                Environment activeEnvironment = null;

                Map<Integer, Map<String, String>> environments;
                List<Environment> environmentList = Administration.getInstance().listEnvironmentsWhereUserIsActive(request.getIdUser(), request.getChannel());

                boolean calendarIsValid;
                boolean ipsAreValid;
                for (Environment e : environmentList) {
                    calendarIsValid = CalendarRestrictionsUtils.validateAccessByEnvironmentAndUser(e.getIdEnvironment(),request.getIdUser(), request.getValueDate());
                    ipsAreValid = IPAuthorizationHandler.getInstance().checkUserEnvironmentAccess(e.getIdEnvironment(), request.getIdUser(), request.getClientIP());
                    e.setAllowToAccess(calendarIsValid && ipsAreValid);

                    if (e.getIdEnvironment() == request.getIdEnvironment()) {
                        activeEnvironment = e;
                        break;
                    }
                }

                // no debería darse nunca el caso de que activeEnvironment sea null
                // pero se agrega el if por sugerencia de Sonar para evitar NullPointerException
                if (activeEnvironment == null) {
                    throw new ActivityException(ReturnCodes.API_FINGER_SESSION_ENDED);
                } else {
                    environments = SessionUtils.assembleEnviromentMap(environmentList);

                    Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

                    SynchronizationUtils.syncEnvironmentProducts(request.getIdTransaction(), environment, activeEnvironment.getLastSynchronization());

                    if (!StringUtils.isEmpty(environment.getProductGroupId())) {
                        SynchronizationUtils.syncUserData(request.getIdTransaction(), request.getIdUser(), environment.getProductGroupId());
                    }

                    Integer idEnvironmentToChange = request.getIdEnvironment();
                    User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());

                    AccessManagementHandlerFactory.getHandler().updateUserDefaultEnvironment(request.getIdUser(), idEnvironmentToChange);

                    GeneralConditions gcHandler = GeneralConditions.getInstance();

                    int actualCondition = gcHandler.actualCondition();

                    EnvironmentUser envUser = Administration.getInstance().readEnvironmentUserInfo(request.getIdUser(), activeEnvironment.getIdEnvironment());

                    Response response;

                    if (envUser.getIdCondition() == actualCondition || actualCondition == 0) {
                        request.setIdEnvironment(activeEnvironment.getIdEnvironment());
                        response = SelectEnvironmentActivity.generateLastLoginStepResponse(request.getCredential("accessToken").getValue(), request, user, activeEnvironment, environments);

                        response.setReturnCode(ReturnCodes.OK);
                    } else {
                        response = new Response(request);
                        GeneralCondition generalCondition = gcHandler.readGeneralConditionInfo(actualCondition);
                        if (envUser.getIdCondition() == 0) {
                            response.putItem(FingerPrintActivity.OutParams.GENERAL_CONDITIONS_SHOW_EXPIRATION, false);
                        } else {
                            response.putItem(FingerPrintActivity.OutParams.GENERAL_CONDITIONS_SHOW_EXPIRATION, true);
                        }

                        response.putItem(FingerPrintActivity.OutParams.GENERAL_CONDITIONS, generalCondition);
                        response.putItem(FingerPrintActivity.OutParams.GENERAL_CONDITION_TEXT, I18nFactory.getHandler().getMessage("generalConditions.term." + generalCondition.getTerm() + ".body", request.getLang()));
                        response.putItem(FingerPrintActivity.OutParams.GENERAL_CONDITION_EXPIRATION_DATE, DateUtils.formatShortDate(generalCondition.getDateFinal()));

                        response.setReturnCode(ReturnCodes.OK);
                    }

                    return response;
                }
            } else {
                throw new ActivityException(ReturnCodes.API_FINGER_SESSION_ENDED);
            }
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (BackendConnectorException e) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, e);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        Integer idEnvironment = request.getIdEnvironment();
        if(!CalendarRestrictionsUtils.validateAccessByEnvironmentAndUser(idEnvironment,request.getIdUser(),request.getValueDate())){
            result.put("NO_FIELD", "core.calendarRestrictions.NotAuthorized");
        }
        return result;
    }

}
