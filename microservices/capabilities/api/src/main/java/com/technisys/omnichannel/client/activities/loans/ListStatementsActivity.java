/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.loans;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.StatementLoan;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.List;

/**
 *
 */
@DocumentedActivity("List Loans Statements")
public class ListStatementsActivity extends Activity {

    public static final String ID = "loans.listStatements";

    public static final String ALL_FEES = "allFees";
    public static final String PAID_FEES = "paidFees";
    public static final String PENDING_FEES = "pendingFees";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Load ID")
        String ID_LOAN = "idLoan";

        @DocumentedParam(type = String.class, description = "Requested statements")
        String REQUESTED_STATEMENTS = "requestedStatements";

        @DocumentedParam(type = String.class, description = "Page number", optional = true)
        String PAGE_NUMBER = "pageNumber";

        @DocumentedParam(type = String.class, description = "Full list", optional = true)
        String FULL_LIST = "fullList";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Statements")
        String STATEMENTS = "statements";

        //Flag que indica si hay más movimientos para mostrar el paginador
        @DocumentedParam(type = String.class, description = "Flag that indicates if there are more elements to show")
        String MORE_STATEMENTS = "moreStatements";

        @DocumentedParam(type = String.class, description = "Page number")
        String PAGE_NUMBER = "pageNumber";

        //Se devuelve la moneda del prestamo ya que en este caso
        //los movimientos del mismo no tienen la misma.
        @DocumentedParam(type = String.class, description = "Loan currency")
        String CURRENCY = "currency";

        @DocumentedParam(type = String.class, description = "Loan description")
        String LOAN_LABEL = "loanLabel";

        //Se devuelve el numero de cuotas del prestamo ya que en este caso
        //los movimientos del mismo no tienen la cantidad total de cuotas.
        @DocumentedParam(type = String.class, description = "Number of feeds")
        String NUMBER_OF_FEES = "numberOfFees";

        @DocumentedParam(type = String.class, description = "Total count")
        String TOTAL_COUNT = "totalCount";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int movementsPerPage = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "loans.statementsPerPage", 10);

            List<StatementLoan> statements;

            String idLoan = request.getParam(InParams.ID_LOAN, String.class);
            Integer pageNumber = request.getParam(InParams.PAGE_NUMBER, Integer.class);

            if (pageNumber == null) {
                pageNumber = 1;
            }

            int offset = movementsPerPage * (pageNumber - 1);

            Boolean fullList = request.getParam(InParams.FULL_LIST, Boolean.class);
            String requestedStatements = request.getParam(InParams.REQUESTED_STATEMENTS, String.class);

            boolean moreStatements = false;

            Product product = Administration.getInstance().readProduct(idLoan, request.getIdEnvironment());
            Loan loan = CoreLoanConnectorOrchestrator.read(request.getIdTransaction(), product);


            int totalCount;
            if (PENDING_FEES.equals(requestedStatements)) {
                totalCount = loan.getNumberOfFees() - loan.getNumberOfPaidFees();
            } else if (PAID_FEES.equals(requestedStatements)) {
                totalCount = loan.getNumberOfPaidFees();
            } else {
                totalCount = loan.getNumberOfFees();
            }

            if (fullList != null && fullList) {
                statements = searchStatements(request, product, 0);
            } else {
                statements = CoreLoanConnectorOrchestrator.listInstallments(request.getIdTransaction(), loan,
                        requestedStatements, movementsPerPage, offset);
                if (movementsPerPage * pageNumber < totalCount) {
                    moreStatements = true;
                }
            }

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            response.putItem(OutParams.MORE_STATEMENTS, moreStatements);
            response.putItem(OutParams.PAGE_NUMBER, pageNumber);
            response.putItem(OutParams.STATEMENTS, statements);
            response.putItem(OutParams.CURRENCY, loan.getCurrency());
            response.putItem(OutParams.LOAN_LABEL, pLabeler.calculateLabel(loan));
            response.putItem(OutParams.NUMBER_OF_FEES, loan.getNumberOfFees());
            response.putItem(OutParams.TOTAL_COUNT, totalCount);

            // Cargo el codigo de retorno
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    public static List<StatementLoan> searchStatements(Request request, Product product, Integer offset) throws BackendConnectorException {
        return CoreLoanConnectorOrchestrator.listInstallments(request.getIdTransaction(), product,
                null, ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "loans.export.maxStatements", 100), offset);
    }

}
