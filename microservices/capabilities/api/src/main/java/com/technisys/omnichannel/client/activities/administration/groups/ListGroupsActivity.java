/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.groups;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.users.ListUsersActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DocumentedActivity("List groups")
public class ListGroupsActivity extends Activity {

    public static final String ID = "administration.groups.list";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "Request page number")
        String PAGE_NUMBER = "pageNumber";
        @DocumentedParam(type = String.class, description = "Order by field name")
        String ORDER_BY = "orderBy";
    }

    public interface OutParams {

        @DocumentedParam(type = List.class, description = "Group list to show")
        String GROUPS = "groups";
        @DocumentedParam(type = Map.class, description = "Map with extended info content (massiveEnabled flag)")
        String GROUPS_EXTENDED_INFO = "groupsExtendedInfo";
        @DocumentedParam(type = Integer.class, description = "Current page number")
        String CURRENT_PAGE = "currentPage";
        @DocumentedParam(type = Integer.class, description = "Total pages")
        String TOTAL_PAGES = "totalPages";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (!Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            int rowsPerPage = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "administration.rowsPerPage");

            int pageNumber = request.getParam(ListUsersActivity.InParams.PAGE_NUMBER, int.class);
            String orderBy = request.getParam(ListUsersActivity.InParams.ORDER_BY, String.class);

            if (orderBy == null || "".equals(orderBy)) {
                orderBy = "name ASC";
            }

            PaginatedList list = AccessManagementHandlerFactory.getHandler().getGroups(null, null, request.getIdEnvironment(), pageNumber, rowsPerPage, orderBy);

            I18n i18n = I18nFactory.getHandler();
            Map<Integer, Map<String, Object>> groupsExtendedInfo = new HashMap<>();
            for (Group group : (List<Group>) list.getElementList()) {
                Map<String, Object> map = new HashMap<>();

                group.setUserIdList(AccessManagementHandlerFactory.getHandler().getUsersIdByGroup(group.getIdGroup()));
                group.setStatus(group.isBlocked() ? i18n.getMessage("group.status.blocked", request.getLang()) : i18n.getMessage("group.status.active", request.getLang()));

                boolean massiveEnabled = true;
                List<GroupPermission> permissions = AccessManagementHandlerFactory.getHandler().getGroupPermissions(group.getIdGroup());
                for (GroupPermission permission : permissions) {
                    if (Constants.ADMINISTRATION_VIEW_PERMISSION.equals(permission.getIdPermission())
                            || Constants.ADMINISTRATION_MANAGE_PERMISSION.equals(permission.getIdPermission())) {

                        massiveEnabled = false;
                        break;
                    }
                }
                map.put("massiveEnabled", massiveEnabled);
                groupsExtendedInfo.put(group.getIdGroup(), map);
            }

            response.putItem(OutParams.GROUPS, list.getElementList());
            response.putItem(OutParams.GROUPS_EXTENDED_INFO, groupsExtendedInfo);
            response.putItem(OutParams.CURRENT_PAGE, list.getCurrentPage());
            response.putItem(OutParams.TOTAL_PAGES, list.getTotalPages());

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
