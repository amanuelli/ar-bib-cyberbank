package com.technisys.omnichannel.client.utils;

import javax.ws.rs.core.MultivaluedHashMap;
import javax.ws.rs.core.MultivaluedMap;
import com.technisys.omnichannel.core.Request;

public class HeadersUtils {
    public static MultivaluedMap<String, String> buildRequestHeaders(Request request){
        MultivaluedMap<String, String> headers = new MultivaluedHashMap<>();
        headers.add("idEnvironment", String.valueOf(request.getIdEnvironment()));
        headers.add("lang", request.getLang());
        headers.add("channel", request.getChannel());
        headers.add("X-Forwarded-For", request.getClientIP());
        headers.add("User-Agent", request.getUserAgent());
        return headers;
    }
}
