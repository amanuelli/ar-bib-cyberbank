package com.technisys.omnichannel.client.forms.fields.accountfundplaid;

import com.technisys.omnichannel.client.domain.fields.AccountFundPlaidField;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.fields.FieldHandler;
import com.technisys.omnichannel.core.utils.DBUtils;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class AccountfundplaidFieldHandler extends FieldHandler {

    @Override
    public String getShortDescription(Map<String, String> map, String lang) {
        return getBasicShortDescription(map, lang);
    }

    @Override
    public String getFullDescription(Map<String, String> map, String lang) {
        return "";
    }

    @Override
    public String getPDFDescription(FormField field, String lang) {
        return "";
    }

    @Override
    public Map<String, String> validate(Map<String, String> map) throws IOException {
        return new HashMap<>();
    }

    @Override
    public Map<String, String> validate(FormField field) throws IOException {
        return new HashMap<>();
    }

    @Override
    public void createFormField(SqlSession session, Map<String, String> map) throws IOException {
        //It is no necesary because fild does not contain custom tables
    }

    @Override
    public void createFormField(SqlSession session, FormField field) throws IOException {
        //It is no necesary because fild does not contain custom tables
    }

    @Override
    public void updateFormField(SqlSession session, Map<String, String> map) throws IOException {
        //It is no necesary because fild does not contain custom tables
    }

    @Override
    public FormField readFormField(String idForm, Integer formVersion, String idField, boolean withMessages) throws IOException {
        AccountFundPlaidField accountFundPlaidField = new AccountFundPlaidField();
        accountFundPlaidField.setIdForm(idForm);
        accountFundPlaidField.setFormVersion(formVersion);
        accountFundPlaidField.setIdField(idField);

        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        try(SqlSession session = DBUtils.getInstance().openReadSession()) {
            if(withMessages){
                accountFundPlaidField.setRequiredErrorMap(fmh.listFieldMessages(session, idField, idForm, accountFundPlaidField.getFormVersion(), "requiredError"));
                accountFundPlaidField.setInvalidErrorMap(fmh.listFieldMessages(session, idField, idForm, accountFundPlaidField.getFormVersion(), "invalidError"));
            }
        }

        return accountFundPlaidField;
    }

    @Override
    public void deleteFormField(SqlSession session, String idForm, int formVersion, String idField) throws IOException {
        //It is no necesary because fild does not contain custom tables
    }

    @Override
    public Map<String, String> validateValue(Request request, String idField, Map<String, FormField> fields) throws IOException {
        return new HashMap<>();
    }

}

