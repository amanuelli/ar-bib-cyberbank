/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.PayLoanDetail;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.rubicon.core.loans.Loans;
import com.technisys.omnichannel.rubicon.core.loans.WsPayLoanRequest;
import com.technisys.omnichannel.rubicon.core.loans.WsPayLoanResponse;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.BindingProvider;
import java.text.MessageFormat;

/**
 *
 * @author salva
 */
public class RubiconCoreConnectorP extends RubiconCoreConnector {

    private static final Logger log = LoggerFactory.getLogger(RubiconCoreConnectorP.class);

    // TODO: All the method are returning a POJO without considering the return code... and throwing an exception when there is an error at network level, which is good, but also when the error is at application level (return_code != 0); we should create a Response<T> object with the return code and the data inside ;)

    public static PayLoanDetail payLoanValidate(String idTransaction, String productGroupId, Account debitAccount, Loan loan, String currency, double amount, String reference) throws BackendConnectorException {
        return payLoan(RubiconCoreConnector.OPERATION_VALIDATE, idTransaction, productGroupId, debitAccount, loan, currency, amount, reference);
    }

    public static PayLoanDetail payLoanSend(String idTransaction, String productGroupId, Account debitAccount, Loan loan, String currency, double amount, String reference) throws BackendConnectorException {
        return payLoan(RubiconCoreConnector.OPERATION_CONFIRMATION, idTransaction, productGroupId, debitAccount, loan, currency, amount, reference);
    }

    /**
     * Srv P? - Pago de préstamo
     *
     * Dado un número de cuenta origen, un número de préstamo, un importe y una
     * moneda, se realiza una pago del mismo.
     *
     * @param operacion Identificador de la operación a realizar (V, C)
     * @param productGroupId Cliente
     * @param idTransaction Identificador de la transacción de Omnichannel
     * @param debitAccount Cuenta de débito
     * @param loan Préstamo currency currency Identificador de la moneda del
     * importe
     * @param amount Importe a pagar
     * @param reference Referencia corta del movimiento a mostrar
     * @return Detalle de la validación o confirmación del pago
     */
    private static PayLoanDetail payLoan(String operacion, String idTransaction, String productGroupId, Account debitAccount, Loan loan, String currency, double amount, String reference) throws BackendConnectorException {
        PayLoanDetail detail = null;
        WsPayLoanResponse response = null;

        try {
            String logMessage;
            WsPayLoanRequest request = new WsPayLoanRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setMode(operacion);

            request.setAmount(amount);
            request.setClientID(Integer.parseInt(productGroupId));
            request.setCurrency(currency);
            request.setDebitAccountID(ProductUtils.getBackendID(debitAccount.getExtraInfo()));

            if (loan.getNumber() != null) {
                request.setLoanID(loan.getNumber());
            } else {
                request.setLoanID(ProductUtils.getBackendID(loan.getExtraInfo()));
            }

            request.setReference(reference);

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Enviar al backend: {0}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            Loans port = WS_LOANS_POOL.borrowObject();
            try {
                response = port.payLoanLoan(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_LOANS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Respuesta backend: {0}", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logMessage= MessageFormat.format("Error en backend (Srv P1 - Pago de préstamo): {0}", errorDescription);
                log.warn(logMessage);

            }

            detail = new PayLoanDetail(response);
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return detail;
    }
}
