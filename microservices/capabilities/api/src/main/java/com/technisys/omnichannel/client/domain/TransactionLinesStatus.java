package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.domain.TransactionLine.Status;

import java.io.Serializable;

public class TransactionLinesStatus implements Serializable {
    private Integer count;
    private Status status;

    public TransactionLinesStatus() { }

    public TransactionLinesStatus(Integer count, Status status) {
        this.count = count;
        this.status = status;
    }

    public Integer getCount() {
        return count;
    }

    public void setCount(Integer count) {
        this.count = count;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
