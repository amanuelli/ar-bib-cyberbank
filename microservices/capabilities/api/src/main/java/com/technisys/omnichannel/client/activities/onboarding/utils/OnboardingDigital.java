/*
 * Copyright 2019 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.onboarding.utils;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.onboarding.*;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.domain.OnboardingDocument;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.documenttypes.DocumentTypesHandler;
import com.technisys.omnichannel.core.domain.DocumentTypesCountryCodes;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.onboardings.plugins.DocumentReaderPluginFactory;
import com.technisys.omnichannel.core.onboardings.plugins.DocumentReaderResponse;
import com.technisys.omnichannel.core.onboardings.plugins.FaceRecognitionPluginFactory;
import com.technisys.omnichannel.core.onboardings.plugins.FaceRecognitionResponse;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.LinkedHashMap;

import static com.technisys.omnichannel.client.Constants.*;

public class OnboardingDigital implements OnboardingSteps {

    private static final Logger log = LoggerFactory.getLogger(OnboardingDigital.class);

    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    private static DocumentReaderResponse getResponseReadDocumentDigital(long idOnboarding, String documentToSave) throws IOException {
        int count = 0;
        int maxTries = 3;
        DocumentReaderResponse res = null;
        while (++count <= maxTries) {
            res = DocumentReaderPluginFactory.getDocumentReaderPlugin().readDocument(idOnboarding, documentToSave);
            if (res.getExpiryDateVerified() && res.getDocumentNumberVerified()) return res;
        }
        return res;
    }

    @Override
    public Response doUploadFrontDocumentExecute(Request request) throws ActivityException {
        Response response = new Response(request);
        boolean isProductRequest = false;
        try {
            String documentToSave = request.getParam(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, String.class);
            Boolean rotatePicture = request.getParam(UploadFrontDocumentActivity.InParams.ROTATE_PICTURE, Boolean.class);

            //puede que el usuario haya vuelto atras en el wizard y esté repitiendo este paso, o puede que sea la primera vez
            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);

            //si es la primera vez lo creamos
            if (StringUtils.isBlank(exchangeTokenId)) {
                exchangeTokenId = ExchangeTokenHandler.create(request.getChannel(), request.getLang(), request.getIdUser(),
                        -1, request.getUserAgent(), request.getClientIP(), new String[]{UploadFrontDocumentActivity.ID, UploadBackDocumentActivity.ID});
            } else {
                isProductRequest = OnboardingUtils.isProductRequest(OnboardingHandlerFactory.getInstance().read(ExchangeTokenHandler.read(exchangeTokenId).getAttribute("idOnboarding")));
            }

            if(isProductRequest) {
                ExchangeTokenHandler.updateActivities(exchangeTokenId, new String[]{UploadFrontDocumentActivity.ID, UploadBackDocumentActivity.ID});
            }

            ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);

            Long idOnboarding = exchangeToken.getAttribute("idOnboarding");
            boolean idOnboardingNew = false;
            if (idOnboarding == null) {
                Onboarding onboarding = new Onboarding();
                onboarding.setStatus(Onboarding.STATUS_STARTED);
                onboarding.setType(Onboarding.ONBOARDING_DEFAULT);
                idOnboarding = OnboardingHandlerFactory.getInstance().create(onboarding);
                idOnboardingNew = true;
            }

            exchangeToken.addAttribute("idOnboarding", idOnboarding);
            ExchangeTokenHandler.updateData(exchangeTokenId, exchangeToken.getData());

            Boolean isPassport = request.getParam(UploadFrontDocumentActivity.InParams.IS_PASSPORT, Boolean.class);

            if (ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "demo.safeMode")) {
                log.info("UploadFrontDocumentActivity. Returns OK because demo.safeMode is on");
                response.putItem(UploadFrontDocumentActivity.OutParams.EXCHANGE_TOKEN, exchangeTokenId);
                response.setReturnCode(ReturnCodes.OK);
                if (isPassport) {
                    response = doUploadBackDocumentExecute(request, exchangeTokenId);
                }
            } else {
                OnboardingDocument document = new OnboardingDocument(Base64.decodeBase64(documentToSave), DOCUMENT_FRONT, idOnboarding);
                OnboardingHandlerFactory.getInstance().createDocument(document);

                if (isProductRequest) {
                    OnboardingHandlerFactory.getInstance().updateStatus(idOnboarding, Onboarding.STATUS_ID_PARTIALLY_LOADED);
                }

                if (!idOnboardingNew) {
                    try {
                        FaceRecognitionPluginFactory.getFaceRecognitionPlugin().deleteImages(String.valueOf(idOnboarding));
                    } catch (IOException e) {
                        log.error("Error deleting image at face recognition plugin", e);
                    }
                }

                FaceRecognitionResponse fcResponse = FaceRecognitionPluginFactory.getFaceRecognitionPlugin().enroll(String.valueOf(idOnboarding), documentToSave, rotatePicture);

                if (fcResponse.isSuccess()) {
                    response.setReturnCode(ReturnCodes.OK);
                    response.putItem(UploadFrontDocumentActivity.OutParams.EXCHANGE_TOKEN, exchangeTokenId);

                    if (isPassport) {
                        response = doUploadBackDocumentExecute(request, exchangeTokenId);
                    }

                } else {
                    LinkedHashMap<String, Object> errors = new LinkedHashMap<>();
                    if (I18nFactory.getHandler().isMessageExists("onboarding.uploadDocument.errorCode." + fcResponse.getProviderReturnCode(), request.getLang())) {
                        errors.put(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadDocument.errorCode." + fcResponse.getProviderReturnCode());
                    } else {
                        errors.put(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadDocument.errorUploadingFrontDocument");
                    }

                    response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
                    response.setData(errors);
                }

            }

        } catch (IOException ex) {
            LinkedHashMap<String, Object> errors = new LinkedHashMap<>();
            errors.put(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.faceRecognitionServiceFail");
            response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
            response.setData(errors);
        }

        return response;
    }

    /**
     * Ejecuta el procesamiento para obtener los datos del documento
     *
     * @param request              Request realizado
     * @param requestExchangeToken Si el exchange del request es nulo, se debe
     *                             crear uno y enviarlo como parámetro, como en el primer paso del
     *                             onboarding (UploadFrontDocumentActivity)
     * @throws ActivityException En caso de algún error con el procesamiento del documento
     */
    @Override
    public Response doUploadBackDocumentExecute(Request request, String requestExchangeToken) throws ActivityException {
        Response response = new Response(request);
        LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

        try {
            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);
            if (StringUtils.isBlank(exchangeTokenId)) {
                exchangeTokenId = requestExchangeToken;
            }
            ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);

            String documentToSave = request.getParam(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, String.class);
            long idOnboarding = exchangeToken.getAttribute("idOnboarding");
            OnboardingDocument document = new OnboardingDocument(Base64.decodeBase64(documentToSave), DOCUMENT_BACK, idOnboarding);
            OnboardingHandlerFactory.getInstance().createDocument(document);

            DocumentReaderResponse drResponse = null;
            if (ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "demo.safeMode")) {
                drResponse = OnboardingUtils.demoSafeModeReadDocument();
            } else {
                drResponse = getResponseReadDocumentDigital(idOnboarding, documentToSave);
            }

            if (drResponse != null && !drResponse.isSuccess()) {
                //buscamos el mrz en el frente del documento
                OnboardingDocument frontDoc = OnboardingHandlerFactory.getInstance().readDocument(idOnboarding, DOCUMENT_FRONT);
                if (frontDoc != null) {
                    documentToSave = Base64.encodeBase64String(frontDoc.getContent());
                    drResponse = getResponseReadDocumentDigital(idOnboarding, documentToSave);
                }
            }

            if (drResponse != null) {
                if (drResponse.isSuccess()) {

                    if (!drResponse.getDocumentNumberVerified()) {
                        errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.abbyy.exception");
                        response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
                        response.setData(errors);
                        return response;
                    }

                    String alpha2DocumentCountry = CountryCodesHandler.getCountryCodyByAlpha3(drResponse.getDocumentCountry());
                    String documentTypeCode = "";
                    //Con el codigo de documento MRTD obtengo el document-type correspondiente
                    DocumentTypesCountryCodes documentTypesCountryCodes = new DocumentTypesCountryCodes(alpha2DocumentCountry, drResponse.getDocumentType());
                    documentTypeCode = DocumentTypesHandler.getDocumentTypeFromMrtdDocumentType(documentTypesCountryCodes);
                    OnboardingUtils.commonValidations(errors, drResponse, alpha2DocumentCountry, documentTypeCode);

                    if (errors.isEmpty()) {
                        OnboardingUtils.extractFullDataResponse(response, drResponse, idOnboarding, documentTypeCode, alpha2DocumentCountry, exchangeToken.getAttribute("creditCardId"), exchangeTokenId);
                        ExchangeTokenHandler.updateActivities(exchangeTokenId, new String[]{UploadFrontDocumentActivity.ID, UploadBackDocumentActivity.ID, UploadSelfieActivity.ID});
                    }
                } else if (StringUtils.isBlank(drResponse.getProviderReturnCode())) {
                    errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.documentReaderFail");
                } else {
                    if (I18nFactory.getHandler().isMessageExists("onboarding.uploadBackDocument.errorCode." + drResponse.getProviderReturnCode(), request.getLang())) {
                        errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.errorCode." + drResponse.getProviderReturnCode());
                    } else {
                        errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.documentReaderFail");
                    }
                }
            } else {
                errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.documentReaderFail");
            }
            if (errors.size() > 0) {
                response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
                response.setData(errors);
            } else {
                response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
            }

        } catch (Exception ex) {
            log.error("Error parsing document", ex);
            errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.abbyy.exception");
            response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
            response.setData(errors);
        }

        return response;
    }

    @Override
    public Response doUploadSelfieExecute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);

            ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);

            String documentSave = request.getParam(UploadSelfieActivity.InParams.DOCUMENT_TO_SAVE, String.class);
            Long idOnboarding = exchangeToken.getAttribute("idOnboarding");

            OnboardingDocument document = new OnboardingDocument(Base64.decodeBase64(documentSave), DOCUMENT_SELFIE, idOnboarding);
            OnboardingHandlerFactory.getInstance().createDocument(document);

            boolean isProductRequest = OnboardingUtils.isProductRequest(OnboardingHandlerFactory.getInstance().read(ExchangeTokenHandler.read(exchangeTokenId).getAttribute("idOnboarding")));

            if (isProductRequest) {
                OnboardingHandlerFactory.getInstance().updateStatus(idOnboarding, Onboarding.STATUS_ID_VERIFICATION_DONE);
            }

            if (ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "demo.safeMode")) {
                log.info("UploadSelfieActivity. Returns OK because demo.safeMode is on");
                response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
            } else {

                FaceRecognitionResponse fcResponse = FaceRecognitionPluginFactory.getFaceRecognitionPlugin().verify(String.valueOf(idOnboarding), documentSave);

                if (fcResponse.isSuccess()) {
                    response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
                } else if (StringUtils.isBlank(fcResponse.getProviderReturnCode())) {
                    LinkedHashMap<String, Object> errors = new LinkedHashMap<>();
                    errors.put(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.faceRecognitionFail");
                    response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
                    response.setData(errors);
                } else {
                    LinkedHashMap<String, Object> errors = new LinkedHashMap<>();
                    if (I18nFactory.getHandler().isMessageExists("onboarding.uploadSelfie.errorCode." + fcResponse.getProviderReturnCode(), request.getLang())) {
                        errors.put(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.errorCode." + fcResponse.getProviderReturnCode());
                    } else {
                        errors.put(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.errorUploadingSelfie");
                    }

                    response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
                    response.setData(errors);
                }
            }

            ExchangeTokenHandler.updateActivities(exchangeTokenId, new String[]{UploadFrontDocumentActivity.ID, UploadBackDocumentActivity.ID, UploadSelfieActivity.ID,  Step5Activity.ID});
            response.putItem(UploadSelfieActivity.OutParams.EXCHANGE_TOKEN, exchangeTokenId);
            return response;

        } catch (IOException ex) {
            LinkedHashMap<String, Object> errors = new LinkedHashMap<>();
            errors.put(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadSelfie.faceRecognitionFail");
            response.setReturnCode(com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR);
            response.setData(errors);
            return response;
        }
    }

}
