/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.accounts;

import java.io.IOException;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.StatementLine;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.exceptions.ActivityException;

/**
 *
 */
@DocumentedActivity("Download account's statement lines")
public class DownloadStatementLineActivity extends Activity {

    public static final String ID = "accounts.downloadStatementLine";

    public interface InParams {

        @DocumentedParam(type =  Account.class, description =  "ID of account")
        String ID_ACCOUNT = "idAccount";
        @DocumentedParam(type = StatementLine.class, description =  "ID of statement line")
        String ID_STATEMENT_LINE = "idStatementLine";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description =  "Name of the file to export")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = String.class, description =  "Exported file on base64 string")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            StatementLine statementLine = RubiconCoreConnectorC.readAccountStatementLine((String) request.getParam(InParams.ID_STATEMENT_LINE, String.class));

            response.putItem(OutParams.FILE_NAME, statementLine.getFilename());
            response.putItem(OutParams.CONTENT, new String(statementLine.getContent()));

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
