/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.forms.preloaders;

import java.text.MessageFormat;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.SelectorOption;
import com.technisys.omnichannel.core.domain.fields.SelectorField;
import com.technisys.omnichannel.core.forms.FormPreloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.HashMap;

/**
 *
 * @author danireb
 */
public class FrequentDestinationPreloader implements FormPreloader {

    private static final Logger log = LoggerFactory.getLogger(FrequentDestinationPreloader.class);

    private static final String PRODUCT_TYPE = "productType";
    private static final String FIELD_TYPE = "fieldType";

    @Override
    public Map<String, Object> execute(Form form, Request request) {
        Map<String, Object> formData = new HashMap<>();
        String logMessage;

        String fieldType = request.getParam(FIELD_TYPE, String.class);

        if (StringUtils.isNotEmpty(fieldType)
            && (form != null)) {

                switch (fieldType) {
                    case "default":
                        fieldType = "account";
                        break;
                    case "creditCardSelector":
                        fieldType = "creditCard";
                        break;
                    case "loanSelector":
                        fieldType = "loan";
                        break;
                    case "foreignAccountSelector":
                        fieldType = "foreignAccount";
                        break;
                    case "localAccountSelector":
                        fieldType = "externalAccount";
                        break;
                    default:
                        logMessage= MessageFormat.format("Unexpected fieldType value {0}", fieldType);
                        log.error(logMessage);
                        break;
                }
                for (FormField ff : form.getFieldList()) {
                    if (ff.getIdField().equals(PRODUCT_TYPE)) {
                        List<SelectorOption> options = ((SelectorField) ff).getOptionList();
                        Iterator<SelectorOption> iter = options.iterator();
                        while (iter.hasNext()) {
                            SelectorOption so = iter.next();
                            if (so.getId().equals(fieldType)) {
                                formData.put(PRODUCT_TYPE, fieldType);
                            } else {
                                iter.remove();
                            }
                        }
                        break;
                    }
                }
        }

        return formData;
    }
}
