/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnector;
import com.technisys.omnichannel.rubicon.core.accounts.WsAccountMovement;
import java.util.Date;
import org.apache.commons.lang3.StringUtils;

/**
 * Representa una l&iacute;nea en el estado de cuenta de una cuenta.
 *
 * @author norbes
 */
public class Statement {

    private String idAccount;
    private int idStatement;
    private Date date;
    private Date valueDate;
    private String accountCurrency;
    private String concept;
    private String reference;
    private Double debit;
    private Double credit;
    private Double balance;
    private String productAccountLabel;
    private String productAccountShortLabel;
    private String storedFileName;
    private String note;
    private String voucher;
    private int check;
    private String channel;

    public Statement() {
    }

    public Statement(WsAccountMovement movement) {
        this.idStatement = Integer.parseInt(movement.getMovementId());
        this.date = RubiconCoreConnector.convertXMLGregorianCalendarToDate(movement.getDate());
        this.valueDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(movement.getValueDate());
        this.concept = movement.getConcept();
        this.reference = movement.getReference();
        this.debit = movement.getDebit();
        this.credit = movement.getCredit();
        this.balance = movement.getBalance();
        this.check = movement.getCheckNumber();

        if (StringUtils.isNotBlank(movement.getChannel())) {
            this.channel = movement.getChannel();
        } 
    }

    public String getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(String idAccount) {
        this.idAccount = idAccount;
    }

    public int getIdStatement() {
        return idStatement;
    }

    public void setIdStatement(int idStatement) {
        this.idStatement = idStatement;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public String getAccountCurrency() {
        return accountCurrency;
    }

    public void setAccountCurrency(String accountCurrency) {
        this.accountCurrency = accountCurrency;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getProductAccountLabel() {
        return productAccountLabel;
    }

    public void setProductAccountLabel(String productAccountLabel) {
        this.productAccountLabel = productAccountLabel;
    }

    public String getProductAccountShortLabel() {
        return productAccountShortLabel;
    }

    public void setProductAccountShortLabel(String productAccountShortLabel) {
        this.productAccountShortLabel = productAccountShortLabel;
    }

    public String getStoredFileName() {
        return storedFileName;
    }

    public void setStoredFileName(String storedFileName) {
        this.storedFileName = storedFileName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }

    public int getCheck() {
        return check;
    }

    public void setCheck(int check) {
        this.check = check;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

}
