/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.frequentdestinations;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class CreateFrequentDestinationPreviewActivity extends Activity {

    public static final String ID = "frequentdestinations.create.preview";
    
    public interface InParams {
        String PRODUCT_TYPE = "productType";
    }
    
    public interface OutParams {
        String NAME = "name";
        String PRODUCT_TYPE = "productType";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.putItem("name", request.getParam(OutParams.NAME, String.class));
        String productType = null;

        List<String> productsTypes = request.getParam(InParams.PRODUCT_TYPE, List.class);

        if(productsTypes != null) {
            productType = productsTypes.get(0);
        }        
        response.putItem(OutParams.PRODUCT_TYPE, productType);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        CreateFrequentDestinationActivity createActivity = new CreateFrequentDestinationActivity();
        Map<String, String> result = createActivity.validate(request);
        return result;
    }
}
