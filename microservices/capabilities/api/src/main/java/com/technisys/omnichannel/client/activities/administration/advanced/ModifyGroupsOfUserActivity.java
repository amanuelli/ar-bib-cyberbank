package com.technisys.omnichannel.client.activities.administration.advanced;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;


/**
 *
 * @author Marcelo Bruno
 */

@DocumentedActivity("Modify user's groups")
public class ModifyGroupsOfUserActivity extends Activity {
    public static final String ID = "administration.user.detail.groups.modify.send";
    
    public interface InParams {
        
        @DocumentedParam(type = String.class, description = "User id to modify groups")
        String ID_USER = "idUser";
        @DocumentedParam(type = Integer[].class, description = "Group's Ids to associate for the user")
        String GROUPS = "groups";
    }

    public interface OutParams {

    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        String idUser = request.getParam(InParams.ID_USER, String.class);
        Integer[] groups = request.getParam(InParams.GROUPS, Integer[].class);
        List<Integer> groupList = Arrays.asList(groups);
        try {
            AccessManagementHandlerFactory.getHandler().updateUserGroups(idUser, request.getIdEnvironment(), groupList);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        
        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);
        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            String idUser = request.getParam(InParams.ID_USER, String.class);
            Integer[] groups = request.getParam(ModifyGroupsOfUserActivity.InParams.GROUPS, Integer[].class);
            if(StringUtils.isBlank(idUser)) {
                result.put("groups", "administration.users.mustSelectUser");
            } else{
                if (groups == null || groups.length == 0) {
                    result.put("groups", "administration.users.mustSelectGroup");
                } else {
                    List<Integer> environmentGroups = Administration.getInstance().listEnvironmentGroupIds(request.getIdEnvironment());
                    if (environmentGroups == null || !environmentGroups.containsAll(Arrays.asList(groups))) {
                        throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                    } else {
                        List<GroupPermission> permissions;
                        boolean hasAdminPermission = false;
                        for (Integer idGroup : groups) {
                            permissions = AccessManagementHandlerFactory.getHandler().getGroupPermissions(idGroup);
                            GroupPermission gp = new GroupPermission(idGroup, Authorization.TARGET_NONE, Constants.ADMINISTRATION_VIEW_PERMISSION);
                            hasAdminPermission = permissions.contains(gp);
                            if (hasAdminPermission) {
                                break;
                            }
                        }

                        boolean isAdministrator = Authorization.hasPermission(idUser, request.getIdEnvironment(), null, Constants.ADMINISTRATION_VIEW_PERMISSION);
                        
                        //verify if the user is an administrator that does not remove the admin permissions
                        if (isAdministrator && !hasAdminPermission) {
                            result.put("groups", "administration.users.cantRemoveAdminPermission");
                        }

                        //verify if the user is not an administrator that does not add admin permissions
                        if (!isAdministrator && hasAdminPermission) {
                            result.put("groups", "administration.users.cantAddAdminPermission");
                        }
                    }
                }
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }
    
}
