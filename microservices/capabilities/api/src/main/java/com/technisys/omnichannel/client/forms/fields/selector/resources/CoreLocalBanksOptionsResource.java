package com.technisys.omnichannel.client.forms.fields.selector.resources;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreGeneralServicesOrchestrator;
import com.technisys.omnichannel.client.domain.Bank;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.forms.fields.FrontendOption;
import com.technisys.omnichannel.core.forms.fields.OptionsResource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CoreLocalBanksOptionsResource implements OptionsResource {

    @Override
    public List<? extends FrontendOption> getOptions(FormField field, Request request) throws IOException {
        List<FrontendOption> options = new ArrayList<>();

        try {
            List<Bank> banks = CoreGeneralServicesOrchestrator.listBanks(false);

            for (Bank bank : banks) {
                options.add(new FrontendOption(bank.getCode(), bank.getBankName()));
            }

        } catch (BackendConnectorException e) {
            throw new IOException(e);
        }

        return options;
    }

}
