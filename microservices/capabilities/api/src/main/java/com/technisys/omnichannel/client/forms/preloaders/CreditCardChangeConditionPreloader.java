/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.forms.preloaders;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.forms.FormPreloader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dimoda
 */
public class CreditCardChangeConditionPreloader implements FormPreloader {

    private static final Logger log = LoggerFactory.getLogger(CreditCardChangeConditionPreloader.class);
    
    public interface OutParams {
        String EMAIL = "email";
        String PHONE = "phone";
    }
    
    @Override
    public Map<String, Object> execute(Form form, Request request) {
        Map<String, Object> formData = new HashMap();
        try {
            if (!User.USER_ANONYMOUS.equals(request.getIdUser())) {
                //Tener en cuenta que en implantaciones se puede utilizar el mail y mobile de la relación ambiente-usuario
                //y en todo caso si allí no hay nada leer el mail y mobile del usuario. Esto depende de cada implantacion
                User userInfo = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
                
                if (userInfo.getMobileNumber() != null){
                    formData.put(OutParams.PHONE, userInfo.getMobileNumber());
                }
                if (StringUtils.isNotBlank(userInfo.getEmail())){
                    formData.put(OutParams.EMAIL, userInfo.getEmail());
                }
            }
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
        return formData;
    }
}
