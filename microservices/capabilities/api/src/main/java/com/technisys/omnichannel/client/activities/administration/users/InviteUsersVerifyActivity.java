/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.utils.MaskUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.attemptscounter.AttemptsCounterHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.documenttypes.DocumentTypesHandler;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import com.technisys.omnichannel.utils.ReCaptcha;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.*;

/**
 *
 */
@DocumentedActivity("Get user's data for environment's invitation code generation")
public class InviteUsersVerifyActivity extends Activity {

    public static final String ID = "administration.users.invite.verify";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "User's document country")
        String DOCUMENT_COUNTRY = "docCountry";
        @DocumentedParam(type = String.class, description = "User's document type")
        String DOCUMENT_TYPE = "docType";
        @DocumentedParam(type = String.class, description = "User's document number")
        String DOCUMENT_NUMBER = "docNumber";

        @DocumentedParam(type = String.class, description = "Captcha user response, used after several failed login attempts")
        String CAPTCHA = "captcha";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "username")
        String NEW_USER = "newUser";

        @DocumentedParam(type = String.class, description = "User's firstName")
        String FIRST_NAME = "firstName";
        @DocumentedParam(type = String.class, description = "User's lastName")
        String LAST_NAME = "lastName";
        @DocumentedParam(type = String.class, description = "User's email")
        String EMAIL = "email";
        @DocumentedParam(type = String.class, description = "User's mobile phone")
        String MOBILE_PHONE = "mobilePhone";

        @DocumentedParam(type = String.class, description = "User's role list")
        String ROLE_LIST = "roleList";
        @DocumentedParam(type = String.class, description = "User's group list")
        String GROUP_LIST = "groupList";

        @DocumentedParam(type = String.class, description = "User's document")
        String DOCUMENT = "document";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }
            if (!ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "administration.users.invite.enabled")) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED, "No se encuentra activada la funcionalidad de invitar usuarios desde el Frontend");
            }

            String docCountry = request.getParam(InParams.DOCUMENT_COUNTRY, String.class);
            String docType = request.getParam(InParams.DOCUMENT_TYPE, String.class);
            String docNumber = request.getParam(InParams.DOCUMENT_NUMBER, String.class);

            ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), docCountry, docType, docNumber, null);
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(docCountry, docType, docNumber);
            if (client == null && user == null) {
                response.putItem(OutParams.NEW_USER, Boolean.TRUE);
            } else {
                response.putItem(OutParams.NEW_USER, Boolean.FALSE);

                if (client != null) {
                    response.putItem(OutParams.FIRST_NAME, client.getFirstName());
                    response.putItem(OutParams.LAST_NAME, client.getLastName());

                    response.putItem(OutParams.EMAIL, MaskUtils.maskEmail(client.getEmail()));
                    response.putItem(OutParams.MOBILE_PHONE, MaskUtils.maskPhone(client.getMobileNumber()));
                } else {
                    response.putItem(OutParams.FIRST_NAME, user.getFirstName());
                    response.putItem(OutParams.LAST_NAME, user.getLastName());

                    response.putItem(OutParams.EMAIL, MaskUtils.maskEmail(user.getEmail()));
                    response.putItem(OutParams.MOBILE_PHONE, MaskUtils.maskPhone(user.getMobileNumber()));
                }
            }

            // Grupos sin permisos de administración
            List<Group> groups = AccessManagementHandlerFactory.getHandler().getGroups(null, null, request.getIdEnvironment(), -1, -1, null).getElementList();
            for (Iterator<Group> it = groups.iterator(); it.hasNext();) {
                Group group = it.next();

                List<GroupPermission> permissions = AccessManagementHandlerFactory.getHandler().getGroupPermissions(group.getIdGroup());
                for (GroupPermission permission : permissions) {
                    if (Constants.ADMINISTRATION_VIEW_PERMISSION.equals(permission.getIdPermission())
                            || Constants.ADMINISTRATION_MANAGE_PERMISSION.equals(permission.getIdPermission())) {
                        it.remove();
                        break;
                    }
                }
            }

            I18n i18n = I18nFactory.getHandler();
            List<Map<String, String>> roleList = new ArrayList<>();
            for (String role : ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "invitation.permissions.roleList.frontend")) {
                Map<String, String> map = new HashMap<>();

                map.put("id", role);
                map.put("label", i18n.getMessage("invitation.permissions.role." + role, request.getLang()));

                roleList.add(map);
            }

            String document = SecurityUtils.encrypt(docCountry + "|" + docType + "|" + docNumber + "|" + System.currentTimeMillis());
            response.putItem(OutParams.DOCUMENT, document);

            response.putItem(OutParams.GROUP_LIST, groups);
            response.putItem(OutParams.ROLE_LIST, roleList);

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String docCountry = request.getParam(InParams.DOCUMENT_COUNTRY, String.class);
            String docType = request.getParam(InParams.DOCUMENT_TYPE, String.class);
            String docNumber = request.getParam(InParams.DOCUMENT_NUMBER, String.class);

            List<String> emptyList = new ArrayList();
            List<String> invalidList = new ArrayList();

            validateEmptyParams(result, docCountry, docType, docNumber, emptyList, invalidList);

            // Control del captcha
            boolean isCaptchaValid = isCaptchaValid(request, result, false);

            // Si esta bien, verifico si es válida la consulta al backend (Si no supero los intentos)
            if (isCaptchaValid) {
                AttemptsCounter attemptCounter = AttemptsCounterHandler.incrementAttemptsCounter(Constants.ATTEMPTS_INVITE_FEATURE, request.getIdUser());

                if (attemptCounter != null && attemptCounter.getAttempts() > AttemptsCounterHandler.getMaxAttemptsToBlock(Constants.ATTEMPTS_INVITE_FEATURE)) {
                    GregorianCalendar date = new GregorianCalendar();
                    Long timeout = ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "core.attempts." + Constants.ATTEMPTS_INVITE_FEATURE + ".maxAttemptsTimeout");
                    if (timeout != null) {
                        date.add(Calendar.MILLISECOND, timeout.intValue());
                    } else {
                        date.add(Calendar.MILLISECOND, ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "core.attempts." + Constants.ATTEMPTS_INVITE_FEATURE + ".lockoutWindow").intValue());
                    }

                    HashMap<String, String> fillers = new HashMap<>();
                    fillers.put("DATE", DateUtils.formatHumanReadableDate(date.getTime(), request.getLang(), true));
                    result.put("@NO_FIELD", I18nFactory.getHandler().getMessage("administration.users.maxInviteAttemptsReached", request.getLang(), fillers));
                } else {
                    ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), docCountry, docType, docNumber, null);

                    if (client == null && !ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "administration.users.invite.notInBackend.enabled")) {
                        result.put("NO_FIELD", "administration.users.documentDontExistsInBackend");
                    } 
                    User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(docCountry, docType, docNumber);

                    //Si el ambiente existe, hay que ver que el usuario no esté ya asociado al mismo
                    if (user != null && Administration.getInstance().readEnvironmentUserInfo(user.getIdUser(), request.getIdEnvironment()) != null) {
                        result.put("NO_FIELD", "administration.users.environmentUserAlreadyExists");
                    }

                    //Si es avanzado, controlo que exista por lo menos un grupo que no contenga permisos de administracion
                    if (Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme())) {
                        hasValidGroup(request, result);
                    }
                }
            }

        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }

    private boolean isCaptchaValid(Request request, Map<String, String> result, boolean isCaptchaValid) throws IOException {
        if (result.isEmpty()) {
            String captchaAnswer = request.getParam(InParams.CAPTCHA, String.class);
            if (StringUtils.isBlank(captchaAnswer)) {
                result.put("NO_FIELD", "administration.users.emptyCaptcha");
            } else {
                isCaptchaValid = ReCaptcha.isValid(captchaAnswer);
                if (!isCaptchaValid) {
                    result.put("NO_FIELD", "administration.users.invalidCaptcha");
                }
            }
        }
        return isCaptchaValid;
    }

    private void hasValidGroup(Request request, Map<String, String> result) throws IOException {
        // Controlo que tenga al menos 1 grupo sin permisos de administración, ya que desde la administracion
        // del frontend no se puede asignar el permiso de administrar.
        Boolean hasValidGroup = false;
        List<Group> groups = AccessManagementHandlerFactory.getHandler().getGroups(null, null, request.getIdEnvironment(), -1, -1, null).getElementList();
        for (Iterator<Group> it = groups.iterator(); it.hasNext() && !hasValidGroup;) {
            Group group = it.next();
            Boolean hasAdministrationPermission = false;
            List<GroupPermission> permissions = AccessManagementHandlerFactory.getHandler().getGroupPermissions(group.getIdGroup());
            for (GroupPermission permission : permissions) {
                if (Constants.ADMINISTRATION_VIEW_PERMISSION.equals(permission.getIdPermission())
                        || Constants.ADMINISTRATION_MANAGE_PERMISSION.equals(permission.getIdPermission())) {
                    hasAdministrationPermission = true;
                    break;
                }
            }
            hasValidGroup = !hasAdministrationPermission;
        }
        if(!hasValidGroup){
            result.put("NO_FIELD", "administration.users.environmentNoValidGroup");
        }
    }

    private void validateEmptyParams(Map<String, String> result, String docCountry, String docType, String docNumber, List<String> emptyList, List<String> invalidList) throws IOException {
        // Empty
        if (StringUtils.isBlank(docNumber)) {
            emptyList.add("docNumber");
        } else if (docNumber.length() > 25) {
            invalidList.add("docNumber");
        }

        List<String> validCountryCodes = CountryCodesHandler.listCountryCodes();
        if (StringUtils.isBlank(docCountry)) {
            emptyList.add("docCountry");
        } else if (!validCountryCodes.contains(docCountry)) {
            invalidList.add("docCountry");
        }

        if (StringUtils.isBlank(docType)) {
            emptyList.add("docType");
        } else if (StringUtils.isNotBlank(docCountry)) {
            List<String> validDocumentTypes = DocumentTypesHandler.listDocumentTypes(docCountry);
            if (StringUtils.isNotBlank(docCountry) && !validDocumentTypes.contains(docType)) {
                invalidList.add("docType");
            }
        }

        if (!emptyList.isEmpty()) {
            for (int i = 0; i < emptyList.size(); i++) {
                result.put(emptyList.get(i), i == 0 ? "administration.users.emptyDocument" : "");
            }
        } else if (!invalidList.isEmpty()) {
            for (int i = 0; i < invalidList.size(); i++) {
                result.put(invalidList.get(i), i == 0 ? "administration.users.invalidDocument" : "");
            }
        }
    }

}
