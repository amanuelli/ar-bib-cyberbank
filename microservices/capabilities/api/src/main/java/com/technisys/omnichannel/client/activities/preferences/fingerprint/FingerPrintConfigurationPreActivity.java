/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.fingerprint;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.SessionException;
import com.technisys.omnichannel.core.session.Session;
import com.technisys.omnichannel.core.session.SessionHandlerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.technisys.omnichannel.client.utils.HeadersUtils.buildRequestHeaders;

/**
 * Return the user list of devices with fingerprint configured
 */
@DocumentedActivity("List FingerPrint Devices")
public class FingerPrintConfigurationPreActivity extends Activity {

    public static final String ID = "preferences.fingerprint.pre";

    public interface OutParams {

        @DocumentedParam(type = Boolean.class, description = "Return true if user has sessions with fingerprint")
        String IS_DEVICE_WITH_FINGER_PRINT = "isThisDeviceWithFingerPrint";
        @DocumentedParam(type = List.class, description = "List user devices with fingerprint configured")
        String DEVICES = "devices";
    }

    public interface DeviceInfo {

        String DEVICE_ID = "deviceId";
        String DEVICE_MODEL = "deviceModel";
        String ID_SESSION = "idSession";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            List<Session> sessions = SessionHandlerFactory.getInstance().readFingerPrintSessionsByIdUser(request.getIdUser());
            Session currentSession = null;
            try {
                currentSession = SessionHandlerFactory.getInstance().readSession(request.getCredential("accessToken").getValue(), buildRequestHeaders(request));
            } catch (SessionException e) {
                // Si no existe no la proceso.
            }

            boolean isThisDeviceWithFingerPrint = false;
            ArrayList<HashMap<String, String>> devices = new ArrayList();

            for (Session session : sessions) {
                HashMap<String, String> deviceInfo = new HashMap();
                deviceInfo.put(DeviceInfo.DEVICE_ID, session.getAttribute("deviceId"));
                deviceInfo.put(DeviceInfo.DEVICE_MODEL, session.getAttribute("deviceModel"));
                deviceInfo.put(DeviceInfo.ID_SESSION, session.getIdSession());
                devices.add(deviceInfo);
                if (currentSession != null && currentSession.getIdSession().equals(session.getIdSession())) {
                    isThisDeviceWithFingerPrint = true;
                }
            }

            response.putItem(OutParams.IS_DEVICE_WITH_FINGER_PRINT, isThisDeviceWithFingerPrint);
            response.putItem(OutParams.DEVICES, devices);

            response.setReturnCode(ReturnCodes.OK);

            return response;
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }
}
