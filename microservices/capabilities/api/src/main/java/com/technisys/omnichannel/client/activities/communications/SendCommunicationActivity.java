/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import static com.technisys.omnichannel.client.activities.communications.SendCommunicationActivity.InParams.BODY;
import static com.technisys.omnichannel.client.activities.communications.SendCommunicationActivity.InParams.ID_COMMUNICATION_TRAY;
import static com.technisys.omnichannel.client.activities.communications.SendCommunicationActivity.InParams.ID_FILE_LIST;
import static com.technisys.omnichannel.client.activities.communications.SendCommunicationActivity.InParams.SUBJECT;
import com.technisys.omnichannel.client.utils.StringUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.files.FilesHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@DocumentedActivity("Send a communication message")
public class SendCommunicationActivity extends Activity {

    public static final String ID = "communications.send";
    
    public interface InParams {
        @DocumentedParam(type =  Integer.class, description =  "Communication's tray ID")
        String ID_COMMUNICATION_TRAY = "idCommunicationTray";
        @DocumentedParam(type =  String.class, description =  "Communication's subject")
        String SUBJECT = "subject";
        @DocumentedParam(type =  String.class, description =  "Communication's message body")
        String BODY = "body";
        @DocumentedParam(type =  Integer[].class, description =  "Array of id's attached files")
        String ID_FILE_LIST = "idFileList";
    }

    public interface OutParams {
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            
            int idCommunicationTray = 0;
            String idCommunicationTrayString = request.getParam(ID_COMMUNICATION_TRAY, String.class);
            if (org.apache.commons.lang.StringUtils.isNotEmpty(idCommunicationTrayString)){
                idCommunicationTray = Integer.parseInt(idCommunicationTrayString);
            }
            String subject = request.getParam(SUBJECT, String.class);
            String body = request.getParam(BODY, String.class);
            int[] idFileList = request.getParam(ID_FILE_LIST, int[].class);
            
            body = "<p>" + StringUtils.stripHTML(body) + "</p>";
            subject = StringUtils.stripHTML(subject);

            NotificationsHandlerFactory.getHandler().sendCommunicationToBank(request.getIdUser(), idCommunicationTray, subject, body, idFileList);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        int idCommunicationTray = 0;
        String idCommunicationTrayString = request.getParam(ID_COMMUNICATION_TRAY, String.class);
        if (org.apache.commons.lang.StringUtils.isNotEmpty(idCommunicationTrayString)){
            idCommunicationTray = Integer.parseInt(idCommunicationTrayString);
        }
        String subject = request.getParam(SUBJECT, String.class);
        String body = request.getParam(BODY, String.class);
        
        if (org.apache.commons.lang.StringUtils.isEmpty(subject)) {
            result.put("subject", "communications.send.subject.empty");
        }
        if (org.apache.commons.lang.StringUtils.isEmpty(body)) {
            result.put("body", "communications.send.body.empty");
        }
        if (idCommunicationTray <= 0) {
            result.put("idCommunicationTray", "communications.send.idCommunicationTray.empty");
        }
        
        try{
            //valido los archivos que hayan sido subidos por el usuario actual
            File file;
            int[] idFileList = request.getParam(ID_FILE_LIST, int[].class);
            if (idFileList != null){
                for(int idFile : idFileList){
                    file = FilesHandler.getInstance().read(request.getIdUser(), false, idFile);

                    if(file == null){
                        throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                    }
                }
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}