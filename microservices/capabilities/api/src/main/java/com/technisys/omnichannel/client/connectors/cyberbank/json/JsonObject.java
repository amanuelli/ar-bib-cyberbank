package com.technisys.omnichannel.client.connectors.cyberbank.json;

import org.json.JSONObject;

public class JsonObject extends JSONObject {
    public JsonObject build() {
        return this;
    }

    public JsonObject add(String key, Object value) {
        put(key, value);
        return this;
    }
}