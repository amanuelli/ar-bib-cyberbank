/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.StringUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.files.FilesHandler;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.technisys.omnichannel.client.activities.communications.ReplyCommunicationActivity.InParams.*;

/**
 *
 */
@DocumentedActivity("Reply communication")
public class ReplyCommunicationActivity extends Activity {

    public static final String ID = "communications.reply";
    
    public interface InParams {
        @DocumentedParam(type = Integer.class, description = "Communication's tray ID")
        String ID_COMMUNICATION_TRAY = "idCommunicationTray";
        @DocumentedParam(type = String.class, description = "Subject")
        String SUBJECT = "subject";
        @DocumentedParam(type = String.class, description = "Body")
        String BODY = "body";
        @DocumentedParam(type = Integer[].class, description = "Array of file id")
        String ID_FILE_LIST = "idFileList";
        @DocumentedParam(type = Integer.class, description = "Communication ID")
        String ID_COMMUNICATION = "idCommunication";
        @DocumentedParam(type = Integer.class, description = "Communication priority")
        String COMMUNICATION_PRIORITY = "communicationPriority";
    }

    public interface OutParams {
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            String subject = request.getParam(SUBJECT, String.class);
            String body = request.getParam(BODY, String.class);
            int[] idFileList = request.getParam(ID_FILE_LIST, int[].class);
            int idCommunication = Integer.parseInt(request.getParam(InParams.ID_COMMUNICATION, String.class));

            Communication communication =  NotificationsHandlerFactory.getHandler().read(idCommunication, request.getIdUser(), Communication.TRANSPORT_DEFAULT);
            if (communication == null) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

            int communicationPriority = Integer.parseInt(request.getParam(InParams.COMMUNICATION_PRIORITY, String.class));
            
            body = "<p>" + StringUtils.stripHTML(body) + "</p>";
            subject = StringUtils.stripHTML(subject);

            NotificationsHandlerFactory.getHandler().replyToBank(request.getIdUser(), idCommunication, subject, body, idFileList, communicationPriority);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        int idCommunicationTray = 0;
        String idCommunicationTrayString = request.getParam(ID_COMMUNICATION_TRAY, String.class);
        if (org.apache.commons.lang.StringUtils.isNotEmpty(idCommunicationTrayString)){
            idCommunicationTray = Integer.parseInt(idCommunicationTrayString);
        }
        String subject = request.getParam(SUBJECT, String.class);
        String body = request.getParam(BODY, String.class);

        if (org.apache.commons.lang.StringUtils.isBlank(subject)) {
            result.put(SUBJECT, "communications.send.subject.empty");
        }
        if (org.apache.commons.lang.StringUtils.isBlank(body)) {
            result.put(BODY, "communications.send.body.empty");
        }
        if (idCommunicationTray <= 0) {
            result.put(ID_COMMUNICATION_TRAY, "communications.send.idCommunicationTray.empty");
        }

        String priority = request.getParam(InParams.COMMUNICATION_PRIORITY, String.class);
        if(org.apache.commons.lang.StringUtils.isBlank(priority))
            result.put(COMMUNICATION_PRIORITY,"communications.send.communicationPriority.empty");

        try{
            //valido los archivos que hayan sido subidos por el usuario actual
            File file;
            int[] idFileList = request.getParam(ID_FILE_LIST, int[].class);
            if (idFileList != null){
                for(int idFile : idFileList){
                    file = FilesHandler.getInstance().read(request.getIdUser(), false, idFile);

                    if(file == null){
                        throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                    }
                }
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}