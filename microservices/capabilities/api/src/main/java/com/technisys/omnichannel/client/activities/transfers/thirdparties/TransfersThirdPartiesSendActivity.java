/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.transfers.thirdparties;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.NotificableActivity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreTransferConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.TransferInternalDetail;
import com.technisys.omnichannel.client.utils.CurrencyUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.NumberUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;

/**
 *
 */
@DocumentedActivity("Transfers to thirdparties, Send activity")
public class TransfersThirdPartiesSendActivity extends Activity implements NotificableActivity {

    private static final Logger log = LoggerFactory.getLogger(TransfersThirdPartiesSendActivity.class);

    public static final String ID = "transfers.thirdParties.send";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Debit account")
        String ID_DEBIT_ACCOUNT = "debitAccount";

        @DocumentedParam(type = String.class, description = "Credit Account")
        String CREDIT_ACCOUNT = "creditAccount";

        @DocumentedParam(type = Amount.class, description = "Amount")
        String AMOUNT = "amount";

        @DocumentedParam(type = Date.class, description = "Value Date")
        String VALUE_DATE = "valueDate";

        @DocumentedParam(type = String.class, description = "Reference")
        String REFERENCE = "reference";

        @DocumentedParam(type = String.class, description = "Notification Mails")
        String NOTIFICATION_MAILS = "notificationMails";

        @DocumentedParam(type = String.class, description = "Nofification body")
        String NOTIFICATION_BODY = "notificationBody";
    }

    public interface OutParams {

        @DocumentedParam(type = Amount.class, description = "Debit amount")
        String DEBIT_AMOUNT = "debitAmount";

        @DocumentedParam(type = Amount.class, description = "Credit Amount")
        String CREDIT_AMOUNT = "creditAmount";

        @DocumentedParam(type = Double.class, description = "Rate")
        String RATE = "rate";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            String reference = request.getParam(InParams.REFERENCE, String.class);
            String creditAccountNumber = request.getParam(InParams.CREDIT_ACCOUNT, String.class);

            Map map = request.getParam(InParams.ID_DEBIT_ACCOUNT, Map.class);
            String debitAccountParam = (String) map.get("value");

            Product debitProduct = Administration.getInstance().readProduct(debitAccountParam, request.getIdEnvironment());
            Account debitAccount = new Account(debitProduct);

            Account creditAccount = new Account();
            creditAccount.setNumber(creditAccountNumber);

            Amount amount = request.getParam(InParams.AMOUNT, Amount.class);

            TransferInternalDetail detail = CoreTransferConnectorOrchestrator.thirdParty(request.getIdTransaction(), environment.getIdEnvironment(),
                    environment.getProductGroupId(), debitAccount, creditAccountNumber, amount, reference, reference);

            if (detail.getReturnCode() == 0) {
                response.putItem(OutParams.DEBIT_AMOUNT, detail.getDebitAmount());
                response.putItem(OutParams.CREDIT_AMOUNT, detail.getCreditAmount());
                response.putItem(OutParams.RATE, detail.getRate());

                Transaction transaction = TransactionHandlerFactory.getInstance().read(request.getIdTransaction());

                /*---------------------------------------------------------*/
                /*                                                         */
                /*  Call the CORE here to get the taxes list that should   */
                /*  be applied the transaction                             */
                /*                                                         */
                /*---------------------------------------------------------*/

                Amount taxAmount = new Amount();
                taxAmount.setCurrency(amount.getCurrency());
                taxAmount.setQuantity(amount.getQuantity() * 0.004);

                transaction.getData().put("tax", taxAmount);
                TransactionHandlerFactory.getInstance().updateTransactionData(transaction);

                try {
                    sendNotificationMails(request);
                    response.setReturnCode(ReturnCodes.OK);
                } catch (IOException | MessagingException e) {
                    log.warn("Error enviando notificaciones vía mail por transferencia interna " + request.getIdTransaction(), e);
                    response.setReturnCode(ReturnCodes.ERROR_SENDING_TRANSACTION_NOTIFICATIONS);
                }

            } else {
                LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

                /*
                 * 1 - cuenta debito invalida
                 * 2 - cuenta credito invalida
                 * 3 - cuenta debito sin saldo suficiente
                 * 4 - cuenta de debito y credito coinciden
                 */
                switch (detail.getReturnCode()) {
                    case 100:
                        errors.put(InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("transfer.thirdParties.debitAccount.invalid", request.getLang()));
                        break;
                    case 101:
                        errors.put(InParams.CREDIT_ACCOUNT, I18nFactory.getHandler().getMessage("transfer.thirdParties.creditAccount.invalid", request.getLang()));
                        break;
                    case 103:
                        errors.put(InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("transfer.thirdParties.debitAccount.insufficientBalance", request.getLang()));
                        break;
                    case 102:
                        errors.put(InParams.CREDIT_ACCOUNT, I18nFactory.getHandler().getMessage("transfers.thirdParties.sameCreditDebitAccount", request.getLang()));
                        break;
                    default:
                        String key = "transfer.thirdParties.returnCode." + detail.getReturnCode();
                        String message = I18nFactory.getHandler().getMessage(key, request.getLang());
                        if (!StringUtils.isBlank(message) && !message.equals(key)) {
                            errors.put("NO_FIELD", message);
                        } else {
                            response.setReturnCode(ReturnCodes.BACKEND_SERVICE_ERROR);
                        }
                }
                response.setReturnCode(ReturnCodes.VALIDATION_ERROR);
                response.setData(errors);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (BackendConnectorException bcE) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, bcE);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            //ejecutamos validaciones del formulario y dejar aquí solo las especificas de la actividad
            Map<String, String> result = FormsHandler.getInstance().validateRequest(request);

            // No pueden ser la misma cuenta. Solo hago esta validación si ya no hay un error sobre alguno de estos campos
            if (!result.containsKey(InParams.CREDIT_ACCOUNT) && !result.containsKey(InParams.ID_DEBIT_ACCOUNT)) {
                String idDebitAccount = request.getParam(InParams.ID_DEBIT_ACCOUNT, String.class);
                String creditAccount = request.getParam(InParams.CREDIT_ACCOUNT, String.class);

                if (StringUtils.isNotBlank(creditAccount) && StringUtils.isNotBlank(idDebitAccount) && creditAccount.equalsIgnoreCase(idDebitAccount)) {
                    result.put(InParams.CREDIT_ACCOUNT, "transfers.thirdPartiess.sameCreditDebitAccount");
                }
            }

            return result;
        } catch (SchedulerException ex) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    @Override
    public void sendNotificationMails(Transaction transaction) throws IOException, MessagingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sendNotificationMails(Request request) throws IOException, MessagingException {
        sendNotificationMails(request.getIdEnvironment(), request.getLang(), request.getValueDate(), request.getParameters());
    }

    private void sendNotificationMails(int idEnvironment, String lang, Date valueDate, Map<String, Object> params) throws IOException, MessagingException {
        List<String> mails = RequestParamsUtils.getValue(params.get(InParams.NOTIFICATION_MAILS), List.class);

        if (mails != null && mails.size() > 0) {

            Environment env = Administration.getInstance().readEnvironment(idEnvironment);

            Amount amount = RequestParamsUtils.getValue(params.get(TransfersThirdPartiesSendActivity.InParams.AMOUNT), Amount.class);

            int defaultMinimumFractionDigits = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "defaultDecimal.minimum");
            int defaultMaximumFractionDigits = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "defaultDecimal.maximum");

            String creditAccount = RequestParamsUtils.getValue(params.get(InParams.CREDIT_ACCOUNT), String.class);

            // Message Subject
            String subject = I18nFactory.getHandler().getMessage("transfer.internal.notificationEmail.subject", lang);

            // User Notification Body
            HashMap<String, String> fillers = new HashMap<>();
            String notificationBody = RequestParamsUtils.getValue(params.get(InParams.NOTIFICATION_BODY), String.class);
            if (StringUtils.isBlank(notificationBody)) {
                notificationBody = "";
            } else {
                fillers.put("MESSAGE", notificationBody);
                notificationBody = I18nFactory.getHandler().getMessage("transfer.internal.notificationEmail.body.commentSection", lang, fillers);
            }

            String referenceBody = RequestParamsUtils.getValue(params.get(InParams.REFERENCE), String.class);
            if (StringUtils.isBlank(referenceBody)) {
                referenceBody = "";
            } else {
                fillers.put("REFERENCE", referenceBody);
                referenceBody = I18nFactory.getHandler().getMessage("transfer.internal.notificationEmail.body.referenceSection", lang, fillers);
            }

            // Message Body
            fillers = new HashMap<>();
            fillers.put("DATE", DateUtils.formatShortDate(valueDate));
            fillers.put("HOUR", DateUtils.formatTime(valueDate));
            fillers.put("CREDIT_ACCOUNT", creditAccount);
            fillers.put("ENVIRONMENT_NAME", StringUtils.defaultString(env.getName()));
            fillers.put("DEBIT_CURRENCY", CurrencyUtils.getCurrencyLabel(amount.getCurrency(), lang));
            fillers.put("DEBIT_AMOUNT", NumberUtils.formatDecimal(amount.getQuantity(), true, defaultMinimumFractionDigits, defaultMaximumFractionDigits, lang));
            fillers.put("NOTIFICATION_BODY", notificationBody);
            fillers.put("REFERENCE_BODY", referenceBody);

            String body = I18nFactory.getHandler().getMessage("transfer.internal.notificationEmail.body", lang, fillers);

            NotificationsHandlerFactory.getHandler().sendAsyncEmail(subject, body, mails, null, lang, true);
        }
    }

}
