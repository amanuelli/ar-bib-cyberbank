/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnector;
import com.technisys.omnichannel.rubicon.core.creditcards.WsCreditCardMovement;
import java.util.Date;

/*
 * Representa una l&iacute;nea en el los movimientos de tarjeta.
 * 
 * @author psuarez
 */
public class StatementCreditCard {

    private String idProduct;
    private int idStatement;
    private Date date;
    private Date valueDate;
    private String concept;
    private String cardUsedLabel;
    private Double sourceAmount;
    private String sourceAmountCurrency;
    private Double amount;
    private String productCreditCardLabel;
    private String productCreditCardShortLabel;
    private String storedFileName;
    private String idUsedCreditCard;
    private String note;
    private String reference;
    private double latitude;
    private double longitude;

    public StatementCreditCard() {
    }

    public StatementCreditCard(WsCreditCardMovement movement) {
        this.idStatement = Integer.parseInt(movement.getMovementId());
        this.date = RubiconCoreConnector.convertXMLGregorianCalendarToDate(movement.getDate());
        this.valueDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(movement.getValueDate());
        this.concept = movement.getConcept();
        this.amount = movement.getAmount2();
        this.sourceAmount = movement.getSourceAmount();
        this.idUsedCreditCard = movement.getUsedCreditCardId();
        this.sourceAmountCurrency = ClientProduct.CURRENCY_USD;
        this.latitude = movement.getLatitude();
        this.longitude = movement.getLongitude();
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public int getIdStatement() {
        return idStatement;
    }

    public void setIdStatement(int idStatement) {
        this.idStatement = idStatement;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getCardUsedLabel() {
        return cardUsedLabel;
    }

    public void setCardUsedLabel(String cardUsedLabel) {
        this.cardUsedLabel = cardUsedLabel;
    }

    public String getStoredFileName() {
        return storedFileName;
    }

    public void setStoredFileName(String storedFileName) {
        this.storedFileName = storedFileName;
    }

    public Double getSourceAmount() {
        return sourceAmount;
    }

    public void setSourceAmount(Double sourceAmount) {
        this.sourceAmount = sourceAmount;
    }

    public String getProductCreditCardLabel() {
        return productCreditCardLabel;
    }

    public void setProductCreditCardLabel(String productCreditCardLabel) {
        this.productCreditCardLabel = productCreditCardLabel;
    }

    public String getProductCreditCardShortLabel() {
        return productCreditCardShortLabel;
    }

    public void setProductCreditCardShortLabel(String productCreditCardShortLabel) {
        this.productCreditCardShortLabel = productCreditCardShortLabel;
    }

    public String getSourceAmountCurrency() {
        return sourceAmountCurrency;
    }

    public void setSourceAmountCurrency(String sourceAmountCurrency) {
        this.sourceAmountCurrency = sourceAmountCurrency;
    }

    public Date getValueDate() {
        return valueDate;
    }

    public void setValueDate(Date valueDate) {
        this.valueDate = valueDate;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getIdUsedCreditCard() {
        return idUsedCreditCard;
    }

    public void setIdUsedCreditCard(String idUsedCreditCard) {
        this.idUsedCreditCard = idUsedCreditCard;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }
    
    public Double getLatitude()
    {
        return this.latitude; 
    }
    
    public void setLatitude(double latitude)
    {
        this.latitude = latitude;
    }
    
    public Double getLongitude()
    {
        return this.longitude;
    }
    
    public void setLongitude(double longitude)
    {
        this.longitude = longitude;
    }
}
