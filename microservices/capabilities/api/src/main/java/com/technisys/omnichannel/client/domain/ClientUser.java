/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.utils.JsonUtils;
import com.technisys.omnichannel.rubicon.core.general.WsClient;
import com.technisys.omnichannel.rubicon.core.general.WsReadPersonResponse;
import org.apache.commons.lang3.StringUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * @author fpena
 */
public class ClientUser extends User {

    private String productGroupId;
    private String address;
    private String mailingAddress;
    private String district;
    private String province;
    private String department;
    private String residenceCountry;
    private String nationality;
    private String gender;
    private String ocupation;
    private String coreId;

    public ClientUser() {
    }

    public ClientUser(WsReadPersonResponse response, String productGroupId) {
        setFirstName(response.getFirstName());
        setLastName(response.getLastName());

        this.gender = response.getGender();

        if (response.getClients() != null && response.getClients().getClient() != null) {
            for (WsClient client : response.getClients().getClient()) {
                if (productGroupId == null || client.getClientID() == Integer.parseInt(productGroupId)) {
                    this.address = client.getAddressLine1();
                    this.mailingAddress = client.getMaillingAddress();
                    this.residenceCountry = client.getCountry();
                    this.district = client.getDistrict();
                    this.province = client.getProvince();
                    this.department = client.getState();
                    this.nationality = client.getNationality();
                    this.ocupation = client.getOcupation();

                    setEmail(client.getEmailAddress());
                    setMobileNumber(getPhoneValue(client.getMobile()));

                    break;
                }
            }
        }
    }


    public Map<String, String> getAdditionalData() {
        Map<String, String> extendedData = new HashMap<>();
        extendedData.put("address", StringUtils.trimToNull(this.getAddress()));
        extendedData.put("district", StringUtils.trimToNull(this.getDistrict()));
        extendedData.put("province", StringUtils.trimToNull(this.getProvince()));
        extendedData.put("department", StringUtils.trimToNull(this.getDepartment()));
        extendedData.put("residenceCountry", StringUtils.isEmpty(this.getResidenceCountry()) ? null : "key=country.name." + this.getResidenceCountry());
        extendedData.put("nationality", StringUtils.trimToNull(this.getNationality()));
        extendedData.put("gender", StringUtils.isEmpty(this.getGender()) ? null : "key=gender.name." + this.getGender());
        extendedData.put("ocupation", StringUtils.trimToNull(this.getOcupation()));
        extendedData.put("coreId", StringUtils.isBlank(this.coreId) ? "" : this.coreId);
        return extendedData;
    }


    public String getJsonAdditionalData() {
        return JsonUtils.toJson(getAdditionalData(), false, false);
    }


    public String getSafewayCommonName() {
        return this.getIdUser();
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public String getDepartment() {
        return department;
    }

    public void setDepartment(String department) {
        this.department = department;
    }

    public String getResidenceCountry() {
        return residenceCountry;
    }

    public void setResidenceCountry(String residenceCountry) {
        this.residenceCountry = residenceCountry;
    }

    public String getNationality() {
        return nationality;
    }

    public void setNationality(String nationality) {
        this.nationality = nationality;
    }

    public String getProductGroupId() {
        return productGroupId;
    }

    public void setProductGroupId(String productGroupId) {
        this.productGroupId = productGroupId;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getOcupation() {
        return ocupation;
    }

    public void setOcupation(String ocupation) {
        this.ocupation = ocupation;
    }

    public String getCoreId() {
        return coreId;
    }

    public void setCoreId(String coreId) {
        this.coreId = coreId;
    }

    public static String getPhoneValue(String phone) {
        phone = com.technisys.omnichannel.client.utils.StringUtils.cleanNumber(phone);
        if (StringUtils.isNotBlank(phone) && phone.length() <= 20) {
            return phone;
        } else {
            return null;
        }
    }

    public String getMailingAddress() {
        return mailingAddress;
    }

    public void setMailingAddress(String mailingAddress) {
        this.mailingAddress = mailingAddress;
    }

}
