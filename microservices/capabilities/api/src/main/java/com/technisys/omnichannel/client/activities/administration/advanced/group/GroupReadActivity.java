package com.technisys.omnichannel.client.activities.administration.advanced.group;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.common.Permissions;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DocumentedActivity("Fetch group details")
public class GroupReadActivity extends Activity {

    public static final String ID = "administration.advanced.group.read";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "ID of the env group to fetch information")
        String ID = "id";
    }

    public interface OutParams {

        @DocumentedParam(type = Group.class, description = "Group information")
        String GROUP = "group";
        @DocumentedParam(type = List.class, description = "Group permissions")
        String PERMISSIONS = "permissions";
        @DocumentedParam(type = List.class, description = "Permissions categories to build layout")
        String UI_PERMISSIONS = "uiPermissions";
        @DocumentedParam(type = List.class, description = "Environment products")
        String PRODUCTS = "products";
        @DocumentedParam(type = List.class, description = "Ids of admin users in the current group")
        String ADMIN_USERS = "adminUsers";
        @DocumentedParam(type = List.class, description = "This helps to decide which actions you show")
        String IS_ADMIN_GROUP = "isAdminGroup";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (!Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            int idEnvironment = request.getIdEnvironment();
            String lang = request.getLang();
            int idGroup = request.getParam(InParams.ID, Integer.class);
            Group group = AccessManagementHandlerFactory.getHandler().getGroup(idGroup);
            GroupPermission adminGroup = new GroupPermission(idGroup, Authorization.TARGET_NONE, Constants.ADMINISTRATION_VIEW_PERMISSION);
            List<GroupPermission> groupPermissions = AccessManagementHandlerFactory.getHandler().getGroupPermissions(idGroup);
            boolean isAdminGroup = groupPermissions.contains(adminGroup);

            response.putItem(OutParams.GROUP, group);
            response.putItem(OutParams.PERMISSIONS, Permissions.buildPermissionsMap(group, idEnvironment).get("allPermissions"));
            response.putItem(OutParams.UI_PERMISSIONS, Permissions.listUIPermissions(idEnvironment, lang));
            response.putItem(OutParams.PRODUCTS, Permissions.listProducts(idEnvironment, lang));
            response.putItem(OutParams.ADMIN_USERS, Permissions.filterAdminUsers(idEnvironment, group.getUserList()));
            response.putItem(OutParams.IS_ADMIN_GROUP, isAdminGroup);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            int idGroup = request.getParam(InParams.ID, Integer.class);
            Group group = AccessManagementHandlerFactory.getHandler().getGroup(idGroup);

            if (group == null || group.getIdEnvironment() != request.getIdEnvironment()) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
