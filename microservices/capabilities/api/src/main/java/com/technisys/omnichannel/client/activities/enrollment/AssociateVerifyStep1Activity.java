/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.SecuritySealUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.attemptscounter.AttemptsCounterHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.credentials.CredentialPluginFactory;
import com.technisys.omnichannel.core.domain.AttemptsCounter;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.domain.UserStatus;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import com.technisys.omnichannel.utils.ReCaptcha;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;

/**
 *
 */
@ExchangeActivity
public class AssociateVerifyStep1Activity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(AssociateVerifyStep1Activity.class);

    public static final String ID = "enrollment.associate.verifyStep1";

    private static final String NEXT_ACTIVITY_IN_FLOW = AssociateVerifyStep2Activity.ID;

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
        String SECOND_FACTOR = "_secondFactor";
        String USERNAME = "username";
    }

    public interface OutParams {
        String EXCHANGE_TOKEN = "_exchangeToken";
        String SECURITY_SEAL = "_securitySeal";
        String CAPTCHA_REQUIRED = "captchaRequired";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            InvitationCode invitation = getInvitationCode(request);

            String username = StringUtils.lowerCase(request.getParam(InParams.USERNAME, String.class));

            User user = AccessManagementHandlerFactory.getHandler().getUserByUsername(username);
            if (user == null) {
                throw new ActivityException(ReturnCodes.USER_DOESNT_EXIST, "Unable to read user '" + request.getIdUser() + "'");
            } else if (!UserStatus.USER_STATUS_ACTIVE.equals(user.getIdUserStatus())) {
                throw new ActivityException(ReturnCodes.USER_DISABLED, "User '" + request.getIdUser() + "' is disabled");
            }

            User userC = AccessManagementHandlerFactory.getHandler().getUserByDocument(invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber());
            if (userC == null) {
                throw new ActivityException(ReturnCodes.INVALID_INVITATION_CODE);
            }

            // Valido las credenciales
            try {
                // Genero una request artificial para enviar al validate de los plugines
                Request auxRequest = new Request();
                auxRequest.setIdActivity(ID);
                auxRequest.setIdUser(user.getIdUser());
                auxRequest.setCredentials(request.getCredentials());
                auxRequest.setLang(request.getLang());

                CredentialPlugin cp = CredentialPluginFactory.getCredentialPlugin(Credential.CAPTCHA_CREDENTIAL);
                cp.validate(auxRequest, request.getCredential(Credential.CAPTCHA_CREDENTIAL));

                String secondFactor = ConfigurationFactory.getInstance().getStringSafe(Configuration.PLATFORM, "auth.login.credentialRequested");
                cp = CredentialPluginFactory.getCredentialPlugin(secondFactor);
                cp.validate(auxRequest, new Credential(secondFactor, StringUtils.defaultString(request.getParam(InParams.SECOND_FACTOR, String.class))));
            } catch (DispatchingException e) {
                throw new ActivityException(e.getReturnCode());
            }

            // Actualizo el exchange token
            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);

            List<String> activities = new ArrayList<>(Arrays.asList(StringUtils.split(token.getActivities(), ',')));
            if (!activities.contains(NEXT_ACTIVITY_IN_FLOW)) {
                activities.add(NEXT_ACTIVITY_IN_FLOW);
            }

            ExchangeTokenHandler.updateActivities(exchangeToken, activities.toArray(new String[0]));

            response.putItem(OutParams.CAPTCHA_REQUIRED, ReCaptcha.mustBeDisplayedForUser(AttemptsCounterHandler.getFeature(ID), user.getIdUser()));
            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            response.putItem(OutParams.SECURITY_SEAL, SecuritySealUtils.getSeal(user.getIdSeal()));

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            // Valido que exista la invitación
            InvitationCode invitation = getInvitationCode(request);

            String username = StringUtils.lowerCase(request.getParam(InParams.USERNAME, String.class));
            if (StringUtils.isBlank(username)) {
                result.put(InParams.USERNAME, "enrollment.associate.step1.username.error.empty");
            } else {
                AttemptsCounter counter = AttemptsCounterHandler.getAttemptsCounter(Constants.ATTEMPTS_ASSOCIATE_ENROLLMENT, invitation.getInvitationCode());

                User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber());
                if (user == null || !StringUtils.equals(user.getUsername(), username)) {
                    counter = AttemptsCounterHandler.incrementAttemptsCounter(Constants.ATTEMPTS_ASSOCIATE_ENROLLMENT, invitation.getInvitationCode());

                    result.put(InParams.USERNAME, "enrollment.associate.step1.username.error.invitationUsernameMismatch");
                }

                if (counter != null && counter.getAttempts() >= AttemptsCounterHandler.getMaxAttemptsToBlock(Constants.ATTEMPTS_ASSOCIATE_ENROLLMENT)) {
                    InvitationCodesHandler.cancelInvitationCode(invitation.getId());
                    log.warn("Se cancelo la invitación debido a que se alcanzó el máximo intentos de ingreso con un usuario diferente al de la invitación.");

                    throw new ActivityException(ReturnCodes.INVITATION_CODE_CANCELED);
                }
            }

            String credentialRequested = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "auth.login.credentialRequested");

            String secondFactor = request.getParam(InParams.SECOND_FACTOR, String.class);
            if (StringUtils.isBlank(secondFactor)) {
                result.put(credentialRequested, "enrollment.associate.step1.secondFactor.error.empty");
            }

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }

}
