/* 
 * Copyright 2018 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 *
 * @author ahernandez
 */
@DocumentedActivity("Delete communication")
public class DeleteCommunicationActivity extends Activity {
    
    public static final String ID = "communications.delete";

    public interface InParams {
        @DocumentedParam(type =  Integer.class, description =  "Communication ID")
        String COMMUNICATION_ID = "idCommunication";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            Integer idCommunication = request.getParam(InParams.COMMUNICATION_ID, Integer.class);
            Communication communication =  NotificationsHandlerFactory.getHandler().read(idCommunication, request.getIdUser(), Communication.TRANSPORT_DEFAULT);

            if (communication == null) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

            int threadId = communication.getIdThread();
            NotificationsHandlerFactory.getHandler().markThreadRecipientAsDeleted(threadId, request.getIdUser());
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

}
