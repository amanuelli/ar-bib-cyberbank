/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.domain.*;
import com.technisys.omnichannel.client.handlers.notes.NotesHandler;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.IOUtils;
import com.technisys.omnichannel.rubicon.core.creditcards.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import java.io.IOException;
import java.io.InputStream;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

/**
 * @author jbaccino
 */
public class RubiconCoreConnectorTC extends RubiconCoreConnector {

    private static final Logger log = LoggerFactory.getLogger(RubiconCoreConnectorTC.class);

    // TODO: All the method are returning a POJO without considering the return code... and throwing an exception when there is an error at network level, which is good, but also when the error is at application level (return_code != 0); we should create a Response<T> object with the return code and the data inside ;)

    /**
     * Srv T1 - Consultar detalles de una tarjeta
     *
     * @param idTransaction Id de transaccion
     * @param idProduct     Id del producto
     * @param extraInfo     Extra info del producto
     * @return Informacion de la tarjeta
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static CreditCard readCreditCardDetails(String idTransaction, String idProduct, String extraInfo) throws BackendConnectorException {
        CreditCard creditCard = null;
        WsReadCreditCardResponse response = null;
        String logMessage;

        try {
            WsReadCreditCardRequest request = new WsReadCreditCardRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setProductID(ProductUtils.getBackendID(extraInfo));

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Enviar al backend: {0}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            CreditCards port = WS_CREDITCARDS_POOL.borrowObject();
            try {
                response = port.readCreditCard(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_CREDITCARDS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Respuesta backend: {0}", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logMessage= MessageFormat.format("Error en backend (Srv T1 - Consultar detalles de una tarjeta): {0}", errorDescription);
                log.warn(logMessage);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                creditCard = new CreditCard(response);
                creditCard.setIdProduct(idProduct);
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return creditCard;
    }

    public static StatementCreditCard readStatementDetails(String idTransaction, String idCreditCard, int idStatement, String creditCardProductExtraInfo) throws BackendConnectorException {

        StatementCreditCard statement = new StatementCreditCard();
        CreditCard creditCardDetails = RubiconCoreConnectorTC.readCreditCardDetails(idTransaction, idCreditCard, creditCardProductExtraInfo);
        WsCreditCardMovementResponse response = null;
        try {
            WsReadCreditCardStatementDetailRequest request = new WsReadCreditCardStatementDetailRequest();
            request.setChannel(CHANNEL);
            request.setIdStatement(idStatement);

            if (log.isDebugEnabled()) {
                log.debug("Send to backend: {}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
            }

            CreditCards port = WS_CREDITCARDS_POOL.borrowObject();
            try {
                response = port.readCreditCardStatementDetail(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_CREDITCARDS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                log.debug("Backend response: {}", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                log.warn("Backend error (Srv T1 - Get credit card movement detail): {}", errorDescription);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                WsCreditCardMovement movement = response.getCreditCardMovement();
                statement.setIdStatement(idStatement);
                statement.setIdProduct(creditCardDetails.getIdProduct());
                statement.setDate(movement.getDate().toGregorianCalendar().getTime());
                statement.setValueDate(movement.getDate().toGregorianCalendar().getTime());
                statement.setConcept(movement.getConcept());
                statement.setCardUsedLabel(creditCardDetails.getLabel());
                statement.setSourceAmount(movement.getAmount2());
                statement.setSourceAmountCurrency("USD");
                statement.setIdUsedCreditCard(creditCardDetails.getNumber());
                statement.setReference(movement.getConcept());
                statement.setLatitude(movement.getLatitude());
                statement.setLongitude(movement.getLongitude());
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return statement;

    }

    //DUMMY METHOD
    public static List<StatementLine> listCreditCardStatementLines(String idAccount) throws IOException {
        List<StatementLine> statementLines = new ArrayList();
        StatementLine statementLine = getDummyStatementLine();
        statementLine.setIdProduct(idAccount);
        statementLines.add(statementLine);

        return statementLines;
    }

    //DUMMY METHOD
    public static StatementLine readCreditCardStatementLine(String idCreditCard) throws IOException {
        StatementLine statementLine = getDummyStatementLine();
        statementLine.setIdProduct(idCreditCard);
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try(InputStream pdfSource = classLoader.getResourceAsStream("/dummy/estado-de-cuenta-ejemplo.pdf")) {
            byte[] content = Base64.encodeBase64(IOUtils.toByteArray(pdfSource));
            statementLine.setContent(content);
        }
        return statementLine;
    }

    /**
     * Srv T2 - Total de movimientos de una tarjeta
     * @param extraInfo     ExtraInfo del producto
     * @return La cantidad de movimientos de la tarjeta
     * @throws BackendConnectorException Error de conexion con el Backend
     */
    public static int getTotalCreditCardStatements(String extraInfo) throws BackendConnectorException {
        WsListCreditCardMovementsResponse response = null;
        String logMessage;

        int totalStatements = 0;
        try {
            WsListCreditCardMovementsRequest request = new WsListCreditCardMovementsRequest();
            request.setChannel(CHANNEL);
            request.setProductID(ProductUtils.getBackendID(extraInfo));

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Enviar al backend: {0}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            CreditCards port = WS_CREDITCARDS_POOL.borrowObject();
            try {
                response = port.listTotalCreditCardMovements(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_CREDITCARDS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Respuesta backend: {0}", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logMessage= MessageFormat.format("Error en backend (Srv T2 - Get total movimientos de una tarjeta): {0}", errorDescription);
                log.warn(logMessage);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                totalStatements = response.getTotalRecords();
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return totalStatements;
    }



    /**
     * Srv T2 - Cantidad de movimientos de una tarjeta dentro del período actual.
     * @param extraInfo     ExtraInfo del producto
     * @return La cantidad de movimientos de la tarjeta dentro del período actual.
     * @throws BackendConnectorException Error de conexion con el Backend
     */
    public static int getCurrentPeriodCreditCardStatements(String extraInfo) throws BackendConnectorException {
        WsListCreditCardMovementsResponse response = null;
        String logMessage;

        int totalCurrentPeriod = 0;
        try {
            WsListCreditCardMovementsRequest request = new WsListCreditCardMovementsRequest();
            request.setChannel(CHANNEL);
            request.setProductID(ProductUtils.getBackendID(extraInfo));

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Enviar al backend: {0}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            CreditCards port = WS_CREDITCARDS_POOL.borrowObject();
            try {
                response = port.listCurrentPeriodCreditCardMovements(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_CREDITCARDS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Respuesta backend: {0}", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logMessage= MessageFormat.format("Error en backend (Srv T2 - Get total movimientos de una tarjeta): {0}", errorDescription);
                log.warn(logMessage);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                totalCurrentPeriod = response.getCurrentPeriodRecords();
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return totalCurrentPeriod;
    }





    /**
     * Srv T2 - Listar movimientos de una tarjeta
     *
     * @param idTransaction Id de transaccion
     * @param idProduct     Id de producto
     * @param extraInfo     ExtraInfo del producto
     * @param dateFrom      Filtrado por fecha de inicio
     * @param dateTo        Filtrado por fecha de fin
     * @param minAmount     Filtrado por importe minimo
     * @param maxAmount     Filtrado por importe maximo
     * @param concept       Filtrado por concepto
     * @param currency      Filtrado por moneda
     * @param limit         Limite de los resultados
     * @param offset        Offset de los resultados
     * @return Lista de los movimientos de la tarjeta
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static List<StatementCreditCard> listCreditCardStatements(String idTransaction, String idProduct, String extraInfo, Date dateFrom, Date dateTo, Double minAmount, Double maxAmount, String concept, String currency, int limit, int offset) throws BackendConnectorException {
        List<StatementCreditCard> statements = new ArrayList<>();
        WsListCreditCardMovementsResponse response = null;
        String logMessage;

        try {
            WsListCreditCardMovementsRequest request = new WsListCreditCardMovementsRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setProductID(ProductUtils.getBackendID(extraInfo));

            XMLGregorianCalendar xmlDate = null;
            if (dateFrom != null) {
                GregorianCalendar date = new GregorianCalendar();
                date.setTime(dateFrom);
                date = DateUtils.changeTime(date, 0, 0, 0, 0);
                xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
            }
            request.setDateFrom(xmlDate);

            xmlDate = null;
            if (dateTo != null) {
                GregorianCalendar date = new GregorianCalendar();
                date.setTime(dateTo);
                date = DateUtils.changeTime(date, 23, 59, 59, 999);
                xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
            }
            request.setDateTo(xmlDate);

            request.setOffset(offset);
            request.setLimit(limit);
            request.setMaxAmount(maxAmount);
            request.setMinAmount(minAmount);
            request.setCurrency(currency);
            request.setReference(concept);

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Enviar al backend: {0}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            CreditCards port = WS_CREDITCARDS_POOL.borrowObject();
            try {
                response = port.listCreditCardMovements(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_CREDITCARDS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Respuesta backend: {0}", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logMessage= MessageFormat.format("Error en backend (Srv T2 - Listar movimientos de una tarjeta): {0}", errorDescription);
                log.warn(logMessage);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                if (response.getMovements() != null && response.getMovements().getMovement() != null) {
                    StatementCreditCard statement;
                    for (WsCreditCardMovement movement : response.getMovements().getMovement()) {
                        statement = new StatementCreditCard(movement);
                        statement.setIdProduct(idProduct);
                        statement.setNote(NotesHandler.readNote(statement.getIdStatement(), idProduct));
                        statements.add(statement);
                    }
                }
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return statements;
    }

    //DUMMY METHOD
    private static StatementLine getDummyStatementLine() {
        StatementLine statementLine = new StatementLine();
        statementLine.setMonth(12);
        statementLine.setYear(2012);
        statementLine.setName("Diciembre 2012");
        statementLine.setIdStatementLine("5");
        statementLine.setFilename("estado-de-cuenta-ejemplo.pdf");

        return (statementLine);
    }

    public static PayCreditCardDetail payCreditCardValidate(String idTransaction, String idClient, Account debitAccount, String creditcardNumber, String creditcardBank, String currency, double amount, String reference, double latitude, double longitude) throws BackendConnectorException {
        return payCreditCard(RubiconCoreConnector.OPERATION_VALIDATE, idTransaction, idClient, debitAccount, creditcardNumber, creditcardBank, currency, amount, reference, latitude, longitude);
    }

    public static PayCreditCardDetail payCreditCardSend(String idTransaction, String idClient, Account debitAccount, String creditcardNumber, String creditcardBank, String currency, double amount, String reference, double latitude, double longitude) throws BackendConnectorException {
        return payCreditCard(RubiconCoreConnector.OPERATION_CONFIRMATION, idTransaction, idClient, debitAccount, creditcardNumber, creditcardBank, currency, amount, reference, latitude, longitude);
    }

    /**
     * Srv P? - Pago de tarjeta
     * <p>
     * Dado un número de cuenta origen, un número de tarjeta, un importe y una
     * moneda, se realiza una pago de la tarjeta.
     *
     * @param operacion     Identificador de la operación a realizar (V, C)
     * @param idClient      Cliente
     * @param idTransaction Identificador de la transacción de Omnichannel
     * @param debitAccount  Cuenta de débito
     * @param amount        Importe a pagar
     * @param reference     Referencia corta del movimiento a mostrar
     * @param latitude      latitud de geolocalización del movimiento a mostrar
     * @param longitude     longitud de geolocalización del movimiento a mostrar
     * @return Detalle de la validación o confirmación del pago
     */
    public static PayCreditCardDetail payCreditCard(String operacion, String idTransaction, String idClient, Account debitAccount, String creditcardNumber, String creditcardBank, String currency, double amount, String reference, double latitude, double longitude) throws BackendConnectorException {
        PayCreditCardDetail detail = null;
        WsPayCreditCardResponse response = null;
        String logMessage;

        try {
            WsPayCreditCardRequest request = new WsPayCreditCardRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);
            request.setMode(operacion);
            request.setAmount(amount);
            request.setClientID(idClient);
            request.setCurrency(currency);
            request.setDebitAccountID(ProductUtils.getBackendID(debitAccount.getExtraInfo()));
            request.setCreditcardID(creditcardNumber);
            request.setBankID(creditcardBank);
            request.setReference(reference);
            request.setLatitude(latitude);
            request.setLongitude(longitude);

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Enviar al backend: {0}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            CreditCards port = WS_CREDITCARDS_POOL.borrowObject();
            try {
                response = port.payCreditCard(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_CREDITCARDS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Respuesta backend: {0}", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logMessage= MessageFormat.format("Error en backend (Srv P1 - Pago de Tarjeta): {0}", errorDescription);
                log.warn(logMessage);

            }

            detail = new PayCreditCardDetail(response);
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return detail;
    }

    public static CreditCard createCreditCard(String idTransaction, String clientId, String cardType) throws BackendConnectorException {
        CreditCard creditCard = null;
        WsCreateCreditCardResponse response = null;
        String logMessage;

        try {
            WsCreateCreditCardRequest request = new WsCreateCreditCardRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);
            request.setClientID(clientId);
            request.setCardType(cardType);

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Enviar al backend: {0}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            CreditCards port = WS_CREDITCARDS_POOL.borrowObject();
            try {
                response = port.createCreditCard(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_CREDITCARDS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Respuesta backend: {0}", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logMessage= MessageFormat.format("Error en backend (Srv T1 - Crear una tarjeta): {0}", errorDescription);
                log.warn(logMessage);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                creditCard = new CreditCard(response);
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return creditCard;
    }
}
