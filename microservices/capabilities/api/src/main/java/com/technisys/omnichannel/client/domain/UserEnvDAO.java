/*
 * Copyright 2019 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.domain.User;

/**
 * @author Marcelo Bruno
 */

public class UserEnvDAO {

    private int idEnvironment;
    private User user;

    public UserEnvDAO() {}

    public UserEnvDAO(int idEnvironment, User user) {
        this.idEnvironment = idEnvironment;
        this.user = user;
    }

    public int getIdEnvironment() {
        return idEnvironment;
    }

    public void setIdEnvironment(int idEnvironment) {
        this.idEnvironment = idEnvironment;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
