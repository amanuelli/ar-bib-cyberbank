/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.client.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.StringWriter;
import java.util.Map;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import com.technisys.omnichannel.client.activities.onboarding.Step5Activity;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.exception.MethodInvocationException;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author sbarbosa
 */
public class TemplatingUtils {

    private static final Logger log = LoggerFactory.getLogger(TemplatingUtils.class);

    // Initialize
    static {
        Properties props = new Properties();
        try {
            ResourceBundle config = ResourceBundle.getBundle("velocity");
            for (String key : config.keySet()) {
                props.put(key, config.getString(key));
            }
        } catch (MissingResourceException ex) {
            String logString = String.format("Couldn't find resource velocity.properties. Using default values. Err: %s", ex.getMessage());
            log.warn(logString);
        }

        if (props.isEmpty()) {
            props.put("runtime.log.logsystem.class", "org.apache.velocity.runtime.log.NullLogChute");

            log.info("Initializing velocity engine using default options and no logging.");
        } else {
            log.info("Initializing velocity engine using [velocity.properties] file.");
        }

        Velocity.init(props);
    }

    private TemplatingUtils() {
        throw new IllegalStateException("Utility class");
    }

    public static String evaluate(String template, Map<String, Object> data) throws IOException {
        try {
            VelocityContext context = new VelocityContext();

            for (Map.Entry<String, Object> entry : data.entrySet()) {
                context.put(entry.getKey(), entry.getValue());
            }

            StringWriter swOut = new StringWriter();
            Velocity.evaluate(context, swOut, "velocity", template);

            return swOut.toString();
        } catch (MethodInvocationException | ParseErrorException | ResourceNotFoundException ex) {
            throw new IOException(ex);
        }
    }

    public static String getHtmlTemplate(String templateName) throws IOException {
        InputStream is = Step5Activity.class.getResourceAsStream(templateName);
        StringBuilder template = new StringBuilder();
        String line;
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            while ((line = reader.readLine()) != null) {
                template.append(line).append("\n");
            }
        } finally {
            is.close();
        }
        return template.toString();
    }

}
