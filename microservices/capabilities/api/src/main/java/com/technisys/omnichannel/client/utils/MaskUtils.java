/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 *
 * @author sbarbosa
 */
public class MaskUtils {

    private MaskUtils(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    public static String maskEmail(String email) {
        String result = "****";
        if (email != null) {
            int index = email.lastIndexOf('@');
            if (index > 0) {
                result += email.substring(index);
            }
        }
        return result;
    }

    public static String maskPhone(String phone) {
        String result = "****";
        if (phone != null) {
            result += org.apache.commons.lang3.StringUtils.right(phone, 3);
        }
        return result;
    }

    /**
     * Masks text from start to end leaving visible only the selected quantity of characters from end to start.
     * @param text Text input to mask.
     * @param visible Amount of visible (not masked) characters. Visible characters go from last to first. 0 will mask the whole text.
     * @param pattern Character used to replace hidden characters. If null '*' will act as default.
     * @return
     */
    public static String maskRange(String text, int visible, String pattern) {
        if(pattern == null){
            pattern = "*";
        }
        final String simbol = pattern;
        int textLength = text.length();
        if(textLength <= visible){
            visible = textLength; // Avoids entering a number larger than texts length causing an exception.
        }
        String endOfText = text.substring(textLength-visible,textLength);
        return IntStream
                .range(0,textLength-(visible + 1))
                .mapToObj(i -> simbol)
                .collect(Collectors.joining("")) + endOfText;
    }
}
