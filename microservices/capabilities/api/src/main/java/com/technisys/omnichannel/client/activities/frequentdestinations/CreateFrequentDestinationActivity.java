/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.frequentdestinations;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.domain.Bank;
import com.technisys.omnichannel.client.handlers.banks.BanksHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Document;
import com.technisys.omnichannel.core.domain.FrequentDestination;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.frequentdestinations.FrequentDestinationsHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
public class CreateFrequentDestinationActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(CreateFrequentDestinationActivity.class);
    public static final String ID = "frequentdestinations.create";
    
    public interface InParams {
        String MODAL = "modal";
        String NAME = "name";
        String PRODUCT_TYPE = "productType";
        String LOCAL_BANK = "localBank";
        String EXTERIOR_BANK = "exteriorBank";
        String ACCOUNT_NUMBER = "accountNumber";
        String EXTERNAL_ACCOUNT_NUMBER = "externalAccountNumber";
        String FOREIGN_ACCOUNT_NUMBER = "foreignAccountNumber";
        String RECIPIENT_NAME = "recipientName";
        String RECIPIENT_DOCUMENT = "recipientDocument";
        String RECIPIENT_ADDRESS = "recipientAddress";
        String CREDIT_CARD = "creditCardNumber";
        String LOAN_NUMBER = "loanNumber";
        String SAVE_DATA = "saveData";
    }

    public interface OutParams {
        String FREQUENT_DESTINATION = "frequentDestination";
    }
    
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            String logMessage;
            boolean isSaveData;
            String modal = request.getParam(InParams.MODAL, String.class);
            String documentCountry = null;
            String documentType = null;
            String document= null;

            List<String> productsTypes = request.getParam(InParams.PRODUCT_TYPE, List.class);
            String productType = productsTypes.get(0);

            Map documentData = request.getParam(InParams.RECIPIENT_DOCUMENT, Map.class);
            if(documentData != null) {
                documentCountry = RequestParamsUtils.getValue(documentData.get("documentCountry"), String.class);
                documentType = RequestParamsUtils.getValue(documentData.get("documentType"), String.class);
                document = RequestParamsUtils.getValue(documentData.get("document"), String.class);
            }
            
            List<String> saveDataParam = request.getParam(InParams.SAVE_DATA, List.class);
            if ("yes".equals(modal)) {
                isSaveData = (saveDataParam != null && !saveDataParam.isEmpty() && "yes".equals(saveDataParam.get(0)));
            } else {
                isSaveData = true;
            }

            FrequentDestination fd = new FrequentDestination();
            
            String accountNumber = null;

            switch (productType) {
                case "account":
                    accountNumber = request.getParam(InParams.ACCOUNT_NUMBER, String.class);
                    break;
                case "externalAccount":
                    accountNumber = request.getParam(InParams.EXTERNAL_ACCOUNT_NUMBER, String.class);
                    break;
                case "foreignAccount":
                    accountNumber = request.getParam(InParams.FOREIGN_ACCOUNT_NUMBER, String.class);
                    break;
                default:
                    logMessage= MessageFormat.format("Unexpected productType value {0}", productType);
                    log.error(logMessage);
                    break;
            }
            
            fd.setAccountNumber(accountNumber);       
            
            fd.setLoanNumber(request.getParam(InParams.LOAN_NUMBER, String.class));            
            fd.setCreditCardNumber(request.getParam(InParams.CREDIT_CARD, String.class));
            fd.setIdEnvironment(request.getIdEnvironment());

            String localBank = null;
            Map map = request.getParam(InParams.LOCAL_BANK, Map.class);
            if(map != null) {
                localBank = RequestParamsUtils.getValue(map.get("id"), String.class);
            }
            fd.setLocalBank(localBank);
            
            Map exteriorBankData = request.getParam(InParams.EXTERIOR_BANK, Map.class);
            if(exteriorBankData != null) {
                Bank bank = BanksHandler.readBank(RequestParamsUtils.getValue(exteriorBankData.get("code"), String.class), RequestParamsUtils.getValue(exteriorBankData.get("type"), String.class));
                bank.setBankCountryLabel(I18nFactory.getHandler().getMessage("country.name." + bank.getBankCountryCode() , request.getLang()));
                fd.setExteriorBank(bank);
            }
            
            String name = request.getParam(InParams.NAME, String.class);
            if (!isSaveData) {
                name = I18nFactory.getHandler().getMessage("frequentDestinations.defaul.name", request.getLang());
            }
            
            fd.setName(name);
            fd.setProductType(productType);
            fd.setRecipientAddress(request.getParam(InParams.RECIPIENT_ADDRESS, String.class));
            fd.setRecipientDocument(new Document(documentCountry, documentType, document));
            fd.setRecipientName(request.getParam(InParams.RECIPIENT_NAME, String.class));
            
            if (isSaveData) {
                FrequentDestinationsHandler.createFrequentDestination(fd);
            }
            
            response.getData().put(OutParams.FREQUENT_DESTINATION, fd);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        
        try {
            result = FormsHandler.getInstance().validateRequest(request);
            if(result.isEmpty()
                && (FrequentDestinationsHandler.readFrequentDestinationByName(request.getIdEnvironment(), request.getParam(InParams.NAME, String.class)) != null)) {
                    result.put("name", "frequentDestinations.form.validation.name.exist");
            }
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (SchedulerException e) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, e);
        }

        return result;
    }
    
}