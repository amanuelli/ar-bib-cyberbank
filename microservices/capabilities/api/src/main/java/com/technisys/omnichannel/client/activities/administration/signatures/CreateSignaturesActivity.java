/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Signature;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.limits.CapForSignature;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DBUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.util.*;

/**
 * author: iocampo
 */
@DocumentedActivity("Creates a new signatures scheme")
public class CreateSignaturesActivity extends Activity {

    public static final String ID = "administration.signatures.create.send";

    public interface InParams {

        @DocumentedParam(type = String[].class, description = "Selected frequencies for transactions with amount")
        String CAP_FREQUENCIES = "capFrequencies";
        @DocumentedParam(type = String[].class, description = "Selected functional groups")
        String FUNCTIONAL_GROUPS = "functionalGroups";
        @DocumentedParam(type = Integer.class, description = "Maximum amount for transactions with amount")
        String MAX_AMOUNT = "maxAmount";
        @DocumentedParam(type = Map.class, description = "Number of signers for each signature level")
        String SIGNATURE_LEVELS_COUNTS = "signatureLevelsCounts";
        @DocumentedParam(type = String.class, description = "Signature type")
        String SIGNATURE_TYPE = "signatureType";
        @DocumentedParam(type = List.class, description = "Products list associated to the schema")
        String PRODUCTS = "products";
        @DocumentedParam(type = boolean.class, description = "Signature dispatch is the last signature to execute transaction")
        String SIGNATURE_DISPATCH = "signatureDispatch";
        @DocumentedParam(type = String.class, description = "Signature alias")
        String SIGNATURE_ALIAS = "signatureAlias";
        
    }

    public interface OutParams {

        @DocumentedParam(type = Integer.class, description = "ID of created signatures scheme")
        String SIGNATURE_ID = "signatureId";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }
            
            String[] capFrequencies = request.getParam(InParams.CAP_FREQUENCIES, String[].class);
            String[] functionalGroups = request.getParam(InParams.FUNCTIONAL_GROUPS, String[].class);
            Double maxAmount = request.getParam(InParams.MAX_AMOUNT, Double.class);
            Map signatureLevelsCounts = request.getParam(InParams.SIGNATURE_LEVELS_COUNTS, Map.class);
            String signatureType = request.getParam(InParams.SIGNATURE_TYPE, String.class);
            boolean signatureDispatch = request.getParam(InParams.SIGNATURE_DISPATCH, boolean.class);
            String signatureAlias = request.getParam(InParams.SIGNATURE_ALIAS, String.class);
            String[] products = request.getParam(InParams.PRODUCTS, String[].class);
            
            // Creamos el registro de la firma
            Signature signature = new Signature();
            signature.setIdEnvironment(request.getIdEnvironment());
            signature.setSignatureType(signatureType);
            signature.setSignatureDispatch(signatureDispatch);
            signature.setSignatureAlias(signatureAlias);
            signature.setProducts(products != null ? Arrays.asList(products) : Collections.emptyList());
            
            if (Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme()) && Signature.TYPE_AMOUNT.equals(signature.getSignatureType())) {
                //cargo los caps
                List<CapForSignature> capList = new ArrayList<>();
                CapForSignature cap = new CapForSignature();
                cap.setChannel(CoreUtils.CHANNEL_ALL);
                cap.setFrequency(capFrequencies[0]);
                cap.setMaximum(maxAmount);
                capList.add(cap);
                signature.setCapList(capList);
            }else if (Environment.ADMINISTRATION_SCHEME_MEDIUM.equals(request.getEnvironmentAdminScheme()) && Signature.TYPE_AMOUNT.equals(signature.getSignatureType())) {
                    List<CapForSignature> capList = new ArrayList<>();
                    double defaultCapMaximum = ConfigurationFactory.getInstance().getDouble(Configuration.LIMITS, "default_cap_signature_" + request.getEnvironmentType());
                    String frequency = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "administration.signatures.topAmount.defaultFrequency");
                    CapForSignature capDefault = new CapForSignature(signature.getIdEnvironment(), CoreUtils.CHANNEL_ALL, signature.getIdSignature(), frequency);
                    capDefault.setMaximum(defaultCapMaximum);
                    capList.add(capDefault);
                    signature.setCapList(capList);
            }
            signature.setSignatureGroup(generateSignatureGroup(signatureLevelsCounts));
            if(functionalGroups != null){
                signature.setSignatureFeatures(Arrays.asList(functionalGroups));
            }
            try (SqlSession session = DBUtils.getInstance().openWriteSession()) {
                LimitsHandlerFactory.getHandler().createSignature(session,signature);
                session.commit();
            }

            response.putItem(OutParams.SIGNATURE_ID, signature.getIdSignature());
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String[] capFrequencies = request.getParam(InParams.CAP_FREQUENCIES, String[].class);
            Double maxAmount = request.getParam(InParams.MAX_AMOUNT, Double.class);
            Map signatureLevelsCounts = request.getParam(InParams.SIGNATURE_LEVELS_COUNTS, Map.class);
            String signatureType = request.getParam(InParams.SIGNATURE_TYPE, String.class);
            Boolean signatureDispatch = request.getParam(InParams.SIGNATURE_DISPATCH, Boolean.class);

            if (MapUtils.isEmpty(signatureLevelsCounts)) {
                result.put("signatureGroup", "administration.signatures.emptySignatureGroup");
            } else {
                if (signatureLevelsCounts.values().stream().allMatch(value -> value.equals(0))) {
                    result.put("signatureGroup", "administration.signatures.mustSelectSignatures");
                } else if (signatureLevelsCounts.values().stream().anyMatch(value -> (int)value > 5)) {
                    result.put("signatureGroup", "administration.signatures.tooManySignatures");
                }
            }

            if(signatureDispatch == null)
                result.put(InParams.SIGNATURE_DISPATCH, "administration.signatures.emptySignatureDispatch");
            if (StringUtils.isBlank(signatureType)) {
                result.put("signatureType", "administration.signatures.emptySignatureType");
            } else if (Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme()) && Signature.TYPE_AMOUNT.equals(signatureType)) {
                List<String> enabledCapFrequencies = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "frontend.channels.enabledFrequencies");

                //cargo los caps
                if (maxAmount == null || maxAmount <= 0 || maxAmount > 9_999_999_999_999d) {
                    result.put("maxAmount", "administration.signatures.negativeAmount");
                }

                if (StringUtils.isBlank(capFrequencies[0])) {
                    result.put("capFrequency", "administration.signatures.emptyCapFrequency");
                } else if (!enabledCapFrequencies.contains(capFrequencies[0])) {
                    result.put("capFrequency", "administration.signatures.invalidCapFrequency");
                }
            } else if (!(Signature.TYPE_AMOUNT.equals(signatureType) || Signature.TYPE_NO_AMOUNT.equals(signatureType))) {
                result.put("signatureType", "administration.signatures.invalidSignatureType");
            }

            return result;

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    public static String generateSignatureGroup(Map signatureLevelsCount) {
        StringBuilder signatureGroup = new StringBuilder();
        
        signatureLevelsCount.forEach((key, value) ->
            signatureGroup.append(StringUtils.repeat(key.toString(), (int)value))
        );
        
        return signatureGroup.toString();
    }
}