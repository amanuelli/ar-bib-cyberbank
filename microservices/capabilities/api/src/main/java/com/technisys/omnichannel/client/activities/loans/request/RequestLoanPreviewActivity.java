/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */
package com.technisys.omnichannel.client.activities.loans.request;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.util.Map;

/**
 * @author Cristobal Meneses
 */
@DocumentedActivity("Preview Request Loand")
public class RequestLoanPreviewActivity extends Activity {

    public static final String ID = "request.loan.preview";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Loan Type")
        String LOAN_TYPE = "loanType";

        @DocumentedParam(type = String.class, description = "Amount to request")
        String AMOUNT_TO_REQUEST = "amountToRequest";

        @DocumentedParam(type = String.class, description = "Amount of fees")
        String AMOUNT_OF_FEES = "amountOfFees";

        @DocumentedParam(type = String.class, description = "Authorize debit to account")
        String IS_AUTHORIZE_DEBIT_TO_ACCOUNT = "iAuthorizeDebitToAccount";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            return FormsHandler.getInstance().validateRequest(request);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (SchedulerException e) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, e);
        }
    }
}
