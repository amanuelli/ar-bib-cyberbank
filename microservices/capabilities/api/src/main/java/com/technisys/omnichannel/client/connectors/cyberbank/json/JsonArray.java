package com.technisys.omnichannel.client.connectors.cyberbank.json;

import org.json.JSONArray;

public class JsonArray extends JSONArray {
    public JsonArray build() {
        return this;
    }

    public JsonArray add(Object value) {
        put(value);
        return this;
    }
}