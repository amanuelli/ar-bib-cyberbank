/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnector;
import com.technisys.omnichannel.rubicon.core.deposits.WsDepositMovement;
import java.util.Date;

/**
 *
 * @author aelters
 */
public class StatementDeposit {

    private String idAccount;
    private int idStatement;
    private Date date;
    private Date accountingDate;
    private String accountCurrency;
    private String concept;
    private String reference;
    private Double debit;
    private Double credit;
    private Double balance;
    private String productAccountLabel;
    private String productAccountShortLabel;
    private String storedFileName;
    private String note;
    private String voucher;

    public StatementDeposit() {
    }

    public StatementDeposit(WsDepositMovement movement) {
        this.idStatement = Integer.parseInt(movement.getMovementId());
        this.date = RubiconCoreConnector.convertXMLGregorianCalendarToDate(movement.getDate());
        this.accountingDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(movement.getAccountingDate());
        this.concept = movement.getConcept();
        this.reference = movement.getReference();
        this.debit = movement.getDebit();
        this.credit = movement.getCredit();
        this.balance = movement.getBalance();
        this.voucher = movement.getVoucher();
    }

    public String getIdAccount() {
        return idAccount;
    }

    public void setIdAccount(String idAccount) {
        this.idAccount = idAccount;
    }

    public int getIdStatement() {
        return idStatement;
    }

    public void setIdStatement(int idStatement) {
        this.idStatement = idStatement;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getAccountingDate() {
        return accountingDate;
    }

    public void setAccountingDate(Date accountingDate) {
        this.accountingDate = accountingDate;
    }

    public String getAccountCurrency() {
        return accountCurrency;
    }

    public void setAccountCurrency(String accountCurrency) {
        this.accountCurrency = accountCurrency;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Double getDebit() {
        return debit;
    }

    public void setDebit(Double debit) {
        this.debit = debit;
    }

    public Double getCredit() {
        return credit;
    }

    public void setCredit(Double credit) {
        this.credit = credit;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

    public String getProductAccountLabel() {
        return productAccountLabel;
    }

    public void setProductAccountLabel(String productAccountLabel) {
        this.productAccountLabel = productAccountLabel;
    }

    public String getProductAccountShortLabel() {
        return productAccountShortLabel;
    }

    public void setProductAccountShortLabel(String productAccountShortLabel) {
        this.productAccountShortLabel = productAccountShortLabel;
    }

    public String getStoredFileName() {
        return storedFileName;
    }

    public void setStoredFileName(String storedFileName) {
        this.storedFileName = storedFileName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getVoucher() {
        return voucher;
    }

    public void setVoucher(String voucher) {
        this.voucher = voucher;
    }
}
