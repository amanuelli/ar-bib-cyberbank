/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.forms.preloaders;

import java.util.HashMap;
import java.util.Map;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.forms.FormPreloader;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;

/**
 *
 * @author pantusap
 */

public class ModifyCreditLetterPreloader implements FormPreloader {

    public interface OutParams {

        String NOTE = "note";
        String DISCLAIMER = "disclaimer";
    }

    @Override
    public Map<String, Object> execute(Form form, Request request) {
       
        Map<String, Object> formData = new HashMap<String, Object>();
        I18n i18n = I18nFactory.getHandler();
        String lang = request.getLang();
    
        formData.put(OutParams.NOTE, i18n.getMessage("client.modifyCreditLetter.note", lang));
        formData.put(OutParams.DISCLAIMER, i18n.getMessage("client.modifyCreditLetter.disclaimer", lang));
                 
        return formData;
    }
}
