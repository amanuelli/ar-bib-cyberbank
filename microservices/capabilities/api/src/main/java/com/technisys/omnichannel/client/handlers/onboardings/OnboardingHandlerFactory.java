/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */
package com.technisys.omnichannel.client.handlers.onboardings;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.OnboardingHandlerUnavailableException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OnboardingHandlerFactory {

    private  OnboardingHandlerFactory() {
        throw new IllegalStateException("Utility class");
    }

    private static IOnboardingHandler instance;

    public static final Logger LOG = LoggerFactory.getLogger(OnboardingHandlerFactory.class);

    public static synchronized IOnboardingHandler getInstance(){
        if(instance == null) {
            initOnboardingHandlerInstance();
        }
        return instance;
    }

    private static synchronized void initOnboardingHandlerInstance() {
        try {
            String entitlementHandlerFQN = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,
                    "core.onboardingHandler.componentFQN",
                    "com.technisys.omnichannel.client.handlers.onboardings.ms.OnboardingsHandlerMS");

            instance = (IOnboardingHandler) Class.forName(entitlementHandlerFQN).newInstance();

            LOG.info("Using the {} onboarding handler class", entitlementHandlerFQN);

        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException ex) {
            throw new OnboardingHandlerUnavailableException(ReturnCodes.MICROSERVICE_ERROR, "Error instantiating onboarding handler class", ex);
        }
    }



}

