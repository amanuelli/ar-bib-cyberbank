/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain.comparators;

import com.technisys.omnichannel.core.domain.Product;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author sbarbosa
 */
public class ProductComparator implements Comparator<Product> {

    private static final List<String> DEFAULT_PRODUCT_TYPE_ORDER = Arrays.asList("CA", "CC", "PF", "PA", "PI", "TC");

    private final List<String> productOrderList;

    // Se utilizan para ordenar al final los productos que no se encuentran en la lista de orden
    private int freeIndex;
    private final Map<String, Integer> unknownProductTypeIndex = new HashMap<>();

    public ProductComparator() {
        productOrderList = DEFAULT_PRODUCT_TYPE_ORDER;

        freeIndex = productOrderList.size();
    }

    public ProductComparator(List<String> productOrderList) {
        this.productOrderList = productOrderList != null ? productOrderList : new ArrayList<String>();

        freeIndex = this.productOrderList.size();
    }

    @Override
    public int compare(Product p1, Product p2) {
        return Integer.compare(getProductIndex(p1), getProductIndex(p2));
    }

    private synchronized int getProductIndex(Product p) {
        int index = productOrderList.indexOf(p.getProductType());
        if (index < 0) {
            Integer unknownIndex = unknownProductTypeIndex.get(p.getProductType());
            if (unknownIndex == null) {
                index = freeIndex++;
                unknownProductTypeIndex.put(p.getProductType(), index);
            } else {
                index = unknownIndex;
            }
        }

        return index;
    }
}
