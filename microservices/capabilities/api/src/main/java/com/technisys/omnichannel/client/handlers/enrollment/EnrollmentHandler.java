/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.handlers.enrollment;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.domain.UserEnvDAO;
import com.technisys.omnichannel.client.handlers.environment.EnvironmentHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.notifications.legacy.SMSCommunicationsDispatcher;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.ReturnCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DBUtils;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.security.SecureRandom;
import java.util.*;

import static com.technisys.omnichannel.client.ReturnCodes.*;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;

/**
 *
 * @author sbarbosa
 */
public class EnrollmentHandler {

    private EnrollmentHandler(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    private static final Logger log = LoggerFactory.getLogger(EnrollmentHandler.class);

    public static final int VERIFICATION_CODE_VALID = 0;
    public static final int VERIFICATION_CODE_INVALID = 1;
    public static final int VERIFICATION_CODE_MAX_ATTEMPTS_REACHED = 2;

    public static ReturnCode validateInvitationCode(InvitationCode invitation) {
        //si el codigo es nulo, el codigo no es valido
        if (invitation == null) {
            return INVALID_INVITATION_CODE;
        } else {
            //valido tiempo de validez
            Long validityInMillis = ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "client.enrollment.invitationCode.validity");
            if (validityInMillis == null) {
                validityInMillis = 172800000L; // 48hs por defecto
            }

            Calendar now = Calendar.getInstance();
            long diff = now.getTimeInMillis() - invitation.getCreationDate().getTime();

            if (diff > validityInMillis) {
                //el codigo de invitacion esta vencido
                return INVITATION_CODE_EXPIRED;
            } else if (InvitationCode.STATUS_EXPIRED.equals(invitation.getStatus())) {
                //el codigo de invitacion ya fue utilizado
                return INVITATION_CODE_EXPIRED;
            } else if (InvitationCode.STATUS_USED.equals(invitation.getStatus())) {
                //el codigo de invitacion ya fue utilizado
                return INVITATION_CODE_ALREADY_USED;
            } else if (InvitationCode.STATUS_CANCELED.equals(invitation.getStatus())) {
                //el codigo de invitacion ya fue utilizado
                return INVITATION_CODE_CANCELED;
            }

            if (StringUtils.isBlank(invitation.getAccessType()) && StringUtils.isBlank(invitation.getGroups())) {
                log.error("The invitation code {} has no role or group.", invitation.getInvitationCode());
                return INVALID_INVITATION_CODE_DATA;
            }
        }

        return null;
    }

    public static void sendVerificationCode(InvitationCode invitation, String lang) throws ActivityException, IOException, MessagingException {
        if (invitation == null) {
            throw new ActivityException(INVALID_INVITATION_CODE);
        }
        String sendChannel = Constants.TRANSPORT_SMS.equals(invitation.getChannelSent()) ? Constants.TRANSPORT_MAIL : Constants.TRANSPORT_SMS;

        // Genero el PIN de verificación
        SecureRandom random = new SecureRandom();
        int length = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "enrollment.verificationCode.length", 4);
        String pin = org.apache.commons.lang3.StringUtils.leftPad(String.valueOf(((Double) (random.nextDouble() * Math.pow(10, length))).longValue()), length, "0");

        String subject = I18nFactory.getHandler().getMessage("enrollment.step1.verificationCode.notification." + sendChannel + ".subject", lang);

        HashMap<String, String> fillers = new HashMap<>();

        fillers.put("PIN", pin);
        String body = I18nFactory.getHandler().getMessage("enrollment.step1.verificationCode.notification." + sendChannel + ".body", lang, fillers);

        switch (sendChannel) {
            case Constants.TRANSPORT_SMS:
                try {
                    NotificationsHandlerFactory.getHandler().sendSMS(invitation.getMobileNumber(), body, true);
                } catch (ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
                    throw new MessagingException("Error sending an sms", ex);
                }
                break;
            case Constants.TRANSPORT_MAIL:
                NotificationsHandlerFactory.getHandler().sendEmails(subject, body, Arrays.asList(invitation.getEmail()), null, lang, true);
                break;
            default:
                throw new ActivityException(INVALID_INVITATION_CODE, "El canal de envío del código de invitación es inválido");
        }

        InvitationCodesHandler.setVerificationCode(invitation.getId(), SecurityUtils.encrypt(pin));
        InvitationCodesHandler.addSendRetry(invitation.getId());
    }

    public static int validateVerificationCode(InvitationCode invitation, String verificationCode) throws ActivityException, IOException {
        if (invitation == null) {
            throw new ActivityException(INVALID_INVITATION_CODE);
        }

        int result = VERIFICATION_CODE_INVALID;
        int maxAttempts = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "enrollment.verificationCode.maxFailedAttempts", 5);
        if (invitation.getVerificationCodeFailedAttempts() < maxAttempts - 1) {
            if (SecurityUtils.decrypt(invitation.getVerificationCode()).equals(verificationCode)) {
                result = VERIFICATION_CODE_VALID;
            }
        } else {
            result = VERIFICATION_CODE_MAX_ATTEMPTS_REACHED;
        }

        if (VERIFICATION_CODE_VALID != result) {
            InvitationCodesHandler.addFailedAttempts(invitation.getId());
            
            if (result == VERIFICATION_CODE_MAX_ATTEMPTS_REACHED) {
                InvitationCodesHandler.cancelInvitationCode(invitation.getId());
            }
        }

        return result;
    }

    public static UserEnvDAO createUser(String idTransaction, String username, int securitySeal, String lang,
                                        InvitationCode invitation, ClientUser backendClient) throws IOException, BackendConnectorException, ActivityException {
        int idEnvironment = -1;

        User user = new User();
        user.setUsername(username);
        user.setEmail(invitation.getEmail());
        user.setIdSeal(securitySeal);
        user.setIdUserStatus(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "enrollment.newUser.status"));
        user.setCreationDate(new Date());
        user.setLang(lang);

        // Setea el nivel de confianza del usuario (si no esta en el banco no es confiable)
        user.setTrustfull(invitation.isBackendUser());

        //- 
        // Si el usuario no es del banco se utilizan los datos de la invitación
        if (backendClient == null) {
            user.setFirstName(StringUtils.trimToEmpty(invitation.getFirstName()));
            user.setLastName(StringUtils.trimToEmpty(invitation.getLastName()));
            user.setMobileNumber(invitation.getMobileNumber());
            user.setSsnid(invitation.getSsnid());
        } else {
            user.setFirstName(StringUtils.trimToEmpty(backendClient.getFirstName()));
            user.setLastName(StringUtils.trimToEmpty(backendClient.getLastName()));
            user.setMobileNumber(backendClient.getMobileNumber());
            if(invitation != null){ user.setSsnid(invitation.getSsnid()); }
        }

        user.setDocumentCountry(invitation.getDocumentCountry());
        user.setDocumentType(invitation.getDocumentType());
        user.setDocumentNumber(invitation.getDocumentNumber());
          
        Map<String, String> extendedData = new HashMap<>();

        if (backendClient != null) {
            extendedData = backendClient.getAdditionalData();
        }

        // Convierto los grupos de la invitación (si es que vinieron) a una lista de enteros
        List<Integer> groups = null;
        if (StringUtils.isNotBlank(invitation.getGroups())) {
            groups = new ArrayList<>();
            for (String group : StringUtils.split(invitation.getGroups(), ",")) {
                groups.add(Integer.parseInt(group));
            }
        }

        try (SqlSession session = DBUtils.getInstance().openWriteSession()) {
            
            if(AccessManagementHandlerFactory.getHandler().usernameExists(user.getUsername())){
                log.error("Error attempting to insert the user. The username {}  already exists", user.getUsername());
                InvitationCodesHandler.setInvitationCodeAsUsed(invitation.getId());
                throw new ActivityException(ReturnCodes.USER_ALREADY_EXISTS);
            }
            
            log.info("Creating data for the user (users) {} ", user.getIdUser());
            
            AccessManagementHandlerFactory.getHandler().createUser(session, user, null, null, extendedData);

            log.info("Creating data for the relation user-backend (client_users) {} ", user.getIdUser());

            if(invitation.getProductGroupId() != null) {
                log.info("Associating user to account {} ", invitation.getProductGroupId());
            } else{
                log.info("Associating user to environment {} ", invitation.getIdEnvironment());
            }

            idEnvironment = EnvironmentHandler.getInstance().addClientToEnvironment(session, idTransaction, user, invitation, groups);

            session.commit();
        }

        return new UserEnvDAO(idEnvironment, user);
    }

    public static void associateUserToAccount(String idTransaction, String username, InvitationCode invitation) throws IOException, BackendConnectorException {
        // Convierto los grupos de la invitación (si es que vinieron) a una lista de enteros
        List<Integer> groups = null;
        if (StringUtils.isNotBlank(invitation.getGroups())) {
            groups = new ArrayList<>();
            for (String group : StringUtils.split(invitation.getGroups(), ",")) {
                groups.add(Integer.parseInt(group));
            }
        }

        try (SqlSession session = DBUtils.getInstance().openWriteSession()) {
            User user = AccessManagementHandlerFactory.getHandler().getUser(username);

            EnvironmentHandler.getInstance().addClientToEnvironment(session, idTransaction, user, invitation, groups);

            session.commit();
        }
    }

}
