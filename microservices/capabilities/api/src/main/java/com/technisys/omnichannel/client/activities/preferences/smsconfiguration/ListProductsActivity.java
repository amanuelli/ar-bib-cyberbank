/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.smsconfiguration;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.List;

/**
 *
 */
public class ListProductsActivity extends Activity {

    public static final String ID = "preferences.smsconfiguration.products.list";

    public interface OutParams {

        String PRODUCTS = "products";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            //obtengo los tipos de productos y los permisos para esos tipos de productos
            List<String> productTypes = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "preferences.sms.configuration.productTypes");
            List<String> permissions = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "preferences.sms.configuration.productPermissions");

            //leo todos los productos del ambiente de los tipos dados y que tengan al menos un permisos de los dados
            List<Product> products = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), permissions, productTypes);

            //cargo las labels
            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            String alias;
            for(Product product : products){
                alias = product.getProductAlias();
                product.setProductAlias(null);
                product.setShortLabel(pLabeler.calculateShortLabel(product));
                product.setProductAlias(alias);

                product.setLabel(pLabeler.calculateLabel(product));
            }
            
            response.putItem(OutParams.PRODUCTS, products);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}