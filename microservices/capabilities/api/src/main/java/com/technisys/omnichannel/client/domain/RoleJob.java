/*
 *  Copyright 2014 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import java.util.Objects;

/**
 * Represents Jobe Role
 */
public class RoleJob {

    private int idRoleJob;
    private String name;
    private String codeName;

    public RoleJob() {}

    public RoleJob(int idRoleJob, String name) {
        this.idRoleJob = idRoleJob;
        this.name = name;
    }

    public RoleJob(int idRoleJob, String name, String codeName) {
        this.idRoleJob = idRoleJob;
        this.name = name;
        this.codeName = codeName;
    }

    public int getIdRoleJob() {
        return idRoleJob;
    }

    public void setIdRoleJob(int idRoleJob) {
        this.idRoleJob = idRoleJob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCodeName() {
        return codeName;
    }

    public void setCodeName(String codeName) {
        this.codeName = codeName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoleJob roleJob = (RoleJob) o;
        return idRoleJob == roleJob.idRoleJob &&
                Objects.equals(name, roleJob.name) &&
                Objects.equals(codeName, roleJob.codeName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRoleJob, name, codeName);
    }

    @Override
    public String toString() {
        return "RoleJob{" +
                "idRoleJob=" + idRoleJob +
                ", name='" + name + '\'' +
                ", codeName='" + codeName + '\'' +
                '}';
    }
}
