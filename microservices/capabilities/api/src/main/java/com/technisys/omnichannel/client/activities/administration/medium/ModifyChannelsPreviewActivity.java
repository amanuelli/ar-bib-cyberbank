package com.technisys.omnichannel.client.activities.administration.medium;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;

/**
 * Modify user channels in medium scheme. use this to modify user channels in medium schema
 */
@DocumentedActivity("Modify user channels in medium scheme preview")
public class ModifyChannelsPreviewActivity extends Activity {

    public static final String ID = "administration.medium.modify.channels.preview";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "ID of the user to apply changes")
        String ID_USER = "idUser";
        @DocumentedParam(type = String[].class, description = "Channels to be enabled")
        String ENABLED_CHANNELS = "enabledChannels";
        @DocumentedParam(type = Double[].class, description = "Amounts to apply by matching index of enabledChannels param")
        String MAX_AMOUNTS = "maxAmounts";
        @DocumentedParam(type = String[].class, description = "Frequencies to apply by matching index of enabledChannels param")
        String CAP_FREQUENCIES = "capFrequencies";
    }

    public interface OutParams {
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        response.setReturnCode(ReturnCodes.OK);

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return ModifyChannelsActivity.validateFields(request);
    }
}
