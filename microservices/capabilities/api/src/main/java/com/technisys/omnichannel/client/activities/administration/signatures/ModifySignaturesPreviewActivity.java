/*
 *  Copyright (c) 2020 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

/**
 * Modify signatures scheme (PREVIEW)
 */

@DocumentedActivity("Modify signatures scheme (PREVIEW)")
public class ModifySignaturesPreviewActivity extends Activity {
    public static final String ID = "administration.signatures.modify.preview";

    public interface InParams {
        @DocumentedParam(type = String[].class, description = "Selected frequencies for transactions with amount")
        String CAP_FREQUENCIES = "capFrequencies";

        @DocumentedParam(type = Integer.class, description = "Maximum amount for transactions with amount")
        String MAX_AMOUNT = "maxAmount";

        @DocumentedParam(type = Integer.class, description = "Signatures scheme's identifier")
        String SIGNATURE_ID = "signatureId";

        @DocumentedParam(type = Map.class, description = "Number of signers for each signature level")
        String SIGNATURE_LEVELS_COUNTS = "signatureLevelsCounts";

        @DocumentedParam(type = String.class, description = "Signature type")
        String SIGNATURE_TYPE = "signatureType";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return ModifySignaturesActivity.validateFields(request);
    }
}