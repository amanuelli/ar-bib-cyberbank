/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.desktop;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.desktop.LayoutHandler;
import com.technisys.omnichannel.core.domain.Widget;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;
import java.util.List;

/**
 * Receives the widget list that the user has configured on his desktop and saves it.
 */
@DocumentedActivity("Desktop save layout")
public class SaveLayoutActivity extends Activity {

    public static final String ID = "desktop.saveLayout";
    
    public interface InParams {
        @DocumentedParam(type = List.class, description = "Widget list to be saved")
        String WIDGETS = "widgets";
    }

    public interface OutParams {
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        Widget[] widgets = request.getParam(InParams.WIDGETS, Widget[].class);
        
        try {
            LayoutHandler.saveLayout(request.getIdUser(), request.getIdEnvironment(), widgets);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
