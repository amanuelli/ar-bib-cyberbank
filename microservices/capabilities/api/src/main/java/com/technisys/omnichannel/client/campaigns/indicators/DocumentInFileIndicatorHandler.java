/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.campaigns.indicators;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.technisys.omnichannel.client.domain.indicators.DocumentInFileIndicatorInfo;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.campaigns.CampaignsDataAccess;
import com.technisys.omnichannel.core.campaigns.CampaignsHandler;
import com.technisys.omnichannel.core.campaigns.IndicatorHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.CampaignBigContent;
import com.technisys.omnichannel.core.domain.CampaignContextParameters;
import com.technisys.omnichannel.core.domain.Document;
import com.technisys.omnichannel.core.domain.Indicator;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.HtmlUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author fpena
 */
public class DocumentInFileIndicatorHandler extends IndicatorHandler{

    private static final Logger log = LoggerFactory.getLogger(DocumentInFileIndicatorHandler.class);
    private static final String OPERATOR_ALL = "all";
    private static final String OPERATOR_NONE = "none";
    private static final List<String> OPERATORS;
    private static Cache<String, Object> cache = null;
    
    static{
        OPERATORS = new ArrayList();
        OPERATORS.add(OPERATOR_ALL);
        OPERATORS.add(OPERATOR_NONE);
        
        // Maximun size
        int maximumSize = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "campaigns.indicators.documentInFile.cache.maximumSize", 50);

        // ExpireAfter
        long expire = ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "campaigns.indicators.documentInFile.cache.expireAfter");

        cache = CacheBuilder.newBuilder()
                .maximumSize(maximumSize)
                .expireAfterWrite(expire, TimeUnit.MILLISECONDS)
                .build();

    }
    
    @Override
    public String getDescription(Indicator indicator, String lang) throws IOException{
        Gson gson = new GsonBuilder().create();
        DocumentInFileIndicatorInfo indicatorInfo = gson.fromJson(indicator.getData(), DocumentInFileIndicatorInfo.class);
        
        StringBuilder builder = new StringBuilder();
        builder.append("<b>");
        
        builder.append(I18nFactory.getHandler().getMessage("backoffice.campaigns.indicators.documentInFile.operator." + indicatorInfo.getOperator() +".description", lang));
       
        builder.append("</b> ");
        
        boolean first = true;
        int docsToShow = 5;
        
        List<Document> documents;
                
        CampaignBigContent content = CampaignsHandler.getInstance().readBigContent(indicatorInfo.getIdContent());
        if (content != null){
            if (StringUtils.isEmpty(content.getProcessedContent())){
                content = CampaignsHandler.getInstance().readBigContentBinary(indicatorInfo.getIdContent());
                documents = readDocuments(content, null);
            }else{
                documents = Arrays.asList(gson.fromJson(content.getProcessedContent(), Document[].class));
            }

            for (int i=0; i<documents.size() && i < docsToShow; i++){
                if (!first){
                    builder.append(", ");
                }

                builder.append(documents.get(i).getDocumentCountry()).append(" ").append(documents.get(i).getDocumentType()).append(" ").append(documents.get(i).getDocument());
                first = false;
            }
            if (documents.size() > docsToShow){
                HashMap<String, String> fillers = new HashMap<>();
                fillers.put("records", String.valueOf(documents.size() - docsToShow));
                
                builder.append(" ").append(I18nFactory.getHandler().getMessage("backoffice.campaigns.indicators.documentInFile.description.recordsHint", lang, fillers));
            }
        }

        return builder.toString();
    }

    @Override
    public Map<String, String> validate(int ruleId, int indicatorId, String jsonData) throws IOException{
        
        Map<String, String> result = new HashMap<>();
        
        JSONObject jsonObj = new JSONObject(jsonData);
        
        String operator = jsonObj.getString("operator");
        if (StringUtils.isEmpty(operator) || !OPERATORS.contains(operator)){
            result.put(ruleId + "_" + indicatorId + "_operator", "backoffice.campaign.rule.documentInFile.noOperator");
        }
        
        if (jsonObj.getInt("idNewContent") > 0){ //si es > 0 y diferente al idContent está editando, por lo tanto ha subido un nuevo archivo
            CampaignBigContent content = CampaignsHandler.getInstance().readBigContentBinary(jsonObj.getInt("idNewContent"));
            if (content == null || content.getContent() == null || readDocuments(content, 1).isEmpty()){
                result.put(ruleId + "_" + indicatorId + "_file", "backoffice.campaign.rule.documentInFile.emptyFile");
            }
        }else if (jsonObj.isNull("idContent") || jsonObj.getInt("idContent") <= 0){
            //no venia un idNewContent y tampoco me viene el actual
            result.put(ruleId + "_" + indicatorId + "_file", "backoffice.campaign.rule.documentInFile.noFileSelected");
        }
        
        return result;
    }
    
    @Override
    public Map<String, Object> getFieldDataForConfiguration(String lang) throws IOException {
        Map<String, Object> data = new HashMap();
        
        data.put("operators", OPERATORS);
        
        return data;
    }
    
    /**
     * Para quien actualice este método. Tener en cuenta que tiene que ser lo más performante posible
     * @param indicator
     * @param contextParameters
     * @return
     * @throws IOException 
     */
    @Override
    public boolean evaluate(Indicator indicator, CampaignContextParameters contextParameters) throws IOException {
        
        if (StringUtils.isEmpty(contextParameters.getIdUser())){
            return false;
        }
        Document doc = AccessManagementHandlerFactory.getHandler().getUserDocument(contextParameters.getIdUser());
        if (doc == null){
            return false;
        }
        
        CacheObject cacheObject = (CacheObject)cache.getIfPresent(indicator.getIdRule() + "-" + indicator.getIdIndicator());
        
        //verificar si es null o si la fecha del indicador de la cache es mas vieja que la del indicador del parametro
        if (cacheObject == null || cacheObject.indicator.getModificationDate().before(indicator.getModificationDate())){
            log.debug("Recargando cache de documentos.");
            Gson gson = new GsonBuilder().create();
            DocumentInFileIndicatorInfo indicatorInfo = gson.fromJson(indicator.getData(), DocumentInFileIndicatorInfo.class);
            CampaignBigContent content = CampaignsHandler.getInstance().readBigContent(indicatorInfo.getIdContent());
        
            List<Document> documents = null;
            if (content.getProcessedContent() != null){
                documents = Arrays.asList(gson.fromJson(content.getProcessedContent(), Document[].class));
            }
            
            cacheObject = new CacheObject();
            cacheObject.documents = documents;
            cacheObject.indicator = indicator;
            cacheObject.indicatorInfo = indicatorInfo;
            
            cache.put(indicator.getIdRule() + "-" + indicator.getIdIndicator(), cacheObject);   
        }
            
        boolean found = false;
        if (cacheObject.documents != null){
            found = cacheObject.documents.contains(doc);
        }
        
        switch (cacheObject.indicatorInfo.getOperator()){
            case OPERATOR_ALL:
                return found;
            case OPERATOR_NONE:
                return !found;
            default:
                String logString = String.format("Unexpected operator value %s", cacheObject.indicatorInfo.getOperator());
                log.error(logString);
                break;
        }
        
        return false;
    }
    
    @Override
    public void save (SqlSession session, Indicator indicator) throws IOException{
        Gson gson = new GsonBuilder().create();
        DocumentInFileIndicatorInfo indicatorInfo = gson.fromJson(indicator.getData(), DocumentInFileIndicatorInfo.class);
        if (indicatorInfo.getIdNewContent() > 0){ 
            CampaignBigContent content = CampaignsHandler.getInstance().readBigContentBinary(session, indicatorInfo.getIdNewContent());
            List<Document> record = readDocuments(content, null);

            Document[] documents = new Document[record.size()];
            documents = record.toArray(documents);
            
            CampaignsDataAccess.getInstance().updateIndicatorBigContentProcessed (session, indicator.getIdIndicator(), indicatorInfo.getIdContent(), gson.toJson(documents));
        }
        
        //actualizo la data quitando el idNewContent, para que el proximo save, o validate no de problemas
        indicatorInfo.setIdNewContent(0);
        
        CampaignsDataAccess.getInstance().updateIndicatorData(session, indicator.getIdIndicator(), gson.toJson(indicatorInfo));
    };
    
    private List<Document> readDocuments (CampaignBigContent content, Integer count) throws IOException{
        InputStreamReader isr = new InputStreamReader(new ByteArrayInputStream(content.getContent()), "UTF-8");
        BufferedReader br = new BufferedReader(isr);

        List<Document> result = new ArrayList();
        
        String[] parts;
        Document document;

        String line = br.readLine();
        while(line != null && (count == null || result.size() < count)){
            if (StringUtils.isNotEmpty(line)
                && (line.length() < 255)){
                //no procesamos lineas mayores a 254, es también una forma de prevenir archivos binarios, etc....
                    parts = line.split(",");
                    if (parts.length == 3){
                        document = new Document();
                        document.setDocumentCountry(HtmlUtils.stripHTMLTags(parts[0]));
                        document.setDocumentType(HtmlUtils.stripHTMLTags(parts[1]));
                        document.setDocument(HtmlUtils.stripHTMLTags(parts[2]));
                        result.add(document);
                    }
            }
            line = br.readLine();
        } 
        
        return result;
    }
}

class CacheObject{
    Indicator indicator;
    DocumentInFileIndicatorInfo indicatorInfo;
    List<Document> documents;
}
