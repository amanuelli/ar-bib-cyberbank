package com.technisys.omnichannel.client.activities.pay.multiline;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.util.Map;

public class PaySalaryPreviewActivity extends Activity {

    public static final String ID = "pay.multiline.salary.preview";

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        response.setReturnCode(ReturnCodes.OK);

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            return FormsHandler.getInstance().validateRequest(request);
        } catch (SchedulerException ex) {
            throw new ActivityException(com.technisys.omnichannel.client.ReturnCodes.SCHEDULER_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(com.technisys.omnichannel.client.ReturnCodes.IO_ERROR, ex);
        }
    }
}
