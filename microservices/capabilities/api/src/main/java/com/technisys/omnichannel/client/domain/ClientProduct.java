/*
 *  Copyright 2014 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.clients.ClientHandler;
import com.technisys.omnichannel.core.domain.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Map;

/*
 * @author Diego Curbelo
 */
public class ClientProduct extends Product {

    public static final String CURRENCY_USD = "USD";
    public static final String CURRENCY_UYP = "UYU";

    private static final Logger log = LoggerFactory.getLogger(ClientProduct.class);

    protected String number;
    protected String ownerName;
    protected double consolidatedAmount;
    protected transient Map<String, Boolean> permissions;

    public ClientProduct() {
    }

    public ClientProduct(Product product) {
        super(product.getIdProduct(), product.getProductType(), product.getLabel(), product.getProductAlias());
        setExtraInfo(product.getExtraInfo());
        this.number = ProductUtils.getNumber(product.getExtraInfo());
        client = product.getClient();
    }

    public ClientProduct(Map<String, String> fieldsMap) {
        setExtraInfo(ProductUtils.generateExtraInfo(fieldsMap));
        setIdProduct(ProductUtils.generateIdProduct(getExtraInfo()));
        setProductType(fieldsMap.get(ProductUtils.FIELD_PRODUCT_TYPE));

        try {
            client = ClientHandler.readClient(fieldsMap.get(ProductUtils.FIELD_CLIENT_ID));
        } catch (Exception ex){
            log.warn("Error creating reading client: " + ex.getMessage(), ex);
        }

        this.ownerName = fieldsMap.get(ProductUtils.FIELD_OWNER_NAME);
        this.number = fieldsMap.get(ProductUtils.FIELD_PRODUCT_NUMBER);
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getConsolidatedAmount() {
        return consolidatedAmount;
    }

    public void setConsolidatedAmount(double consolidatedAmount) {
        this.consolidatedAmount = consolidatedAmount;
    }

    public Map<String, Boolean> getPermissions() {
        return permissions;
    }

    public void setPermissions(Map<String, Boolean> permissions) {
        this.permissions = permissions;
    }
}
