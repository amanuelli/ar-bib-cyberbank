/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.fingerprint;
import org.apache.commons.lang3.StringUtils;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.SessionException;
import com.technisys.omnichannel.core.session.Session;
import com.technisys.omnichannel.core.session.SessionHandlerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.technisys.omnichannel.client.utils.HeadersUtils.buildRequestHeaders;

/**
 * Save user fingerprint session configuration
 */
@DocumentedActivity("Save FingerPrint Configuration")
public class FingerPrintConfigurationSaveActivity extends Activity {

    public static final String ID = "preferences.fingerprint.save";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "User device id to save fingerprint session")
        String DEVICE_ID = "deviceId";
        @DocumentedParam(type = String.class, description = "User device model to save fingerprint session")
        String DEVICE_MODEL = "deviceModel";
    }

    public interface OutParams {

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            Session currentSession = SessionHandlerFactory.getInstance().readSession(request.getCredential("accessToken").getValue(), buildRequestHeaders(request));
            String currentDeviceId = request.getParam(InParams.DEVICE_ID, String.class);
            currentSession.addAttribute(InParams.DEVICE_ID, currentDeviceId);
            currentSession.addAttribute(InParams.DEVICE_MODEL, (String) request.getParam(InParams.DEVICE_MODEL, String.class));

            SessionHandlerFactory.getInstance().cleanSessionsWithDeviceId(currentSession);

            SessionHandlerFactory.getInstance().updateBiometricSession(currentSession);
            response.setReturnCode(ReturnCodes.OK);

            return response;
        } catch (SessionException | IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        if (StringUtils.isBlank(request.getParam(InParams.DEVICE_ID, String.class))) {
            result.put(InParams.DEVICE_ID, "settings.fingerprintConfiguration.device.invalid.id");
        }
        if (StringUtils.isBlank(request.getParam(InParams.DEVICE_MODEL, String.class))
                || StringUtils.isNumeric(request.getParam(InParams.DEVICE_MODEL, String.class))) {
            result.put(InParams.DEVICE_MODEL, "settings.fingerprintConfiguration.device.invalid.model");
        }

        return result;
    }
}
