/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import net.sf.jxls.exception.ParsePropertyException;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author sbarbosa
 */
public class ExportUtils {

    private ExportUtils(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    private static final Logger log = LoggerFactory.getLogger(ExportUtils.class);

    public static byte[] toCsvFormat(List list, String[] columns, String[] attributes) {
        return toCsvFormat(list, columns, attributes, null);
    }

    public static byte[] toCsvFormat(List<Map> list, String[] columns, String[] attributes, Map<String, Object> params) {
        byte[] result = null;
        try {
            StringBuilder cvsFile = new StringBuilder();
            // UTF-8 marker
            cvsFile.append("\ufeff");
            for (int i = 0; i < columns.length; i++) {
                if (i > 0) {
                    cvsFile.append(",");
                }
                cvsFile.append("\"").append(StringEscapeUtils.unescapeHtml4(columns[i])).append("\"");
            }
            cvsFile.append("\n");

            SimpleDateFormat dateFormatter = new SimpleDateFormat(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "export.csv.dateFormat"));

            String[] subAttribs;
            String separator, listName, fieldName, fieldValue, paramName;
            StringBuilder valueAux;
            for (Map map : list) {
                for (int j = 0; j < attributes.length; j++) {
                    valueAux = new StringBuilder();
                    if (attributes[j].contains(">")) {
                        //es una lista
                        subAttribs = attributes[j].split(">");
                        listName = subAttribs[0];
                        subAttribs = subAttribs[1].split("\\|");
                        fieldName = subAttribs[0];
                        separator = subAttribs[1];
                        List<Map> l = (List<Map>) map.get(listName);
                        for (int k = 0; k < l.size(); k++) {
                            if (k > 0) {
                                valueAux.append(separator);
                                valueAux.append(" ");
                            }
                            valueAux.append(StringUtils.defaultString((String) l.get(k).get(fieldName)));
                        }
                    } else if (attributes[j].contains("#")) {
                        //es un mapeo de strings
                        HashMap<String, String> mappings = new HashMap();
                        subAttribs = attributes[j].split("#");
                        fieldName = subAttribs[0];
                        subAttribs = subAttribs[1].split("\\|");
                        String[] mapping;
                        for (String subAttrib : subAttribs) {
                            mapping = subAttrib.split("=");
                            mappings.put(mapping[0], mapping[1]);
                        }

                        fieldValue = StringUtils.defaultString((String) map.get(fieldName));
                        //remapeo
                        fieldValue = (mappings.get(fieldValue) != null) ? mappings.get(fieldValue) : fieldValue;
                        valueAux.append(fieldValue);
                    } else if (attributes[j].contains("$")) {
                        //es un parametro map
                        subAttribs = attributes[j].split("\\$");
                        fieldName = subAttribs[0];
                        paramName = subAttribs[1];
                        Map<String, String> mappings = (Map<String, String>) params.get(paramName);
                        fieldValue = (String) map.get(fieldName);
                        //remapeo
                        fieldValue = (mappings.get(fieldValue) != null) ? mappings.get(fieldValue) : fieldValue;
                        valueAux.append(fieldValue);
                    } else {
                        //es un parametro
                        Object value = map.get(attributes[j]);
                        if (value instanceof Date) {
                            fieldValue = dateFormatter.format(value);
                        } else {
                            fieldValue = (value != null) ? value.toString() : "";
                        }
                        valueAux.append(fieldValue);
                    }

                    // Cambio los caracteres html por los corrrespondientes en UTF-8.
                    cvsFile.append(j > 0 ? "," : "").append("\"").append(StringEscapeUtils.unescapeHtml4(valueAux.toString())).append("\"");
                }
                cvsFile.append("\n");
            }

            result = cvsFile.toString().getBytes("UTF-8");
        } catch (Exception ex) {
            String logString = String.format("Error generando archivo csv: %s", ex.getMessage());
            log.warn(logString);
        }
        return result;
    }

    public static byte[] toExcelFormat(String templateName, String[] columns, List list, String[] attributes, String templateTitle, String subTitle) {
        return toExcelFormat(templateName, list, columns, attributes, null, templateTitle, subTitle);
    }

    public static byte[] toExcelFormat(String templateName, List<Map> list, String[] columns, String[] attributes, Map<String, Object> params, String title, String subTitle) {
        byte[] result = null;
        try {
            XLSTransformer transformer = new XLSTransformer();

            List<List<String>> records = new ArrayList<>();

            String[] subAttribs;
            String separator, listName, fieldName, fieldValue, paramName;
            StringBuilder valueAux;
            List<String> objAttributes;
            for (Map map : list) {
                objAttributes = new ArrayList<>();
                for (String attribute : attributes) {
                    valueAux = new StringBuilder();
                    if (attribute.contains(">")) {
                        //es una lista
                        subAttribs = attribute.split(">");
                        listName = subAttribs[0];
                        subAttribs = subAttribs[1].split("\\|");
                        fieldName = subAttribs[0];
                        separator = subAttribs[1];
                        List<Map> l = (List<Map>) map.get(listName);
                        for (int k = 0; k < l.size(); k++) {
                            if (k > 0) {
                                valueAux.append(separator);
                                valueAux.append(" ");
                            }
                            valueAux.append(StringUtils.defaultString((String) l.get(k).get(fieldName)));
                        }
                    } else if (attribute.contains("#")) {
                        //es un mapeo de strings
                        HashMap<String, String> mappings = new HashMap();
                        subAttribs = attribute.split("#");
                        fieldName = subAttribs[0];
                        subAttribs = subAttribs[1].split("\\|");
                        String[] mapping;
                        for (String subAttrib : subAttribs) {
                            mapping = subAttrib.split("=");
                            mappings.put(mapping[0], mapping[1]);
                        }
                        fieldValue = StringUtils.defaultString((String) map.get(fieldName));
                        //remapeo
                        fieldValue = (mappings.get(fieldValue) != null) ? mappings.get(fieldValue) : fieldValue;
                        valueAux.append(fieldValue);
                    } else if (attribute.contains("$")) {
                        //es un parametro map
                        subAttribs = attribute.split("\\$");
                        fieldName = subAttribs[0];
                        paramName = subAttribs[1];
                        Map<String, String> mappings = (Map<String, String>) params.get(paramName);
                        fieldValue = (String) map.get(fieldName);
                        //remapeo
                        fieldValue = (mappings.get(fieldValue) != null) ? mappings.get(fieldValue) : fieldValue;
                        valueAux.append(fieldValue);
                    } else {
                        //es un parametro
                        Object value = map.get(attribute);
                        fieldValue = (value != null) ? value.toString() : "";
                        valueAux.append(fieldValue);
                    }
                    // Cambio los caracteres html por los corrrespondientes en UTF-8.
                    objAttributes.add(StringEscapeUtils.unescapeHtml4(valueAux.toString()));
                }
                records.add(objAttributes);
            }

            Map<String, Object> beans = new HashMap<>();

            // Unescape all content
            List<String> cols = new ArrayList<>();
            for (String col : columns) {
                cols.add(StringEscapeUtils.unescapeHtml4(col));
            }

            List<List<String>> unescapedRecords = new ArrayList<>();
            for (List<String> sublist : records) {
                List<String> unescapedSublist = new ArrayList<>();

                for (String record : sublist) {
                    unescapedSublist.add(StringEscapeUtils.unescapeHtml4(record));
                }

                unescapedRecords.add(unescapedSublist);
            }

            beans.put("cols", cols);
            beans.put("records", unescapedRecords);
            beans.put("templateTitle", StringEscapeUtils.unescapeHtml4(title));

            if (StringUtils.isNotBlank(subTitle)) {
                beans.put("templateSubTitle", StringEscapeUtils.unescapeHtml4(subTitle));
            }

            try (ByteArrayOutputStream out = new ByteArrayOutputStream();
                 Workbook work = WorkbookFactory.create(ExportUtils.class.getResourceAsStream(templateName))) {

                transformer.transformWorkbook(work, beans);
                work.write(out);
                result = out.toByteArray();
                out.flush();
            }
        } catch (ParsePropertyException | IOException ex) {
            log.warn("Error generando excel: " + ex.getMessage(), ex);
        }
        return result;
    }

    private static String formatHtml(String html, String[] columns, List<Map<String, Object>> elements, String[] attributes, String title, String disclaimer) {
        StringBuilder strBuilderColumns = new StringBuilder("");  
        StringBuilder strBuilderElements = new StringBuilder("");  

        for(String column : columns){            
            strBuilderColumns.append("<th>").append(column).append("</th>");            
        }

        for(Map<String, Object> element: elements) {
             strBuilderElements.append("<tr>");
             for(String attribute : attributes){
                 strBuilderElements.append("<td>").append(element.get(attribute)).append("</td>");
             }
             strBuilderElements.append("</tr>");
        }

        html = html.replace("%%LIST_TITLE%%", title);
        html = html.replace("%%HEADER%%", strBuilderColumns.toString());
        html = html.replace("%%ELEMENTS%%", strBuilderElements.toString());
        html = html.replace("%%OMNICHANNEL_DISCLAIMER%%", disclaimer);

        return html;
    }

    public static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        documentBuilderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
        documentBuilderFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        documentBuilderFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");

        return documentBuilderFactory.newDocumentBuilder();
    }
}