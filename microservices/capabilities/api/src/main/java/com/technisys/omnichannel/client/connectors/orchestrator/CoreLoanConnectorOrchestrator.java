/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankConsolidateConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankLoanConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.ISOProduct;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.LoanType;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.StatementLoan;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CoreLoanConnectorOrchestrator extends CoreConnectorOrchestrator {

    /**
     * Loans list
     *
     * @param idTransaction
     * @return
     * @throws BackendConnectorException
     */
    public static List<Loan> list(String idTransaction, List<Client> clients, List<String> typeList) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return (List<Loan>) RubiconCoreConnectorC.listProducts(idTransaction, clients, typeList);
                case CYBERBANK_CONNECTOR:
                    return CyberbankLoanConnector.list(idTransaction, clients).getData();
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     *
     * @param idTransaction
     * @param product
     * @param requestedStatements
     * @param movementsPerPage
     * @param offset
     * @return
     * @throws BackendConnectorException
     */
    public static List<StatementLoan> listInstallments(String idTransaction, Product product, String requestedStatements, int movementsPerPage, int offset) throws BackendConnectorException {

        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return RubiconCoreConnectorC.listLoanStatements(idTransaction, product.getIdProduct(), product.getExtraInfo(), requestedStatements, movementsPerPage, offset);
                case CYBERBANK_CONNECTOR:
                    CyberbankCoreConnectorResponse<List<StatementLoan>> listCyberbankCoreConnectorResponse = CyberbankLoanConnector.listInstallments(product);
                    if (CyberbankCoreConnectorResponse.RESPONSE_OK == listCyberbankCoreConnectorResponse.getCode()) {
                        return listCyberbankCoreConnectorResponse.getData();
                    } else {
                        throw new CyberbankCoreConnectorException("Failed to request loan's installments from CyberbankCore");
                    }
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     * Read Loan
     *
     * @param idTransaction
     * @param product
     * @return
     * @throws BackendConnectorException
     */
    public static Loan read(String idTransaction, Product product) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return RubiconCoreConnectorC.readLoanDetails(idTransaction, product.getIdProduct(), product.getExtraInfo());
                case CYBERBANK_CONNECTOR:
                    return CyberbankLoanConnector.read(product).getData();
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException e) {
            throw new BackendConnectorException(e);
        }
    }


    /**
     * List Loan Type
     * @return
     * @throws BackendConnectorException
     */
    public static List<LoanType> listLoanType() throws BackendConnectorException {
        try {
            List<LoanType> loanTypeList = new ArrayList<>();
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    loanTypeList.add(new LoanType("0911", "Loan 1"));
                    return loanTypeList;
                case CYBERBANK_CONNECTOR:
                    List<ISOProduct> isoProductList = CyberbankConsolidateConnector.listLoanISOProducts();
                    loanTypeList = CyberbankLoanConnector.listLoanType(isoProductList).getData();
                    return loanTypeList;
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new BackendConnectorException(e);
        }
    }

    public static Loan request(Account debitAccount, String subProductCode, String idCustomer, Amount amount, Integer amountOfFees) throws BackendConnectorException {
        try {
            Loan loan = null;
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    loan = new Loan();
                    loan.setNumber("4242");
                    return loan;
                case CYBERBANK_CONNECTOR:
                    List<ISOProduct> isoProductList = CyberbankConsolidateConnector.listLoanISOProducts();
                    String productCode = !isoProductList.isEmpty() ? isoProductList.get(0).getProductId() : "";
                    CyberbankCoreConnectorResponse<Loan> response = CyberbankLoanConnector.request(debitAccount, productCode, subProductCode, idCustomer,  amount, amountOfFees);
                    loan  = response.getData();
                    return loan;
                default:
                    throw new BackendConnectorException("No core connector configured");

            }
        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new BackendConnectorException(e);
        }
    }
}
