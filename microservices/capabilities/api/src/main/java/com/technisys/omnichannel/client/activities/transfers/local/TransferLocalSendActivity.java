/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.transfers.local;

import com.technisys.omnichannel.activities.forms.SendFormActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreTransferConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.TransferOtherBankDetails;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;

import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * @author pbanales
 */
@DocumentedActivity("Local transfers")
public class TransferLocalSendActivity extends Activity {

    public static final String ID = "transfers.local.send";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Amount")
        String AMOUNT = "amount";
        @DocumentedParam(type = String.class, description = "Debit account")
        String DEBIT_ACCOUNT = "debitAccount";
        @DocumentedParam(type = String.class, description = "Selected bank")
        String BANK_SELECTOR = "bankSelector";
        @DocumentedParam(type = String.class, description = "Credit account number")
        String CREDIT_ACCOUNT = "creditAccountNumber";
        @DocumentedParam(type = String.class, description = "Credit reference")
        String CREDIT_REFERENCE = "creditReference";
        @DocumentedParam(type = String.class, description = "Credit account name")
        String CREDIT_ACCOUNT_NAME = "creditAccountName";
        @DocumentedParam(type = String.class, description = "Document")
        String DOCUMENT = "document";
    }

    public interface OutParams {

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Transaction transaction = TransactionHandlerFactory.getInstance().read(request.getIdTransaction());
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            Amount amount = request.getParam(InParams.AMOUNT, Amount.class);
            String debitAccountIdProduct = (String) request.getParam(InParams.DEBIT_ACCOUNT, Map.class).get("value");
            String creditBankId = (String) request.getParam(InParams.BANK_SELECTOR, List.class).get(0);
            String creditAccountNumber = request.getParam(InParams.CREDIT_ACCOUNT, String.class);
            String reference = request.getParam(InParams.CREDIT_REFERENCE, String.class);
            String creditAccountName = request.getParam(InParams.CREDIT_ACCOUNT_NAME, String.class);
            String creditDocumentNumber = (String) request.getParam(InParams.DOCUMENT, Map.class).get("document");

            Product product = Administration.getInstance().readProduct(debitAccountIdProduct, request.getIdEnvironment());
            Account debitAccount = new Account(product);

            TransferOtherBankDetails transferOtherBankDetails = CoreTransferConnectorOrchestrator.localBanks(environment.getProductGroupId(), debitAccount, creditAccountNumber, creditAccountName, creditDocumentNumber, amount, reference, creditBankId);

            transaction.getData().put("tax", transferOtherBankDetails.getTax());
            TransactionHandlerFactory.getInstance().updateTransactionData(transaction);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (BackendConnectorException e) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, e);
        }

        return new SendFormActivity().execute(request);
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return new SendFormActivity().validate(request);
    }
}
