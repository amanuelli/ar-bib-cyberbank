/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.utils;

import java.util.HashMap;

public class DocumentTypeResolver {

    private static final HashMap<String, String> documentTypes;

    static {
        documentTypes = new HashMap<>();
        documentTypes.put("DNI", "DNI");
        documentTypes.put("CI", "DNI");
        documentTypes.put("PAS", "PA");
        documentTypes.put("PA", "PAS");
        documentTypes.put("CUIL", "CUIL");
    }

    private DocumentTypeResolver() {
    }

    public static String resolve(String value) {
        return documentTypes.get(value);
    }

}
