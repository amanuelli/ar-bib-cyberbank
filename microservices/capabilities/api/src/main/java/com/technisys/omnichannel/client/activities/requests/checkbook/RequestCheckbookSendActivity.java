/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.requests.checkbook;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.bpm.BPMHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.utils.CoreUtils;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.util.Map;

/**
 *
 */
public class RequestCheckbookSendActivity extends Activity {

    public static final String ID = "core.forms.send";

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Form form = FormsHandler.getInstance().readFormFull(Constants.FORM_REQUEST_CHECKBOOK, null);
            Map<String, Object> variables = CoreUtils.getFormVariablesReader().readVariables(form, request.getParameters());

            String idBPMProcess = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "request.checkbook.bpmProcessKey");

            request.setFormVersion(form.getVersion());
            BPMHandler.createBPMProcess(request, idBPMProcess, variables);

            response.setReturnCode(ReturnCodes.BACKEND_IS_PROCESSING_THE_TRANSACTION);

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new ActivityException(ReturnCodes.UNEXPECTED_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            //ejecutamos validaciones generales del formulario
            return FormsHandler.getInstance().validateRequest(request);
        } catch (SchedulerException ex) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }
}
