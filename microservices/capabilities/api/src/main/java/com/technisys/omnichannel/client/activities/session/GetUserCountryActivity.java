/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session;

import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.postloginchecks.geoip.PostLoginCheckGeoIPDataAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;

/**
 * @author Jhossept G
 */
@AnonymousActivity
@DocumentedActivity("Get current user country")
public class GetUserCountryActivity extends Activity {
    
    public static final String ID = "session.getUserCountry";

    private static final Logger log = LoggerFactory.getLogger(GetUserCountryActivity.class);

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Current requesting user country")
        String COUNTRY = "country";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        InetAddress inetAddress = null;
        try {
            inetAddress = InetAddress.getByName(request.getClientIP());

            // En desarrollo, la IP es 127.0.0.1 o 0:0:0:0:0:0:0:1 y en testing los IPs son de la red local;
            // las cuales no estan la DB. Por tanto si estamos se utiliza la IP publica del lab de montevideo
            if (inetAddress.isSiteLocalAddress() || inetAddress.isLoopbackAddress()) {
                String clientIp = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "cyberbank.localAddressTesting");
                inetAddress = InetAddress.getByName(clientIp);
            }
            // Buscamos los datos en la base de datos de GeoIP2 or GeoLite2 database

            CityResponse cityRes = PostLoginCheckGeoIPDataAccess.getInstance().getCity(inetAddress);
            response.putItem(OutParams.COUNTRY, cityRes.getCountry());

        } catch (AddressNotFoundException e) {
            log.warn("The IP (" + inetAddress.getHostName() + ") is not in the DB", e);
        } catch (GeoIp2Exception e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

}
