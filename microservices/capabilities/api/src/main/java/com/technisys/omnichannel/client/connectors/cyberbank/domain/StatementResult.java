/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */


package com.technisys.omnichannel.client.connectors.cyberbank.domain;

import com.technisys.omnichannel.client.domain.Statement;

import java.util.List;
import java.util.Objects;

public class StatementResult {

    private int totalAccount;
    private List<Statement> statementList;

    public StatementResult(List<Statement> statementList) {
        this.statementList = statementList;
    }

    public StatementResult(int totalAccount, List<Statement> statementList) {
        this.totalAccount = totalAccount;
        this.statementList = statementList;
    }

    public int getTotalAccount() {
        return totalAccount;
    }

    public void setTotalAccount(int totalAccount) {
        this.totalAccount = totalAccount;
    }

    public List<Statement> getStatementList() {
        return statementList;
    }

    public void setStatementList(List<Statement> statementList) {
        this.statementList = statementList;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        StatementResult that = (StatementResult) o;
        return totalAccount == that.totalAccount &&
                Objects.equals(statementList, that.statementList);
    }

    @Override
    public int hashCode() {
        return Objects.hash(totalAccount, statementList);
    }
}
