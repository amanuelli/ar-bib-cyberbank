/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.accounts;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.apache.hc.client5.http.classic.methods.HttpPost;
import org.apache.hc.client5.http.impl.ChainElements;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpEntity;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.apache.hc.core5.http.io.entity.StringEntity;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.util.Map;

/**
 * @author Marcelo Bruno
 */
@DocumentedActivity("Fund an account")
public class FundAccountSendActivity extends Activity {

    public static final String ID = "account.fund.send";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Account ID to fund")
        String CREDIT_ACCOUNT = "creditAccount";

        @DocumentedParam(type = String.class, description = "Account ID to debit")
        String DEBIT_ACCOUNT = "debitAccount";

        @DocumentedParam(type = Amount.class, description = "Amount to be credited to the account")
        String AMOUNT = "amount";

    }

    public interface OutParams {

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Response response = new Response(request);

            Map map = RequestParamsUtils.getValue(request.getParameters().get(InParams.CREDIT_ACCOUNT), Map.class);
            String idCreditAccount = (String) map.get("value");

            Amount amount = request.getParam(InParams.AMOUNT, Amount.class);

            Product creditProduct = Administration.getInstance().readProduct(idCreditAccount, request.getIdEnvironment());
            Account creditAccount = new Account(creditProduct);

            RubiconCoreConnectorC.fundAccount(creditAccount.getNumber(), amount.getQuantity(), "Account fund", "Account fund");

            response.setReturnCode(ReturnCodes.OK);
            return response;

        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (BackendConnectorException e) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, e);
        }

    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result;
        try {
            result = FormsHandler.getInstance().validateRequest(request);
            Amount amount = RequestParamsUtils.getValue(request.getParameters().get(InParams.AMOUNT), Amount.class);

            Map map = RequestParamsUtils.getValue(request.getParameters().get(InParams.DEBIT_ACCOUNT), Map.class);
            String debitAccountId = (String) map.get("debitAccountId");
            String publicToken = (String) map.get("publicToken");

            double debitAccountBalance = retrievePlaidAccountBalance(publicToken, debitAccountId);

            if(debitAccountBalance < amount.getQuantity()) {
                result.put("NO_FIELD", "accounts.fund.noAvailableBalance");
            }

        } catch (SchedulerException ex) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }

    private double retrievePlaidAccountBalance(String publicToken, String accountId) throws ActivityException {
        double accountBalance = 0;

        CloseableHttpResponse httpResponse;
        try(CloseableHttpClient httpclient = HttpClients.custom()
                .build()) {
            String clientId = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "plaid.integration.clientId");
            String secret = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "plaid.integration.secret");
            String balanceGetEndpoint = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "plaid.integration.balance.get.url");

            HttpPost httpPost = new HttpPost(balanceGetEndpoint);
            String accessToken = requestoForAccessToken(clientId, secret, publicToken);

            String json = "{ \"client_id\": \""+ clientId + "\", \"secret\": \"" + secret + "\", \"access_token\": \"" + accessToken + "\", \"options\": {\"account_ids\": [\"" + accountId + "\"] } }";

            StringEntity jsonParams = new StringEntity(json);
            httpPost.setEntity(jsonParams);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            httpResponse = httpclient.execute(httpPost);

            if (httpResponse.getCode() != 200) {
                throw new ActivityException(ReturnCodes.IO_ERROR, "error getting source account balance");
            } else {
                HttpEntity entity = httpResponse.getEntity();
                String responseString = EntityUtils.toString(entity, "UTF-8");

                JsonElement element = new JsonParser().parse(responseString);
                JsonElement element2 = element.getAsJsonObject().get("accounts");

                accountBalance = element2.getAsJsonArray().get(0).getAsJsonObject().get("balances").getAsJsonObject().get("available").getAsDouble();
            }

        } catch (IOException | ParseException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return accountBalance;
    }

    private String requestoForAccessToken(String clientId, String secret, String publicToken) throws IOException, ActivityException{
        String endpoint = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "plaid.integration.public.token.exchange.url");
        CloseableHttpResponse httpResponse;
        try(CloseableHttpClient httpclient = HttpClients.custom()
                .build()) {
            HttpPost httpPost = new HttpPost(endpoint);

            String json = "{ \"client_id\": \""+ clientId + "\", \"secret\": \"" + secret + "\", \"public_token\": \"" + publicToken + "\" }";

            StringEntity jsonParams = new StringEntity(json);
            httpPost.setEntity(jsonParams);
            httpPost.setHeader("Accept", "application/json");
            httpPost.setHeader("Content-type", "application/json");

            httpResponse = httpclient.execute(httpPost);

            if (httpResponse.getCode() != 200) {
                throw new ActivityException(ReturnCodes.IO_ERROR, "error connecting with PLAID");
            } else {
                HttpEntity entity = httpResponse.getEntity();
                String responseString = EntityUtils.toString(entity, "UTF-8");
                JsonElement element = new JsonParser().parse(responseString);

                return element.getAsJsonObject().get("access_token").getAsString();
            }

        } catch (IOException | ParseException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

}