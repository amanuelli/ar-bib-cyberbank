/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.userdata;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.ValidationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.validationcodes.ValidationCodesHandler;
import org.apache.commons.lang.StringUtils;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * Validate that the validation code sent to the mobile phone is correct. Then modify the user phone number.
 */
@DocumentedActivity("Validate that the validation code sent to the mobile phone is correct. Then modify the user phone number.")
public class UpdateMobilePhoneActivity extends Activity {

    public static final String ID = "preferences.userData.mobilePhone.update";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Validation code sent to the user mobile phone. e.g. &quot;3286&quot;")
        String MOBILE_PHONE_CODE = "mobilePhoneCode";
        
        // Used by frontend, not this activity
        @DocumentedParam(type = String.class, description = "idForm that makes the modification of the user data, always send &quot;<code style=color:red;>modifyUserData</code>&quot;")
        String ID_FORM = "idForm";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {

            String mobilePhoneCode = com.technisys.omnichannel.client.utils.StringUtils.normalizeInvitationCode(request.getParam(InParams.MOBILE_PHONE_CODE, String.class));
            ValidationCode mobilePhoneCodeValidationCode = ValidationCodesHandler.readValidationCode(mobilePhoneCode);

            AccessManagementHandlerFactory.getHandler().updateUserMobileNumber(request.getIdUser(), mobilePhoneCodeValidationCode.getExtraInfo());

            ValidationCodesHandler.changeValidationCodeStatus(mobilePhoneCodeValidationCode.getId(), ValidationCode.STATUS_USED);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            Map<String, String> result = FormsHandler.getInstance().validateRequest(request);
            
            String mobilePhoneCode = com.technisys.omnichannel.client.utils.StringUtils.normalizeInvitationCode(request.getParam(InParams.MOBILE_PHONE_CODE, String.class));
            
            if (StringUtils.isBlank(mobilePhoneCode)) {
                result.put(InParams.MOBILE_PHONE_CODE, "userInfo.preferences.userData.mobilePhoneCode.invalid");
                return result;
            }
            
            ValidationCode validationCode = ValidationCodesHandler.readValidationCode(mobilePhoneCode);
            if (validationCode == null) {
                result.put(InParams.MOBILE_PHONE_CODE, "userInfo.preferences.userData.mobilePhoneCode.invalid");
            }
            else if (!ValidationCode.TYPE_CHANGEMOBILEPHONE.equals(validationCode.getType())) {
                result.put(InParams.MOBILE_PHONE_CODE, "userInfo.preferences.userData.mobilePhoneCode.invalid");
            }
            else if (!ValidationCode.STATUS_AVAILABLE.equals(validationCode.getStatus())) {
                result.put(InParams.MOBILE_PHONE_CODE, "userInfo.preferences.userData.mobilePhoneCode.used");
            }
            else {
                long validityTime = ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "changeMobilePhone.sms.validity");
                long currentTime = (new Date()).getTime();
                long creationTime = validationCode.getCreationDate().getTime();
                if (creationTime + validityTime < currentTime) {
                    result.put(InParams.MOBILE_PHONE_CODE, "userInfo.preferences.userData.mobilePhoneCode.expired");
                }
            }
            

            return result;
        } catch (SchedulerException ex) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

}
