/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.frequentdestinations;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.FrequentDestination;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.frequentdestinations.FrequentDestinationsHandler;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;

import java.io.IOException;
import java.util.List;

/**
 *
 */
public class ListFrequentDestinationsActivity extends Activity {

    public static final String ID = "frequentdestinations.list";

    public interface InParams {
        String PAGE_NUMBER = "pageNumber";
        String NAME_SEARCH = "nameSearch";
        String FULL_LIST = "fullList";
        String FIELD_TYPE = "fieldType";
    }

    public interface OutParams {
        String FREQUENT_DESTINATIONS = "frequentDestinations";
        String MORE_STATEMENTS = "moreStatements";
        String PAGE_NUMBER = "pageNumber";
        String MANAGE_PERMISSION = "managePermission";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            
            int movementsPerPage = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "frequentDestinations.statementsPerPage", 10);
            
            Integer pageNumber = request.getParam(InParams.PAGE_NUMBER, Integer.class);
            if (pageNumber == null) {
                pageNumber = 1;
            }
            
            int offset = movementsPerPage * (pageNumber - 1);
            
            boolean moreStatements = false;
            
            String nameSearch = request.getParam(InParams.NAME_SEARCH, String.class);
            
            Boolean fullList = request.getParam(InParams.FULL_LIST, Boolean.class);
            if (fullList == null) {
                fullList = false;
            }
            
            String fieldType = request.getParam(InParams.FIELD_TYPE, String.class);
            
            List<FrequentDestination> frequentDestinations;
            
            if (fullList) {
                frequentDestinations = FrequentDestinationsHandler.getFrequentDestinationsByEnvironment(request.getIdEnvironment(), nameSearch, 99999, 0);
            }else {
                if(fieldType != null) 
                    frequentDestinations = FrequentDestinationsHandler.getEnvironmentFrequentDestinationsByType(request.getIdEnvironment(), fieldType, -1);
                else
                    frequentDestinations = FrequentDestinationsHandler.getFrequentDestinationsByEnvironment(request.getIdEnvironment(), nameSearch, movementsPerPage + 1, offset);
            }
            
            
            if (frequentDestinations.size() > movementsPerPage) {
                moreStatements = true;
                frequentDestinations.remove(frequentDestinations.size() - 1);
            }
            
            response.putItem(OutParams.FREQUENT_DESTINATIONS, frequentDestinations);
            response.putItem(OutParams.MORE_STATEMENTS, moreStatements);
            response.putItem(OutParams.PAGE_NUMBER, pageNumber);
            response.putItem(OutParams.MANAGE_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), Constants.FREQUENT_DESTINATIONS_MANAGE_PERMISSION));
            
            response.setReturnCode(ReturnCodes.OK);
            
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    
}
