/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.utils.ObjectPool;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.utils.NumberUtils;
import com.technisys.omnichannel.rubicon.core.accounts.Accounts;
import com.technisys.omnichannel.rubicon.core.creditcards.CreditCards;
import com.technisys.omnichannel.rubicon.core.deposits.Deposits;
import com.technisys.omnichannel.rubicon.core.general.General;
import com.technisys.omnichannel.rubicon.core.loans.Loans;
import com.technisys.omnichannel.rubicon.core.onboarding.Onboarding;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.XMLGregorianCalendar;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author Sebastian Barbosa
 */
public class RubiconCoreConnector {

    protected RubiconCoreConnector(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    private static final Logger log = LoggerFactory.getLogger(RubiconCoreConnector.class);

    protected static final String WEBSERVICES_URL = ConfigurationFactory.getInstance().getURLSafe(Configuration.PLATFORM, "backend.webservices.url");

    protected static final String CHANNEL = "I";

    protected static final String OPERATION_VALIDATE = "V";
    protected static final String OPERATION_CONFIRMATION = "C";

    protected static final List<String> CORPORATE_SEGMENT_LIST = Collections.singletonList("CMB");
    protected static final List<String> RETAIL_SEGMENT_LIST = Collections.singletonList("PFS");

    private static final int CONCURRENT_REQUEST = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "backend.webservices.concurrentRequest", 50);
    protected static final ObjectPool<General> WS_GENERAL_POOL = new ObjectPool("WSGeneral", new WSGeneralFactory(), CONCURRENT_REQUEST);
    protected static final ObjectPool<Accounts> WS_ACCOUNTS_POOL = new ObjectPool("WSAccounts", new WSAccountsFactory(), CONCURRENT_REQUEST);
    protected static final ObjectPool<Loans> WS_LOANS_POOL = new ObjectPool("WSLoans", new WSLoansFactory(), CONCURRENT_REQUEST);
    protected static final ObjectPool<CreditCards> WS_CREDITCARDS_POOL = new ObjectPool("WSCreditCards", new WSCreditCardsFactory(), CONCURRENT_REQUEST);
    protected static final ObjectPool<Deposits> WS_DEPOSITS_POOL = new ObjectPool("WSDeposits", new WSDepositsFactory(), CONCURRENT_REQUEST);
    protected static final ObjectPool<Onboarding> WS_ONBOARDING_POOL = new ObjectPool("WSOnboarding", new WSOnboardingFactory(), CONCURRENT_REQUEST);

    /**
     * Parseo de numeros retornados como string en los servicios
     *
     * @param value Número en formato string
     * @param defaultValue Valor por defecto en caso de error o que el valor sea
     * vacío
     * @return Valor en formato double
     */
    public static double parseDouble(String value, double defaultValue) {
        double result = defaultValue;

        try {
            String pattern = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "backend.webservices.decimalFormat");
            if (StringUtils.isNotEmpty(value)) {
                DecimalFormat df = NumberUtils.getDecimalFormatter(pattern, ".");
                result = df.parse(value).doubleValue();
            }
        } catch (IOException e) {
            log.error("Configuration error (key: backend.webservices.decimalFormat)", e);
        } catch (ParseException e) {
            log.info("Invalid double from backend {}", value, e);
        }
        return result;
    }

    public static double parseDouble(String value) {
        return parseDouble(value, 0);
    }

    /**
     * Parseo de fechas retornados como string en los servicios
     *
     * @param value Fecha en formato string
     * @return Fecha convertida en Date
     */
    public static Date parseDate(String value) {
        Date result = null;

        try {
            String formato = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "backend.webservices.dateFormat");
            if (StringUtils.isNotEmpty(value)) {
                SimpleDateFormat sdf = new SimpleDateFormat(formato);
                result = sdf.parse(value);
            }
        } catch (IOException e) {
            log.error("Configuration error (key: backend.webservices.dateFormat)", e);
        } catch (ParseException e) {
            log.info("Invalid date from backend {}", value, e);
        }
        return result;
    }

    public static Map<String, String> convertDynamicListToMap(List fieldList) {
        Map<String, String> map = new LinkedHashMap<>();

        for (Object obj : fieldList) {
            if (obj instanceof com.technisys.omnichannel.rubicon.core.general.WsDynamicField) {
                com.technisys.omnichannel.rubicon.core.general.WsDynamicField field = (com.technisys.omnichannel.rubicon.core.general.WsDynamicField) obj;
                map.put(field.getKey(), field.getValue());
            } else if (obj instanceof com.technisys.omnichannel.rubicon.core.deposits.WsDynamicField) {
                com.technisys.omnichannel.rubicon.core.deposits.WsDynamicField field = (com.technisys.omnichannel.rubicon.core.deposits.WsDynamicField) obj;
                map.put(field.getKey(), field.getValue());
            } else if (obj instanceof com.technisys.omnichannel.rubicon.core.accounts.WsDynamicField) {
                com.technisys.omnichannel.rubicon.core.accounts.WsDynamicField field = (com.technisys.omnichannel.rubicon.core.accounts.WsDynamicField) obj;
                map.put(field.getKey(), field.getValue());
            } else if (obj instanceof com.technisys.omnichannel.rubicon.core.creditcards.WsDynamicField) {
                com.technisys.omnichannel.rubicon.core.creditcards.WsDynamicField field = (com.technisys.omnichannel.rubicon.core.creditcards.WsDynamicField) obj;
                map.put(field.getKey(), field.getValue());
            } else if (obj instanceof com.technisys.omnichannel.rubicon.core.loans.WsDynamicField) {
                com.technisys.omnichannel.rubicon.core.loans.WsDynamicField field = (com.technisys.omnichannel.rubicon.core.loans.WsDynamicField) obj;
                map.put(field.getKey(), field.getValue());
            }
        }
        return map;
    }

    public static Date convertXMLGregorianCalendarToDate(XMLGregorianCalendar calendar) {
        Date date = null;
        if (calendar != null) {
            date = calendar.toGregorianCalendar().getTime();
        }
        return date;
    }
}
