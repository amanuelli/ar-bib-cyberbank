/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.desktop;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreProductConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.utils.CurrencyUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 * @author Marcelo Bruno
 */

@DocumentedActivity("Corporate group desktop")
public class CorporateGroupDesktopActivity extends Activity {

    public static final String ID = "desktop.corporateGroup.accounts";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Currency filter")
        String CURRENCY = "currency";
    }

    public interface OutParams {

        @DocumentedParam(type = Map.class, description = "Corporate group account information")
        String CLIENT_DATA = "clientsData";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        Environment environment = null;
        try {
            environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            if (!Environment.ENVIRONMENT_TYPE_CORPORATE_GROUP.equals(environment.getEnvironmentType())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME_DATA);
            }

            String masterCurrency = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency");
            Map<String, Object> resultData = new HashMap<>();

            List<Environment> userEnvs =  Administration.getInstance().listEnvironments(request.getIdUser());

            Map<String, Object> clientsData = new HashMap<>();
            long equivalentTotalBalance = 0;
            for (Client client : environment.getClients()) {
                Map<String, Object> clientMap = new HashMap<>();
                clientMap.put("name", client.getName());

                equivalentTotalBalance = 0;
                List<String> idPermissions = new ArrayList<>();
                idPermissions.add(PRODUCT_READ_PERMISSION);
                List<String> productTypes = Arrays.asList(Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY);
                List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);
                List<Product> coreProducts = CoreProductConnectorOrchestrator.list(request.getIdTransaction(), environment.getClients(), productTypes);
                ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
                List<Account> accounts = new ArrayList<>();
                for (Product p : coreProducts) {
                    for (Product environmentProduct : environmentProducts) {
                        Account account = (Account) p;
                        if (p.getClient().getIdClient().equals(client.getIdClient()) && p.getExtraInfo().equals(environmentProduct.getExtraInfo())
                                && checkFilters(request, account) ) {

                            account.setProductAlias(environmentProduct.getProductAlias());
                            account.setShortLabel(pLabeler.calculateShortLabel(account, false));
                            equivalentTotalBalance += !masterCurrency.equals(account.getCurrency()) ? CurrencyUtils.convert(masterCurrency, account.getCurrency(), account.getBalance(), request.getIdTransaction(), account.getClient().getIdClient()) : account.getBalance();

                            accounts.add(account);
                        }
                    }
                }

                Environment clientEnv = Administration.getInstance().getEnvironment(client.getName());

                int idEnvironment = clientEnv != null ? clientEnv.getIdEnvironment() : -1;

                clientMap.put("accounts", accounts);
                clientMap.put("total", equivalentTotalBalance);
                clientMap.put("canLogin", userEnvs.contains(clientEnv));
                clientMap.put("idEnvironment", idEnvironment);
                clientsData.put(client.getIdClient(), clientMap);
            }

            resultData.put(OutParams.CLIENT_DATA, clientsData);
            response.setData(resultData);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        }

        return response;
    }

    private boolean checkFilters(Request request, Account account) {
        boolean result = true;

        if(request.getParameters().get(InParams.CURRENCY) != null) {
            String currency = request.getParam("currency", String.class);
            if(!account.getCurrency().equals(currency)) {
                result = false;
            }
        }

        return result;
    }
}
