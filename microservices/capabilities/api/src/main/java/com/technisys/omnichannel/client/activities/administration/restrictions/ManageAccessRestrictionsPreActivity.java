/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.restrictions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author KevinGaray
 */
/**
 * Manage the access restriction pre
 */
@DocumentedActivity("Manage the access restriction (PRE)")
public class ManageAccessRestrictionsPreActivity extends Activity {

    public static final String ID = "administration.restrictions.manage.pre";

    public interface OutParams {
        @DocumentedParam(type = List.class, description = "List of time zones")
        String TIME_ZONES = "timeZones";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            List<String> timezoneValues = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "administration.timezone.values");
            response.putItem(OutParams.TIME_ZONES, timezoneValues);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException e) {
            response.setReturnCode(ReturnCodes.IO_ERROR);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String adminScheme = request.getEnvironmentAdminScheme();
        if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(adminScheme)) {
            throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
        }

        return result;
    }
}