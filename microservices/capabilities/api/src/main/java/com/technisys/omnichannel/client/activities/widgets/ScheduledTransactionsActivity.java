package com.technisys.omnichannel.client.activities.widgets;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.client.activities.widgets.PendingTransactionsActivity.getTransactionMap;

public class ScheduledTransactionsActivity extends Activity {

    public static final String ID = "widgets.scheduledTransactions";

    public interface OutParams {

        String TRANSACTIONS = "scheduledTransactions";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            List<Map<String, Object>> transactions = this.getTransactions(request);

            response.putItem(OutParams.TRANSACTIONS, transactions);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    private List<Map<String, Object>> getTransactions(Request request) throws IOException {
        List<Map<String, Object>> transactionFinalList = new ArrayList<>();
        List<String> availableStates = new ArrayList<>();
        availableStates.add(Transaction.STATUS_SCHEDULED);
        List<Transaction> transactionList = TransactionHandlerFactory.getInstance()
                .listTransactions(request.getIdUser(), request.getIdEnvironment(), null, null, availableStates.toArray(new String[availableStates.size()]),
                        "creation_date_time DESC", 1, ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "desktop.widgets.scheduledTransactions.max"))
                .getElementList();

        Map<String, Form> formMap = new HashMap<>();
        for (Transaction transaction : transactionList) {
            if (transaction.isProgramed()) {                
                transaction.setProgram(TransactionHandlerFactory.getInstance().readProgram(transaction.getIdTransaction()));
                transactionFinalList.add(getTransactionMap(request, formMap, transaction));
            }
        }

        return transactionFinalList;
    }
}
