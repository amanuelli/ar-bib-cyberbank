/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.handlers.banks;

import com.technisys.omnichannel.client.domain.Bank;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.utils.DBUtils;
import org.apache.ibatis.session.SqlSession;
import java.io.IOException;

/**
 *
 * @author sbarbosa
 */
public class BanksHandler {

    private BanksHandler(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    public static PaginatedList<Bank> listBanks(String codeType, String codeNumber, String bankName, String bankCountryCode, int pageNumber, int rowsPerPage, String orderBy) throws IOException {
        try (SqlSession sqlSession = DBUtils.getInstance().openReadSession()) {
            return BanksDataAccess.getInstance().listBanks(sqlSession, codeType, codeNumber, bankName, bankCountryCode, pageNumber, rowsPerPage, orderBy);
        }
    }
    
    public static Bank readBank(String code, String type) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {
            return BanksDataAccess.getInstance().readBank(session, code, type);
        }
    }
}
