package com.technisys.omnichannel.client.activities.administration.advanced;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.common.UserDetails;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.limits.CapForUser;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author marcelobruno
 */

@DocumentedActivity("Read user detail of advanced environment")
public class ReadUserDetailsActivity extends Activity {
    
    public static final String ID = "administration.advanced.user.details.read";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "ID of the user to fetch information")
        String ID = "id";
    }

    public interface OutParams {
        @DocumentedParam(type = User.class, description = "User information")
        String USER = "user";
        @DocumentedParam(type = Set.class, description = "Permissions for the user")
        String GROUPS = "groups";
        @DocumentedParam(type = Set.class, description = "Products for the user")
        String PRODUCTS = "products";
        @DocumentedParam(type = List.class, description = "CAPS for the user")
        String CAPS = "caps";
        @DocumentedParam(type = String.class, description = "Currency")
        String CURRENCY = "currency";
        @DocumentedParam(type = CapForUser.class, description = "Top amount")
        String TOP_AMOUNT = "topAmount";
        @DocumentedParam(type = Boolean.class, description = "Signature level of environment user")
        String SIGNATURE_LEVEL = "signatureLevel";
        @DocumentedParam(type = Boolean.class, description = "Flag to indicate if user has actions enabled")
        String HAS_MASSIVE_ENABLED = "hasMassiveEnabled";
        @DocumentedParam(type = String.class, description = "Status of the environment user")
        String USER_ENV_STATUS = "userEnvStatus";
        @DocumentedParam(type = List.class, description = "Groups configured for the user")
        String SELECTED_GROUPS = "selectedGroups";
        @DocumentedParam(type = List.class, description = "Available groups to configure for the user")
        String AVAILABLE_GROUPS = "availableGroups";
        @DocumentedParam(type = List.class, description = "List of groups ids with administration permission")
        String ADMIN_GROUPS_IDS = "adminGroupsIds";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String idUser = request.getParam(InParams.ID, String.class);
            String lang = request.getLang();
            int idEnvironment = request.getIdEnvironment();
            Administration administration = Administration.getInstance();

            List<Group> groups = AccessManagementHandlerFactory.getHandler()
                    .getGroups(idUser, null, idEnvironment, -1, -1, null).getElementList();
             
            if (!this.isSchemeValid(groups, administration.readEnvironment(idEnvironment).getAdministrationScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME_DATA);
            }
            
            EnvironmentUser envUser = administration.readEnvironmentUserInfo(idUser, idEnvironment);
           
            Map<String, Object> data = UserDetails.buildUserDetails(envUser, idUser, idEnvironment, request.getEnvironmentType(), lang);
         
            validateGroupsForScheme(groups, request.getEnvironmentAdminScheme());
            List<Group> availableGroups = AccessManagementHandlerFactory.getHandler().getGroups(null, null, idEnvironment, -1, -1, null).getElementList();

            //Remove all administrator groups except the current user
            Group availableGroup;
            List<GroupPermission> permissionsList;
            GroupPermission gp;
            boolean hasAdminPermission;
            List<String> adminGroups = new ArrayList();
            List<String> users;
            for (Iterator<Group> it = availableGroups.iterator(); it.hasNext();) {
                availableGroup = it.next();
                permissionsList = AccessManagementHandlerFactory.getHandler().getGroupPermissions(availableGroup.getIdGroup());
                gp = new GroupPermission (availableGroup.getIdGroup(), Authorization.TARGET_NONE, Constants.ADMINISTRATION_VIEW_PERMISSION);
                hasAdminPermission = permissionsList.contains(gp);
                if (hasAdminPermission) {
                    adminGroups.add(availableGroup.getIdGroupAsString());
                    users = AccessManagementHandlerFactory.getHandler().getUsersIdByGroup(availableGroup.getIdGroup());
                    if (!users.contains(idUser)) {
                        it.remove();
                    }
                }
            }
            response.setData(data);
            response.putItem(OutParams.SELECTED_GROUPS, groups);
            response.putItem(OutParams.AVAILABLE_GROUPS, availableGroups);
            response.putItem(OutParams.ADMIN_GROUPS_IDS, adminGroups);
            response.setReturnCode(ReturnCodes.OK);
            
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String userId = request.getParam(InParams.ID, String.class);
            List<String> environmentUsers = Administration.getInstance()
                    .listEnvironmentUserIds(request.getIdEnvironment());

            if (environmentUsers == null || (!environmentUsers.contains(StringUtils.left(userId, 253)))) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    private boolean isSchemeValid(List<Group> groups, final String scheme) {
        return Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(scheme)
                && (!CollectionUtils.isEmpty(groups) || groups.size() == 1);
    }
    
    private void validateGroupsForScheme(List<Group> groups, String scheme) throws ActivityException {
        if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(scheme) || Environment.ADMINISTRATION_SCHEME_MEDIUM.equals(scheme)
            && (CollectionUtils.isEmpty(groups) || groups.size() > 1)) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME_DATA);
        }
    }
}
