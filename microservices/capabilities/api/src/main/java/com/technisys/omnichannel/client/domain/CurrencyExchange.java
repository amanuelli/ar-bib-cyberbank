/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.rubicon.core.general.WsExchangeRate;
import java.util.Objects;

/**
 *
 * @author aelters
 */
public class CurrencyExchange {

    /**
     * Moneda a la cual se hace el cambio (ejemplo: dolares)
     */
    private String targetCurrencyCode;
    /**
     * Moneda desde la cual se hace el cambio (ejemplo: pesos uruguayos)
     */
    private String baseCurrencyCode;

    private double purchase;

    private double sale;

    public CurrencyExchange() {
    }

    public CurrencyExchange(WsExchangeRate exchangeRate, String baseCurrencyCode) {
        this.purchase = exchangeRate.getBuyRate();
        this.sale = exchangeRate.getSellRate();

        this.baseCurrencyCode = baseCurrencyCode;
        this.targetCurrencyCode = exchangeRate.getCurrency();
    }

    public String getTargetCurrencyCode() {
        return targetCurrencyCode;
    }

    public void setTargetCurrencyCode(String targetCurrencyCode) {
        this.targetCurrencyCode = targetCurrencyCode;
    }

    public String getBaseCurrencyCode() {
        return baseCurrencyCode;
    }

    public void setBaseCurrencyCode(String baseCurrencyCode) {
        this.baseCurrencyCode = baseCurrencyCode;
    }

    public double getPurchase() {
        return purchase;
    }

    public void setPurchase(double purchase) {
        this.purchase = purchase;
    }

    public double getSale() {
        return sale;
    }

    public void setSale(double sale) {
        this.sale = sale;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof CurrencyExchange) {
            CurrencyExchange ce = (CurrencyExchange) o;
            return (ce.getBaseCurrencyCode().equals(baseCurrencyCode) && ce.getTargetCurrencyCode().equals(targetCurrencyCode));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 79 * hash + Objects.hashCode(this.targetCurrencyCode);
        hash = 79 * hash + Objects.hashCode(this.baseCurrencyCode);
        return hash;
    }
}
