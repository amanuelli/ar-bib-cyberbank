/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.domain.BankBase;

/**
 * Representa un banco
 *
 * @author sbarbosa
 */
public class Bank extends BankBase {

    private String bankName;
    private String bankAddress;
    private String bankCountryCode;
    private String bankCountryLabel;

    public Bank() {
    }
    
    public Bank(String bankType, String bankCode, String bankName, String bankAddress, String bankCountry, String bankCountryLabel) {
        super(bankType,bankCode);
        this.bankName = bankName;
        this.bankAddress = bankAddress;
        this.bankCountryCode = bankCountry;
        this.bankCountryLabel = bankCountryLabel;
    }
    
    public void setType(String type) {
        super.setCodeType(type);
    }
    
    public String getType() {
        return super.getCodeType();
    }
    
    public void setCode(String code) {
        super.setCodeNumber(code);
    }
    
    public String getCode() {
        return super.getCodeNumber();
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String bankName) {
        this.bankName = bankName;
    }

    public String getBankAddress() {
        return bankAddress;
    }

    public void setBankAddress(String bankAddress) {
        this.bankAddress = bankAddress;
    }

    public String getBankCountryCode() {
        return bankCountryCode;
    }

    public void setBankCountryCode(String bankCountryCode) {
        this.bankCountryCode = bankCountryCode;
    }

    public String getBankCountryLabel() {
        return bankCountryLabel;
    }

    public void setBankCountryLabel(String bankCountryLabel) {
        this.bankCountryLabel = bankCountryLabel;
    }

    @Override
    public String toString() {
        return "Bank{" + "type=" + super.getCodeType() + ", code=" + super.getCodeNumber() + bankName + ", bankAddress=" + bankAddress + ", bankCountryCode=" + bankCountryCode + ", bankCountryLabel=" + bankCountryLabel + '}';
    }
}
