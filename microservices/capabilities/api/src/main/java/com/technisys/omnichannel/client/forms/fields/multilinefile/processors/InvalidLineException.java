package com.technisys.omnichannel.client.forms.fields.multilinefile.processors;

import com.technisys.omnichannel.client.domain.TransactionLine.ErrorCode;

public class InvalidLineException extends Exception {

    private final ErrorCode errorCode;

    InvalidLineException(ErrorCode errorCode) {
        super(errorCode.toString());

        this.errorCode = errorCode;
    }

    ErrorCode getErrorCode() {
        return errorCode;
    }
}
