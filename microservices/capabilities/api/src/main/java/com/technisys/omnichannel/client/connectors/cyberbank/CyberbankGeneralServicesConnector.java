/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.cyberbank.domain.FundDestiny;
import com.technisys.omnichannel.client.domain.Bank;
import com.technisys.omnichannel.client.utils.JsonTemplateUtils;
import com.technisys.omnichannel.core.domain.Currency;
import com.technisys.omnichannel.core.utils.CacheUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CyberbankGeneralServicesConnector extends CyberbankCoreConnector {
    private static final Logger LOG = LoggerFactory.getLogger(CyberbankGeneralServicesConnector.class);
    private static final String CURRENCY_LIST_CACHE_KEY = "backendCurrencies";
    private static final String FUND_DESTINY_LIST_CACHE_KEY = "cyberbank.core.fundsDestinys";

    public static CyberbankCoreConnectorResponse<List<Bank>> listCorrespondentBanks(boolean isForInternationalBanks) throws CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<List<Bank>> response = new CyberbankCoreConnectorResponse<>();

        String serviceName = "Srv - List Correspondent banks";

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("massiveSelectBankCorrespondent", true)
                .addRequestParameter("externalCorrespondent", isForInternationalBanks ? "true" : "false")
                .addRequestParameter("internalCorrespondent", "false");
        LOG.info("{}: Request {}", serviceName, URL);

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(serviceName, jsonRequest);

        if (callHasError(serviceResponse)) {
            processErrors(serviceName, serviceResponse, response);
            return response;
        }

        if (serviceResponse.getJSONObject("out.bank_list").has("collection")) {

            JSONArray serviceResponseBanks = serviceResponse.getJSONObject("out.bank_list").getJSONArray("collection");
            List<Bank> banks = new ArrayList<>();
            for (int i = 0; i < serviceResponseBanks.length(); i++) {
                JSONObject serviceBank = serviceResponseBanks.getJSONObject(i);

                Bank bank = new Bank();
                bank.setCode(String.valueOf(serviceBank.getInt("bankId")));
                bank.setBankName(serviceBank.getString("shortDesc"));

                banks.add(bank);
            }

            response.setData(banks);
        } else {
            response.setData(new ArrayList<>());
        }

        return response;
    }

    public static CyberbankCoreConnectorResponse<List<Currency>> listCurrencies() throws CyberbankCoreConnectorException, IOException {
        CyberbankCoreConnectorResponse<List<Currency>> response = new CyberbankCoreConnectorResponse<>();


        List<Currency> currencies = (List<Currency>) CacheUtils.get(CURRENCY_LIST_CACHE_KEY);

        if (currencies != null) {
            response.setData(currencies);
            return response;
        }

        String serviceName = "Srv - massiveSelectCurrency_Active";

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("massiveSelectCurrency_Active", true);
        LOG.info("{}: Request {}", serviceName, URL);

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(serviceName, jsonRequest);

        if (callHasError(serviceResponse)) {
            processErrors(serviceName, serviceResponse, response);
            return response;
        }

        if (serviceResponse.has("out.currency_list")) {
            JSONArray serviceCurrencies = serviceResponse.getJSONObject("out.currency_list").getJSONArray("collection");
            currencies = new ArrayList<>();

            for (int i = 0; i < serviceCurrencies.length(); i++) {
                JSONObject serviceCurrency = serviceCurrencies.getJSONObject(i);

                Currency currency = new Currency(
                        String.valueOf(serviceCurrency.getInt("currencyCodeId")),
                        serviceCurrency.getJSONObject("currencySwift").getString("nemotecnico")
                );

                currencies.add(currency);
            }

            response.setData(currencies);
            CacheUtils.put(CURRENCY_LIST_CACHE_KEY, currencies);
        } else {
            throw new CyberbankCoreConnectorException("Unable to retrieve Cyberbank core currencies");
        }

        return response;
    }

    public static CyberbankCoreConnectorResponse<Currency> getCurrency(String currencyCode) throws IOException, CyberbankCoreConnectorException {
        Currency currency = listCurrencies().getData().stream().filter(c -> c.getCode().equalsIgnoreCase(currencyCode)).findAny().orElse(null);
        CyberbankCoreConnectorResponse<Currency> response = new CyberbankCoreConnectorResponse<>();
        if (currency != null) {
            response.setData(currency);
            return response;
        } else {
            throw new CyberbankCoreConnectorException("Unable to retrieve Cyberbank core currency");
        }

    }

    public static CyberbankCoreConnectorResponse<List<FundDestiny>> listFundDestiny(String subProductId) throws CyberbankCoreConnectorException, IOException {
        CyberbankCoreConnectorResponse<List<FundDestiny>> response = new CyberbankCoreConnectorResponse<>();

        List<FundDestiny> fundDestinyList = (List<FundDestiny>) CacheUtils.get(FUND_DESTINY_LIST_CACHE_KEY);
        if(fundDestinyList != null ){
            response.setData(fundDestinyList);
            return response;
        }

        String serviceName = "Srv - massiveSelectSubproduct_Loan_Fund_Destiny";

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("massiveSelectSubproduct_Loan_Fund_Destiny", true);
        cyberbankCoreRequest.addRequestParameter("subProductId", subProductId);

        LOG.info("{}: Request {}", serviceName, URL);

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(serviceName, jsonRequest);

        if (callHasError(serviceResponse)) {
            processErrors(serviceName, serviceResponse, response);
            return response;
        }

        if(serviceResponse.has("out.subproduct_loan_fund_destiny_list")){
            fundDestinyList = new ArrayList<>();
            JSONArray jsonArrayfundDestini = serviceResponse.getJSONObject("out.subproduct_loan_fund_destiny_list").getJSONArray("collection");
            for(Object object : jsonArrayfundDestini){
                JSONObject funDestiny = ((JSONObject)object).getJSONObject("fundDestiny");
                Integer fundDestinyId =  funDestiny.getInt("fundDestinyId");
                String fundDestinyCode = funDestiny.getString("nemotecnico");
                String fundDestinyDescription = funDestiny.getString("shortDesc");
                fundDestinyList.add(new FundDestiny(fundDestinyId,fundDestinyCode, fundDestinyDescription));
            }
            CacheUtils.put(FUND_DESTINY_LIST_CACHE_KEY, fundDestinyList);
            response.setData(fundDestinyList);
        }else{
            response.setData(new ArrayList<>());
        }

        return response;
    }
}
