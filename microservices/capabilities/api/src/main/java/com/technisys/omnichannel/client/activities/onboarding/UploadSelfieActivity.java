package com.technisys.omnichannel.client.activities.onboarding;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingDigital;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingSafeway;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;


/**
 *
 */
@ExchangeActivity
@DocumentedActivity("This activity uploads the selfie iamge in the onboarding flow")
public class UploadSelfieActivity extends Activity {

    public static final String ID = "onboarding.wizard.uploadSelfie";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Document image to save in base 64 format")
        String DOCUMENT_TO_SAVE = "_documentToSave";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Exchange token to continue the flow")
        String EXCHANGE_TOKEN = "_exchangeToken";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response;
        if("digital".equals(ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "onboarding.mode", "digital"))) {
            response = new OnboardingDigital().doUploadSelfieExecute(request);
        } else {
            response = new OnboardingSafeway().doUploadSelfieExecute(request);
        }
        return response;

    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return OnboardingUtils.validateOnboardingImageLength(request.getParam(UploadFrontDocumentActivity.InParams.DOCUMENT_TO_SAVE, String.class));
    }
}
