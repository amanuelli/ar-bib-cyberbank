/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

/**
 * Clase para el manejo de un pool de objetos
 * @author fpena
 */
public class ObjectPool <T> {

    private static final Logger log = LoggerFactory.getLogger(ObjectPool.class);

    public static interface ObjectPoolFactory<T>
    {
        public T create();
    }
 
    private final BlockingQueue<T> objects;
    private ObjectPoolFactory<T> factory = null;
    private final String poolName;
    private final int poolSize;
    private int objectsCreated = 0;
    private final int warningThreshold;
 
    /**
     * Crea un pool de objetos con un tamaño máximo
     * @param objFactory Factory responsable de crear los objetos
     * @param poolSize Tamaño máximo del pool
     */
    public ObjectPool(String poolName, ObjectPoolFactory<T> objFactory, int poolSize) {
        this.poolName = poolName;
        this.factory = objFactory;
        this.objects = new LinkedBlockingQueue<>(poolSize);
        this.poolSize = poolSize;
        this.warningThreshold = (int)Math.ceil(poolSize * 0.1);
    }
 
    /**
     * Solicitar un objeto del pool. Bloquea hasta tener uno disponible
     * @return Objeto del pool
     * @throws InterruptedException 
     */
    public T borrowObject() throws InterruptedException {
        //Si aún no llegamos al pool size y no pude obtener un objeto al instante, entonces creamos uno nuevo.
        //En algun momento probé inicializarlos estáticamente, pero el primero en hacer referencia a la clase tenia que esperar por la creación de todos los port
        T t = objects.poll();
        
        if (t == null && objectsCreated < poolSize){
            t = createObject();
        }
        
        //notificacion al log de que el poolSize está corto, por ejemplo cuando está libre menos del 10% del pool size
        String logString = "";
        if (objectsCreated == poolSize && objects.size() <= warningThreshold){
            logString = String.format("Waiting. Object pool '%s' with less than 10%% free (%s free of %s total)", poolName, objects.size(), poolSize);
            log.warn(logString);
        }
        
        if (t == null){
            //bloqueo a la espera de un objeto. Podría utilizar poll pero con un timeout
            t = objects.take();
        }
        
        //notificacion al log de que el poolSize está corto, por ejemplo cuando está libre menos del 10% del pool size
        if (objectsCreated == poolSize && objects.size() <= warningThreshold){
            logString = String.format("Object pool '%s' with less than 10%% free (%s free of %s total)", poolName, objects.size(), poolSize);
            log.warn(logString);
        }
    
        return t;
    }
    
    /**
     * Se debe utilizar para retornar el objeto al pool una vez finalizado su uso
     * @param object Objeto obtenido con el método {@link #borrowObject() borrowObject}
     */
    public void returnObject(T object){
        if (object != null && objects.offer(object)) log.info("the object was add in queue");
    }
    
    private synchronized T createObject (){
        if (objectsCreated < poolSize){
            objectsCreated++;
            return factory.create();
        }
        return null;
    }
    
    public int size (){
        return objects.size();
    }
}
