/*
 * Copyright 2019 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.onboarding;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.exceptions.ActivityException;

/**
 * @author Marcelo Bruno
 */

public interface OnboardingSteps {

    Response doUploadFrontDocumentExecute(Request request) throws ActivityException;
    Response doUploadBackDocumentExecute(Request request, String requestExchangeToken) throws ActivityException;
    Response doUploadSelfieExecute(Request request) throws ActivityException;

}
