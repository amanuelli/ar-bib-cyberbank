/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.restrictions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.List;

/**
 * @author Jhossept
 */
@DocumentedActivity("Get if the restrictions are available for the current environment")
public class GetRestrictionsAvailabilityActivity extends Activity {

    public static final String ID = "administration.restrictions.availability.get";

    public interface InParams {

    }

    public interface OutParams {
        @DocumentedParam(type = List.class, description = "Specify if the calendar is enabled")
        String CALENDAR_ENABLED = "calendarEnabled";

        @DocumentedParam(type = List.class, description = "Specify if the IP is enabled")
        String IP_ENABLED = "iPEnabled";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        boolean iPEnabled = false;
        boolean calendarEnabled = false;
        try {
            Environment currentEnv =  Administration.getInstance().readEnvironment(request.getIdEnvironment());
            if(currentEnv != null && !currentEnv.getAdministrationScheme().equals(Environment.ADMINISTRATION_SCHEME_SIMPLE)){
                iPEnabled = currentEnv.isIpRestrictionsEnabled();
                calendarEnabled = currentEnv.getCalendarLoginRestrictionsEnabled();
            }
        } catch (Exception e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
        response.putItem(OutParams.CALENDAR_ENABLED, calendarEnabled);
        response.putItem(OutParams.IP_ENABLED, iPEnabled);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }
}
