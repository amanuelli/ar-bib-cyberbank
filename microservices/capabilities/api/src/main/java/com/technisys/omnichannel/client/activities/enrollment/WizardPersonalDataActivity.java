/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.client.ReturnCodes;
import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 *
 */
@ExchangeActivity
public class WizardPersonalDataActivity extends Activity {

    public static final String ID = "enrollment.wizard.personalData";

    private static final String NEXT_ACTIVITY_IN_FLOW = WizardFinishActivity.ID;

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
    }

    public interface OutParams {

        String EXCHANGE_TOKEN = "_exchangeToken";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            // Actualizo el exchange token
            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);

            List<String> activities = new ArrayList<>(Arrays.asList(StringUtils.split(token.getActivities(), ',')));
            if (!activities.contains(NEXT_ACTIVITY_IN_FLOW)) {
                activities.add(NEXT_ACTIVITY_IN_FLOW);
            }
            if (!activities.contains(ListSecuritySealsActivity.ID)) {
                activities.add(ListSecuritySealsActivity.ID);
            }

            ExchangeTokenHandler.updateActivities(exchangeToken, activities.toArray(new String[0]));

            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            InvitationCode invitation = getInvitationCode(request);

            // Pasos anteriores
            Map<String, String> result = WizardVerificationCodeActivity.validateFields(request, invitation);

            if (!result.isEmpty()) {
                result.put("NO_FIELD", "enrollment.wizard.invalidEnrollmentData");
            }

            result.putAll(validateFields(request, invitation));

            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    protected static Map<String, String> validateFields(Request request, InvitationCode invitation) throws ActivityException {
        return new HashMap<>();
    }

}
