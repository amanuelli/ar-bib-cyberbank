/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.technisys.omnichannel.core.utils.JsonUtils;
import com.technisys.omnichannel.rubicon.core.general.WsClient;
import com.technisys.omnichannel.rubicon.core.general.WsReadClientResponse;

/**
 *
 * @author ?
 */
public class ClientEnvironment implements Serializable {

    private String productGroupId;
    private int idEnvironment;
    private String accountName;
    private String segment;
    private String permissions;
    private String correspondenceAddress;
    private boolean paperStatements;
    
    
    public Map<String, String> getAddionalData() {
        Map<String, String> extendedData = new HashMap<>();
        extendedData.put("correspondenceAddress", StringUtils.trimToNull(this.getCorrespondenceAddress()));
        //mando una key para que en el jsp se renderice el mensaje
        extendedData.put("paperStatements", this.getPaperStatements()?"key=environment.additional.data.paperStatements.yes":"key=environment.additional.data.paperStatements.no");
        return extendedData;
    }
    
    public String getJsonAddionalData() {
        return JsonUtils.toJson(getAddionalData(), false, false);
    }
    

    public ClientEnvironment() {
    }

    public ClientEnvironment(WsClient client) {
        this.productGroupId = ""+client.getClientID();
        this.segment = client.getSegment();
        this.accountName = client.getClientName();
    }

    public ClientEnvironment(WsReadClientResponse response) {
        this.segment = response.getSegment();
        this.accountName = response.getClientName();
        this.permissions = response.getPermissions();
        this.correspondenceAddress = response.getCorrespondenceAddress();
        this.paperStatements = response.isPaperStatements();
    }

    public String getProductGroupId() {
        return productGroupId;
    }

    public void setProductGroupId(String productGroupId) {
        this.productGroupId = productGroupId;
    }

    public int getIdEnvironment() {
        return idEnvironment;
    }

    public void setIdEnvironment(int idEnvironment) {
        this.idEnvironment = idEnvironment;
    }

    public String getSegment() {
        return segment;
    }

    public void setSegment(String segment) {
        this.segment = segment;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public String getPermissions() {
        return permissions;
    }

    public void setPermissions(String permissions) {
        this.permissions = permissions;
    }
    
    public String getCorrespondenceAddress() {
        return correspondenceAddress;
    }

    public void setCorrespondenceAddress(String correspondenceAddress) {
        this.correspondenceAddress = correspondenceAddress;
    }

    public boolean getPaperStatements() {
        return paperStatements;
    }

    public void setPaperStatements(boolean paperStatements) {
        this.paperStatements = paperStatements;
    }

    
    @Override
    public int hashCode() {
        int hash = 5;
        hash = 59 * hash +  Objects.hashCode(this.productGroupId);
        hash = 59 * hash + this.idEnvironment;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        final ClientEnvironment other = (ClientEnvironment) obj;
        if (!this.productGroupId.equals(other.productGroupId)) {
            return false;
        }
        return this.idEnvironment == other.idEnvironment;
    }

}
