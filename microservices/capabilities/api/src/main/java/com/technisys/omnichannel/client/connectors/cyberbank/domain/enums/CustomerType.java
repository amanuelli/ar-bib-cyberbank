package com.technisys.omnichannel.client.connectors.cyberbank.domain.enums;

public enum CustomerType {
    PROSPECT("PROSPECT"),
    CLIENT("CLIENT");

    private final String value;

    private CustomerType(String value) {
        this.value = value;
    }

    public String getValueForService() {
        return value;
    }
}
