/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.utils.RequestParamsUtils;

import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;

public class Address {

    private String addressLine1;
    private String addressLine2;
    private String country;
    private String city;
    private String federalState;
    private String zipcode;
    private Date residentSince;

    public Address() {
    }

    public Address(Map values) {
        this.addressLine1 = RequestParamsUtils.getValue(values, "addressLine1", String.class);
        this.addressLine2 = RequestParamsUtils.getValue(values, "addressLine2", String.class);
        this.city = RequestParamsUtils.getValue(values, "city", String.class);
        this.country = RequestParamsUtils.getValue(values, "country", String.class);
        this.federalState = RequestParamsUtils.getValue(values, "federalState", String.class);
        this.zipcode = RequestParamsUtils.getValue(values, "zipcode", String.class);
        this.residentSince = RequestParamsUtils.getValue(values, "residentSince", Date.class);
    }

    public Address(String addressLine1, String addressLine2, String country, String city, String federalState, String zipcode, Date residentSince) {
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.country = country;
        this.city = city;
        this.federalState = federalState;
        this.zipcode = zipcode;
        this.residentSince = residentSince;
    }

    public Address(JSONObject jsonObject) {
        this.addressLine1 = (String) jsonObject.get("addressLine1");
        if(jsonObject.has("addressLine2")){
            this.addressLine2 = (String) jsonObject.get("addressLine2");
        }
        if(jsonObject.has("country")){
            this.country = (String) jsonObject.get("country");
        }
        this.city = (String) jsonObject.get("city");
        this.federalState = (String) jsonObject.get("federalState");
        this.zipcode = (String) jsonObject.get("zipcode");
        this.residentSince = null;
    }

    public Boolean valdiateFields() {
        return StringUtils.isNoneBlank(addressLine1) &&
                StringUtils.isNoneBlank(country) &&
                StringUtils.isNoneBlank(city) &&
                StringUtils.isNoneBlank(federalState) &&
                StringUtils.isNoneBlank(zipcode);
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getFederalState() {
        return federalState;
    }

    public void setFederalState(String federalState) {
        this.federalState = federalState;
    }

    public String getZipcode() {
        return zipcode;
    }

    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public Date getResidentSince() {
        return residentSince;
    }

    public void setResidentSince(Date residentSince) {
        this.residentSince = residentSince;
    }

    public JSONObject toJSON() {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("addressLine1", addressLine1);
        jsonObject.put("addressLine2", addressLine2);
        jsonObject.put("city", city);
        jsonObject.put("country", country);
        jsonObject.put("federalState", federalState);
        jsonObject.put("zipcode", zipcode);
        return jsonObject;
    }

    @Override
    public String toString() {
        return String.format("%s%s%s%s%s%s", this.addressLine1, this.addressLine2 != null ? this.addressLine2 : "", this.city, this.federalState, this.zipcode, this.residentSince != null ? this.residentSince.toString() : "");
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this.getClass() != o.getClass()) {
            return false;
        }

        Address address = (Address) o;
        return address.getAddressLine1().equals(this.addressLine1) && address.getAddressLine2().equals(this.addressLine2)
                && address.getCity().equals(this.city) && address.getFederalState().equals(this.federalState)
                && address.getZipcode().equals(this.zipcode) && address.getResidentSince().equals(this.residentSince);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }
}
