package com.technisys.omnichannel.client.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.api.client.util.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.text.StrSubstitutor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;

public class JsonTemplateUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(JsonTemplateUtils.class);

    private JsonTemplateUtils() {
    }



    public static String applyTemplateToJson(String jsonMsg, String templatePath) {
        if (jsonMsg != null && !"".equals(jsonMsg)) {
            try {
                HashMap<String, Object> sourceMessageMap = (HashMap) convertJsonToObject(jsonMsg, HashMap.class);
                String template = IOUtils.toString(JsonTemplateUtils.class.getResourceAsStream(templatePath), Charsets.UTF_8);

                JSONObject request = new JSONObject(StrSubstitutor.replace(template, sourceMessageMap, "${_", "_}"));
                applyTemplateToJsonCollection(template, jsonMsg, request);
                //remove string key from template that have no values, ex: ${_anyParameter_}
                String requestMessage = request.toString().replaceAll("\\$\\{.*?_}", "");

                if (LOGGER.isDebugEnabled()) {
                    LOGGER.debug("JsonTemplateUtils applyTemplateToJson jsonRequest {} ", requestMessage);
                }
                return requestMessage;
            } catch (Exception e) {
                String logString = String.format("Can't apply template to Json message. Path %s: ", templatePath);
                LOGGER.error(logString);
            }
        }
        return jsonMsg;
    }

    public static Object convertJsonToObject(String json, Class<?> map) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.configure(JsonParser.Feature.ALLOW_SINGLE_QUOTES, true);
            mapper.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
            return mapper.readValue(json, map);
        } catch (Exception e) {
            LOGGER.error("convertJsonToObject error {}", e);
            return null;
        }
    }

    /**
     * Apply template JSON for JSONArray Request
     * @param template
     * @param jsonMsg
     * @param jsonRequest
     */
    private static void applyTemplateToJsonCollection(String template, String jsonMsg, JSONObject jsonRequest) {
        //Verify if jsonMsg constains Collection type JSONArray
        Map<String, Object> mapCollection = new JSONObject(jsonMsg).toMap().entrySet().stream().filter(map -> map.getValue() instanceof ArrayList).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
        if(!mapCollection.isEmpty()){
            JSONObject jsonTemplate = new JSONObject(template);
            for (Map.Entry<String, Object> entry : mapCollection.entrySet()) {
                //Verify exists key of Collection in Template JSON, by example: in.addressList
                if (keyExists(jsonTemplate, entry.getKey())) {
                    List jsonArrayRequest = (ArrayList) entry.getValue();
                    //Get JSONObject from template example: in.addressList
                    JSONObject jsonElementTemplate =  getJsonObjectByKey(jsonTemplate, entry.getKey());
                    //Get JSONObject from  Request
                    JSONObject jsonElementRequest = getJsonObjectByKey(jsonRequest, entry.getKey());
                    //Remove element coleeciont of  Request and Add collection data
                    //TODO get array dinamically no implicitly
                    if( jsonElementRequest != null && jsonElementTemplate != null) {
                        jsonElementRequest.getJSONArray("collection").remove(0);
                        for (Object jsonElement : jsonArrayRequest) {
                            jsonElementRequest.getJSONArray("collection").put(new JSONObject(StrSubstitutor.replace(jsonElementTemplate.getJSONArray("collection").get(0), (Map) jsonElement, "${_", "_}")));
                        }
                    }
                }
            }
        }
    }

    /**
     * Verify if exist key in the JSON
     *
     * @param jsonObject
     * @param searchedKey
     * @return
     */
    public static boolean keyExists(JSONObject jsonObject, String searchedKey) {
        boolean exists = jsonObject.has(searchedKey);
        if (!exists) {
            Iterator<?> keys = jsonObject.keys();
            while (keys.hasNext()) {
                String key = (String) keys.next();
                if (jsonObject.get(key) instanceof JSONObject) {
                    exists = keyExists((JSONObject) jsonObject.get(key), searchedKey);
                }
            }
        }
        return exists;
    }

    /**
     * Search and get a specific elemnt in JSONObject
     * @param jsonObject
     * @param searchedKey
     * @return
     */

    public static JSONObject getJsonObjectByKey(JSONObject jsonObject, String searchedKey) {
        if(jsonObject == null) return null;
        if(jsonObject.has(searchedKey)) return (JSONObject) jsonObject.get(searchedKey);
        Iterator<?> keys = jsonObject.keys();
        while (keys.hasNext()) {
            String key = (String) keys.next();
            if (jsonObject.get(key) instanceof JSONObject) {
                JSONObject jsonResult =  getJsonObjectByKey( (JSONObject) jsonObject.get(key) , searchedKey);
                if(jsonResult != null){
                    return jsonResult;
                }
            }
        }
        return null;
    }

}
