/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.client.activities.productrequest.creditcard;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.onboarding.UploadFrontDocumentActivity;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.ValidationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.validationcodes.ValidationCodesHandler;

import java.io.IOException;
import java.util.Calendar;

import static com.technisys.omnichannel.client.ReturnCodes.*;

@DocumentedActivity("Validates an email")
public class VerifyEmailActivity extends Activity {

    public static final String ID = "productrequest.creditcard.verifyEmail";

    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "The verification code")
        String CODE = "_code";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "The new exchange token")
        String EXCHANGE_TOKEN = "_exchangeToken";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(INVALID_INVITATION_CODE);

        try {
            String code = request.getParam(InParams.CODE, String.class);
            ValidationCode validationCode = ValidationCodesHandler.readValidationCode(code);

            if (validationCode != null) {
                Long validityInMillis = ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "client.productRequest.invitationCode.validity");
                if (validityInMillis == null) {
                    validityInMillis = 172800000L; // 48hs by default
                }

                Calendar now = Calendar.getInstance();
                long diff = now.getTimeInMillis() - validationCode.getCreationDate().getTime();

                if (diff > validityInMillis || ValidationCode.STATUS_EXPIRED.equals(validationCode.getStatus())) {
                    response.setReturnCode(INVITATION_CODE_EXPIRED);
                } else if (ValidationCode.STATUS_USED.equals(validationCode.getStatus())) {
                    response.setReturnCode(INVITATION_CODE_ALREADY_USED);
                } else if (ValidationCode.STATUS_CANCELLED.equals(validationCode.getStatus())) {
                    response.setReturnCode(INVITATION_CODE_CANCELED);
                } else if (ValidationCode.STATUS_AVAILABLE.equals(validationCode.getStatus())) {
                    String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
                    ExchangeToken token = ExchangeTokenHandler.read(request.getParam(EXCHANGE_TOKEN, String.class));
                    OnboardingHandlerFactory.getInstance().updateStatus(token.getAttribute("idOnboarding"),
                            Onboarding.STATUS_EMAIL_VERIFICATION_DONE);
                    ExchangeTokenHandler.updateActivities(exchangeToken,
                            new String[] { VerifyEmailActivity.ID, UploadFrontDocumentActivity.ID });

                    response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
                    response.setReturnCode(ReturnCodes.OK);
                }
            }

            return response;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

}
