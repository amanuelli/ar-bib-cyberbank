/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnector;
import com.technisys.omnichannel.rubicon.core.loans.WsLoanMovement;

import java.io.Serializable;
import java.util.Date;

/**
 * @author alfredo
 */
public class StatementLoan implements Serializable {

    private String idProduct;
    private int idStatement;
    private Date paidDate;
    private Date dueDate;
    private String concept;
    private Double importAmount;
    private Double interestsAmount;
    private Double capitalAmount;
    private Double currentBalance;
    private String status;
    private int numberOfFees;
    private int feeNumber;
    private String storedFileName;
    private String note;

    public StatementLoan() {
    }

    public StatementLoan(WsLoanMovement movement) {
        this.idStatement = Integer.parseInt(movement.getMovementId());
        this.paidDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(movement.getPaidDate());
        this.concept = movement.getConcept();
        this.importAmount = movement.getImportAmount();
        this.currentBalance = movement.getCurrentBalance();
        this.dueDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(movement.getDueDate());
        this.interestsAmount = movement.getInterestsAmount();
        this.capitalAmount = movement.getCapitalAmount();
        this.feeNumber = movement.getFeeNumber();
        this.status = movement.getStatus();
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public int getIdStatement() {
        return idStatement;
    }

    public void setIdStatement(int idStatement) {
        this.idStatement = idStatement;
    }

    public Date getPaidDate() {
        return paidDate;
    }

    public void setPaidDate(Date date) {
        this.paidDate = date;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public String getConcept() {
        return concept;
    }

    public void setConcept(String concept) {
        this.concept = concept;
    }

    public Double getImportAmount() {
        return importAmount;
    }

    public void setImportAmount(Double importAmount) {
        this.importAmount = importAmount;
    }

    public Double getInterestsAmount() {
        return interestsAmount;
    }

    public void setInterestsAmount(Double interestsAmount) {
        this.interestsAmount = interestsAmount;
    }

    public Double getCapitalAmount() {
        return capitalAmount;
    }

    public void setCapitalAmount(Double capitalAmount) {
        this.capitalAmount = capitalAmount;
    }

    public Double getCurrentBalance() {
        return currentBalance;
    }

    public void setCurrentBalance(Double currentBalance) {
        this.currentBalance = currentBalance;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getNumberOfFees() {
        return numberOfFees;
    }

    public void setNumberOfFees(int numberOfFees) {
        this.numberOfFees = numberOfFees;
    }

    public int getFeeNumber() {
        return feeNumber;
    }

    public void setFeeNumber(int feeNumber) {
        this.feeNumber = feeNumber;
    }

    public String getStoredFileName() {
        return storedFileName;
    }

    public void setStoredFileName(String storedFileName) {
        this.storedFileName = storedFileName;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

}
