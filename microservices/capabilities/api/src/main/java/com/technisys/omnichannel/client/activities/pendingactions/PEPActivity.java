/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.activities.pendingactions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;

import static com.technisys.omnichannel.ReturnCodes.OK;

/**
 * @author Jhossept G
 */
@DocumentedActivity("Accept PEP")
public class PEPActivity extends Activity {

    public static final String ID = "pendingActions.pep";

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            if(user != null) {
                AccessManagementHandlerFactory.getHandler().updateUserPEP(user.getIdUser());
            } else {
                throw new ActivityException(ReturnCodes.IO_ERROR, "User not found on PEP action");
            }

            response.setReturnCode(OK);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        return response;
    }

}
