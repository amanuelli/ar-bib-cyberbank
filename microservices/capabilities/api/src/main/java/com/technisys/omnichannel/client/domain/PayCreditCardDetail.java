/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.rubicon.core.creditcards.WsAmount;
import com.technisys.omnichannel.rubicon.core.creditcards.WsPayCreditCardResponse;

/**
 *
 * @author jbaccino
 */
public class PayCreditCardDetail {
    private int returnCode;
    private String returnCodeDescrption;

    private Amount debitAmount;
    private double rate;

    public PayCreditCardDetail() {
    }

    public PayCreditCardDetail(WsPayCreditCardResponse response) {
        this.returnCode = response.getReturnCode();
        this.returnCodeDescrption = response.getReturnCodeDescription();

        WsAmount wsAmount = response.getDebitAmount();
        
        if(wsAmount!=null) {
        	 this.debitAmount = new Amount(wsAmount.getCurrency(), wsAmount.getQuantity());
        }
       
        this.rate = response.getRate();
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnCodeDescrption() {
        return returnCodeDescrption;
    }

    public void setReturnCodeDescrption(String returnCodeDescrption) {
        this.returnCodeDescrption = returnCodeDescrption;
    }

    public Amount getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(Amount debitAmount) {
        this.debitAmount = debitAmount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

}
