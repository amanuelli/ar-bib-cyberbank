/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain;

import java.io.Serializable;

public class Province implements Serializable {
    private String code;
    private String description;
    private String gpCode;
    private String nemotecnico;

    public Province() {
    }

    public Province(String code) {
        this.code = code;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getGpCode() {
        return gpCode;
    }

    public void setGpCode(String gpCode) {
        this.gpCode = gpCode;
    }

    public String getNemotecnico() {
        return nemotecnico;
    }

    public void setNemotecnico(String nemotecnico) {
        this.nemotecnico = nemotecnico;
    }
}
