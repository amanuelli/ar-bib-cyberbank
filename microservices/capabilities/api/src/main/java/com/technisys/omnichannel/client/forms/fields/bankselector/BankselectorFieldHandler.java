/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.forms.fields.bankselector;

import com.technisys.omnichannel.client.domain.fields.BankselectorField;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.fields.FieldHandler;
import com.technisys.omnichannel.core.forms.fields.FrontendOption;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.DBUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.io.Serializable;
import java.util.*;

/**
 * Implementacion del handler de campos especifica para campos de tipo
 * 'selector'
 *
 * @author ?
 */
public class BankselectorFieldHandler extends FieldHandler {

    private static final String SUBTYPE_FOREIGNERS = "foreigners";
    private static final String SUBTYPE_BANK_INTERMEDIARY = "intermediary";
    private static final String BANK_LABEL_SUFFIX_TARGET = "Target";
    private static final String[] bankKeys = {"BBVA", "ING", "HSBC", "SANTANDER", "SCOTIA"};
    private static final String OPTIONS_OUTPUT="codes";

    /**
     * Obtiene la descripcion corta de la data de una transaccion. Este metodo
     * sera utilizado en backoffice para el despliegue de la informacion breve
     * para una transaccion de creacion/modificacion de un campo de formulario
     *
     * @param map Mapa con los parametros ingresados
     * @param lang Lenguaje para labels
     * @return String en formato html con la descripcion breve de la transaccion
     */
    @Override
    public String getShortDescription(Map<String, String> map, String lang) {
        //no agrego nada especifico de este tipo de campo
        return getBasicShortDescription(map, lang);
    }

    /**
     * Obtiene la descripcion completa de la data de una transaccion. Este
     * metodo sera utilizado en backoffice para el despliegue de la informacion
     * de una transaccion de creacion/modificacion de un campo de formulario
     *
     * @param map Mapa con los parametros ingresados
     * @param lang Lenguaje para labels
     * @return String en formato html con la descripcion completa de la
     * transaccion
     */
    @Override
    public String getFullDescription(Map<String, String> map, String lang) {
        StringBuilder strBuilder = new StringBuilder(getBasicFullDescription(map, lang));

        //info especifica de este tipo de campo
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.bankselector.displayType", lang)).append(":</dt>").append("<dd>").append(i18n.getMessage("backoffice.forms.fields.bankselector.displayType." + map.get("displayType"), lang)).append("</dd>");
        String [] subtypeValues = StringUtils.split(map.get("subType"),".");
        String subType = subtypeValues[0];
        if (SUBTYPE_FOREIGNERS.equals(subType)) {
            //tipo de codigos  ...
            String codeTypeListStr = map.get("codeTypeList");
            if (StringUtils.isNotBlank(codeTypeListStr)) {
                strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.bankselector.codeTypeList", lang)).append(":</dt>");
                strBuilder.append("<dd>");
                String[] codeTypes = codeTypeListStr.split("\\|");
                for (int i = 0; i < codeTypes.length; i++) {
                    if (i > 0) {
                        strBuilder.append(", ");
                    }
                    strBuilder.append(i18n.getMessage("bankselector.codeType." + codeTypes[i], lang));
                }
                strBuilder.append("</dd>");
            }
        }

        //textos internacionalizados
        List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);
        for (String key : languages) {
            if (StringUtils.isNotBlank(map.get("placeholder" + key))) {
                strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.bankselector.placeholder", lang)).append(" (").append(key).append("):</dt>").append("<dd>").append(map.get("placeholder" + key)).append("</dd>");
            }
        }

        for (String key : languages) {
            if (StringUtils.isNotBlank(map.get("requiredError" + key))) {
                strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.bankselector.requiredError", lang)).append(" (").append(key).append("):</dt>").append("<dd>").append(map.get("requiredError" + key)).append("</dd>");
            }
            if (StringUtils.isNotBlank(map.get("invalidError" + key))) {
                strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.bankselector.invalidError", lang)).append(" (").append(key).append("):</dt>").append("<dd>").append(map.get("invalidError" + key)).append("</dd>");
            }
        }

        return strBuilder.toString();
    }

    /**
     * Obtiene la descripcion a utilizar en el PDF de detalle del formulario.
     *
     * @param field Campo de formulario
     * @param lang Lenguaje para labels
     * @return String en formato html con la descripcion completa del campo
     */
    @Override
    public String getPDFDescription(FormField field, String lang) {
        StringBuilder strBuilder = new StringBuilder();

        BankselectorField bsField = (BankselectorField) field;

        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.bankselector.displayType", lang)).append(":</em> ").append(i18n.getMessage("backoffice.forms.fields.bankselector.displayType." + bsField.getDisplayType(), lang)).append("<br/>");
        String [] subtypeValues = StringUtils.split(bsField.getSubType(),".");
        String subType = subtypeValues[0];
        if (SUBTYPE_FOREIGNERS.equals(subType)
            && (bsField.getCodeTypeList() != null)) {
                List<String> codeTypeList = bsField.getCodeTypeList();
                strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.bankselector.codeTypeList", lang)).append(":</em> ");
                for (int i = 0; i < codeTypeList.size(); i++) {
                    if (i > 0) {
                        strBuilder.append(", ");
                    }
                    strBuilder.append(i18n.getMessage("bankselector.codeType." + codeTypeList.get(i), lang));
                }
                strBuilder.append("<br/>");
        }

        //textos internacionalizados
        List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);

        //placeholder
        if (bsField.getPlaceholderMap() != null) {
            for (String key : languages) {
                if (StringUtils.isNotBlank(bsField.getPlaceholderMap().get(key))) {
                    strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.bankselector.placeholder", lang)).append(" (").append(key).append(")").append(":</em> ").append(bsField.getPlaceholderMap().get(key)).append("<br/>");
                }
            }
        }

        //requiredError
        if (bsField.getRequiredErrorMap() != null) {
            for (String key : languages) {
                if (StringUtils.isNotBlank(bsField.getRequiredErrorMap().get(key))) {
                    strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.bankselector.requiredError", lang)).append(" (").append(key).append(")").append(":</em> ").append(bsField.getRequiredErrorMap().get(key)).append("<br/>");
                }
            }
        }

        //invalidError
        if (bsField.getInvalidErrorMap() != null) {
            for (String key : languages) {
                if (StringUtils.isNotBlank(bsField.getInvalidErrorMap().get(key))) {
                    strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.bankselector.invalidError", lang)).append(" (").append(key).append(")").append(":</em> ").append(bsField.getInvalidErrorMap().get(key)).append("<br/>");
                }
            }
        }

        return strBuilder.toString();
    }

    /**
     * Valida los datos ingresados por el usuario y retorna los errores
     * encontrados
     *
     * @param map Mapa con los datos ingresados
     * @return Mapa con los identificadores de campos/keys de mensaje
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, String> validate(Map<String, String> map) throws IOException {
        Map<String, String> result = new HashMap<>();

        //tipo de despliegue
        if (StringUtils.isBlank(map.get("displayType"))) {
            result.put("displayType", "backoffice.forms.fields.bankselector.displayTypeEmpty");
        }
        String [] subtypeValues = StringUtils.split(map.get("subType"),".");
        String subType = subtypeValues[0];
        if (SUBTYPE_FOREIGNERS.equals(subType)
            && (StringUtils.isBlank(map.get("codeTypeList")))) {
                result.put("codeTypeList", "backoffice.forms.fields.bankselector.codeTypeListEmpty");
        }

        return result;
    }

    /**
     * Valida los datos de un campo de tipo selector de producto y retorna los
     * errores encontrados
     *
     * @param field Campo a validar
     * @return Mapa con los identificadores de campos/keys de mensaje
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, String> validate(FormField field) throws IOException {
        Map<String, String> result = new HashMap<>();

        BankselectorField bsField = (BankselectorField) field;

        //tipo de despliegue
        if (StringUtils.isEmpty(bsField.getDisplayType())) {
            result.put("displayType", "backoffice.forms.fields.bankselector.displayTypeEmpty");
        }
        String [] subtypeValues = StringUtils.split(bsField.getSubType(),".");
        String subType = subtypeValues[0];
        if (SUBTYPE_FOREIGNERS.equals(subType)
            && (bsField.getCodeTypeList() == null || bsField.getCodeTypeList().isEmpty())) {
                result.put("codeTypeList", "backoffice.forms.fields.bankselector.codeTypeListEmpty");
        }

        return result;
    }

    /**
     * Valida el valor ingresado para un campo y retorna los errores encontrados
     *
     * @param request Solicitud enviada
     * @param idField Identificador de campo a validar
     * @param fields Mapa de campos &lt;id, campo&gt; del formulario
     * @return Mapa con los identificadores de campos/keys de mensaje
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, String> validateValue(Request request, String idField, Map<String, FormField> fields) throws IOException {
        Map<String, String> result = new HashMap<>();
        FormField field = fields.get(idField);
        String lang = request.getLang();
        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        Map<String, Object> fieldParams = (Map<String, Object>) request.getParameters().get(idField);
        if (fieldParams != null
            && (!this.isBankCodeValid((String) fieldParams.get("type"), (String) fieldParams.get("code")))) {
                try (SqlSession session = DBUtils.getInstance().openReadSession()) {
                    String invalidError = fmh.listFieldMessages(session, idField, field.getIdForm(), field.getFormVersion(), "invalidError").get(lang);
                    result.put(idField, invalidError);
                }
        }

        return result;
    }

    private boolean isBankCodeValid(String type, String code){
        switch (type) {
            case "ABA":
                if (code.length() != 9) {
                    return false;
                }
                break;
            case "BLZ":
                if (code.length() != 5 && code.length() != 8) {
                    return false;
                }
                break;
            case "CHIPS":
                if (code.length() < 4) {
                    return false;
                }
                break;
            case "SWIFT":
                if (code.length() < 8) {
                    return false;
                }
                break;
            case "IBAN":
                if (code.length() < 4) {
                    return false;
                }
                break;
            default:
                break;
        }

        return true;
    }

    /**
     * Indica si el campo tiene un valor definido o esta vacio
     *
     * @param field Campo de formulario
     * @param fieldValue Valor del campo
     * @return Booleano indicando si el campo tiene valor definido
     */
    @Override
    public boolean isHasValue(FormField field, Object fieldValue) {
        Map valueMap = RequestParamsUtils.getValue(fieldValue, Map.class);
        if (valueMap != null) {
            String [] subtypeValues = StringUtils.split(field.getSubType(),".");
            String subType = subtypeValues[0];
            if (SUBTYPE_FOREIGNERS.equals(subType)) {
                String code = RequestParamsUtils.getValue(valueMap.get("code"), String.class);
                String type = RequestParamsUtils.getValue(valueMap.get("type"), String.class);
                Map bank = RequestParamsUtils.getValue(valueMap.get("bank"), Map.class);

                return StringUtils.isNotBlank(code) && StringUtils.isNotBlank(type) && bank != null;
            } else {
                String code = RequestParamsUtils.getValue(valueMap.get("code"), String.class);
                String type = RequestParamsUtils.getValue(valueMap.get("type"), String.class);
                return StringUtils.isNotBlank(code) && StringUtils.isNotBlank(type);
            }
        }
        return false;
    }

    /**
     * Indica si el campo cumple la condicion dada ( &lt;fieldValue&gt; &lt;operator&gt;
     * &lt;comparingValue&gt; ) Para este campo se hace una comparacion lexicografica
     *
     * @param field Campo de formulario
     * @param fieldValue Valor del campo
     * @param operator Operador (&lt;, &lt;=, ==, &gt;, &gt;=)
     * @param comparingValue Valor a comparar
     * @return Booleano indicando si el valor del campo cumple la condicion
     */
    @Override
    public boolean isValueCondition(FormField field, Object fieldValue, String operator, String comparingValue) {
        //@SB: ver si luego se precisa esto, contra que dato comparar
        return false;
    }

    /**
     * Persiste los datos de un campo
     *
     * @param session Sesion transaccional de base de datos
     * @param map Mapa con los datos ingresados
     * @throws java.io.IOException Error de comunicacion
     */
    @Override
    public void createFormField(SqlSession session, Map<String, String> map) throws IOException {
        BankselectorFieldDataAccess bsda = BankselectorFieldDataAccess.getInstance();
        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        //paso el mapa a bean
        BankselectorField field = toField(map);

        //creo el campo selector de banco
        bsda.createField(session, field);

        //creo placeholder
        if (field.getPlaceholderMap() != null) {
            for (String key : field.getPlaceholderMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "placeholder", field.getPlaceholderMap().get(key));
            }
        }

        //creo mensaje de error requerido
        if (field.getRequiredErrorMap() != null) {
            for (String key : field.getRequiredErrorMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "requiredError", field.getRequiredErrorMap().get(key));
            }
        }

        // invalid error messages
        if (field.getInvalidErrorMap() != null) {
            for (String key : field.getInvalidErrorMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "invalidError", field.getInvalidErrorMap().get(key));
            }
        }

    }

    /**
     * Persiste los datos de un campo.
     *
     * @param session Sesion transaccional de base de datos
     * @param field Cammpo a persistir
     * @throws IOException Error de comunicacion
     */
    @Override
    public void createFormField(SqlSession session, FormField field) throws IOException {
        BankselectorField sField = (BankselectorField) field;

        BankselectorFieldDataAccess bsda = BankselectorFieldDataAccess.getInstance();
        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        //creo el campo selector de banco
        bsda.createField(session, sField);

        //creo placeholder
        for (String key : sField.getPlaceholderMap().keySet()) {
            fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "placeholder", sField.getPlaceholderMap().get(key));
        }

        //creo mensaje de error requerido
        for (String key : sField.getRequiredErrorMap().keySet()) {
            fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "requiredError", sField.getRequiredErrorMap().get(key));
        }

        // invalid error messages
        for (String key : sField.getInvalidErrorMap().keySet()) {
            fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "invalidError", sField.getInvalidErrorMap().get(key));
        }

    }

    /**
     * Actualiza los datos de un campo
     *
     * @param session Sesion transaccional de base de datos
     * @param map Mapa con los nuevos datos ingresados
     * @throws IOException Error de comunicacion
     */
    @Override
    public void updateFormField(SqlSession session, Map<String, String> map) throws IOException {
        BankselectorField field = toField(map);

        //borro el campo y lo creo de nuevo
        //esto tiene que ser una eleccion de la implementacion particular,
        //en algunos casos se podra hacer esto y en otros habra que hacer una actualizacion
        //de los datos (dependiendo de la estructuracion de cada implementacion en base
        //y de las dependencias que se definan para el tipo de campo
        //para este tipo de campo no hay problema de hacer un delete y create
        BankselectorFieldDataAccess bsda = BankselectorFieldDataAccess.getInstance();
        bsda.deleteField(session, field.getIdForm(), map.get("oldId"));
        bsda.createField(session, field);

        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        //modifico placeholder
        fmh.deleteFieldMessage(session, map.get("oldId"), field.getIdForm(), "placeholder");
        if (field.getPlaceholderMap() != null) {
            for (String key : field.getPlaceholderMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "placeholder", field.getPlaceholderMap().get(key));
            }
        }

        //modifico mensaje de error requerido
        fmh.deleteFieldMessage(session, map.get("oldId"), field.getIdForm(), "requiredError");
        if (field.getRequiredErrorMap() != null) {
            for (String key : field.getRequiredErrorMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "requiredError", field.getRequiredErrorMap().get(key));
            }
        }

        // modify invalid error messages
        fmh.deleteFieldMessage(session, map.get("oldId"), field.getIdForm(), "invalidError");
        if (field.getInvalidErrorMap() != null) {
            for (String key : field.getInvalidErrorMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "invalidError", field.getInvalidErrorMap().get(key));
            }
        }

    }

    /**
     * Lee los datos de un campo
     *
     * @param idForm Identificador de formulario
     * @param formVersion Versión de formulario
     * @param idField Identificador de campo
     * @param withMessages indica si cargar mensajes, placeholders, etc
     * @return Datos especificos del campo
     * @throws IOException Error de comunicacion
     */
    @Override
    public FormField readFormField(String idForm, Integer formVersion, String idField, boolean withMessages) throws IOException {

        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        BankselectorField field;
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {
            field = BankselectorFieldDataAccess.getInstance().readField(session, idForm, formVersion, idField);
            if(withMessages){
                //listo los placeholders
                field.setPlaceholderMap(fmh.listFieldMessages(session, idField, idForm, field.getFormVersion(), "placeholder"));

                //listo errores requerido
                field.setRequiredErrorMap(fmh.listFieldMessages(session, idField, idForm, field.getFormVersion(), "requiredError"));

                // list invalid errors
                field.setInvalidErrorMap(fmh.listFieldMessages(session, idField, idForm, field.getFormVersion(), "invalidError"));
            }
        }

        return field;
    }

    /**
     * Borra los datos de un campo
     *
     * @param session Sesion transaccional de base de datos
     * @param idForm Identificador de formulario
     * @param idField Identificador del campo
     * @throws IOException Error de comunicacion
     */
    @Override
    public void deleteFormField(SqlSession session, String idForm, int formVersion, String idField) throws IOException {
        BankselectorFieldDataAccess.getInstance().deleteField(session, idForm, idField);

        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        //borro placeholder
        fmh.deleteFieldMessage(session, idField, idForm, "placeholder");

        //borro error requerido
        fmh.deleteFieldMessage(session, idField, idForm, "requiredError");

        // remove invalid errors
        fmh.deleteFieldMessage(session, idField, idForm, "invalidError");

    }

    /**
     * Obtiene la informacion requerida para el despliegue de configuracion en
     * backoffice
     *
     * @param idForm Identificador de formulario
     * @param idField Identificador de campo del formulario
     * @param lang Lenguaje
     * @return Mapa con la informacion requerida para configuracion (listados,
     * parametros, etc)
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, Serializable> getFieldDataForConfiguration(String idForm, String idField, String lang) throws IOException {
        Map<String, Serializable> data = new HashMap<>();

        List<String> codeTypeList = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "bankselector.validCodeTypes", String.class);
        data.put("codeTypeList", (Serializable) codeTypeList);

        return data;
    }

    /**
     * Dado un Request y el campo especifico carga, de ser necesario, la
     * información de despliegue del mismo. Por ejemplo para el caso de un
     * selector de productos, este método debe retornar la lista de las opciones
     * de dicho selector (list de identificador de productos, su label, etc).
     *
     * @param field Campo del formulario
     * @param request Request del usuario para obtener el usuario, ambiente, etc
     * @param transaction Transaccion (opcional) que se esta queriendo desplegar
     * junto con el formulario,
     * @return Retorna un Mapa con la información necesaria para armar el
     * despliegue del campo. En caso de no requerilo retorna null.
     * @throws IOException Error de comunicacion
     * @throws org.quartz.SchedulerException
     */
    @Override
    public Map<String, Serializable> loadFormFieldData(FormField field, Request request, Transaction transaction) throws IOException, SchedulerException {
        //En base al parametro lastVersion se puede saber si es para modo vista o edicion. 
        //En caso de modo vista debe venir un atributo idTransaction para saber que data cargar
        Map<String, Serializable> data = super.loadFormFieldData(field, request, transaction);
        I18n i18n = I18nFactory.getHandler();

        BankselectorField bankselectorField = (BankselectorField) field;
        String [] subtypeValues = StringUtils.split(bankselectorField.getSubType(),".");
        String subType = subtypeValues[0];
        
        if (transaction == null || Transaction.STATUS_DRAFT.equals(transaction.getIdTransactionStatus())) {
            
            if (SUBTYPE_FOREIGNERS.equals(subType)) {

                List<FrontendOption> options = new ArrayList<>();

                options.add(new FrontendOption("", i18n.getMessage("form.selectors.emptyOptionLabel", request.getLang())));
                if (bankselectorField.getCodeTypeList() != null) {
                    for (String codeType : bankselectorField.getCodeTypeList()) {
                        options.add(new FrontendOption(codeType, i18n.getMessage("forms.bankselector.codeType." + codeType, request.getLang())));
                    }
                }

                data.put(OPTIONS_OUTPUT, (Serializable) options);

                options = new ArrayList<>();

                for (String country : CountryCodesHandler.listCountryCodes()) {
                    options.add(new FrontendOption(country, i18n.getMessage("country.name." + country, request.getLang())));
                }
                
                Collections.sort(options, (FrontendOption o1, FrontendOption o2) -> {
                    if (o1 != null && o2 != null) {
                        if (o1.getLabel() != null) {
                            return o1.getLabel().compareTo(o2.getLabel());
                        } else {
                            return -1;
                        }
                    } else {
                        return 0;
                    }
                });                

                options.add(0, new FrontendOption("", i18n.getMessage("form.selectors.emptyOptionLabel", request.getLang())));

                data.put("countries", (Serializable) options);

                bankselectorField.addExtraLabel("searchBank", i18n.getMessage("bankselector.searchBank", request.getLang()));
                bankselectorField.addExtraLabel("tableCode", i18n.getMessage("bankselector.tableCode", request.getLang()));
                bankselectorField.addExtraLabel("tableName", i18n.getMessage("bankselector.tableName", request.getLang()));
                bankselectorField.addExtraLabel("tableCountry", i18n.getMessage("bankselector.tableCountry", request.getLang()));

                String bankType = subtypeValues.length > 1 ? subtypeValues[1] : "" ;
                String messageKeySuffix = bankType.equals(SUBTYPE_BANK_INTERMEDIARY) ? "" : BANK_LABEL_SUFFIX_TARGET;
                bankselectorField.addExtraLabel("infoBankAddress", i18n.getMessage("bankselector.infoBankAddress" + messageKeySuffix, request.getLang()));
                bankselectorField.addExtraLabel("infoBankName", i18n.getMessage("bankselector.infoBankName" + messageKeySuffix, request.getLang()));
                bankselectorField.addExtraLabel("infoBankCountry", i18n.getMessage("bankselector.infoBankCountry" + messageKeySuffix, request.getLang()));

                bankselectorField.addExtraLabel("close", i18n.getMessage("global.close", request.getLang()));
                bankselectorField.addExtraLabel("filterBankCode", i18n.getMessage("bankselector.filterBankCode", request.getLang()));
                bankselectorField.addExtraLabel("filterExtraInfo", i18n.getMessage("bankselector.filterExtraInfo", request.getLang()));
                bankselectorField.addExtraLabel("filterCountryPlaceholder", i18n.getMessage("bankselector.filterCountryPlaceholder", request.getLang()));
                bankselectorField.addExtraLabel("filterBankNamePlaceholder", i18n.getMessage("bankselector.filterBankNamePlaceholder", request.getLang()));

                bankselectorField.addExtraLabel("mustPerformSearch", i18n.getMessage("bankselector.mustPerformSearch", request.getLang()));
                bankselectorField.addExtraLabel("hasMoreResults", i18n.getMessage("bankselector.hasMoreResults", request.getLang()));
                bankselectorField.addExtraLabel("emptyList", i18n.getMessage("bankselector.noRecordsFound", request.getLang()));
                bankselectorField.addExtraLabel("performSearchInfo", i18n.getMessage("bankselector.performSearchInfo", request.getLang()));

                bankselectorField.addExtraLabel("nameToShort", i18n.getMessage("bankselector.nameToShort", request.getLang()));
                bankselectorField.addExtraLabel("typeRequired", i18n.getMessage("bankselector.typeRequired", request.getLang()));
                bankselectorField.addExtraLabel("codeOrNameNeeded", i18n.getMessage("bankselector.codeOrNameNeeded", request.getLang()));
            } else {

                List<FrontendOption> options = new ArrayList<>();

                options.add(new FrontendOption("", i18n.getMessage("form.selectors.emptyOptionLabel", request.getLang())));

                for(String bankKey : bankKeys) {
                    options.add(new FrontendOption(bankKey, i18n.getMessage("bank.name." + bankKey, request.getLang())));
                }
                data.put(OPTIONS_OUTPUT, (Serializable) options);
            }

            //textos internacionalizados
            List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);

            //required
            if (bankselectorField.getRequiredErrorMap() != null) {
                for (String lang : languages) {
                    if (StringUtils.isBlank(bankselectorField.getRequiredErrorMap().get(lang))) {
                        bankselectorField.getRequiredErrorMap().put(lang, i18n.getMessage("fields.defaultForm.defaultField.requiredError", lang));
                    }
                }
            }

            // invalid
            if (bankselectorField.getInvalidErrorMap() != null) {
                for (String lang : languages) {
                    if (StringUtils.isBlank(bankselectorField.getInvalidErrorMap().get(lang))) {
                        bankselectorField.getInvalidErrorMap().put(lang, i18n.getMessage("fields.defaultForm.defaultField.invalidError", lang));
                    }
                }
            }

        } else {

            if (SUBTYPE_FOREIGNERS.equals(subType)) {
                List<FrontendOption> options = new ArrayList<>();
                Map valueMap = RequestParamsUtils.getValue(transaction.getData().get(field.getIdField()), Map.class);
                if (valueMap != null) {
                    String type = RequestParamsUtils.getValue(valueMap.get("type"), String.class);
                    options.add(new FrontendOption(type, i18n.getMessage("bankselector.codeType." + type, request.getLang())));
                }
                data.put("codes", (Serializable) options);

                bankselectorField.addExtraLabel("infoBankAddress", i18n.getMessage("bankselector.infoBankAddress", request.getLang()));
                bankselectorField.addExtraLabel("infoBankName", i18n.getMessage("bankselector.infoBankName", request.getLang()));
                bankselectorField.addExtraLabel("infoBankCountry", i18n.getMessage("bankselector.infoBankCountry", request.getLang()));

            }
        }

        return data;
    }

    /**
     * Dado un mapa, creo la bean especifica Este metodo es especifico de la
     * implementacion del campo y por tanto no obligatorio de la interfase
     *
     * @param map Mapa con los datos del campo
     * @return Instancia especifica del campo de tipo selector
     */
    private BankselectorField toField(Map<String, String> map) {
        BankselectorField ff = new BankselectorField();
        ff.setIdField(map.get("id"));
        ff.setIdForm(map.get("idForm"));
        if (StringUtils.isNotEmpty(map.get("formVersion"))) {
            ff.setFormVersion(Integer.parseInt(map.get("formVersion")));
        }
        ff.setType(map.get("type"));
        ff.setDisplayType(map.get("displayType"));

        String codeTypeListStr = map.get("codeTypeList");
        if (StringUtils.isNotBlank(codeTypeListStr)) {
            String[] codeTypes = codeTypeListStr.split("\\|");
            for (String codeType : codeTypes) {
                ff.addCodeType(codeType);
            }
        }

        List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);

        for (String lang : languages) {
            if (StringUtils.isNotEmpty(map.get("placeholder" + lang))) {
                ff.addPlaceholder(lang, map.get("placeholder" + lang));
            }

            if (StringUtils.isNotEmpty(map.get("requiredError" + lang))) {
                ff.addRequiredError(lang, map.get("requiredError" + lang));
            }

            if (StringUtils.isNotEmpty(map.get("invalidError" + lang))) {
                ff.addInvalidError(lang, map.get("invalidError" + lang));
            }
        }

        return ff;
    }

}
