package com.technisys.omnichannel.client.connectors.cyberbank.domain.enums;

public enum AddressType {
    REAL("Domicilio Real"),
    LEGAL("Domicilio Legal"),
    PARTICULAR("PARTICULAR");

    private final String value;

    private AddressType(String value) {
        this.value = value;
    }

    public String getValueForService() {
        return value;
    }
}
