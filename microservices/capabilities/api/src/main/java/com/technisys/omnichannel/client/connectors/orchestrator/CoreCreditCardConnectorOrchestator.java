/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.*;
import org.apache.commons.lang3.NotImplementedException;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class CoreCreditCardConnectorOrchestator extends CoreConnectorOrchestrator {

    public CoreCreditCardConnectorOrchestator(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    public static CreditCard readCreditCardDetails(String idTransaction, String idProduct, String extraInfo) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.readCreditCardDetails(idTransaction, idProduct, extraInfo);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException( CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static StatementCreditCard readStatementDetails(String idTransaction, String idCreditCard, int idStatement, String creditCardProductExtraInfo) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.readStatementDetails(idTransaction, idCreditCard, idStatement, creditCardProductExtraInfo);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException(CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static List<StatementLine> listCreditCardStatementLines(String idAccount) throws IOException, BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.listCreditCardStatementLines(idAccount);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException(CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static StatementLine readCreditCardStatementLine(String idCreditCard) throws IOException, BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.readCreditCardStatementLine(idCreditCard);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException(CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static int getTotalCreditCardStatements(String extraInfo) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.getTotalCreditCardStatements(extraInfo);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException(CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static int getCurrentPeriodCreditCardStatements(String extraInfo) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.getCurrentPeriodCreditCardStatements(extraInfo);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException(CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static List<StatementCreditCard> listCreditCardStatements(String idTransaction, String idProduct, String extraInfo, Date dateFrom, Date dateTo, Double minAmount, Double maxAmount, String concept, String currency, int limit, int offset) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.listCreditCardStatements(idTransaction, idProduct, extraInfo, dateFrom, dateTo, minAmount, maxAmount, concept, currency, limit, offset);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException(CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static PayCreditCardDetail payCreditCardValidate(String idTransaction, String idClient, Account debitAccount, String creditcardNumber, String creditcardBank, String currency, double amount, String reference, double latitude, double longitude) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.payCreditCardValidate(idTransaction, idClient, debitAccount, creditcardNumber,
                        creditcardBank, currency, amount, reference, latitude, longitude);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException(CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static PayCreditCardDetail payCreditCardSend(String idTransaction, String idClient, Account debitAccount, String creditcardNumber, String creditcardBank, String currency, double amount, String reference, double latitude, double longitude) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.payCreditCardValidate(idTransaction, idClient, debitAccount, creditcardNumber,
                        creditcardBank, currency, amount, reference, latitude, longitude);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException(CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static CreditCard createCreditCard(String idTransaction, String clientId, String cardType) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorTC.createCreditCard(idTransaction, clientId, cardType);
            case CYBERBANK_CONNECTOR:
                throw new NotImplementedException(CYBERBANK_CONNECTOR + " does not support this feature");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }
}
