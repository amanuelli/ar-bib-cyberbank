/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnector;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.rubicon.core.loans.WsReadLoanResponse;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author pfernandez
 */
public class Loan extends ClientProduct {

    private String alias;
    private double totalAmount;
    private Date nextDueDate;
    private Date constitutedDate;
    private double paymentAmount;
    private double paidAmount;
    private String currency;
    private double pendingBalance;
    private double interestRate;
    private double interests;
    private double debtLeft;
    private int numberOfPaidFees;
    private int numberOfFees;
    private double paidPercentage;

    public Loan() {
    }

    public Loan(Product product) {
        super(product);
        this.currency = ProductUtils.getCurrency(product.getExtraInfo());
    }

    public Loan(Map<String, String> fieldsMap) {
        super(fieldsMap);

        this.nextDueDate = RubiconCoreConnector.parseDate(fieldsMap.get("nextDueDate"));
        this.totalAmount = RubiconCoreConnector.parseDouble(fieldsMap.get("totalAmount"));
        this.interests = RubiconCoreConnector.parseDouble(fieldsMap.get("interests"));
        this.paidAmount = RubiconCoreConnector.parseDouble(fieldsMap.get("paidAmount"));
        this.paymentAmount = RubiconCoreConnector.parseDouble(fieldsMap.get("paymentAmount"));
        this.numberOfPaidFees = Integer.parseInt(fieldsMap.get("numberOfPaidFees"));
        this.numberOfFees = Integer.parseInt(fieldsMap.get("numberOfFees"));

        this.currency = fieldsMap.get(ProductUtils.FIELD_PRODUCT_CURRENCY);
    }

    public Loan(WsReadLoanResponse response) {
        super(RubiconCoreConnector.convertDynamicListToMap(response.getFields().getField()));
        this.totalAmount = response.getTotalAmount();
        this.nextDueDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(response.getNextDueDate());
        this.paymentAmount = response.getPaymentAmount();
        this.currency = ProductUtils.getCurrency(extraInfo);
        this.pendingBalance = response.getPendingBalance();
        this.interestRate = response.getInterestRate();
        this.numberOfFees = response.getNumberOfFees();
        this.numberOfPaidFees = response.getNumberOfPaidFees();
        this.paidAmount = response.getPaidAmount();
        this.interests = response.getInterests();
        this.constitutedDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(response.getConstitutedDate());
        this.consolidatedAmount = response.getConsolidatedAmount();
    }

    public double getPaidAmount() {
        return paidAmount;
    }

    public void setPaidAmount(double paidAmount) {
        this.paidAmount = paidAmount;
    }

    public int getNumberOfPaidFees() {
        return numberOfPaidFees;
    }

    public void setNumberOfPaidFees(int numberOfPaidFees) {
        this.numberOfPaidFees = numberOfPaidFees;
    }

    public int getNumberOfFees() {
        return numberOfFees;
    }

    public void setNumberOfFees(int numberOfFees) {
        this.numberOfFees = numberOfFees;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public double getTotalAmount() {
        return totalAmount;
    }

    public void setTotalAmount(double totalAmount) {
        this.totalAmount = totalAmount;
    }

    public Date getConstitutedDate() {
        return constitutedDate;
    }

    public void setConstitutedDate(Date constitutedDate) {
        this.constitutedDate = constitutedDate;
    }

    public Date getNextDueDate() {
        return nextDueDate;
    }

    public void setNextDueDate(Date nextDueDate) {
        this.nextDueDate = nextDueDate;
    }

    public double getPaymentAmount() {
        return paymentAmount;
    }

    public void setPaymentAmount(double paymentAmount) {
        this.paymentAmount = paymentAmount;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getPendingBalance() {
        return pendingBalance;
    }

    public void setPendingBalance(double pendingBalance) {
        this.pendingBalance = pendingBalance;
    }

    public double getInterestRate() {
        return interestRate;
    }

    public void setInterestRate(double interestRate) {
        this.interestRate = interestRate;
    }

    public double getInterests() {
        return interests;
    }

    public void setInterests(double interests) {
        this.interests = interests;
    }

    public double getDebtLeft() {
        return debtLeft;
    }

    public void setDebtLeft(double debtLeft) {
        this.debtLeft = debtLeft;
    }

    public double getPaidPercentage() {
        return paidPercentage;
    }

    public void setPaidPercentage(double paidPercentage) {
        this.paidPercentage = paidPercentage;
    }

}
