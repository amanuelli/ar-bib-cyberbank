package com.technisys.omnichannel.client.activities.administration.medium;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;

@DocumentedActivity("Modify user permissions preview")
public class ModifyPermissionsPreviewActivity extends Activity {

    public static final String ID = "administration.medium.modify.permissions.preview";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "User id to modify permissions")
        String ID_USER = "idUser";
        @DocumentedParam(type = Map.class, description = "Permissions map to set for the user")
        String PERMISSIONS = "permissions";
    }

    public interface OutParams {
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        response.setReturnCode(ReturnCodes.OK);

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return ModifyPermissionsActivity.validateFields(request);
    }
}
