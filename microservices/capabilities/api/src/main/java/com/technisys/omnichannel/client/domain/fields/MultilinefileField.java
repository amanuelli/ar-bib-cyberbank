package com.technisys.omnichannel.client.domain.fields;

import com.technisys.omnichannel.activities.other.CreateTransactionTemplateActivity;
import com.technisys.omnichannel.activities.other.SaveDraftTransactionActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedTypeParam;
import com.technisys.omnichannel.client.forms.fields.multilinefile.processors.ProcessorFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.domain.FormFieldWithCap;
import com.technisys.omnichannel.core.files.FilesHandler;
import com.technisys.omnichannel.core.forms.fields.FileField;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

public class MultilinefileField extends FormFieldWithCap implements FileField {

    private static final Logger log = LoggerFactory.getLogger(MultilinefileField.class);

    @DocumentedTypeParam(description = "Internationalized messages to display when no field value is set")
    private Map<String, String> requiredErrorMap;

    @DocumentedTypeParam(description = "Maximum file size to be uploaded")
    private int maxFileSizeMB;

    @DocumentedTypeParam(description = "List of accepted file types")
    private List<String> acceptedFileTypes;

    @Override
    public String getLabelDefault() {
        return I18nFactory.getHandler().getMessage("core.notApply", ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "core.default.lang", "es"));
    }

    public Map<String, String> getRequiredErrorMap() {
        return requiredErrorMap;
    }

    public void setRequiredErrorMap(Map<String, String> requiredErrorMap) {
        this.requiredErrorMap = requiredErrorMap;
    }

    public void addRequiredError(String lang, String value) {
        if (requiredErrorMap == null) {
            requiredErrorMap = new LinkedHashMap<>();
        }
        requiredErrorMap.put(lang, value);
    }

    /**
     * @return the acceptedFileTypes
     */
    public List<String> getAcceptedFileTypes() {
        return acceptedFileTypes;
    }

    /**
     * @param acceptedFileTypes the acceptedFileTypes to set
     */
    public void setAcceptedFileTypes(List<String> acceptedFileTypes) {
        this.acceptedFileTypes = acceptedFileTypes;
    }

    /**
     * @return the maxFileSizeMB
     */
    public int getMaxFileSizeMB() {
        return maxFileSizeMB;
    }

    /**
     * @param maxFileSizeMB the maxFileSizeMB to set
     */
    public void setMaxFileSizeMB(int maxFileSizeMB) {
        this.maxFileSizeMB = maxFileSizeMB;
    }

    @Override
    public List<Integer> getFiles(String idActivity, Map<String, Object> parameters) {
        List<Integer> files = new ArrayList();
        Object obj = parameters.get(idField);
        if (SaveDraftTransactionActivity.ID.equals(idActivity) || CreateTransactionTemplateActivity.ID.equals(idActivity)) {
            Object paramValue = parameters.get("transactionData");
            obj = RequestParamsUtils.getValue(paramValue, Map.class).get(idField);
        }

        if (obj instanceof List) {
            List value = (List<Map>) obj;
            value.forEach(file -> files.add((Integer) ((Map) file).get("fileId")));
        }
        return files;
    }

    @Override
    public List<Amount> getAmounts(Map<String, Object> parameters) {
        List<Amount> amounts = new ArrayList();
        String logMessage;
        List inputFile = RequestParamsUtils.getValue(parameters.get(idField), List.class);

        if (inputFile !=null && !inputFile.isEmpty()) {
            Map fileParam = (Map) inputFile.get(0);
            if (fileParam != null) {
                Integer idFile = (Integer) fileParam.get("fileId");
                //I read the file unauthenticated just to get the totalAmount
                //The dispatcher already check permissions, etc.
                if (idFile != null && idFile > 0) {
                    try {
                        File file = FilesHandler.getInstance().readUnauthenticatedWithContents(idFile);
                        if (file != null) {
                            amounts.add((Amount) ProcessorFactory.getProcessorSubType(this.getSubType()).validate(file.getContents(), I18n.LANG_ENGLISH).get("totalAmount"));
                        } else {
                            logMessage= MessageFormat.format("Missing file {0} in \"files\" table", idFile);
                            log.error(logMessage);
                        }
                    } catch (IOException ioE) {
                        log.error("Error processing file " + idFile, ioE);
                    }
                }
            }
        }

        return amounts;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof MultilinefileField) {
            MultilinefileField ff = (MultilinefileField) o;
            return (ff.getIdForm().equals(idForm) && ff.getIdField().equals(idField));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.idField);
        hash = 41 * hash + Objects.hashCode(this.idForm);
        hash = 41 * hash + getFormVersion();
        return hash;
    }
}
