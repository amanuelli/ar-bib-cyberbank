package com.technisys.omnichannel.client.activities.administration.advanced.group.create;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.common.Permissions;
import com.technisys.omnichannel.client.activities.administration.medium.ModifyPermissionsActivity;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.client.utils.StringUtils.stripHTML;

/**
 * Use this to create a new group
 */
@DocumentedActivity("Create a new group")
public class GroupCreateActivity extends Activity {

    public static final String ID = "administration.advanced.group.create.send";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Group's name")
        String NAME = "name";
        @DocumentedParam(type = String.class, description = "Group's description")
        String DESCRIPTION = "description";
        @DocumentedParam(type = String.class, description = "Group's status")
        String STATUS = "status";
        @DocumentedParam(type = List.class, description = "Group's users")
        String USERS = "users";
        @DocumentedParam(type = Map.class, description = "Group's permissions")
        String PERMISSIONS = "permissions";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Group's id, it can be used to redirect to the group detail page")
        String ID_GROUP = "idGroup";
    }

    public static void validateUsers(Request request) throws IOException, ActivityException {
        Administration admin = Administration.getInstance();
        List<String> envUsers = admin.listEnvironmentUserIds(request.getIdEnvironment());
        String[] users = request.getParam(InParams.USERS, String[].class);

        if (users != null) {
            for (String idUser : users) {
                if (!envUsers.contains(idUser)) {
                    throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                }
            }
        }
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (!Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            String name = request.getParam(InParams.NAME, String.class);
            String description = request.getParam(InParams.DESCRIPTION, String.class);
            String status = request.getParam(InParams.STATUS, String.class);
            String[] users = request.getParam(InParams.USERS, String[].class);
            Group group = new Group(stripHTML(name), stripHTML(description), request.getIdEnvironment());
            int groupId = group.getIdGroup();
            Map<String, List<String>> newPermissions = request.getParam(InParams.PERMISSIONS, Map.class);
            List<GroupPermission> defaultUserPermissions = Permissions.listDefault(groupId);
            List<GroupPermission> permissions = new ArrayList<>();

            permissions.addAll(defaultUserPermissions);
            Permissions.addIncomingPermissions(groupId, newPermissions, permissions);
            AccessManagementHandlerFactory.getHandler().createGroup(group, (users != null ? Arrays.asList(users) : new ArrayList<>()), permissions);

            if ("blocked".equals(status)) {
                AccessManagementHandlerFactory.getHandler().updateGroupsStatus(Arrays.asList(group.getIdGroup()), true);
            }

            response.putItem(OutParams.ID_GROUP, group.getIdGroup());
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            Administration admin = Administration.getInstance();
            List<String> envProducts = admin.listEnvironmentProductIds(request.getIdEnvironment());
            String name = request.getParam(InParams.NAME, String.class);
            Map<String, List<String>> newPermissions = request.getParam(ModifyPermissionsActivity.InParams.PERMISSIONS, Map.class);

            validateName(request, result, name);
            Permissions.validatePermissions(result, envProducts, newPermissions);
            validateUsers(request);

            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    private static void validateName(Request request, Map<String, String> result, String name) throws IOException {
        if (StringUtils.isEmpty(stripHTML(name))) {
            result.put("name", "administration.groups.nameEmpty");
        } else if (name.length() > 253) {
            result.put("name", "administration.groups.nameMaxLengthExceeded");
        } else {
            List<Group> listGroups = AccessManagementHandlerFactory.getHandler().getGroups(null, null, request.getIdEnvironment(), 0, 0, null).getElementList();
            boolean duplicatedName = false;

            for (int i = 0; i < listGroups.size() && !duplicatedName; i++) {
                Group group = listGroups.get(i);

                if (name.equals(group.getName())) {
                    duplicatedName = true;
                }
            }

            if (duplicatedName) {
                result.put("name", "administration.groups.nameAlreadyExists");
            }
        }
    }
}
