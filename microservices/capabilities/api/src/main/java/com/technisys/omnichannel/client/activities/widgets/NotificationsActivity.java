/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.widgets;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.client.domain.Expiration;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;
import static com.technisys.omnichannel.client.domain.Expiration.STATUS_EXPIRED;
import static com.technisys.omnichannel.client.domain.Expiration.STATUS_WARNING;

public class NotificationsActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(NotificationsActivity.class);
    public static final String ID = "widgets.notifications";

    public interface InParams {
    }

    public interface OutParams {

        String EXPIRATIONS = "expirations";
        String TRANSACTIONS = "transactions";
        String COMMUNICATIONS = "communications";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        List<Map<String, Object>> pendingTransactions = getPendingTransactions(request);
        List<Expiration> nextExpirations = getNextExpirations(request);
        List<CommunicationUser> communications = getLastCommunications(request);
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        response.putItem(OutParams.EXPIRATIONS, nextExpirations);
        response.putItem(OutParams.TRANSACTIONS, pendingTransactions);
        response.putItem(OutParams.COMMUNICATIONS, communications);
        return response;
    }

    public List<CommunicationUser> getLastCommunications(Request request) throws ActivityException {
        try {
            List<CommunicationUser> list = NotificationsHandlerFactory.getHandler().listLatestCommunications(request.getIdUser(), Communication.TRANSPORT_DEFAULT, true,
                    ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "widgets.notifications.maxCommunications"));
            return list;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    @SuppressWarnings("unchecked")
    public List<Expiration> getNextExpirations(Request request) throws ActivityException {
        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            List<Expiration> result = new ArrayList<>();

            List<String> idPermissions = new ArrayList<>();
            idPermissions.add(PRODUCT_READ_PERMISSION);
            List<String> productTypes = Arrays.asList(Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY, Constants.PRODUCT_TC_KEY);
            List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);
            List<Product> backendProducts = (List<Product>) RubiconCoreConnectorC.listProducts(request.getIdTransaction(), environment.getClients(), productTypes);

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
            long expiredTimeThreshold = System.currentTimeMillis() + ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "nextExpirations.threshold.error", 0) * 24 * 3600 * 1000;
            long warningTimeThreshold = System.currentTimeMillis() + ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "nextExpirations.threshold.warning", 7) * 24 * 3600 * 1000;
            long showTimeThreshold = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "nextExpirations.threshold.show", 90);
            long showTimeMiliseconds = System.currentTimeMillis() + showTimeThreshold * 24 * 3600 * 1000;
            Expiration expiration;

            CreditCard card;
            Loan loan;
            long expirationDate;
            for (Product p : backendProducts) {
                for (Product envP : environmentProducts) {
                    if (p.getExtraInfo().equals(envP.getExtraInfo())) {
                        switch (p.getProductType()) {
                            case Constants.PRODUCT_TC_KEY:
                                card = (CreditCard) p;
                                expiration = new Expiration(card);
                                expirationDate = expiration.getDate().getTime();
                                if (expirationDate < showTimeMiliseconds) {
                                    expiration.setDetail(pLabeler.calculateShortLabel(card));
                                    if (expirationDate < expiredTimeThreshold) {
                                        expiration.setStatus(STATUS_EXPIRED);
                                    } else if (expirationDate < warningTimeThreshold) {
                                        expiration.setStatus(STATUS_WARNING);
                                    }
                                    result.add(expiration);
                                }
                                break;
                            case Constants.PRODUCT_PA_KEY:
                            case Constants.PRODUCT_PI_KEY:
                                loan = (Loan) p;
                                expiration = new Expiration(loan);
                                expirationDate = expiration.getDate().getTime();
                                if (expirationDate < showTimeMiliseconds) {
                                    expiration.setDetail(pLabeler.calculateShortLabel(loan));
                                    if (expiration.getDate().getTime() < expiredTimeThreshold) {
                                        expiration.setStatus(STATUS_EXPIRED);
                                    } else if (expiration.getDate().getTime() < warningTimeThreshold) {
                                        expiration.setStatus(STATUS_WARNING);
                                    }
                                    result.add(expiration);
                                }
                                break;
                            default:
                                String logString = String.format("Unexpected p.getProductType() value %s", p.getProductType());
                                log.error(logString);
                                break;
                        }
                    }
                }
            }

            Collections.sort(result);
            int maxExpirations = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "widgets.notifications.maxExpirations");
            if (result.size() > maxExpirations) {
                result = result.subList(0, maxExpirations);
            }
            return result;

        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    private List<Map<String, Object>> getPendingTransactions(Request request) throws ActivityException {
        try {
            List<Map<String, Object>> transactionFinalList = new ArrayList<>();
            Map<String, Object> transactionMap;
            List<String> availableStates = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "widget.transactions.availableStates");
            List<Transaction> transactionList = TransactionHandlerFactory.getInstance()
                    .listTransactions(request.getIdUser(), request.getIdEnvironment(), null, null, availableStates.toArray(new String[availableStates.size()]),
                            "creation_date_time DESC", 1, ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "widgets.notifications.maxPending"))
                    .getElementList();

            Collections.sort(transactionList, new Comparator<Transaction>() {

                @Override
                public int compare(Transaction t1, Transaction t2) {
                    return Integer.compare(mapStatusToInt(t1.getIdTransactionStatus()), mapStatusToInt(t2.getIdTransactionStatus()));
                }

                private int mapStatusToInt(String status) {
                    switch (status) {
                        case Transaction.STATUS_PENDING:
                            return 1;
                        case Transaction.STATUS_DRAFT:
                            return 2;
                        case Transaction.STATUS_PROCESSING:
                            return 3;
                        case Transaction.STATUS_SCHEDULED:
                            return 4;
                        default:
                            return 999;
                    }
                }
            });

            Map<String, Form> formMap = new HashMap<>();
            Form form;
            String formId;
            List<FormField> formFields;
            List<Amount> amounts;
            Map<String, Double> transactionAmounts;
            for (Transaction transaction : transactionList) {
                transactionAmounts = new HashMap<>();
                formId = transaction.getIdForm();
                if (formId != null) {
                    form = formMap.get(formId);
                    if (form == null) {
                        form = FormsHandler.getInstance().readFormFull(transaction.getIdForm(), Transaction.STATUS_DRAFT.equals(transaction.getIdTransactionStatus()) ? null : transaction.getFormVersion());
                        formMap.put(form.getIdForm(), form);
                    }
                    formFields = form.getFieldList();
                    if (Form.FORM_TYPE_PROCESS.equals(form.getType())) {
                        transaction.setActivityName(FormMessagesHandler.getInstance().readFormMessage(form.getIdForm(), form.getVersion(), "formName", request.getLang()));
                    } else {
                        I18n i18n = I18nFactory.getHandler();
                        transaction.setActivityName(i18n.getMessageForUser("activities." + transaction.getIdActivity(), request.getIdUser()));
                    }
                    for (FormField formField : formFields) {
                        if (formField instanceof FormFieldWithCap) {
                            amounts = ((FormFieldWithCap) formField).getAmounts(transaction.getData());
                            calculateAmounts(transactionAmounts, amounts);
                        }
                    }
                }
                transactionMap = new HashMap<>();
                transactionMap.put("transaction", transaction);
                transactionMap.put("transactionAmounts", transactionAmounts);
                transactionFinalList.add(transactionMap);
            }
            return transactionFinalList;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    private void calculateAmounts(Map<String, Double> transactionAmounts, List<Amount> amounts) {
        for (Amount amount : amounts) {
            if (amount != null) {
                if (transactionAmounts.containsKey(amount.getCurrency())) {
                    transactionAmounts.put(amount.getCurrency(), transactionAmounts.get(amount.getCurrency()) + amount.getQuantity());
                } else {
                    transactionAmounts.put(amount.getCurrency(), amount.getQuantity());
                }
            }
        }
    }
}
