/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Delete a signatures scheme
 */
@DocumentedActivity("Delete a signatures scheme")
public class DeleteSignaturesActivity extends Activity {

    public static final String ID = "administration.signatures.delete.send";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "Signature scheme identifier")
        String SIGNATURE_ID = "signatureId";
        
        // Needed to register entry data into transaction.
        // These data will be used later to show the transaction's ticket
        @DocumentedParam(type = List.class, description = "Selected frequencies for transactions with amount")
        String CAP_FREQUENCIES = "capFrequencies";
        @DocumentedParam(type = List.class, description = "Selected functional groups")
        String FUNCTIONAL_GROUPS = "functionalGroups";
        @DocumentedParam(type = Integer.class, description = "Maximum amount for transactions with amount")
        String MAX_AMOUNT = "maxAmount";
        @DocumentedParam(type = Map.class, description = "Number of signers for each signature level")
        String SIGNATURE_LEVELS_COUNTS = "signatureLevelsCounts";
        @DocumentedParam(type = String.class, description = "Signature type")
        String SIGNATURE_TYPE = "signatureType";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            int idSignature = request.getParam(InParams.SIGNATURE_ID, int.class);

            LimitsHandlerFactory.getHandler().deleteSignature(request.getIdEnvironment(), idSignature);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            Integer idSignature = request.getParam(InParams.SIGNATURE_ID, Integer.class);

            List<Integer> environmentSignatures = LimitsHandlerFactory.getHandler().listEnvironmentSignatureIds(request.getIdEnvironment());

            if (idSignature == null || idSignature <= 0) {
                result.put("NO_FIELD", "administration.signatures.mustSelectSignature");
            } else if (!environmentSignatures.contains(idSignature)) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
