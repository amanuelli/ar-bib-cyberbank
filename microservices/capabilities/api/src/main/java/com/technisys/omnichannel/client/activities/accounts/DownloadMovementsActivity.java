/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.accounts;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.StatementResult;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.IOUtils;
import com.technisys.omnichannel.core.utils.NumberUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DocumentedActivity("Download account's movements")
public class DownloadMovementsActivity extends Activity {

    public static final String ID = "accounts.downloadMovements";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "Account's ID")
        String ID_ACCOUNT = "idAccount";
        @DocumentedParam(type = String.class, description = "Date from")
        String DATE_FROM = "dateFrom";
        @DocumentedParam(type = String.class, description = "Date to")
        String DATE_TO = "dateTo";
        @DocumentedParam(type = Double.class, description = "Min amount")
        String MIN_AMOUNT = "minAmount";
        @DocumentedParam(type = Double.class, description = "Max aount")
        String MAX_AMOUNT = "maxAmount";
        @DocumentedParam(type = String.class, description = "Reference")
        String REFERENCE = "reference";
        @DocumentedParam(type = String[].class, description = "Channel's list")
        String CHANNELS = "channels";
        @DocumentedParam(type = String.class, description = "Check")
        String CHECK = "check";
        @DocumentedParam(type = Integer.class, description = "Max count")
        String MAX_COUNT = "maxCount";
        @DocumentedParam(type = String.class, description = "Export file's format")
        String FORMAT = "format";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Exported file's name")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = String.class, description = "Encoded base64")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response;

        try {
            String idAccount = request.getParam(ListStatementsActivity.InParams.ID_ACCOUNT, String.class);
            Product product = Administration.getInstance().readProduct(idAccount, request.getIdEnvironment());
            Account account = CoreAccountConnectorOrchestrator.readAccount(request.getIdTransaction(), product);
            Integer maxCount = request.getParam(InParams.MAX_COUNT, Integer.class);
            Integer movementsPerPage = maxCount != null ? maxCount : ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "accounts.export.maxStatements", 10);
            StatementResult statementResult = ListStatementsActivity.searchStatements(request, product, movementsPerPage,  0);
            List<Statement> statements = statementResult.getStatementList();
            response = exportXLS(statements, account, request);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException | BackendConnectorException | ParserConfigurationException | SAXException | InvalidFormatException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    /*
    private Response exportPDF(List<Statement> statements, Account account, Request request) throws IOException, ParserConfigurationException, SAXException {

        StringBuilder template = new StringBuilder();
        String line, html;
        ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

        String accountLabel = pLabeler.calculateLabel(account);

        try (InputStream is = this.getClass().getResourceAsStream("/templates/account_pdf_template.html")) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            while ((line = reader.readLine()) != null) {
                template.append(line).append("\n");
            }
        }

        html = formatHtml(template.toString(), request.getLang(), statements, accountLabel);

        Document doc = ExportUtils.getDocumentBuilder().parse(new InputSource(new StringReader(html)));

        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(doc, "");
        renderer.layout();

        byte[] content;

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            renderer.createPDF(os);
            InputStream pdfSource = new ByteArrayInputStream(os.toByteArray());
            content = Base64.encodeBase64(IOUtils.toByteArray(pdfSource));
        }

        String filename = accountLabel + "-" + I18nFactory.getHandler().getMessage("accounts.statements", request.getLang()) + ".pdf";

        Response response = new Response(request);
        response.putItem(OutParams.FILE_NAME, filename);
        response.putItem(OutParams.CONTENT, new String(content));

        return response;
    }

     */

    private Response exportXLS(List<Statement> statements, Account account, Request request) throws IOException, ParserConfigurationException, SAXException, InvalidFormatException {

        ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
        String accountLabel = pLabeler.calculateLabel(account);

        List<List<String>> header = new ArrayList<>();
        List<List<String>> lines = new ArrayList<>();
        List<List<String>> footer = new ArrayList<>();

        I18n i18n = I18nFactory.getHandler();

        // Header
        List<String> headLine = new ArrayList<>();
        headLine.add(i18n.getMessage("accounts.details.export.title", request.getLang()));
        header.add(headLine);

        headLine = new ArrayList<>();
        headLine.add(I18nFactory.getHandler().getMessage("accounts.details.export.idAccount", request.getLang()));
        headLine.add(accountLabel);
        header.add(headLine);

        // Statements
        List<String> statementLines = new ArrayList<>();

        statementLines.add(i18n.getMessage("accounts.details.statements.accountingDate", request.getLang()));
        statementLines.add(i18n.getMessage("accounts.details.statements.concept", request.getLang()));
        statementLines.add(i18n.getMessage("accounts.details.statements.reference", request.getLang()));
        statementLines.add(i18n.getMessage("accounts.details.statements.debit", request.getLang()));
        statementLines.add(i18n.getMessage("accounts.details.statements.credit", request.getLang()));
        statementLines.add(i18n.getMessage("accounts.details.statements.balance", request.getLang()));

        lines.add(statementLines);

        for (Statement statement : statements) {
            statementLines = new ArrayList<>();

            statementLines.add(DateUtils.formatShortDate(statement.getDate(), request.getLang()));
            statementLines.add(statement.getConcept());
            statementLines.add(statement.getReference());
            statementLines.add(String.valueOf(statement.getDebit()!=null ? NumberUtils.formatDecimal(statement.getDebit(), request.getLang()) : ""));
            statementLines.add(String.valueOf(statement.getCredit()!= null ? NumberUtils.formatDecimal(statement.getCredit(), request.getLang()) : ""));
            statementLines.add(String.valueOf(NumberUtils.formatDecimal(statement.getBalance(), request.getLang())));

            lines.add(statementLines);
        }

        // Footer
        List<String> footerLine = new ArrayList<>();
        footerLine.add(i18n.getMessage("accounts.details.statements.count", request.getLang()));
        footerLine.add(String.valueOf(statements.size()));
        footer.add(footerLine);

        //Realizamos la transformacion
        XLSTransformer transformer = new XLSTransformer();

        Map<String, Object> beans = new HashMap<>();

        // Unescape all content
        List<List<String>> unescapedHeader = new ArrayList<>();
        for (List<String> sublist : header) {
            List<String> unescapedSublist = new ArrayList<>();

            sublist.forEach((record) -> {
                unescapedSublist.add(StringEscapeUtils.unescapeHtml4(record));
            });

            unescapedHeader.add(unescapedSublist);
        }

        List<List<String>> unescapedLines = new ArrayList<>();
        for (List<String> sublist : lines) {
            List<String> unescapedSublist = new ArrayList<>();

            sublist.forEach((record) -> {
                unescapedSublist.add(StringEscapeUtils.unescapeHtml4(record));
            });

            unescapedLines.add(unescapedSublist);
        }

        List<List<String>> unescapedFooter = new ArrayList<>();
        for (List<String> sublist : footer) {
            List<String> unescapedSublist = new ArrayList<>();

            sublist.forEach((record) -> {
                unescapedSublist.add(StringEscapeUtils.unescapeHtml4(record));
            });

            unescapedFooter.add(unescapedSublist);
        }

        beans.put("header", unescapedHeader);
        beans.put("lines", unescapedLines);
        beans.put("footer", unescapedFooter);

        byte[] data;

        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             Workbook work = WorkbookFactory.create(DownloadMovementsActivity.class.getResourceAsStream("/templates/account_template.xls"))) {
            transformer.transformWorkbook(work, beans);
            work.write(out);
            data = out.toByteArray();
            out.flush();
        }
        InputStream pdfSource = new ByteArrayInputStream(data);

        byte[] content = Base64.encodeBase64(IOUtils.toByteArray(pdfSource));

        String filename = accountLabel + "-" + i18n.getMessage("accounts.statements", request.getLang()) + ".xls";

        Response response = new Response(request);
        response.putItem(OutParams.FILE_NAME, filename);
        response.putItem(OutParams.CONTENT, content);

        return response;
    }

    private String formatHtml(String html, String lang, List<Statement> statements, String accountLabel) {
        I18n i18n = I18nFactory.getHandler();
        html = html.replace("%%LIST_STATEMENTS_TITLE%%", i18n.getMessage("accounts.details.export.title", lang));
        html = html.replace("%%ACCOUNT_NAME%%", accountLabel);
        html = html.replace("%%ACCOUNTING_DATE%%", i18n.getMessage("accounts.details.statements.accountingDate", lang));
        html = html.replace("%%CONCEPT%%", i18n.getMessage("accounts.details.statements.concept", lang));
        html = html.replace("%%REFERENCE%%", i18n.getMessage("accounts.details.statements.reference", lang));
        html = html.replace("%%DEBIT%%", i18n.getMessage("accounts.details.statements.debit", lang));
        html = html.replace("%%CREDIT%%", i18n.getMessage("accounts.details.statements.credit", lang));
        html = html.replace("%%BALANCE%%", i18n.getMessage("accounts.details.statements.balance", lang));
        StringBuilder strBuilder = new StringBuilder("");

        for (Statement statement : statements) {
            strBuilder.append("<tr>");
            strBuilder.append("<td>").append(statement.getDate()).append("</td>");
            strBuilder.append("<td>").append(statement.getConcept()).append("</td>");
            strBuilder.append("<td>").append(statement.getReference()).append("</td>");
            strBuilder.append("<td>").append(statement.getDebit()!=null ? NumberUtils.formatDecimal(statement.getDebit(), lang) : "").append("</td>");
            strBuilder.append("<td>").append(statement.getCredit()!=null ? NumberUtils.formatDecimal(statement.getCredit(), lang) : "").append("</td>");
            strBuilder.append("<td>").append(NumberUtils.formatDecimal(statement.getBalance(), lang)).append("</td>");
            strBuilder.append("</tr>");
        }

        html = html.replace("%%STATEMENTS%%", strBuilder.toString());
        html = html.replace("%%STATEMENTS_COUNT_TEXT%%", i18n.getMessage("accounts.details.statements.count", lang));
        html = html.replace("%%STATEMENTS_COUNT%%", String.valueOf(statements.size()));
        html = html.replace("%%OMNICHANNEL_DISCLAIMER%%", i18n.getMessage("accounts.details.statements.export.disclaimer", lang));

        return html;
    }
}
