/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.transfers.thirdparties;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreTransferConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.TransferInternalDetail;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@DocumentedActivity("Transfers to thirdparties, Preview activity")
public class TransfersThirdPartiesPreviewActivity extends Activity {

    public static final String ID = "transfers.thirdParties.preview";

    public interface InParams {

        @DocumentedParam(type = Amount.class, description = "Debit account")
        String ID_DEBIT_ACCOUNT = "debitAccount";

        @DocumentedParam(type = Amount.class, description = "Credit Account")
        String CREDIT_ACCOUNT = "creditAccount";

        @DocumentedParam(type = Amount.class, description = "Amount")
        String AMOUNT = "amount";

        @DocumentedParam(type = Date.class, description = "Value Date")
        String VALUE_DATE = "valueDate";

        @DocumentedParam(type = String.class, description = "Reference")
        String REFERENCE = "reference";

        @DocumentedParam(type = String.class, description = "Nofification body")
        String NOTIFICATION_BODY = "notificationBody";
    }

    public interface OutParams {

        @DocumentedParam(type = Amount.class, description = "Debit amount")
        String DEBIT_AMOUNT = "debitAmount";

        @DocumentedParam(type = String.class, description = "Debit Account Alias")
        String DEBIT_ACCOUNT_ALIAS = "debitAccountAlias";

        @DocumentedParam(type = String.class, description = "Nofification body")
        String CREDIT_ACCOUNT = "creditAccount";

        @DocumentedParam(type = Amount.class, description = "Nofification body")
        String CREDIT_AMOUNT = "creditAmount";

        @DocumentedParam(type = Double.class, description = "Rate")
        String RATE = "rate";

        @DocumentedParam(type = String.class, description = "Reference")
        String REFERENCE = "reference";

        @DocumentedParam(type = Amount.class, description = "Amount")
        String AMOUNT = "amount";

        @DocumentedParam(type = String.class, description = "Nofification Mails")
        String NOTIFICATION_MAILS = "notificationMails";
        @DocumentedParam(type = String.class, description = "Nofification body")
        String NOTIFICATION_BODY = "notificationBody";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            
            Map map = request.getParam(InParams.ID_DEBIT_ACCOUNT, Map.class);
            String debitAccountParam = (String)map.get("value");
    
            String creditAccountNumber = request.getParam(InParams.CREDIT_ACCOUNT, String.class);

            Product debitProduct = Administration.getInstance().readProduct(debitAccountParam, request.getIdEnvironment());
            Account debitAccount = new Account(debitProduct);
            Amount amount = request.getParam(InParams.AMOUNT, Amount.class);
            String reference = request.getParam(InParams.REFERENCE, String.class);
            
            Account creditAccount = new Account();
            creditAccount.setNumber(creditAccountNumber);            
            TransferInternalDetail detail = CoreTransferConnectorOrchestrator.thirdPartyValidate(request.getIdTransaction(),
                    environment.getIdEnvironment(), environment.getProductGroupId(), debitAccount, creditAccountNumber,
                    amount, reference, reference);

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
            
            if (detail.getReturnCode() == 0) {
                response.putItem(OutParams.DEBIT_AMOUNT, detail.getDebitAmount());
                response.putItem(OutParams.DEBIT_ACCOUNT_ALIAS, pLabeler.calculateShortLabel(debitProduct));
                response.putItem(OutParams.CREDIT_AMOUNT, detail.getCreditAmount());
                response.putItem(OutParams.CREDIT_ACCOUNT, creditAccountNumber);
                response.putItem(OutParams.RATE, detail.getRate());
                
                List<String> notificationMails = request.getParam(TransfersThirdPartiesSendActivity.InParams.NOTIFICATION_MAILS, List.class);
                String notificationBody = request.getParam(TransfersThirdPartiesSendActivity.InParams.NOTIFICATION_BODY, String.class);
                
                response.putItem(OutParams.AMOUNT, amount);
                response.putItem(OutParams.REFERENCE, reference);
                response.putItem(OutParams.NOTIFICATION_MAILS, notificationMails);
                response.putItem(OutParams.NOTIFICATION_BODY, notificationBody);

                response.setReturnCode(ReturnCodes.OK);
            } else {
                LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

                /*
                 * 1 - cuenta debito invalida
                 * 2 - cuenta credito invalida
                 * 3 - cuenta debito sin saldo suficiente
                 * 4 - cuenta de debito y credito coinciden
                 */
                switch (detail.getReturnCode()) {
                    case 100:
                        errors.put(InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("transfer.thirdParties.debitAccount.invalid", request.getLang()));
                        break;
                    case 101:
                        errors.put(InParams.CREDIT_ACCOUNT, I18nFactory.getHandler().getMessage("transfer.thirdParties.creditAccount.invalid", request.getLang()));
                        break;
                    case 102:
                        errors.put(InParams.CREDIT_ACCOUNT, I18nFactory.getHandler().getMessage("transfers.thirdParties.sameCreditDebitAccount", request.getLang()));
                        break;
                    case 103:
                        errors.put(InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("transfer.thirdParties.debitAccount.insufficientBalance", request.getLang()));
                        break;
                    default:
                        String key = "transfer.thirdParties." + detail.getReturnCode();
                        String message = I18nFactory.getHandler().getMessage(key, request.getLang());
                        if (!StringUtils.isBlank(message) && !message.equals(key)) {
                            errors.put("NO_FIELD", message);
                        } else {
                            response.setReturnCode(ReturnCodes.BACKEND_SERVICE_ERROR);
                        }
                }
                response.setReturnCode(ReturnCodes.VALIDATION_ERROR);
                response.setData(errors);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (BackendConnectorException bcE) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, bcE);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new TransfersThirdPartiesSendActivity().validate(request);
        return result;
    }
}
