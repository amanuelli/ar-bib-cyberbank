/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.client.ReturnCodes;
import static com.technisys.omnichannel.client.activities.files.ListFileActivity.InParams.ID_FILE_LIST;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.files.FilesHandler;
import java.io.IOException;
import java.util.List;

/**
 *
 */
public class ListFileActivity extends Activity {

    public static final String ID = "files.list";

    public interface InParams {

        String ID_FILE_LIST = "idFileList";
    }

    public interface OutParams {

        String FILE_LIST = "fileList";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            int[] idFileList = request.getParam(ID_FILE_LIST, int[].class);

            List<File> files = FilesHandler.getInstance().list(request.getIdUser(), false, idFileList);

            response.putItem(OutParams.FILE_LIST, files);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
