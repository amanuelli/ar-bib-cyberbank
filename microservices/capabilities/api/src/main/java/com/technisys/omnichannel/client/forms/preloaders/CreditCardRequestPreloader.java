/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.forms.preloaders;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.forms.FormPreloader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author dimoda
 */
public class CreditCardRequestPreloader implements FormPreloader {

    private static final Logger log = LoggerFactory.getLogger(CreditCardRequestPreloader.class);
    
    public interface OutParams {
        String EMAIL = "email";
        String PHONE = "phone";
    }
        
    @Override
    public Map<String, Object> execute(Form form, Request request) {
        Map<String, Object> formData = new HashMap();
        try {
            if (!User.USER_ANONYMOUS.equals (request.getIdUser())){
                EnvironmentUser environmentUser = Administration.getInstance().readEnvironmentUserInfo(request.getIdUser(), request.getIdEnvironment());

                if (environmentUser.getMobileNumber() != null){
                    formData.put(CreditCardChangeConditionPreloader.OutParams.PHONE, environmentUser.getMobileNumber());
                }
                if (StringUtils.isNotBlank(environmentUser.getEmail())){
                    formData.put(CreditCardChangeConditionPreloader.OutParams.EMAIL, environmentUser.getEmail());
                }
            }
        } catch (IOException ex) {
            log.error(ex.getMessage(), ex);
        }
        return formData;
    }
}