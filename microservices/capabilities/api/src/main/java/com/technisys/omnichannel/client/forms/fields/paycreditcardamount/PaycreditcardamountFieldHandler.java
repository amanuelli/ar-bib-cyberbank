/*
 *  Copyright 2015 Manentia Software.
 *
 *  This software component is the intellectual property of 5IT S.R.L. (Manentia Software).
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  http://www.manentiasoftware.com
 */
package com.technisys.omnichannel.client.forms.fields.paycreditcardamount;

import com.technisys.omnichannel.client.domain.fields.PaycreditcardamountField;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.domain.fields.ProductselectorField;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.forms.fields.FieldHandler;
import com.technisys.omnichannel.core.forms.fields.FrontendOption;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.DBUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.MessageFormat;
import java.util.*;

import static com.technisys.omnichannel.core.domain.FormFieldWithCap.MAX_TRANSACTION_AMOUNT;

/**
 * Implementacion del handler de campos especifica para campos de tipo
 * 'paycreditcardamount' (monto para pago de tarjeta de credito)
 *
 * @author ?
 */
public class PaycreditcardamountFieldHandler extends FieldHandler {

    private static final Logger log = LoggerFactory.getLogger(PaycreditcardamountFieldHandler.class);

    /**
     * Obtiene la descripcion corta de la data de una transaccion. Este metodo
     * sera utilizado en backoffice para el despliegue de la informacion breve
     * para una transaccion de creacion/modificacion de un campo de formulario
     *
     * @param map Mapa con los parametros ingresados
     * @param lang Lenguaje para labels
     * @return String en formato html con la descripcion breve de la transaccion
     */
    @Override
    public String getShortDescription(Map<String, String> map, String lang) {
        //no agrego nada especifico de este tipo de campo
        return getBasicShortDescription(map, lang);
    }

    /**
     * Obtiene la descripcion completa de la data de una transaccion. Este
     * metodo sera utilizado en backoffice para el despliegue de la informacion
     * de una transaccion de creacion/modificacion de un campo de formulario
     *
     * @param map Mapa con los parametros ingresados
     * @param lang Lenguaje para labels
     * @return String en formato html con la descripcion completa de la
     * transaccion
     */
    @Override
    public String getFullDescription(Map<String, String> map, String lang) {
        StringBuilder strBuilder = new StringBuilder(getBasicFullDescription(map, lang));

        //info especifica de este tipo de campo
        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.displayType", lang)).append(":</dt>").append("<dd>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.displayType." + map.get("displayType"), lang)).append("</dd>");

        String idCreditCardSelector = map.get("idCreditCardSelector");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.idCreditCardSelector", lang)).append(":</dt>");
        strBuilder.append("<dd>").append(idCreditCardSelector).append("</dd>");

        //monedas ...
        String currencyListStr = map.get("currencyList");
        if (StringUtils.isNotBlank(currencyListStr)) {
            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.currencyList", lang)).append(":</dt>");
            strBuilder.append("<dd>");
            String[] currencies = currencyListStr.split("\\|");
            for (int i = 0; i < currencies.length; i++) {
                if (i > 0) {
                    strBuilder.append(", ");
                }
                strBuilder.append(i18n.getMessage("core.currency.label." + currencies[i], lang));
            }
            strBuilder.append("</dd>");
        }

        String idProductSelectorCurrencyCheck = map.get("idProductSelectorCurrencyCheck");

        if (StringUtils.isNotBlank(idProductSelectorCurrencyCheck)) {
            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.amount.idProductSelectorCurrencyCheck", lang)).append(":</dt>");
            strBuilder.append("<dd>").append(idProductSelectorCurrencyCheck).append("</dd>");
        }

        String controlLimits = map.get("controlLimits");

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.controlLimits", lang)).append(":</dt>");
        if (StringUtils.isNotBlank(controlLimits) && Boolean.parseBoolean(controlLimits)) {
            strBuilder.append("<dd>").append(i18n.getMessage("backoffice.yes", lang)).append("</dd>");
        } else {
            strBuilder.append("<dd>").append(i18n.getMessage("backoffice.no", lang)).append("</dd>");
        }

        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getDefaultBoolean(Configuration.LIMITS,  "core.use_cap_environment_product", false);

        if (capsByProductEnabled) {
            String controlLimitOverProduct = map.get("controlLimitOverProduct");
            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.controlLimitOverProduct", lang)).append(":</dt>");
            strBuilder.append("<dd>").append(controlLimitOverProduct).append("</dd>");
        }

        //textos internacionalizados
        List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);
        for (String key : languages) {
            if (StringUtils.isNotBlank(map.get("placeholder" + key))) {
                strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.placeholder", lang)).append(" (").append(key).append("):</dt>").append("<dd>").append(map.get("placeholder" + key)).append("</dd>");
            }
        }

        for (String key : languages) {
            if (StringUtils.isNotBlank(map.get("requiredError" + key))) {
                strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.requiredError", lang)).append(" (").append(key).append("):</dt>").append("<dd>").append(map.get("requiredError" + key)).append("</dd>");
            }
        }

        for (String key : languages) {
            if (StringUtils.isNotBlank(map.get("invalidError" + key))) {
                strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.invalidError", lang)).append(" (").append(key).append("):</dt>").append("<dd>").append(map.get("invalidError" + key)).append("</dd>");
            }
        }

        return strBuilder.toString();
    }

    /**
     * Obtiene la descripcion a utilizar en el PDF de detalle del formulario.
     *
     * @param field Campo de formulario
     * @param lang Lenguaje para labels
     * @return String en formato html con la descripcion completa del campo
     */
    @Override
    public String getPDFDescription(FormField field, String lang) {
        StringBuilder strBuilder = new StringBuilder();

        PaycreditcardamountField pccAmountField = (PaycreditcardamountField) field;

        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.displayType", lang)).append(":</em> ").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.displayType." + pccAmountField.getDisplayType(), lang)).append("<br/>");

        String idCreditCardSelector = pccAmountField.getIdCreditCardSelector();

        strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.idCreditCardSelector", lang)).append(":</em> ").append(idCreditCardSelector).append("<br />");

        if (pccAmountField.getCurrencyList() != null) {
            List<String> currencyList = pccAmountField.getCurrencyList();
            strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.currencyList", lang)).append(":</em> ");
            for (int i = 0; i < currencyList.size(); i++) {
                if (i > 0) {
                    strBuilder.append(", ");
                }
                strBuilder.append(i18n.getMessage("core.currency.label." + currencyList.get(i), lang));
            }
            strBuilder.append("<br/>");
        }

        strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.controlLimits", lang)).append(":</em> ");
        if (pccAmountField.isValidateCap()) {
            strBuilder.append(i18n.getMessage("backoffice.yes", lang)).append("<br/>");
        } else {
            strBuilder.append(i18n.getMessage("backoffice.no", lang)).append("<br/>");
        }

        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getDefaultBoolean(Configuration.LIMITS,  "core.use_cap_environment_product", false);

        if (capsByProductEnabled) {
            strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.controlLimitOverProduct", lang)).append(":</em> ");
            strBuilder.append(pccAmountField.getIdFieldProductToValidateCap()).append("<br/>");
        }

        //textos internacionalizados
        List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);

        //placeholder
        if (pccAmountField.getPlaceholderMap() != null) {
            for (String key : languages) {
                if (StringUtils.isNotBlank(pccAmountField.getPlaceholderMap().get(key))) {
                    strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.placeholder", lang)).append(" (").append(key).append(")").append(":</em> ").append(pccAmountField.getPlaceholderMap().get(key)).append("<br/>");
                }
            }
        }

        //requiredError
        if (pccAmountField.getRequiredErrorMap() != null) {
            for (String key : languages) {
                if (StringUtils.isNotBlank(pccAmountField.getRequiredErrorMap().get(key))) {
                    strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.requiredError", lang)).append(" (").append(key).append(")").append(":</em> ").append(pccAmountField.getRequiredErrorMap().get(key)).append("<br/>");
                }
            }
        }

        //invalidError
        if (pccAmountField.getInvalidErrorMap() != null) {
            for (String key : languages) {
                if (StringUtils.isNotBlank(pccAmountField.getInvalidErrorMap().get(key))) {
                    strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.paycreditcardamount.invalidError", lang)).append(" (").append(key).append(")").append(":</em> ").append(pccAmountField.getInvalidErrorMap().get(key)).append("<br/>");
                }
            }
        }

        return strBuilder.toString();
    }

    /**
     * Valida los datos ingresados por el usuario y retorna los errores
     * encontrados
     *
     * @param map Mapa con los datos ingresados
     * @return Mapa con los identificadores de campos/keys de mensaje
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, String> validate(Map<String, String> map) throws IOException {
        Map<String, String> result = new HashMap<>();

        //tipo de despliegue
        if (StringUtils.isBlank(map.get("displayType"))) {
            result.put("displayType", "backoffice.forms.fields.paycreditcardamount.displayTypeEmpty");
        }

        if (StringUtils.isBlank(map.get("idCreditCardSelector"))) {
            result.put("idCreditCardSelector", "backoffice.forms.fields.paycreditcardamount.idCreditCardSelectorRequired");
        }

        if (StringUtils.isBlank(map.get("currencyList"))) {
            result.put("currencyList", "backoffice.forms.fields.paycreditcardamount.currencyListEmpty");
        }

        return result;
    }

    /**
     * Valida los datos de un campo de tipo monto y retorna los errores
     * encontrados
     *
     * @param field Campo a validar
     * @return Mapa con los identificadores de campos/keys de mensaje
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, String> validate(FormField field) throws IOException {
        Map<String, String> result = new HashMap<>();

        PaycreditcardamountField pccAmountField = (PaycreditcardamountField) field;

        //tipo de despliegue
        if (StringUtils.isEmpty(pccAmountField.getDisplayType())) {
            result.put("displayType", "backoffice.forms.fields.paycreditcardamount.displayTypeEmpty");
        }

        return result;
    }

    /**
     * Valida el valor ingresado para un campo y retorna los errores encontrados
     *
     * @param request Solicitud enviada
     * @param idField Identificador de campo a validar
     * @param fields Mapa de campos &lt;id, campo&gt; del formulario
     * @return Mapa con los identificadores de campos/keys de mensaje
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, String> validateValue(Request request, String idField, Map<String, FormField> fields) throws IOException {
        Map<String, String> result = new HashMap<>();

        PaycreditcardamountField pccAmountField = (PaycreditcardamountField) fields.get(idField);

        Amount value = RequestParamsUtils.getValue(idField, Amount.class);

        //chequeo que el valor ingresado sea valido en funcion de los parametros del campo
        String message = pccAmountField.getInvalidErrorMap().get(request.getLang());

        if (value != null) {

            //valido que la moneda sea una moneda valida del sistema
            String currency = value.getCurrency();

            List<String> currencies = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.currencies", String.class);
            if (!currencies.contains(currency) || !pccAmountField.getCurrencyList().contains(currency)) {
                if (StringUtils.isNotBlank(message)) {
                    result.put("@" + pccAmountField.getIdField(), message);
                } else {
                    result.put(pccAmountField.getIdField(), "fields.defaultForm.defaultField.invalidError");
                }
            }

            //valido que el monto no supere el maximo permitido
            if (value.getQuantity() > MAX_TRANSACTION_AMOUNT) {
                if (StringUtils.isNotBlank(message)) {
                    result.put("@" + pccAmountField.getIdField(), message);
                } else {
                    result.put(pccAmountField.getIdField(), "fields.defaultForm.defaultField.invalidError");
                }
            }
        }

        return result;
    }

    /**
     * Indica si el campo tiene un valor definido o esta vacio
     *
     * @param field Campo de formulario
     * @param fieldValue Valor del campo
     * @return Booleano indicando si el campo tiene valor definido
     */
    @Override
    public boolean isHasValue(FormField field, Object fieldValue) {
        if (fieldValue != null) {
            Amount value = RequestParamsUtils.getValue(fieldValue, Amount.class);
            if (value != null) {
                return (value.getQuantity() != null && value.getQuantity() > 0);
            }
        }
        return false;
    }

    /**
     * Indica si el campo cumple la condicion dada ( &lt;fieldValue&gt; &lt;operator&gt;
     * &lt;comparingValue&gt;) Para este campo se hace una comparacion lexicografica
     *
     * @param field Campo de formulario
     * @param fieldValue Valor del campo
     * @param operator Operador (&lt;, &lt;=, ==, &gt;, &gt;=)
     * @param comparingValue Valor a comparar
     * @return Booleano indicando si el valor del campo cumple la condicion
     */
    @Override
    public boolean isValueCondition(FormField field, Object fieldValue, String operator, String comparingValue) {
        Amount value = RequestParamsUtils.getValue(fieldValue, Amount.class);
        String logMessage;

        Double comparingDouble = Double.parseDouble(comparingValue);

        if (value != null && value.getQuantity() != null) {
            switch (operator) {
                case "==":
                    return value.getQuantity().equals(comparingDouble);
                case "<":
                    return value.getQuantity().compareTo(comparingDouble) < 0;
                case "<=":
                    return value.getQuantity().compareTo(comparingDouble) <= 0;
                case ">":
                    return value.getQuantity().compareTo(comparingDouble) > 0;
                case ">=":
                    return value.getQuantity().compareTo(comparingDouble) >= 0;
                case "!=":
                    return !value.getQuantity().equals(comparingDouble);
                default:
                    logMessage= MessageFormat.format("Unexpected operator value {0}", operator);
                    log.error(logMessage);
                    break;
            }
        }

        return false;
    }

    /**
     * Persiste los datos de un campo
     *
     * @param session Sesion transaccional de base de datos
     * @param map Mapa con los datos ingresados
     * @throws java.io.IOException Error de comunicacion
     */
    @Override
    public void createFormField(SqlSession session, Map<String, String> map) throws IOException {
        PaycreditcardamountFieldDataAccess afda = PaycreditcardamountFieldDataAccess.getInstance();
        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        //paso el mapa a bean
        PaycreditcardamountField field = toField(map);
        field.setIdFieldProductToValidateCap("");

        //creo el campo monto
        afda.createField(session, field);

        //creo placeholder
        if (field.getPlaceholderMap() != null) {
            for (String key : field.getPlaceholderMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "placeholder", field.getPlaceholderMap().get(key));
            }
        }

        //creo mensaje de error requerido
        if (field.getRequiredErrorMap() != null) {
            for (String key : field.getRequiredErrorMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "requiredError", field.getRequiredErrorMap().get(key));
            }
        }

        //creo mensaje de error invalido
        if (field.getInvalidErrorMap() != null) {
            for (String key : field.getInvalidErrorMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "invalidError", field.getInvalidErrorMap().get(key));
            }
        }
    }

    /**
     * Persiste los datos de un campo.
     *
     * @param session Sesion transaccional de base de datos
     * @param field Cammpo a persistir
     * @throws IOException Error de comunicacion
     */
    @Override
    public void createFormField(SqlSession session, FormField field) throws IOException {
        PaycreditcardamountField pccAmountField = (PaycreditcardamountField) field;

        PaycreditcardamountFieldDataAccess afda = PaycreditcardamountFieldDataAccess.getInstance();
        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        //creo el campo selector de producto
        afda.createField(session, pccAmountField);

        //creo placeholder
        for (String key : pccAmountField.getPlaceholderMap().keySet()) {
            fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "placeholder", pccAmountField.getPlaceholderMap().get(key));
        }

        //creo mensaje de error requerido
        for (String key : pccAmountField.getRequiredErrorMap().keySet()) {
            fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "requiredError", pccAmountField.getRequiredErrorMap().get(key));
        }

        //creo mensaje de error invalido
        for (String key : pccAmountField.getInvalidErrorMap().keySet()) {
            fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "invalidError", pccAmountField.getInvalidErrorMap().get(key));
        }
    }

    /**
     * Actualiza los datos de un campo
     *
     * @param session Sesion transaccional de base de datos
     * @param map Mapa con los nuevos datos ingresados
     * @throws IOException Error de comunicacion
     */
    @Override
    public void updateFormField(SqlSession session, Map<String, String> map) throws IOException {
        PaycreditcardamountField field = toField(map);

        //borro el campo y lo creo de nuevo
        //esto tiene que ser una eleccion de la implementacion particular,
        //en algunos casos se podra hacer esto y en otros habra que hacer una actualizacion
        //de los datos (dependiendo de la estructuracion de cada implementacion en base
        //y de las dependencias que se definan para el tipo de campo
        //para este tipo de campo no hay problema de hacer un delete y create
        PaycreditcardamountFieldDataAccess afda = PaycreditcardamountFieldDataAccess.getInstance();
        afda.deleteField(session, field.getIdForm(), map.get("oldId"));
        afda.createField(session, field);

        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        //modifico placeholder
        fmh.deleteFieldMessage(session, map.get("oldId"), field.getIdForm(), "placeholder");
        if (field.getPlaceholderMap() != null) {
            for (String key : field.getPlaceholderMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "placeholder", field.getPlaceholderMap().get(key));
            }
        }

        //modifico mensaje de error requerido
        fmh.deleteFieldMessage(session, map.get("oldId"), map.get("idForm"), "requiredError");
        if (field.getRequiredErrorMap() != null) {
            for (String key : field.getRequiredErrorMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "requiredError", field.getRequiredErrorMap().get(key));
            }
        }

        //modifico mensaje de error invalido
        fmh.deleteFieldMessage(session, map.get("oldId"), map.get("idForm"), "invalidError");
        if (field.getInvalidErrorMap() != null) {
            for (String key : field.getInvalidErrorMap().keySet()) {
                fmh.addFieldMessage(session, field.getIdField(), field.getIdForm(), key, "invalidError", field.getInvalidErrorMap().get(key));
            }
        }
    }

    /**
     * Lee los datos de un campo
     *
     * @param idForm Identificador de formulario
     * @param formVersion Versión de formulario
     * @param idField Identificador de campo
     * @param withMessages indica si cargar mensajes, placeholders, etc
     * @return Datos especificos del campo
     * @throws IOException Error de comunicacion
     */
    @Override
    public FormField readFormField(String idForm, Integer formVersion, String idField, boolean withMessages) throws IOException {
        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        PaycreditcardamountField field;
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {
            field = PaycreditcardamountFieldDataAccess.getInstance().readField(session, idForm, formVersion, idField);
            if(withMessages){
                //listo los placeholders
                field.setPlaceholderMap(fmh.listFieldMessages(session, idField, idForm, formVersion, "placeholder"));

                //listo errores requerido
                field.setRequiredErrorMap(fmh.listFieldMessages(session, idField, idForm, formVersion, "requiredError"));

                //listo errores invalido
                field.setInvalidErrorMap(fmh.listFieldMessages(session, idField, idForm, formVersion, "invalidError"));
            }
        }

        return field;
    }

    /**
     * Borra los datos de un campo
     *
     * @param session Sesion transaccional de base de datos
     * @param idForm Identificador de formulario
     * @param idField Identificador del campo
     * @throws IOException Error de comunicacion
     */
    @Override
    public void deleteFormField(SqlSession session, String idForm, int formVersion, String idField) throws IOException {
        PaycreditcardamountFieldDataAccess.getInstance().deleteField(session, idForm, idField);

        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        //borro placeholder
        fmh.deleteFieldMessage(session, idField, idForm, "placeholder");

        //borro error requerido
        fmh.deleteFieldMessage(session, idField, idForm, "requiredError");

        //borro error invalido
        fmh.deleteFieldMessage(session, idField, idForm, "invalidError");
    }

    /**
     * Dado un Request y el campo especifico carga, de ser necesario, la
     * información de despliegue del mismo. Por ejemplo para el caso de un
     * selector de productos, este método debe retornar la lista de las opciones
     * de dicho selector (list de identificador de productos, su label, etc).
     *
     * @param field Campo del formulario
     * @param request Request del usuario para obtener el usuario, ambiente, etc
     * @param transaction Transaccion (opcional) que se esta queriendo desplegar
     * junto con el formulario,
     * @return Retorna un Mapa con la información necesaria para armar el
     * despliegue del campo. En caso de no requerilo retorna null.
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, Serializable> loadFormFieldData(FormField field, Request request, Transaction transaction) throws IOException, SchedulerException {
        //En base al parametro transaction se puede saber si es para modo vista o edicion. 
        Map<String, Serializable> data = new HashMap<>();

        // Cargo de acuerdo al idioma los valores para mostrar los importes: separador de miles, decimales y la precisión
        char decimalSeparator = '.';
        char thousandsSeparator = ',';
        int precision = 2;

        try {
            DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.forLanguageTag(request.getLang()));
            decimalSeparator = symbols.getDecimalSeparator();
            thousandsSeparator = symbols.getGroupingSeparator();
        } catch (Exception e) {
            log.warn("Error obteniendo separadores", e);
        }
        try {
            String mask = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.decimalFormat");
            DecimalFormat format = new DecimalFormat(mask);
            precision = format.getMaximumFractionDigits();
        } catch (IOException e) {
            log.warn("Error obteniendo precisión", e);
        }
        data.put("decimalSeparator", decimalSeparator);
        data.put("thousandsSeparator", thousandsSeparator);
        data.put("precision", precision);

        List<FrontendOption> options = new ArrayList<>();

        PaycreditcardamountField pccAmountField = (PaycreditcardamountField) field;

        // Si la transaccion no es nula y no se trata de un borrador, esta mostrando el ticket y no es necesario cargar
        // todos los datos del combo. Solamente enviamos los datos necesarios.
        if (transaction != null && !Transaction.STATUS_DRAFT.equals(transaction.getIdTransactionStatus())) {
            Amount amount = RequestParamsUtils.getValue(transaction.getData().get(field.getIdField()), Amount.class);
            if (amount != null) {
                options.add(new FrontendOption(amount.getCurrency(), I18nFactory.getHandler().getMessage("currency.label." + amount.getCurrency(), request.getLang())));
            }
        } else {
            List<String> currencies = pccAmountField.getCurrencyList();
            currencies.forEach((currency) -> {
                //la key utilizada es la de frontend
                options.add(new FrontendOption(currency, I18nFactory.getHandler().getMessage("currency.label." + currency, request.getLang())));
            });

            //textos internacionalizados
            List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);

            //required
            if (pccAmountField.getRequiredErrorMap() != null) {
                for (String lang : languages) {
                    if (StringUtils.isBlank(pccAmountField.getRequiredErrorMap().get(lang))) {
                        pccAmountField.getRequiredErrorMap().put(lang, I18nFactory.getHandler().getMessage("fields.defaultForm.defaultField.requiredError", lang));
                    }
                }
            }

            //invalid
            if (pccAmountField.getInvalidErrorMap() != null) {
                for (String lang : languages) {
                    if (StringUtils.isBlank(pccAmountField.getInvalidErrorMap().get(lang))) {
                        pccAmountField.getInvalidErrorMap().put(lang, I18nFactory.getHandler().getMessage("fields.defaultForm.defaultField.invalidError", lang));
                    }
                }
            }
        }

        data.put("options", (Serializable) options);

        return data;
    }

    /**
     * Obtiene la informacion requerida para el despliegue de configuracion en
     * backoffice
     *
     * @param idForm Identificador de formulario
     * @param idField Identificador de campo del formulario
     * @param lang Lenguaje
     * @return Mapa con la informacion requerida para configuracion (listados,
     * parametros, etc)
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, Serializable> getFieldDataForConfiguration(String idForm, String idField, String lang) throws IOException {
        Map<String, Serializable> data = new HashMap<>();

        //monedas
        List<String> currencies = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.currencies", String.class);
        data.put("currencyList", (Serializable) currencies);

        //listo los campos
        List<FormField> allFields = FormsHandler.getInstance().listFormFields(idForm, null);

        List<FormField> fields = new ArrayList<>();
        //filtro unicamente los campos 
        for (FormField field : allFields) {
            if (field instanceof ProductselectorField
                && (StringUtils.isBlank(idField) || !idField.equals(field.getIdField()))) {
                    fields.add(field);
            }
        }
        data.put("productSelectorFieldList", (Serializable) fields);

        //esta habilitado caps por producto
        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getBoolean(Configuration.LIMITS, "core.use_cap_environment_product");
        data.put("capsByProductEnabled", capsByProductEnabled);

        fields = new ArrayList<>();
        //filtro unicamente los campos selector de tarjeta de credito
        for (FormField field : allFields) {
            if (field instanceof ProductselectorField
                && (field.getSubType().equals("creditcardselector"))
                    && (StringUtils.isBlank(idField) || !idField.equals(field.getIdField()))) {
                        fields.add(field);
            }
        }
        data.put("creditCardSelectorFieldList", (Serializable) fields);

        return data;
    }

    /**
     * Dado un mapa, creo la bean especifica Este metodo es especifico de la
     * implementacion del campo y por tanto no obligatorio de la interfase
     *
     * @param map Mapa con los datos del campo
     * @return Instancia especifica del campo de tipo amount
     */
    private PaycreditcardamountField toField(Map<String, String> map) {
        PaycreditcardamountField ff = new PaycreditcardamountField();
        ff.setIdField(map.get("id"));
        ff.setIdForm(map.get("idForm"));
        ff.setType(map.get("type"));

        String idCreditCardSelector = map.get("idCreditCardSelector");
        ff.setIdCreditCardSelector(idCreditCardSelector);

        //monedas ...
        String currencyListStr = map.get("currencyList");
        if (StringUtils.isNotBlank(currencyListStr)) {
            String[] currencies = currencyListStr.split("\\|");
            for (String currency : currencies) {
                ff.addCurrency(currency);
            }
        }

        ff.setDisplayType(map.get("displayType"));

        String controlLimits = map.get("controlLimits");
        if (StringUtils.isNotBlank(controlLimits)) {
            ff.setValidateCap(Boolean.parseBoolean(controlLimits));
        }

        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getDefaultBoolean(Configuration.LIMITS,  "core.use_cap_environment_product", false);
        String controlLimitOverProduct = map.get("controlLimitOverProduct");
        if (capsByProductEnabled && StringUtils.isNotBlank(controlLimitOverProduct)) {
            ff.setIdFieldProductToValidateCap(controlLimitOverProduct);
        }

        List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);

        for (String lang : languages) {
            if (StringUtils.isNotEmpty(map.get("placeholder" + lang))) {
                ff.addPlaceholder(lang, map.get("placeholder" + lang));
            }

            if (StringUtils.isNotEmpty(map.get("requiredError" + lang))) {
                ff.addRequiredError(lang, map.get("requiredError" + lang));
            }

            if (StringUtils.isNotEmpty(map.get("invalidError" + lang))) {
                ff.addInvalidError(lang, map.get("invalidError" + lang));
            }
        }

        return ff;
    }
}
