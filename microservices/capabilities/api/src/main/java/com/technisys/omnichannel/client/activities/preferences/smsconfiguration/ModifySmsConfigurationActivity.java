/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.smsconfiguration;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.ChannelProduct;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
public class ModifySmsConfigurationActivity extends Activity {

    public static final String ID = "preferences.smsconfiguration.modify";
    
    public interface InParams {
        String PERMISSION = "permission";
        String PRODUCTS = "product";
        String ALIAS = "alias";
    }

    public interface OutParams {

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String permission = request.getParam(InParams.PERMISSION, String.class);
            String[] products = request.getParam(InParams.PRODUCTS, String[].class);
            String[] alias = request.getParam(InParams.ALIAS, String[].class);
            
            List<ChannelProduct> channelProducts = new ArrayList<>();
            
            ChannelProduct channelProduct;
            if(products != null && alias != null){
                for(int i=0; i<products.length; i++){
                    channelProduct = new ChannelProduct();
                    channelProduct.setIdProduct(products[i]);
                    channelProduct.setChannelAlias(alias[i]);

                    channelProducts.add(channelProduct);
                }
            }
            
            Administration.getInstance().updateChannelConfiguration(request.getIdEnvironment(), request.getIdUser(), "sms", permission, channelProducts);
            
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        
        //valido permission (tipo de operativa)
        String permission = request.getParam(InParams.PERMISSION, String.class);
        
        if(StringUtils.isBlank(permission)){
            result.put(InParams.PERMISSION, "preferences.smsConfiguration.permissionEmpty");
        }else if(!ChannelProduct.PERMISSION_NONE.equals(permission) && !ChannelProduct.PERMISSION_VIEW.equals(permission) && !ChannelProduct.PERMISSION_TRANSACTIONS.equals(permission)){
            result.put(InParams.PERMISSION, "preferences.smsConfiguration.permissionInvalid");
        }

        //valido productos
        try{
            List<String> productTypes = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "preferences.sms.configuration.productTypes");
            List<String> permissions = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "preferences.sms.configuration.productPermissions");

            List<Product> productsAvailable = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), permissions, productTypes);
            
            String[] products = request.getParam(InParams.PRODUCTS, String[].class);
            
            if(products != null){
                //valido que cada producto este en la lista de productos disponibles
                Product product;
                int i = 0;
                for(String idProduct : products){
                    product = new Product();
                    product.setIdProduct(idProduct);

                    if(!productsAvailable.contains(product)){
                        result.put(InParams.PRODUCTS + "_" + products[i], "preferences.smsConfiguration.productInvalid");
                    }
                    i++;
                }
            }

            //valido aliases
            String[] aliases = request.getParam(InParams.ALIAS, String[].class);

            Map<String, String> alreadyChecked = new HashMap<>();
            if(aliases != null){
                Pattern aliasPattern = Pattern.compile("[0-9a-zA-Z]*");

                Matcher matcher;
                int i = 0;
                for(String alias : aliases){
                    if(StringUtils.isBlank(alias)){
                        result.put(InParams.ALIAS + "_" + products[i], "preferences.smsConfiguration.aliasEmpty");
                    }else{
                        matcher = aliasPattern.matcher(alias);

                        if(matcher.matches()){
                            if(alias.length() > 8){
                                result.put(InParams.ALIAS + "_" + products[i], "preferences.smsConfiguration.aliasLengthInvalid");
                            }else{
                                //chequeo que ya no este repetida
                                if(alreadyChecked.get(alias) != null){
                                    result.put(InParams.ALIAS + "_" + products[i], "preferences.smsConfiguration.aliasAlreadyExists");
                                }else{
                                    alreadyChecked.put(alias, alias);
                                }
                            }
                        }else{
                            result.put(InParams.ALIAS + "_" + products[i], "preferences.smsConfiguration.aliasInvalid");
                        }
                    }
                    i++;
                }
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        
        return result;
    }
}