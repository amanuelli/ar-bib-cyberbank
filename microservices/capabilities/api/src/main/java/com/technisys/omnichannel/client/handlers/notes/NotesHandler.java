/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.handlers.notes;

import com.technisys.omnichannel.core.utils.DBUtils;
import org.apache.ibatis.session.SqlSession;
import java.io.IOException;
import org.apache.commons.lang.StringUtils;

/**
 *
 * @author fpena
 */
public class NotesHandler {

    private NotesHandler(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    private static final int NOTA_MAX_LENGTH = 4000;

    public static String readNote(int idStatement, String idProduct) throws IOException {
        try (SqlSession sqlSession = DBUtils.getInstance().openReadSession()) {
            return NotesDataAccess.getInstance().readNote(sqlSession, idStatement, idProduct);
        }
    }

    public static void deleteNote(int idStatement, String idroduct) throws IOException {
        try (SqlSession sqlSession = DBUtils.getInstance().openWriteSession()) {
            NotesDataAccess.getInstance().deleteNote(sqlSession, idStatement, idroduct);
            sqlSession.commit();
        }
    }

    public static void modifyNote(int idStatement, String idProduct, String note) throws IOException {
        try (SqlSession sqlSession = DBUtils.getInstance().openWriteSession()) {
            if (StringUtils.isNotEmpty(note) && note.length() > NOTA_MAX_LENGTH) {
                note = note.substring(0, NOTA_MAX_LENGTH);
            }

            NotesDataAccess nDataAccess = NotesDataAccess.getInstance();

            String previousNote = nDataAccess.readNote(sqlSession, idStatement, idProduct);

            if (previousNote == null){ 
                nDataAccess.createNote(sqlSession, idStatement, idProduct, note);
            } else {
                nDataAccess.modifyNote(sqlSession, idStatement, idProduct, note);
            }
            sqlSession.commit();
        }
    }

}
