/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnector;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.clients.ClientHandler;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.rubicon.core.creditcards.WsDynamicField;
import com.technisys.omnichannel.rubicon.core.creditcards.WsReadCreditCardResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Date;
import java.util.Map;

/**
 *
 * @author grosso
 */
public class CreditCard extends ClientProduct {

    private String account;
    private String alias;
    private String holder;
    private double balance;
    private Date expirationDate;
    private Date closingDate;
    private double availableBalance;
    private String availableBalanceCurrency;
    private double creditLimit;
    private String creditLimitCurrency;
    private double minimumPayment;
    private Date lastPaymentDate;
    private double lastPaymentAmmount;
    private String franchise;

    private static final Logger log = LoggerFactory.getLogger(CreditCard.class);

    public CreditCard() {
    }

    public CreditCard(Product product) {
        super(product);
    }

    public CreditCard(Map<String, String> fieldsMap) {
        super(fieldsMap);

        try {
            client = ClientHandler.readClient(fieldsMap.get(ProductUtils.FIELD_CLIENT_ID));
        } catch (Exception ex){
            log.warn("Error creating reading client: " + ex.getMessage(), ex);
        }

        this.availableBalance = RubiconCoreConnector.parseDouble(fieldsMap.get("availableBalance"));
        this.balance = RubiconCoreConnector.parseDouble(fieldsMap.get("balance2"));
        this.minimumPayment = RubiconCoreConnector.parseDouble(fieldsMap.get("minimunPayment2"));
        this.expirationDate = RubiconCoreConnector.parseDate(fieldsMap.get("expirationDate"));
        this.closingDate = RubiconCoreConnector.parseDate(fieldsMap.get("closingDate"));
        this.creditLimit = RubiconCoreConnector.parseDouble(fieldsMap.get("creditLimit"));
        this.availableBalanceCurrency = CURRENCY_USD;
        this.creditLimitCurrency = CURRENCY_USD;
    }

    public CreditCard(WsReadCreditCardResponse response) {
        super(RubiconCoreConnector.convertDynamicListToMap(response.getFields().getField()));

        // Obtengo el cliente del mapa de campos
        if (response.getFields().getField() != null) {
            for (WsDynamicField field : response.getFields().getField()) {
                if (ProductUtils.FIELD_CLIENT_ID.equals(field.getKey())) {
                    try {
                        client = ClientHandler.readClient(field.getValue());
                    } catch (Exception ex){
                        log.warn("Error creating reading client: " + ex.getMessage(), ex);
                    }
                    break;
                }
            }
        }

        this.account = response.getAccount();
        this.balance = response.getBalance2();
        this.expirationDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(response.getExpirationDate());
        this.availableBalance = response.getAvailableBalance();
        this.creditLimit = response.getCreditLimit();
        this.closingDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(response.getClosingDate());
        this.lastPaymentDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(response.getLastPaymentDate());
        this.lastPaymentAmmount = response.getLastPaymentAmmount2();
        this.minimumPayment = response.getMinimunPayment2();
        this.consolidatedAmount = response.getConsolidatedAmount();
        this.holder = response.getHolder();
        this.availableBalanceCurrency = CURRENCY_USD;
        this.creditLimitCurrency = CURRENCY_USD;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getClosingDate() {
        return closingDate;
    }

    public void setClosingDate(Date closingDate) {
        this.closingDate = closingDate;
    }

    public double getAvailableBalance() {
        return availableBalance;
    }

    public void setAvailableBalance(double availableBalance) {
        this.availableBalance = availableBalance;
    }

    public double getCreditLimit() {
        return creditLimit;
    }

    public void setCreditLimit(double creditLimit) {
        this.creditLimit = creditLimit;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getAvailableBalanceCurrency() {
        return availableBalanceCurrency;
    }

    public void setAvailableBalanceCurrency(String availableBalanceCurrency) {
        this.availableBalanceCurrency = availableBalanceCurrency;
    }

    public String getCreditLimitCurrency() {
        return creditLimitCurrency;
    }

    public void setCreditLimitCurrency(String creditLimitCurrency) {
        this.creditLimitCurrency = creditLimitCurrency;
    }

    public Date getLastPaymentDate() {
        return lastPaymentDate;
    }

    public void setLastPaymentDate(Date lastPaymentDate) {
        this.lastPaymentDate = lastPaymentDate;
    }

    public String getHolder() {
        return holder;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getMinimumPayment() {
        return minimumPayment;
    }

    public void setMinimumPayment(double minimunPayment) {
        this.minimumPayment = minimunPayment;
    }

    public double getLastPaymentAmmount() {
        return lastPaymentAmmount;
    }

    public void setLastPaymentAmmount(double lastPaymentAmmount) {
        this.lastPaymentAmmount = lastPaymentAmmount;
    }

    public String getFranchise() { return franchise; }

    public void setFranchise(String franchise) { this.franchise = franchise; }
}
