/*
 * Copyright 2019 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.model.CityResponse;
import com.technisys.omnichannel.client.domain.Coordinate;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.postloginchecks.geoip.PostLoginCheckGeoIPDataAccess;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Map;

/**
 * @author Marcelo Bruno
 */
public class GeolocalizationUtils {

    private static final Logger log = LoggerFactory.getLogger(GeolocalizationUtils.class);

    private GeolocalizationUtils() {}

    public static Coordinate getLocalizationByIp(String clientIp) {
        Coordinate coordinate = new Coordinate();
        InetAddress inetAddress;
        try {
            inetAddress = InetAddress.getByName(clientIp);
            // local net
            if (inetAddress.isSiteLocalAddress() || inetAddress.isLoopbackAddress()) {
                // ip lab uruguay
                clientIp = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "cyberbank.localAddressTesting");
            }
            CityResponse cityResponse = PostLoginCheckGeoIPDataAccess.getInstance().getCity(InetAddress.getByName(clientIp));
            if(cityResponse != null) {
                coordinate.setLatitude(cityResponse.getLocation().getLatitude());
                coordinate.setLongitude(cityResponse.getLocation().getLongitude());
            }
        } catch (AddressNotFoundException | UnknownHostException e) {
            log.warn("The IP (" + clientIp + ") is not in the DB", e);
        } catch (Exception e) {
            log.warn("Exception getting Geolocalization", e);
        }
        return coordinate;
    }

    public static Coordinate getCoordinatesFromParam(Map localizationMap) {
        Coordinate coordinate = new Coordinate();
        try {
            coordinate.setLatitude((double) localizationMap.get("latitude"));
            coordinate.setLongitude((double) localizationMap.get("longitude"));
        } catch (Exception e) {
            log.warn("Could not get coordinates from params");
        }
        return coordinate;
    }
}
