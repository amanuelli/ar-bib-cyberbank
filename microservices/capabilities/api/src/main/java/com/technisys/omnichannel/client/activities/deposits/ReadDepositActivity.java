/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.deposits;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.Deposit;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import java.io.IOException;

/**
 *
 */
public class ReadDepositActivity extends Activity {

    public static final String ID = "deposits.read";

    public interface InParams {

        String ID_DEPOSIT = "idDeposit";
    }

    public interface OutParams {

        String DEPOSIT = "deposit";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            String idDeposit = (String) request.getParam(InParams.ID_DEPOSIT, String.class);

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            Product product = Administration.getInstance().readProduct(idDeposit, request.getIdEnvironment());
            Deposit deposit = RubiconCoreConnectorC.readDepositDetails(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo());

            deposit.setPaperless(product.getPaperless());
            deposit.setProductAlias(product.getProductAlias());
            deposit.setShortLabel(pLabeler.calculateShortLabel(deposit));
            deposit.setLabel(pLabeler.calculateLabel(deposit));

            response.putItem(OutParams.DEPOSIT, deposit);
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
}
