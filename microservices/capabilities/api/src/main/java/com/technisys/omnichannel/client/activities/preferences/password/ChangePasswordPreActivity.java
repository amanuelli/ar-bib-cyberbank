/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.password;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.SecuritySealUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 * Return user id seal and image seal.
 */
@DocumentedActivity("Change Password Pre")
public class ChangePasswordPreActivity extends Activity {

    public static final String ID = "preferences.changepassword.pre";

    public interface InParams {
    }

    public interface OutParams {
        @DocumentedParam(type = Integer.class, description = "User id seal")
        String SECURITY_SEAL = "_securitySeal";
        @DocumentedParam(type = String.class, description = "Base64 seal image")
        String SECURITY_SEAL_IMAGE = "_securitySealImage";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());

            response.putItem(OutParams.SECURITY_SEAL, user.getIdSeal());
            response.putItem(OutParams.SECURITY_SEAL_IMAGE, SecuritySealUtils.getSeal(user.getIdSeal()));

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
}
