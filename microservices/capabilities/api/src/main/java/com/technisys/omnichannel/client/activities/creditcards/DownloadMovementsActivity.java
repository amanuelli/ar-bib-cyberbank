/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.creditcards;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.client.domain.StatementCreditCard;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.IOUtils;
import com.technisys.omnichannel.core.utils.NumberUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author dlondono
 */
@DocumentedActivity("Download credit card detail")
public class DownloadMovementsActivity extends Activity {

    public static final String ID = "creditCards.downloadMovements";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "credit card id to download")
        String ID_CREDITCARD = "idCreditCard";
        @DocumentedParam(type = String.class, description = "File format can be pdf and xls")
        String FORMAT = "format";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "File name to save")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = Object.class, description = "Download file with credit cards lines, it is Object because depends of file format, in xls is byte and pdf is String")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response;
        try {
            String idCreditCard = request.getParam(InParams.ID_CREDITCARD, String.class);
            Product product = Administration.getInstance().readProduct(idCreditCard, request.getIdEnvironment());
            CreditCard creditCard = RubiconCoreConnectorTC.readCreditCardDetails(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo());

            List<StatementCreditCard> statements = com.technisys.omnichannel.client.activities.creditcards.ListStatementsActivity.searchStatements(request, product, 0);

            response = new Response(request);
            ExportResource export;
            String lang = request.getLang();
            ProductLabeler pLabeler = CoreUtils.getProductLabeler(lang);
            String creditCardLabel = pLabeler.calculateLabel(creditCard);
            export = exportXLS(statements, creditCardLabel, lang);
            response.putItem(OutParams.CONTENT, export.getContent());
            response.putItem(OutParams.FILE_NAME, export.getFilename());
            response.setReturnCode(ReturnCodes.OK);
        } catch (Exception ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    private List<List<String>> getHeader(String lang, String creditCardLabel) {
        List<List<String>> header = new ArrayList<>();
        List<String> headLine = new ArrayList<>();
        headLine.add(I18nFactory.getHandler().getMessage("creditCards.details.export.title", lang));
        header.add(headLine);
        headLine = new ArrayList<>();
        headLine.add(I18nFactory.getHandler().getMessage("creditCards.details.export.idCreditCard", lang));
        headLine.add(creditCardLabel);
        header.add(headLine);
        return header;
    }

    private List<List<String>> buildLines(List<StatementCreditCard> statements, String lang) {
        List<List<String>> lines = new ArrayList<>();
        List<String> statementLines = new ArrayList<>();
        statementLines.add(I18nFactory.getHandler().getMessage("creditCards.details.statements.accountingDate", lang));
        statementLines.add(I18nFactory.getHandler().getMessage("creditCards.details.statements.Concept", lang));
        statementLines.add(I18nFactory.getHandler().getMessage("creditCards.details.statements.totalAmount", lang));
        lines.add(statementLines);
        for (StatementCreditCard statement : statements) {
            statementLines = new ArrayList<>();
            statementLines.add(DateUtils.formatShortDate(statement.getDate(), lang));
            statementLines.add(statement.getConcept());
            statementLines.add(NumberUtils.formatDecimal(statement.getAmount(), lang));

            lines.add(statementLines);
        }
        return lines;
    }

    private List<List<String>> getFooter(List<StatementCreditCard> statements, String lang) {
        List<List<String>> footer = new ArrayList<>();
        List<String> footerLine = new ArrayList<>();
        footerLine.add(I18nFactory.getHandler().getMessage("creditCards.details.statements.count", lang));
        footerLine.add(String.valueOf(statements.size()));
        footer.add(footerLine);
        return footer;
    }

    private List<List<String>> unescapeHtml(List<List<String>> section) {
        List<List<String>> unescape = new ArrayList<>();
        for (List<String> sublist : section) {
            List<String> unescapedSublist = new ArrayList<>();
            sublist.forEach(record -> unescapedSublist.add(StringEscapeUtils.unescapeHtml4(record)));
            unescape.add(unescapedSublist);
        }
        return unescape;
    }

    private byte[] buildXLS(List<List<String>> unescapedHeader, List<List<String>> unescapedLines, List<List<String>> unescapedFooter) throws IOException, InvalidFormatException {
        XLSTransformer transformer = new XLSTransformer();
        Map<String, Object> beans = new HashMap<>();

        beans.put("header", unescapedHeader);
        beans.put("lines", unescapedLines);
        beans.put("footer", unescapedFooter);

        Workbook work = WorkbookFactory.create(DownloadMovementsActivity.class.getResourceAsStream("/templates/creditcard_template.xls"));
        transformer.transformWorkbook(work, beans);
        byte[] data;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream()) {
            work.write(out);
            data = out.toByteArray();
            out.flush();
        }
        InputStream pdfSource = new ByteArrayInputStream(data);
        return Base64.encodeBase64(IOUtils.toByteArray(pdfSource));
    }

    private ExportResource exportXLS(List<StatementCreditCard> statements, String creditCardLabel, String lang) throws IOException, InvalidFormatException {
        List<List<String>> header = getHeader(lang, creditCardLabel);
        List<List<String>> lines = buildLines(statements, lang);
        List<List<String>> footer = getFooter(statements, lang);

        List<List<String>> unescapedHeader = unescapeHtml(header);
        List<List<String>> unescapedLines = unescapeHtml(lines);
        List<List<String>> unescapedFooter = unescapeHtml(footer);

        byte[] xls = buildXLS(unescapedHeader, unescapedLines, unescapedFooter);

        String filename = I18nFactory.getHandler().getMessage("creditCard.statements", lang) + ".xls";

        return new ExportResource(filename, xls);
    }

    private String compileHtml(String html, String lang, List<StatementCreditCard> statements, String creditCardLabel) {
        I18n i18n = I18nFactory.getHandler();
        html = html.replace("%%LIST_STATEMENTS_TITLE%%", i18n.getMessage("creditCards.details.export.title", lang));
        html = html.replace("%%ACCOUNTING_DATE%%", i18n.getMessage("creditCards.details.statements.accountingDate", lang));
        html = html.replace("%%CONCEPT%%", i18n.getMessage("creditCards.details.statements.concept", lang));
        html = html.replace("%%AMOUNT%%", i18n.getMessage("creditCards.details.statements.totalAmount", lang));
        html = html.replace("%%ACCOUNT_NAME%%", creditCardLabel);

        StringBuilder strBuilder = new StringBuilder("");

        for (StatementCreditCard statement : statements) {
            strBuilder.append("<tr>");
            strBuilder.append("<td>").append(DateUtils.formatShortDate(statement.getDate(), lang)).append("</td>");
            strBuilder.append("<td>").append(statement.getConcept()).append("</td>");
            strBuilder.append("<td>").append(NumberUtils.formatDecimal(statement.getAmount(), lang)).append("</td>");
            strBuilder.append("</tr>");
        }

        html = html.replace("%%STATEMENTS%%", strBuilder.toString());
        html = html.replace("%%STATEMENTS_COUNT_TEXT%%", i18n.getMessage("creditCards.details.statements.count", lang));
        html = html.replace("%%STATEMENTS_COUNT%%", String.valueOf(statements.size()));
        html = html.replace("%%OMNICHANNEL_DISCLAIMER%%", i18n.getMessage("creditCards.details.statements.export.disclaimer", lang));

        return html;
    }

    private String getHtmlTemplate() throws IOException {
        InputStream is = this.getClass().getResourceAsStream("/templates/creditcard_pdf_template.html");
        StringBuilder template = new StringBuilder();
        String line;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            while ((line = reader.readLine()) != null) {
                template.append(line).append("\n");
            }
        } finally {
            is.close();
        }

        return template.toString();
    }

    // comment the functions, it's possible to reuse them with another library
/*
    private byte[] generatePdf(String html) throws ParserConfigurationException, SAXException, IOException, DocumentException {
        Document doc = ExportUtils.getDocumentBuilder().parse(new InputSource(new StringReader(html)));
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(doc, "");
        renderer.layout();

        byte[] content;

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            renderer.createPDF(os);
            InputStream pdfSource = new ByteArrayInputStream(os.toByteArray());
            content = Base64.encodeBase64(IOUtils.toByteArray(pdfSource));
        }
        return content;
    }
*/

/*
    private ExportResource exportPDF(List<StatementCreditCard> statements, String creditCardLabel, String lang) throws IOException, ParserConfigurationException, SAXException, DocumentException {
        String template = getHtmlTemplate();
        byte[] pdf = generatePdf(compileHtml(template, lang, statements, creditCardLabel));
        String filename = I18nFactory.getHandler().getMessage("creditCard.statements", lang) + ".pdf";

        return new ExportResource(filename, pdf);
    }
*/


    static class ExportResource {

        String filename;
        byte[] content;

        public ExportResource(String filename, byte[] content) {
            this.filename = filename;
            this.content = content;
        }

        public String getFilename() {
            return filename;
        }

        public byte[] getContent() {
            return content;
        }
    }
}
