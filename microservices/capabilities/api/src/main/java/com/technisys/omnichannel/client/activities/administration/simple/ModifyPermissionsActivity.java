package com.technisys.omnichannel.client.activities.administration.simple;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.common.Permissions;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DocumentedActivity("Modify user permissions (simple scheme)")
public class ModifyPermissionsActivity extends Activity {

    public static final String ID = "administration.simple.modify.permissions.send";
    
    public interface InParams {

        @DocumentedParam(type = String.class, description = "User id to modify permissions")
        String ID_USER = "idUser";
        @DocumentedParam(type = Map.class, description = "Permissions map to set for the user")
        String PERMISSIONS = "permissions";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String idUser = request.getParam(InParams.ID_USER, String.class);
            Map<String, List<String>> newPermissions = request.getParam(InParams.PERMISSIONS, Map.class);
            Group group = AccessManagementHandlerFactory.getHandler()
                    .getGroups(idUser, null, request.getIdEnvironment(), -1, -1, null).getElementList().get(0);
            int groupId = group.getIdGroup();
            List<GroupPermission> defaultUserPermissions = Permissions.listDefault(groupId);
            List<GroupPermission> permissions = Permissions.list(groupId);

            permissions.addAll(defaultUserPermissions);
            Permissions.addIncomingPermissions(groupId, newPermissions, permissions);
            AccessManagementHandlerFactory.getHandler().updateGroup(group, null, permissions);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            Administration admin = Administration.getInstance();
            List<String> envProducts = admin.listEnvironmentProductIds(request.getIdEnvironment());
            String idUser = request.getParam(InParams.ID_USER, String.class);
            List<String> envUsers = Administration.getInstance().listEnvironmentUserIds(request.getIdEnvironment());
            Map<String, List<String>> newPermissions = request.getParam(InParams.PERMISSIONS, Map.class);

            if (envUsers != null && !envUsers.contains(idUser)) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

            // Reject request if schema is not simple
            if (!Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            newPermissions.entrySet().forEach((entry) -> {
                entry.getValue().forEach((target) -> {
                    if (!StringUtils.startsWith(target, Authorization.GENERIC_PRODUCT_TYPE_PREFIX) && !StringUtils.equals(target, Authorization.TARGET_NONE) && !envProducts.contains(target)) {
                        result.put("NO_FIELD", "administration.groups.permissionNotExist");
                    }
                });
            });
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
