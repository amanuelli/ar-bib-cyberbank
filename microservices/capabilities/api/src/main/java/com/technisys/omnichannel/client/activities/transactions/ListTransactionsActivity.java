/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.transactions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.api.resources.BaseResource;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.DateUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 *
 */
@DocumentedActivity("List transaction activity")
public class ListTransactionsActivity extends Activity {
    private static final Logger log = LoggerFactory.getLogger(BaseResource.class);
    public static final String ID = "transactions.list";

    public interface InParams {

        @DocumentedParam(type = Date.class, description = "Date from filter")
        String DATE_FROM = "dateFrom";
        @DocumentedParam(type = Date.class, description = "Date to filter")
        String DATE_TO = "dateTo";
        @DocumentedParam(type = Integer.class, description = "Page number filter")
        String PAGE_NUMBER = "pageNumber";
        @DocumentedParam(type = Double.class, description = "Minimum amount filter")
        String MIN_AMOUNT = "minAmount";
        @DocumentedParam(type = Double.class, description = "Maximum amount filter")
        String MAX_AMOUNT = "maxAmount";
        @DocumentedParam(type = String.class, description = "Channel filter")
        String CHANNEL = "channel";
        @DocumentedParam(type = Boolean.class, description = "List only pending dispatch transactions")
        String PENDING_DISPATCH = "pendingDispatch";
        @DocumentedParam(type = Boolean.class, description = "List only transactions in pending status")
        String ONLY_PENDINGS = "onlyPendings";
        @DocumentedParam(type = String.class, description = "For Economic Groups, filter transactions for just this member")
        String CLIENT_TO_FILTER_BY ="client";
    }

    public interface OutParams {

        @DocumentedParam(type = List.class, description = "Transactions list")
        String TRANSACTIONS = "transactions";
        @DocumentedParam(type = List.class, description = "Totla pages result")
        String TOTAL_PAGES = "totalPages";
        @DocumentedParam(type = List.class, description = "Page number to return")
        String PAGE_NUMBER = "pageNumber";
    }

    @Override
    public Response execute(Request request) throws ActivityException {

        Response response = new Response(request);
        try {
            Environment env = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            PaginatedList<Transaction> transactions = null;
            Date dateFrom = request.getParam(InParams.DATE_FROM, Date.class);
            Date dateTo = request.getParam(InParams.DATE_TO, Date.class);
            Integer pageNumber = request.getParam(InParams.PAGE_NUMBER, Integer.class);

            if (pageNumber == null) {
                pageNumber = 1;
            }
            Double minAmount = request.getParam(InParams.MIN_AMOUNT, Double.class);
            Double maxAmount = request.getParam(InParams.MAX_AMOUNT, Double.class);
            request.getParam(InParams.CHANNEL, String.class);
            String pendingDispatch = request.getParam(InParams.PENDING_DISPATCH, String.class);
            Boolean pendingDispatchB = null;

            if(pendingDispatch!=null && !pendingDispatch.isEmpty()){
                pendingDispatchB = Boolean.getBoolean(pendingDispatch);
            }

            String[] idTransactionStatuses = null;
            Boolean onlyPendings = request.getParam(InParams.ONLY_PENDINGS, Boolean.class);
            if(onlyPendings != null && onlyPendings) {
                idTransactionStatuses = new String[] {Transaction.STATUS_PENDING};
            }

            if (env.getEnvironmentType().equals(Environment.ENVIRONMENT_TYPE_CORPORATE_GROUP)){
                transactions = TransactionHandlerFactory.getInstance().listCorporateGroupTransactions(request.getIdUser(), env, dateFrom, dateTo, pendingDispatchB,
                        idTransactionStatuses, minAmount, maxAmount, null, "creation_date_time DESC", pageNumber,
                        ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "transactions.rowsPerPage", 10), request.getParam(InParams.CLIENT_TO_FILTER_BY, String.class));
            } else {
                transactions = TransactionHandlerFactory.getInstance().listTransactions(request.getIdUser(), env, dateFrom, dateTo, pendingDispatchB, idTransactionStatuses, minAmount, maxAmount, null, "creation_date_time DESC", pageNumber, ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "transactions.rowsPerPage", 10));
            }

            Calendar cancellationValidityDate = Calendar.getInstance();
            cancellationValidityDate.setTimeInMillis(cancellationValidityDate.getTimeInMillis() - ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "requestTransactionCancellation.validityWindow"));

            List<Map<String, Object>> transactionFinalList = new ArrayList<>();
            Map<String, Object> transactionMap;
            Map<String, Form> formMap = new HashMap<>();
            Form form;
            String formId;
            List<FormField> formFields;
            List<Amount> amounts;
            Map<String, Double> transactionAmounts;
            for (Transaction transaction : transactions.getElementList()) {
                transactionAmounts = new HashMap<>();
                formId = transaction.getIdForm();
                if (formId != null) {
                    form = formMap.get(formId);
                    if (form == null) {
                        form = FormsHandler.getInstance().readFormFull(transaction.getIdForm(), Transaction.STATUS_DRAFT.equals(transaction.getIdTransactionStatus()) ? null : transaction.getFormVersion());
                        formMap.put(form.getIdForm(), form);
                    }
                    formFields = form.getFieldList();
                    if(Form.FORM_TYPE_PROCESS.equals(form.getType())){
                        transaction.setActivityName(FormMessagesHandler.getInstance().readFormMessage(form.getIdForm(), form.getVersion(), "formName", request.getLang()));
                    }else{
                        I18n i18n = I18nFactory.getHandler();
                        transaction.setActivityName(i18n.getMessageForUser("activities." + transaction.getIdActivity(), request.getIdUser()));
                    }
                    for (FormField formField : formFields) {
                        if (formField instanceof FormFieldWithCap) {
                            amounts = ((FormFieldWithCap) formField).getAmounts(transaction.getData());
                            calculateAmounts(transactionAmounts, amounts);
                        }
                    }
                }
                transactionMap = new HashMap<>();
                transactionMap.put("transaction", transaction);
                transactionMap.put("transactionAmounts", transactionAmounts);
                transactionMap.put("cancelEnabled",  (transaction.getIdTransactionStatus().equals(Transaction.STATUS_PROCESSING) || transaction.getIdTransactionStatus().equals(Transaction.STATUS_ACCEPTED)) && transaction.getSubmitDateTime().after(cancellationValidityDate.getTime()));
                transactionFinalList.add(transactionMap);
            }

            response.putItem(OutParams.TOTAL_PAGES, transactions.getTotalPages());
            response.putItem(OutParams.PAGE_NUMBER, pageNumber);
            response.putItem(OutParams.TRANSACTIONS, transactionFinalList);
            response.setReturnCode(ReturnCodes.OK);

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap();
        String dateFrom = request.getParam(InParams.DATE_FROM, String.class);
        String dateTo = request.getParam(InParams.DATE_TO, String.class);
        String pageNumber = request.getParam(InParams.PAGE_NUMBER, String.class);
        String pendingDispatch = request.getParam(InParams.PENDING_DISPATCH, String.class);
        String onlyPending = request.getParam(InParams.ONLY_PENDINGS, String.class);

        if(!isValidDate(dateFrom, request.getLang()))
            result.put(InParams.DATE_FROM,"valueDate.invalid");

        if(!isValidDate(dateTo, request.getLang()))
            result.put(InParams.DATE_TO,"valueDate.invalid");

        if (!isValidInteger(pageNumber))
            result.put(InParams.PAGE_NUMBER, "administration.signatures.list.pageNumber.invalid");

        if(!isValidBoolean(pendingDispatch))
            result.put(InParams.PENDING_DISPATCH, "forms.inputFile.massivePayments.invalid.field");

        if(!isValidBoolean(onlyPending))
            result.put(InParams.ONLY_PENDINGS, "forms.inputFile.massivePayments.invalid.field");

        return result;
    }


    private boolean isValidBoolean(String param){
        return !StringUtils.isBlank(param)
                && (param.equalsIgnoreCase("true")
                || param.equalsIgnoreCase("false"));
    }

    private boolean isValidInteger (String value){
        try {
            if(!StringUtils.isBlank(value)) {
                Integer parsed = Integer.parseInt(value);
                if (parsed >= 0)
                    return true;
            }
        } catch (NumberFormatException ex) {
            log.error("Page Number provided is not a number: {}", value);
        }
        return false;
    }

    private boolean isValidDate(String param, String lang ){
        if(!StringUtils.isBlank(param)){
            String dateTo = DateUtils.formatShortDate(param, lang);
            return !dateTo.equals("N/A");
        }
        return false;
    }

    private void calculateAmounts(Map<String, Double> transactionAmounts, List<Amount> amounts) {
        for (Amount amount : amounts) {
            if (amount != null) {
                if (transactionAmounts.containsKey(amount.getCurrency())) {
                    transactionAmounts.put(amount.getCurrency(), transactionAmounts.get(amount.getCurrency()) + amount.getQuantity());
                } else {
                    transactionAmounts.put(amount.getCurrency(), amount.getQuantity());
                }
            }
        }
    }
}
