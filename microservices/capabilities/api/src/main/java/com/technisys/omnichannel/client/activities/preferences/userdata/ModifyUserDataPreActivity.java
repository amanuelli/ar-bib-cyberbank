/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.userdata;


import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 * Returns user data
 */
@DocumentedActivity("Modify User Data Pre")
public class ModifyUserDataPreActivity extends Activity {

    public static final String ID = "preferences.userData.modify.pre";
    
    public interface InParams {
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "User's first name, e.g. &quot;John&quot;, &quot;Brittny&quot;")
        String CLIENT_NAME = "clientName";
        @DocumentedParam(type = String.class, description = "User's last name, e.g. &quot;Snow&quot;, &quot;Beall&quot;")
        String CLIENT_LAST_NAME = "clientLastName";
        @DocumentedParam(type = String.class, description = "User's email address")
        String MAIL = "mail";
        @DocumentedParam(type = String.class, description = "User's mobile phone number")
        String MOBILE_PHONE = "mobilePhone";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
                
            response.putItem(OutParams.CLIENT_NAME, user.getFirstName());
            response.putItem(OutParams.CLIENT_LAST_NAME, user.getLastName());
            response.putItem(OutParams.MAIL, user.getEmail());
            if (user.getMobileNumber() == null){
                response.putItem(OutParams.MOBILE_PHONE, null);
            }else{
                response.putItem(OutParams.MOBILE_PHONE, String.valueOf(user.getMobileNumber()));
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
}
