/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.forms.preloaders;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.forms.FormPreloader;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author sbarbosa
 */
public class RequestTransactionCancellationPreloader implements FormPreloader {

    private static final Logger log = LoggerFactory.getLogger(RequestTransactionCancellationPreloader.class);

    public interface InParams {

        String REFERENCE_TO_CANCEL = "referenceToCancel";
    }

    public interface OutParams {

        String REFERENCE = "reference";
        String DATE = "date";
        String TRANSACTION_TYPE = "transactionType";
    }

    @Override
    public Map<String, Object> execute(Form form, Request request) {
        Map<String, Object> formData = new HashMap();

        String idTransaction = request.getParam(InParams.REFERENCE_TO_CANCEL, String.class);
        if (StringUtils.isNotBlank(idTransaction)) {
            Transaction transaction = null;

            try {
                transaction = TransactionHandlerFactory.getInstance().readUserTransaction(idTransaction, request.getIdUser(), request.getIdEnvironment());
            } catch (IOException ex) {
                String logString = String.format("No se pudo obtener los datos de la transacción #{%s}. Err: %s", idTransaction, ex.getMessage());
                log.warn(logString);
            }

            if (transaction != null) {
                formData.put(OutParams.REFERENCE, transaction.getIdTransaction());

                formData.put(OutParams.TRANSACTION_TYPE, transaction.getActivityName());

                formData.put(OutParams.DATE, DateUtils.formatShortDate(transaction.getSubmitDateTime()));
            }
        }

        return formData;
    }
}
