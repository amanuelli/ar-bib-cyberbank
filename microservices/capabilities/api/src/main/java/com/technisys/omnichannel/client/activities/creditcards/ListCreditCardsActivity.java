/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.creditcards;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 *
 */
@DocumentedActivity("List credit cards")
public class ListCreditCardsActivity extends Activity {

    public static final String ID = "creditCards.list";

    public interface InParams {
    }

    public interface OutParams {

        @DocumentedParam(type = CreditCard[].class, description = "Array of credit cards's objects details")
        String CREDIT_CARDS = "creditCards";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {

            List<CreditCard> creditCards = getCreditCardList(request, Arrays.asList(Constants.PRODUCT_TC_KEY));

            response.putItem(OutParams.CREDIT_CARDS, creditCards);
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    public static List<CreditCard> getCreditCardList(Request request, List<String> productTypes) throws IOException, BackendConnectorException {
        Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

        List<String> idPermissions = new ArrayList<>();
        idPermissions.add(PRODUCT_READ_PERMISSION);

        List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);

        List<CreditCard> creditCards = (List<CreditCard>) RubiconCoreConnectorC.listProducts(request.getIdTransaction(), environment.getClients(), Arrays.asList(new String[]{Constants.PRODUCT_TC_KEY}));

        List<CreditCard> authorizedCreditCards = new ArrayList<>();

        ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

        for (CreditCard cards : creditCards) {
            for (Product environmentProduct : environmentProducts) {
                if (cards.getExtraInfo().equals(environmentProduct.getExtraInfo())) {
                    cards.setProductAlias(environmentProduct.getProductAlias());
                    cards.setShortLabel(pLabeler.calculateShortLabel(cards));
                    cards.setFranchise(pLabeler.calculateFranchise(cards));
                    cards.setLabel(pLabeler.calculateLabel(cards));
                    String number = cards.getNumber();
                    cards.setHolder("•••• •••• •••• " + number.substring(number.length() - 4, number.length()));
                    cards.setIdProduct(environmentProduct.getIdProduct());
                    authorizedCreditCards.add(cards);
                    break;
                }
            }
        }

        return authorizedCreditCards;

    }

}
