/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Feature;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.Signature;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.core.utils.CoreUtils.CHANNEL_ALL;

/**
 * Creates a new signatures scheme (PRE)
 */
@DocumentedActivity("Creates a new signatures scheme (PRE)")
public class CreateSignaturesPreActivity extends Activity {

    public static final String ID = "administration.signatures.create.pre";

    public interface InParams {
    }

    public interface OutParams {
        @DocumentedParam(type = List.class, description = "List of frequencies for transactions with amount")
        String CAP_FREQUENCY_LIST = "capFrequencyList";
        @DocumentedParam(type = String.class, description = "Default frequency for transactions with amount")
        String DEFAULT_FREQUENCY = "defaultFrequency";
        @DocumentedParam(type = List.class, description = "List of enabled channels")
        String ENABLED_CHANNELS = "enabledChannels";
        @DocumentedParam(type = List.class, description = "List of available functional groups")
        String FUNCTIONAL_GROUPS = "functionalGroups";
        @DocumentedParam(type = List.class, description = "List of available products")
        String ENVIRONMENT_PRODUCTS = "environmentProducts";
        @DocumentedParam(type = String.class, description = "Master currency")
        String MASTER_CURRENCY = "masterCurrency";
        @DocumentedParam(type = List.class, description = "List of available signature types")
        String SIGNATURE_TYPE_LIST = "signatureTypeList";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Administration administration = Administration.getInstance();
            String adminScheme = request.getEnvironmentAdminScheme();
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(adminScheme)) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            Environment env = administration.readEnvironment(request.getIdEnvironment());
            
            response.putItem(OutParams.CAP_FREQUENCY_LIST, ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "frontend.channels.enabledFrequencies"));
            response.putItem(OutParams.DEFAULT_FREQUENCY, env.getCapFrequency());
            response.putItem(OutParams.MASTER_CURRENCY, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency"));
            response.putItem(OutParams.SIGNATURE_TYPE_LIST, Arrays.asList(new String[]{Signature.TYPE_AMOUNT, Signature.TYPE_NO_AMOUNT}));
            
            String environmentType = request.getEnvironmentType();
            
            List<Map<String, String>> functionalGroups = new ArrayList<>();
            for (Feature feature : LimitsHandlerFactory.getHandler().listFeatures(environmentType)) {
                Map<String, String> functionalGroup = new HashMap<>();
                functionalGroup.put("idFeature", feature.getIdFeature());
                
                functionalGroups.add(functionalGroup);
            }
            
            response.putItem(OutParams.FUNCTIONAL_GROUPS, functionalGroups);
            
            List<Product> products = loadEnviromentProductsLabeled(administration, request.getIdEnvironment(), request.getLang());
            
            response.putItem(OutParams.ENVIRONMENT_PRODUCTS, products);
            
            List enabledChannels = new ArrayList<>();
            enabledChannels.add(CHANNEL_ALL);
            response.putItem(OutParams.ENABLED_CHANNELS, enabledChannels);
            
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
    private List<Product> loadEnviromentProductsLabeled(Administration administration, Integer idEnvironment, String lang) throws IOException{
        List<Product> products = administration.listEnvironmentProducts(idEnvironment);
        products = products == null ? Collections.emptyList() : products;
        populateProductLabes(products, lang);
        return products;
	}
    
	private void populateProductLabes(List<Product> products, String lang) {
		ProductLabeler productLabeler = CoreUtils.getProductLabeler(lang);
		products.forEach((p) -> p.setLabel(productLabeler.calculateShortLabel(p)));
	}
	
	
}