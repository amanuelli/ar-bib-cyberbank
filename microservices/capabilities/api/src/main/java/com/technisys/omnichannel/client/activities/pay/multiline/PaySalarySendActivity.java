package com.technisys.omnichannel.client.activities.pay.multiline;

import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.client.domain.TransactionLine.ErrorCode;
import com.technisys.omnichannel.client.domain.fields.TransactionlinesField;
import com.technisys.omnichannel.client.forms.fields.multilinefile.processors.InvalidTotalAmountException;
import com.technisys.omnichannel.client.forms.fields.multilinefile.processors.Processor;
import com.technisys.omnichannel.client.forms.fields.multilinefile.processors.ProcessorFactory;
import com.technisys.omnichannel.client.handlers.multiline.MultilineHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.ReturnCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.FileTooBigException;
import com.technisys.omnichannel.core.files.FilesHandler;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.forms.fields.FileField;
import org.apache.commons.lang3.StringUtils;
import org.joda.time.DateTime;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PaySalarySendActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(PaySalarySendActivity.class);

    public static final String ID = "pay.multiline.salary.send";

    public interface InParams {
        @DocumentedParam(type = List.class, description = "File with the payment lines")
        String FILE = "file";
        @DocumentedParam(type = String.class, description = "Reference assigned to the payment")
        String REFERENCE = "reference";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        ReturnCode returnCode = ReturnCodes.BACKEND_IS_PROCESSING_THE_TRANSACTION;

        try {
            List paramFile = request.getParam(InParams.FILE, List.class);
            List<FormField> fields = FormsHandler.getInstance().listFormFields(request.getIdForm(), request.getFormVersion());
            if (paramFile != null && !paramFile.isEmpty()) {
                Integer fileId = (Integer) ((Map) paramFile.get(0)).get("fileId");

                File file = FilesHandler.getInstance().readTransactionFileWithContents(fileId, request.getIdUser(), request.getIdEnvironment());
                try {
                    List<TransactionLine> transactionLines = processFileLines(request, file);
                    boolean headerError = (transactionLines.get(0).getErrorCode() == ErrorCode.EMPTY_HEADER) || (transactionLines.get(0).getErrorCode() == ErrorCode.INVALID_HEADER_LENGTH);
                    if ((transactionLines.size() == 1) && headerError) {
                        throw new IOException("Header error - " + transactionLines.get(0).getErrorCode());
                    } else {
                        MultilineHandler.addTransactionLines(transactionLines);
                    }
                } catch (InvalidTotalAmountException itae) {
                    returnCode = ReturnCodes.TOTAL_AMOUNT_CAN_NOT_BE_ZERO;
                }
            } else if (fields.stream()
                    .anyMatch(ff -> ff instanceof TransactionlinesField)) {
                FormField linesField = fields.stream()
                        .filter(ff -> ff instanceof TransactionlinesField)
                        .collect(Collectors.toList()).get(0);
                
                StringBuilder fileString = new StringBuilder();
                List<Map<String, Object>> lines = request.getParam(linesField.getIdField(), List.class);
                
                long fileId = System.currentTimeMillis();
                String currency;
                if (lines.isEmpty()) {
                    currency = "";
                } else {
                    TransactionLine line = new TransactionLine((Map<String, Object>) lines.get(0));
                    currency = line.getCreditAmountCurrency();
                }
                fileString.append(fileId)
                        .append(",")
                        .append(currency)
                        .append("\n");
                
                double totalAmount = 0;
                List<TransactionLine> transactionLines = new ArrayList();
                for (Map<String, Object> item : (List<Map<String, Object>>) request.getParam(linesField.getIdField(), List.class)) {
                    TransactionLine line = new TransactionLine((Map<String, Object>) item);
                    line.setIdTransaction(request.getIdTransaction());
                    line.setStatus(TransactionLine.Status.PENDING);
                    line.setStatusDate(new Date());
                    fileString.append(line.toString())
                            .append("\n");
                    totalAmount += line.getCreditAmountQuantity();
                    transactionLines.add(line);
                }
                
                if(totalAmount == 0) {
                     returnCode = ReturnCodes.TOTAL_AMOUNT_CAN_NOT_BE_ZERO;
                }

                String fileNameAndDescription = DateTime.now().toString() + ".txt";
                FilesHandler.getInstance().create(
                        request.getIdUser(),
                        false,
                        new ByteArrayInputStream(fileString.toString().trim().getBytes()),
                        fileNameAndDescription,
                        fileNameAndDescription,
                        ID
                );
                MultilineHandler.addTransactionLines(transactionLines);
            }
            response.setReturnCode(returnCode);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (FileTooBigException e) {
            log.error("Pay salary submit", e);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            return FormsHandler.getInstance().validateRequest(request);
        } catch (SchedulerException ex) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    private List<TransactionLine> processFileLines(Request request, File file) throws IOException, InvalidTotalAmountException {
        List<TransactionLine> transactionLines = new ArrayList<>();

        if (!StringUtils.isBlank(request.getIdForm())) {
            FormField formField = FormsHandler.getInstance().readField(request.getIdForm(), null, InParams.FILE);
            if (formField instanceof FileField) {
                //Instantiate subtype plugin and process file lines
                Processor processor = ProcessorFactory.getProcessorSubType(formField.getSubType());

                if (processor != null) {
                    transactionLines = processor.process(file.getContents(), request.getIdTransaction(), request.getParam(InParams.REFERENCE, String.class));
                }
            }
        }

        return transactionLines;
    }
}
