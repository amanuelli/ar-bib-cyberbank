/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.loans;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.StatementLoan;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.IOUtils;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author iocampo
 */
@DocumentedActivity("Download loan detail")
public class DownloadPaymentActivity extends Activity {

    public static final String ID = "loans.downloadPayment";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Loan id to download")
        String ID_LOAN = "idLoan";
        @DocumentedParam(type = String.class, description = "File format can be pdf and xls")
        String FORMAT = "format";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "File name to save")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = Object.class, description = "Download file with loan lines, it is Object because depends of file format, in xls is byte and pdf is String")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response;
        try {
            String idLoan = request.getParam(InParams.ID_LOAN, String.class);
            String format = request.getParam(InParams.FORMAT, String.class);

            Product product = Administration.getInstance().readProduct(idLoan, request.getIdEnvironment());
            List<StatementLoan> statements = com.technisys.omnichannel.client.activities.loans.ListStatementsActivity.searchStatements(request, product, 0);

            response = new Response(request);
            ExportResource export;
            export = exportXLS(statements, request.getLang());
            response.putItem(OutParams.CONTENT, export.getContent());
            response.putItem(OutParams.FILE_NAME, export.getFilename());
            response.setReturnCode(ReturnCodes.OK);

        } catch (IOException | BackendConnectorException | InvalidFormatException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        String idLoan = request.getParam(InParams.ID_LOAN, String.class);
        String format = request.getParam(InParams.FORMAT,String.class);

        if (!("xls".equalsIgnoreCase(format) || "pdf".equalsIgnoreCase(format))
                || idLoan.isEmpty()) {
            result.put(InParams.FORMAT, "administration.export.invalidFormat");
        }

        return result;

    }

    private List<List<String>> getHeader(String lang) {
        List<List<String>> header = new ArrayList<>();
        List<String> headLine = new ArrayList<>();
        headLine.add(I18nFactory.getHandler().getMessage("loans.details.export.title", lang));
        header.add(headLine);
        headLine = new ArrayList<>();
        headLine.add(I18nFactory.getHandler().getMessage("loans.details.export.idLoan", lang));
        header.add(headLine);
        return header;
    }

    private List<List<String>> buildLines(List<StatementLoan> statements, String lang) {
        List<List<String>> lines = new ArrayList<>();
        List<String> statementLines = new ArrayList<>();
        statementLines.add(I18nFactory.getHandler().getMessage("loans.details.statements.dueDate", lang));
        statementLines.add(I18nFactory.getHandler().getMessage("loans.details.statements.status", lang));
        statementLines.add(I18nFactory.getHandler().getMessage("loans.details.statements.paidDate", lang));
        statementLines.add(I18nFactory.getHandler().getMessage("loans.details.totalAmount", lang));
        lines.add(statementLines);
        for (StatementLoan statement : statements) {
            statementLines = new ArrayList<>();
            statementLines.add(DateUtils.formatShortDate(statement.getDueDate(), lang));
            statementLines.add(statement.getStatus());
            statementLines.add(statement.getPaidDate() != null ? DateUtils.formatShortDate(statement.getPaidDate(), lang) : "");
            statementLines.add(String.valueOf(statement.getImportAmount()));
            lines.add(statementLines);
        }
        return lines;
    }

    private List<List<String>> getFooter(List<StatementLoan> statements, String lang) {
        List<List<String>> footer = new ArrayList<>();
        List<String> footerLine = new ArrayList<>();
        footerLine.add(I18nFactory.getHandler().getMessage("loans.details.statements.count", lang));
        footerLine.add(String.valueOf(statements.size()));
        footer.add(footerLine);
        return footer;
    }

    private List<List<String>> unescapeHtml(List<List<String>> section) {
        List<List<String>> unescape = new ArrayList<>();
        for (List<String> sublist : section) {
            List<String> unescapedSublist = new ArrayList<>();
            sublist.forEach(record -> unescapedSublist.add(StringEscapeUtils.unescapeHtml4(record)));
            unescape.add(unescapedSublist);
        }
        return unescape;
    }

    private byte[] buildXLS(List<List<String>> unescapedHeader, List<List<String>> unescapedLines, List<List<String>> unescapedFooter) throws IOException, InvalidFormatException {
        XLSTransformer transformer = new XLSTransformer();
        Map<String, Object> beans = new HashMap<>();

        beans.put("header", unescapedHeader);
        beans.put("lines", unescapedLines);
        beans.put("footer", unescapedFooter);

        byte[] data;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             Workbook work = WorkbookFactory.create(DownloadPaymentActivity.class.getResourceAsStream("/templates/loan_template.xls"));) {
            transformer.transformWorkbook(work, beans);
            work.write(out);
            data = out.toByteArray();
            out.flush();
        }
        InputStream pdfSource = new ByteArrayInputStream(data);
        return Base64.encodeBase64(IOUtils.toByteArray(pdfSource));
    }

    private ExportResource exportXLS(List<StatementLoan> statements, String lang) throws IOException, InvalidFormatException {
        List<List<String>> header = getHeader(lang);
        List<List<String>> lines = buildLines(statements, lang);
        List<List<String>> footer = getFooter(statements, lang);

        List<List<String>> unescapedHeader = unescapeHtml(header);
        List<List<String>> unescapedLines = unescapeHtml(lines);
        List<List<String>> unescapedFooter = unescapeHtml(footer);

        byte[] xls = buildXLS(unescapedHeader, unescapedLines, unescapedFooter);

        String filename = I18nFactory.getHandler().getMessage("loans.statements", lang) + ".xls";

        return new ExportResource(filename, xls);
    }

    private String compileHtml(String html, String lang, List<StatementLoan> statements) {
        I18n i18n = I18nFactory.getHandler();
        html = html.replace("%%LIST_STATEMENTS_TITLE%%", i18n.getMessage("loans.details.export.title", lang));
        html = html.replace("%%DUE_DATE%%", i18n.getMessage("loans.details.statements.dueDate", lang));
        html = html.replace("%%STATUS%%", i18n.getMessage("loans.details.statements.status", lang));
        html = html.replace("%%PAID_DATE%%", i18n.getMessage("loans.details.statements.paidDate", lang));
        html = html.replace("%%AMOUNT%%", i18n.getMessage("loans.details.totalAmount", lang));
        StringBuilder strBuilder = new StringBuilder("");

        for (StatementLoan statement : statements) {
            strBuilder.append("<tr>");
            strBuilder.append("<td>").append(DateUtils.formatShortDate(statement.getDueDate(), lang)).append("</td>");
            strBuilder.append("<td>").append(statement.getStatus()).append("</td>");
            strBuilder.append("<td>").append(statement.getPaidDate() != null ? DateUtils.formatShortDate(statement.getPaidDate(), lang) : "").append("</td>");
            strBuilder.append("<td>").append(statement.getImportAmount()).append("</td>");
            strBuilder.append("</tr>");
        }

        html = html.replace("%%STATEMENTS%%", strBuilder.toString());
        html = html.replace("%%STATEMENTS_COUNT_TEXT%%", i18n.getMessage("loans.details.statements.count", lang));
        html = html.replace("%%STATEMENTS_COUNT%%", String.valueOf(statements.size()));
        html = html.replace("%%OMNICHANNEL_DISCLAIMER%%", i18n.getMessage("loans.details.statements.export.disclaimer", lang));

        return html;
    }

    private String getHtmlTemplate() throws IOException {
        InputStream is = this.getClass().getResourceAsStream("/templates/loan_pdf_template.html");
        StringBuilder template = new StringBuilder();
        String line;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            while ((line = reader.readLine()) != null) {
                template.append(line).append("\n");
            }
        } finally {
            is.close();
        }

        return template.toString();
    }
    //TECDIGSK-625
/*
    private byte[] generatePdf(String html) throws ParserConfigurationException, SAXException, IOException, DocumentException {
        Document doc = ExportUtils.getDocumentBuilder().parse(new InputSource(new StringReader(html)));
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(doc, "");
        renderer.layout();

        byte[] content;

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            renderer.createPDF(os);
            InputStream pdfSource = new ByteArrayInputStream(os.toByteArray());
            content = Base64.encodeBase64(IOUtils.toByteArray(pdfSource));
        }
        return content;
    }

    private ExportResource exportPDF(List<StatementLoan> statements, String lang) throws IOException, ParserConfigurationException, SAXException, DocumentException {
        String template = getHtmlTemplate();

        byte[] pdf = generatePdf(compileHtml(template, lang, statements));
        String filename = I18nFactory.getHandler().getMessage("loans.statements", lang) + ".pdf";

        return new ExportResource(filename, pdf);
    }

 */

    static class ExportResource {

        String filename;
        byte[] content;

        public ExportResource(String filename, byte[] content) {
            this.filename = filename;
            this.content = content;
        }

        public String getFilename() {
            return filename;
        }

        public byte[] getContent() {
            return content;
        }
    }
}
