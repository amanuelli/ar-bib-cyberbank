/*
 *  Copyright 2013 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.CurrencyExchange;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.utils.plugins.CurrencyConverter;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author salva
 */
public class RubiconCurrencyConverter implements CurrencyConverter {

    private static final Logger log = LoggerFactory.getLogger(RubiconCurrencyConverter.class);

    @Override
    public double convert(String fromCurrency, String toCurrency, double amount) {
        if (fromCurrency.equals(toCurrency)) {
            return amount;
        }

        try {
            String masterCurrency = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency");
            List<CurrencyExchange> currencyList = RubiconCoreConnectorC.listExchangeRates(null, null);

            CurrencyExchange quoteFrom = getCurrencyMarketRate(fromCurrency, currencyList);
            CurrencyExchange quoteTo = getCurrencyMarketRate(toCurrency, currencyList);

            double result = amount;

            if (!fromCurrency.equals(masterCurrency)) {
                // convert from fromCurrency to masterCurrency
                result = result * quoteFrom.getPurchase();
            }

            if (!toCurrency.equals(masterCurrency)) {
                // convert from masterCurrency to toCurrency
                result = result / quoteTo.getSale();
            }

            return result;
        } catch (BackendConnectorException ex) {
            log.error("Error en los servicios del backend: Obteniendo el listado de tasa de cambio entre monedas", ex);
            return amount;
        } catch (IOException ex) {
            log.error("Error en los servicios del backend: Obteniendo valor de la configuracion de core.masterCurrency", ex);
            return amount;
        }
    }

    @Override
    public double exchange(String fromCurrency, String toCurrency, double amount) {
        if (fromCurrency.equals(toCurrency)) {
            return 1.0;
        }

        try {
            List<CurrencyExchange> currencyList = RubiconCoreConnectorC.listExchangeRates(null, null);

            CurrencyExchange quoteFrom = getCurrencyMarketRate(fromCurrency, currencyList);
            CurrencyExchange quoteTo = getCurrencyMarketRate(toCurrency, currencyList);

            double result = 0.0;
            if (quoteTo.getSale() > 0) {
                result = quoteFrom.getPurchase() / quoteTo.getSale();
            }

            return result;
        } catch (BackendConnectorException ex) {
            log.error("Error en los servicios del backend", ex);
            return 1.0;
        }
    }

    @Override
    public Map<String, Double> arbitrages() {
        return new HashMap<>();
    }

    private static CurrencyExchange getCurrencyMarketRate(String idCurrency, List<CurrencyExchange> currencyList) {
        CurrencyExchange result = new CurrencyExchange();
        if (!idCurrency.isEmpty()
                && currencyList != null
                && !currencyList.isEmpty()) {
            for (CurrencyExchange cur : currencyList) {
                if (StringUtils.equals(cur.getTargetCurrencyCode(), idCurrency)) {
                    result = cur;
                    break;
                }
            }
        }
        return result;
    }
}
