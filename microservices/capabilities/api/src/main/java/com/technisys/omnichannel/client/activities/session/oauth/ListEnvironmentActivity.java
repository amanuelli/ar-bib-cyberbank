/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session.oauth;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.CalendarRestrictionsUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.ipauthorization.IPAuthorizationHandler;
import com.technisys.omnichannel.core.session.SessionUtils;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
 * Get the list of environments of user after calling oauth endpoint
 */
@DocumentedActivity("Login step 2.2")
public class ListEnvironmentActivity extends Activity {

    public static final String ID = "session.login.oauth.step2b";

    public interface InParams {

    }

    public interface OutParams {
        @DocumentedParam(type = Map.class, description = "Map of available environments: &lt;identifier, name&gt;")
        String ENVIRONMENTS = "environments";

        @DocumentedParam(type = String.class, description = "Indicates if the user must select one environment on the next step")
        String SELECT_ENVIRONMENT = "selectEnvironment";

        @DocumentedParam(type = String.class, description = "Default language, selected by the user, e.g. &quot;en&quot;")
        String LANGUAGE = "lang";

        @DocumentedParam(type = String.class, description = "First name of the user, e.g. &quot;Brittny&quot;")
        String USER_FIRST_NAME = "_userFirstName";

        @DocumentedParam(type = String.class, description = "Full name of the user, e.g. &quot;Brittny Beall&quot;")
        String USER_FULL_NAME = "_userFullName";

        @DocumentedParam(type = Integer.class, description = "Default user environment")
        String DEFAULT_ENVIRONMENT = "defEnvironment";

        @DocumentedParam(type = Integer.class, description = "True if user environment is enabled, false if not")
        String DEFAULT_ENVIRONMENT_ENABLED = "defEnvironmentEnabled";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try{
            // Controlo que este en algun ambiente activo
            List<Environment> environmentList = Administration.getInstance().listEnvironmentsWhereUserIsActive(request.getIdUser(), request.getChannel());
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            boolean activeInDefaultEnvironment = false;
            int userDefaultEnv = -1;
            for (Environment e : environmentList) {
                if (user.getIdDefaultEnvironment() != -1 && e.getIdEnvironment() == user.getIdDefaultEnvironment()) {
                    activeInDefaultEnvironment = true;
                    userDefaultEnv = user.getIdDefaultEnvironment();
                    break;
                }
            }
            boolean calendarIsValid;
            boolean ipsAreValid;
            for (Environment env:environmentList){
                calendarIsValid = CalendarRestrictionsUtils.validateAccessByEnvironmentAndUser(env.getIdEnvironment(),request.getIdUser(),request.getValueDate());
                ipsAreValid = IPAuthorizationHandler.getInstance().checkUserEnvironmentAccess(env.getIdEnvironment(), request.getIdUser(), request.getClientIP());
                env.setAllowToAccess(calendarIsValid && ipsAreValid);
            }

            Map<Integer, Map<String, String>> environments = SessionUtils.assembleEnviromentMap(environmentList);

            if (environments.isEmpty()) {
                throw new ActivityException(ReturnCodes.NO_ACTIVE_ENVIRONMENTS, "Invalid active environment " + request.getIdEnvironment() + " for user " + request.getIdUser());
            }

            // Muestro la página de elegir ambiente si tiene mas de un ambiente activo Y:
            // 1- No tiene un ambiente por defecto
            // 2- Tiene un ambiente por defecto pero se encuentra deshabilitado
            boolean selectEnvironment = environments.size() > 1 && !activeInDefaultEnvironment;

            String userFirstName = user == null ? "" : user.getFirstName();
            String userFullName = user == null ? "" : user.getFullName();
            String userLang = user == null ? request.getLang() : user.getLang();

            Response response = new Response(request);
            response.setReturnCode(ReturnCodes.OK);
            response.putItem(OutParams.ENVIRONMENTS, environments);
            response.putItem(OutParams.SELECT_ENVIRONMENT, selectEnvironment);
            response.putItem(OutParams.LANGUAGE, userLang);
            response.putItem(OutParams.USER_FIRST_NAME, userFirstName);
            response.putItem(OutParams.USER_FULL_NAME, userFullName);
            response.putItem(OutParams.DEFAULT_ENVIRONMENT, userDefaultEnv);
            response.putItem(OutParams.DEFAULT_ENVIRONMENT_ENABLED, user != null && user.getIdDefaultEnvironment() == userDefaultEnv);

            return response;
        } catch (IOException e) {
        throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

}