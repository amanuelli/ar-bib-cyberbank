/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.bpm.servicetasks;

import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.client.utils.SynchronizationUtils;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.FormTransaction;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;

public class RequestAccount implements JavaDelegate {

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String idTransaction = (String) execution.getVariable(FormsHandler.WORKFLOW_VARIABLE_ID_TRANSACTION);
        FormTransaction formTransaction = FormsHandler.getInstance().getWorkflowData(idTransaction);
        Environment environment = Administration.getInstance().readEnvironment(formTransaction.getIdEnvironment());


        // TODO - By default a saving account will be created, please change this to create the account with the type selected
        CoreAccountConnectorOrchestrator.addAccount(idTransaction, environment.getProductGroupId());

        SynchronizationUtils.syncEnvironmentProducts(idTransaction, environment, null);
        // TODO - Maybe we need to add permissions to the created account

        TransactionHandlerFactory.getInstance().updateStatus(idTransaction, Transaction.STATUS_FINISHED, null);
    }

}
