package com.technisys.omnichannel.client.activities.administration.medium;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.Map;
import java.util.HashMap;

@DocumentedActivity("Modify signature level")
public class ModifySignatureActivity extends Activity {

    public static final String ID = "administration.medium.modify.signature.send";
    
    public interface InParams {
        
        @DocumentedParam(type = String.class, description = "User id to configure signature level")
        String ID_USER = "idUser";
        @DocumentedParam(type = String.class, description = "Signature level (null to disable signature)")
        String SIGNATURE_LEVEL = "signatureLevel";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }
            
            Administration administration = Administration.getInstance();
            String idUser = request.getParam(InParams.ID_USER, String.class);
            int idEnvironment = request.getIdEnvironment();
            EnvironmentUser envUser = administration.readEnvironmentUserInfo(idUser, idEnvironment);

            administration.modifyUserInEnvironment(AccessManagementHandlerFactory.getHandler().getUser(idUser), idEnvironment, request.getParam(InParams.SIGNATURE_LEVEL, String.class), envUser.getIdUserStatus(), null, null);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
