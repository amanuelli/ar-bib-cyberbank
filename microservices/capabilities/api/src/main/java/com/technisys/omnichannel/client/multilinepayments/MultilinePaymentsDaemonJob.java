package com.technisys.omnichannel.client.multilinepayments;

/*
 * Schedulable task for execution of daemon for multiline payments
 */

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorMP;
import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.client.domain.TransactionLine.Status;
import com.technisys.omnichannel.client.domain.TransactionLineResult;
import com.technisys.omnichannel.client.handlers.multiline.MultilineHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class MultilinePaymentsDaemonJob implements Job {

    private static final Logger log = LoggerFactory.getLogger(MultilinePaymentsDaemonJob.class);

    public static final String DAEMON_JOB_NAME = "multilinePayments";

    public static final String DAEMON_TRIGGER_NAME = DAEMON_JOB_NAME;

    private TransactionLine getTransactionLine(TransactionLineResult tlr) throws IOException {
        TransactionLine transactionLine = MultilineHandler.getTransactionLine(tlr.getIdTransaction(), tlr.getLineNumber());

        Map<String, Object> transactionLineData = new HashMap<>();
        transactionLineData.put("idTransaction", transactionLine.getIdTransaction());
        transactionLineData.put("lineNumber", transactionLine.getLineNumber());
        transactionLineData.put("reference", transactionLine.getReference());
        transactionLineData.put("creditAccountName", transactionLine.getCreditAccountName());
        transactionLineData.put("creditAccountNumber", transactionLine.getCreditAccountNumber());
        transactionLineData.put("creditAmountCurrency", transactionLine.getCreditAmountCurrency());
        transactionLineData.put("creditAmountQuantity", transactionLine.getCreditAmountQuantity());
        transactionLineData.put("status", tlr.getStatus());
        transactionLineData.put("statusDate", new Date());
        transactionLineData.put("errorCode", tlr.getStatus() == Status.PROCESSED ? null : tlr.getErrorCode());

        return new TransactionLine(transactionLineData);
    }

    @Override
    public void execute(JobExecutionContext context) throws JobExecutionException {
        log.info("MultilinePayments daemon started");

        try {
            TransactionLine nextTransaction = MultilineHandler.getNextPendingTransaction();
            if (nextTransaction != null) {
                Integer batchCount = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "multilinePayments.transactionLinesBatch.count");
                List<TransactionLine> transactionLines = MultilineHandler.listPendingTransactionLinesByCount(nextTransaction.getIdTransaction(),  batchCount + 1);
                boolean setFinalizedTransaction = transactionLines.size() <= batchCount;

                if (!transactionLines.isEmpty()) {
                    List<TransactionLineResult> result = RubiconCoreConnectorMP.salaryPaymentSend(transactionLines.size() <= batchCount ? transactionLines : transactionLines.subList(0, batchCount - 1));
                    Map<Status, Long> groupedResult = result.stream().collect(Collectors.groupingBy(TransactionLineResult::getStatus, Collectors.counting()));

                    transactionLines = new ArrayList<>();
                    for (TransactionLineResult tlr : result) {
                        transactionLines.add(getTransactionLine(tlr));
                    }

                    MultilineHandler.updateTransactionLines(transactionLines);

                    String logString = String.format("MultilinePayments daemon ended - Transaction ID: %s - Processed lines: %s - Lines with error: %s - Lines without error: %s", nextTransaction.getIdTransaction(), result.size(), groupedResult.get(Status.PROCESSED_WITH_ERROR), groupedResult.get(Status.PROCESSED));
                    log.info(logString);
                } else {
                    log.info("MultilinePayments daemon ended - No pending transaction lines");
                }

                if (setFinalizedTransaction) {
                    TransactionHandlerFactory.getInstance().updateStatus(nextTransaction.getIdTransaction(), Transaction.STATUS_FINISHED, ReturnCodes.OK.getReturnCodeAsString());
                }
            } else {
                log.info("MultilinePayments daemon ended - No pending transactions");
            }
        } catch (IOException e) {
            log.error("MultilinePayments daemon ended", e); // With errors :(
        }
    }
}
