/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.ReturnCodes.OK;

/**
 * @author Marcelo Bruno
 */
@ExchangeActivity
@DocumentedActivity("Enrollment Accept ESign")
public class EsignAccept extends Activity {

    public static final String ID = "enrollment.esignAccept";

    private static final String EXCHANGE_TOKEN = "_exchangeToken";


    public interface InParams {

        @DocumentedParam(type = String.class, description = "User Mail used in during onboarding")
        String USERMAIL = "userMail";

        @DocumentedParam(type = Integer.class, description = "Id environment to accet ESign")
        String ID_ENVVIRONMENT = "idEnvironment";
    }


    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int actualCondition = GeneralConditions.getInstance().actualCondition();
            int idEnvironment = -1;

            String userMail = request.getParam(InParams.USERMAIL, String.class);
            User user = AccessManagementHandlerFactory.getHandler().getUserByEmail(userMail);
            if(user != null) {
                idEnvironment = request.getParam(InParams.ID_ENVVIRONMENT, Integer.class);
                EnvironmentUser envUser = Administration.getInstance().readEnvironmentUserInfo(user.getIdUser(), idEnvironment);
                if(envUser != null && user.getEmail().equals(envUser.getEmail())) {
                    List<Integer> environmentList = new ArrayList();
                    environmentList.add(idEnvironment);
                    GeneralConditions.getInstance().acceptGeneralConditions(actualCondition, user.getIdUser(), environmentList);
                } else {
                    throw new ActivityException(ReturnCodes.IO_ERROR, "EnvUser not found or emails are differents on sign action");
                }
            } else {
                throw new ActivityException(ReturnCodes.IO_ERROR, "User not found on sign action");
            }

            ExchangeTokenHandler.delete(request.getParam(EXCHANGE_TOKEN, String.class));

            response.setReturnCode(OK);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        if(StringUtils.isBlank(request.getParam(InParams.USERMAIL, String.class))) {
            result.put(InParams.USERMAIL, "enrollment.esignAccept.usermail.empty");
        } else {
            if(request.getParam(InParams.ID_ENVVIRONMENT, Integer.class) == null) {
                result.put(InParams.ID_ENVVIRONMENT, "enrollment.esignAccept.idenvironment.empty");
            }
        }
        return result;
    }

}
