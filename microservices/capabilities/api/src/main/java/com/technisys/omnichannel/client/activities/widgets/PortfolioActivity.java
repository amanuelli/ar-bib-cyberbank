/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.widgets;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreProductConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Deposit;
import com.technisys.omnichannel.client.utils.CurrencyUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 * List portfolio assets
 */
public class PortfolioActivity extends Activity {

    public static final String ID = "widgets.investments";

    public interface InParams {
    }

    public interface OutParams {
        String FIXED_INCOMES_QUANTITY = "fixedIncomesQuantity";
        String FIXED_INCOMES_AMOUNT = "fixedIncomesAmount";
        
        String LIQUIDITY_QUANTITY = "liquidityQuantity";
        String LIQUIDITY_AMOUNT = "liquidityAmount";
        
        String OTHERS_QUANTITY = "othersQuantity";
        String OTHERS_AMOUNT = "othersAmount";
        
        String VARIABLE_INCOMES_QUANTITY = "variableIncomesQuantity";
        String VARIABLE_INCOMES_AMOUNT = "variableIncomesAmount";
    }

    @SuppressWarnings("unchecked")
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            List<String> idPermissions = new ArrayList<>();
            idPermissions.add(PRODUCT_READ_PERMISSION);
            List<String> productTypes = Arrays.asList(Constants.PRODUCT_PF_KEY, Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY);
            List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);
            List<Product> coreProducts = CoreProductConnectorOrchestrator.list(request.getIdTransaction(), environment.getClients(), productTypes);
            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
            String masterCurrency = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency");
            
            
            List<Product> fixedIncomes = new ArrayList<>();
            long fixedIncomesAmount = 0;
            
            List<Product> liquidity = new ArrayList<>();
            long liquidityAmount = 0;
            
            List<Product> others = new ArrayList<>();
            long othersAmount = 0;
            
            List<Product> variableIncomes = new ArrayList<>();
            long variableIncomesAmount = 0;
            
            for (Product p : coreProducts) {
                for (Product environmentProduct : environmentProducts) {
                    if (p.getExtraInfo().equals(environmentProduct.getExtraInfo())) {
                        switch (p.getProductType()) {
                            case Constants.PRODUCT_CA_KEY:
                            case Constants.PRODUCT_CC_KEY:
                                Account account = (Account) p;
                                account.setProductAlias(environmentProduct.getProductAlias());
                                account.setShortLabel(pLabeler.calculateShortLabel(account, true));
                                Map<String, Boolean> permissions = new HashMap<>();
                                if (Constants.PRODUCT_CC_KEY.equals(p.getProductType())) {
                                    permissions.put(Constants.REQUEST_CHECKBOOK_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(),
                                            account.getIdProduct(), Constants.REQUEST_CHECKBOOK_PERMISSION));
                                }
                                permissions.put(Constants.TRANSFER_INTERNAL_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(),
                                        account.getIdProduct(), Constants.TRANSFER_INTERNAL_PERMISSION));
                                account.setPermissions(permissions);
                                if (!masterCurrency.equals(account.getCurrency())) {
                                    double value = CurrencyUtils.convert(masterCurrency, account.getCurrency(), account.getBalance(), request.getIdTransaction(), environmentProduct.getClient().getIdClient());
                                    liquidityAmount += value;
                                } else {
                                    liquidityAmount += account.getBalance();
                                }
                                liquidity.add(account);
                                break;
                            case Constants.PRODUCT_PF_KEY:
                                Deposit deposit = (Deposit) p;
                                deposit.setProductAlias(environmentProduct.getProductAlias());
                                deposit.setShortLabel(pLabeler.calculateShortLabel(deposit));
                                deposit.setLabel(pLabeler.calculateLabel(deposit));
                                deposit.setIdProduct(environmentProduct.getIdProduct());
                                if (!masterCurrency.equals(deposit.getCurrency())) {
                                    double value = CurrencyUtils.convert(masterCurrency, deposit.getCurrency(), deposit.getBalance(), request.getIdTransaction(),
                                            environmentProduct.getClient().getIdClient());
                                    fixedIncomesAmount += value;
                                } else {
                                    fixedIncomesAmount += deposit.getBalance();
                                }
                                fixedIncomes.add(deposit);
                                break;
                            default:
                                break;
                        }

                    }
                }
            }
            
            /**
             * norbes@20180807: Esto se deja acá de común acuerdo con F, para
             * mostrar algún dato en el ambiente premier de John Snow.
             * 
             * Cuando se haga el detalle vamos a tener que listar los productos
             * puntuales
             */
            if (request.getIdEnvironment() == 12) { // Devkoa LLC
                // 125k USD en 3 productos diferentes
                variableIncomes.add(null);
                variableIncomes.add(null);
                variableIncomes.add(null);
                variableIncomesAmount = 3728750;
                
                // 7k USD en 1 producto
                others.add(null);
                othersAmount = 20881;
            }
            
            response.putItem(OutParams.FIXED_INCOMES_QUANTITY, fixedIncomes.size());
            response.putItem(OutParams.FIXED_INCOMES_AMOUNT, fixedIncomesAmount);
            
            response.putItem(OutParams.LIQUIDITY_QUANTITY, liquidity.size());
            response.putItem(OutParams.LIQUIDITY_AMOUNT, liquidityAmount);
            
            response.putItem(OutParams.OTHERS_QUANTITY, others.size());
            response.putItem(OutParams.OTHERS_AMOUNT, othersAmount);
            
            response.putItem(OutParams.VARIABLE_INCOMES_QUANTITY, variableIncomes.size());
            response.putItem(OutParams.VARIABLE_INCOMES_AMOUNT, variableIncomesAmount);
            
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
