/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.credentials.CredentialPluginFactory;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;

/**
 *
 */
public class WizardAccessDataActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(WizardAccessDataActivity.class);

    public static final String ID = "enrollment.wizard.accessData";

    public interface InParams {
        String USER_PIN = "_pin";
        String USER_PIN_CONFIRMATION = "_pinConfirmation";

        String USERNAME = "username";
        String USER_PASSWORD = "_password";
        String USER_PASSWORD_CONFIRMATION = "_passwordConfirmation";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            InvitationCode invitation = getInvitationCode(request);

            // Pasos anteriores
            Map<String, String> result = WizardVerificationCodeActivity.validateFields(request, invitation);
            if (ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "enrollment.personalData.enabled")) {
                result.putAll(WizardPersonalDataActivity.validateFields(request, invitation));
            }

            if (!result.isEmpty()) {
                result.put("NO_FIELD", "enrollment.wizard.invalidEnrollmentData");
            }

            // Paso actual
            result.putAll(validateFields(request));

            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String username = request.getParam(InParams.USERNAME, String.class);
            if (StringUtils.isBlank(username)) {
                result.put(InParams.USERNAME, "enrollment.step3.username.error.empty");
            } else if (!ValidationUtils.validateUsername(username)) {
                result.put(InParams.USERNAME, "enrollment.step3.username.error.invalidFormat");
            } else if (!AccessManagementHandlerFactory.getHandler().isUsernameAvailable(username)) {
                result.put(InParams.USERNAME, "enrollment.step3.username.error.alreadyExists");
            }

            try {
                CredentialPlugin credentialPlugin = CredentialPluginFactory.getCredentialPlugin(Credential.PWD_CREDENTIAL);

                String password = request.getParam(InParams.USER_PASSWORD, String.class);
                if (StringUtils.isBlank(password)) {
                    result.put(InParams.USER_PASSWORD, "enrollment.step3.password.error.empty");
                } else if (!credentialPlugin.validateFormat(username, password)) {
                    result.put(InParams.USER_PASSWORD, "enrollment.step3.password.error.unfulfilledRules");
                }

                String passwordConfirmation = request.getParam(InParams.USER_PASSWORD_CONFIRMATION, String.class);
                if (StringUtils.isBlank(passwordConfirmation)) {
                    result.put(InParams.USER_PASSWORD_CONFIRMATION, "enrollment.step3.passwordConfirmation.error.empty");
                }

                if (result.get(InParams.USER_PASSWORD) == null && result.get(InParams.USER_PASSWORD_CONFIRMATION) == null
                    && (!StringUtils.equals(password, passwordConfirmation))) {
                        result.put(InParams.USER_PASSWORD, "enrollment.step3.password.error.passwordConfirmationMustMatch");
                        result.put(InParams.USER_PASSWORD_CONFIRMATION, "enrollment.step3.password.error.passwordMustMatch");
                }

            } catch (DispatchingException ex) {
                log.error("Error instanciando credencial de Password", ex);
                result.put(InParams.USER_PASSWORD, "enrollment.accessData.passwordError");
            }

            // Segundo factor de autenticación
            switch (StringUtils.defaultString(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "auth.login.credentialRequested"))) {
                case Credential.PIN_CREDENTIAL:
                    try {
                        CredentialPlugin credentialPlugin = CredentialPluginFactory.getCredentialPlugin(Credential.PIN_CREDENTIAL);

                        String pin = request.getParam(InParams.USER_PIN, String.class);
                        if (StringUtils.isBlank(pin)) {
                            result.put(InParams.USER_PIN, "enrollment.step3.pin.error.empty");
                        } else if (!credentialPlugin.validateFormat(username, pin)) {
                            result.put(InParams.USER_PIN, "enrollment.step3.pin.error.invalidFormat");
                        }

                        String pinConfirmation = request.getParam(InParams.USER_PIN_CONFIRMATION, String.class);
                        if (StringUtils.isBlank(pinConfirmation)) {
                            result.put(InParams.USER_PIN_CONFIRMATION, "enrollment.step3.pinConfirmation.error.empty");
                        }

                        if (result.get(InParams.USER_PIN) == null && result.get(InParams.USER_PIN_CONFIRMATION) == null
                            && (!StringUtils.equals(pin, pinConfirmation))) {
                                result.put(InParams.USER_PIN, "enrollment.step3.pin.error.pinsMustMatch");
                                result.put(InParams.USER_PIN_CONFIRMATION, "enrollment.step3.pinConfirmation.error.pinsMustMatch");
                        }

                    } catch (DispatchingException ex) {
                        log.error("Error instanciando credencial de PIN", ex);
                        result.put(InParams.USER_PIN, "enrollment.accessData.pinError");
                    }
                    break;
                case Credential.OTP_CREDENTIAL:
                    Credential otpCredential = request.getCredential(Credential.OTP_CREDENTIAL);
                    if(otpCredential != null){ otpCredential.getValue();}

                    // TODO: No tengo todavía información de como quieren que se manejen los OTPS
                    break;
                default:
                    log.error("Unexpected case.");
                    break;
            }

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

}
