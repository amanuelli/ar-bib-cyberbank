/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.onboarding;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.activities.enrollment.DigitalPreActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.domain.Country;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.ReturnCodes.OK;
import static com.technisys.omnichannel.client.ReturnCodes.BACKEND_SERVICE_ERROR;

/**
 * @author Jhossept G
 */
@AnonymousActivity
@DocumentedActivity("List Onboarding Selectors")
public class ListOnboardingSelectorsActivity extends Activity {

    public static final String ID = "onboarding.selectors.list";

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "List of occupations")
        String OCCUPATIONS = "occupation_options";

        @DocumentedParam(type = String.class, description = "List of sources of wealth")
        String SOURCES_OF_WEALTH = "source_of_wealth_options";

        @DocumentedParam(type = String.class, description = "List of annual incomes")
        String ANNUAL_INCOME = "annual_income_options";

        @DocumentedParam(type = Country[].class, description = "List of countries")
        String COUNTRY_LIST = "countryList";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            I18n i18n = I18nFactory.getHandler();
            response.putItem(OutParams.OCCUPATIONS, getSelectorOptions(i18n, request.getLang(),"onboarding.selectors.occupations"));
            response.putItem(OutParams.SOURCES_OF_WEALTH, getSelectorOptions(i18n, request.getLang(),"onboarding.selectors.source_of_wealth"));
            response.putItem(OutParams.ANNUAL_INCOME, getSelectorOptions(i18n, request.getLang(),"onboarding.selectors.annual_income"));

            List<Country> countryCodeList = CountryCodesHandler.getCountryList(request.getLang());
            response.putItem(DigitalPreActivity.OutParams.COUNTRY_LIST, countryCodeList);
            response.setReturnCode(OK);
        } catch (Exception e) {
            throw new ActivityException(BACKEND_SERVICE_ERROR, e);
        }
        return response;
    }

    Map<String, String> getSelectorOptions(I18n i18n, String lang, String id) throws IOException {
        Map<String, String> map = new HashMap<>();

        List<String> listOptions = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, id);
        for (String opt : listOptions) {
            map.put(opt,i18n.getMessage(id + "." + opt,lang));
        }


        return map;
    }

}
