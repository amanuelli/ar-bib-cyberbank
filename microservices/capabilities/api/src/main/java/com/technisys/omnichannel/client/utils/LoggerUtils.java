/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import org.slf4j.Logger;
import java.text.MessageFormat;

/**
 *
 * @author agonzalez
 */
public class LoggerUtils {

    private LoggerUtils(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    /**
     * Método que sirve para simplificar el loggueo de información.
     * @param logObject Logger object de la clase
     * @param message Mensaje principal enviado
     * @param param Parametros del mensaje a loggear
     */
    public static void logInfoMessage(Logger logObject, String message, String ...param) {
        String logMessage= MessageFormat.format(message, (Object[]) param);
        logObject.info(logMessage);
    }

    /**
     * Método que sirve para simplificar el loggueo de advertencias.
     * @param logObject Logger object de la clase
     * @param message Mensaje principal enviado
     * @param param Parametros del mensaje a loggear
     */
    public static void logWarningMessage(Logger logObject, String message, String ...param) {
        String logMessage= MessageFormat.format(message, (Object[]) param);
        logObject.warn(logMessage);
    }

    /**
     * Método que sirve para simplificar el loggueo de errores.
     * @param logObject Logger object de la clase
     * @param message Mensaje principal enviado
     * @param param Parametros del mensaje a loggear
     */
    public static void logErrorMessage(Logger logObject, String message, String ...param) {
        String logMessage= MessageFormat.format(message, (Object[]) param);
        logObject.error(logMessage);
    }

}
