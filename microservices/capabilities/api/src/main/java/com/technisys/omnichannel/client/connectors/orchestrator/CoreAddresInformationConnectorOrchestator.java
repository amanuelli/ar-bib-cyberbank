package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankAddresConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.Province;

import java.util.ArrayList;
import java.util.List;

/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
public class CoreAddresInformationConnectorOrchestator extends CoreConnectorOrchestrator {

    public static List<Province> listProvince(String codeCountry) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                ArrayList<Province> states = new ArrayList<>();

                Province province = new Province();
                province.setCode("MVD");
                province.setDescription("Montevideo");

                states.add(province);

                return states;
            case CYBERBANK_CONNECTOR:
                return CyberbankAddresConnector.listProvince(codeCountry).getData();
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

}
