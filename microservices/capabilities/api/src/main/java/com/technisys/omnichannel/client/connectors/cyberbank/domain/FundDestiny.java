/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Cristobal Meneses
 */
public class FundDestiny implements Serializable {

    private Integer fundDestinyId;
    private String fundDestinyCode;
    private String fundDestinyDescription;

    public FundDestiny() {
    }

    public FundDestiny(Integer fundDestinyId, String fundDestinyCode, String fundDestinyDescription) {
        this.fundDestinyId = fundDestinyId;
        this.fundDestinyCode = fundDestinyCode;
        this.fundDestinyDescription = fundDestinyDescription;
    }

    public Integer getFundDestinyId() {
        return fundDestinyId;
    }

    public void setFundDestinyId(Integer fundDestinyId) {
        this.fundDestinyId = fundDestinyId;
    }

    public String getFundDestinyCode() {
        return fundDestinyCode;
    }

    public void setFundDestinyCode(String fundDestinyCode) {
        this.fundDestinyCode = fundDestinyCode;
    }

    public String getFundDestinyDescription() {
        return fundDestinyDescription;
    }

    public void setFundDestinyDescription(String fundDestinyDescription) {
        this.fundDestinyDescription = fundDestinyDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        FundDestiny that = (FundDestiny) o;
        return Objects.equals(fundDestinyId, that.fundDestinyId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(fundDestinyId, fundDestinyCode, fundDestinyDescription);
    }
}
