/*
 *  Copyright 2019 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

public class CreditScore {
    
    private int returnCode;
    private String returnCodeDescription;
    
    private int score;
    private String documentId;
    
    public CreditScore (int returnCode, String returnCodeDescription, int score, String documentId) {
        this.returnCode = returnCode;
        this.returnCodeDescription = returnCodeDescription;
        this.score = score;
        this.documentId = documentId;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnCodeDescription() {
        return returnCodeDescription;
    }

    public void setReturnCodeDescription(String returnCodeDescription) {
        this.returnCodeDescription = returnCodeDescription;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
    
    
    
}
