/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.deposits;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.client.handlers.notes.NotesHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 *
 */
public class ListStatementDetailsActivity extends Activity {

    public static final String ID = "deposits.listStatementDetails";

    public interface InParams {

        String ID_DEPOSIT = "idDeposit";
        String ID_STATEMENT = "idStatement";
    }

    public interface OutParams {

        String STATEMENT = "statement";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            int idStatement = request.getParam(InParams.ID_STATEMENT, Integer.class);
            String idDeposit = request.getParam(InParams.ID_DEPOSIT, String.class);

            Statement statemet = RubiconCoreConnectorC.readNoteBoucher(idStatement);

            String note = NotesHandler.readNote(idStatement, idDeposit);
            statemet.setNote(note);

            response.putItem(OutParams.STATEMENT, statemet);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
