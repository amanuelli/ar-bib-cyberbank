/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

/**
 *
 */
public class ModifySocialNetworksConfigurationPreviewActivity extends Activity {

    public static final String ID = "preferences.socialnetworks.configuration.modify.preview";

    public interface InParams {

        public String FACEBOOK_PROFILE_ID = "facebookId";
        public String FACEBOOK_PROFILE_NAME = "facebookName";
        public String TWITTER_USER_ID = "twitterId";
        public String TWITTER_USER_NAME = "twitterName";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        ModifySocialNetworksConfigurationActivity modifyNotif = new ModifySocialNetworksConfigurationActivity();
        return modifyNotif.validateFields();
    }
}
