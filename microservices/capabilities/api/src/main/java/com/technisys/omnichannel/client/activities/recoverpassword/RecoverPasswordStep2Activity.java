/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.recoverpassword;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.ValidationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.validationcodes.ValidationCodesHandler;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Validate that the invitation code sent by mail is correct.
 * Return exchangeToken to execute the next step.
 */
@AnonymousActivity
@DocumentedActivity("Recover Password Step 2")
public class RecoverPasswordStep2Activity extends Activity {

    public static final String ID = "session.recoverPassword.step2";

    private static final String NEXT_ACTIVITY_IN_FLOW = RecoverPasswordStep3Activity.ID;

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Validation code sent to the user by mail. e.g. &quot;BB75-9816-3A88&quot;") 
        String CODE = "_resetCode";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Validation code sent to the user by mail. e.g. &quot;BB75-9816-3A88&quot;")
        String CODE = "_resetCode";
        @DocumentedParam(type = String.class, description = "Token required to call the next step on this process. Next step: " + NEXT_ACTIVITY_IN_FLOW)
        String EXCHANGE_TOKEN = "_exchangeToken";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            String code = com.technisys.omnichannel.client.utils.StringUtils.normalizeInvitationCode(request.getParam(InParams.CODE, String.class));
            ValidationCode validationCode = ValidationCodesHandler.readValidationCode(code);

            String exchangeToken = ExchangeTokenHandler.create(request.getChannel(), request.getLang(), validationCode.getIdUser(),
                    -1, request.getUserAgent(), request.getClientIP(), new String[]{NEXT_ACTIVITY_IN_FLOW});

            response.putItem(OutParams.CODE, code);
            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return validateCode(request);
    }

    public Map<String, String> validateCode(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String code = com.technisys.omnichannel.client.utils.StringUtils.normalizeInvitationCode(request.getParam(InParams.CODE, String.class));

            if (StringUtils.isBlank(code)) {
                result.put(InParams.CODE, RecoverPasswordStep2Activity.ID + ".param.code.invalid");
            } else {
                ValidationCode validationCode = ValidationCodesHandler.readValidationCode(code);
                if (validationCode == null) {
                    result.put(InParams.CODE, RecoverPasswordStep2Activity.ID + ".param.code.invalid");
                } else if (!ValidationCode.TYPE_RECOVERPASS.equals(validationCode.getType())) {
                    result.put(InParams.CODE, RecoverPasswordStep2Activity.ID + ".param.code.invalid");
                } else if (!ValidationCode.STATUS_AVAILABLE.equals(validationCode.getStatus())) {
                    result.put(InParams.CODE, RecoverPasswordStep2Activity.ID + ".param.code.used");
                } else {
                    //hago la misma validacion que se hace despues
                    long validityTime = ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "recoverPassword.mail.validity");
                    long currentTime = (new Date()).getTime();
                    long creationTime = validationCode.getCreationDate().getTime();
                    if (creationTime + validityTime < currentTime) {
                        result.put(InParams.CODE, RecoverPasswordStep2Activity.ID + ".param.code.expired");
                    }
                }
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

}
