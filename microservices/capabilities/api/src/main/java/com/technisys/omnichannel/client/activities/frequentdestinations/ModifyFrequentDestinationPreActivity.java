/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.frequentdestinations;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.FrequentDestination;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.frequentdestinations.FrequentDestinationsHandler;
import com.technisys.omnichannel.core.utils.JsonUtils;
import java.io.IOException;

/**
 *
 */
public class ModifyFrequentDestinationPreActivity extends Activity {

    public static final String ID = "frequentdestinations.modify.pre";
    
    public interface InParams {
        String FD_ID = "id";
    }

    public interface OutParams {
        String FREQUENT_DESTINATION = "frequentDestination";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            
            Integer id = request.getParam(InParams.FD_ID, Integer.class);
            
            FrequentDestination fd = FrequentDestinationsHandler.readFrequentDestination(id);

            response.putItem(OutParams.FREQUENT_DESTINATION, JsonUtils.toJson(fd, false, false));
            response.setReturnCode(ReturnCodes.OK);
            
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
}