/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.forms.fields.productselector;

import com.technisys.omnichannel.client.domain.StatementLoan;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.FrequentDestination;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.domain.fields.ProductselectorField;
import com.technisys.omnichannel.core.forms.fields.FrontendOption;
import com.technisys.omnichannel.core.forms.fields.OptionsResourceHandler;
import com.technisys.omnichannel.core.forms.fields.productselector.ProductselectorBaseFieldHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;

/**
 *
 * @author <a href="mailto:nicolas.orbes@technisys.com">Nicol&aacute;s Orbes</a>
 */
public class ProductselectorFieldHandler extends ProductselectorBaseFieldHandler {
        
    /**
     * Dado un Request y el campo espec&iacute;fico carga la lista de productos
     * para los cuales el usuario xxx
     *
     * @param field Campo del formulario
     * @param request Request del usuario para obtener el usuario, ambiente, etc
     * @param transaction Transaccion (opcional) que se esta queriendo desplegar
     * junto con el formulario,
     * @return Retorna un Mapa con la información necesaria para armar el
     * despliegue del campo.
     * @throws IOException Error de comunicaci&oacute;n
     * @throws org.quartz.SchedulerException
     */
    @Override
    public Map<String, Serializable> loadFormFieldData(FormField field, Request request, Transaction transaction) throws IOException, SchedulerException {
        //En base al parametro lastVersion se puede saber si es para modo vista o edicion. 
        //En caso de modo vista debe venir un atributo idTransaction para saber que data cargar
        Map<String, Serializable> data = super.loadFormFieldData(field, request, transaction);

        ProductselectorField productSelectorField = (ProductselectorField) field;

        List<Option> options = new ArrayList<>();

        if (transaction != null && !Transaction.STATUS_DRAFT.equals(transaction.getIdTransactionStatus())) {
            //si es un ticket no se consultan saldos, siquiera se listan los productos dado que el producto puede no existir ahora
            //Los datos se obtienen de la transaccion

            String idProduct = null;
            Map map = (Map)transaction.getData().get(field.getIdField());
            Boolean isFrequentDestination = RequestParamsUtils.getValue(map, "isFrequentDestination", Boolean.class);
                
            if (map != null
                && (map.get("value") instanceof String)) {
                    idProduct = (String) map.get("value");
            }

            if (!"_empty".equals(idProduct) && StringUtils.isNotBlank(idProduct)
                && (!"other".equals(idProduct))){
                    Option option = new Option();
                    option.setId(idProduct);
                    if(isFrequentDestination) {
                        FrequentDestination fd = RequestParamsUtils.getValue(map, FrequentDestination.class);  
                        String number = "";
                        if(StringUtils.isNotEmpty(fd.getAccountNumber())) {
                            number = fd.getAccountNumber();
                        }else if(StringUtils.isNotEmpty(fd.getCreditCardNumber())) {
                            number = fd.getCreditCardNumber();
                        }else if(StringUtils.isNotEmpty(fd.getLoanNumber())) {
                            number = fd.getLoanNumber();
                        }
                        option.setLabel(number);
                        option.setFrequentDestination(fd);
                    }else 
                        option.setLabel(CoreUtils.getProductLabeler(request.getLang()).calculateShortLabel(idProduct, request.getIdEnvironment()));
                    
                    options.add(option);
            }
        } else {
            //cargo opciones en funcion del subtipo
            options = (List<Option>) OptionsResourceHandler.getOptions(productSelectorField, request);
            data.put("emptyOptionLabel", I18nFactory.getHandler().getMessage("form.selectors.emptyOptionLabel", request.getLang()));
        }

        if (productSelectorField.isShowOtherOption()) {
            boolean addOtherOption = true;
            if (ProductselectorField.SHOW_OTHER_BY_PRODUCT_TYPES.equals(productSelectorField.getShowOtherBy())) {
                //chequeo si tiene permiso para ver la opcion "otro producto" (chequeando permiso sobre tipos de productos o generico)
                List<String> otherProductTypes = productSelectorField.getShowOtherProductTypeList();

                if (otherProductTypes != null && !otherProductTypes.isEmpty()) {
                    if (StringUtils.isNotBlank(productSelectorField.getShowOtherPermission())) {
                        List<Product> otherPersmissionProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), new String[]{productSelectorField.getShowOtherPermission()}, otherProductTypes.toArray(new String[0]));

                        if (otherPersmissionProducts == null || otherPersmissionProducts.isEmpty()) {
                            //no tiene productos con permiso 
                            addOtherOption = false;
                        }
                    }
                } else if (StringUtils.isNotBlank(productSelectorField.getShowOtherPermission())) {
                    //chequeo permiso generico
                    boolean hasPermission = Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), null, productSelectorField.getShowOtherPermission());
                    addOtherOption = hasPermission;
                }
            }

            if (addOtherOption) {
                Option option = new Option();
                option.setId("other");
                option.setLabel(productSelectorField.getShowOtherTextMap().get(request.getLang()));
                options.add(option);
            }
        }

        data.put("options", (Serializable) options);

        return data;
    }
    
    
    @Override
    public List<String> getFieldAttributes() throws IOException {
        List<String> attributes = new ArrayList<>();
        attributes.add("type");
        attributes.add("currency");
        return attributes;
    }
    
    
    public class Option extends FrontendOption implements Comparable<Option>, Serializable {

        private String currency;
        private Amount balance;
        private List<StatementLoan> statementsLoan;
        private double minimumPayment;
        private double totalPayment;
        private List<String> permissions;
        private String type;
        private FrequentDestination frequentDestination;

        public String getCurrency() {
            return currency;
        }

        public void setCurrency(String currency) {
            this.currency = currency;
        }

        public Amount getBalance() {
            return balance;
        }

        public void setBalance(Amount balance) {
            this.balance = balance;
        }

        public List<String> getPermissions() {
            return permissions;
        }

        public void setPermissions(List<String> permissions) {
            this.permissions = permissions;
        }
        
        public String getType() {
            return type;
        }

        public double getMinimumPayment() {
            return minimumPayment;
        }

        public void setMinimumPayment(double minimumPayment) {
            this.minimumPayment = minimumPayment;
        }

        public double getTotalPayment() {
            return totalPayment;
        }

        public void setTotalPayment(double totalPayment) {
            this.totalPayment = totalPayment;
        }
        
        public FrequentDestination getFrequentDestination() {
            return frequentDestination;
        }

        public void setFrequentDestination(FrequentDestination frequentDestination) {
            this.frequentDestination = frequentDestination;
        }

        public void setType(String type) {
            this.type = type;
        }

        @Override
        public int compareTo(Option o) {
            if (o != null) {
                return label.compareTo(o.getLabel());
            }
            return -1;
        }

        public List<StatementLoan> getStatementsLoan() {
            return statementsLoan;
        }

        public void setStatementsLoan(List<StatementLoan> statementsLoan) {
            this.statementsLoan = statementsLoan;
        }

    }
    
    
}
