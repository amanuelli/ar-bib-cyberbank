/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.frequentdestinations;


import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

/**
 *
 */
public class ModifyFrequentDestinationPreviewActivity extends Activity {

    public static final String ID = "frequentdestinations.modify.preview";
    
    public interface OutParams {
        String NAME = "name";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.putItem(OutParams.NAME, request.getParam(OutParams.NAME, String.class));
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        ModifyFrequentDestinationActivity modifyUserData = new ModifyFrequentDestinationActivity();
        Map<String, String> result = modifyUserData.validate(request);
        return result;
    }
}
