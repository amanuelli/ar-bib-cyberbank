/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.handlers.enrollment.EnrollmentHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.ReturnCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;

/**
 *
 */
@ExchangeActivity @DocumentedActivity("Enrollment wizard pre")
public class WizardPreActivity extends Activity {
    public static final String ID = "enrollment.wizard.pre";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Invitation's code")
        String CODE = "_code";
    }

    public interface OutParams {
        @DocumentedParam(type = Integer.class, description = "Invitation's code ID")
        String INVITATION = "invitation";
        @DocumentedParam(type = Client.class, description = "Client's object details")
        String CLIENT = "client";
        @DocumentedParam(type = String.class, description = "Second factor type")
        String SECOND_FACTOR_AUTH = "secondFactorAuth";
        @DocumentedParam(type = Boolean.class, description = "Personal data status")
        String PERSONAL_DATA_ENABLED = "personalDataEnabled";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            InvitationCode invitation = getInvitationCode(request);

            // Por si entran al flujo de enrollment wizzard teninendo un codigo de invitaciÃ³n de una asociacion
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber());
            if (user != null) {
                throw new ActivityException(ReturnCodes.INVALID_INVITATION_CODE, "No se puede realizar un enrollment cuando el usuario ya existe en omnichannel, es necesario realziar una asociacion de cuenta. Code: " + invitation.getInvitationCode());
            }

            ClientUser backendClient = null;
            if(invitation.getProductGroupId() != null) {
                backendClient = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber(), invitation.getProductGroupId());
            }

            if (backendClient == null && !ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "administration.users.invite.notInBackend.enabled")) {
                throw new ActivityException(ReturnCodes.INVALID_INVITATION_CODE, "No esta habilitada la invitaciÃ³n de usuario no existentes en el backend");
            }

            response.putItem(OutParams.INVITATION, invitation);
            response.putItem(OutParams.CLIENT, backendClient);
            response.putItem(OutParams.SECOND_FACTOR_AUTH, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "auth.login.credentialRequested"));
            response.putItem(OutParams.PERSONAL_DATA_ENABLED, ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "enrollment.personalData.enabled"));

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    public static InvitationCode getInvitationCode(Request request) throws ActivityException, IOException {
        String code = request.getParam(InParams.CODE, String.class);
        if (StringUtils.isBlank(code)) {
            throw new ActivityException(ReturnCodes.INVALID_INVITATION_CODE);
        }

        InvitationCode invitation = InvitationCodesHandler.readInvitationCode(code);
        ReturnCode error = EnrollmentHandler.validateInvitationCode(invitation);
        if (error != null) {
            throw new ActivityException(error);
        }

        return invitation;
    }
}
