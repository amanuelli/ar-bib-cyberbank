/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.handlers.enrollment.EnrollmentHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;

import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;

/**
 *
 */
public class WizardResendVerificationCodeActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(WizardResendVerificationCodeActivity.class);

    public static final String ID = "enrollment.wizard.resendVerificationCode";

    public interface InParams {
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            InvitationCode invitation = getInvitationCode(request);

            EnrollmentHandler.sendVerificationCode(invitation, request.getLang());

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (MessagingException ex) {
            log.error("No se pudo enviar el código de verificación para el enrollment", ex);
        }

        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

}
