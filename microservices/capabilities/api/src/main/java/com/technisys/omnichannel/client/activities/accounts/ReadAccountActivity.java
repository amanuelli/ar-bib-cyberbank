/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.accounts;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@DocumentedActivity("Read account")
public class ReadAccountActivity extends Activity {

    public static final String ID = "accounts.read";

    public interface InParams {

        @DocumentedParam(type =  String.class, description =  "Account ID")
        String ID_ACCOUNT = "idAccount";
    }

    public interface OutParams {

        @DocumentedParam(type =  Account.class, description =  "Account details object")
        String ACCOUNT = "account";
        @DocumentedParam(type =  String[].class, description =  "Channel's list")
        String CHANNEL_LIST = "channelList";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {

            String idAccount = (String) request.getParam(InParams.ID_ACCOUNT, String.class);

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            Product product = Administration.getInstance().readProduct(idAccount, request.getIdEnvironment());
            Account account = CoreAccountConnectorOrchestrator.readAccount(request.getIdTransaction(), product);

            account.setPaperless(product.getPaperless());
            account.setProductAlias(product.getProductAlias());
            account.setShortLabel(pLabeler.calculateShortLabel(account));
            account.setLabel(pLabeler.calculateLabel(account));

            Map<String, Boolean> permissions = new HashMap<>();
            permissions.put(Constants.TRANSFER_INTERNAL_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), account.getIdProduct(), Constants.TRANSFER_INTERNAL_PERMISSION));
            permissions.put(Constants.TRANSFER_THIRD_PARTIES_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), account.getIdProduct(), Constants.TRANSFER_THIRD_PARTIES_PERMISSION));
            permissions.put(Constants.TRANSFER_FOREIGN_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), account.getIdProduct(), Constants.TRANSFER_FOREIGN_PERMISSION));
            permissions.put(Constants.TRANSFER_LOCAL_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), account.getIdProduct(), Constants.TRANSFER_LOCAL_PERMISSION));
            permissions.put(Constants.REQUEST_CHECKBOOK_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), account.getIdProduct(), Constants.REQUEST_CHECKBOOK_PERMISSION));
            permissions.put(Constants.ACCOUNT_FUND, Authorization.hasPermissionOverForm(request.getIdUser(), request.getIdEnvironment(), Constants.ACCOUNT_FUND));

            account.setPermissions(permissions);

            response.putItem(OutParams.ACCOUNT, account);
            response.putItem(OutParams.CHANNEL_LIST, CoreUtils.getEnabledChannels());

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
