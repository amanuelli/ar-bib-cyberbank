/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

/**
 *
 * @author salva
 */
public class AssetsAndLiabilities {
    double[] assets;
    double[] liabilities;

    public double[] getAssets() {
        return assets;
    }

    public void setAssets(double[] assets) {
        this.assets = assets;
    }

    public double[] getLiabilities() {
        return liabilities;
    }

    public void setLiabilities(double[] liabilities) {
        this.liabilities = liabilities;
    }
    
}
