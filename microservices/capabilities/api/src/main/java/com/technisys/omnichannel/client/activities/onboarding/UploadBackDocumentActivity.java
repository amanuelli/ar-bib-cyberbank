package com.technisys.omnichannel.client.activities.onboarding;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingDigital;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingSafeway;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;


/**
 *
 */
@ExchangeActivity
@DocumentedActivity("This activity uploads and validate the back side of the document in the onboarding flow")
public class UploadBackDocumentActivity extends Activity {

    public static final String ID = "onboarding.wizard.uploadBackDocument";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Document image to save in base 64 format")
        String DOCUMENT_TO_SAVE = "_documentToSave";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Exchange token to continue the flow")
        String EXCHANGE_TOKEN = "_exchangeToken";

        @DocumentedParam(type = String.class, description = "Document country extracted")
        String DOCUMENT_COUNTRY = "documentCountry";

        @DocumentedParam(type = String.class, description = "Document number extracted")
        String DOCUMENT_NUMBER = "documentNumber";

        @DocumentedParam(type = String.class, description = "Firstname extracted")
        String FIRST_NAME = "firstName";

        @DocumentedParam(type = String.class, description = "Lastname extracted")
        String LAST_NAME = "lastName";

        @DocumentedParam(type = String.class, description = "Date of birth extracted")
        String DATE_OF_BIRTH = "dateOfBirth";

        @DocumentedParam(type = String.class, description = "Full document extracted")
        String FULL_DOCUMENT = "fullDocument";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response;
        if("digital".equals(ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "onboarding.mode", "digital"))) {
            response = new OnboardingDigital().doUploadBackDocumentExecute(request, null);
        } else {
            response = new OnboardingSafeway().doUploadBackDocumentExecute(request, null);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return OnboardingUtils.validateOnboardingImageLength(request.getParam(InParams.DOCUMENT_TO_SAVE, String.class));
    }

}
