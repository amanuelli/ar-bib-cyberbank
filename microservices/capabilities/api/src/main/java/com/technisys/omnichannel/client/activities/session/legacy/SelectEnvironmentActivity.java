/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session.legacy;

import com.maxmind.geoip2.exception.AddressNotFoundException;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.session.ChangeEnvironmentActivity;
import com.technisys.omnichannel.client.activities.session.oauth.OauthSelectEnvironmentActivity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.utils.CalendarRestrictionsUtils;
import com.technisys.omnichannel.client.utils.MaskUtils;
import com.technisys.omnichannel.client.utils.PermissionsUtils;
import com.technisys.omnichannel.client.utils.SynchronizationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.GeneralCondition;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.postloginchecks.geoip.GeoIPLoginInfo;
import com.technisys.omnichannel.core.postloginchecks.geoip.PostLoginCheckGeoIPDataAccess;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.preprocessors.ipauthorization.IPAuthorizationHandler;
import com.technisys.omnichannel.core.session.SessionHandlerFactory;
import com.technisys.omnichannel.core.session.SessionUtils;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.util.*;

import static java.lang.Math.min;

/**
 *
 */
@ExchangeActivity
@DocumentedActivity("Third login step")
public class SelectEnvironmentActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(SelectEnvironmentActivity.class);

    private final String NEXT_ACTIVITY_IN_FLOW = AcceptGeneralConditionsActivity.ID;

    public static final String ID = "session.login.legacy.step3";

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Identifier of environment you want to work with")
        String ENVIRONMENT = "environment";

        @DocumentedParam(type = Boolean.class, description = "If you want to set this as default")
        String SET_AS_DEFAULT = "setAsDefault";

        @DocumentedParam(type = String.class, description = "Tells if the user can administrate the environment")
        String LATITUDE = "latitude";

        @DocumentedParam(type = String.class, description = "Tells if the user can administrate the environment")
        String LONGITUDE = "longitude";

        @DocumentedParam(type = String.class, description = "Id device", optional = true)
        String ID_DEVICE = "idDevice";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "User's identifier")
        String USERID = "userId";

        @DocumentedParam(type = String.class, description = "User's full name, e.g. &quot;Brittny Beall")
        String USER_FULLNAME = "userFullName";

        @DocumentedParam(type = String.class, description = "User's email address")
        String EMAIL = "email";

        @DocumentedParam(type = String.class, description = "User's default Avatar identifier")
        String DEFAULT_AVATAR_ID = "defaultAvatarId";

        @DocumentedParam(type = Integer.class, description = "User's security seal identifier")
        String SECURITY_SEAL = "_securitySeal";

        @DocumentedParam(type = Integer.class, description = "User's default environment identifier")
        String DEFAULT_ENVIRONMENT_ID = "idDefaultEnvironment";

        @DocumentedParam(type = Integer.class, description = "User's active environment identifier")
        String ACTIVE_ENVIRONMENT_ID = "activeIdEnvironment";

        @DocumentedParam(type = String.class, description = "User's active environment name")
        String ACTIVE_ENVIRONMENT_NAME = "activeEnvironmentName";

        @DocumentedParam(type = String.class, description = "User's active environment type")
        String ACTIVE_ENVIRONMENT_TYPE = "activeEnvironmentType";

        @DocumentedParam(type = String.class, description = "User's active environment administration scheme")
        String ADMINISTRATION_SCHEME = "administrationScheme";

        @DocumentedParam(type = Map.class, description = "Map of available environments: &lt;identifier, name&gt;")
        String ENVIRONMENTS = "environments";

        @DocumentedParam(type = Map.class, description = "Map of available permissions: &lt;identifier, boolean indicator&gt;")
        String PERMISIONS = "permissions";

        @DocumentedParam(type = List.class, description = "List of forms, those the user can interact with")
        String FORMS = "forms";

        @DocumentedParam(type = List.class, description = "List of activities identifiers, those the user most use")
        String FREQUENT_ACTIONS = "frequentActions";

        @DocumentedParam(type = String.class, description = "Default language, selected by the user, e.g. &quot;en&quot;")
        String LANGUAGE = "lang";

        @DocumentedParam(type = String.class, description = "Channel identifier, e.g. &qout;frontend&quot")
        String CHANNEL = "channel";

        @DocumentedParam(type = String.class, description = "Token required to call the authenticated activities")
        String ACCESS_TOKEN = "_accessToken";

        @DocumentedParam(type = GeoIPLoginInfo.class, description = "Assorted data of previous login")
        String PREVIOUS_LOGIN_INFO = "previousLoginInfo";

        @DocumentedParam(type = GeneralCondition.class, description = "Assorted data of general conditions")
        String GENERAL_CONDITIONS = "generalConditions";

        @DocumentedParam(type = String.class, description = "Text of general conditions")
        String GENERAL_CONDITION_TEXT = "generalConditionsText";

        @DocumentedParam(type = String.class, description = "Due date for general condition acceptance")
        String GENERAL_CONDITION_EXPIRATION_DATE = "generalConditionsExpirationDate";

        @DocumentedParam(type = Boolean.class, description = "If due date should be displayed")
        String GENERAL_CONDITIONS_SHOW_EXPIRATION = "generalConditionsShowExpiration";

        @DocumentedParam(type = String.class, description = "Token required to call the next step on this process")
        String EXCHANGE_TOKEN = "_exchangeToken";

        @DocumentedParam(type = String.class, description = "Tells if the user can administrate the environment")
        String IS_ADMINISTRATOR = "isAdministrator";

        @DocumentedParam(type = List.class, description = "Clients of the environment")
        String CLIENTS = "clients";

        @DocumentedParam(type = Integer.class, description = "Environment id to accept conditions")
        String ID_ENVIRONMENT_TO_ACCEPT_CONDITIONS = "idEnvironmentToAcceptConditions";

        @DocumentedParam(type = Date.class, description = "Date of last signed T&Cs")
        String SIGNED_TERMS_AND_CONDITIONS_DATE = "signedTAndCDate";

        @DocumentedParam(type = Date.class, description = "Date when the user did the IRS statement")
        String IRS_DATE = "irsDate";

        @DocumentedParam(type = Date.class, description = "Specify if the PEP data is completed by the user")
        String PEP_COMPLETED = "pepCompleted";

        @DocumentedParam(type = Date.class, description = "Specify if the IRS data is completed by the user")
        String IRS_COMPLETED = "irsCompleted";

        @DocumentedParam(type = Date.class, description = "Return the ssnid code")
        String SSNID = "ssnid";

        @DocumentedParam(type = Boolean.class, description = "Enabled channels")
        String ENABLED_ASSISTANT = "enabledAssistant";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Environment activeEnvironment = null;
            Integer idEnvironment = request.getParam(InParams.ENVIRONMENT, Integer.class);
            idEnvironment = idEnvironment != null && idEnvironment > 0 ? idEnvironment : request.getIdEnvironment();

            boolean isActive = Administration.getInstance().isEnvironmentUserActive(request.getIdUser(), idEnvironment, request.getChannel());

            if (!isActive) {
                throw new ActivityException(ReturnCodes.USER_NOT_ACTIVE_IN_ENVIRONMENT, "Invalid active environment (environment: " + idEnvironment + ")");
            }

            List<Environment> environmentList = Administration.getInstance().listEnvironmentsWhereUserIsActive(request.getIdUser(), request.getChannel());
            activeEnvironment =  Administration.getInstance().readEnvironment(idEnvironment);
            boolean isAdministrator = Authorization.hasPermission(request.getIdUser(), idEnvironment, null, Constants.ADMINISTRATION_VIEW_PERMISSION);

            // Sincronizar los ambientes del usuarios
            SynchronizationUtils.syncEnvironmentProducts(request.getIdTransaction(), activeEnvironment, activeEnvironment.getLastSynchronization());

            //Sincronizar datos usuarios
            if (!StringUtils.isEmpty(activeEnvironment.getProductGroupId())) {
                SynchronizationUtils.syncUserData(request.getIdTransaction(), request.getIdUser(), activeEnvironment.getProductGroupId());
            }
            // Si es necesario marcamos el ambiente como el por defecto del usuario
            Integer idEnvironmentToChange = request.getParam(InParams.ENVIRONMENT, Integer.class);
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            if (idEnvironmentToChange != null && idEnvironmentToChange > 0 && user.getIdDefaultEnvironment() != idEnvironmentToChange) {
                boolean setAsDefault = request.getParameters().containsKey(ChangeEnvironmentActivity.InParams.SET_AS_DEFAULT) ? request.getParam(ChangeEnvironmentActivity.InParams.SET_AS_DEFAULT, Boolean.class) : false;
                if (setAsDefault) {
                    AccessManagementHandlerFactory.getHandler().updateUserDefaultEnvironment(request.getIdUser(), idEnvironmentToChange);
                    user.setIdDefaultEnvironment((idEnvironmentToChange));
                }
            }

            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);

            ExchangeTokenHandler.updateEnvironment(exchangeToken, activeEnvironment.getIdEnvironment());

            //Condiciones generales
            GeneralConditions gcHandler = GeneralConditions.getInstance();

            int actualCondition = gcHandler.actualCondition();

            EnvironmentUser envUser = Administration.getInstance().readEnvironmentUserInfo(request.getIdUser(), activeEnvironment.getIdEnvironment());
            boolean enabledAssistant = envUser.getEnabledChannels().contains("assistant");

            Response response;

            if (envUser.getIdCondition() == actualCondition || actualCondition == 0) {
                //No tiene que aceptar, termino el flujo
                request.setIdEnvironment(activeEnvironment.getIdEnvironment());
                ExchangeTokenHandler.delete(request.getParam(EXCHANGE_TOKEN, String.class));
                boolean calendarIsValid;
                boolean ipsAreValid;
                for (Environment env:environmentList){
                    calendarIsValid = CalendarRestrictionsUtils.validateAccessByEnvironmentAndUser(env.getIdEnvironment(),request.getIdUser(), request.getValueDate());
                    ipsAreValid = IPAuthorizationHandler.getInstance().checkUserEnvironmentAccess(env.getIdEnvironment(), request.getIdUser(), request.getClientIP());
                    env.setAllowToAccess(calendarIsValid && ipsAreValid);
                }
                response = generateLastLoginStepResponse(request, user, activeEnvironment, SessionUtils.assembleEnviromentMap(environmentList));
                response.putItem(OauthSelectEnvironmentActivity.OutParams.SIGNED_TERMS_AND_CONDITIONS_DATE,
                        gcHandler.
                                getSignatureDate(gcHandler.actualCondition(),
                                        request.getIdUser(),
                                        idEnvironment));

                if(!envUser.isIrsValue()){
                    response.putItem(OutParams.IRS_DATE,envUser.getIrsDate());
                }
            } else {
                //Tiene que aceptar las condiciones
                response = new Response(request);
                GeneralCondition generalCondition = gcHandler.readGeneralConditionInfo(actualCondition);
                if (envUser.getIdCondition() == 0) {
                    response.putItem(OutParams.GENERAL_CONDITIONS_SHOW_EXPIRATION, false);
                } else {
                    response.putItem(OutParams.GENERAL_CONDITIONS_SHOW_EXPIRATION, true);
                }

                response.putItem(OutParams.GENERAL_CONDITIONS, generalCondition);
                response.putItem(OutParams.GENERAL_CONDITION_TEXT, I18nFactory.getHandler().getMessage("generalConditions.term." + generalCondition.getTerm() + ".body", request.getLang()));
                response.putItem(OutParams.GENERAL_CONDITION_EXPIRATION_DATE, DateUtils.formatShortDate(generalCondition.getDateFinal()));
                response.putItem(OutParams.ID_ENVIRONMENT_TO_ACCEPT_CONDITIONS, activeEnvironment.getIdEnvironment());
                ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);

                List<String> activities = new ArrayList<>(Arrays.asList(StringUtils.split(token.getActivities(), ',')));
                if (!activities.contains(NEXT_ACTIVITY_IN_FLOW)) {
                    activities.add(NEXT_ACTIVITY_IN_FLOW);
                }
                ExchangeTokenHandler.updateActivities(exchangeToken, activities.toArray(new String[activities.size()]));

                response.setReturnCode(ReturnCodes.OK);
                response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            }

            response.putItem(OutParams.CLIENTS, activeEnvironment.getClients());
            response.putItem(OutParams.IS_ADMINISTRATOR, isAdministrator);
            response.putItem(OutParams.ENABLED_ASSISTANT, enabledAssistant);

            //Get PEP & IRS Completed Values
            if(user != null){
                response.putItem(OutParams.PEP_COMPLETED,user.isPepCompleted());
                response.putItem(OutParams.IRS_COMPLETED,user.isIrsCompleted());
                if(user.getSsnid()!=null)
                    response.putItem(OutParams.SSNID,MaskUtils.maskRange(user.getSsnid(),4,null));
            }
            return response;
        } catch (NullPointerException e) {
            throw new NullPointerException(e.getMessage());
        } catch (ActivityException ex) {
            throw ex;
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        }catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (Exception e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    private static List<GeoIPLoginInfo> updateUserLoginHistory(Request request) throws IOException {
        // Actualizo los datos de login del usuario
        AccessManagementHandlerFactory.getHandler().updateUserLoginData(request.getIdUser(), true, new Date(), request.getClientIP());

        Double locationAux = request.getParam(InParams.LATITUDE, Double.class);
        double latitude = (locationAux != null) ? locationAux : 0;
        locationAux = request.getParam(InParams.LONGITUDE, Double.class);
        double longitude = (locationAux != null) ? locationAux : 0;

        String idDevice = request.getParam(InParams.ID_DEVICE, String.class);

        String userAgent = request.getUserAgent();
        String browser = RequestParamsUtils.getBrowserFromUserAgent(userAgent);

        // Agrego el IP del usuario a la lista de los ultimos 5.
        PostLoginCheckGeoIPDataAccess.getInstance().addIPToGeoIPLog(request.getIdUser(), request.getClientIP(), new Date(), latitude, longitude, idDevice, browser);

        // Busco los datos del login anterior
        List<GeoIPLoginInfo> lastFiveLoginInfoList = Collections.<GeoIPLoginInfo>emptyList();
        List<GeoIPLoginInfo> loginInfoList = PostLoginCheckGeoIPDataAccess.getInstance().listLastIPs(request.getIdUser());

        if (loginInfoList != null && loginInfoList.size() > 1) { // Si la lista no es vacia y tiene al menos 2 elementos (necesario porque se acaba de insertar uno)

            lastFiveLoginInfoList = loginInfoList.subList(1, min(loginInfoList.size(), 6)); // Guardo la sublista de los ultimos 5 registros disponibles

            for (int i = 0; i < lastFiveLoginInfoList.size(); i++) { // Recorro la lista

                if (StringUtils.isNotBlank(lastFiveLoginInfoList.get(i).getIp())) {
                    InetAddress inetAddress = InetAddress.getByName(lastFiveLoginInfoList.get(i).getIp());

                    // En desarrollo, la IP es 127.0.0.1 o 0:0:0:0:0:0:0:1 y en testing los IPs son de la red local;
                    // las cuales no estan la DB. Por tanto si estamos se utiliza la IP publica del lab de montevideo
                    if (inetAddress.isSiteLocalAddress() || inetAddress.isLoopbackAddress()) {
                        String clientIp = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "cyberbank.localAddressTesting");
                        lastFiveLoginInfoList.get(i).setIp(clientIp);
                        inetAddress = InetAddress.getByName(clientIp);
                    }

                    // Buscamos los datos en la base de datos de GeoIP2 or GeoLite2 database
                    try {
                        CityResponse response = PostLoginCheckGeoIPDataAccess.getInstance().getCity(inetAddress);
                        lastFiveLoginInfoList.get(i).setCity(response.getCity().getName());

                        lastFiveLoginInfoList.get(i).setCountry(response.getCountry().getName());
                        if(lastFiveLoginInfoList.get(i).getLatitude() == 0 && lastFiveLoginInfoList.get(i).getLongitude() == 0) {
                            lastFiveLoginInfoList.get(i).setLatitude(response.getLocation().getLatitude());
                            lastFiveLoginInfoList.get(i).setLongitude(response.getLocation().getLongitude());
                        } else {
                            lastFiveLoginInfoList.get(i).setLatitude(lastFiveLoginInfoList.get(i).getLatitude());
                            lastFiveLoginInfoList.get(i).setLongitude(lastFiveLoginInfoList.get(i).getLongitude());
                        }

                    } catch (AddressNotFoundException e) {
                        log.warn("The IP (" + lastFiveLoginInfoList.get(i).getIp() + ") is not in the DB", e);

                    } catch (GeoIp2Exception e) {
                        throw new IOException(e);
                    }
                }

            }
        }
        return lastFiveLoginInfoList;
    }

    public static Response generateLastLoginStepResponse(Request request, User user, Environment activeEnvironment, Map<Integer, Map<String, String>> environments) throws IOException {
        // Generamos la sesion del usuario en la base de datos
        //TODO: Cuando se use solo lo de Oauth, hay que remover esta generación de token y quitarlo del response.
        String accessToken = SessionHandlerFactory.getInstance().createSession(request, "SecondFactor-Password");
        return generateLastLoginStepResponse(accessToken, request, user, activeEnvironment, environments);
    }

    public static Response generateLastLoginStepResponse(String accessToken, Request request, User user, Environment activeEnvironment, Map<Integer, Map<String, String>> environments) throws IOException {
        // Actualizamos los datos del hitorico de accesos del usuario y obtenemos los datos del login anterior.
        List<GeoIPLoginInfo> previousLoginInfo = updateUserLoginHistory(request);
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        response.putItem(OutParams.PREVIOUS_LOGIN_INFO, previousLoginInfo);
        response.putItem(OutParams.LANGUAGE, user.getLang());
        response.putItem(OutParams.EMAIL, user.getEmail());
        response.putItem(OutParams.ACCESS_TOKEN, accessToken);
        response.putItem(OutParams.USERID, user.getIdUser());
        response.putItem(OutParams.USER_FULLNAME, user.getFullName());
        response.putItem(OutParams.SECURITY_SEAL, user.getIdSeal());
        response.putItem(OutParams.CHANNEL, request.getChannel());
        response.putItem(OutParams.ACTIVE_ENVIRONMENT_ID, activeEnvironment.getIdEnvironment());
        response.putItem(OutParams.ACTIVE_ENVIRONMENT_NAME, activeEnvironment.getName());
        response.putItem(OutParams.ACTIVE_ENVIRONMENT_TYPE, activeEnvironment.getEnvironmentType());
        response.putItem(OutParams.ADMINISTRATION_SCHEME, activeEnvironment.getAdministrationScheme());
        response.putItem(OutParams.ENVIRONMENTS, environments);
        response.putItem(OutParams.DEFAULT_AVATAR_ID, StringUtils.replace(String.valueOf(user.getFullName().hashCode() % 12), "-", ""));
        response.putItem(OutParams.DEFAULT_ENVIRONMENT_ID, user.getIdDefaultEnvironment());


        List<String> userPermissions = AccessManagementHandlerFactory.getHandler().listPermissions(request.getIdUser(), request.getIdEnvironment());

        response.putItem(OutParams.PERMISIONS, PermissionsUtils.getPermissions(request.getIdUser(), request.getIdEnvironment()));
        response.putItem(OutParams.FORMS, PermissionsUtils.getFormList(userPermissions, request.getLang()));

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        Integer idEnvironment = request.getParam(InParams.ENVIRONMENT, Integer.class);
        idEnvironment = idEnvironment != null && idEnvironment > 0 ? idEnvironment : request.getIdEnvironment();

        if(!CalendarRestrictionsUtils.validateAccessByEnvironmentAndUser(idEnvironment,request.getIdUser(),request.getValueDate())){
            result.put(SelectEnvironmentActivity.InParams.ENVIRONMENT, "core.calendarRestrictions.NotAuthorized");
        }

        return result;
    }
}
