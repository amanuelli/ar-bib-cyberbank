/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.ReturnCodes;
import static com.technisys.omnichannel.client.activities.communications.ReadCommunicationActivity.InParams.COMMUNICATION_ID;
import static com.technisys.omnichannel.client.activities.communications.ReadCommunicationActivity.OutParams.ATTACHMENTS;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.domain.CommunicationAttachments;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.files.FilesHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;

/**
 *
 */
@DocumentedActivity("Read a communication details")
public class ReadCommunicationActivity extends Activity {

    public static final String ID = "communications.read";
    
    public interface InParams {
        @DocumentedParam(type =  Integer.class, description =  "Communication ID")
        String COMMUNICATION_ID = "communicationId";
    }

    public interface OutParams {
        @DocumentedParam(type =  String[].class, description =  "Array of communication's thread")
        String COMMUNICATION_THREAD = "communicationThread";
        @DocumentedParam(type =  CommunicationAttachments.class, description =  "Communication's attachments")
        String ATTACHMENTS = "attachments";
    }

    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int communicationId = request.getParam(COMMUNICATION_ID, Integer.class);
            Communication comm = NotificationsHandlerFactory.getHandler().read(communicationId, request.getIdUser(), Communication.TRANSPORT_DEFAULT);

            if (comm == null) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

            List<Communication> thread = NotificationsHandlerFactory.getHandler().readThread(comm.getIdThread(), request.getIdUser());

            List<CommunicationAttachments> attachments = new ArrayList<>();
            for (Communication communication : thread) {
                // marco cada comunicación de las que hay hasta ahora en el thread como procesada
                NotificationsHandlerFactory.getHandler().markRecipientAsProcessed(communication.getIdCommunication(), request.getIdUser(), Communication.TRANSPORT_DEFAULT);

                // obtengo los adjuntos de cada comunicación del thread y las guardo en un Map
                Integer[] fileList = new Integer[communication.getIdFileList().size()];
                fileList = communication.getIdFileList().toArray(fileList);

                List<File> communicationAttachments = FilesHandler.getInstance().list(request.getIdUser(), false, ArrayUtils.toPrimitive(fileList));
                attachments.add(new CommunicationAttachments(communication.getIdCommunication(), communicationAttachments));
            }

            response.putItem(OutParams.COMMUNICATION_THREAD, thread);
            response.putItem(ATTACHMENTS, attachments);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}