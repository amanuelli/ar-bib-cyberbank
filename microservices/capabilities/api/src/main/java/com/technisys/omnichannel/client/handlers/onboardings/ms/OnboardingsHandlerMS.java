/*
 *   Copyright (c) 2020 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */
package com.technisys.omnichannel.client.handlers.onboardings.ms;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.technisys.commons.msclient.exception.MsClientException;
import com.technisys.digital.onboarding.OnboardingsFactory;
import com.technisys.digital.onboarding.model.Document;
import com.technisys.digital.onboarding.model.DocumentIdentification;
import com.technisys.digital.onboarding.model.ExtraInfo;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.domain.OnboardingDocument;
import com.technisys.omnichannel.client.handlers.onboardings.IOnboardingHandler;
import com.technisys.omnichannel.client.utils.MSUtils;
import com.technisys.omnichannel.core.domain.PaginatedList;
import org.apache.commons.codec.binary.Base64;
import org.apache.poi.ss.formula.functions.T;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.net.URI;
import java.text.SimpleDateFormat;
import java.util.*;

public class OnboardingsHandlerMS implements IOnboardingHandler {

    private static final Logger log = LoggerFactory.getLogger(OnboardingsHandlerMS.class);
    private static final String ERROR_CALL_MS = "Error in call onboarding MS";

    @Override
    public long create(Onboarding onboarding) throws IOException {
        com.technisys.digital.onboarding.model.Onboarding onboardingMS = mapOnboardingToOnboardingMS(onboarding);
        try {
            ResponseEntity<Void> responseEntity = OnboardingsFactory.onboardingClient().createOnboarding(onboardingMS);
            if (responseEntity == null) {
                throw new IOException("Error obtaining the response of the create service onboarding");
            }
            URI uri = responseEntity.getHeaders().getLocation();
            if (uri == null) {
                throw new IOException("Error obtaining the response header location of the create service onboarding");
            }
            return MSUtils.parseLocationId(uri.toString());
        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
    }

    @Override
    public void updateInvitationCode(long idOnboarding, int idInvitationCode) throws IOException {
        try {
            OnboardingsFactory.onboardingClient().updateInvitationCodeOnboarding((int) idOnboarding, idInvitationCode);
        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
    }

    @Override
    public void updateStatusByInvitationCode(int idInvitationCode, String status) throws IOException {
        try {
            OnboardingsFactory.onboardingClient().updateStatusByInvitationCodeOnboarding(idInvitationCode, status);
        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
    }

    @Override
    public void updateStatus(long idOnboarding, String status) throws IOException {
        try {
            OnboardingsFactory.onboardingClient().updateStatusOnboarding((int) idOnboarding, status);
        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
    }

    @Override
    public void updateDocument(long idOnboarding, String documentType, String documentCountry, String documentNumber) throws IOException {
        try {
            DocumentIdentification documentIdentification =
                    new DocumentIdentification.DocumentIdentificationBuilder()
                            .setDocumentCountry(documentCountry)
                            .setDocumentNumber(documentNumber)
                            .setDocumentType(documentType).build();
            OnboardingsFactory.onboardingClient().updatePersonalDocumentOnboarding((int) idOnboarding, documentIdentification);
        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
    }

    @Override
    public void updateExtraInfo(long idOnboarding, String extraInfo) throws IOException {

        try {
            ExtraInfo extraInfoBody = new ExtraInfo();
            extraInfoBody.setValue(extraInfo);
            OnboardingsFactory.onboardingClient().updateExtraInfoOnboarding((int) idOnboarding, extraInfoBody);
        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }

    }

    @Override
    public void updateEmailAndMobile(long idOnboarding, String email, String mobileNumber) throws IOException {
        try {
            OnboardingsFactory.onboardingClient().updateMobileEmailOnboarding((int) idOnboarding, mobileNumber, email);
        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
    }

    @Override
    public Onboarding read(long idOnboarding) throws IOException {
        Onboarding onboarding = null;
        try {
            ResponseEntity<com.technisys.digital.onboarding.model.Onboarding> onboardingResponseEntity = OnboardingsFactory.onboardingClient().readOnboarding((int) idOnboarding);
            if (onboardingResponseEntity == null) {
                throw new IOException("Error obtaining the response of the read service onboarding");
            }
            com.technisys.digital.onboarding.model.Onboarding onboardingResponse = onboardingResponseEntity.getBody();
            if (onboardingResponse == null) {
                throw new IOException("Error obtaining the body response of the read service onboarding");
            }
            onboarding = mapMSOnboardingResponseToOnboarding(onboardingResponse);

        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
        return onboarding;
    }

    @Override
    public void createDocument(OnboardingDocument document) throws IOException {

        try {
            OnboardingsFactory.documentClient().createDocument(
                    new Document.DocumentBuilder()
                            .setDocumentType(document.getDocumentType())
                            .setOnboardingId((int) document.getIdOnboarding())
                            .setContentBase64(new String(Base64.encodeBase64(document.getContent())))
                            .build());
        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
    }

    @Override
    public OnboardingDocument readDocument(long idOnboarding, String documentType) throws IOException {
        OnboardingDocument onboardingDocument = null;
        try {
            ResponseEntity<Document> responseEntity = OnboardingsFactory.documentClient().readDocument((int) idOnboarding, documentType);
            if(responseEntity != null) {
                Document document = responseEntity.getBody();
                if (document != null) {
                    onboardingDocument = new OnboardingDocument.OnboardingDocumentBuilder()
                            .setIdDocument(document.getDocumentId())
                            .setDocumentType(document.getDocumentType())
                            .setIdOnboarding(document.getOnboardingId())
                            .setContent(Base64.decodeBase64(document.getContentBase64()))
                            .build();
                }
            }
        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
        return onboardingDocument;
    }

    @Override
    public PaginatedList listOnboardings(String documentCountry, String documentType, String documentNumber, String email, Date creationDateFrom, Date creationDateTo, String status, String mobileNumber, String type, int offset, int limit, String orderBy) throws IOException {

        PaginatedList<Onboarding> paginatedList = null;
        try {
            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Map<String, Object> queryParams = new LinkedHashMap<>();
            if(documentNumber != null) queryParams.put("documentNumber", documentNumber);
            if(documentCountry != null) queryParams.put("documentCountry", documentCountry);
            if(documentType != null) queryParams.put("documentType", documentType);
            if(email != null) queryParams.put("email", email);
            if(creationDateFrom != null) queryParams.put("creationDateFrom", formatDate.format(creationDateFrom));
            if(creationDateTo != null) queryParams.put("creationDateTo", formatDate.format(creationDateTo));
            if(status != null) queryParams.put("status", status);
            if(type != null) queryParams.put("type", type);
            if(offset > 0) queryParams.put("offset", offset);
            if(limit > 0) queryParams.put("limit", limit);
            if(orderBy != null) queryParams.put("orderBy", orderBy);

            ResponseEntity<com.technisys.digital.onboarding.model.PaginatedList> responseEntity = OnboardingsFactory.onboardingClient().listOnboardings(queryParams);
            if(responseEntity == null){
                throw new IOException("Error obtaining the response of the list service onboarding");
            }

            com.technisys.digital.onboarding.model.PaginatedList<T> paginatedListResponse = responseEntity.getBody();

            if(paginatedListResponse == null){
                throw new IOException("Error obtaining the response body of the list service onboarding");
            }

            ObjectMapper mapper = new ObjectMapper();
            List<com.technisys.digital.onboarding.model.Onboarding> onboardingMSResponseList = mapper.convertValue(paginatedListResponse.getElementList(), new TypeReference<List<com.technisys.digital.onboarding.model.Onboarding>>(){});

            paginatedList = new PaginatedList<>();
            for (com.technisys.digital.onboarding.model.Onboarding onboardingMS : onboardingMSResponseList) {
                paginatedList.addElement(mapMSOnboardingResponseToOnboarding(onboardingMS));
            }
            paginatedList.setCurrentPage(paginatedListResponse.getCurrentPage());
            paginatedList.setOrderBy(paginatedListResponse.getOrderBy());
            paginatedList.setRowsPerPage(paginatedListResponse.getRowsPerPage());
            paginatedList.setTotalPages(paginatedListResponse.getTotalPages());
            paginatedList.setTotalRows(paginatedListResponse.getTotalRows());

        } catch (MsClientException e) {
            log.error(ERROR_CALL_MS, e);
            throw new IOException(ERROR_CALL_MS);
        }
        return paginatedList;
    }

    private Onboarding mapMSOnboardingResponseToOnboarding(com.technisys.digital.onboarding.model.Onboarding onboardingResponse) {
        return new Onboarding.OnboardingBuilder()
                .setIdOnboarding(onboardingResponse.getOnboardingId())
                .setCreationDate(onboardingResponse.getCreatedOn())
                .setDocumentCountry(onboardingResponse.getDocumentCountry())
                .setDocumentNumber(onboardingResponse.getDocumentNumber())
                .setDocumentType(onboardingResponse.getDocumentType())
                .setEmail(onboardingResponse.getEmail())
                .setExtraInfo(onboardingResponse.getExtraInfo())
                .setIdInvitationCode(onboardingResponse.getInvitationCodeId())
                .setMobileNumber(onboardingResponse.getMobileNumber())
                .setModificationDate(onboardingResponse.getUpdatedOn())
                .setStatus(onboardingResponse.getStatus())
                .setType(onboardingResponse.getType())
                .build();

    }

    private com.technisys.digital.onboarding.model.Onboarding mapOnboardingToOnboardingMS(Onboarding onboarding) {
        return new com.technisys.digital.onboarding.model.Onboarding.OnboardingBuilder()
                .setOnboardingId((int) onboarding.getIdOnboarding())
                .setCreatedOn(onboarding.getCreationDate())
                .setDocumentCountry(onboarding.getDocumentCountry())
                .setDocumentNumber(onboarding.getDocumentNumber())
                .setDocumentType(onboarding.getDocumentType())
                .setEmail(onboarding.getEmail())
                .setExtraInfo(onboarding.getExtraInfo())
                .setInvitationCodeId((onboarding.getIdInvitationCode()!=null)?onboarding.getIdInvitationCode():0)
                .setMobileNumber(onboarding.getMobileNumber())
                .setUpdatedOn(onboarding.getModificationDate())
                .setStatus(onboarding.getStatus())
                .setType(onboarding.getType())
                .build();
    }


}
