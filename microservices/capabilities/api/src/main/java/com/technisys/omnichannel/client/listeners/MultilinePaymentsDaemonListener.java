package com.technisys.omnichannel.client.listeners;

import com.technisys.omnichannel.client.multilinepayments.MultilinePaymentsDaemonJob;
import com.technisys.omnichannel.core.scheduler.legacy.SchedulerHandlerLegacy;
import com.technisys.omnichannel.core.scheduler.SchedulerHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

public class MultilinePaymentsDaemonListener implements ServletContextListener {

    private static final Logger log = LoggerFactory.getLogger(MultilinePaymentsDaemonListener.class);

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        synchronized (this) {
            Scheduler scheduler = SchedulerHandlerFactory.getInstance().getScheduler();

            if (scheduler != null) {
                JobKey jobKey = new JobKey(MultilinePaymentsDaemonJob.DAEMON_JOB_NAME, SchedulerHandlerLegacy.GroupName.daemon.toString());

                Boolean registered = null;
                try {
                    JobDetail jobDetail = scheduler.getJobDetail(jobKey);

                    registered = jobDetail != null;
                } catch (SchedulerException e) {
                    log.error("Error checking if MultilinePaymentsDaemonJob is already registered", e);
                }

                if (registered != null && !registered) {
                    try {
                        log.info("Registering MultilinePaymentsDaemonJob...");

                        String interval = "2m";

                        JobDetail jobDetail = newJob(MultilinePaymentsDaemonJob.class)
                                .withIdentity(jobKey)
                                .usingJobData("interval", interval)
                                .storeDurably()
                                .build();

                        TriggerKey triggerKey = new TriggerKey(MultilinePaymentsDaemonJob.DAEMON_TRIGGER_NAME, SchedulerHandlerLegacy.GroupName.daemon.toString());

                        SimpleTrigger trigger = newTrigger()
                                .withIdentity(triggerKey)
                                .startAt(DateBuilder.futureDate(1, DateBuilder.IntervalUnit.MINUTE))
                                .withSchedule(simpleSchedule()
                                        .withIntervalInMilliseconds(CoreUtils.parseTimeAsMillis(interval))
                                        .repeatForever())
                                .build();
                        scheduler.scheduleJob(jobDetail, trigger);
                    } catch (ObjectAlreadyExistsException e) {
                        // The job was already registered by another process.
                    } catch (SchedulerException e) {
                        log.error("Error registering MultilinePaymentsDaemonJob", e);
                    }
                }
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        // Nothing to do
    }
}
