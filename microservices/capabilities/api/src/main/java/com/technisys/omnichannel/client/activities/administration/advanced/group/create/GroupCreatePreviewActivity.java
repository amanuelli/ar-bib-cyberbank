package com.technisys.omnichannel.client.activities.administration.advanced.group.create;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;

/**
 * Validates incoming form data. Use this to create a new group
 */
@DocumentedActivity("Validates incoming form data")
public class GroupCreatePreviewActivity extends Activity {

    public static final String ID = "administration.advanced.group.create.preview";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Group's description")
        String DESCRIPTION = "description";

        @DocumentedParam(type = String.class, description = "Group's name")
        String NAME = "name";

        @DocumentedParam(type = Map.class, description = "Group's created permissions")
        String PERMISSIONS = "permissions";

        @DocumentedParam(type = String.class, description = "Group's status")
        String STATUS = "status";

        @DocumentedParam(type = String[].class, description = "Group's users")
        String USERS = "users";

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return GroupCreateActivity.validateFields(request);
    }
}
