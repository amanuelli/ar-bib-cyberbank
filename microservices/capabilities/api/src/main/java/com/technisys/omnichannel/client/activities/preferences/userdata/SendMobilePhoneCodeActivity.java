/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.userdata;


import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.legacy.SMSCommunicationsDispatcher;
import com.technisys.omnichannel.core.domain.CommunicationRecipient;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.domain.ValidationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.validationcodes.ValidationCodesHandler;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 * Send a validation code to the new user mobile phone number
 */
@DocumentedActivity("Send a validation code to the new user mobile phone number")
public class SendMobilePhoneCodeActivity extends Activity {

    public static final String ID = "preferences.userData.mobilePhone.sendCode";
    
    public interface InParams {
        @DocumentedParam(type = String.class, description = "New user mobile phone number")
        String MOBILE_PHONE = "mobilePhone";
    }
    
    public interface OutParams {
        @DocumentedParam(type = String.class, description = "New user mobile phone number")
        String MOBILE_PHONE = "mobilePhone";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {

            String idUser = request.getIdUser();
            String mobilePhone = request.getParam(InParams.MOBILE_PHONE, String.class);
            
            User user = AccessManagementHandlerFactory.getHandler().getUser(idUser);

            ValidationCode validationCode = new ValidationCode();
            validationCode.setCreationDate(new Date());
            validationCode.setIdUser(user.getIdUser());
            validationCode.setType(ValidationCode.TYPE_CHANGEMOBILEPHONE);
            validationCode.setStatus(ValidationCode.STATUS_AVAILABLE);
            validationCode.setExtraInfo(mobilePhone);

            String idValidationCode = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
            ValidationCodesHandler.createValidationCode(validationCode, idValidationCode);
            
            HashMap<String, String> fillers = new HashMap<>();
            fillers.put("validationCode", idValidationCode);
            
            String body = I18nFactory.getHandler().getMessage("changeMobilePhone.sms.body", request.getLang(), fillers);
            
            NotificationsHandlerFactory.getHandler().sendSMS(mobilePhone, body, true);
            
            response.putItem(OutParams.MOBILE_PHONE, mobilePhone);
            response.setReturnCode(ReturnCodes.OK);
            
        } catch (IOException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException{
       try{
            Map<String, String> result = new HashMap<>();
         
            String mobilePhone = request.getParam(InParams.MOBILE_PHONE, String.class);
            if (StringUtils.isBlank(mobilePhone)){
                result.put(InParams.MOBILE_PHONE, "userInfo.preferences.userData.mobilePhone.empty");
            } else {
                User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
                if (StringUtils.equals(String.valueOf(user.getMobileNumber()), mobilePhone)){
                    result.put(InParams.MOBILE_PHONE, "userInfo.preferences.userData.mobilePhone.sameAsBefore");
                }
            }

            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }
    
}
