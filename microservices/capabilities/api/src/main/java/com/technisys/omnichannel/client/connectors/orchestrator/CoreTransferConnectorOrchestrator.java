/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorT;
import com.technisys.omnichannel.client.connectors.cyberbank.*;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.TransferForeignBankDetail;
import com.technisys.omnichannel.client.domain.TransferInternalDetail;
import com.technisys.omnichannel.client.domain.TransferOtherBankDetails;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Currency;
import com.technisys.omnichannel.core.domain.Product;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.List;

public class CoreTransferConnectorOrchestrator extends CoreConnectorOrchestrator {
    private static final Logger LOG = LoggerFactory.getLogger(CoreTransferConnectorOrchestrator.class);
    private static final String NO_CORE_CONNECTOR_CONFIGURED = "No core connector configured";

    public static TransferInternalDetail thirdPartyValidate(String idTransaction, Integer idEnvironment, String productGroupId, Account debitAccount, //NOSONAR
                                                            String creditAccountNumber, Amount amount, String debitReference, String creditReference) throws BackendConnectorException, IOException {

        Account creditAccount = getCreditAccountProduct(creditAccountNumber);

        return internalValidate(idTransaction, productGroupId, debitAccount, creditAccount, amount, debitReference, creditReference);
    }

    public static TransferInternalDetail thirdParty(String idTransaction, Integer idEnvironment, String productGroupId, Account debitAccount, //NOSONAR
                                                    String creditAccountNumber, Amount amount, String debitReference, String creditReference) throws BackendConnectorException, IOException {

        Account creditAccount = getCreditAccountProduct(creditAccountNumber);

        return internal(idTransaction, productGroupId, debitAccount, creditAccount, amount, debitReference, creditReference);
    }

    public static TransferInternalDetail internalValidate(String idTransaction, String productGroupId, Account debitAccount,
                                                          Account creditAccount, Amount amount, String debitReference, String creditReference) throws BackendConnectorException {
        LOG.info("Internal transfer validate");

        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorT.transferInternalValidate(idTransaction, productGroupId, debitAccount,
                        creditAccount, amount.getCurrency(), amount.getQuantity(), debitReference, creditReference);
            case CYBERBANK_CONNECTOR:
                TransferInternalDetail transferInternalDetail = new TransferInternalDetail();
                transferInternalDetail.setCreditAmount(amount);
                transferInternalDetail.setDebitAmount(amount);
                transferInternalDetail.setRate(0);
                transferInternalDetail.setReturnCode(0);
                transferInternalDetail.setReturnCodeDescrption("");
                return transferInternalDetail;
            default:
                throw new BackendConnectorException(NO_CORE_CONNECTOR_CONFIGURED);
        }
    }

    public static TransferInternalDetail internal(String idTransaction, String productGroupId, Account debitAccount,
                                                  Account creditAccount, Amount amount, String debitReference, String creditReference) throws BackendConnectorException {
        LOG.info("Internal transfer confirmation");

        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return RubiconCoreConnectorT.transferInternalSend(idTransaction, productGroupId, debitAccount,
                            creditAccount, amount.getCurrency(), amount.getQuantity(), debitReference, creditReference);
                case CYBERBANK_CONNECTOR:
                    CyberbankCoreConnectorResponse<TransferInternalDetail> response = CyberbankTransferConnector.internal(debitAccount, creditAccount, amount.getQuantity(), creditReference);
                    if (response.getCode() != 0) {
                        TransferInternalDetail transferInternalDetail = new TransferInternalDetail();
                        CyberbankCoreError coreError = response.getError();
                        List<CyberbankCoreError.Error> errors = coreError.getErrors();

                        for (CyberbankCoreError.Error error : errors) {
                            transferInternalDetail.setReturnCode(Integer.parseInt(error.getCode()));
                        }

                        return transferInternalDetail;
                    }
                    return response.getData();
                default:
                    throw new BackendConnectorException(NO_CORE_CONNECTOR_CONFIGURED);
            }
        } catch (CyberbankCoreConnectorException e) {
            throw new BackendConnectorException(e);
        }
    }

    public static TransferOtherBankDetails localBanks(String idCustomer, Account from, String destinationAccount,
                                                      String destinationCustomerName, String destinationCustomerIdentification,
                                                      Amount amount,  String reference,
                                                      String idBank) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    /*---------------------------------------------------------*/
                    /*                                                         */
                    /*  Call the CORE here to get the taxes list that should   */
                    /*  be applied the transaction                             */
                    /*                                                         */
                    /*---------------------------------------------------------*/
                    TransferOtherBankDetails details = new TransferOtherBankDetails(new Amount(amount.getCurrency(), amount.getQuantity() * 0.004));
                    return details;
                case CYBERBANK_CONNECTOR:
                    Currency currency = CyberbankGeneralServicesConnector.getCurrency(amount.getCurrency()).getData();
                    CyberbankCoreConnectorResponse<TransferOtherBankDetails> response = CyberbankTransferConnector.toLocalBanks(idCustomer, from, destinationAccount,
                            destinationCustomerName, destinationCustomerIdentification, amount, currency.getBackendId(), reference, idBank);

                    return response.getData();
                default:
                    throw new BackendConnectorException(NO_CORE_CONNECTOR_CONFIGURED);
            }
        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new BackendConnectorException(e);
        }
    }

    private static Account getCreditAccountProduct(String creditAccountNumber) throws IOException {

        if (RUBICON_CONNECTOR.equals(getCurrentCore())) {
            Account account = new Account();
            account.setNumber(creditAccountNumber);
            return account;
        }

        Product product = Administration.getInstance().readProduct(ProductUtils.generateIdProduct(creditAccountNumber + Constants.PRODUCT_CC_KEY), -1);

        if (product == null) {
            product = Administration.getInstance().readProduct(ProductUtils.generateIdProduct(creditAccountNumber + Constants.PRODUCT_CA_KEY), -1);
        }

        return new Account(product);
    }

    public static TransferForeignBankDetail foreignBanks(String idCustomer, Account from, String destinationAccount,
                                                         String destinationCustomerName, String destinationCustomerIdentification,
                                                         Amount amount,  String reference,
                                                         String idBank, String recipientBankCodeType, String recipientBankCode, String comments) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    /*---------------------------------------------------------*/
                    /*                                                         */
                    /*  Call the CORE here to get the taxes list that should   */
                    /*  be applied the transaction                             */
                    /*                                                         */
                    /*---------------------------------------------------------*/
                    TransferForeignBankDetail details = new TransferForeignBankDetail(new Amount(amount.getCurrency(), amount.getQuantity() * 0.004));
                    return details;
                case CYBERBANK_CONNECTOR:
                    Currency currency = CyberbankGeneralServicesConnector.getCurrency(amount.getCurrency()).getData();
                    CyberbankCoreConnectorResponse<TransferForeignBankDetail> response = CyberbankTransferConnector.toForeignlBank(idCustomer, from, destinationAccount,
                            destinationCustomerName, destinationCustomerIdentification, amount, currency.getBackendId() , reference, idBank, recipientBankCodeType, recipientBankCode, comments);

                    return response.getData();
                default:
                    throw new BackendConnectorException(NO_CORE_CONNECTOR_CONFIGURED);
            }
        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new BackendConnectorException(e);
        }
    }

}
