/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.handlers.banks;

import com.technisys.omnichannel.client.domain.Bank;
import com.technisys.omnichannel.core.domain.PaginatedList;
import org.apache.ibatis.session.SqlSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author sbarbosa
 */
class BanksDataAccess {

    private static BanksDataAccess instance;

    private BanksDataAccess() {
    }

    public static synchronized BanksDataAccess getInstance() {
        if (instance == null) {
            instance = new BanksDataAccess();
        }

        return instance;
    }

    public PaginatedList<Bank> listBanks(SqlSession session, String codeType, String codeNumber, String bankName, String bankCountryCode, int pageNumber, int rowsPerPage, String orderBy) throws IOException {
        PaginatedList<Bank> result = new PaginatedList<>();

        Map<String, Object> parameters = new HashMap();

        if (StringUtils.isBlank(orderBy)) { 
            orderBy = "bank_name ASC";
        }

        parameters.put("codeType", codeType);
        parameters.put("codeNumber", (StringUtils.isNotBlank(codeNumber)) ? ("%" + codeNumber.toUpperCase() + "%") : codeNumber);
        parameters.put("bankName", (StringUtils.isNotBlank(bankName)) ? ("%" + bankName.toUpperCase() + "%") : bankName);
        parameters.put("bankCountryCode", bankCountryCode);
        parameters.put("orderBy", orderBy);

        // Obtenemos el numero total de filas y calculamos la cantidad de paginas
        int count = (Integer) session.selectOne("com.technisys.omnichannel.client.handlers.banks.listBanksCount", parameters);
        int totalPages = 1;

        if (pageNumber > 0 && rowsPerPage > 0) {
            parameters.put("offset", (pageNumber - 1) * rowsPerPage);
            parameters.put("limit", rowsPerPage);

            totalPages = count / rowsPerPage;
            if (count % rowsPerPage > 0) {
                totalPages++;
            }
        }

        result.setElementList(session.<Bank>selectList("com.technisys.omnichannel.client.handlers.banks.listBanks", parameters));
        result.setTotalPages(totalPages);
        result.setTotalRows(count);
        result.setCurrentPage(pageNumber);
        result.setOrderBy(orderBy);
        result.setRowsPerPage(rowsPerPage);

        return result;
    }
    
    public Bank readBank(SqlSession session , String code, String type) throws IOException {
        Map params = new HashMap();
        params.put("codeNumber", code);
        params.put("codeType", type);
        Bank bank = (Bank)session.selectOne("com.technisys.omnichannel.client.handlers.banks.readBank",params);
        return bank;
    }
}
