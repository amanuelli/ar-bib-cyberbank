/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.domain.CreditScore;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;

public class RubiconCoreConnectorS extends RubiconCoreConnector {

    // TODO: All the method are returning a POJO without considering the return code... and throwing an exception when there is an error at network level, which is good, but also when the error is at application level (return_code != 0); we should create a Response<T> object with the return code and the data inside ;)

    // TODO: Move this logic to the core and remove the configuration variable, the name is not a good one...
    /**
     * Srv S? - Score crediticio
     *
     * Dado un documento de cliente se consulta al servicio de scoring bancario
     * si la persona puede recibir una tarjeta de crédito.
     *
     * @param documentId El número de documento
     * @return Detalle de la validación o confirmación del score crediticio
     */
    public static CreditScore creditScore(String documentId) throws BackendConnectorException {
        CreditScore detail = null;

        try {
            if (documentId.endsWith(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "creditCardRequest.score"))) {
                detail = new CreditScore(0, null, 49, documentId);
            } else {
                detail = new CreditScore(0, null, 70, documentId);
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return detail;
    }
}
