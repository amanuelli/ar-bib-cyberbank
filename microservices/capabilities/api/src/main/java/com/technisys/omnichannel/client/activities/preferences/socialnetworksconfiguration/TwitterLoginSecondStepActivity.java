/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;

import java.io.IOException;

/**
 *
 */
public class TwitterLoginSecondStepActivity extends Activity {

    public static final String ID = "preferences.socialnetworks.twitter.secondStep";

    public interface InParams {

        String OAUTH_TOKEN = "requestToken";
        String OAUTH_TOKEN_SECRET = "requestSecret";
        String OAUTH_VERIFIER = "requestVerifier";
    }

    public interface OutParams {

        String TWITTER_ID = "twitterId";
        String TWITTER_NAME = "twitterName";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            Twitter twitter = new TwitterFactory().getInstance();
            String apiKey = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "socialnetworks.twitter.apiKey");
            String apiSecret = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "socialnetworks.twitter.apiSecret");
            twitter.setOAuthConsumer(apiKey, apiSecret);
            String oauthToken = (String) request.getParam(InParams.OAUTH_TOKEN, String.class);
            String oauthTokenSecret = (String) request.getParam(InParams.OAUTH_TOKEN_SECRET, String.class);
            String oauthVerifier = (String) request.getParam(InParams.OAUTH_VERIFIER, String.class);
            RequestToken requestToken = new RequestToken(oauthToken, oauthTokenSecret);
            AccessToken accessToken = twitter.getOAuthAccessToken(requestToken, oauthVerifier);
            if (accessToken != null) {
                String twitterId = String.valueOf(accessToken.getUserId());
                String twitterName = accessToken.getScreenName();
                response.putItem(OutParams.TWITTER_ID, twitterId);
                response.putItem(OutParams.TWITTER_NAME, twitterName);
            }
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException | TwitterException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
