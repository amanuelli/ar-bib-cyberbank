/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.domain.CommunicationUser;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;

import java.io.IOException;

import static com.technisys.omnichannel.core.domain.Communication.DIRECTION_BANK_TO_CUSTOMER;

/**
 *
 */
@DocumentedActivity("List communications")
public class ListCommunicationsActivity extends Activity {

    public static final String ID = "communications.list";
    
    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "Desired page number")
        String PAGE_NUMBER = "pageNumber";
        @DocumentedParam(type = String.class, description = "Communication's directions")
        String DIRECTION = "direction";
        @DocumentedParam(type = String.class, description = "Communication's tray")
        String COMMUNICATION_TRAY = "communicationTray";
        @DocumentedParam(type = String.class, description = "Communication's type")
        String COMMUNICATION_TYPE = "communicationType";
        @DocumentedParam(type = String.class, description = "Desired communications messages order")
        String ORDER_BY = "orderBy";
        @DocumentedParam(type = String.class, description = "Filter to only unread messages")
        String ONLY_UNREAD = "onlyUnread";
    }

    public interface OutParams {
        @DocumentedParam(type = Communication[].class, description = "List of communications")
        String COMMUNICATIONS = "communications";
        @DocumentedParam(type = Integer.class, description = "Current page number")
        String CURRENT_PAGE = "currentPage";
        @DocumentedParam(type = Integer.class, description = "Total pages")
        String TOTAL_PAGES = "totalPages";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            int messagesPerPage = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "messages.communicationsPerPage", 10);
                
            String pageNumberString = request.getParam(InParams.PAGE_NUMBER, String.class);
            int pageNumber;
            if (pageNumberString == null){
                pageNumber = 1;
            } else {
                pageNumber = Integer.parseInt(pageNumberString);
            }
            String direction = request.getParam(InParams.DIRECTION, String.class);
            if (direction == null){
                direction = DIRECTION_BANK_TO_CUSTOMER;
            }
            String communicationTrayString = request.getParam(InParams.COMMUNICATION_TRAY, String.class);
            int communicationTray;
            if (communicationTrayString == null){
                communicationTray = 0;
            } else {
                communicationTray = Integer.parseInt(communicationTrayString);
            }
            String communicationType = request.getParam(InParams.COMMUNICATION_TYPE, String.class);
            String orderBy = request.getParam(InParams.ORDER_BY, String.class);
            
            Boolean onlyUnread = null;
            String onlyUnreadString = request.getParam(InParams.ONLY_UNREAD, String.class);
            if (onlyUnreadString != null) {
                onlyUnread = Boolean.valueOf(onlyUnreadString);
            }
            PaginatedList <CommunicationUser> communications = NotificationsHandlerFactory.getHandler().listUserCommunications(request.getIdUser(),
                direction, communicationTray,
                communicationType, Communication.TRANSPORT_DEFAULT,
                onlyUnread, pageNumber, messagesPerPage, orderBy);
            
            response.putItem(OutParams.COMMUNICATIONS, communications.getElementList());
            response.putItem(OutParams.CURRENT_PAGE, communications.getCurrentPage());
            response.putItem(OutParams.TOTAL_PAGES, communications.getTotalPages());
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}