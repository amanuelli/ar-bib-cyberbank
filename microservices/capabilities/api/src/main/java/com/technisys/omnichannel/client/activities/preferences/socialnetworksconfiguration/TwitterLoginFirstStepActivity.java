/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.RequestToken;

import java.io.IOException;

/**
 *
 */
public class TwitterLoginFirstStepActivity extends Activity {

    public static final String ID = "preferences.socialnetworks.twitter.firstStep";

    public interface InParams {
    }

    public interface OutParams {

        String TWITTER_AUTHENTICATION_URL = "twitterAuthenticationUrl";
        String REQUEST_TOKEN = "requestToken";
        String REQUEST_TOKEN_SECRET = "requestTokenSecret";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Twitter twitter = new TwitterFactory().getInstance();
            String apiKey = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "socialnetworks.twitter.apiKey");
            String apiSecret = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "socialnetworks.twitter.apiSecret");
            twitter.setOAuthConsumer(apiKey, apiSecret);
            RequestToken requestToken = twitter.getOAuthRequestToken(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "client.baseUrl") + "/preferences/socialnetworksconfiguration/twitter-login-second-step");
            response.putItem(OutParams.TWITTER_AUTHENTICATION_URL, requestToken.getAuthenticationURL());
            response.putItem(OutParams.REQUEST_TOKEN, requestToken.getToken());
            response.putItem(OutParams.REQUEST_TOKEN_SECRET, requestToken.getTokenSecret());
            response.setReturnCode(ReturnCodes.OK);
        } catch (TwitterException | IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
