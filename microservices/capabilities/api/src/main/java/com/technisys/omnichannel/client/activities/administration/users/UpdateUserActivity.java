/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@DocumentedActivity("Update user")
public class UpdateUserActivity extends Activity {

    public static final String ID = "administration.users.update";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "The user to update")
        String USER_ID = "userId";

        @DocumentedParam(type = Boolean.class, description = "If this user is dispatcher or not")
        String DISPATCHER = "dispatcher";
    }

    public interface OutParams {
        @DocumentedParam(type = Map.class, description = "Map with extended info content (massiveEnabled flag, signatureLevel and status)")
        String USER_EXTENDED_INFO = "userExtendedInfo";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getParam(InParams.USER_ID, String.class));
            EnvironmentUser eu = Administration.getInstance().readEnvironmentUserInfo(user.getIdUser(), request.getIdEnvironment());

            eu.setDispatcher(request.getParam(InParams.DISPATCHER, Boolean.class));
            Administration.getInstance().updateEnvironmentUserInfo(eu);

            Map<String, Object> userExtendedInfo = new HashMap<>();
            userExtendedInfo.put("idUser", user.getIdUser());
            userExtendedInfo.put("status", eu.getIdUserStatus());
            userExtendedInfo.put("signatureLevel", eu.getSignatureLevel());
            userExtendedInfo.put("massiveEnabled", !Authorization.hasPermission(user.getIdUser(), request.getIdEnvironment(), null, Constants.ADMINISTRATION_VIEW_PERMISSION));
            userExtendedInfo.put("dispatcher", eu.isDispatcher());

            response.putItem(OutParams.USER_EXTENDED_INFO, userExtendedInfo);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {

            EnvironmentUser eu = Administration.getInstance().readEnvironmentUserInfo(request.getIdUser(), request.getIdEnvironment());
            result.putAll(ValidationUtils.validateEmptyCredentials(request));

            if (eu == null) {
                result.put("NO_FIELD", "administration.users.mustSelectUser");
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
