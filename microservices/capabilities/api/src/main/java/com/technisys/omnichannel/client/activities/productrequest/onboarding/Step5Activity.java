package com.technisys.omnichannel.client.activities.productrequest.onboarding;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCreditScoreConnectorOrchestator;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.*;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.legacy.SMSCommunicationsDispatcher;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.PhoneNumber;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.onboardings.plugins.FaceRecognitionPluginFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.JsonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;

/**
 *
 */
@ExchangeActivity
public class Step5Activity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(Step5Activity.class);
    public static final String ID = "productrequest.onboarding.step5";

    private static final Map<String, String> creditCards = new HashMap<>();

    static {
        creditCards.put("1", "visa");
        creditCards.put("2", "visa");
        creditCards.put("3", "visa");
        creditCards.put("4", "master");
        creditCards.put("5", "master");
    }

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);

        try {
            String logMessage;
            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);

            ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);
            long idOnboarding = exchangeToken.getAttribute("idOnboarding");

            Onboarding onboarding = OnboardingHandlerFactory.getInstance().read(idOnboarding);
            Map<String, Object> extraInfo = JsonUtils.fromJson(onboarding.getExtraInfo(), Map.class);

            LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

            //1 - Verificar si ya es cliente
            try {
                List<ClientEnvironment> clients = CoreCustomerConnectorOrchestrator.listClients(request.getIdTransaction(), onboarding.getDocumentCountry(), onboarding.getDocumentType(), onboarding.getDocumentNumber());
                if (!clients.isEmpty()) {
                    logMessage= MessageFormat.format("Account exists ({0})", onboarding.getDocumentNumber());
                    log.error(logMessage);
                    OnboardingHandlerFactory.getInstance().updateStatus(Integer.valueOf(Long.toString(idOnboarding)), Onboarding.STATUS_ACCOUNT_EXISTS);
                    response.setReturnCode(com.technisys.omnichannel.ReturnCodes.ACCOUNT_EXISTS);
                    errors.put(String.valueOf(idOnboarding), I18nFactory.getHandler().getMessage("onboarding.finish.account", request.getLang()));
                    response.setData(errors);
                    return response;
                }
            } catch (Exception e) {
                log.info("Account does not exists (" + onboarding.getDocumentNumber() + ")", e);
            }


            //2 - Verificar score
            CreditScore score = CoreCreditScoreConnectorOrchestator.creditScore(onboarding.getDocumentNumber());
            if (score.getReturnCode() != 0) {
                logMessage= MessageFormat.format("The score service returned an error: {0} for the onboarding ({1})", score.getReturnCodeDescription(), idOnboarding);
                log.error(logMessage);
                OnboardingHandlerFactory.getInstance().updateStatus(Integer.valueOf(Long.toString(idOnboarding)), Onboarding.STATUS_CREDIT_SCORE_FAILED);
                errors.put(String.valueOf(idOnboarding), I18nFactory.getHandler().getMessage("onboarding.finish.score", request.getLang()));
            } else if (score.getScore() < ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "creditCardRequest.minScoreRequired")) {
                logMessage= MessageFormat.format("Score too low for the onboarding ({0})", idOnboarding);
                log.error(logMessage);
                OnboardingHandlerFactory.getInstance().updateStatus(Integer.valueOf(Long.toString(idOnboarding)), Onboarding.STATUS_CREDIT_SCORE_BELLOW_REQUESTED);
                errors.put(String.valueOf(idOnboarding), I18nFactory.getHandler().getMessage("onboarding.finish.score", request.getLang()));
            }

            if (errors.size() > 0) {
                response.setReturnCode(com.technisys.omnichannel.ReturnCodes.CREDIT_SCORE_REQUEST_FAILED);
                response.setData(errors);
                return response;
            }

            Address address = new Address(request.getParam(com.technisys.omnichannel.client.activities.onboarding.Step5Activity.InParams.ADDRESS, Map.class));
            JobInformation jobInformation = new JobInformation(request.getParam(com.technisys.omnichannel.client.activities.onboarding.Step5Activity.InParams.JOB_INFORMATION, Map.class));

            String addedClientId =
                    CoreCustomerConnectorOrchestrator.add(
                            request.getIdTransaction(),
                            (String) extraInfo.get("firstName"),
                            (String) extraInfo.get("lastName"),
                            onboarding.getEmail(),
                            "",
                            (PhoneNumber) extraInfo.get("mobileNumber"),
                            onboarding.getDocumentNumber(),
                            onboarding.getDocumentType(),
                            onboarding.getDocumentCountry(),
                            onboarding.getDocumentCountry(),
                            "",
                            "",
                            "PFS",
                            false,
                            (String) extraInfo.get("dateOfBirth"),
                            address,
                            jobInformation
                    );

            try {
                //Crear la tarjeta
                RubiconCoreConnectorTC.createCreditCard(request.getIdTransaction(), addedClientId, creditCards.get(extraInfo.get("creditCardId")));
            } catch (Exception e) {
                log.error("Error creating the credit card on core for the onboarding (" + idOnboarding + ")", e);
            }

            //4 - Creación del código de invitación
            InvitationCode ic = new InvitationCode();
            ic.setStatus(InvitationCode.STATUS_NOT_USED);
            ic.setCreationDate(new Date());
            ic.setLang(request.getLang());
            ic.setDocumentCountry(onboarding.getDocumentCountry());
            ic.setDocumentNumber(onboarding.getDocumentNumber());
            ic.setDocumentType(onboarding.getDocumentType());
            ic.setEmail(onboarding.getEmail());
            if (!onboarding.getMobileNumber().equals("")) {
                ic.setMobileNumber(onboarding.getMobileNumber());
            }
            ic.setLastName((String) extraInfo.get("lastName"));
            ic.setFirstName((String) extraInfo.get("firstName"));
            ic.setProductGroupId(addedClientId);
            ic.setBackendUser(true);
            ic.setAccessType("administrator");
            ic.setAdministrationScheme(Environment.ADMINISTRATION_SCHEME_SIMPLE);
            ic.setSignatureQty(1);

            String sendChannel = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "invitation.notification.transport");

            ic.setChannelSent(sendChannel);

            String invitationCodePlain = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
            InvitationCodesHandler.createInvitationCode(ic, invitationCodePlain);

            // Envío la notificación de la invitación
            sendInvitationEmail(ic, request, invitationCodePlain);

            OnboardingHandlerFactory.getInstance().updateInvitationCode(idOnboarding, ic.getId());

            OnboardingHandlerFactory.getInstance().updateStatus(idOnboarding, Onboarding.STATUS_FINISHED);

            // De aquí en más es limpieza de datos
            // Elimino las fotos que pude haber subido a la API
            try {
                FaceRecognitionPluginFactory.getFaceRecognitionPlugin().deleteImages(String.valueOf(idOnboarding));
            } catch (IOException e) {
                log.error("Error deleting image at face recognition plugin", e);
            }

            // Elimino el exchange Token
            ExchangeTokenHandler.delete(request.getParam(EXCHANGE_TOKEN, String.class));
        } catch (IOException ex) {
            log.error("IO error", ex);
        } catch (BackendConnectorException ex) {
            log.error(ex.getMessage(), ex);
        }

        return response;
    }

    public static void sendInvitationEmail(InvitationCode invitationCode, IBRequest request, String code) throws ActivityException {
        try {
            // Envío el mail con la invitacion
            HashMap<String, String> fillers = new HashMap<>();
            fillers.put("ENVIRONMENT", request.getEnvironmentName());
            String subject = I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode." + invitationCode.getChannelSent() + ".subject", request.getLang(), fillers);

            fillers.put("BASE_URL", ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, Constants.BASE_URL_KEY));

            String enrollmentUrl = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "administration.users.invite.enrollmentUrl", fillers);
            String logMessage = MessageFormat.format("URL de inicio de invitacion: {0}", enrollmentUrl);
            log.info(logMessage);

            fillers.put("ENROLLMENT_URL", enrollmentUrl);

            fillers.put("FULL_NAME", invitationCode.getFirstName() + " " + invitationCode.getLastName());
            fillers.put("INVITATION_CODE", code);
            fillers.put("EXPIRATION_DATE", invitationCode.getExpiration());
            String body = I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode." + invitationCode.getChannelSent() + ".body", request.getLang(), fillers);

            switch (invitationCode.getChannelSent()) {
                case Constants.TRANSPORT_SMS:
                    NotificationsHandlerFactory.getHandler().sendSMS(invitationCode.getMobileNumber(), body, true);
                    break;
                case Constants.TRANSPORT_MAIL:
                     NotificationsHandlerFactory.getHandler().sendEmails(subject, body, Arrays.asList(invitationCode.getEmail()), null, request.getLang(), true);
                    break;
                default:
                    throw new ActivityException(ReturnCodes.ERROR_SENDING_INVITATION, "No se encuentra bien configurado el transporte de la notificacion para invitaciones");
            }

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | MessagingException ex) {
            throw new ActivityException(ReturnCodes.ERROR_SENDING_INVITATION, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    public static Map<String, String> getCreditCards() {
        return creditCards;
    }

}
