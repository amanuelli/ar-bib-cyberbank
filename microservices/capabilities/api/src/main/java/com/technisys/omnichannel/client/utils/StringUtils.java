/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.core.i18n.I18nFactory;

/**
 *
 * @author salva
 */
public class StringUtils {

    private StringUtils(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    public static String stripHTMLTags(String htmlString) {
        String noHTMLString = null;
        if (htmlString != null) {
            // Remove HTML tags
            noHTMLString = htmlString.replaceAll("\\<.*?\\>", "");
        }

        return noHTMLString;
    }

    public static String stripHTML(String htmlString) {
        String noHTMLString = null;
        if (htmlString != null) {
            // Remove HTML tags
            noHTMLString = htmlString.replaceAll("\\<.*?\\>", "");

            // Replace carriage returns with BR entity
            noHTMLString = noHTMLString.replaceAll("\r", "");
            noHTMLString = noHTMLString.replaceAll("\n", "<br/>");

            // Replace new lines and quotes
            noHTMLString = noHTMLString.replaceAll("\'", "&#39;");
            noHTMLString = noHTMLString.replaceAll("\"", "&quot;");
        }
        return noHTMLString;
    }

    public static String prettyPrintInterval(String interval, String lang) {
        String result = interval;
        if (interval != null) {
            String value;
            if (interval.contains("s")) {
                value = interval.substring(0, interval.indexOf('s'));
                result = value + " " + ((Integer.parseInt(value) == 1) ? I18nFactory.getHandler().getMessage("global.second", lang) : I18nFactory.getHandler().getMessage("global.second.plural", lang));
            } else if (interval.contains("m")) {
                value = interval.substring(0, interval.indexOf('m'));
                result = value + " " + ((Integer.parseInt(value) == 1) ? I18nFactory.getHandler().getMessage("global.minute", lang) : I18nFactory.getHandler().getMessage("global.minute.plural", lang));
            } else if (interval.contains("h")) {
                value = interval.substring(0, interval.indexOf('h'));
                result = value + " " + ((Integer.parseInt(value) == 1) ? I18nFactory.getHandler().getMessage("global.hour", lang) : I18nFactory.getHandler().getMessage("global.hour.plural", lang));
            }
        }
        return result;
    }

    public static String normalizeInvitationCode(String invitationCode) {
        if (org.apache.commons.lang.StringUtils.isNotBlank(invitationCode) && !invitationCode.contains("-")) {
            String[] chunks = splitToChunks(invitationCode, 4, 8);
            invitationCode = "";
            for (int i = 0; i < chunks.length; i++) {
                if (i > 0) {
                    invitationCode += "-";
                }
                invitationCode += chunks[i];
            }
        }

        return invitationCode;
    }

    public static String[] splitToChunks(String s, int chunkSize, int maxArraySize) {
        String[] res = new String[maxArraySize];
        if (org.apache.commons.lang.StringUtils.isNotBlank(s)) {
            res = s.split("(?<=\\G.{" + chunkSize + "})", maxArraySize);
        }
        return res;
    }

    public static String cleanNumber(String number) {
        if (number != null) {
            number = number.replaceAll("\\D", "");
        }
        return number;
    }

}
