/*
 *  Copyright (c) 2020 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */
package com.technisys.omnichannel.client.postprocessors;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementacion de plugin de ejecucion de notificaciones de transacciones
 * @author ?
 */
public class NotificationsPostProcessor implements IPostProcessor {

    private static final Logger log = LoggerFactory.getLogger(NotificationsPostProcessor.class);

    @Override
    public void process(Request request, Response response, String newIdTransactionStatus) {
        //Debemos desarrollar una implementación a modo de ejemplo en Rubicon que envíe siempre la misma notificación,
        //indicando únicamente el nombre de la transacción ejecutada y el estado en la que quedó la misma. Y esto solo
        //cuando la transacción queda aceptada, rechazada, finalizada o fallida. Esta notificación debería ir a los
        //administradores y a todos los que firmaron la transacción.

        String logMessage;
        if(Transaction.STATUS_ACCEPTED.equals(newIdTransactionStatus) || Transaction.STATUS_CANCELLED.equals(newIdTransactionStatus)
                || Transaction.STATUS_FINISHED.equals(newIdTransactionStatus) || Transaction.STATUS_FAILED.equals(newIdTransactionStatus)
                || (Transaction.STATUS_PENDING.equals(newIdTransactionStatus) && isNotTransactionalChannel(request.getChannel())) ){
            try {
                //tipo de comunicacion
                String communicationType = null;

                switch(newIdTransactionStatus){
                    case Transaction.STATUS_ACCEPTED:
                    case Transaction.STATUS_FINISHED:
                        communicationType = Communication.TYPE_ACCEPTED;
                        break;
                    case Transaction.STATUS_PENDING:
                        communicationType = Communication.TYPE_PENDING;
                        break;
                    case Transaction.STATUS_CANCELLED:
                    case Transaction.STATUS_FAILED:
                        communicationType = Communication.TYPE_REJECTED;
                        break;
                    default:
                        logMessage= MessageFormat.format("Unexpected newIdTransactionStatus value {0}", newIdTransactionStatus);
                        log.error(logMessage);
                        break;
                }

                //bandeja
                int idTray = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "services.notification.communicationTray." + communicationType);

                //leo la transaccion para obtener las firmas
                Transaction transaction = TransactionHandlerFactory.getInstance().readDetail(request.getIdTransaction());

                Map<String, String> usersToNotify = new HashMap<>();

                //leo los usuarios del ambiente
                List<String> users = Administration.getInstance().listEnvironmentUserIds(transaction.getIdEnvironment());

                //agrego los administradores
                for(String idUser : users){
                    if(Authorization.hasPermission(idUser, transaction.getIdEnvironment(), null, Constants.ADMINISTRATION_VIEW_PERMISSION)){
                        usersToNotify.put(idUser, idUser);
                    }
                }

                //agrego los firmantes
                for(TransactionSignature signature : transaction.getSignatures()){
                    usersToNotify.put(signature.getIdUser(), signature.getIdUser());
                }

                //genero las notificaciones
                for(String idUser : usersToNotify.values()){
                    try{
                        HashMap<String, String> fillers = new HashMap<>();
                        fillers.put("TRANSACTION_NAME", I18nFactory.getHandler().getMessageForUser("activities." + transaction.getIdActivity(), idUser));
                        fillers.put("ID_TRANSACTION", transaction.getIdTransaction());
                        fillers.put("STATUS", I18nFactory.getHandler().getMessageForUser("transaction.status." + newIdTransactionStatus, idUser));

                        String subject = I18nFactory.getHandler().getMessageForUser("transactions.notifications." + newIdTransactionStatus + ".subject", idUser, fillers);

                        CommunicationConfiguration communicationConfiguration = NotificationsHandlerFactory.getHandler().readCommunicationConfiguration(idUser, communicationType);

                        Map<String, String> bodyByTransportMap = new HashMap<>();
                        String body = obtainBody(request, newIdTransactionStatus,idUser,fillers);

                        if (communicationConfiguration != null) {
                            if (communicationConfiguration.isSubscribedMail()) {
                                bodyByTransportMap.put(Communication.TRANSPORT_MAIL, body);
                            }
                            if (communicationConfiguration.isSubscribedSMS()) {
                                bodyByTransportMap.put(Communication.TRANSPORT_SMS, body);
                            }
                            if (communicationConfiguration.isSubscribedDefault()) {
                                bodyByTransportMap.put(Communication.TRANSPORT_DEFAULT, body);
                            }
                            if (communicationConfiguration.isSubscribedFacebook()) {
                                bodyByTransportMap.put(Communication.TRANSPORT_FACEBOOK, body);
                            }
                            if (communicationConfiguration.isSubscribedTwitter()) {
                                bodyByTransportMap.put(Communication.TRANSPORT_TWITTER, body);
                            }
                            if (communicationConfiguration.isSubscribedPush()) {
                                bodyByTransportMap.put(Communication.TRANSPORT_PUSH, body);
                            }
                        }

                        NotificationsHandlerFactory.getHandler().create(null, idTray, subject, bodyByTransportMap, null, communicationType, idUser, transaction.getIdEnvironment());
                    }catch(IOException ioe){
                        logMessage= MessageFormat.format("Error sending transaction notification to user {0}. {1}", idUser, ioe.getMessage());
                        log.error(logMessage);
                    }
                }
            }catch(IOException e){
                logMessage= MessageFormat.format("Error sending transaction notifications for transaction {0}. {1}", request.getIdTransaction(), e.getMessage());
                log.error(logMessage);
            }
        }
    }

    private String obtainBody(Request request, String newIdTransactionStatus, String idUser, HashMap<String, String> fillers) {

        String idMessage = "transactions.notifications." + newIdTransactionStatus.toUpperCase() + "." + request.getChannel() + ".body";
        if(I18nFactory.getHandler().isMessageExists(idMessage, request.getLang())) {
            return I18nFactory.getHandler().getMessageForUser(idMessage, idUser, fillers);
        } else {
            idMessage="transactions.notifications."+ newIdTransactionStatus.toUpperCase()+".body";
            if(I18nFactory.getHandler().isMessageExists(idMessage, request.getLang())){
                return I18nFactory.getHandler().getMessageForUser(idMessage, idUser, fillers);
            } else{
                return I18nFactory.getHandler().getMessageForUser("transactions.notifications.body", idUser, fillers);
            }
        }
    }


    private boolean isNotTransactionalChannel(String channel) {
        return ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.notTransactionalChannels", String.class).contains(channel);
    }
}
