/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;


public abstract class CoreConnectorOrchestrator { //NOSONAR

    public static final String RUBICON_CONNECTOR = "rubicon";
    public static final String CYBERBANK_CONNECTOR = "cyberbank";

    public static String getCurrentCore() {
        return ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "connectors.core.default", "rubicon");
    }

}
