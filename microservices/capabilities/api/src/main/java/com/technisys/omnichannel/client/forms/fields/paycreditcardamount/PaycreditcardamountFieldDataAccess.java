/*
 *  Copyright 2015 Manentia Software.
 *
 *  This software component is the intellectual property of 5IT S.R.L. (Manentia Software).
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  http://www.manentiasoftware.com
 */
package com.technisys.omnichannel.client.forms.fields.paycreditcardamount;

import com.technisys.omnichannel.client.domain.fields.PaycreditcardamountField;
import org.apache.ibatis.session.SqlSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ?
 */
class PaycreditcardamountFieldDataAccess {

    private static PaycreditcardamountFieldDataAccess instance;

    private PaycreditcardamountFieldDataAccess() {
    }

    /**
     * Genera instancia singleton
     *
     * @return Instancia unica de la clase
     */
    public static synchronized PaycreditcardamountFieldDataAccess getInstance() {
        if (instance == null) {
            instance = new PaycreditcardamountFieldDataAccess();
        }

        return instance;
    }

    /**
     * Crea un campo de texto en base
     *
     * @param session Sesion sql
     * @param field Campo a crear
     */
    public void createField(SqlSession session, PaycreditcardamountField field) {
        session.insert("com.technisys.omnichannel.forms.fields.paycreditcardamount.createField", field);

        //inserto las monedas
        if (field.getCurrencyList() != null) {
            Map params = new HashMap();
            params.put("idForm", field.getIdForm());
            params.put("idField", field.getIdField());
            for (String idCurrency : field.getCurrencyList()) {
                params.put("idCurrency", idCurrency);
                session.insert("com.technisys.omnichannel.forms.fields.paycreditcardamount.addCurrency", params);
            }
        }
    }

    /**
     * Lee los datos especificos de un campo
     *
     * @param session Sesion sql
     * @param idForm Identificador de formulario
     * @param idField Identificador de campo
     * @return Datos especificos del campo
     * @throws IOException Error de comunicacion
     */
    public PaycreditcardamountField readField(SqlSession session, String idForm, Integer formVersion, String idField) throws IOException {
        Map map = new HashMap();
        map.put("idField", idField);
        map.put("idForm", idForm);
        map.put("formVersion", formVersion);

        PaycreditcardamountField field = (PaycreditcardamountField) session.selectOne("com.technisys.omnichannel.forms.fields.paycreditcardamount.readField", map);
        return field;
    }

    /**
     * Borra los datos de un campo
     *
     * @param session Sesion transaccional de base de datos
     * @param idForm Identificador de formulario
     * @param idField Identificador del campo
     */
    public void deleteField(SqlSession session, String idForm, String idField) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idField", idField);
        parameters.put("idForm", idForm);
        session.insert("com.technisys.omnichannel.forms.fields.paycreditcardamount.deleteField", parameters);
    }
}
