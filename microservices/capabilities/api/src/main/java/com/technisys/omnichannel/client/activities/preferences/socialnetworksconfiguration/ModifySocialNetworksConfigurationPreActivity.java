/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration;

import com.technisys.omnichannel.client.ReturnCodes;
import static com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration.ModifySocialNetworksConfigurationPreActivity.OutParams.FACEBOOK_ID;
import static com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration.ModifySocialNetworksConfigurationPreActivity.OutParams.FACEBOOK_NAME;
import static com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration.ModifySocialNetworksConfigurationPreActivity.OutParams.TWITTER_ID;
import static com.technisys.omnichannel.client.activities.preferences.socialnetworksconfiguration.ModifySocialNetworksConfigurationPreActivity.OutParams.TWITTER_NAME;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 *
 */
public class ModifySocialNetworksConfigurationPreActivity extends Activity {

    public static final String ID = "preferences.socialnetworks.configuration.modify.pre";

    public interface InParams {
    }

    public interface OutParams {

        public String FACEBOOK_ID = "facebookId";
        public String FACEBOOK_NAME = "facebookName";
        public String TWITTER_ID = "twitterId";
        public String TWITTER_NAME = "twitterName";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }
            User usuario = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            response.putItem(FACEBOOK_ID, usuario.getFacebookId());
            response.putItem(FACEBOOK_NAME, usuario.getFacebookName());
            response.putItem(TWITTER_ID, usuario.getTwitterId());
            response.putItem(TWITTER_NAME, usuario.getTwitterName());
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
}
