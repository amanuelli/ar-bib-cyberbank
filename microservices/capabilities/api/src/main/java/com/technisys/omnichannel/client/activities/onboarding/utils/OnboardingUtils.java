package com.technisys.omnichannel.client.activities.onboarding.utils;

import com.google.api.client.util.Base64;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.client.activities.onboarding.UploadBackDocumentActivity;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.onboardings.plugins.DocumentReaderResponse;
import com.technisys.omnichannel.core.utils.JsonUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.util.*;

public class OnboardingUtils {

    private static final Logger log = LoggerFactory.getLogger(OnboardingUtils.class);

    public static final String CREDIT_CARD = "creditCard";

    private OnboardingUtils() {}

    public static Map<String, String> validateOnboardingImageLength(String documentToSave) throws ActivityException {
        Map<String, String> errors = new HashMap();

        try {
            if (StringUtils.isBlank(documentToSave)) {
                errors.put("NO_FIELD", "onboarding.wizard.document.empty");
            } else {

                int totalLength;
                try (InputStream fileStream = new ByteArrayInputStream(Base64.decodeBase64(documentToSave))) {
                    ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
                    byte[] buf = new byte[1024];
                    totalLength = 0;
                    int len;
                    while ((len = fileStream.read(buf)) > 0) {
                        byteArrayOS.write(buf, 0, len);
                        totalLength += len;
                    }
                }

                if (totalLength > ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "core.maxFileSize")) {
                    errors.put("NO_FIELD", "onboarding.wizard.document.tooBig");
                }

            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return errors;
    }

    public static void commonValidations(Map<String, Object> errors, DocumentReaderResponse drResponse, String alpha2DocumentCountry, String documentTypeCode) {
        if (StringUtils.isBlank(alpha2DocumentCountry)) {
            errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.documentCountry.notSupported");
        } else {
            if (StringUtils.isBlank(documentTypeCode)) {
                errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.documentType.notSupported");
            }
        }

        if (!drResponse.getExpiryDateVerified()) {
            errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.expirationDate.noVerified");
        }

        if (drResponse.getExpirationDate() != null && drResponse.getExpirationDate().before(new Date())) {
            errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.expirationDate.expired");
        }

        GregorianCalendar tooOld = new GregorianCalendar();
        tooOld.add(Calendar.YEAR, -150);
        if (drResponse.getDateOfBirth() == null || drResponse.getDateOfBirth().before(tooOld.getTime())) {
            errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.dateOfBirth.youAreElder");
        }

        GregorianCalendar tooYoung = new GregorianCalendar();
        tooYoung.add(Calendar.YEAR, -18);
        if (drResponse.getDateOfBirth() == null || drResponse.getDateOfBirth().after(tooYoung.getTime())) {
            errors.put(UploadBackDocumentActivity.InParams.DOCUMENT_TO_SAVE, "onboarding.uploadBackDocument.dateOfBirth.youAreAKid");
        }
    }

    public static void extractFullDataResponse(Response response, DocumentReaderResponse drResponse, Long idOnboarding, String documentTypeCode, String alpha2DocumentCountry, String creditCardId, String exchangeTokenId) throws  IOException {
        OnboardingHandlerFactory.getInstance().updateDocument(idOnboarding, documentTypeCode, alpha2DocumentCountry, drResponse.getDocumentNumber());

        Map<String, String> extraInfo = new HashMap();
        extraInfo.put("firstName", drResponse.getFirstName());
        extraInfo.put("lastName", drResponse.getLastName());
        if(StringUtils.isNotBlank(creditCardId)) {
            extraInfo.put("creditCard", creditCardId);
        }
        extraInfo.put("nationality", drResponse.getNationality());
        extraInfo.put("dateOfBirth", String.valueOf(drResponse.getDateOfBirth().getTime()));
        OnboardingHandlerFactory.getInstance().updateExtraInfo(idOnboarding, JsonUtils.toJson(extraInfo, false, false));

        response.putItem(UploadBackDocumentActivity.OutParams.DOCUMENT_COUNTRY, drResponse.getDocumentCountry());
        response.putItem(UploadBackDocumentActivity.OutParams.DOCUMENT_NUMBER, drResponse.getDocumentNumber());
        response.putItem(UploadBackDocumentActivity.OutParams.FIRST_NAME, drResponse.getFirstName());
        response.putItem(UploadBackDocumentActivity.OutParams.LAST_NAME, drResponse.getLastName());
        response.putItem(UploadBackDocumentActivity.OutParams.DATE_OF_BIRTH, drResponse.getDateOfBirth());
        response.putItem(UploadBackDocumentActivity.OutParams.EXCHANGE_TOKEN, exchangeTokenId);

        //retorna el fullDocument para mostrar en la pantalla de datos personales
        String fullDocument = drResponse.getDocumentCountry() + " - " + documentTypeCode + " - " + drResponse.getDocumentNumber();
        response.putItem(UploadBackDocumentActivity.OutParams.FULL_DOCUMENT, fullDocument);
    }

    public static DocumentReaderResponse demoSafeModeReadDocument() throws IOException {
        DocumentReaderResponse response = new DocumentReaderResponse();
        response.setDocumentCountry(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.issuingCountry"));
        response.setDocumentType(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.documentType"));
        response.setDocumentSubType(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.documentSubtype"));
        response.setDocumentNumber(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.documentNumber"));

        response.setFirstName(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.givenName"));
        response.setLastName(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.lastName"));
        response.setSex(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.sex"));
        response.setNationality(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.nationality"));
        response.setDocumentNumberVerified("true");
        response.setExpiryDateVerified("true");

        try {
            response.setDateOfBirth(DateUtils.parseDate(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.dateOfBirth"), "yyyy-MM-dd"));
        } catch (ParseException e) {
            log.warn("Error parsing birth date", e);
        }

        try {
            response.setExpirationDate(DateUtils.parseDate(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "demo.safeMode.onboarding.expirationDate"), "yyyy-MM-dd"));
        } catch (ParseException e) {
            log.warn("Error parsing expiry date", e);
        }

        response.setSuccess(true);
        return response;

    }

    public static boolean isProductRequest(Onboarding onboarding) {
        String extraInfo = onboarding.getExtraInfo();
        return StringUtils.isNotBlank(extraInfo) && extraInfo.contains(CREDIT_CARD);
    }

}
