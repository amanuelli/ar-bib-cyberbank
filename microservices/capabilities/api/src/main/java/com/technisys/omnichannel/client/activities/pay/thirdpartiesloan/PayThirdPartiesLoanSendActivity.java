/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.pay.thirdpartiesloan;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.NotificableActivity;
import com.technisys.omnichannel.client.activities.pay.loan.PayLoanSendActivity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorP;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.PayLoanDetail;
import com.technisys.omnichannel.client.utils.CurrencyUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 */
public class PayThirdPartiesLoanSendActivity extends Activity implements NotificableActivity {

    public static final String ID = "pay.loan.send";

    public interface InParams {

        String ID_DEBIT_ACCOUNT = "debitAccount";
        String LOAN_NUMBER = "loanNumber";
        String VALUE_DATE = "valueDate";
        String AMOUNT = "amount";
        String NOTIFICATION_MAILS = "notificationEmails";
        String NOTIFICATION_BODY = "notificationBody";
    }

    public interface OutParams {

        String DEBIT_AMOUNT = "debitAmount";
        String RATE = "rate";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Administration.getInstance().readEnvironment(request.getIdEnvironment());

            Map map = request.getParam(InParams.ID_DEBIT_ACCOUNT, Map.class);
            String debitAccountValue = (String)map.get("value");
            Product debitProduct = Administration.getInstance().readProduct(debitAccountValue, request.getIdEnvironment());
            Account debitAccount = new Account(debitProduct);
            
            Loan loan = new Loan();
            loan.setNumber(request.getParam(InParams.LOAN_NUMBER, String.class));
            Amount amount = request.getParam(PayLoanSendActivity.InParams.AMOUNT, Amount.class);

            PayLoanDetail detail = RubiconCoreConnectorP.payLoanSend(request.getIdTransaction(),
                    debitAccount.getClient().getIdClient(), debitAccount,
                    loan, amount.getCurrency(), amount.getQuantity(),
                    "");

            if (detail.getReturnCode() == 0) {
                if (!StringUtils.equals(detail.getDebitAmount().getCurrency(), amount.getCurrency())) {
                    //Leo la transaccion
                    Transaction transaction = TransactionHandlerFactory.getInstance().readDetail(request.getIdTransaction());
                    transaction.getData().put(OutParams.RATE, CurrencyUtils.generateExchangeRateText(detail.getRate(), detail.getDebitAmount().getCurrency(), amount.getCurrency(), request.getLang()));
                    transaction.getData().put(OutParams.DEBIT_AMOUNT, detail.getDebitAmount());
                    // Actualizo los datos de respuesta del servicio en el data
                    TransactionHandlerFactory.getInstance().updateTransactionData(transaction, false);
                }

                response.setReturnCode(ReturnCodes.OK);
            } else {
                LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

                /*
                 * 200 - cuenta debito invalida
                 * 201 - préstamo invalido
                 * 202 - cuenta debito sin saldo suficiente
                 */
                switch (detail.getReturnCode()) {
                    case 200:
                        errors.put(PayThirdPartiesLoanSendActivity.InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.thirdPartiesLoan.debitAccount.invalid", request.getLang()));
                        break;
                    case 201:
                        errors.put(PayThirdPartiesLoanSendActivity.InParams.LOAN_NUMBER, I18nFactory.getHandler().getMessage("pay.thirdPartiesLoan.loan.invalid", request.getLang()));
                        break;
                    case 202:
                        errors.put(PayThirdPartiesLoanSendActivity.InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.thirdPartiesLoan.debitAccount.insufficientBalance", request.getLang()));
                        break;
                    default:
                        String key = "pay.thirdPartiesLoan." + detail.getReturnCode();
                        String message = I18nFactory.getHandler().getMessage(key, request.getLang());
                        if (!StringUtils.isBlank(message) && !message.equals(key)) {
                            errors.put("NO_FIELD", message);
                        } else {
                            response.setReturnCode(ReturnCodes.BACKEND_SERVICE_ERROR);
                        }
                }
                response.setReturnCode(ReturnCodes.VALIDATION_ERROR);
                response.setData(errors);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (BackendConnectorException bcE) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, bcE);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            //ejecutamos validaciones del formulario y dejar aquí solo las especificas de la actividad
            Map<String, String> result = FormsHandler.getInstance().validateRequest(request);

            return result;
        } catch (SchedulerException ex) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    @Override
    public void sendNotificationMails(Transaction transaction) throws IOException, MessagingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sendNotificationMails(Request request) throws IOException, MessagingException {
        throw new UnsupportedOperationException();
    }

}
