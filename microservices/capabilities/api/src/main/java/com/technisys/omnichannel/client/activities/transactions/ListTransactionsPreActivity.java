/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.activities.transactions;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;

/**
 *
 */
public class ListTransactionsPreActivity extends Activity {
    public static final String ID = "transactions.list.pre";
    
    public interface InParams {
    }

    public interface OutParams {

        String CHANNELS = "channels";
    }
    
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.putItem(OutParams.CHANNELS, CoreUtils.getEnabledChannels());
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }
    
}
