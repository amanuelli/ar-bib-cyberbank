/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.credentials.development;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.exceptions.InvalidCredentialException;
import java.io.IOException;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author Diego Curbelo
 */
public class OTPPlugin implements CredentialPlugin {

    @Override
    public void validate(IBRequest request, Credential credential) throws DispatchingException, IOException {

        if (!"111111".equals(credential.getValue())) {
            throw new InvalidCredentialException(ReturnCodes.INVALID_OTP_CREDENTIAL, Credential.OTP_CREDENTIAL);
        }

    }

    @Override
    public boolean validateFormat(IBRequest request, String value) throws DispatchingException, IOException {
        return validateFormat("", value);
    }

    @Override
    public boolean validateFormat(String commonName, String value) throws DispatchingException, IOException {
        return StringUtils.isNotBlank(value) && value.length() == 6;
    }

    @Override
    public void changeStatus(String commonName, String newValue) throws DispatchingException, IOException {
        // No implementation required
    }

    @Override
    public void modify(IBRequest request, String currentValue, String newValue) throws DispatchingException, IOException {
        // No implementation required
    }

    @Override
    public void recover(String idUser, String newValue) throws DispatchingException, IOException {
        // No implementation required
    }

    @Override
    public void assign(String commonName, String value) throws DispatchingException, IOException {
        // No implementation required
    }

    @Override
    public void assign(IBRequest request, String commonName, String newValue) throws DispatchingException, IOException {
        // No implementation required
    }

}
