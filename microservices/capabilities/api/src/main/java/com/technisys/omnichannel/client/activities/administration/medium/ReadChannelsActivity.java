package com.technisys.omnichannel.client.activities.administration.medium;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.ListTransactionsActivity;
import com.technisys.omnichannel.client.activities.administration.common.Channels;
import com.technisys.omnichannel.client.activities.administration.users.ListUsersActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.limits.CapForUser;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DocumentedActivity("Fetch user caps")
public class ReadChannelsActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(ListTransactionsActivity.class);

    public static final String ID = "administration.medium.read.channels";
    protected static final List<String> NON_REMOVABLE_CHANNELS_FOR_ADMINISTRATORS = Collections.singletonList("frontend");

    public interface InParams {

        @DocumentedParam(type = String.class, description = "ID of the env user to fetch information")
        String ID = "id";
    }

    public interface OutParams {

        @DocumentedParam(type = User.class, description = "User information")
        String USER = "user";
        @DocumentedParam(type = List.class, description = "User caps")
        String CAPS = "caps";
        @DocumentedParam(type = String.class, description = "Master currency")
        String CURRENCY = "currency";
        @DocumentedParam(type = CapForUser.class, description = "Channel's All cap")
        String TOP_AMOUNT = "topAmount";
        @DocumentedParam(type = String[].class, description = "Enabled channels in the environment")
        String ENABLED_CHANNELS = "enabledChannels";
        @DocumentedParam(type = String[].class, description = "Enabled frequencies in the environment")
        String ENABLED_CHANNELS_FREQUENCIES = "enabledChannelsFrequencies";
        @DocumentedParam(type = String[].class, description = "Channels that cannot be removed from the user")
        String NON_REMOVABLE_CHANNELS = "nonRemovableChannels";
        @DocumentedParam(type = String.class, description = "Id environment")
        String ID_ENVIRONMENT = "idEnvironment";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String idUser = request.getParam(InParams.ID, String.class);
            int idEnvironment = request.getIdEnvironment();
            Administration administration = Administration.getInstance();
            EnvironmentUser envUser = administration.readEnvironmentUserInfo(idUser, idEnvironment);
            List<String> enabledChannels = CoreUtils.getEnabledChannels();
            List<String> nonRemovableChannels = null;

            enabledChannels.remove(CoreUtils.CHANNEL_ALL);

            if (Authorization.hasPermission(idUser, idEnvironment, null, Constants.ADMINISTRATION_VIEW_PERMISSION)) {
                nonRemovableChannels = NON_REMOVABLE_CHANNELS_FOR_ADMINISTRATORS;
            }

            response.putItem(OutParams.NON_REMOVABLE_CHANNELS, nonRemovableChannels);
            response.putItem(OutParams.ENABLED_CHANNELS, enabledChannels);
            response.putItem(OutParams.ENABLED_CHANNELS_FREQUENCIES, ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "frontend.channels.enabledFrequencies"));
            response.putItem(OutParams.USER, AccessManagementHandlerFactory.getHandler().getUser(idUser));
            response.putItem(OutParams.CAPS, Channels.listCaps(envUser, idEnvironment, request.getEnvironmentType(), idUser));
            response.putItem(OutParams.CURRENCY, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency"));
            response.putItem(OutParams.TOP_AMOUNT, Channels.getChannelAll(idUser, idEnvironment, request.getEnvironmentType()));
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String idEnvironment = request.getParam(ListUsersActivity.InParams.ID_ENVIRONMENT, String.class);

        try {
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                result.put("idEnvironment", "returnCode.COR005E");
            } else if (StringUtils.isBlank(idEnvironment) || !StringUtils.isNumeric(idEnvironment) || (Integer.parseInt(idEnvironment) < 1)) {
                result.put("idEnvironment", "administration.idEnvironment.invalid");
            } else {
                String idUser = request.getParam(InParams.ID, String.class);
                List<String> environmentUsers = Administration.getInstance()
                        .listEnvironmentUserIds(request.getIdEnvironment());

                if (environmentUsers == null) {
                    throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                }

                if (StringUtils.isEmpty(idUser)) {
                    result.put(InParams.ID, "administration.medium.modify.channels.idUser.required");
                } else {
                    User user = AccessManagementHandlerFactory.getHandler().getUser(idUser);
                    if (user == null) {
                        result.put(InParams.ID, "administration.medium.modify.channels.idUser.invalid");
                    }
                }
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (NumberFormatException ex) {
            result.put("idEnvironment", "administration.idEnvironment.invalid");
            log.error("Parameter idEnvironment provided is not a number: {}", idEnvironment);
        }
        return result;
    }

}
