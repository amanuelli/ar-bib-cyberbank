/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.plugins.DefaultProductLabeler;
import org.apache.commons.lang3.StringUtils;

/**
 * Plugin para calcular etiquetas de productos.
 *
 * Para utilizarlo se utiliza el metodo CoreUtils.getProductLabeler, dicho
 * metodo utiliza la variable de configuración "core.productLabeler" con el FQN
 *
 * NOTA: Tener en cuenta que para BO se utiliza el metodo
 * calculateBackofficeLabel..
 *
 * @author ?
 */
public class ProductLabeler extends DefaultProductLabeler {

    public final static Integer CREDIT_CARDS_SYMBOL_TO_SHOW = 2;
    public final static Integer CREDIT_CARDS_NUMBER_DIGIT_TO_SHOW = 4;
    public final static String CREDIT_CARDS_SYMBOL_LABEL = "*";

    public ProductLabeler(String lang) {
        super(lang);
    }

    @Override
    public String calculateLabel(Product product) {
        String result = "";

        if (product != null) {
            try {
                if (Constants.PRODUCT_TC_KEY.equals(product.getProductType())) {
                    String number = StringUtils.defaultString(ProductUtils.getNumber(product.getExtraInfo()));

                    result = ProductUtils.shortenCreditCardNumber(number, getLang());
                } else {
                    if (Constants.PRODUCT_CA_KEY.equals(product.getProductType())
                            || (Constants.PRODUCT_CC_KEY.equals(product.getProductType()))
                            || Constants.PRODUCT_PF_KEY.equals(product.getProductType())) {

                        String number = StringUtils.defaultString(ProductUtils.getNumber(product.getExtraInfo()));
                        String currency = ProductUtils.getCurrency(product.getExtraInfo());

                        result = StringUtils.leftPad(number, 8) + " " + I18nFactory.getHandler().getMessage("productType." + product.getProductType(), getLang()) + " " + I18nFactory.getHandler().getMessage("core.currency.label." + currency, getLang());
                    } else {
                        if (Constants.PRODUCT_PI_KEY.equals(product.getProductType())
                                || Constants.PRODUCT_PA_KEY.equals(product.getProductType())) {

                            String number = StringUtils.defaultString(ProductUtils.getNumber(product.getExtraInfo()));
                            String currency = ProductUtils.getCurrency(product.getExtraInfo());

                            result = StringUtils.leftPad(number, 15) + " " + I18nFactory.getHandler().getMessage("productType." + product.getProductType(), getLang()) + " " + I18nFactory.getHandler().getMessage("core.currency.label." + currency, getLang());
                        } else {
                            return super.calculateLabel(product);
                        }
                    }
                }
            } catch (Exception ex) {
                result = product.getIdProduct();
            }
        }
        return result;
    }

    @Override
    public String calculateShortLabel(Product product) {
        return calculateShortLabel(product, true);
    }

    @Override
    public String calculateShortLabel(Product product, boolean replaceWithAlias) {
        String result = "";

        if (product != null) {
            try {
                if (StringUtils.isNotEmpty(product.getProductAlias()) && replaceWithAlias) {
                    result = product.getProductAlias();
                } else {
                    if (Constants.PRODUCT_TC_KEY.equals(product.getProductType())) {
                        String number = StringUtils.defaultString( ProductUtils.getNumber(product.getExtraInfo()));
                        // Dummy shortLabel
                        result = "**** **** **** ";
                        result += number.substring(number.length() - 4, number.length());
                    } else {
                        if (Constants.PRODUCT_CA_KEY.equals(product.getProductType()) || (Constants.PRODUCT_CC_KEY.equals(product.getProductType()))
                                || Constants.PRODUCT_PF_KEY.equals(product.getProductType())) {

                            String number = StringUtils.defaultString( ProductUtils.getNumber(product.getExtraInfo()));
                            String currency = ProductUtils.getCurrency(product.getExtraInfo());

                            result = StringUtils.leftPad(number, 8) + " " + product.getProductType() + " "
                                    + I18nFactory.getHandler().getMessage("core.currency.label." + currency, getLang());
                        } else {
                            if (Constants.PRODUCT_PI_KEY.equals(product.getProductType()) || Constants.PRODUCT_PA_KEY.equals(product.getProductType())) {
                                String number = StringUtils.defaultString(ProductUtils.getNumber(product.getExtraInfo()));
                                String currency = ProductUtils.getCurrency(product.getExtraInfo());

                                result = StringUtils.leftPad(number, 15) + " " + product.getProductType() + " "
                                        + I18nFactory.getHandler().getMessage("core.currency.label." + currency, getLang());
                            } else {
                                return super.calculateShortLabel(product);
                            }
                        }
                    }
                }
            } catch (Exception ex) {
                result = product.getIdProduct();
            }
        }
        return result;
    }

    @Override
    public String calculateBackofficeLabel(Product product) {
        I18n i18n = I18nFactory.getHandler();
        String result = "";

        if (product != null) {
            try {
                String[] values = StringUtils.split(product.getExtraInfo(), "|");

                if (Constants.PRODUCT_TC_KEY.equals(product.getProductType())) {
                    String number = values[1];

                    result = shortenCreditCardNumber(number);
                } else {
                    if (Constants.PRODUCT_CA_KEY.equals(product.getProductType())
                            || (Constants.PRODUCT_CC_KEY.equals(product.getProductType()))
                            || Constants.PRODUCT_PF_KEY.equals(product.getProductType())) {

                        String number = values[1];
                        String currency = values[2];

                        result = StringUtils.leftPad(number, 8) + " " + i18n.getMessage("productType." + product.getProductType(), getLang()) + " " + i18n.getMessage("core.currency.label." + currency, getLang());
                    } else {
                        if (Constants.PRODUCT_PI_KEY.equals(product.getProductType())
                                || Constants.PRODUCT_PA_KEY.equals(product.getProductType())) {

                            String number = values[1];
                            String currency = values[2];

                            result = StringUtils.leftPad(number, 15) + " " + i18n.getMessage("productType." + product.getProductType(), getLang()) + " " + i18n.getMessage("core.currency.label." + currency, getLang());
                        } else {
                            result = i18n.getMessage("productType." + product.getProductType(), getLang()) + " " + product.getIdProduct();
                        }
                    }
                }
            } catch (Exception ex) {
                result = product.getIdProduct();
            }
        }
        return result;
    }

    @Override
    public String calculateFranchise(Product product){
        String result = "";

        if (product != null) {
            try {
                if (Constants.PRODUCT_TC_KEY.equals(product.getProductType())) {
                    String number = StringUtils.defaultString(ProductUtils.getNumber(product.getExtraInfo()));

                    result = creditCardFranchise(number);
                }
            } catch (Exception ex) {
                result = product.getIdProduct();
            }
        }
        return result;
    }

    /**
     * Acorta el n&uacute;mero de tarjeta generando una etiqueta que contiene
     * simplemente la marca y los &uacute;ltimos cuatro d&iacute;gitos. Por
     * ejemplo &quot;4012888888881881&quot; se transforma en &quot;VISA
     * 1881&quot;.
     *
     * Si no reconoce la marca se obtienen solo los &uacute;ltimos cuatro
     * d&iacute;gitos.
     *
     * @param number N&uacute;mero de la tarjeta
     * @param lang Lenguaje
     * @return Etiqueta corta del n&uacute;mero
     */
    private String shortenCreditCardNumber(String number) {
        StringBuilder result = new StringBuilder();

        if (org.apache.commons.lang.StringUtils.isNotEmpty(number) && number.length() >= 4) {
            result.append(creditCardFranchise(number));
            result.append(" ").append(number.substring(number.length() - 4, number.length()));
        }

        return result.toString();
    }

    private String creditCardFranchise(String number) {
        StringBuilder result = new StringBuilder();

        if (org.apache.commons.lang.StringUtils.isNotEmpty(number) && number.length() >= 4) {
            if (number.startsWith("4")) {
                result.append(I18nFactory.getHandler().getMessage("productType.VISA", getLang()));
            } else if (number.startsWith("5")) {
                result.append(I18nFactory.getHandler().getMessage("productType.MASTER", getLang()));
            } else if (number.startsWith("3")) {
                result.append(I18nFactory.getHandler().getMessage("productType.AMEX", getLang()));
            }
        }

        return result.toString();
    }
}
