/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain;

import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;

/**
 *
 * @author Divier
 */
public class DefaultValuesClient {

    // CP = Contact Phone
    // BID = Basic Individual Data
    // ED = Employment Details
    // EC = Electronics Contact
    
    public static final String ADDRESS_STREET;
    public static final String ADDRESS_STREET_NUMBER;
    public static final String ADDRESS_COUNTRY;
    public static final String ADDRESS_LOCALITY_ZIP_CODE;
    public static final String ADDRESS_PROVINCE_CODE;
    public static final String ADDRESS_COUNTRY_ZIP_CODE;
    public static final String ADDRESS_TOWN_ZONE;
    public static final String ADDRESS_FLOOR;
    public static final String ADDRESS_SECTOR;
    public static final String ADDRESS_APARTMENT;
    public static final String ADDRESS_COMMENTS;
    public static final String ED_PROFESSION;
    public static final String ED_EMPLOYMENT_SITUATION;
    public static final String EC_EMAIL_TYPE;

    private DefaultValuesClient() {}
    static {
        ADDRESS_STREET = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.street.default", "Integracion CDP-CORE");
        ADDRESS_STREET_NUMBER = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.streetNumber.default", "1234");
        ADDRESS_COUNTRY = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.country.default", "Argentina");
        ADDRESS_LOCALITY_ZIP_CODE = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.localityZipCode.default", "440820");
        ADDRESS_PROVINCE_CODE = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.provinceCode.default", "500002");
        ADDRESS_COUNTRY_ZIP_CODE = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.countryZipCode.default", "1778");
        ADDRESS_TOWN_ZONE = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.townZone.default", "RETIRO");
        ADDRESS_FLOOR = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.floor.default", "19");
        ADDRESS_SECTOR = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.sector.default", "");
        ADDRESS_APARTMENT = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.apartment.default", "1778");
        ADDRESS_COMMENTS = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.address.comments.default", "");
        EC_EMAIL_TYPE = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.electronicContacts.emailType.default", "De Contacto");
        ED_PROFESSION = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.employmentDetails.profession.default", "FUNCIONARIO");
        ED_EMPLOYMENT_SITUATION = ConfigurationFactory.getInstance()
                .getDefaultString(Configuration.PLATFORM,"cyberbank.client.employmentDetails.employmentSituation.default", "10001");
    }
}
