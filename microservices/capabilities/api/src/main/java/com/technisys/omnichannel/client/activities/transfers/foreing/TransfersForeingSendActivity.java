/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.transfers.foreing;

import com.technisys.omnichannel.activities.forms.SendFormActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreTransferConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.TransferForeignBankDetail;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.ReturnCodes.IO_ERROR;

@DocumentedActivity("Transfer Foreign Send")
public class TransfersForeingSendActivity extends Activity {

    public static final String ID = "transfers.foreign.send";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Amount")
        String AMOUNT = "amount";
        @DocumentedParam(type = String.class, description = "Debit Account")
        String DEBIT_ACCOUNT = "debitAccount";
        @DocumentedParam(type = String.class, description = "Credit Account")
        String CREDIT_ACCOUNT = "creditAccount";
        @DocumentedParam(type = String.class, description = "Recipient Account Code")
        String RECIPIENT_ACCOUNT_CODE = "recipientAccountCode";
        @DocumentedParam(type = String.class, description = "Recipient Name")
        String RECIPIENT_NAME = "recipientName";
        @DocumentedParam(type = String.class, description = "Debit Reference")
        String DEBIT_REFERENCE = "debitReference";
        @DocumentedParam(type = String.class, description = "Notification Body")
        String NOTIFICATION_BODY = "notificationBody";
        @DocumentedParam(type = String.class, description = "Intermediary Bank")
        String INTERMEDIARY_BANK = "intermediaryBank";
        @DocumentedParam(type = String.class, description = "Document")
        String DOCUMENT = "Recipient document Number";
    }


    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Transaction transaction = TransactionHandlerFactory.getInstance().read(request.getIdTransaction());
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            Amount amount = request.getParam(InParams.AMOUNT, Amount.class);
            String debitAccountIdProduct = (String) request.getParam(InParams.DEBIT_ACCOUNT, Map.class).get("value");

            Map recipientAccountCodeMap = request.getParam(InParams.RECIPIENT_ACCOUNT_CODE, Map.class);
            String recipientBankCodeType = recipientAccountCodeMap.get("type").toString();
            String recipientBankCode = recipientAccountCodeMap.get("code").toString();

            String creditAccountNumber = request.getParam(InParams.CREDIT_ACCOUNT, String.class);

            String recipientName = request.getParam(InParams.RECIPIENT_NAME, String.class);
            Map documentMap = request.getParam(InParams.DOCUMENT, Map.class);
            String recipientDocument = documentMap.get(InParams.DOCUMENT).toString();

            String reference = request.getParam(InParams.DEBIT_REFERENCE, String.class);

            List intermediaryBankList = RequestParamsUtils.getValue(request.getParameters().get(InParams.INTERMEDIARY_BANK), List.class);
            String creditBankId = (intermediaryBankList != null && !intermediaryBankList.isEmpty()) ? intermediaryBankList.get(0).toString() : "";

            String comments = request.getParam(InParams.NOTIFICATION_BODY, String.class);

            Product product = Administration.getInstance().readProduct(debitAccountIdProduct, request.getIdEnvironment());
            Account debitAccount = new Account(product);

            TransferForeignBankDetail transferForeignBankDetail = CoreTransferConnectorOrchestrator.foreignBanks(environment.getProductGroupId(), debitAccount, creditAccountNumber, recipientName, recipientDocument, amount, reference, creditBankId, recipientBankCodeType, recipientBankCode, comments);
            transaction.getData().put("tax", transferForeignBankDetail.getTax());
            TransactionHandlerFactory.getInstance().updateTransactionData(transaction);


        } catch (IOException e) {
            throw new ActivityException(IO_ERROR, e);
        } catch (BackendConnectorException e) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, e);
        }

        return new SendFormActivity().execute(request);
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return new SendFormActivity().validate(request);
    }

}