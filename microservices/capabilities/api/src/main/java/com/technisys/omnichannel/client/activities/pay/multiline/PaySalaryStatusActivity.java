/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.pay.multiline;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.handlers.multiline.MultilineHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 * @author isilveira
 */
@DocumentedActivity("Get the multiline payment status")
public class PaySalaryStatusActivity extends Activity {

    public static final String ID = "pay.multiline.salary.status";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Transaction id to get line status from")
        String TRANSACTION_ID = "transactionId";
    }

    public interface OutParams {
        @DocumentedParam(type = List.class, description = "Status of every line in the transaction")
        String TRANSACTION_LINES_STATUS = "transactionLinesStatus";
    }

    private static final List<String> ACCEPTED_STATUS = Arrays.asList(Transaction.STATUS_PENDING, Transaction.STATUS_RUNNING, Transaction.STATUS_ACCEPTED, Transaction.STATUS_PROCESSING, Transaction.STATUS_FINISHED, Transaction.STATUS_CANCELLED, Transaction.STATUS_RETURNED, Transaction.STATUS_FAILED);

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Transaction transaction = TransactionHandlerFactory.getInstance().read(request.getParam(InParams.TRANSACTION_ID, String.class));

            if (ACCEPTED_STATUS.contains(transaction.getIdTransactionStatus())) {
                response.putItem(OutParams.TRANSACTION_LINES_STATUS, MultilineHandler.getTransactionLinesStatus(transaction.getIdTransaction()));
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}