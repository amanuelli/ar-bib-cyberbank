/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain;

import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.enums.MaritalStatus;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.enums.PersonType;
import com.technisys.omnichannel.core.domain.User;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CyberbankCoreCustomer extends ClientUser {

    private String countryOfBirth;
    private Date birthDate;
    private MaritalStatus maritalStatus;
    private Double annualHouseholdIncome;
    private String landLineNumber;
    private List<Identification> identificationList = new ArrayList<>();
    private List<Address> addressList = new ArrayList<>();
    private PersonType personType;
    private String occupation;
    private String employer;
    private String employmentStartDate;
    private String mobilePhoneCountryCode;
    private String mobilePhoneAreaCode;
    private String mobilePhoneNumber;

    public String getCountryOfBirth() {
        return countryOfBirth;
    }

    public void setCountryOfBirth(String countryOfBirth) {
        this.countryOfBirth = countryOfBirth;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public MaritalStatus getMaritalStatus() {
        return maritalStatus;
    }

    public void setMaritalStatus(MaritalStatus maritalStatus) {
        this.maritalStatus = maritalStatus;
    }

    public Double getAnnualHouseholdIncome() {
        return annualHouseholdIncome;
    }

    public void setAnnualHouseholdIncome(Double annualHouseholdIncome) {
        this.annualHouseholdIncome = annualHouseholdIncome;
    }

    public String getLandLineNumber() {
        return landLineNumber;
    }

    public void setLandLineNumber(String landLineNumber) {
        this.landLineNumber = landLineNumber;
    }

    public List<Identification> getIdentificationList() {
        return identificationList;
    }

    public void setIdentificationList(List<Identification> identificationList) {
        this.identificationList = identificationList;
    }

    public List<Address> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<Address> addressList) {
        this.addressList = addressList;
    }

    public PersonType getPersonType() {
        return personType;
    }

    public void setPersonType(PersonType personType) {
        this.personType = personType;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public String getEmployer() {
        return employer;
    }

    public void setEmployer(String employer) {
        this.employer = employer;
    }

    public String getMobilePhoneCountryCode() {
        return mobilePhoneCountryCode;
    }

    public void setMobilePhoneCountryCode(String mobilePhoneCountryCode) {
        this.mobilePhoneCountryCode = mobilePhoneCountryCode;
    }

    public String getMobilePhoneAreaCode() {
        return mobilePhoneAreaCode;
    }

    public void setMobilePhoneAreaCode(String mobilePhoneAreaCode) {
        this.mobilePhoneAreaCode = mobilePhoneAreaCode;
    }

    public String getMobilePhoneNumber() {
        return mobilePhoneNumber;
    }

    public void setMobilePhoneNumber(String mobilePhoneNumber) {
        this.mobilePhoneNumber = mobilePhoneNumber;
    }

    public String getEmploymentStartDate() {
        return employmentStartDate;
    }

    public void setEmploymentStartDate(String employmentStartDate) {
        this.employmentStartDate = employmentStartDate;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if (obj instanceof User) {
            result = ((User) obj).getIdUser().equals(getIdUser());
        }

        return result;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 71 * hash + (getIdUser() != null ? getIdUser().hashCode() : 0);
        return hash;
    }
}
