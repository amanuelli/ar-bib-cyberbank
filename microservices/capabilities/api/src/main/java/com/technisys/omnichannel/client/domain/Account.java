/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.AccountStatus;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.domain.Product;

import java.util.Map;

/**
 * Representa una cuenta.
 *
 * @author norbes
 */
public class Account extends ClientProduct {

    private String currency;
    private double overdraftLine;
    private double pendingBalance;
    private double balance;
    private double countableBalance;
    private double totalCheckAmount;
    private String nationalIdentifier;
    private AccountStatus status;

    public Account() {
    }

    public Account(Product product) {
        super(product);
        this.currency = ProductUtils.getCurrency(product.getExtraInfo());
    }

    public Account(Map<String, String> fieldsMap) {
        super(fieldsMap);

        this.currency = fieldsMap.get(ProductUtils.FIELD_PRODUCT_CURRENCY);
        this.balance = RubiconCoreConnector.parseDouble(fieldsMap.get("balance"));
        this.countableBalance = RubiconCoreConnector.parseDouble(fieldsMap.get("countableBalance"));
        this.overdraftLine = RubiconCoreConnector.parseDouble(fieldsMap.get("overdraftLine"));
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getOverdraftLine() {
        return overdraftLine;
    }

    public void setOverdraftLine(double overdraftLine) {
        this.overdraftLine = overdraftLine;
    }

    public double getPendingBalance() {
        return pendingBalance;
    }

    public void setPendingBalance(double pendingBalance) {
        this.pendingBalance = pendingBalance;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public double getTotalCheckAmount() {
        return totalCheckAmount;
    }

    public void setTotalCheckAmount(double totalCheckAmount) {
        this.totalCheckAmount = totalCheckAmount;
    }

    public double getCountableBalance() {
        return countableBalance;
    }

    public void setCountableBalance(double countableBalance) {
        this.countableBalance = countableBalance;
    }
    
    public String getOwnerName() {
        return this.ownerName;
    }
    
    public void setOwnerName(String pOwnerName) {
        this.ownerName = pOwnerName;
    }

    public String getNationalIdentifier() {
        return nationalIdentifier;
    }

    public void setNationalIdentifier(String nationalIdentifier) {
        this.nationalIdentifier = nationalIdentifier;
    }

    public AccountStatus getStatus() {
        return status;
    }

    public void setStatus(AccountStatus status) {
        this.status = status;
    }
}