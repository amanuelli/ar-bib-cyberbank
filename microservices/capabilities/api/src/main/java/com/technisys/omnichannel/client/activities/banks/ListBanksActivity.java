/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.banks;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.domain.Bank;
import com.technisys.omnichannel.client.handlers.banks.BanksHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */

@DocumentedActivity("List Banks")
public class ListBanksActivity extends Activity {

    public static final String ID = "banks.list";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Code type")
        String CODE_TYPE = "type";

        @DocumentedParam(type = String.class, description = "Code number")
        String CODE_NUMBER = "code";

        @DocumentedParam(type = String.class, description = "Bank's name")
        String BANK_NAME = "name";

        @DocumentedParam(type = String.class, description = "Bank's country")
        String BANK_COUNTRY = "country";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Bank's list")
        String BANKS = "banks";

        @DocumentedParam(type = String.class, description = "More results")
        String MORE_RESULTS = "moreResults";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int maxRecords = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "forms.bankselector.maxRecords");

            String codeType = request.getParam(InParams.CODE_TYPE, String.class);
            String codeNumber = request.getParam(InParams.CODE_NUMBER, String.class);
            String bankName = request.getParam(InParams.BANK_NAME, String.class);
            String bankCountry = request.getParam(InParams.BANK_COUNTRY, String.class);

            List<Bank> banks = BanksHandler.listBanks(codeType, codeNumber, bankName, bankCountry, 1, maxRecords + 1, null).getElementList();
            boolean moreResults = banks.size() > maxRecords;

            // Remuevo el último registro
            if (moreResults) {
                banks.remove(maxRecords);
            }

            I18n i18n = I18nFactory.getHandler();
            for (Bank bank : banks) {
                bank.setBankCountryLabel(i18n.getMessage("country.name." + bank.getBankCountryCode(), request.getLang()));
            }

            response.putItem(OutParams.BANKS, banks);
            response.putItem(OutParams.MORE_RESULTS, moreResults);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String codeType = request.getParam(InParams.CODE_TYPE, String.class);
        if((codeType == null || codeType.isEmpty())){
            result.put(InParams.CODE_TYPE,"forms.transferForeign.bankselector.bankCode.invalid");
        }
        return result;
    }

}
