package com.technisys.omnichannel.client.activities.session.oauth;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.activities.session.legacy.RegisterUserDeviceActivity;
import com.technisys.omnichannel.core.Request;

import java.util.Map;

/**
 * Register user device on login flow (Oauth case)
 */
@DocumentedActivity("Register a user device")
public class OauthRegisterUserDeviceActivity extends RegisterUserDeviceActivity {

    public static final String ID = "session.login.oauth.registerUserDevice";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Token required to call the next step on this process")
        String EXCHANGE_TOKEN = "_exchangeToken";
        @DocumentedParam(type = String.class, description = "User device Id")
        String ID_DEVICE = "idDevice";
        @DocumentedParam(type = Map.class, description = "Device extra info")
        String EXTRA_INFO = "extraInfo";
        @DocumentedParam(type = String.class, description = "Device push token")
        String PUSH_TOKEN = "pushToken";
    }

    @Override
    protected String retrieveIdUser(Request request) {
        return request.getIdUser();
    }

}
