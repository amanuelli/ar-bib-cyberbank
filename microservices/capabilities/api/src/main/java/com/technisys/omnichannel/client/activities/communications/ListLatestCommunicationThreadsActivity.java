/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.domain.CommunicationUser;
import com.technisys.omnichannel.core.domain.PairIdThreadIdCommunication;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author amauro
 */
public class ListLatestCommunicationThreadsActivity extends Activity {

    public static final String ID = "communications.latestCommunicationThreads";

    public interface InParams {

    }

    public interface OutParams {
        String UNREAD_COMMUNICATIONS = "unreadCommunications";
        String UNREAD_COMMUNICATIONS_IDS = "unreadCommunicationsIds";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            List<CommunicationUser> latestUnreadCommunicationThreads = NotificationsHandlerFactory.getHandler().listLatestCommunications(request.getIdUser(), Communication.TRANSPORT_DEFAULT, true, ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "communications.latestQty"));
            List<Integer> idThreads = new ArrayList<>();
            latestUnreadCommunicationThreads.forEach(x -> idThreads.add(x.getIdThread()));

            List<PairIdThreadIdCommunication> pairIds = new ArrayList<>();
            latestUnreadCommunicationThreads.forEach(x -> pairIds.add(new PairIdThreadIdCommunication(x.getIdThread(), x.getIdCommunication())));

            response.putItem(OutParams.UNREAD_COMMUNICATIONS, latestUnreadCommunicationThreads.size());
            response.putItem(OutParams.UNREAD_COMMUNICATIONS_IDS, pairIds);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}