/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.groups;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ExportUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.codec.binary.Base64;
import org.xml.sax.SAXException;

@DocumentedActivity("Download file with list of groups")
public class ExportGroupsActivity extends Activity {

    public static final String ID = "administration.groups.export";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "File download format (xls or pdf)")
        String FORMAT = "format";
        @DocumentedParam(type = String.class, description = "List order")
        String ORDER_BY = "orderBy";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Downloaded file name")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = String.class, description = "File content (Base64 format)")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (!Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            I18n i18n = I18nFactory.getHandler();
            String orderBy = request.getParam(InParams.ORDER_BY, String.class);
            String format = request.getParam(InParams.FORMAT, String.class);

            if (StringUtils.isBlank(orderBy)) {
                orderBy = "name ASC";
            } else {
                String[] split = orderBy.split(" ");
                if (split.length <= 1) {
                    orderBy += " ASC";
                }
            }

            List<Group> groups = AccessManagementHandlerFactory.getHandler().getGroups(null, null, request.getIdEnvironment(), -1, -1, orderBy).getElementList();

            List<Map<String, Object>> groupsToExport = new ArrayList<>();
            for (Group group : groups) {
                Map<String, Object> map = new HashMap<>();
                map.put("name", group.getName());
                map.put("description", group.getDescription());
                map.put("usersInGroup", AccessManagementHandlerFactory.getHandler().getUsersIdByGroup(group.getIdGroup()).size());
                map.put("status", group.isBlocked() ? i18n.getMessage("core.group.status.locked", request.getLang()) : i18n.getMessage("core.group.status.active", request.getLang()));
                groupsToExport.add(map);
            }

            // Export columns
            String[] columns = {
                    i18n.getMessage("administration.groups.export.name", request.getLang()),
                    i18n.getMessage("administration.groups.export.description", request.getLang()),
                    i18n.getMessage("administration.groups.export.usersInGroup", request.getLang()),
                    i18n.getMessage("administration.groups.export.status", request.getLang())
            };

            // Export attributes
            String[] attributes = {"name", "description", "usersInGroup", "status"};

            byte[] content;
            String filename = i18n.getMessage("administration.groups.export.filename", request.getLang());


            content = ExportUtils.toExcelFormat("/templates/administration_xls_template.xls", columns, groupsToExport, attributes, i18n.getMessage("administration.groups.export.title", request.getLang()), null);
            filename += ".xls";
            response.putItem(OutParams.CONTENT, Base64.encodeBase64(content));


            response.putItem(OutParams.FILE_NAME, filename);
            response.setReturnCode(ReturnCodes.OK);

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        String orderBy = request.getParam(InParams.ORDER_BY, String.class);
        String format = request.getParam(InParams.FORMAT, String.class);

        if (StringUtils.isBlank(format)) {
            result.put(InParams.FORMAT, "administration.groups.export.format.blank");
            return result;
        }

        if (!"xls".equalsIgnoreCase(format) && !"pdf".equalsIgnoreCase(format)) {
            result.put(InParams.FORMAT, "administration.groups.export.format.invalid");
            return result;
        }

        if(!StringUtils.isBlank(orderBy)) {
            ArrayList<String> auxGroups = new ArrayList();
            auxGroups.add("idGroup");
            auxGroups.add("name");
            auxGroups.add("description");
            auxGroups.add("blocked");

            String[] split = orderBy.split(" ");
            boolean isValidFilter = true;
            if (split.length > 1 && !split[1].equalsIgnoreCase("ASC") && !split[1].equalsIgnoreCase("DESC")) {
                result.put(InParams.FORMAT, "administration.groups.export.order.invalid");
                isValidFilter = false;
            }
            if (isValidFilter) {
                isValidFilter = auxGroups.stream().anyMatch(t -> t.equalsIgnoreCase(split[0]));
            }

            if (!isValidFilter) {
                result.put(InParams.FORMAT, "administration.groups.export.order.invalid");
            }
        }

        return result;
    }

}
