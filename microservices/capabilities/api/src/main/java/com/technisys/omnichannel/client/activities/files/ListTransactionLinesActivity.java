package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.client.domain.TransactionLinesTotals;
import com.technisys.omnichannel.client.handlers.multiline.MultilineHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
@DocumentedActivity("List transaction lines")
public class ListTransactionLinesActivity extends Activity {
    public static final String ID = "files.list.transaction.lines";
    public static final String SAME_BANK = "sameBank";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Transaction id to fetch lines")
        String ID_TRANSACTION = "id";
        @DocumentedParam(type = Integer.class, description = "Page number to fetch")
        String PAGE_NUMBER = "pageNumber";
        @DocumentedParam(type = String.class, description = "Status to filter")
        String STATUS = "status";
        @DocumentedParam(type = String.class, description = "Account name to filter")
        String ACCOUNT_NAME = "accountName";
        @DocumentedParam(type = String.class, description = "Credit account to filter")
        String CREDIT_ACCOUNT = "creditAccount";
        @DocumentedParam(type = Double.class, description = "Min amount to filter")
        String MIN_AMOUNT = "minAmount";
        @DocumentedParam(type = Double.class, description = "Max amount to filter")
        String MAX_AMOUNT = "maxAmount";
        @DocumentedParam(type = String.class, description = "Bank to filter")
        String ACCOUNT_BANK = "accountBank";
        
    }

    public interface OutParams {
        @DocumentedParam(type = List.class, description = "List of <TransactionLine> lines")
        String LIST = "list";
        @DocumentedParam(type = Boolean.class, description = "Used to know if there is more data to fetch")
        String IS_LAST_PAGE = "isLastPage";
        @DocumentedParam(type = Integer.class, description = "Total amount of lines in the payment")
        String TOTAL_LINES = "totalLines";
        @DocumentedParam(type = Integer.class, description = "Total amount in the payment")
        String TOTAL_AMOUNT_QUANTITY = "totalAmountQuantity";
        @DocumentedParam(type = Integer.class, description = "Currency of the amount in the payment")
        String TOTAL_AMOUNT_CURRENCY = "totalAmountCurrency";
    }

    public static List<TransactionLine> list(Request request, String idTransaction, Integer pageNumber) throws ActivityException {
        String status = request.getParam(InParams.STATUS, String.class);
        String accountName = request.getParam(InParams.ACCOUNT_NAME, String.class);
        String creditAccount = request.getParam(InParams.CREDIT_ACCOUNT, String.class);
        Double minAmount = request.getParam(InParams.MIN_AMOUNT, Double.class);
        Double maxAmount = request.getParam(InParams.MAX_AMOUNT, Double.class);
        String accountBank = request.getParam(InParams.ACCOUNT_BANK, String.class);
        Map<String, Object> params = getListTransactionsLinesParams(status, accountName, creditAccount, minAmount, maxAmount, accountBank, idTransaction, pageNumber);
        try {
            return MultilineHandler.listTransactionLines(params);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }
    
    public static Map<String, Object> getListTransactionsLinesParams(String status, String accountName, String creditAccount, Double minAmount, Double maxAmount, String accountBank, String idTransaction, Integer pageNumber){
        Map<String, Object> params = new HashMap<>();
        if (pageNumber != null) {
            Integer linesPerPage = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "transactions.linesPerPage", 10);
            Integer offset = linesPerPage * (pageNumber - 1);
            params.put("linesPerPage", linesPerPage + 1);
            params.put("offset", offset);
        }
        
        //Get massive.payments.allowed.banks.sameBank config to know if we are filtering by techbank (same bank)
        if(!StringUtils.isBlank(accountBank)){
            String sameBank = ConfigurationFactory.getInstance().getStringSafe(Configuration.PLATFORM, "massive.payments.allowed.banks.sameBank");
            if(!StringUtils.isBlank(sameBank) && StringUtils.equals(sameBank.toLowerCase(), accountBank.toLowerCase())) 
                accountBank = SAME_BANK;
        }
        
        params.put("idTransaction", idTransaction);
        params.put("status", status);
        params.put("accountName", StringUtils.isBlank(accountName) ? null : "%" + accountName.trim() + "%");
        params.put("creditAccount", StringUtils.isBlank(creditAccount) ? null : creditAccount.trim());
        params.put("minAmount", minAmount);
        params.put("maxAmount", maxAmount);
        params.put("accountBank", StringUtils.isBlank(accountBank) ? null: accountBank);
        
        return params;
    }
    
    private TransactionLinesTotals getTotals(String idTransaction) throws ActivityException {
        try {
            return MultilineHandler.getTransactionLinesTotals(idTransaction);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        Integer pageNumber = request.getParam(InParams.PAGE_NUMBER, Integer.class);
        List<TransactionLine> lines = list(request, request.getParam(InParams.ID_TRANSACTION, String.class), pageNumber == null ? 1 : pageNumber);
        TransactionLinesTotals totals = getTotals(request.getParam(InParams.ID_TRANSACTION, String.class));
        Integer linesPerPage = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "transactions.linesPerPage", 10);
        lines.forEach((line) -> {
            TransactionLine.ErrorCode errorCode = line.getErrorCode();
            if (errorCode != null) {
                line.setErrorCodeMsg(errorCode.toString(request.getLang()));
            }
        });
        if (lines.size() > linesPerPage) {
            lines.remove(lines.size() - 1);
            response.putItem(OutParams.IS_LAST_PAGE, false);
        } else {
            response.putItem(OutParams.IS_LAST_PAGE, true);
        }
        response.putItem(OutParams.LIST, lines);
        response.putItem(OutParams.TOTAL_LINES, totals != null ? totals.getTotalLines() : 0);
        response.putItem(OutParams.TOTAL_AMOUNT_QUANTITY, totals != null ? totals.getTotalAmountQuantity() : 0);
        response.putItem(OutParams.TOTAL_AMOUNT_CURRENCY, totals != null ? totals.getTotalAmountCurrency() : 0);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String idTransaction = request.getParam(InParams.ID_TRANSACTION, String.class);
            if (StringUtils.isEmpty(idTransaction)){
                result.put(InParams.ID_TRANSACTION, "forms.transaction.ticket.id.required");
            }

            Transaction transaction = TransactionHandlerFactory.getInstance().readDetail(idTransaction);
            if (transaction == null) {
                result.put(InParams.ID_TRANSACTION, "forms.transaction.ticket.id.invalid");
            }
            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }
}
