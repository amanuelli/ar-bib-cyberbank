/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.products;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;
/**
 * @author Jhossept Garay
 */
@DocumentedActivity("Set all of the paperless value to the user products")
public class SetAllPaperlessValueActivity extends Activity {

    public static final String ID = "core.product.setAllPaperlessValue";

    public interface InParams {
        @DocumentedParam(type = Boolean.class, description = "Specify whether the products will accept paperless or not")
        String PAPERLESS = "paperless";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            Boolean paperless = request.getParam(InParams.PAPERLESS, Boolean.class);

            List<String> idPermissions = new ArrayList<>();
            idPermissions.add(PRODUCT_READ_PERMISSION);

            List<String> productTypes = Arrays.asList(Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY, Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY, Constants.PRODUCT_TC_KEY);
            List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);
            if(environmentProducts != null && !environmentProducts.isEmpty()){
                List<String> productsID = environmentProducts.stream()
                        .map(Product::getIdProduct)
                        .collect(Collectors.toList());
                Administration.getInstance().setAllPaperlessValue(productsID, request.getIdEnvironment(), paperless);
            }

            response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        //Paperless parameter
        if(request.getParam(InParams.PAPERLESS, Boolean.class) == null){
            result.put(InParams.PAPERLESS, "core.product.setPaperless.paperless.empty");
        }

        return result;
    }
}