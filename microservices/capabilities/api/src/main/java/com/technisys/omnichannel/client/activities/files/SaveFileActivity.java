/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import org.apache.commons.codec.binary.Base64;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.FileTooBigException;
import com.technisys.omnichannel.core.files.FilesHandler;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;

@DocumentedActivity("Save file")
public class SaveFileActivity extends Activity {

    public static final String ID = "files.save";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "File to save (IMG on base64)")
        String FILE_TO_SAVE = "fileToSave";
        @DocumentedParam(type = String.class, description = "File name")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = String.class, description = "File description", optional = true)
        String FILE_DSCRIPTION = "fileDescription";
        @DocumentedParam(type = String.class, description = "File activity's id")
        String FILE_ID_ACTIVITY = "fileIdActivity";        
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "File object's details")
        String FILE = "file";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String fileToSave = request.getParam(InParams.FILE_TO_SAVE, String.class);
            String fileName = request.getParam(InParams.FILE_NAME, String.class);
            String fileDescription = request.getParam(InParams.FILE_DSCRIPTION, String.class);
            String fileIdActivity = request.getParam(InParams.FILE_ID_ACTIVITY, String.class);

            if (StringUtils.isBlank(fileDescription)) {
                fileDescription = "Communication attachment";
            }

            byte[] decodedBytes = Base64.decodeBase64(fileToSave);

            File file = FilesHandler.getInstance().create(request.getIdUser(), false, new ByteArrayInputStream(decodedBytes), fileName, fileDescription, fileIdActivity);

            response.putItem(OutParams.FILE, file);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (FileTooBigException ex) {
            throw new ActivityException(ReturnCodes.UPLOAD_FILE_TOO_BIG, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        if(StringUtils.isBlank(request.getParam(InParams.FILE_NAME, String.class))) {
            result.put(InParams.FILE_NAME, "global.activity.required.param");
        }
        if(StringUtils.isBlank(request.getParam(InParams.FILE_ID_ACTIVITY, String.class))) {
            result.put(InParams.FILE_ID_ACTIVITY, "global.activity.required.param");
        }
        if(StringUtils.isBlank(request.getParam(InParams.FILE_TO_SAVE, String.class))) {
            result.put(InParams.FILE_TO_SAVE, "global.activity.required.param");
        }
        return result;
    }

}
