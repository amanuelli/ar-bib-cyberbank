/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreProductConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.handlers.environment.EnvironmentHandler;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.environmentdata.EnvironmentDataHandler;
import com.technisys.omnichannel.core.limits.CapForEnvironment;
import com.technisys.omnichannel.core.limits.CapForUser;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DBUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 * @author
 */
public class SynchronizationUtils {

    private static final Logger log = LoggerFactory.getLogger(SynchronizationUtils.class);

    private SynchronizationUtils() {
        throw new IllegalStateException("Utility class");
    }

    /**
     * Agrega o quita ambientes, no sincroniza productos
     *
     * @param idTransaction
     * @param idUser
     * @throws IOException
     * @throws BackendConnectorException
     */
    public static void syncEnvironments(String idTransaction, String idUser) throws IOException, BackendConnectorException {

        // Solo sincronizo nuevos ambientes a clientes confiables (Existian en Backend al momento de la invitación)
        User user = AccessManagementHandlerFactory.getHandler().getUser(idUser);
        if (user.isTrustfull()) {
            //agregamos los ambientes nuevos, no quitamos ambientes porque se soporta invitación a no clientes

            List<ClientEnvironment> clients = CoreCustomerConnectorOrchestrator.listClients(idTransaction, user.getDocumentCountry(), user.getDocumentType(), user.getDocumentNumber());
            List<Environment> environments = Administration.getInstance().listEnvironments(idUser);

            ClientEnvironment aux;
            for (Environment env : environments) {
                Environment environment = Administration.getInstance().readEnvironment(env.getIdEnvironment());
                if (environment != null) {
                    aux = new ClientEnvironment();
                    aux.setProductGroupId(environment.getProductGroupId());
                    aux.setIdEnvironment(0); // Los id de ambientes del servicio vienen en 0

                    clients.remove(aux);
                }
            }

            //Agregamos los que no se encontraran en la iteracion anterior
            try (SqlSession sqlSession = DBUtils.getInstance().openWriteSession()) {
                List<CapForEnvironment> forUsers = new ArrayList<>();

                for (ClientEnvironment clientEnv : clients) {
                    //TODO: el agregarlo como administrador o no puede depender de otros factores en cada cliente
                    EnvironmentHandler.getInstance().addClientToEnvironment(sqlSession, idTransaction, user, clientEnv.getProductGroupId(), null, null, Constants.INVITATION_ROLE_ADMINISTRATOR, null, null);

                    // Creamos los caps por defecto para el usuario en el ambiente
                    String defaultCapFrequency = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "core.capFrequency.list").get(0);
                    for (String channel : CoreUtils.getEnabledChannels()) {
                        forUsers.add(new CapForUser(clientEnv.getIdEnvironment(), channel, user.getIdUser(), defaultCapFrequency));
                    }
                }

                LimitsHandlerFactory.getHandler().createCaps(sqlSession, forUsers);

                sqlSession.commit();
            }
        }
    }

    private static void syncEnvironmentAdditionalData(SqlSession sqlSession, String idTransaction, String productGroupId, int idEnvironment) throws IOException, BackendConnectorException {
        ClientEnvironment client = RubiconCoreConnectorC.readClient(idTransaction, productGroupId);
        String envExtendedData = null;
        //se completan datos del ambiente del core
        if (client != null) {
            envExtendedData = client.getJsonAddionalData();
        }
        EnvironmentData envData = new EnvironmentData();
        envData.setIdEnvironment(idEnvironment);
        envData.setData(envExtendedData);
        EnvironmentDataHandler.createOrUpdateEnvironmentData(sqlSession, envData);
    }

    public static void syncEnvironmentProducts(String idTransaction, Environment environment, Date lastSynchronization) throws IOException, BackendConnectorException {

        if (!environment.getClients().isEmpty()) {
            GregorianCalendar now = new GregorianCalendar();
            now.add(Calendar.MILLISECOND, -1 * ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "backend.synchronization.interval").intValue());
            if (lastSynchronization == null || now.getTime().after(lastSynchronization)) {
                List<String> typeList = Arrays.asList(new String[]{Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY, Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PF_KEY, Constants.PRODUCT_PI_KEY, Constants.PRODUCT_TC_KEY});
                List<Product> products = CoreProductConnectorOrchestrator.list(idTransaction, environment.getClients(), typeList);

                try (SqlSession sqlSession = DBUtils.getInstance().openWriteSession()) {
                    Administration.getInstance().modifyEnvironmentProducts(sqlSession, environment.getIdEnvironment(), products);

                    //Para los grupos economicos no estamos sincronizando el adittionalData pero se podria.
                    //Por eso solo lo sincroniza si exsite 1 solo cliente
                    if (environment.getClients().size() == 1) {
                        syncEnvironmentAdditionalData(sqlSession, idTransaction, environment.getClients().get(0).getIdClient(), environment.getIdEnvironment());
                    }
                    Administration.getInstance().resetEnvironmentLastSynchronization(sqlSession, environment.getIdEnvironment());
                    sqlSession.commit();
                }
            }
        }
    }


    public static void syncUserData(String idTransaction, String idUser, String productGroupId) throws IOException, BackendConnectorException {

        if (idUser != null) {
            User user = AccessManagementHandlerFactory.getHandler().getUser(idUser);
            Date lastSynchronization = user.getLastSynchronization();
            GregorianCalendar now = new GregorianCalendar();
            now.add(Calendar.MILLISECOND, -1 * ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "backend.synchronization.interval").intValue());
            if (lastSynchronization == null || now.getTime().after(lastSynchronization)) {
                ClientUser backendClient = CoreCustomerConnectorOrchestrator.read(idTransaction, user.getDocumentCountry(), user.getDocumentType(), user.getDocumentNumber(), productGroupId);
                String userExtendedData = null;
                if (backendClient != null) {
                    userExtendedData = backendClient.getJsonAdditionalData();
                    UserData userData = new UserData();
                    userData.setIdUser(idUser);
                    userData.setData(userExtendedData);

                    try (SqlSession sqlSession = DBUtils.getInstance().openWriteSession()) {
                        AccessManagementHandlerFactory.getHandler().createOrUpdateUserData(sqlSession, userData);
                        AccessManagementHandlerFactory.getHandler().resetUserLastSynchronization(sqlSession, idUser);
                        sqlSession.commit();
                    }
                }
            }
        }
    }

    public static List<String> loadFrequentActions(String idUser, int idEnvironment, String environmentType) throws IOException {

        int limit = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "frequentActions.limit");
        int maxToShow = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "frequentActions.maxToShow");

        List<String> frequentForms = new ArrayList<>();

        boolean userHasPermission;

        // obtiene la lista de acciones frecuentes
        List<String> frequentActions = TransactionHandlerFactory.getInstance().listFrequentActions(idUser, idEnvironment, limit);
        for (String idForm : frequentActions) {
            if (frequentForms.size() < maxToShow) {
                userHasPermission = Authorization.hasPermissionOverForm(idUser, idEnvironment, idForm);
                if (userHasPermission) {
                    frequentForms.add(idForm);
                }
            }
        }
        // si no llegue al limite, completo con transacciones del usuario
        if (frequentForms.size() < maxToShow) {
            List<String> defaultFrequentActionsList = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM, "frequentActions.defaults." + environmentType, String.class);

            int actual = 0;
            for (String defaultFrequentForm : defaultFrequentActionsList) {
                try {
                    //Obtengo el original del formulario dado que la lista que viene del core es de esos ids
                    if (!frequentForms.contains(defaultFrequentForm)) {
                        userHasPermission = Authorization.hasPermissionOverForm(idUser, idEnvironment, defaultFrequentForm);

                        if (userHasPermission) {
                            frequentForms.add(defaultFrequentForm);
                            actual++;
                        }
                        if (actual >= maxToShow) {
                            break;
                        }
                    }
                } catch (Exception e) {
                    log.warn("Error reading frequent actions", e);
                }
            }
        }

        //Debo quedarme con el ultimo de cada form
        List<String> result = new ArrayList();
        for (String idForm : frequentForms) {
            result.add(idForm);
        }

        return result;
    }
}
