/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.notes;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.handlers.notes.NotesHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 *
 */
public class DeleteStatementNoteActivity extends Activity {

    public static final String ID = "notes.deleteStatementNote";

    public interface InParams {

        String ID_PRODUCT = "idProduct";
        String ID_STATEMENT = "idStatement";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            int idSatatement = request.getParam(InParams.ID_STATEMENT, Integer.class);
            String idProduct = request.getParam(InParams.ID_PRODUCT, String.class);

            NotesHandler.deleteNote(idSatatement, idProduct);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
