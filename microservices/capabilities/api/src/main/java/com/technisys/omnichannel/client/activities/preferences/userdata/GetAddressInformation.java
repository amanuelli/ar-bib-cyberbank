/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.userdata;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Address;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.domain.Country;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;


/**
 * @author Marcelo Bruno
 */

/**
 * Activity to get backend client address information
 */
@DocumentedActivity("Get user address information")
public class GetAddressInformation extends Activity {

    public static final String ID = "preferences.userData.getUserAddressInformation";

    public interface OutParams {

        @DocumentedParam(type = List.class, description = "Country list")
        String COUNTRY_LIST = "countries";
        @DocumentedParam(type = Address.class, description = "Address")
        String ADDRESS = "address";
        @DocumentedParam(type = Address.class, description = "Mailing Address")
        String MAILING_ADDRESS = "mailingAddress";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            List<Country> countryCodeList = CountryCodesHandler.getCountryList(request.getLang());

            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            ClientUser backendClient = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), user.getDocumentCountry(), user.getDocumentType(), user.getDocumentNumber(), environment.getProductGroupId());

            response.putItem(OutParams.COUNTRY_LIST, countryCodeList);

            JSONObject jsonObject;
            if (backendClient != null) {
                    if(StringUtils.isNotBlank(backendClient.getAddress())) {
                        jsonObject = new JSONObject(backendClient.getAddress());
                        response.putItem(OutParams.ADDRESS, new Address(jsonObject));
                    } else {
                        throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                    }

                    Address mailingAddress = null;
                    if(StringUtils.isNotBlank(backendClient.getMailingAddress())) {
                        jsonObject = new JSONObject(backendClient.getMailingAddress());
                        mailingAddress = new Address(jsonObject);
                    }
                    response.putItem(OutParams.MAILING_ADDRESS, mailingAddress);

            } else {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

            response.setReturnCode(ReturnCodes.OK);

        } catch (IOException | BackendConnectorException | JSONException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

}
