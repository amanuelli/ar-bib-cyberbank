/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications.pushnotifications;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * Unsubscribe a device to receive push notifications
 */
@DocumentedActivity("Unsubscribe to push notifications")

public class UnSubscribePushNotificationsActivity extends Activity {

    public static final String ID = "communications.pushnotifications.unsubscribe";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Id device to unsubscribe")
        String ID_DEVICE = "idDevice";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            String idDevice = request.getParam(InParams.ID_DEVICE, String.class);
            AccessManagementHandlerFactory.getHandler().unsubscribeUserDevicePushNotofications(request.getIdUser(), idDevice);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> errors = new HashMap<>();
        
        if (StringUtils.isBlank(request.getParam(InParams.ID_DEVICE, String.class))) {
            throw new ActivityException(ReturnCodes.BAD_REQUEST);
        }
        
        return errors;
    }

    
}
