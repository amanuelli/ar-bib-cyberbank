/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.documenttypes.DocumentTypesHandler;
import com.technisys.omnichannel.core.domain.Country;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

@DocumentedActivity("Invite user (PRE)")
public class InviteUsersPreActivity extends Activity {

    public static final String ID = "administration.users.invite.pre";

    public interface InParams {
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Default country code")
        String DEFAULT_COUNTRY = "defaultCountry";
        @DocumentedParam(type = List.class, description = "Country list")
        String COUNTRY_LIST = "countryList";
        @DocumentedParam(type = List.class, description = "Document type list by country")
        String DOCUMENT_TYPE_LIST = "documentTypeList";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }
            if (!ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "administration.users.invite.enabled")) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED, "No se encuentra activada la funcionalidad de invitar usuarios desde el Frontend");
            }

            List<Country> countryCodeList = CountryCodesHandler.getCountryList(request.getLang());
            String defaultCountry = "UY";

            List<Map<String, String>> documentTypeList = DocumentTypesHandler.listDocumentTypesWithCountryCode();    
            
            response.putItem(OutParams.DEFAULT_COUNTRY, defaultCountry);
            response.putItem(OutParams.COUNTRY_LIST, countryCodeList);
            response.putItem(OutParams.DOCUMENT_TYPE_LIST, documentTypeList);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
