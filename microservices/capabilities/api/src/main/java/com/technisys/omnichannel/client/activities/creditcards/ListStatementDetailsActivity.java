/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.creditcards;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.client.domain.StatementCreditCard;
import com.technisys.omnichannel.client.handlers.notes.NotesHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

import static com.technisys.omnichannel.ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED;
import static com.technisys.omnichannel.ReturnCodes.VALIDATION_ERROR;

/**
 *
 */
@DocumentedActivity("List credit card statement's details")
public class ListStatementDetailsActivity extends Activity {

    public static final String ID = "creditCard.listStatementDetails";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "Credit card Id")
        String ID_CREDIT_CARD = "idCreditCard";
        @DocumentedParam(type = Integer.class, description = "Statement Id")
        String ID_STATEMENT = "idStatement";
    }

    public interface OutParams {

        @DocumentedParam(type = Statement.class, description = "Statement object details")
        String STATEMENT = "statement";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ENVIRONMENT_NOT_AUTHORIZED);
            }

            Integer idStatement = request.getParam(InParams.ID_STATEMENT, Integer.class);
            if(idStatement == null) {
                throw new ActivityException(VALIDATION_ERROR,"Statement id should not be empty");
            }
            String idCreditCard = request.getParam(InParams.ID_CREDIT_CARD, String.class);

            Product product = Administration.getInstance().readProduct(idCreditCard, request.getIdEnvironment());
            StatementCreditCard statemet =  RubiconCoreConnectorTC.readStatementDetails(request.getIdTransaction(), idCreditCard, idStatement, product.getExtraInfo());

            String note = NotesHandler.readNote(idStatement, idCreditCard);
            statemet.setNote(note);

            response.putItem(OutParams.STATEMENT, statemet);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException | BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
