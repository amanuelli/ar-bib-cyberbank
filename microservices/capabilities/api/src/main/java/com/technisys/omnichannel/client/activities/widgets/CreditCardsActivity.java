/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.widgets;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 *
 */
public class CreditCardsActivity extends Activity {

    public static final String ID = "widgets.creditCards";

    public interface InParams {
    }

    public interface OutParams {

        String CREDIT_CARDS = "creditCards";
    }

    @SuppressWarnings("unchecked")
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            List<String> idPermissions = new ArrayList<>();
            idPermissions.add(PRODUCT_READ_PERMISSION);

            List<String> productTypes = Arrays.asList(Constants.PRODUCT_TC_KEY);

            List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);
            List<Product> coreProducts = (List<Product>) RubiconCoreConnectorC.listProducts(request.getIdTransaction(), environment.getClients(), productTypes);
            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
            List<CreditCard> creditCards = new ArrayList<CreditCard>();

            for (Product p : coreProducts) {
                for (Product environmentProduct : environmentProducts) {
                    if (p.getExtraInfo().equals(environmentProduct.getExtraInfo())) {
                        CreditCard card = (CreditCard) p;
                        String number = card.getNumber();
                        card.setProductAlias(environmentProduct.getProductAlias());
                        card.setShortLabel(pLabeler.calculateShortLabel(card));
                        card.setLabel(pLabeler.calculateLabel(card));
                        card.setFranchise(pLabeler.calculateFranchise(card));
                        card.setHolder("•••• •••• •••• " + number.substring(number.length() - 4, number.length()));
                        card.setIdProduct(environmentProduct.getIdProduct());
                        creditCards.add(card);
                    }
                }
            }

            response.putItem(OutParams.CREDIT_CARDS, creditCards);
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
