/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */
package com.technisys.omnichannel.client.forms.fields.selector.resources;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.LoanType;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.forms.fields.FrontendOption;
import com.technisys.omnichannel.core.forms.fields.OptionsResource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CoreLoanTypeOptionsResource implements OptionsResource {

    @Override
    public List<? extends FrontendOption> getOptions(FormField field, Request request) throws IOException {

        try {
            List<FrontendOption> options = new ArrayList<>();
            List<LoanType> loanTypeLitst =   CoreLoanConnectorOrchestrator.listLoanType();
            for (LoanType loanType : loanTypeLitst){
                options.add(new FrontendOption(loanType.getLoanTypeId(), loanType.getLoanTypeDescription()));
            }
            return options;
        } catch (BackendConnectorException e) {
            throw new IOException(e);
        }
    }
}


