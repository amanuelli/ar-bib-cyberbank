package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.groups.ExportGroupsActivity;
import com.technisys.omnichannel.client.activities.administration.users.ListUsersActivity;
import com.technisys.omnichannel.client.utils.ExportUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Signature;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.limits.CapForSignature;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import com.technisys.omnichannel.core.utils.NumberUtils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.*;

@DocumentedActivity("Download file with list of signature schemes")
public class ExportSignaturesActivity extends Activity {

    public static final String ID = "administration.signatures.export";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "File download format (xls or pdf)")
        String FORMAT = "format";
        @DocumentedParam(type = String.class, description = "List order")
        String ORDER_BY = "orderBy";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Downloaded file name")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = String.class, description = "File content (Base64 format)")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        I18n i18n = I18nFactory.getHandler();

        try {
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            String orderBy = request.getParam(ListUsersActivity.InParams.ORDER_BY, String.class);

            if (StringUtils.isEmpty(orderBy)) {
                orderBy = "id_signature ASC";
            } else {
                ArrayList auxOrder = new ArrayList();
                auxOrder.add("id_signature".toLowerCase());
                auxOrder.add("id_environment".toLowerCase());
                auxOrder.add("signature_group".toLowerCase());
                auxOrder.add("signature_type".toLowerCase());
                auxOrder.add("signatureTypeDescription".toLowerCase());
                auxOrder.add("dispatcher".toLowerCase());
                auxOrder.add("signature_alias".toLowerCase());
                String[] split = orderBy.split(" ");
                boolean isValidFilter = true;
                if (split.length > 1) {
                    if (!auxOrder.contains(split[0].toLowerCase())) {
                        isValidFilter = false;
                    }
                } else {
                    if (!auxOrder.contains(orderBy.toLowerCase())) {
                        isValidFilter = false;
                    }
                }
                if (!isValidFilter) {
                    throw new ActivityException(
                            ReturnCodes.VALIDATION_ERROR,
                            i18n.getMessage(
                                    "administration.signatures.export.fileExtension.required",
                                    request.getLang()
                            ));
                }
            }

            List<String> includeSignatureTypes = Arrays.asList(new String[]{Signature.TYPE_AMOUNT, Signature.TYPE_NO_AMOUNT});
            List<Signature> signatures = LimitsHandlerFactory.getHandler().listSignatures(request.getIdEnvironment(), includeSignatureTypes, -1, -1, orderBy).getElementList();


            Double defaultAmount = ConfigurationFactory.getInstance().getDouble(Configuration.LIMITS, "default_cap_signature_" + request.getEnvironmentType());

            List<Map<String, Object>> signatureToExport = new ArrayList<>();
            for (Signature signature : signatures) {
                Map<String, Object> map = new HashMap<>();
                map.put("signatureGroup", signature.getSignatureGroup());
                map.put("signatureType", signature.getSignatureType());
                map.put("signatureTypeDescription", i18n.getMessage("signatures.type." + signature.getSignatureType(), request.getLang()));

                StringBuilder maxAmount = new StringBuilder();
                if (Signature.TYPE_AMOUNT.equals(signature.getSignatureType())) {
                    boolean first = true;
                    for (CapForSignature cap : signature.getCapList()) {
                        if (!first) {
                            maxAmount.append(", ");
                        }
                        maxAmount.append(i18n.getMessage("channels." + cap.getChannel(), request.getLang())).append(": ");
                        maxAmount.append(NumberUtils.formatDecimal((cap.getMaximum() <= 0 ? defaultAmount : cap.getMaximum()), request.getLang()));
                        maxAmount.append(" (").append(i18n.getMessage("administration.signatures.list.maxAmount.by", request.getLang()));
                        maxAmount.append(" ").append(i18n.getMessage("administration.channels.capFrequency." + cap.getFrequency(), request.getLang())).append(")");
                        first = false;
                    }
                }

                map.put("maxAmount", maxAmount);

                signatureToExport.add(map);
            }

            HashMap<String, String> fillers = new HashMap<>();
            fillers.put("0", I18nFactory.getHandler().getMessage("currency.label." + ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency"), request.getLang()));
            String[] columns = {
                    i18n.getMessage("administration.signatures.export.signatureGroup", request.getLang()),
                    i18n.getMessage("administration.signatures.export.signatureType", request.getLang()),
                    i18n.getMessage("administration.signatures.export.maxAmount", request.getLang(), fillers),

            };

            String[] attributes = {"signatureGroup", "signatureTypeDescription", "maxAmount"};

            byte[] content;
            String filename = i18n.getMessage("administration.signatures.export.filename", request.getLang());
            String fileExtension = request.getParam(ExportGroupsActivity.InParams.FORMAT, String.class);

            if (StringUtils.isBlank(fileExtension)) {
                throw new ActivityException(
                        ReturnCodes.VALIDATION_ERROR,
                        i18n.getMessage(
                                "administration.signatures.export.fileExtension.required",
                                request.getLang()
                        ));
            }

            fileExtension = fileExtension.toLowerCase();

            if (!fileExtension.equals("xls") && !fileExtension.equals("pdf")) {
                throw new ActivityException(
                        ReturnCodes.VALIDATION_ERROR,
                        i18n.getMessage(
                                "administration.signatures.export.fileExtension.invalid",
                                request.getLang()
                        ));
            }

            content = ExportUtils.toExcelFormat("/templates/administration_xls_template.xls", columns, signatureToExport, attributes, i18n.getMessage("administration.signatures.export.title", request.getLang()), null);
            filename += ".xls";
            response.putItem(ExportGroupsActivity.OutParams.CONTENT, Base64.encodeBase64(content));
            response.putItem(ExportGroupsActivity.OutParams.FILE_NAME, filename);
            response.setReturnCode(ReturnCodes.OK);

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.VALIDATION_ERROR, ex);
        }

        return response;
    }
}
