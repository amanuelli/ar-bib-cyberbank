/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.misc;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.documenttypes.DocumentTypesHandler;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class ListDocumentTypesActivity extends Activity {

    public static final String ID = "misc.listDocumentTypes";

    public interface InParams {

        String ID_COUNTRY = "idCountry";
    }

    public interface OutParams {

        String DOCUMENT_TYPE_LIST = "documentTypeList";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String idCountryCode = request.getParam(InParams.ID_COUNTRY, String.class);

            I18n i18n = I18nFactory.getHandler();
            List<Map<String, String>> documentTypeList = new ArrayList<>();
            for (String docType : DocumentTypesHandler.listDocumentTypes(idCountryCode)) {
                Map<String, String> map = new HashMap<>();

                map.put("id", docType);
                map.put("label", i18n.getMessage("documentType.label." + docType, request.getLang()));

                documentTypeList.add(map);
            }

            response.putItem(OutParams.DOCUMENT_TYPE_LIST, documentTypeList);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
