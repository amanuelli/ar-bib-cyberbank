/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.restrictions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.environments.EnvironmentHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Marcelo Bruno
 */

/**
 * Delete all user access restrictions
 */
@DocumentedActivity("Delete all user access restrictions")
public class DeleteAllUserRestrictionsActivity extends Activity {

    public static final String ID = "administration.restrictions.user.delete.send";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "User Id to delete restrictions")
        String ID_USER = "idUser";
    }

    public interface OutParams {

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        if(Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
            throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
        }

        Response response = new Response(request);
        String idUser = request.getParam(ManageAccessRestrictionsActivity.InParams.ID_USER, String.class);

        try {
            Administration.getInstance().deleteAllEnvironmentUserAccessIP(request.getIdEnvironment(), idUser);
            EnvironmentHandler.deleteAllEnvironmentUserAccessCalendar(request.getIdEnvironment(), idUser);

            Transaction t = TransactionHandlerFactory.getInstance().read(request.getIdTransaction());
            t.getData().put("userName", AccessManagementHandlerFactory.getHandler().getUser(idUser).getFullName());
            TransactionHandlerFactory.getInstance().updateTransactionData(t);

            response.setReturnCode(ReturnCodes.OK);
            return response;
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String idUser = request.getParam(ManageAccessRestrictionsActivity.InParams.ID_USER, String.class);
        if (StringUtils.isBlank(idUser)) {
            result.put(InParams.ID_USER, "administration.restrictions.user.delete.empty");
        } else {
            try {
                User user = AccessManagementHandlerFactory.getHandler().getUser(idUser);
                if(user == null) {
                    result.put(InParams.ID_USER, "administration.users.idUserDoesntExists");
                }
            } catch (IOException e) {
                throw new ActivityException(ReturnCodes.IO_ERROR, e);
            }
        }
        return result;
    }
}
