/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.ListTransactionsActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@DocumentedActivity("List users")
public class ListUsersActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(ListTransactionsActivity.class);

    public static final String ID = "administration.users.list";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "Request page number")
        String PAGE_NUMBER = "pageNumber";
        @DocumentedParam(type = String.class, description = "Order by field name")
        String ORDER_BY = "orderBy";
        @DocumentedParam(type = String.class, description = "Id environment")
        String ID_ENVIRONMENT = "idEnvironment";
    }

    public interface OutParams {

        @DocumentedParam(type = Boolean.class, description = "Flag to indicate if invitation feature is enabled")
        String INVITE_ENABLED = "inviteEnabled";
        @DocumentedParam(type = List.class, description = "User's list to show")
        String USERS = "users";
        @DocumentedParam(type = Map.class, description = "Map with extended info content (massiveEnabled flag, signatureLevel and status)")
        String USERS_EXTENDED_INFO = "usersExtendedInfo";
        @DocumentedParam(type = Integer.class, description = "Current page number")
        String CURRENT_PAGE = "currentPage";
        @DocumentedParam(type = Integer.class, description = "Total pages")
        String TOTAL_PAGES = "totalPages";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Administration admin = Administration.getInstance();
            int rowsPerPage = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "administration.rowsPerPage");

            int pageNumber = request.getParam(InParams.PAGE_NUMBER, int.class);
            String orderBy = request.getParam(InParams.ORDER_BY, String.class);

            if (orderBy == null || "".equals(orderBy)) {
                orderBy = "last_login DESC";
            }

            PaginatedList list = AccessManagementHandlerFactory.getHandler().getUsers(request.getIdEnvironment(), pageNumber, rowsPerPage, orderBy);

            List<User> listUsers = null;

            Map<String, Map<String, Object>> usersExtendedInfo = new HashMap<>();
            if (list.getElementList() != null) {
                listUsers = ((List<User>) list.getElementList()).stream()
                        .map( i -> {
                            i.setPassword(null);
                            return i;
                        }).collect(Collectors.toList());
                EnvironmentUser envUser;
                for (User user : listUsers) {
                    Map<String, Object> map = new HashMap<>();
                    envUser = admin.readEnvironmentUserInfo(user.getIdUser(), request.getIdEnvironment());
                    map.put("status", envUser.getIdUserStatus());
                    map.put("signatureLevel", envUser.getSignatureLevel());
                    map.put("massiveEnabled", !Authorization.hasPermission(user.getIdUser(), request.getIdEnvironment(), null, Constants.ADMINISTRATION_VIEW_PERMISSION));
                    map.put("dispatcher", envUser.isDispatcher());
                    usersExtendedInfo.put(user.getIdUser(), map);
                }
            }

            response.putItem(OutParams.USERS, listUsers);
            response.putItem(OutParams.USERS_EXTENDED_INFO, usersExtendedInfo);
            response.putItem(OutParams.CURRENT_PAGE, list.getCurrentPage());
            response.putItem(OutParams.TOTAL_PAGES, list.getTotalPages());
            response.putItem(OutParams.INVITE_ENABLED, ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "administration.users.invite.enabled"));

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String idEnvironment = request.getParam(InParams.ID_ENVIRONMENT, String.class);

        try {
            if (StringUtils.isBlank(idEnvironment) || !StringUtils.isNumeric(idEnvironment) || (Integer.parseInt(idEnvironment) < 1)) {
                result.put("idEnvironment", "administration.idEnvironment.invalid");
            }
        } catch (NumberFormatException ex) {
            result.put("idEnvironment", "administration.idEnvironment.invalid");
            log.error("Parameter idEnvironment provided is not a number: {}", idEnvironment);
        }
        return result;
    }
}
