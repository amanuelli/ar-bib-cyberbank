/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.position;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.PositionProduct;
import com.technisys.omnichannel.client.domain.PositionProductListByCurrency;
import com.technisys.omnichannel.client.domain.PositionProductListByType;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

/**
 *
 */
public class ListProductsActivity extends Activity {

    public static final String ID = "products.list";

    public interface InParams {
    }

    public interface OutParams {

        String PRODUCTS_BY_TYPE = "productsByType";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            if (environment == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
            List<String> productTypes = Arrays.asList(new String[]{Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY, Constants.PRODUCT_TC_KEY, Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY});
            List<PositionProductListByType> result = RubiconCoreConnectorC.listPositionProducts(request.getIdTransaction(), environment.getClients(), productTypes);

            // Agrego las etiquetas a los productos y si el usuario tiene permiso sobre el mismo
            for (PositionProductListByType productType : result) {
                for (PositionProductListByCurrency productsByCurrency : productType.getProductsByCurrencyList()) {
                    for (PositionProduct pp : productsByCurrency.getProductList()) {
                        pp.setShortLabel(pLabeler.calculateShortLabel(pp));
                        pp.setLabel(pLabeler.calculateLabel(pp));

                        //marco si tiene permiso de lectura
                        pp.setCanRead(Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), pp.getIdProduct(), Constants.PRODUCT_READ_PERMISSION));
                    }
                }
            }

            response.putItem(OutParams.PRODUCTS_BY_TYPE, result);

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

}
