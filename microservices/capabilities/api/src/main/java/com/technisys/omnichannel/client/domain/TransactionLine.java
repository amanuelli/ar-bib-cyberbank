package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.i18n.I18nFactory;
import org.apache.commons.lang.StringUtils;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class TransactionLine implements Serializable {

    public enum Status {
        FORMAT_ERROR("FORMAT_ERROR"),
        PENDING("PENDING"),
        PROCESSED("PROCESSED"),
        PROCESSED_WITH_ERROR("PROCESSED_WITH_ERROR");

        private final String statusName;

        private static Map<String, Status> valuesMap = new HashMap<>();

        static {
            for (Status status : values()) {
                valuesMap.put(status.statusName, status);
            }
        }

        Status(String statusName) {
            this.statusName = statusName;
        }

        @Override
        public String toString() {
            return statusName;
        }

        public static Status fromValue(String value) {
            return valuesMap.get(value);
        }
    }

    public enum ErrorCode {
        EMPTY_HEADER("0"),
        EMPTY_LINE("1"),
        INVALID_LINE_LENGTH("2"),
        INVALID_HEADER_LENGTH("3"),
        INVALID_FILE_IDENTIFIER("4"),
        EMPTY_CURRENCY("5"),
        UNKNOWN_CURRENCY("6"),
        INVALID_CURRENCY("7"),
        INVALID_ACCOUNT_LENGTH("8"),
        INVALID_ACCOUNT_FORMAT("9"),
        NEGATIVE_AMOUNT("10"),
        INVALID_AMOUNT("11"),
        EMPTY_BENEFICIARY("12"),
        INVALID_BANK_IDENTIFIER("13"),
        INVALID_TOTAL_AMOUNT("14"),
        BLOCKED_ACCOUNT("100"),
        INVALID_ACCOUNT("101");

        private final String error;

        private static Map<String, ErrorCode> valuesMap = new HashMap<>();

        static {
            for (ErrorCode errorCode : values()) {
                valuesMap.put(errorCode.error, errorCode);
            }
        }

        ErrorCode(String error) {
            this.error = error;
        }

        @Override
        public String toString() {
            return error;
        }

        public String toString(String lang) {
            return I18nFactory.getHandler().getMessage("pay.multiline.error." + error, lang);
        }

        public static ErrorCode fromValue(String value) {
            return valuesMap.get(value);
        }
    }

    private String idTransaction;
    private Integer lineNumber;
    private String creditAccountNumber;
    private String creditAccountName;
    private Double creditAmountQuantity;
    private String creditAmountCurrency;
    private String bankIdentifier;
    private String reference;
    private Status status;
    private Date statusDate;
    private ErrorCode errorCode;
    private String errorCodeMsg;
    private static final String CREDIT_AMOUNT_QUANTITY = "creditAmountQuantity";


    public TransactionLine() {
    }

    public TransactionLine(Map<String, Object> transactionLineData) {
        idTransaction = (String) transactionLineData.get("idTransaction");
        lineNumber = (Integer) transactionLineData.get("lineNumber");
        creditAccountNumber = (String) transactionLineData.get("creditAccountNumber");
        creditAccountName = (String) transactionLineData.get("creditAccountName");
        creditAmountCurrency = (String) transactionLineData.get("creditAmountCurrency");

        creditAmountQuantity = null;
        if (transactionLineData.get(CREDIT_AMOUNT_QUANTITY) != null) {
            if (transactionLineData.get(CREDIT_AMOUNT_QUANTITY) instanceof Double) {
                creditAmountQuantity = (Double) transactionLineData.get(CREDIT_AMOUNT_QUANTITY);
            } else if (transactionLineData.get(CREDIT_AMOUNT_QUANTITY) instanceof Integer) {
                creditAmountQuantity = ((Integer) transactionLineData.get(CREDIT_AMOUNT_QUANTITY)).doubleValue();
            } else if (transactionLineData.get(CREDIT_AMOUNT_QUANTITY) instanceof Long) {
                creditAmountQuantity = Double.parseDouble(((Long) transactionLineData.get(CREDIT_AMOUNT_QUANTITY)).toString());
            } else {
                creditAmountQuantity = Double.parseDouble((String) transactionLineData.get(CREDIT_AMOUNT_QUANTITY));
            }
        }
        bankIdentifier = (String) transactionLineData.get("bankIdentifier");
        reference = (String) transactionLineData.get("reference");
        status = (Status) transactionLineData.get("status");
        statusDate = (Date) transactionLineData.get("statusDate");
        errorCode = (ErrorCode) transactionLineData.get("errorCode");
    }

    public String getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public String getCreditAccountNumber() {
        return creditAccountNumber;
    }

    public void setCreditAccountNumber(String creditAccountNumber) {
        this.creditAccountNumber = creditAccountNumber;
    }

    public String getCreditAccountName() {
        return creditAccountName;
    }

    public void setCreditAccountName(String creditAccountName) {
        this.creditAccountName = creditAccountName;
    }

    public Double getCreditAmountQuantity() {
        return creditAmountQuantity;
    }

    public void setCreditAmountQuantity(Double creditAmountQuantity) {
        this.creditAmountQuantity = creditAmountQuantity;
    }

    public String getCreditAmountCurrency() {
        return creditAmountCurrency;
    }

    public void setCreditAmountCurrency(String creditAmountCurrency) {
        this.creditAmountCurrency = creditAmountCurrency;
    }
    
    public String getBankIdentifier() {
        return bankIdentifier;
    }

    public void setBankIdentifier(String bankIdentifier) {
        this.bankIdentifier = bankIdentifier;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public Date getStatusDate() {
        return statusDate;
    }

    public void setStatusDate(Date statusDate) {
        this.statusDate = statusDate;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorCodeMsg() {
        return errorCodeMsg;
    }

    public void setErrorCodeMsg(String errorCodeMsg) {
        this.errorCodeMsg = errorCodeMsg;
    }

    public String toErrorString(String lang) {
        String creditAccountNumberError = StringUtils.defaultString(creditAccountNumber);
        String creditAmountCurrencyError = StringUtils.defaultString(creditAmountCurrency);
        String creditAmountQuantityError = StringUtils.defaultString(creditAmountQuantity != null ? creditAmountQuantity.toString() : "");
        String creditAccountNameError = StringUtils.defaultString(creditAccountName);
        String errorCodeError = StringUtils.defaultString(errorCode.toString(lang));

        return creditAccountNumberError + "," + creditAmountCurrencyError + "," + creditAmountQuantityError + "," + creditAccountNameError + "," + errorCodeError;
    }

    @Override
    public String toString() {
        String accountNumber = StringUtils.defaultString(creditAccountNumber);
        String currency = StringUtils.defaultString(creditAmountCurrency);
        String amount = StringUtils.defaultString(creditAmountQuantity != null ? creditAmountQuantity.toString() : "");
        String accountName = StringUtils.defaultString(creditAccountName);
        String bankId = StringUtils.defaultString(bankIdentifier);

        return accountNumber + "," + currency + "," + amount + "," + accountName + "," + bankId;
    }
}
