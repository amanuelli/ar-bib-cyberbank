/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.fingerprint;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.session.legacy.SelectEnvironmentActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import com.technisys.omnichannel.core.session.SessionUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class FingerPrintAcceptGeneralConditions extends Activity {

    public interface InParams {

        String ACCEPT_CONDITIONS = "acceptConditions";
    }

    @Override
    public Response execute(Request request) throws ActivityException {

        try {

            boolean acceptConditions = request.getParam(InParams.ACCEPT_CONDITIONS, boolean.class);

            int actualCondition = GeneralConditions.getInstance().actualCondition();

            if (acceptConditions) {
                //Acepto las condiciones generales
                List<Integer> environmentList = new ArrayList();
                environmentList.add(request.getIdEnvironment());
                GeneralConditions.getInstance().acceptGeneralConditions(actualCondition, request.getIdUser(), environmentList);
            }

            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());

            Environment activeEnvironment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            Map<Integer, Map<String, String>> environments = SessionUtils.assembleEnviromentMap(Administration.getInstance().listEnvironmentsWhereUserIsActive(request.getIdUser(), request.getChannel()));

            request.setIdEnvironment(activeEnvironment.getIdEnvironment());
            Response response = SelectEnvironmentActivity.generateLastLoginStepResponse(request, user, activeEnvironment, environments);

            response.setReturnCode(ReturnCodes.OK);

            return response;
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (Exception e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

    }

}
