package com.technisys.omnichannel.client.activities.onboarding;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingUtils;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Address;
import com.technisys.omnichannel.client.domain.JobInformation;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.client.utils.TemplatingUtils;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.legacy.SMSCommunicationsDispatcher;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Attachment;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.PhoneNumber;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.onboardings.plugins.FaceRecognitionPluginFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 */
@ExchangeActivity
public class Step5Activity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(Step5Activity.class);

    public static final String ID = "onboarding.wizard.step5";

    // It can be included on inParams since it came as header and loaded
    // automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {

        String DOCUMENT_NUMBER = "_documentNumber";
        String FIRST_NAME = "_firstName";
        String LAST_NAME = "_lastName";
        String EMAIL = "_email";
        String MOBILE = "_mobile";
        String ADDRESS = "_address";
        String JOB_INFORMATION = "_jobInformation";
        String SSNID = "_SSNID";
        String REGION = "_region";
    }

    public interface OutParams {

        String ERROR = "error";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(com.technisys.omnichannel.ReturnCodes.OK);
        try {
            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);

            String documentNumber = request.getParam(InParams.DOCUMENT_NUMBER, String.class);
            String firstName = request.getParam(InParams.FIRST_NAME, String.class);
            String lastName = request.getParam(InParams.LAST_NAME, String.class);
            String email = request.getParam(InParams.EMAIL, String.class);
            String ssnid = request.getParam(InParams.SSNID, String.class);
            String region = request.getParam(InParams.REGION, String.class);
            PhoneNumber mobile = request.getParam(InParams.MOBILE, PhoneNumber.class);

            Address address = new Address(request.getParam(InParams.ADDRESS, Map.class));
            JobInformation jobInformation = new JobInformation(request.getParam(InParams.JOB_INFORMATION, Map.class));

            ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);
            long idOnboarding = exchangeToken.getAttribute("idOnboarding");

            Onboarding onboarding = OnboardingHandlerFactory.getInstance().read(idOnboarding);


            // 1 - actualizar email y mobile en la tabla onboarding
            OnboardingHandlerFactory.getInstance().updateEmailAndMobile(idOnboarding, email, mobile.toString());

            if (OnboardingUtils.isProductRequest(onboarding)) {
                return new com.technisys.omnichannel.client.activities.productrequest.onboarding.Step5Activity().execute(request);
            }

            Map<String, String> extraInfo = JsonUtils.fromJson(onboarding.getExtraInfo(), Map.class);
            int addedClientId = Integer.parseInt(CoreCustomerConnectorOrchestrator.add(request.getIdTransaction(),
                    firstName, lastName, email, "", mobile, documentNumber, onboarding.getDocumentType(),
                    onboarding.getDocumentCountry(), extraInfo.get("nationality"), "", "", "PFS",
                    false, extraInfo.get("dateOfBirth"), address, jobInformation));

            if (addedClientId == 0) {
                response.setReturnCode(ReturnCodes.USER_ALREADY_EXISTS);
                return response;
            }

            this.createAccount(request, addedClientId);

            String addedClientIdStr = String.valueOf(addedClientId);
            String documentType = onboarding.getDocumentType();
            // 3 - Creación del código de invitación
            InvitationCode ic = new InvitationCode();
            ic.setStatus(InvitationCode.STATUS_NOT_USED);
            ic.setCreationDate(new Date());
            ic.setLang(request.getLang());
            ic.setDocumentCountry(onboarding.getDocumentCountry());
            ic.setDocumentNumber(onboarding.getDocumentNumber());
            ic.setDocumentType(documentType);
            ic.setEmail(email);
            ic.setMobileNumber(mobile.toString());
            ic.setLastName(extraInfo.get("lastName"));
            ic.setFirstName(extraInfo.get("firstName"));
            ic.setProductGroupId(addedClientIdStr);
            ic.setBackendUser(true);
            ic.setAccessType("administrator");
            ic.setAdministrationScheme(Environment.ADMINISTRATION_SCHEME_SIMPLE);
            ic.setSignatureQty(1);
            ic.setSignatureLevel("A");
            if(StringUtils.isNotBlank(ssnid)){
                ic.setSsnid(ssnid);
            }

            String sendChannel = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "invitation.notification.transport");

            ic.setChannelSent(sendChannel);

            String invitationCodePlain = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
            InvitationCodesHandler.createInvitationCode(ic, invitationCodePlain);

            sendInvitationEmail(ic, request, invitationCodePlain, region);

            OnboardingHandlerFactory.getInstance().updateInvitationCode(idOnboarding, ic.getId());

            this.removeFaceRecognitionImages();

            // Elimino el exchange Token
            ExchangeTokenHandler.delete(request.getParam(EXCHANGE_TOKEN, String.class));
        } catch (IOException ex) {
            log.error("IO error", ex);
        } catch (BackendConnectorException ex) {
            log.error(ex.getMessage(), ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            Map<String, String> result = new HashMap();
            String documentNumber = request.getParam(InParams.DOCUMENT_NUMBER, String.class);

            String email = request.getParam(InParams.EMAIL, String.class);
            if (StringUtils.isBlank(email)) {
                result.put(InParams.EMAIL, "onboarding.step4.email.error.empty");
            } else if (!isValidEmail(email)) {
                result.put(InParams.EMAIL, "onboarding.step4.email.error.incorrectFormat");
            }

            String mobile = request.getParam(InParams.MOBILE, String.class);
            if (StringUtils.isBlank(mobile)) {
                result.put(InParams.MOBILE, "onboarding.step4.cellPhone.error.empty");
            }

            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);
            long idOnboarding = exchangeToken.getAttribute("idOnboarding");
            Onboarding onboarding = OnboardingHandlerFactory.getInstance().read(idOnboarding);

            if (this.clientExistOnCore(request.getIdTransaction(), documentNumber, onboarding.getDocumentType(), onboarding.getDocumentCountry())) {
                result.put(InParams.DOCUMENT_NUMBER, "onboarding.step5.documentAlreadyIsClient");
            }

            return result;
        } catch (IOException bcE) {
            throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, bcE);
        }
    }

    private boolean clientExistOnCore(String idTransaction, String documentNumber, String documentType, String documentCountry) {
        try {
            if (!CoreCustomerConnectorOrchestrator.listClients(idTransaction, documentCountry, documentType, documentNumber).isEmpty()) {
                return true;
            }
        } catch (BackendConnectorException bcE) {
            return false;
        }

        return false;
    }

    private void removeFaceRecognitionImages() {
        // De aquí en más es limpieza de datos
        // Elimino las fotos que pude haber subido a la API
        if ("digital".equals(ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "onboarding.mode", "digital"))) {
            try {
                FaceRecognitionPluginFactory.getFaceRecognitionPlugin().deleteImages(String.valueOf("idOnboarding"));
            } catch (IOException e) {
                log.error("Error deleting image at face recognition plugin", e);
            }
        }

    }

    private void createAccount(Request request, int addedClientId) {
        try {
            boolean createAccount = ConfigurationFactory.getInstance().getDefaultBoolean(Configuration.PLATFORM,  "enrollment.account.create", true);

            if (createAccount) {
                CoreAccountConnectorOrchestrator.addAccount(request.getIdTransaction(), String.valueOf(addedClientId));
            }
        } catch (BackendConnectorException | NumberFormatException e) {
            log.warn("Error adding account", e);
        }
    }

    /**
     * Indica si un string de email es valido
     *
     * @param email String de email
     * @return Booleano indicando si el string es un email valido
     */
    private boolean isValidEmail(String email) {
        String mask = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "email.validationFormat",
                "^[a-zA-Z0-9+&*-]+(?:\\.[a-zA-Z0-9_+&*-]+)*@(?:[a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,7}$");
        Pattern emailPattern = Pattern.compile(mask);
        Matcher matcher = emailPattern.matcher(email != null ? email : "");
        return matcher.matches();
    }

    public static void sendInvitationEmail(InvitationCode invitationCode, IBRequest request, String code, String region) throws ActivityException {
        try {
            String envName = request.getEnvironmentName();

            HashMap<String, String> fillers = new HashMap<>();
            fillers.put("ENVIRONMENT", envName);
            String subject = I18nFactory.getHandler().getMessage(
                    "enrollment.requestInvitationCode.invitationCode." + invitationCode.getChannelSent() + ".subject",
                    request.getLang(), fillers);

            fillers.put("BASE_URL", ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, Constants.BASE_URL_KEY));

            String enrollmentUrl = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "administration.users.invite.enrollmentUrl",
                    fillers);

            log.info("URL de inicio de invitacion: {}  ", enrollmentUrl);

            fillers.put("ENROLLMENT_URL", enrollmentUrl);

            fillers.put("FULL_NAME", invitationCode.getFirstName() + " " + invitationCode.getLastName());
            fillers.put("INVITATION_CODE", code);
            fillers.put("EXPIRATION_DATE", invitationCode.getExpiration());
            String body = I18nFactory.getHandler().getMessage(
                    "enrollment.requestInvitationCode.invitationCode." + invitationCode.getChannelSent() + ".body",
                    request.getLang(), fillers);

            switch (invitationCode.getChannelSent()) {
                case Constants.TRANSPORT_SMS:
                    NotificationsHandlerFactory.getHandler().sendSMS(invitationCode.getMobileNumber(), body, true);
                    break;
                case Constants.TRANSPORT_MAIL:
                    String template = TemplatingUtils.getHtmlTemplate("/templates/onboarding_mail_template.html");
                    template = replaceTemplateValues(template, invitationCode, code, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "client.baseURL"), request.getLang(), region);
                    CoreUtils.sendClientEmail(subject, template, invitationCode.getEmail(), request.getLang(), new Attachment[]{}, false);
                    break;
                default:
                    throw new ActivityException(ReturnCodes.ERROR_SENDING_INVITATION,
                            "No se encuentra bien configurado el transporte de la notificacion para invitaciones");
            }

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException | MessagingException ex) {
             throw new ActivityException(ReturnCodes.ERROR_SENDING_INVITATION, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    private static String replaceTemplateValues(String template, InvitationCode invitationCode, String codePlain, String clientBaseUrl, String lang, String region) {
        template = template.replace("%%IMAGE_TECHBANK_URL%%", clientBaseUrl + "/images/techbank_logo_email.png");

        HashMap<String, String> fillers = new HashMap<>();
        fillers.put("FIRST_NAME", invitationCode.getFirstName());
        template = template.replace("%%TEXT1%%", I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.text1", lang, fillers));
        template = template.replace("%%TEXT2%%", I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.text2", lang));
        template = template.replace("%%TEXT3%%", I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.text3", lang));
        template = template.replace("%%INVITATION_CODE%%", codePlain);

        String urlWithCode = I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.url", lang);
        String codeUrl = clientBaseUrl + "/enrollment/code/"+codePlain+"/"+lang + (region == null ? "" : "/" + region);

        urlWithCode = urlWithCode.replace("URL_CODE", codeUrl);

        template = template.replace("%%LINK_CODE%%", urlWithCode);

        fillers = new HashMap<>();
        String[] expirationDateHour = invitationCode.getExpiration().split(" ");
        fillers.put("EXPIRATION_DATE", expirationDateHour[0]);
        fillers.put("EXPIRATION_DATE_HOUR", expirationDateHour[1]);
        template = template.replace("%%TEXT4%%", I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.text4", lang, fillers));

        template = template.replace("%%TEXT5%%", I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.text5", lang));
        template = template.replace("%%TEXT6%%", I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.text6", lang));
        template = template.replace("%%PRIVACY%%", I18nFactory.getHandler().getMessage("global.privacy", lang));
        template = template.replace("%%TERM_AND_CONDITIONS%%", I18nFactory.getHandler().getMessage("global.termAndConditions", lang));

        template = template.replace("%%PRIVACY_URL%%", clientBaseUrl + "/privacyPolicy");
        template = template.replace("%%TERM_AND_CONDITIONS_URL%%", clientBaseUrl + "/termsAndConditions");

        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        template = template.replace("%%CURRENT_YEAR%%", String.valueOf(currentYear));

        template = template.replace("%%TEXT7%%", I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.text7", lang));
        template = template.replace("%%TEXT8%%", I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.text8", lang));

        return template;
    }
}
