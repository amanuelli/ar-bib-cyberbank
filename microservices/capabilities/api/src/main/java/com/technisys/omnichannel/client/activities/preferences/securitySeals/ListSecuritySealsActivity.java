/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.securitySeals;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.SecuritySealUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;
import java.util.List;

/**
 * Returns list of security seals.
 */
@DocumentedActivity("List Security Seals")
public class ListSecuritySealsActivity extends Activity {

    public static final String ID = "preferences.securityseals.list";
    
    public interface OutParams {
        @DocumentedParam(type = List.class, description = "List of security seals")
        String SECURITY_SEALS = "_securitySeals";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            response.putItem(OutParams.SECURITY_SEALS, SecuritySealUtils.getSealList());

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        
        return response;
    }
}