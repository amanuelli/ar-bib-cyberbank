/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.accounts;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;

/**
 * @author Marcelo Bruno
 */
public class FundAccountPreviewActivity extends Activity {

    public static final String ID = "account.fund.preview";

    public interface InParams {

    }

    public interface OutParams {

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return new FundAccountSendActivity().validate(request);
    }
}