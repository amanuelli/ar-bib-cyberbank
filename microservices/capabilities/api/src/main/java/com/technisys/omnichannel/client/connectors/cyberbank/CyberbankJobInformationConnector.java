package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.OccupationJob;
import com.technisys.omnichannel.client.domain.RoleJob;
import com.technisys.omnichannel.client.utils.JsonTemplateUtils;
import com.technisys.omnichannel.core.utils.CacheUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class CyberbankJobInformationConnector extends CyberbankCoreConnector {

    private static final Logger LOG = LoggerFactory.getLogger(CyberbankJobInformationConnector.class);
    private static final String NEMOTECNICO = "nemotecnico";

    /**
     * get de list roles job from core
     *
     * @return
     * @throws BackendConnectorException
     */
    public static CyberbankCoreConnectorResponse<List<RoleJob>> listRolesJob() throws BackendConnectorException {
        try {

            List<RoleJob> roleJobList = (List<RoleJob>) CacheUtils.get("cyberbank.core.roles.job");
            CyberbankCoreConnectorResponse<List<RoleJob>> response = new CyberbankCoreConnectorResponse<>();

            if (roleJobList != null) {
                response.setData(roleJobList);
            } else {
                String serviceName = "Srv - massiveSelectGeneral_TableProfession";
                CyberbankCoreRequest request = new CyberbankCoreRequest("massiveSelectGeneral_TableProfession", true);
                LOG.info("{}: Request {}", serviceName, URL);

                String jsonRequest = JsonTemplateUtils.applyTemplateToJson(request.getJSON(), REQ_TEMPLATE_PATH + request.getTransactionId());
                JSONObject serviceResponse = call(serviceName, jsonRequest);

                if (callHasError(serviceResponse)) {
                    processErrors(serviceName, serviceResponse, response);
                } else {
                    response.setData(processRoleJob(serviceResponse));
                }
            }

            return response;
        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     * get List of Occupation Jobs
     * @return
     * @throws BackendConnectorException
     */
    public static CyberbankCoreConnectorResponse<List<OccupationJob>> listOccupationJob() throws BackendConnectorException {
        try{
            List<OccupationJob> occupationJobList = (List<OccupationJob>) CacheUtils.get("cyberbank.core.occupations.job");
            CyberbankCoreConnectorResponse<List<OccupationJob>> response = new CyberbankCoreConnectorResponse<>();
            if(occupationJobList != null){
                response.setData(occupationJobList);
            }else{
                String serviceName = "Srv - massiveSelectGeneral_TableOccupation_Code";
                CyberbankCoreRequest request = new CyberbankCoreRequest("massiveSelectGeneral_TableOccupation_Code", true);
                LOG.info("{}: Request {}", serviceName, URL);

                String jsonRequest = JsonTemplateUtils.applyTemplateToJson(request.getJSON(), REQ_TEMPLATE_PATH + request.getTransactionId());
                JSONObject serviceResponse = call(serviceName, jsonRequest);

                if (callHasError(serviceResponse)) {
                    processErrors(serviceName, serviceResponse, response);
                } else {
                    response.setData(processOccupationJob(serviceResponse));
                }
            }

            return response;

        }catch (CyberbankCoreConnectorException | IOException e){
            throw new BackendConnectorException(e);
        }
    }


    /**
     * parse rol response from core
     *
     * @param serviceResponse
     * @return
     */
    private static List<RoleJob> processRoleJob(JSONObject serviceResponse) throws IOException {
        ArrayList<RoleJob> rolesList = new ArrayList<>();
        JSONArray collection = serviceResponse.getJSONObject("out.result").getJSONArray("collection");
        for (Object object : collection) {
            JSONObject role = (JSONObject) object;
            if (role.has(NEMOTECNICO) && !role.isNull(NEMOTECNICO)) {
                String codeName = role.getString(NEMOTECNICO);
                int idRol = role.getInt("id");
                String nameRol = role.getString("shortDesc");
                rolesList.add(new RoleJob(idRol, nameRol, codeName));

            }
        }
        if(!rolesList.isEmpty()){
            CacheUtils.put("cyberbank.core.roles.job", rolesList);
        }
        return rolesList;
    }

    /**
     * Parse occupation Job from Core
     * @param serviceResponse
     * @return
     */
    public static List<OccupationJob> processOccupationJob(JSONObject serviceResponse) throws IOException {
        ArrayList<OccupationJob> occupationJobsList = new ArrayList<>();
        JSONArray collection = serviceResponse.getJSONObject("out.occupation_code_list").getJSONArray("collection");
        for (Object object : collection) {
            JSONObject occupation = (JSONObject) object;
            if (occupation.has("id") && !occupation.isNull("id")) {
                String name = occupation.getString("shortDesc").replaceAll("^[ \t]+|[ \t]+$", "");
                int id = occupation.getInt("id");
                occupationJobsList.add(new OccupationJob(id, name));

            }
        }
        if(!occupationJobsList.isEmpty()){
            occupationJobsList.sort(Comparator.comparing(OccupationJob::getName));
            CacheUtils.put("cyberbank.core.occupations.job", occupationJobsList);
        }
        return occupationJobsList;
    }

}
