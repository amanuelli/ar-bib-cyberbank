/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.activities.pendingactions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.ReturnCodes.OK;

/**
 * @author Jhossept G
 */
@DocumentedActivity("Enrollment Accept IRS")
public class IRSActivity extends Activity {
    public static final String ID = "pendingActions.irs";

    public interface InParams {

        @DocumentedParam(type = Boolean.class, description = "Parameter to specify whether the user is subject to backup withholding or not")
        String IRS = "IRS";

        @DocumentedParam(type = Boolean.class, description = "Parameter to specify whether the user is subject to backup withholding or not")
        String SSNIDVERIFICATION = "validateSSNID";
    }


    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Boolean isIRS = request.getParam(InParams.IRS, Boolean.class);
            List<Environment> listEnv = Administration.getInstance().listEnvironments(request.getIdUser());
            if(listEnv.isEmpty()){
                throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, "Environment not found on IRS action");
            }
            int idEnvironment = listEnv.get(0).getIdEnvironment();
            Date currentDate = new Date();

            AccessManagementHandlerFactory.getHandler().updateUserIRS(isIRS,currentDate,idEnvironment,request.getIdUser());

            response.setReturnCode(OK);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            Map<String, String> result = new HashMap<>();
            if (request.getParam(InParams.IRS, Boolean.class) == null) {
                result.put(InParams.IRS, "enrollment.irs.empty");
            }

            String verificationParam = request.getParam(InParams.SSNIDVERIFICATION, String.class);
            if(verificationParam != null ){
                User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
                if(!user.getSsnid().equals(verificationParam)){
                    result.put(InParams.SSNIDVERIFICATION,"enrollment.irs.SSNID.incorrect");
                }
            }else{
                result.put(InParams.SSNIDVERIFICATION,"enrollment.irs.SSNID.empty");
            }
            return result;
        }
        catch(Exception ex)
        {
            throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, ex);
        }
    }

}
