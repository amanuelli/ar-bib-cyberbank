package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.domain.CalendarLoginRestriction;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.environments.EnvironmentHandler;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.utils.days.Day;

import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

/**
 *
 * @author Jhossept
 */
public class CalendarRestrictionsUtils {

    private CalendarRestrictionsUtils() {
        //not called
    }

    public static boolean validateAccessByEnvironmentAndUser(int idEnvironment,String idUser, Date requestDate) throws ActivityException{
        try {
            PaginatedList<CalendarLoginRestriction> restrictions = EnvironmentHandler.listCalendarLoginRestrictions(idEnvironment, idUser, 0,0,  true);
            if(restrictions.getElementList().isEmpty()){
                restrictions = EnvironmentHandler.listCalendarLoginRestrictions(idEnvironment, null, 0,0, true);
            }
            boolean validationResult = false;

            for (CalendarLoginRestriction calendarLog: restrictions.getElementList()) {
                validationResult |= CalendarRestrictionsUtils.checkCalendarAccessRestriction(calendarLog, requestDate);
                if(validationResult) {
                    break;
                }
            }
            return validationResult || restrictions.getElementList().isEmpty();
        } catch (Exception e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    public static boolean checkCalendarAccessRestriction(CalendarLoginRestriction restriction, Date requestDate){
        Day requestDay = Day.fromDate(requestDate);

        if(restriction.getDaysEntity().contains(requestDay)){
            ZonedDateTime serverTime = DateUtils.getDateInOtherTimeZone(restriction.getTimeZone(),requestDate, ZoneId.systemDefault());

            ZonedDateTime restrictionStartTime =
                    ZonedDateTime.of(serverTime.getYear(), serverTime.getMonth().getValue(), serverTime.getDayOfMonth(), 0, 0, 0, 0, ZoneId.of(restriction.getTimeZone()))
                            .plusMinutes(restriction.getStart());

            ZonedDateTime restrictionEndTime =
                    ZonedDateTime.of(serverTime.getYear(), serverTime.getMonth().getValue(), serverTime.getDayOfMonth(), 0, 0, 0, 0, ZoneId.of(restriction.getTimeZone()))
                            .plusMinutes(restriction.getEnd());

            //if the request was made before the starting time, or after of the ending time, then the user can not login
            return serverTime.isAfter(restrictionStartTime) && serverTime.isBefore(restrictionEndTime);
        } else {
            return false;
        }
    }
}
