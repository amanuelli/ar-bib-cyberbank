/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.annotations.docs.DocumentedTypeParam;
import java.util.Date;

/**
 *
 * @author msouza
 */
public class WeatherInfoCache {
    
    @DocumentedTypeParam(description = "City identifier")
    private String cityKey;

    @DocumentedTypeParam(description = "Weather, e.g. &quot;cloudy, rainy, snowy&quot;")
    private String weather;

    @DocumentedTypeParam(description = "IsDay")
    private Boolean isDay;

    @DocumentedTypeParam(description = "Date and time of last update")
    private Date lastUpdate;

    public String getCityKey() {
        return cityKey;
    }

    public void setCityKey(String cityKey) {
        this.cityKey = cityKey;
    }
       
    public Boolean getIsDay() {
        return isDay;
    }
    
    public void setIsDay(boolean isDay) {
        this.isDay = isDay;
    }
    
    public String getWeather() {
        return weather;
    }
    
    public void setWeather(String weather) {
        this.weather = weather;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }
    
    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

}