/*
 *  Copyright 2010 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.listeners;

import com.technisys.omnichannel.core.notifications.legacy.CommunicationsDaemonJob;
import com.technisys.omnichannel.core.scheduler.SchedulerHandlerFactory;
import com.technisys.omnichannel.core.scheduler.legacy.SchedulerHandlerLegacy.GroupName;
import com.technisys.omnichannel.core.utils.CoreUtils;
import org.quartz.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import static org.quartz.JobBuilder.newJob;
import static org.quartz.SimpleScheduleBuilder.simpleSchedule;
import static org.quartz.TriggerBuilder.newTrigger;

/**
 * TODO: Es necesario incluir la definición del job en un DBVS y borrar esta
 * clase ;)
 *
 * @author rzuasti
 */
public class CommunicationsDaemonListener implements ServletContextListener {

    private static final Logger log = LoggerFactory.getLogger(CommunicationsDaemonListener.class);

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        synchronized (this) {
            Scheduler scheduler = SchedulerHandlerFactory.getInstance().getScheduler();

            if (scheduler != null) {
                JobKey jobKey = new JobKey(CommunicationsDaemonJob.DAEMON_JOB_NAME, GroupName.daemon.toString());

                Boolean registered = null;
                try {
                    registered = Boolean.FALSE;

                    JobDetail jobDetail = scheduler.getJobDetail(jobKey);

                    if (jobDetail != null) {
                        registered = Boolean.TRUE;
                    }
                } catch (SchedulerException e) {
                    log.error("Error checking if CommunicationsDaemonJob is already registered", e);
                }

                if (!registered) {
                    try {
                        log.info("Registering CommunicationsDaemonJob...");

                        String interval = "5m";

                        JobDetail jobDetail = newJob(CommunicationsDaemonJob.class)
                                .withIdentity(jobKey)
                                .usingJobData("interval", interval)
                                .storeDurably()
                                .build();

                        TriggerKey triggerKey = new TriggerKey(CommunicationsDaemonJob.DAEMON_TRIGGER_NAME, GroupName.daemon.toString());

                        SimpleTrigger trigger = newTrigger()
                                .withIdentity(triggerKey)
                                .startAt(DateBuilder.futureDate(1, DateBuilder.IntervalUnit.MINUTE))
                                .withSchedule(simpleSchedule()
                                        .withIntervalInMilliseconds(CoreUtils.parseTimeAsMillis(interval))
                                        .repeatForever())
                                .build();
                        scheduler.scheduleJob(jobDetail, trigger);
                    } catch (ObjectAlreadyExistsException e) {
                        // The job was already registered by another process.
                    } catch (SchedulerException e) {
                        log.error("Error registering CommunicationsDaemonJob", e);
                    }
                }
            }
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        // No implementation required
    }
}
