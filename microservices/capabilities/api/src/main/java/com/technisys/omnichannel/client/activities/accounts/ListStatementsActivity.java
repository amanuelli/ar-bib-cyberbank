/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.accounts;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.StatementResult;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.fields.DateField;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import static com.technisys.omnichannel.ReturnCodes.IO_ERROR;
import static com.technisys.omnichannel.client.ReturnCodes.BACKEND_SERVICE_ERROR;
import static com.technisys.omnichannel.client.ReturnCodes.OK;

/**
 * List account movements that fulfill some filter conditions.
 *
 * This list will be filtered and can be returned as one complete list or it can
 * be returned as a paginated list, page by page.
 *
 * For this last option, the configuration key
 * <code>accounts.statementsPerPage</code> is set to specify the page size.
 *
 * Additionally the follow filtering options can be also set:
 * <ul>
 * <li>date range</li>
 * <li>amount range</li>
 * </ul>
 */
@DocumentedActivity("List account's statements")
public class ListStatementsActivity extends Activity {

    public static final String ID = "accounts.listStatements";

    public interface InParams {

        @DocumentedParam(type = Account.class, description = "Account Id")
        String ID_ACCOUNT = "idAccount";
        @DocumentedParam(type = DateField.class, description = "Date from to be included")
        String DATE_FROM = "dateFrom";
        @DocumentedParam(type = DateField.class, description = "Date to to be included")
        String DATE_TO = "dateTo";
        @DocumentedParam(type = String.class, description = "Page number to be displayed")
        String PAGE_NUMBER = "pageNumber";
        @DocumentedParam(type = Amount.class, description = "Min amount to be included")
        String MIN_AMOUNT = "minAmount";
        @DocumentedParam(type = Amount.class, description = "Max amount to be included")
        String MAX_AMOUNT = "maxAmount";
        @DocumentedParam(type = String.class, description = "Reference to be included")
        String REFERENCE = "reference";
        @DocumentedParam(type = String[].class, description = "Channels to be included")
        String CHANNELS = "channels";
        @DocumentedParam(type = String.class, description = "Check to be included")
        String CHECK = "check";
        @DocumentedParam(type = String.class, description = "Statement max count to be displayed")
        String MAX_COUNT = "maxCount";
    }

    public interface OutParams {

        @DocumentedParam(type = String[].class, description = "List of statements")
        String STATEMENTS = "statements";
        @DocumentedParam(type = String.class, description = "Availability to display more statements")
        String MORE_STATEMENTS = "moreStatements";
        @DocumentedParam(type = String.class, description = "Displayed page number")
        String PAGE_NUMBER = "pageNumber";
        @DocumentedParam(type = String.class, description = "Displayed currency")
        String CURRENCY = "currency";
        @DocumentedParam(type = String.class, description = "Displayed account's label")
        String ACCOUNT_LABEL = "accountLabel";
        @DocumentedParam(type = String.class, description = "Displayed statements total count")
        String TOTAL_COUNT = "totalCount";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String idAccount = request.getParam(InParams.ID_ACCOUNT, String.class);
            Product product = Administration.getInstance().readProduct(idAccount, request.getIdEnvironment());
            Account account = CoreAccountConnectorOrchestrator.readAccount(request.getIdTransaction(), product);
            Integer maxCount = request.getParam(InParams.MAX_COUNT, Integer.class);
            Integer pageNumber = request.getParam(InParams.PAGE_NUMBER, Integer.class);
            Integer movementsPerPage = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "accounts.statementsPerPage", 10);
            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
            boolean moreStatements = false;

            if (maxCount != null) {
                movementsPerPage = maxCount;
            }

            if (pageNumber == null) {
                pageNumber = 1;
            }

            Integer offset = movementsPerPage * (pageNumber - 1);

            if (request.getParam(InParams.DATE_TO, Date.class) == null && request.getParam(InParams.DATE_FROM, Date.class) == null) {
                movementsPerPage = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "accounts.details.filter.lastN", 10);
            }

            StatementResult statementResult = ListStatementsActivity.searchStatements(request, product, movementsPerPage + 1, offset);
            List<Statement> statements = statementResult.getStatementList();
            //get total count
            int totalCount = CoreAccountConnectorOrchestrator.countMovements(product.getExtraInfo(), statementResult);

            if (statements.size() > movementsPerPage) {
                moreStatements = true;
                statements.remove(statements.size() - 1);
            }

            response.putItem(OutParams.MORE_STATEMENTS, moreStatements);
            response.putItem(OutParams.PAGE_NUMBER, pageNumber);
            response.putItem(OutParams.STATEMENTS, statements);
            response.putItem(OutParams.CURRENCY, account.getCurrency());
            response.putItem(OutParams.ACCOUNT_LABEL, pLabeler.calculateLabel(account));
            response.putItem(OutParams.TOTAL_COUNT, totalCount);
            response.setReturnCode(OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(IO_ERROR, ex);
        }

        return response;
    }

    public static StatementResult searchStatements(Request request, Product product, Integer movementsPerPage, Integer offset) throws BackendConnectorException {
        Date dateFrom = request.getParam(InParams.DATE_FROM, Date.class);
        Date dateTo = request.getParam(InParams.DATE_TO, Date.class);
        Double minAmount = request.getParam(InParams.MIN_AMOUNT, Double.class);
        Double maxAmount = request.getParam(InParams.MAX_AMOUNT, Double.class);
        String reference = request.getParam(InParams.REFERENCE, String.class);
        String[] channels = request.getParam(InParams.CHANNELS, String[].class);
        Integer check = request.getParam(InParams.CHECK, Integer.class);

        return CoreAccountConnectorOrchestrator.statements(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo(), dateFrom, dateTo, minAmount, maxAmount, reference, channels, check, movementsPerPage, offset);
    }
}
