package com.technisys.omnichannel.client.activities.administration.common;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.client.domain.comparators.ProductComparator;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.administration.permissions.AdminUIPermissionHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static com.technisys.omnichannel.core.utils.MapUtils.addElementToMapOfLists;

public class Permissions {

    private Permissions() {
        throw new IllegalStateException("Utility class");
    }

    private static final String[] EXCLUDED_PERMISSIONS = new String[]{
            Constants.ADMINISTRATION_VIEW_PERMISSION,
            Constants.ADMINISTRATION_MANAGE_PERMISSION
    };

    public static Map<String, Object> buildPermissionsMap(Group group, final int idEnvironment) throws IOException {
        Map<String, Object> permissions = new HashMap<>();
        List<GroupPermission> groupPermissions = AccessManagementHandlerFactory.getHandler().getGroupPermissions(group.getIdGroup());
        List<Product> products = Administration.getInstance()
                .listEnvironmentProducts(idEnvironment, null, -1, -1, null)
                .getElementList();
        Map<String, List<String>> productsMap = new HashMap<>();
        Map<String, List<String>> genericsMap = new HashMap<>();
        Map<String, List<String>> allPermissions = new HashMap<>();

        groupPermissions.forEach(permission -> {
            addElementToMapOfLists(allPermissions, permission.getIdPermission(), permission.getTarget());

            if (Authorization.TARGET_NONE.equals(permission.getTarget())
                    || permission.getTarget().startsWith(Authorization.GENERIC_PRODUCT_TYPE_PREFIX)) {
                addElementToMapOfLists(genericsMap, permission.getIdPermission(), permission.getTarget());
            } else {
                // doing a filter on products with bad data or deleted
                int index = products.indexOf(new Product(permission.getTarget(), ""));

                if (index >= 0) {
                    addElementToMapOfLists(productsMap, permission.getIdPermission(), permission.getTarget());
                }
            }
        });

        permissions.put("generics", genericsMap);
        permissions.put("products", productsMap);
        permissions.put("allPermissions", allPermissions);

        return permissions;
    }

    public static List<Product> listProducts(final int idEnvironment, final String lang) throws IOException {
        ProductLabeler labeler = CoreUtils.getProductLabeler(lang);
        List<Product> products = Administration.getInstance()
                .listEnvironmentProducts(idEnvironment, null, -1, -1, null).getElementList();
        List<Product> processedProducts = new ArrayList();

        Collections.sort(products, new ProductComparator());

        for (Product product : products) {
            String label = labeler.calculateLabel(product);

            product.setLabel(label);

            if (Constants.PRODUCT_TC_KEY.equals(product.getProductType())) {
                processedProducts.add(new CreditCard(product));
            } else {
                processedProducts.add(product);
            }
        }

        return processedProducts;
    }

    public static Set<UIAdminItem> listUIPermissions(final int idEnvironment, final String lang) throws IOException {
        return AdminUIPermissionHandler.getInstance()
                .generateAdminLayout(idEnvironment, lang, Arrays.asList(EXCLUDED_PERMISSIONS));
    }

    public static boolean isAdministrationPermission(String permission) {
        return Constants.ADMINISTRATION_VIEW_PERMISSION.equals(permission)
                || Constants.ADMINISTRATION_MANAGE_PERMISSION.equals(permission);
    }

    public static List<GroupPermission> list(int groupId) throws IOException {
        return AccessManagementHandlerFactory.getHandler()
                .getGroupPermissions(groupId)
                .stream()
                .filter(permission -> isAdministrationPermission(permission.getIdPermission()))
                .collect(Collectors.toList());
    }

    public static List<GroupPermission> listDefault(int groupId) throws IOException {
        return ConfigurationFactory.getInstance()
                .getList(Configuration.PLATFORM, Constants.DEFAULT_GROUP_PERMISSIONS_KEY)
                .stream()
                .map(id -> new GroupPermission(groupId, Authorization.TARGET_NONE, id))
                .collect(Collectors.toList());
    }

    public static List<String> filterAdminUsers(int idEnvironment, List<User> users) throws IOException {
        List<String> adminUsers = new ArrayList<>();

        for (User user : users) {
            if (Authorization.hasPermission(user.getIdUser(), idEnvironment, null, Constants.ADMINISTRATION_VIEW_PERMISSION)) {
                adminUsers.add(user.getIdUser());
            }
        }

        return adminUsers;
    }

    public static void addIncomingPermissions(int groupId, Map<String, List<String>> newPermissions, List<GroupPermission> permissions) {
        newPermissions.entrySet().forEach(entry -> {
            String idPermission = entry.getKey();

            if (!isAdministrationPermission(idPermission)) {
                entry.getValue().forEach(target -> {
                    GroupPermission newPermission = new GroupPermission(groupId, target, idPermission);

                    if (!permissions.contains(newPermission)) {
                        permissions.add(newPermission);
                    }
                });
            }
        });
    }

    public static void validatePermissions(Map<String, String> result, List<String> envProducts, Map<String, List<String>> newPermissions) {
        if(newPermissions != null) {
            newPermissions.entrySet().forEach(entry ->
                    entry.getValue().forEach(target -> {
                        if (!StringUtils.startsWith(target, Authorization.GENERIC_PRODUCT_TYPE_PREFIX) && !StringUtils.equals(target, Authorization.TARGET_NONE) && !envProducts.contains(target)) {
                            result.put("NO_FIELD", "administration.groups.permissionNotExist");
                        }
                    })
            );
        } else {
            result.put("NO_FIELD", "administration.groups.permissionsRequired");
        }
    }
}
