/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.restrictions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.CalendarLoginRestriction;
import com.technisys.omnichannel.core.domain.IAccessRestriction;
import com.technisys.omnichannel.core.domain.IPAccessEnvironment;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.environments.EnvironmentHandler;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.lang3.StringUtils;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Jhossept
 */
@DocumentedActivity("List of restrictions for a given user and/or the current environment")
public class ListAccessRestrictionsActivity extends Activity {

    public static final String ID = "administration.restrictions.list";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "ID of the user to get restrictions")
        String ID_USER = "idUser";

        @DocumentedParam(type = Boolean.class, description = "Specify if should include environment restrictions with the user ones")
        String INCLUDE_ENVIRONMENT = "includeEnvironment";

    }

    public interface OutParams {
        @DocumentedParam(type = List.class, description = "Collection with the restrictions")
        String RESTRICTIONS = "restrictions";

        @DocumentedParam(type = List.class, description = "Username just in case of it's called from user restrictions")
        String USER_NAME = "userName";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        List<IAccessRestriction> listRestrictionsToReturn = new ArrayList<>();
        String userName = "";
        try {
            int idEnvironment = request.getIdEnvironment();
            String idUser = request.getParam(InParams.ID_USER, String.class);

            //Get Calendar Restrictions
            PaginatedList<CalendarLoginRestriction> calendarRestrictionsByEnv = EnvironmentHandler.listCalendarLoginRestrictions(idEnvironment, null, 0, 0, false);
            listRestrictionsToReturn.addAll(calendarRestrictionsByEnv.getElementList());
            if (!StringUtils.isBlank(idUser)) {
                PaginatedList<CalendarLoginRestriction> calendarRestrictionsByUsr = EnvironmentHandler.listCalendarLoginRestrictions(idEnvironment, idUser, 0, 0, false);
                listRestrictionsToReturn.addAll(calendarRestrictionsByUsr.getElementList());
                PaginatedList<IPAccessEnvironment> listIps = Administration.getInstance().listAllowedIPsByEnvironmentOrUser(idEnvironment, 0, 0, "", idUser);
                listRestrictionsToReturn.addAll(listIps.getElementList());

                //Get the username
                User user = AccessManagementHandlerFactory.getHandler().getUser(idUser);
                if(user != null){
                    userName = user.getFullName();
                }
            } else {
                PaginatedList<IPAccessEnvironment> listIps = Administration.getInstance().listAllowedIPs(idEnvironment, 0, 0, "");
                listRestrictionsToReturn.addAll(listIps.getElementList());
            }
        } catch (Exception e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
        response.putItem(OutParams.USER_NAME, userName);
        response.putItem(OutParams.RESTRICTIONS, listRestrictionsToReturn);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }
}