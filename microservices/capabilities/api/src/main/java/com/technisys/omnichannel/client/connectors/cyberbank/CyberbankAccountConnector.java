/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.AccountStatus;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.ProductType;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.StatementResult;
import com.technisys.omnichannel.client.connectors.cyberbank.utils.CoreProductUtils;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.client.handlers.notes.NotesHandler;
import com.technisys.omnichannel.client.utils.JsonTemplateUtils;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class CyberbankAccountConnector extends CyberbankCoreConnector {
    private static final Logger LOG = LoggerFactory.getLogger(CyberbankAccountConnector.class);

    private static final String TRANSACTION_DEBIT_TYPE = "127030";
    private static final String TRANSACTION_CREDIT_TYPE = "127031";
    private static final String TRANSACTION_DEFAULT_DATE_FROM = "20000323000000000";
    private static final String TRANSACTION_DEFAULT_DATE_TO = "20200323000000000";



    /**
     * Create an account on Cyberbank core for a given customer, the type of the account is given in configuration
     *
     * @param idCustomer
     * @return
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<String> create(String idCustomer) throws CyberbankCoreConnectorException {
        String productCode = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "enrollment.cyberbank.account.productId", "01000");
        String subProductCode = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "enrollment.cyberbank.account.subProductId", "10001");
        String currency = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "enrollment.cyberbank.account.currency", "1");

        return create(idCustomer, productCode, subProductCode, currency);
    }

    /**
     * Create an account on Cyberbank core for a given customer
     *
     * @param idCustomer
     * @return The account number created
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<String> create(String idCustomer, String productCode, String subProductCode, String currency) throws CyberbankCoreConnectorException {
        try {
            CyberbankCoreConnectorResponse<String> response = new CyberbankCoreConnectorResponse<>();
            String serviceName = "Srv - A1";

            ProductType productType = CyberbankConsolidateConnector.getProductType(productCode, subProductCode);
            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("insertAccountAutomatic", true)
                    .addRequestParameter("currencyCodeId", currency)
                    .addRequestParameter("branchId", "1")
                    .addRequestParameter("customerId", idCustomer)
                    .addRequestParameter("subproductId", productType.getIdSubProduct())
                    .addRequestParameter("productId", productType.getIdProduct());

            // TODO This must be remove or adjust with core changes
            cyberbankCoreRequest.addCredential("bankId", "499");

            LOG.info("{}: Request {}", serviceName, URL);

            String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(serviceName, jsonRequest);

            if (callHasError(serviceResponse)) {
                processErrors(serviceName, serviceResponse, response);
            } else {
                JSONObject accountInfo = serviceResponse.getJSONObject("out.account");
                String accountNumber = "";
                if (accountInfo.has("OperationId")) {
                    accountNumber = accountInfo.getString("OperationId");
                } else if (accountInfo.has("operationId")) {
                    accountNumber = accountInfo.getString("operationId");
                }

                response.setData(accountNumber);
            }
            return response;
        } catch (IOException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    /**
     * List account movements
     */
    public static CyberbankCoreConnectorResponse<StatementResult> statements(String idAccount, String extraInfo, Date dateFrom, Date dateTo, Integer movementsPerPage, Integer offset) throws CyberbankCoreConnectorException {

        CyberbankCoreConnectorResponse<StatementResult> response = new CyberbankCoreConnectorResponse<>();

        String serviceName = "Srv - massiveSelectTxnPagination";
        CyberbankCoreRequest request = new CyberbankCoreRequest("massiveSelectTxnPagination", true)
                .addRequestParameter("dateFrom", dateFrom != null ? CyberbankCoreConnector.formatDate(dateFrom) : TRANSACTION_DEFAULT_DATE_FROM)
                .addRequestParameter("dateTo", dateTo != null ? CyberbankCoreConnector.formatDate(dateTo) : TRANSACTION_DEFAULT_DATE_TO)
                .addRequestParameter("accountId", CoreProductUtils.getPhysicAccountId(extraInfo))
                .addRequestParameter("initRow", offset)
                .addRequestParameter("maxElementsBlockAmount", movementsPerPage);

        LOG.info("{}: Request {}", serviceName, URL);

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(request.getJSON(), REQ_TEMPLATE_PATH + request.getTransactionId());
        JSONObject serviceResponse = call(serviceName, jsonRequest);

        if (callHasError(serviceResponse)) {
            processErrors(serviceName, serviceResponse, response);
        } else {
            ArrayList<Statement> statements = new ArrayList<>();
            JSONObject jsonTotalList =  serviceResponse.getJSONObject("out.txn_list");
            int totalElementsAmount = 0;
            if(!jsonTotalList.isNull("collection") && jsonTotalList.has("collection") ){

                JSONArray collection = serviceResponse.getJSONObject("out.txn_list").getJSONArray("collection");
                totalElementsAmount =  serviceResponse.getJSONObject("out.pagination_info").getInt("totalElementsAmount");
                statements = new ArrayList<>();
                for (int i = 0; i < collection.length(); i++) {
                    statements.add(processStatement(idAccount, collection.getJSONObject(i)));
                }
                statements.sort((o1, o2) -> o1.getDate().compareTo(o2.getDate()) * -1);
            }
            response.setData(new StatementResult(totalElementsAmount, statements));
        }
        return response;
    }

    /**
     * Read an account detail on Cyberbank core
     *
     * @return The account detail
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<Account> read(Product product) throws CyberbankCoreConnectorException {
        try {
            CyberbankCoreConnectorResponse<Account> response = new CyberbankCoreConnectorResponse<>();
            String serviceName = "Srv - A2";

            String extraInfo = product.getExtraInfo();
            String productId = ProductUtils.getBackendID(extraInfo);
            ProductType productType = CyberbankConsolidateConnector.getProductType(productId, CoreProductUtils.getSubProductId(extraInfo));
            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("singleSelectAccount", true)
                    .addRequestParameter("productId", productType.getProductCode())
                    .addRequestParameter("operationId", ProductUtils.getNumber(extraInfo))
                    .addRequestParameter("currencyCodeId", CoreProductUtils.getCurrencyCode(extraInfo))
                    .addRequestParameter("subproductId", productType.getIdSubProduct())
                    .addRequestParameter("branchId", CoreProductUtils.getBranch(extraInfo));

            LOG.info("{}: Request {}", serviceName, URL);

            String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(serviceName, jsonRequest);

            if (callHasError(serviceResponse)) {
                processErrors(serviceName, serviceResponse, response);
            } else {
                response.setData(processServiceResponse(serviceResponse, product));
            }
            return response;
        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    /**
     * Read Statement on cyberbank core
     * @param idStatement
     * @return Object Statement with info Transaction
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<Statement> readStatement(int idStatement, String idAccount) throws CyberbankCoreConnectorException{
        try {
            CyberbankCoreConnectorResponse<Statement> response = new CyberbankCoreConnectorResponse<>();
            String serviceName = "Srv - Statement";

            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("singleSelectLog_Mov", true);
            cyberbankCoreRequest.addRequestParameter("idMov", idStatement);

            LOG.info("{}: Request {}", serviceName, URL);
            String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(serviceName, jsonRequest);

            if (callHasError(serviceResponse)) {
                processErrors(serviceName, serviceResponse, response);
            } else {
                response.setData(processStatement(idAccount, serviceResponse.getJSONObject("out.log_mov")));
            }

            return response;
        } catch (CyberbankCoreConnectorException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    private static Account processServiceResponse(JSONObject response, Product product) throws CyberbankCoreConnectorException {
        try {
            if (response.getJSONObject("out.account") == null) {
                return null;
            }

            JSONObject serviceAccount = response.getJSONObject("out.account");
            Account account = new Account(product);

            return processAccount(account, serviceAccount);

        } catch (JSONException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    private static Account processAccount(Account account, JSONObject serviceAccount) {

        Double balance = serviceAccount.has("balanceToday") ? Double.parseDouble(serviceAccount.get("balanceToday").toString()) : 0.0;
        account.setBalance(balance);

        account.setNationalIdentifier(serviceAccount.has("bukNumber") ? serviceAccount.get("bukNumber").toString() : "");

        if(StringUtils.isEmpty(account.getOwnerName()) && account.getClient() != null && !StringUtils.isEmpty(account.getClient().getName())) {
                account.setOwnerName(account.getClient().getName());
        }
        return account;
    }

    private static Statement processStatement(String idAccount, JSONObject jsonObject) throws CyberbankCoreConnectorException {
        try{
            Statement statement = new Statement();

            if(jsonObject.has("dbCr")){
                //existen algunos registros en donde no viene especificado el tipo de transacción
                if (!jsonObject.isNull("dbCr") && "DB".equals(jsonObject.getString("dbCr"))) {
                    statement.setDebit(Double.parseDouble(jsonObject.getString("amount")));
                } else {
                    statement.setCredit(Double.parseDouble(jsonObject.getString("amount")));
                }
            }else{
                // for response service singleSelectLog_Mov
                String idTypeTransaction = jsonObject.getJSONObject("transactionReason").getJSONObject("transaction").get("id").toString();
                if(TRANSACTION_DEBIT_TYPE.equals(idTypeTransaction)){
                    statement.setDebit(Double.parseDouble(jsonObject.getString("amount")));
                }else if (TRANSACTION_CREDIT_TYPE.equals(idTypeTransaction)){
                    statement.setCredit(Double.parseDouble(jsonObject.getString("amount")));
                }
            }

            statement.setIdStatement(jsonObject.getInt("id"));
            //service singleSelectLog_Mov not return balanceAccum
            statement.setBalance((!jsonObject.isNull("balanceAccum") && jsonObject.has("balanceAccum"))? Double.parseDouble(jsonObject.getString("balanceAccum")): null);
            statement.setAccountCurrency("USD");
            statement.setChannel("frontend");
            statement.setIdAccount(idAccount);
            statement.setDate(CyberbankCoreConnector.parseDate(jsonObject.getString("businessDate")));
            statement.setValueDate(CyberbankCoreConnector.parseDate(jsonObject.getString("imputationTime")));
            statement.setConcept(!jsonObject.isNull("txnDescription") ? jsonObject.getString("txnDescription") : "");
            statement.setReference(!jsonObject.isNull("txnDescription") ? jsonObject.getString("txnDescription") : "");
            statement.setNote(NotesHandler.readNote(statement.getIdStatement(), idAccount));
            return statement;
        }
        catch (Exception e){
            throw new CyberbankCoreConnectorException(e);
        }
    }

    /**
     * List account on Cyberbank core
     *
     * @return The clients accounts list
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<List<Account>> list(String idTransaction, List<Client> clients) throws CyberbankCoreConnectorException {
        try {
            CyberbankCoreConnectorResponse<List<Account>> response = new CyberbankCoreConnectorResponse<>();
            String serviceName = "Srv - massiveSelectAccountAllFrom_Customer";

            for (Client client: clients) {
                CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("massiveSelectAccountAllFrom_Customer", true)
                        .addRequestParameter("customerId", client.getIdClient() );

                LOG.info("{}: Request {}", serviceName, URL);

                String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
                JSONObject serviceResponse = call(serviceName, jsonRequest);

                if (callHasError(serviceResponse)) {
                    processErrors(serviceName, serviceResponse, response);
                } else {
                    if(response.getData() == null){
                        response.setData(new ArrayList<>());
                    }
                    response.getData().addAll(processAccountList(serviceResponse));
                }
            }

            return response;
        } catch (CyberbankCoreConnectorException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    private static List<Account> processAccountList(JSONObject serviceResponse) {
        JSONArray collection = serviceResponse.getJSONObject("out.account_list").getJSONArray("collection");
        List<Account> accounts = new ArrayList<>();

        for (int i = 0; i < collection.length(); i++) {
            JSONObject jsonObject = collection.getJSONObject(i);
            Account account = new Account();

            Double balance = jsonObject.has("balanceToday") ? Double.parseDouble(jsonObject.get("balanceToday").toString()) : 0.0;
            account.setBalance(balance);

            String coreProductId = jsonObject.getJSONObject("product").getString("productId");
            String productType = coreProductId.equals("02000") ? Constants.PRODUCT_CC_KEY : Constants.PRODUCT_CA_KEY;
            String accountNumber = jsonObject.getString("operationId");
            account.setIdProduct(ProductUtils.generateIdProduct(accountNumber + productType));

            if (jsonObject.has("status")) {
                JSONObject statusObject = jsonObject.getJSONObject("status");
                AccountStatus accountStatus = new AccountStatus();
                accountStatus.setStatusCode(statusObject.getInt("id"));
                accountStatus.setDescription(statusObject.getString("nemotecnico"));
                accountStatus.setShortDescription(statusObject.getString("shortDesc"));
                accountStatus.setLongDescription(statusObject.getString("longDesc"));
                account.setStatus(accountStatus);
            }

            accounts.add(account);
        }
        return accounts;
    }

    /**
     * Get statement whit Boucher image
     * @param idStatement
     * @return
     * @throws IOException
     */

    public static Statement readNoteBoucher(int idStatement) throws IOException {
        //TODO implemets logic for get voucher from core
        Statement statement = new Statement();
        statement.setIdStatement(idStatement);
        statement.setVoucher(null);
        return statement;
    }

}
