/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session.legacy;

import com.google.common.io.ByteStreams;
import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.session.oauth.OAuthActivity;
import com.technisys.omnichannel.client.utils.LoggerUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.utils.UserResolverFromUsername;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

/**
 * First login step, use this to initiate a login process. You must continue
 * with other steps until you have an <code>_accessToken</code>.
 */
@AnonymousActivity
@DocumentedActivity("First login step")
public class LoginWithSecondFactorAndPasswordStep1Activity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(LoginWithSecondFactorAndPasswordStep1Activity.class);

    public static final String ID = "session.login.legacy.step1";

    private final String NEXT_ACTIVITY_IN_FLOW = LoginWithSecondFactorAndPasswordStep2Activity.ID;

    private static final ConcurrentMap<Integer, String> SECURITY_IMAGES = new ConcurrentHashMap<>();

    public interface InParams {

        // Used by API, not this activity
        @DocumentedParam(type = String.class, description = "Username who perform the request, e.g. &quot;bbeall&quot;, &quot;32525648&quot;")
        String USERNAME = "userEmail";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Token required to call the next step on this process")
        String EXCHANGE_TOKEN = "_exchangeToken";

        @DocumentedParam(type = String.class, description = "Security seal picture (base 64 encoded)")
        String SECURITY_SEAL = "_securitySeal";

        @DocumentedParam(type = String.class, description = "Security seal description, e.g. &quot;A man smiling&quot;")
        String SECURITY_SEAL_ALT = "_securitySealAlt";

        @DocumentedParam(type = String.class, description = "User has logged previously")
        String HAS_LOGGED = "hasLogged";

        @DocumentedParam(type = Boolean.class, description = "Enabled channels")
        String ENABLED_ASSISTANT = "enabledAssistant";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Response response = new Response(request);
            String username = request.getParam(InParams.USERNAME, String.class);

            // Se intenta traer el objeto User que anteriormente se traía en el request
            // Se hace aquí para evitar el mensaje de error enviado cuando el usuario no existía.
            User requestUser =
                    AccessManagementHandlerFactory.getHandler().getUser(UserResolverFromUsername.getIdUser(username));
            User user = requestUser == null ? AccessManagementHandlerFactory.getHandler().getUser(username) : requestUser;

            String idUser =  user == null ? username : user.getIdUser();

            // Default environment will be caught but not returned in order to comply with Assistant login
            int idEnvironment = user == null ? -1 : user.getIdDefaultEnvironment();

            EnvironmentUser envUser = Administration.getInstance().readEnvironmentUserInfo(idUser, idEnvironment);
            boolean enabledAssistant = (envUser != null && envUser.getEnabledChannels().contains("assistant"));

            String exchangeToken = ExchangeTokenHandler.create(request.getChannel(), request.getLang(), request.getIdUser(),
                    -1, request.getUserAgent(), request.getClientIP(), new String[]{NEXT_ACTIVITY_IN_FLOW, OAuthActivity.ID}); // Missing: Remove session.login.oauth when the ApiDocs migration to Swagger is done

            // Security Seal
            int imageNumber = user == null || user.getIdSeal() < 0 ? getRandomAvatar(idUser) : user.getIdSeal();
            String imageData = SECURITY_IMAGES.get(imageNumber);

            if (imageData == null) {
                imageData = "data:image/jpeg;base64,";
                try {
                    try (InputStream image = getClass().getResourceAsStream("/securityimages/" + imageNumber + ".jpg")) {
                        imageData += new String(Base64.encodeBase64(ByteStreams.toByteArray(image)));
                    }
                } catch (IOException e) {
                    // Si no se pudo cargar la imagen se devuelve un placeholder.
                    log.warn("No se pudo cargar la imagen, se devuelve un placeholder.", e);
                    imageData += "/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABfAAD/4QNtaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MzFDNzRBM0UyNzQ1RTIxMTg1MTZENUFENjhDQ0EyRkQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6REI2NUQ2MTI0NTM2MTFFMkJCRThCMjM5ODRBRjM2NTQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6REI2NUQ2MTE0NTM2MTFFMkJCRThCMjM5ODRBRjM2NTQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTBBMUE5QjEzNjQ1RTIxMTg1MTZENUFENjhDQ0EyRkQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzFDNzRBM0UyNzQ1RTIxMTg1MTZENUFENjhDQ0EyRkQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQECAQEBAQECAgICAgICAgICAgIDAgICAwMDAwMDBQUFBQUFBQUFBQUFBQUFAQEBAQIBAgMCAgMEBAMEBAUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQX/wAARCAB4AHgDAREAAhEBAxEB/8QAdgABAAEFAQEBAAAAAAAAAAAAAAIEBQcICQYDCgEBAAAAAAAAAAAAAAAAAAAAABAAAQQBAgMEBQsFAAAAAAAAAAECAwQFEQYhMRKxcjMHQSKSFLRhkTITU5NU1DZ2GFFDFWYXEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD9eNy5av2ZrlyeSxZsSOlmmler3ve9dV4qBTAAAAAAAAAAAAAAAVNO5aoWYblOeSvZryNlhmierHsexdU4oBTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACTE6nNavJXInzqBz+2p5ef9JzFiOR+LsZqfHzZy9ktwyyuda0khZJ67Ip39SvsN0TTTTX5EAyJ/F619tsT763+QAfxetfbbE++t/kAH8XrX22xPvrf5ADHe6/Lz/m2YrxxvxdfNQY+HOUclt6WVrquskzI/XfFA/qR9d2qaaaafKgHQF6dLnNTkjlT5lAiAAAAAAAAAnH4kffb2gaj+QXHdc6f6Zd+LxgHh945vcuZ3Pl7V/OZ7HSU8peqY+hjsrPjIMRBSlfGxrWQvY1ZNG6vkVNVXX0aIgbXeWGXy+c2Vib2be6fII67UfdcxI3ZCOlM+Jk6oiI3qe1qI/TgrkVU05Ae/A1K8/P1XB+zKXxeTA24k8STvu7QIAAAAAAAAAJx+JH329oGo/kH+q5/2Zd+LxgGxGX2Fs7PX/wDJ5bCMsX3KxZ54bc1NLnQmifWtikY1y6InrJo5fSoF7t28Vt7FS2rL6uJw2Jp6uXRIatOnXTREaif0TgiImqrwTVVAxJs/zqxm59wy4S7ipcDFdmVm2r1u42VMvprpHMzoYlWaRE6o2dTkd9Hq6tEcGN/Pz9Vwfsyl8XkwNuJPEk77u0CAAAAAAAAACcfiR99vaBpF5YbqxezcjdzWWdIsTNoWq1WtXb12shfmtY5Y4Im8EVz0Yq8VRERFcqoiKoF/w3nZuupuGbKbhZFb21fkayXBUa7Fn29XTg2SvKjWyWlanGZJF9bmxG8GgWbzE39Y35kPdqqzVtpY6fqo1ZGuhlzNqJeFudi6ORiL4MTk1T6TvWVEaGPpoWTMWN6LpwVrmr0vY9vFHNVOKK1eKKgFy3BnsxuGKvYzczbV3G4BmF9+/vX4KktqVksqcvrFbYRr1TmqdXp0QOg0niSd93aBAAAAAAAAABOPg9iryR7e0DnfLQt41/uGQrSVbtLSvYgnYrJYZok6XIqLx5oBAAAA+kVG3k3pj6FeW1cuItetXhYskkssqdLURE1XmoHQ+Ti96pyV7u0CAAAAAAAAAABUtuWmNRrJ5WtTkiO4IBL3+5+Jl9oB7/c/Ey+0A9/ufiZfaAi65ae1Wvnlc1eaK7goFMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqblSzQsz07kMlezXkfFNDK1WPY9i6KiooFMAAAAAAAAAAAAAABU06lm/Zgp04ZLFmxIyKGGJqve9710RERAP/9k=";
                }
                SECURITY_IMAGES.put(imageNumber, imageData);
            }

            // Sends TRUE if user does not exist or if user has previously logged.
            Boolean userHasLogged = user == null || user.getLastLogin() != null;

            response.setReturnCode(ReturnCodes.OK);
            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            response.putItem(OutParams.SECURITY_SEAL, imageData);
            response.putItem(OutParams.SECURITY_SEAL_ALT, "securitySeal.image.description." + imageNumber);
            response.putItem(OutParams.HAS_LOGGED, userHasLogged);
            response.putItem(OutParams.ENABLED_ASSISTANT, enabledAssistant);

            return response;

        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    protected static Integer getRandomAvatar(String alias) {
        if(StringUtils.isBlank(alias)){
            return 0;
        }
        String[] partition = alias.split("@");
        int valorAlias = partition[0].hashCode();

        if(valorAlias < 0)
            valorAlias = -valorAlias;
        while (valorAlias > 20){
            valorAlias = valorAlias/2;
        }

        return valorAlias;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        if(StringUtils.isBlank(request.getParam(InParams.USERNAME, String.class))){
            LoggerUtils.logWarningMessage( LoggerFactory.getLogger(LoginWithSecondFactorAndPasswordStep1Activity.class),"Invalid user login attempt.");
            result.put(ReturnCodes.USER_DOESNT_EXIST.toString(), "Invalid user submitted");
        }
        return result;
    }
}
