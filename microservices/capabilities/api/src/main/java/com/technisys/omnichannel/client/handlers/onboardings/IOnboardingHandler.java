/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */
package com.technisys.omnichannel.client.handlers.onboardings;

import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.domain.OnboardingDocument;
import com.technisys.omnichannel.core.domain.PaginatedList;

import java.io.IOException;
import java.util.Date;

public interface IOnboardingHandler {

    long create(Onboarding onboarding) throws IOException;

    void  updateInvitationCode(long idOnboarding, int idInvitationCode) throws IOException;

    void updateStatusByInvitationCode(int idInvitationCode, String status) throws IOException;

    void updateStatus(long idOnboarding, String status) throws IOException;

    void updateDocument (long idOnboarding, String documentType, String documentCountry, String documentNumber) throws IOException;

    void updateExtraInfo (long idOnboarding, String extraInfo) throws IOException;

    void updateEmailAndMobile (long idOnboarding, String email, String mobileNumber) throws IOException;

    Onboarding read(long idOnboarding) throws IOException;

    void createDocument (OnboardingDocument document) throws IOException;

    OnboardingDocument readDocument(long idOnboarding, String documentType) throws IOException;

    PaginatedList listOnboardings(String documentCountry, String documentType, String documentNumber, String email, Date creationDateFrom, Date creationDateTo, String status, String mobileNumber, String type, int offset, int limit, String orderBy) throws IOException;

}
