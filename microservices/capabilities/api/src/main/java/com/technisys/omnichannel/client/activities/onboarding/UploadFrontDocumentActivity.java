package com.technisys.omnichannel.client.activities.onboarding;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingDigital;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingSafeway;
import com.technisys.omnichannel.client.activities.onboarding.utils.OnboardingUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;


/**
 *
 */
@ExchangeActivity
@AnonymousActivity
@DocumentedActivity("Upload front document in Onboarding")
public class UploadFrontDocumentActivity extends Activity {

    public static final String ID = "onboarding.wizard.uploadFrontDocument";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Base64 document to save")
        String DOCUMENT_TO_SAVE = "_documentToSave";
        @DocumentedParam(type = Boolean.class, description = "Specify if the document is a passport")
        String IS_PASSPORT = "isPassport";
        @DocumentedParam(type = Boolean.class, description = "Specify is the picture is rotated")
        String ROTATE_PICTURE = "rotatePicture";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Exchange Token")
        String EXCHANGE_TOKEN = "_exchangeToken";
        @DocumentedParam(type = String.class, description = "Specify is must skip the back side")
        String SKIP_BACK_DOCUMENT_STEP = "skipBackDocumentStep";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response;
        if("digital".equals(ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "onboarding.mode", "digital"))) {
            response = new OnboardingDigital().doUploadFrontDocumentExecute(request);
        } else {
            response = new OnboardingSafeway().doUploadFrontDocumentExecute(request);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return OnboardingUtils.validateOnboardingImageLength(request.getParam(InParams.DOCUMENT_TO_SAVE, String.class));
    }

}
