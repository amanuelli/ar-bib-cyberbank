/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.CurrencyExchange;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.NumberUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author sbarbosa
 */
public class CurrencyUtils {

    private CurrencyUtils(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    public static String getCurrencyLabel(String currencyCode, String lang) {
        return I18nFactory.getHandler().getMessage("currency.label." + currencyCode, lang);
    }

    public static String generateExchangeRateText(double rate, String currencyCodeFrom, String currencyCodeTo, String lang) {
        HashMap<String, String> fillers = new HashMap<>();
        fillers.put("RATE", NumberUtils.formatDecimal(rate, true, 0, 3, lang));
        fillers.put("CURRENCY_FROM", getCurrencyLabel(currencyCodeFrom, lang));
        fillers.put("CURRENCY_TO", getCurrencyLabel(currencyCodeTo, lang));
        return I18nFactory.getHandler().getMessage("transactions.ticket.quotation", lang, fillers);
    }

    public static double convert(String fromCurrency, String toCurrency, double amount, String idTransaction, String productGroupId) throws BackendConnectorException, IOException {
        List<CurrencyExchange> listRates = RubiconCoreConnectorC.listExchangeRates(idTransaction, productGroupId);
        CurrencyExchange currencyExchange = null;
        double result = amount;
        String masterCurrency = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency");

        for (int i = 0; i < listRates.size() && currencyExchange == null; i++) {
            CurrencyExchange currentRate = listRates.get(i);
            if (StringUtils.equals(currentRate.getBaseCurrencyCode(), fromCurrency) && StringUtils.equals(currentRate.getTargetCurrencyCode(), toCurrency)) {
                currencyExchange = currentRate;
            }
        }

        if (currencyExchange != null) {
            if (masterCurrency.equals(fromCurrency)) {
                result *= currencyExchange.getPurchase();
            } else {
                result *= currencyExchange.getSale();
            }
        }

        return result;
    }
}
