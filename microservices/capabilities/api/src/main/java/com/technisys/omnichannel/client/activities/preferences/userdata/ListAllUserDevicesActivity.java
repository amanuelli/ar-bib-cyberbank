package com.technisys.omnichannel.client.activities.preferences.userdata;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.communications.pushnotifications.ListUserDevicesActivity;
import com.technisys.omnichannel.client.domain.ClientUserDevice;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.UserDevice;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Activity to list all devices user has logged id
 */
@DocumentedActivity("List all user devices activity")
public class ListAllUserDevicesActivity extends Activity {

    public static final String ID = "preferences.userData.listAllUserDevices";

    public interface OutParams {

        @DocumentedParam(type = List.class, description = "User's device list")
        String LIST_USER_DEVICES = "listUserDevices";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            List<UserDevice> listUserDevices = AccessManagementHandlerFactory.getHandler().listUserDevices(request.getIdUser(), false);
            List<ClientUserDevice> listClientUserDevices = new ArrayList<>();
            for (UserDevice userDevice : listUserDevices) {
                listClientUserDevices.add(new ClientUserDevice(userDevice));
            }
            response.putItem(ListUserDevicesActivity.OutParams.LIST_USER_DEVICES, listClientUserDevices);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
