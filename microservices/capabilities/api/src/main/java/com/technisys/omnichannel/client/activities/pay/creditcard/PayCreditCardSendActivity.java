/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.client.activities.pay.creditcard;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.NotificableActivity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Coordinate;
import com.technisys.omnichannel.client.domain.PayCreditCardDetail;
import com.technisys.omnichannel.client.utils.CurrencyUtils;
import com.technisys.omnichannel.client.utils.GeolocalizationUtils;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.NumberUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.*;

/**
 *
 */
@DocumentedActivity("Pay Credit Card")
public class PayCreditCardSendActivity extends Activity implements NotificableActivity {

    private static final Logger log = LoggerFactory.getLogger(PayCreditCardSendActivity.class);

    public static final String ID = "pay.creditcard.send";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Credit Card")
        String ID_CREDIT_CARD = "creditCard";

        @DocumentedParam(type = String.class, description = "Debit Account")
        String ID_DEBIT_ACCOUNT = "debitAccount";

        @DocumentedParam(type = String.class, description = "Amount")
        String AMOUNT = "amount";

        @DocumentedParam(type = String.class, description = "notification Emails")
        String NOTIFICATION_MAILS = "notificationEmails";

        @DocumentedParam(type = String.class, description = "notification Body")
        String NOTIFICATION_BODY = "notificationBody";

        @DocumentedParam(type = String.class, description = "value Date")
        String VALUE_DATE = "valueDate";

        @DocumentedParam(type = String.class, description = "localization")
        String LOCALIZATION = "localization";

    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "debit Amount")
        String DEBIT_AMOUNT = "debitAmount";

        @DocumentedParam(type = String.class, description = "rate")
        String RATE = "rate";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            Map map = request.getParam(InParams.ID_DEBIT_ACCOUNT, Map.class);
            String debitAccountValue = (String) map.get("value");
            Product debitProduct = Administration.getInstance().readProduct(debitAccountValue, request.getIdEnvironment());

            Account debitAccount = new Account(debitProduct);

            Map mapCC = request.getParam(InParams.ID_CREDIT_CARD, Map.class);
            String creditcardId = (String) mapCC.get("value");

            String creditcardBank;
            String creditcardNumber;
            Amount amount;

            Product creditProduct = Administration.getInstance().readProduct(creditcardId, request.getIdEnvironment());
            creditcardNumber = ProductUtils.getBackendID(creditProduct.getExtraInfo());
            creditcardBank = Constants.TECHNISYS_BANK_ID;
            amount = request.getParam(InParams.AMOUNT, Amount.class);

            Coordinate coordianates = GeolocalizationUtils.getCoordinatesFromParam(request.getParam(InParams.LOCALIZATION, Map.class));

            //For example, user dont allow geolocalization
            if(coordianates.getLatitude() == 0 && coordianates.getLongitude() == 0) {
                coordianates = GeolocalizationUtils.getLocalizationByIp(request.getClientIP());
            }

            PayCreditCardDetail detail = RubiconCoreConnectorTC.payCreditCardSend(request.getIdTransaction(), environment.getProductGroupId(), debitAccount,
                    creditcardNumber, creditcardBank,
                    amount.getCurrency(), amount.getQuantity(),
                    "", coordianates.getLatitude(), coordianates.getLongitude());

            if (detail.getReturnCode() == 0) {
                if (!StringUtils.equals(detail.getDebitAmount().getCurrency(), amount.getCurrency())) {
                    //Leo la transaccion
                    Transaction transaction = TransactionHandlerFactory.getInstance().readDetail(request.getIdTransaction());
                    transaction.getData().put(OutParams.RATE, CurrencyUtils.generateExchangeRateText(detail.getRate(), detail.getDebitAmount().getCurrency(), amount.getCurrency(), request.getLang()));
                    transaction.getData().put(OutParams.DEBIT_AMOUNT, detail.getDebitAmount());
                    transaction.getData().put("latitude", coordianates.getLatitude());
                    transaction.getData().put("longitude", coordianates.getLongitude());
                    // Actualizo los datos de respuesta del servicio en el data
                    TransactionHandlerFactory.getInstance().updateTransactionData(transaction, false);
                }

                try {
                    sendNotificationMails(request);
                    response.setReturnCode(ReturnCodes.OK);
                } catch (IOException | MessagingException e) {
                    log.warn("Error enviando notificaciones vía mail por pago de tarjeta " + request.getIdTransaction(), e);
                    response.setReturnCode(ReturnCodes.ERROR_SENDING_TRANSACTION_NOTIFICATIONS);
                }
            } else {
                LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

                /*
                 * 300 - cuenta debito invalida
                 * 301 - tarjeta invalido
                 * 302 - cuenta debito sin saldo suficiente
                 */
                switch (detail.getReturnCode()) {
                    case 300:
                        errors.put(InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.creditcard.debitAccount.invalid", request.getLang()));
                        break;
                    case 301:
                        errors.put(InParams.ID_CREDIT_CARD, I18nFactory.getHandler().getMessage("pay.creditcard.creditcard.invalid", request.getLang()));
                        break;
                    case 302:
                        errors.put(InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.creditcard.debitAccount.insufficientBalance", request.getLang()));
                        break;
                    default:
                        String key = "pay.creditcard.returnCode." + detail.getReturnCode();
                        String message = I18nFactory.getHandler().getMessage(key, request.getLang());
                        if (!StringUtils.isBlank(message) && !message.equals(key)) {
                            errors.put("NO_FIELD", message);
                        } else {
                            response.setReturnCode(ReturnCodes.BACKEND_SERVICE_ERROR);
                        }
                }
                response.setReturnCode(ReturnCodes.VALIDATION_ERROR);
                response.setData(errors);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (BackendConnectorException bcE) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, bcE);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            //ejecutamos validaciones del formulario y dejar aquí solo las especificas de la actividad

            Map<String, String> result = FormsHandler.getInstance().validateRequest(request);

            return result;
        } catch (SchedulerException ex) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    @Override
    public void sendNotificationMails(Transaction transaction) throws IOException, MessagingException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void sendNotificationMails(Request request) throws IOException, MessagingException {
        sendNotificationMails(request.getIdEnvironment(), request.getLang(), request.getValueDate(), request.getParameters());
    }

    private void sendNotificationMails(int idEnvironment, String lang, Date valueDate, Map<String, Object> params) throws IOException, MessagingException {
        List<String> mails = RequestParamsUtils.getValue(params.get(InParams.NOTIFICATION_MAILS), List.class);

        if (mails != null && mails.size() > 0) {
//            El dia ${DATE} a la hora ${HOUR} se ha enviado un pago a cuenta de la tarjeta ${CREDIT_CARD} por ${CURRENCY} ${AMOUNT} que se debitará de la cuenta ${DEBIT_ACCOUNT}.
//            <br/><br/>Comentarios del ordenante: ${MESSAGE}.
//            <br/><br/>Nota: Los comentarios del ordenante han sido redactados por el cliente que ha ordenado esta transacción. ${i18n:global.companyName} no se responsabiliza del contenido de los mismos.

            Environment env = Administration.getInstance().readEnvironment(idEnvironment);

            int defualtMinimumFractionDigits = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "defaultDecimal.minimum");
            int defaultMaximumFractionDigits = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "defaultDecimal.maximum");

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(lang);

            Map mapCC = RequestParamsUtils.getValue(params.get(InParams.ID_CREDIT_CARD), Map.class);
            String creditcardId = (String) mapCC.get("value");

            Amount amount;
            String creditcardLabel;

            Product creditProduct = Administration.getInstance().readProduct(creditcardId, idEnvironment);
            creditcardLabel = pLabeler.calculateShortLabel(creditProduct);
            amount = RequestParamsUtils.getValue(params.get(InParams.AMOUNT), Amount.class);

            Map map = RequestParamsUtils.getValue(params.get(InParams.ID_DEBIT_ACCOUNT), Map.class);
            String debitAccountValue = (String) map.get("value");

            Product debitProduct = Administration.getInstance().readProduct(debitAccountValue, idEnvironment);
            String debitaccount = pLabeler.calculateShortLabel(debitProduct);

            // Message Subject
            String subject = I18nFactory.getHandler().getMessage("pay.creditcard.notificationEmail.subject", lang);

            // User Notification Body
            HashMap<String, String> fillers = new HashMap<>();
            String notificationBody = RequestParamsUtils.getValue(params.get(InParams.NOTIFICATION_BODY), String.class);
            if (StringUtils.isBlank(notificationBody)) {
                notificationBody = "";
            } else {
                fillers.put("MESSAGE", notificationBody);
                notificationBody = I18nFactory.getHandler().getMessage("pay.creditcard.notificationEmail.body.commentSection", lang, fillers);
            }

            // Message Body
            fillers = new HashMap<>();
            fillers.put("DATE", DateUtils.formatShortDate(valueDate));
            fillers.put("HOUR", DateUtils.formatTime(valueDate));
            fillers.put("CREDIT_CARD", creditcardLabel);
            fillers.put("DEBIT_ACCOUNT", debitaccount);
            fillers.put("ENVIRONMENT_NAME", StringUtils.defaultString(env.getName()));
            fillers.put("CURRENCY", I18nFactory.getHandler().getMessage("currency.label." + amount.getCurrency(), lang));
            fillers.put("AMOUNT", NumberUtils.formatDecimal(amount.getQuantity(), true, defualtMinimumFractionDigits, defaultMaximumFractionDigits, lang));
            fillers.put("NOTIFICATION_BODY", notificationBody);

            String body = I18nFactory.getHandler().getMessage("pay.creditcard.notificationEmail.body", lang, fillers);

            NotificationsHandlerFactory.getHandler().sendAsyncEmail(subject, body, mails, null, lang, true);
        }
    }

}
