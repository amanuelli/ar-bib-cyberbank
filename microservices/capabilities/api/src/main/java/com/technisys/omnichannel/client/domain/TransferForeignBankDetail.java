/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.domain.Amount;

public class TransferForeignBankDetail {
    private Amount tax;

    public TransferForeignBankDetail() {
    }

    public TransferForeignBankDetail(Amount tax) {
        this.tax = tax;
    }

    public Amount getTax() {
        return tax;
    }

    public void setTax(Amount tax) {
        this.tax = tax;
    }
}