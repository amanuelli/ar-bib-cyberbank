/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session.oauth;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.activities.session.legacy.AcceptGeneralConditionsActivity;
import com.technisys.omnichannel.core.Request;

import java.util.List;
import java.util.Map;

/**
 * 
 */
@DocumentedActivity("Login step 4")
public class OauthAcceptGeneralConditionsActivity extends AcceptGeneralConditionsActivity {
    public static final String ID = "session.login.oauth.step4";

    public interface InParams {
        @DocumentedParam(type = Boolean.class, description = "True for accept conditions")
        String ACCEPT_CONDITIONS = "acceptConditions";

        @DocumentedParam(type = Integer.class, description = "Environment id to accept conditions")
        String ID_ENVIRONMENT_TO_ACCEPT_CONDITIONS = "idEnvironmentToAcceptConditions";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "User's identifier")
        String USERNAME = "username";
        @DocumentedParam(type = String.class, description = "User's full name, e.g. &quot;Brittny Beall")
        String USER_FULLNAME = "userFullName";
        @DocumentedParam(type = String.class, description = "User's default Avatar identifier")
        String DEFAULT_AVATAR_ID = "defaultAvatarId";
        @DocumentedParam(type = Integer.class, description = "User's security seal identifier")
        String SECURITY_SEAL = "_securitySeal";
        @DocumentedParam(type = Integer.class, description = "User's active environment identifier")
        String ACTIVE_ENVIRONMENT_ID = "activeIdEnvironment";
        @DocumentedParam(type = String.class, description = "User's active environment name")
        String ACTIVE_ENVIRONMENT_NAME = "activeEnvironmentName";
        @DocumentedParam(type = String.class, description = "User's active environment administration scheme")
        String ADMINISTRATION_SCHEME = "administrationScheme";
        @DocumentedParam(type = Map.class, description = "Map of available environments: &lt;identifier, name&gt;")
        String ENVIRONMENTS = "environments";
        @DocumentedParam(type = Map.class, description = "Map of available permissions: &lt;identifier, boolean indicator&gt;")
        String PERMISIONS = "permissions";
        @DocumentedParam(type = List.class, description = "List of forms, those the user can interact with")
        String FORMS = "forms";
        @DocumentedParam(type = List.class, description = "List of activities identifiers, those the user most use")
        String FREQUENT_ACTIONS = "frequentActions";
        @DocumentedParam(type = String.class, description = "Default language, selected by the user, e.g. &quot;en&quot;")
        String LANGUAGE = "lang";

        @DocumentedParam(type = String.class, description = "Token required to call the authenticated activities")
        String ACCESS_TOKEN = "_accessToken";
        @DocumentedParam(type = String.class, description = "Token required to call the next step on this process")
        String PREVIOUS_LOGIN_INFO = "previousLoginInfo";
    }

    protected int getIdEnvironmentToAcceptConditions(Request request) {
        return  request.getParam(InParams.ID_ENVIRONMENT_TO_ACCEPT_CONDITIONS, Integer.class);
    }

}
