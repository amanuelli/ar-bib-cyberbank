/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

@DocumentedActivity("Delete users preview")
public class DeleteUsersPreviewActivity extends Activity {

    public static final String ID = "administration.users.delete.preview";

    public interface InParams {
        @DocumentedParam(type = String[].class, description = "User's ids list to delete")
        String USER_ID_LIST = "userIdList";
        @DocumentedParam(type = String[].class, description = "User's name list to delete")
        String USER_NAME_LIST = "userNameList";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return DeleteUsersActivity.validateFields(request);
    }

}
