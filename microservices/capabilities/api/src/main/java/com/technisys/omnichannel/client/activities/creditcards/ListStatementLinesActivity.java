/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.creditcards;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.StatementLine;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;
import java.util.List;

/**
 *
 */
public class ListStatementLinesActivity extends Activity {

    public static final String ID = "client.creditCard.listStatementLines";

    public interface InParams {

        String ID_CREDIT_CARD = "idCreditCard";
    }

    public interface OutParams {

        String STATEMENT_LINES = "statementLines";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            List<StatementLine> statementLines = RubiconCoreConnectorTC.listCreditCardStatementLines((String) request.getParam(InParams.ID_CREDIT_CARD, String.class));

            response.putItem(OutParams.STATEMENT_LINES, statementLines);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
