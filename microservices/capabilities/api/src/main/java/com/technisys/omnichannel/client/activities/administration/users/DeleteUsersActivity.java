/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;

@DocumentedActivity("Delete users")
public class DeleteUsersActivity extends Activity {

    public static final String ID = "administration.users.delete.send";

    public interface InParams {

        @DocumentedParam(type = String[].class, description = "User's ids list to delete")
        String USER_ID_LIST = "userIdList";
        @DocumentedParam(type = String[].class, description = "User's name list to delete")
        String USER_NAME_LIST = "userNameList";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String[] userIdList = request.getParam(InParams.USER_ID_LIST, String[].class);

            Administration admin = Administration.getInstance();

            List<Integer> groupsToDelete = new ArrayList<>();
            for (String idUser : userIdList) {
                if (!Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme())) {
                    List<Group> groups = AccessManagementHandlerFactory.getHandler().getGroups(idUser, null, request.getIdEnvironment(), -1, -1, null).getElementList();
                    if (CollectionUtils.size(groups) == 1) {
                        groupsToDelete.add(groups.get(0).getIdGroup());
                    }
                }

                admin.removeUserFromEnvironment(idUser, request.getIdEnvironment());

                User user = AccessManagementHandlerFactory.getHandler().getUser(idUser);
                // Si le des-asociamos el ambiente por defecto le asociamos uno nuevo
                if (user.getIdDefaultEnvironment() == request.getIdEnvironment()) {
                    List<Environment> environments = admin.listEnvironments(idUser);
                    if (!environments.isEmpty()) {
                        AccessManagementHandlerFactory.getHandler().updateUserDefaultEnvironment(idUser, environments.get(0).getIdEnvironment());
                    }
                }
            }

            if (!groupsToDelete.isEmpty()) {
                AccessManagementHandlerFactory.getHandler().deleteGroups(groupsToDelete);
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String[] userIdList = request.getParam(InParams.USER_ID_LIST, String[].class);
            String[] userNameList = request.getParam(InParams.USER_NAME_LIST, String[].class);

            List<String> environmentUsers = Administration.getInstance().listEnvironmentUserIds(request.getIdEnvironment());

            if (userIdList == null || userIdList.length == 0 || userNameList == null || userNameList.length == 0 || userIdList.length != userNameList.length) {
                result.put("NO_FIELD", "administration.users.mustSelectUser");
            } else {
                for (String userId : userIdList) {
                    if (StringUtils.isBlank(userId)) {
                        result.put("NO_FIELD", "administration.users.mustSelectUser");
                    } else {
                        User user = AccessManagementHandlerFactory.getHandler().getUser(userId);
                        if (user == null) {
                            result.put("NO_FIELD", "administration.users.idUserDoesntExists");
                        } else if (environmentUsers == null || (!environmentUsers.contains(userId))) {
                            throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                        } else if (Authorization.hasPermission(userId, request.getIdEnvironment(), null, Constants.ADMINISTRATION_VIEW_PERMISSION)) {
                            result.put("NO_FIELD", "administration.users.cantDeleteAdministrators");
                        }
                    }
                }
            }

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
