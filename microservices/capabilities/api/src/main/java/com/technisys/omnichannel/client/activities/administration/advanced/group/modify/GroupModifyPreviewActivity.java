package com.technisys.omnichannel.client.activities.administration.advanced.group.modify;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;

/**
 * Validates incoming form data. Use this to modify a new group (Preview)
 */
@DocumentedActivity("Modify group preview")
public class GroupModifyPreviewActivity extends Activity {

    public static final String ID = "administration.advanced.group.modify.preview";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "Id of group")
        String MODIFY_ID = "id";
        @DocumentedParam(type = String.class, description = "Name of group")
        String MODIFY_NAME = "name";
        @DocumentedParam(type = String.class, description = "Description of group")
        String MODIFY_DESCRIPTION = "description";
        @DocumentedParam(type = String.class, description = "Status of group")
        String MODIFY_STATUS = "status";
        @DocumentedParam(type = String[].class, description = "Users of group")
        String MODIFY_USERS = "users";
        @DocumentedParam(type = Map.class, description = "Permissions of group")
        String MODIFY_PERMISSIONS = "permissions";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return GroupModifyActivity.validateFields(request);
    }
}