/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.client.activities.pay.creditcard;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.PayCreditCardDetail;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 */
@DocumentedActivity("Pay Credit Card Preview")
public class PayCreditCardPreviewActivity extends Activity {

    public static final String ID = "pay.creditcard.preview";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Credit Card")
        String ID_CREDIT_CARD = "creditCard";

        @DocumentedParam(type = String.class, description = "Debit Account")
        String ID_DEBIT_ACCOUNT = "debitAccount";

        @DocumentedParam(type = String.class, description = "Amount")
        String AMOUNT = "amount";

        @DocumentedParam(type = String.class, description = "notification Emails")
        String NOTIFICATION_MAILS = "notificationEmails";

        @DocumentedParam(type = String.class, description = "notification Body")
        String NOTIFICATION_BODY = "notificationBody";

        @DocumentedParam(type = String.class, description = "value Date")
        String VALUE_DATE = "valueDate";

        @DocumentedParam(type = String.class, description = "localization")
        String LOCALIZATION = "localization";

    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Debit account Alias")
        String DEBIT_ACCOUNT_ALIAS = "debitAccountAlias";

        @DocumentedParam(type = String.class, description = "Debit amount")
        String DEBIT_AMOUNT = "debitAmount";

        @DocumentedParam(type = String.class, description = "Amount")
        String AMOUNT = "amount";

        @DocumentedParam(type = String.class, description = "Rate")
        String RATE = "rate";

        @DocumentedParam(type = String.class, description = "Credit Card Alias")
        String CREDIT_CARD = "creditCardAlias";

        @DocumentedParam(type = String.class, description = "Credit card bank")
        String CREDIT_CARD_BANK = "creditCardBank";

        @DocumentedParam(type = String.class, description = "Credit Card mark")
        String CREDIT_CARD_MARK = "creditCardMark";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            Map map = request.getParam(InParams.ID_DEBIT_ACCOUNT, Map.class);
            String debitAccountValue = (String) map.get("value");
            Product debitProduct = Administration.getInstance().readProduct(debitAccountValue, request.getIdEnvironment());

            Account debitAccount = new Account(debitProduct);

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            Map mapCC = request.getParam(InParams.ID_CREDIT_CARD, Map.class);
            String creditcardId = (String) mapCC.get("value");

            Product creditProduct = Administration.getInstance().readProduct(creditcardId, request.getIdEnvironment());
            String creditcardNumber = ProductUtils.getBackendID(creditProduct.getExtraInfo());
            String creditcardLabel = pLabeler.calculateShortLabel(creditProduct);
            String creditcardBank = Constants.TECHNISYS_BANK_ID;
            String creditCardMark = ProductUtils.getCreditCardBrand(ProductUtils.getNumber(creditProduct.getExtraInfo()), request.getLang());
            Amount amount = request.getParam(InParams.AMOUNT, Amount.class);
            
            PayCreditCardDetail detail = RubiconCoreConnectorTC.payCreditCardValidate(request.getIdTransaction(), environment.getProductGroupId(), debitAccount,
                    creditcardNumber, creditcardBank,
                    amount.getCurrency(), amount.getQuantity(),
                    "", 0, 0);

            if (detail.getReturnCode() == 0) {
                response.putItem(OutParams.DEBIT_AMOUNT, detail.getDebitAmount());
                response.putItem(OutParams.AMOUNT, amount);
                response.putItem(OutParams.CREDIT_CARD, creditcardLabel);
                response.putItem(OutParams.CREDIT_CARD_BANK, creditcardBank);
                response.putItem(OutParams.DEBIT_ACCOUNT_ALIAS, pLabeler.calculateShortLabel(debitProduct));
                response.putItem(OutParams.RATE, detail.getRate());
                response.putItem(OutParams.CREDIT_CARD_MARK, creditCardMark);

                response.setReturnCode(ReturnCodes.OK);
            } else {
                LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

                /*
                 * 300 - cuenta debito invalida
                 * 301 - tarjeta invalido
                 * 302 - cuenta debito sin saldo suficiente
                 */
                switch (detail.getReturnCode()) {
                    case 300:
                        errors.put(InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.creditcard.debitAccount.invalid", request.getLang()));
                        break;
                    case 301:
                        errors.put(InParams.ID_CREDIT_CARD, I18nFactory.getHandler().getMessage("pay.creditcard.creditcard.invalid", request.getLang()));
                        break;
                    case 302:
                        errors.put(InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.creditcard.debitAccount.insufficientBalance", request.getLang()));
                        break;
                    default:
                        String key = "pay.creditcard." + detail.getReturnCode();
                        String message = I18nFactory.getHandler().getMessage(key, request.getLang());
                        if (!StringUtils.isBlank(message) && !message.equals(key)) {
                            errors.put("NO_FIELD", message);
                        } else {
                            response.setReturnCode(ReturnCodes.BACKEND_SERVICE_ERROR);
                        }
                }
                response.setReturnCode(ReturnCodes.VALIDATION_ERROR);
                response.setData(errors);
            }

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (BackendConnectorException bcE) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, bcE);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new PayCreditCardSendActivity().validate(request);
        return result;
    }
}
