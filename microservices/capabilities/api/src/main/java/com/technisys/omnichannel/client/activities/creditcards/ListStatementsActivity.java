/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.creditcards;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.client.domain.StatementCreditCard;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 * List credit card movements.
 */
public class ListStatementsActivity extends Activity {

    public static final String ID = "client.creditCards.listStatements";

    public interface InParams {

        String ID_CREDITCARD = "idCreditCard";
        String DATE_FROM = "dateFrom";
        String DATE_TO = "dateTo";
        String PAGE_NUMBER = "pageNumber";
        String MIN_AMOUNT = "minAmount";
        String MAX_AMOUNT = "maxAmount";
        String CURRENCY = "currency";
        String CONCEPT = "concept";
        String MAX_COUNT = "maxCount";
    }

    public interface OutParams {

        String STATEMENTS = "statements";
        String MORE_STATEMENTS = "moreStatements";
        String PAGE_NUMBER = "pageNumber";
        String CURRENCY = "currency";
        String CREDITCARD_LABEL = "creditCardLabel";
        String TOTAL_COUNT = "totalCount";
        String TOTAL_CURRENT_PERIOD_COUNT = "totalCurrentPeriod";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Integer maxCount = request.getParam(InParams.MAX_COUNT, Integer.class);
            int movementsPerPage = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "creditCard.statementsPerPage", 10);
            if (maxCount != null) {
                movementsPerPage = maxCount;
            }

            List<StatementCreditCard> statements;

            String idCreditCard = request.getParam(InParams.ID_CREDITCARD, String.class);
            Date dateFrom = request.getParam(InParams.DATE_FROM, Date.class);
            Date dateTo = request.getParam(InParams.DATE_TO, Date.class);
            Integer pageNumber = request.getParam(InParams.PAGE_NUMBER, Integer.class);

            if (pageNumber == null) {
                pageNumber = 1;
            }

            int offset = movementsPerPage * (pageNumber - 1);

            Double minAmount = request.getParam(InParams.MIN_AMOUNT, Double.class);
            Double maxAmount = request.getParam(InParams.MAX_AMOUNT, Double.class);
            String currency = request.getParam(InParams.CURRENCY, String.class);
            String concept = request.getParam(InParams.CONCEPT, String.class);

            boolean moreStatements = false;

            Product product = Administration.getInstance().readProduct(idCreditCard, request.getIdEnvironment());
            CreditCard creditCard = RubiconCoreConnectorTC.readCreditCardDetails(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo());

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            String label = pLabeler.calculateLabel(creditCard);
            statements = RubiconCoreConnectorTC.listCreditCardStatements(request.getIdTransaction(), creditCard.getIdProduct(), product.getExtraInfo(), dateFrom, dateTo, minAmount, maxAmount, concept, currency, movementsPerPage + 1, offset);
            if (statements.size() > movementsPerPage) {
                moreStatements = true;
                statements.remove(statements.size() - 1);
            }
            //get total count and total current period count
            int totalCount = RubiconCoreConnectorTC.getTotalCreditCardStatements(product.getExtraInfo());
            int totalCurrentPeriod = RubiconCoreConnectorTC.getCurrentPeriodCreditCardStatements(product.getExtraInfo());
            //En caso de que se tengan tarjetas adicionales, es necesario mostrar de la tarjeta desde la cual
            //se realizó la transacción de forma amigable para el usuario
            for (StatementCreditCard statement : statements) {
                statement.setCardUsedLabel(ProductUtils.shortenCreditCardNumber(statement.getIdUsedCreditCard(), request.getLang()));
            }
            response.putItem(OutParams.CREDITCARD_LABEL, label);
            response.putItem(OutParams.MORE_STATEMENTS, moreStatements);
            response.putItem(OutParams.PAGE_NUMBER, pageNumber);
            response.putItem(OutParams.STATEMENTS, statements);
            response.putItem(OutParams.CURRENCY, creditCard.getAvailableBalanceCurrency());
            response.putItem(OutParams.TOTAL_COUNT, totalCount);
            response.putItem(OutParams.TOTAL_CURRENT_PERIOD_COUNT, totalCurrentPeriod);
            // Cargo el codigo de retorno
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
    public static List<StatementCreditCard> searchStatements(Request request, Product product, Integer offset) throws BackendConnectorException {
        Date dateFrom = request.getParam(InParams.DATE_FROM, Date.class);
        Date dateTo = request.getParam(InParams.DATE_TO, Date.class);
        Double minAmount = request.getParam(InParams.MIN_AMOUNT, Double.class);
        Double maxAmount = request.getParam(InParams.MAX_AMOUNT, Double.class);
        String currency = request.getParam(InParams.CURRENCY, String.class);
        String concept = request.getParam(InParams.CONCEPT, String.class);

        return RubiconCoreConnectorTC.listCreditCardStatements(
            request.getIdTransaction(),
            product.getIdProduct(),
            product.getExtraInfo(),
            dateFrom,
            dateTo,
            minAmount,
            maxAmount,
            concept,
            currency,
            ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "creditCards.export.maxStatements", 100),
            offset
        );
    }
}
