/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.deposits;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.transactions.ListTransactionsActivity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.Deposit;
import com.technisys.omnichannel.client.domain.StatementDeposit;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.Date;
import java.util.List;

/**
 *
 */
public class ListStatementsActivity extends Activity {

    public static final String ID = "deposits.listStatements";

    public interface InParams {

        String ID_DEPOSIT = "idDeposit";
        String DATE_FROM = "dateFrom";
        String DATE_TO = "dateTo";
        String PAGE_NUMBER = "pageNumber";
        String MIN_AMOUNT = "minAmount";
        String MAX_AMOUNT = "maxAmount";
        String REFERENCE = "reference";
        String FULL_LIST = "fullList";
    }

    public interface OutParams {

        String STATEMENTS = "statements";
        String MORE_STATEMENTS = "moreStatements";
        String PAGE_NUMBER = "pageNumber";
        String CURRENCY = "currency";
        String DEPOSIT_LABEL = "depositLabel";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int movementsPerPage = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "deposits.statementsPerPage", 10);

            List<StatementDeposit> statements;

            String idDeposit = request.getParam(InParams.ID_DEPOSIT, String.class);
            Date dateFrom = request.getParam(InParams.DATE_FROM, Date.class);
            Date dateTo = request.getParam(InParams.DATE_TO, Date.class);
            Integer pageNumber = request.getParam(ListTransactionsActivity.InParams.PAGE_NUMBER, Integer.class);

            if (pageNumber == null) {
                pageNumber = 1;
            }

            int offset = movementsPerPage * (pageNumber - 1);

            Double minAmount = request.getParam(InParams.MIN_AMOUNT, Double.class);
            Double maxAmount = request.getParam(InParams.MAX_AMOUNT, Double.class);
            String reference = request.getParam(InParams.REFERENCE, String.class);
            Boolean fullList = request.getParam(InParams.FULL_LIST, Boolean.class);

            boolean moreStatements = false;

            Product product = Administration.getInstance().readProduct(idDeposit, request.getIdEnvironment());
            Deposit deposit = RubiconCoreConnectorC.readDepositDetails(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo());

            if (dateFrom == null && dateTo == null) {
                statements = RubiconCoreConnectorC.listDepositsStatements(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo(), dateFrom, dateTo, minAmount, maxAmount, reference, movementsPerPage, offset);
                if (statements.size() > movementsPerPage) {
                    moreStatements = true;
                    statements.remove(statements.size() - 1);
                }
            } else {
                if (fullList) {
                    statements = RubiconCoreConnectorC.listDepositsStatements(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo(), dateFrom, dateTo, minAmount, maxAmount, reference, ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "deposits.export.maxStatements", 99999), 0);
                } else {
                    statements = RubiconCoreConnectorC.listDepositsStatements(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo(), dateFrom, dateTo, minAmount, maxAmount, reference, movementsPerPage + 1, offset);
                    if (statements.size() > movementsPerPage) {
                        moreStatements = true;
                        statements.remove(statements.size() - 1);
                    }
                }
            }

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            response.putItem(OutParams.MORE_STATEMENTS, moreStatements);
            response.putItem(OutParams.PAGE_NUMBER, pageNumber);
            response.putItem(OutParams.STATEMENTS, statements);
            response.putItem(OutParams.CURRENCY, deposit.getCurrency());
            response.putItem(OutParams.DEPOSIT_LABEL, pLabeler.calculateLabel(deposit));

            // Cargo el codigo de retorno
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

}
