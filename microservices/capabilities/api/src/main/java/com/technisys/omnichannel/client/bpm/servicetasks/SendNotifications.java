package com.technisys.omnichannel.client.bpm.servicetasks;

import com.technisys.omnichannel.client.utils.CurrencyUtils;
import com.technisys.omnichannel.client.utils.LoggerUtils;
import com.technisys.omnichannel.client.utils.MaskUtils;
import com.technisys.omnichannel.client.utils.TemplatingUtils;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.domain.FormTransaction;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.NumberUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.mail.MessagingException;
import org.apache.commons.lang3.StringUtils;
import org.camunda.bpm.engine.delegate.DelegateExecution;
import org.camunda.bpm.engine.delegate.JavaDelegate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Service Task para utilizar en los flujos que se encarga de enviar las
 * notificaciones via mail. El formulario debe tener los campos
 * notificationEmails y notificationBody
 *
 * @author fpena
 */
public class SendNotifications implements JavaDelegate {
    
    private static final Logger log = LoggerFactory.getLogger(SendNotifications.class);
    private static String idMessage= "forms.notificationEmail.";

    @Override
    public void execute(DelegateExecution execution) throws Exception {
        String idTransaction = (String) execution.getVariable(FormsHandler.WORKFLOW_VARIABLE_ID_TRANSACTION);
        LoggerUtils.logInfoMessage(log,"Sending notifications for transaction {0} from BPM Workflow", idTransaction);
        try {
            if (StringUtils.isNotBlank(idTransaction)) {
                String transactionStatus = TransactionHandlerFactory.getInstance().readTransactionStatus(idTransaction);
                FormTransaction formTransaction = FormsHandler.getInstance().getWorkflowData(idTransaction);

                if (formTransaction != null && (Transaction.STATUS_FINISHED.equals(transactionStatus) ||
                        Transaction.STATUS_CANCELLED.equals(transactionStatus) || Transaction.STATUS_DRAFT.equals(transactionStatus))) {

                    List<String> mails = RequestParamsUtils.getValue(formTransaction.getData().get("notificationEmails"), List.class);

                    if (mails != null && !mails.isEmpty()) {
                        String lang = AccessManagementHandlerFactory.getHandler().getUser(formTransaction.getIdUserCreator()).getLang();
                        Form form = FormsHandler.getInstance().readForm(formTransaction.getIdForm(), formTransaction.getFormVersion());
                        Environment env = Administration.getInstance().readEnvironment(formTransaction.getIdEnvironment());

                        Map<String, Object> data = mapData(idTransaction, formTransaction, env, form, transactionStatus, lang);

                        String subject = createSubject(formTransaction, transactionStatus, data, lang);
                        String templateIdMessage = getTempMessage(formTransaction, transactionStatus, lang,".body");
                        String body = finalMessage(transactionStatus,data,lang,templateIdMessage, "body");

                        // Send the notification only if both subject and body are not null
                        if (subject != null && body != null) {
                            NotificationsHandlerFactory.getHandler().sendAsyncEmail(subject, body, mails, null, lang, true);
                        } else {
                            LoggerUtils.logInfoMessage(log,"Notification was not sent. Body or Subject were not defined correctly.");
                        }
                    }
                }
            }
        } catch (IOException | MessagingException e) {
            //no queremos que el proceso falle si el error estuvo en enviar una notificacion
            LoggerUtils.logErrorMessage(log,"Error sending notifications from BPM Workflow", e.getMessage());
        }
        LoggerUtils.logInfoMessage(log,"Finished sending notifications for transaction {0} from BPM Workflow", idTransaction);
    }

    private Map<String, Object> mapData (String idTransaction, FormTransaction formTransaction, Environment env, Form form, String transactionStatus, String lang){
        Map<String, Object> data = new HashMap<>();
        data.put("idTransaction", idTransaction);
        data.put("transactionStatus", I18nFactory.getHandler().getMessage("transaction.status." + transactionStatus, lang));
        data.put("transactionName", form.getFormNameMap().get(lang));
        data.put("environmentName", env.getName());
        data.put("idEnvironment", env.getIdEnvironment());
        data.put("dateTime", formTransaction.getSubmitDateTime());
        data.put("data", formTransaction.getData());
        data.put("lang", lang);
        data.put("DateUtils", DateUtils.class);
        data.put("NumberUtils", NumberUtils.class);
        data.put("CurrencyUtils", CurrencyUtils.class);
        data.put("MaskUtils", MaskUtils.class);
        data.put("StringUtils", StringUtils.class);
        data.put("ProductLabeler", CoreUtils.getProductLabeler(lang));

        return data;
    }

    private String createSubject(FormTransaction formTransaction, String transactionStatus, Map<String, Object> data, String lang){
        String templateIdMessage = getTempMessage(formTransaction,transactionStatus,lang,".subject");
        return finalMessage(transactionStatus,data,lang,templateIdMessage,"subject");
    }

    private String getTempMessage(FormTransaction formTransaction, String transactionStatus, String lang, String segment){
        String templateIdMessage= "";
        if (I18nFactory.getHandler().isMessageExists(idMessage + formTransaction.getIdForm() + "." + transactionStatus + segment, lang)) {
            templateIdMessage = idMessage + formTransaction.getIdForm() + "." + transactionStatus + segment;
        } else if (I18nFactory.getHandler().isMessageExists(idMessage + transactionStatus + segment, lang)) {
            templateIdMessage = idMessage + transactionStatus + segment;
        }
        return templateIdMessage;
    }

    private String finalMessage(String transactionStatus, Map<String, Object> data, String lang, String templateIdMessage, String segment){
        String message = "";
        if (templateIdMessage != null) {
            try {
                message = TemplatingUtils.evaluate(I18nFactory.getHandler().getMessage(templateIdMessage, lang), data);
            } catch (IOException e) {
                LoggerUtils.logInfoMessage(log,"Error while processing the {0} for the notification ({1})", segment, templateIdMessage);
            }
        } else {
            LoggerUtils.logInfoMessage(log,"Notification {0} doesnt exist ({1}{2}.subject)",segment,idMessage, transactionStatus);
        }
        return message;
    }

}
