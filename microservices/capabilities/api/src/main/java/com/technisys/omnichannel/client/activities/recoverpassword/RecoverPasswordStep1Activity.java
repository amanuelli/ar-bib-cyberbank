/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.recoverpassword;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.legacy.SMSCommunicationsDispatcher;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.credentials.CredentialPluginFactory;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.domain.ValidationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.validationcodes.ValidationCodesHandler;
import org.apache.commons.lang3.StringUtils;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Validate username and second factor.
 * Then send an email to the user with a validation code.
 */
@AnonymousActivity
@DocumentedActivity("Recover Password Step 1")
public class RecoverPasswordStep1Activity extends Activity {

    public static final String ID = "session.recoverPassword.step1";

    public interface InParams {
        // Used by API, not this activity
        @DocumentedParam(type = String.class, description = "Username who perform the request, e.g. &quot;bbeall&quot;, &quot;32525648&quot;")
        String USER_NAME = "_username";
        
        @DocumentedParam(type = String.class, description = "User second factor authentication, for example: PIN")
        String SECOND_FACTOR = "_secondFactor";
        
        // Used by frontend, not this activity
        @DocumentedParam(type = String.class, description = "Captcha user response, used after several failed login attempts", optional = true)
        String CAPTCHA = "captcha";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            if (user == null) {
                throw new ActivityException(ReturnCodes.USER_DOESNT_EXIST, "Unable to read user '" + request.getIdUser() + "'");
            }

            try {
                CredentialPlugin cp = CredentialPluginFactory.getCredentialPlugin(Credential.CAPTCHA_CREDENTIAL);
                cp.validate(request, request.getCredential(Credential.CAPTCHA_CREDENTIAL));

                String secondFactor = ConfigurationFactory.getInstance().getStringSafe(Configuration.PLATFORM, "auth.login.credentialRequested");
                cp = CredentialPluginFactory.getCredentialPlugin(secondFactor);
                cp.validate(request, new Credential(secondFactor, StringUtils.defaultString(request.getParam(InParams.SECOND_FACTOR, String.class))));
            } catch (DispatchingException e) {
                throw new ActivityException(e.getReturnCode());
            }

            ValidationCode codigoValidacion = new ValidationCode();
            codigoValidacion.setCreationDate(new Date());
            codigoValidacion.setIdUser(user.getIdUser());
            codigoValidacion.setType(ValidationCode.TYPE_RECOVERPASS);
            codigoValidacion.setStatus(ValidationCode.STATUS_AVAILABLE);

            String idValidationCode = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
            ValidationCodesHandler.createValidationCode(codigoValidacion, idValidationCode);

            String baseURL = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, Constants.BASE_URL_KEY);
            String expirationTime = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "recoverPassword.mail.validity");
            HashMap<String, String> fillers = new HashMap<>();

            fillers.put("link", baseURL + "/recoverpassword/code?resetCode=" + codigoValidacion.getId());
            fillers.put("code", idValidationCode);

            fillers.put("expirationTime", com.technisys.omnichannel.client.utils.StringUtils.prettyPrintInterval(expirationTime, request.getLang()));

            String subject = I18nFactory.getHandler().getMessage("recoverPassword.mail.subject", request.getLang());
            String body = I18nFactory.getHandler().getMessage("recoverPassword.mail.body", request.getLang(), fillers);

            String sendChannel = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "invitation.notification.transport");

            switch (sendChannel) {
                case Constants.TRANSPORT_SMS:
                    NotificationsHandlerFactory.getHandler().sendSMS(user.getMobileNumber(), body, true);
                    break;
                case Constants.TRANSPORT_MAIL:
                    NotificationsHandlerFactory.getHandler().sendEmails(subject, body, Arrays.asList(user.getEmail()), null, request.getLang(), true);
                    break;
                default:
                    throw new ActivityException(ReturnCodes.SENDING_MAIL_ERROR, "No se encuentra bien configurado el transporte de la notificacion para invitaciones");
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (MessagingException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new ActivityException(ReturnCodes.SENDING_MAIL_ERROR, ex);
        }
        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        if(StringUtils.isBlank(request.getIdUser())||User.USER_ANONYMOUS.equals(request.getIdUser())){
            result.put(InParams.USER_NAME, "login.step1.username.required");
        }        
        if(StringUtils.isBlank(request.getParam(InParams.SECOND_FACTOR, String.class))){
            result.put(InParams.SECOND_FACTOR, "login.step1.secondFactor.required");
        }        
        return result;
    }
}
