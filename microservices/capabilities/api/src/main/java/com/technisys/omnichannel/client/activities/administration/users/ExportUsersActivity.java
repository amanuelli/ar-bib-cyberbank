/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ExportUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DocumentedActivity("Download file with list of users")
public class ExportUsersActivity extends Activity {

    public static final String ID = "administration.users.export";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "File download format (xls or pdf)")
        String FORMAT = "format";
        @DocumentedParam(type = String.class, description = "List order")
        String ORDER_BY = "orderBy";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Downloaded file name")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = String.class, description = "File content (Base64 format)")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Administration admin = Administration.getInstance();

            I18n i18n = I18nFactory.getHandler();

            String orderBy = request.getParam(ListUsersActivity.InParams.ORDER_BY, String.class);
            String format = request.getParam(InParams.FORMAT, String.class);


            if (StringUtils.isBlank(format)) {
                throw new ActivityException(
                        ReturnCodes.VALIDATION_ERROR,
                        i18n.getMessage(
                                "administration.users.export.format.blank",
                                request.getLang()
                        )
                );
            }

            if (!"xls".equalsIgnoreCase(format) && !"pdf".equalsIgnoreCase(format)) {
                throw new ActivityException(
                        ReturnCodes.VALIDATION_ERROR,
                        i18n.getMessage(
                                "administration.users.export.format.invalid",
                                request.getLang()
                        ));
            }

            ArrayList<String> orderList = new ArrayList();
            orderList.add("id_user");
            orderList.add("username");
            orderList.add("email");
            orderList.add("id_user_status");
            orderList.add("deleted");
            orderList.add("first_name");
            orderList.add("last_name");
            orderList.add("password");
            orderList.add("lang");
            orderList.add("id_default_environment");
            orderList.add("logged_in");
            orderList.add("last_access_ip");
            orderList.add("last_login");
            orderList.add("creation_date");
            orderList.add("id_seal");
            orderList.add("mobile_number");
            orderList.add("trustfull");
            orderList.add("document");
            orderList.add("document_type");
            orderList.add("document_country");

            if (StringUtils.isBlank(orderBy)) {
                orderBy = "last_login DESC";
            } else {
                String[] split = orderBy.split(" ");
                boolean isValidFilter = false;
                if (split.length > 1) {
                    isValidFilter = lookUpValue(split[0], orderList,split[split.length-1]);
                } else {
                    isValidFilter = lookUpValue(orderBy,orderList,null);
                    orderBy += " ASC";
                }
                if (!isValidFilter) {
                    throw new ActivityException(
                            ReturnCodes.VALIDATION_ERROR,
                            i18n.getMessage(
                                    "administration.users.export.format.invalid",
                                    request.getLang()
                            ));
                }
            }

            List<User> users = AccessManagementHandlerFactory.getHandler().getUsers(request.getIdEnvironment(), -1, -1, orderBy).getElementList();

            List<Map<String, Object>> usersToExport = new ArrayList<>();
            boolean includesSignatureLevel = !Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme());
            EnvironmentUser environmentUser;
            for (User user : users) {
                Map<String, Object> map = new HashMap<>();

                map.put("fullName", user.getFullName());
                map.put("email", user.getEmail());
                map.put("lastLogin", user.getLastLoginAsMediumString());
                map.put("statusDescription", i18n.getMessage("users.status." + admin.readEnvironmentUserStatus(user.getIdUser(), request.getIdEnvironment()), request.getLang()));

                String signatureLevel;
                if (includesSignatureLevel) {
                    environmentUser = admin.readEnvironmentUserInfo(user.getIdUser(), request.getIdEnvironment());
                    if (StringUtils.isEmpty(environmentUser.getSignatureLevel())) {
                        signatureLevel = i18n.getMessage("global.no", request.getLang());
                    } else {
                        if (Environment.ADMINISTRATION_SCHEME_MEDIUM.equals(request.getEnvironmentAdminScheme())) {
                            signatureLevel = i18n.getMessage("global.yes", request.getLang());
                        } else {
                            signatureLevel = environmentUser.getSignatureLevel();
                        }
                    }
                    map.put("signatureLevel", signatureLevel);
                }
                usersToExport.add(map);
            }
            List<String> columns = new ArrayList();
            List<String> attributes = new ArrayList();
            columns.add(i18n.getMessage("administration.users.export.firstName", request.getLang()));
            attributes.add("fullName");
            columns.add(i18n.getMessage("administration.users.export.email", request.getLang()));
            attributes.add("email");

            if (!Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                columns.add(i18n.getMessage("administration.users.list.signature", request.getLang()));
                attributes.add("signatureLevel");
            }

            columns.add(i18n.getMessage("administration.users.export.lastLogin", request.getLang()));
            attributes.add("lastLogin");
            columns.add(i18n.getMessage("administration.users.export.status", request.getLang()));
            attributes.add("statusDescription");

            byte[] content;
            String filename = i18n.getMessage("administration.users.export.filename", request.getLang());
            content = ExportUtils.toExcelFormat("/templates/administration_xls_template.xls", columns.toArray(new String[0]), usersToExport, attributes.toArray(new String[0]), i18n.getMessage("administration.users.export.title", request.getLang()), null);filename += ".xls";response.putItem(OutParams.CONTENT, Base64.encodeBase64(content));
            response.putItem(OutParams.FILE_NAME, filename);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    private boolean lookUpValue (String value, ArrayList<String> array, String optional){
        boolean found = false;
        for(String i : array) {
            if (value.equalsIgnoreCase(i)) {
                found = true;
                break;
            }
        }
        if(optional != null
            && (!(optional.equalsIgnoreCase("ASC") || optional.equalsIgnoreCase("DESC")))){
                found = false;
        }

        return found;
    }

}
