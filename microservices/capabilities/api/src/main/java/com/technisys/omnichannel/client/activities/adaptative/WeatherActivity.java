/*
 *  Copyright 2011 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.adaptative;


import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.domain.WeatherInfoCache;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.postloginchecks.geoip.PostLoginCheckGeoIPDataAccess;
import com.technisys.omnichannel.core.utils.CacheUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.hc.client5.http.classic.methods.HttpGet;
import org.apache.hc.client5.http.impl.ChainElements;
import org.apache.hc.client5.http.impl.classic.CloseableHttpClient;
import org.apache.hc.client5.http.impl.classic.CloseableHttpResponse;
import org.apache.hc.client5.http.impl.classic.HttpClientBuilder;
import org.apache.hc.client5.http.impl.classic.HttpClients;
import org.apache.hc.core5.http.HttpStatus;
import org.apache.hc.core5.http.ParseException;
import org.apache.hc.core5.http.io.entity.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;

/**
 * Get current weather conditions from AccuWeather.
 */
@AnonymousActivity
@DocumentedActivity("Get weather conditions")
public class WeatherActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(WeatherActivity.class);

    public static final String ID = "weather.get";
    private static final String CACHE_CITY_KEY = "weatherActivity.cacheCityKey";
    private static final String CACHE_WEATHER_INFO = "weatherActivity.cacheWeatherInfo";

    public interface InParams {
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Returns weather value: e.g. &quot;Clear&quot;, &quot;Cloudy&quot;, &quot;Snowy&quot;, &quot;Rainy&quot; ")
        String WEATHER = "weather";
        @DocumentedParam(type = String.class, description = "Returns &quot;day&quot; or &quot;night&quot; to know at what moment of the day we are")
        String MOMENT_DAY = "momentDay";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        double longitude = 0;
        double latitude = 0;
        String cityKey = null;

        // Getting ip from the incoming request
        String client_ip = request.getClientIP();
        try {
            InetAddress inetAddress;
            inetAddress = InetAddress.getByName(client_ip);
            if (inetAddress.isSiteLocalAddress() || inetAddress.isLoopbackAddress()) { // si estoy en red local o localmente
                client_ip = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "cyberbank.localAddressTesting"); // ip lab uruguay
            }

            // Getting the position and asking if there's some cityKey catched for these position
            String[] abcd = client_ip.split("\\.");
            if (abcd.length >= 3) {
                try {
                    CityResponse cityResponse = PostLoginCheckGeoIPDataAccess.getInstance().getCity(InetAddress.getByName(client_ip));
                    if (cityResponse != null) {
                        longitude = cityResponse.getLocation().getLongitude();
                        latitude = cityResponse.getLocation().getLatitude();
                        cityKey = (String) CacheUtils.get(CACHE_CITY_KEY, longitude, latitude);
                    }
                } catch (GeoIp2Exception | IOException ex) {
                    log.info("Error getting city key", ex);
                }
            }
        } catch (IOException ex) {
            log.warn("Error getting ip from request", ex);
        }

        // Getting zone code
        try {
            if (StringUtils.isBlank(cityKey)) {
                boolean ip_querying = false;
                String endpoint;
                HashMap<String, String> fillers = new HashMap<>();

                if (longitude == 0 || latitude == 0) { // If in the last step couldnt get the position
                    fillers.put("CLIENT_IP", client_ip);
                    endpoint = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "adaptative.accuWeather.serviceUrl.cityKey.ip", fillers);// Forming endpoint for get city_key by ip
                    ip_querying = true;
                } else {
                    fillers.put("LATITUDE", String.valueOf(latitude));
                    fillers.put("LONGITUDE", String.valueOf(longitude));
                    endpoint = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "adaptative.accuWeather.serviceUrl.cityKey.latitudeAndLongitude", fillers); // Forming endpoint for get city_key by position
                }

                HttpClientBuilder builder = HttpClients.custom();
                try (CloseableHttpClient httpClient = builder.build()) {
                    HttpGet httpGet = new HttpGet(endpoint);
                    try (CloseableHttpResponse httprResponse = httpClient.execute(httpGet)) {
                        if (httprResponse.getCode() != HttpStatus.SC_OK) {
                            response.putItem(OutParams.WEATHER, -1);
                        } else {
                            String responseString = EntityUtils.toString(httprResponse.getEntity(), "UTF-8");

                            JsonElement element = new JsonParser().parse(responseString);
                            cityKey = (((JsonObject) element).get("Key").toString()).replace('"', ' ').replace(" ", ""); // mejorar

                            if (ip_querying) {
                                String longitudeFromResponse = (((JsonObject) element).getAsJsonObject("GeoPosition").get("Longitude").toString()).replace('"', ' ').replace(" ", ""); // mejorar
                                String latitudeFromResponse = (((JsonObject) element).getAsJsonObject("GeoPosition").get("Latitude").toString()).replace('"', ' ').replace(" ", ""); // mejorar
                                longitude = Double.parseDouble(longitudeFromResponse);
                                latitude = Double.parseDouble(latitudeFromResponse);

                                // Updating data cached
                                CacheUtils.put(CACHE_CITY_KEY, cityKey, longitude, latitude);
                            }
                        }
                    } catch (ParseException ex) {
                        log.warn("Error parsing city key for get weather data", ex);
                        response.putItem(OutParams.WEATHER, -1);
                    }
                }
            }
        } catch (IOException ex) {
            log.warn("Error getting city key for get weather data", ex);
            response.putItem(OutParams.WEATHER, -1);
        }

        // Trying to get weather data catched
        WeatherInfoCache weather = null;

        try {
            weather = (WeatherInfoCache) CacheUtils.get(CACHE_WEATHER_INFO, cityKey);
        } catch (IOException ex) {
            log.warn("Error getting cache weather info", ex);
        }

        if (weather != null) {
            response.putItem(OutParams.WEATHER, weather.getWeather());
            if (weather.getIsDay()) {
                response.putItem(OutParams.MOMENT_DAY, "day");
            } else {
                response.putItem(OutParams.MOMENT_DAY, "night");
            }
        } else if (cityKey != null) {
            try {
                HashMap<String, String> fillers = new HashMap<>();
                fillers.put("CITY_KEY", cityKey);
                String endpoint = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "adaptative.accuWeather.serviceUrl.currentConditions", fillers);

                HttpClientBuilder builder = HttpClients.custom();
                try (CloseableHttpClient httpClient = builder.build()) {
                    final HttpGet httpGet = new HttpGet(endpoint);
                    try (CloseableHttpResponse httpResponse = httpClient.execute(httpGet)) {
                        if (httpResponse.getCode() != HttpStatus.SC_OK) {
                            response.putItem(OutParams.WEATHER, -1);
                        } else {
                            String responseString = (EntityUtils.toString(httpResponse.getEntity(), "UTF-8")).replace("[", "").replace("]", ""); // mejorar
                            JsonElement element = new JsonParser().parse(responseString);

                            String weatherText = getCustomCode(Integer.parseInt(((JsonObject) element).get("WeatherIcon").toString()));
                            response.putItem(OutParams.WEATHER, weatherText);

                            boolean isDay = Boolean.parseBoolean(((JsonObject) element).get("IsDayTime").toString());
                            if (isDay) {
                                response.putItem(OutParams.MOMENT_DAY, "day");
                            } else {
                                response.putItem(OutParams.MOMENT_DAY, "night");
                            }

                            weather = new WeatherInfoCache();
                            weather.setCityKey(cityKey);
                            weather.setIsDay(isDay);
                            weather.setWeather(weatherText);
                            CacheUtils.put(CACHE_WEATHER_INFO, weather, cityKey);
                        }
                    } catch (ParseException ex) {
                        log.warn("Error parsing city key for get weather data", ex);
                        response.putItem(OutParams.WEATHER, -1);
                    }
                }
            } catch (IOException ex) {
                log.warn("Error getting weather data", ex);
                response.putItem(OutParams.WEATHER, -1);
            }
        }

        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    public static String getCustomCode(int code) {
        // look at https://developer.accuweather.com/weather-icons for know what means every number
        switch (code) {
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 33:
            case 34:
            case 35:
            case 36:
                return "Clear";
            case 6:
            case 7:
            case 8:
            case 11:
            case 19:
            case 20:
            case 21:
            case 23:
            case 24:
            case 37:
            case 38:
            case 43:
            case 44:
                return "Cloudy";
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 18:
            case 25:
            case 39:
            case 40:
            case 41:
            case 42:
                return "Rainy";
            case 22:
            case 26:
            case 29:
                return "Snowy";
            default:
                return "";
        }
    }
}
