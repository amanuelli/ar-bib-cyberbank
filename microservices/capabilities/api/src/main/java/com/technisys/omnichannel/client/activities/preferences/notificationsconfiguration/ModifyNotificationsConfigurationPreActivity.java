/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.notificationsconfiguration;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.domain.CommunicationConfiguration;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

@DocumentedActivity("Modify Notifications Configuration (PRE)")
public class ModifyNotificationsConfigurationPreActivity extends Activity {

    public static final String ID = "preferences.notifications.configuration.modify.pre";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Communication type" , optional = true)
        String COMMUNICATION_TYPE = "communicationType";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Communication configuration")
        String COMMUNICATION_CONFIGURATIONS = "communicationConfigurations";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            CommunicationConfiguration communicationConfiguration = NotificationsHandlerFactory.getHandler().readUserCommunicationConfiguration(request.getIdUser(), request.getParam(InParams.COMMUNICATION_TYPE, String.class));
            response.putItem(OutParams.COMMUNICATION_CONFIGURATIONS, communicationConfiguration);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        
        return response;
    }
}