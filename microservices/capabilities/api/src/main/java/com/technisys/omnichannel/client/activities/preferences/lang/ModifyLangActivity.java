/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.lang;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.session.SessionHandlerFactory;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author isilveira
 */
@DocumentedActivity("Change lang")
public class ModifyLangActivity extends Activity {

    public static final String ID = "preferences.lang.modify";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Desired lang")
        String USER_LANG = "userLang";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Changed new lang")
        String NEW_LANG = "newLang";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            String userLang = request.getParam(InParams.USER_LANG, String.class);

            user.setLang(userLang);

            AccessManagementHandlerFactory.getHandler().updateUser(user);
            response.putItem(OutParams.NEW_LANG, user.getLang());

            Credential accessToken = request.getCredential(Credential.ACCESS_TOKEN_CREDENTIAL);
            SessionHandlerFactory.getInstance().updateSessionLang(accessToken.getValue(), userLang);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    public Map<String, String> validateFields(Request request) {
        Map<String, String> result = new HashMap<>();

        String userLangParam = request.getParam(InParams.USER_LANG, String.class);

        if (StringUtils.isEmpty(userLangParam)) {
            result.put(InParams.USER_LANG, "settings.changeLanguage.languageEmpty");
        } else {
            List<String> supportedLanguages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);
            boolean validLang = supportedLanguages.stream()
                    .anyMatch(element -> element.equals(userLangParam));
            if(!validLang) {
                result.put(InParams.USER_LANG, "settings.changeLanguage.invalidLanguage");
            }
        }

        return result;
    }
}
