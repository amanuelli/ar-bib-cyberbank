/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.notes;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.handlers.notes.NotesHandler;
import static com.technisys.omnichannel.client.utils.StringUtils.stripHTML;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 *
 */
@DocumentedActivity("Edit Statement Note")
public class EditStatementNoteActivity extends Activity {

    public static final String ID = "notes.editStatementNote";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Product Id")
        String ID_PRODUCT = "idProduct";

        @DocumentedParam(type = String.class, description = "Statement Id")
        String ID_STATEMENT = "idStatement";

        @DocumentedParam(type = String.class, description = "Note")
        String NOTE = "note";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Note")
        String NOTE = "note";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int idStatement = request.getParam(InParams.ID_STATEMENT, Integer.class);
            String idProduct = request.getParam(InParams.ID_PRODUCT, String.class);
            String note = stripHTML(request.getParam(InParams.NOTE, String.class));

            NotesHandler.modifyNote(idStatement, idProduct, note);

            response.putItem(OutParams.NOTE, note);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
