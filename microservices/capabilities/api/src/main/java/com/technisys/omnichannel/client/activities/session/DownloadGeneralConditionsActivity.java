/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.users.ExportUsersActivity;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.GeneralConditionDocument;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
@AnonymousActivity
@DocumentedActivity("Download general condition file")
public class DownloadGeneralConditionsActivity extends Activity {
    
    public static final String ID = "session.downloadGeneralConditions";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "File key identifier")
        String FILE_NAME_KEY = "fileNameKey";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "File key identifier")
        String FILE_NAME_KEY = "fileNameKey";
        @DocumentedParam(type = String.class, description = "File content (base 64)")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Response response = new Response(request);

            int actualCondition =  GeneralConditions.getInstance().actualCondition();
            String fileNameKey = request.getParam(InParams.FILE_NAME_KEY, String.class);

            GeneralConditionDocument generalConditionDocument = GeneralConditions.getInstance().readGeneralConditionDocument(actualCondition, fileNameKey);

            if (generalConditionDocument == null) {
                throw new ActivityException(ReturnCodes.IO_ERROR, "General condition document file not found");
            }
            response.putItem(ExportUsersActivity.OutParams.CONTENT, Base64.getEncoder().encodeToString(generalConditionDocument.getContent()));
            response.putItem(ExportUsersActivity.OutParams.FILE_NAME, fileNameKey);
            response.setReturnCode(ReturnCodes.OK);
                    
            return response;

        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        if(StringUtils.isBlank(request.getParam(InParams.FILE_NAME_KEY, String.class))) {
            result.put(InParams.FILE_NAME_KEY, "session.downloadGeneralConditions.fileNameKey.empty");
        }
        return result;
    }

}
