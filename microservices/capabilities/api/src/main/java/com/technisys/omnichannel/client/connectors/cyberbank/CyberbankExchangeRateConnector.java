/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.QuotationType;
import com.technisys.omnichannel.client.domain.CurrencyExchange;
import com.technisys.omnichannel.client.utils.JsonTemplateUtils;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.UserData;
import com.technisys.omnichannel.core.utils.CacheUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class CyberbankExchangeRateConnector extends CyberbankCoreConnector {

    private static final Logger LOG = LoggerFactory.getLogger(CyberbankExchangeRateConnector.class);
    private static final String CUSTOMER_TYPE = "CLIENT";

    /**
     * get list exchange rates
     * @param idUser
     * @param customerBehaviourType
     * @return
     * @throws IOException
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<List<CurrencyExchange>> listExchangeRates(String idUser, int customerBehaviourType) throws IOException, CyberbankCoreConnectorException {

        String serviceName = "Srv - massiveSelectQuotationBy_Date";

        List<CurrencyExchange> currencyExchangeList = (List<CurrencyExchange>) CacheUtils.get("cyberbank.core.currencyExchangeList");

        CyberbankCoreConnectorResponse<List<CurrencyExchange>> response = new CyberbankCoreConnectorResponse<>();

        if(currencyExchangeList != null){
            response.setData(currencyExchangeList);
        }else{
            UserData userData = AccessManagementHandlerFactory.getHandler().readUserData(idUser);
            ObjectMapper objectMapper = new ObjectMapper();
            Map data = objectMapper.readValue(userData.getData(), Map.class);

            QuotationType quotationType = getQuotationType(CUSTOMER_TYPE).getData();

            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("massiveSelectQuotationBy_Date", true)
                    .addRequestParameter("stampAdditional", data.get("stampAdditional"))
                    .addRequestParameter("statusDate", formatDate(new Date()))
                    .addRequestParameter("stampDateTime", formatDate(new Date()))
                    .addRequestParameter("customerBehaviourType", customerBehaviourType)
                    .addRequestParameter("idQuotationType", quotationType.getIdQuotation())
                    .addRequestParameter("quotationTypeId", quotationType.getIdQuotationType());


            String request = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(serviceName, request);

            if (callHasError(serviceResponse)) {
                return processErrors(serviceName, serviceResponse, response);
            } else {
                currencyExchangeList = parseSrvResponse(serviceResponse);
                CacheUtils.put("cyberbank.core.currencyExchangeList", currencyExchangeList);
                response.setData(currencyExchangeList);
            }
        }
        return response;

    }

    /**
     * get quotattion type
     * @param customerType
     * @return
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<QuotationType> getQuotationType(String customerType) throws CyberbankCoreConnectorException, IOException {
        CyberbankCoreConnectorResponse<QuotationType> response = new CyberbankCoreConnectorResponse<>();

        String serviceName = "Srv - processCustomerTo_Obtain_Quotation_Type";

        QuotationType quotationType = (QuotationType)CacheUtils.get("cyberbank.core.quotationType");

        if(quotationType != null){
            response.setData(quotationType);
        }else{
            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("processCustomerTo_Obtain_Quotation_Type", true)
                    .addRequestParameter("customerType", customerType);

            String request = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(serviceName, request);
            if (callHasError(serviceResponse)) {
                return processErrors(serviceName, serviceResponse, response);
            } else {
                quotationType = processQuotationType(serviceResponse);
                CacheUtils.put("cyberbank.core.quotationType", quotationType);
                response.setData(quotationType);
            }
        }
        return response;
    }

    /**
     * process response of service massiveSelectQuotationBy_Date
     * @param serviceResponse
     * @return
     */
    private static List<CurrencyExchange> parseSrvResponse(JSONObject serviceResponse) {
        if (!serviceResponse.has("out.quotation_list") || serviceResponse.isNull("out.quotation_list")) {
            return new ArrayList<>();
        }

        JSONObject outQuotationList = serviceResponse.getJSONObject("out.quotation_list");


        if (!outQuotationList.has("collection") || outQuotationList.isNull("collection")) {
            return new ArrayList<>();
        }

        JSONArray quotationList = outQuotationList.getJSONArray("collection");
        ArrayList<CurrencyExchange> quotationListResult = new ArrayList<>();
        for (int i = 0; i < quotationList.length(); i++) {
            JSONObject quotationObject = quotationList.getJSONObject(i);
            try{
                Currency currency = Currency.getInstance(quotationObject.getJSONObject("currency").getString("currAcronym"));
                CurrencyExchange currencyExchange = new CurrencyExchange();
                currencyExchange.setBaseCurrencyCode(ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "core.masterCurrency", "USD"));
                currencyExchange.setPurchase(Double.parseDouble(quotationObject.getString("quotBuy")));
                currencyExchange.setSale(Double.parseDouble(quotationObject.getString("quotSale")));
                currencyExchange.setTargetCurrencyCode(currency.getCurrencyCode());
                quotationListResult.add(currencyExchange);
            }catch (IllegalArgumentException e) {
                LOG.warn("Invalid currency specified: " + quotationObject.getJSONObject("currency").getString("currAcronym"), e);
            }
        }
        return quotationListResult;
    }

    /**
     * process response of service processCustomerTo_Obtain_Quotation_Type
     * @param response
     * @return
     * @throws CyberbankCoreConnectorException
     */
    private static QuotationType processQuotationType(JSONObject response ) throws CyberbankCoreConnectorException{
        try {
            if (response.getJSONObject("out.quotation_type") == null) {
                return null;
            }
            JSONObject jsonQuotationType = response.getJSONObject("out.quotation_type");
            Integer id = (Integer) jsonQuotationType.get("id");
            Integer quotationTypeId = (Integer) jsonQuotationType.get("quotationTypeId");
            String shortDesc = jsonQuotationType.getString("shortDesc");

            return new QuotationType(id, quotationTypeId, shortDesc );

        } catch (JSONException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

}
