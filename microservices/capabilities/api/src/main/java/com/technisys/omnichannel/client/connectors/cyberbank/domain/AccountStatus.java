/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain;

import java.io.Serializable;

public class AccountStatus implements Serializable {

    private Integer statusCode;
    private String shortDescription;
    private String description;
    private String longDescription;

    public AccountStatus() {
    }

    public AccountStatus(Integer statusCode, String shortDescription, String description, String longDescription) {
        this.statusCode = statusCode;
        this.shortDescription = shortDescription;
        this.description = description;
        this.longDescription = longDescription;
    }

    public Integer getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(Integer statusCode) {
        this.statusCode = statusCode;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLongDescription() {
        return longDescription;
    }

    public void setLongDescription(String longDescription) {
        this.longDescription = longDescription;
    }
}
