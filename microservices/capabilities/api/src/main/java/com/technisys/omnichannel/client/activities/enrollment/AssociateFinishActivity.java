/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.handlers.enrollment.EnrollmentHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;

/**
 *
 */
@ExchangeActivity
public class AssociateFinishActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(AssociateFinishActivity.class);

    public static final String ID = "enrollment.associate.finish";

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            InvitationCode invitation = getInvitationCode(request);
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber());
            if (user == null) {
                throw new ActivityException(ReturnCodes.INVALID_INVITATION_CODE);
            }

            log.info("Iniciando asociación de cuenta para la invitación {} ", invitation.getInvitationCode());

            log.info("Por asociar al usuario en la cuenta.");
            EnrollmentHandler.associateUserToAccount(request.getIdTransaction(), user.getIdUser(), invitation);
            log.info("Se asoció el usuario en la cuenta.");

            InvitationCodesHandler.setInvitationCodeAsUsed(invitation.getId());
            log.info("Se marcó la invitación {} como utilizada.", invitation.getInvitationCode());

            // Elimino el exchange Token
            ExchangeTokenHandler.delete(request.getParam(EXCHANGE_TOKEN, String.class));

            log.info("Asociación de cuenta para la invitación {} termino correctamente.", invitation.getInvitationCode());

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            log.error("Error de base de datos", ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            InvitationCode invitation = getInvitationCode(request);
            result.putAll(AssociateVerifyStep2Activity.validateFields());
            Environment environment;
            if(invitation.getProductGroupId() != null) {
                environment = Administration.getInstance().readEnvironmentByProductGroupId(invitation.getProductGroupId());
            } else {
                environment = Administration.getInstance().readEnvironment(invitation.getIdEnvironment());
            }
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber());
            if (environment != null && user != null && Administration.getInstance().readEnvironmentUserInfo(user.getIdUser(), environment.getIdEnvironment()) != null) {
                result.put("NO_FIELD", "enrollment.associate.step3.error.userAlreadyExistsInEnvironment");
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }
}
