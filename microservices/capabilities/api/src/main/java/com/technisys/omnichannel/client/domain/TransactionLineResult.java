package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.domain.TransactionLine.Status;
import com.technisys.omnichannel.client.domain.TransactionLine.ErrorCode;

public class TransactionLineResult {

    private String idTransaction;
    private Integer lineNumber;
    private Status status;
    private ErrorCode errorCode;

    public TransactionLineResult(String idTransaction, Integer lineNumber, Status status, ErrorCode errorCode) {
        this.idTransaction = idTransaction;
        this.lineNumber = lineNumber;
        this.status = status;
        this.errorCode = errorCode;
    }

    public String getIdTransaction() {
        return idTransaction;
    }

    public void setIdTransaction(String idTransaction) {
        this.idTransaction = idTransaction;
    }

    public Integer getLineNumber() {
        return lineNumber;
    }

    public void setLineNumber(Integer lineNumber) {
        this.lineNumber = lineNumber;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public ErrorCode getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(ErrorCode errorCode) {
        this.errorCode = errorCode;
    }
}
