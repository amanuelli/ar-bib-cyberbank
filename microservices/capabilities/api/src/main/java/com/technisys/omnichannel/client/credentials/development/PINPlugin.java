/* 
 * Copyright 2015 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.credentials.development;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.attemptscounter.AttemptsCounterHandler;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.domain.AttemptsCounter;
import com.technisys.omnichannel.core.domain.UserStatus;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.exceptions.InvalidCredentialException;
import com.technisys.omnichannel.credentials.notifications.BlockedUserNotifierFactory;
import com.technisys.omnichannel.utils.ReCaptcha;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 * @author ?
 */
public class PINPlugin implements CredentialPlugin {
    
    private static final Logger log = LoggerFactory.getLogger(PINPlugin.class);

    @Override
    public void validate(IBRequest request, Credential credential) throws DispatchingException, IOException {
        String feature = AttemptsCounterHandler.getFeature(request.getIdActivity());

        String inputPin = credential.getValue() == null ? "" : credential.getValue();

        if ("1111".equals(inputPin)) {
            AttemptsCounterHandler.resetAttemptsCounter(feature, request.getIdUser());
        } else {
            AttemptsCounter attemptsCounter = AttemptsCounterHandler.incrementAttemptsCounter(feature, request.getIdUser());

            int maxAttemptsToBlock = AttemptsCounterHandler.getMaxAttemptsToBlock(feature);
            if (maxAttemptsToBlock > 0 && attemptsCounter.getAttempts() >= maxAttemptsToBlock) {
                String logString = String.format("El usuario %s ha alcanzado los reintentos máximos y será bloqueado", request.getIdUser());
                log.warn(logString);
                AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
                AccessManagementHandlerFactory.getHandler().updateUserStatus(request.getIdUser(), UserStatus.USER_STATUS_BLOCKED);
                BlockedUserNotifierFactory.notify(request.getIdUser());
                throw new DispatchingException(ReturnCodes.USER_DISABLED);
            }

            if (ReCaptcha.mustBeDisplayedForUser(feature, request.getIdUser())) {
                throw new InvalidCredentialException(ReturnCodes.INVALID_PIN_CREDENTIAL_CAPTCHA_REQUIRED, Credential.PIN_CREDENTIAL);
            }
            throw new InvalidCredentialException(ReturnCodes.INVALID_PIN_CREDENTIAL, Credential.PIN_CREDENTIAL);
        }
    }

    @Override
    public boolean validateFormat(IBRequest request, String value) throws DispatchingException, IOException {
        return validateFormat("", value);
    }

    @Override
    public boolean validateFormat(String commonName, String value) throws DispatchingException, IOException {
        return StringUtils.isNotBlank(value) && value.length() == 4;
    }

    @Override
    public void changeStatus(String commonName, String newValue) throws DispatchingException, IOException {
        // No implementation required
    }

    @Override
    public void modify(IBRequest request, String currentValue, String newValue) throws DispatchingException, IOException {
        // No implementation required
    }

    @Override
    public void recover(String idUser, String newValue) throws DispatchingException, IOException {
        // No implementation required
    }

    @Override
    public void assign(String commonName, String value) throws DispatchingException, IOException {
        // No implementation required
    }

    @Override
    public void assign(IBRequest request, String commonName, String newValue) throws DispatchingException, IOException {
        // No implementation required
    }

}
