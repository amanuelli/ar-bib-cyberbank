/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.Address;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.CyberbankCoreCustomer;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.DefaultValuesClient;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.Identification;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.enums.*;
import com.technisys.omnichannel.client.connectors.cyberbank.json.JsonArray;
import com.technisys.omnichannel.client.connectors.cyberbank.utils.DocumentTypeResolver;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.utils.JsonTemplateUtils;
import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;


public class CyberbankCustomerConnector extends CyberbankCoreConnector {
    private static final String SERVICE_NAME = "Srv - C1";

    /**
     * Create a customer con Cyberbank Core
     *
     * @param client Customer information to send to Cyberbank Core
     * @return Cutomer's identification
     * @throws CyberbankCoreConnectorException In case a network error happened
     */
    public static CyberbankCoreConnectorResponse<String> create(CyberbankCoreCustomer client) throws CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<String> response = new CyberbankCoreConnectorResponse<>();

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("insertCustomerComplete", true);
        // Contact Phone Info
        cyberbankCoreRequest.addRequestParameter("in.contact_phone_list", builContactPhoneList(client));
        cyberbankCoreRequest.addRequestParameter("contactPhoneList.collectionEntityId", "ContactPhone");
        // Info Indivudual
        cyberbankCoreRequest.addRequestParameter("sexCode.nemotecnico", client.getGender());
        cyberbankCoreRequest.addRequestParameter("nationality.nemotecnico", client.getNationality());
        cyberbankCoreRequest.addRequestParameter("countryOfBirth.nemotecnico", client.getNationality());
        cyberbankCoreRequest.addRequestParameter("birthDate", CyberbankCoreConnector.formatDate(client.getBirthDate()));
        //Electronic contacts
        cyberbankCoreRequest.addRequestParameter("emailType.nemotecnico", DefaultValuesClient.EC_EMAIL_TYPE);
        cyberbankCoreRequest.addRequestParameter("emailAddressComplete", client.getEmail());
        //Customer Identification
        cyberbankCoreRequest.addRequestParameter("in.customerIdentificationList", buildIdentificationList(client.getIdentificationList()));
        cyberbankCoreRequest.addRequestParameter("customerIdentificationList.collectionEntityId", "CustomerIdentification");
        //Addres Info
        cyberbankCoreRequest.addRequestParameter("in.addressList", buildAddressList(client.getAddressList()));
        cyberbankCoreRequest.addRequestParameter("addressList.collectionEntityId", "Address");
        //Info Customer
        cyberbankCoreRequest.addRequestParameter("customerType.nemotecnico", CustomerType.CLIENT.toString());
        cyberbankCoreRequest.addRequestParameter("customer.firstName1", client.getFirstName());
        cyberbankCoreRequest.addRequestParameter("customer.lastName1", client.getLastName());
        cyberbankCoreRequest.addRequestParameter("personType.nemotecnico", client.getPersonType());
        //employment details
        cyberbankCoreRequest.addRequestParameter("occupationCode", client.getOccupation());
        cyberbankCoreRequest.addRequestParameter("employmentSituation.nemotecnico", DefaultValuesClient.ED_EMPLOYMENT_SITUATION);
        cyberbankCoreRequest.addRequestParameter("employer", client.getEmployer());
        cyberbankCoreRequest.addRequestParameter("employmentStartDate", client.getEmploymentStartDate());
        cyberbankCoreRequest.addRequestParameter("fixedIncome", String.valueOf(client.getAnnualHouseholdIncome()));

        String request = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(SERVICE_NAME, request);

        if (callHasError(serviceResponse)) {
            processErrors(SERVICE_NAME, serviceResponse, response);
        } else {
            JSONObject customer = serviceResponse.getJSONObject("out.customer");
            response.setData(customer.getString("customerId"));
        }

        return response;
    }

    /**
     * Build an identification with templaje JSON  list to send to Cyberbank core
     *
     * @param identificationList
     * @return
     */
    private static JSONArray buildIdentificationList(List<Identification> identificationList) {

        JSONObject jsonIdentification;
        JSONArray jsonArrayIdentification = new JsonArray();
        for (Identification identification : identificationList) {
            jsonIdentification = new JSONObject();
            jsonIdentification.put("identificationNumber", identification.getNumber());
            jsonIdentification.put("identificationType", identification.getType());
            jsonArrayIdentification.put(jsonIdentification);
        }
        return jsonArrayIdentification;
    }


    /**
     * Built an address list with templaje JSON to send to Cyberbank core
     *
     * @param addressList
     * @return
     */
    private static JSONArray buildAddressList(List<Address> addressList) {
        JSONObject jsonAddres;
        JSONArray jsonArrayAddres = new JsonArray();
        for (Address address : addressList) {
            jsonAddres = new JSONObject();
            jsonAddres.put("country.nemotecnico", DefaultValuesClient.ADDRESS_COUNTRY);
            jsonAddres.put("streetNumber", address.getStreetNumber());
            jsonAddres.put("street", address.getStreetName());
            jsonAddres.put("addressType.nemotecnico", AddressType.REAL.getValueForService());
            //normalize the setting of this value from the list of departments address.getProvince().getCode());.
            jsonAddres.put("provinceCode.id", "500001");
            jsonAddres.put("countryZipCode", address.getPostalCode());
            jsonArrayAddres.put(jsonAddres);
        }
        return jsonArrayAddres;
    }

    private static JSONArray builContactPhoneList(CyberbankCoreCustomer client) {

        JSONArray jsonArrayContactPhone = new JsonArray();
        JSONObject jsonContactPhone = new JSONObject();
        jsonContactPhone.put("areaCodeId", "500002"); // TODO Change it by mobileNumber.getAreaCode()
        jsonContactPhone.put("phoneNumber", String.valueOf(client.getMobilePhoneNumber()));
        jsonContactPhone.put("areaCountryId", "500050"); // TODO Change it bu mobileNumber.getCountryCode()
        jsonContactPhone.put("telephoneType.nemotecnico", TelephoneType.MOBILE.getValue());
        jsonArrayContactPhone.put(jsonContactPhone);
        return jsonArrayContactPhone;
    }

    /**
     * Read a person with Cyberbank Core
     *
     * @param documentType
     * @param documentNumber
     * @return Person's information
     * @throws BackendConnectorException In case a network error happened
     */
    public static CyberbankCoreConnectorResponse<ClientUser> read(String documentType, String documentNumber) throws BackendConnectorException, CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<ClientUser> response = new CyberbankCoreConnectorResponse<>();
        String serviceName = "Srv - C2";

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("singleSelectCustomerBy_Identification_Complete", true);
        cyberbankCoreRequest.addRequestParameter("identificationNumber", documentNumber);
        cyberbankCoreRequest.addRequestParameter("identification_type", DocumentTypeResolver.resolve(documentType));

        String request = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(serviceName, request);

        if (callHasError(serviceResponse)) {
            processErrors(serviceName, serviceResponse, response);
        } else {
            response.setData(parseSrvResponse(serviceResponse));
        }


        return response;
    }

    /**
     * Read a Cyberbank core customer by id
     *
     * @param idCustomer
     * @return
     * @throws BackendConnectorException
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<ClientEnvironment> read(String idCustomer) throws BackendConnectorException, CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<ClientEnvironment> response = new CyberbankCoreConnectorResponse<>();
        String serviceName = "Srv - C3";

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("singleSelectCustomerBy_Customer_id_Complete", true)
                .addRequestParameter("customerId", idCustomer);

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());

        JSONObject srvResponse = call(serviceName, jsonRequest);

        ClientUser clientUser = parseSrvResponse(srvResponse);

        ClientEnvironment clientEnvironment = new ClientEnvironment();

        clientEnvironment.setProductGroupId(idCustomer);
        clientEnvironment.setAccountName(clientUser.getFirstName() + " " + clientUser.getLastName());
        clientEnvironment.setCorrespondenceAddress(clientUser.getEmail());

        response.setData(clientEnvironment);
        return response;
    }

    /**
     * Parse Cyberbank customer response
     *
     * @param srvResponse
     * @return
     */
    private static ClientUser parseSrvResponse(JSONObject srvResponse) throws BackendConnectorException {
        CyberbankCoreCustomer clientUser = new CyberbankCoreCustomer();
        List<Address> addressList = new ArrayList<>();
        List<Identification> identificationList = new ArrayList<>();

        JSONArray outAddressList = srvResponse.getJSONObject("out.address_list").getJSONArray("collection");
        JSONArray electronicsContactList = srvResponse.getJSONObject("out.electronics_contact_list").getJSONArray("collection");
        JSONArray contactPhoneList = srvResponse.getJSONObject("out.contact_phone_list").has("collection") ? srvResponse.getJSONObject("out.contact_phone_list").getJSONArray("collection") : null;
        JSONArray customerIdentificationList = srvResponse.getJSONObject("out.customer_identification_list").getJSONArray("collection");
        JSONObject basicIndividualData = srvResponse.getJSONObject("out.basic_individual_data");
        JSONObject customer = srvResponse.getJSONObject("out.customer");

        for (int i = 0; i < outAddressList.length(); i++) {

            JSONObject objAddress = outAddressList.getJSONObject(i);
            Address address = new Address();

            if (objAddress.has("streetNumber") && !objAddress.isNull("streetNumber")) {
                address.setStreetNumber(String.valueOf(objAddress.getInt("streetNumber")));
            }

            if (objAddress.has("street") && !objAddress.isNull("street")) {
                address.setStreetName(objAddress.getString("street"));
            }

            address.setAddressType(objAddress.getJSONObject("addressType").getString("nemotecnico"));
            addressList.add(address);
        }

        clientUser.setFirstName(customer.getString("firstName1"));
        clientUser.setLastName(customer.getString("lastName1"));
        clientUser.setProductGroupId(customer.getString("customerId"));
        clientUser.setCoreId(String.valueOf(customer.getInt("id")));

        if (contactPhoneList != null) {
            for (int c = 0; c < contactPhoneList.length(); c++) {
                JSONObject contactPhone = contactPhoneList.getJSONObject(c);
                if (contactPhone.getJSONObject("telephoneType").getString("nemotecnico").equals(TelephoneType.MOBILE.getValue())) {
                    clientUser.setMobileNumber(contactPhone.getString("phoneNumber"));
                    break;
                }
            }
        }

        for (int e = 0; e < electronicsContactList.length(); e++) {
            JSONObject objElectronicContact = electronicsContactList.getJSONObject(e);
            clientUser.setEmail(objElectronicContact.getString("emailAddressComplete"));
        }

        for (int d = 0; d < customerIdentificationList.length(); d++) {
            JSONObject objCustomerIdentification = customerIdentificationList.getJSONObject(d);
            Identification identification = new Identification();
            identification.setNumber(objCustomerIdentification.getString("identificationNumber"));

            identification.setType(DocumentTypeResolver.resolve(objCustomerIdentification.getJSONObject("identificationType").getString("nemotecnico")));

            if (objCustomerIdentification.has("validityIdentification")
                    && !objCustomerIdentification.isNull("validityIdentification")) {
                String validityIdentification = objCustomerIdentification.getString("validityIdentification");
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmssSSS");
                try {
                    identification.setExpirationDate(dateFormat.parse(validityIdentification));
                } catch (ParseException ex) {
                    throw new BackendConnectorException(ex);
                }
            }

            identificationList.add(identification);
        }

        clientUser.setDocumentNumber(customer.getString("identificationNumber"));
        clientUser.setIdentificationList(identificationList);
        clientUser.setDocumentType(String.valueOf(customer.getJSONObject("identificationType").getInt("id")));
        clientUser.setNationality(basicIndividualData.getJSONObject("nationality").getString("nemotecnico"));
        clientUser.setMaritalStatus(MaritalStatus.valueOf(basicIndividualData.getJSONObject("maritalStatus").getString("nemotecnico")));
        clientUser.setGender(basicIndividualData.getJSONObject("sexCode").getString("nemotecnico"));
        clientUser.setPersonType(PersonType.INDIVIDUAL);
        clientUser.setAddressList(addressList);

        return clientUser;
    }


    /**
     * Read core customer classification
     *
     * @param idCustomer
     * @return
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<Integer> getClassification(String idCustomer) throws CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<Integer> response = new CyberbankCoreConnectorResponse<>();
        String serviceName = "Srv - singleSelectCustomer_Classification";

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("singleSelectCustomer_Classification", true)
                .addRequestParameter("customerId", idCustomer);

        String request = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(serviceName, request);

        if (callHasError(serviceResponse)) {
            processErrors(serviceName, serviceResponse, response);
        } else {
            JSONObject customerBehaviourType = serviceResponse.getJSONObject("out.customer_classification").getJSONObject("customerBehaviourType");
            response.setData(customerBehaviourType.getInt("id"));
        }

        return response;
    }

}
