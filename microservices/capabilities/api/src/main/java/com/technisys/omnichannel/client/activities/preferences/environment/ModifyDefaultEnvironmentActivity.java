/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.environment;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Jhossept Garay
 */
@DocumentedActivity("Modify default environment")
public class ModifyDefaultEnvironmentActivity extends Activity {

    public static final String ID = "preferences.environment.modify";

    public interface InParams {
        @DocumentedParam(type = Integer.class, description = "Identifier of environment you want to set as default, If the parameter is 0 then the user will be without default environment")
        String ID_ENVIRONMENT_TO_SET_DEFAULT = "idEnvironmentToSetDefault";
    }

    public interface OutParams {
        @DocumentedParam(type = Integer.class, description = "Identifier of environment you just set")
        String ID_ENVIRONMENT = "idEnvironment";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int idEnvironmentToSetDefault = request.getParam(ModifyDefaultEnvironmentActivity.InParams.ID_ENVIRONMENT_TO_SET_DEFAULT, Integer.class);
            AccessManagementHandlerFactory.getHandler().updateUserDefaultEnvironment(request.getIdUser(), idEnvironmentToSetDefault);
            response.putItem(OutParams.ID_ENVIRONMENT, idEnvironmentToSetDefault);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        if (request.getParam(InParams.ID_ENVIRONMENT_TO_SET_DEFAULT, Integer.class) == null) {
            result.put(InParams.ID_ENVIRONMENT_TO_SET_DEFAULT, "preferences.environment.idEnvironmentToSetDefaultEmpty");
        }

        return result;
    }
}