/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */
package com.technisys.omnichannel.client.activities.loans.request;



import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.utils.SynchronizationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.ReturnCodes.IO_ERROR;
import static com.technisys.omnichannel.client.ReturnCodes.BACKEND_SERVICE_ERROR;



/**
 * @author Cristobal Meneses
 */
@DocumentedActivity("Send Request Loand")
public class RequestLoanSendActivity extends Activity {

    public static final String ID = "request.loan.send";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Loan Type")
        String LOAN_TYPE = "loanType";

        @DocumentedParam(type = String.class, description = "Amount to request")
        String AMOUNT_TO_REQUEST = "amountToRequest";

        @DocumentedParam(type = String.class, description = "Amount of fees")
        String AMOUNT_OF_FEES = "amountOfFees";

        @DocumentedParam(type = String.class, description = "Authorize debit to account")
        String IS_AUTHORIZE_DEBIT_TO_ACCOUNT = "iAuthorizeDebitToAccount";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Response response = new Response(request);
            String idTransaction = request.getIdTransaction();
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            List loanTypeList = RequestParamsUtils.getValue(request.getParameters().get(InParams.LOAN_TYPE), List.class);
            String loanType = (loanTypeList != null && !loanTypeList.isEmpty()) ? loanTypeList.get(0).toString() : "";

            Amount amount = request.getParam(InParams.AMOUNT_TO_REQUEST, Amount.class);

            List amountOfFeesList = RequestParamsUtils.getValue(request.getParameters().get(InParams.AMOUNT_OF_FEES), List.class);
            Integer amountOfFees = (amountOfFeesList != null && !amountOfFeesList.isEmpty()) ? Integer.parseInt(amountOfFeesList.get(0).toString()) : null;

            String debitAccountIdProduct = (String) request.getParam(InParams.IS_AUTHORIZE_DEBIT_TO_ACCOUNT, Map.class).get("value");
            Product product = Administration.getInstance().readProduct(debitAccountIdProduct, request.getIdEnvironment());
            Account debitAccount = new Account(product);

            CoreLoanConnectorOrchestrator.request(debitAccount, loanType, environment.getClients().get(0).getIdClient(), amount, amountOfFees);

            SynchronizationUtils.syncEnvironmentProducts(idTransaction, environment, null);

            response.setReturnCode(ReturnCodes.OK);
            return response;

        } catch (IOException e) {
            throw new ActivityException(IO_ERROR, e);
        } catch (BackendConnectorException e){
            throw new ActivityException(BACKEND_SERVICE_ERROR, e);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            return FormsHandler.getInstance().validateRequest(request);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (SchedulerException e) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, e);
        }
    }
}

