/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.smsconfiguration;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.ChannelProduct;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.apache.commons.lang.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class ModifySmsConfigurationPreActivity extends Activity {

    public static final String ID = "preferences.smsconfiguration.modify.pre";

    public interface InParams {
    }

    public interface OutParams {

        String MOBILE_NUMBER = "mobileNumber";
        String PERMISSION = "permission";
        String PRODUCTS = "products";
        String PREFIXES = "prefixes";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            //datos del usuario
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());

            //permiso
            String permission = Administration.getInstance().getChannelPermission(request.getIdEnvironment(), request.getIdUser(), "sms");
            if(StringUtils.isBlank(permission)){
                permission = ChannelProduct.PERMISSION_NONE;
            }
            
            //productos (en caso de que el permiso no sea "none")
            List<ChannelProduct> products = null;
            if(!ChannelProduct.PERMISSION_NONE.equals(permission)){
                products = Administration.getInstance().listChannelEnabledProducts(request.getIdEnvironment(), request.getIdUser(), "sms");
                
                //cargo las labels
                ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
                
                for(ChannelProduct product : products){
                    product.setShortLabel(pLabeler.calculateShortLabel(product));
                    product.setLabel(pLabeler.calculateLabel(product));
                }
            }
            
            //prefijos para los alias
            Map prefixes = new HashMap<>();
            List<String> productTypes = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "preferences.sms.configuration.productTypes");
            for(String productType : productTypes){
                prefixes.put(productType, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "preferences.sms.configuration.aliasPrefix." + productType));
            }
            
            response.putItem(OutParams.MOBILE_NUMBER, user.getMobileNumber());
            response.putItem(OutParams.PERMISSION, permission);
            response.putItem(OutParams.PRODUCTS, products);
            response.putItem(OutParams.PREFIXES, prefixes);
            
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
}