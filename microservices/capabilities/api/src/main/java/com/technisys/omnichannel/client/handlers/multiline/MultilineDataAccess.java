package com.technisys.omnichannel.client.handlers.multiline;

import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.client.domain.TransactionLinesStatus;
import com.technisys.omnichannel.client.domain.TransactionLinesTotals;
import com.technisys.omnichannel.core.utils.DBUtils;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.ibatis.session.RowBounds;
import org.apache.ibatis.session.SqlSession;

public class MultilineDataAccess {
    private static MultilineDataAccess instance;

    public static synchronized MultilineDataAccess getInstance() {
        if (instance == null) {
            instance = new MultilineDataAccess();
        }

        return instance;
    }

    private void addTransactionLine(SqlSession session, TransactionLine transactionLine) {
        session.insert("com.technisys.omnichannel.client.handlers.multiline.addTransactionLine", transactionLine);
    }

    public void addTransactionLines(List<TransactionLine> transactionLines) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openWriteSession()) {
            for (TransactionLine transactionLine : transactionLines) {
                addTransactionLine(session, transactionLine);
            }

            session.commit();
        }
    }

    public void deleteTransactionLine(String idTransaction, Integer lineNumber) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openWriteSession()) {
            Map<String, Object> params = new HashMap<>();
            params.put("idTransaction", idTransaction);
            params.put("lineNumber", lineNumber);

            session.delete("com.technisys.omnichannel.client.handlers.multiline.deleteTransactionLine", params);

            session.commit();
        }
    }

    public TransactionLine getNextPendingTransaction() throws IOException {
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {
            return session.selectOne("com.technisys.omnichannel.client.handlers.multiline.getNextPendingTransaction");
        }
    }

    public TransactionLine getTransactionLine(String idTransaction, Integer lineNumber) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {
            Map<String, Object> params = new HashMap<>();
            params.put("idTransaction", idTransaction);
            params.put("lineNumber", lineNumber);

            return session.selectOne("com.technisys.omnichannel.client.handlers.multiline.getTransactionLine", params);
        }
    }

    public List<TransactionLinesStatus> getTransactionLinesStatus(String idTransaction) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {
            return session.selectList("com.technisys.omnichannel.client.handlers.multiline.getTransactionLinesStatus", idTransaction);
        }
    }

    public List<TransactionLine> getTransactionLinesWithErrors(String idTransaction) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {
            return session.selectList("com.technisys.omnichannel.client.handlers.multiline.getTransactionLinesWithErrors", idTransaction);
        }
    }

    public List<TransactionLine> listPendingTransactionLinesByCount(String idTransaction, Integer count) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {

            return session.selectList("com.technisys.omnichannel.client.handlers.multiline.listPendingTransactionLines", idTransaction, new RowBounds(RowBounds.NO_ROW_OFFSET, count));
        }
    }

    public List<TransactionLine> listTransactionLines(Map<String, Object> parameters) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {
            return session.selectList("com.technisys.omnichannel.client.handlers.multiline.listTransactionLines", parameters);
        }
    }

    public void updateTransactionLine(TransactionLine transactionLine) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openWriteSession()) {
            session.update("com.technisys.omnichannel.client.handlers.multiline.modifyTransactionLine", transactionLine);

            session.commit();
        }
    }

    public TransactionLinesTotals getTransactionLinesTotals(String idTransaction) throws IOException {
        try (SqlSession session = DBUtils.getInstance().openReadSession()) {
            return session.selectOne("com.technisys.omnichannel.client.handlers.multiline.getTransactionLinesTotals", idTransaction);
        }
    }
}
