/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.cyberbank.json.JsonObject;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import org.json.JSONObject;

public class CyberbankCoreRequest {

    private static final String PROJECT_ID = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "connector.cyberbank.core.project", "Tech");
    private static final String USER_ID = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "connector.cyberbank.core.userId", "TECHNISYS");
    private static final String CHANNEL_ID = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "connector.cyberbank.core.channelId", "8");
    private static final String WORKFLOW_ID = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "connector.cyberbank.core.workflowId", "massiveSelectGeneralTableQueryApoyo");
    private static final String BRANCH_ID = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "connector.cyberbank.core.branchId", "1");
    private static final String INSTITUTION_ID = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "connector.cyberbank.core.institutionId", "1111");

    private JSONObject requestParameters;
    private String project;
    private String transactionId;
    private boolean applyTemplate;



    private CyberbankCoreRequest() { }

    /**
     * Creates new Cyberbank Core request
     *
     * @param transactionId Transaction ID to execute on core (ex.: 'massiveSelectView_MenuBy_User_Branch')
     */
    public CyberbankCoreRequest(String transactionId, boolean applyTemplate) {

        this.project = PROJECT_ID;
        this.requestParameters = new JSONObject();
        this.transactionId = transactionId;
        this.applyTemplate = applyTemplate;
        if(!applyTemplate){
            this.requestParameters.put("glb.credential", new JSONObject()
                    .put("branchId", BRANCH_ID)
                    .put("institutionId", INSTITUTION_ID)
                    .put("userId", USER_ID)
                    .put("channelId", CHANNEL_ID)
                    .put("workflowId", WORKFLOW_ID)
                    .put("sourceTime", "20170704124255005-0300")
                    .put("parityQuotationTypeNemotecnic", "BOLETOS_INTERNOS")
                    .put("sessionId", "C521B16C2DDFC7C8330193328E174683")
                    .put("locale", "es_AR")
                    .put("localCountryId", "1")
                    .put("businessDate", "20090323000000000")
                    .put("parityCurrencyCodeId", "2")
                    .put("localCurrencyCodeId", "1")
                    .put("msgTypeId", "200")
                    .put("workflowModule","Apoyo")
                    .put("sourceDate", "20170704124255005-0300")
            );
        }else{
            requestParameters
                    .put("branchId" , BRANCH_ID)
                    .put("institutionId", INSTITUTION_ID)
                    .put("userId", USER_ID)
                    .put("channelId", CHANNEL_ID)
                    .put("workflowId", WORKFLOW_ID)
                    .put("sourceTime", "20170704124255005-0300")
                    .put("parityQuotationTypeNemotecnic", "BOLETOS_INTERNOS")
                    .put("sessionId", "C521B16C2DDFC7C8330193328E174683")
                    .put("locale", "es_AR")
                    .put("localCountryId", "1")
                    .put("businessDate", "20090323000000000")
                    .put("parityCurrencyCodeId", "2")
                    .put("localCurrencyCodeId", "1")
                    .put("msgTypeId", "200")
                    .put("workflowModule","Apoyo")
                    .put("sourceDate", "20170704124255005-0300");
        }

    }

    public void addCredential(String credential, String value) {
        if(!applyTemplate){
            ((JSONObject)this.requestParameters.get("glb.credential")).put(credential, value);
        }else{
            this.requestParameters.put(credential,value);
        }

    }

    /**
     * Add a new param to requestParameters
     * @param name Param name
     * @param value Param value
     */
    public void addRequestParameter(String name, JSONObject value) {
        this.requestParameters.put(name, value);
    }

    public CyberbankCoreRequest addRequestParameter(String name, JsonObject value) {
        this.requestParameters.put(name, value);
        return this;
    }
    
    /**
     * Add a new param to requestParameters
     * @param name Param name
     * @param value Param value
     */
    public CyberbankCoreRequest addRequestParameter(String name, Object value) {
        this.requestParameters.put(name, value);
        return this;
    }

    public String getTransactionId() {
        return transactionId;
    }

    /**
     * Gets Cyberbank Core request JSON String
     *
     * @return JSON String
     */
    public String getJSON() {
        String requestJson = "";
        if(!applyTemplate){
            requestJson =  new JSONObject()
                    .put("project", this.project)
                    .put("transactionId", this.transactionId)
                    .put("requestParameters", this.requestParameters)
                    .toString();

        }else if (requestParameters!= null) {
            requestParameters.put("project", this.project);
            requestParameters.put("transactionId", this.transactionId);
            requestJson = requestParameters.toString();
        }
        return requestJson;
    }

}
