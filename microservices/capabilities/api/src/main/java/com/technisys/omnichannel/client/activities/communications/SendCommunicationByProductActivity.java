/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.CurrencyUtils;
import com.technisys.omnichannel.client.utils.MaskUtils;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.client.utils.TemplatingUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.domain.RecipientCriteria;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.NumberUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

/**
 *
 */
@AnonymousActivity
public class SendCommunicationByProductActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(SendCommunicationByProductActivity.class);

    public static final String ID = "communications.sendByProduct";
    
    public interface InParams {
        String ID_COMMUNICATION_TRAY = "idCommunicationTray";

        String MESSAGE_TYPE = "messageType";

        String PRODUCT_TYPE = "producType";
        String PRODUCT_NUMBER = "productNumber";
        String CLIENT_ID = "clientId";
        String PRODUCT_CURRENCY = "productCurrency";
        String PRODUCT_ID = "productId";

        String CHANNELS = "channels";
    }

    public interface OutParams {
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            
            int idCommunicationTray = Integer.parseInt(request.getParam(InParams.ID_COMMUNICATION_TRAY, String.class));

            String messageType = request.getParam(InParams.MESSAGE_TYPE, String.class);
            String[] channels = request.getParam(InParams.CHANNELS, String[].class);
            Map<String, Object> fillers = request.getParam("fillers", Map.class);

            fillers.put("lang", I18n.LANG_ENGLISH);
            fillers.put("DateUtils", DateUtils.class);
            fillers.put("NumberUtils", NumberUtils.class);
            fillers.put("CurrencyUtils", CurrencyUtils.class);
            fillers.put("MaskUtils", MaskUtils.class);
            fillers.put("StringUtils", StringUtils.class);
            fillers.put("ProductLabeler", CoreUtils.getProductLabeler(I18n.LANG_ENGLISH));

            List<String> channelsList = new ArrayList();
            if (channels != null) {
                channelsList = Arrays.asList(channels);
            }

            Map<String, String> fieldsMap = new HashMap();
            fieldsMap.put(ProductUtils.FIELD_PRODUCT_TYPE, request.getParam(InParams.PRODUCT_TYPE, String.class));
            fieldsMap.put(ProductUtils.FIELD_PRODUCT_NUMBER, request.getParam(InParams.PRODUCT_NUMBER, String.class));
            fieldsMap.put(ProductUtils.FIELD_CLIENT_ID, request.getParam(InParams.CLIENT_ID, String.class));
            fieldsMap.put(ProductUtils.FIELD_PRODUCT_CURRENCY, request.getParam(InParams.PRODUCT_CURRENCY, String.class));
            if (Constants.PRODUCT_TC_KEY.equals(request.getParam(InParams.PRODUCT_TYPE, String.class))) {
                fieldsMap.put(ProductUtils.FIELD_PRODUCT_ID, request.getParam(InParams.PRODUCT_NUMBER, String.class));
            } else {
                fieldsMap.put(ProductUtils.FIELD_PRODUCT_ID, request.getParam(InParams.PRODUCT_ID, String.class));
            }

            String extraInfo = ProductUtils.generateExtraInfo(fieldsMap);
            String idProduct = ProductUtils.generateIdProduct(extraInfo);

            List<Integer> envs = Administration.listEnvironmentsWithProduct(idProduct);

            RecipientCriteria recipientCriteria = new RecipientCriteria(RecipientCriteria.RECIPIENT_TYPE_ENVIRONMENTS, null, envs, null, null);

            Map<String, String> bodyByTransportMap = new HashMap();

            String subject = I18nFactory.getHandler().getMessage("communications.send.communicationByProduct." + messageType + ".subject", I18n.LANG_ENGLISH);

            if (channelsList.contains(Communication.TRANSPORT_DEFAULT)) {
                bodyByTransportMap.put(Communication.TRANSPORT_DEFAULT, processBody(messageType, fillers, Communication.TRANSPORT_DEFAULT));
            }
            if (channelsList.contains(Communication.TRANSPORT_MAIL)) {
                bodyByTransportMap.put(Communication.TRANSPORT_MAIL, processBody(messageType, fillers, Communication.TRANSPORT_MAIL));
            }
            if (channelsList.contains(Communication.TRANSPORT_SMS)) {
                bodyByTransportMap.put(Communication.TRANSPORT_SMS, processBody(messageType, fillers, Communication.TRANSPORT_SMS));
            }

            NotificationsHandlerFactory.getHandler().create(null, idCommunicationTray,
                    subject, bodyByTransportMap, null, "message", -1, -1, Communication.PRIORITY_HIGH, recipientCriteria);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    private String processBody(String messageType, Map<String, Object> fillers, String channel) {
        String body = I18nFactory.getHandler().getMessage("communications.send.communicationByProduct." + messageType + "." + channel + ".body", I18n.LANG_ENGLISH);
        try {
            body = TemplatingUtils.evaluate(body, fillers);
        } catch (IOException e) {
            log.warn("Error while processing the body for the communications", e);
        }
        return body;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        int idCommunicationTray = 0;
        String idCommunicationTrayString = request.getParam(InParams.ID_COMMUNICATION_TRAY, String.class);
        if (org.apache.commons.lang3.StringUtils.isNotBlank(idCommunicationTrayString)) {
            try {
                idCommunicationTray = Integer.parseInt(idCommunicationTrayString);
            } catch (NumberFormatException e) {
                result.put("idCommunicationTray", "communications.send.idCommunicationTray.empty");
            }
        }
        String messageType = request.getParam(InParams.MESSAGE_TYPE, String.class);
        
        if (org.apache.commons.lang3.StringUtils.isBlank(messageType)) {
            result.put("subject", "communications.send.subject.messageType");
        }

        if (idCommunicationTray <= 0) {
            result.put("idCommunicationTray", "communications.send.idCommunicationTray.empty");
        }
        
        if (org.apache.commons.lang3.StringUtils.isBlank(request.getParam(InParams.PRODUCT_TYPE, String.class))) {
            result.put("idCommunicationTray", "communications.send.productType.empty");
        }

        if (org.apache.commons.lang3.StringUtils.isBlank(request.getParam(InParams.PRODUCT_NUMBER, String.class))) {
            result.put("idCommunicationTray", "communications.send.productNumber.empty");
        }

        return result;
    }
}