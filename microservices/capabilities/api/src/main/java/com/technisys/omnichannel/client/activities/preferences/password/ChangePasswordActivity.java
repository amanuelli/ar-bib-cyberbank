/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.password;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.credentials.CredentialPluginFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 * Validations: 
 *  <ul> 
 *   <li>current password not empty</li>
 *   <li>current password incorrect</li>
 *   <li>new password not empty</li>
 *   <li>new password equal new password confirmation</li>
 *   <li>new password wrong format</li>
 *   <li>new password not equal as last</li>
 *  </ul>
*/
@DocumentedActivity("Change Password")
public class ChangePasswordActivity extends Activity {

    public static final String ID = "preferences.changepassword.send";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Current password")
        String PASSWORD = "_password";
        @DocumentedParam(type = String.class, description = "New password")
        String NEW_PASSWORD = "_newPassword";
        @DocumentedParam(type = String.class, description = "New password confirmation")
        String NEW_PASSWORD_CONFIRMATION = "_newPasswordConfirmation";
    }

    public interface OutParams {}

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            // Modifico el password del usuario
            String newPassword = request.getParam(InParams.NEW_PASSWORD, String.class);
            if (StringUtils.isNotBlank(newPassword)) {
                CredentialPlugin credentialPlugin = CredentialPluginFactory.getCredentialPlugin(Credential.PWD_CREDENTIAL);
                credentialPlugin.modify(request, request.getCredential(Credential.PWD_CREDENTIAL).getValue(), newPassword);
            }
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (DispatchingException e) {
            throw new ActivityException(ReturnCodes.CREDENTIAL_PLUGIN_INSTANTIATION_ERROR, "Error instantiating Password credential validation plugin", e);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        
        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        // Solo chequeo validaciones de contraseña si el nuevo password no es vacío
        String password = request.getCredential(Credential.PWD_CREDENTIAL).getValue();
        String newPassword = request.getParam(InParams.NEW_PASSWORD, String.class);
        String newPasswordConfirmation = request.getParam(InParams.NEW_PASSWORD_CONFIRMATION, String.class);
        if (StringUtils.isNotBlank(password) || StringUtils.isNotBlank(newPassword) || StringUtils.isNotBlank(newPasswordConfirmation)) {

            if (!newPassword.equals(newPasswordConfirmation)) {
                result.put(InParams.NEW_PASSWORD, "preferences.password.newPassword.passwordNotTheSame");
                result.put(InParams.NEW_PASSWORD_CONFIRMATION, "");
            }

            if (StringUtils.isEmpty(password)) {
                result.put("_"+Credential.PWD_CREDENTIAL, "preferences.password.password.empty");
            } else {
                // Valido el password anterior
                try {
                    CredentialPlugin credentialPlugin = CredentialPluginFactory.getCredentialPlugin(Credential.PWD_CREDENTIAL);

                    if (StringUtils.isBlank(newPassword)) {
                        result.put(InParams.NEW_PASSWORD, "preferences.password.newPassword.empty");
                    } else if (!credentialPlugin.validateFormat(request, newPassword)) {
                        // Valido el formato de la nueva contraseña
                        result.put(InParams.NEW_PASSWORD, "preferences.password.newPassword.wrongFormat");
                    }

                    // Valido que la nueva contraseña no sea la mima que la anterior
                    if (newPassword.equals(password)) {
                        result.put(InParams.NEW_PASSWORD, "preferences.password.newPassword.sameAsLast");
                    }

                    // Valido la contraseña
                    if (result.isEmpty()) {
                        credentialPlugin.validate(request, new Credential("Password", password));
                    }

                } catch (DispatchingException | IOException e) {
                    result.put("_"+Credential.PWD_CREDENTIAL, "preferences.password.password.incorrect");
                }

            }

        }else{ 
            result.put("_" + Credential.PWD_CREDENTIAL, "preferences.password.password.empty"); 
            result.put(InParams.NEW_PASSWORD, "preferences.password.newPassword.empty"); 
        }

        return result;
    }
}
