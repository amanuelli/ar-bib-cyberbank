/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.handlers.notes;

import org.apache.ibatis.session.SqlSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author fpena
 */
class NotesDataAccess {

    private static NotesDataAccess instance;

    private NotesDataAccess(){
    }

    public static synchronized NotesDataAccess getInstance(){
        if (instance == null) {
            instance = new NotesDataAccess();
        }

        return instance;
    }
    
    public void createNote(SqlSession session, int idStatement, String idProduct, String note) throws IOException {
        
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idStatement", idStatement);
        parameters.put("idProduct", idProduct);
        parameters.put("note", note);

        session.insert("com.technisys.omnichannel.client.handlers.notes.createNote", parameters);

    }
    
    public void modifyNote(SqlSession session, int idStatement, String idProduct, String note) throws IOException {
        
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idStatement", idStatement);
        parameters.put("idProduct", idProduct);
        parameters.put("note", note);

        session.insert("com.technisys.omnichannel.client.handlers.notes.modifyNote", parameters);

    }
    
    public void deleteNote(SqlSession session, int idStatement, String idProduct) throws IOException {
       
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idStatement", idStatement);
        parameters.put("idProduct", idProduct);

        session.delete("com.technisys.omnichannel.client.handlers.notes.deleteNote", parameters);
       
    }
    
    public String readNote(SqlSession session, int idStatement, String idProduct) throws IOException {
        String result = null;

        Map<String, Object> parameters = new HashMap();
        parameters.put("idStatement", idStatement);
        parameters.put("idProduct", idProduct);

        result = (String) session.selectOne("com.technisys.omnichannel.client.handlers.notes.readNote", parameters);

        return result;
    }
}