package com.technisys.omnichannel.client.handlers.multiline;

import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.client.domain.TransactionLinesStatus;
import com.technisys.omnichannel.client.domain.TransactionLinesTotals;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class MultilineHandler {

    private MultilineHandler() { }

    public static void addTransactionLines(List<TransactionLine> transactionLines) throws IOException {
        MultilineDataAccess.getInstance().addTransactionLines(transactionLines);
    }

    public static void deleteTransactionLine(String idTransaction, Integer lineNumber) throws IOException {
        MultilineDataAccess.getInstance().deleteTransactionLine(idTransaction, lineNumber);
    }

    public static TransactionLine getNextPendingTransaction() throws IOException {
        return MultilineDataAccess.getInstance().getNextPendingTransaction();
    }

    public static TransactionLine getTransactionLine(String idTransaction, Integer lineNumber) throws IOException {
        return MultilineDataAccess.getInstance().getTransactionLine(idTransaction, lineNumber);
    }

    public static List<TransactionLinesStatus> getTransactionLinesStatus(String idTransaction) throws IOException {
        return MultilineDataAccess.getInstance().getTransactionLinesStatus(idTransaction);
    }

    public static List<TransactionLine> getTransactionLinesWithErrors(String idTransaction) throws IOException {
        return MultilineDataAccess.getInstance().getTransactionLinesWithErrors(idTransaction);
    }

    public static List<TransactionLine> listPendingTransactionLinesByCount(String idTransaction, Integer count) throws IOException {
        return MultilineDataAccess.getInstance().listPendingTransactionLinesByCount(idTransaction, count);
    }

    public static List<TransactionLine> listTransactionLines(Map<String, Object> parameters) throws IOException {
        return MultilineDataAccess.getInstance().listTransactionLines(parameters);
    }

    public static void updateTransactionLine(TransactionLine transactionLine) throws IOException {
        MultilineDataAccess.getInstance().updateTransactionLine(transactionLine);
    }

    public static void updateTransactionLines(List<TransactionLine> transactionLines) throws IOException {
        for (TransactionLine transactionLine : transactionLines) {
            updateTransactionLine(transactionLine);
        }
    }

    public static TransactionLinesTotals getTransactionLinesTotals(String idTransaction) throws IOException {
        return MultilineDataAccess.getInstance().getTransactionLinesTotals(idTransaction);
    }
}