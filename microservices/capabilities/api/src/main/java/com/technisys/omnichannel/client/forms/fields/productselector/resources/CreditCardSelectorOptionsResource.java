package com.technisys.omnichannel.client.forms.fields.productselector.resources;

import com.technisys.omnichannel.client.activities.creditcards.ListCreditCardsActivity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.client.forms.fields.productselector.ProductselectorFieldHandler;
import com.technisys.omnichannel.client.forms.fields.productselector.ProductselectorFieldHandler.Option;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.fields.ProductselectorField;
import com.technisys.omnichannel.core.forms.fields.FrontendOption;
import com.technisys.omnichannel.core.forms.fields.OptionsResource;
import com.technisys.omnichannel.core.utils.CoreUtils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Implementacion del recurso de opciones para subtipo creditCardSelector del
 * campo productSelector
 *
 * @author ?
 */
public class CreditCardSelectorOptionsResource implements OptionsResource {

    @Override
    public List<? extends FrontendOption> getOptions(FormField field, Request request) throws IOException {
        List<Option> options = new ArrayList<>();

        try {
            // Cargo los productos desde el backend
            ProductselectorField productSelectorField = (ProductselectorField) field;
            List<CreditCard> creditCards = ListCreditCardsActivity.getCreditCardList(request, productSelectorField.getProductTypeList());
            ProductselectorFieldHandler psfh = new ProductselectorFieldHandler();
            for (CreditCard cc : creditCards) {
                
                Option option = psfh.new Option(); 
                option.setMinimumPayment(cc.getMinimumPayment());
                option.setTotalPayment(cc.getBalance());
                
                // Id
                option.setId(cc.getIdProduct());
                // Etiqueta
                option.setLabel(CoreUtils.getProductLabeler(request.getLang()).calculateShortLabel(cc));
                //tipo
                option.setType(ProductUtils.getCreditCardBrand(ProductUtils.getNumber(cc.getExtraInfo()), request.getLang()));
                options.add(option);
            }
        } catch (BackendConnectorException bce) {
            throw new IOException(bce);
        }
        return options;
    }
}
