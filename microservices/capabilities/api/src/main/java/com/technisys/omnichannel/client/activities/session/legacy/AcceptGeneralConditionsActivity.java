/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session.legacy;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.CalendarRestrictionsUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.GeneralCondition;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.preprocessors.ipauthorization.IPAuthorizationHandler;
import com.technisys.omnichannel.core.session.SessionUtils;

import java.io.IOException;
import java.util.*;

/**
 * 
 */
@ExchangeActivity
@DocumentedActivity("Accept General Conditions")
public class AcceptGeneralConditionsActivity extends Activity {
    
    public static final String ID = "session.login.legacy.step4";

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
        @DocumentedParam(type = Boolean.class, description = "True for accept conditions")
        String ACCEPT_CONDITIONS = "acceptConditions";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "User's identifier")
        String USERNAME = "username";
        @DocumentedParam(type = String.class, description = "User's full name, e.g. &quot;Brittny Beall")
        String USER_FULLNAME = "userFullName";
        @DocumentedParam(type = String.class, description = "User's default Avatar identifier")
        String DEFAULT_AVATAR_ID = "defaultAvatarId";
        @DocumentedParam(type = Integer.class, description = "User's security seal identifier")
        String SECURITY_SEAL = "_securitySeal";
        @DocumentedParam(type = Integer.class, description = "User's active environment identifier")
        String ACTIVE_ENVIRONMENT_ID = "activeIdEnvironment";
        @DocumentedParam(type = String.class, description = "User's active environment name")
        String ACTIVE_ENVIRONMENT_NAME = "activeEnvironmentName";
        @DocumentedParam(type = String.class, description = "User's active environment administration scheme")
        String ADMINISTRATION_SCHEME = "administrationScheme";
        @DocumentedParam(type = Map.class, description = "Map of available environments: &lt;identifier, name&gt;")
        String ENVIRONMENTS = "environments";
        @DocumentedParam(type = Map.class, description = "Map of available permissions: &lt;identifier, boolean indicator&gt;")
        String PERMISIONS = "permissions";
        @DocumentedParam(type = List.class, description = "List of forms, those the user can interact with")
        String FORMS = "forms";
        @DocumentedParam(type = List.class, description = "List of activities identifiers, those the user most use")
        String FREQUENT_ACTIONS = "frequentActions";
        @DocumentedParam(type = String.class, description = "Default language, selected by the user, e.g. &quot;en&quot;")
        String LANGUAGE = "lang";

        @DocumentedParam(type = String.class, description = "Token required to call the authenticated activities")
        String ACCESS_TOKEN = "_accessToken";
        @DocumentedParam(type = String.class, description = "Token required to call the next step on this process")
        String PREVIOUS_LOGIN_INFO = "previousLoginInfo";
        @DocumentedParam(type = String.class, description = "Tells if the user can administrate the environment")
        String IS_ADMINISTRATOR = "isAdministrator";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            
            boolean acceptConditions = request.getParam(InParams.ACCEPT_CONDITIONS, boolean.class);

            int actualCondition = GeneralConditions.getInstance().actualCondition();

            int idEnvironment = getIdEnvironmentToAcceptConditions(request);
            if (acceptConditions){
                //Acepto las condiciones generales
                List <Integer> environmentList = new ArrayList();
                environmentList.add(idEnvironment);
                GeneralConditions.getInstance().acceptGeneralConditions(actualCondition, request.getIdUser(), environmentList);
            }

            ExchangeTokenHandler.delete(request.getParam(EXCHANGE_TOKEN, String.class));

            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            Environment activeEnvironment = Administration.getInstance().readEnvironment(idEnvironment);

            List<Environment> environmentList = Administration.getInstance().listEnvironmentsWhereUserIsActive(request.getIdUser(), request.getChannel());

            request.setIdEnvironment(activeEnvironment.getIdEnvironment());
            boolean calendarIsValid;
            boolean ipsAreValid;

            for (Environment env : environmentList){
                calendarIsValid = CalendarRestrictionsUtils.validateAccessByEnvironmentAndUser(env.getIdEnvironment(),request.getIdUser(), request.getValueDate());
                ipsAreValid = IPAuthorizationHandler.getInstance().checkUserEnvironmentAccess(env.getIdEnvironment(), request.getIdUser(), request.getClientIP());
                env.setAllowToAccess(calendarIsValid && ipsAreValid);
            }
            Response response = SelectEnvironmentActivity.generateLastLoginStepResponse(request, user, activeEnvironment, SessionUtils.assembleEnviromentMap(environmentList));

            boolean isAdministrator = Authorization.hasPermission(request.getIdUser(), idEnvironment, null, Constants.ADMINISTRATION_VIEW_PERMISSION);
            response.putItem(OutParams.IS_ADMINISTRATOR, isAdministrator);

            response.setReturnCode(ReturnCodes.OK);
                    
            return response;

        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            
            boolean acceptConditions = request.getParam(InParams.ACCEPT_CONDITIONS, boolean.class);

            GeneralConditions gcHandler = GeneralConditions.getInstance();

            int actualCondition = gcHandler.actualCondition();

            EnvironmentUser envUser = Administration.getInstance().readEnvironmentUserInfo(request.getIdUser(), getIdEnvironmentToAcceptConditions(request));

            if (!acceptConditions){
                GeneralCondition generalCondition = gcHandler.readGeneralConditionInfo(actualCondition);
                if ((envUser.getIdCondition() == 0) || (generalCondition.getDateFinal().before(new Date()))){
                    result.put("NO_FIELD", "generalConditions.mustAcceptConditions");
                }
            }
        
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        return result;
    }

    protected int getIdEnvironmentToAcceptConditions(Request request) throws IOException {
        String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);
        ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenId);

        return exchangeToken.getIdEnvironment();
    }

}
