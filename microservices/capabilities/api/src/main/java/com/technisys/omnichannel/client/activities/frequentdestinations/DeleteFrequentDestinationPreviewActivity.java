/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.frequentdestinations;


import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

/**
 *
 */
public class DeleteFrequentDestinationPreviewActivity extends Activity {

    public static final String ID = "frequentdestinations.delete.preview";
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        DeleteFrequentDestinationActivity deleteActivity = new DeleteFrequentDestinationActivity();
        Map<String, String> result = deleteActivity.validate(request);
        return result;
    }
}
