/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.products;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 * Return a list of the user products to be listed in settings
 */
@DocumentedActivity("List of the user products")
public class GetProductsListActivity extends Activity {

    public static final String ID = "preferences.products.list";

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "List of loans")
        String LOANS = "loans";

        @DocumentedParam(type = String.class, description = "List of accounts")
        String ACCOUNTS = "accounts";

        @DocumentedParam(type = String.class, description = "List of credit cards")
        String CREDIT_CARDS = "creditCards";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            List<String> idPermissions = new ArrayList<>();
            idPermissions.add(PRODUCT_READ_PERMISSION);

            List<String> productTypes = Arrays.asList(Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY, Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY, Constants.PRODUCT_TC_KEY);
            List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);

            response.putItem(OutParams.LOANS, getLoans(request, environment.getClients(), environmentProducts));
            response.putItem(OutParams.ACCOUNTS, getAccounts(request, environment.getClients(), environmentProducts));
            response.putItem(OutParams.CREDIT_CARDS, getCreditCards(request, environment.getClients(), environmentProducts));

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    List<Loan> getLoans(Request request, List<Client> clients, List<Product> environmentProducts) throws BackendConnectorException {
        List<Loan> coreProducts = CoreLoanConnectorOrchestrator.list(request.getIdTransaction(), clients, Arrays.asList(Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY));

        List<Loan> loans = new ArrayList<>();
        for (Loan loan : coreProducts) {
            for (Product environmentProduct : environmentProducts) {
                if (loan.getIdProduct().equals(environmentProduct.getIdProduct())) {
                    String loannumber = StringUtils.defaultString(ProductUtils.getNumber(environmentProduct.getExtraInfo()));
                    String loanCurrency = ProductUtils.getCurrency(environmentProduct.getExtraInfo());
                    String loanLabel = StringUtils.leftPad(loannumber, 15) + " "
                            + loan.getProductType() + " " + I18nFactory.getHandler().getMessage("core.currency.label." + loanCurrency, request.getLang());
                    loan.setProductAlias(environmentProduct.getProductAlias());
                    loan.setShortLabel(loanLabel);
                    loan.setIdProduct(environmentProduct.getIdProduct());
                    loan.setProductType(environmentProduct.getProductType());
                    loan.setPaperless(environmentProduct.getPaperless());
                    loans.add(loan);
                }
            }
        }
        return loans;
    }

    List<Account> getAccounts(Request request, List<Client> clients, List<Product> environmentProducts) throws BackendConnectorException {
        List<Account> accounts = new ArrayList<Account>();
        List<Account> coreAccounts = CoreAccountConnectorOrchestrator.list(request.getIdTransaction(), clients, Arrays.asList(Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY));

        ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
        for (Product environmentProduct : environmentProducts) {
            for (Account coreAccount : coreAccounts) {
                if (coreAccount.getIdProduct().equals(environmentProduct.getIdProduct())) {
                    Account account = new Account(environmentProduct);
                    account.setProductAlias(environmentProduct.getProductAlias());
                    account.setShortLabel(pLabeler.calculateShortLabel(account, false));
                    account.setBalance(coreAccount.getBalance());
                    account.setStatus(coreAccount.getStatus());
                    account.setPaperless(environmentProduct.getPaperless());
                    accounts.add(account);
                }
            }
        }
        return accounts;
    }

    List<CreditCard> getCreditCards(Request request, List<Client> clients, List<Product> environmentProducts) throws BackendConnectorException {
        List<Product> coreProducts = (List<Product>) RubiconCoreConnectorC.listProducts(request.getIdTransaction(), clients, Arrays.asList(Constants.PRODUCT_TC_KEY));

        ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
        List<CreditCard> creditCards = new ArrayList<CreditCard>();

        for (Product p : coreProducts) {
            for (Product environmentProduct : environmentProducts) {
                if (p.getExtraInfo().equals(environmentProduct.getExtraInfo())) {
                    CreditCard card = (CreditCard) p;
                    String number = card.getNumber();
                    card.setProductAlias(environmentProduct.getProductAlias());
                    card.setShortLabel(pLabeler.calculateShortLabel(card));
                    card.setLabel(pLabeler.calculateLabel(card));
                    card.setFranchise(pLabeler.calculateFranchise(card));
                    card.setHolder("•••• •••• •••• " + number.substring(number.length() - 4, number.length()));
                    card.setIdProduct(environmentProduct.getIdProduct());
                    card.setPaperless(environmentProduct.getPaperless());
                    creditCards.add(card);
                }
            }
        }
        return creditCards;
    }
}