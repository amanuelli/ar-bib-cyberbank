/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.domain.UserDevice;
import org.json.JSONObject;

/**
 *
 * @author dimoda
 */
public class ClientUserDevice extends UserDevice {

    private Boolean available;
    private String platform;
    private String version;
    private String uuid;
    private String cordova;
    private String model;
    private String manufacturer;
    private Boolean isVirtual;
    private String serial;
    private Boolean isTablet;
    private Boolean isAndroid;
    private Boolean isIOS;

    public ClientUserDevice(UserDevice userDevice) {
        super(userDevice.getIdUser(), userDevice.getIdDevice(), userDevice.getExtraInfo(), userDevice.getPushToken());
        super.setCreationDate(userDevice.getCreationDate());
        super.setModificationDate(userDevice.getModificationDate());
        super.setLastEntryCountry(userDevice.getLastEntryCountry());
        super.setLastEntryCity(userDevice.getLastEntryCity());

        JSONObject jsonObj = new JSONObject(userDevice.getExtraInfo());
        this.available = jsonObj.optBoolean("available");
        this.platform = jsonObj.optString("platform");
        this.version = jsonObj.optString("version");
        this.uuid = jsonObj.optString("uuid");
        this.cordova = jsonObj.optString("cordova");
        this.model = jsonObj.optString("model");
        this.manufacturer = jsonObj.optString("manufacturer");
        this.isVirtual = jsonObj.optBoolean("isVirtual");
        this.serial = jsonObj.optString("serial");
        this.isTablet = jsonObj.optBoolean("isTablet");
        this.isAndroid = jsonObj.optBoolean("isAndroid");
        this.isIOS = jsonObj.optBoolean("isIOS");
    }

    public Boolean getAvailable() {
        return available;
    }

    public void setAvailable(Boolean available) {
        this.available = available;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getCordova() {
        return cordova;
    }

    public void setCordova(String cordova) {
        this.cordova = cordova;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public Boolean getIsVirtual() {
        return isVirtual;
    }

    public void setIsVirtual(Boolean isVirtual) {
        this.isVirtual = isVirtual;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Boolean getisTablet() {
        return isTablet;
    }

    public void setisTablet(Boolean isTablet) {
        this.isTablet = isTablet;
    }

    public Boolean getisAndroid() {
        return isAndroid;
    }

    public void setisAndroid(Boolean isAndroid) {
        this.isAndroid = isAndroid;
    }

    public Boolean getisIOS() {
        return isIOS;
    }

    public void setisIOS(Boolean isIOS) {
        this.isIOS = isIOS;
    }
}
