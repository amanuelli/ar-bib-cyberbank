/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.connectors.RubiconCoreConnector;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.rubicon.core.deposits.WsReadDepositResponse;
import java.util.Date;
import java.util.Map;

/**
 *
 * @author fpena
 */
public class Deposit extends Account {

    private String depositNumber;
    private Date openDate;
    private Date dueDate;
    private Date nextInterestPaymentDate;
    private double rate;
    private double nextPaymentAmount;
    private int deadline;

    public Deposit(Map<String, String> fieldsMap) {
        super(fieldsMap);

        this.depositNumber = fieldsMap.get(ProductUtils.FIELD_PRODUCT_NUMBER);
        setBalance(RubiconCoreConnector.parseDouble(fieldsMap.get("balance")));

        this.rate = RubiconCoreConnector.parseDouble(fieldsMap.get("rate"));
        this.dueDate = RubiconCoreConnector.parseDate(fieldsMap.get("dueDate"));
    }

    public Deposit(WsReadDepositResponse response) {
        super(RubiconCoreConnector.convertDynamicListToMap(response.getFields().getField()));

        this.depositNumber = ProductUtils.getNumber(extraInfo);
        setBalance(response.getBalance());

        this.rate = response.getRate();
        this.deadline = response.getDeadline();
        this.dueDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(response.getDueDate());
        this.openDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(response.getOpenDate());
        this.nextInterestPaymentDate = RubiconCoreConnector.convertXMLGregorianCalendarToDate(response.getNextInterestPaymentDate());
        this.nextPaymentAmount = response.getNextPaymentAmount();
        this.consolidatedAmount = response.getConsolidatedAmount();
    }

    public String getDepositNumber() {
        return depositNumber;
    }

    public void setDepositNumber(String depositNumber) {
        this.depositNumber = depositNumber;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }

    public double getRate() {
        return rate;
    }

    public Date getOpenDate() {
        return openDate;
    }

    public void setOpenDate(Date openDate) {
        this.openDate = openDate;
    }

    public Date getDueDate() {
        return dueDate;
    }

    public Date getNextInterestPaymentDate() {
        return nextInterestPaymentDate;
    }

    public void setNextInterestPaymentDate(Date nextInterestPaymentDate) {
        this.nextInterestPaymentDate = nextInterestPaymentDate;
    }

    public void getRate(double rate) {
        this.rate = rate;
    }

    public void setDueDate(Date dueDate) {
        this.dueDate = dueDate;
    }

    public double getNextPaymentAmount() {
        return nextPaymentAmount;
    }

    public void setNextPaymentAmount(double nextPaymentAmount) {
        this.nextPaymentAmount = nextPaymentAmount;
    }

    public int getDeadline() {
        return deadline;
    }

    public void setDeadline(int deadline) {
        this.deadline = deadline;
    }

}
