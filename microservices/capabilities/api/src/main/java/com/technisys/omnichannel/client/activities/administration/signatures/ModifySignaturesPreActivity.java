/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Feature;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.Signature;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.limits.CapForSignature;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.core.utils.CoreUtils.CHANNEL_ALL;

/**
 * Modify signatures scheme (PRE)
 */
@DocumentedActivity("Modify signatures scheme (PRE)")
public class ModifySignaturesPreActivity extends Activity {

    public static final String ID = "administration.signatures.modify.pre";

    public interface InParams {
        
        @DocumentedParam(type = Integer.class, description = "Signatures scheme's identifier")
        String ID = "id";
    }

    public interface OutParams {

        @DocumentedParam(type = Map.class, description = "Caps for signature")
        String CAPS = "caps";
        @DocumentedParam(type = List.class, description = "List of frequencies for transactions with amount")
        String CAP_FREQUENCY_LIST = "capFrequencyList";
        @DocumentedParam(type = List.class, description = "List of enabled channels")
        String ENABLED_CHANNELS = "enabledChannels";
        @DocumentedParam(type = String[].class, description = "Selected functional groups")
        String FUNCTIONAL_GROUPS = "functionalGroups";
        @DocumentedParam(type = String.class, description = "Master currency")
        String MASTER_CURRENCY = "masterCurrency";
        @DocumentedParam(type = List.class, description = "Selected functional groups")
        String SELECTED_FUNCTIONAL_GROUPS = "selectedFunctionalGroups";
        @DocumentedParam(type = Signature.class, description = "Signature data")
        String SIGNATURE = "signature";
        @DocumentedParam(type = Signature.class, description = "Signature data")
        String ENVIRONMENT_PRODUCTS = "environmentProducts";
        @DocumentedParam(type = Map.class, description = "Number of signers for each signature level")
        String SIGNATURE_GROUP_MAP = "signatureGroupMap";
        @DocumentedParam(type = List.class, description = "List of available signature types")
        String SIGNATURE_TYPE_LIST = "signatureTypeList";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Administration administration = Administration.getInstance();
            String adminScheme = request.getEnvironmentAdminScheme();
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(adminScheme)) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            int idSignature = request.getParam(InParams.ID, int.class);
            Signature signature = LimitsHandlerFactory.getHandler().readSignature(idSignature);
            
            // CAPS
            Map<String, CapForSignature> caps = new HashMap<>();
            double defaultCapMaximum = ConfigurationFactory.getInstance().getDouble(Configuration.LIMITS, "default_cap_signature_" + request.getEnvironmentType());

            CapForSignature cap = signature.getCap(CHANNEL_ALL);
            if (cap != null) {
                if (cap.getMaximum() < 0) {
                    cap.setMaximum(defaultCapMaximum);
                }
                caps.put(CHANNEL_ALL, cap);
            }

            Map<String, Integer> signatureGroupMap = new HashMap<>();
            String currLevel;
            Integer totalNedded;
            for (int i = 0; i < signature.getSignatureGroup().length(); i++) {
                currLevel = String.valueOf(signature.getSignatureGroup().charAt(i));
                totalNedded = signatureGroupMap.get(currLevel);
                if (totalNedded == null) {
                    totalNedded = 0;
                }

                signatureGroupMap.put(currLevel, totalNedded + 1);
            }
            
            List<Product> products = loadEnviromentProductsLabeled(administration, request.getIdEnvironment(), request.getLang());
            
            response.putItem(OutParams.ENVIRONMENT_PRODUCTS, products);
            response.putItem(OutParams.SIGNATURE, signature);
            response.putItem(OutParams.SIGNATURE_GROUP_MAP, signatureGroupMap);
            response.putItem(OutParams.SIGNATURE_TYPE_LIST, Arrays.asList(new String[]{Signature.TYPE_AMOUNT, Signature.TYPE_NO_AMOUNT}));
            response.putItem(OutParams.CAP_FREQUENCY_LIST, ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "frontend.channels.enabledFrequencies"));
            response.putItem(OutParams.MASTER_CURRENCY, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency"));
            
            String environmentType = request.getEnvironmentType();
            
            List<Map<String, String>> functionalGroups = new ArrayList<>();
            for (Feature feature : LimitsHandlerFactory.getHandler().listFeatures(environmentType)) {
                Map<String, String> functionalGroup = new HashMap<>();
                functionalGroup.put("idFeature", feature.getIdFeature());
                
                functionalGroups.add(functionalGroup);
            }
            
            response.putItem(OutParams.FUNCTIONAL_GROUPS, functionalGroups);
            
            response.putItem(OutParams.SELECTED_FUNCTIONAL_GROUPS, signature.getSignatureFeatures());

            List enabledChannels = new ArrayList<>();
            enabledChannels.add(CHANNEL_ALL);
            response.putItem(OutParams.ENABLED_CHANNELS, enabledChannels);
            response.putItem(OutParams.CAPS, caps);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    private List<Product> loadEnviromentProductsLabeled(Administration administration, Integer idEnvironment, String lang) throws IOException{
        List<Product> products = administration.listEnvironmentProducts(idEnvironment);
        products = products == null ? Collections.emptyList() : products;
        populateProductLabes(products, lang);
        return products;
	}
    
	private void populateProductLabes(List<Product> products, String lang) {
		ProductLabeler productLabeler = CoreUtils.getProductLabeler(lang);
		products.forEach((p) -> p.setLabel(productLabeler.calculateShortLabel(p)));
	}

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            int idSignature = request.getParam(InParams.ID, int.class);
            Signature signature = LimitsHandlerFactory.getHandler().readSignature(idSignature);

            if (signature == null || signature.getIdEnvironment() != request.getIdEnvironment() || Signature.TYPE_ADMINISTRATIVE.equals(signature.getSignatureType())) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
