/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.AccountStatus;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.ISOProduct;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.ProductType;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.utils.JsonTemplateUtils;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.utils.CacheUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CyberbankConsolidateConnector extends CyberbankCoreConnector {
    private static final Logger LOG = LoggerFactory.getLogger(CyberbankConsolidateConnector.class);
    private static final String SERVICE_NAME = "Srv - Consolidate";
    private static final String LOAN_ISO_PRODUCT_LIST_CACHE_KEY = "loan.isoProducts.list";

    public static CyberbankCoreConnectorResponse<List<Product>> products(String customerId, List<String> productTypes) throws CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<List<Product>> response = new CyberbankCoreConnectorResponse<>();


        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("processCustomer_OperationTo_Construct_A_Consolidated_Info_Channel", true)
                .addRequestParameter("customerId", customerId);

        LOG.info("{}: Request {}", SERVICE_NAME, URL);

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(SERVICE_NAME, jsonRequest);

        if (callHasError(serviceResponse)) {
            processErrors(SERVICE_NAME, serviceResponse, response);
        } else {
            response.setData(processServiceResponse(customerId, serviceResponse, productTypes));
        }

        return response;
    }

    public static List<ISOProduct> listLoanISOProducts() throws CyberbankCoreConnectorException, IOException {
        List<ISOProduct> response = (List<ISOProduct>) CacheUtils.get(LOAN_ISO_PRODUCT_LIST_CACHE_KEY);

        if (response == null) {
            response = new ArrayList<>();
            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("massiveSelectProductBy_Iso_Product_Loan_Prestamo", true);

            LOG.info("{}: Request {}", SERVICE_NAME, URL);

            String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(SERVICE_NAME, jsonRequest);

            if (callHasError(serviceResponse)) {
                throw new CyberbankCoreConnectorException("And error happened searching for loan products id");
            } else {
                if (serviceResponse.getJSONObject("out.product_list").has("collection")) {
                    JSONArray products = serviceResponse.getJSONObject("out.product_list").getJSONArray("collection");

                    for (int i = 0; i < products.length(); i++) {
                        JSONObject jsonProduct = products.getJSONObject(i);
                        ISOProduct isoProduct = new ISOProduct();

                        isoProduct.setId(jsonProduct.getInt("id"));
                        isoProduct.setProductId(jsonProduct.getString("productId"));
                        isoProduct.setShortDesc("shortDesc");
                        isoProduct.setLongDesc("longDesc");

                        response.add(isoProduct);
                    }
                }
                CacheUtils.put(LOAN_ISO_PRODUCT_LIST_CACHE_KEY, response);
            }
        }
        return response;
    }


    /**
     * Get a product type from core based on product code and sub-product code
     *
     * @param productCode
     * @param subProductCode
     * @return
     * @throws IOException
     * @throws CyberbankCoreConnectorException
     */
    public static ProductType getProductType(String productCode, String subProductCode) throws IOException, CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<List<ProductType>> response = productTypes();
        List<ProductType> productTypes = response.getData();
        return productTypes.stream()
                .filter(productType -> subProductCode.equals(productType.getSubProductCode()) && productCode.equals(productType.getProductCode()))
                .findAny()
                .orElse(null);
    }

    public static CyberbankCoreConnectorResponse<List<ProductType>> productTypes() throws IOException, CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<List<ProductType>> response = new CyberbankCoreConnectorResponse<>();

        List<ProductType> productTypes = (List<ProductType>) CacheUtils.get("cyberbank.core.productTypes");

        if (productTypes != null) {
            response.setData(productTypes);
        } else {
            CyberbankCoreRequest request = new CyberbankCoreRequest("massiveSelectSubproductBy_Iso_Product", true);
            ArrayList<ProductType> result = new ArrayList<>();
            String jsonRequest = JsonTemplateUtils.applyTemplateToJson(request.getJSON(), REQ_TEMPLATE_PATH + request.getTransactionId());
            JSONObject serviceResponse = call("Srv - massiveSelectSubproductBy_Iso_Product", jsonRequest);
            if (callHasError(serviceResponse)) {
                processErrors(SERVICE_NAME, serviceResponse, response);
            } else {
                if (serviceResponse.has("out.subproduct_list") && serviceResponse.getJSONObject("out.subproduct_list").has("collection")) {
                    JSONArray productsTypes = serviceResponse.getJSONObject("out.subproduct_list").getJSONArray("collection");

                    productsTypes.forEach(obj -> {
                        if (obj instanceof JSONObject) {
                            JSONObject productType = (JSONObject) obj;

                            result.add(
                                    new ProductType(
                                            productType.getJSONObject("product").getString("productId"),
                                            productType.getJSONObject("product").getInt("id"),
                                            productType.getString("subproductId"),
                                            productType.getInt("id")
                                    )
                            );
                        }
                    });

                    CacheUtils.put("cyberbank.core.productTypes", result);
                    response.setData(result);
                } else {
                    response.setData(new ArrayList<>());
                }
            }
        }

        return response;
    }

    private static List<Product> processServiceResponse(String customerId, JSONObject response, List<String> productTypes) throws CyberbankCoreConnectorException {
        try {
            if (!response.getJSONObject("out.cust_ope_balance_list").has("collection")) {
                return new ArrayList<>();
            }

            JSONArray productList = response.getJSONObject("out.cust_ope_balance_list").getJSONArray("collection");
            ArrayList<Product> result = new ArrayList<>();

            productList.forEach(obj -> {
                if (obj instanceof JSONObject) {
                    JSONObject product = (JSONObject) obj;
                    Object coreIdProduct = product.has("string10") ? product.get("string10") : null;

                    switch (String.valueOf(coreIdProduct)) {
                        case "02000":
                        case "01000":
                            if (productTypes.contains(Constants.PRODUCT_CA_KEY) || productTypes.contains(Constants.PRODUCT_CC_KEY)) {
                                result.add(processAccount(customerId, product));
                            }
                            break;
                        case "09001":
                            result.add(processLoan(customerId, product));
                            break;
                        default:
                            LOG.info("{}: Product not recognized", SERVICE_NAME);
                            if (LOG.isDebugEnabled()) {
                                LOG.debug("Unrecognized product: {}", obj);
                            }
                    }
                }
            });

            return result;
        } catch (JSONException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    private static Account processAccount(String customerId, JSONObject object) {

        Account account = new Account();

        HashMap<String, String> fieldMap = new HashMap<>();

        String productType = object.getString("string10").equals("02000") ? Constants.PRODUCT_CC_KEY : Constants.PRODUCT_CA_KEY;
        String accountNumber = object.getString("string7");
        String coreSubProductId = object.getString("string11");
        String coreIsoProduct = String.valueOf(object.getInt("integer1"));
        String branch = String.valueOf(object.getInt("integer11"));
        String currencyCode = String.valueOf(object.getInt("integer10"));

        Integer physicCurrencyId = object.getInt("integer2");
        Integer physicProductId = object.getInt("integer3");
        Integer physicAccountId = object.getInt("integer13");
        Integer physicBranchId = object.getInt("integer6");

        String coreExtraInfo = String.format("|%s|%s|%s|%s", coreIsoProduct, coreSubProductId, branch, currencyCode);
        String physicIds = String.format("|%s|%s|%s|%s", physicAccountId, physicProductId, physicCurrencyId, physicBranchId);

        fieldMap.put(ProductUtils.FIELD_PRODUCT_TYPE, productType);
        fieldMap.put(ProductUtils.FIELD_PRODUCT_NUMBER, accountNumber);
        fieldMap.put(ProductUtils.FIELD_PRODUCT_CURRENCY, "USD"); // TODO see currencies with core
        fieldMap.put(ProductUtils.FIELD_PRODUCT_ID, object.getString("string10"));

        account.setExtraInfo(ProductUtils.generateExtraInfo(fieldMap) + coreExtraInfo + physicIds);
        account.setIdProduct(ProductUtils.generateIdProduct(accountNumber + productType));
        account.setOwnerName(object.getString("string1"));
        account.setProductAlias(object.getString("string14"));
        account.setLabel(object.getString("string4"));
        account.setProductType(productType);
        account.setNumber(accountNumber);
        account.setBalance(Double.parseDouble(object.getString("double1")));
        account.setCountableBalance(Double.parseDouble(object.getString("double2")));
        account.setConsolidatedAmount(Double.parseDouble(object.getString("double1")));
        account.setCurrency("USD");
        account.setClient(new Client(customerId, ""));
        account.setNationalIdentifier(object.getString("string12"));

        AccountStatus accountStatus = new AccountStatus();
        accountStatus.setStatusCode(object.getInt("integer12"));

        account.setStatus(accountStatus);

        return account;
    }

    private static Loan processLoan(String customerId, JSONObject object) {

        Loan loan = new Loan();

        HashMap<String, String> fieldMap = new HashMap<>();

        String productType = Constants.PRODUCT_PA_KEY;
        String loanNumber = object.getString("string7");
        String coreSubProductId = object.getString("string11");
        String coreIsoProduct = String.valueOf(object.getInt("integer1"));
        String branch = String.valueOf(object.getInt("integer11"));
        String currencyCode = String.valueOf(object.getInt("integer10"));

        Integer physicCurrencyId = object.getInt("integer2");
        Integer physicProductId = object.getInt("integer3");
        Integer physicloanId = object.getInt("integer14");

        String coreExtraInfo = String.format("|%s|%s|%s|%s", coreIsoProduct, coreSubProductId, branch, currencyCode);
        String physicIds = String.format("|%s|%s|%s", physicloanId, physicProductId, physicCurrencyId);

        fieldMap.put(ProductUtils.FIELD_PRODUCT_TYPE, productType);
        fieldMap.put(ProductUtils.FIELD_PRODUCT_NUMBER, loanNumber);
        fieldMap.put(ProductUtils.FIELD_PRODUCT_CURRENCY, "USD"); // TODO see currencies with core
        fieldMap.put(ProductUtils.FIELD_PRODUCT_ID, object.getString("string10"));

        loan.setExtraInfo(ProductUtils.generateExtraInfo(fieldMap) + coreExtraInfo + physicIds);
        loan.setIdProduct(ProductUtils.generateIdProduct(loanNumber + productType));
        loan.setLabel(object.getString("string4"));
        loan.setProductType(productType);
        loan.setNumber(loanNumber);
        loan.setConsolidatedAmount(Double.parseDouble(object.getString("double1")));
        loan.setCurrency("USD");
        loan.setClient(new Client(customerId, ""));

        return loan;
    }


}
