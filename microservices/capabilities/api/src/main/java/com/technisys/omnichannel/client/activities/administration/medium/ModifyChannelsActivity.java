package com.technisys.omnichannel.client.activities.administration.medium;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.limits.CapForUser;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.ReturnCodes.CHANNEL_INVALID;

@DocumentedActivity("Modify user's channels in medium scheme")
public class ModifyChannelsActivity extends Activity {

    public interface InParams {

        @DocumentedParam(type = String.class, description = "ID of the user to apply changes")
        String ID_USER = "idUser";
        @DocumentedParam(type = String[].class, description = "Channels to be enabled")
        String ENABLED_CHANNELS = "enabledChannels";
        @DocumentedParam(type = Double[].class, description = "Amounts to apply by matching index of enabledChannels param")
        String MAX_AMOUNTS = "maxAmounts";
        @DocumentedParam(type = String[].class, description = "Frequencies to apply by matching index of enabledChannels param")
        String CAP_FREQUENCIES = "capFrequencies";
    }

    public interface OutParams {
    }

    public static final String ID = "administration.medium.modify.channels.send";

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String idUser = request.getParam(InParams.ID_USER, String.class);
            int idEnvironment = request.getIdEnvironment();
            String[] enabledChannels = request.getParam(InParams.ENABLED_CHANNELS, String[].class);
            Double[] maxAmounts = request.getParam(InParams.MAX_AMOUNTS, Double[].class);
            String[] capFrequencies = request.getParam(InParams.CAP_FREQUENCIES, String[].class);
            List<CapForUser> caps = new ArrayList<>();

            // setting enabled channels
            EnvironmentUser envUser = Administration.getInstance().readEnvironmentUserInfo(idUser, request.getIdEnvironment());
            envUser.setEnabledChannels(org.apache.commons.lang3.StringUtils.join(enabledChannels, ","));
            Administration.getInstance().updateEnvironmentUserInfo(envUser);

            for (int i = 0; i < enabledChannels.length; i++) {
                caps.add(new CapForUser(idEnvironment, enabledChannels[i], idUser, capFrequencies[i], maxAmounts[i] != null ? maxAmounts[i] : -1d));
            }

            LimitsHandlerFactory.getHandler().updateUserCaps(caps);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String idUser = request.getParam(InParams.ID_USER, String.class);
            String[] enabledChannels = request.getParam(InParams.ENABLED_CHANNELS, String[].class);
            Double[] maxAmounts = request.getParam(InParams.MAX_AMOUNTS, Double[].class);
            String[] capFrequencies = request.getParam(InParams.CAP_FREQUENCIES, String[].class);

            boolean isValid = true;
            // Checks if environment schema is not simple type, if so an invalid schema return code will be sent.
            // Activity is USED by BOTH medium and advanced schemas
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            if (StringUtils.isBlank(idUser)) {
                isValid = false;
                result.put(InParams.ID_USER, "administration.medium.modify.channels.idUser.required");
            }

            if (ArrayUtils.isEmpty(enabledChannels)) {
                isValid = false;
                result.put(InParams.ENABLED_CHANNELS, "administration.medium.modify.channels.enabledChannels.required");
            }

            if (ArrayUtils.isEmpty(maxAmounts)) {
                isValid = false;
                result.put(InParams.MAX_AMOUNTS, "administration.medium.modify.channels.maxAmounts.required");
            }

            if (!(ArrayUtils.isSameLength(enabledChannels, maxAmounts) && ArrayUtils.isSameLength(enabledChannels, capFrequencies))) {
                isValid = false;
                result.put("NO_FIELD", "administration.users.invalidChannelList");
            }

            if (ArrayUtils.isEmpty(capFrequencies)) {
                isValid = false;
                result.put(InParams.CAP_FREQUENCIES, "administration.medium.modify.channels.capFrequencies.required");
            }

            if (isValid) {
                List<String> systemEnabledChannels = CoreUtils.getEnabledChannels();
                List<String> systemEnabledFrequencies = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "frontend.channels.enabledFrequencies");

                systemEnabledChannels.add(CoreUtils.CHANNEL_ALL);

                if(!request.getChannel().isEmpty()) {
                    // validate channels
                    if (enabledChannels != null) {
                        for (String channel : enabledChannels) {
                            if (!systemEnabledChannels.contains(channel)) {
                                result.put("NO_FIELD", "administration.users.invalidChannel");
                            }
                        }
                    }
                } else {
                    result.put(CHANNEL_INVALID.getReturnCodeAsString(), "administration.users.invalidChannel");
                }

                // checking irremovable channels for administrators
                if (Authorization.hasPermission(idUser, request.getIdEnvironment(), null, Constants.ADMINISTRATION_VIEW_PERMISSION)) {
                    List<String> nonRemovableChannels = ReadChannelsActivity.NON_REMOVABLE_CHANNELS_FOR_ADMINISTRATORS;
                    if (!nonRemovableChannels.isEmpty()) {
                        List<String> selectedChannels = enabledChannels != null ? Arrays.asList(enabledChannels) : new ArrayList<String>();
                        if (!CollectionUtils.isSubCollection(nonRemovableChannels, selectedChannels)) {
                            HashMap<String, String> fillers = new HashMap<>();
                            StringBuilder builder = new StringBuilder();
                            for (String channel : nonRemovableChannels) {
                                if (builder.length() != 0) {
                                    builder.append(", ");
                                }
                                builder.append(I18nFactory.getHandler().getMessage("channels." + channel, request.getLang()));
                            }
                            fillers.put("CHANNELS", builder.toString());

                            result.put("@NO_FIELD", I18nFactory.getHandler().getMessage("administration.users.cantRemoveChannel" + (nonRemovableChannels.size() > 1 ? ".plural" : ""), request.getLang(), fillers));
                        }
                    }
                }

                if (enabledChannels != null) {
                    for (String channel : enabledChannels) {
                        if (!systemEnabledChannels.contains(channel)) {
                            result.put("NO_FIELD", "administration.users.invalidChannel");
                        }
                    }
                }

                // validate frequencies
                if (capFrequencies != null) {
                    for (int i = 0; i < capFrequencies.length; i++) {
                        String capFrequency = capFrequencies[i];
                        if (!systemEnabledFrequencies.contains(capFrequency)) {
                            result.put("capFrequency" + enabledChannels[i], "administration.users.invalidCapFrequency");
                        }
                    }
                }
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
