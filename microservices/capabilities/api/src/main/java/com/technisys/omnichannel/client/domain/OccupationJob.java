/*
 *  Copyright 2014 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import java.util.Objects;

/**
 * Represent a Occupation Job
 */
public class OccupationJob {

    private int idOccupationJob;
    private String name;

    public OccupationJob(int idOccupationJob, String name) {
        this.idOccupationJob = idOccupationJob;
        this.name = name;
    }

    public int getIdOccupationJob() {
        return idOccupationJob;
    }

    public void setIdOccupationJob(int idOccupationJob) {
        this.idOccupationJob = idOccupationJob;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OccupationJob that = (OccupationJob) o;
        return idOccupationJob == that.idOccupationJob &&
                Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idOccupationJob, name);
    }

    @Override
    public String toString() {
        return "OccupationJob{" +
                "idOccupationJob=" + idOccupationJob +
                ", name='" + name + '\'' +
                '}';
    }
}
