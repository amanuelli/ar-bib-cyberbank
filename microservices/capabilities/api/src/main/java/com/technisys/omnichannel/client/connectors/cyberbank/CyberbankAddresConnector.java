/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.Province;
import com.technisys.omnichannel.client.utils.JsonTemplateUtils;
import com.technisys.omnichannel.core.utils.CacheUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CyberbankAddresConnector extends CyberbankCoreConnector {

    private static final Logger LOG = LoggerFactory.getLogger(CyberbankAddresConnector.class);

    /**
     * Return provinces of country
     *
     * @param codeCountry
     * @return
     * @throws BackendConnectorException
     */
    public static CyberbankCoreConnectorResponse<List<Province>> listProvince(String codeCountry) throws BackendConnectorException {
        try {
            List<Province> provinceList = (List<Province>) CacheUtils.get("cyberbank.core.provinces");
            CyberbankCoreConnectorResponse<List<Province>> response = new CyberbankCoreConnectorResponse<>();

            if (provinceList != null) {
                response.setData(provinceList);
            } else {
                String serviceName = "Srv - massiveSelectProvinceBy_Country";
                CyberbankCoreRequest request = new CyberbankCoreRequest("massiveSelectProvinceBy_Country", true);
                request.addRequestParameter("codeCountry", (codeCountry != null) ? codeCountry : DEFAULT_COUNTRY_CODE);
                LOG.info("{}: Request {}", serviceName, URL);

                String jsonRequest = JsonTemplateUtils.applyTemplateToJson(request.getJSON(), REQ_TEMPLATE_PATH + request.getTransactionId());
                JSONObject serviceResponse = call(serviceName, jsonRequest);

                if (callHasError(serviceResponse)) {
                    processErrors(serviceName, serviceResponse, response);
                } else {
                    response.setData(processProvince(serviceResponse));
                }
            }
            return response;

        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     * parse provinces response from core
     *
     * @param serviceResponse
     * @return
     */
    private static List<Province> processProvince(JSONObject serviceResponse) throws IOException {
        ArrayList<Province> provinceList = new ArrayList<>();
        JSONArray collection = serviceResponse.getJSONObject("out.province_code_list").getJSONArray("collection");
        for (Object object : collection) {
            JSONObject provinceJson = (JSONObject) object;
            Province province = new Province();
            province.setCode(String.valueOf(provinceJson.getInt("id")));
            province.setDescription(provinceJson.getString("shortDesc"));
            provinceList.add(province);
        }
        if(!provinceList.isEmpty()){
            CacheUtils.put("cyberbank.core.provinces", provinceList);
        }
        return provinceList;
    }

}
