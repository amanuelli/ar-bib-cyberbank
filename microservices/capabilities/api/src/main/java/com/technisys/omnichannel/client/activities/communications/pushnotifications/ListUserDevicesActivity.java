/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications.pushnotifications;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.domain.ClientUserDevice;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.domain.UserDevice;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * list linked user devices to receive push notifications
 */
@DocumentedActivity("List linked user devices")
public class ListUserDevicesActivity extends Activity {

    public static final String ID = "communications.pushnotifications.listUserDevices";

    public interface OutParams {

        @DocumentedParam(type = List.class, description = "User's device list")
        String LIST_USER_DEVICES = "listUserDevices";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            List<UserDevice> listUserDevices = AccessManagementHandlerFactory.getHandler().listUserDevices(request.getIdUser(), true);
            List<ClientUserDevice> listClientUserDevices = new ArrayList<>();
            for (UserDevice userDevice : listUserDevices) {
                listClientUserDevices.add(new ClientUserDevice(userDevice));
            }
            response.putItem(OutParams.LIST_USER_DEVICES, listClientUserDevices);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
