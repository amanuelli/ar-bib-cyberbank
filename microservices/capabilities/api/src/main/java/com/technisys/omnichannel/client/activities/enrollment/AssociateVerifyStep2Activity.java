/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.client.ReturnCodes;
import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.attemptscounter.AttemptsCounterHandler;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.credentials.CredentialPluginFactory;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.utils.ReCaptcha;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 *
 */
@ExchangeActivity
public class AssociateVerifyStep2Activity extends Activity {

    public static final String ID = "enrollment.associate.verifyStep2";

    private final String NEXT_ACTIVITY_IN_FLOW = AssociateFinishActivity.ID;

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
    }

    public interface OutParams {
        String EXCHANGE_TOKEN = "_exchangeToken";
        String CAPTCHA_REQUIRED = "captchaRequired";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            InvitationCode invitation = getInvitationCode(request);
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber());
            if (user == null) {
                throw new ActivityException(ReturnCodes.INVALID_INVITATION_CODE);
            }

            try {
                // Genero una request artificial para enviar al validate de los plugines
                Request auxRequest = new Request();
                auxRequest.setIdActivity(ID);
                auxRequest.setIdUser(user.getIdUser());
                auxRequest.setCredentials(request.getCredentials());
                auxRequest.setLang(request.getLang());

                CredentialPlugin cp = CredentialPluginFactory.getCredentialPlugin(Credential.CAPTCHA_CREDENTIAL);
                cp.validate(auxRequest, request.getCredential(Credential.CAPTCHA_CREDENTIAL));

                cp = CredentialPluginFactory.getCredentialPlugin(Credential.PWD_CREDENTIAL);
                cp.validate(auxRequest, request.getCredential(Credential.PWD_CREDENTIAL));
            } catch (DispatchingException e) {
                throw new ActivityException(e.getReturnCode());
            }

            // Actualizo el exchange token
            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);

            List<String> activities = new ArrayList<>(Arrays.asList(StringUtils.split(token.getActivities(), ',')));
            if (!activities.contains(NEXT_ACTIVITY_IN_FLOW)) {
                activities.add(NEXT_ACTIVITY_IN_FLOW);
            }

            ExchangeTokenHandler.updateActivities(exchangeToken, activities.toArray(new String[0]));

            response.putItem(OutParams.CAPTCHA_REQUIRED, ReCaptcha.mustBeDisplayedForUser(AttemptsCounterHandler.getFeature(ID), user.getIdUser()));
            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return validateFields();
    }

    protected static Map<String, String> validateFields() {
        return new HashMap<>();
    }
}
