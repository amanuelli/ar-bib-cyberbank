/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.files.FilesHandler;
import org.apache.commons.codec.binary.Base64;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static com.technisys.omnichannel.client.activities.files.DownloadFileActivity.InParams.ID_FILE;

@DocumentedActivity("Files download")
public class DownloadFileActivity extends Activity {

    public static final String ID = "files.download";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "idFile")
        String ID_FILE = "idFile";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "File name")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = String.class, description = "Encoded base64 content")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int idFile = request.getParam(ID_FILE, Integer.class);

            File file = FilesHandler.getInstance().readWithContents(request.getIdUser(), false, idFile);

            if (file == null) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
            String content = Base64.encodeBase64String(file.getContents());

            response.putItem(OutParams.FILE_NAME, file.getFileName());
            response.putItem(OutParams.CONTENT, content);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        Integer idFile = request.getParam(ID_FILE, Integer.class);
        if (idFile == null) {
            result.put(ID_FILE, "files.download.idFile.empty");
        }
        return result;
    }

}
