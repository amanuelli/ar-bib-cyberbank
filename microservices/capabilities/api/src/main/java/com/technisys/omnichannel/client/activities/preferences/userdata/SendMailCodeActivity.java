/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.userdata;


import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.domain.ValidationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.validationcodes.ValidationCodesHandler;
import org.apache.commons.lang.StringUtils;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Send a validation code to the new user mail
 */
@DocumentedActivity("Send a validation code to the new user mail")
public class SendMailCodeActivity extends Activity {

    public static final String ID = "preferences.userData.mail.sendCode";
    
    public interface InParams {
        @DocumentedParam(type = String.class, description = "New user email address")
        String MAIL = "mail";
    }
    
    public interface OutParams {
        @DocumentedParam(type = String.class, description = "New user email address")
        String MAIL = "mail";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {

            String idUser = request.getIdUser();
            String mail = request.getParam(InParams.MAIL, String.class);
            
            User user = AccessManagementHandlerFactory.getHandler().getUser(idUser);
            
            ValidationCode codigoValidacion = new ValidationCode();
            codigoValidacion.setCreationDate(new Date());
            codigoValidacion.setIdUser(user.getIdUser());
            codigoValidacion.setType(ValidationCode.TYPE_CHANGEEMAIL);
            codigoValidacion.setStatus(ValidationCode.STATUS_AVAILABLE);
            codigoValidacion.setExtraInfo(mail);

            String idValidationCode = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
            ValidationCodesHandler.createValidationCode(codigoValidacion, idValidationCode);
            
            ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, Constants.BASE_URL_KEY);
            String expirationTime = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "changeEmail.mail.validity");
            HashMap<String, String> fillers = new HashMap<>();
            fillers.put("validationCode", idValidationCode);
            fillers.put("expirationTime", com.technisys.omnichannel.client.utils.StringUtils.prettyPrintInterval(expirationTime, request.getLang()));

            String subject = I18nFactory.getHandler().getMessage("changeEmail.mail.subject", request.getLang());
            String body = I18nFactory.getHandler().getMessage("changeEmail.mail.body", request.getLang(), fillers);
            
            NotificationsHandlerFactory.getHandler().sendEmails(subject, body, Arrays.asList(mail), null, request.getLang(), true);
            
            response.putItem(OutParams.MAIL, mail);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (MessagingException ex) {
            throw new ActivityException(ReturnCodes.SENDING_MAIL_ERROR, ex);
        }
        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException{
       try{
            Map<String, String> result = new HashMap<>();
         
            String mail = request.getParam(InParams.MAIL, String.class);
            if (StringUtils.isBlank(mail)){
                result.put(InParams.MAIL, "userInfo.preferences.userData.mail.empty");
            } else if (!ValidationUtils.validateEmailPattern(mail)) {
                result.put(InParams.MAIL, "userInfo.preferences.userData.mail.format.invalid");
            } else {
                User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
                if (StringUtils.equals(user.getEmail(), mail)){
                    result.put(InParams.MAIL, "userInfo.preferences.userData.mail.sameAsBefore");
                }
            }

            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }
    
}
