/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.client.connectors.safeway;

import com.google.api.client.util.Base64;
import com.technisys.omnichannel.client.domain.OnboardingDocument;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.connectors.SafewayRESTConnectorBase;
import com.technisys.omnichannel.core.onboardings.plugins.DocumentReaderResponse;
import com.technisys.safeway.ApiException;
import com.technisys.safeway.client.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.ParseException;

import static com.technisys.omnichannel.client.Constants.*;

public class SafewayRESTConnector extends SafewayRESTConnectorBase {
    private static final Logger log = LoggerFactory.getLogger(SafewayRESTConnector.class);

    public SafewayRESTConnector() throws ApiException {
        super();
    }

    //<editor-fold desc="Documents">
    public ValidateDataDocumentResult validateOnboardingDocument(long idOnboarding, boolean isPassport) {

        ValidateDataDocumentResult validationResult = new ValidateDataDocumentResult();
        try {
            OnboardingDocument documentFront = OnboardingHandlerFactory.getInstance().readDocument(idOnboarding, DOCUMENT_FRONT);
            OnboardingDocument documentBack = OnboardingHandlerFactory.getInstance().readDocument(idOnboarding, DOCUMENT_BACK);
            OnboardingDocument selfie = OnboardingHandlerFactory.getInstance().readDocument(idOnboarding, DOCUMENT_SELFIE);

            DocumentDTO doc = new DocumentDTO();
            if(!isPassport){
                doc.setBackImage(Base64.encodeBase64String(documentBack.getContent()));
            }else{
                doc.setBackImage("");
            }
            doc.setSelfieImage(Base64.encodeBase64String(selfie.getContent()));
            doc.setFrontImage(Base64.encodeBase64String(documentFront.getContent()));
            doc.setSystem(system);
            doc.setClientSystemPassword(systemPassword);

            validationResult = documentApi.validateUsingPOST(doc);

        } catch (IOException | ApiException e) {
            log.error(e.getMessage(), e);
        }

        return validationResult;
    }

    public DocumentReaderResponse extractDocumentData(long idOnboarding) {
        return extractDocumentData(idOnboarding, false, false);
    }

    public DocumentReaderResponse extractDocumentData(long idOnboarding, boolean invertDocumentOrder, boolean isPassport) {
        DocumentReaderResponse result = null;

        try {
            OnboardingDocument documentFront = null;
            OnboardingDocument documentBack = null;

            if (invertDocumentOrder) {
                //in the case that the MRZ is in the front side of the document
                documentFront = OnboardingHandlerFactory.getInstance().readDocument(idOnboarding, DOCUMENT_BACK);
                documentBack = OnboardingHandlerFactory.getInstance().readDocument(idOnboarding, DOCUMENT_FRONT);
            } else {
                documentFront = OnboardingHandlerFactory.getInstance().readDocument(idOnboarding, DOCUMENT_FRONT);
                documentBack = OnboardingHandlerFactory.getInstance().readDocument(idOnboarding, DOCUMENT_BACK);
            }

            DocumentDTO doc = new DocumentDTO();
            if(!isPassport) {
                doc.setBackImage(Base64.encodeBase64String(documentBack.getContent()));
            }else{
                doc.setBackImage("");
            }
            doc.setFrontImage(Base64.encodeBase64String(documentFront.getContent()));
            doc.setSelfieImage("");
            doc.setSystem(system);
            doc.setClientSystemPassword(systemPassword);

            ExtractDataDocumentResult data = documentApi.extractUsingPOST(doc);

            GetConfigurationListResult configurations = this.getConfigurationList();

            result = new DocumentReaderResponse(data, configurations.getDateFormat());

        } catch (IOException | ApiException | ParseException e) {
            log.error(e.getMessage(), e);
        }

        return result;
    }



    public String addUserPasswordWithEndToEnd(String commonName, String password, String endToEndValue, String idTransaction) {
        AddSecretKeyResult result = null;

        try {
            SecretKeyDto key = new SecretKeyDto();
            key.setCommonName(commonName);
            key.setDomainType(domainType);
            key.setSecretKeyType(keyType);
            key.setClientSystemId(system);
            key.setClientSystemPassword(systemPassword);
            key.setEncryptedValue(password);
            key.setEndToEndValue(endToEndValue);
            key.setTransactionId(idTransaction);

            result = secretKeyApi.addSecretKeyEndToEndUsingPOST(key);

        } catch (ApiException e) {
            log.error(e.getMessage(), e);
        }
        if(result != null) {
            return result.getReturnCode().toString();
        }
        return null;
    }


    public String validateSecretKeyWithEndToEnd(String commonName, String password, String endToEndValue, String idTransaction) {
        ValidationSecretKeyResult result = null;

        try {
            SecretKeyDto key = new SecretKeyDto();
            key.setCommonName(commonName);
            key.setDomainType(domainType);
            key.setSecretKeyType(keyType);
            key.setClientSystemId(system);
            key.setClientSystemPassword(systemPassword);
            key.value(password);
            key.setEndToEndValue(endToEndValue);
            key.setTransactionId(idTransaction);

            result = secretKeyApi.validateSecretKeyEndToEndUsingPOST(key);

        } catch (ApiException e) {
            log.error(e.getMessage(), e);
        }
        if(result != null) {
            return result.getReturnCode().toString();
        }
        return null;
    }


    //</editor-fold>

    public GetConfigurationListResult getConfigurationList() {
        GetConfigurationListResult dataResult = new GetConfigurationListResult();
        try {
            MiscDto dto = new MiscDto();
            dto.setClientSystemPassword(systemPassword);
            dto.setClientSystemId(system);
            dataResult = miscApi.getConfigurationListUsingPOST(dto);
        } catch (ApiException e) {
            log.error(e.getMessage(), e);
        }

        dataResult.setDateFormat(dataResult.getDateFormat());
        return dataResult;
    }

}
