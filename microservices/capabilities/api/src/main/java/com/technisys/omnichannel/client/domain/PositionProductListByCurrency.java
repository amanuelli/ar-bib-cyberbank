/*
 *  Copyright 2014 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import java.util.ArrayList;
import java.util.List;

/*
 * @author ?
 */
public class PositionProductListByCurrency {
    
    private String currency;
    private double total;
    private List<PositionProduct> products;
    
    public PositionProductListByCurrency(){
        products = new ArrayList<>();
    }
    
    public String getCurrency(){
        return currency;
    }
    
    public void setCurrency(String currency){
        this.currency = currency;
    }
    
    public double getTotal(){
        return total;
    }
    
    public void addToTotal(Double total){
        this.total += total; 
    }
    
    public void setTotal(double total){
        this.total = total;
    }
    
    public List<PositionProduct> getProductList(){
        return products;
    }
    
    public void setProductList(List<PositionProduct> products){
        this.products = products;
    }
    
    public void addProduct(PositionProduct product){
        products.add(product);
    }
}