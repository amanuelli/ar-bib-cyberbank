/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.loans;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;

/**
 *
 */
@DocumentedActivity("Read Loan")
public class ReadLoanActivity extends Activity {

    public static final String ID = "loans.read";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Loan Id")
        String ID_LOAN = "idLoan";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Loan detail")
        String LOAN = "loan";

        @DocumentedParam(type = String.class, description = "Number of feeds")
        String NUMBER_OF_FEES = "numberOfFees";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            String idLoan = (String) request.getParam(InParams.ID_LOAN, String.class);

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            Product product = Administration.getInstance().readProduct(idLoan, request.getIdEnvironment());
            Loan loan = CoreLoanConnectorOrchestrator.read(request.getIdTransaction(), product);
            loan.setPaperless(product.getPaperless());
            loan.setProductAlias(loan.getProductAlias());
            loan.setShortLabel(pLabeler.calculateShortLabel(loan));
            loan.setLabel(pLabeler.calculateLabel(loan));
            loan.setPaidPercentage(loan.getNumberOfPaidFees() * 100.0 / loan.getNumberOfFees());
            loan.setProductAlias(product.getProductAlias());

            response.putItem(OutParams.LOAN, loan);
            response.putItem(OutParams.NUMBER_OF_FEES, loan.getNumberOfFees());
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
