/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.google.common.io.ByteStreams;
import com.google.gson.internal.LinkedTreeMap;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.URL;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.Map;

/**
 *
 * @author sbarbosa
 */
public class SecuritySealUtils {

    private SecuritySealUtils(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    private static final Logger log = LoggerFactory.getLogger(SecuritySealUtils.class);

    /**
     * Lista los sellos de seguridad que existen en el sistema
     *
     * @return Un mapa que tiene como clave el id del sello y como valor el
     * Base64 de la imagen
     * @throws IOException
     */
    public static Map<Integer, String> getSealList() throws IOException {
        Map<Integer, String> result = new LinkedTreeMap<>();

        // La carpeta de sellos por defecto es
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        assert classLoader != null;

        Enumeration<URL> resources = classLoader.getResources("/");
        URL resource = resources.nextElement();
        String fullPath = StringUtils.replace(resource.getFile(), "%20", " ");
        File root = new File(fullPath);

        // Me muevo a la carpeta securityimages
        File sealsDir = new File(root.getAbsolutePath() + File.separator + "securityimages");

        File[] images = sealsDir.listFiles((dir, name) -> {
            try {
                Integer.parseInt(FilenameUtils.getBaseName(name));
                return true;
            } catch (NumberFormatException ex) {
                log.warn("Se excluye el sello de seguridad " + name + ", el nombre no es numerico.", ex);
                return false;
            }
        });

        if (images != null) {
            // Ordeno los sellos de seguridad en órden numérico primero
            Arrays.sort(images, (f1, f2) -> {
                int id1 = Integer.parseInt(FilenameUtils.getBaseName(f1.getName()));
                int id2 = Integer.parseInt(FilenameUtils.getBaseName(f2.getName()));

                return Integer.compare(id1, id2);
            });

            // Security Seal
            String imageData = "data:image/jpeg;base64,", data;
            for (File image : images) {
                int id = Integer.parseInt(FilenameUtils.getBaseName(image.getName()));

                data = imageData;
                try {
                    try (InputStream fis = new FileInputStream(image)) {
                        data += new String(Base64.encodeBase64(ByteStreams.toByteArray(fis)));
                    }
                } catch (IOException ex) {
                    // Si no se pudo cargar la imagen se devuelve un placeholder.
                    log.warn("No se pudo cargar la imagen " + image.getName() + ", se devuelve un placeholder.", ex);
                    data += "/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABfAAD/4QNtaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MzFDNzRBM0UyNzQ1RTIxMTg1MTZENUFENjhDQ0EyRkQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6REI2NUQ2MTI0NTM2MTFFMkJCRThCMjM5ODRBRjM2NTQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6REI2NUQ2MTE0NTM2MTFFMkJCRThCMjM5ODRBRjM2NTQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTBBMUE5QjEzNjQ1RTIxMTg1MTZENUFENjhDQ0EyRkQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzFDNzRBM0UyNzQ1RTIxMTg1MTZENUFENjhDQ0EyRkQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQECAQEBAQECAgICAgICAgICAgIDAgICAwMDAwMDBQUFBQUFBQUFBQUFBQUFAQEBAQIBAgMCAgMEBAMEBAUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQX/wAARCAB4AHgDAREAAhEBAxEB/8QAdgABAAEFAQEBAAAAAAAAAAAAAAIEBQcICQYDCgEBAAAAAAAAAAAAAAAAAAAAABAAAQQBAgMEBQsFAAAAAAAAAAECAwQFEQYhMRKxcjMHQSKSFLRhkTITU5NU1DZ2GFFDFWYXEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD9eNy5av2ZrlyeSxZsSOlmmler3ve9dV4qBTAAAAAAAAAAAAAAAVNO5aoWYblOeSvZryNlhmierHsexdU4oBTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACTE6nNavJXInzqBz+2p5ef9JzFiOR+LsZqfHzZy9ktwyyuda0khZJ67Ip39SvsN0TTTTX5EAyJ/F619tsT763+QAfxetfbbE++t/kAH8XrX22xPvrf5ADHe6/Lz/m2YrxxvxdfNQY+HOUclt6WVrquskzI/XfFA/qR9d2qaaaafKgHQF6dLnNTkjlT5lAiAAAAAAAAAnH4kffb2gaj+QXHdc6f6Zd+LxgHh945vcuZ3Pl7V/OZ7HSU8peqY+hjsrPjIMRBSlfGxrWQvY1ZNG6vkVNVXX0aIgbXeWGXy+c2Vib2be6fII67UfdcxI3ZCOlM+Jk6oiI3qe1qI/TgrkVU05Ae/A1K8/P1XB+zKXxeTA24k8STvu7QIAAAAAAAAAJx+JH329oGo/kH+q5/2Zd+LxgGxGX2Fs7PX/wDJ5bCMsX3KxZ54bc1NLnQmifWtikY1y6InrJo5fSoF7t28Vt7FS2rL6uJw2Jp6uXRIatOnXTREaif0TgiImqrwTVVAxJs/zqxm59wy4S7ipcDFdmVm2r1u42VMvprpHMzoYlWaRE6o2dTkd9Hq6tEcGN/Pz9Vwfsyl8XkwNuJPEk77u0CAAAAAAAAACcfiR99vaBpF5YbqxezcjdzWWdIsTNoWq1WtXb12shfmtY5Y4Im8EVz0Yq8VRERFcqoiKoF/w3nZuupuGbKbhZFb21fkayXBUa7Fn29XTg2SvKjWyWlanGZJF9bmxG8GgWbzE39Y35kPdqqzVtpY6fqo1ZGuhlzNqJeFudi6ORiL4MTk1T6TvWVEaGPpoWTMWN6LpwVrmr0vY9vFHNVOKK1eKKgFy3BnsxuGKvYzczbV3G4BmF9+/vX4KktqVksqcvrFbYRr1TmqdXp0QOg0niSd93aBAAAAAAAAABOPg9iryR7e0DnfLQt41/uGQrSVbtLSvYgnYrJYZok6XIqLx5oBAAAA+kVG3k3pj6FeW1cuItetXhYskkssqdLURE1XmoHQ+Ti96pyV7u0CAAAAAAAAAABUtuWmNRrJ5WtTkiO4IBL3+5+Jl9oB7/c/Ey+0A9/ufiZfaAi65ae1Wvnlc1eaK7goFMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqblSzQsz07kMlezXkfFNDK1WPY9i6KiooFMAAAAAAAAAAAAAABU06lm/Zgp04ZLFmxIyKGGJqve9710RERAP/9k=";
                }

                result.put(id, data);
            }
        }

        return result;
    }

    /**
     * Obtiene un sello de seguridad
     *
     * @param seal Id del sello de seguridad
     * @return La imagen del sello de seguridad en Base64
     * @throws IOException
     */
    public static String getSeal(Integer seal) throws IOException {
        // Security Seal
        String imageData = "data:image/jpeg;base64,";
        try {
            try (InputStream image = SecuritySealUtils.class.getResourceAsStream("/securityimages/" + seal + ".jpg")) {
                imageData += new String(Base64.encodeBase64(ByteStreams.toByteArray(image)));
            }
        } catch (Exception e) {
            // Si no se pudo cargar la imagen se devuelve un placeholder.
            log.warn("No se pudo cargar la imagen, se devuelve un placeholder.", e);
            imageData += "/9j/4QAYRXhpZgAASUkqAAgAAAAAAAAAAAAAAP/sABFEdWNreQABAAQAAABfAAD/4QNtaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wLwA8P3hwYWNrZXQgYmVnaW49Iu+7vyIgaWQ9Ilc1TTBNcENlaGlIenJlU3pOVGN6a2M5ZCI/PiA8eDp4bXBtZXRhIHhtbG5zOng9ImFkb2JlOm5zOm1ldGEvIiB4OnhtcHRrPSJBZG9iZSBYTVAgQ29yZSA1LjAtYzA2MCA2MS4xMzQ3NzcsIDIwMTAvMDIvMTItMTc6MzI6MDAgICAgICAgICI+IDxyZGY6UkRGIHhtbG5zOnJkZj0iaHR0cDovL3d3dy53My5vcmcvMTk5OS8wMi8yMi1yZGYtc3ludGF4LW5zIyI+IDxyZGY6RGVzY3JpcHRpb24gcmRmOmFib3V0PSIiIHhtbG5zOnhtcE1NPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvbW0vIiB4bWxuczpzdFJlZj0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL3NUeXBlL1Jlc291cmNlUmVmIyIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6MzFDNzRBM0UyNzQ1RTIxMTg1MTZENUFENjhDQ0EyRkQiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6REI2NUQ2MTI0NTM2MTFFMkJCRThCMjM5ODRBRjM2NTQiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6REI2NUQ2MTE0NTM2MTFFMkJCRThCMjM5ODRBRjM2NTQiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6QTBBMUE5QjEzNjQ1RTIxMTg1MTZENUFENjhDQ0EyRkQiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6MzFDNzRBM0UyNzQ1RTIxMTg1MTZENUFENjhDQ0EyRkQiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7/7gAOQWRvYmUAZMAAAAAB/9sAhAABAQEBAQEBAQEBAQEBAQEBAQEBAQECAQEBAQECAgICAgICAgICAgIDAgICAwMDAwMDBQUFBQUFBQUFBQUFBQUFAQEBAQIBAgMCAgMEBAMEBAUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQUFBQX/wAARCAB4AHgDAREAAhEBAxEB/8QAdgABAAEFAQEBAAAAAAAAAAAAAAIEBQcICQYDCgEBAAAAAAAAAAAAAAAAAAAAABAAAQQBAgMEBQsFAAAAAAAAAAECAwQFEQYhMRKxcjMHQSKSFLRhkTITU5NU1DZ2GFFDFWYXEQEAAAAAAAAAAAAAAAAAAAAA/9oADAMBAAIRAxEAPwD9eNy5av2ZrlyeSxZsSOlmmler3ve9dV4qBTAAAAAAAAAAAAAAAVNO5aoWYblOeSvZryNlhmierHsexdU4oBTAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAACTE6nNavJXInzqBz+2p5ef9JzFiOR+LsZqfHzZy9ktwyyuda0khZJ67Ip39SvsN0TTTTX5EAyJ/F619tsT763+QAfxetfbbE++t/kAH8XrX22xPvrf5ADHe6/Lz/m2YrxxvxdfNQY+HOUclt6WVrquskzI/XfFA/qR9d2qaaaafKgHQF6dLnNTkjlT5lAiAAAAAAAAAnH4kffb2gaj+QXHdc6f6Zd+LxgHh945vcuZ3Pl7V/OZ7HSU8peqY+hjsrPjIMRBSlfGxrWQvY1ZNG6vkVNVXX0aIgbXeWGXy+c2Vib2be6fII67UfdcxI3ZCOlM+Jk6oiI3qe1qI/TgrkVU05Ae/A1K8/P1XB+zKXxeTA24k8STvu7QIAAAAAAAAAJx+JH329oGo/kH+q5/2Zd+LxgGxGX2Fs7PX/wDJ5bCMsX3KxZ54bc1NLnQmifWtikY1y6InrJo5fSoF7t28Vt7FS2rL6uJw2Jp6uXRIatOnXTREaif0TgiImqrwTVVAxJs/zqxm59wy4S7ipcDFdmVm2r1u42VMvprpHMzoYlWaRE6o2dTkd9Hq6tEcGN/Pz9Vwfsyl8XkwNuJPEk77u0CAAAAAAAAACcfiR99vaBpF5YbqxezcjdzWWdIsTNoWq1WtXb12shfmtY5Y4Im8EVz0Yq8VRERFcqoiKoF/w3nZuupuGbKbhZFb21fkayXBUa7Fn29XTg2SvKjWyWlanGZJF9bmxG8GgWbzE39Y35kPdqqzVtpY6fqo1ZGuhlzNqJeFudi6ORiL4MTk1T6TvWVEaGPpoWTMWN6LpwVrmr0vY9vFHNVOKK1eKKgFy3BnsxuGKvYzczbV3G4BmF9+/vX4KktqVksqcvrFbYRr1TmqdXp0QOg0niSd93aBAAAAAAAAABOPg9iryR7e0DnfLQt41/uGQrSVbtLSvYgnYrJYZok6XIqLx5oBAAAA+kVG3k3pj6FeW1cuItetXhYskkssqdLURE1XmoHQ+Ti96pyV7u0CAAAAAAAAAABUtuWmNRrJ5WtTkiO4IBL3+5+Jl9oB7/c/Ey+0A9/ufiZfaAi65ae1Wvnlc1eaK7goFMAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAqblSzQsz07kMlezXkfFNDK1WPY9i6KiooFMAAAAAAAAAAAAAABU06lm/Zgp04ZLFmxIyKGGJqve9710RERAP/9k=";
        }
        return imageData;
    }

    /**
     * Chequea si el sello de seguridad existe en el sistema
     *
     * @param seal Id del sello de seguridad
     * @return True si el sello existe en el sistema
     */
    public static boolean existsSeal(Integer seal) {
        URL u = SecuritySealUtils.class.getClassLoader().getResource("/securityimages/" + seal + ".jpg");
        return u != null;
    }

}
