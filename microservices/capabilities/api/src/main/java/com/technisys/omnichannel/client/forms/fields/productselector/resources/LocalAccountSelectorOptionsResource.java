package com.technisys.omnichannel.client.forms.fields.productselector.resources;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.technisys.omnichannel.client.forms.fields.productselector.ProductselectorFieldHandler.Option;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.forms.fields.FrontendOption;
import com.technisys.omnichannel.core.forms.fields.OptionsResource;

public class LocalAccountSelectorOptionsResource implements OptionsResource {
    
    @Override
    public List<? extends FrontendOption> getOptions(FormField field, Request request) throws IOException {
        
        return new ArrayList<Option>();
    }

}
