/*
 * Copyright 2019 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.forms.fields.coordinates;

import com.technisys.omnichannel.client.domain.fields.CoordinatesField;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.fields.FieldHandler;
import com.technisys.omnichannel.core.utils.DBUtils;
import org.apache.ibatis.session.SqlSession;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Marcelo Bruno
 */
public class CoordinatesFieldHandler extends FieldHandler {

    @Override
    public String getShortDescription(Map<String, String> map, String lang) {
        return getBasicShortDescription(map, lang);
    }

    @Override
    public String getFullDescription(Map<String, String> map, String lang) {
        return "";
    }

    @Override
    public String getPDFDescription(FormField field, String lang) {
        return "";
    }

    @Override
    public Map<String, String> validate(Map<String, String> map) throws IOException {
        return new HashMap<>();
    }

    @Override
    public Map<String, String> validate(FormField field) throws IOException {
        return new HashMap<>();
    }

    @Override
    public void createFormField(SqlSession session, Map<String, String> map) throws IOException {
        //It is no necesary because fild does not contain custom tables
    }

    @Override
    public void createFormField(SqlSession session, FormField field) throws IOException {
        //It is no necesary because fild does not contain custom tables
    }

    @Override
    public void updateFormField(SqlSession session, Map<String, String> map) throws IOException {
        //It is no necesary because fild does not contain custom tables
    }

    @Override
    public FormField readFormField(String idForm, Integer formVersion, String idField, boolean withMessages) throws IOException {
        CoordinatesField coordinatesField = new CoordinatesField();
        coordinatesField.setIdForm(idForm);
        coordinatesField.setFormVersion(formVersion);
        coordinatesField.setIdField(idField);

        FormMessagesHandler fmh = FormMessagesHandler.getInstance();

        try(SqlSession session = DBUtils.getInstance().openReadSession()) {
            if(withMessages){
                coordinatesField.setRequiredErrorMap(fmh.listFieldMessages(session, idField, idForm, coordinatesField.getFormVersion(), "requiredError"));
                coordinatesField.setInvalidErrorMap(fmh.listFieldMessages(session, idField, idForm, coordinatesField.getFormVersion(), "invalidError"));
            }
        }

        return coordinatesField;
    }

    @Override
    public void deleteFormField(SqlSession session, String idForm, int formVersion, String idField) throws IOException {
        //It is no necesary because fild does not contain custom tables
    }

    @Override
    public Map<String, String> validateValue(Request request, String idField, Map<String, FormField> fields) throws IOException {
        return new HashMap<>();
    }

}

