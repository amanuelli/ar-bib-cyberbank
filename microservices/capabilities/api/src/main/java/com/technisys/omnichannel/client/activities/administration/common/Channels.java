package com.technisys.omnichannel.client.activities.administration.common;

import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.limits.CapForUser;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Channels {
    
    private Channels() {
        throw new IllegalStateException("Utility class");
    }

    public static List<CapForUser> listCaps(EnvironmentUser envUser, final int idEnvironment, String environmentType, String idUser) throws IOException {
        List<CapForUser> caps = new ArrayList<>();
        double defaultCapMaximum = ConfigurationFactory.getInstance().getDouble(Configuration.LIMITS, "default_cap_user_" + environmentType);
        final List<String> enabledChannels = CoreUtils.getEnabledChannels();
        final String userEnabledChannels = envUser.getEnabledChannels();

        enabledChannels.remove(CoreUtils.CHANNEL_ALL);

        for (String channel : enabledChannels) {
            if (StringUtils.contains(userEnabledChannels, channel)) {
                CapForUser cap = LimitsHandlerFactory.getHandler().readCapForUser(idEnvironment, channel, idUser);

                if (cap != null && cap.getMaximum() < 0) {
                    cap.setMaximum(defaultCapMaximum);
                }

                caps.add(cap);
            }
        }

        return caps;
    }

    public static CapForUser getChannelAll(String idUser, int idEnvironment, String environmentType) throws IOException {
        CapForUser channelAll = LimitsHandlerFactory.getHandler().readCapForUser(idEnvironment, IBRequest.Channel.all, idUser);

        if (channelAll != null && channelAll.getMaximum() < 0) {
            channelAll.setMaximum(ConfigurationFactory.getInstance().getDouble(Configuration.LIMITS, "default_cap_user_" + environmentType));
        }

        return channelAll;
    }
}
