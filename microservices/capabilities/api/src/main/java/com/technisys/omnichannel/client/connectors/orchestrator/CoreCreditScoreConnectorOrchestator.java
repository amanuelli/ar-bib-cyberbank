/*
 *  Copyright (c) 2019 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */

package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorS;
import com.technisys.omnichannel.client.domain.CreditScore;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;

import java.io.IOException;

public class CoreCreditScoreConnectorOrchestator  extends CoreConnectorOrchestrator {

    public CoreCreditScoreConnectorOrchestator(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    public static CreditScore creditScore(String documentId) throws BackendConnectorException{
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return RubiconCoreConnectorS.creditScore(documentId);
                case CYBERBANK_CONNECTOR:
                    if (documentId.endsWith(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "creditCardRequest.score"))) {
                        return new CreditScore(0, null, 49, documentId);
                    } else {
                        return new CreditScore(0, null, 70, documentId);
                    }
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (IOException e) {
            throw new BackendConnectorException(e);
        }
    }
}
