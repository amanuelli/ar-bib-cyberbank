/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.loans;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 *
 */
@DocumentedActivity("List Loans")
public class ListLoansActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(ListLoansActivity.class);
    public static final String ID = "loans.list";

    public interface InParams {
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Amortized loans")
        String AMORTIZED_LOANS = "amortizedLoans";

        @DocumentedParam(type = String.class, description = "Properties loans")
        String PROPERTY_LOANS = "propertiesLoans";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            List<String> productTypes = Arrays.asList(Constants.PRODUCT_PI_KEY, Constants.PRODUCT_PA_KEY);

            List<Loan> loans = getLoanList(request, productTypes);

            List<Loan> authorizedAmortizedLoans = new ArrayList<>();

            List<Loan> authorizedPropertiesLoans = new ArrayList<>();

            for (Loan loan : loans) {
                loan.setPaidPercentage(Math.round(loan.getPaidPercentage()));
                switch (loan.getProductType()) {
                    case Constants.PRODUCT_PA_KEY:
                        authorizedAmortizedLoans.add(loan);
                        break;
                    case Constants.PRODUCT_PI_KEY:
                        authorizedPropertiesLoans.add(loan);
                        break;
                    default:
                        String logString = String.format("Unexpected loan.getProductType() value %s", loan.getProductType());
                        log.error(logString);
                        break;
                }
            }

            response.putItem(OutParams.AMORTIZED_LOANS, authorizedAmortizedLoans);
            response.putItem(OutParams.PROPERTY_LOANS, authorizedPropertiesLoans);

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    public static List<Loan> getLoanList(Request request, List<String> productTypes) throws IOException, BackendConnectorException {
        Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

        List<String> idPermissions = new ArrayList<>();
        idPermissions.add(PRODUCT_READ_PERMISSION);

        List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);

        List<Loan> coreProducts = CoreLoanConnectorOrchestrator.list(request.getIdTransaction(), environment.getClients(), productTypes);

        List<Loan> authorizedLoans = new ArrayList<>();

        ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

        for (Loan loan : coreProducts) {
            for (Product environmentProduct : environmentProducts) {
                if (loan.getIdProduct().equals(environmentProduct.getIdProduct())) {
                    loan.setProductAlias(environmentProduct.getProductAlias());
                    loan.setShortLabel(pLabeler.calculateShortLabel(loan));
                    loan.setLabel(pLabeler.calculateLabel(loan));
                    loan.setIdProduct(environmentProduct.getIdProduct());
                    loan.setProductType(environmentProduct.getProductType());
                    loan.setPaidPercentage((loan.getPaidAmount() * 100) / loan.getTotalAmount());
                    authorizedLoans.add(loan);
                    break;
                }
            }
        }

        return authorizedLoans;

    }
}
