package com.technisys.omnichannel.client.forms.fields.productselector.resources;

import com.technisys.omnichannel.client.forms.fields.productselector.ProductselectorFieldHandler.Option;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.forms.fields.FrontendOption;
import com.technisys.omnichannel.core.forms.fields.OptionsResource;
import com.technisys.omnichannel.core.forms.fields.OptionsResourceHandler;
import java.io.IOException;
import java.util.List;

/**
 * Implementacion del recurso de opciones para subtipo loanSelector del campo productSelector
 * @author ?
 */
public class LoanSelectorOptionsResource implements OptionsResource {
    
    @Override
    public List<? extends FrontendOption> getOptions(FormField field, Request request) throws IOException {
        
        //obtengo los productos por defecto
        List<Option> options = (List<Option>)OptionsResourceHandler.getOptions(field.getType(), "default", field, request);

        //aca iria mas logica especifica para el subtipo de campo loanSelector sobre las opciones obtenidas para el subtipo "default"
        
        return options;
    }
}