/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.onboarding;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreJobInformationConnectorOrchestator;
import com.technisys.omnichannel.client.domain.RoleJob;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.List;

import static com.technisys.omnichannel.client.ReturnCodes.BACKEND_SERVICE_ERROR;
import static com.technisys.omnichannel.client.ReturnCodes.OK;

/**
 * @author cmeneses
 */
@AnonymousActivity
public class ListJobActivity extends Activity {

    public static final String ID = "onboarding.jobs.list";

    public interface InParams {

    }

    public interface OutParams {
        String JOBS_LIST = "jobs";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            List<RoleJob> states = CoreJobInformationConnectorOrchestator.listRolesJob();
            response.putItem(OutParams.JOBS_LIST, states);
            response.setReturnCode(OK);
        } catch (BackendConnectorException e) {
            throw new ActivityException(BACKEND_SERVICE_ERROR, e);
        }
        return response;
    }

}