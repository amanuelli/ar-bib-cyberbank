/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.files.FilesHandler;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

@DocumentedActivity("Delete file")
public class DeleteFileActivity extends Activity {

    public static final String ID = "files.delete";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "File ID")
        String ID_FILE = "idFile";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Deleted File ID")
        String ID_FILE = "idFile";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int idFile = request.getParam(InParams.ID_FILE, Integer.class);

            FilesHandler.getInstance().deleteTransactionFile(request.getIdUser(), idFile, request.getIdEnvironment());

            response.putItem(OutParams.ID_FILE, idFile);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        
        String idFile = request.getParam(InParams.ID_FILE, String.class);
        if (StringUtils.isBlank(idFile)){
            result.put(InParams.ID_FILE, "file.delete.idFile.required");
        }else{
            try {
                Integer.parseInt(idFile);
            } catch (NumberFormatException e) {
                result.put(InParams.ID_FILE, "file.delete.idFile.invalid");
            }
        }
        return result;
    }
}