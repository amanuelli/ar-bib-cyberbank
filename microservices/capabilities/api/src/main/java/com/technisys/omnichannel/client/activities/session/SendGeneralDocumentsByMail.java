package com.technisys.omnichannel.client.activities.session;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.TemplatingUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Attachment;
import com.technisys.omnichannel.core.domain.GeneralConditionDocument;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import org.apache.commons.lang.StringUtils;

import javax.mail.MessagingException;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AnonymousActivity
@DocumentedActivity("List all general condition documents")
public class SendGeneralDocumentsByMail extends Activity {

    public static final String ID = "session.sendGeneralDocumentsByMail";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "User email to send documents")
        String USER_EMAIL = "userEmail";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Response response = new Response(request);

            int actualCondition =  GeneralConditions.getInstance().actualCondition();
            List<GeneralConditionDocument> generalConditionDocuments = GeneralConditions.getInstance().listAllGeneralConditionDocuments(actualCondition);
            I18n i18n =I18nFactory.getHandler();

            String userEmail = request.getParam(InParams.USER_EMAIL, String.class);
            Attachment[] attachments = new Attachment[generalConditionDocuments.size()];
            int index = 0;
            String fileName;
            for(GeneralConditionDocument generalConditionDocument : generalConditionDocuments) {
                fileName = i18n.getMessage(generalConditionDocument.getFileNameKey(), request.getLang());
                attachments[index] = new Attachment(new ByteArrayInputStream(generalConditionDocument.getContent()), fileName, "application/pdf");
                index++;
            }

            String subject = i18n.getMessage("generalConditionDocument.email.subject", request.getLang());
            String template = TemplatingUtils.getHtmlTemplate("/templates/general_documents_mail_template.html");
            template = replaceTemplateValues(template, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "client.baseURL"), request.getLang());
            CoreUtils.sendAsyncClientEmail(subject, template, new String[] {userEmail}, attachments, request.getLang(), false);

            response.setReturnCode(ReturnCodes.OK);
            return response;

        } catch (IOException | MessagingException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        if(StringUtils.isBlank(request.getParam(InParams.USER_EMAIL, String.class))) {
            result.put(DownloadGeneralConditionsActivity.InParams.FILE_NAME_KEY, "session.endGeneralDocumentsByMail.userEmail.empty");
        }
        return result;
    }

    private static String replaceTemplateValues(String template, String clientBaseUrl, String lang) {
        I18n i18n =I18nFactory.getHandler();
        template = template.replace("%%IMAGE_TECHBANK_URL%%", clientBaseUrl + "/images/techbank_logo_email.png");

        template = template.replace("%%TEXT1%%", i18n.getMessage("generalConditionDocument.email.text1", lang));
        template = template.replace("%%TEXT2%%", i18n.getMessage("generalConditionDocument.email.text2", lang));
        template = template.replace("%%TEXT3%%", i18n.getMessage("generalConditionDocument.email.text3", lang));

        template = template.replace("%%PRIVACY%%", I18nFactory.getHandler().getMessage("global.privacy", lang));
        template = template.replace("%%TERM_AND_CONDITIONS%%", I18nFactory.getHandler().getMessage("global.termAndConditions", lang));
        template = template.replace("%%PRIVACY_URL%%", clientBaseUrl + "/privacyPolicy");
        template = template.replace("%%TERM_AND_CONDITIONS_URL%%", clientBaseUrl + "/termsAndConditions");
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        template = template.replace("%%CURRENT_YEAR%%", String.valueOf(currentYear));
        template = template.replace("%%TEXT8%%", I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode.MAIL.body.text8", lang));

        return template;
    }

}
