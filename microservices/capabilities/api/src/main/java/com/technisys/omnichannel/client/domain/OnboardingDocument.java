package com.technisys.omnichannel.client.domain;

import java.io.Serializable;

/**
 *
 * @author fpena
 */
public class OnboardingDocument implements Serializable{
    private long idDocument;
    private long idOnboarding;
    private byte[] content;
    private String documentType;

    public OnboardingDocument() {}

    public OnboardingDocument(byte[] content, String documentType, long idOnboarding) {
        this.content = content;
        this.documentType = documentType;
        this.idOnboarding = idOnboarding;
    }

    public long getIdDocument() {
        return idDocument;
    }

    public void setIdDocument(long idDocument) {
        this.idDocument = idDocument;
    }
    
    public long getIdOnboarding() {
        return idOnboarding;
    }

    public void setIdOnboarding(long idOnboarding) {
        this.idOnboarding = idOnboarding;
    }

    public byte[] getContent() {
        return content;
    }

    public void setContent(byte[] content) {
        this.content = content;
    }

    public String getDocumentType() {
        return documentType;
    }

    public void setDocumentType(String documentType) {
        this.documentType = documentType;
    }


    public static final class OnboardingDocumentBuilder {
        private long idDocument;
        private long idOnboarding;
        private byte[] content;
        private String documentType;


        public static OnboardingDocumentBuilder anOnboardingDocument() {
            return new OnboardingDocumentBuilder();
        }

        public OnboardingDocumentBuilder setIdDocument(long idDocument) {
            this.idDocument = idDocument;
            return this;
        }

        public OnboardingDocumentBuilder setIdOnboarding(long idOnboarding) {
            this.idOnboarding = idOnboarding;
            return this;
        }

        public OnboardingDocumentBuilder setContent(byte[] content) {
            this.content = content;
            return this;
        }

        public OnboardingDocumentBuilder setDocumentType(String documentType) {
            this.documentType = documentType;
            return this;
        }

        public OnboardingDocument build() {
            OnboardingDocument onboardingDocument = new OnboardingDocument();
            onboardingDocument.setIdDocument(idDocument);
            onboardingDocument.setIdOnboarding(idOnboarding);
            onboardingDocument.setContent(content);
            onboardingDocument.setDocumentType(documentType);
            return onboardingDocument;
        }
    }
}
