/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain.fields;

import com.technisys.omnichannel.core.domain.FormField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Represents a form field of type bank selector
 * 
 * @author sbarbosa
 */
public class BankselectorField extends FormField {

    protected transient String displayType;
    protected transient Map<String, String> placeholderMap;
    protected transient Map<String, String> requiredErrorMap;
    protected transient Map<String, String> invalidErrorMap;
    protected transient Map<String, String> extraLabels;
    protected transient List<String> codeTypeList;

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public Map<String, String> getPlaceholderMap() {
        return placeholderMap;
    }

    public void setPlaceholderMap(Map<String, String> placeholderMap) {
        this.placeholderMap = placeholderMap;
    }

    public void addPlaceholder(String lang, String value) {
        if (placeholderMap == null) {
            placeholderMap = new LinkedHashMap<>();
        }
        placeholderMap.put(lang, value);
    }

    public Map<String, String> getRequiredErrorMap() {
        return requiredErrorMap;
    }

    public void setRequiredErrorMap(Map<String, String> requiredErrorMap) {
        this.requiredErrorMap = requiredErrorMap;
    }

    public void addRequiredError(String lang, String value) {
        if (requiredErrorMap == null) {
            requiredErrorMap = new LinkedHashMap<>();
        }
        requiredErrorMap.put(lang, value);
    }

    public Map<String, String> getInvalidErrorMap() {
        return invalidErrorMap;
    }

    public void setInvalidErrorMap(Map<String, String> invalidErrorMap) {
        this.invalidErrorMap = invalidErrorMap;
    }

    public void addInvalidError(String lang, String value) {
        if (invalidErrorMap == null) {
            invalidErrorMap = new LinkedHashMap<>();
        }
        invalidErrorMap.put(lang, value);
    }

    public Map<String, String> getExtraLabels() {
        return extraLabels;
    }

    public void setExtraLabels(Map<String, String> extraLabels) {
        this.extraLabels = extraLabels;
    }

    public void addExtraLabel(String key, String value) {
        if (extraLabels == null) {
            extraLabels = new HashMap<>();
        }
        extraLabels.put(key, value);
    }

    public List<String> getCodeTypeList() {
        return codeTypeList;
    }

    public void setCodeTypeList(List<String> codeTypeList) {
        this.codeTypeList = codeTypeList;
    }

    public void addCodeType(String codeType) {
        if (codeTypeList == null) {
            codeTypeList = new ArrayList<>();
        }
        this.codeTypeList.add(codeType);
    }

}
