/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.client.ReturnCodes;
import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 */
public class WizardAdditionalFactorValidationActivity extends Activity {

    public static final String ID = "enrollment.wizard.additionalFactorValidation";

    public interface InParams {
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            InvitationCode invitation = getInvitationCode(request);

            // Pasos anteriores
            Map<String, String> result = WizardVerificationCodeActivity.validateFields(request, invitation);

            if (!result.isEmpty()) {
                result.put("NO_FIELD", "enrollment.wizard.invalidEnrollmentData");
            }

            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

}
