/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.groups;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@DocumentedActivity("Block/Unblock groups")
public class BlockUnblockGroupsActivity extends Activity {

    public static final String ID = "administration.groups.blockunblock.send";

    public interface InParams {

        @DocumentedParam(type = Integer[].class, description = "Group's list ids to block/unblock")
        String GROUP_ID_LIST = "groupIdList";
        @DocumentedParam(type = String[].class, description = "Group's list names to block/unblock")
        String GROUP_NAME_LIST = "groupNameList";
        @DocumentedParam(type = Boolean.class, description = "Operation to execute, true for block, false for unblock")
        String BLOCKED = "blocked";
    }

    public interface OutParams {
        
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (!Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            Integer[] groupIdList = request.getParam(InParams.GROUP_ID_LIST, Integer[].class);
            Boolean blocked = request.getParam(InParams.BLOCKED, Boolean.class);

            AccessManagementHandlerFactory.getHandler().updateGroupsStatus(Arrays.asList(groupIdList), blocked != null && blocked);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            Integer[] groupIdList = request.getParam(InParams.GROUP_ID_LIST, Integer[].class);

            if (groupIdList == null || groupIdList.length == 0) {
                result.put("NO_FIELD", "administration.groups.mustSelectGroup");
            } else {
                for (Integer groupId : groupIdList) {
                    if (groupId == null || groupId == -1) {
                        result.put("NO_FIELD", "administration.groups.mustSelectGroup");
                    } else {
                        Group group = AccessManagementHandlerFactory.getHandler().getGroup(groupId);
                        if (group == null || group.getIdEnvironment() != request.getIdEnvironment()) {
                            throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                        }

                        List<GroupPermission> permissions = AccessManagementHandlerFactory.getHandler().getGroupPermissions(group.getIdGroup());
                        for (GroupPermission permission : permissions) {
                            if (Constants.ADMINISTRATION_VIEW_PERMISSION.equals(permission.getIdPermission())
                                    || Constants.ADMINISTRATION_MANAGE_PERMISSION.equals(permission.getIdPermission())) {

                                result.put("NO_FIELD", "administration.groups.cantBlockAdminGroup");
                                break;
                            }
                        }
                    }
                }
            }

            return result;

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }
}
