/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankJobInformationConnector;
import com.technisys.omnichannel.client.domain.OccupationJob;
import com.technisys.omnichannel.client.domain.RoleJob;

import java.util.ArrayList;
import java.util.List;

public class CoreJobInformationConnectorOrchestator extends CoreConnectorOrchestrator {

    public static List<RoleJob> listRolesJob() throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                RoleJob roleJob = new RoleJob();
                roleJob.setCodeName("ING");
                roleJob.setName("Engineer");

                ArrayList<RoleJob> jobs = new ArrayList<>();
                jobs.add(roleJob);
                return jobs;
            case CYBERBANK_CONNECTOR:
                return CyberbankJobInformationConnector.listRolesJob().getData();
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static List<OccupationJob> listOccupationJobs() throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                OccupationJob occupationJob = new OccupationJob(500002, "Funcionarios del PLN, provincial, municipal ");
                List<OccupationJob> occupationJobs = new ArrayList<>();
                occupationJobs.add(occupationJob);
                return occupationJobs;

            case CYBERBANK_CONNECTOR:
                return CyberbankJobInformationConnector.listOccupationJob().getData();
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }
}
