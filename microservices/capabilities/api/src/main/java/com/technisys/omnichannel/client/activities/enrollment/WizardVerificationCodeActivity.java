/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;
import com.technisys.omnichannel.client.handlers.enrollment.EnrollmentHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 */
@ExchangeActivity @DocumentedActivity("Enrollment wizard code verification")
public class WizardVerificationCodeActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(WizardVerificationCodeActivity.class);
    public static final String ID = "enrollment.wizard.verificationCode";

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
        @DocumentedParam(type = Boolean.class, description = "Personal data enabled")
        String PERSONAL_DATA_ENABLED = "personalDataEnabled";
        @DocumentedParam(type = String.class, description = "Verification code")
        String VERIFICATION_CODE = "_verificationCode";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Exchange token")
        String EXCHANGE_TOKEN = "_exchangeToken";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            // Actualizo el exchange token
            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);

            String nextActivity = WizardFinishActivity.ID;
            List<String> activities = new ArrayList<>(Arrays.asList(StringUtils.split(token.getActivities(), ',')));
            if (!activities.contains(nextActivity)) {
                activities.add(nextActivity);
            }

            ExchangeTokenHandler.updateActivities(exchangeToken, activities.toArray(new String[0]));

            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            InvitationCode invitation = getInvitationCode(request);

            return validateFields(request, invitation);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    protected static Map<String, String> validateFields(Request request, InvitationCode invitation) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        try {
            // Valido que no me esten tratando de realziar un enrollment de un usuario ya existente en omnichannel, ya debería de haber echo la aplicación en redirect correspondiente a la página de asociar
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber());
            if (user != null) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED, "Llamada inválida al enrollment con el codigo de invitación " + invitation.getInvitationCode() + ". Este código corresponde con un asociar ambiente no un registro de usuario.");
            }

            String verificationCode = request.getParam(InParams.VERIFICATION_CODE, String.class);
            if (StringUtils.isBlank(verificationCode)) {
                result.put(InParams.VERIFICATION_CODE, "enrollment.step1.verificationCode.empty");
            } else {
                switch (EnrollmentHandler.validateVerificationCode(invitation, verificationCode)) {
                    case EnrollmentHandler.VERIFICATION_CODE_INVALID:
                        result.put(InParams.VERIFICATION_CODE, "enrollment.step1.verificationCode.invalidFormat");
                        break;
                    case EnrollmentHandler.VERIFICATION_CODE_MAX_ATTEMPTS_REACHED:
                        result.put("NO_FIELD", "enrollment.step1.verificationCode.maxAttemptsReached");
                        break;
                    default:
                        log.error("Unexpected EnrollmentHandler.validateVerificationCode value.");
                        break;
                }
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return result;
    }

}
