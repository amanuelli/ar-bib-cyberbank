/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.client.handlers.multiline.MultilineHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.activities.ActivityHandler;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import org.apache.commons.codec.binary.Base64;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author isilveira
 */
@DocumentedActivity("Download file with errors of multiline payment")
public class DownloadMultilinePaymentErrorsActivity extends Activity {

    public static final String ID = "files.download.multiline.errors";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "ID of the associated transaction")
        String TRANSACTION_ID = "idTransactionToRead";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Filename")
        String FILE_NAME = "filename";
        @DocumentedParam(type = String.class, description = "Base64 encoded file content")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        String idTransactionToRead = request.getParam(InParams.TRANSACTION_ID, String.class);
        try {
            List<TransactionLine> errors = MultilineHandler.getTransactionLinesWithErrors(idTransactionToRead);
            String content;

            try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
                for (TransactionLine error : errors) {
                    String textError = error.toErrorString(request.getLang()) + System.lineSeparator();

                    os.write(textError.getBytes());
                }

                content = new String(Base64.encodeBase64(os.toByteArray()));
            }

            response.putItem(OutParams.FILE_NAME, idTransactionToRead + "_errors.txt");
            response.putItem(OutParams.CONTENT, content);
            response.setReturnCode(ReturnCodes.OK);

            return response;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String idTransactionToRead = request.getParam(InParams.TRANSACTION_ID, String.class);

        try {
            Transaction transaction = TransactionHandlerFactory.getInstance().read(idTransactionToRead);

            if (transaction == null) {
                throw new ActivityException(ReturnCodes.TRANSACTION_DOESNT_EXIST);
            }
            if (transaction.getIdEnvironment() != request.getIdEnvironment()) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

            ClientActivityDescriptor ad = ActivityHandler.getInstance().readDescriptor(transaction.getIdActivity());
            if (!Authorization.hasPermissionOverTransaction(request.getIdUser(), request.getIdEnvironment(), ad, transaction)) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}