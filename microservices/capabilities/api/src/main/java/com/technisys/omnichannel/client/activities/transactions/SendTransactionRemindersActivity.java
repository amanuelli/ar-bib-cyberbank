/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.transactions;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.activities.ActivityHandler;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.*;

public class SendTransactionRemindersActivity extends Activity {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    public static final String ID = "creditcard.sendRequestReminders";

    public interface InParams {

        String ID_TRANSACTION = "idTransactionToSendReminders";
    }

    public interface OutParams {

        String USERS_NOTIFIED_AMOUNT = "usersNotifiedAmount";
    }

    /**
     * Envia recordatorio de firma de transaccion a los usuarios capaces de
     * firmar (que aun no lo hayan hecho)
     *
     * @param request Parametros de solicitud
     * @return ReturnCodes.OK si se enviaron los recordatorios correctamente, o
     * codigo del motivo de error en otro caso
     * @throws ActivityException Error de comunicacion
     */
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            Transaction transaction = TransactionHandlerFactory.getInstance().read(request.getParam(InParams.ID_TRANSACTION, String.class));
            if (transaction != null) {
                if (transaction.getIdEnvironment() != request.getIdEnvironment()) {
                    throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                }

                //descriptor de la actividad
                ClientActivityDescriptor ad = ActivityHandler.getInstance().readDescriptor(transaction.getIdActivity());

                //firmas del ambiente
                List<Signature> signatures = null;
                // se guarda los que ya han firmado la transaccion, de la forma <idUser, iduser>
                Map<String, String> signatureUsers = new HashMap<>();

                //obtengo los ya firmantes de la transaccion
                List<TransactionSignature> transactionSignatures = transaction.getSignatures();
                for (TransactionSignature ts : transactionSignatures) {
                    signatureUsers.put(ts.getIdUser(), ts.getIdUser());
                }

                //la cantidad en funcion del nivel de firma
                List<SignatureCount> signatureLevelCounts = TransactionHandlerFactory.getInstance().listTransactionSignaturesBySignatureLevel(transaction.getIdTransaction());

                //chequeo el tipo de actividad para saber si la transaccion es con monto o sin monto o administrativa
                switch (ad.getKind()) {
                    case Admin:
                        //obtengo los esquemas de firma para transacciones administrativas
                        signatures = LimitsHandlerFactory.getHandler().listSignatures(request.getIdEnvironment(), Signature.TYPE_ADMINISTRATIVE, request.getIdActivity(), request.getIdForm(), request.getProducts());
                        break;
                    case Transactional:
                        //obtengo los esquemas de firma para transacciones con monto
                        signatures = LimitsHandlerFactory.getHandler().listSignatures(request.getIdEnvironment(), Signature.TYPE_AMOUNT, request.getIdActivity(), request.getIdForm(), request.getProducts());
                        break;
                    case Other:
                        //obtengo los esquemas de firma para transacciones sin monto
                        signatures = LimitsHandlerFactory.getHandler().listSignatures(request.getIdEnvironment(), Signature.TYPE_NO_AMOUNT, request.getIdActivity(), request.getIdForm(), request.getProducts());
                        break;
                }

                //reviso las firmas del ambiente y busco los niveles de firma que me faltan
                Map<String, String> remainingSignatureLevels = new HashMap<>();
                if (signatures != null && !signatures.isEmpty()) {
                    for (Signature signature : signatures) {
                        for (Character c : signature.getGroupsMap().keySet()) {
                            if (!enoughForThisLevel(c, signature.getGroupsMap().get(c), signatureLevelCounts)) {
                                remainingSignatureLevels.put(c.toString(), c.toString());
                            }
                        }
                    }
                }

                //obtenidos los niveles de firmas que preciso aun, obtengo los usuarios del ambiente que tengan ese nivel de firma, que puedan
                //efectuar la transaccion dada y firmarla
                List<String> availableUsersToSign = TransactionHandlerFactory.getInstance().listClientsForTransactionSigning(transaction.getIdTransaction(), new ArrayList<>(remainingSignatureLevels.keySet()));

                //Si hay un formulario asociado, leo el formulario
                Form form = null;
                if (transaction.getIdForm() != null) {
                    form = FormsHandler.getInstance().readForm(transaction.getIdForm(), transaction.getFormVersion());
                }

                int notificationsSend = 0;
                if (availableUsersToSign != null) {
                    for (String availableUserToSign : availableUsersToSign) {
                        //si el usuario aun no firmo la transaccion, envio notificacion
                        if (!signatureUsers.containsKey(availableUserToSign)) {
                            sendNotification(availableUserToSign, transaction, form);
                            notificationsSend++;
                        }
                    }
                }

                response.putItem(OutParams.USERS_NOTIFIED_AMOUNT, notificationsSend);
            }

            response.setReturnCode(ReturnCodes.OK);

            return response;

        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    /**
     * Indica si la cantidad actual de firmas de cierto nivel son suficientes
     *
     * @param c Nivel de firma
     * @param levelsCount Cantidad necesarias de este nivel de firma
     * @param signatureLevelCounts Cantidad actual de firmas de la transaccion
     * para todos los niveles
     * @return Booleano indicando si la cantidad de firmas del nivel dado son
     * suficientes
     */
    private boolean enoughForThisLevel(Character c, Integer levelsCount, List<SignatureCount> signatureLevelCounts) {
        Integer count = getSignaturesCount(signatureLevelCounts, c.toString());

        if (count == null) {
            //no se precisan firmas de este nivel
            return true;
        } else if (count < levelsCount) {
            //se precisan mas firmas de este nivel de las que hay
            return false;
        }

        //ya hay suficientes firmas de este nivel
        return true;
    }

    /**
     * Obtiene la cantidad de firmas para un nivel dado dentro de la lista de
     * cantidades por nivel de firma
     *
     * @param signatureLevelCounts Lista de cantidades por nivel de firma
     * @param signatureLevel Nivel de firma a buscar
     * @return Cantidad de firmas para el nivel de firma dado
     */
    private Integer getSignaturesCount(List<SignatureCount> signatureLevelCounts, String signatureLevel) {
        SignatureCount sc = new SignatureCount();
        sc.setSignatureLevel(signatureLevel);
        int idx = signatureLevelCounts.indexOf(sc);

        if (idx != -1) {
            return signatureLevelCounts.get(idx).getCount();
        }
        return null;
    }

    /**
     * Envia el recordatorio de posible firmante al un usuario dado
     *
     * @param idUser Identificador de usuario
     * @param transaction Datos de la transaccion
     * @param form Formulario asociado a la transaccion
     * @throws IOException Error de comunicacion
     */
    private void sendNotification(String idUser, Transaction transaction, Form form) throws IOException {
        String communicationType = "transactionReminders";
        int idTray = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "transactions.reminder.idTray");

        HashMap<String, String> fillers = new HashMap<>();

        String userLang = null;
        try {
            userLang = AccessManagementHandlerFactory.getHandler().getUserLang(idUser);
        } catch (IOException ioe) {
            log.error("Input/Output error ({}).",ioe.getMessage());
            userLang = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.default.lang");
        }

        String transactionName;
        if (form != null) {
            transactionName = form.getFormNameMap().get(userLang);
        } else {
            transactionName = I18nFactory.getHandler().getMessage("activities." + transaction.getIdActivity(), userLang);
        }

        //nombre de la transaccion
        fillers.put("TRANSACTION_NAME", transactionName);

        //identificador de la transaccion
        fillers.put("ID_TRANSACTION", transaction.getIdTransaction());

        //link a la transaccion
        String link = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "client.baseURL");
        link += "/forms/index?idTransaction=" + transaction.getIdTransaction();
        fillers.put("TRANSACTION_LINK", link);

        //busco mensajes especificos para la actividad
        String subject;
        if (I18nFactory.getHandler().isMessageExists("transactions.reminder." + transaction.getIdActivity() + ".subject", userLang)) {
            subject = I18nFactory.getHandler().getMessage("transactions.reminder." + transaction.getIdActivity() + ".subject", userLang, fillers);
        } else {
            subject = I18nFactory.getHandler().getMessage("transactions.reminder.subject", userLang, fillers);
        }

        String defaultBody;
        if (I18nFactory.getHandler().isMessageExists("transactions.reminder." + transaction.getIdActivity() + ".body", userLang)) {
            defaultBody = I18nFactory.getHandler().getMessage("transactions.reminder." + transaction.getIdActivity() + ".body", userLang, fillers);
        } else {
            defaultBody = I18nFactory.getHandler().getMessage("transactions.reminder.body", userLang, fillers);
        }
        
        RecipientCriteria recipient = new RecipientCriteria(RecipientCriteria.RECIPIENT_TYPE_USERS, Arrays.asList(idUser), null, null, null);
        
        // Se crea el 'body' para cada uno de los tipo de transporte (transactions.reminder.body.mail, transactions.reminder.body.sms ... )
        Map<String, String> bodyByTransportMap = new HashMap<>();
        bodyByTransportMap.put(Communication.TRANSPORT_MAIL, getBodyTransport(Communication.TRANSPORT_MAIL.toLowerCase(), fillers, transaction.getIdActivity(), userLang, defaultBody));
        bodyByTransportMap.put(Communication.TRANSPORT_DEFAULT, getBodyTransport(Communication.TRANSPORT_DEFAULT.toLowerCase(), fillers, transaction.getIdActivity(), userLang, defaultBody));
        bodyByTransportMap.put(Communication.TRANSPORT_FACEBOOK, getBodyTransport(Communication.TRANSPORT_FACEBOOK.toLowerCase(), fillers, transaction.getIdActivity(), userLang, defaultBody));
        bodyByTransportMap.put(Communication.TRANSPORT_SMS, getBodyTransport(Communication.TRANSPORT_SMS.toLowerCase(), fillers, transaction.getIdActivity(), userLang, defaultBody));
        bodyByTransportMap.put(Communication.TRANSPORT_TWITTER, getBodyTransport(Communication.TRANSPORT_TWITTER, fillers, transaction.getIdActivity(), userLang, defaultBody));
        bodyByTransportMap.put(Communication.TRANSPORT_PUSH, getBodyTransport(Communication.TRANSPORT_PUSH, fillers, transaction.getIdActivity(), userLang, defaultBody));

        NotificationsHandlerFactory.getHandler().create(null, idTray, subject, bodyByTransportMap, null, communicationType, -1,
                transaction.getIdEnvironment(), Communication.PRIORITY_HIGH, recipient);
    }
    
    /**
     * Retorna el texto configurado en messages para el tipo de transporte indicado
     * @param transportType Tipo de transporte, ej. mail, sms, ...
     * @param fillers
     * @param idActivity id de la actividad
     * @param userLang lenguaje del mensaje a retornar
     * @param defaultBody mensaje por defecto, si no se encuentra datos para el transporte solicitado
     * @return texto con el mensaje para transportType, o 'defaultBody' si no existe el dato
    */
    private String getBodyTransport(String transportType, HashMap<String, String> fillers, String idActivity, String userLang, String defaultBody) {

        String bodyReturn = null;
        String idMessageActivityTransport = "transactions.reminder." + idActivity + ".body." + transportType;
        String idMessageTransport = "transactions.reminder.body." + transportType;

        //chequeo el tipo de transporte para el body
        if (I18nFactory.getHandler().isMessageExists(idMessageActivityTransport, userLang)) {
            bodyReturn = I18nFactory.getHandler().getMessage(idMessageActivityTransport, userLang, fillers);
        } else if (I18nFactory.getHandler().isMessageExists(idMessageTransport, userLang)) {
            bodyReturn = I18nFactory.getHandler().getMessage(idMessageTransport, userLang, fillers);
        }

        if (StringUtils.isBlank(bodyReturn)) {
            bodyReturn = defaultBody;
        }

        return bodyReturn;
    }
}
