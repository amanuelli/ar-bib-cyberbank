/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.utils.ObjectPool;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.rubicon.core.loans.Loans;
import com.technisys.omnichannel.rubicon.core.loans.WSLoans;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.BindingProvider;
import java.net.URL;

import static com.technisys.omnichannel.client.connectors.RubiconCoreConnector.WEBSERVICES_URL;
/**
 *
 * @author fpena
 */
public class WSLoansFactory implements ObjectPool.ObjectPoolFactory<Loans> {

    private static final Logger log = LoggerFactory.getLogger(WSLoansFactory.class);

    @Override
    public Loans create() {

        String wsdlSufix = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "backend.webservices.loans.wsdl", "");
        String endpoint = WEBSERVICES_URL + "/WSLoans";

        Loans port = null;
        WSLoans service;
        boolean useRemoteWSDL = !"".equals(wsdlSufix);

        try {
            if (useRemoteWSDL) {
                URL url = new URL(WEBSERVICES_URL + wsdlSufix);
                service = new WSLoans(url);
            } else {
                service = new WSLoans();
            }

            port = service.getLoansPort();

            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
        } catch (Exception e) {
            log.warn("Error creating WSLoans port : " + e.getMessage(), e);
        }
        
        return port;
    }
    
}
