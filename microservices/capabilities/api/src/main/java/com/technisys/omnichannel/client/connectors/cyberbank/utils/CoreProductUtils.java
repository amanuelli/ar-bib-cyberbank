/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.utils;

public class CoreProductUtils {

    private CoreProductUtils() {
    }

    public static String getIsoProduct(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        return values[4];
    }

    public static String getSubProductId(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        return values[5];
    }

    public static String getBranch(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        return values[6];
    }

    public static String getCurrencyCode(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        return values[7];
    }

    public static String getPhysicAccountId(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        return values[8];
    }

    public static String getPhysicProductId(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        return values[9];
    }

    public static String getPhysicCurrencyId(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        return values[10];
    }

    public static String getPhysicBranchId(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        return values[11];
    }

}
