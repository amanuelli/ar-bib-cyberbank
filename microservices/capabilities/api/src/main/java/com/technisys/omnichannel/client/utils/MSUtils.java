/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

public class MSUtils {


    private MSUtils() {
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    public static int parseLocationId(String location) {
        int lastIndexSlash = location.lastIndexOf('/');
        return Integer.parseInt(location.substring(lastIndexSlash + 1));

    }

}
