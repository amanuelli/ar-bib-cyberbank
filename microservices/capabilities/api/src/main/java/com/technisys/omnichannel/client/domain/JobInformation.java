/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;

import java.util.Date;
import java.util.Map;

public class JobInformation {

    private String companyName;
    private String occupation;
    private Amount income;
    private Date entryDate;

    public JobInformation() {
    }

    public JobInformation(Map values) {
        this.companyName = RequestParamsUtils.getValue(values, "companyName", String.class);
        this.occupation = RequestParamsUtils.getValue(values, "occupation", String.class);
        this.income = RequestParamsUtils.getValue(values, "income", Amount.class);
        this.entryDate = RequestParamsUtils.getValue(values, "entryDate", Date.class);
    }

    public JobInformation(String companyName, String occupation, Amount income, Date entryDate) {
        this.companyName = companyName;
        this.occupation = occupation;
        this.income = income;
        this.entryDate = entryDate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getOccupation() {
        return occupation;
    }

    public void setOccupation(String occupation) {
        this.occupation = occupation;
    }

    public Amount getIncome() {
        return income;
    }

    public void setIncome(Amount income) {
        this.income = income;
    }

    public Date getEntryDate() {
        return entryDate;
    }

    public void setEntryDate(Date entryDate) {
        this.entryDate = entryDate;
    }

    @Override
    public String toString() {
        return String.format("%s%s%s%s", this.companyName, this.occupation, this.income, this.entryDate.toString());
    }

    @Override
    public boolean equals(Object o) {
        if (o == null) {
            return false;
        }

        if (this.getClass() != o.getClass()) {
            return false;
        }

        JobInformation jobInformation = (JobInformation) o;
        return jobInformation.getCompanyName().equals(this.companyName) && jobInformation.occupation.equals(this.occupation)
                && jobInformation.getIncome().equals(this.income) && jobInformation.getEntryDate().equals(this.entryDate);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}
