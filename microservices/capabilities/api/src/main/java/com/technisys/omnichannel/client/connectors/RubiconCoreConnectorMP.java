package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.client.domain.TransactionLine.ErrorCode;
import com.technisys.omnichannel.client.domain.TransactionLine.Status;
import com.technisys.omnichannel.client.domain.TransactionLineResult;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class RubiconCoreConnectorMP extends RubiconCoreConnector {

    // TODO: All the method are returning a POJO without considering the return code... and throwing an exception when there is an error at network level, which is good, but also when the error is at application level (return_code != 0); we should create a Response<T> object with the return code and the data inside ;)

    public static List<TransactionLineResult> salaryPaymentSend(List<TransactionLine> transactionLines) {
        List<TransactionLineResult> result = new ArrayList<>();

        for (TransactionLine transactionLine : transactionLines) {
            if (transactionLine.getCreditAccountNumber().endsWith("666")) {
                int random = ThreadLocalRandom.current().nextInt(100, 102);

                result.add(new TransactionLineResult(transactionLine.getIdTransaction(), transactionLine.getLineNumber(), Status.PROCESSED_WITH_ERROR, ErrorCode.fromValue(Integer.toString(random))));
            } else {
                result.add(new TransactionLineResult(transactionLine.getIdTransaction(), transactionLine.getLineNumber(), Status.PROCESSED, null));
            }
        }


        return result;
    }
}
