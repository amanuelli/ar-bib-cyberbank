/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.connectors.cyberbank.utils.CoreProductUtils;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.TransferForeignBankDetail;
import com.technisys.omnichannel.client.domain.TransferInternalDetail;
import com.technisys.omnichannel.client.domain.TransferOtherBankDetails;
import com.technisys.omnichannel.client.utils.JsonTemplateUtils;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.domain.Amount;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class CyberbankTransferConnector extends CyberbankCoreConnector {

    private static final Logger LOG = LoggerFactory.getLogger(CyberbankTransferConnector.class);
    private static final String SERVICE_NAME = "Srv - processAccountTransfers";
    private static final String TRANSFER_REASON_FOREIGN_BANK = "TFC";
    private static final String TRANSFER_REASON_LOCAL_BANK = "TFT";
    private static final String CODE_TYPE_SWIFT = "SWIFT";
    private static final String CODE_TYPE_ABA = "ABA";
    private static final String CODE_TYPE_IBAN = "IBAN";
    private static final String LOCAL_SUBPRODUCT_ID = "303364";
    private static final String FOREIGN_SUBPRODUCT_ID = "303182";



    /**
     * Transfer money between two accounts
     *
     * @param from        Account to debit amount
     * @param to          account to credit amount
     * @param description Transaction description
     * @return
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<TransferInternalDetail> internal(Account from, Account to, double amount, String description) throws CyberbankCoreConnectorException {

        String fromExtraInfo = from.getExtraInfo();
        String toExtraInfo = to.getExtraInfo();

        CyberbankCoreConnectorResponse<TransferInternalDetail> response = new CyberbankCoreConnectorResponse<>();
        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("processAccountTransfers", true)
                .addRequestParameter("fromSubProductId", CoreProductUtils.getSubProductId(fromExtraInfo))
                .addRequestParameter("fromCurrencyCodeId", CoreProductUtils.getCurrencyCode(fromExtraInfo))
                .addRequestParameter("fromAccountNumber", ProductUtils.getNumber(fromExtraInfo))
                .addRequestParameter("fromProductId", ProductUtils.getBackendID(fromExtraInfo))
                .addRequestParameter("fromBranchId", CoreProductUtils.getBranch(fromExtraInfo))
                .addRequestParameter("fromReason", "211")
                .addRequestParameter("fromTxnCode", "6")
                .addRequestParameter("fromIsoProductId", CoreProductUtils.getIsoProduct(fromExtraInfo))

                .addRequestParameter("toCurrencyCodeId", CoreProductUtils.getCurrencyCode(toExtraInfo))
                .addRequestParameter("toProductId", ProductUtils.getBackendID(toExtraInfo))
                .addRequestParameter("toBranchId", CoreProductUtils.getBranch(toExtraInfo))
                .addRequestParameter("toSubProductId", CoreProductUtils.getSubProductId(toExtraInfo))
                .addRequestParameter("toAccountNumber", ProductUtils.getNumber(toExtraInfo))
                .addRequestParameter("toReason", "210")
                .addRequestParameter("toTxnCode", "7")

                .addRequestParameter("currency", CoreProductUtils.getCurrencyCode(toExtraInfo))
                .addRequestParameter("transactionDescription", description)
                .addRequestParameter("amount", String.valueOf(amount));

        LOG.info("{}: Request {}", SERVICE_NAME, URL);

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(SERVICE_NAME, jsonRequest);

        if (callHasError(serviceResponse)) {
            processErrors(SERVICE_NAME, serviceResponse, response);
        } else {
            TransferInternalDetail transferInternalDetail = new TransferInternalDetail();
            transferInternalDetail.setCreditAmount(new Amount("1", amount));
            transferInternalDetail.setDebitAmount(new Amount("1", amount));
            transferInternalDetail.setRate(0);
            transferInternalDetail.setReturnCode(0);
            transferInternalDetail.setReturnCodeDescrption("");
            response.setData(transferInternalDetail);
        }

        return response;
    }

    public static CyberbankCoreConnectorResponse<TransferOtherBankDetails> toLocalBanks(String idCustomer, Account from, String to, String destinationCustomerName,
                                                                                        String destinationCustomerIdentification, Amount amount, String currency,
                                                                                        String reference, String idBank) throws CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<TransferOtherBankDetails> response = new CyberbankCoreConnectorResponse<>();
        String serviceName = "Srv - List Correspondent banks";

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("processEft_TransferInsert_Other_Bank_Channel", true)
                .addRequestParameter("subproductId", LOCAL_SUBPRODUCT_ID)
                .addRequestParameter("reasonSent", TRANSFER_REASON_LOCAL_BANK)
                .addRequestParameter("targetAccountOperationId", to)
                .addRequestParameter("customerId", idCustomer)
                .addRequestParameter("targetCustomerName", destinationCustomerName)
                .addRequestParameter("amount", amount.getQuantity())
                .addRequestParameter("targetIdentificationNumber", destinationCustomerIdentification)
                .addRequestParameter("accountOperationId", ProductUtils.getNumber(from.getExtraInfo()))
                .addRequestParameter("reference", reference)
                .addRequestParameter("currencyId", currency)
                .addRequestParameter("bankId", idBank);
        LOG.info("{}: Request {}", serviceName, URL);

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(serviceName, jsonRequest);

        if (callHasError(serviceResponse)) {
            processErrors(serviceName, serviceResponse, response);
        } else {
            TransferOtherBankDetails details = new TransferOtherBankDetails(new Amount("USD", 0.4));
            response.setData(details);
            return response;
        }

        return response;
    }

    public static CyberbankCoreConnectorResponse<TransferForeignBankDetail> toForeignlBank(String idCustomer, Account from, String to, String destinationCustomerName,
                                                                                           String destinationCustomerIdentification, Amount amount, String currency,
                                                                                           String reference, String idBank, String recipientBankCodeType, String recipientBankCode, String comments) throws CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<TransferForeignBankDetail> response = new CyberbankCoreConnectorResponse<>();
        String serviceName = "Srv - List Correspondent banks";

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("processEft_TransferInsert_Other_Bank_Channel", true)
                .addRequestParameter("customerId", idCustomer)
                .addRequestParameter("accountOperationId", ProductUtils.getNumber(from.getExtraInfo()))
                .addRequestParameter("currencyId", currency)
                .addRequestParameter("amount", amount.getQuantity())
                .addRequestParameter("subproductId", FOREIGN_SUBPRODUCT_ID)
                .addRequestParameter("reference", reference)
                .addRequestParameter("bankId", (idBank != null && !idBank.isEmpty() ? idBank : "11"))
                .addRequestParameter("targetCustomerName", destinationCustomerName)
                .addRequestParameter("targetAccountOperationId", to)
                .addRequestParameter("targetIdentificationNumber", destinationCustomerIdentification)
                .addRequestParameter("reasonSent", TRANSFER_REASON_FOREIGN_BANK)
                .addRequestParameter("comments", comments);

        if (recipientBankCodeType.equalsIgnoreCase(CODE_TYPE_SWIFT)) {
            cyberbankCoreRequest.addRequestParameter("swiftNumber", recipientBankCode);
        }else if (recipientBankCodeType.equalsIgnoreCase(CODE_TYPE_ABA)) {
            cyberbankCoreRequest.addRequestParameter("abaNumber", recipientBankCode);
        }else if (recipientBankCodeType.equalsIgnoreCase(CODE_TYPE_IBAN)) {
            cyberbankCoreRequest.addRequestParameter("ibanNumber", recipientBankCode);
        }

        LOG.info("{}: Request {}", serviceName, URL);

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(serviceName, jsonRequest);

        if (callHasError(serviceResponse)) {
            processErrors(serviceName, serviceResponse, response);
        } else {
            TransferForeignBankDetail details = new TransferForeignBankDetail(new Amount("USD", 0.4));
            response.setData(details);
            return response;
        }
        return response;
    }
}
