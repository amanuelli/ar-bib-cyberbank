/*
 *  Copyright (c) 2020 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;

/**
 * Delete a signatures scheme (PREVIEW)
 */

@DocumentedActivity("Delete a signatures scheme (PREVIEW)")
public class DeleteSignaturesPreviewActivity extends Activity {
    public static final String ID = "administration.signatures.delete.preview";

    public interface InParams {
        @DocumentedParam(type = Integer.class, description = "Signature scheme identifier")
        String SIGNATURE_ID = "signatureId";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {

        return DeleteSignaturesActivity.validateFields(request);

    }
}