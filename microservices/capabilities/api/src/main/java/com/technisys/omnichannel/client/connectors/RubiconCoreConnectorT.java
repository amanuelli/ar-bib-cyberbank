/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.TransferInternalDetail;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.rubicon.core.accounts.Accounts;
import com.technisys.omnichannel.rubicon.core.accounts.WsTransferRequest;
import com.technisys.omnichannel.rubicon.core.accounts.WsTransferResponse;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.BindingProvider;
import java.text.MessageFormat;

/**
 *
 * @author fpena
 */
public class RubiconCoreConnectorT extends RubiconCoreConnector {

    private static final Logger log = LoggerFactory.getLogger(RubiconCoreConnectorT.class);
    // TODO: All the method are returning a POJO without considering the return code... and throwing an exception when there is an error at network level, which is good, but also when the error is at application level (return_code != 0); we should create a Response<T> object with the return code and the data inside ;)
    public static TransferInternalDetail transferInternalValidate(String idTransaction, String productGroupId, Account debitAccount, Account creditAccount, String currency, double amount, String debitReference, String creditReference) throws BackendConnectorException {
        return transferInternal(RubiconCoreConnectorC.OPERATION_VALIDATE, idTransaction, productGroupId, debitAccount, creditAccount, currency, amount, debitReference, creditReference);
    }

    public static TransferInternalDetail transferInternalSend(String idTransaction, String productGroupId, Account debitAccount, Account creditAccount, String currency, double amount, String debitReference, String creditReference) throws BackendConnectorException {
        return transferInternal(RubiconCoreConnectorC.OPERATION_CONFIRMATION, idTransaction, productGroupId, debitAccount, creditAccount, currency, amount, debitReference, creditReference);
    }


    /**
     * Srv T? - Transferencia entre cuentas del banco
     *
     * Dado un número de cuenta origen y un número de cuenta destino, un importe
     * y una moneda, se realiza una transferencia de fondos entre las cuentas.
     *
     * @param operacion Identificador de la operación a realizar (V, C)
     * @param idTransaction Identificador de la transacción de Omnichannel
     * @param debitAccount Cuenta de débito
     * @param creditAccount Cuenta de crédito currency currency Identificador de
     * la moneda del importe (restringido a la de debito o credito)
     * @param amount Importe a transferir
     * @return Detalle de la validación o confirmación de la transferencia
     */
    private static TransferInternalDetail transferInternal(String operacion, String idTransaction, String productGroupId, Account debitAccount, Account creditAccount, String currency, double amount, String debitReference, String creditReference) throws BackendConnectorException {
        TransferInternalDetail detail = null;
        WsTransferResponse response = null;

        try {
            String logMessage;
            WsTransferRequest request = new WsTransferRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setMode(operacion);

            request.setClientID(Integer.parseInt(productGroupId));
            request.setAmount(amount);
            request.setCurrency(currency);
            request.setDebitAccountID(ProductUtils.getBackendID(debitAccount.getExtraInfo()));
            if (creditAccount.getNumber() != null) {
                request.setCreditAccountID(creditAccount.getNumber());
            } else {
                request.setCreditAccountID(ProductUtils.getBackendID(creditAccount.getExtraInfo()));
            }

            request.setDebitReference(debitReference);
            request.setCreditReference(creditReference);

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Enviar al backend: {0}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            Accounts port = WS_ACCOUNTS_POOL.borrowObject();
            try {
                response = port.transfer(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_ACCOUNTS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logMessage= MessageFormat.format("Respuesta backend: {0}", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logMessage);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logMessage= MessageFormat.format("Error en backend (Srv T? - Transferencia entre cuentas del banco): {0}", errorDescription);
                log.warn(logMessage);

            }

            detail = new TransferInternalDetail(response);
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return detail;
    }
}
