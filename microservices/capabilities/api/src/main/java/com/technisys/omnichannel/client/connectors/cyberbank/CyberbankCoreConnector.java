/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.ssl.SSLContexts;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.net.ssl.SSLContext;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public abstract class CyberbankCoreConnector { //NOSONAR

    private static final Logger LOG = LoggerFactory.getLogger(CyberbankCoreConnector.class);
    private static final String DATE_FORMAT = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "connector.cyberbank.core.dateFormat", "yyyyMMddhhmmssSSS");
    private static final Integer MAX_TOTAL_CONNECTIONS = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "connector.cyberbank.connections.maxTotal", 100);
    private static final Integer MAX_CONNECTIONS_PER_ROUTE = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "connector.cyberbank.connections.maxPerRoute", 20);
    protected static final String URL = ConfigurationFactory.getInstance().getURLSafe(Configuration.PLATFORM, "connector.cyberbank.webservices.url");
    protected static final String REQ_TEMPLATE_PATH = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "connector.cyberbank.core.template.request", "/templates/connectors/cyberbank/request/");
    protected static final Integer DEFAULT_COUNTRY_CODE = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "connector.cyberbank.core.default.countryCode", 1);


    protected static final CloseableHttpClient client;

    static {
        LOG.info("Initiating Cyberbank Core Connector pool against {}, with {} connections per route and {} in total...", URL, MAX_CONNECTIONS_PER_ROUTE, MAX_TOTAL_CONNECTIONS);

        SSLContext sslContext = null;
        PoolingHttpClientConnectionManager poolingConnManager = null;
        try {
            sslContext = SSLContexts.custom()
                    .loadTrustMaterial(TrustSelfSignedStrategy.INSTANCE)
                    .build();
        } catch (NoSuchAlgorithmException | KeyManagementException | KeyStoreException e) {
            LOG.error("Error creating SSLContext", e);
        }

        if(sslContext != null){
            Registry<ConnectionSocketFactory> registry = RegistryBuilder.<ConnectionSocketFactory> create()
                    .register("http", PlainConnectionSocketFactory.INSTANCE)
                    .register("https", new SSLConnectionSocketFactory(sslContext, NoopHostnameVerifier.INSTANCE))
                    .build();
            poolingConnManager = new PoolingHttpClientConnectionManager(registry);
        }else{
            poolingConnManager = new PoolingHttpClientConnectionManager();
        }
        poolingConnManager.setMaxTotal(MAX_TOTAL_CONNECTIONS);
        poolingConnManager.setDefaultMaxPerRoute(MAX_CONNECTIONS_PER_ROUTE);

        client = HttpClients.custom()
                .setConnectionManager(poolingConnManager)
                .disableAutomaticRetries()
                .disableConnectionState()
                .build();
    }

    /**
     * Attempt to return a formatted date from a date object
     *
     * @param date
     * @return String|null
     */
    public static String formatDate(Date date) {
        if (date == null) {
            return null;
        }
        return (new SimpleDateFormat(DATE_FORMAT)).format(date);
    }

    public static Date parseDate(String date) {
        try {
            if (StringUtils.isEmpty(date)) {
                return null;
            }
            return (new SimpleDateFormat(DATE_FORMAT)).parse(date);
        } catch (ParseException e) {
            return null;
        }
    }

    /**
     * Make a POST request against Cyberbank Core
     *
     * @param serviceName          Service name to execute
     * @param cyberbankCoreRequest Cyberbank Core request payload
     * @return JSONObject with Cyberbank Core response
     * @throws CyberbankCoreConnectorException In case a network error happened
     */

    protected static JSONObject call(String serviceName, CyberbankCoreRequest cyberbankCoreRequest) throws CyberbankCoreConnectorException {
        return call(serviceName, cyberbankCoreRequest.getJSON());
    }

    protected static JSONObject call(String serviceName, String  requestJson ) throws CyberbankCoreConnectorException {
        URI uri;
        HttpUriRequest request;

        try {
            uri = new URIBuilder(URL).build();
            request = new HttpPost(uri);
            request.setHeader(HttpHeaders.CONTENT_TYPE, ContentType.APPLICATION_JSON.toString());

            String cyberbankCoreRequestJSON = requestJson;
            StringEntity entity = new StringEntity(cyberbankCoreRequestJSON, ContentType.APPLICATION_JSON);
            LOG.info("{}: Request {}", serviceName, URL);

            if (LOG.isDebugEnabled()) {
                LOG.debug("{}: <==  {}", serviceName, cyberbankCoreRequestJSON);
            }
            ((HttpPost) request).setEntity(entity);
        } catch (Exception e) {
            throw new CyberbankCoreConnectorException(e);
        }

        long start = System.currentTimeMillis();
        try (CloseableHttpResponse httpResponse = client.execute(request)) {
            switch (httpResponse.getStatusLine().getStatusCode()) {
                case HttpServletResponse.SC_OK:
                    LOG.info("{}: Response [200] Ok. It took {} ms.", serviceName, System.currentTimeMillis() - start);

                    String body = EntityUtils.toString(httpResponse.getEntity(), StandardCharsets.UTF_8);
                    if (LOG.isDebugEnabled()) {
                        LOG.debug("{}: ==> {}", serviceName, body);
                    }

                    JSONObject result = new JSONObject(body);

                    return result;
                default:
                    LOG.error("{}: Response [{}] {}. It took {} ms.", serviceName, httpResponse.getStatusLine().getStatusCode(), httpResponse.getStatusLine().getReasonPhrase(), System.currentTimeMillis() - start);
                    throw new CyberbankCoreConnectorException(httpResponse.getStatusLine().getStatusCode() + ": " + httpResponse.getStatusLine().getReasonPhrase());

            }
        } catch (IOException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    protected static boolean callHasError(JSONObject response) {
        return "error".equals(response.getString("responseCode"));
    }

    protected static CyberbankCoreConnectorResponse processErrors(String serviceName, JSONObject serviceResponse, CyberbankCoreConnectorResponse response) {
        LOG.error("{}: Response with error", serviceName);
        if (LOG.isDebugEnabled()) {
            LOG.debug("{}: ==> {}", serviceName, serviceResponse);
        }

        response.setCode(-1);
        response.setError(new CyberbankCoreError(serviceResponse));
        return response;
    }

}
