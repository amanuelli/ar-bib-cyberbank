/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.forms.preloaders;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.forms.FormPreloader;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author
 */
public class TestformPreloader implements FormPreloader{
    
     public interface OutParams {
        String NOMBRE = "nombre";
    }
    
    @Override
    public Map<String, Object> execute(Form form, Request request) {
        Map<String, Object> formData = new HashMap();
        formData.put(OutParams.NOMBRE, "Juan Perez");
        return formData;
    }
}