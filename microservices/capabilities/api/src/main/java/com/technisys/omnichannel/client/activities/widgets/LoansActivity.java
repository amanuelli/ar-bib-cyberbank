/*
 *  Copyright 2017 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.widgets;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreLoanConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 *
 */
public class LoansActivity extends Activity {

    public static final String ID = "widgets.loans";

    public interface InParams {
    }

    public interface OutParams {

        String LOANS = "loans";
    }

    @SuppressWarnings("unchecked")
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {

            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            List<String> idPermissions = new ArrayList<>();
            idPermissions.add(PRODUCT_READ_PERMISSION);

            List<String> productTypes = Arrays.asList(Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY);

            List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);
            List<Loan> coreProducts = CoreLoanConnectorOrchestrator.list(request.getIdTransaction(), environment.getClients(), productTypes);

            List<Loan> loans = new ArrayList<>();
            if (coreProducts != null && !coreProducts.isEmpty()) {
                for (Loan loan : coreProducts) {
                    for (Product environmentProduct : environmentProducts) {
                        if (loan.getIdProduct().equals(environmentProduct.getIdProduct())) {
                            String loannumber = StringUtils.defaultString(ProductUtils.getNumber(environmentProduct.getExtraInfo()));
                            String loanCurrency = ProductUtils.getCurrency(environmentProduct.getExtraInfo());
                            String loanLabel = StringUtils.leftPad(loannumber, 15) + " "
                                    + loan.getProductType() + " " + I18nFactory.getHandler().getMessage("core.currency.label." + loanCurrency, request.getLang());
                            loan.setProductAlias(environmentProduct.getProductAlias());
                            loan.setShortLabel(loanLabel);
                            loan.setIdProduct(environmentProduct.getIdProduct());
                            loan.setProductType(environmentProduct.getProductType());
                            loan.setPaidPercentage((loan.getPaidAmount() * 100) / loan.getTotalAmount());
                            loans.add(loan);
                        }
                    }
                }
            }
            response.putItem(OutParams.LOANS, loans);
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
