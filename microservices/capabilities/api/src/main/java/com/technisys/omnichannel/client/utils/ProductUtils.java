/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.SecurityUtils;

import java.util.Map;

/**
 * @author salva
 */
public class ProductUtils {

    private ProductUtils(){
        // Private class constructor added in order to prevent Java from generating an implicit public one.
        throw new IllegalStateException("Utility class");
    }

    // Format: productType|number|currency|backendID|ownerBackendID
    public static final String FIELD_PRODUCT_ID = "productID";
    public static final String FIELD_PRODUCT_TYPE = "type";
    public static final String FIELD_PRODUCT_CURRENCY = "currencyKey";
    public static final String FIELD_PRODUCT_NUMBER = "productKey";
    public static final String FIELD_CLIENT_ID = "clientID";
    public static final String FIELD_OWNER_NAME = "ownerName";

    public static String generateIdProduct(String extraInfo) {
        return SecurityUtils.hash(extraInfo);
    }

    public static String getOwnerID(String extraInfo) throws Exception {
        String[] values = extraInfo.split("\\|");

        if (values.length >= 5) {
            return values[4];
        } else {
            throw new Exception("Extra Info does not include the required info to get the owner name.");
        }
    }

    public static String getOwnerName(String transactionID, String extraInfo) throws Exception {
        String[] values = extraInfo.split("\\|");
        String result;
        if (values.length >= 5) {
            Environment env = Administration.getInstance().readEnvironmentByProductGroupId(values[4]);
            if (env != null) {
                result = env.getName();
            } else {
                result = RubiconCoreConnectorC.readClient(transactionID, values[4]).getAccountName();
            }
        } else {
            throw new Exception("Extra Info does not include the required info to get the owner id.");
        }

        return result;
    }

    public static String getNumber(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        String productType = values[0];

        switch (productType) {
            case Constants.PRODUCT_CA_KEY:
            case Constants.PRODUCT_CC_KEY:
            case Constants.PRODUCT_PF_KEY:
            case Constants.PRODUCT_PA_KEY:
            case Constants.PRODUCT_PI_KEY:
            case Constants.PRODUCT_TC_KEY:
                return values[1];
            default:
                return null;
        }
    }

    public static String getCurrency(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        String productType = values[0];

        switch (productType) {
            case Constants.PRODUCT_CA_KEY:
            case Constants.PRODUCT_CC_KEY:
            case Constants.PRODUCT_PF_KEY:
            case Constants.PRODUCT_PA_KEY:
            case Constants.PRODUCT_PI_KEY:
                return values[2];
            case Constants.PRODUCT_TC_KEY:
            default:
                return null;
        }
    }

    public static String getProductType(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        String productType = values[0];

        return productType;
    }

    public static String getBackendID(String extraInfo) {
        String[] values = extraInfo.split("\\|");
        String productType = values[3];

        return productType;
    }

    /**
     * Acorta el n&uacute;mero de tarjeta generando una etiqueta que contiene
     * simplemente la marca y los &uacute;ltimos cuatro d&iacute;gitos. Por
     * ejemplo &quot;4012888888881881&quot; se transforma en &quot;VISA
     * 1881&quot;.
     * Si no reconoce la marca se obtienen solo los &uacute;ltimos cuatro
     * d&iacute;gitos.
     *
     * @param number N&uacute;mero de la tarjeta
     * @param lang   Lenguaje
     * @return Etiqueta corta del n&uacute;mero
     */
    public static String shortenCreditCardNumber(String number, String lang) {
        StringBuilder result = new StringBuilder();

        if (org.apache.commons.lang.StringUtils.isNotEmpty(number) && number.length() >= 4) {
            if (number.startsWith("4")) {
                result.append(I18nFactory.getHandler().getMessage("productType.VISA", lang))
                        .append(" ");
            } else if (number.startsWith("5")) {
                result.append(I18nFactory.getHandler().getMessage("productType.MASTER", lang))
                        .append(" ");
            } else if (number.startsWith("3")) {
                result.append(I18nFactory.getHandler().getMessage("productType.AMEX", lang))
                        .append(" ");
            }
            result.append(number.substring(number.length() - 4, number.length()));
        }

        return result.toString();
    }

    public static String getCreditCardBrand(String number, String lang) {
        String shorten = shortenCreditCardNumber(number, lang);
        return shorten.split(" ")[0];
    }

    /**
     * Mapea un producto del conector a uno de omnichannel
     *
     * @param fieldsMap Mapa con los campos de los productos
     * @return Producto de omnichannel conteniendo los datos básicos para poder
     * persistirlo en base
     */
    public static String generateExtraInfo(Map<String, String> fieldsMap) {
        String extraInfo;

        if (Constants.PRODUCT_TC_KEY.equals(fieldsMap.get(FIELD_PRODUCT_TYPE))) {
            extraInfo = fieldsMap.get(FIELD_PRODUCT_TYPE) + "|" + fieldsMap.get(FIELD_PRODUCT_NUMBER) + "|" + fieldsMap.get(FIELD_CLIENT_ID) + "|" + fieldsMap.get(FIELD_PRODUCT_ID);
        } else {
            extraInfo = fieldsMap.get(FIELD_PRODUCT_TYPE) + "|" + fieldsMap.get(FIELD_PRODUCT_NUMBER) + "|" + fieldsMap.get(FIELD_PRODUCT_CURRENCY) + "|" + fieldsMap.get(FIELD_PRODUCT_ID);
        }

        return extraInfo;
    }

}
