/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;

/**
 *
 */
@ExchangeActivity @DocumentedActivity("Enrollment associate pre")
public class AssociatePreActivity extends Activity {

    public static final String ID = "enrollment.associate.pre";

    private static final String NEXT_ACTIVITY_IN_FLOW = AssociateVerifyStep1Activity.ID;

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Invitation code text", optional = true)
        String CODE = "_code";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Exchange token")
        String EXCHANGE_TOKEN = "_exchangeToken";
        @DocumentedParam(type = String.class, description = "Invitation's object details")
        String INVITATION = "invitation";
        @DocumentedParam(type = Client.class, description = "Client's object details")
        String CLIENT = "client";
        @DocumentedParam(type = Account.class, description = "Account's object details")
        String ACCOUNT = "account";
        @DocumentedParam(type = String.class, description = "Second factor type")
        String SECOND_FACTOR_AUTH = "secondFactorAuth";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            InvitationCode invitation = getInvitationCode(request);

            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber());
            if (user == null) {
                throw new ActivityException(ReturnCodes.INVALID_INVITATION_CODE, "No es posible realizar una asociación de ambiente si el usuario no existe en omnichannel. Code: " + invitation.getInvitationCode());
            }

            String accountName;
            Environment environment;
            ClientEnvironment clientEnv = new ClientEnvironment();
            if(invitation.getProductGroupId() != null) {
                clientEnv = RubiconCoreConnectorC.readClient(request.getIdTransaction(), invitation.getProductGroupId());
                accountName = clientEnv.getAccountName();
            } else {
                environment = Administration.getInstance().readEnvironment(invitation.getIdEnvironment());
                accountName = environment.getName();
            }

            clientEnv.setAccountName(accountName);
            // Actualizo el exchange token
            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);

            List<String> activities = new ArrayList<>(Arrays.asList(StringUtils.split(token.getActivities(), ',')));
            if (!activities.contains(NEXT_ACTIVITY_IN_FLOW)) {
                activities.add(NEXT_ACTIVITY_IN_FLOW);
            }

            ExchangeTokenHandler.updateActivities(exchangeToken, activities.toArray(new String[0]));

            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            response.putItem(OutParams.INVITATION, invitation);
            response.putItem(OutParams.CLIENT, user);
            response.putItem(OutParams.ACCOUNT, clientEnv);
            response.putItem(OutParams.SECOND_FACTOR_AUTH, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "auth.login.credentialRequested"));

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException e) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, e);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}
