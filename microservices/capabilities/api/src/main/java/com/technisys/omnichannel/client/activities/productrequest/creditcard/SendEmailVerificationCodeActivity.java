/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.client.activities.productrequest.creditcard;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.users.InviteUsersActivity;
import com.technisys.omnichannel.client.activities.productrequest.onboarding.Step5Activity;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.domain.ValidationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.JsonUtils;
import com.technisys.omnichannel.core.validationcodes.ValidationCodesHandler;
import org.apache.commons.lang.StringUtils;

import javax.mail.MessagingException;
import java.io.IOException;
import java.io.Serializable;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author cherta
 */
@DocumentedActivity("Sends a verification code to an email")
@AnonymousActivity
public class SendEmailVerificationCodeActivity extends Activity {

    public static final String ID = "productrequest.creditcard.sendVerificationCode";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Email to be verified")
        String EMAIL = "email";

        @DocumentedParam(type = String.class, description = "Username from this user")
        String USERNAME = "user_name";

        @DocumentedParam(type = String.class, description = "The credit card id")
        String CREDIT_CARD_ID = "creditCardId";

    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Message")
        String MESSAGE = "message";

        @DocumentedParam(type = String.class, description = "Exchange token")
        String EXCHANGE_TOKEN = "_exchangeToken";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            Response response = new Response(request);

            ValidationCode validationCode = new ValidationCode();
            validationCode.setCreationDate(new Date());
            validationCode.setIdUser(request.getParam(InParams.EMAIL, String.class));
            validationCode.setType(ValidationCode.TYPE_CREDITCARDREQUEST);
            validationCode.setStatus(ValidationCode.STATUS_AVAILABLE);
            validationCode.setExtraInfo(request.getParam(InParams.CREDIT_CARD_ID, String.class));

            String idValidationCode = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
            ValidationCodesHandler.createValidationCode(validationCode, idValidationCode);

            HashMap<String, String> fillers = new HashMap<>();
            fillers.put("EMAIL", validationCode.getIdUser());
            response.putItem(OutParams.MESSAGE, I18nFactory.getHandler().getMessage("productrequest.creditcard.sendVerificationCode.success", request.getLang(), fillers));

            String exchangeToken = ExchangeTokenHandler.create(
                    request.getChannel(),
                    request.getLang(),
                    User.USER_ANONYMOUS,
                    -1,
                    request.getUserAgent(),
                    request.getClientIP(),
                    new String[]{VerifyEmailActivity.ID, PreVerifyEmailActivity.ID}
            );

            Onboarding onboarding = new Onboarding();
            onboarding.setEmail(request.getParam(InParams.EMAIL, String.class));

            Map<String, Object> extraInfo = new HashMap();
            extraInfo.put("creditCardId", request.getParam(InParams.CREDIT_CARD_ID, String.class));
            onboarding.setExtraInfo(JsonUtils.toJson(extraInfo, false, false));
            onboarding.setType(Onboarding.ONBOARDING_CREDITCARD);

            long idOnboarding = OnboardingHandlerFactory.getInstance().create(onboarding);

            ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);
            HashMap<String, Serializable> data = new HashMap<>();
            data.put("email", request.getParam(InParams.EMAIL, String.class));
            data.put("username", request.getParam(InParams.USERNAME, String.class));
            data.put("creditCardId", request.getParam(InParams.CREDIT_CARD_ID, String.class));
            data.put("idOnboarding", idOnboarding);
            token.setData(data);

            ExchangeTokenHandler.updateData(exchangeToken, token.getData());
            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);

            sendEmail(validationCode, idValidationCode, exchangeToken, request.getLang());

            response.setReturnCode(ReturnCodes.OK);
            return response;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (MessagingException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        }
    }

    private void sendEmail(ValidationCode validationCode, String validationCodePlain, String exchangeToken, String lang) throws IOException, MessagingException {
        HashMap<String, String> fillers = new HashMap<>();

        fillers.put("BASE_URL", ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, Constants.BASE_URL_KEY));
        fillers.put("INVITATION_CODE", validationCodePlain);
        fillers.put("USERNAME", validationCode.getExtraInfo());
        fillers.put("EXCHANGE_TOKEN", exchangeToken);
        String subject = I18nFactory.getHandler().getMessage("productrequest.creditcard.sendVerificationCode.subject", lang, fillers);
        String body = I18nFactory.getHandler().getMessage("productrequest.creditcard.sendVerificationCode.body", lang, fillers);

        NotificationsHandlerFactory.getHandler().sendAsyncEmail(subject, body, Arrays.asList(validationCode.getIdUser()), null, lang, true);
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String requiredString = "global.activity.required.param";
        if(StringUtils.isBlank(request.getParam(InParams.USERNAME, String.class))) {
            result.put(InParams.USERNAME, requiredString);
        }
        if(StringUtils.isBlank(request.getParam(InParams.EMAIL, String.class))) {
            result.put(InParams.EMAIL, requiredString);
        } else {
            if (!ValidationUtils.validateEmailPattern(request.getParam(InParams.EMAIL, String.class))) {
                result.put(InviteUsersActivity.InParams.EMAIL, "creditCardRequest.emailForm.email.invalid");
            }
        }
        if(StringUtils.isBlank(request.getParam(InParams.CREDIT_CARD_ID, String.class))) {
            result.put(InParams.CREDIT_CARD_ID, requiredString);
        } else {
            if(!Step5Activity.getCreditCards().containsKey(request.getParam(InParams.CREDIT_CARD_ID, String.class))) {
                result.put(InParams.CREDIT_CARD_ID, "creditCardRequest.emailForm.creditCardId.invalid");
            }
        }
        return result;
    }

}
