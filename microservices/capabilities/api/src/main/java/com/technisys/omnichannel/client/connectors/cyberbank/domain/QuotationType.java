/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain;

import java.util.Objects;

public class QuotationType {

    private Integer idQuotation;
    private Integer idQuotationType;
    private String quotationDesc;


    public QuotationType(Integer idQuotation, Integer idQuotationType, String quotationDesc) {
        this.idQuotation = idQuotation;
        this.idQuotationType = idQuotationType;
        this.quotationDesc = quotationDesc;
    }


    public Integer getIdQuotation() {
        return idQuotation;
    }

    public void setIdQuotation(Integer idQuotation) {
        this.idQuotation = idQuotation;
    }

    public Integer getIdQuotationType() {
        return idQuotationType;
    }

    public void setIdQuotationType(Integer idQuotationType) {
        this.idQuotationType = idQuotationType;
    }

    public String getQuotationDesc() {
        return quotationDesc;
    }

    public void setQuotationDesc(String quotationDesc) {
        this.quotationDesc = quotationDesc;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        QuotationType that = (QuotationType) o;
        return Objects.equals(idQuotation, that.idQuotation) &&
                Objects.equals(idQuotationType, that.idQuotationType) &&
                Objects.equals(quotationDesc, that.quotationDesc);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idQuotation, idQuotationType, quotationDesc);
    }

    @Override
    public String toString() {
        return "QuotationType{" +
                "idQuotation=" + idQuotation +
                ", idQuotationType=" + idQuotationType +
                ", quotationDesc='" + quotationDesc + '\'' +
                '}';
    }
}
