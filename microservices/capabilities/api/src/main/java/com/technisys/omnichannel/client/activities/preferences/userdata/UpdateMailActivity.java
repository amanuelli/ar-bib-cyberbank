/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.userdata;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.ValidationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.validationcodes.ValidationCodesHandler;
import org.apache.commons.lang.StringUtils;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.util.Date;
import java.util.Map;

/**
 * Validate that the validation code sent by mail is correct. Then modify the user email.
 */
@DocumentedActivity("Validate that the validation code sent by mail is correct. Then modify the user email.")
public class UpdateMailActivity extends Activity {

    public static final String ID = "preferences.userData.mail.update";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Validation code sent to the user by mail. e.g. &quot;BB75-9816-3A88&quot;")
        String MAIL_CODE = "mailCode";
        
        // Used by frontend, not this activity
        @DocumentedParam(type = String.class, description = "idForm that makes the modification of the user data, always send &quot;<code style=color:red;>modifyUserData</code>&quot;")
        String ID_FORM = "idForm";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "New user email address")
        String MAIL = "mail";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String mailCode = com.technisys.omnichannel.client.utils.StringUtils
                    .normalizeInvitationCode(request.getParam(InParams.MAIL_CODE, String.class));
            ValidationCode mailValidationCode = ValidationCodesHandler.readValidationCode(mailCode);
            String extraInfo = mailValidationCode.getExtraInfo();

            AccessManagementHandlerFactory.getHandler().updateUserEmail(request.getIdUser(), extraInfo);
            //en una implantacion real, seguramente sea mejor definir un trigger
            AccessManagementHandlerFactory.getHandler().updateUsername(request.getIdUser(), extraInfo);

            ValidationCodesHandler.changeValidationCodeStatus(mailValidationCode.getId(), ValidationCode.STATUS_USED);

            response.putItem(OutParams.MAIL, extraInfo);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            Map<String, String> result = FormsHandler.getInstance().validateRequest(request);

            String mailCode = com.technisys.omnichannel.client.utils.StringUtils
                    .normalizeInvitationCode(request.getParam(InParams.MAIL_CODE, String.class));

            if (StringUtils.isBlank(mailCode)) {
                result.put(InParams.MAIL_CODE, "userInfo.preferences.userData.mailCode.invalid");
                return result;
            }

            ValidationCode validationCode = ValidationCodesHandler.readValidationCode(mailCode);
            if (validationCode == null) {
                result.put(InParams.MAIL_CODE, "userInfo.preferences.userData.mailCode.invalid");
            } else if (!ValidationCode.TYPE_CHANGEEMAIL.equals(validationCode.getType())) {
                result.put(InParams.MAIL_CODE, "userInfo.preferences.userData.mailCode.invalid");
            } else if (!ValidationCode.STATUS_AVAILABLE.equals(validationCode.getStatus())) {
                result.put(InParams.MAIL_CODE, "userInfo.preferences.userData.mailCode.used");
            } else {
                long validityTime = ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "changeEmail.mail.validity");
                long currentTime = (new Date()).getTime();
                long creationTime = validationCode.getCreationDate().getTime();
                if (creationTime + validityTime < currentTime) {
                    result.put(InParams.MAIL_CODE, "userInfo.preferences.userData.mailCode.expired");
                }
            }

            if (validationCode != null
                && (AccessManagementHandlerFactory.getHandler().usernameExists(validationCode.getExtraInfo()))) {
                    result.put(InParams.MAIL_CODE, "userInfo.preferences.userData.mailCode.userNameExists");
            }


            return result;
        } catch (SchedulerException ex) {
            throw new ActivityException(ReturnCodes.SCHEDULER_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

}
