/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.SecuritySealUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 *
 */
@DocumentedActivity("List security seals")
public class ListSecuritySealsActivity extends Activity {

    public static final String ID = "enrollment.securitySeals.list";
    private static final String NEXT_ACTIVITY_IN_FLOW = WizardFinishActivity.ID;

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Exchange token")
        String EXCHANGE_TOKEN = "_exchangeToken";
        @DocumentedParam(type = String[].class, description = "Base64 encoded seals's list")
        String SECURITY_SEALS = "_securitySeals";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            response.putItem(OutParams.SECURITY_SEALS, SecuritySealUtils.getSealList());

            // Actualizo el exchange token
            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);

            List<String> activities = new ArrayList<>(Arrays.asList(StringUtils.split(token.getActivities(), ',')));
            if (!activities.contains(NEXT_ACTIVITY_IN_FLOW)) {
                activities.add(NEXT_ACTIVITY_IN_FLOW);
            }

            ExchangeTokenHandler.updateActivities(exchangeToken, activities.toArray(new String[0]));

            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
}
