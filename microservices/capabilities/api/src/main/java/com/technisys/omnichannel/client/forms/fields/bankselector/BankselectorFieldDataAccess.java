/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.forms.fields.bankselector;

import com.technisys.omnichannel.client.domain.fields.BankselectorField;
import org.apache.ibatis.session.SqlSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author ?
 */
class BankselectorFieldDataAccess {

    private static BankselectorFieldDataAccess instance;

    private BankselectorFieldDataAccess() {
    }

    /**
     * Genera instancia singleton
     *
     * @return Instancia unica de la clase
     */
    public static synchronized BankselectorFieldDataAccess getInstance() {
        if (instance == null) {
            instance = new BankselectorFieldDataAccess();
        }

        return instance;
    }

    /**
     * Crea un campo de texto en base
     *
     * @param session Sesion sql
     * @param field Campo a crear
     */
    public void createField(SqlSession session, BankselectorField field) {
        session.insert("com.technisys.omnichannel.forms.fields.bankselector.createField", field);

        //inserto las monedas
        if (field.getCodeTypeList() != null) {
            Map params = new HashMap();
            params.put("idForm", field.getIdForm());
            params.put("idField", field.getIdField());
            for (String codeType : field.getCodeTypeList()) {
                params.put("codeType", codeType);
                session.insert("com.technisys.omnichannel.forms.fields.bankselector.addCodeType", params);
            }
        }
    }

    /**
     * Lee los datos especificos de un campo
     *
     * @param session Sesion sql
     * @param idForm Identificador de formulario
     * @param formVersion Versión del formulario
     * @param idField Identificador de campo
     * @return Datos especificos del campo
     * @throws IOException Error de comunicacion
     */
    public BankselectorField readField(SqlSession session, String idForm, Integer formVersion, String idField) throws IOException {
        Map map = new HashMap();
        map.put("idField", idField);
        map.put("idForm", idForm);
        map.put("formVersion", formVersion);

        BankselectorField field = (BankselectorField) session.selectOne("com.technisys.omnichannel.forms.fields.bankselector.readField", map);
        return field;
    }

    /**
     * Borra los datos de un campo
     *
     * @param session Sesion transaccional de base de datos
     * @param idForm Identificador de formulario
     * @param idField Identificador del campo
     */
    public void deleteField(SqlSession session, String idForm, String idField) {
        Map<String, Object> parameters = new HashMap<>();
        parameters.put("idField", idField);
        parameters.put("idForm", idForm);
        session.insert("com.technisys.omnichannel.forms.fields.bankselector.deleteField", parameters);
    }

}
