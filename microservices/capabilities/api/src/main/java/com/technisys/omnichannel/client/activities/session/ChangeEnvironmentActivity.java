/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.session.oauth.OauthSelectEnvironmentActivity;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.utils.CalendarRestrictionsUtils;
import com.technisys.omnichannel.client.utils.MaskUtils;
import com.technisys.omnichannel.client.utils.PermissionsUtils;
import com.technisys.omnichannel.client.utils.SynchronizationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.SessionException;
import com.technisys.omnichannel.core.generalconditions.GeneralConditions;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.preprocessors.ipauthorization.IPAuthorizationHandler;
import com.technisys.omnichannel.core.session.SessionHandlerFactory;
import com.technisys.omnichannel.core.session.SessionUtils;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
/**
 *
 */
@DocumentedActivity("Change User Environment")
public class ChangeEnvironmentActivity extends Activity {

    public static final String ID = "session.changeEnvironment";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Identifier of environment you want to work with")
        String ID_ENVIRONMENT_TO_CHANGE = "idEnvironmentToChange";
        @DocumentedParam(type = Boolean.class, description = "If you want to set this as default")
        String SET_AS_DEFAULT = "setAsDefault";
    }

    public interface OutParams {
        @DocumentedParam(type = Integer.class, description = "User's active environment identifier")
        String ACTIVE_ENVIRONMENT_ID = "activeIdEnvironment";
        @DocumentedParam(type = String.class, description = "User's active environment name")
        String ACTIVE_ENVIRONMENT_NAME = "activeEnvironmentName";
        @DocumentedParam(type = String.class, description = "User's active environment administration scheme")
        String ADMINISTRATION_SCHEME = "administrationScheme";
        @DocumentedParam(type = Map.class, description = "Map of available environments: &lt;identifier, name&gt;")
        String ENVIRONMENTS = "environments";
        @DocumentedParam(type = Map.class, description = "Map of available permissions: &lt;identifier, boolean indicator&gt;")
        String PERMISIONS = "permissions";
        @DocumentedParam(type = List.class, description = "List of forms, those the user can interact with")
        String FORMS = "forms";
        @DocumentedParam(type = String.class, description = "User's active environment type")
        String ACTIVE_ENVIRONMENT_TYPE = "activeEnvironmentType";
        @DocumentedParam(type = List.class, description = "List of activities identifiers, those the user most use")
        String FREQUENT_ACTIONS = "frequentActions";
        @DocumentedParam(type = String.class, description = "Tells if the user can administrate the environment")
        String IS_ADMINISTRATOR = "isAdministrator";
        @DocumentedParam(type = List.class, description = "Clients of the environment")
        String CLIENTS = "clients";
        @DocumentedParam(type = Date.class, description = "Date of last signed T&Cs")
        String SIGNED_TERMS_AND_CONDITIONS_DATE = "signedTAndCDate";
        @DocumentedParam(type = Date.class, description = "Date when the user did the IRS statement")
        String IRS_DATE = "irsDate";
        @DocumentedParam(type = Date.class, description = "Specify if the PEP data is completed by the user")
        String PEP_COMPLETED = "pepCompleted";
        @DocumentedParam(type = Date.class, description = "Specify if the IRS data is completed by the user")
        String IRS_COMPLETED = "irsCompleted";
        @DocumentedParam(type = Date.class, description = "Return the ssnid code")
        String SSNID = "ssnid";

        @DocumentedParam(type = Boolean.class, description = "Enabled channels")
        String ENABLED_ASSISTANT = "enabledAssistant";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            int idEnvironmentToChange = request.getParam(InParams.ID_ENVIRONMENT_TO_CHANGE, Integer.class);

            int actualCondition = GeneralConditions.getInstance().actualCondition();
            EnvironmentUser envUser = Administration.getInstance().readEnvironmentUserInfo(request.getIdUser(), idEnvironmentToChange);
            boolean enabledAssistant = (envUser != null && envUser.getEnabledChannels().contains("assistant"));

            if (envUser.getIdCondition() != actualCondition) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
            List<Environment> enviornmentList = Administration.getInstance().listEnvironmentsWhereUserIsActive(request.getIdUser(), request.getChannel());
            Environment env = null;

            boolean calendarIsValid;
            boolean ipsAreValid;
            for (Environment environment : enviornmentList) {
                if (idEnvironmentToChange == environment.getIdEnvironment()) {
                    env = environment;
                }
                calendarIsValid = CalendarRestrictionsUtils.validateAccessByEnvironmentAndUser(environment.getIdEnvironment(),request.getIdUser(), request.getValueDate());
                ipsAreValid = IPAuthorizationHandler.getInstance().checkUserEnvironmentAccess(environment.getIdEnvironment(), request.getIdUser(), request.getClientIP());
                environment.setAllowToAccess(calendarIsValid && ipsAreValid);
            }

            Map<Integer, Map<String, String>> environments = SessionUtils.assembleEnviromentMap(enviornmentList);

            if (env == null) {
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            Environment environment = Administration.getInstance().readEnvironment(idEnvironmentToChange);

            // Sincronizar los productos del ambiente
            SynchronizationUtils.syncEnvironmentProducts(request.getIdTransaction(), environment, env.getLastSynchronization());

            // Actualizo los datos de ambiente y lenguaje de la sesión
            String accessToken = request.getCredential(Credential.ACCESS_TOKEN_CREDENTIAL).getValue();
            SessionHandlerFactory.getInstance().updateSession(accessToken, idEnvironmentToChange, request.getLang(), request.getChannel());

            // Si es necesario marcamos el ambiente como el por defecto del usuario
            boolean setAsDefault = request.getParameters().containsKey(InParams.SET_AS_DEFAULT) ? request.getParam(InParams.SET_AS_DEFAULT, Boolean.class) : false;
            if (setAsDefault) {
                AccessManagementHandlerFactory.getHandler().updateUserDefaultEnvironment(request.getIdUser(), idEnvironmentToChange);
            }

            boolean isAdministrator = Authorization.hasPermission(request.getIdUser(), idEnvironmentToChange, null, Constants.ADMINISTRATION_VIEW_PERMISSION);

            response.setReturnCode(ReturnCodes.OK);
            response.putItem(OutParams.ACTIVE_ENVIRONMENT_ID, env.getIdEnvironment());
            response.putItem(OutParams.ACTIVE_ENVIRONMENT_NAME, env.getName());
            response.putItem(OutParams.ACTIVE_ENVIRONMENT_TYPE, env.getEnvironmentType());
            response.putItem(OutParams.ADMINISTRATION_SCHEME, env.getAdministrationScheme());
            response.putItem(OutParams.ENVIRONMENTS, environments);
            response.putItem(OutParams.IS_ADMINISTRATOR, isAdministrator);
            List<String> userPermissions = AccessManagementHandlerFactory.getHandler().listPermissions(request.getIdUser(), idEnvironmentToChange);
            response.putItem(OutParams.PERMISIONS, PermissionsUtils.getPermissions(request.getIdUser(), idEnvironmentToChange));
            response.putItem(OutParams.FORMS, PermissionsUtils.getFormList(userPermissions, request.getLang()));
            response.putItem(OutParams.FREQUENT_ACTIONS, SynchronizationUtils.loadFrequentActions(request.getIdUser(), idEnvironmentToChange, env.getEnvironmentType()));
            response.putItem(OutParams.CLIENTS, environment.getClients());
            response.putItem(OutParams.ENABLED_ASSISTANT, enabledAssistant);

            //Condiciones generales
            GeneralConditions gcHandler = GeneralConditions.getInstance();

            //Gets last T&C sign date
            response.putItem(OauthSelectEnvironmentActivity.OutParams.SIGNED_TERMS_AND_CONDITIONS_DATE,
                    gcHandler.
                            getSignatureDate(gcHandler.actualCondition(),
                                    request.getIdUser(),
                                    idEnvironmentToChange));
            if(!envUser.isIrsValue()){
                response.putItem(OutParams.IRS_DATE,envUser.getIrsDate());
            }

            //Get PEP & IRS Completed Values
            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());
            if(user != null){
                response.putItem(OutParams.PEP_COMPLETED,user.isPepCompleted());
                response.putItem(OutParams.IRS_COMPLETED,user.isIrsCompleted());
                if(user.getSsnid()!=null)
                    response.putItem(OutParams.SSNID,MaskUtils.maskRange(user.getSsnid(),4,null));
            }
            return response;

        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (SessionException e) {
            throw new ActivityException(e.getReturnCode(), e);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        int idEnvironmentToChange = request.getParam(InParams.ID_ENVIRONMENT_TO_CHANGE, Integer.class);
        if (request.getParam(InParams.ID_ENVIRONMENT_TO_CHANGE, Integer.class) == request.getIdEnvironment()) {
            result.put(InParams.ID_ENVIRONMENT_TO_CHANGE, "environment.change.same");
        }else{
            if(!CalendarRestrictionsUtils.validateAccessByEnvironmentAndUser(idEnvironmentToChange,request.getIdUser(),request.getValueDate())){
                result.put("cantAccessByRestriction", "core.calendarRestrictions.NotAuthorized");
            }
        }
        return result;
    }
}
