package com.technisys.omnichannel.client.domain;

public class TransactionLinesTotals {
    private Double totalAmountQuantity;
    private Integer totalLines;
    private String totalAmountCurrency;

    public TransactionLinesTotals() { }

    public TransactionLinesTotals(Double totalAmountQuantity, Integer totalLines, String totalAmountCurrency) {
        this.totalAmountQuantity = totalAmountQuantity;
        this.totalLines = totalLines;
        this.totalAmountCurrency = totalAmountCurrency;
    }

    public Double getTotalAmountQuantity() {
        return totalAmountQuantity;
    }

    public void setTotalAmountQuantity(Double totalAmountQuantity) {
        this.totalAmountQuantity = totalAmountQuantity;
    }

    public Integer getTotalLines() {
        return totalLines;
    }

    public void setTotalLines(Integer totalLines) {
        this.totalLines = totalLines;
    }

    public String getTotalAmountCurrency() {
        return totalAmountCurrency;
    }

    public void setTotalAmountCurrency(String totalAmountCurrency) {
        this.totalAmountCurrency = totalAmountCurrency;
    }
}
