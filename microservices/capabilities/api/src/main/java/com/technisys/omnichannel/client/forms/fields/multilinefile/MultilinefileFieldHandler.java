package com.technisys.omnichannel.client.forms.fields.multilinefile;

import com.technisys.omnichannel.client.domain.fields.MultilinefileField;
import com.technisys.omnichannel.client.forms.fields.multilinefile.processors.ProcessorFactory;
import com.technisys.omnichannel.client.handlers.multiline.MultilineHandler;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.files.FilesHandler;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.forms.fields.FieldHandler;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.DBUtils;
import com.technisys.omnichannel.core.utils.JsonUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.util.*;

public class MultilinefileFieldHandler extends FieldHandler {

    private static final Logger log = LoggerFactory.getLogger(MultilinefileFieldHandler.class);

    protected static final String MAX_FILE_SIZE_INPUT = "maxFileSizeMB";
    protected static final String ACCEPTED_FILE_TYPES_INPUT = "acceptedFileTypes";
    protected static final String ALLOW_MULTIPLE_INPUT = "allowMultiple";
    protected static final String CONTROL_LIMITS = "controlLimits";
    protected static final String USE_FOR_TOTAL_AMOUNT = "useForTotalAmount";
    protected static final String CONTROL_LIMIT_OVER_PRODUCT = "controlLimitOverProduct";

    private static final List<String> ACCEPTED_STATUS = Arrays.asList(Transaction.STATUS_SCHEDULED, Transaction.STATUS_PENDING, Transaction.STATUS_RUNNING, Transaction.STATUS_ACCEPTED, Transaction.STATUS_PROCESSING, Transaction.STATUS_FINISHED, Transaction.STATUS_CANCELLED, Transaction.STATUS_RETURNED, Transaction.STATUS_FAILED, Transaction.STATUS_DRAFT);

    /**
     * Obtiene la descripcion corta de la data de una transaccion. Este metodo
     * sera utilizado en backoffice para el despliegue de la informacion breve
     * para una transaccion de creacion/modificacion de un campo de formulario
     *
     * @param map  Mapa con los parametros ingresados
     * @param lang Lenguaje para labels
     * @return String en formato html con la descripcion breve de la transaccion
     */
    @Override

    public String getShortDescription(Map<String, String> map, String lang) {
        //no agrego nada especifico de este tipo de campo
        return getBasicShortDescription(map, lang);
    }

    /**
     * Obtiene la descripcion completa de la data de una transaccion. Este
     * metodo sera utilizado en backoffice para el despliegue de la informacion
     * de una transaccion de creacion/modificacion de un campo de formulario
     *
     * @param map  Mapa con los parametros ingresados
     * @param lang Lenguaje para labels
     * @return String en formato html con la descripcion completa de la
     * transaccion
     */
    @Override
    public String getFullDescription(Map<String, String> map, String lang) {
        I18n i18n = I18nFactory.getHandler();

        StringBuilder strBuilder = new StringBuilder();

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.id", lang)).append(":</dt>").append("<dd>").append(StringUtils.isNotBlank(map.get("id")) ? map.get("id") : i18n.getMessage("backoffice.notApply", lang)).append("</dd>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.type", lang)).append(":</dt>").append("<dd>").append(i18n.getMessage("form.fieldType." + map.get("type"), lang)).append("</dd>");

        if (StringUtils.isNotBlank(map.get("note"))) {
            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.note", lang)).append(":</dt>").append("<dd>").append(map.get("note")).append("</dd>");
        }

        String visibilityText = FormsHandler.getInstance().getConditionMessage("visible", map, lang);

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.visibility", lang)).append(":</dt>").append("<dd>").append(visibilityText).append("</dd>");

        if (StringUtils.isNotBlank(map.get("maxFileSizeMB"))) {
            String maxFileSizeMB = map.get("maxFileSizeMB");
            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.multilinefile.maxFileSizeMB", lang)).append(":</dt> ").append("<dd>").append(maxFileSizeMB).append("</dd>");
        }

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.multilinefile.acceptedFileTypes", lang)).append(":</dt> ").append("<br />");

        String[] acceptedFileTypes = map.get("acceptedFileTypes").split("\\|");
        Arrays.stream(acceptedFileTypes).forEach(fileType ->
                strBuilder.append("<dt>").append("<dd>").append(fileType).append("</dd>").append("</dt> ")
        );

        String requiredText = FormsHandler.getInstance().getConditionMessage("required", map, lang);

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.mandatoryness", lang)).append(":</dt>").append("<dd>").append(requiredText).append("</dd>");

        return strBuilder.toString();
    }

    /**
     * Obtiene la informacion requerida para el despliegue de configuracion en
     * backoffice
     *
     * @param idForm  Identificador de formulario
     * @param idField Identificador del campo
     * @param lang    Idioma del usuario
     * @return Mapa con la informacion requerida para configuracion (listados,
     * parametros, etc)
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, Serializable> getFieldDataForConfiguration(String idForm, String idField, String lang) throws IOException {
        Map<String, Serializable> data = new HashMap<>();
        //tipos de archivos soportados por el sistema.
        data.put("fileTypesList", (Serializable)ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "form.multilinefile.acceptedFileTypes", String.class));

        //esta habilitado caps por producto
        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getBoolean(Configuration.LIMITS, "core.use_cap_environment_product");
        data.put("capsByProductEnabled", capsByProductEnabled);

        return data;
    }

    /**
     * Dado un Request y el campo especifico carga, de ser necesario, la
     * información de despliegue del mismo. Por ejemplo para el caso de un
     * selector de productos, este método debe retornar la lista de las opciones
     * de dicho selector (list de identificador de productos, su label, etc).
     *
     * @param field       Campo del formulario
     * @param request     Request del usuario para obtener el usuario, ambiente, etc
     * @param transaction Transaccion (opcional) que se esta queriendo desplegar
     *                    junto con el formulario,
     * @return Retorna un Mapa con la información necesaria para armar el
     * despliegue del campo. En caso de no requerilo retorna null.
     * @throws IOException Error de comunicaci&oacute;n
     */
    @Override
    public Map<String, Serializable> loadFormFieldData(FormField field, Request request, Transaction transaction) throws IOException, SchedulerException {
        Map<String, Serializable> data = super.loadFormFieldData(field, request, transaction);
        String logMessage;

        if (transaction == null) {
            List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);
            MultilinefileField psField = (MultilinefileField) field;
            //required
            if (psField.getRequiredErrorMap() != null) {
                languages.forEach(lang -> {
                    if (StringUtils.isBlank(psField.getRequiredErrorMap().get(lang))) {
                        psField.getRequiredErrorMap().put(lang, I18nFactory.getHandler().getMessage("fields.defaultForm.defaultField.requiredError", lang));
                    }
                });
            }

            String json = readSpecificData(field.getIdForm(), field.getFormVersion(), field.getIdField());
            return JsonUtils.fromJson(json, Map.class);
        }

        if (ACCEPTED_STATUS.contains(transaction.getIdTransactionStatus())) {
            List fileValues = RequestParamsUtils.getValue(transaction.getData().get(field.getIdField()), List.class);
            if (fileValues != null && !fileValues.isEmpty()) {
                int idFile = (Integer) ((Map) fileValues.get(0)).get("fileId");
                File file = FilesHandler.getInstance().readUnauthenticatedWithContents(idFile);

                if (file != null) {
                    Map fileData = ProcessorFactory.getProcessorSubType(field.getSubType()).validate(file.getContents(), request.getLang());
                    data.putAll(fileData);
                    data.put("transactionLinesStatus", (Serializable)MultilineHandler.getTransactionLinesStatus(transaction.getIdTransaction()));
                } else {
                    logMessage= MessageFormat.format("Missing file {0} in \"files\" table", idFile);
                    log.error(logMessage);
                }
            }
        }

        return data;
    }

    /**
     * Obtiene la descripcion a utilizar en el PDF de detalle del formulario.
     *
     * @param field Campo de formulario
     * @param lang  Lenguaje para labels
     * @return String en formato html con la descripcion completa del campo
     */
    @Override
    public String getPDFDescription(FormField field, String lang) {
        StringBuilder strBuilder = new StringBuilder();

        MultilinefileField multilineFileField = (MultilinefileField) field;

        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.multilinefile.maxFileSizeMB", lang)).append(":</em> ").append(multilineFileField.getMaxFileSizeMB()).append("<br/>");
        strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.multilinefile.acceptedFileTypes", lang)).append(":</em> ").append("<br />");

        List<String> acceptedFileTypes = multilineFileField.getAcceptedFileTypes();
        acceptedFileTypes.forEach(fileType ->
                strBuilder.append(" - ").append(fileType).append("<br />"));


        //textos internacionalizados
        List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);

        //requiredError
        if (multilineFileField.getRequiredErrorMap() != null) {
            for (String key : languages) {
                if (StringUtils.isNotBlank(multilineFileField.getRequiredErrorMap().get(key))) {
                    strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.multilinefile.requiredError", lang)).append(" (").append(key).append(")").append(":</em> ").append(multilineFileField.getRequiredErrorMap().get(key)).append("<br/>");
                }
            }
        }

        return strBuilder.toString();
    }

    /**
     * Valida los datos ingresados por el usuario y retorna los errores
     * encontrados
     *
     * @param map Mapa con los datos ingresados
     * @return Mapa con los identificadores de campos/keys de mensaje
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, String> validate(Map<String, String> map) throws IOException {
        Map<String, String> result = new HashMap<>();

        try {
            Integer.parseInt(map.get(MAX_FILE_SIZE_INPUT));
        } catch (NumberFormatException e) {
            result.put(MAX_FILE_SIZE_INPUT, "backoffice.forms.fields.multilinefile.maxFileSizeMB.required");
        }

        List<String> acceptedFileTypes = new LinkedList(Arrays.asList(StringUtils.defaultString(map.get(ACCEPTED_FILE_TYPES_INPUT)).split("\\|")));
        List<String> acceptedFileTypesConfigured = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "form.multilinefile.acceptedFileTypes", String.class);

        acceptedFileTypes.removeAll(acceptedFileTypesConfigured);
        if (!acceptedFileTypes.isEmpty()) {
            result.put(ACCEPTED_FILE_TYPES_INPUT, "backoffice.forms.fields.multilinefile.acceptedFileTypes.invalidValue");
        }

        return result;
    }

    /**
     * Valida los datos de un campo y retorna los errores encontrados
     *
     * @param field Campo a validar
     * @return Mapa con los identificadores de campos/keys de mensaje
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, String> validate(FormField field) throws IOException {
        return new HashMap<>();
    }

    /**
     * Persiste los datos de un campo.
     *
     * @param session Sesion transaccional de base de datos
     * @param map     Mapa con los datos del campo (mapeo obtenido desde web)
     * @throws IOException Error de comunicacion
     */
    @Override
    public void createFormField(SqlSession session, Map<String, String> map) throws IOException {
        Map<String, Object> specificData = new HashMap();

        specificData.put(MAX_FILE_SIZE_INPUT, Integer.parseInt(map.get(MAX_FILE_SIZE_INPUT)));
        specificData.put(ALLOW_MULTIPLE_INPUT, false);
        specificData.put(ACCEPTED_FILE_TYPES_INPUT, Arrays.asList(StringUtils.defaultString(map.get(ACCEPTED_FILE_TYPES_INPUT)).split("\\|")));

        String controlLimits = map.get("controlLimits");
        if (StringUtils.isNotBlank(controlLimits)) {
            specificData.put(CONTROL_LIMITS, Boolean.parseBoolean(controlLimits));
        }

        String useForTotalAmount = map.get("useForTotalAmount");
        if (StringUtils.isNotBlank(useForTotalAmount)) {
            specificData.put(USE_FOR_TOTAL_AMOUNT, Boolean.parseBoolean(useForTotalAmount));
        }

        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getDefaultBoolean(Configuration.LIMITS,  "core.use_cap_environment_product", false);
        String controlLimitOverProduct = map.get("controlLimitOverProduct");
        if (capsByProductEnabled && StringUtils.isNotBlank(controlLimitOverProduct)) {
            specificData.put(CONTROL_LIMIT_OVER_PRODUCT, controlLimitOverProduct);
        }


        createSpecificData(session, (String) map.get("idForm"), (String) map.get("id"), JsonUtils.toJson(specificData, false, false));
    }

    /**
     * Persiste los datos de un campo.
     *
     * @param session Sesion transaccional de base de datos
     * @param field   Cammpo a persistir
     * @throws IOException Error de comunicacion
     */
    @Override
    public void createFormField(SqlSession session, FormField field) throws IOException {

        MultilinefileField multilineFileField = (MultilinefileField) field;

        Map<String, Object> specificData = new HashMap();
        specificData.put(MAX_FILE_SIZE_INPUT, multilineFileField.getMaxFileSizeMB());
        specificData.put(ALLOW_MULTIPLE_INPUT, false);
        specificData.put(ACCEPTED_FILE_TYPES_INPUT, multilineFileField.getAcceptedFileTypes());
        specificData.put(CONTROL_LIMITS, multilineFileField.isValidateCap());
        specificData.put(USE_FOR_TOTAL_AMOUNT, multilineFileField.isUseForTotalAmount());
        specificData.put(CONTROL_LIMIT_OVER_PRODUCT, multilineFileField.getIdFieldProductToValidateCap());

        createSpecificData(session, field.getIdForm(), field.getIdField(), JsonUtils.toJson(specificData, false, false));
    }

    /**
     * Actualiza los datos de un campo
     *
     * @param session Sesion transaccional de base de datos
     * @param map     Mapa con los nuevos datos ingresados
     * @throws IOException Error de comunicacion
     */
    @Override
    public void updateFormField(SqlSession session, Map<String, String> map) throws IOException {
        Map<String, Object> specificData = new HashMap();
        specificData.put(MAX_FILE_SIZE_INPUT, Integer.parseInt(map.get(MAX_FILE_SIZE_INPUT)));
        specificData.put(ALLOW_MULTIPLE_INPUT, false);
        specificData.put(ACCEPTED_FILE_TYPES_INPUT, Arrays.asList(StringUtils.defaultString(map.get(ACCEPTED_FILE_TYPES_INPUT)).split("\\|")));

        String controlLimits = map.get("controlLimits");
        if (StringUtils.isNotBlank(controlLimits)) {
            specificData.put(CONTROL_LIMITS, Boolean.parseBoolean(controlLimits));
        }

        String useForTotalAmount = map.get("useForTotalAmount");
        if (StringUtils.isNotBlank(useForTotalAmount)) {
            specificData.put(USE_FOR_TOTAL_AMOUNT, Boolean.parseBoolean(useForTotalAmount));
        }

        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getDefaultBoolean(Configuration.LIMITS,  "core.use_cap_environment_product", false);
        String controlLimitOverProduct = map.get("controlLimitOverProduct");
        if (capsByProductEnabled && StringUtils.isNotBlank(controlLimitOverProduct)) {
            specificData.put(CONTROL_LIMIT_OVER_PRODUCT, controlLimitOverProduct);
        }

        String idForm = map.get("idForm");
        String oldIdField = map.get("oldId");
        String newIdField = map.get("id");
        String jsonData = JsonUtils.toJson(specificData, false, false);
        if (StringUtils.equals(oldIdField, newIdField)) {
            modifySpecificData(session, idForm, newIdField, jsonData);
        } else {
            createSpecificData(session, idForm, newIdField, jsonData);
        }
    }

    /**
     * Lee los datos de un campo
     *
     * @param idForm       Identificador de formulario
     * @param formVersion  Version del formulario
     * @param idField      Identificador de campo
     * @param withMessages indica si cargar mensajes, placeholders, etc
     * @return Datos especificos del campo
     * @throws IOException Error de comunicacion
     */
    @Override
    public FormField readFormField(String idForm, Integer formVersion, String idField, boolean withMessages) throws IOException {
        MultilinefileField multilineFile = new MultilinefileField();
        multilineFile.setIdForm(idForm);
        multilineFile.setFormVersion(formVersion);
        multilineFile.setIdField(idField);

        String json = readSpecificData(idForm, formVersion, idField);
        if (json != null) {
            Map<String, Object> specificData = JsonUtils.fromJson(json, Map.class);

            multilineFile.setMaxFileSizeMB((int) specificData.get(MAX_FILE_SIZE_INPUT));
            multilineFile.setAcceptedFileTypes((List) specificData.get(ACCEPTED_FILE_TYPES_INPUT));

            multilineFile.setIdFieldProductToValidateCap((String) specificData.get(CONTROL_LIMIT_OVER_PRODUCT));
            multilineFile.setValidateCap(specificData.get(CONTROL_LIMITS) != null && (Boolean) (specificData.get(CONTROL_LIMITS)));
            multilineFile.setUseForTotalAmount((Boolean) specificData.get(USE_FOR_TOTAL_AMOUNT));
        }

        if (withMessages) {

            FormMessagesHandler fmh = FormMessagesHandler.getInstance();

            try (SqlSession session = DBUtils.getInstance().openReadSession()) {
                //listo errores requerido
                multilineFile.setRequiredErrorMap(fmh.listFieldMessages(session, idField, idForm, multilineFile.getFormVersion(), "requiredError"));
            }
        }

        return multilineFile;
    }

    /**
     * Borra los datos de un campo
     *
     * @param session     Sesion transaccional de base de datos
     * @param idForm      Identificador de formulario
     * @param formVersion Versión de formulario
     * @param idField     Identificador del campo
     * @throws IOException Error de comunicacion
     */
    @Override
    public void deleteFormField(SqlSession session, String idForm, int formVersion, String idField) throws IOException {
        //No es necesario realizar acción dado que no posee tablas custom
        //El registro en form_field_data se borra con la cascada de la base
    }

    /**
     * Valida el valor ingresado para un campo y retorna los errores encontrados
     *
     * @param request Solicitud enviada
     * @param idField Identificador de campo a validar
     * @param fields  Mapa de campos &lt;id, campo&gt; del formulario
     * @return Mapa con los identificadores de campos/keys de mensaje
     * @throws IOException Error de comunicacion
     */
    @Override
    public Map<String, String> validateValue(Request request, String idField, Map<String, FormField> fields) throws IOException {
        Map errors = new HashMap();
        List<Map> files = request.getParam(idField, List.class);
        int idFile;
        if (files != null) {
            for (Map file : files) {
                try {
                    idFile = (Integer) file.get("fileId");
                    File f = FilesHandler.getInstance().readTransactionFile(idFile, request.getIdUser(), request.getIdEnvironment());
                    if (f == null || !StringUtils.equals(f.getFileName(), (String) file.get("fileName"))) {
                        errors.put(idField, "fields.defaultForm.defaultField.invalidError");
                    }
                } catch (IOException e) {
                    errors.put(idField, "fields.defaultForm.defaultField.invalidError");
                }
            }
        }
        return errors;
    }

    /**
     * Indica si el campo tiene un valor definido o esta vacio Este metodo
     * debiera sobrescribirse por cada implementacion en caso de que precise una
     * implementacion especifica
     *
     * @param field      Campo de formulario
     * @param fieldValue Valor del campo
     * @return Booleano indicando si el campo tiene valor definido
     */
    @Override
    public boolean isHasValue(FormField field, Object fieldValue) {
        if (fieldValue instanceof List) {
            return !((List) fieldValue).isEmpty();
        }
        return false;
    }
}