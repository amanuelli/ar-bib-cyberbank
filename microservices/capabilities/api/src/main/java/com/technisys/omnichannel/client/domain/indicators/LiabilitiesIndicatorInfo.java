/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain.indicators;

import com.technisys.omnichannel.core.domain.IndicatorInfo;

/**
 *
 * @author fpena
 */
public class LiabilitiesIndicatorInfo implements IndicatorInfo{
    private String operator;
    private double amount;
    
    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
