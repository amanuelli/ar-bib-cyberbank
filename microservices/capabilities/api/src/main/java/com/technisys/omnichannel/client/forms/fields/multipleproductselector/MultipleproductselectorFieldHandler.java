/*
 *  Copyright 2016 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.forms.fields.multipleproductselector;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.forms.fields.productselector.ProductselectorFieldHandler;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.domain.fields.MultipleproductselectorField;
import com.technisys.omnichannel.core.forms.fields.multipleproductselector.MultipleproductselectorBaseFieldHandler;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import org.apache.commons.lang3.StringUtils;
import org.quartz.SchedulerException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Serializable;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 *
 * @author ?
 * 
 */
public class MultipleproductselectorFieldHandler extends MultipleproductselectorBaseFieldHandler {

    private static final Logger log = LoggerFactory.getLogger(MultipleproductselectorFieldHandler.class);
    
    /**
     * Dado un Request y el campo espec&iacute;fico carga la lista de productos
     * para los cuales el usuario xxx
     *
     * @param field Campo del formulario
     * @param request Request del usuario para obtener el usuario, ambiente, etc
     * @param transaction Transaccion (opcional) que se esta queriendo desplegar
     * junto con el formulario,
     * @return Retorna un Mapa con la información necesaria para armar el
     * despliegue del campo.
     * @throws IOException Error de comunicaci&oacute;n
     */
    @Override
    public Map<String, Serializable> loadFormFieldData(FormField field, Request request, Transaction transaction) throws IOException, SchedulerException {
        //En base al parametro lastVersion se puede saber si es para modo vista o edicion. 
        //En caso de modo vista debe venir un atributo idTransaction para saber que data cargar

        try{
            Map<String, Serializable> data = super.loadFormFieldData(field, request, transaction);

            // Cargo de acuerdo al idioma los valores para mostrar los importes: separador de miles, decimales y la precisión
            char decimalSeparator = '.';
            char thousandsSeparator = ',';
            int precision = 2;

            try {
                DecimalFormatSymbols symbols = new DecimalFormatSymbols(Locale.forLanguageTag(request.getLang()));
                decimalSeparator = symbols.getDecimalSeparator();
                thousandsSeparator = symbols.getGroupingSeparator();
            } catch (Exception e) {
                log.warn("Error obteniendo separadores", e);
            }
            try {
                String mask = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.decimalFormat");
                DecimalFormat format = new DecimalFormat(mask);
                precision = format.getMaximumFractionDigits();
            } catch (Exception e) {
                log.warn("Error obteniendo precisión", e);
            }
            data.put("decimalSeparator", decimalSeparator);
            data.put("thousandsSeparator", thousandsSeparator);
            data.put("precision", precision);
            
            MultipleproductselectorField mpSelectorField = (MultipleproductselectorField) field;

            ProductselectorFieldHandler psfh = new ProductselectorFieldHandler();
            
            List<ProductselectorFieldHandler.Option> options = new ArrayList<>();

            if (transaction != null && !Transaction.STATUS_DRAFT.equals(transaction.getIdTransactionStatus())) {
                Map[] products = RequestParamsUtils.getValue(transaction.getData().get(field.getIdField()), Map[].class);
                
                if(products != null){
                    String idProduct;
                    for(Map product : products){
                        idProduct = (String)product.get("id");
                        if(StringUtils.isNotBlank(idProduct)){
                            ProductselectorFieldHandler.Option option = psfh.new Option();
                            option.setId(idProduct);
                            option.setLabel(CoreUtils.getProductLabeler(request.getLang()).calculateShortLabel(idProduct, request.getIdEnvironment()));
                            options.add(option);
                        }
                    }
                }
            } else {
                // Cargo las monedas especificadas en el campo
                Set<String> validCurrencies = new HashSet<>();
                validCurrencies.addAll(mpSelectorField.getCurrencyList());

                List<String> productTypes = mpSelectorField.getProductTypeList();
                
                List<Product> omnichannelProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), mpSelectorField.getPermissionList(), productTypes);

                // Cargo los productos desde el backend
                Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

                List<Account> backendProducts = CoreAccountConnectorOrchestrator.list(request.getIdTransaction(), environment.getClients(), productTypes);
                
                Map<String, Product> omnichannelProductsById = new HashMap<>();
                for(Product product : omnichannelProducts) {
                    // Si no hay monedas, simplemente no filtro los productos
                    if (validCurrencies.isEmpty()) {
                        omnichannelProductsById.put(product.getIdProduct(), product);
                    } else { // Si hay monedas...
                        String currency = ProductUtils.getCurrency(product.getExtraInfo());

                        // ... filtro el producto
                        if (validCurrencies.contains(currency)) {
                            omnichannelProductsById.put(product.getIdProduct(), product);
                        }
                    }
                }
                
                /**
                 * Recorro los productos llegados desde el backend y para
                 * cado uno creo una bean a devolver, si los mismos están
                 * listados en los productos autorizados de omnichannel para los
                 * permisos cargados en el tipo de campo.
                 *
                 * Para cada bean cargo: .- El id interno de omnichannel .- La
                 * etiqueta .- El saldo si el usuario tiene permiso de
                 * lectura .- La moneda
                 */
                for (Product backendProduct : backendProducts) {
                    Product authorizedProduct = omnichannelProductsById.get(backendProduct.getIdProduct());

                    if (authorizedProduct != null) {
                        ProductselectorFieldHandler.Option option = psfh.new Option();

                        // Id
                        option.setId(backendProduct.getIdProduct());

                        // Etiqueta
                        option.setLabel(CoreUtils.getProductLabeler(request.getLang()).calculateShortLabel(authorizedProduct));

                        // Saldo
                        String currency = ProductUtils.getCurrency(backendProduct.getExtraInfo());
                        if (Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), authorizedProduct.getIdProduct(), PRODUCT_READ_PERMISSION)) {
                            Double quantity;

                            if(backendProduct instanceof Account){
                                Account account = (Account)backendProduct;
                                quantity = account.getBalance();

                                option.setBalance(new Amount(currency, quantity));
                            }
                        }
                        
                        // Moneda
                        option.setCurrency(currency);
                        options.add(option);
                    }
                }
            }

            //ordeno las opciones por label
            Collections.sort(options);

            data.put("options", (Serializable) options);

            return data;                
        }catch(BackendConnectorException ex){
            throw new IOException(ex);
        }
    }  
}