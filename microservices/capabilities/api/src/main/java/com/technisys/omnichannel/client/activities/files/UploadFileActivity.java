/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.domain.fields.InputfileField;
import com.technisys.omnichannel.client.domain.fields.MultilinefileField;
import com.technisys.omnichannel.client.forms.fields.multilinefile.processors.Processor;
import com.technisys.omnichannel.client.forms.fields.multilinefile.processors.ProcessorFactory;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.files.FilesHandler;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.IOUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Upload a file
 */
@DocumentedActivity("Upload a file by Multipart post")
public class UploadFileActivity extends Activity {

    public static final String ID = "files.upload";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Form identifier related to the file", optional = true)
        String ID_FORM = "idForm";

        @DocumentedParam(type = String.class, description = "Form field identifier related to the file", optional = true)
        String ID_FORM_FIELD = "idFormField";

        @DocumentedParam(type = String.class, description = "Activity identifier related to the file")
        String ID_ACTIVITY = "idActivity";

        @DocumentedParam(type = String.class, description = "File description")
        String DESCRIPTION = "description";

        @DocumentedParam(type = InputStream.class, description = "Multipart file")
        String FILE = "_file";
    }

    public interface OutParams {

        @DocumentedParam(type = Transaction.class, description = "File identifier")
        String ID_FILE = "idFile";

        @DocumentedParam(type = String.class, description = "File identifier related to uploaded file")
        String ID_RELATED_FILE = "idRelatedFile";

        @DocumentedParam(type = String.class, description = "File name related to uploaded file")
        String NAME_RELATED_FILE = "nameRelatedFile";

        @DocumentedParam(type = Amount.class, description = "Total amount to pay")
        String TOTAL_AMOUNT = "totalAmount";

        @DocumentedParam(type = Integer.class, description = "Amount of invalid lines in the file")
        String INVALID_LINES = "invalidLines";

        @DocumentedParam(type = Integer.class, description = "Amount of valid lines in the file")
        String VALID_LINES = "validLines";

        @DocumentedParam(type = Byte[].class, description = "Content of invalid lines in the file")
        String INVALID_LINES_CONTENT = "invalidLinesContent";

        @DocumentedParam(type = Integer.class, description = "Amount of lines with amount 0")
        String LINES_WITH_NO_AMOUNT = "linesWithNoAmount";

    }

    /*
        Estos parametros no son enviados en el post, los injecta la API
    */
    public interface InternalParams {
        String FILE_SIZE = "fileSize";
        String FILE_NAME = "fileName";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {

            File file = new File();
            BufferedInputStream stream = request.getParam(InParams.FILE, BufferedInputStream.class);

            long fileSize = request.getParam(InternalParams.FILE_SIZE, long.class);
            String fileName = request.getParam(InternalParams.FILE_NAME, String.class);
            String description = request.getParam(InParams.DESCRIPTION, String.class);
            String idActivity = request.getParam(InParams.ID_ACTIVITY, String.class);

            String idForm = request.getParam(InParams.ID_FORM, String.class);
            String idFormField = request.getParam(InParams.ID_FORM_FIELD, String.class);

            if (stream != null && fileSize != 0) {
                file.setFileName(fileName);

                Processor processor = getProcessor(idForm, idFormField);
                byte[] content = IOUtils.toByteArray(stream);
                if (processor != null) {
                    file.setContents(processor.getValidContent(content));
                    Map<String, Object> processorResponse = processor.validate(content, request.getLang());
                    response.putAllItems(processorResponse);
                } else {
                    file.setContents(content);
                }
            }
            file.setFromBackoffice(false);
            file.setIdUser(request.getIdUser());
            file.setDescription(description);
            file.setIdActivity(idActivity);

            int fileId = FilesHandler.getInstance().create(file);
            response.putItem(OutParams.ID_FILE, fileId);

            Integer errorLines = (Integer) response.getData().get("invalidLines");
            if(errorLines != null && errorLines > 0){
                byte[] errorLinesContent = (byte[]) response.getData().get("invalidLinesContent");
                response.getData().remove("invalidLinesContent");
                String relatedFileName = I18nFactory.getHandler().getMessage("pay.multiline.error.lines.file.name.prefix", request.getLang()) + " - " + fileName;
                File errorLinesFile = new File();
                errorLinesFile.setContents(errorLinesContent);
                errorLinesFile.setFileName(relatedFileName);
                errorLinesFile.setDescription("Error lines of file " + fileName + ". Id: " + fileId);
                errorLinesFile.setFromBackoffice(false);
                errorLinesFile.setIdUser(request.getIdUser());
                errorLinesFile.setIdActivity(idActivity);

                response.putItem(OutParams.ID_RELATED_FILE, FilesHandler.getInstance().create(errorLinesFile));
                response.putItem(OutParams.NAME_RELATED_FILE, relatedFileName);
            }

            response.setReturnCode(ReturnCodes.OK);

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    private Processor getProcessor(String idForm, String idFormField) throws IOException {
        if (!StringUtils.isBlank(idForm)) {
            FormField formField = FormsHandler.getInstance().readField(idForm, null, idFormField);
            if (formField instanceof MultilinefileField) {
                //Instantiate subtype plugin and process file lines
                return ProcessorFactory.getProcessorSubType(formField.getSubType());
            }
        }
        return null;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        Long fileSize = request.getParam(InternalParams.FILE_SIZE, Long.class);
        String idForm = request.getParam(InParams.ID_FORM, String.class);
        String idFormField = request.getParam(InParams.ID_FORM_FIELD, String.class);
        String description = request.getParam(InParams.DESCRIPTION, String.class);
        String idActivity = request.getParam(InParams.ID_ACTIVITY, String.class);
        BufferedInputStream stream = request.getParam(InParams.FILE, BufferedInputStream.class);

        try {
            if (!StringUtils.isBlank(idForm) && StringUtils.isBlank(idFormField)) {
                result.put(InParams.ID_FORM_FIELD, "file.upload.idFormField.required");
            }
            if (stream == null || fileSize == null || fileSize <= 0) {
                result.put(InParams.FILE, "file.upload.file.required");
            }
            if (StringUtils.isBlank(description)) {
                result.put(InParams.DESCRIPTION, "file.upload.description.required");
            }
            if (StringUtils.isBlank(idActivity)) {
                result.put(InParams.ID_ACTIVITY, "file.upload.idActivity.required");
            }

            if (result.isEmpty() && !StringUtils.isBlank(idForm) && !StringUtils.isBlank(idFormField)) {

                FormField formField = FormsHandler.getInstance().readField(idForm, null, idFormField);

                if (formField instanceof InputfileField) {
                    InputfileField inputFileField = (InputfileField) formField;
                    if (fileSize > (inputFileField.getMaxFileSizeMB() * 1048576)) {
                        result.put(InParams.FILE, "file.upload.file.maxFileSizeExceeded");
                    } else {
                        //validemos si el tipo es el correcto
                        String mimeType = IOUtils.detectMediaType(stream);
                        if (!inputFileField.getAcceptedFileTypes().contains(mimeType)) {
                            result.put(InParams.FILE, "file.upload.file.fileTypeNotAllowed");
                        }
                    }
                } else if (formField instanceof MultilinefileField) {
                    MultilinefileField multilineFileField = (MultilinefileField) formField;
                    if (fileSize > (multilineFileField.getMaxFileSizeMB() * 1048576)) {
                        result.put(InParams.FILE, "file.upload.file.maxFileSizeExceeded");
                    } else {
                        //validemos si el tipo es el correcto
                        String mimeType = IOUtils.detectMediaType(stream);
                        if (!multilineFileField.getAcceptedFileTypes().contains(mimeType)) {
                            result.put(InParams.FILE, "file.upload.file.fileTypeNotAllowed");
                        }
                    }

                } else {
                    result.put(InParams.FILE, "file.upload.file.invalidFormField");
                }
            }

            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

}
