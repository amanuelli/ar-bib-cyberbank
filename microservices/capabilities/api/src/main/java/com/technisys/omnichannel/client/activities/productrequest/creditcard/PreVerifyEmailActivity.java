
package com.technisys.omnichannel.client.activities.productrequest.creditcard;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import java.io.IOException;

@DocumentedActivity("Given an exchange token retrieves retrieves all the stored user data")
@ExchangeActivity
public class PreVerifyEmailActivity extends Activity {
    
    public static final String ID = "productrequest.creditcard.preVerifyEmail";
    
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "The user selected username")
        String USERNAME = "username";
        
        @DocumentedParam(type = String.class, description = "The user selected email")
        String EMAIL = "email";
        
        @DocumentedParam(type = String.class, description = "The selected credit card")
        String CREDIT_CARD_ID = "creditCardId";

        @DocumentedParam(type = String.class, description = "The new exchange token")
        String EXCHANGE_TOKEN = "_exhangeToken";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);
            
            response.putItem(OutParams.EMAIL, token.getAttribute("email"));
            response.putItem(OutParams.USERNAME, token.getAttribute("username"));
            response.putItem(OutParams.CREDIT_CARD_ID, token.getAttribute("creditCardId"));
            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            
            response.setReturnCode(ReturnCodes.OK);
            return response;
        } catch (IOException ex) {
            throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, ex);
        }
    }
}
