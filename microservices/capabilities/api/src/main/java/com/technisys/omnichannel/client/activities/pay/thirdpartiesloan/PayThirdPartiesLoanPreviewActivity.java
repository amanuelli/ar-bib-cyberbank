/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.pay.thirdpartiesloan;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorP;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.PayLoanDetail;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 */
public class PayThirdPartiesLoanPreviewActivity extends Activity {

    public static final String ID = "pay.loan.preview";

    public interface OutParams {

        String DEBIT_ACCOUNT_ALIAS = "debitAccountAlias";
        String DEBIT_ACCOUNT_CURRENCY = "debitAccountCurrency";
        String AMOUNT = "amount";
        String DEBIT_AMOUNT = "debitAmount";
        String RATE = "rate";
        String LOAN_NUMBER = "loanNumber";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Administration.getInstance().readEnvironment(request.getIdEnvironment());

            Map map = request.getParam(PayThirdPartiesLoanSendActivity.InParams.ID_DEBIT_ACCOUNT, Map.class);
            String debitAccountValue = (String) map.get("value");
            Product debitProduct = Administration.getInstance().readProduct(debitAccountValue, request.getIdEnvironment());
            Account debitAccount = new Account(debitProduct);

            Loan loan = new Loan();
            loan.setNumber(request.getParam(PayThirdPartiesLoanSendActivity.InParams.LOAN_NUMBER, String.class));
            Amount amount = request.getParam(PayThirdPartiesLoanSendActivity.InParams.AMOUNT, Amount.class);

            PayLoanDetail detail = RubiconCoreConnectorP.payLoanValidate(request.getIdTransaction(),
                    debitAccount.getClient().getIdClient(), debitAccount, loan,
                    amount.getCurrency(), amount.getQuantity(),
                    "");

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            if (detail.getReturnCode() == 0) {
                response.putItem(OutParams.DEBIT_AMOUNT, detail.getDebitAmount());

                response.putItem(OutParams.LOAN_NUMBER, loan.getNumber());

                response.putItem(OutParams.AMOUNT, amount);
                response.putItem(OutParams.DEBIT_ACCOUNT_ALIAS, pLabeler.calculateShortLabel(debitProduct));
                response.putItem(OutParams.DEBIT_ACCOUNT_CURRENCY, debitAccount.getCurrency());
                response.putItem(OutParams.RATE, detail.getRate());

                response.setReturnCode(ReturnCodes.OK);
            } else {
                LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

                /*
                 * 1 - cuenta debito invalida
                 * 2 - préstamo invalido
                 * 3 - cuenta debito sin saldo suficiente
                 */
                switch (detail.getReturnCode()) {
                    case 200:
                        errors.put(PayThirdPartiesLoanSendActivity.InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.thirdPartiesLoan.debitAccount.invalid", request.getLang()));
                        break;
                    case 201:
                        errors.put(PayThirdPartiesLoanSendActivity.InParams.LOAN_NUMBER, I18nFactory.getHandler().getMessage("pay.thirdPartiesLoan.loan.invalid", request.getLang()));
                        break;
                    case 202:
                        errors.put(PayThirdPartiesLoanSendActivity.InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.thirdPartiesLoan.debitAccount.insufficientBalance", request.getLang()));
                        break;
                    default:
                        String key = "pay.thirdPartiesLoan." + detail.getReturnCode();
                        String message = I18nFactory.getHandler().getMessage(key, request.getLang());
                        if (!StringUtils.isBlank(message) && !message.equals(key)) {
                            errors.put("NO_FIELD", message);
                        } else {
                            response.setReturnCode(ReturnCodes.BACKEND_SERVICE_ERROR);
                        }
                }
                response.setReturnCode(ReturnCodes.VALIDATION_ERROR);
                response.setData(errors);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (BackendConnectorException bcE) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, bcE);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new PayThirdPartiesLoanSendActivity().validate(request);
        return result;
    }
}
