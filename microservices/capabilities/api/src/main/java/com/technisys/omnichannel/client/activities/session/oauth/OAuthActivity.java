/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session.oauth;

import com.technisys.digital.auth.AuthFactory;
import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.ReturnCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.springframework.http.ResponseEntity;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * Second login step that invokes the authorization server.
 * This endpoint exists only for testing purposes until the ApiDoc supports endpoints from other microservices
 */
@ExchangeActivity
@DocumentedActivity("Login step 2.1")
public class OAuthActivity extends Activity {
    public static final String ID = "session.login.oauth.step2bapidocs";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Username")
        String USERNAME = "username";
        @DocumentedParam(type = String.class, description = "Password")
        String PASSWORD = "password";
    }
    // This parameter isn't a DocumentedParam so it doesn't show in the ApiDocs
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface OutParams {}

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);

        try {
            String username = encryptWithPublicKey(request.getParam(InParams.USERNAME, String.class));

            String rawPassword = request.getParam(InParams.PASSWORD, String.class);
            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
            String password = encryptWithPublicKey(rawPassword + "#" + exchangeToken);
            String auth = "Basic " + Base64.getEncoder().encodeToString("web-client:web-secret".getBytes());
            Map<String, String> headers = new HashMap<>();
            headers.put("channel", "frontend");
            ResponseEntity<Map<String, String>> responseEntity = AuthFactory.getClient().loginWithResourceOwnerPasswordCredentials(username, password, null, auth, headers);

            if (responseEntity.getStatusCodeValue() != 200) {
                String errorCode = Objects.requireNonNull(responseEntity.getBody()).get("error_description");
                if (errorCode ==  null) {
                    throw new ActivityException(ReturnCodes.UNEXPECTED_ERROR);
                } else {
                    ReturnCode.Group group = ReturnCode.Group.API;
                    int code = Integer.parseInt(errorCode.substring(3, 6));
                    String categoryLiteral = errorCode.substring(6, 7);
                    ReturnCode.Category category;
                    switch (categoryLiteral) {
                        case "I":
                            category = ReturnCode.Category.valueOf("Info");
                            break;
                        case "W":
                            category = ReturnCode.Category.valueOf("Warn");
                            break;
                        default:
                            category = ReturnCode.Category.valueOf("Error");
                    }
                    throw new ActivityException(new ReturnCode(group, code, category));
                }
            }
            response.putAllItems(new HashMap<>(Objects.requireNonNull(responseEntity.getBody())));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException | NoSuchPaddingException | BadPaddingException | IllegalBlockSizeException | InvalidKeyException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
        return response;
    }

    private static String encryptWithPublicKey(String textToEncrypt) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException {
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, getPublicKey());
        return Base64.getEncoder().encodeToString(cipher.doFinal(textToEncrypt.getBytes()));
    }

    private static PublicKey getPublicKey() throws NoSuchAlgorithmException, InvalidKeySpecException {
        String publicKeyContent =
                "-----BEGIN PUBLIC KEY-----\n" +
                "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4b2L3Gk8Q+qlcFQSRmho\n" +
                "/fDPvrZ3A5jx3/9XE2X7N6EO2F9RcH23b8EY8hEEd7lHNXqHDYcpt97QPgey1lTq\n" +
                "NItUrRqYFACU+5FFxUD6hwcibZFRaBy8PjHsfT2m9VcTlCAvViybHrwWMH73o9V8\n" +
                "zLJfoFtxKKxfiTv4CBVIxLN+dTOJbqKgOYSRCG/3022rWcB4KpUqQlqTfp+3CreI\n" +
                "IOcTVq9MZ41MvniMczBdC4ywOtBm/sd9acpUch2MKUbEe8nL9izfjahS6MADFAHq\n" +
                "DTStCAsfPwh5S3yOj0cVtun6rkMhPG1Av6HT1bAWW8Du6BvF1ZKdC4ZY4t3/e005\n" +
                "oQIDAQAB\n" +
                "-----END PUBLIC KEY-----";
        publicKeyContent = publicKeyContent.replaceAll("\\n", "").replace("-----BEGIN PUBLIC KEY-----", "").replace("-----END PUBLIC KEY-----", "");
        byte [] publicKeyContents = Base64.getDecoder().decode(publicKeyContent);

        X509EncodedKeySpec keySpecX509 = new X509EncodedKeySpec(publicKeyContents);
        KeyFactory kf = KeyFactory.getInstance("RSA");
        return kf.generatePublic(keySpecX509);
    }

}