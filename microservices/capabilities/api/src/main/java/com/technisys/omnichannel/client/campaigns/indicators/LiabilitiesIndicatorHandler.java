/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.campaigns.indicators;

import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.gson.GsonBuilder;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.domain.ClientProduct;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.indicators.AssetsIndicatorInfo;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.campaigns.IndicatorHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.CampaignContextParameters;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Indicator;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.NumberUtils;
import com.technisys.omnichannel.core.utils.plugins.CurrencyConverter;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

/**
 *
 * @author fpena
 */
public class LiabilitiesIndicatorHandler extends IndicatorHandler{

    private static final Logger log = LoggerFactory.getLogger(LiabilitiesIndicatorHandler.class);
    private static final String OPERATOR_GREATER = "greaterOrEqual";
    private static final String OPERATOR_LESS = "lessOrEqual";
    private static final List<String> OPERATORS;
    private static Cache<Integer, Double> cache = null;
    
    static{
        OPERATORS = new ArrayList();
        OPERATORS.add(OPERATOR_GREATER);
        OPERATORS.add(OPERATOR_LESS);
        
        // Maximun size
        int maximumSize = ConfigurationFactory.getInstance().getDefaultInt(Configuration.PLATFORM, "campaigns.indicators.liabilities.cache.maximumSize", 50);

        // ExpireAfter
        long expire = ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "campaigns.indicators.liabilities.cache.expireAfter");

        cache = CacheBuilder.newBuilder()
                .maximumSize(maximumSize)
                .expireAfterWrite(expire, TimeUnit.MILLISECONDS)
                .build();
    }
    
    @Override
    public String getDescription(Indicator indicator, String lang) {
        AssetsIndicatorInfo sIndicator = new GsonBuilder().create().fromJson(indicator.getData(), AssetsIndicatorInfo.class);
        
        StringBuilder builder = new StringBuilder();
        builder.append("<b>");
        
        builder.append(I18nFactory.getHandler().getMessage("backoffice.campaigns.indicators.liabilities.operator." + sIndicator.getOperator() +".description", lang));
        
        builder.append("</b> ");
        
        String masterCurr = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "core.masterCurrency", "");
        
        builder.append(StringUtils.isNotBlank(masterCurr) ? I18nFactory.getHandler().getMessage("core.currency.label." + masterCurr, lang) : "");
        builder.append(" ").append(NumberUtils.formatDecimal(sIndicator.getAmount(), lang));
        
        return builder.toString();
    }

    @Override
    public Map<String, String> validate(int ruleId, int indicatorId, String jsonData) throws IOException{
        
        Map<String, String> result = new HashMap<>();
        
        JSONObject jsonObj = new JSONObject(jsonData);
        
        String operator = jsonObj.getString("operator");
        if (StringUtils.isEmpty(operator) || !OPERATORS.contains(operator)){
            result.put(ruleId + "_" + indicatorId + "_operator", "backoffice.campaign.rule.liabilities.noOperator");
        }
        if (jsonObj.isNull("amount")){
            result.put(ruleId + "_" + indicatorId + "_amount", "backoffice.campaign.rule.liabilities.noAmount");
        }else{
            double amount = jsonObj.getDouble("amount");
            if (amount < 0){
                result.put(ruleId + "_" + indicatorId + "_segment", "backoffice.campaign.rule.liabilities.noAmount");
            }
        }
        return result;
    }
    
    @Override
    public Map<String, Object> getFieldDataForConfiguration(String lang) throws IOException {
        Map<String, Object> data = new HashMap();
        
        data.put("operators", OPERATORS);
        
        return data;
    }
    
    /**
     * Para quien actualice este método. Tener en cuenta que tiene que ser lo más performante posible
     * @param indicator
     * @param contextParameters
     * @return
     * @throws IOException 
     */
    @Override
    public boolean evaluate(Indicator indicator, CampaignContextParameters contextParameters) throws IOException {
        
        if (contextParameters.getIdEnvironment() <= 0){
            return false;
        }
                
        Double enviromentAssets = cache.getIfPresent(contextParameters.getIdEnvironment());
        if (enviromentAssets == null){
            enviromentAssets = 0d;
            Environment environment = Administration.getInstance().readEnvironment(contextParameters.getIdEnvironment());
            if (environment == null){
                return false;
            }

            CurrencyConverter converter = CoreUtils.getCurrencyConverter();
            Loan l;
            CreditCard cc;
            try {
                List<Product> environmentProducts = (List<Product>) RubiconCoreConnectorC.listProducts("", environment.getClients(), Arrays.asList(new String[]{Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PI_KEY, Constants.PRODUCT_TC_KEY}));
            
                String masterCurrency = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency");
                
                for (Product p : environmentProducts){

                    switch (ProductUtils.getProductType(p.getExtraInfo())) {
                            case Constants.PRODUCT_PA_KEY:
                            case Constants.PRODUCT_PI_KEY:
                                l = (Loan)p;
                                enviromentAssets += converter.convert(l.getCurrency(), masterCurrency, l.getPendingBalance());
                                break;
                            case Constants.PRODUCT_PF_KEY:
                                cc = (CreditCard)p;
                                enviromentAssets += converter.convert(ClientProduct.CURRENCY_USD, masterCurrency, cc.getBalance());
                                break;
                        default:
                            String logMessage= MessageFormat.format("Unexpected ProductUtils.getProductType(p.getExtraInfo() value {0}", ProductUtils.getProductType(p.getExtraInfo()));
                            log.error(logMessage);
                            break;
                        }
                }
                cache.put(contextParameters.getIdEnvironment(), enviromentAssets);
            } catch (BackendConnectorException e) {
                log.error("Error while checking on product.",e);
            }
        }
        
        AssetsIndicatorInfo aIndicator = new GsonBuilder().create().fromJson(indicator.getData(), AssetsIndicatorInfo.class);
        
        switch (aIndicator.getOperator()){
            case OPERATOR_GREATER:
                return enviromentAssets >= aIndicator.getAmount();
            case OPERATOR_LESS:
                return enviromentAssets <= aIndicator.getAmount();
            default:
                String logMessage= MessageFormat.format("Unexpected aIndicator.getOperator() value {0}", aIndicator.getOperator());
                log.error(logMessage);
                break;
        }
        
        return false;
    }

}
