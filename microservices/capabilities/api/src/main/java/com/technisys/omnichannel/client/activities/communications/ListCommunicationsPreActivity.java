/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.List;

import static com.technisys.omnichannel.client.activities.communications.ListCommunicationsPreActivity.OutParams.COMMUNICATION_TYPES;

/**
 *
 */
@DocumentedActivity("List pre for communication's messages")
public class ListCommunicationsPreActivity extends Activity {

    public static final String ID = "communications.listPre";
    
    public interface InParams {
    }

    public interface OutParams {
        @DocumentedParam(type =  String[].class, description =  "List of communication types")
        String COMMUNICATION_TYPES = "communicationTypes";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
        
            List <String> communicationTypes = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "core.communications.communicationTypes");
            
            response.putItem(COMMUNICATION_TYPES, communicationTypes);
            response.setReturnCode(ReturnCodes.OK);
            
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}