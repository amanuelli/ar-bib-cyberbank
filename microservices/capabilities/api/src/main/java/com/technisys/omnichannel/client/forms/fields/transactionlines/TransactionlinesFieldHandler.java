package com.technisys.omnichannel.client.forms.fields.transactionlines;

import com.technisys.omnichannel.client.domain.fields.MultilinefileField;
import com.technisys.omnichannel.client.domain.fields.TransactionlinesField;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.forms.fields.FieldHandler;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.DBUtils;
import com.technisys.omnichannel.core.utils.JsonUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.quartz.SchedulerException;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TransactionlinesFieldHandler extends FieldHandler {
    protected static final String MAX_ACCEPTED_LINES = "maxAcceptedLines";
    protected static final String CONTROL_LIMITS = "controlLimits";
    protected static final String USE_FOR_TOTAL_AMOUNT = "useForTotalAmount";
    protected static final String CONTROL_LIMIT_OVER_PRODUCT = "controlLimitOverProduct";

    @Override
    public String getShortDescription(Map<String, String> map, String lang) {
        //no agrego nada especifico de este tipo de campo
        return getBasicShortDescription(map, lang);
    }

    @Override
    public String getFullDescription(Map<String, String> map, String lang) {
        I18n i18n = I18nFactory.getHandler();

        StringBuilder strBuilder = new StringBuilder();

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.id", lang)).append(":</dt>").append("<dd>").append(StringUtils.isNotBlank(map.get("id")) ? map.get("id") : i18n.getMessage("backoffice.notApply", lang)).append("</dd>");
        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.type", lang)).append(":</dt>").append("<dd>").append(i18n.getMessage("form.fieldType." + map.get("type"), lang)).append("</dd>");

        if (StringUtils.isNotBlank(map.get("note"))) {
            strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.note", lang)).append(":</dt>").append("<dd>").append(map.get("note")).append("</dd>");
        }

        String visibilityText = FormsHandler.getInstance().getConditionMessage("visible", map, lang);

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.visibility", lang)).append(":</dt>").append("<dd>").append(visibilityText).append("</dd>");

        String requiredText = FormsHandler.getInstance().getConditionMessage("required", map, lang);

        strBuilder.append("<dt>").append(i18n.getMessage("backoffice.forms.fields.mandatoryness", lang)).append(":</dt>").append("<dd>").append(requiredText).append("</dd>");

        return strBuilder.toString();
    }

    @Override
    public Map<String, Serializable> getFieldDataForConfiguration(String idForm, String idField, String lang) throws IOException {
        Map<String, Serializable> data = new HashMap<>();
        //esta habilitado caps por producto
        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getBoolean(Configuration.LIMITS, "core.use_cap_environment_product");
        data.put("capsByProductEnabled", capsByProductEnabled);
        return data;
    }

    @Override
    public Map<String, Serializable> loadFormFieldData(FormField field, Request request, Transaction transaction) throws IOException, SchedulerException {
        Map<String, Serializable> data = super.loadFormFieldData(field, request, transaction);

        if (transaction == null) {
            List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);
            TransactionlinesField psField = (TransactionlinesField) field;
            //required
            if (psField.getRequiredErrorMap() != null) {
                languages.forEach(lang -> {
                    if (StringUtils.isBlank(psField.getRequiredErrorMap().get(lang))) {
                        psField.getRequiredErrorMap().put(lang, I18nFactory.getHandler().getMessage("fields.defaultForm.defaultField.requiredError", lang));
                    }
                });
            }


            String json = readSpecificData(field.getIdForm(), field.getFormVersion(), field.getIdField());
            return JsonUtils.fromJson(json, Map.class);
        }


        return data;
    }

    @Override
    public Map<String, String> validate(Map<String, String> map) throws IOException {
        Map<String, String> result = new HashMap<>();
        return result;
    }

    @Override
    public Map<String, String> validate(FormField field) throws IOException {
        return new HashMap<>();
    }

    @Override
    public void createFormField(SqlSession session, Map<String, String> map) throws IOException {
        Map<String, Object> specificData = new HashMap();

        specificData.put(MAX_ACCEPTED_LINES, Integer.parseInt(map.get(MAX_ACCEPTED_LINES)));


        String controlLimits = map.get("controlLimits");
        if (StringUtils.isNotBlank(controlLimits)) {
            specificData.put(CONTROL_LIMITS, Boolean.parseBoolean(controlLimits));
        }

        String useForTotalAmount = map.get("useForTotalAmount");
        if (StringUtils.isNotBlank(useForTotalAmount)) {
            specificData.put(USE_FOR_TOTAL_AMOUNT, Boolean.parseBoolean(useForTotalAmount));
        }

        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getDefaultBoolean(Configuration.LIMITS,  "core.use_cap_environment_product", false);
        String controlLimitOverProduct = map.get("controlLimitOverProduct");
        if (capsByProductEnabled && StringUtils.isNotBlank(controlLimitOverProduct)) {
            specificData.put(CONTROL_LIMIT_OVER_PRODUCT, controlLimitOverProduct);
        }


        createSpecificData(session, (String) map.get("idForm"), (String) map.get("id"), JsonUtils.toJson(specificData, false, false));

    }

    @Override
    public void createFormField(SqlSession session, FormField field) throws IOException {

        TransactionlinesField transactionLinesField = (TransactionlinesField) field;

        Map<String, Object> specificData = new HashMap();
        specificData.put(MAX_ACCEPTED_LINES, transactionLinesField.getMaxAcceptedLines());
        specificData.put(CONTROL_LIMITS, transactionLinesField.isValidateCap());
        specificData.put(USE_FOR_TOTAL_AMOUNT, transactionLinesField.isUseForTotalAmount());
        specificData.put(CONTROL_LIMIT_OVER_PRODUCT, transactionLinesField.getIdFieldProductToValidateCap());

        createSpecificData(session, field.getIdForm(), field.getIdField(), JsonUtils.toJson(specificData, false, false));
    }

    @Override
    public void updateFormField(SqlSession session, Map<String, String> map) throws IOException {
        Map<String, Object> specificData = new HashMap();
        specificData.put(MAX_ACCEPTED_LINES, Integer.parseInt(map.get(MAX_ACCEPTED_LINES)));

        String controlLimits = map.get("controlLimits");
        if (StringUtils.isNotBlank(controlLimits)) {
            specificData.put(CONTROL_LIMITS, Boolean.parseBoolean(controlLimits));
        }

        String useForTotalAmount = map.get("useForTotalAmount");
        if (StringUtils.isNotBlank(useForTotalAmount)) {
            specificData.put(USE_FOR_TOTAL_AMOUNT, Boolean.parseBoolean(useForTotalAmount));
        }

        boolean capsByProductEnabled = ConfigurationFactory.getInstance().getDefaultBoolean(Configuration.LIMITS,  "core.use_cap_environment_product", false);
        String controlLimitOverProduct = map.get("controlLimitOverProduct");
        if (capsByProductEnabled && StringUtils.isNotBlank(controlLimitOverProduct)) {
            specificData.put(CONTROL_LIMIT_OVER_PRODUCT, controlLimitOverProduct);
        }

        String idForm = map.get("idForm");
        String oldIdField = map.get("oldId");
        String newIdField = map.get("id");
        String jsonData = JsonUtils.toJson(specificData, false, false);
        if (StringUtils.equals(oldIdField, newIdField)) {
            modifySpecificData(session, idForm, newIdField, jsonData);
        } else {
            createSpecificData(session, idForm, newIdField, jsonData);
        }
    }

    @Override
    public FormField readFormField(String idForm, Integer formVersion, String idField, boolean withMessages) throws IOException {
        TransactionlinesField transactionLinesField = new TransactionlinesField();
        transactionLinesField.setIdForm(idForm);
        transactionLinesField.setFormVersion(formVersion);
        transactionLinesField.setIdField(idField);

        String json = readSpecificData(idForm, formVersion, idField);
        if (json != null) {
            Map<String, Object> specificData = JsonUtils.fromJson(json, Map.class);

            transactionLinesField.setMaxAcceptedLines((int) specificData.get(MAX_ACCEPTED_LINES));

            transactionLinesField.setIdFieldProductToValidateCap((String) specificData.get(CONTROL_LIMIT_OVER_PRODUCT));
            transactionLinesField.setValidateCap(specificData.get(CONTROL_LIMITS) != null && (Boolean) (specificData.get(CONTROL_LIMITS)));
            transactionLinesField.setUseForTotalAmount((Boolean) specificData.get(USE_FOR_TOTAL_AMOUNT));
        }

        if (withMessages) {

            FormMessagesHandler fmh = FormMessagesHandler.getInstance();

            try (SqlSession session = DBUtils.getInstance().openReadSession()) {
                //listo errores requerido
                transactionLinesField.setRequiredErrorMap(fmh.listFieldMessages(session, idField, idForm, transactionLinesField.getFormVersion(), "requiredError"));
            }
        }
        return transactionLinesField;
    }

    @Override
    public Map<String, String> validateValue(Request request, String idField, Map<String, FormField> fields) throws IOException {
        Map<String, String> errors = new HashMap<>();
        TransactionlinesField field = (TransactionlinesField) fields.get(idField);
        if (field.getMaxAcceptedLines() != 0 && request.getParam(idField, List.class).size() > field.getMaxAcceptedLines()) {
            errors.put(idField, "fields.defaultForm.defaultField.invalidError");
        }
        return errors;
    }

    @Override
    public boolean isHasValue(FormField field, Object fieldValue) {
        if (fieldValue instanceof List) {
            return !((List) fieldValue).isEmpty();
        }
        return false;
    }

    @Override
    public void deleteFormField(SqlSession session, String idForm, int formVersion, String idField) throws IOException {
        //No es necesario realizar acción dado que no posee tablas custom
        //El registro en form_field_data se borra con la cascada de la base
    }

    @Override
    public String getPDFDescription(FormField field, String lang) {
        StringBuilder strBuilder = new StringBuilder();

        MultilinefileField multilineFileField = (MultilinefileField) field;

        I18n i18n = I18nFactory.getHandler();

        strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.multilinefile.maxFileSizeMB", lang)).append(":</em> ").append(multilineFileField.getMaxFileSizeMB()).append("<br/>");
        strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.multilinefile.acceptedFileTypes", lang)).append(":</em> ").append("<br />");

        List<String> acceptedFileTypes = multilineFileField.getAcceptedFileTypes();
        acceptedFileTypes.forEach(fileType ->
                strBuilder.append(" - ").append(fileType).append("<br />"));


        //textos internacionalizados
        List<String> languages = ConfigurationFactory.getInstance().getListSafe(Configuration.PLATFORM,  "core.languages", String.class);

        //requiredError
        if (multilineFileField.getRequiredErrorMap() != null) {
            for (String key : languages) {
                if (StringUtils.isNotBlank(multilineFileField.getRequiredErrorMap().get(key))) {
                    strBuilder.append("<em>").append(i18n.getMessage("backoffice.forms.fields.multilinefile.requiredError", lang)).append(" (").append(key).append(")").append(":</em> ").append(multilineFileField.getRequiredErrorMap().get(key)).append("<br/>");
                }
            }
        }

        return strBuilder.toString();
    }
}
