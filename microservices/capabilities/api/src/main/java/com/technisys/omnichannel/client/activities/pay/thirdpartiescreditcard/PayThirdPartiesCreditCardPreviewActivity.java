/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com 
 */
package com.technisys.omnichannel.client.activities.pay.thirdpartiescreditcard;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.PayCreditCardDetail;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
public class PayThirdPartiesCreditCardPreviewActivity extends Activity {

    public static final String ID = "pay.thirdPartiesCreditCard.preview";

    public interface OutParams {

        String DEBIT_ACCOUNT_ALIAS = "debitAccountAlias";
        String DEBIT_AMOUNT = "debitAmount";
        String AMOUNT = "amount";
        String RATE = "rate";
        String CREDIT_CARD_NUMBER = "creditCardNumber";
        String CREDIT_CARD_BANK = "creditCardBank";
        String CREDIT_CARD_MARK = "creditCardMark";
        
        String NOTIFICATION_EMAILS = "notificationEmails";
        String NOTIFICATION_BODY = "notificationBody";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Administration.getInstance().readEnvironment(request.getIdEnvironment());

            Map map = request.getParam(PayThirdPartiesCreditCardSendActivity.InParams.ID_DEBIT_ACCOUNT, Map.class);
            String debitAccountValue = (String) map.get("value");
            Product debitProduct = Administration.getInstance().readProduct(debitAccountValue, request.getIdEnvironment());

            Account debitAccount = new Account(debitProduct);

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());
            String creditCardNumber = request.getParam(PayThirdPartiesCreditCardSendActivity.InParams.CREDIT_CARD_NUMBER, String.class);

            String creditCardBank = Constants.TECHNISYS_BANK_ID;
            String creditCardMark = ProductUtils.getCreditCardBrand(creditCardNumber, request.getLang());
            Amount amount = request.getParam(PayThirdPartiesCreditCardSendActivity.InParams.AMOUNT, Amount.class);

            PayCreditCardDetail detail = RubiconCoreConnectorTC.payCreditCardValidate(request.getIdTransaction(),
                    debitAccount.getClient().getIdClient(), debitAccount,
                    creditCardNumber, creditCardBank,
                    amount.getCurrency(), amount.getQuantity(),
                    "", 0, 0);

            if (detail.getReturnCode() == 0) {
                response.putItem(OutParams.DEBIT_AMOUNT, detail.getDebitAmount());
                response.putItem(OutParams.AMOUNT, amount);
                response.putItem(OutParams.CREDIT_CARD_NUMBER, creditCardNumber);
                response.putItem(OutParams.CREDIT_CARD_BANK, creditCardBank);
                response.putItem(OutParams.DEBIT_ACCOUNT_ALIAS, pLabeler.calculateShortLabel(debitProduct));
                response.putItem(OutParams.RATE, detail.getRate());
                response.putItem(OutParams.CREDIT_CARD_MARK, creditCardMark);
                
                List<String> notificationMails = request.getParam(PayThirdPartiesCreditCardSendActivity.InParams.NOTIFICATION_EMAILS, List.class);
                String notificationBody = request.getParam(PayThirdPartiesCreditCardSendActivity.InParams.NOTIFICATION_BODY, String.class);
                
                response.putItem(OutParams.NOTIFICATION_EMAILS, notificationMails);
                response.putItem(OutParams.NOTIFICATION_BODY, notificationBody);

                response.setReturnCode(ReturnCodes.OK);
            } else {
                LinkedHashMap<String, Object> errors = new LinkedHashMap<>();

                /*
                 * 300 - cuenta debito invalida
                 * 301 - tarjeta invalido
                 * 302 - cuenta debito sin saldo suficiente
                 */
                switch (detail.getReturnCode()) {
                    case 300:
                        errors.put(PayThirdPartiesCreditCardSendActivity.InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.thirdPartiesCreditCard.debitAccount.invalid", request.getLang()));
                        break;
                    case 301:
                        errors.put(PayThirdPartiesCreditCardSendActivity.InParams.CREDIT_CARD_NUMBER, I18nFactory.getHandler().getMessage("pay.thirdPartiesCreditCard.creditCardNumber.invalid", request.getLang()));
                        break;
                    case 302:
                        errors.put(PayThirdPartiesCreditCardSendActivity.InParams.ID_DEBIT_ACCOUNT, I18nFactory.getHandler().getMessage("pay.thirdPartiesCreditCard.debitAccount.insufficientBalance", request.getLang()));
                        break;
                    default:
                        String key = "pay.thirdPartiesCreditCard." + detail.getReturnCode();
                        String message = I18nFactory.getHandler().getMessage(key, request.getLang());
                        if (!StringUtils.isBlank(message) && !message.equals(key)) {
                            errors.put("NO_FIELD", message);
                        } else {
                            response.setReturnCode(ReturnCodes.BACKEND_SERVICE_ERROR);
                        }
                }
                response.setReturnCode(ReturnCodes.VALIDATION_ERROR);
                response.setData(errors);
            }

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (BackendConnectorException bcE) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, bcE);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new PayThirdPartiesCreditCardSendActivity().validate(request);
        return result;
    }
}
