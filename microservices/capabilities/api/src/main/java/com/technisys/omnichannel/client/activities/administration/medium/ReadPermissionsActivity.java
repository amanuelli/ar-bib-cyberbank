package com.technisys.omnichannel.client.activities.administration.medium;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.common.Permissions;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@DocumentedActivity("Read user's permissions (medium scheme)")
public class ReadPermissionsActivity extends Activity {

    public static final String ID = "administration.medium.read.permissions";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "User id to read permissions")
        String ID = "id";
    }

    public interface OutParams {

        @DocumentedParam(type = Map.class, description = "Permission's map")
        String PERMISSIONS = "permissions";
        @DocumentedParam(type = Set.class, description = "UI permissions set")
        String GROUPS = "groups";
        @DocumentedParam(type = User.class, description = "User info")
        String USER = "user";
        @DocumentedParam(type = List.class, description = "Product's list")
        String PRODUCTS = "products";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            final String idUser = request.getParam(InParams.ID, String.class);
            final int idEnvironment = request.getIdEnvironment();
            final List<Group> groups = AccessManagementHandlerFactory.getHandler()
                    .getGroups(idUser, null, idEnvironment, -1, -1, null).getElementList();
            final String lang = request.getLang();

            if (!this.isSchemeValid(groups, request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME_DATA);
            }

            response.putItem(OutParams.PERMISSIONS, Permissions.buildPermissionsMap(groups.get(0), idEnvironment).get("allPermissions"));
            response.putItem(OutParams.GROUPS, Permissions.listUIPermissions(idEnvironment, lang));
            response.putItem(OutParams.USER, AccessManagementHandlerFactory.getHandler().getUser(idUser));
            response.putItem(OutParams.PRODUCTS, Permissions.listProducts(idEnvironment, lang));
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String userId = request.getParam(InParams.ID, String.class);
            List<String> environmentUsers = Administration.getInstance()
                    .listEnvironmentUserIds(request.getIdEnvironment());

            if (environmentUsers == null || (!environmentUsers.contains(StringUtils.left(userId, 253)))) {
                throw new ActivityException(ReturnCodes.VALIDATION_ERROR);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    private boolean isSchemeValid(List<Group> groups, final String scheme) {
        return Environment.ADMINISTRATION_SCHEME_MEDIUM.equals(scheme)
                && !CollectionUtils.isEmpty(groups) && groups.size() == 1;
    }
}
