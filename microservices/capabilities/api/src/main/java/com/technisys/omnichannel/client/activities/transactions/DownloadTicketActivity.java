/*
 * Copyright (c) 2019 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com.
 */
package com.technisys.omnichannel.client.activities.transactions;

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import com.technisys.omnichannel.activities.forms.ReadFormActivity;
import com.technisys.omnichannel.activities.forms.SendFormActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.activities.ActivityHandler;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.domain.fields.ProductselectorField;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.forms.fields.FieldHandler;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.DateUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author divie
 */
@DocumentedActivity("Download transaction ticket")
public class DownloadTicketActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(DownloadTicketActivity.class);
    public static final String ID = "transactions.downloadTicket";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "transaction id to download")
        String ID_TICKET = "idTicket";
        @DocumentedParam(type = String.class, description = "File format can be pdf and xls")
        String FORMAT = "format";
        @DocumentedParam(type = String.class, description = "form Id")
        String ID_FORM = "idForm";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "File name to save")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = Object.class, description = "Download file with ticket lines, it is Object because depends of file format, in xls is byte and pdf is String")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            ExportResource export;
            String idTransaction = request.getParam(InParams.ID_TICKET, String.class);
            String idForm = request.getParam(InParams.ID_FORM, String.class);
            String lang = request.getLang();

            if (!Authorization.hasPermissionOverForm(request.getIdUser(), request.getIdEnvironment(), idForm)) {
                throw new ActivityException(com.technisys.omnichannel.ReturnCodes.NOT_AUTHORIZED, "El usuario " + request.getIdUser() + " no tiene permiso sobre el formulario " + idForm);
            }

            Transaction transaction = TransactionHandlerFactory.getInstance().readUserTransaction(idTransaction, request.getIdUser(), request.getIdEnvironment());

            Integer formVersion = Transaction.STATUS_DRAFT.equals(transaction.getIdTransactionStatus()) ? null : transaction.getFormVersion();

            if (formVersion != null && (StringUtils.isNotBlank(idTransaction))) {
                if (transaction.getIdEnvironment() != request.getIdEnvironment()) {
                    throw new ActivityException(com.technisys.omnichannel.ReturnCodes.NOT_AUTHORIZED);
                }
                ClientActivityDescriptor ad = ActivityHandler.getInstance().readDescriptor(transaction.getIdActivity());

                boolean allowed = Authorization.hasPermissionOverTransaction(request.getIdUser(), request.getIdEnvironment(), ad, transaction, true);

                if (!allowed) {
                    throw new ActivityException(com.technisys.omnichannel.ReturnCodes.NOT_AUTHORIZED);
                }
            }

            Form form = FormsHandler.getInstance().readFormFull(idForm, formVersion);

            Map<String, Serializable> formFieldData;
            for (FormField ff : form.getFieldList()) {
                formFieldData = FieldHandler.getFieldHandler(ff.getType()).loadFormFieldData(ff, request, transaction);
                if (formFieldData != null && formFieldData.size() > 0) {
                    ff.setData(formFieldData);
                }

                if (formVersion == null && (ff instanceof ProductselectorField && FormField.CONDITION_TRUE.equals(ff.getRequired()) && FormField.CONDITION_TRUE.equals(ff.getVisible()))) {
                        Map<String, Serializable> data = ff.getData();
                        if (data == null || data.get("options") == null || ((List) data.get("options")).isEmpty()) {
                            throw new ActivityException(com.technisys.omnichannel.ReturnCodes.INSUFFICIENT_PRODUCTS);
                        }
                    }
            }

            //si el form no es de actividad, seteo la actividad generica de envio
            if (!Form.FORM_TYPE_ACTIVITY.equals(form.getType())) {
                form.setIdActivity(SendFormActivity.ID);
            } else {
                //si el formulario es de actividad, leo la url de redireccion al presionar el botón cancel
                String cancelUrl = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "forms." + form.getIdForm() + ".cancelUrl");
                if (StringUtils.isNotBlank(cancelUrl)) {
                    response.putItem(ReadFormActivity.OutParams.CANCEL_URL, cancelUrl);
                }
            }
/*
            export = exportPDF(transaction, form, lang);
            response.putItem(OutParams.CONTENT, new String(export.getContent()));

            response.putItem(OutParams.FILE_NAME, export.getFilename());

*/
            response.setReturnCode(ReturnCodes.OK);
        } catch (Exception ex) {
            throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {

        Map<String, String> result = new HashMap<>();
        String idTransaction = request.getParam(InParams.ID_TICKET, String.class);
        String idForm = request.getParam(InParams.ID_FORM, String.class);

        if (StringUtils.isBlank(idTransaction)) {
            result.put(InParams.ID_TICKET, "transactions.ticket.export.idTransaction.required");
        }

        if (StringUtils.isBlank(idForm)) {
            result.put(InParams.ID_FORM, "transactions.ticket.export.idForm.required");
        }

        try {
            Transaction transaction = TransactionHandlerFactory.getInstance().readUserTransaction(idTransaction, request.getIdUser(), request.getIdEnvironment());
            if (transaction == null) {
                result.put(InParams.ID_TICKET, "transactions.ticket.export.idTransaction.invalid");
            } else {
                Integer formVersion = Transaction.STATUS_DRAFT.equals(transaction.getIdTransactionStatus()) ? null : transaction.getFormVersion();
                Form form = FormsHandler.getInstance().readFormFull(idForm, formVersion);
                if (form == null) {
                    result.put(InParams.ID_TICKET, "transactions.ticket.export.idForm.invalid");
                }
            }

            return result;
        } catch (IOException ex) {
            throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, ex);
        }
    }

    private String getHtmlTemplate() throws IOException {
        InputStream is = this.getClass().getResourceAsStream("/templates/transactionticket_pdf_template.html");
        StringBuilder template = new StringBuilder();
        String line;

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(is))) {
            while ((line = reader.readLine()) != null) {
                template.append(line).append("\n");
            }
        } finally {
            is.close();
        }

        return template.toString();
    }

    private String deleteBetween(String input, String start, String end) {
        int startIndex = input.indexOf(start);
        int endIndex = input.indexOf(end);

        StringBuilder sb = new StringBuilder(input);
        sb.delete(startIndex, endIndex);
        return sb.toString();
    }

    private String compileHtml(String html, String lang, Transaction transaction, Form form) {
        I18n i18n = I18nFactory.getHandler();

        String formName = form.getFormNameMap().get(lang);
        StringBuilder formFieldsTicket = new StringBuilder();

        html = html.replace("%%FORMNAME%%", formName);
        html = html.replace("%%TICKET_TITLE%%", i18n.getMessage("forms.transaction.ticket", lang));
        html = html.replace("%%DATE_TRANSACTION_TITLE%%", i18n.getMessage("forms.transaction.ticket.date", lang));
        html = html.replace("%%DATE_TRANSACTION%%", transaction.getFullCreationDateTimeAsString());
        html = html.replace("%%ID_TRANSACTION_TITLE%%", i18n.getMessage("forms.transaction.ticket.number", lang));
        html = html.replace("%%ID_TRANSACTION%%", transaction.getIdTransaction());
        html = html.replace("%%SIGNATURES_TITLE%%", i18n.getMessage("forms.transaction.ticket.title", lang));
        html = html.replace("%%CREATED_BY_TITLE%%", i18n.getMessage("forms.transaction.ticket.createdBy", lang));
        html = html.replace("%%USER_CREATOR_LASTNAME%%", transaction.getUserCreatorLastName());
        html = html.replace("%%USER_CREATOR_FIRSTNAME%%", transaction.getUserCreatorFirstName());
        html = html.replace("%%DATE_CREATED%%", transaction.getCreationDateTimeAsString());
        html = html.replace("%%MODIFIED_BY_TITLE%%", i18n.getMessage("forms.transaction.ticket.modifiedBy", lang));
        html = html.replace("%%USER_EDITOR_LASTNAME%%", transaction.getUserEditorLastName());
        html = html.replace("%%USER_EDITOR_FIRSTNAME%%", transaction.getUserEditorFirstName());
        html = html.replace("%%DATE_MODIFIED%%", (new SimpleDateFormat("dd/MM/yyyy")).format(transaction.getModificationDateTime()));

        List<FormField> formFields = form.getFieldList();
        for (FormField ff : formFields) {
            if(ff instanceof TicketPrintableField){
                String label = StringUtils.EMPTY;
                String value = ((TicketPrintableField) ff).getPrintableFieldValue(ff, transaction.getData());

                if (ff.getLabelMap() != null) {
                    label = ff.getLabelMap().get(lang);
                }

                if(!StringUtils.isBlank(value)) {
                    formFieldsTicket.append(String.format("<div class=\"data-wrapper\"><span class=\"data-label\">%1$s</span><div class=\"newProduct\"><div class=\"newProduct-row\"><div class=\"newProduct-cell newProduct-cell--ellipsis\"><span>%2$s</span><p class=\"data-product\"></p></div></div></div></div>", label, value));
                }
            }
        }

        StringBuilder authorizedSignatures = new StringBuilder();
        String authorizedSignature = "<span>%1$s, %2$s (%3$s) - %4$s <br/></span>";
        //si la transaccion esta en un estado final, separo las firmas en las firmas usadas y las firmas que no se usaron para la transaccion
        List<TransactionSignature> transactionSignatures = transaction.getSignatures();

        if (!transactionSignatures.isEmpty()) {
            authorizedSignatures.append(String.format("<div class=\"data-wrapper\"><span class=\"data-label\"><span>%1$s</span></span>", i18n.getMessage("forms.transaction.ticket.authorizedBy", lang)));
            for (TransactionSignature ts : transactionSignatures) {
                Date signatureDate = ts.getCreationDateTime();
                authorizedSignatures.append(String.format(authorizedSignature, ts.getUserLastName(), ts.getUserFirstName(), ts.getSignatureLevel(), (new SimpleDateFormat("dd/MM/yyyy")).format(signatureDate)));
            }
            authorizedSignatures.append("</div>");
        }

        html = html.replace("%%FORMFIELDS%%", formFieldsTicket.toString());
        html = html.replace("%%AUTHORIZED_BY%%", authorizedSignatures.toString());

        if (transaction.isProgramed()){
            Program program = transaction.getProgram();
            if(program == null) {
                program = new Program((Map) transaction.getData().get("program"));
            }

            if(program != null) {
                html = html.replace("%%FREQUENCY_TITLE%%", i18n.getMessage("scheduler.frequency", lang));
                html = html.replace("%%FREQUENCY%%", program.toStringHumanReadable(lang));
            }

        } else if(transaction.isOnceExecutionScheduledTransaction()){
            html = html.replace("%%FREQUENCY_TITLE%%", i18n.getMessage("scheduler.frequency", lang));
            Map scheduler = (Map)transaction.getData().get("scheduler");
            Date executionDate = (Date) scheduler.get("valueDate");

            String i18nText = "";
            long diffInHours = DateUtils.hoursBetween(DateUtils.removeTime(new Date()), DateUtils.removeTime(executionDate));
            if(diffInHours == 24){
                i18nText = i18n.getMessage("scheduler.executeTomorrow", lang);
            }else{
                i18nText = i18n.getMessage("scheduler.executeFuture", lang);
            }

            SimpleDateFormat dateFormat = new SimpleDateFormat(ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "core.shortDateFormat", "dd/MM/yyyy"));
            String exeDateStr = dateFormat.format(executionDate);
            HashMap<String, Object> data = new HashMap<>();
            data.put("day", exeDateStr);

            Writer writer = new StringWriter();
            MustacheFactory mf = new DefaultMustacheFactory();

            try{
                Mustache mustache = mf.compile(new StringReader(i18nText), "");
                mustache.execute(writer, data);
                String result = writer.toString();
                writer.flush();
                html = html.replace("%%FREQUENCY%%", result);
            } catch (IOException e) {
                log.error("Error on compileHtml method: {}", e.getMessage());
            }

        }else{
            html = deleteBetween(html, "<!-- FREQUENCY_START_DONT_DELETE -->", "<!-- FREQUENCY_END_DONT_DELETE -->");
        }

        return html;
    }
    //TECDIGSK-625
    /*
    private byte[] generatePdf(String html) throws ParserConfigurationException, SAXException, IOException {
        Document doc = ExportUtils.getDocumentBuilder().parse(new InputSource(new StringReader(html)));
        ITextRenderer renderer = new ITextRenderer();
        renderer.setDocument(doc, "");
        renderer.layout();

        byte[] content;

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            renderer.createPDF(os);
            InputStream pdfSource = new ByteArrayInputStream(os.toByteArray());
            content = Base64.encodeBase64(IOUtils.toByteArray(pdfSource));
        }
        return content;
    }

    private ExportResource exportPDF(Transaction transaction, Form form, String lang) throws IOException, ParserConfigurationException, SAXException {
        String template = getHtmlTemplate();
        byte[] pdf = generatePdf(compileHtml(template, lang, transaction, form));
        String filename = I18nFactory.getHandler().getMessage("transactions.ticket.export.filename", lang) + ".pdf";

        return new ExportResource(filename, pdf);
    }

     */

    static class ExportResource {

        String filename;
        byte[] content;

        public ExportResource(String filename, byte[] content) {
            this.filename = filename;
            this.content = content;
        }

        public String getFilename() {
            return filename;
        }

        public byte[] getContent() {
            return content;
        }
    }

}
