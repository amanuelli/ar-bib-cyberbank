/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.domain.Onboarding;
import com.technisys.omnichannel.client.domain.UserEnvDAO;
import com.technisys.omnichannel.client.handlers.enrollment.EnrollmentHandler;
import com.technisys.omnichannel.client.handlers.onboardings.OnboardingHandlerFactory;
import com.technisys.omnichannel.connectors.SafewayConnector;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.credentials.CredentialPluginFactory;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import com.technisys.omnichannel.utils.SafewayUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Map;

import static com.technisys.omnichannel.client.activities.enrollment.WizardPreActivity.getInvitationCode;

/**
 *
 */
@ExchangeActivity @DocumentedActivity("Enrollment wizard finish")
public class WizardFinishActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(WizardFinishActivity.class);

    public static final String ID = "enrollment.wizard.finish";

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Password")
        String USER_PASSWORD = "_password";
        @DocumentedParam(type = String.class, description = "Password confirmation")
        String USER_PASSWORD_CONFIRMATION = "_passwordConfirmation";
        @DocumentedParam(type = String.class, description = "Security seal ID")
        String SECURITY_SEAL = "_securitySeal";
        @DocumentedParam(type = String.class, description = "Username")
        String USERNAME = "username";
    }

    public interface OutParams {
        @DocumentedParam(type = Integer.class, description = "Username")
        String ID_ENVIRONMENT = "idEnvironment";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            InvitationCode invitation = getInvitationCode(request);

            log.info("Iniciando registro para la invitación {}", invitation.getInvitationCode());

            // Paso 1: Creo al usuario en omnichannel (En este paso el usuario no existe ya que si existe el paso del enrollment sería el de asociar)
            ClientUser backendClient = null;
            if(invitation.getProductGroupId() != null) {
                backendClient = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), invitation.getDocumentCountry(), invitation.getDocumentType(), invitation.getDocumentNumber(), invitation.getProductGroupId());
            }

            String username = request.getParam(InParams.USERNAME, String.class);
            Integer securitySeal = request.getParam(InParams.SECURITY_SEAL, Integer.class);
            log.info("Por crear los datos del usuario/ambiente.");

            UserEnvDAO userEnvDao = EnrollmentHandler.createUser(request.getIdTransaction(), username, securitySeal, request.getLang(),
                    invitation, backendClient);

            response.putItem(OutParams.ID_ENVIRONMENT, userEnvDao.getIdEnvironment());
            User cUser = userEnvDao.getUser();
            log.info("Se creó los datos del usuario/ambiente.");

            // Agrego la clave en Safeway y la activo
            log.info("Por asignar la credencial de PASSWORD al usuario.");
            Credential password = request.getCredential(Credential.PWD_CREDENTIAL);
            String commonName = SafewayUtils.getUserInstance().getCommonName(cUser);

            CredentialPlugin credentialPlugin = CredentialPluginFactory.getCredentialPlugin(Credential.PWD_CREDENTIAL);
            credentialPlugin.assign(commonName, password.getValue());
            log.info("Credencial de PASSWORD asignada al usuario.");

            InvitationCodesHandler.setInvitationCodeAsUsed(invitation.getId());
            log.info("Se marcó la invitación {} como utilizada.", invitation.getInvitationCode() );

            changeValidationCodeStatus(invitation.getId());

            log.info("Registro para la invitación {} terminó correctamente.", invitation.getInvitationCode() );

            String exchangeTokenId = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeTokenHandler.updateActivities(exchangeTokenId, new String[]{EsignAccept.ID});

            credentialPlugin.changeStatus(commonName, SafewayConnector.CREDENTIAL_STATUS_ACTIVE);
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (DispatchingException ex) {
            throw new ActivityException(ex.getReturnCode(), ex.getMessage());
        }
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        try {
            InvitationCode invitation = getInvitationCode(request);

            // Verificacion de los pasos anteriores
            Map<String, String> result = WizardVerificationCodeActivity.validateFields(request, invitation);
            if (ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "enrollment.personalData.enabled")) {
                result.putAll(WizardPersonalDataActivity.validateFields(request, invitation));
            }
            result.putAll(WizardVerificationCodeActivity.validateFields(request, invitation));

            Credential password = request.getCredential(Credential.PWD_CREDENTIAL);
            boolean passwordIsEmpty = password == null || StringUtils.isBlank(password.getValue());
            if (passwordIsEmpty){
                result.put(InParams.USER_PASSWORD, "enrollment.step3.password.error.empty");
            }

            // Validate SECURITY_SEAL field
            Integer securitySeal = request.getParam(InParams.SECURITY_SEAL, Integer.class);
            boolean securitySealIsEmpty = securitySeal == null || securitySeal<1;
            if (securitySealIsEmpty){
                result.put(InParams.SECURITY_SEAL, "enrollment.step3.securitySeal.error.empty");
            }

            //Validate USERNAME field
            String userName = request.getParam(InParams.USERNAME, String.class);
            boolean userNameIsInvalid = userName == null || userName.isEmpty();
            if (userNameIsInvalid){
                result.put(InParams.USERNAME, "enrollment.step3.username.error.empty");
            }

            String passwordConfirmation = request.getParam(InParams.USER_PASSWORD_CONFIRMATION, String.class);
            boolean passwordConfirmationIsEmpty = passwordConfirmation == null || StringUtils.isBlank(passwordConfirmation);
            if (passwordConfirmationIsEmpty) {
                result.put(InParams.USER_PASSWORD_CONFIRMATION, "enrollment.step3.passwordConfirmation.error.empty");
            }

            if (!(passwordIsEmpty || passwordConfirmationIsEmpty || StringUtils.equals(password.getValue(), passwordConfirmation))) {
                result.put(InParams.USER_PASSWORD, "enrollment.step3.password.error.passwordConfirmationMustMatch");
                result.put(InParams.USER_PASSWORD_CONFIRMATION, "enrollment.step3.passwordConfirmation.error.passwordMustMatch");
            }

            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    private void changeValidationCodeStatus(int idInvitation) {
        try {
            OnboardingHandlerFactory.getInstance().updateStatusByInvitationCode(idInvitation, Onboarding.STATUS_FINISHED);

            log.info("Se actualiza el registro de onboarding si corresponde.");
        } catch (IOException e) {
            //si falla queremos que el usuario de todos modos finalice el proceso de enrollment sin ser notificado de este error
            log.error("Error actualizando el registro de onboarding si correspondiera.", e);
        }
    }
}
