/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class CyberbankCoreError {

    private String transactionId;
    private String responseCode;
    private List<Error> errors;

    public CyberbankCoreError(JSONObject errorResponse) {
        errors = new ArrayList<>();
        JSONArray errorList = errorResponse.getJSONObject("out.error_list").getJSONArray("collection");

        for (int i=0; i<errorList.length(); i++) {
            errors.add(new Error(errorList.getJSONObject(i)));
        }

        this.transactionId = errorResponse.getString("transactionId");
        this.responseCode = errorResponse.getString("responseCode");
    }

    public String getTransactionId() {
        return transactionId;
    }

    public String getResponseCode() {
        return responseCode;
    }

    public List<Error> getErrors() {
        return errors;
    }

    public class Error {
        private String code;
        private String description;
        private int id;
        private String errorCode;

        private Error(JSONObject error) {
            this.code = String.valueOf(error.getInt("codigo"));
            this.description = error.getString("description");
            this.id = error.getInt("id");
            this.errorCode = String.valueOf(error.getInt("codigoerror"));
        }

        public String getCode() {
            return code;
        }

        public String getDescription() {
            return description;
        }

        public int getId() {
            return id;
        }

        public String getErrorCode() {
            return errorCode;
        }
    }

}
