/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain.indicators;

import com.technisys.omnichannel.core.domain.IndicatorInfo;

/**
 *
 * @author fpena
 */
public class DocumentInFileIndicatorInfo implements IndicatorInfo{
    private String operator;
    private int idContent;
    private int idNewContent;
    private String filename;
    private String documents;
    
    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public int getIdContent() {
        return idContent;
    }

    public void setIdContent(int idContent) {
        this.idContent = idContent;
    }

    public int getIdNewContent() {
        return idNewContent;
    }

    public void setIdNewContent(int idNewContent) {
        this.idNewContent = idNewContent;
    }
    
    public String getFilename() {
        return filename;
    }

    public void setFilename(String filename) {
        this.filename = filename;
    }
    
    public String getDocuments() {
        return documents;
    }

    public void setDocuments(String documents) {
        this.documents = documents;
    }
}
