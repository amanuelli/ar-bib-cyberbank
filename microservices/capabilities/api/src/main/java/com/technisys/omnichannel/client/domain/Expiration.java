/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import java.util.Date;

/**
 *
 * @author aelters
 */
public class Expiration implements Comparable<Expiration> {

    public static final String STATUS_EXPIRED = "EXPIRED";
    public static final String STATUS_WARNING = "WARNING";
    private Date date;
    private String detail;
    private double importAmount;
    private double balance;
    private String idCurrency;
    private String status;
    private String productType;
    private String idProduct;
    private String number;

    public Expiration() {

    }

    public Expiration(Date date, double importAmount, String idCurrency) {
        this.date = date;
        this.importAmount = importAmount;
        this.idCurrency = idCurrency;
    }

    public Expiration(CreditCard card) {
        this.date = card.getExpirationDate();
        this.balance = card.getBalance();
        this.setProductType(card.getProductType());
        this.setNumber(card.getNumber());
        setIdProduct(card.getIdProduct());
    }

    public Expiration(Loan loan) {
        this.date = loan.getNextDueDate();
        this.importAmount = loan.getPaymentAmount();
        this.idCurrency = loan.getCurrency();
        this.setProductType(loan.getProductType());
        setIdProduct(loan.getIdProduct());

    }

    @Override
    public int compareTo(Expiration o) {
        //Para ordenar los vencimientos por fecha ascendente
        return this.date.compareTo(o.getDate());
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getImportAmount() {
        return importAmount;
    }

    public void setImportAmount(double importAmount) {
        this.importAmount = importAmount;
    }

    public String getIdCurrency() {
        return idCurrency;
    }

    public void setIdCurrency(String idCurrency) {
        this.idCurrency = idCurrency;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getProductType() {
        return productType;
    }

    public void setProductType(String productType) {
        this.productType = productType;
    }

    public String getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(String idProduct) {
        this.idProduct = idProduct;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

}
