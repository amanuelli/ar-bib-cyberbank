package com.technisys.omnichannel.client.forms.fields.productselector.resources;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreProductConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.client.domain.StatementLoan;
import com.technisys.omnichannel.client.forms.fields.productselector.ProductselectorFieldHandler;
import com.technisys.omnichannel.client.forms.fields.productselector.ProductselectorFieldHandler.Option;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.domain.fields.ProductselectorField;
import com.technisys.omnichannel.core.forms.fields.FrontendOption;
import com.technisys.omnichannel.core.forms.fields.OptionsResource;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

/**
 * Implementacion del recurso de opciones para subtipo default del campo
 * productSelector
 *
 * @author ?
 */
public class DefaultOptionsResource implements OptionsResource {

    @Override
    public List<? extends FrontendOption> getOptions(FormField field, Request request) throws IOException {
        List<Option> options = new ArrayList<>();

        try {
            ProductselectorField productSelectorField = (ProductselectorField) field;

            // Cargo las monedas especificadas en el campo
            Set<String> validCurrencies = new HashSet<>();
            validCurrencies.addAll(productSelectorField.getCurrencyList());

            /**
             * Cargo los productos actuales en Omnichannel, quedandomé solo con
             * los que tienen la moneda elegida en el tipo de campo
             */
            List<Product> omnichannelProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), productSelectorField.getPermissionList(), productSelectorField.getProductTypeList());
            Map<String, Product> omnichannelProductsById = new HashMap<>();
            for (Product product : omnichannelProducts) {
                // Si no hay monedas, simplemente no filtro los productos
                if (validCurrencies.isEmpty()) {
                    omnichannelProductsById.put(product.getIdProduct(), product);
                } else { // Si hay monedas...
                    String currency = ProductUtils.getCurrency(product.getExtraInfo());

                    // ... filtro el producto
                    if (validCurrencies.contains(currency)) {
                        omnichannelProductsById.put(product.getIdProduct(), product);
                    }
                }
            }

            // Cargo los productos desde el backend
            // NO@20150715: TODO: Hay que estudiar si poner el idCliente en la tabla environment o mever este método para el ClientEnvironmentHandler :( 
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            List<String> productTypes = productSelectorField.getProductTypeList();
            List<Product> backendProducts = CoreProductConnectorOrchestrator.list(request.getIdTransaction(), environment.getClients(), productTypes);


            /**
             * Recorro los productos llegados desde el backend y para cado uno
             * creo una bean a devolver, si los mismos están listados en los
             * productos autorizados de omnichannel para los permisos cargados
             * en el tipo de campo.
             *
             * Para cada bean cargo: .- El id interno de omnichannel .- La
             * etiqueta .- El saldo si el usuario tiene permiso de lectura .- La
             * moneda
             */
            ProductselectorFieldHandler psfh = new ProductselectorFieldHandler();
            for (Product backendProduct : backendProducts) {
                Product authorizedProduct = omnichannelProductsById.get(backendProduct.getIdProduct());

                if (backendProduct instanceof Account) {
                    Account backendAccount = (Account) backendProduct;
                    List<String> allowedStatus = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "backend.accounts.status.allowed");

                    if (backendAccount.getStatus() != null && !allowedStatus.contains(String.valueOf(backendAccount.getStatus().getStatusCode()))) {
                        authorizedProduct = null;
                    }
                }

                if (authorizedProduct != null) {
                    options.add(getOption(psfh, backendProduct, request, authorizedProduct));
                }
            }
        } catch (BackendConnectorException bce) {
            throw new IOException(bce);
        }

        return options;
    }

    private Option getOption(ProductselectorFieldHandler psfh, Product backendProduct, Request request, Product authorizedProduct) throws IOException, BackendConnectorException {
        Option option = psfh.new Option();

        // Id
        option.setId(backendProduct.getIdProduct());

        // Etiqueta
        option.setLabel(CoreUtils.getProductLabeler(request.getLang()).calculateShortLabel(authorizedProduct));

        // Saldo
        String currency = ProductUtils.getCurrency(backendProduct.getExtraInfo());
        if (backendProduct instanceof Account && Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), authorizedProduct.getIdProduct(), PRODUCT_READ_PERMISSION)) {
            Account account = (Account) backendProduct;
            Double quantity = account.getBalance();
            option.setBalance(new Amount(currency, quantity));
        }

        // Moneda
        option.setCurrency(currency);

        // Marca
        if (backendProduct instanceof CreditCard) {
            option.setType(ProductUtils.getCreditCardBrand(ProductUtils.getNumber(backendProduct.getExtraInfo()), request.getLang()));
        } else {
            option.setType(ProductUtils.getProductType(backendProduct.getExtraInfo()));
        }

        //TODO: esto debería estar en LoanSelectorOptionsResource
        if (Constants.PRODUCT_PA_KEY.equals(authorizedProduct.getProductType()) || Constants.PRODUCT_PI_KEY.equals(authorizedProduct.getProductType())) {
            List<StatementLoan> statements = RubiconCoreConnectorC.listLoanStatements(request.getIdTransaction(), authorizedProduct.getIdProduct(), authorizedProduct.getExtraInfo(), "pendingFees", 50, 0);
            List<StatementLoan> dueStatements = new ArrayList();

            Date now = new Date();
            boolean firstStatement = true;
            for (StatementLoan s : statements) {
                if (s.getDueDate().before(now)) {
                    dueStatements.add(s);
                } else if (firstStatement) {
                    dueStatements.add(s);
                    firstStatement = false;
                }
            }

            option.setStatementsLoan(dueStatements);
        }

        // Permisos sobre el producto
        option.setPermissions(Authorization.listProductPermissions(request.getIdUser(), request.getIdEnvironment(), backendProduct.getIdProduct()));

        return option;
    }

}
