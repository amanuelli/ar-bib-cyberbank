/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.handlers.environment;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.clients.ClientHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.desktop.LayoutHandler;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.limits.CapForEnvironment;
import com.technisys.omnichannel.core.limits.CapForSignature;
import com.technisys.omnichannel.core.limits.CapForUser;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.profiles.ProfileDataHandler;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.DBUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.*;
import java.util.stream.Collectors;

import static com.technisys.omnichannel.core.domain.Environment.ENVIRONMENT_TYPE_CORPORATE_GROUP;

/**
 * @author fpena
 */
public class EnvironmentHandler {

    private static final Logger LOG = LoggerFactory.getLogger(EnvironmentHandler.class);
    private static final List<String> PRODUCT_TYPES = Arrays.asList(Constants.PRODUCT_CA_KEY,
            Constants.PRODUCT_CC_KEY,
            Constants.PRODUCT_PA_KEY,
            Constants.PRODUCT_PF_KEY,
            Constants.PRODUCT_PI_KEY,
            Constants.PRODUCT_TC_KEY);
    private static EnvironmentHandler instance;

    private EnvironmentHandler() {
    }

    public static synchronized EnvironmentHandler getInstance() {
        if (instance == null) {
            instance = new EnvironmentHandler();
        }

        return instance;
    }

    public static Environment createHoldingEnvironment(String idTransaction, String[] clientIDs, String environmentName,
                                                       String adminScheme, int signatureQty) throws IOException, BackendConnectorException {
        Environment result = null;

        List<String> typeList = Arrays.asList(Constants.PRODUCT_CA_KEY,
                Constants.PRODUCT_CC_KEY,
                Constants.PRODUCT_PA_KEY,
                Constants.PRODUCT_PF_KEY,
                Constants.PRODUCT_PI_KEY,
                Constants.PRODUCT_TC_KEY);

        List<Product> products = new ArrayList<>();
        List<Client> clients = new ArrayList<>();
        Map<String, String> extendedData = new HashMap<>();
        List<Product> memberProducts = null;
        for (String clientID : clientIDs) {
            memberProducts = (List<Product>) RubiconCoreConnectorC.listProducts(idTransaction, clientID, typeList);
            products.addAll(memberProducts);
            ClientEnvironment clientEnvironment = RubiconCoreConnectorC.readClient(idTransaction, clientID);
            String clientId = clientEnvironment.getProductGroupId();
            String clientName = clientEnvironment.getAccountName();
            Client client = new Client(clientId, clientName);
            clients.add(client);
        }

        String signatureQtyTimesA = StringUtils.repeat("A", signatureQty);
        String defaultCapFrequency = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "core.capFrequency.list").get(0);

        List<Signature> signatures = new ArrayList<>();
        signatures.add(new Signature(null, signatureQtyTimesA, Signature.TYPE_ADMINISTRATIVE));
        signatures.add(new Signature(null, "A", Signature.TYPE_NO_AMOUNT));

        //default caps
        List<CapForSignature> caps = new ArrayList<>();
        CapForSignature cap;
        cap = new CapForSignature();
        cap.setFrequency(defaultCapFrequency);
        cap.setMaximum(-1);
        cap.setChannel(CoreUtils.CHANNEL_ALL);
        caps.add(cap);
        Signature signature = new Signature(caps, signatureQtyTimesA, Signature.TYPE_AMOUNT);
        signatures.add(signature);

        int idEnvironment;

        for (Client client : clients) {
            ClientHandler.createOrUpdateClient(client);
        }

        try (SqlSession lSqlSession = DBUtils.getInstance().openWriteSession()) {
            idEnvironment = Administration.getInstance().createEnvironment(lSqlSession, environmentName,
                    null, null, ENVIRONMENT_TYPE_CORPORATE_GROUP, signatures, false, defaultCapFrequency,
                    adminScheme, null, extendedData, products);
            lSqlSession.commit();
        }

        for (Client client : clients) {
            com.technisys.omnichannel.core.environments.EnvironmentHandler.addEnvironmentClient(idEnvironment, client);
        }

        return Administration.getInstance().readEnvironment(idEnvironment);
    }

    private static Environment createEnvironment(SqlSession session, String idTransaction, String productGroupId, String adminScheme, int signatureQty, String documentType, String documentNumber) throws IOException {
        ClientEnvironment backendEnv = CoreCustomerConnectorOrchestrator.readClientEnvironment(idTransaction, productGroupId, documentType, documentNumber);
        String envType = "retail";

        if (StringUtils.isBlank(adminScheme)) {
            adminScheme = "simple";
            List<String> environmentsTypesList = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "backoffice.environments.types");
            for (String environmentType : environmentsTypesList) {
                List<String> segmentsList = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "environment.type." + environmentType + ".segments");
                if (segmentsList.contains(backendEnv.getSegment())) {
                    adminScheme = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "environment.type." + environmentType + ".adminScheme");
                    envType = environmentType;
                    break;
                }
            }
        }

        String defaultCapFrequency = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "core.capFrequency.list").get(0);

        List<com.technisys.omnichannel.core.domain.Signature> signatures = new ArrayList<>();

        signatures.add(new com.technisys.omnichannel.core.domain.Signature(null, StringUtils.repeat("A", signatureQty), com.technisys.omnichannel.core.domain.Signature.TYPE_ADMINISTRATIVE));
        signatures.add(new com.technisys.omnichannel.core.domain.Signature(null, "A", com.technisys.omnichannel.core.domain.Signature.TYPE_NO_AMOUNT));

        //creo caps por defecto
        List<CapForSignature> caps = new ArrayList<>();
        CapForSignature cap;
        cap = new CapForSignature();
        cap.setFrequency(defaultCapFrequency);
        cap.setMaximum(-1);
        cap.setChannel(CoreUtils.CHANNEL_ALL);
        caps.add(cap);
        Signature signature = new Signature(caps, StringUtils.repeat("A", signatureQty), com.technisys.omnichannel.core.domain.Signature.TYPE_AMOUNT);
        signatures.add(signature);

        Map<String, String> envExtendedData = null;
        //se completan datos del ambiente del core
        if (backendEnv != null) {
            envExtendedData = backendEnv.getAddionalData();

            int idEnvironment = Administration.getInstance().createEnvironment(session, backendEnv.getAccountName(), backendEnv.getSegment(), productGroupId, envType, signatures, false, defaultCapFrequency, adminScheme, envExtendedData);
            String clientId = backendEnv.getProductGroupId();
            String clientName = backendEnv.getAccountName();
            Client client = new Client(clientId, clientName);
            com.technisys.omnichannel.core.environments.EnvironmentHandler.addEnvironmentClient(session, idEnvironment, client);

            return Administration.getInstance().readEnvironment(session, idEnvironment);
        } else {
            throw new IOException("backendEnv is null");
        }
    }

    public Environment removeHoldingClients(int environmentID, List<String> clientsCoreIdsToRemove)
            throws IOException, BackendConnectorException {
        String logMessage;
        Environment env = Administration.getInstance().readEnvironment(environmentID);
        List<String> members = env.getClients().stream().map(client -> client.getIdClient()).collect(Collectors.toList());

        if (env.isDeleted()) {
            logMessage= MessageFormat.format("Economic group is deleted (idEnvironment={0}) can not remove members from a deleted economic group.", environmentID);
            LOG.info(logMessage);
            throw new BackendConnectorException(ReturnCodes.NO_ACTIVE_ENVIRONMENTS.toString());
        }

        for (String clientId : clientsCoreIdsToRemove) {
            if (StringUtils.isNotBlank(clientId)) {
                if (members.contains(clientId)) {
                    Administration.getInstance().removeClientProductsFromEnvironment(clientId, environmentID);
                } else {
                    logMessage= MessageFormat.format("Client with product group id ={0} can not be removed from the economic group because it is not a member of the economic group with environment id = {1}", clientId, environmentID);
                    LOG.warn(logMessage);
                }
            }
        }

        for (String clientId : clientsCoreIdsToRemove) {
            if (StringUtils.isNotEmpty(clientId) && members.contains(clientId)) {
                com.technisys.omnichannel.core.environments.EnvironmentHandler.deleteEnvironmentClient(environmentID, clientId);
            }
        }
        return Administration.getInstance().readEnvironment(environmentID);
    }

    public Environment addHoldingClients(String idTransaction, int environmentID, List<String> newClientsCoreIds)
            throws BackendConnectorException, IOException {
        String logMessage;
        Environment env = Administration.getInstance().readEnvironment(environmentID);
        List<String> members = env.getClients().stream().map(client -> client.getIdClient()).collect(Collectors.toList());

        if (env.isDeleted()) {
            logMessage= MessageFormat.format("New members will not be added to the economic group because it is deleted (idEnvironment={0}).", environmentID);
            LOG.info(logMessage);
            throw new BackendConnectorException(ReturnCodes.NO_ACTIVE_ENVIRONMENTS.toString());
        }

        List<Product> products = Administration.getInstance().listEnvironmentProducts(environmentID, true);
        for (String newClientId : newClientsCoreIds) {
            if (StringUtils.isNotEmpty(newClientId) && !members.contains(newClientId)) {
                com.technisys.omnichannel.core.environments.EnvironmentHandler.addEnvironmentClient(environmentID,
                        ClientHandler.readClient(newClientId));
                for (Product clientProduct : RubiconCoreConnectorC.listProducts(idTransaction, newClientId, PRODUCT_TYPES)) {
                    if (!products.contains(clientProduct)) {
                        products.add(clientProduct);
                    }
                }
            } else {
                logMessage= MessageFormat.format("Client with product group id ={0} is already member of the economic group with environment id = {1}", newClientId, environmentID);
                LOG.warn(logMessage);
            }
        }
        Administration.getInstance().modifyEnvironmentProducts(environmentID, products);
        return Administration.getInstance().readEnvironment(environmentID);
    }

    public int addClientToEnvironment(SqlSession session, String idTransaction, User user, InvitationCode invtationCode, List<Integer> groups) throws IOException, BackendConnectorException {
        if (invtationCode.getProductGroupId() != null) {
            Environment environment = Administration.getInstance().readEnvironmentByProductGroupId(session, invtationCode.getProductGroupId());
            return addClientToEnvironment(session, environment, idTransaction, user, invtationCode.getProductGroupId(), invtationCode.getAdministrationScheme(), invtationCode.getSignatureQty(), invtationCode.getAccessType(), invtationCode.getSignatureLevel(), groups);
        } else {
            Environment environment = Administration.getInstance().readEnvironment(session, invtationCode.getIdEnvironment());
            return addClientToEnvironment(session, environment, idTransaction, user, null, invtationCode.getAdministrationScheme(), invtationCode.getSignatureQty(), invtationCode.getAccessType(), invtationCode.getSignatureLevel(), groups);
        }

    }

    public void addClientToEnvironment(SqlSession session, String idTransaction, User user, String productGroupId, String administrationScheme, Integer signatureQty, String role, List<Integer> groups, String signatureLevel) throws IOException, BackendConnectorException {
        Environment environment = Administration.getInstance().readEnvironmentByProductGroupId(session, productGroupId);
        addClientToEnvironment(session, environment, idTransaction, user, productGroupId, administrationScheme, signatureQty, role, signatureLevel, groups);
    }

    private int addClientToEnvironment(SqlSession session, Environment environment, String idTransaction, User user, String productGroupId, String administrationScheme, Integer signatureQty, String role, String signatureLevel, List<Integer> groups) throws IOException, BackendConnectorException {
        int idEnvironment;
        if (environment == null) {
            LOG.info("The account is not registered, it will proceed to create it. (productGroupId={}).", productGroupId);
            environment = createEnvironment(session, idTransaction, productGroupId, administrationScheme, (signatureQty != null && signatureQty > 0 ? signatureQty : 1), user.getDocumentType(), user.getDocumentNumber());
            groups = null; // Si me pasaron un grupo y no existe en omnichannel el ambiente de seguro que el grupo es invalido, lo vacio para que no moleste el resto del proceso
        } else {
            LOG.info("The account is already registered (idEnvironment={}).", environment.getIdEnvironment());
        }

        Environment e = Administration.getInstance().readEnvironment(session, environment.getIdEnvironment());

        idEnvironment = e.getIdEnvironment();
        if (!e.isDeleted()) {
            //Grupo del usuario
            if (StringUtils.isNotBlank(role)) {
                LOG.info("Proceed to create a group to the user with a role of {}.", role);

                createEnvironmentGroup(session, user.getIdUser(), user.getFullName(), environment.getIdEnvironment(), role, environment.getEnvironmentType());
            } else if (groups != null && !groups.isEmpty()) {
                String groupsMessage = StringUtils.join(groups, ",");
                LOG.info("The user will be associated with the groups ({}).", groupsMessage);

                AccessManagementHandlerFactory.getHandler().addUserToGroups(session, user.getIdUser(), groups);
            }

            // Asocio el usuario al ambiente
            EnvironmentUser envUser = new EnvironmentUser();
            envUser.setIdEnvironment(environment.getIdEnvironment());
            envUser.setIdUser(user.getIdUser());
            envUser.setIdUserStatus(UserStatus.USER_STATUS_ACTIVE);
            envUser.setSignatureLevel(signatureLevel);
            envUser.setCreationDate(new Date());
            envUser.setEnabledChannels(StringUtils.join(ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "enrollment.newUser.enabledChannels"), ","));
            envUser.setMobileNumber(user.getMobileNumber());
            envUser.setEmail(user.getEmail());

            LOG.info("Associating the user with the environment (idEnvironment={}).", environment.getIdEnvironment());
            Administration.getInstance().addUserToEnvironment(session, envUser);

            // Cargamos la lista de widgets por defecto para el tipo de ambiente
            List<String> widgetIds = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "environments.desktopLayout." + environment.getEnvironmentType());

            LOG.info("Associating default widgets for the type of environment: {}", Arrays.asList(widgetIds));

            Widget[] widgets = new Widget[widgetIds.size()];
            int idx = 0;
            Widget widget;
            for (String widgetId : widgetIds) {
                widget = new Widget();
                widget.setId(widgetId);
                widget.setColumn(1);
                widget.setRow(idx + 1);
                widgets[idx] = widget;
                idx++;
            }

            LayoutHandler.saveLayout(session, user.getIdUser(), environment.getIdEnvironment(), widgets);

            // Creamos los caps por defecto para el usuario en el ambiente
            String defaultCapFrequency = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "core.capFrequency.list").get(0);

            List<CapForEnvironment> forUsers = new ArrayList<>();
            for (String channel : CoreUtils.getEnabledChannels()) {
                forUsers.add(new CapForUser(environment.getIdEnvironment(), channel, user.getIdUser(), defaultCapFrequency));
            }
            LimitsHandlerFactory.getHandler().createCaps(session, forUsers);
        } else {
            LOG.info("The environment will not synchronize because it is deleted (idEnvironment={}).", environment.getIdEnvironment());
        }

        return idEnvironment;
    }

    public int createEnvironmentGroup(SqlSession sqlSession, String idUser, String name, int idEnvironment, String permissionSet, String environmentType) throws IOException {
        Group group = new Group();
        group.setName(name);
        group.setIdEnvironment(idEnvironment);

        List<GroupPermission> permissions = new ArrayList();

        // Target NONE
        List<String> tempPermissions = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "client.permissions.defaults");
        tempPermissions.addAll(ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "client.permissions." + permissionSet + ".none"));
        for (String idPermission : tempPermissions) {
            permissions.add(new GroupPermission(group.getIdGroup(), Authorization.TARGET_NONE, idPermission));
        }

        // Generic Targets
        List<String> targets = Arrays.asList(new String[]{Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY, Constants.PRODUCT_PA_KEY, Constants.PRODUCT_PF_KEY, Constants.PRODUCT_PI_KEY, Constants.PRODUCT_TC_KEY});
        for (String target : targets) {
            tempPermissions = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "client.permissions." + permissionSet + ".all" + target);
            for (String idPermission : tempPermissions) {
                permissions.add(new GroupPermission(group.getIdGroup(), Authorization.GENERIC_PRODUCT_TYPE_PREFIX + target, idPermission));
            }
        }

        List<GroupPermission> profileFilteredPermissions = new ArrayList();
        int environmentTypesCount = ProfileDataHandler.profileEnvironmentTypesCount(environmentType);
        //Si existen perfiles para el tipo de ambiente, debemos filtrar los permisos,
        //y agregar solamente los permisos que los perfiles nos permitan.
        //Tambien se agregan los permisos por defecto configurados en la key 'client.permissions.defaults'
        if (environmentTypesCount > 0) {
            List<String> profilePermissions = ProfileDataHandler.getEnvironmentTypePermissions(environmentType);
            List<String> clientPermissionsDefaults = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "client.permissions.defaults");

            for (GroupPermission permission : permissions) {
                if ((profilePermissions != null && profilePermissions.contains(permission.getIdPermission()))
                        || (clientPermissionsDefaults != null && clientPermissionsDefaults.contains(permission.getIdPermission()))) {
                    profileFilteredPermissions.add(permission);
                }
            }
        }

        AccessManagementHandlerFactory.getHandler().createGroup(sqlSession, group, Arrays.asList(idUser), (environmentTypesCount > 0) ? profileFilteredPermissions : permissions);

        return group.getIdGroup();
    }
}
