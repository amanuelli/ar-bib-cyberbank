/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.client.ReturnCodes;
import static com.technisys.omnichannel.client.activities.files.DownloadFileAsStreamActivity.InParams.ID_FILE;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.File;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.files.FilesHandler;
import java.io.IOException;

public class DownloadFileAsStreamActivity extends Activity {

    public static final String ID = "files.downloadStream";

    public interface InParams {

        String ID_FILE = "idFile";
    }

    public interface OutParams {

        String FILE_NAME = "fileName";
        String FILE = "file";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            int idFile = request.getParam(ID_FILE, Integer.class);

            File file = FilesHandler.getInstance().readTransactionFileWithContents(idFile, request.getIdUser(), request.getIdEnvironment());

            response.putItem(OutParams.FILE_NAME, file.getFileName());
            response.putItem(OutParams.FILE, file);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
