/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.communications;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;
import java.util.List;

/**
 *
 * @author ahernandez
 */
@DocumentedActivity("Mark communication as unread")
public class MarkAsUnReadActivity extends Activity {

    public static final String ID = "communications.mark.unread";
    
    public interface InParams {
        @DocumentedParam(type =  Integer.class, description =  "Communication ID")
        String COMMUNICATION_ID = "idCommunication";
    }

    public interface OutParams {
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            int idCommunication = request.getParam(MarkAsReadActivity.InParams.COMMUNICATION_ID, Integer.class);
            Communication comm =  NotificationsHandlerFactory.getHandler().read(idCommunication, request.getIdUser(), Communication.TRANSPORT_DEFAULT);
            if (comm == null) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

            List<Communication> thread = NotificationsHandlerFactory.getHandler().readThread(comm.getIdThread(), request.getIdUser());
            for (Communication communication : thread) {
                // marco cada comunicación de las que hay hasta ahora en el thread como procesada
                NotificationsHandlerFactory.getHandler().markRecipientAsUnprocessed(communication.getIdCommunication(), request.getIdUser(), Communication.TRANSPORT_DEFAULT);
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
}
