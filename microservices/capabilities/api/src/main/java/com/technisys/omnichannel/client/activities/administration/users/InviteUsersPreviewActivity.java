/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

/**
 * Inviting users to an environment: Previous step
 */
@DocumentedActivity("Invite user preview")
public class InviteUsersPreviewActivity extends Activity {

    public static final String ID = "administration.users.invite.preview";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Document of the user that will be invited to environment (encrypted value)")
        String DOCUMENT = "document";
        @DocumentedParam(type = String.class, description = "Country of issuance of the document")
        String DOCUMENT_COUNTRY = "docCountry";
        @DocumentedParam(type = String.class, description = "Document's type")
        String DOCUMENT_TYPE = "docType";
        @DocumentedParam(type = String.class, description = "Document's number")
        String DOCUMENT_NUMBER = "docNumber";
        @DocumentedParam(type = String.class, description = "Email of the user that will be invited to environment")
        String EMAIL = "email";
        @DocumentedParam(type = String.class, description = "Mobile phone of the user that will be invited to environment")
        String MOBILE_PHONE = "mobilePhone";
        @DocumentedParam(type = String.class, description = "First name of the user that will be invited to environment")
        String FIRST_NAME = "firstName";
        @DocumentedParam(type = String.class, description = "Last name of the user that will be invited to environment")
        String LAST_NAME = "lastName";
        @DocumentedParam(type = String.class, description = "Role. e.g. transactions, readonly, login")
        String ROLE = "role";
        @DocumentedParam(type = String.class, description = "Signature level")
        String SIGNATURE_LEVEL = "signatureLevel";
        @DocumentedParam(type = String.class, description = "Grupos")
        String GROUPS = "groups";

    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return InviteUsersActivity.validateFields(request);
    }

}
