package com.technisys.omnichannel.client.activities.administration.common;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.activities.administration.medium.ReadUserDetailsActivity;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Marcelo Bruno
 */
public class UserDetails {

    private UserDetails() {}

    public static Map<String, Object> buildUserDetails(EnvironmentUser envUser, String idUser, int idEnvironment, String environmentType, String lang) throws IOException {
        Map<String, Object> detailsMap = new HashMap<>();

        detailsMap.put(ReadUserDetailsActivity.OutParams.GROUPS, Permissions.listUIPermissions(idEnvironment, lang));
        detailsMap.put(ReadUserDetailsActivity.OutParams.USER, AccessManagementHandlerFactory.getHandler().getUser(idUser));
        detailsMap.put(ReadUserDetailsActivity.OutParams.PRODUCTS, Permissions.listProducts(idEnvironment, lang));
        detailsMap.put(ReadUserDetailsActivity.OutParams.CAPS, Channels.listCaps(envUser, idEnvironment, environmentType, idUser));
        detailsMap.put(ReadUserDetailsActivity.OutParams.CURRENCY, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency"));
        detailsMap.put(ReadUserDetailsActivity.OutParams.TOP_AMOUNT, Channels.getChannelAll(idUser, idEnvironment, environmentType));
        detailsMap.put(ReadUserDetailsActivity.OutParams.SIGNATURE_LEVEL, envUser.getSignatureLevel());
        detailsMap.put(ReadUserDetailsActivity.OutParams.HAS_MASSIVE_ENABLED, !Authorization.hasPermission(idUser, idEnvironment, null, Constants.ADMINISTRATION_VIEW_PERMISSION));
        detailsMap.put(ReadUserDetailsActivity.OutParams.USER_ENV_STATUS, envUser.getIdUserStatus());
        detailsMap.put(ReadUserDetailsActivity.OutParams.DISPATCHER, envUser.isDispatcher());

        return detailsMap;
    }
    
}
