package com.technisys.omnichannel.client.domain.fields;

import com.technisys.omnichannel.activities.other.CreateTransactionTemplateActivity;
import com.technisys.omnichannel.activities.other.SaveDraftTransactionActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedTypeParam;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.forms.fields.FileField;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class InputfileField extends FormField implements FileField{

    /**
     * @return the maxFiles
     */
    public int getMaxFiles() {
        return maxFiles;
    }

    /**
     * @param maxFiles the maxFiles to set
     */
    public void setMaxFiles(int maxFiles) {
        this.maxFiles = maxFiles;
    }
    
    @DocumentedTypeParam(description = "Internationalized messages to display when no field value is set")
    private Map<String, String> requiredErrorMap;
    
    @DocumentedTypeParam(description = "Maximum file size to be uploaded")
    private int maxFileSizeMB;
    
    @DocumentedTypeParam(description = "Set if file uploader allow multiple files")
    private boolean allowMultiple;
    
    @DocumentedTypeParam(description = "Maximum total file size to be uploaded")
    private int maxTotalFileSizeMB;
    
    @DocumentedTypeParam(description = "The maximum number of files that the field can handle")
    private int maxFiles;
    
    @DocumentedTypeParam(description = "List of accepted file types")
    private List<String> acceptedFileTypes;
    
    @Override
    public String getLabelDefault() {
       return I18nFactory.getHandler().getMessage("core.notApply", ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "core.default.lang", "es"));
    }
    
    public Map<String, String> getRequiredErrorMap() {
        return requiredErrorMap;
    }

    public void setRequiredErrorMap(Map<String, String> requiredErrorMap) {
        this.requiredErrorMap = requiredErrorMap;
    }

    public void addRequiredError(String lang, String value){
        if(requiredErrorMap == null){
            requiredErrorMap = new LinkedHashMap<>();
        }
        requiredErrorMap.put(lang, value);
    }

    /**
     * @return the allowMultiple
     */
    public boolean isAllowMultiple() {
        return allowMultiple;
    }

    /**
     * @param allowMultiple the allowMultiple to set
     */
    public void setAllowMultiple(boolean allowMultiple) {
        this.allowMultiple = allowMultiple;
    }

    /**
     * @return the acceptedFileTypes
     */
    public List<String> getAcceptedFileTypes() {
        return acceptedFileTypes;
    }

    /**
     * @param acceptedFileTypes the acceptedFileTypes to set
     */
    public void setAcceptedFileTypes(List<String> acceptedFileTypes) {
        this.acceptedFileTypes = acceptedFileTypes;
    }

    /**
     * @return the maxFileSizeMB
     */
    public int getMaxFileSizeMB() {
        return maxFileSizeMB;
    }

    /**
     * @param maxFileSizeMB the maxFileSizeMB to set
     */
    public void setMaxFileSizeMB(int maxFileSizeMB) {
        this.maxFileSizeMB = maxFileSizeMB;
    }

    /**
     * @return the maxTotalFileSizeMB
     */
    public int getMaxTotalFileSizeMB() {
        return maxTotalFileSizeMB;
    }

    /**
     * @param maxTotalFileSizeMB the maxTotalFileSizeMB to set
     */
    public void setMaxTotalFileSizeMB(int maxTotalFileSizeMB) {
        this.maxTotalFileSizeMB = maxTotalFileSizeMB;
    }

    @Override
    public List<Integer> getFiles(String idActivity, Map<String, Object> parameters) {
        List<Integer> files = new ArrayList();
        Object obj = parameters.get(idField);
        if (SaveDraftTransactionActivity.ID.equals(idActivity) || CreateTransactionTemplateActivity.ID.equals(idActivity)){
            Object paramValue = parameters.get("transactionData");
            obj = RequestParamsUtils.getValue(paramValue, Map.class).get(idField);
        }
        
        if(obj instanceof List){
            List value = (List<Map>)obj;
            value.forEach(file -> {
               files.add((Integer)((Map)file).get("fileId"));
            });
        }
        return files;
    }
}
