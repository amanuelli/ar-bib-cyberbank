/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

public class CyberbankCoreConnectorResponse<T> {

    public static final int RESPONSE_OK = 0;

    private int code = RESPONSE_OK;
    private T data;
    private CyberbankCoreError error;

    public CyberbankCoreConnectorResponse() {

    }

    public CyberbankCoreConnectorResponse(int code) {
        this.code = code;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public CyberbankCoreError getError() {
        return error;
    }

    public void setError(CyberbankCoreError error) {
        this.error = error;
    }
}
