/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client;

import com.technisys.omnichannel.client.activities.enrollment.AssociateVerifyStep1Activity;
import com.technisys.omnichannel.client.activities.enrollment.AssociateVerifyStep2Activity;
import com.technisys.omnichannel.client.activities.recoverpassword.RecoverPasswordStep1Activity;
import com.technisys.omnichannel.client.activities.session.legacy.LoginWithSecondFactorAndPasswordStep1Activity;
import com.technisys.omnichannel.client.activities.session.legacy.LoginWithSecondFactorAndPasswordStep2Activity;
import com.technisys.omnichannel.client.activities.session.oauth.ValidateOauthPasswordActivity;
import com.technisys.omnichannel.core.domain.ReturnCode;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Diego Curbelo
 */
public class ReturnCodes extends com.technisys.omnichannel.ReturnCodes {

    /*
     * Hay ciertos errores que si bien se distinguen a nivel de programación y logs, de cara al usuario no se diferencian;
     * en general por motivos de seguridad.
     * Ej: "usuario no existe" y "contraseña incorrecta".
     * Para estos casos se definen mapas de relación de errores: "Error original -> Error a mostrar"
     */
    
    private static final Map<String, Map<ReturnCode, ReturnCode>> GROUPINGS = new HashMap<>();
    
    static {
        Map<ReturnCode, ReturnCode> codes = new HashMap<>();
        codes.put(USER_DOESNT_EXIST,                           OK);
        codes.put(USER_DISABLED,                               OK);
        codes.put(CHANNEL_NOT_ENABLED_FOR_USER_IN_ENVIRONMENT, OK);
        codes.put(NO_ACTIVE_ENVIRONMENTS,                      OK);

        GROUPINGS.put(LoginWithSecondFactorAndPasswordStep1Activity.ID, codes);
        GROUPINGS.put(AssociateVerifyStep1Activity.ID, codes);
     
        codes = new HashMap<>();
        codes.put(USER_DOESNT_EXIST,                                API_INVALID_PASSWORD);
        codes.put(USER_DISABLED,                                    API_INVALID_PASSWORD);
        codes.put(CHANNEL_NOT_ENABLED_FOR_USER_IN_ENVIRONMENT,      API_INVALID_PASSWORD);
        codes.put(NO_ACTIVE_ENVIRONMENTS,                           NO_ACTIVE_ENVIRONMENTS);
        // PASSWORD
        codes.put(INVALID_PASSWORD_CREDENTIAL,                      API_INVALID_PASSWORD);
        codes.put(PASSWORD_LOCKED,                                  API_INVALID_PASSWORD);
        codes.put(INVALID_PASSWORD_CREDENTIAL_CAPTCHA_REQUIRED,     API_INVALID_PASSWORD_CAPTCHA_REQUIRED);
        GROUPINGS.put(LoginWithSecondFactorAndPasswordStep2Activity.ID, codes);
        GROUPINGS.put(AssociateVerifyStep2Activity.ID, codes);
        GROUPINGS.put(ValidateOauthPasswordActivity.ID, codes);
     
        codes = new HashMap<>();
        codes.put(USER_DOESNT_EXIST,                                API_INVALID_USER_OR_PASSWORD);
        codes.put(USER_DISABLED,                                    API_INVALID_USER_OR_PASSWORD);
        codes.put(CHANNEL_NOT_ENABLED_FOR_USER_IN_ENVIRONMENT,      API_INVALID_USER_OR_PASSWORD);
        codes.put(NO_ACTIVE_ENVIRONMENTS,                           API_INVALID_USER_OR_PASSWORD);
        // PASSWORD
        codes.put(INVALID_PASSWORD_CREDENTIAL,                      API_INVALID_USER_OR_PASSWORD);
        codes.put(PASSWORD_LOCKED,                                  API_INVALID_USER_OR_PASSWORD);
        codes.put(INVALID_PASSWORD_CREDENTIAL_CAPTCHA_REQUIRED,     API_INVALID_USER_OR_PASSWORD_CAPTCHA_REQUIRED);
     
        codes = new HashMap<>();
        codes.put(USER_DOESNT_EXIST,                                API_INVALID_USER_OR_SECOND_FACTOR);
        codes.put(USER_DISABLED,                                    API_INVALID_USER_OR_SECOND_FACTOR);
        codes.put(CHANNEL_NOT_ENABLED_FOR_USER_IN_ENVIRONMENT,      API_INVALID_USER_OR_SECOND_FACTOR);
        codes.put(NO_ACTIVE_ENVIRONMENTS,                           API_INVALID_USER_OR_SECOND_FACTOR);
        // PIN
        codes.put(INVALID_PIN_CREDENTIAL,                           API_INVALID_USER_OR_SECOND_FACTOR);
        codes.put(PIN_LOCKED,                                       API_INVALID_USER_OR_SECOND_FACTOR);
        codes.put(INVALID_PIN_CREDENTIAL_CAPTCHA_REQUIRED,          API_INVALID_USER_OR_SECOND_FACTOR_CAPTCHA_REQUIRED);
        // OTP
        codes.put(INVALID_OTP_CREDENTIAL,                           API_INVALID_USER_OR_SECOND_FACTOR);
        codes.put(INVALID_OTP_CREDENTIAL_CAPTCHA_REQUIRED,          API_INVALID_USER_OR_SECOND_FACTOR_CAPTCHA_REQUIRED);
        GROUPINGS.put(RecoverPasswordStep1Activity.ID, codes);
        
    }

    public static final ReturnCode BACKEND_SERVICE_ERROR = new ReturnCode(ReturnCode.Group.Backend, 1, ReturnCode.Category.Error);

    // Códigos de retorno
    // La numeracion debe comenzar en 500 (para el cliente)
    public static final ReturnCode INVALID_ENVIRONMENT_SCHEME_DATA = new ReturnCode(ReturnCode.Group.API, 503, ReturnCode.Category.Error);
    public static final ReturnCode ERROR_SENDING_TRANSACTION_NOTIFICATIONS = new ReturnCode(ReturnCode.Group.API, 504, ReturnCode.Category.Info);
    public static final ReturnCode INVALID_ENVIRONMENT_SCHEME = new ReturnCode(ReturnCode.Group.API, 505, ReturnCode.Category.Error);
    public static final ReturnCode INVITE_USER_WRONG_INFO = new ReturnCode(ReturnCode.Group.API, 506, ReturnCode.Category.Error);
    public static final ReturnCode ERROR_SENDING_INVITATION = new ReturnCode(ReturnCode.Group.API, 507, ReturnCode.Category.Error);

    public static final ReturnCode INVALID_INVITATION_CODE = new ReturnCode(ReturnCode.Group.API, 508, ReturnCode.Category.Warn);
    public static final ReturnCode INVITATION_CODE_EXPIRED = new ReturnCode(ReturnCode.Group.API, 509, ReturnCode.Category.Warn);
    public static final ReturnCode INVITATION_CODE_ALREADY_USED = new ReturnCode(ReturnCode.Group.API, 510, ReturnCode.Category.Warn);
    public static final ReturnCode INVITATION_CODE_CANCELED = new ReturnCode(ReturnCode.Group.API, 511, ReturnCode.Category.Warn);
    public static final ReturnCode INVALID_INVITATION_CODE_DATA = new ReturnCode(ReturnCode.Group.API, 512, ReturnCode.Category.Warn);
    public static final ReturnCode USER_ALREADY_EXISTS = new ReturnCode(ReturnCode.Group.API, 513, ReturnCode.Category.Error);
    public static final ReturnCode TOTAL_AMOUNT_CAN_NOT_BE_ZERO = new ReturnCode(ReturnCode.Group.API, 514, ReturnCode.Category.Warn);

    @Override
    public Map<ReturnCode, ReturnCode> getReturnCodeGrouping(String idActivity) {
        Map<ReturnCode, ReturnCode> result = super.getReturnCodeGrouping(idActivity);
        if (GROUPINGS.containsKey(idActivity)) {
            result.putAll(GROUPINGS.get(idActivity));
        }
        return result;
    }

}
