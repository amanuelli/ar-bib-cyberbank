/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.forms.preloaders;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Form;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.forms.FormPreloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class OpenCreditLetterPreloader implements FormPreloader {

    private static final Logger log = LoggerFactory.getLogger(OpenCreditLetterPreloader.class);

    public interface OutParams {
        String PAYER_NAME = "payerName";
        String DOCUMENT_PERIOD = "documentPeriod";
    }

    @Override
    public Map<String, Object> execute(Form form, Request request) {
        Map<String, Object> formData = new HashMap<String, Object>();
        try {
            if (!User.USER_ANONYMOUS.equals(request.getIdUser())) {
                Environment e = Administration.getInstance().readEnvironment(request.getIdEnvironment());
                if (e.getName() != null) {
                    formData.put(OutParams.PAYER_NAME, e.getName());
                }
                // Segun el ESRE 155P.02 el valor por defecto es de 21
                formData.put(OutParams.DOCUMENT_PERIOD, Integer.valueOf(21));
            }
        } catch (IOException ex) {
            log.warn("Error al recuperar al nombre del ambiente", ex);
        }
        return formData;
    }
}