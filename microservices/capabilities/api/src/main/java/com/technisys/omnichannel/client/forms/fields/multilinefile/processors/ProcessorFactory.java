package com.technisys.omnichannel.client.forms.fields.multilinefile.processors;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ProcessorFactory {

    private static final Logger log = LoggerFactory.getLogger(ProcessorFactory.class);

    private ProcessorFactory() { }

    public static Processor getProcessorSubType(String fieldSubType) {
        try {
            return (Processor) Class.forName("com.technisys.omnichannel.client.forms.fields.multilinefile.processors." +
                    StringUtils.capitalize(fieldSubType) + "Processor").newInstance();
        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException e) {
            String logString = String.format("Could not find processor class for input file of subtype: %s", fieldSubType);
            log.info(logString);
        }
        return null;
    }
}
