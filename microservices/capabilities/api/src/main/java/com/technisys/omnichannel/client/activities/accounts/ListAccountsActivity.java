/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.accounts;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreAccountConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.utils.CurrencyUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.client.Constants.PRODUCT_READ_PERMISSION;

@DocumentedActivity("List user's accounts")
public class ListAccountsActivity extends Activity {

    public static final String ID = "accounts.list";

    public interface OutParams {
        @DocumentedParam(type = Account.class, description = "Array of account's objects")
        String ACCOUNTS = "accounts";
        @DocumentedParam(type = Integer.class, description = "Total accounts balance")
        String EQUIVALENT_TOTAL_BALANCE = "equivalentTotalBalance";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Double equivalentTotalBalance = Double.valueOf(0);
            
            List<String> productTypes = Arrays.asList(Constants.PRODUCT_CA_KEY, Constants.PRODUCT_CC_KEY);
            List<Account> accounts = getAccountList(request, productTypes);

            response.putItem(OutParams.ACCOUNTS, accounts);

            String masterCurrency = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency");
            
            for (Account account : accounts) {
                equivalentTotalBalance += !masterCurrency.equals(account.getCurrency()) ? CurrencyUtils.convert(masterCurrency, account.getCurrency(), account.getBalance(), request.getIdTransaction(), account.getClient().getIdClient()) : account.getBalance();
            }
            response.putItem(OutParams.EQUIVALENT_TOTAL_BALANCE, equivalentTotalBalance);
            
            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    public static List<Account> getAccountList(Request request, List<String> productTypes) throws IOException, BackendConnectorException {
        Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

        List<String> idPermissions = new ArrayList<>();
        idPermissions.add(PRODUCT_READ_PERMISSION);

        List<Product> environmentProducts = Administration.getInstance().listAuthorizedProducts(request.getIdUser(), request.getIdEnvironment(), idPermissions, productTypes);

        List<Account> coreAccounts = CoreAccountConnectorOrchestrator.list(request.getIdTransaction(), environment.getClients(), productTypes);

        List<Account> authorizedAccounts = new ArrayList<>();

        ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

        for (Product environmentProduct : environmentProducts) {
            for (Account coreAccount : coreAccounts) {
                if (coreAccount.getIdProduct().equals(environmentProduct.getIdProduct())) {
                    Account account = new Account(environmentProduct);
                    account.setShortLabel(pLabeler.calculateShortLabel(environmentProduct));
                    account.setLabel(pLabeler.calculateLabel(environmentProduct));
                    account.setBalance(coreAccount.getBalance());
                    //Se agregan los permisos necesarios para limitar el comportamiento de la pagina
                    Map<String, Boolean> permissions = new HashMap<>();
                    if (Constants.PRODUCT_CC_KEY.equals(account.getProductType())) {
                        permissions.put(Constants.REQUEST_CHECKBOOK_PERMISSION, Authorization.hasPermission(request.getIdUser(), request.getIdEnvironment(), account.getIdProduct(), Constants.REQUEST_CHECKBOOK_PERMISSION));
                    }

                    permissions.put(Constants.TRANSFERS_PERMISSION, Authorization.hasAtLeastOnePermission(request.getIdUser(), request.getIdEnvironment(), account.getIdProduct(), new String[] { Constants.TRANSFER_FOREIGN_PERMISSION, Constants.TRANSFER_INTERNAL_PERMISSION, Constants.TRANSFER_LOCAL_PERMISSION }));
                    account.setPermissions(permissions);
                    account.setStatus(coreAccount.getStatus());
                    authorizedAccounts.add(account);
                    break;
                }
            }
        }

        return authorizedAccounts;
    }
}