/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.groups;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

@DocumentedActivity("Block/Unblock groups preview")
public class BlockUnblockGroupsPreviewActivity extends Activity {

    public static final String ID = "administration.groups.blockunblock.preview";

    public interface InParams {
        
        @DocumentedParam(type = Boolean.class, description = "Operation to execute, true for block, false for unblock")
        String BLOCKED = "blocked";

        @DocumentedParam(type = Integer[].class, description = "Group's list ids to block/unblock")        
        String GROUP_ID_LIST = "groupIdList";
        
        @DocumentedParam(type = String[].class, description = "Group's list names to block/unblock")        
        String GROUP_NAME_LIST = "groupNameList";
    }

    public interface OutParams {
        
    }

    @Override
    public Response execute(Request request) throws ActivityException {      
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return BlockUnblockGroupsActivity.validateFields(request);
    }

}
