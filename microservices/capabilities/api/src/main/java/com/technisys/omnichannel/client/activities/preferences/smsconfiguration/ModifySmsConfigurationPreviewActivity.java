/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.smsconfiguration;


import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

/**
 *
 */
public class ModifySmsConfigurationPreviewActivity extends Activity {

    public static final String ID = "preferences.smsconfiguration.modify.preview";
    
    public interface InParams {
    }

    public interface OutParams {
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        ModifySmsConfigurationActivity modifySms = new ModifySmsConfigurationActivity();
        Map<String, String> result = modifySms.validate(request);
        return result;
    }
}
