/*
 *  Copyright 2015 Manentia Software.
 *
 *  This software component is the intellectual property of 5IT S.R.L. (Manentia Software).
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  http://www.manentiasoftware.com
 */
package com.technisys.omnichannel.client.domain.fields;

import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.FormField;
import com.technisys.omnichannel.core.domain.FormFieldWithCap;
import com.technisys.omnichannel.core.domain.TicketPrintableField;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.NumberUtils;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;


/**
 * Represents a form field of type bank selector
 * 
 * @author ?
 */
public class PaycreditcardamountField extends FormFieldWithCap implements TicketPrintableField {

    protected String displayType;
    protected transient Map<String, String> placeholderMap;
    protected transient Map<String, String> requiredErrorMap;
    protected transient Map<String, String> invalidErrorMap;

    protected transient List<String> currencyList;
    protected String idCreditCardSelector;

    protected transient Map<String, String> extraLabels;

    public PaycreditcardamountField() {
        useForTotalAmount = true;
    }

    public Map<String, String> getExtraLabels() {
        return extraLabels;
    }

    public void setExtraLabels(Map<String, String> extraLabels) {
        this.extraLabels = extraLabels;
    }

    public void addExtraLabel(String key, String value) {
        if (extraLabels == null) {
            extraLabels = new HashMap<>();
        }
        extraLabels.put(key, value);
    }

    public String getDisplayType() {
        return displayType;
    }

    public void setDisplayType(String displayType) {
        this.displayType = displayType;
    }

    public Map<String, String> getPlaceholderMap() {
        return placeholderMap;
    }

    public void setPlaceholderMap(Map<String, String> placeholderMap) {
        this.placeholderMap = placeholderMap;
    }

    public void addPlaceholder(String lang, String value) {
        if (placeholderMap == null) {
            placeholderMap = new LinkedHashMap<>();
        }
        placeholderMap.put(lang, value);
    }

    public Map<String, String> getRequiredErrorMap() {
        return requiredErrorMap;
    }

    public void setRequiredErrorMap(Map<String, String> requiredErrorMap) {
        this.requiredErrorMap = requiredErrorMap;
    }

    public void addRequiredError(String lang, String value) {
        if (requiredErrorMap == null) {
            requiredErrorMap = new LinkedHashMap<>();
        }
        requiredErrorMap.put(lang, value);
    }

    public Map<String, String> getInvalidErrorMap() {
        return invalidErrorMap;
    }

    public void setInvalidErrorMap(Map<String, String> invalidErrorMap) {
        this.invalidErrorMap = invalidErrorMap;
    }

    public void addInvalidError(String lang, String value) {
        if (invalidErrorMap == null) {
            invalidErrorMap = new LinkedHashMap<>();
        }
        invalidErrorMap.put(lang, value);
    }

    public String getIdCreditCardSelector() {
        return idCreditCardSelector;
    }

    public void setIdCreditCardSelector(String idCreditCardSelector) {
        this.idCreditCardSelector = idCreditCardSelector;
    }

    public List<String> getCurrencyList() {
        return currencyList;
    }

    public void setCurrencyList(List<String> currencyList) {
        this.currencyList = currencyList;
    }

    public void addCurrency(String currency) {
        if (currencyList == null) {
            currencyList = new ArrayList<>();
        }
        currencyList.add(currency);
    }

    @Override
    public List<String> getDisplayDependsOn() {
        List<String> result = super.getDependencies();
        if (result == null) {
            result = new ArrayList<>();
        }

        if (!result.contains(idCreditCardSelector)) {
            result.add(idCreditCardSelector);
        }

        return result;
    }

    @Override
    public List<Amount> getAmounts(Map<String, Object> parameters) {
        List<Amount> amounts = new ArrayList();

        Amount amount = RequestParamsUtils.getValue(parameters.get(idField), Amount.class);

        if (amount != null) {
            amounts.add(amount);
        }
        return amounts;
    }

    @Override
    public boolean isUseForTotalAmount() {
        return true;
    }

    @Override
    public String getPrintableFieldValue(FormField formField, Map<String, Object> transactionData) {
        String value = "";
        String lang = transactionData.get("lang").toString();
        Amount amount = (Amount) transactionData.get(formField.getIdField());
        if(amount != null) {
            value = I18nFactory.getHandler().getMessage("currency.label." + amount.getCurrency(), "en") + " " + NumberUtils.formatDecimal(amount.getQuantity(), lang);
        }
        return value;
    }

}
