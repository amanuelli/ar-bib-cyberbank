/*
 *
 *  *  Copyright 2020 Technisys.
 *  *
 *  *  This software component is the intellectual property of Technisys S.A.
 *  *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  *
 *  *  https://www.technisys.com
 *
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author Cristobal Meneses
 */
public class LoanType implements Serializable {

    private String loanTypeId;
    private String loanTypeDescription;

    public LoanType() {
    }

    public LoanType(String loanTypeId, String loanTypeDescription) {
        this.loanTypeId = loanTypeId;
        this.loanTypeDescription = loanTypeDescription;
    }

    public String getLoanTypeId() {
        return loanTypeId;
    }

    public void setLoanTypeId(String loanTypeId) {
        this.loanTypeId = loanTypeId;
    }

    public String getLoanTypeDescription() {
        return loanTypeDescription;
    }

    public void setLoanTypeDescription(String loanTypeDescription) {
        this.loanTypeDescription = loanTypeDescription;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LoanType loanType = (LoanType) o;
        return loanTypeId.equals(loanType.loanTypeId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(loanTypeId, loanTypeDescription);
    }
}
