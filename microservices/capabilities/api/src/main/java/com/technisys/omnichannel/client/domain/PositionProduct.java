/*
 *  Copyright 2014 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnector;
import com.technisys.omnichannel.client.utils.ProductUtils;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/*
 * @author ?
 */
public class PositionProduct extends ClientProduct {

    private static final Logger log = LoggerFactory.getLogger(PositionProduct.class);
    public final String EXTRA_CURRENCY = "currency";
    public static final String EXTRA_BALANCE = "balance";
    public static final String EXTRA_PENDING_BALANCE = "pendingBalance";

    protected transient Map<String, Object> extras;
    protected transient Map<String, Double> grouping;

    protected String currency;

    protected boolean canRead;

    public PositionProduct(Map<String, String> fieldsMap) {
        super(fieldsMap);

        currency = ProductUtils.getCurrency(extraInfo);

        extras = new LinkedHashMap<>();

        //extra info para posicion
        switch (StringUtils.defaultString(ProductUtils.getProductType(extraInfo))) {
            case Constants.PRODUCT_CA_KEY:
            case Constants.PRODUCT_CC_KEY:
            case Constants.PRODUCT_PF_KEY:
                extras.put(EXTRA_CURRENCY, currency);
                extras.put(EXTRA_BALANCE, fieldsMap.get("balance"));
                extras.put(EXTRA_PENDING_BALANCE, fieldsMap.get("availableBalance"));
                break;
            case Constants.PRODUCT_PA_KEY:
            case Constants.PRODUCT_PI_KEY:
                extras.put("currency", currency);
                extras.put("totalAmount", RubiconCoreConnector.parseDouble(fieldsMap.get("totalAmount")));
                extras.put("interests", RubiconCoreConnector.parseDouble(fieldsMap.get("interests")));
                extras.put("pendingBalance", RubiconCoreConnector.parseDouble(fieldsMap.get("pendingBalance")));
                extras.put("numberOfPaidFees", fieldsMap.get("numberOfPaidFees"));
                extras.put("numberOfFees", fieldsMap.get("numberOfFees"));
                extras.put("nextDueDate", RubiconCoreConnector.parseDate(fieldsMap.get("nextDueDate")));
                extras.put("paymentAmount", RubiconCoreConnector.parseDouble(fieldsMap.get("paymentAmount")));
                break;
            case Constants.PRODUCT_TC_KEY:
                extras.put("availableBalance", RubiconCoreConnector.parseDouble(fieldsMap.get("availableBalance")));
                extras.put("creditLimitCurrency", currency);
                extras.put("payment", RubiconCoreConnector.parseDouble(fieldsMap.get("balance2")));
                extras.put("expirationDate", RubiconCoreConnector.parseDate(fieldsMap.get("expirationDate")));
                extras.put("closingDate", RubiconCoreConnector.parseDate(fieldsMap.get("closingDate")));
                extras.put("minimumPayment", RubiconCoreConnector.parseDouble(fieldsMap.get("minimunPayment2")));
                break;
            default:
                String logString = String.format("Unexpected StringUtils.defaultString(ProductUtils.getProductType(extraInfo)) value %s", StringUtils.defaultString(ProductUtils.getProductType(extraInfo)));
                log.error(logString);
                break;
        }
    }

    public void setGrouping(Map<String, Double> grouping) {
        this.grouping = grouping;
    }

    public Map<String, Double> getGrouping() {
        return grouping;
    }

    public void addGrouping(String currency, Double balance) {
        grouping.put(currency, balance);
    }

    public Map<String, Object> getExtras() {
        return extras;
    }

    public void addExtra(String key, Object value) {
        extras.put(key, value);
    }

    public boolean isCanRead() {
        return canRead;
    }

    public void setCanRead(boolean canRead) {
        this.canRead = canRead;
    }
}
