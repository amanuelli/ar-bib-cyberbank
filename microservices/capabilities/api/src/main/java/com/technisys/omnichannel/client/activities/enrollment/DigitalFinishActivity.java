/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.enrollment;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.attemptscounter.AttemptsCounterHandler;
import com.technisys.omnichannel.core.notifications.legacy.SMSCommunicationsDispatcher;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.documenttypes.DocumentTypesHandler;
import com.technisys.omnichannel.core.domain.AttemptsCounter;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.utils.ReCaptcha;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 */
@ExchangeActivity
public class DigitalFinishActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(DigitalFinishActivity.class);

    public static final String ID = "enrollment.digital.finish";

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {

        String DOCUMENT_COUNTRY = "docCountry";
        String DOCUMENT_TYPE = "docType";
        String DOCUMENT_NUMBER = "docNumber";
    }

    public interface OutParams {

        String CHANNEL_SENT = "channelSent";
        String AUTOMATIC = "automatic";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        // No importando el error que de siempre retorno OK, para evitar informar clientes
        response.setReturnCode(ReturnCodes.OK);

        try {
            boolean automatic = ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "enrollment.digital.automatic");

            response.putItem(OutParams.CHANNEL_SENT, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "invitation.notification.transport"));
            response.putItem(OutParams.AUTOMATIC, automatic);

            String docCountry = request.getParam(InParams.DOCUMENT_COUNTRY, String.class);
            String docType = request.getParam(InParams.DOCUMENT_TYPE, String.class);
            String docNumber = request.getParam(InParams.DOCUMENT_NUMBER, String.class);

            ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), docCountry, docType, docNumber, null);
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(docCountry, docType, docNumber);

            // Se impide el enrollment digital a clientes ya existentes en el banco, para no interferir en la lógica de la confianza del cliente
            if (client == null || user != null) {
                String msg = user != null ? "ya exitente en Omnichannel" : "no existente en backend";
                log.warn("Se intento generar una invitación para un documento {}: {} {} {}", msg, docCountry, docType, docNumber);

                return response;
            }

            List<ClientEnvironment> clients = CoreCustomerConnectorOrchestrator.listClients(request.getIdTransaction(), docCountry, docType, docNumber);

            // No es posible realizar un enrollment digital si el usuario no tiene cuentas en el banco
            if (clients == null || clients.isEmpty()) {
                log.warn("Se intento generar una invitación para un documento que no tiene cuentas asociadas en backend: {} {} {}", docCountry, docType, docNumber);

                return response;
            }

            // Valido que los datos (por lo menos de mail y mobil) sean válidos
            if (automatic && (client.getMobileNumber() == null || StringUtils.isEmpty(client.getEmail()) || !ValidationUtils.validateEmailPattern(client.getEmail()))) {
                log.warn("Se intento generar una invitación para un cliente con los datos de mail y/o celular inválidos: {} {} {}", docCountry, docType, docNumber);
                return response;
            }

            //-
            // Si llegue aca es porque pasaron todas las validaciones previas, es seguro realizar la invitacion
            //-
            if (automatic) {
                // Se asocia a la primera cuenta que retorna el Backend
                String productGroupId = clients.get(0).getProductGroupId();

                AttemptsCounter counter = AttemptsCounterHandler.incrementAttemptsCounter(Constants.ATTEMPTS_DIGITAL_ENROLLMENT, docCountry + "|" + docType + "|" + docNumber);
                if (counter.getAttempts() <= AttemptsCounterHandler.getMaxAttemptsToBlock(Constants.ATTEMPTS_DIGITAL_ENROLLMENT)) {
                    InvitationCode invitationCode = new InvitationCode();
                    invitationCode.setStatus(InvitationCode.STATUS_NOT_USED);

                    invitationCode.setEmail(client.getEmail());
                    invitationCode.setMobileNumber(client.getMobileNumber());
                    invitationCode.setFirstName(client.getFirstName());
                    invitationCode.setLastName(client.getLastName());

                    invitationCode.setProductGroupId(productGroupId);

                    invitationCode.setDocumentCountry(docCountry);
                    invitationCode.setDocumentType(docType);
                    invitationCode.setDocumentNumber(docNumber);

                    invitationCode.setBackendUser(true);

                    invitationCode.setLang(request.getLang());
                    String sendChannel = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "invitation.notification.transport");
                    invitationCode.setChannelSent(sendChannel);

                    invitationCode.setAccessType(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "enrollment.digital.userRole"));
                    invitationCode.setSignatureLevel("A");

                    InvitationCode prevoiusInvitationCode = InvitationCodesHandler.readPrevoiusInvitationCode(docCountry, docType, docNumber, productGroupId);
                    if(prevoiusInvitationCode != null) {
                        invitationCode.setSsnid(prevoiusInvitationCode.getSsnid());
                    }

                    String invitationCodePlain = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
                    InvitationCodesHandler.createInvitationCode(invitationCode, invitationCodePlain);

                    // Envío el mail con la invitacion
                    HashMap<String, String> fillers = new HashMap<>();
                    fillers.put("ENVIRONMENT", request.getEnvironmentName());
                    String subject = I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode." + sendChannel + ".subject", request.getLang(), fillers);

                    fillers.put("BASE_URL", ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, Constants.BASE_URL_KEY));

                    String enrollmentUrl = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "administration.users.invite.enrollmentUrl", fillers);
                    log.info("URL de inicio de invitacion: {}", enrollmentUrl);

                    fillers.put("ENROLLMENT_URL", enrollmentUrl);

                    fillers.put("FULL_NAME", invitationCode.getFirstName() + " " + invitationCode.getLastName());
                    fillers.put("INVITATION_CODE", invitationCodePlain);
                    fillers.put("EXPIRATION_DATE", invitationCode.getExpiration());
                    String body = I18nFactory.getHandler().getMessage("enrollment.requestInvitationCode.invitationCode." + sendChannel + ".body", request.getLang(), fillers);

                    switch (sendChannel) {
                        case Constants.TRANSPORT_SMS:
                            NotificationsHandlerFactory.getHandler().sendAsyncSMS(invitationCode.getMobileNumber(), body, true); 
                            break;
                        case Constants.TRANSPORT_MAIL:
                            NotificationsHandlerFactory.getHandler().sendAsyncEmail(subject, body, Arrays.asList(invitationCode.getEmail()), null, request.getLang(), true);
                            break;
                        default:
                            throw new ActivityException(ReturnCodes.ERROR_SENDING_INVITATION, "No se encuentra bien configurado el transporte de la notificacion para invitaciones");
                    }
                } else {
                    log.warn("Se alcanzo el maximo numero de intentos para generar codigos de invitación para el documento: {} {} {}", docCountry, docType, docNumber);
                }

            } else {
                // TODO: Trámite en BO

            }

            // Elimino el exchange Token
            ExchangeTokenHandler.delete(request.getParam(EXCHANGE_TOKEN, String.class));

        } catch (IOException ex) {
            log.error("Error de base de datos", ex);
        } catch (MessagingException | ClassNotFoundException | IllegalAccessException | InstantiationException ex) {
            log.error("Error mientras se enviaba el mensaje", ex);
        } catch (Exception ex) {
            log.error("Error interno", ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String docCountry = request.getParam(InParams.DOCUMENT_COUNTRY, String.class);
            String docType = request.getParam(InParams.DOCUMENT_TYPE, String.class);
            String docNumber = request.getParam(InParams.DOCUMENT_NUMBER, String.class);

            if (StringUtils.isBlank(docNumber)) {
                result.put(InParams.DOCUMENT_NUMBER, "enrollment.requestInvitationCode.document.document.empty");
            } else if (docNumber.length() > 25) {
                result.put(InParams.DOCUMENT_NUMBER, "administration.users.docNumberMaxLengthExceeded");
            }

            List<String> validCountryCodes = CountryCodesHandler.listCountryCodes();
            if (StringUtils.isBlank(docCountry)) {
                result.put(InParams.DOCUMENT_COUNTRY, "enrollment.requestInvitationCode.document.country.empty");
            } else if (!validCountryCodes.contains(docCountry)) {
                result.put(InParams.DOCUMENT_COUNTRY, "enrollment.requestInvitationCode.document.country.invalid");
            }

            List<String> validDocumentTypes = DocumentTypesHandler.listDocumentTypes(docCountry);
            if (StringUtils.isBlank(docType)) {
                result.put(InParams.DOCUMENT_TYPE, "enrollment.requestInvitationCode.document.type.empty");
            } else if (result.get(InParams.DOCUMENT_COUNTRY) == null && !validDocumentTypes.contains(docType)) {
                result.put(InParams.DOCUMENT_TYPE, "enrollment.requestInvitationCode.document.type.invalid");
            }
            // Control del captcha
            boolean isMobile = IBRequest.Channel.phonegap.equals(request.getChannel());
            if (!isMobile && result.isEmpty()) {
                Credential captchaCredential = request.getCredential(Credential.CAPTCHA_CREDENTIAL);
                String captcha = captchaCredential != null ? captchaCredential.getValue() : null;
                if (StringUtils.isBlank(captcha)) {
                    result.put(Credential.CAPTCHA_CREDENTIAL, "enrollment.requestInvitationCode.captcha.required");
                } else if (!ReCaptcha.isValid(captcha)) {
                    result.put("captchaReset", ""); // Se agarra en el formulario y se resetea el captcha en la pagina
                    result.put(Credential.CAPTCHA_CREDENTIAL, "enrollment.requestInvitationCode.captcha.invalid");
                }
            }

        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
