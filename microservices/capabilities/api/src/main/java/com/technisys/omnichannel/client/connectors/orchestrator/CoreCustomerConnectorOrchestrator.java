/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorO;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCustomerConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.Address;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.CyberbankCoreCustomer;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.Identification;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.Province;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.enums.MaritalStatus;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.enums.PersonType;
import com.technisys.omnichannel.client.connectors.cyberbank.utils.DocumentTypeResolver;
import com.technisys.omnichannel.client.domain.ClientEnvironment;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.domain.JobInformation;
import com.technisys.omnichannel.core.countrycodes.CountryCodesHandler;
import com.technisys.omnichannel.core.domain.PhoneNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class CoreCustomerConnectorOrchestrator extends CoreConnectorOrchestrator {

    private static final Logger LOG = LoggerFactory.getLogger(CoreCustomerConnectorOrchestrator.class);

    /**
     * Add customer orchestrator
     *
     * @param idTransaction
     * @param name
     * @param lastName
     * @param email
     * @param phone
     * @param mobilePhone
     * @param documentNumber
     * @param documentType
     * @param residenceCountry
     * @param nationality
     * @param civilStatus
     * @param gender
     * @param segment
     * @param shipAccountStatus
     * @param address
     * @param jobInformation
     * @return Customer's identification
     * @throws BackendConnectorException
     */
    public static String add(String idTransaction, String name, String lastName, String email, //NOSONAR
                             String phone, PhoneNumber mobilePhone, String documentNumber, String documentType, String residenceCountry,
                             String nationality, String civilStatus, String gender, String segment,
                             boolean shipAccountStatus, String dateOfBirth,
                             com.technisys.omnichannel.client.domain.Address address, JobInformation jobInformation) throws BackendConnectorException {
        try {
            String customerIdentification = null;
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    customerIdentification = rubiconAddCustomer(idTransaction, 0, name, lastName, email,
                            address.toJSON().toString(), phone,
                            mobilePhone != null ? mobilePhone.toString() : "", documentNumber, documentType,
                            residenceCountry, residenceCountry, civilStatus, gender, jobInformation.getOccupation(), segment, "", shipAccountStatus);
                    break;
                case CYBERBANK_CONNECTOR:
                    String coreNationality = CountryCodesHandler.getCoreCodeByAlpha3(nationality);
                    Calendar dateBirth = Calendar.getInstance();
                    dateBirth.setTimeInMillis(Long.parseLong(dateOfBirth));
                    customerIdentification = cyberbankAddCustomer(name, lastName, email, address,
                            phone, mobilePhone, documentNumber, documentType, coreNationality, gender,
                            jobInformation, dateBirth.getTime());
                    break;
                default:
                    throw new BackendConnectorException("No core connector configured");
            }

            return customerIdentification;
        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     * Read person orchestrator
     *
     * @param idTransaction
     * @param documentCountry
     * @param documentType
     * @param documentNumber
     * @param productGroupId
     * @return Person's information
     * @throws BackendConnectorException
     */
    public static ClientUser read(String idTransaction, String documentCountry, String documentType, String documentNumber, String productGroupId) throws BackendConnectorException {
        ClientUser client = null;
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                client = rubiconReadPerson(idTransaction, documentCountry, documentType, documentNumber, productGroupId);
                break;
            case CYBERBANK_CONNECTOR:
                client = cyberbankReadPerson(documentType, documentNumber);
                break;
            default:
                throw new BackendConnectorException("No core connector configured");

        }
        return client;
    }

    /**
     * Read client environment orchestrator
     *
     * @param idTransaction
     * @param idClient
     * @return Client Environment
     */
    public static ClientEnvironment readClientEnvironment(String idTransaction, String idClient, String documentType, String documentNumber) {
        ClientEnvironment clientEnvironment = null;
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                try {
                    clientEnvironment = rubiconReadClientEnvironment(idTransaction, idClient);
                } catch (BackendConnectorException ex) {
                    return null;
                }
                break;
            case CYBERBANK_CONNECTOR:
                try {
                    clientEnvironment = cyberbankReadClientEnvironment(documentType, documentNumber);
                } catch (BackendConnectorException ex) {
                    return null;
                }
                break;
            default:
                LOG.error("No core connector configured");

        }
        return clientEnvironment;
    }

    public static List<ClientEnvironment> listClients(String idTransaction, String documentCountry, String documentType, String documentNumber) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return RubiconCoreConnectorC.listClients(idTransaction, documentCountry, documentType, documentNumber);
                case CYBERBANK_CONNECTOR:
                    return cyberbankListClients(documentType, documentNumber);
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException e) {
            throw new BackendConnectorException(e);
        }
    }

    public static void updateClientAddress(String idTransaction, int idClient, com.technisys.omnichannel.client.domain.Address address, com.technisys.omnichannel.client.domain.Address mailingAddress) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                RubiconCoreConnectorC.updateClientAddress(idTransaction, idClient, address, mailingAddress);
                break;
            case CYBERBANK_CONNECTOR:
                throw new BackendConnectorException("Not implemented");
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }


    // <editor-fold defaultstate="collapsed" desc="Add customer">

    /**
     * Call to Core Simulator
     *
     * @param idTransaction
     * @param idClient
     * @param name
     * @param lastName
     * @param email
     * @param homeAddress
     * @param phone
     * @param mobilePhone
     * @param documentNumber
     * @param documentType
     * @param residenceCountry
     * @param nationality
     * @param civilStatus
     * @param gender
     * @param occupation
     * @param segment
     * @param correspondenceAddress
     * @param shipAccountStatus
     * @return
     * @throws BackendConnectorException
     */
    private static String rubiconAddCustomer(String idTransaction, int idClient, String name, String lastName, String email, String homeAddress, String phone, String mobilePhone, String documentNumber, String documentType, String residenceCountry, String nationality, String civilStatus, String gender, String occupation, String segment, String correspondenceAddress, boolean shipAccountStatus) throws BackendConnectorException {//NOSONAR

        int customerIdentification = RubiconCoreConnectorO.addClient(idTransaction, idClient, name, lastName, email, homeAddress,
                phone, mobilePhone, documentNumber, documentType, residenceCountry, nationality, civilStatus, gender,
                occupation, segment, correspondenceAddress, shipAccountStatus);

        return String.valueOf(customerIdentification);
    }

    /**
     * Call to Cyberbank Core
     *
     * @param name
     * @param lastName
     * @param email
     * @param homeAddress
     * @param phone
     * @param mobilePhone
     * @param documentNumber
     * @param documentType
     * @param nationality
     * @param gender
     * @param jobInformation
     * @return
     * @throws BackendConnectorException
     */
    private static String cyberbankAddCustomer(String name, String lastName, String email, com.technisys.omnichannel.client.domain.Address homeAddress, //NOSONAR
                                               String phone, PhoneNumber mobilePhone, String documentNumber, String documentType, String nationality,
                                               String gender, JobInformation jobInformation, Date dateOfBirth) throws CyberbankCoreConnectorException {

        CyberbankCoreCustomer customer = new CyberbankCoreCustomer();

        List<Identification> identificationList = new ArrayList<>();
        List<Address> addressList = new ArrayList<>();

        Identification identification = new Identification();
        identification.setNumber(documentNumber);
        identification.setType(DocumentTypeResolver.resolve(documentType));

        identificationList.add(identification);

        Address address = new Address();
        if (homeAddress != null) {
            address.setStreetName(homeAddress.getAddressLine1() + " " + homeAddress.getAddressLine2());
            address.setPostalCode(homeAddress.getZipcode());
            address.setProvince(new Province(homeAddress.getFederalState()));
            address.setPostalCode(homeAddress.getZipcode());
        }


        addressList.add(address);

        customer.setFirstName(name);
        customer.setLastName(lastName);
        customer.setEmail(email);
        customer.setLandLineNumber(phone);
        customer.setEmail(email);
        customer.setDocumentNumber(documentNumber);
        customer.setIdentificationList(identificationList);
        customer.setDocumentType(documentType);
        customer.setNationality(nationality);
        customer.setMaritalStatus(MaritalStatus.CASADO);
        customer.setGender(gender);
        customer.setEmployer(jobInformation.getCompanyName());
        customer.setOccupation(jobInformation.getOccupation());
        Date entryDate = jobInformation.getEntryDate();

        customer.setEmploymentStartDate(CyberbankCoreConnector.formatDate(entryDate));
        customer.setAnnualHouseholdIncome(jobInformation.getIncome().getQuantity());
        customer.setPersonType(PersonType.INDIVIDUAL);
        customer.setAddressList(addressList);
        customer.setMobileNumber(mobilePhone.toString());
        customer.setMobilePhoneCountryCode(mobilePhone.getCountryCode());
        customer.setMobilePhoneAreaCode(mobilePhone.getAreaCode());
        customer.setMobilePhoneNumber(mobilePhone.getNumber());
        customer.setBirthDate(dateOfBirth);

        CyberbankCoreConnectorResponse<String> response = CyberbankCustomerConnector.create(customer);

        if (CyberbankCoreConnectorResponse.RESPONSE_OK == response.getCode()) {
            return response.getData();
        }

        return String.valueOf(0);
    }
    // </editor-fold>

    // </editor-fold>
    // <editor-fold defaultstate="collapsed" desc="Read person">

    /**
     * Call to Rubicon core
     *
     * @param idTransaction
     * @param documentCountry
     * @param documentType
     * @param documentNumber
     * @param productGroupId
     * @return Person's information
     * @throws BackendConnectorException
     */
    private static ClientUser rubiconReadPerson(String idTransaction, String documentCountry, String documentType, String documentNumber, String productGroupId) throws BackendConnectorException {
        return RubiconCoreConnectorC.readPerson(idTransaction, documentCountry, documentType, documentNumber, productGroupId);
    }

    /**
     * Call to Cyberbank Core
     *
     * @param documentType
     * @param documentNumber
     * @return Person's information
     * @throws BackendConnectorException
     */
    private static ClientUser cyberbankReadPerson(String documentType, String documentNumber) throws BackendConnectorException {

        try {
            CyberbankCoreConnectorResponse<ClientUser> response = CyberbankCustomerConnector.read(documentType, documentNumber);
            return response.getData();
        } catch (CyberbankCoreConnectorException e) {
            return null;
        }

    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="List client accounts">
    private static List<ClientEnvironment> cyberbankListClients(String documentType, String documentNumber) throws BackendConnectorException, CyberbankCoreConnectorException {
        CyberbankCoreConnectorResponse<ClientUser> coreConnectorResponse = CyberbankCustomerConnector.read(documentType, documentNumber);
        if (coreConnectorResponse.getCode() == 0) {
            ClientUser clientUser = coreConnectorResponse.getData();

            ClientEnvironment clientEnvironment = new ClientEnvironment();
            clientEnvironment.setProductGroupId(clientUser.getProductGroupId());
            clientEnvironment.setAccountName(clientUser.getFirstName() + " " + clientUser.getLastName());
            clientEnvironment.setCorrespondenceAddress(clientUser.getEmail());

            ArrayList<ClientEnvironment> result = new ArrayList<>();
            result.add(clientEnvironment);

            return result;
        }

        return new ArrayList<>();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="Read client environment">

    /**
     * Call to Rubicon core
     *
     * @param idTransaction
     * @param idClient
     * @return Client Environment
     * @throws BackendConnectorException
     */
    private static ClientEnvironment rubiconReadClientEnvironment(String idTransaction, String idClient) throws BackendConnectorException {
        return RubiconCoreConnectorC.readClient(idTransaction, idClient);
    }

    /**
     * Call to Cyberbank Core
     *
     * @param documentType
     * @param documentNumber
     * @return ClientEnvironment
     * @throws BackendConnectorException
     */
    private static ClientEnvironment cyberbankReadClientEnvironment(String documentType, String documentNumber) throws BackendConnectorException {
        try {
            CyberbankCoreConnectorResponse<ClientUser> coreConnectorResponse = CyberbankCustomerConnector.read(documentType, documentNumber);
            if (coreConnectorResponse.getCode() == 0) {
                ClientUser clientUser = coreConnectorResponse.getData();

                ClientEnvironment clientEnvironment = new ClientEnvironment();
                clientEnvironment.setProductGroupId(clientUser.getProductGroupId());
                clientEnvironment.setAccountName(clientUser.getFirstName() + " " + clientUser.getLastName());
                clientEnvironment.setCorrespondenceAddress(clientUser.getEmail());
                return clientEnvironment;
            }

            return null;
        } catch (BackendConnectorException | CyberbankCoreConnectorException e) {
            return null;
        }
    }
    // </editor-fold>
}
