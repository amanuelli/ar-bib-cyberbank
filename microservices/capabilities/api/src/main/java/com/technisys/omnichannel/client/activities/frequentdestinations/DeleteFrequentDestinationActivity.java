/*
 *  Copyright 2010 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.frequentdestinations;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.frequentdestinations.FrequentDestinationsHandler;
import com.technisys.omnichannel.core.utils.DBUtils;
import org.apache.ibatis.session.SqlSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import org.apache.commons.lang3.StringUtils;

/**
 *
 */
public class DeleteFrequentDestinationActivity extends Activity {

    public static final String ID = "frequentdestinations.delete";
    
    public interface InParams {
        String FD_ID = "id";
    }

    public interface OutParams {
    }
    
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);        
        try {            
            String frequentDestinationsIdsParam = request.getParam(InParams.FD_ID, String.class);
            if(frequentDestinationsIdsParam != null) {
                String ids[] = frequentDestinationsIdsParam.split(",");
                try(SqlSession session = DBUtils.getInstance().openWriteSession()){
                    for(String id : ids) {                    
                        FrequentDestinationsHandler.deleteFrequentDestination(session, Integer.parseInt(id.trim()), request.getIdEnvironment());
                    }
                    session.commit();
                }
                response.getData().put("frequentDestination", frequentDestinationsIdsParam);
            }
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        
        String ids = request.getParam(InParams.FD_ID, String.class);
        if(StringUtils.isEmpty(ids)) {
            result.put("NO_FIELD", "frequentDestinations.form.validation.id.empty");
        }

        return result;
    }
}