/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.utils.ObjectPool;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.rubicon.core.creditcards.CreditCards;
import com.technisys.omnichannel.rubicon.core.creditcards.WSCreditCards;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.BindingProvider;
import java.net.URL;

import static com.technisys.omnichannel.client.connectors.RubiconCoreConnector.WEBSERVICES_URL;
/**
 *
 * @author fpena
 */
public class WSCreditCardsFactory implements ObjectPool.ObjectPoolFactory<CreditCards>{

    private static final Logger log = LoggerFactory.getLogger(WSCreditCardsFactory.class);

    @Override
    public CreditCards create() {

        String wsdlSufix = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "backend.webservices.credicards.wsdl", "");
        String endpoint = WEBSERVICES_URL + "/WSCreditCards";

        CreditCards port = null;
        WSCreditCards service;
        boolean useRemoteWSDL = !"".equals(wsdlSufix);

        try {
            if (useRemoteWSDL) {
                URL url = new URL(WEBSERVICES_URL + wsdlSufix);
                service = new WSCreditCards(url);
            } else {
                service = new WSCreditCards();
            }

            port = service.getCreditCardsPort();
            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
        } catch (Exception e) {
            log.warn("Error creating WSCreditCards port : " + e.getMessage(), e);
        }
        
        return port;
    }
    
}
