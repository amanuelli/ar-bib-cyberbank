/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankGeneralServicesConnector;
import com.technisys.omnichannel.client.domain.Bank;
import com.technisys.omnichannel.core.utils.CacheUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CoreGeneralServicesOrchestrator extends CoreConnectorOrchestrator {

    private static final String BANK_LIST_CACHE_KEY = "backend.banks";

    public static List<Bank> listBanks(boolean listInternationals) throws BackendConnectorException {
        try {
            List<Bank> banks = (List<Bank>) CacheUtils.get(BANK_LIST_CACHE_KEY);

            if (banks != null) {
                return banks;
            }

            banks = new ArrayList<>();

            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    // For core banking simulator we return only UY local banks

                    Bank bandes = new Bank();
                    bandes.setBankName("BANDES");
                    bandes.setCode("bandes");

                    Bank bbva = new Bank();
                    bbva.setBankName("BBVA");
                    bbva.setCode("bbva");

                    Bank hsbc = new Bank();
                    hsbc.setBankName("HSBC");
                    hsbc.setCode("hsbc");

                    Bank santander = new Bank();
                    santander.setBankName("Santander");
                    santander.setCode("santander");

                    Bank scotiabank = new Bank();
                    scotiabank.setBankName("Scotiabank");
                    scotiabank.setCode("scotiabank");

                    banks.add(bandes);
                    banks.add(bbva);
                    banks.add(hsbc);
                    banks.add(santander);
                    banks.add(scotiabank);

                    break;
                case CYBERBANK_CONNECTOR:
                    CyberbankCoreConnectorResponse<List<Bank>> response;

                    response = CyberbankGeneralServicesConnector.listCorrespondentBanks(listInternationals);

                    banks = response.getData();
                    break;
                default:
                    throw new BackendConnectorException("No core connector configured");
            }

            CacheUtils.put(BANK_LIST_CACHE_KEY, banks);
            return banks;
        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new BackendConnectorException(e);
        }
    }

}
