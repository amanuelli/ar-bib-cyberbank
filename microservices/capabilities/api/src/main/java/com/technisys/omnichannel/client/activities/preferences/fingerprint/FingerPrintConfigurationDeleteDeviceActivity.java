/* 
 * Copyright 2017 Technisys. 
 * 
 * This software component is the intellectual property of Technisys S.A. 
 * You are not allowed to use, change or distribute it without express written consent from its author. 
 * 
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.fingerprint;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.SessionException;
import com.technisys.omnichannel.core.session.Session;
import com.technisys.omnichannel.core.session.SessionHandlerFactory;
import com.technisys.omnichannel.core.session.SessionKind;
import java.io.IOException;

import static com.technisys.omnichannel.client.utils.HeadersUtils.buildRequestHeaders;

/**
 * Delete FingerPrint Configuration of a device
 */
@DocumentedActivity("Delete FingerPrint Configuration")
public class FingerPrintConfigurationDeleteDeviceActivity extends Activity {

    public static final String ID = "preferences.fingerprint.delete";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Id of the session to delete")
        String ID_SESSION = "idSession";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            Session currentSession = SessionHandlerFactory.getInstance().readSession(request.getCredential("accessToken").getValue(), buildRequestHeaders(request));

            String idSession = request.getParam(InParams.ID_SESSION, String.class);

            if (currentSession.getIdSession().equals(idSession)) {
                SessionHandlerFactory.getInstance().updateSessionKind(SessionKind.NORMAL, idSession);
            } else {
                SessionHandlerFactory.getInstance().deleteSession(idSession);
            }
        } catch (SessionException e) {
            // Si no existe la session no me preocupo por actualizarla/borrarla
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }

        response.setReturnCode(ReturnCodes.OK);

        return response;
    }

}
