/*
 *  Copyright 2014 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import java.util.ArrayList;
import java.util.List;

/*
 * @author ?
 */
public class PositionProductListByType {
    
    private String type;
    private String currency;
    private double total;
    private List<PositionProductListByCurrency> productsByCurrencyList;
    
    public PositionProductListByType(){
        productsByCurrencyList = new ArrayList<>();
    }
    
    public String getType(){
        return type;
    }
    
    public void setType(String type){
        this.type = type;
    }
    
    public String getCurrency(){
        return currency;
    }
    
    public void setCurrency(String currency){
        this.currency = currency;
    }
    
    public double getTotal(){
        return total;
    }
    
    public void setTotal(double total){
        this.total = total;
    }
    
    public void addToTotal(double total){
        this.total += total;
    }
    
    public List<PositionProductListByCurrency> getProductsByCurrencyList(){
        return productsByCurrencyList;
    }
    
    public void setProductsByCurrencyList(List<PositionProductListByCurrency> productsByCurrencyList){
        this.productsByCurrencyList = productsByCurrencyList;
    }
    
    public void addProductsByCurrency(PositionProductListByCurrency productsByCurrency){
        productsByCurrencyList.add(productsByCurrency);
    }
}