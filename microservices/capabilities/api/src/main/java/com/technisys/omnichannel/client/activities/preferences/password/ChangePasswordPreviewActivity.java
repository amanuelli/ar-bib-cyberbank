/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.password;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

/**
 * Validations: 
 * <ul>
 *   <li>current password not empty</li>
 *   <li>current password incorrect</li>
 *   <li>new password not empty</li>
 *   <li>new password equal new password confirmation</li>
 *   <li>new password wrong format</li>
 *   <li>new password not equal as last</li>
 * </ul>
 *
*/
@DocumentedActivity("Change Password Preview")
public class ChangePasswordPreviewActivity extends Activity {

    public static final String ID = "preferences.changepassword.preview";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Current password")
        String PASSWORD = "_password";
        @DocumentedParam(type = String.class, description = "New password")
        String NEW_PASSWORD = "_newPassword";
        @DocumentedParam(type = String.class, description = "New password confirmation")
        String NEW_PASSWORD_CONFIRMATION = "_newPasswordConfirmation";
        
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return ChangePasswordActivity.validateFields(request);
    }

}
