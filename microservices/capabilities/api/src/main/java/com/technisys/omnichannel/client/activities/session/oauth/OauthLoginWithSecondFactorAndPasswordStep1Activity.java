package com.technisys.omnichannel.client.activities.session.oauth;

import com.technisys.omnichannel.annotations.AnonymousActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.activities.session.legacy.LoginWithSecondFactorAndPasswordStep1Activity;

/**
 * First login step, use this to initiate a login process. You must continue
 * with other steps until you have an <code>_accessToken</code>.
 */
@AnonymousActivity
@DocumentedActivity("Login step 1")
public class OauthLoginWithSecondFactorAndPasswordStep1Activity extends LoginWithSecondFactorAndPasswordStep1Activity {
    public static final String ID = "session.login.oauth.step1";

    public interface InParams {

        // Used by API, not this activity
        @DocumentedParam(type = String.class, description = "Username who perform the request, e.g. &quot;bbeall&quot;, &quot;32525648&quot;")
        String USERNAME = "username";

        @DocumentedParam(type = String.class, description = "Second authentication factor value, e.g. &quot;1111&quot;")
        String SECOND_FACTOR = "_secondFactor";

        // Used by frontend, not this activity
        @DocumentedParam(type = String.class, description = "Captcha user response, used after several failed login attempts", optional = true)
        String CAPTCHA = "captcha";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Default language, selected by the user, e.g. &quot;en&quot;")
        String LANGUAGE = "lang";

        @DocumentedParam(type = String.class, description = "Token required to call the next step on this process")
        String EXCHANGE_TOKEN = "_exchangeToken";

        @DocumentedParam(type = String.class, description = "Security seal picture (base 64 encoded)")
        String SECURITY_SEAL = "_securitySeal";

        @DocumentedParam(type = String.class, description = "Security seal description, e.g. &quot;A man smiling&quot;")
        String SECURITY_SEAL_ALT = "_securitySealAlt";

        @DocumentedParam(type = String.class, description = "First name of the user, e.g. &quot;Brittny&quot;")
        String USER_FIRST_NAME = "_userFirstName";

        @DocumentedParam(type = String.class, description = "Full name of the user, e.g. &quot;Brittny Beall&quot;")
        String USER_FULL_NAME = "_userFullName";

        @DocumentedParam(type = Integer.class, description = "Default environment identifier, selected by the user")
        String ENVIRONMENT = "environment";
    }
}
