/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.SessionException;
import com.technisys.omnichannel.core.session.Session;
import com.technisys.omnichannel.core.session.SessionHandler;
import com.technisys.omnichannel.core.session.SessionHandlerFactory;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import static com.technisys.omnichannel.client.utils.HeadersUtils.buildRequestHeaders;

public class GetSessionActivity extends Activity {

    public static final String ID = "session.get";

    public interface OutParams {
        String USER = "user";
        String LOGIN_CREDENTIALS = "loginCredentials";
        String ENVIRONMENT = "environment";
        String CREATED_AT = "createdAt";
        String MODIFIED_AT = "modifiedAt";
        String EXPIRES_AT = "expiresAt";
        String EXPIRED = "expired";
        String STATUS = "status";
        String LANG = "lang";
        String IP = "ip";
        String CHANNEL = "channel";
        String USER_AGENT = "userAgent";
        String SECONDS_TO_EXPIRE = "secondsToExpire";
        String ENVIRONMENTS = "environments";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            SessionHandler sessionHandler = SessionHandlerFactory.getInstance();
            Session session = sessionHandler.readSession(request.getCredential(Credential.ACCESS_TOKEN_CREDENTIAL).getValue(), buildRequestHeaders(request));

            Date modifiedAt = new Date();
            if(session.getModifiedAt() != null){
                modifiedAt = session.getModifiedAt();
            }
            Long sessionDuration = ConfigurationFactory.getInstance().getTimeInMillis(Configuration.PLATFORM, "session.duration");
            Date expiresAt = new Date(modifiedAt.getTime() + sessionDuration);
            Date now = new Date();
            boolean expired = expiresAt.before(now);
            long secondsToExpire = expired ? 0 : (expiresAt.getTime() - now.getTime()) / 1000;
            response.putItem(OutParams.EXPIRES_AT, expiresAt);
            response.putItem(OutParams.EXPIRED, expired);
            response.putItem(OutParams.SECONDS_TO_EXPIRE, secondsToExpire);


            List<Environment> environments = Administration.getInstance().listEnvironmentsWhereUserIsActive(request.getIdUser(), request.getChannel());
            HashMap<Integer, String> envs = new HashMap<>();
            environments.forEach((environment) -> {
                envs.put(environment.getIdEnvironment(), environment.getName());
            });

            response.putItem(OutParams.USER, session.getIdUser());
            response.putItem(OutParams.LOGIN_CREDENTIALS, session.getLoginCredentials());
            response.putItem(OutParams.ENVIRONMENT, request.getIdEnvironment());
            response.putItem(OutParams.CREATED_AT, session.getCreatedAt());
            response.putItem(OutParams.MODIFIED_AT, session.getModifiedAt());
            response.putItem(OutParams.STATUS, session.getStatus());
            response.putItem(OutParams.LANG, session.getLang());
            response.putItem(OutParams.IP, session.getIpAddress());
            response.putItem(OutParams.CHANNEL, session.getChannel());
            response.putItem(OutParams.USER_AGENT, session.getUserAgent());
            response.putItem(OutParams.ENVIRONMENTS, envs);
        } catch (SessionException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }
}
