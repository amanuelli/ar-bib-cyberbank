package com.technisys.omnichannel.client.activities.widgets;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.forms.FormMessagesHandler;
import com.technisys.omnichannel.core.forms.FormsHandler;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class PendingTransactionsActivity extends Activity {

    public static final String ID = "widgets.pendingTransactions";

    public interface OutParams {
        String TRANSACTIONS = "pendingTransactions";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            List<Map<String, Object>> transactions = this.getTransactions(request);

            response.putItem(OutParams.TRANSACTIONS, transactions);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
    
    private List<Map<String, Object>> getTransactions(Request request) throws IOException {
        List<Map<String, Object>> transactionFinalList = new ArrayList<>();
        List<String> availableStates = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "desktop.widgets.pendingTransactions.status");
        List<Transaction> transactionList = TransactionHandlerFactory.getInstance()
                .listTransactions(request.getIdUser(), request.getIdEnvironment(), null, null, availableStates.toArray(new String[availableStates.size()]),
                        "creation_date_time DESC", 1, ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "desktop.widgets.pendingTransactions.max"))
                .getElementList();

        Map<String, Form> formMap = new HashMap<>();
        for (Transaction transaction : transactionList) {
            if ((! Transaction.STATUS_SCHEDULED.equals(transaction.getIdTransactionStatus())) ||
                    (! transaction.isProgramed())) {
                transactionFinalList.add(getTransactionMap(request, formMap, transaction));
            }
        }

        return transactionFinalList;
    }
    
    public static Map<String, Object> getTransactionMap(Request request, Map<String, Form> formMap, Transaction transaction) throws IOException {
        Map<String, Object> transactionMap = new HashMap<>();
        
        Map<String, Double> transactionAmounts = new HashMap<>();
        Form form;
        List<FormField> formFields;
        List<Amount> amounts;
        String formId = transaction.getIdForm();

        if (formId != null) {
            form = formMap.get(formId);

            if (form == null) {
                form = FormsHandler.getInstance().readFormFull(transaction.getIdForm(), Transaction.STATUS_DRAFT.equals(transaction.getIdTransactionStatus()) ? null : transaction.getFormVersion());
                formMap.put(form.getIdForm(), form);
            }

            formFields = form.getFieldList();

            if (Form.FORM_TYPE_PROCESS.equals(form.getType())) {
                transaction.setActivityName(FormMessagesHandler.getInstance().readFormMessage(form.getIdForm(), form.getVersion(), "formName", request.getLang()));
            } else {
                I18n i18n = I18nFactory.getHandler();
                transaction.setActivityName(i18n.getMessageForUser("activities." + transaction.getIdActivity(), request.getIdUser()));
            }

            for (FormField formField : formFields) {
                if (formField instanceof FormFieldWithCap) {
                    amounts = ((FormFieldWithCap) formField).getAmounts(transaction.getData());
                    for (Amount amount : amounts) {
                        if (amount != null) {
                            if (transactionAmounts.containsKey(amount.getCurrency())) {
                                transactionAmounts.put(amount.getCurrency(), transactionAmounts.get(amount.getCurrency()) + amount.getQuantity());
                            } else {
                                transactionAmounts.put(amount.getCurrency(), amount.getQuantity());
                            }
                        }
                    }
                }
            }
        }

        transactionMap.put("transaction", transaction);
        transactionMap.put("transactionAmounts", transactionAmounts);

        return transactionMap;
    }
}
