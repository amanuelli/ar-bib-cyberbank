/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.users;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.orchestrator.CoreCustomerConnectorOrchestrator;
import com.technisys.omnichannel.client.domain.ClientUser;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.notifications.legacy.SMSCommunicationsDispatcher;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.domain.InvitationCode;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.invitationcodes.CodeGeneratorFactory;
import com.technisys.omnichannel.core.invitationcodes.InvitationCodesHandler;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.SecurityUtils;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.client.utils.StringUtils.stripHTML;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;

/**
 * Use this activity to generate an environment's invitation code.
 */
@DocumentedActivity("Generate environment's invitation code")
public class InviteUsersActivity extends Activity {

    private static final Logger log = LoggerFactory.getLogger(InviteUsersActivity.class);

    public static final String ID = "administration.users.invite.send";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Document of the user that will be invited to environment (encrypted value)")
        String DOCUMENT = "document";
        @DocumentedParam(type = String.class, description = "Country of issuance of the document")
        String DOCUMENT_COUNTRY = "docCountry";
        @DocumentedParam(type = String.class, description = "Document's type")
        String DOCUMENT_TYPE = "docType";
        @DocumentedParam(type = String.class, description = "Document's number")
        String DOCUMENT_NUMBER = "docNumber";

        @DocumentedParam(type = String.class, description = "Email of the user that will be invited to environment")
        String EMAIL = "email";
        @DocumentedParam(type = String.class, description = "Mobile phone of the user that will be invited to environment")
        String MOBILE_PHONE = "mobilePhone";
        @DocumentedParam(type = String.class, description = "First name of the user that will be invited to environment")
        String FIRST_NAME = "firstName";
        @DocumentedParam(type = String.class, description = "Last name of the user that will be invited to environment")
        String LAST_NAME = "lastName";

        @DocumentedParam(type = String.class, description = "Role. e.g. transactions, readonly, login")
        String ROLE = "role";
        @DocumentedParam(type = String.class, description = "Signature level")
        String SIGNATURE_LEVEL = "signatureLevel";
        @DocumentedParam(type = String.class, description = "Grupos")
        String GROUPS = "groups";
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String logMessage;
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }
            if (!ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "administration.users.invite.enabled")) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED, "No se encuentra activada la funcionalidad de invitar usuarios desde el Frontend");
            }

            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());

            String document = request.getParam(InParams.DOCUMENT, String.class);
            String[] tokens = StringUtils.split(SecurityUtils.decrypt(document), "|");
            String documentCountry = tokens[0];
            String documentType = tokens[1];
            String documentNumber = tokens[2];

            ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), documentCountry, documentType, documentNumber, null);
            User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(documentCountry, documentType, documentNumber);
            boolean isNewUser = client == null && user == null;
            logMessage= MessageFormat.format("Invitando usuario inexistente en Omnichannel ({0} {1} {2}).", documentCountry, documentType, documentNumber);
            log.info(logMessage);

            InvitationCode invitationCode = new InvitationCode();
            invitationCode.setStatus(InvitationCode.STATUS_NOT_USED);

            if (isNewUser) {

                String firstName = stripHTML(request.getParam(InParams.FIRST_NAME, String.class));
                String lastName = stripHTML(request.getParam(InParams.LAST_NAME, String.class));
                String email = request.getParam(InParams.EMAIL, String.class);
                String mobilePhone = request.getParam(InParams.MOBILE_PHONE, String.class);

                invitationCode.setEmail(email);
                invitationCode.setMobileNumber(mobilePhone);
                invitationCode.setFirstName(stripHTML(firstName));
                invitationCode.setLastName(stripHTML(lastName));

            } else {
                User auxClient = client != null ? client : user;
                invitationCode.setEmail(auxClient.getEmail());
                invitationCode.setMobileNumber(auxClient.getMobileNumber());
                invitationCode.setFirstName(auxClient.getFirstName());
                invitationCode.setLastName(auxClient.getLastName());
            }
            invitationCode.setProductGroupId(environment.getProductGroupId());

            invitationCode.setDocumentCountry(documentCountry);
            invitationCode.setDocumentType(documentType);
            invitationCode.setDocumentNumber(documentNumber);

            invitationCode.setBackendUser(client != null);

            invitationCode.setLang(request.getLang());
            String sendChannel = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "invitation.notification.transport");
            invitationCode.setChannelSent(sendChannel);

            String signatureLevel = request.getParam(InParams.SIGNATURE_LEVEL, String.class);
            if ("N".equals(signatureLevel)) {
                signatureLevel = null;
            }
            switch (StringUtils.defaultString(request.getEnvironmentAdminScheme())) {
                case Environment.ADMINISTRATION_SCHEME_SIMPLE:
                case Environment.ADMINISTRATION_SCHEME_MEDIUM:
                    String role = request.getParam(InParams.ROLE, String.class);
                    invitationCode.setSignatureLevel(signatureLevel);
                    invitationCode.setAccessType(role);
                    break;
                case Environment.ADMINISTRATION_SCHEME_ADVANCED:
                    Integer[] groups = request.getParam(InParams.GROUPS, Integer[].class);
                    invitationCode.setSignatureLevel(signatureLevel);
                    invitationCode.setGroups(StringUtils.join(groups, ","));
                    break;
                default:
                    break;
            }


            String invitationCodePlain = CodeGeneratorFactory.getCodeGenerator().generateUniqueCode();
            InvitationCodesHandler.createInvitationCode(invitationCode, invitationCodePlain);

            // Send mail with the invitation
            HashMap<String, String> fillers = new HashMap<>();
            fillers.put("ENVIRONMENT", request.getEnvironmentName());
            String subject = I18nFactory.getHandler().getMessage("administration.users.invite." + sendChannel + ".subject", request.getLang(), fillers);

            fillers.put("BASE_URL", ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, Constants.BASE_URL_KEY));

            String enrollmentUrl = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "administration.users.invite.enrollmentUrl", fillers);
            logMessage= MessageFormat.format("URL de inicio de invitacion: {0}", enrollmentUrl);
            log.info(logMessage);

            fillers.put("ENROLLMENT_URL", enrollmentUrl);

            fillers.put("FULL_NAME", invitationCode.getFirstName() + " " + invitationCode.getLastName());
            fillers.put("INVITATION_CODE", invitationCodePlain);
            fillers.put("EXPIRATION_DATE", invitationCode.getExpiration());
            String body = I18nFactory.getHandler().getMessage("administration.users.invite." + sendChannel + ".body", request.getLang(), fillers);

            switch (sendChannel) {
                case Constants.TRANSPORT_SMS:
                    NotificationsHandlerFactory.getHandler().sendSMS(invitationCode.getMobileNumber(), body, true);
                    break;
                case Constants.TRANSPORT_MAIL:
                    NotificationsHandlerFactory.getHandler().sendEmails(subject, body, Arrays.asList(invitationCode.getEmail()), null, request.getLang(), true);
                    break;
                default:
                    throw new ActivityException(ReturnCodes.ERROR_SENDING_INVITATION, "No se encuentra bien configurado el transporte de la notificacion para invitaciones");
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (MessagingException | ClassNotFoundException | InstantiationException | IllegalAccessException ex) {
            throw new ActivityException(ReturnCodes.ERROR_SENDING_INVITATION, ex);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String firstName = stripHTML(request.getParam(InParams.FIRST_NAME, String.class));
            String lastName = stripHTML(request.getParam(InParams.LAST_NAME, String.class));
            String email = request.getParam(InParams.EMAIL, String.class);
            String mobilePhone = request.getParam(InParams.MOBILE_PHONE, String.class);

            // Verification of the document entered in step 1
            String docCountry = request.getParam(InParams.DOCUMENT_COUNTRY, String.class);
            String docType = request.getParam(InParams.DOCUMENT_TYPE, String.class);
            String docNumber = request.getParam(InParams.DOCUMENT_NUMBER, String.class);

            // Encrypted document
            String document = request.getParam(InParams.DOCUMENT, String.class);
            if (StringUtils.isBlank(document)) {
                result.put("NO_FIELD", "administration.users.invalidDocument");
            } else {
                String decrypedDocument = SecurityUtils.decrypt(document);
                String[] tokens = StringUtils.split(decrypedDocument, "|");
                if (tokens == null || tokens.length < 3) {
                    result.put("NO_FIELD", "administration.users.invalidDocument");
                } else if (!StringUtils.equals(docCountry, tokens[0])
                        || !StringUtils.equals(docType, tokens[1])
                        || !StringUtils.equals(docNumber, tokens[2])) {
                    throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                }
            }

            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            if (environment == null) {
                result.put("NO_FIELD", "administration.users.invalidEnvironmentForInvitation");
            }

            if (result.isEmpty()) {
                ClientUser client = CoreCustomerConnectorOrchestrator.read(request.getIdTransaction(), docCountry, docType, docNumber, null);
                User user = AccessManagementHandlerFactory.getHandler().getUserByDocument(docCountry, docType, docNumber);
                boolean isNewUser = client == null && user == null;

                if (isNewUser && !ConfigurationFactory.getInstance().getBoolean(Configuration.PLATFORM, "administration.users.invite.notInBackend.enabled")) {
                    result.put("NO_FIELD", "administration.users.documentDontExistsInBackend");
                }

                if (isNewUser) {
                    if (StringUtils.isEmpty(firstName)) {
                        result.put("firstName", "administration.users.firstNameEmpty");
                    } else if (StringUtils.length(firstName) > 50) {
                        result.put(InParams.LAST_NAME, "administration.users.firstNameMaxLengthExceeded");
                    }

                    if (StringUtils.isEmpty(lastName)) {
                        result.put(InParams.LAST_NAME, "administration.users.lastNameEmpty");
                    } else if (StringUtils.length(lastName) > 50) {
                        result.put(InParams.LAST_NAME, "administration.users.lastNameMaxLengthExceeded");
                    }

                    if (StringUtils.isEmpty(email)) {
                        result.put(InParams.EMAIL, "administration.users.emailEmpty");
                    } else if (StringUtils.length(email) > 253) {
                        result.put(InParams.EMAIL, "administration.users.emailMaxLengthExceeded");
                    } else if (!ValidationUtils.validateEmailPattern(email)) {
                        result.put(InParams.EMAIL, "administration.users.emailIncorrectFormat");
                    }

                    if (StringUtils.isEmpty(mobilePhone)) {
                        result.put(InParams.MOBILE_PHONE, "administration.users.mobilePhoneEmpty");
                    } else if (StringUtils.length(mobilePhone) > 20) {
                        result.put(InParams.MOBILE_PHONE, "administration.users.mobilePhoneMaxLengthExceeded");
                    } else if (!StringUtils.isNumeric(mobilePhone)) {
                        result.put(InParams.MOBILE_PHONE, "administration.users.invalidMobilePhone");
                    }
                } else {
                    // If a user is invited that is not omnichannel but if in the backend, it is valid that the data is correct
                    User auxClient = client != null ? client : user;
                    if (auxClient != null) { // Never will be null
                        if (auxClient.getMobileNumber() == null) {
                            result.put("NO_FIELD", "administration.users.invalidBackendMobilePhone");
                        }

                        if (StringUtils.isEmpty(auxClient.getEmail()) || !ValidationUtils.validateEmailPattern(auxClient.getEmail())) {
                            result.put("NO_FIELD", "administration.users.invalidBackendEmail");
                        }
                        
                        if (Administration.getInstance().readEnvironmentUserInfo(auxClient.getIdUser(), request.getIdEnvironment()) != null) {
                            result.put("NO_FIELD", "administration.users.environmentUserAlreadyExists");
                        }
                    }
                }

                switch (request.getEnvironmentAdminScheme()) {
                    case Environment.ADMINISTRATION_SCHEME_SIMPLE:
                    case Environment.ADMINISTRATION_SCHEME_MEDIUM:
                        String role = request.getParam(InParams.ROLE, String.class);

                        List<String> validRoles = ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "invitation.permissions.roleList.frontend");
                        if (StringUtils.isBlank(role)) {
                            result.put("role", "administration.users.emptyRole");
                        } else if (!validRoles.contains(role)) {
                            result.put("role", "administration.users.invalidRole");
                        }
                        break;
                    case Environment.ADMINISTRATION_SCHEME_ADVANCED:
                        String signatureLevel = request.getParam(InParams.SIGNATURE_LEVEL, String.class);
                        Integer[] groups = request.getParam(InParams.GROUPS, Integer[].class);

                        if (StringUtils.isEmpty(signatureLevel)) {
                            result.put(InParams.SIGNATURE_LEVEL, "administration.users.emptySignatureLevel");
                        } else if (Arrays.binarySearch(new String[]{"A", "B", "C", "D", "E", "F", "G", "N"}, signatureLevel) < 0) {
                            result.put(InParams.SIGNATURE_LEVEL, "administration.users.invalidSignatureLevel");
                        }

                        if (groups == null || groups.length == 0) {
                            result.put(InParams.GROUPS, "administration.users.mustSelectGroup");
                        } else {
                            List<Integer> environmentGroups = Administration.getInstance().listEnvironmentGroupIds(request.getIdEnvironment());
                            if (environmentGroups == null || !environmentGroups.containsAll(Arrays.asList(groups))) {
                                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
                            } else {
                                for (Integer idGroup : groups) {
                                    List<GroupPermission> permissions = AccessManagementHandlerFactory.getHandler().getGroupPermissions(idGroup);
                                    for (GroupPermission permission : permissions) {
                                        if (Constants.ADMINISTRATION_VIEW_PERMISSION.equals(permission.getIdPermission())) {
                                            result.put(InParams.GROUPS, "administration.users.cantAddAdminPermission");
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                        break;
                    default:
                        break;
                }
            }

        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}