/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain;

import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.rubicon.core.accounts.WsAmount;
import com.technisys.omnichannel.rubicon.core.accounts.WsTransferResponse;

/**
 *
 * @author fpena
 */
public class TransferInternalDetail {

    private int returnCode;
    private String returnCodeDescrption;

    private Amount debitAmount;
    private Amount creditAmount;
    private double rate;

    public TransferInternalDetail() {}
    public TransferInternalDetail(WsTransferResponse response) {
        this.returnCode = response.getReturnCode();
        this.returnCodeDescrption = response.getReturnCodeDescription();

        WsAmount amount = response.getDebitAmount();
        if (amount != null){
            this.debitAmount = new Amount(amount.getCurrency(), amount.getQuantity());
        }
        amount = response.getCreditAmount();
        if (amount != null){
            this.creditAmount = new Amount(amount.getCurrency(), amount.getQuantity());
        }

        this.rate = response.getRate();
    }

    public Amount getDebitAmount() {
        return debitAmount;
    }

    public void setDebitAmount(Amount debitAmount) {
        this.debitAmount = debitAmount;
    }

    public int getReturnCode() {
        return returnCode;
    }

    public void setReturnCode(int returnCode) {
        this.returnCode = returnCode;
    }

    public String getReturnCodeDescrption() {
        return returnCodeDescrption;
    }

    public void setReturnCodeDescrption(String returnCodeDescrption) {
        this.returnCodeDescrption = returnCodeDescrption;
    }

    public Amount getCreditAmount() {
        return creditAmount;
    }

    public void setCreditAmount(Amount creditAmount) {
        this.creditAmount = creditAmount;
    }

    public double getRate() {
        return rate;
    }

    public void setRate(double rate) {
        this.rate = rate;
    }
}
