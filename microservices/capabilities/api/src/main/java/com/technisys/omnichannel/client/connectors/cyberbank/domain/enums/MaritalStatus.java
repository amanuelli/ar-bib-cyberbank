/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain.enums;

public enum MaritalStatus {
    SOLTERO("SOLTERO"),
    CASADO("CASADO"),
    DIVORCIADO("DIVORCIADO"),
    SEPARADO_DE_HECHO("SEPARADO DE HECHO"),
    CONCUBINO("CONCUBINO"),
    VIUDO("VIUDO");

    private final String value;

    MaritalStatus(String value) {
        this.value = value;
    }

    public String getValueForService() {
        return value;
    }

    @Override
    public String toString() {
        return value;
    }
}
