/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.restrictions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.util.Map;

/**
 * @author Marcelo Bruno
 */

/**
 * Manage the access restriction Preview
 */
@DocumentedActivity("Manage the access restriction preview")
public class ManageAccessRestrictionsPreviewActivity extends Activity {

    public static final String ID = "administration.restrictions.manage.preview";

    public interface InParams {
        @DocumentedParam(type = Integer.class, description = "Calendar Restriction id, if it's a modification")
        String CALENDAR_RESTRICTION_ID = "calendarRestrictionId";
        @DocumentedParam(type = String.class, description = "User Id")
        String ID_USER = "idUser";
        @DocumentedParam(type = Integer.class, description = "days: int value that converted in bits means the active days of a week")
        String DAYS = "days";
        @DocumentedParam(type = Boolean.class, description = "it means whether the restrictions are everyday, all the time or not")
        String PERPETUAL = "perpetual";
        @DocumentedParam(type = String.class, description = "Time zone name that restriction will apply")
        String TIME_ZONE = "timeZone";
        @DocumentedParam(type = Integer.class, description = "start Time specified in minutes")
        String START_TIME = "startTime";
        @DocumentedParam(type = Integer.class, description = "end Time specified in minutes")
        String END_TIME = "endTime";
        @DocumentedParam(type = String[].class, description = "Array of strings with the ips to apply th restriction")
        String IPS_LIST = "ipsList";
        @DocumentedParam(type = Boolean.class, description = "Any ip")
        String FROM_ANY_IP = "anyIP";
        @DocumentedParam(type = Boolean.class, description = "Specify whether calendar restrictions will save or not")
        String CALENDAR_TO_SAVE = "calendarToSave";
        @DocumentedParam(type = Boolean.class, description = "Specify whether IP restrictions will be disabled or not")
        String DISABLE_IP = "iPEnabled";
        @DocumentedParam(type = Boolean.class, description = "Specify whether calendar restrictions will be disabled or not")
        String DISABLE_CALENDAR = "calendarEnabled";

    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return new ManageAccessRestrictionsActivity().validate(request);
    }
}