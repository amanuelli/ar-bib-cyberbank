/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.restrictions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.CalendarLoginRestriction;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.IPAccessEnvironment;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.environments.EnvironmentHandler;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.utils.days.Days;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.validator.routines.InetAddressValidator;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author JhosseptG
 */
/**
 * Manage the access restriction
 */
@DocumentedActivity("Manage the access restriction")
public class ManageAccessRestrictionsActivity extends Activity {

    public static final String ID = "administration.restrictions.manage.send";

    public interface InParams {
        @DocumentedParam(type = Integer.class, description = "Calendar Restriction id, if it's a modification")
        String CALENDAR_RESTRICTION_ID = "calendarRestrictionId";
        @DocumentedParam(type = String.class, description = "User Id")
        String ID_USER = "idUser";
        @DocumentedParam(type = Integer.class, description = "days: int value that converted in bits means the active days of a week")
        String DAYS = "days";
        @DocumentedParam(type = Boolean.class, description = "it means whether the restrictions are everyday, all the time or not")
        String PERPETUAL = "perpetual";
        @DocumentedParam(type = String.class, description = "Time zone name that restriction will apply")
        String TIME_ZONE = "timeZone";
        @DocumentedParam(type = Integer.class, description = "start Time specified in minutes")
        String START_TIME = "startTime";
        @DocumentedParam(type = Integer.class, description = "end Time specified in minutes")
        String END_TIME = "endTime";
        @DocumentedParam(type = String[].class, description = "Array of strings with the ips to apply th restriction")
        String IPS_LIST = "ipsList";
        @DocumentedParam(type = Boolean.class, description = "Any ip")
        String FROM_ANY_IP = "anyIP";
        @DocumentedParam(type = Boolean.class, description = "Specify whether calendar restrictions will save or not")
        String CALENDAR_TO_SAVE = "calendarToSave";
        @DocumentedParam(type = Boolean.class, description = "Specify whether IP restrictions will be disabled or not", optional = true)
        String ENABLE_IP = "iPEnabled";
        @DocumentedParam(type = Boolean.class, description = "Specify whether calendar restrictions will be disabled or not", optional = true)
        String ENABLE_CALENDAR = "calendarEnabled";

    }

    public interface OutParams {
        @DocumentedParam(type = Integer.class, description = "ID of created restrictions")
        String CALENDAR_RESTRICTION_ID = "calendarRestrictionId";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            String idUser = request.getParam(InParams.ID_USER, String.class);
            Boolean fromAnyIP = request.getParam(InParams.FROM_ANY_IP, Boolean.class);
            List<String> ipsList = request.getParam(InParams.IPS_LIST, ArrayList.class);

            Boolean itsOnlyEnvironment = StringUtils.isBlank(idUser);
            fromAnyIP = fromAnyIP && !itsOnlyEnvironment;

            Boolean enableIP = true;
            Boolean enableCalendar = true;
            //Only allow enable?disable restrictions in case of environments
            if(itsOnlyEnvironment) {
                //Check if should we disabled restrictions
                enableCalendar = request.getParam(InParams.ENABLE_CALENDAR, Boolean.class) !=null ? request.getParam(InParams.ENABLE_CALENDAR, Boolean.class) : true;
                enableIP = request.getParam(InParams.ENABLE_IP, Boolean.class) !=null ? request.getParam(InParams.ENABLE_IP, Boolean.class) : true;

                if(enableCalendar) {
                    Administration.getInstance().enableEnvironmentCalendarRestrictions(request.getIdEnvironment());
                } else {
                    Administration.getInstance().disableEnvironmentCalendarRestrictions(request.getIdEnvironment());
                }
                if(enableIP) {
                    Administration.getInstance().enableEnvironmentIpRestrictions(request.getIdEnvironment());
                } else {
                    Administration.getInstance().disableEnvironmentIpRestrictions(request.getIdEnvironment());
                }
            }

            if(enableIP) {
                manageIpRestrictions(fromAnyIP, request.getIdEnvironment(), idUser, ipsList);
            }

            if(enableCalendar) {
                manageCalendarRestrictions(request, response, idUser);
            }

            if(!StringUtils.isBlank(idUser)) {
                Transaction t = TransactionHandlerFactory.getInstance().read(request.getIdTransaction());
                t.getData().put("userName", AccessManagementHandlerFactory.getHandler().getUser(idUser).getFullName());
                TransactionHandlerFactory.getInstance().updateTransactionData(t);
            }

            response.setReturnCode(ReturnCodes.OK);
            return response;
        }catch (Exception e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    private void manageCalendarRestrictions(Request request, Response response, String idUser) throws IOException {
        Integer calendarRestrictionId = request.getParam(InParams.CALENDAR_RESTRICTION_ID, Integer.class);
        Integer days = request.getParam(InParams.DAYS, Integer.class);
        Boolean perpetual = request.getParam(InParams.PERPETUAL, Boolean.class);
        String timeZone = request.getParam(InParams.TIME_ZONE, String.class);
        Integer startTime = request.getParam(InParams.START_TIME, Integer.class);
        Integer endTime = request.getParam(InParams.END_TIME, Integer.class);
        Boolean calendarToSave = request.getParam(InParams.CALENDAR_TO_SAVE, Boolean.class);

        //Save calendar restrictions
        if(calendarToSave) {
            if(perpetual) {
                days = 127; //everyday
                startTime = 0; //from hour 0
                endTime = CalendarLoginRestriction.MAX_VALUE_A_DAY_IN_MIN; //until 23:59h
                timeZone = "Zulu";
            }

            if(calendarRestrictionId == null) {
                int restrictionId;
                if(StringUtils.isBlank(idUser)) {
                    restrictionId = EnvironmentHandler.addEnvironmentCalendarLoginRestriction(request.getIdEnvironment(), new Days(days), startTime, endTime, timeZone);
                }else{
                    restrictionId = EnvironmentHandler.addEnvironmentCalendarLoginRestrictionForUser(request.getIdEnvironment(), idUser, new Days(days), startTime, endTime, timeZone);
                }

                response.putItem(OutParams.CALENDAR_RESTRICTION_ID, restrictionId);
            } else {
                EnvironmentHandler.updateEnvironmentCalendarLoginRestriction(calendarRestrictionId, new Days(days),startTime, endTime, timeZone);
            }

        }
    }

    private void manageIpRestrictions(Boolean fromAnyIP, int idEnvironment, String idUser, List<String> ipsList) throws IOException {
        List<IPAccessEnvironment> restrictionList = new ArrayList<>();
        IPAccessEnvironment iPAccessEnvironment;
        if(fromAnyIP != null && fromAnyIP) {
            iPAccessEnvironment = new IPAccessEnvironment();
            iPAccessEnvironment.setIdEnvironment(idEnvironment);
            iPAccessEnvironment.setIp(IPAccessEnvironment.IP_USER_TOTAL_ACCESS);
            iPAccessEnvironment.setIdUser(idUser);
            restrictionList.add(iPAccessEnvironment);
        } else {
            if(ipsList != null){
                for(String ip : ipsList) {
                    iPAccessEnvironment = new IPAccessEnvironment();
                    iPAccessEnvironment.setIdEnvironment(idEnvironment);
                    iPAccessEnvironment.setIp(ip);
                    restrictionList.add(iPAccessEnvironment);
                }
            }
        }
        if(!StringUtils.isBlank(idUser)){
            Administration.getInstance().addOrEditEnvironmentUserAccessIP(idEnvironment, idUser, restrictionList);
        }else{
            Administration.getInstance().addOrEditEnvironmentAccessIP(idEnvironment, restrictionList);
        }
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String adminScheme = request.getEnvironmentAdminScheme();
        if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(adminScheme)) {
            throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
        }
        result.putAll(validateCalendarData(request));
        result.putAll(validateIPData(request));
        return result;
    }

    private Map<String, String> validateCalendarData(Request request){
        Map<String, String> result = new HashMap<>();

        Boolean enableCalendar;
        enableCalendar = request.getParam(InParams.ENABLE_CALENDAR, Boolean.class);

        if(enableCalendar != null && !enableCalendar) {
            return result;
        }

        String fieldRequiredLabel = "administration.restrictions.field.error.empty";
        Integer days = request.getParam(InParams.DAYS, Integer.class);
        Boolean perpetual = request.getParam(InParams.PERPETUAL, Boolean.class);
        String timeZone = request.getParam(InParams.TIME_ZONE, String.class);
        Integer startTime = request.getParam(InParams.START_TIME, Integer.class);
        Integer endTime = request.getParam(InParams.END_TIME, Integer.class);
        Boolean calendarToSave = request.getParam(InParams.CALENDAR_TO_SAVE,Boolean.class);
        String idUser = request.getParam(InParams.ID_USER, String.class);
        Boolean itsOnlyEnvironment = StringUtils.isBlank(idUser);

        //validate if there is calendar restriction
        //and if it's environment set up, then days, timezone, start and end time must be required
        if(calendarToSave && (itsOnlyEnvironment || !perpetual)){
            if(days == null || days == 0){
                result.put(InParams.DAYS, fieldRequiredLabel);
            }
            if(StringUtils.isBlank(timeZone)){
                result.put(InParams.TIME_ZONE, fieldRequiredLabel);
            }
            if(startTime == null){
                result.put(InParams.START_TIME, fieldRequiredLabel);
            }else{
                if(startTime > CalendarLoginRestriction.MAX_VALUE_A_DAY_IN_MIN){
                    result.put(InParams.START_TIME, "administration.restrictions.error.startTime.invalid");
                }
            }
            if(endTime == null){
                result.put(InParams.END_TIME, fieldRequiredLabel);
            }else{
                if(endTime == 0 || endTime > CalendarLoginRestriction.MAX_VALUE_A_DAY_IN_MIN){
                    result.put(InParams.END_TIME, "administration.restrictions.error.endTime.invalid");
                }
            }

            if(startTime != null && endTime != null && startTime > endTime){
                result.put(InParams.START_TIME, "administration.restrictions.error.startTimeGreaterThanEnd");
            }
        }
        return result;
    }

    private Map<String, String> validateIPData(Request request){
        Map<String, String> result = new HashMap<>();

        List<String> ipsList = request.getParam(InParams.IPS_LIST, ArrayList.class);
        Boolean enableIP;
        enableIP = request.getParam(InParams.ENABLE_IP, Boolean.class);

        if(enableIP != null && !enableIP) {
            return result;
        }

        //validate if the IPs are valid
        if(ipsList != null && !ipsList.isEmpty()){
            InetAddressValidator validator = new InetAddressValidator();
            for(String ip : ipsList) {
                if(!validator.isValid(ip)) {
                    result.put(InParams.IPS_LIST, "administration.environment.restrictions.ip.some.invalid");
                    break;
                }
            }
        }

        return result;
    }
}