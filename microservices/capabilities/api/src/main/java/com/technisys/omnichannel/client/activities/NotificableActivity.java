/*
 *  Copyright 2015 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities;

import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.domain.Transaction;
import java.io.IOException;
import javax.mail.MessagingException;

/**
 *
 */
public interface NotificableActivity {
    void sendNotificationMails(Request request) throws IOException, MessagingException;
    void sendNotificationMails(Transaction transaction) throws IOException, MessagingException;
}
