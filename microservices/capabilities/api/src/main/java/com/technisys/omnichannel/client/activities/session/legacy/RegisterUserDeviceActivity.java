package com.technisys.omnichannel.client.activities.session.legacy;

import com.maxmind.geoip2.exception.GeoIp2Exception;
import com.maxmind.geoip2.model.CityResponse;
import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.UserDevice;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.postloginchecks.geoip.PostLoginCheckGeoIPDataAccess;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.net.InetAddress;
import java.util.Map;

/**
 * Register user device on login flow
 */
@ExchangeActivity
@DocumentedActivity("Register a user device")
public class RegisterUserDeviceActivity extends Activity {

    public static final String ID = "session.login.legacy.registerUserDevice";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Token required to call the next step on this process")
        String EXCHANGE_TOKEN = "_exchangeToken";
        @DocumentedParam(type = String.class, description = "User device Id")
        String ID_DEVICE = "idDevice";
        @DocumentedParam(type = Map.class, description = "Device extra info")
        String EXTRA_INFO = "extraInfo";
        @DocumentedParam(type = String.class, description = "Device push token")
        String PUSH_TOKEN = "pushToken";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {

            String idDevice = request.getParam(InParams.ID_DEVICE, String.class);

            if (StringUtils.isEmpty(idDevice)) {
                //si no me llega el registro de dispositivo devuelvo un error
                throw new ActivityException(ReturnCodes.UNEXPECTED_ERROR);
            }

            String extraInfo = request.getParam(InParams.EXTRA_INFO, String.class);
            String pushToken = request.getParam(InParams.PUSH_TOKEN, String.class);

            String clientIp = request.getClientIP();
            InetAddress inetAddress = InetAddress.getByName(clientIp);
            if (inetAddress.isSiteLocalAddress() || inetAddress.isLoopbackAddress()) {
                clientIp = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "cyberbank.localAddressTesting");
                inetAddress = InetAddress.getByName(clientIp);
            }

            CityResponse cityResponse = PostLoginCheckGeoIPDataAccess.getInstance().getCity(inetAddress);

            UserDevice userDevice = new UserDevice(retrieveIdUser(request), idDevice, extraInfo, pushToken);
            userDevice.setLastEntryCountry(cityResponse.getCountry().getName());
            userDevice.setLastEntryCity(cityResponse.getCity().getName());

            AccessManagementHandlerFactory.getHandler().createOrUpdateUserDevice(userDevice);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException | GeoIp2Exception ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

    protected String retrieveIdUser(Request request) throws IOException {
        String exchangeTokenString = request.getParam(InParams.EXCHANGE_TOKEN, String.class);
        ExchangeToken exchangeToken = ExchangeTokenHandler.read(exchangeTokenString);

        return exchangeToken.getIdUser();
    }

}
