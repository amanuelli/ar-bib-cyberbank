/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.utils.ObjectPool;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.rubicon.core.onboarding.Onboarding;
import com.technisys.omnichannel.rubicon.core.onboarding.WSOnboarding;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.BindingProvider;
import java.net.MalformedURLException;
import java.net.URL;

import static com.technisys.omnichannel.client.connectors.RubiconCoreConnector.WEBSERVICES_URL;
/**
 *
 * @author fpena
 */
public class WSOnboardingFactory implements ObjectPool.ObjectPoolFactory<Onboarding> {

    private static final Logger log = LoggerFactory.getLogger(WSOnboardingFactory.class);

    @Override
    public Onboarding create() {

        String wsdlSufix = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "backend.webservices.onboarding.wsdl", "");
        String endpoint = WEBSERVICES_URL + "/WSOnboarding";

        Onboarding port = null;
        WSOnboarding service;
        boolean useRemoteWSDL = !"".equals(wsdlSufix);

        try {
            if (useRemoteWSDL) {
                URL url = new URL(WEBSERVICES_URL + wsdlSufix);
                service = new WSOnboarding(url);
            } else {
                service = new WSOnboarding();
            }

            port = service.getOnboardingPort();

            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);

        } catch (MalformedURLException e) {
            log.warn("Error creating WSOnboarding port : " + e.getMessage(), e);
        }
        
        return port;
    }
    
}
