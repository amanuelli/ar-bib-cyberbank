package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCustomerConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankExchangeRateConnector;
import com.technisys.omnichannel.client.domain.CurrencyExchange;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CoreExchangeRateConnectorOrchestrator extends CoreConnectorOrchestrator {

    public static List<CurrencyExchange> listExchangeRates(String idTransaction, String idClient, String idUser) throws BackendConnectorException, IOException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorC.listWidgetsExchangeRates(idTransaction, idClient);
            case CYBERBANK_CONNECTOR:
                try {
                    int customerBehaviourType = CyberbankCustomerConnector.getClassification(idClient).getData();
                    CyberbankCoreConnectorResponse<List<CurrencyExchange>> response = CyberbankExchangeRateConnector.listExchangeRates(idUser, customerBehaviourType);
                    if (response.getCode() == 0) {
                        return response.getData();
                    }

                    return new ArrayList<>();
                } catch (CyberbankCoreConnectorException e) {
                    throw new BackendConnectorException(e);
                }
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

}
