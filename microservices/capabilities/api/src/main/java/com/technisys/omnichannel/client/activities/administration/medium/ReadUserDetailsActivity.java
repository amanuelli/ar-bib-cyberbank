package com.technisys.omnichannel.client.activities.administration.medium;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.common.Permissions;
import com.technisys.omnichannel.client.activities.administration.common.UserDetails;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.limits.CapForUser;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.EnvironmentUser;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

@DocumentedActivity("Read user´s detail of medium environment")
public class ReadUserDetailsActivity extends Activity {

    public static final String ID = "administration.medium.user.details.read";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "ID of the user to fetch information")
        String ID = "id";
    }

    public interface OutParams {

        @DocumentedParam(type = Map.class, description = "Permission's map")
        String PERMISSIONS = "permissions";
        @DocumentedParam(type = Set.class, description = "UI permissions set")
        String GROUPS = "groups";
        @DocumentedParam(type = User.class, description = "User info")
        String USER = "user";
        @DocumentedParam(type = List.class, description = "Product's list")
        String PRODUCTS = "products";
        @DocumentedParam(type = List.class, description = "User caps")
        String CAPS = "caps";
        @DocumentedParam(type = String.class, description = "Master currency")
        String CURRENCY = "currency";
        @DocumentedParam(type = CapForUser.class, description = "Channel's All cap")
        String TOP_AMOUNT = "topAmount";
        @DocumentedParam(type = String.class, description = "Signature level")
        String SIGNATURE_LEVEL = "signatureLevel";
        @DocumentedParam(type = Boolean.class, description = "True if user has administration permissions")
        String HAS_MASSIVE_ENABLED = "hasMassiveEnabled";
        @DocumentedParam(type = String.class, description = "User environment status")
        String USER_ENV_STATUS = "userEnvStatus";
        @DocumentedParam(type = String.class, description = "User dispatcher value")
        String DISPATCHER = "dispatcher";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            final String idUser = request.getParam(InParams.ID, String.class);
            final String lang = request.getLang();
            final int idEnvironment = request.getIdEnvironment();
            final Administration administration = Administration.getInstance();
            final EnvironmentUser envUser = administration.readEnvironmentUserInfo(idUser, idEnvironment);
            final List<Group> groups = AccessManagementHandlerFactory.getHandler()
                    .getGroups(idUser, null, idEnvironment, -1, -1, null).getElementList();

            if (!this.isSchemeValid(groups, request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME_DATA);
            }

            Map<String, Object> data = UserDetails.buildUserDetails(envUser, idUser, idEnvironment, request.getEnvironmentType(), lang);
            data.put(OutParams.PERMISSIONS, Permissions.buildPermissionsMap(groups.get(0), idEnvironment).get("allPermissions"));
            response.setData(data);

            response.setReturnCode(ReturnCodes.OK);
            
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            String userId = request.getParam(InParams.ID, String.class);
            List<String> environmentUsers = Administration.getInstance()
                    .listEnvironmentUserIds(request.getIdEnvironment());

            if (environmentUsers == null || (!environmentUsers.contains(StringUtils.left(userId, 253)))) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    private boolean isSchemeValid(List<Group> groups, final String scheme) {
        return Environment.ADMINISTRATION_SCHEME_MEDIUM.equals(scheme)
                && (!CollectionUtils.isEmpty(groups) || groups.size() == 1);
    }
}
