/*
 *  Copyright 2016 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.client.utils.ObjectPool;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.rubicon.core.deposits.Deposits;
import com.technisys.omnichannel.rubicon.core.deposits.WSDeposits;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.BindingProvider;
import java.net.URL;

import static com.technisys.omnichannel.client.connectors.RubiconCoreConnector.WEBSERVICES_URL;
/**
 *
 * @author fpena
 */
public class WSDepositsFactory implements ObjectPool.ObjectPoolFactory<Deposits> {

    private static final Logger log = LoggerFactory.getLogger(WSDepositsFactory.class);

    @Override
    public Deposits create() {

        String wsdlSufix = ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "backend.webservices.deposits.wsdl", "");
        String endpoint = WEBSERVICES_URL + "/WSDeposits";

        Deposits port = null;
        WSDeposits service;
        boolean useRemoteWSDL = !"".equals(wsdlSufix);

        try {
            if (useRemoteWSDL) {
                URL url = new URL(WEBSERVICES_URL + wsdlSufix);
                service = new WSDeposits(url);
            } else {
                service = new WSDeposits();
            }

            port = service.getDepositsPort();

            ((BindingProvider) port).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpoint);
        } catch (Exception e) {
            log.warn("Error creating WSDeposits port : " + e.getMessage(), e);
        }
        
        return port;
    }
    
}
