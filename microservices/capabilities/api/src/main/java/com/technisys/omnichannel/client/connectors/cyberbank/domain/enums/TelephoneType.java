/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain.enums;

public enum TelephoneType {
    MOBILE("Celular principal"),
    LAND_LINE("Fijo de Contacto"),
    HOME("HOME");

    private final String value;

    TelephoneType(String value){ this.value = value; }

    public String getValue(){
        return value;
    }


    @Override
    public String toString() {
        return value;
    }
}
