/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

public class CyberbankCoreConnectorException extends Exception {

    public CyberbankCoreConnectorException(String message) {
        super(message);
    }

    public CyberbankCoreConnectorException(Throwable cause) {
        super(cause);
    }

    public CyberbankCoreConnectorException(String message, Throwable cause) {
        super(message, cause);
    }

}
