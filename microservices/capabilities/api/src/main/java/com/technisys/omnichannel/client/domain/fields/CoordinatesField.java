/*
 * Copyright 2019 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.domain.fields;

import com.technisys.omnichannel.annotations.docs.DocumentedTypeParam;
import com.technisys.omnichannel.core.domain.FormField;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 *
 * @author Marcelo Bruno
 */
public class CoordinatesField extends FormField {

    @DocumentedTypeParam(description = "Internationalized messages to display when no field value is set")
    private Map<String, String> requiredErrorMap;

    @DocumentedTypeParam(description = "Internationalized messages to display when field value is not a valid one")
    private Map<String, String> invalidErrorMap;

    public Map<String, String> getRequiredErrorMap() {
        return requiredErrorMap;
    }

    public void setRequiredErrorMap(Map<String, String> requiredErrorMap) {
        this.requiredErrorMap = requiredErrorMap;
    }

    public void addRequiredError(String lang, String value) {
        if (requiredErrorMap == null) {
            requiredErrorMap = new LinkedHashMap<>();
        }
        requiredErrorMap.put(lang, value);
    }

    public Map<String, String> getInvalidErrorMap() {
        return invalidErrorMap;
    }

    public void setInvalidErrorMap(Map<String, String> invalidErrorMap) {
        this.invalidErrorMap = invalidErrorMap;
    }

    public void addInvalidError(String lang, String value) {
        if (invalidErrorMap == null) {
            invalidErrorMap = new LinkedHashMap<>();
        }
        invalidErrorMap.put(lang, value);
    }

}
