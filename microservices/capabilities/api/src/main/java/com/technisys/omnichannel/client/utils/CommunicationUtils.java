/*
 * Copyright 2020 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */

package com.technisys.omnichannel.client.utils;

import com.technisys.omnichannel.core.domain.Communication;
import com.technisys.omnichannel.core.domain.CommunicationRecipient;

/**
 * Utility class for Coummunications
 * @author Marcelo Bruno
 */
public class CommunicationUtils {

    private CommunicationUtils() {}

    /**
     * Check if a communication is from a certain user
     *
     * @param communication Communication param
     * @param idUser idUser param
     * @return true if Communication communication is from user idUser
     */
    public static boolean checkCommunicationUser(Communication communication, String idUser) {
        Boolean result = true;
        if(communication == null) {
            result = false;
        } else {
            if(communication.getIdUser() != null) {
                if(!idUser.equals(communication.getIdUser())) {
                    result = false;
                }
            } else {
                CommunicationRecipient communicationRecipient = communication.getRecipientList().stream()
                        .filter(elem -> elem.getIdUser().equals(idUser))
                        .findAny()
                        .orElse(null);
                if(communicationRecipient == null) {
                    result = false;
                }
            }
        }
        return result;
    }

}
