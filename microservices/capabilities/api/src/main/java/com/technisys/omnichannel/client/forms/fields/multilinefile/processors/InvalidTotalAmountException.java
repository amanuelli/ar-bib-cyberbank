package com.technisys.omnichannel.client.forms.fields.multilinefile.processors;

import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.client.domain.TransactionLine.ErrorCode;

/**
 *
 * @author Marcelo Bruno
 */
public class InvalidTotalAmountException extends Exception {

    private final ErrorCode errorCode;

    InvalidTotalAmountException(TransactionLine.ErrorCode errorCode) {
        super(errorCode.toString());

        this.errorCode = errorCode;
    }

    TransactionLine.ErrorCode getErrorCode() {
        return errorCode;
    }
    
}

