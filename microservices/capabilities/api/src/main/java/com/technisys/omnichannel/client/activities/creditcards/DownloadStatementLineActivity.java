/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.creditcards;

import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.StatementLine;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 *
 */
public class DownloadStatementLineActivity extends Activity {

    public static final String ID = "creditCards.downloadStatementLine";

    public interface InParams {

        String ID_CREDIT_CARD = "idAccount";
        String ID_STATEMENT_LINE = "idStatementLine";
    }

    public interface OutParams {

        String FILE_NAME = "fileName";
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }

            StatementLine statementLine = RubiconCoreConnectorTC.readCreditCardStatementLine((String) request.getParam(InParams.ID_STATEMENT_LINE, String.class));

            response.putItem(OutParams.FILE_NAME, statementLine.getFilename());
            response.putItem(OutParams.CONTENT, new String(statementLine.getContent()));

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }
}
