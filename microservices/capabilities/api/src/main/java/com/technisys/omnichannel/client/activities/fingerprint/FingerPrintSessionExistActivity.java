/*
 *  Copyright 2018 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.fingerprint;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.SessionException;
import com.technisys.omnichannel.core.session.Session;
import com.technisys.omnichannel.core.session.SessionHandlerFactory;
import com.technisys.omnichannel.core.session.SessionKind;
import java.io.IOException;

import static com.technisys.omnichannel.client.utils.HeadersUtils.buildRequestHeaders;

public class FingerPrintSessionExistActivity extends Activity{
    
    public static final String ID = "fingerprint.sessionExist";
    
    public interface OutParams {
        String EXIST_SESSION_WITH_FINGER_PRINT = "existSessionWithFingerPrint";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            Session session = SessionHandlerFactory.getInstance().readSession(request.getCredential("accessToken").getValue(), buildRequestHeaders(request));
            if ((session != null) && (session.getKind().equals(SessionKind.FINGER))) {
                response.putItem(OutParams.EXIST_SESSION_WITH_FINGER_PRINT, true);
            }
        } catch (SessionException | IOException ex) {
            response.putItem(OutParams.EXIST_SESSION_WITH_FINGER_PRINT, false);
        }
        
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }
}
