/*
 *  Copyright 2017 Technisys.
 * 
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *  
 *  https://www.technisys.com
 */

package com.technisys.omnichannel.client.forms.preloaders;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.*;
import com.technisys.omnichannel.core.forms.FormPreloader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author dimoda
 */
public class AdditionalCreditCardRequestPreloader implements FormPreloader {

    private static final Logger log = LoggerFactory.getLogger(AdditionalCreditCardRequestPreloader.class);
    
     public interface OutParams {
        String AMOUNT = "amount";
    }
    
    @Override
    public Map<String, Object> execute(Form form, Request request) {
        Map<String, Object> formData = new HashMap();
        if (!User.USER_ANONYMOUS.equals (request.getIdUser())){
            Amount minimumAmount = getCreditCardMinimumCreditLimit(request);
            if (minimumAmount != null){
                formData.put(OutParams.AMOUNT, minimumAmount);
            }
        }
        return formData;
    }
    
    
    public static Amount getCreditCardMinimumCreditLimit(Request request) {
        try {
            Environment environment = Administration.getInstance().readEnvironment(request.getIdEnvironment());
            
            List<CreditCard> creditCards = (List<CreditCard>) RubiconCoreConnectorC.listProducts(request.getIdTransaction(), environment.getClients(), Arrays.asList(new String[]{Constants.PRODUCT_TC_KEY}));
            CreditCard creditCardMinimumLimit = null;
            Double minimunCreditLimit = Double.MAX_VALUE;
            for (CreditCard creditCard : creditCards) {
                Product product = Administration.getInstance().readProduct(creditCard.getIdProduct(), request.getIdEnvironment());
                CreditCard creditCardDetail =RubiconCoreConnectorTC.readCreditCardDetails(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo());
                if (creditCardDetail.getCreditLimit() < minimunCreditLimit){
                    creditCardMinimumLimit = creditCardDetail;
                }
            }
            if (creditCardMinimumLimit != null){
                return new Amount(creditCardMinimumLimit.getCreditLimitCurrency(), creditCardMinimumLimit.getCreditLimit());
            }
        } catch (IOException | BackendConnectorException ex) {
            log.error(ex.getMessage(), ex);
        }
        return null;
    }
}