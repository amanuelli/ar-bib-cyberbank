/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.transactions;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;

import java.io.IOException;
import java.util.Map;

/**
 * @author Marcelo Bruno
 */

@DocumentedActivity("Get pending transactions quantity")
public class RefreshPendingTransactionsQuantityActivity extends Activity {

    public static final String ID = "transactions.get.pending.quantity";

    public interface InParams {

    }

    public interface OutParams {
        @DocumentedParam(type = Integer.class, description = "Pending transactions quantiy")
        String PENDING_TRANSACTIONS_QUANTITY = "pendingTransactionsQuantity";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            Integer totalPendings = 0;
            Map<String, Integer> totals = TransactionHandlerFactory.getInstance().listTotalTransactionsByStatus(request.getIdUser(), request.getIdEnvironment());
            totalPendings = totals.get(Transaction.STATUS_PENDING);
            response.putItem(OutParams.PENDING_TRANSACTIONS_QUANTITY, totalPendings != null ? totalPendings : 0);
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        return response;
    }

}