/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.preferences.notificationsconfiguration;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;

/**
 *
 */
@DocumentedActivity("Modify Notifications Configuration")
public class ModifyNotificationsConfigurationActivity extends Activity {
    
    public static final String ID = "preferences.notifications.configuration.modify";
    
    public interface InParams {
        @DocumentedParam(type = String.class, description = "Notification type")
        String NOTIFICATION_TYPE = "notificationType";

        @DocumentedParam(type = String.class, description = "Notification transport")
        String TRANSPORT = "transport";

        @DocumentedParam(type = String.class, description = "Boolean indicates if suscribed or not")
        String SUBSCRIBED = "subscribed";
    }
    
    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Boolean indicates transport modification")
        String TRANSPORT_MODIFIED = "transportModified";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        
        try {
            String notificationType = request.getParam(InParams.NOTIFICATION_TYPE, String.class);
            String transport = request.getParam(InParams.TRANSPORT, String.class);
            boolean subscribed = request.getParam(InParams.SUBSCRIBED, boolean.class);
            
            response.putItem(OutParams.TRANSPORT_MODIFIED, NotificationsHandlerFactory.getHandler().toggleUserCommunicationTypeTransport(request.getIdUser(), notificationType, transport, subscribed));
            
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        
        return response;
    }
}