/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.orchestrator;

import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorC;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorO;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankAccountConnector;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorException;
import com.technisys.omnichannel.client.connectors.cyberbank.CyberbankCoreConnectorResponse;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.StatementResult;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Statement;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class CoreAccountConnectorOrchestrator extends CoreConnectorOrchestrator {

    /**
     * Add an account
     *
     * @param idTransaction
     * @param customerId
     * @return
     * @throws BackendConnectorException
     */
    public static int addAccount(String idTransaction, String customerId) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return RubiconCoreConnectorO.addAccount(idTransaction, Integer.parseInt(customerId));
                case CYBERBANK_CONNECTOR:
                    return Integer.parseInt(CyberbankAccountConnector.create(customerId).getData());
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     * Read account
     *
     * @param idTransaction
     * @param product
     * @return
     * @throws BackendConnectorException
     */
    public static Account readAccount(String idTransaction, Product product) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return RubiconCoreConnectorC.readAccountDetails(idTransaction, product.getIdProduct(), product.getExtraInfo());
                case CYBERBANK_CONNECTOR:
                    return CyberbankAccountConnector.read(product).getData();
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     * List account statements
     *
     * @param idTransaction
     * @return
     * @throws BackendConnectorException
     */
    public static StatementResult statements(String idTransaction, String idProduct, String productExtraInfo, Date dateFrom, //NOSONAR
                                             Date dateTo, Double minAmount, Double maxAmount, String reference, String[] channels,
                                             Integer check, Integer movementsPerPage, Integer offset) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return new StatementResult(RubiconCoreConnectorC.listAccountStatements(idTransaction, idProduct, productExtraInfo,
                            dateFrom, dateTo, minAmount, maxAmount, reference, channels, check, movementsPerPage, offset));
                case CYBERBANK_CONNECTOR:
                    CyberbankCoreConnectorResponse<StatementResult> statementResult  = CyberbankAccountConnector.statements(idProduct, productExtraInfo, dateFrom, dateTo, movementsPerPage, offset);
                    return statementResult.getData();
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException e) {
            throw new BackendConnectorException(e);
        }
    }

    public static int countMovements(String extraInfo, StatementResult statementResult) throws BackendConnectorException {
        switch (getCurrentCore()) {
            case RUBICON_CONNECTOR:
                return RubiconCoreConnectorC.listAccountStatementsTotalCount(extraInfo);
            case CYBERBANK_CONNECTOR:
                return statementResult.getTotalAccount();
            default:
                throw new BackendConnectorException("No core connector configured");
        }
    }

    public static Statement readStatement (int idStatement, String idAccount) throws BackendConnectorException {
        try{
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return RubiconCoreConnectorC.readStatement(idStatement,idAccount);
                case CYBERBANK_CONNECTOR:
                    return CyberbankAccountConnector.readStatement(idStatement, idAccount).getData();
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException | IOException e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     * Account list
     *
     * @param idTransaction
     * @return
     * @throws BackendConnectorException
     */
    public static List<Account> list(String idTransaction, List<Client> clients, List<String> typeList) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return (List<Account>) RubiconCoreConnectorC.listProducts(idTransaction, clients, typeList);
                case CYBERBANK_CONNECTOR:
                    return CyberbankAccountConnector.list(idTransaction, clients).getData();
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        } catch (CyberbankCoreConnectorException e) {
            throw new BackendConnectorException(e);
        }
    }

    public static Statement readNoteBoucher(int idStatement) throws BackendConnectorException {
        try {
            switch (getCurrentCore()) {
                case RUBICON_CONNECTOR:
                    return RubiconCoreConnectorC.readNoteBoucher(idStatement);
                case CYBERBANK_CONNECTOR:
                    return CyberbankAccountConnector.readNoteBoucher(idStatement);
                default:
                    throw new BackendConnectorException("No core connector configured");
            }
        }catch (IOException e) {
            throw new BackendConnectorException(e);
        }
    }



}
