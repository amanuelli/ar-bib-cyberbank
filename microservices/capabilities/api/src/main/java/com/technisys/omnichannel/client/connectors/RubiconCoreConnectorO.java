/*
 * Copyright 2017 Technisys.
 *
 * This software component is the intellectual property of Technisys S.A.
 * You are not allowed to use, change or distribute it without express written consent from its author.
 *
 * https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.rubicon.core.onboarding.*;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.ws.BindingProvider;

/**
 * @author dimoda
 */
public class RubiconCoreConnectorO extends RubiconCoreConnector {

    private static final Logger log = LoggerFactory.getLogger(RubiconCoreConnectorO.class);

    // TODO: All the method are returning a POJO without considering the return code... and throwing an exception when there is an error at network level, which is good, but also when the error is at application level (return_code != 0); we should create a Response<T> object with the return code and the data inside ;)

    /**
     * Srv G1 - Consultar informacion de una persona
     *
     * @param idTransaction   Id de transaccion
     * @return Los datos asociados a la persona, null si no existe en el banco
     * @throws BackendConnectorException Error inesperado del backend
     */
    public static int addClient(String idTransaction, int idCliente, String nombres, String apellidos, String email, String direccion,
                                String telefono, String celular, String documento, String tipoDocumento, String paisResidencia, String nacionalidad, String estadoCivil, String sexo,
                                String ocupacion, String segmento, String direccionCorrespondencia, boolean envioFisicoEstadoCuenta) throws BackendConnectorException {

        WsAddClientResponse response = null;

        try {
            WsAddClientRequest request = new WsAddClientRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setIdCliente(idCliente);
            request.setNombres(nombres);
            request.setApellidos(apellidos);
            request.setEmail(email);
            request.setDireccion(direccion);
            request.setTelefono(telefono);
            request.setCelular(celular);
            request.setDocumento(documento);
            request.setTipoDocumento(tipoDocumento);
            request.setPaisResidencia(paisResidencia);
            request.setNacionalidad(nacionalidad);
            request.setEstadoCivil(estadoCivil);
            request.setSexo(sexo);
            request.setOcupacion(ocupacion);
            request.setSegmento(segmento);
            request.setDireccionCorrespondencia(direccionCorrespondencia);
            request.setEnvioFisicoEstadoCuenta(envioFisicoEstadoCuenta);

            if (log.isDebugEnabled()) {
                log.debug("Send to backend: {} ", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
            }

            Onboarding port = WS_ONBOARDING_POOL.borrowObject();
            try {
                response = port.addClient(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_ONBOARDING_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                log.debug("Backend response: {} ", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                log.warn("Backend error (Srv G1 - Consultar informacion de una persona): {} ",  errorDescription);
            } else {
                return response.getIdCliente();
            }
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new BackendConnectorException(e);
        }

        return 0;
    }

    /**
     * Srv G1 - Consultar informacion de una persona
     *
     * @param idTransaction Id de transaccion
     * @param idClient      Cliente del cual se sacan los datos de la direccion (SI
     *                      es null retorna los datos de la primer cuenta)
     * @return Los datos asociados a la persona, null si no existe en el banco
     * @throws BackendConnectorException Error inesperado del backend
     */
    public static int addAccount(String idTransaction, int idClient) throws BackendConnectorException {

        WsAddAccountResponse response = null;

        try {
            WsAddAccountRequest request = new WsAddAccountRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setNumeroCuenta(String.valueOf(idClient));
            request.setCliente(idClient);
            request.setMoneda(ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency"));
            request.setTipo("CA");
            request.setLineaSobregiro(0);
            request.setSaldoPendiente(0);


            if (log.isDebugEnabled()) {
                log.debug("Send to backend: {}", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
            }

            Onboarding port = WS_ONBOARDING_POOL.borrowObject();
            try {
                response = port.addAccount(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_ONBOARDING_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                log.debug("Backend response: {} ", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                log.warn("Backend error (Srv G1 - Consultar informacion de una persona): {} ", errorDescription);
            } else {
                return idClient;
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return 0;
    }
}
