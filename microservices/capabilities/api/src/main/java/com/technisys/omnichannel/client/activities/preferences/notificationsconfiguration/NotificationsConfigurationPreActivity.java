/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.client.activities.preferences.notificationsconfiguration;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.notifications.NotificationsHandlerFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.io.IOException;
import java.util.List;
import java.util.Map;
@DocumentedActivity("Notifications Configuration (PRE)")
public class NotificationsConfigurationPreActivity extends Activity {

    public static final String ID = "preferences.notifications.configuration.pre";
    
    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Communication configuration")
        String COMMUNICATION_CONFIGURATIONS = "communicationConfigurations";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException{
        Response response = new Response(request);
        
        try {
            List<Map.Entry<String, Boolean>> userCommunicationConfigurations = NotificationsHandlerFactory.getHandler().listUserCommunicationsConfiguration(request.getIdUser());
            
            response.putItem(OutParams.COMMUNICATION_CONFIGURATIONS, userCommunicationConfigurations);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
        
        return response;
    }
}