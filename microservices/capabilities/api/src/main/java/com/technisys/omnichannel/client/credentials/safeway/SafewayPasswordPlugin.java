/*
 *  Copyright 2020 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.credentials.safeway;

import com.technisys.omnichannel.ReturnCodes;
import com.technisys.omnichannel.client.activities.session.oauth.ValidateOauthPasswordActivity;
import com.technisys.omnichannel.client.connectors.safeway.SafewayRESTConnector;
import com.technisys.omnichannel.core.IBRequest;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.exceptions.InvalidCredentialException;
import com.technisys.safeway.ApiException;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

/**
 *
 * @author Sebastian Barbosa
 */
public class SafewayPasswordPlugin implements CredentialPlugin {

    private static final Logger LOG = LoggerFactory.getLogger(SafewayPasswordPlugin.class);

    @Override
    public void validate(IBRequest request, Credential credential) throws DispatchingException, IOException {
        try {
            String commonName = request.getIdUser();

            String[] credentialValues = ((Request) request).getParam(ValidateOauthPasswordActivity.InParams.PASSWORD, String.class).split("#");

            String e2eeKey = credentialValues[0];
            String idTransaction = "N/A";

            String resp =  new SafewayRESTConnector().validateSecretKeyWithEndToEnd(commonName, credential.getValue(),e2eeKey,idTransaction);

            if (!StringUtils.equals(resp, "0" )) {
                throw new InvalidCredentialException(ReturnCodes.INVALID_PASSWORD_CREDENTIAL, Credential.PWD_CREDENTIAL, "Invalid password");
            }
        } catch (ApiException ex) {
            throw new DispatchingException(ReturnCodes.UNEXPECTED_ERROR, "Invocation error to Safeway Rest API client", ex);
        }
    }

    @Override
    public boolean validateFormat(IBRequest request, String value) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method validateFormat is not implemented for " + this.getClass().getName());

    }

    @Override
    public boolean validateFormat(String commonName, String value) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method validateFormat is not implemented for " + this.getClass().getName());
    }

    @Override
    public void changeStatus(String commonName, String newValue) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method validateFormat is not implemented for " + this.getClass().getName());
    }

    @Override
    public void modify(IBRequest request, String currentValue, String newValue) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method validateFormat is not implemented for " + this.getClass().getName());
    }

    @Override
    public void recover(String commonName, String newValue) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method validateFormat is not implemented for " + this.getClass().getName());
    }

    @Override
    public void assign(String commonName, String newValue) throws DispatchingException, IOException {
        throw new DispatchingException(ReturnCodes.CREDENTIAL_PLUGIN_METHOD_NOT_IMPLEMENTED, "The method validateFormat is not implemented for " + this.getClass().getName());

    }

    @Override
    public void assign(IBRequest request, String commonName, String value) throws DispatchingException, IOException {
        String idTransaction = "N/A";
        try {

            String[] credentialValues = ((Request) request).getParam(ValidateOauthPasswordActivity.InParams.PASSWORD, String.class).split("#");

            String e2eeKey = credentialValues[1];

            String resp =  new SafewayRESTConnector().addUserPasswordWithEndToEnd(commonName, value, e2eeKey,idTransaction);

            if (!StringUtils.equals(resp, "0" )) {
                String logString = String.format("Error invocando servicio 'addUserPassword' de safeway. %s", resp);
                LOG.error(logString);
                throw new DispatchingException(ReturnCodes.INVALID_PASSWORD_ASSIGN, resp);
            }
        } catch (ApiException ex) {
            throw new IOException(ex);
        }
    }
}
