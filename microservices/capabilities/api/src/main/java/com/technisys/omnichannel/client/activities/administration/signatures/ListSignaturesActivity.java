/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.PaginatedList;
import com.technisys.omnichannel.core.domain.Signature;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * List available signatures schemes for actual environment
 */
@DocumentedActivity("List available signatures schemes for actual environment")
public class ListSignaturesActivity extends Activity {

    public static final String ID = "administration.signatures.list";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "Order by")
        String ORDER_BY = "orderBy";
        @DocumentedParam(type = Integer.class, description = "Page number")
        String PAGE_NUMBER = "pageNumber";
    }

    public interface OutParams {

        @DocumentedParam(type = Integer.class, description = "Current page")
        String CURRENT_PAGE = "currentPage";
        @DocumentedParam(type = Double.class, description = "Default caps for signatures schemes")
        String DEFAULT_SIGNATURE_CAP = "defaultSignatureCap";
        @DocumentedParam(type = String.class, description = "Master currency")
        String MASTER_CURRENCY = "masterCurrency";
        @DocumentedParam(type = List.class, description = "List of signatures schemes")
        String SIGNATURES = "signatures";
        @DocumentedParam(type = Integer.class, description = "Count of pages")
        String TOTAL_PAGES = "totalPages";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            int rowsPerPage = ConfigurationFactory.getInstance().getInt(Configuration.PLATFORM, "administration.rowsPerPage");

            String orderBy = request.getParam(InParams.ORDER_BY, String.class);
            Integer pageNumber = request.getParam(InParams.PAGE_NUMBER, Integer.class);

            Administration admin = Administration.getInstance();
            Environment env = admin.readEnvironment(request.getIdEnvironment());

            //filtro los tipos de firma
            List<String> includeSignatureTypes = Arrays.asList(new String[]{Signature.TYPE_AMOUNT, Signature.TYPE_NO_AMOUNT});

            PaginatedList list = LimitsHandlerFactory.getHandler().listSignatures(env.getIdEnvironment(), includeSignatureTypes, pageNumber, rowsPerPage, orderBy);

            if (list.getElementList() != null) {
                I18n i18n = I18nFactory.getHandler();
                for (Signature signature : (List<Signature>) list.getElementList()) {
                    signature.setSignatureTypeDescription(i18n.getMessageForUser("signatures.type." + signature.getSignatureType(), request.getIdUser()));
                }
            }

            response.putItem(OutParams.SIGNATURES, list.getElementList());
            response.putItem(OutParams.CURRENT_PAGE, pageNumber);
            response.putItem(OutParams.TOTAL_PAGES, list.getTotalPages());
            response.putItem(OutParams.DEFAULT_SIGNATURE_CAP, ConfigurationFactory.getInstance().getDouble(Configuration.LIMITS, "default_cap_signature_" + request.getEnvironmentType()));
            response.putItem(OutParams.MASTER_CURRENCY, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency"));

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String pageNum = request.getParam(InParams.PAGE_NUMBER, String.class);
        int idEnvironment = request.getIdEnvironment();
        if (idEnvironment < 1){
            result.put("idEnvironment", "administration.idEnvironment.invalid");
        } else {
            try {
                if (StringUtils.isBlank(pageNum)
                        || !StringUtils.isNumeric(pageNum)
                        || Integer.parseInt(pageNum) < 1)
                    result.put(InParams.PAGE_NUMBER, "administration.signatures.list.pageNumber.invalid");
            } catch (NumberFormatException ex) {
                result.put(InParams.PAGE_NUMBER, "administration.signatures.list.pageNumber.invalid");
            }
        }
        return result;
    }
}
