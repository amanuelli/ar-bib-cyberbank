/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain;

import java.io.Serializable;
import java.util.Date;

public class Identification implements Serializable {
    private String type;
    private String number;
    private Date expirationDate;

    public Identification(){
    }

    public Identification(String dni, String documentNumber, Date expirationDate) {
        setType(dni);
        setNumber(documentNumber);
        setExpirationDate(expirationDate);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

}
