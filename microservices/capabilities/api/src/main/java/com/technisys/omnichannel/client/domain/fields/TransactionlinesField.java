package com.technisys.omnichannel.client.domain.fields;

import com.technisys.omnichannel.annotations.docs.DocumentedTypeParam;
import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.FormFieldWithCap;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.utils.RequestParamsUtils;

import java.util.*;
import java.util.stream.Collectors;

public class TransactionlinesField extends FormFieldWithCap {
    @DocumentedTypeParam(description = "Internationalized messages to display when no field value is set")
    private Map<String, String> requiredErrorMap;

    @DocumentedTypeParam(description = "Maximum lines to be uploaded")
    private int maxAcceptedLines;

    public int getMaxAcceptedLines() {
        return maxAcceptedLines;
    }

    public void setMaxAcceptedLines(int maxAcceptedLines) {
        this.maxAcceptedLines = maxAcceptedLines;
    }

    public Map<String, String> getRequiredErrorMap() {
        return requiredErrorMap;
    }

    public void setRequiredErrorMap(Map<String, String> requiredErrorMap) {
        this.requiredErrorMap = requiredErrorMap;
    }

    public void addRequiredError(String lang, String value) {
        if (requiredErrorMap == null) {
            requiredErrorMap = new LinkedHashMap<>();
        }
        requiredErrorMap.put(lang, value);
    }

    @Override
    public String getLabelDefault() {
        return I18nFactory.getHandler().getMessage("core.notApply", ConfigurationFactory.getInstance().getDefaultString(Configuration.PLATFORM,  "core.default.lang", "en"));
    }

    @Override
    public List<Amount> getAmounts(Map<String, Object> parameters) {
        List<Amount> amounts = new ArrayList();
        List list = RequestParamsUtils.getValue(parameters.get(idField), List.class);
        if (list != null) {
            List<TransactionLine> lines = (List<TransactionLine>) list.stream()
                    .map(item -> new TransactionLine((Map<String, Object>) item))
                    .collect(Collectors.toList());
            Amount totalAmount = new Amount();
            totalAmount.setCurrency(lines.get(0).getCreditAmountCurrency());
            for (TransactionLine line : lines) {
                totalAmount.setQuantity(Double.sum(totalAmount.getQuantity(), line.getCreditAmountQuantity()));
            }
            amounts.add(totalAmount);
        }
        return amounts;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof TransactionlinesField) {
            TransactionlinesField ff = (TransactionlinesField) o;
            return (ff.getIdForm().equals(idForm) && ff.getIdField().equals(idField));
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + Objects.hashCode(this.idField);
        hash = 41 * hash + Objects.hashCode(this.idForm);
        hash = 41 * hash + getFormVersion();
        return hash;
    }
}
