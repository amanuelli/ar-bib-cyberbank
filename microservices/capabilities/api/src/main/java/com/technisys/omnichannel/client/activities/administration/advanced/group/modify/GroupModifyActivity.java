package com.technisys.omnichannel.client.activities.administration.advanced.group.modify;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.administration.advanced.group.create.GroupCreateActivity;
import com.technisys.omnichannel.client.activities.administration.common.Permissions;
import com.technisys.omnichannel.client.activities.administration.medium.ModifyPermissionsActivity;
import com.technisys.omnichannel.client.utils.ValidationUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Group;
import com.technisys.omnichannel.core.domain.GroupPermission;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.technisys.omnichannel.client.utils.StringUtils.stripHTML;

/**
 * Use this to modify a new group
 */
@DocumentedActivity("Modify group")
public class GroupModifyActivity extends Activity {

    public static final String ID = "administration.advanced.group.modify.send";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "ID of the env group to modify information")
        String ID = "id";
        @DocumentedParam(type = String.class, description = "Group's name")
        String NAME = "name";
        @DocumentedParam(type = String.class, description = "Group's description")
        String DESCRIPTION = "description";
        @DocumentedParam(type = String.class, description = "Group's status")
        String STATUS = "status";
        @DocumentedParam(type = List.class, description = "Group's users")
        String USERS = "users";
        @DocumentedParam(type = Map.class, description = "Group's modified permissions")
        String PERMISSIONS = "permissions";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "Group's id, it can be used to redirect to the group detail page")
        String ID_GROUP = "idGroup";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            if (!Environment.ADMINISTRATION_SCHEME_ADVANCED.equals(request.getEnvironmentAdminScheme())) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            int idGroup = request.getParam(InParams.ID, int.class);
            Group group = AccessManagementHandlerFactory.getHandler().getGroup(idGroup);
            if (group == null || group.getIdEnvironment() != request.getIdEnvironment()) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

            String name = request.getParam(InParams.NAME, String.class);
            String description = request.getParam(InParams.DESCRIPTION, String.class);
            String status = request.getParam(InParams.STATUS, String.class);
            String[] users = request.getParam(InParams.USERS, String[].class);
            Map<String, List<String>> newPermissions = request.getParam(InParams.PERMISSIONS, Map.class);

            List<GroupPermission> defaultUserPermissions = Permissions.listDefault(idGroup);
            List<GroupPermission> permissions = Permissions.list(idGroup);

            permissions.addAll(defaultUserPermissions);
            Permissions.addIncomingPermissions(idGroup, newPermissions, permissions);
            group.setName(name);
            group.setDescription(description);
            AccessManagementHandlerFactory.getHandler().updateGroup(group, (users != null ? Arrays.asList(users) : new ArrayList<>()), permissions);

            if ("blocked".equals(status)) {
                AccessManagementHandlerFactory.getHandler().updateGroupsStatus(Arrays.asList(group.getIdGroup()), true);
            }

            response.putItem(OutParams.ID_GROUP, group.getIdGroup());
            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = validateFields(request);

        try {
            result.putAll(ValidationUtils.validateEmptyCredentials(request));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }

    protected static Map<String, String> validateFields(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            Administration admin = Administration.getInstance();
            List<String> envProducts = admin.listEnvironmentProductIds(request.getIdEnvironment());
            String name = request.getParam(InParams.NAME, String.class);
            Map<String, List<String>> newPermissions = request.getParam(ModifyPermissionsActivity.InParams.PERMISSIONS, Map.class);

            Integer groupId = request.getParam(InParams.ID, Integer.class);

            if (groupId == null) {
                result.put(InParams.ID, "administration.groups.id.empty");
            } else {
                validateName(request, result, name, groupId);
                Permissions.validatePermissions(result, envProducts, newPermissions);
                GroupCreateActivity.validateUsers(request);
            }

            return result;
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }
    }

    private static void validateName(Request request, Map<String, String> result, String name, Integer idGroup) throws IOException {
        if (StringUtils.isEmpty(stripHTML(name))) {
            result.put(InParams.NAME, "administration.groups.nameEmpty");
        } else if (name.length() > 253) {
            result.put(InParams.NAME, "administration.groups.nameMaxLengthExceeded");
        } else {
            List<Group> listGroups = AccessManagementHandlerFactory.getHandler().getGroups(null, null, request.getIdEnvironment(), 0, 0, null).getElementList();
            boolean duplicatedName = false;

            for (int i = 0; i < listGroups.size() && !duplicatedName; i++) {
                Group group = listGroups.get(i);

                if (name.equals(group.getName()) && group.getIdGroup() != idGroup) {
                    duplicatedName = true;
                }
            }

            if (duplicatedName) {
                result.put(InParams.NAME, "administration.groups.nameAlreadyExists");
            }
        }
    }
}
