/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.creditcards;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.connectors.BackendConnectorException;
import com.technisys.omnichannel.client.connectors.RubiconCoreConnectorTC;
import com.technisys.omnichannel.client.domain.CreditCard;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.utils.CoreUtils;
import com.technisys.omnichannel.core.utils.plugins.ProductLabeler;
import java.io.IOException;

/**
 *
 */
@DocumentedActivity("Read credit card's details")
public class ReadCreditCardActivity extends Activity {

    public static final String ID = "creditCards.read";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "Credit card Id")
        String ID_CREDIT_CARD = "idCreditCard";
    }

    public interface OutParams {

        @DocumentedParam(type = CreditCard.class, description = "Credit card object's details")
        String CREDIT_CARD = "creditCard";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            if (Administration.getInstance().readEnvironment(request.getIdEnvironment()) == null) {
                //si no hay cliente asociado al ambiente
                throw new ActivityException(ReturnCodes.ENVIRONMENT_NOT_AUTHORIZED);
            }
            String idCreditCard = (String) request.getParam(InParams.ID_CREDIT_CARD, String.class);

            ProductLabeler pLabeler = CoreUtils.getProductLabeler(request.getLang());

            Product product = Administration.getInstance().readProduct(idCreditCard, request.getIdEnvironment());
            CreditCard creditCard = RubiconCoreConnectorTC.readCreditCardDetails(request.getIdTransaction(), product.getIdProduct(), product.getExtraInfo());
            creditCard.setPaperless(product.getPaperless());
            creditCard.setProductAlias(product.getProductAlias());
            creditCard.setShortLabel(pLabeler.calculateShortLabel(creditCard));
            creditCard.setLabel(pLabeler.calculateLabel(creditCard));
            creditCard.setFranchise(pLabeler.calculateFranchise(creditCard));

            response.putItem(OutParams.CREDIT_CARD, creditCard);

            response.setReturnCode(ReturnCodes.OK);
        } catch (BackendConnectorException ex) {
            throw new ActivityException(ReturnCodes.BACKEND_SERVICE_ERROR, ex);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }
}
