/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.Signature;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.limits.CapForSignature;
import com.technisys.omnichannel.core.limits.LimitsHandlerFactory;

import java.io.IOException;
import java.util.*;

import static com.technisys.omnichannel.core.utils.CoreUtils.CHANNEL_ALL;

/**
 * Delete a signatures scheme (PRE)
 */
@DocumentedActivity("Delete a signatures scheme (PRE)")
public class DeleteSignaturesPreActivity extends Activity {
    public static final String ID = "administration.signatures.delete.pre";

    public interface InParams {

        @DocumentedParam(type = Integer.class, description = "Signature scheme identifier")
        String ID = "id";
    }

    public interface OutParams {

        @DocumentedParam(type = Map.class, description = "Caps for signature")
        String CAPS = "caps";
        @DocumentedParam(type = List.class, description = "List of frequencies for transactions with amount")
        String CAP_FREQUENCY_LIST = "capFrequencyList";
        @DocumentedParam(type = List.class, description = "List of enabled channels")
        String ENABLED_CHANNELS = "enabledChannels";
        @DocumentedParam(type = String.class, description = "Master currency")
        String MASTER_CURRENCY = "masterCurrency";
        @DocumentedParam(type = List.class, description = "Selected functional groups")
        String SELECTED_FUNCTIONAL_GROUPS = "selectedFunctionalGroups";
        @DocumentedParam(type = Signature.class, description = "Signature data")
        String SIGNATURE = "signature";
        @DocumentedParam(type = Map.class, description = "Number of signers for each signature level")
        String SIGNATURE_GROUP_MAP = "signatureGroupMap";
        @DocumentedParam(type = List.class, description = "List of available signature types")
        String SIGNATURE_TYPE_LIST = "signatureTypeList";
    }
    
    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        try {
            String adminScheme = request.getEnvironmentAdminScheme();
            if (Environment.ADMINISTRATION_SCHEME_SIMPLE.equals(adminScheme)) {
                throw new ActivityException(ReturnCodes.INVALID_ENVIRONMENT_SCHEME);
            }

            int idSignature = request.getParam(InParams.ID, int.class);
            Signature signature = LimitsHandlerFactory.getHandler().readSignature(idSignature);
            
            // CAPS
            Map<String, CapForSignature> caps = new HashMap<>();
            double defaultCapMaximum = ConfigurationFactory.getInstance().getDouble(Configuration.LIMITS, "default_cap_signature_" + request.getEnvironmentType());

            CapForSignature cap = signature.getCap(CHANNEL_ALL);
            if (cap != null) {
                if (cap.getMaximum() < 0) {
                    cap.setMaximum(defaultCapMaximum);
                }
                caps.put(CHANNEL_ALL, cap);
            }

            Map<String, Integer> signatureGroupMap = new HashMap<>();
            String currLevel;
            Integer totalNedded;
            for (int i = 0; i < signature.getSignatureGroup().length(); i++) {
                currLevel = String.valueOf(signature.getSignatureGroup().charAt(i));
                totalNedded = signatureGroupMap.get(currLevel);
                if (totalNedded == null) {
                    totalNedded = 0;
                }

                signatureGroupMap.put(currLevel, totalNedded + 1);
            }

            response.putItem(OutParams.SIGNATURE, signature);
            response.putItem(OutParams.SIGNATURE_GROUP_MAP, signatureGroupMap);
            response.putItem(OutParams.SIGNATURE_TYPE_LIST, Arrays.asList(new String[]{Signature.TYPE_AMOUNT, Signature.TYPE_NO_AMOUNT}));
            response.putItem(OutParams.CAP_FREQUENCY_LIST, ConfigurationFactory.getInstance().getList(Configuration.PLATFORM, "frontend.channels.enabledFrequencies"));
            response.putItem(OutParams.MASTER_CURRENCY, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency"));
            
            response.putItem(OutParams.SELECTED_FUNCTIONAL_GROUPS, signature.getSignatureFeatures());

            List enabledChannels = new ArrayList<>();
            enabledChannels.add(CHANNEL_ALL);
            response.putItem(OutParams.ENABLED_CHANNELS, enabledChannels);
            response.putItem(OutParams.CAPS, caps);

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();

        try {
            int idSignature = request.getParam(InParams.ID, int.class);
            Signature signature = LimitsHandlerFactory.getHandler().readSignature(idSignature);

            if (signature == null || signature.getIdEnvironment() != request.getIdEnvironment() || Signature.TYPE_ADMINISTRATIVE.equals(signature.getSignatureType())) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
