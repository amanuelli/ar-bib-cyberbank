package com.technisys.omnichannel.client.activities.files;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.activities.accounts.DownloadMovementsActivity;
import com.technisys.omnichannel.client.domain.TransactionLine;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.activities.ActivityHandler;
import com.technisys.omnichannel.core.activities.ClientActivityDescriptor;
import com.technisys.omnichannel.core.domain.Transaction;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.i18n.I18n;
import com.technisys.omnichannel.core.i18n.I18nFactory;
import com.technisys.omnichannel.core.preprocessors.authorization.Authorization;
import com.technisys.omnichannel.core.transactions.TransactionHandlerFactory;
import com.technisys.omnichannel.core.utils.IOUtils;
import net.sf.jxls.transformer.XLSTransformer;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author emordezki
 */
@DocumentedActivity("Download file with lines passing a filter of multiline payment")
public class DownloadLinesActivity extends Activity {

    public static final String ID = "files.downloadLines";

    public interface InParams {

        @DocumentedParam(type = String.class, description = "ID of the associated transaction")
        String ID_TRANSACTION = "idTransactionToRead";
        @DocumentedParam(type = String.class, description = "Minimum amount paid")
        String MIN_AMOUNT = "minAmount";
        @DocumentedParam(type = String.class, description = "Maximum amount paid")
        String MAX_AMOUNT = "maxAmount";
        @DocumentedParam(type = String.class, description = "Currency of the amount paid")
        String CURRENCY = "currency";
        @DocumentedParam(type = String.class, description = "Name of the payee")
        String ACCOUNT_NAME = "accountName";
        @DocumentedParam(type = String.class, description = "Account of the payee")
        String CREDIT_ACCOUNT = "creditAccount";
        @DocumentedParam(type = String.class, description = "Format of the file to download")
        String FORMAT = "format";
        @DocumentedParam(type = String.class, description = "Status to filter")
        String STATUS = "status";
    }

    public interface OutParams {

        @DocumentedParam(type = String.class, description = "File name")
        String FILE_NAME = "fileName";
        @DocumentedParam(type = String.class, description = "Base64 encoded file content")
        String CONTENT = "content";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response;

        try {
            String idTransaction = request.getParam(DownloadLinesActivity.InParams.ID_TRANSACTION, String.class);
            String format = request.getParam(DownloadLinesActivity.InParams.FORMAT, String.class);
            List<TransactionLine> lines = ListTransactionLinesActivity.list(request, idTransaction, null);
            String fileName = idTransaction + "_filtered";

            if ("xls".equalsIgnoreCase(format)) {
                response = exportXLS(idTransaction, lines, request, fileName);
            } else {
                response = exportTXT(lines, request, fileName);
            }

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException | InvalidFormatException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        return response;
    }

    private Response exportTXT(List<TransactionLine> lines, Request request, String fileName) throws IOException {
        String content;
        Response response = new Response(request);

        try (ByteArrayOutputStream os = new ByteArrayOutputStream()) {
            for (TransactionLine line : lines) {
                String text = line.toString() + System.lineSeparator();

                os.write(text.getBytes());
            }

            content = new String(Base64.encodeBase64(os.toByteArray()));
        }

        response.putItem(DownloadLinesActivity.OutParams.FILE_NAME, fileName + ".txt");
        response.putItem(DownloadLinesActivity.OutParams.CONTENT, content);
        return response;
    }

    private Response exportXLS(String idTransaction, List<TransactionLine> lines, Request request, String fileName) throws IOException, InvalidFormatException {

        List<List<String>> header = new ArrayList<>();
        List<List<String>> body = new ArrayList<>();
        List<List<String>> footer = new ArrayList<>();

        I18n i18n = I18nFactory.getHandler();

        // Header
        List<String> headLine = new ArrayList<>();
        headLine.add(i18n.getMessage("transaction.process.details.table.header.title", request.getLang()));
        header.add(headLine);

        headLine = new ArrayList<>();
        headLine.add(I18nFactory.getHandler().getMessage("transaction.process.details.table.header.transaction", request.getLang()));
        headLine.add(idTransaction);
        header.add(headLine);

        // Body
        List<String> bodyLines = new ArrayList<>();

        bodyLines.add(i18n.getMessage("transaction.process.details.table.header.accountName", request.getLang()));
        bodyLines.add(i18n.getMessage("transaction.process.details.table.header.creditAccount", request.getLang()));
        bodyLines.add(i18n.getMessage("transaction.process.details.table.header.amount", request.getLang()));
        bodyLines.add(i18n.getMessage("transaction.process.details.table.header.currency", request.getLang()));
        bodyLines.add(i18n.getMessage("transaction.process.details.table.header.bank", request.getLang()));

        body.add(bodyLines);

        for (TransactionLine line : lines) {
            bodyLines = new ArrayList<>();

            bodyLines.add(line.getCreditAccountName());
            bodyLines.add(line.getCreditAccountNumber());
            bodyLines.add(line.getCreditAmountQuantity().toString());
            bodyLines.add(i18n.getMessage("currency.label." + line.getCreditAmountCurrency(), request.getLang()));
            bodyLines.add(line.getBankIdentifier());

            body.add(bodyLines);
        }

        // Footer
        List<String> footerLine = new ArrayList<>();
        footerLine.add(i18n.getMessage("transaction.process.details.table.footer.count", request.getLang()));
        footerLine.add(String.valueOf(lines.size()));
        footer.add(footerLine);

        //Realizamos la transformacion
        XLSTransformer transformer = new XLSTransformer();

        Map<String, Object> beans = new HashMap<>();

        // Unescape all content
        List<List<String>> unescapedHeader = new ArrayList<>();
        for (List<String> sublist : header) {
            List<String> unescapedSublist = new ArrayList<>();

            sublist.forEach(record -> unescapedSublist.add(StringEscapeUtils.unescapeHtml4(record)));

            unescapedHeader.add(unescapedSublist);
        }

        List<List<String>> unescapedLines = new ArrayList<>();
        for (List<String> sublist : body) {
            List<String> unescapedSublist = new ArrayList<>();

            sublist.forEach(record -> unescapedSublist.add(StringEscapeUtils.unescapeHtml4(record)));

            unescapedLines.add(unescapedSublist);
        }

        List<List<String>> unescapedFooter = new ArrayList<>();
        for (List<String> sublist : footer) {
            List<String> unescapedSublist = new ArrayList<>();

            sublist.forEach(record -> unescapedSublist.add(StringEscapeUtils.unescapeHtml4(record)));

            unescapedFooter.add(unescapedSublist);
        }

        beans.put("header", unescapedHeader);
        beans.put("lines", unescapedLines);
        beans.put("footer", unescapedFooter);

        byte[] data;
        try (ByteArrayOutputStream out = new ByteArrayOutputStream();
             Workbook work = WorkbookFactory.create(DownloadMovementsActivity.class.getResourceAsStream("/templates/account_template.xls"));) {
            transformer.transformWorkbook(work, beans);
            work.write(out);
            data = out.toByteArray();
            out.flush();
        }
        InputStream pdfSource = new ByteArrayInputStream(data);

        byte[] content = Base64.encodeBase64(IOUtils.toByteArray(pdfSource));

        Response response = new Response(request);
        response.putItem(DownloadLinesActivity.OutParams.FILE_NAME, fileName + ".xls");
        response.putItem(DownloadLinesActivity.OutParams.CONTENT, content);

        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        String idTransactionToRead = request.getParam(DownloadLinesActivity.InParams.ID_TRANSACTION, String.class);

        try {
            Transaction transaction = TransactionHandlerFactory.getInstance().read(idTransactionToRead);

            if (transaction == null) {
                throw new ActivityException(ReturnCodes.TRANSACTION_DOESNT_EXIST);
            }
            if (transaction.getIdEnvironment() != request.getIdEnvironment()) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }

            ClientActivityDescriptor ad = ActivityHandler.getInstance().readDescriptor(transaction.getIdActivity());
            if (!Authorization.hasPermissionOverTransaction(request.getIdUser(), request.getIdEnvironment(), ad, transaction)) {
                throw new ActivityException(ReturnCodes.NOT_AUTHORIZED);
            }
        } catch (IOException ex) {
            throw new ActivityException(com.technisys.omnichannel.ReturnCodes.IO_ERROR, ex);
        }

        return result;
    }
}
