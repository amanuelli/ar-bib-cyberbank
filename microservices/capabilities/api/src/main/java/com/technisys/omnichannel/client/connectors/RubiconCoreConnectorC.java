/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors;

import com.google.common.io.ByteStreams;
import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.domain.*;
import com.technisys.omnichannel.client.handlers.notes.NotesHandler;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.clients.ClientHandler;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;
import com.technisys.omnichannel.core.utils.CacheUtils;
import com.technisys.omnichannel.core.utils.DateUtils;
import com.technisys.omnichannel.core.utils.IOUtils;
import com.technisys.omnichannel.rubicon.core.accounts.*;
import com.technisys.omnichannel.rubicon.core.creditcards.*;
import com.technisys.omnichannel.rubicon.core.deposits.*;
import com.technisys.omnichannel.rubicon.core.general.WsBaseResponse;
import com.technisys.omnichannel.rubicon.core.general.*;
import com.technisys.omnichannel.rubicon.core.general.WsListProductsRequest.Types;
import com.technisys.omnichannel.rubicon.core.loans.*;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.RecursiveToStringStyle;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.BindingProvider;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

/**
 * @author salva
 */
public class RubiconCoreConnectorC extends RubiconCoreConnector {

    private static final Logger log = LoggerFactory.getLogger(RubiconCoreConnectorC.class);

    // TODO: All the method are returning a POJO without considering the return code... and throwing an exception when there is an error at network level, which is good, but also when the error is at application level (return_code != 0); we should create a Response<T> object with the return code and the data inside ;)

    public static List<? extends Product> listProducts(String idTransaction, String idClient, List<String> typeList) throws BackendConnectorException {
        Client cli = null;
        try {
            cli = ClientHandler.readClient(idClient);
        } catch (IOException e) {
            log.info("Client not yet created in digital, send null client name");
        }
        if (cli == null) {
            cli = new Client(idClient, "");
        }
        List<Client> clients = new ArrayList();
        clients.add(cli);
        return listProducts(idTransaction, clients, typeList);
    }


    /**
     * Srv G4 - Listar productos
     *
     * @param idTransaction Id de transaccion
     * @param clients       Lista de clientes del ambiente para consultar sus productos
     * @param typeList      Lista de tipos de producto a retornar
     * @return Informacion de los productos
     * @throws BackendConnectorException Error de ejecucion en el servicio
     */
    public static List<? extends Product> listProducts(String idTransaction, List<Client> clients, List<String> typeList) throws BackendConnectorException {
        List<Product> productList = new ArrayList<>();
        WsListProductsResponse response = null;

        try {
            WsListProductsRequest request = new WsListProductsRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);
            String logString = "";

            com.technisys.omnichannel.rubicon.core.general.ObjectFactory factory = new com.technisys.omnichannel.rubicon.core.general.ObjectFactory();

            Types types = factory.createWsListProductsRequestTypes();
            for (String type : typeList) {
                types.getType().add(type);
            }

            for (Client client : clients) {
                request.setClientID(Integer.parseInt(client.getIdClient()));
                request.setTypes(types);

                if (log.isDebugEnabled()) {
                    logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                    log.debug(logString);
                }

                General port = WS_GENERAL_POOL.borrowObject();

                try {
                    response = port.listProducts(request);
                } catch (Exception e) {
                    throw e;
                } finally {
                    WS_GENERAL_POOL.returnObject(port);
                }
                if (log.isDebugEnabled()) {
                    logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                    log.debug(logString);
                }

                if (response.getReturnCode() != 0) {
                    String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                    logString = String.format("Error en backend (Srv G4 - Listar productos): %s", errorDescription);
                    log.warn(logString);
                    throw new BackendConnectorException(errorDescription, (int) response.getReturnCode());
                } else {
                    if (response.getProducts() != null && response.getProducts().getProduct() != null) {

                        for (WsProduct product : response.getProducts().getProduct()) {
                            Map<String, String> fieldsMap = RubiconCoreConnector.convertDynamicListToMap(product.getFields().getField());

                            switch (StringUtils.defaultString(fieldsMap.get(ProductUtils.FIELD_PRODUCT_TYPE))) {
                                case Constants.PRODUCT_CA_KEY:
                                case Constants.PRODUCT_CC_KEY:
                                    Account account = new Account(fieldsMap);
                                    account.setClient(client);
                                    productList.add(account);
                                    break;
                                case Constants.PRODUCT_TC_KEY:
                                    CreditCard creditcard = new CreditCard(fieldsMap);
                                    creditcard.setClient(client);
                                    productList.add(creditcard);
                                    break;
                                case Constants.PRODUCT_PI_KEY:
                                case Constants.PRODUCT_PA_KEY:
                                    Loan loan = new Loan(fieldsMap);
                                    loan.setClient(client);
                                    productList.add(loan);
                                    break;
                                case Constants.PRODUCT_PF_KEY:
                                    Deposit deposit = new Deposit(fieldsMap);
                                    deposit.setClient(client);
                                    productList.add(deposit);
                                    break;
                                default:
                                    logString = String.format("Unexpected StringUtils.defaultString(fieldsMap.get(ProductUtils.FIELD_PRODUCT_TYPE)) value %s", StringUtils.defaultString(fieldsMap.get(ProductUtils.FIELD_PRODUCT_TYPE)));
                                    log.error(logString);
                                    break;
                            }
                        }

                    }

                }
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return productList;
    }


    /**
     * Srv C2 - Devuelve el total de movimientos de una cuenta
     *
     * @param extraInfo ExtraInfo del producto
     * @return cantidad total de movimientos de la cuenta
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static int listAccountStatementsTotalCount(String extraInfo) throws BackendConnectorException {
        WsListAccountMovementsResponse response = null;
        int totalStatements = 0;

        try {
            WsListAccountMovementsRequest request = new WsListAccountMovementsRequest();
            request.setChannel(CHANNEL);

            request.setProductID(ProductUtils.getBackendID(extraInfo));


            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            Accounts port = WS_ACCOUNTS_POOL.borrowObject();
            try {
                response = port.listTotalAccountMovements(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_ACCOUNTS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv C2 - Listar total de movimientos de una cuenta): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                totalStatements = response.getTotalRecords();
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return totalStatements;
    }


    /**
     * Srv C2 - Listar movimientos de una cuenta
     *
     * @param idTransaction Id de transaccion
     * @param idProduct     Id de producto
     * @param extraInfo     ExtraInfo del producto
     * @param dateFrom      Filtrado por fecha de inicio
     * @param dateTo        Filtrado por fecha de fin
     * @param minAmount     Filtrado por importe minimo
     * @param maxAmount     Filtrado por importe maximo
     * @param reference     Filtrado por referencia
     * @param channels      Filtrado por canales
     * @param check         Filtrado por cheque
     * @param limit         Limite de los resultados
     * @param offset        Offset de los resultados
     * @return Lista de los movimientos de la cuenta
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static List<Statement> listAccountStatements(String idTransaction, String idProduct, String extraInfo, Date dateFrom, Date dateTo, Double minAmount, Double maxAmount, String reference, String[] channels, Integer check, int limit, int offset) throws BackendConnectorException {
        WsListAccountMovementsResponse response = null;
        List<Statement> statements = new ArrayList<>();

        try {
            WsListAccountMovementsRequest request = new WsListAccountMovementsRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setProductID(ProductUtils.getBackendID(extraInfo));

            XMLGregorianCalendar xmlDate = null;
            if (dateFrom != null) {
                GregorianCalendar date = new GregorianCalendar();
                date.setTime(dateFrom);
                date = DateUtils.changeTime(date, 0, 0, 0, 0);
                xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
            }
            request.setDateFrom(xmlDate);

            xmlDate = null;
            if (dateTo != null) {
                GregorianCalendar date = new GregorianCalendar();
                date.setTime(dateTo);
                date = DateUtils.changeTime(date, 23, 59, 59, 999);
                xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
            }
            request.setDateTo(xmlDate);

            request.setOffset(offset);
            request.setLimit(limit);
            request.setMaxAmount(maxAmount);
            request.setMinAmount(minAmount);
            request.setReference(reference);
            request.setCheck(check);

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            Accounts port = WS_ACCOUNTS_POOL.borrowObject();

            try {
                response = port.listAccountMovements(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_ACCOUNTS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv C2 - Listar movimientos de una cuenta): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {

                if (response.getMovements() != null && response.getMovements().getMovement() != null) {
                    Statement statement;
                    for (WsAccountMovement movement : response.getMovements().getMovement()) {
                        statement = new Statement(movement);
                        statement.setIdAccount(idProduct);
                        statement.setNote(NotesHandler.readNote(statement.getIdStatement(), idProduct));
                        statements.add(statement);
                    }
                }
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return statements;
    }

    /**
     * Srv C3 - Account fund
     *
     * @param accountId Account to fund
     * @param amount    Amount
     * @param concept   Concept
     * @param reference Reference
     * @return WsAccountFundResponse
     * @throws BackendConnectorException Error connecting to Backend
     */
    public static WsAccountFundResponse fundAccount(String accountId, double amount, String concept, String reference) throws BackendConnectorException {
        WsAccountFundResponse response = null;
        try {
            WsAccountFundRequest request = new WsAccountFundRequest();
            request.setChannel(CHANNEL);
            request.setCreditAccountID(accountId);
            request.setAmount(amount);
            request.setConcept(concept);
            request.setReference(reference);

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Send to backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            Accounts port = WS_ACCOUNTS_POOL.borrowObject();

            try {
                response = port.accountFund(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_ACCOUNTS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Backend response: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error on backend (Srv C3 - Account fund): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return response;
    }

    /**
     * Srv D1 - Consultar detalle de un deposito
     *
     * @param idTransaction Id de transaccion
     * @param idProduct     Id del producto
     * @param extraInfo     Extra info del producto
     * @return Informacion del deposito
     * @throws IOException               Error de base de datos
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static Deposit readDepositDetails(String idTransaction, String idProduct, String extraInfo) throws IOException, BackendConnectorException {
        Deposit deposit = null;
        WsReadDepositResponse response = null;

        try {
            WsReadDepositRequest request = new WsReadDepositRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setProductID(ProductUtils.getBackendID(extraInfo));

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            Deposits port = WS_DEPOSITS_POOL.borrowObject();

            try {
                response = port.readDeposit(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_DEPOSITS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv D1 - Consultar detalle de un deposito): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                deposit = new Deposit(response);
                deposit.setIdProduct(idProduct);
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return deposit;
    }

    /**
     * Srv D2 - Listar movimientos de un deposito
     *
     * @param idTransaction Id de transaccion
     * @param idProduct     Id de producto
     * @param extraInfo     ExtraInfo del producto
     * @param dateFrom      Filtrado por fecha de inicio
     * @param dateTo        Filtrado por fecha de fin
     * @param minAmount     Filtrado por importe minimo
     * @param maxAmount     Filtrado por maximo
     * @param reference     Filtrado por referencia
     * @param limit         Limite de los resultados
     * @param offset        Offset de los resultados
     * @return Lista de los movimientos del deposito
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static List<StatementDeposit> listDepositsStatements(String idTransaction, String idProduct, String extraInfo, Date dateFrom, Date dateTo, Double minAmount, Double maxAmount, String reference, int limit, int offset) throws BackendConnectorException {
        WsListDepositMovementsResponse response = null;
        List<StatementDeposit> statements = new ArrayList<>();

        try {
            WsListDepositMovementsRequest request = new WsListDepositMovementsRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setProductID(ProductUtils.getBackendID(extraInfo));

            XMLGregorianCalendar xmlDate = null;
            if (dateFrom != null) {
                GregorianCalendar date = new GregorianCalendar();
                date.setTime(dateFrom);
                xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
            }
            request.setDateFrom(xmlDate);

            xmlDate = null;
            if (dateTo != null) {
                GregorianCalendar date = new GregorianCalendar();
                date.setTime(dateTo);
                xmlDate = DatatypeFactory.newInstance().newXMLGregorianCalendar(date);
            }
            request.setDateTo(xmlDate);

            request.setOffset(offset);
            request.setLimit(limit);
            request.setMaxAmount(maxAmount);
            request.setMinAmount(minAmount);
            request.setReference(reference);

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            Deposits port = WS_DEPOSITS_POOL.borrowObject();

            try {
                response = port.listDepositMovements(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_DEPOSITS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv D2 - Listar movimientos de un deposito): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                if (response.getMovements() != null && response.getMovements().getMovement() != null) {
                    StatementDeposit statement;
                    for (WsDepositMovement movement : response.getMovements().getMovement()) {
                        statement = new StatementDeposit(movement);
                        statement.setIdAccount(idProduct);
                        statement.setNote(NotesHandler.readNote(statement.getIdStatement(), idProduct));
                        statements.add(statement);
                    }
                }
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }
        return statements;
    }

    //DUMMY METHOD
    public static List<StatementLine> listAccountStatementLines(String idAccount) throws IOException {
        List<StatementLine> statementLines = new ArrayList();
        StatementLine statementLine = getDummyStatementLine();
        statementLine.setIdProduct(idAccount);
        statementLines.add(statementLine);

        return statementLines;
    }

    //DUMMY METHOD
    public static Statement readNoteBoucher(int idStatement) throws IOException {
        Statement statement = new Statement();
        statement.setIdStatement(idStatement);
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try(InputStream imageSource = classLoader.getResourceAsStream(String.format("/dummy/check-%s.png", String.valueOf(Math.floorMod(idStatement, 3) + 1)))) {
        String content = "data:image/jpeg;base64,";
        content += new String(Base64.encodeBase64(ByteStreams.toByteArray(imageSource)));
        statement.setVoucher(content);
        }
        return statement;
    }

    //DUMMY METHOD
    public static StatementLine readAccountStatementLine(String idAccount) throws IOException {
        StatementLine statementLine = null;
        statementLine = getDummyStatementLine();
        statementLine.setIdProduct(idAccount);
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        try(InputStream pdfSource = classLoader.getResourceAsStream("/dummy/estado-de-cuenta-ejemplo.pdf")) {
            byte[] content = Base64.encodeBase64(IOUtils.toByteArray(pdfSource));
            statementLine.setContent(content);
        }
        return statementLine;
    }

    /**
     * Listar las cuentas asociadas al documento
     *
     * @param idTransaction   Id de transaccion
     * @param documentCountry Pais del documento
     * @param documentType    Tipo del documento
     * @param documentNumber  Numero del documento
     * @return Listado de cuentas asociadas al documento
     * @throws BackendConnectorException Erorr de conexion con los servicios
     */
    public static List<ClientEnvironment> listClients(String idTransaction, String documentCountry, String documentType, String documentNumber) throws BackendConnectorException {
        List<ClientEnvironment> clients = new ArrayList<>();
        WsReadPersonResponse response = null;

        try {
            WsReadPersonRequest request = new WsReadPersonRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setCountry(StringUtils.defaultString(documentCountry));
            request.setType(StringUtils.defaultString(documentType));
            request.setNumber(StringUtils.defaultString(documentNumber));

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            General port = WS_GENERAL_POOL.borrowObject();

            try {
                response = port.readPerson(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_GENERAL_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv G1 - Consultar informacion de una persona): %s", errorDescription);
                log.error(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                if (response.getClients() != null && response.getClients().getClient() != null) {
                    for (WsClient client : response.getClients().getClient()) {
                        clients.add(new ClientEnvironment(client));
                    }
                }
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return clients;
    }

    /**
     * Update clinet address
     *
     * @param idTransaction   Id de transaccion
     * @throws BackendConnectorException Erorr de conexion con los servicios
     */
    public static void updateClientAddress(String idTransaction, int idClient, Address address, Address mailingAddress) throws BackendConnectorException {
        WsBaseResponse response = null;

        try {
            WsUpdateClientAddress request = new WsUpdateClientAddress();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setIdClient(idClient);
            if(address != null) {
                request.setAddress(address.toJSON().toString());
            }
            if(mailingAddress != null) {
                request.setMaillingAddress(mailingAddress.toJSON().toString());
            }

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            General port = WS_GENERAL_POOL.borrowObject();

            try {
                response = port.updateClientAddress(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_GENERAL_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv G6 - Update client address): %s", errorDescription);
                log.error(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     * Srv G3 - Consultar informacion de una cuenta
     *
     * @param idTransaction  Id de transaccion
     * @param productGroupId Cliente a consultar
     * @return Informacion del cliente en el banco
     * @throws BackendConnectorException Error inesperado del backend
     */
    public static ClientEnvironment readClient(String idTransaction, String productGroupId) throws BackendConnectorException {
        ClientEnvironment client = null;

        WsReadClientResponse response = null;
        try {
            WsReadClientRequest request = new WsReadClientRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setClientID(Integer.parseInt(productGroupId));

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            General port = WS_GENERAL_POOL.borrowObject();

            try {
                response = port.readClient(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_GENERAL_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv G3 - Consultar informacion de una cuenta): %s", errorDescription);
                log.warn(logString);
            } else {
                client = new ClientEnvironment(response);
                client.setProductGroupId(productGroupId);
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return client;
    }

    /**
     * Srv G1 - Consultar informacion de una persona
     *
     * @param idTransaction   Id de transaccion
     * @param documentCountry Pais del documento de la persona
     * @param documentType    Tipo del documento de la persona
     * @param documentNumber  Numero del documento de la persona
     * @param productGroupId  Cliente del cual se sacan los datos de la direccion (SI
     *                        es null retorna los datos de la primer cuenta)
     * @return Los datos asociados a la persona, null si no existe en el banco
     * @throws BackendConnectorException Error inesperado del backend
     */
    public static ClientUser readPerson(String idTransaction, String documentCountry, String documentType, String documentNumber, String productGroupId) throws BackendConnectorException {
        ClientUser client = null;
        WsReadPersonResponse response = null;

        try {
            WsReadPersonRequest request = new WsReadPersonRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setCountry(StringUtils.defaultString(documentCountry));
            request.setType(StringUtils.defaultString(documentType));
            request.setNumber(StringUtils.defaultString(documentNumber));

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            General port = WS_GENERAL_POOL.borrowObject();
            try {
                response = port.readPerson(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_GENERAL_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv G1 - Consultar informacion de una persona): %s", errorDescription);
                log.warn(logString);
            } else {
                client = new ClientUser(response, productGroupId);

                client.setDocumentCountry(documentCountry);
                client.setDocumentType(documentType);
                client.setDocumentNumber(documentNumber);
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return client;
    }

    /**
     * Srv C1 - Consultar detalle de una cuenta
     *
     * @param idTransaction Id de transaccion
     * @param idProduct     Id del producto
     * @param extraInfo     Extra info del producto
     * @return Informacion de la cuenta
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static Account readAccountDetails(String idTransaction, String idProduct, String extraInfo) throws BackendConnectorException {

        Account account = null;
        WsReadAccountResponse response = null;

        try {
            WsReadAccountRequest request = new WsReadAccountRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);
            request.setProductID(ProductUtils.getBackendID(extraInfo));

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            Accounts port = WS_ACCOUNTS_POOL.borrowObject();
            try {
                response = port.readAccount(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_ACCOUNTS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv C1 - Consultar detalle de una cuenta): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                account = generateAccount(idProduct, extraInfo, response);
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return account;
    }

    private static Account generateAccount(String idProduct, String extraInfo, WsReadAccountResponse response) {
        Account account;
        account = new Account();

        account.setIdProduct(idProduct);

        account.setCurrency(ProductUtils.getCurrency(extraInfo));
        account.setOverdraftLine(response.getOverdraftLine());
        account.setPendingBalance(response.getAvailableBalance());
        account.setBalance(response.getBalance());
        account.setConsolidatedAmount(response.getConsolidatedAmount());
        account.setTotalCheckAmount(response.getTotalCheckAmount());
        account.setCountableBalance(response.getCountableBalance());

        Map<String, String> fieldsMap = RubiconCoreConnector.convertDynamicListToMap(response.getFields().getField());

        try {
            account.setExtraInfo(ProductUtils.generateExtraInfo(fieldsMap));
            account.setIdProduct(ProductUtils.generateIdProduct(account.getExtraInfo()));
            account.setProductType(fieldsMap.get(ProductUtils.FIELD_PRODUCT_TYPE));
            account.setOwnerName(fieldsMap.get(ProductUtils.FIELD_OWNER_NAME));
            account.setNumber(fieldsMap.get(ProductUtils.FIELD_PRODUCT_NUMBER));
            account.setClient(ClientHandler.readClient(fieldsMap.get(ProductUtils.FIELD_CLIENT_ID)));
            account.setCurrency(fieldsMap.get(ProductUtils.FIELD_PRODUCT_CURRENCY));
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
        return account;
    }

    public static CreditCard readCreditCardDetails(String idTransaction, String idProduct, String extraInfo) throws BackendConnectorException {

        CreditCard creditCard = null;
        WsReadCreditCardResponse response = null;

        try {
            WsReadCreditCardRequest request = new WsReadCreditCardRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);
            request.setProductID(ProductUtils.getBackendID(extraInfo));

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            CreditCards port = WS_CREDITCARDS_POOL.borrowObject();
            try {
                response = port.readCreditCard(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_CREDITCARDS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv C1 - Consultar detalle de una cuenta): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                creditCard = new CreditCard(response);
                creditCard.setIdProduct(idProduct);
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return creditCard;
    }

    //DUMMY METHOD
    private static StatementLine getDummyStatementLine() {
        StatementLine statementLine = new StatementLine();
        statementLine.setMonth(12);
        statementLine.setYear(2012);
        statementLine.setName("Diciembre 2012");
        statementLine.setIdStatementLine("5");
        statementLine.setFilename("estado-de-cuenta-ejemplo.pdf");
        return (statementLine);
    }

    /**
     * Srv P1 - Consultar detalles de un prestamo
     *
     * @param idTransaction Id de transaccion
     * @param idProduct     Id del producto
     * @param extraInfo     Extra info del producto
     * @return Informacion del prestamo
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static Loan readLoanDetails(String idTransaction, String idProduct, String extraInfo) throws BackendConnectorException {

        Loan loan = null;
        WsReadLoanResponse response = null;

        try {
            WsReadLoanRequest request = new WsReadLoanRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setProductID(ProductUtils.getBackendID(extraInfo));

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            Loans port = WS_LOANS_POOL.borrowObject();
            try {
                response = port.readLoan(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_LOANS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv P1 - Consultar detalles de un prestamo): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                loan = new Loan(response);
                loan.setIdProduct(idProduct);
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return loan;
    }

    /**
     * Srv P2 - Total movimientos de un préstamo
     *
     * @param extraInfo ExtraInfo del producto
     * @return Lista de los movimientos del prestamo
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static int getTotalLoanStatements(String extraInfo) throws BackendConnectorException {
        List<StatementLoan> statements = new ArrayList<>();
        WsListLoanMovementsResponse response = null;
        int totalStatements = 0;

        try {
            WsListLoanMovementsRequest request = new WsListLoanMovementsRequest();
            request.setChannel(CHANNEL);

            request.setProductID(ProductUtils.getBackendID(extraInfo));

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            Loans port = WS_LOANS_POOL.borrowObject();
            try {
                response = port.getTotalLoanMovements(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_LOANS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv P2 - Listar movimientos de un préstamo): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                totalStatements = response.getTotalRecords();
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return totalStatements;
    }

    /**
     * Srv P2 - Listar movimientos de un préstamo
     *
     * @param idTransaction Id de transaccion
     * @param idProduct     Id de producto
     * @param extraInfo     ExtraInfo del producto
     * @param status        Filtrado por estado
     * @param limit         Limite de los resultados
     * @param offset        Offset de los resultados
     * @return Lista de los movimientos del prestamo
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static List<StatementLoan> listLoanStatements(String idTransaction, String idProduct, String extraInfo, String status, int limit, int offset) throws BackendConnectorException {
        List<StatementLoan> statements = new ArrayList<>();
        WsListLoanMovementsResponse response = null;

        try {
            WsListLoanMovementsRequest request = new WsListLoanMovementsRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setProductID(ProductUtils.getBackendID(extraInfo));
            request.setStatus(status);
            request.setOffset(offset);
            request.setLimit(limit);

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            Loans port = WS_LOANS_POOL.borrowObject();
            try {
                response = port.listLoanMovements(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_LOANS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv P2 - Listar movimientos de un préstamo): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                if (response.getMovements() != null && response.getMovements().getMovement() != null) {
                    StatementLoan statement;
                    for (WsLoanMovement movement : response.getMovements().getMovement()) {
                        statement = new StatementLoan(movement);
                        statement.setIdProduct(idProduct);
                        statement.setNote(NotesHandler.readNote(statement.getIdStatement(), idProduct));

                        statements.add(statement);
                    }
                }
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return statements;
    }

    /**
     * Srv P2 - Listar movimientos de un préstamo
     *
     * @param idTransaction Id de transaccion
     * @param idProduct     Id de producto
     * @param extraInfo     ExtraInfo del producto
     * @param limit         Limite de los resultados
     * @param offset        Offset de los resultados
     * @return Lista de los movimientos del prestamo
     * @throws BackendConnectorException Erorr de conexion con el Backend
     */
    public static List<StatementCreditCard> listCreditCardStatements(String idTransaction, String idProduct, String extraInfo, int limit, int offset) throws BackendConnectorException {
        List<StatementCreditCard> statements = new ArrayList<>();
        WsListCreditCardMovementsResponse response = null;

        try {
            WsListCreditCardMovementsRequest request = new WsListCreditCardMovementsRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);

            request.setProductID(ProductUtils.getBackendID(extraInfo));
            request.setOffset(offset);
            request.setLimit(limit);

            String logString = "";

            if (log.isDebugEnabled()) {
                logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            CreditCards port = WS_CREDITCARDS_POOL.borrowObject();
            try {
                response = port.listCreditCardMovements(request);
            } catch (Exception e) {
                throw e;
            } finally {
                WS_CREDITCARDS_POOL.returnObject(port);
            }

            if (log.isDebugEnabled()) {
                logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                log.debug(logString);
            }

            if (response.getReturnCode() != 0) {
                String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                logString = String.format("Error en backend (Srv P2 - Listar movimientos de un crédito): %s", errorDescription);
                log.warn(logString);
                throw new BackendConnectorException(errorDescription, response.getReturnCode());
            } else {
                if (response.getMovements() != null && response.getMovements().getMovement() != null) {
                    StatementCreditCard statement;
                    for (WsCreditCardMovement movement : response.getMovements().getMovement()) {
                        statement = new StatementCreditCard(movement);
                        statement.setIdProduct(idProduct);
                        statement.setNote(NotesHandler.readNote(statement.getIdStatement(), idProduct));

                        statements.add(statement);
                    }
                }
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }

        return statements;
    }


    /**
     * Listar cotizaciones para los widgets
     *
     * @param idTransaction  Id de transaccion
     * @param productGroupId Cliente
     * @return Listado de cotizaciones
     * @throws BackendConnectorException
     */
    public static List<CurrencyExchange> listWidgetsExchangeRates(String idTransaction, String productGroupId) throws BackendConnectorException {
        WsListExchangeRatesResponse response = null;
        try {
            List<CurrencyExchange> exchangeRates = (List<CurrencyExchange>) CacheUtils.get("rubiconCoreConnectorC.listWidgetsExchangeRates", productGroupId);

            if (exchangeRates == null) {
                exchangeRates = new ArrayList<>();

                WsListExchangeRatesRequest request = new WsListExchangeRatesRequest();
                request.setChannel(CHANNEL);
                request.setTransactionID(idTransaction);

                request.setClientID(Integer.parseInt(productGroupId));

                String logString = "";

                if (log.isDebugEnabled()) {
                    logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                    log.debug(logString);
                }

                General port = WS_GENERAL_POOL.borrowObject();
                try {
                    response = port.listExchangeRates(request);
                } catch (Exception e) {
                    throw e;
                } finally {
                    WS_GENERAL_POOL.returnObject(port);
                }

                if (log.isDebugEnabled()) {
                    logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                    log.debug(logString);
                }

                if (response.getReturnCode() != 0) {
                    String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                    logString = String.format("Error en backend (Srv G5 - Listar cotizaciones): %s", errorDescription);
                    log.warn(logString);
                    throw new BackendConnectorException(errorDescription, response.getReturnCode());
                } else {
                    String baseCurrency = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency");
                    if (response.getExchangeRates() != null && response.getExchangeRates().getExchangeRate() != null) {
                        for (WsExchangeRate exchangeRate : response.getExchangeRates().getExchangeRate()) {
                            exchangeRates.add(new CurrencyExchange(exchangeRate, baseCurrency));
                        }
                    }
                    CacheUtils.put("rubiconCoreConnectorC.listWidgetsExchangeRates", exchangeRates, productGroupId);
                }
            }
            return exchangeRates;
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }
    }

    /**
     * Srv G5 - Listar cotizaciones
     *
     * @param idTransaction  Id de transaccion
     * @param productGroupId Cliente
     * @return Listado de cotizaciones
     * @throws BackendConnectorException
     */
    public static List<CurrencyExchange> listExchangeRates(String idTransaction, String productGroupId) throws BackendConnectorException {
        WsListExchangeRatesResponse response = null;

        try {
            List<CurrencyExchange> exchangeRates = (List<CurrencyExchange>) CacheUtils.get("rubiconCoreConnectorC.listExchangeRates", productGroupId);

            if (exchangeRates == null) {
                exchangeRates = new ArrayList<>();

                WsListExchangeRatesRequest request = new WsListExchangeRatesRequest();
                request.setChannel(CHANNEL);
                request.setTransactionID(idTransaction);

                String logString = "";

                if (productGroupId != null) {
                    request.setClientID(Integer.parseInt(productGroupId));
                }

                if (log.isDebugEnabled()) {
                    logString = String.format("Enviar al backend: %s", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                    log.debug(logString);
                }

                General port = WS_GENERAL_POOL.borrowObject();
                try {
                    response = port.listExchangeRates(request);
                } catch (Exception e) {
                    throw e;
                } finally {
                    WS_GENERAL_POOL.returnObject(port);
                }

                if (log.isDebugEnabled()) {
                    logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                    log.debug(logString);
                }

                if (response.getReturnCode() != 0) {
                    String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                    logString = String.format("Error en backend (Srv G5 - Listar cotizaciones): %s", errorDescription);
                    log.warn(logString);
                    throw new BackendConnectorException(errorDescription, response.getReturnCode());
                } else {
                    if (response.getExchangeRates() != null && response.getExchangeRates().getExchangeRate() != null) {

                        String baseCurrency = ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency");
                        for (WsExchangeRate exchangeRate : response.getExchangeRates().getExchangeRate()) {
                            exchangeRates.add(new CurrencyExchange(exchangeRate, baseCurrency));
                        }
                    }
                    CacheUtils.put("rubiconCoreConnectorC.listExchangeRates", exchangeRates, productGroupId);
                }
            }
            return exchangeRates;
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }
    }

    //DUMMY METHOD
    public static AssetsAndLiabilities readConsolidatedAssetsAndLiabilities() {
        AssetsAndLiabilities assetsAndLiabilities = new AssetsAndLiabilities();

        double[] assets = new double[12];
        assets[0] = 100;

        for (int i = 1; i < 12; i++) {
            assets[i] = assets[i - 1] + 200 * i;
        }

        double[] liabilities = new double[12];
        liabilities[0] = 500;
        for (int i = 1; i < 12; i++) {
            liabilities[i] = liabilities[i - 1] + 150 * i;
        }

        assetsAndLiabilities.setAssets(assets);
        assetsAndLiabilities.setLiabilities(liabilities);

        return assetsAndLiabilities;
    }

    //DUMMY METHOD
    public static double[] readAccountAmountEvolution(String number) {
        double[] amountEvolution = new double[12];
        amountEvolution[0] = Double.parseDouble(number);

        for (int i = 1; i < 12; i++) {
            amountEvolution[i] = (amountEvolution[i - 1] + 150 * i) * ((i % 2 == 0) ? -1 : 1);
        }

        return amountEvolution;
    }

    /**
     * Srv G? - Listar productos para la posicion
     *
     * @param idTransaction  Id de transaccion
     * @param clients        Lista de clientes
     * @param typeList       Tipos de producto a listar
     * @return Mapa de productos Productos del cliente que son de algun tipo de
     * producto especificado
     * @throws BackendConnectorException Error en los servicios web del backend
     */
    public static List<PositionProductListByType> listPositionProducts(String idTransaction, List<Client> clients, List<String> typeList) throws BackendConnectorException {
        List<PositionProductListByType> productsByType = new ArrayList<>();
        try {
            WsListPositionProductsRequest request = new WsListPositionProductsRequest();
            request.setChannel(CHANNEL);
            request.setTransactionID(idTransaction);
            com.technisys.omnichannel.rubicon.core.general.ObjectFactory factory = new com.technisys.omnichannel.rubicon.core.general.ObjectFactory();
            WsListPositionProductsRequest.Types types = factory.createWsListPositionProductsRequestTypes();
            for (String type : typeList) {
                types.getType().add(type);
            }
            request.setTypes(types);
            String logString = "";
            for (Client client : clients) {
                request.setClientID(Integer.parseInt(client.getIdClient()));
                if (log.isDebugEnabled()) {
                    logString = String.format("Enviar al backend: %S", ToStringBuilder.reflectionToString(request, new RecursiveToStringStyle()));
                    log.debug(logString);
                }
                General port = WS_GENERAL_POOL.borrowObject();
                WsListPositionProductsResponse response;
                try {
                    response = port.listPositionProducts(request);
                } catch (Exception e) {
                    throw e;
                } finally {
                    WS_GENERAL_POOL.returnObject(port);
                }

                if (log.isDebugEnabled()) {
                    logString = String.format("Respuesta backend: %s", ToStringBuilder.reflectionToString(response, new RecursiveToStringStyle()));
                    log.debug(logString);
                }
                if (response.getReturnCode() != 0) {
                    String errorDescription = response.getReturnCode() + ": " + response.getReturnCodeDescription();
                    logString = String.format("Error en backend (Srv G? - Listar productos para la posicion): %s", errorDescription);
                    log.warn(logString);
                    throw new BackendConnectorException(errorDescription, (int) response.getReturnCode());
                } else {
                    if (response.getProductTypes() != null && response.getProductTypes().getProductType() != null) {
                        Map<String, PositionProductListByType> mapByType = new HashMap<>();
                        for (WsPositionProductTypeWrapper productType : response.getProductTypes().getProductType()) {
                            if (productType.getProducts() != null && productType.getProducts().getProduct() != null) {
                                PositionProductListByType wrapper = mapByType.get(productType.getType());
                                if (wrapper == null) {
                                    wrapper = new PositionProductListByType();
                                    wrapper.setType(productType.getType());
                                    wrapper.setCurrency(PositionProduct.CURRENCY_USD);
                                    mapByType.put(productType.getType(), wrapper);
                                    // Lo agrego a la lista final (el item se va a ir actualizando mientras se itera entre lops tipos de producto)
                                    productsByType.add(wrapper);
                                }
                                // Agrego al total el importe consolidado para este Tipo/Moneda
                                wrapper.addToTotal(productType.getConsolidatedAmount());
                                PositionProductListByCurrency productsByCurrency = new PositionProductListByCurrency();
                                productsByCurrency.setTotal(productType.getBalance());
                                productsByCurrency.setCurrency(productType.getCurrency());
                                for (WsPositionProduct product : productType.getProducts().getProduct()) {
                                    Map<String, String> fieldsMap = RubiconCoreConnector.convertDynamicListToMap(product.getFields().getField());
                                    productsByCurrency.addProduct(new PositionProduct(fieldsMap));
                                }
                                wrapper.addProductsByCurrency(productsByCurrency);
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            throw new BackendConnectorException(e);
        }
        return productsByType;
    }

    //DUMMY METHOD
    public static Statement readStatement(int idStatement, String idAccount) throws IOException {
        Statement statement = new Statement();
        statement.setIdStatement(idStatement);
        statement.setIdAccount(idAccount);
        return statement;
    }
}
