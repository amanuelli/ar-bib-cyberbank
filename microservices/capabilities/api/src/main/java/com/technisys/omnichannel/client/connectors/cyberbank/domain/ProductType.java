/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank.domain;

public class ProductType {
    private String productCode;
    private Integer idProduct;
    private String subProductCode;
    private Integer idSubProduct;

    public ProductType(String productCode, Integer idProduct, String subProductCode, Integer idSubProduct) {
        this.productCode = productCode;
        this.idProduct = idProduct;
        this.subProductCode = subProductCode;
        this.idSubProduct = idSubProduct;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public String getSubProductCode() {
        return subProductCode;
    }

    public void setSubProductCode(String subProductCode) {
        this.subProductCode = subProductCode;
    }

    public Integer getIdSubProduct() {
        return idSubProduct;
    }

    public void setIdSubProduct(Integer idSubProduct) {
        this.idSubProduct = idSubProduct;
    }
}
