/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.recoverpassword;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.credentials.CredentialPluginFactory;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.domain.UserStatus;
import com.technisys.omnichannel.core.domain.ValidationCode;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.validationcodes.ValidationCodesHandler;
import com.technisys.omnichannel.utils.ISafewayUser;
import com.technisys.omnichannel.utils.SafewayUtils;
import java.io.IOException;
import java.util.Map;
import org.apache.commons.lang.StringUtils;

/**
 * Change password if user is unblocked. In case of user blocked, if the validation code was
 * send by backoffice, unlock the user. If the user request validation code, return <code>blocked=true</code>
 */
@ExchangeActivity
@DocumentedActivity("Recover Password Step 3")
public class RecoverPasswordStep3Activity extends Activity {

    public static final String ID = "session.recoverPassword.step3";

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {
        @DocumentedParam(type = String.class, description = "Validation code sent to the user by mail. e.g. &quot;BB75-9816-3A88&quot;") 
        String CODE = "_resetCode";
        @DocumentedParam(type = String.class, description = "New password")
        String NEW_PASSWORD = "_password";
        @DocumentedParam(type = String.class, description = "New password confirmation")
        String PASSWORD_CONFIRMATION = "_passwordConfirmation";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Return true if user is blocked") 
        String BLOCKED = "blocked";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        try {
            String code = com.technisys.omnichannel.client.utils.StringUtils.normalizeInvitationCode(request.getParam(InParams.CODE, String.class));
            ValidationCode validationCode = ValidationCodesHandler.readValidationCode(code);

            // Modifico el password del usuario
            CredentialPlugin credentialPlugin = CredentialPluginFactory.getCredentialPlugin(Credential.PWD_CREDENTIAL);
            credentialPlugin.recover(validationCode.getIdUser(), request.getCredential(Credential.PWD_CREDENTIAL).getValue());

            User user = AccessManagementHandlerFactory.getHandler().getUser(validationCode.getIdUser());

            if (StringUtils.equals(user.getStatus().getIdUserStatus(), UserStatus.USER_STATUS_BLOCKED)) {
                if (StringUtils.equals("BO", validationCode.getExtraInfo())) {
                    AccessManagementHandlerFactory.getHandler().updateUserStatus(user.getIdUser(), UserStatus.USER_STATUS_ACTIVE);
                } else {
                    response.putItem(OutParams.BLOCKED, true);
                }
            }

            ValidationCodesHandler.changeValidationCodeStatus(validationCode.getId(), ValidationCode.STATUS_USED);

            ExchangeTokenHandler.delete(request.getParam(EXCHANGE_TOKEN, String.class));

            response.setReturnCode(ReturnCodes.OK);
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        } catch (DispatchingException e) {
            throw new ActivityException(ReturnCodes.CREDENTIAL_PLUGIN_INSTANTIATION_ERROR, "Error instantiating Password credential validation plugin", e);
        }

        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result;

        RecoverPasswordStep2Activity recoverStep2 = new RecoverPasswordStep2Activity();
        result = recoverStep2.validateCode(request);

        Credential passwordCredential = request.getCredential(Credential.PWD_CREDENTIAL);
        String password = passwordCredential != null ? passwordCredential.getValue() : null;
        String passwordConfirmation = request.getParam(InParams.PASSWORD_CONFIRMATION, String.class);

        if (StringUtils.isBlank(password)) {
            result.put(Credential.PWD_CREDENTIAL, "recoverPassword.step3.password.empty");
        } else {
            if (!StringUtils.equals(password, passwordConfirmation)) {
                result.put(Credential.PWD_CREDENTIAL, "recoverPassword.step3.password.passwordNotTheSame");
                result.put(InParams.PASSWORD_CONFIRMATION, "");
            }

            // Valido el formato de la nueva clave
            try {
                CredentialPlugin credentialPlugin = CredentialPluginFactory.getCredentialPlugin(Credential.PWD_CREDENTIAL);
                String code = com.technisys.omnichannel.client.utils.StringUtils.normalizeInvitationCode(request.getParam(RecoverPasswordStep2Activity.InParams.CODE, String.class));
                ValidationCode validationCode = ValidationCodesHandler.readValidationCode(code);
                User user = AccessManagementHandlerFactory.getHandler().getUser(validationCode.getIdUser());
                
                if (StringUtils.equals(password, user.getUsername())) {
                    result.put(Credential.PWD_CREDENTIAL, "recoverPassword.step3.password.passwordEqualsUserName");
                    result.put(InParams.PASSWORD_CONFIRMATION, "");
                } else {
                    ISafewayUser sc = SafewayUtils.getUserInstance();
                    String commonName = sc.getCommonName(user);
                    // Valido el formato de la nueva contraseña
                    if (!credentialPlugin.validateFormat(commonName, password) || !Administration.getInstance().validatePassword(password)) {
                        result.put(Credential.PWD_CREDENTIAL, "recoverPassword.step3.password.wrongFormat");
                    }
                }
                         
                request.setIdUser(User.USER_ANONYMOUS);

            } catch (DispatchingException | IOException e) {
                throw new ActivityException(ReturnCodes.IO_ERROR, e);
            }

        }

        return result;
    }

}
