package com.technisys.omnichannel.client.activities.administration.advanced;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import java.util.Map;

/**
 *
 * @author Marcelo Bruno
 */

@DocumentedActivity("Modify user's groups preview")
public class ModifyGroupsOfUserPreviewActivity extends Activity{
 
    public static final String ID = "administration.user.detail.groups.modify.preview";

    public interface InParams {
    }

    public interface OutParams {
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }

    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return ModifyGroupsOfUserActivity.validateFields(request);
    }

}
