/*
 *  Copyright 2019 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.connectors.cyberbank;

import com.technisys.omnichannel.client.Constants;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.ISOProduct;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.LoanType;
import com.technisys.omnichannel.client.connectors.cyberbank.domain.ProductType;
import com.technisys.omnichannel.client.connectors.cyberbank.json.JsonArray;
import com.technisys.omnichannel.client.connectors.cyberbank.utils.CoreProductUtils;
import com.technisys.omnichannel.client.domain.Account;
import com.technisys.omnichannel.client.domain.Loan;
import com.technisys.omnichannel.client.domain.StatementLoan;
import com.technisys.omnichannel.client.utils.JsonTemplateUtils;
import com.technisys.omnichannel.client.utils.ProductUtils;
import com.technisys.omnichannel.core.domain.Amount;
import com.technisys.omnichannel.core.domain.Client;
import com.technisys.omnichannel.core.domain.Product;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CyberbankLoanConnector extends CyberbankCoreConnector {
    private static final Logger LOG = LoggerFactory.getLogger(CyberbankLoanConnector.class);

    private static final String TERM_TYPE_MONTH = "MONTHS";
    private static final Integer TERM_CAPITAL_NUMBER = 1;
    private static final String MNL_LOCAL = "1";
    private static final String DATE_BEGIN_LOAN = "20090323000000000-0300";

    /**
     * List loans on Cyberbank core
     *
     * @return The clients loans list
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<List<Loan>> list(String idTransaction, List<Client> clients) throws CyberbankCoreConnectorException {

        CyberbankCoreConnectorResponse<List<Loan>> response = new CyberbankCoreConnectorResponse<>();
        String serviceName = "Srv - massiveSelectLoanFrom_Customer_Operation";

        for (Client client : clients) {
            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("massiveSelectLoanFrom_Customer_Operation", true)
                    .addRequestParameter("customerId", client.getIdClient());

            LOG.info("{}: Request {}", serviceName, URL);

            String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(serviceName, jsonRequest);

            if (callHasError(serviceResponse)) {
                processErrors(serviceName, serviceResponse, response);
            } else {
                if (response.getData() == null) {
                    response.setData(new ArrayList<>());
                }
                response.getData().addAll(processLoanList(serviceResponse));
            }
        }

        return response;

    }

    private static List<Loan> processLoanList(JSONObject serviceResponse) {
        List<Loan> loanList = new ArrayList<>();

        if (serviceResponse.getJSONObject("out.loan_list").has("collection")) {
            JSONArray collection = serviceResponse.getJSONObject("out.loan_list").getJSONArray("collection");

            for (int i = 0; i < collection.length(); i++) {
                JSONObject jsonObject = collection.getJSONObject(i);
                Loan loan = new Loan();

                loan.setTotalAmount((jsonObject.has("totalOperationAmount")) ? Double.parseDouble(jsonObject.getString("totalOperationAmount")) : 0);
                loan.setCurrency("USD");

                loan.setConstitutedDate((jsonObject.has("inputDate")) ? CyberbankCoreConnector.parseDate(jsonObject.getString("inputDate")) : null);

                loan.setNumberOfFees((jsonObject.has("qtyCapitalInstallment")) ? jsonObject.getInt("qtyCapitalInstallment") : 0);
                loan.setPendingBalance((jsonObject.has("dueCapitalBalance")) ? Double.parseDouble(jsonObject.getString("dueCapitalBalance")) : 0);

                String productType = Constants.PRODUCT_PA_KEY;
                String accountNumber = jsonObject.getString("operationId");
                loan.setIdProduct(ProductUtils.generateIdProduct(accountNumber + productType));
                loan.setNumber(accountNumber);

                loan.setPaymentAmount((jsonObject.has("amountInstallment")) ? Double.parseDouble(jsonObject.getString("amountInstallment")) : 0);

                loanList.add(loan);
            }
        }
        return loanList;
    }

    /**
     * Sen Request Loand
     *
     * @return Loan Object
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<Loan> request(Account debitAccount,String productCode, String subProductCode, String idCustomer, Amount amount, Integer amountOfFees) throws CyberbankCoreConnectorException { //NOSONAR
        try {
            CyberbankCoreConnectorResponse<Loan> response = new CyberbankCoreConnectorResponse<>();
            String serviceName = "Srv - insertLoanNormal_CMM";
            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("insertLoanNormal_CMM", true);
            String productId = ProductUtils.getBackendID(debitAccount.getExtraInfo());
            ProductType productType = CyberbankConsolidateConnector.getProductType(productId, CoreProductUtils.getSubProductId(debitAccount.getExtraInfo()));

            cyberbankCoreRequest.
                    addRequestParameter("customer.customerId", idCustomer)
                    .addRequestParameter("account.currency.currencyCodeId", MNL_LOCAL) // In core there is only MLN (Local Currency) in CDP that type of currency does not exist
                    .addRequestParameter("loan.beginDate", DATE_BEGIN_LOAN) // the year in core is 2009
                    .addRequestParameter("loan.capitalTerm", TERM_CAPITAL_NUMBER) // example: 1 monthly fee
                    .addRequestParameter("loan.termType.nemotecnico", TERM_TYPE_MONTH) //Type of monthly fee
                    .addRequestParameter("loan.product.productId", productCode)
                    .addRequestParameter("loan.subproductId", subProductCode)
                    .addRequestParameter("loan.totalOperationAmount", amount.getQuantity())
                    .addRequestParameter("loan.qtyCapitalInstallment", amountOfFees)
                    .addRequestParameter("account.operationId", ProductUtils.getNumber(debitAccount.getExtraInfo()))
                    .addRequestParameter("account.product.productId", productType.getProductCode())
                    .addRequestParameter("account.id",  CoreProductUtils.getPhysicAccountId(debitAccount.getExtraInfo())
           );

            String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(serviceName, jsonRequest);

            if (callHasError(serviceResponse)) {
                processErrors(serviceName, serviceResponse, response);
            } else {
                Loan loan = new Loan();
                loan.setNumber(String.valueOf(serviceResponse.getJSONObject("out.loan").getInt("id")));
                response.setData(loan);
                return response;
            }
            return response;
        }catch (IOException e){
            throw new CyberbankCoreConnectorException(e);
        }


    }


    /**
     * List loans on Cyberbank core
     *
     * @return The clients loans type list
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<List<LoanType>> listLoanType(List<ISOProduct> listIsoProducts) throws CyberbankCoreConnectorException {

        CyberbankCoreConnectorResponse<List<LoanType>> response = new CyberbankCoreConnectorResponse<>();
        List<LoanType> loanTypeList;
        String serviceName = "Srv - massiveSelectSubproductFor_Loan_Standard";

        CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("massiveSelectSubproductFor_Loan_Standard", true)
                .addRequestParameter("in.productList", buildProductList(listIsoProducts));

        String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
        JSONObject serviceResponse = call(serviceName, jsonRequest);

        if (callHasError(serviceResponse)) {
            processErrors(serviceName, serviceResponse, response);
            return response;
        }

        if (serviceResponse.has("out.subproduct_list")) {
            loanTypeList = new ArrayList<>();
            JSONArray jsonArrayLoanType = serviceResponse.getJSONObject("out.subproduct_list").getJSONArray("collection");
            for (Object object : jsonArrayLoanType) {
                JSONObject loanType = (JSONObject) object;
                String loanTypeId = loanType.getString("subproductId");
                String loanTypeDescription = loanType.getString("longDesc");
                loanTypeList.add(new LoanType(loanTypeId, loanTypeDescription));
            }
            response.setData(loanTypeList);
        } else {
            response.setData(new ArrayList<>());
        }
        return response;

    }

    /**
     * Built an ISOProduct list with templaje JSON to send to Cyberbank core
     *
     * @param listIsoProducts
     * @return
     */
    private static JSONArray buildProductList(List<ISOProduct> listIsoProducts) {
        JSONObject jsonProduct;
        JSONArray jsonArrayproducts = new JsonArray();
        for (ISOProduct isoProduct : listIsoProducts) {
            jsonProduct = new JSONObject();
            jsonProduct.put("productId", isoProduct.getId());
            jsonArrayproducts.put(jsonProduct);
        }
        return jsonArrayproducts;
    }

    /**
     * Read an loan detail on Cyberbank core
     *
     * @return The loan detail
     * @throws CyberbankCoreConnectorException
     */
    public static CyberbankCoreConnectorResponse<Loan> read(Product product) throws CyberbankCoreConnectorException {
        try {
            CyberbankCoreConnectorResponse<Loan> response = new CyberbankCoreConnectorResponse<>();
            String serviceName = "Srv - A2";

            String extraInfo = product.getExtraInfo();
            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("singleSelectLoanCMM", true)
                    .addRequestParameter("operationId", ProductUtils.getNumber(extraInfo))
                    .addRequestParameter("branchId", CoreProductUtils.getBranch(extraInfo))
                    .addRequestParameter("productId", ProductUtils.getBackendID(extraInfo))
                    .addRequestParameter("subproductId", CoreProductUtils.getSubProductId(extraInfo));

            LOG.info("{}: Request {}", serviceName, URL);

            String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(serviceName, jsonRequest);

            if (callHasError(serviceResponse)) {
                processErrors(serviceName, serviceResponse, response);
            } else {
                response.setData(processReadLoanResponse(serviceResponse, product));
            }
            return response;
        } catch (CyberbankCoreConnectorException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    private static Loan processReadLoanResponse(JSONObject response, Product product) throws CyberbankCoreConnectorException {
        try {
            if (response.getJSONObject("out.loan") == null) {
                return null;
            }

            JSONObject serviceLoan = response.getJSONObject("out.loan");
            Loan loan = new Loan(product);

            loan.setTotalAmount((serviceLoan.has("totalOperationAmount")) ? Double.parseDouble(serviceLoan.getString("totalOperationAmount")) : 0);
            loan.setCurrency("USD");

            loan.setConstitutedDate((serviceLoan.has("inputDate")) ? CyberbankCoreConnector.parseDate(serviceLoan.getString("inputDate")) : null);

            if (serviceLoan.has("nextInstallmentDate")) {
                String dateString = serviceLoan.getString("nextInstallmentDate");
                try {
                    DateFormat df = new SimpleDateFormat("ddMMyyyy");
                    loan.setNextDueDate(df.parse(dateString));
                } catch (Exception ex) {
                    LOG.debug("Can't parse nextInstallmentDate");
                }
            }

            loan.setNumberOfFees((serviceLoan.has("qtyCapitalInstallment")) ? serviceLoan.getInt("qtyCapitalInstallment") : 0);
            loan.setPendingBalance((serviceLoan.has("dueCapitalBalance")) ? Double.parseDouble(serviceLoan.getString("dueCapitalBalance")) : 0);
            loan.setTotalAmount((serviceLoan.has("totalOperationAmount")) ? Double.parseDouble(serviceLoan.getString("totalOperationAmount")) : 0);
            loan.setCurrency("USD");

            loan.setPaymentAmount((serviceLoan.has("amountInstallment")) ? Double.parseDouble(serviceLoan.getString("amountInstallment")) : 0);

            loan.setInterestRate((serviceLoan.has("interestRateValue")) ? Double.parseDouble(serviceLoan.getString("interestRateValue")) : 0);
            loan.setInterests(response.has("out.total_interest") ? response.getDouble("out.total_interest") : 0);

            int installmentPay = 0;
            if(response.has("out.loan_installment_list") && response.getJSONObject("out.loan_installment_list").has("collection")){
                JSONArray installmentList = response.getJSONObject("out.loan_installment_list").getJSONArray("collection");
                for (int i = 0; i < installmentList.length(); i++) {
                    JSONObject installment = installmentList.getJSONObject(i);
                    JSONObject status = installment.getJSONObject("status");
                    if("COBRADO".equalsIgnoreCase(status.getString("nemotecnico"))){
                        installmentPay ++;
                    }
                }
            }
            loan.setNumberOfPaidFees(installmentPay);
            return loan;

        } catch (JSONException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    public static CyberbankCoreConnectorResponse<List<StatementLoan>> listInstallments(Product product) throws CyberbankCoreConnectorException {
        try {
            CyberbankCoreConnectorResponse<List<StatementLoan>> response = new CyberbankCoreConnectorResponse<>();
            String serviceName = "Srv - SRV.LO.06 - massiveSelectLoan_Installment";
            CyberbankCoreRequest cyberbankCoreRequest = new CyberbankCoreRequest("massiveSelectLoan_Installment", true)
                    .addRequestParameter("idLoan", CoreProductUtils.getPhysicAccountId(product.getExtraInfo()));

            String jsonRequest = JsonTemplateUtils.applyTemplateToJson(cyberbankCoreRequest.getJSON(), REQ_TEMPLATE_PATH + cyberbankCoreRequest.getTransactionId());
            JSONObject serviceResponse = call(serviceName, jsonRequest);

            if (callHasError(serviceResponse)) {
                processErrors(serviceName, serviceResponse, response);
            } else {
                response.setData(processInstallments(product, serviceResponse));
            }

            return response;
        } catch (CyberbankCoreConnectorException e) {
            throw new CyberbankCoreConnectorException(e);
        }
    }

    private static List<StatementLoan> processInstallments(Product product, JSONObject serviceResponse) {
        ArrayList<StatementLoan> installments = new ArrayList<>();

        if (serviceResponse.has("out.loan_installment_list") &&
                serviceResponse.getJSONObject("out.loan_installment_list").has("collection")) {
            JSONArray serviceInstallments = serviceResponse.getJSONObject("out.loan_installment_list").getJSONArray("collection");

            for (int i = 0; i < serviceInstallments.length(); i++) {
                JSONObject serviceInstallment = serviceInstallments.getJSONObject(i);

                StatementLoan statementLoan = new StatementLoan();
                statementLoan.setIdProduct(product.getIdProduct());
                statementLoan.setIdStatement(serviceInstallment.getInt("id"));
                statementLoan.setInterestsAmount(serviceInstallment.getDouble("installmentInterestDue"));
                statementLoan.setImportAmount(serviceInstallment.getDouble("liTax2"));
                statementLoan.setDueDate(CyberbankCoreConnector.parseDate(serviceInstallment.getString("paymentDueDate")));

                boolean isPaymentDataNull = serviceInstallment.isNull("paymentDate");
                statementLoan.setPaidDate(CyberbankCoreConnector.parseDate(isPaymentDataNull ? null : serviceInstallment.getString("paymentDate")));
                statementLoan.setFeeNumber(serviceInstallment.getInt("liInstallmentNumber"));
                statementLoan.setNumberOfFees(serviceInstallments.length());

                int statusId = serviceInstallment.getJSONObject("status").getInt("id");
                String status;
                switch (statusId) {
                    case 7:
                        status = "pending";
                        break;
                    case 20:
                        status = "paid";
                        break;
                    default:
                        status = "expired";
                }

                statementLoan.setStatus(status);


                statementLoan.setCapitalAmount(serviceInstallment.getDouble("installmentCapitalDue"));

                installments.add(statementLoan);
            }

        }

        return installments;
    }

}
