/*
 *  Copyright (c) 2020 Technisys.
 *
 *   This software component is the intellectual property of Technisys S.A.
 *   You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *   https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.administration.signatures;

import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.configuration.Configuration;
import com.technisys.omnichannel.core.configuration.ConfigurationFactory;
import com.technisys.omnichannel.core.exceptions.ActivityException;

import java.io.IOException;
import java.util.Map;

/**
 * Creates a new signatures scheme (PREVIEW)
 */

@DocumentedActivity("Creates a new signatures scheme (PREVIEW)")
public class CreateSignaturesPreviewActivity extends Activity {

    public static final String ID = "administration.signatures.create.preview";

    public interface InParams {
        @DocumentedParam(type = String[].class, description = "Selected frequencies for transactions with amount")
        String CAP_FREQUENCIES = "capFrequencies";

        @DocumentedParam(type = String[].class, description = "List of channels")
        String CHANNELS = "channels";

        @DocumentedParam(type = Double[].class, description = "Maximum amounts")
        String MAX_AMOUNTS = "maxAmounts";

        // Required by validate (CreateSignaturesActivity.validateFields)
        @DocumentedParam(type = String.class, description = "Signature type")
        String SIGNATURE_TYPE = "signatureType";

        // Required by validate (CreateSignaturesActivity.validateFields)
        @DocumentedParam(type = Map.class, description = "Number of signers for each signature level")
        String SIGNATURE_LEVELS_COUNTS = "signatureLevelsCounts";
    }

    public interface OutParams {
        @DocumentedParam(type = String[].class, description = "Selected frequencies for transactions with amount")
        String CAP_FREQUENCIES = "capFrequencies";

        @DocumentedParam(type = String[].class, description = "List of channels")
        String CHANNELS = "channels";

        @DocumentedParam(type = String.class, description = "Master currency")
        String MASTER_CURRENCY = "masterCurrency";

        @DocumentedParam(type = Double[].class, description = "Maximum amounts")
        String MAX_AMOUNTS = "maxAmounts";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        Response response = new Response(request);

        Double[] maxAmounts = request.getParam(InParams.MAX_AMOUNTS, Double[].class);
        String[] capFrequencies = request.getParam(InParams.CAP_FREQUENCIES, String[].class);
        String[] channels = request.getParam(InParams.CHANNELS, String[].class);

        response.putItem(OutParams.MAX_AMOUNTS, maxAmounts);
        response.putItem(OutParams.CAP_FREQUENCIES, capFrequencies);
        response.putItem(OutParams.CHANNELS, channels);

        try {
            response.putItem(OutParams.MASTER_CURRENCY, ConfigurationFactory.getInstance().getString(Configuration.PLATFORM, "core.masterCurrency"));
        } catch (IOException ex) {
            throw new ActivityException(ReturnCodes.IO_ERROR, ex);
        }

        response.putItem(ID, request);
        response.setReturnCode(ReturnCodes.OK);
        return response;
    }


    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        return CreateSignaturesActivity.validateFields(request);
    }
}