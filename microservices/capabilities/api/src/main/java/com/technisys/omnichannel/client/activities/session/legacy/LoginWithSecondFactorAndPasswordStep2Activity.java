/*
 *  Copyright 2015 Technisys.
 *
 *  This software component is the intellectual property of Technisys S.A.
 *  You are not allowed to use, change or distribute it without express written consent from its author.
 *
 *  https://www.technisys.com
 */
package com.technisys.omnichannel.client.activities.session.legacy;

import com.technisys.omnichannel.annotations.ExchangeActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedActivity;
import com.technisys.omnichannel.annotations.docs.DocumentedParam;
import com.technisys.omnichannel.client.ReturnCodes;
import com.technisys.omnichannel.client.utils.CalendarRestrictionsUtils;
import com.technisys.omnichannel.core.Request;
import com.technisys.omnichannel.core.Response;
import com.technisys.omnichannel.core.accessmanagement.AccessManagementHandlerFactory;
import com.technisys.omnichannel.core.activities.Activity;
import com.technisys.omnichannel.core.administration.Administration;
import com.technisys.omnichannel.core.credentials.Credential;
import com.technisys.omnichannel.core.credentials.CredentialPlugin;
import com.technisys.omnichannel.core.credentials.CredentialPluginFactory;
import com.technisys.omnichannel.core.domain.Environment;
import com.technisys.omnichannel.core.domain.User;
import com.technisys.omnichannel.core.exceptions.ActivityException;
import com.technisys.omnichannel.core.exceptions.DispatchingException;
import com.technisys.omnichannel.core.exchangetoken.ExchangeToken;
import com.technisys.omnichannel.core.exchangetoken.ExchangeTokenHandler;
import com.technisys.omnichannel.core.preprocessors.ipauthorization.IPAuthorizationHandler;
import com.technisys.omnichannel.core.session.SessionUtils;
import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.*;

/**
 * Second login step, use this to continue the login process. You must continue
 * with other steps until you have an <code>_accessToken</code>.
 */
@ExchangeActivity
@DocumentedActivity("Second login step")
public class LoginWithSecondFactorAndPasswordStep2Activity extends Activity {

    public static final String ID = "session.login.legacy.step2";

    private final String NEXT_ACTIVITY_IN_FLOW = SelectEnvironmentActivity.ID;

    // It can be included on inParams since it came as header and loaded automatically as param.
    private static final String EXCHANGE_TOKEN = "_exchangeToken";

    public interface InParams {

        // Used by API, not this activity
        @DocumentedParam(type = String.class, description = "Password value, e.g. &quot;Password1.&quot;")
        String PASSWORD = "_password";
    }

    public interface OutParams {
        @DocumentedParam(type = String.class, description = "Token required to call the next step on this process")
        String EXCHANGE_TOKEN = "_exchangeToken";
        
        @DocumentedParam(type = String.class, description = "List of available environments for the user to select from")
        String ENVIRONMENTS = "environments";
        
        @DocumentedParam(type = String.class, description = "Indicates if the user must select one environment on the next step")
        String SELECT_ENVIRONMENT = "selectEnvironment";

        @DocumentedParam(type = String.class, description = "Default language, selected by the user, e.g. &quot;en&quot;")
        String LANGUAGE = "lang";

        @DocumentedParam(type = String.class, description = "First name of the user, e.g. &quot;Brittny&quot;")
        String USER_FIRST_NAME = "_userFirstName";

        @DocumentedParam(type = String.class, description = "Full name of the user, e.g. &quot;Brittny Beall&quot;")
        String USER_FULL_NAME = "_userFullName";

        @DocumentedParam(type = Integer.class, description = "Default user environment")
        String DEFAULT_ENVIRONMENT = "defEnvironment";

        @DocumentedParam(type = Integer.class, description = "True if user environment is enabled, false if not")
        String DEFAULT_ENVIRONMENT_ENABLED = "defEnvironmentEnabled";
    }

    @Override
    public Response execute(Request request) throws ActivityException {
        try {
            try {
                CredentialPlugin cp = CredentialPluginFactory.getCredentialPlugin(Credential.CAPTCHA_CREDENTIAL);
                cp.validate(request, request.getCredential(Credential.CAPTCHA_CREDENTIAL));

                cp = CredentialPluginFactory.getCredentialPlugin(Credential.PWD_CREDENTIAL);
                cp.validate(request, request.getCredential(Credential.PWD_CREDENTIAL));
            } catch (DispatchingException e) {
                throw new ActivityException(e.getReturnCode());
            }

            // Controlo que este en algun ambiente activo
            List<Environment> environmentList = Administration.getInstance().listEnvironmentsWhereUserIsActive(request.getIdUser(), request.getChannel());

            boolean calendarIsValid;
            boolean ipsAreValid;
            for (Environment env:environmentList){
                calendarIsValid = CalendarRestrictionsUtils.validateAccessByEnvironmentAndUser(env.getIdEnvironment(),request.getIdUser(),request.getValueDate());
                ipsAreValid = IPAuthorizationHandler.getInstance().checkUserEnvironmentAccess(env.getIdEnvironment(), request.getIdUser(), request.getClientIP());
                env.setAllowToAccess(calendarIsValid && ipsAreValid);
            }

            Map<Integer, Map<String, String>> environments = SessionUtils.assembleEnviromentMap(environmentList);

            if (environments.isEmpty()) {
                throw new ActivityException(ReturnCodes.NO_ACTIVE_ENVIRONMENTS, "Invalid active environment (environment: " + request.getIdEnvironment() + ")");
            }

            String exchangeToken = request.getParam(EXCHANGE_TOKEN, String.class);
            ExchangeToken token = ExchangeTokenHandler.read(exchangeToken);
            List<String> activities = new ArrayList<>(Arrays.asList(StringUtils.split(token.getActivities(), ',')));
            if (!activities.contains(NEXT_ACTIVITY_IN_FLOW)) {
                activities.add(NEXT_ACTIVITY_IN_FLOW);
                activities.add(RegisterUserDeviceActivity.ID);
            }

            ExchangeTokenHandler.updateActivities(exchangeToken, activities.toArray(new String[0]));

            User user = AccessManagementHandlerFactory.getHandler().getUser(request.getIdUser());

            boolean shouldSelectEnv = shouldSelectEnvironment(user, request, environmentList);
            Response response = new Response(request);
            response.setReturnCode(ReturnCodes.OK);
            response.putItem(OutParams.EXCHANGE_TOKEN, exchangeToken);
            response.putItem(OutParams.ENVIRONMENTS, environments);
            response.putItem(OutParams.SELECT_ENVIRONMENT, shouldSelectEnv);
            response.putItem(OutParams.LANGUAGE, user.getLang());
            response.putItem(OutParams.USER_FIRST_NAME, user.getFirstName());
            response.putItem(OutParams.USER_FULL_NAME, user.getFullName());
            response.putItem(OutParams.DEFAULT_ENVIRONMENT, shouldSelectEnv ? -1 : user.getIdDefaultEnvironment());
            response.putItem(OutParams.DEFAULT_ENVIRONMENT_ENABLED, shouldSelectEnv && user.getIdDefaultEnvironment() == -1);
            return response;

        } catch (IOException e) {
            throw new ActivityException(ReturnCodes.IO_ERROR, e);
        }
    }

    private Boolean shouldSelectEnvironment(User user, Request request, List<Environment> environmentList) throws IOException {
        // Muestro la página de elegir ambiente si tiene mas de un ambiente activo Y: 1- No tiene un ambiente por defecto, 2- Tiene un ambiente por defecto pero se encuentra deshabilitado en el
        boolean activeInDefaultEnvironment = false;
        int defaultEnvironment = user.getIdDefaultEnvironment();
        for (Environment e : environmentList) {
            if (user.getIdDefaultEnvironment() != -1 && e.getIdEnvironment() == user.getIdDefaultEnvironment()){
                activeInDefaultEnvironment = true;
            }
        }
        return (environmentList.size() > 1) &&
                (!activeInDefaultEnvironment || !IPAuthorizationHandler.getInstance().checkUserEnvironmentAccess(defaultEnvironment, user.getIdUser(), request.getClientIP()));
    }
    
    @Override
    public Map<String, String> validate(Request request) throws ActivityException {
        Map<String, String> result = new HashMap<>();
        if(request.getCredential(Credential.PWD_CREDENTIAL) == null || StringUtils.isBlank(request.getCredential(Credential.PWD_CREDENTIAL).getValue())){
            result.put(InParams.PASSWORD, "login.step1.passwordEmpty");
        }

        return result;
    }

}
