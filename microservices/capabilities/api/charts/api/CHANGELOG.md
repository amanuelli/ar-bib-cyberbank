# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]
### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)

### Added
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)

### Changed
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-647](https://technisys.atlassian.net/browse/TECENG-647). 

### Fixed
- Declare REDIS_HOST as variable on ConfigMap [TECENG-563](https://technisys.atlassian.net/browse/TECENG-563)
