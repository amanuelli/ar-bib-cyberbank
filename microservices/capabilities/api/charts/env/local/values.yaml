# Default values for api.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

name: api

labels:
  app: api
  lang: java
  platform: digital

selectorLabels:
  app: api

deployment:

  image:
    repository: tecdigitalacr.azurecr.io/digital/api
    version: #{MS_VERSION}#
    pullPolicy: Always

  replicaCount: 1

  port: 8080

  envFrom:
    - configMapRef:
        name: api-config
    - configMapRef:
        name: api-apm-config
    - secretRef:
        name: api-secrets

  volumeMounts:
    - mountPath: /usr/local/tomcat/shared/logback.xml
      name: logback-config
      subPath: logback.xml


  imagePullSecrets:
    - name: regcred

  serviceAccountName: tecdigital-sa

  volumes:
    - name: logback-config
      configMap:
        name: api-logback-config
        items:
          - key: logback
            path: logback.xml

  resources: null
    # We usually recommend not to specify default resources and to leave this as a conscious
    # choice for the user. This also increases chances charts run on environments with little
    # resources, such as Minikube. If you do want to specify resources, uncomment the following
    # lines, adjust them as necessary, and remove the curly braces after 'resources:'.
    # limits:
    #   cpu: 100m
    #   memory: 128Mi
    # requests:
    #   cpu: 100m
    #   memory: 128Mi


service:
  type: ClusterIP
  port: 80
  targetPort: 8080

configPodReset:
  - configmap.yaml
  - logbackconfig.yaml
  - secrets.yaml
  - apiapmconfig.yaml

configMap:
  name: api-config
  dbserver:
    url: jdbc:sqlserver://host.docker.internal:1433;databaseName=tec_digital
    driver: com.microsoft.sqlserver.jdbc.SQLServerDriver
  database_dbms: "mssql"
  database_poolPingQuery: "SELECT GETDATE()"
  database_poolMaximumCheckoutTime: "30000"
  database_bpm_maxActiveConnections: "20"
  database_maxActiveConnections: "200"
  database_maxIdleConnections: "20"
  database_bpm_poolMaximumCheckoutTime: "20000"
  database_bpm_maxIdleConnections: "5"
  forms_mapper_cache_flushInterval: "0"
  configuration_mapper_cache_flushInterval: "0"
  core_configuration_componentFQN: "com.technisys.omnichannel.core.configuration.ms.ConfigurationMS"
  core_scheduler_componentFQN: "com.technisys.omnichannel.core.scheduler.ms.SchedulerHandlerMS"
  core_transactions_componentFQN: "com.technisys.omnichannel.core.transactions.ms.TransactionHandlerMS"
  core_transactiontemplates_componentFQN: "com.technisys.omnichannel.core.transactiontemplates.ms.TransactionTemplatesHandlerMS"
  AUDIT_URL: "http://audit.digital-local"
  AUTH_HTTPCLIENT_MAXTOTAL: "20"
  AUTH_HTTPCLIENT_DEFAULTMAXPERROUTE: "20"
  AUDIT_ACTIVE_SIGNED: "false"
  AUDIT_BROKER: "KafkaMQ"
  AUDIT_SPRING_KAFKAMQ_ADDRESS: "tec-confluent-oss-cp-kafka-headless"
  AUDIT_SPRING_KAFKAMQ_CLIENTID: "digital"
  AUDIT_SPRING_KAFKAMQ_PORT: "9092"
  AUDIT_SPRING_KAFKAMQ_SCHEMA_REGISTRY_URL: "http://tec-confluent-oss-cp-schema-registry:8081"
  AUDIT_SPRING_KAFKAMQ_TOPICNAME: queue.audit.logs
  AUDIT_MAXIMUM_NUMBER_THREAD: "8"
  AUDIT_MINIMUM_OBJECT: "4"
  AUDIT_MAXIMUM_OBJECTS: "10"
  AUDIT_VALIDATION_INTERVAL: "5"
  JAVA_OPTS: ""
  KAFKA_BROKER_URL: "tec-confluent-oss-cp-kafka-headless:9092"
  SCHEMA_REGISTRY_URL: "http://tec-confluent-oss-cp-schema-registry:8081"

apiApmConfig:
  name: api-apm-config
  ELASTIC_APM_SERVER_URLS: "http://apm-server.monitor-services:8200"
  ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
  ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
  ELASTIC_APM_APPLICATION_PACKAGES: "com.technisys"
  ELASTIC_APM_CENTRAL_CONFIG: "true"
  ELASTIC_APM_SERVICE_NAME: "api"
  ELASTIC_APM_LOG_LEVEL: "INFO"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "false"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: "com.technisys.omnichannel.api.resource.v1.*, com.technisys.omnichannel.activities.*, com.technisys.omnichannel.client.activities.*, com.technisys.omnichannel.Dispatcher"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: "(?-i)java.*, (?-i)javax.*, (?-i)sun.*, (?-i)com.sun.*,
                                                          (?-i)jdk.*, (?-i)org.apache.tomcat.*,
                                                          (?-i)org.apache.catalina.*, (?-i)org.apache.coyote.*,
                                                          (?-i)org.eclipse.jetty.*, (?-i)org.springframework.*"
  ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc, redis, jax-rs, jax-ws"
  ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME: "true"

logbackConfig:
  name: "api-logback-config"
  logback: |-
    <?xml version="1.0" encoding="UTF-8"?>
    <configuration>
        <appender name="CONSOLE" class="ch.qos.logback.core.ConsoleAppender">
            <encoder class="co.elastic.logging.logback.EcsEncoder"/>
        </appender>
        <root level="INFO">
            <appender-ref ref="CONSOLE" />
        </root>
    </configuration>

secrets:
  name: "api-secrets"
  sesUsageKey: "notnil"
  dbserver:
    password: "password"
    username: "sa"
  rabbitmq:
    username: "#{rabbitmq.username}#"
    password: "#{rabbitmq.password}#"
  elastic:
    apmtoken: "#{elastic.apmtoken}#"
  redisConfig: "singleServerConfig:\n  address: \"redis://redis.digital-local:6379\""