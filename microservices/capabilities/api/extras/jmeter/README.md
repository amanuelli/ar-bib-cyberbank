# Stress test

You can use the `api.jmx` JMeter script to run the stress test.

It takes the user list from another file: `users.csv`. Your must create it after running the "Massive user creation" from the "Misc" application with the following structure:

```
<username>,1111,password
...
<username>,1111,password
```

The usernames list can be obtained with this database script:

```
SELECT username FROM users
```
