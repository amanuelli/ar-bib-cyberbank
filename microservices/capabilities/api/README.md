# Digital API

This repository contains the API code. 

### Database Engine
In file **values.yaml** on env folders:
deployment.configMap.dbserver.engine: SQLServer | Oracle
Case SQLServer ex:
```
  dbserver:
    engine: SQLServer
    name: tec-digital.database.windows.net
    digitalDBName: tec-digital-test-aks
    driver: com.microsoft.sqlserver.jdbc.SQLServerDriver
```
Case Oracle ex:
```
  dbserver:
    engine: Oracle
    name: 10.12.22.4
    sid: DB12CR2
    driver: oracle.jdbc.OracleDriver
```


## Configuration

Env                                     | Description                                       
----------------------------------------|---------------------------------------------------
AUTH_HTTPCLIENT_MAXTOTAL                | The maximum number of http communications that Authserver can have
AUTH_HTTPCLIENT_DEFAULTMAXPERROUTE      | The maximum number of http communications that authserver can have with the same route
database_url                            | The URL of the API database
database_driver                         | The driver to use the database, EX: com.microsoft.sqlserver.jdbc.SQLServerDriver
database_poolPingQuery                  | Is the SQL query executed over the connections returned by the pool of connections, EX: SELECT GETDATE()
database_maxActiveConnections           | The maximun number of connection of API DB.
database_dbms                           | Type of database system, Ex: mssql.
database_maxIdleConnections             | The maximum number of iddle communications that API databse can have
database_poolMaximumCheckoutTime        | Amount of time the caller will wait before getting a connection timeout for API database
database_bpm_maxActiveConnections       | The maximun number of connection of core BPM DB.
database_bpm_poolMaximumCheckoutTime    | Amount of time in core BPM DB, the caller will wait before getting a connection timeout
database_bpm_maxIdleConnections         | The maximun number of idle connection of core BPM DB.
I18N_URL                                | URL of microservice I18N, EX: http://i18n.digital-demo/
SES_URL                                 | URL of microservice SES, EX: http://ses.digital-demo/
AUDIT_URL                               | URL of microservice Audit, EX: http://audit.digital-demo/
AUDIT_ACTIVE_SIGNED:                    | Sign each audit message before to sent it to the Broker Ex: false, true
AUDIT_BROKER:                           | Audit broker implementation, Ex: RabbitMQ, KafkaMQ
AUDIT_MAXIMUM_NUMBER_THREAD:            | Minimum number of thread in the pool, default value = 8
AUDIT_MINIMUM_OBJECT:                   | Minimum number of special ExportingProcess instances residing in the pool, default value = 4
AUDIT_MAXIMUM_OBJECTS:                  | Maximum number of special ExportingProcess instances residing in the pool, default value = 10
AUDIT_VALIDATION_INTERVAL:              | Time in seconds for periodical checking of minObjects / maxObjects conditions, default value = 5
AUTH_VALIDATION_SERVICE_URL             | URL of the validation of AUTHSERVER, EX: http://api.digital-demo/api/v1/execute/oauth.password.validate
AUTH_URL                                | URL of microservice AUTH, EX: http://authserver.digital-demo/
STAFFMANAGEMENT_URL                     | URL of microservice STAFFMANAGEMENT, EX: http://staff-management.digital-demo/
BICLIENT_URL                            | URL of microservice BICLIENT, EX: http://bi-client.digital-demo/
CUSTOMERINSIGHTS_URL                    | URL of microservice CUSTOMERINSIGHTS, EX: http://customerinsights.digital-demo/
forms_mapper_cache_flushInterval        | The time of the cache of the forms in the DB.
configuration_mapper_cache_flushInterval| The time of the cache of the configurations in the DB.
LIMITS_URL                              | URL of microservice LIMITS
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_OPTS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"

### Kafka Configuration
Env                                         | Description
--------------------------------------------|---------------------------------------------------
AUDIT_BROKER                                | Audit broker implementation Ex: KafkaMQ
AUDIT_SPRING_KAFKAMQ_ADDRESS                | Kafka broker address. Ex: "http://localhost", "http://host.docker.internal", "tec-confluent-oss-cp-kafka-headless"
AUDIT_SPRING_KAFKAMQ_CLIENTID               | Kafka message id. Ex: "digital"
AUDIT_SPRING_KAFKAMQ_PORT                   | Kafka broker port. Ex: "9092"
AUDIT_SPRING_KAFKAMQ_SCHEMA_REGISTRY_URL    | Kafka Schema Registry URL. Ex: "http://localhost:8081", http://tec-confluent-oss-cp-schema-registry:8081"
AUDIT_SPRING_KAFKAMQ_TOPICNAME              | Kafka Topic Name to store the messages. Ex: "queue.audit.logs"

### Rabbit Configuration
Env                                         | Description
--------------------------------------------|---------------------------------------------------
AUDIT_BROKER                                | Audit broker implementation Ex: RabbitMQ
AUDIT_SPRING_RABBITMQ_ADDRESSES:            | Rabbit broker address Ex: localhost, 0.0.0.0, tec-digital-rabbit.local.svc.cluster.local
AUDIT_SPRING_RABBITMQ_PORT:                 | Rabbit broker port Ex: 5672
AUDIT_SPRING_RABBITMQ_QUEUE:                | Rabbit Queue name Ex: audit.queue, my_queue
AUDIT_SPRING_RABBITMQ_EXCHANGE:             | Rabbit exchange Ex: audit.exchange, my_exchange
AUDIT_SPRING_RABBITMQ_ROUTING:              | Rabbit Routing Key Ex: audit.rountingkey, my_routing
AUDIT_SPRING_RABBITMQ_VIRTUALHOST:          | Rabbit Virtualhost Ex: /, root

### Redis Configuration
Since 1.1.0 chart version, redis configuration must be seted as a secret value, because it may contain sensitive information

[Documentation](https://github.com/redisson/redisson/wiki/2.-Configuration#24-cluster-mode)

Example for singleServerConfig
```
  secrets:
    name: api-secrets
    redisConfig: "singleServerConfig:\n  address: \"redis://redis.digital-local:6379\""
```

Example for clusterServersConfig
```
  secrets:
    name: api-secrets
    redisConfig: "clusterServersConfig:\n  nodeAddresses:\n  - \"redis://staging-redis-cluster.digital-local:6379\"\n  password: \"lI2JGJBdaC\""
    
```