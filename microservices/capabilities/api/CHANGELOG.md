# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [3.2.6] - 2021-10-21
### Changed
- Redis config moved as a secret value [TECDIGSK-663](https://technisys.atlassian.net/browse/TECDIGSK-663)
- Use api 3.2.50 [TECDIGSK-644](https://technisys.atlassian.net/browse/TECDIGSK-644)

## [3.2.5] - 2021-09-27
- Fix StarterKit settings to switch Platform to Kafka [TECDIGSK-504](https://technisys.atlassian.net/browse/TECDIGSK-504)
- Switch Backoffice to use RabbitMQ [TECDIGSK-517](https://technisys.atlassian.net/browse/TECDIGSK-517)
- Use api 3.2.32 [TECDIGSK-482](https://technisys.atlassian.net/browse/TECDIGSK-482)
- Switch Backoffice to use KafkaMQ and use backoffice 3.2.37 [TECDIGSK-537](https://technisys.atlassian.net/browse/TECDIGSK-537)
- Update spring-beans dependency and use backoffice 3.2.40 [TECDIGSK-612](https://technisys.atlassian.net/browse/TECDIGSK-612)

## [3.2.4] - 2021-08-05
- Sonar bugs and vulnerabilities fixed [TECDIGSK-434](https://technisys.atlassian.net/browse/TECDIGSK-434) 

## [3.2.3] - 2021-06-10
- Update fastrack-verion to 3.2.9 and activities that call readSession method have been modified to send headers [TECHBANK20-450](https://technisys.atlassian.net/browse/TECHBANK20-450)
- Update fastrack-version to 3.2.5 [TECDIGSK-413](https://technisys.atlassian.net/browse/TECDIGSK-413) 

## [3.2.2] - 2021-04-27
- Update fastrack-version to 3.2.2 [TECDIGSK-383](https://technisys.atlassian.net/browse/TECDIGSK-383)
- build-info file added [TECDIGSK-375](https://technisys.atlassian.net/browse/TECDIGSK-375)
- profileJAR added for release jar only when needed [TECDIGSK-373](https://technisys.atlassian.net/browse/TECDIGSK-373)

## [3.2.1] - 2021-04-12
### Added
- Copied starterkit/api from tec-omnichannel repository [TECDIGSK-328](https://technisys.atlassian.net/browse/TECDIGSK-328)

