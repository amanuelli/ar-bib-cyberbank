# Limits Microservice 

[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Platform/MS%20-%20Limits/Master%20-%20Limits?repoName=technisys%2Ftec-digital-limits&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=371&repoName=technisys%2Ftec-digital-limits&branchName=master)

# Table of Contents

- [Project Overview](#markdown-header-project-overview)
  - [Microservice](#markdown-header-microservice)
  - [Client (SDK)](#markdown-header-client-sdk)
- [Getting Started](#markdown-header-getting-started)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Installation](#markdown-header-installation)
  - [Configuration](#markdown-header-configuration)
    - [DB Configuration](#markdown-header-db-configuration)
    - [Cache Configuration](#markdown-header-cache-configuration)
    - [Limits Configuration](#markdown-header-limits-configuration)
    - [Tracing Configuration](#markdown-header-tracing-configuration)
  - [Client usage](#markdown-header-client-usage)
    - [Client Configuration](#markdown-header-client-configuration)
- [Documentation](#markdown-header-documentation)
- [DB Seed](#markdown-header-db-seed)
    - [Performance Tests Configuration](#markdown-header-performance_tests)
    - [Demo fresh install](#markdown-header-demo_fresh_install)
    

# Project Overview

This Microservice manages transaction limits by environment, user, product and signature scheme.
It also includes the management of signature schemes.

## Microservice 

### Technology stack

* Maven
* Java 11
* Spring boot 2
* Flyway 6
* Swagger 2
* [MyBatis](https://github.com/mybatis/spring-boot-starter)
* [Redis](https://github.com/spring-projects/spring-data-redis)
* Supported Databases: SQL Server & Oracle


# Getting Started


## Prerequisites

- Java 11
- Maven 3.x
- Docker (Optional)
- A database on one of the supported DB engines.


## Installation
```
docker build -t my-limits-image:latest .
```
Then you can run it (for example on port 9000) using the following command:
```
docker run -d --name limits -p 9000:8080 -e 'spring.datasource.password=yourpassword' -t my-limits-image:latest 
```
By naming the container, we can execute actions using it, for example see logs:
```
docker logs -f limits
```

## Configuration
Default configuration values are defined in application.yml. 

### DB Configuration

Environment variable | Description
 --- | ---
SPRING_DATASOURCE_URL | jdbc to database
SPRING_DATASOURCE_DRIVER | jdbc driver
SPRING_DATASOURCE_USERNAME | Database username
SPRING_DATASOURCE_PASSWORD | Database password
SPRING_DATASOURCE_HIKARI_MINIMUMIDLE | Minimum number of idle connections maintained in a connection pool
SPRING_DATASOURCE_HIKARI_MAXIMUMPOOLSIZE | Maximum connection pool size
SPRING_DATASOURCE_HIKARI_CONNECTIONTIMEOUT | Maximum milliseconds that a client will wait for a connection
SPRING_FLYWAY_LOCATIONS | To choose migrations according to the db engine. Options: `classpath:db/migration/mssql` or `classpath:db/migration/oracle`


### Cache Configuration
To solve caching, the abstractions provided by spring were used. The implementation used was Spring data redis.


Environment variable | Description
 --- | ---
SPRING_CACHE_TYPE | Cache implementation. Options: `none` or `redis`
SPRING_CACHE_REDIS_USE-KEY-PREFIX | Toggle to enable key prefix. Options: `true` or `false`
SPRING_CACHE_REDIS_KEY-PREFIX | All keys will have the specified value
SPRING_CACHE_REDIS_TIME-TO-LIVE | Default TTL for all keys

It is possible to configure a TTL per key using the following nomenclature:
`CACHE_FLUSHINTERVALS_<key_name>` and the value in milliseconds.

The available Keys are:

* features
* signatures
* signaturesPage
* signaturesIds
* TODO: Pending caps

### Limits Configuration

Default caps values can be overwritten acording to the environment variables in the following table:

Environment variable | Default value
 --- | ---
LIMITS_CAP_DEFAULT_ENVIRONMENT_COORPORATE | 3000d
LIMITS_CAP_DEFAULT_ENVIRONMENT_RETAIL | 3000d
LIMITS_CAP_DEFAULT_PRODUCT_COORPORATE | 3000d
LIMITS_CAP_DEFAULT_PRODUCT_RETAIL | 3000d
LIMITS_CAP_DEFAULT_SIGNATURE_COORPORATE | 3000d
LIMITS_CAP_DEFAULT_SIGNATURE_RETAIL | 3000d
LIMITS_CAP_DEFAULT_USER_COORPORATE | 1000d
LIMITS_CAP_DEFAULT_USER_RETAIL | 1000d

Also you can change the cap control mode:

Environment variable | Default value
 --- | ---
LIMITS_CAP_CONTROL_INTERACTIVERUN | deduct
LIMITS_CAP_CONTROL_PROGRAMEDCREATE | check
LIMITS_CAP_CONTROL_PROGRAMEDEXECUTE | deduct
LIMITS_CAP_CONTROL_SCHEDULEDCREATE | check
LIMITS_CAP_CONTROL_SCHEDULEDEXECUTE | deduct

And if you want to turn off caps for some type:

Environment variable | Default value
 --- | ---
LIMITS_CAP_USER_ENABLED | true
LIMITS_CAP_ENVIRONMENT_ENABLED | true
LIMITS_CAP_PRODUCT_ENABLED | true

# Documentation

All the documentation for the microservice can be found in this [Confluence.](https://technisys.atlassian.net/wiki/spaces/TECOMNI/)

# DB seed
### Performance Tests
There is a migration that seeds the DB with data for the performance tests.
It creates features, signatures for a numbers of environments (AMOUT, NO_AMOUNT and ADM signatures), caps, caps for products|users|signatures.
This parametrized migration is located at `src/main/resources/db/migration/performance/mssql/V20200522163156__performance.sql` and the parameters (environment variables) are:

Environment variable | Description
 --- | ---
SPRING_FLYWAY_PLACEHOLDERS_ENVIRONMENTS | Number of environments.
SPRING_FLYWAY_PLACEHOLDERS_FEATURES | Number of features to create.
SPRING_FLYWAY_PLACEHOLDERS_SIGNATURE-PRODUCTS-PERCENT | Percentage of the **AMOUNT SIGNATURES** that have an association with a product.
SPRING_FLYWAY_PLACEHOLDERS_SIGNATURE-FEATURES-PERCENT | Percentage of the **AMOUNT SIGNATURES** that have an association with a feature.
SPRING_FLYWAY_PLACEHOLDERS_CAPS-FOR-PRODUCT | Percentage the **PRODUCTS** that have a specific CAP instead of the default one.
SPRING_FLYWAY_PLACEHOLDERS_CAPS-FOR-SIGNATURE-PERCENT | Percentage the **AMOUNT SIGNATURES** that have a specific CAP instead of the default one.
SPRING_FLYWAY_PLACEHOLDERS_CAPS-FOR-USER-PERCENT | Percentage the **USERS** that have a specific CAP instead of the default one.

### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"
BRIDGE_TRACING_ENABLED | Boolean value to indicate id bridge tracing is enabled https://bitbucket.org/technisys/tec-commons-tracing/src/master/
### Demo fresh install
To seed the limits db with the demo data, append into `spring.flyway.locations` variable the following `, classpath:db/migration/seed/mssql`. Don't forget to include the comma!!