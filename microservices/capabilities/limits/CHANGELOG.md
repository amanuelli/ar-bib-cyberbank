# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

### Changed
- Update platform limits component to 1.0.5 [TECDIGSK-410](https://technisys.atlassian.net/browse/TECDIGSK-410)
- Update to Profile C v2.0.0 with invoke [TECENG-277](https://technisys.atlassian.net/browse/TECENG-277)
- Implement v3 alternative of delivery workflow [TECENG-845](https://technisys.atlassian.net/browse/TECENG-845)

## [1.0.1](https://technisys.atlassian.net/projects/TECDIGSK/versions/53676/tab/release-report-all-issues) - 2021-02-02
### Added

- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)
- Add configuration.url and spring.cloud.config.uri variables into configmap [TECDIGSK-133](https://technisys.atlassian.net/browse/TECDIGSK-133)
- limits::add apm config in chart demo [TECDIGSK-237](https://technisys.atlassian.net/browse/TECDIGSK-237)

### Changed
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-650](https://technisys.atlassian.net/browse/TECENG-650).
- Improve promotion of jar dependencies between PLATFORM to STARTERKIT [TECENG-524](https://technisys.atlassian.net/browse/TECENG-524)
- Apply trivy's security container scanning [TECENG-422](https://technisys.atlassian.net/browse/TECENG-422)
- Limits and Configuration Dockerfiles update to latest snapshot [TECDIGSK-199](https://technisys.atlassian.net/browse/TECDIGSK-199)
- [LIMITS] Update Limits dockerfile to use the latest jar version containing the fix for the NumberFormatException error [TECDIGSK-243](https://technisys.atlassian.net/browse/TECDIGSK-243)
- [LIMITS] tag'n release a new version of starterkit limits that use the latest released version of platform limits [TECDIGSK-281](https://technisys.atlassian.net/browse/TECDIGSK-281)

### Removed
- [Limits] remove seed migration from configmap [TECDIGSK-213](https://technisys.atlassian.net/browse/TECDIGSK-213)

## [1.0.0](https://technisys.atlassian.net/projects/TECDIGSK/versions/53675/tab/release-report-all-issues) - 2020-10-07

### Changed
- Refactor with Jars delivery on PLATFORM to Limits STARTERKIT [TECDIGSK-116](https://technisys.atlassian.net/browse/TECDIGSK-116)
- Changed reference to limits image on Helm values [TECENG-421](https://technisys.atlassian.net/browse/TECENG-421)

