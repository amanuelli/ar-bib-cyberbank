# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [1.0.10]
### Added
- Add permission for communications trays messages. [TECDIGSK-566](https://technisys.atlassian.net/browse/TECDIGSK-566)
- Rename permission for List communications trays messages. [TECDIGSK-581](https://technisys.atlassian.net/browse/TECDIGSK-581) 
- Add approve permission for communications trays messages. [TECDIGSK-586](https://technisys.atlassian.net/browse/TECDIGSK-586)

### Changed
- Update platform staff management component to 1.0.17 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)

## [1.0.9] - 2021-08-23
### Added
- Created audit permission to view Customer authentication audit, User authentication audit. [TECMSAAU-259](https://technisys.atlassian.net/browse/TECMSAAU-259)
- Added endpoint to expose the list of return codes [TECCDPBO-4467](https://technisys.atlassian.net/browse/TMAG-43)
- General conditions add a new term from scratch P2 [TECCDPBO-4323](https://technisys.atlassian.net/browse/TECCDPBO-4323)
- Activate QG in CyBO Staff management client [CE-978](https://technisys.atlassian.net/browse/CE-978)
- Added synchronization of permission from a json file [TECCDPBO-4055](https://technisys.atlassian.net/browse/TECCDPBO-4055)

### Changed
- Update platform staff management component to 1.0.14 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Fix Order by findAllFull [TECCDPBO-4499](https://technisys.atlassian.net/browse/TECCDPBO-4499)
- Error adjusted related to the system not allowing the modification of a BO user [TECCDPBO-4340](https://technisys.atlassian.net/browse/TECCDPBO-4340)
- Change MS response format according to new specification [TMAG-27](https://technisys.atlassian.net/browse/TMAG-27)
- Fixed error 400 when creating users through file import[TECCDPBO-3705](https://technisys.atlassian.net/browse/TECCDPBO-3705)
- Response handling to user image endpoint [TMAG-29](https://technisys.atlassian.net/browse/TMAG-27)
- Tests coverage increased [TECCDPBO-3807](https://technisys.atlassian.net/browse/TECCDPBO-3807)
- Fixed user list filter by document [TECCDPBO-3957](https://technisys.atlassian.net/browse/TECCDPBO-3957)

## [1.0.8] - 2021-07-05
### Added
- Added caps approve permissions [TECCDPBO-4191](https://technisys.atlassian.net/browse/TECCDPBO-4191)
- Added backoffice permission for general conditions  [TECCDPBO-4323](https://technisys.atlassian.net/browse/TECCDPBO-4323)
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
  
### Changed
- Update platform staff management component to 1.0.13 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Added handling of permissions from a json file [TECDIGSK-397](https://technisys.atlassian.net/browse/TECDIGSK-397)
- Added release pipeline and modified Dockerfile to adapt to the Initializer initiative [TECCDPBO-3833](https://technisys.atlassian.net/browse/TECCDPBO-3833)

## [1.0.7] - 2021-03-23
### Changed
- Update platform staff management component to 1.0.9 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- First Version. The code come from cybo staff-management repository [TECENG-1056](https://technisys.atlassian.net/browse/TECENG-1056)


