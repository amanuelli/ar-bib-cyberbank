# Staff Management Microservice 

# Content

By default, this services is based on Staff Management .jar and builds a Docker image.

## Code analysis

N/A. This is not a code project.

## Pipeline Status

 CICD     | 
----------|
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20StaffManagement/Staff%20Management%20-%20Techbank%20-%20CI-CD?branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=1969&branchName=master) |

## Jira project

TEC - Digital Starterkit: [TECDIGSK](https://technisys.atlassian.net/projects/TECDIGSK/issues?filter=allissues)

### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"
BRIDGE_TRACING_ENABLED | Boolean value to indicate id bridge tracing is enabled https://bitbucket.org/technisys/tec-commons-tracing/src/master/
