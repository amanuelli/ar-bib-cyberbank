# Default values for api.
# This is a YAML-formatted file.
# Declare variables to be passed into your templates.

name: safeway

labels:
  app: safeway
  lang: java
  platform: digital

selectorLabels:
  app: safeway

deployment:

  image:
    repository: tecdigitalacr.azurecr.io/digital/safeway
    version: #{MS_VERSION}#
    pullPolicy: Always

  replicaCount: 1

  strategy:
    type: Recreate

  securityContext:
    runAsUser: 1000
    fsGroup: 1005

  port: 8080

  initContainers:
    - name: check-ses
      image: busybox:1.28
      command: ['sh', '-c', 'until nc -zv ses.digital-test 80; do echo waiting for ses; sleep 2; done']
  
  envFrom:
    - configMapRef:
        name: safeway-config
    - secretRef:
        name: safeway-secrets

  imagePullSecrets:
    - name: regcred

  volumeMounts:
    - mountPath: /var/safeway
      name: volume

  volumes:
  - name: volume
    persistentVolumeClaim:
        claimName: volume-safeway

  serviceAccountName: tecdigital-sa


  resources:
    limits:
      cpu: 1500m
      memory: 1024Mi
    requests:
      cpu: 250m
      memory: 512Mi


service:
  type: ClusterIP
  port: 80
  targetPort: 8080

configPodReset:
  - configmap.yaml
  - secrets.yaml

configMap:
  name: safeway-config
  dbserver:
    driver: com.microsoft.sqlserver.jdbc.SQLServerDriver
    url: jdbc:sqlserver://tec-digital.database.windows.net;databaseName=tec-digital-safeway-test-aks
  db_certificate_maxActiveConnections: "100"
  db_certificate_maxIdleConnections: "30"
  db_certificate_engine: "sql-server"
  db_certificate_pingQuery: "select 1 from client_systems"
  db_certificate_pingEnabled: "true"
  db_certificate_maxWait: "10000"
  db_certificate_pingConnectionsNotUsedFor: "10000"
  db_maxRowsReturned: "100000"
  db_autoUpdateDBVS: "true"
  SES_URL: "http://ses.digital-test"
  AUDIT_URL: "http://audit.digital-test"
  AUDIT_SPRING_RABBITMQ_ADDRESSES: "tec-digital-rabbit.digital-test.svc.cluster.local"
  AUDIT_SPRING_RABBITMQ_PORT: "5672"
  AUDIT_SPRING_RABBITMQ_QUEUE: "test.audit.queue"
  AUDIT_SPRING_RABBITMQ_EXCHANGE: "test.audit.exchange"
  AUDIT_SPRING_RABBITMQ_ROUTING: "test.audit.routing"
  AUDIT_SPRING_RABBITMQ_VIRTUALHOST: "/"
  JAVA_OPTS: "-javaagent:/usr/local/tomcat/elastic-apm-agent.jar"
  ELASTIC_APM_SERVER_URLS: "http://apm-server.monitor-services:8200"
  ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
  ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
  ELASTIC_APM_APPLICATION_PACKAGES: "com.technisys"
  ELASTIC_APM_CENTRAL_CONFIG: "true"
  ELASTIC_APM_SERVICE_NAME: "safeway"
  ELASTIC_APM_LOG_LEVEL: "INFO"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "false"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: ""
  ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: ""
  ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc, redis, jax-rs, jax-ws"
  ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME: "true"
  SAFEWAY_LOG_LEVEL: "INFO"

secrets:
  name: safeway-secrets
  dbserver:
    username: "#{database.username}#"
    password: "#{database.password}#"
  rabbitmq:
    username: "#{rabbitmq.username}#"
    password: "#{rabbitmq.password}#"

volume:
  name: volume-safeway
  persistentvolumes:
    local: "false"
    storageClassName: "azurefile"