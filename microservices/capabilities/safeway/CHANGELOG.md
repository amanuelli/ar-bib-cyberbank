# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

## [6.4.5] - 2021-03-17
### Changed
- First Version. The code come from man-safeway repository [TECENG-1055](https://technisys.atlassian.net/browse/TECENG-1055)


