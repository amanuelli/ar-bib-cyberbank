if $DEBUG_MODE; then
  java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -Djava.security.egd=file:/dev/./urandom -jar $TECHNISYS_HOME/onboarding.jar
else
  java -Djava.security.egd=file:/dev/./urandom -jar $TECHNISYS_HOME/onboarding.jar
fi