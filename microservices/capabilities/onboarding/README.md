# Digital Microservices template
This repository is a template to be used in every new cross Digital Microservice. It contains a spring boot project and a standard java library project to use as a client or SDK to consume the microservices API. Please treat this as a starting point and get the project structure and related tools aligned.


## Cross Microservice Technology stack
* Maven
* Java 11
* Spring boot 2.1.7
* Flyway 6
* Swagger 2
* MyBatis (https://github.com/mybatis/spring-boot-starter)

## Cross Microservice Client Technology stack
* Maven
* Java 11
* Open Feign 10.3 (https://github.com/OpenFeign/feign)
* Hystrix Feign (https://github.com/OpenFeign/feign/tree/master/hystrix)

## Cross Microservice

### Application Structure

The structure looks as follows :

```
> src
    > main
        > java
            > com.technisys.digital.onboarding
                > aspect
                > config
                > controller
                > exception
                > model
                > repository
                > service
                CrossMicroserviceTemplateAplication
        > resources
Dockerfile        
pom.xml
```


### Model and DTOs

The various models of the application are organised under the model package, for now, models represent DTOs (data transfer objects) too. DTOs let us transfer only the data that we need to share with the user interface and not the entire model object that we may have aggregated using several sub-objects and persisted in the database. 

If a microservice need to create differents DTOs, you can use ignore notation, and if you need more than one DTO for the same model, feel free and create a dto package at the same level of model.

### Services and DAOs

The data access objects (DAOs) are present in the repository package, and the service layer is defined under the service package. 

### Controllers

Last, but the most important part is the controller layer. It binds everything together right from the moment a request is intercepted till the response is prepared and sent back. The controller layer is present in the controller package, the best practices suggest that we keep this layer versioned to support multiple versions of the application and the same practice is applied here.

### Configuration

All Configuration classes, like Swagger, are located in config package.

### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"
BRIDGE_TRACING_ENABLED | Boolean value to indicate id bridge tracing is enabled https://bitbucket.org/technisys/tec-commons-tracing/src/master/

### Database migrations

Database migrations are managed by [Flyway](https://flywaydb.org/getstarted/). 
MigrationConfig Class  is a spring configuration with a redefinition of Bean Flyway. In that bean we can change dynamically the location of migrations or even the datasource. By default Flyway inspect `classpath:db/migrations`.

### Aspects - Cross cutting concerns

In this package has all functions that span multiple points of the microservice (corss cutting concerns) and are separated from the application's business logic.
Various common cases of aspects are logging, auditing, tracing, declarative transactions, security, etc.

## Client (SDK)

The main propose of the sdk is to  simplify the use of the microservice and avoid multiple client (sdk) implementations.
The library used is [Feign](https://github.com/OpenFeign/feign). Feign is a declarative Http client, which integrates a latency and fault tolerance library (hystrix).

## How to start using this template

Follow the instructions in the [Confluence document](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/580850190/How+to+create+a+Java+Microservice)