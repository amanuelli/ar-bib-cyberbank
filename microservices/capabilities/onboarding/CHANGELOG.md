# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

## [1.0.1] - 
### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)

### Changed
- Update to platform version 1.0.3. [TECDIGSK-545](https://technisys.atlassian.net/browse/TECDIGSK-545)
- Update to Profile C v2.0.0 with invoke [TECMSONB-1](https://technisys.atlassian.net/browse/TECMSONB-1)
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-843](https://technisys.atlassian.net/browse/TECENG-843).
- Remove JAEGER variables and set new variable BRIDGE_TRACING_ENABLED [TECDIGSK-254] (https://technisys.atlassian.net/browse/TECDIGSK-254)

### Security
- `snakeyaml.1.23.jar`, transitive dependency of SpringBoot Starter, updated to `1.27` to fix vulnerability [CVE-2017-18640](https://nvd.nist.gov/vuln/detail/CVE-2017-18640) [TECMSLIB-44](https://technisys.atlassian.net/browse/TECMSLIB-44)
- Exclusion log4j dependency to fix vulnerability reported in issue -> [TECENG-378](https://technisys.atlassian.net/browse/TECENG-378)
- Update guava dependency to fix security issue -> [TECMSTEMP-23](https://technisys.atlassian.net/browse/TECMSTEMP-23)
- Upgrade spring-boot.version 2.2.0.RELEASE -> 2.2.10.RELEASE to avoid known vulnerabilities:
    * CVE-2020-5397 spring-core-5.1.9.RELEASE.jar [TECMSTEMP-13](https://technisys.atlassian.net/browse/TECMSTEMP-13)
    * CVE-2020-10683 dom4j-2.1.1.jar [TECMSTEMP-18](https://technisys.atlassian.net/browse/TECMSTEMP-18)
    * CVE-2019-14900 hibernate-core-5.4.6.Final.jar [TECMSTEMP-19](https://technisys.atlassian.net/browse/TECMSTEMP-19)
- Exclude jakarta.websocket-api from spring-boot-starter-jetty to avoid vulnerability CVE-2020-11050 [TECMSTEMP-21](https://technisys.atlassian.net/browse/TECMSTEMP-21)
- Update to com.technisys.commons.tracing:commons-tracing-spring:1.0.3 to fix vulnerability CVE-2019-0205 libthrift-0.12.0 [TECMSTEMP-22](https://technisys.atlassian.net/browse/TECMSTEMP-22)

### Removed
- Remove unused dependency springfox-swagger-ui -> [TECMSTEMP-23](https://technisys.atlassian.net/browse/TECMSTEMP-23)