# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

## [1.0.4] - 2021-09-22
### Changed
- Fix Oracle migration error platform scheduler component to 1.1.2. [TECMSSCHE-105](https://technisys.atlassian.net/browse/TECMSSCHE-105)


## [1.0.3] - 2021-08-30
### Changed
- Update platform scheduler component to 1.1.1. [TECENG-1890](https://technisys.atlassian.net/browse/TECENG-1890)

### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
### Changed
- Update platform scheduler component to 1.1.0

## [1.0.2] - 2021-05-27
### Changed
- Use scheduler 1.0.4 (platform) that includes api documentation update [TECMSSCHE-93](https://technisys.atlassian.net/browse/TECMSSCHE-93)

## [1.0.1] - 2021-03-26
### Changed
- Use scheduler 1.0.1 (platform) that includes fix for deamon update [TECMSSCHE-79](https://technisys.atlassian.net/browse/TECMSSCHE-79)

### Added
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)

### Changed
- Update to Profile C v2.0.0 with invoke [TECENG-277](https://technisys.atlassian.net/browse/TECENG-277)
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-657](https://technisys.atlassian.net/browse/TECENG-657).

## [[1.0.0]](https://technisys.atlassian.net/projects/TECDIGSK/versions/54169/tab/release-report-all-issues) - 2021-01-18
### Added
- First commit, added (changelog, readme, etc) [TECMSSCHE-3](https://technisys.atlassian.net/browse/TECMSSCHE-3)
- Remove JAEGER variables and set new variable BRIDGE_TRACING_ENABLED [TECDIGSK-255](https://technisys.atlassian.net/browse/TECDIGSK-255)