# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [1.1.7] - 2021-08-24
- Update SES version to 2.0.0 - New V2. [TECDIGSK-451](https://technisys.atlassian.net/browse/TECDIGSK-451)
- Update SES version to 1.4.30 - New V2. [TECDIGSK-444](https://technisys.atlassian.net/browse/TECDIGSK-444)
- Update SES version to 2.0.1 [TECDIGSK-480](https://technisys.atlassian.net/browse/TECDIGSK-480)
- Adding layered spring boot config to Docker file. [TECDIGSK-480](https://technisys.atlassian.net/browse/TECDIGSK-480)
- Changed values.yaml and upgraded audit platform version. [TECDIGSK-480](https://technisys.atlassian.net/browse/TECDIGSK-480)
- Update SES version to 2.0.2 [TECDIGSK-557](https://technisys.atlassian.net/browse/TECDIGSK-557)

## [1.1.6] - 2021-06-11
- Update SES version to 1.3.3 - Fix SES delays. [TECDIGSK-421](https://technisys.atlassian.net/browse/TECDIGSK-421)
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
- Update SES version to 1.3.2 [TECDIGSK-398](https://technisys.atlassian.net/browse/TECDIGSK-398)

## [1.1.5] - 2021-04-28
- Update SES version to 1.3.1 [TECDIGSK-389](https://technisys.atlassian.net/browse/TECDIGSK-389)
- Update SES version to 1.3.0 [TECDIGSK-386](https://technisys.atlassian.net/browse/TECDIGSK-386)

## [1.1.4] - 2021-04-15
### Changed
- Update SES version to 1.2.2 [TECDIGSK-365](https://technisys.atlassian.net/browse/TECDIGSK-370)
- Add config for change redis enabled status [TECDIGSK-365](https://technisys.atlassian.net/browse/TECDIGSK-366)
- Update SES version to 1.2.1 [TECDIGSK-365](https://technisys.atlassian.net/browse/TECDIGSK-365)
- New version from tec-safeway-ses including connectivity with AWS [TECENG-1133](https://technisys.atlassian.net/browse/TECENG-1133)
- First Version. The code come from tec-safeway-ses repository [TECENG-1052](https://technisys.atlassian.net/browse/TECENG-1052)
- Added secrets that contains credentials to connect SES with AWS KMS [TECDIGSK-352](https://technisys.atlassian.net/browse/TECDIGSK-352)


