# Safeway Encryption Service (SES)
Microservice to encrypt / decrypt

[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20SES/SES%20-%20Techbank%20-%20CI-CD?repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=1933&repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)

# Table of Contents

- [Project Overview](#markdown-header-project-overview)
  - [Microservice](#markdown-header-microservice)
- [Getting Started](#markdown-header-getting-started)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Installation](#markdown-header-installation)
  - [Configuration](#markdown-header-configuration)
    - [DB Configuration](#markdown-header-db-configuration)
    - [Tracing Configuration](#markdown-header-tracing-configuration)
- [Documentation](#markdown-header-documentation)

# Project Overview

Microservice to encrypt / decrypt data

## Microservice 

### Technology stack

* Maven
* Java 11
* Spring boot 2.1.7
* Flyway 6
* Swagger 2
* JPA

This microservice use a mssql or oracle scheme to work. It can be configured using environment variable. The suggested name is "digital_ses"

# Getting Started


## Prerequisites

- Java 11
- Maven 3.x
- Docker (Optional)
- A database on one of the supported DB engines.

## Installation
```
docker build -t my-ses-image:latest .
```
Then you can run it (for example on port 9000) using the following command:
```
docker run -d --name ses -p 9000:8080 -e 'spring.datasource.password=yourpassword' -t my-ses-image:latest 
```
By naming the container, we can execute actions using it, for example see logs:
```
docker logs -f ses
```

## Configuration
Default configuration values are defined in application.yml. 

### DB Configuration

Environment variable | Description
 --- | ---
SPRING_DATASOURCE_URL | jdbc to database
SPRING_DATASOURCE_DRIVER | jdbc driver
SPRING_DATASOURCE_USERNAME | Database username
SPRING_DATASOURCE_PASSWORD | Database password
SPRING_DATASOURCE_HIKARI_MINIMUMIDLE | Minimum number of idle connections maintained in a connection pool
SPRING_DATASOURCE_HIKARI_MAXIMUMPOOLSIZE | Maximum connection pool size
SPRING_DATASOURCE_HIKARI_CONNECTIONTIMEOUT | Maximum milliseconds that a client will wait for a connection
SPRING_FLYWAY_LOCATIONS | To choose migrations according to the db engine. Options: `classpath:db/migration/mssql` or `classpath:db/migration/oracle`

### Tracing Configuration

Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"
BRIDGE_TRACING_ENABLED | Boolean value to indicate id bridge tracing is enabled https://bitbucket.org/technisys/tec-commons-tracing/src/master/

# Documentation

All the documentation for the microservice can be found in this [Confluence.](https://technisys.atlassian.net/wiki/spaces/TECOMNI/)
