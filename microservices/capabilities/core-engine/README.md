# Core Engine

[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-core/Core%20Engine/Core%20Engine%20%20Build-starterkit?repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=2392&repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)

# Table of Contents
- [Core Engine](#core-engine)
- [Table of Contents](#table-of-contents)
- [Project Overview](#project-overview)
- [Technology stack](#technology-stack)
- [Configuration](#configuration)
    - [Language and error translations](#language-and-error-translations)
    - [Databases](#databases)
      - [Business](#business)
      - [Services](#services)
    - [HTTP Server settings](#http-server-settings)
    - [Security](#security)
      - [Socket](#socket)
      - [HTTPS](#https)
    - [Filesystem settings](#filesystem-settings)
    - [Authentication settings](#authentication-settings)
    - [General configuration](#general-configuration)
    - [Kafka](#kafka)
- [Kafka installation](#kafka-installation)
  - [Add repo confluentinc](#add-repo-confluentinc)
  - [Update repo](#update-repo)
  - [Install kafka](#install-kafka)
    - [ConfigMap Files Properties](#configmap-files-properties)
- [Documentation](#documentation)

# Project Overview
Cyberbank Core Engine is the main component of Cyberbank Core, acting as the interpreter of the business rules mapped through service compositions specified by the user. The Engine is a Java standalone application that reads and executes the metadata generated using Cyberbank CPG tool. 

# Technology stack
* Maven
* Java 11
* Supported Databases: SQL Server & Oracle

# Configuration
The following properties must be configurated into values.yaml

### Language and error translations
Property|Value
---|---
engine.errors.language| Language chosen to show errors
engine.errors.locales| Locales for error translations
engine.runtime.language| Language chosen to general messages
engine.datamodel.translate.enabled| Enables or disables the transtation of de Data model
engine.datamodel.translate.language| Id number of language chosen to show Data model. Currently 1=Spanish, 2=English.


### Databases

Property|Value
---|---
hibernate.hikari.connectionTimeout| This property controls the maximum number of milliseconds that a client (that's you) will wait for a connection from the pool. If this time is exceeded without a connection becoming available, a SQLException will be thrown. Lowest acceptable connection timeout is 250 ms. Default: 30000 (30 seconds)
hibernate.hikari.autoCommit| This property controls the default auto-commit behavior of connections returned from the pool. It is a boolean value. Default: true
hibernate.hikari.minimumIdle| This property controls the minimum number of idle connections that HikariCP tries to maintain in the pool. If the idle connections dip below this value and total connections in the pool are less than maximumPoolSize, HikariCP will make a best effort to add additional connections quickly and efficiently. However, for maximum performance and responsiveness to spike demands, we recommend not setting this value and instead allowing HikariCP to act as a fixed size connection pool. Default: same as maximumPoolSize
hibernate.hikari.maximumPoolSize| This property controls the maximum size that the pool is allowed to reach, including both idle and in-use connections. Basically this value will determine the maximum number of actual connections to the database backend. A reasonable value for this is best determined by your execution environment. When the pool reaches this size, and no idle connections are available, calls to getConnection() will block for up to connectionTimeout milliseconds before timing out. Please read about pool sizing. Default: 10
hibernate.hikari.idleTimeout| This property controls the maximum amount of time that a connection is allowed to sit idle in the pool. This setting only applies when minimumIdle is defined to be less than maximumPoolSize. Idle connections will not be retired once the pool reaches minimumIdle connections. Whether a connection is retired as idle or not is subject to a maximum variation of +30 seconds, and average variation of +15 seconds. A connection will never be retired as idle before this timeout. A value of 0 means that idle connections are never removed from the pool. The minimum allowed value is 10000ms (10 seconds). Default: 600000 (10 minutes)
hibernate.hikari.connectionTimeout| This property controls the maximum number of milliseconds that a client (that's you) will wait for a connection from the pool. If this time is exceeded without a connection becoming available, a SQLException will be thrown. Lowest acceptable connection timeout is 250 ms. Default: 30000 (30 seconds)
hibernate.connection.provider_class| org.hibernate.hikaricp.internal.HikariCPConnectionProvider
hibernate.id.new_generator_mappings| false

#### Business
Property|Value
---|---
engine.db.connection_string|  Connection string to database.  If is absent, this is taken from cfg.xml
engine.db.vendor| Database engine provider name. Possible values| ORACLE, SQLSERVER(Case insensitive)
engine.db.hibernate.dialect|  Database dialect. e.g. org.hibernate.dialect.OracleDialect, org.hibernate.dialect.SQLServerDialect, org.hibernate.dialect.DB2Dialect
engine.db.schema|  Database schema.  If absent, user name is used.
engine.db.user|  Database username.
engine.db.password|  Database password.
engine.db.max_results|  Limit of result by query. If is absent or 0, the result hasn’t limit.
engine.db.driver_class|  JDBC Driver for database interaction. e.g. oracle.jdbc.OracleDriver, org.postgresql.Driver, COM.ibm.db2.jdbc.app.DB2Driver
engine.db.use_interceptor| Enable or disable logging interceptor
engine.db.interceptor_class| Logging interceptor Java class
engine.db.interceptor.auditloginterceptor.manager| Audit logging interceptor manager Java class

#### Services
Property|Value
---|---
cpg.db.hibernate_cfg_xml|  Filename for Hibernate configurations and mapping to services database.
cpg.db.connection_string| Connection string to services database.
cpg.db.vendor| Database engine provider name.
cpg.db.user| Database username.
cpg.db.password|  Database password.
cpg.db.driver_class|  JDBC Driver for services database interaction. e.g. oracle.jdbc.OracleDriver, org.postgresql.Driver, COM.ibm.db2.jdbc.app.DB2Driver
cpg.db.hibernate.dialect|  Services database dialect. e.g. org.hibernate.dialect.OracleDialect, org.hibernate.dialect.SQLServerDialect, org.hibernate.dialect.DB2Dialect, org.hibernate.dialect.PostgreSQLDialect
cpg.db.schema|  Database schema.  If is absent, user name is used.

### HTTP Server settings
Property|Value
---|---
engine.http.server.enabled| Enable or disable http access to the Engine.
engine.http.server.port| Port number to listen.
engine.http.server.host| Host name
engine.http.server.idletimeout| Waiting time for response in millisecond 
engine.http.server.threads.max| Maximum pool size of threads
engine.http.server.threads.min=Minimum pool size of threads

### Security
#### Socket
Property|Value
---|---
engine.transaction.socket.ssl.enabled| Enables or disables secured communication
engine.transaction.socket.ssl.keystore.file| Keystore Java path
engine.transaction.socket.ssl.keystore.password| Keystore password
engine.transaction.socket.ssl.truststore.file| Truststore (Warehouse of trust certificates) path
engine.transaction.socket.ssl.truststore.password| Truststore password

#### HTTPS
Property|Value
---|---
engine.https.server.enabled| Enables or disables CORE-CHANNEL communication by HTTPS
engine.https.server.host| Host name
engine.https.server.port| Secure port of core transactions. By default it is 8443
engine.https.server.keystore.path| Certificate Keystore Java
engine.https.server.keystore.password| Keystore certificate password
engine.https.server.keymanager.password| Certificate key store
engine.https.server.truststore.path| Warehouse of recognized certificates
engine.https.server.truststore.password| Certified certificates key

### Filesystem settings
Property|Value
---|---
engine.fs.path.services| Service folder path
engine.fs.path.errors| Error message folder path
engine.fs.path.language| Language folder path

### Authentication settings
Property|Value
---|---
engine.auth.request.time| Timeout for authentication requests in seconds. Default 2.
engine.auth.enabled| Enable or disable authentication by HMac. Default false.


### General configuration
Property|Value
---|---
engine.interface.project.alias| Load operation only for the project alias set. Default Tech, set NONE for disable
region.project.alias| Load operation only for the region alias set. Set NONE for disable
engine.operations.loader| Java class for Operations loader factory. For DB connection  net.technisys.cyberbank.core.engine.service.OperationsLoaderFactory. For filesystem           net.technisys.cyberbank.core.engine.service.OperationsJsonFSLoader.
auditable.entity.status| Entity status required to be auditable.
engine.folder.entities | Internal folder where core read entities. Default /home/technisys/entities
engine.folder.basicservices | Internal folder where core read basicservices. Default /home/technisys/basicservices
engine.folder.extensions | Internal folder where core read extensions. Default /home/technisys/extensions
engine.monitor.handler | configure which technology  will be used for the core-monitor. ( Default:LOGBACK) the possible values can be:  (KAFKA,NONE,LOGBACK)
engine.port.monitor.enabled | true or false. Enables the monitor by socket and disables the other handlers, when using KAFKA or ELASTIC handlers it must be 'false'.

### Kafka 
Property|Value
---|---
engine.monitor.kafka.topic| Topic to which the monitor will subscribe
engine.monitor.kafka.bootstrap.servers | Kafka address in  format IP: PORT where it is running in the cluster or VM
engine.monitor.kafka.client.id | Identifier of the client to which it connects.

# Kafka installation
As of version 3.5.0 or higher, the core-engine can be configured with kafka. 

It is recommended to use the kafka version:
```
 confluentinc/cp-enterprise-kafka: 6.1.0
```
For install it you must execute the following commands:

## Add repo confluentinc
helm repo add confluentinc https://confluentinc.github.io/cp-helm-charts/

## Update repo
helm repo update

## Install kafka

Execute the next command where:

- **AVAILABLE_STARTING_NODE_PORT**: is the starting available node port to expose kafka brokers, if you will deploy a kafka with 3 replicas you need 3 consecutives available ports for exameple 30180, 30181, 30182 where the starting node port is 30180.
- **NAMESPACE**: Kubernetes namespace to deploy kafka.

```bash
helm install tec-confluent-oss confluentinc/cp-helm-charts --set cp-kafka-rest.enabled=false,cp-kafka-connect.enabled=false,cp-ksql-server.enabled=false,cp-control-center.enabled=false,cp-schema-registry.enabled=false,cp-control-center.prometheus.jmx.enabled=false,cp-zookeeper.persistence.enabled=false,cp-zookeeper.prometheus.jmx.enabled=false,cp-kafka.persistence.enabled=false,cp-kafka.prometheus.jmx.enabled=false,cp-schema-registry.prometheus.jmx.enabled=false,cp-kafka.nodeport.enabled=truecp-kafka.nodeport.firstListenerPort=<AVAILABLE_STARTING_NODE_PORT> -n <NAMESPACE>
```

By default 3 instances of zookeeper and kafka are deployed. For expose each kafka broker service, patch k8s service with:

```bash
## Patch broker 0
kubectl patch svc tec-confluent-oss-cp-kafka-0-nodeport -p '{"metadata": { "annotations": { "service.beta.kubernetes.io/azure-load-balancer-internal": "true", "service.beta.kubernetes.io/azure-load-balancer-internal-subnet": "<SUBNET_NAME_ILB>"  } }, "spec": { "type": "LoadBalancer" } }' -n <NAMESPACE>

## Patch broker 1
kubectl patch svc tec-confluent-oss-cp-kafka-1-nodeport -p '{"metadata": { "annotations": { "service.beta.kubernetes.io/azure-load-balancer-internal": "true", "service.beta.kubernetes.io/azure-load-balancer-internal-subnet": "<SUBNET_NAME_ILB>"  } }, "spec": { "type": "LoadBalancer" } }' -n <NAMESPACE>

## Patch broker 2
kubectl patch svc tec-confluent-oss-cp-kafka-2-nodeport -p '{"metadata": { "annotations": { "service.beta.kubernetes.io/azure-load-balancer-internal": "true", "service.beta.kubernetes.io/azure-load-balancer-internal-subnet": "<SUBNET_NAME_ILB>"  } }, "spec": { "type": "LoadBalancer" } }' -n <NAMESPACE>
```


### ConfigMap Files Properties
Inside folder **core-engine/templates/config** are the properties files that are rendered by Helm and a configMap is created containing them. This configamap is mounted inside the core-engine pod.

Following files are inside:
* Engine.properties: Engine configuration.
* DbBag.properties: Additional database properties.
* logConfiguration.xml: Configuration file  logging.
* glbs.ini: Default values for global variables.
* cacheable_entities.txt: List of entities to be used in the Backoffice cache strategy.
* toplevel.txt: Services to be loaded at startup.
* errorHandlerList.properties: List of handler error's

In this way you can have multiple folders with configuration of these files for each environment.

For example: **core-engine/templates/configDev** and **core-engine/templates/configQA**

At the moment of installing the chart core-engine, it is indicated within the **configMap_properties** block in the values ​​(using the key **config_folder**: "folder_name"), the folder from which to take these files to load.
For example:
Deploy over QA using the properties folder for configQA
```
....
....
configMap_properties:
  name: core-engine-config-properties
  config_folder: configQA
....
....
```

# Documentation
All the documentation for the microservice can be found in this [Confluence.](https://technisys.atlassian.net/wiki/spaces/TC/pages/2114884420/Cyberbank+Core+Engine)
