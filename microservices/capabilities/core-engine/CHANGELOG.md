# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [3.5.2] 2021-11-18

### Changed
- Updated script to remove entities jars from lib directory. [TECDIGSK-726](https://technisys.atlassian.net/browse/TECDIGSK-726)

## [3.5.1] 2021-11-08

### Changed
- Core-Engine Update Readme.md [TECDIGSK-716](https://technisys.atlassian.net/browse/TECDIGSK-716)
- Changes in implementation of Kafka support in the engine. [RCCP-2047](https://technisys.atlassian.net/browse/RCCP-2047)
### Added
- Adding context information on error handling and additional entity information. [RCCP-1655](https://technisys.atlassian.net/browse/RCCP-1655)

## [3.5.0] 2021-10-21

### Added
- Add connection for monitor with kafka. [RCCP-1948](https://technisys.atlassian.net/browse/RCCP-1948)

### Fixed
- Expression Evaluator not working with "tostring" in primitives. [RCCP-2022](https://technisys.atlassian.net/browse/RCCP-2022)

### Changed
- Fixed response for invalid service request. [RCCP-1917](https://technisys.atlassian.net/browse/RCCP-1917)


## [3.4.6] 2021-09-01

### Added
- Unit and integration testing skeleton for Core Engine. [RCCP-1502](https://technisys.atlassian.net/browse/RCCP-1502)
- Increased Core Engine unit test coverage to ten percent. [RCCP-1675](https://technisys.atlassian.net/browse/RCCP-1675)
- Audit interceptor extensibility. [RCCP-1527](https://technisys.atlassian.net/browse/RCCP-1527)

### Changed
- Solved an error while performing the restart of services by name with a filesystem datasource. [RCCP-1674](https://technisys.atlassian.net/browse/RCCP-1674)
- Improved Core Common's Sonar reliability score to comply with quality gate. [RCCP-1647](https://technisys.atlassian.net/browse/RCCP-1647)
- Solved a concurrency issue in the creation of a SessionFactory during application start. [RCCP-1845](https://technisys.atlassian.net/browse/RCCP-1845)
- Applied Snyk security recommendations for Core Engine dependencies. [RCCP-1931](https://technisys.atlassian.net/browse/RCCP-1931)
- Fixed owasp top 10 issues reported by Sonar. [RCCP-1929](https://technisys.atlassian.net/browse/RCCP-1929)
- Solved an issue causing some exceptions to have its qualified name returned as part of the response of services. [RCCP-1939](https://technisys.atlassian.net/browse/RCCP-1939)


## [3.4.5]
- Enable apm agent on demo [SDLC-37](https://technisys.atlassian.net/browse/SDLC-37)


## [3.4.4] 2021-07-20

### Changed
- Update functional environments values [SDLC-117](https://technisys.atlassian.net/browse/SDLC-117)
- Update demo database for CoreEngine [TECDIGSK-462](https://technisys.atlassian.net/browse/TECDIGSK-462)
- Initial configuration with Profile C v2.0.0 [TECENG-1191](https://technisys.atlassian.net/browse/TECENG-1191)
- Configmap was divided into 2, one for environment variables and one for variables for configuration files. [TECENG-1419](https://technisys.atlassian.net/browse/TECENG-1419)
- Add JAVA environments variables [TECENG-1419](https://technisys.atlassian.net/browse/TECENG-1419)
- Image was updated, It was defined the entire package from env: TECHNISYS_HOME [TECENG-1419](https://technisys.atlassian.net/browse/TECENG-1419)
- Add and configure Elasticsearch APM agent [TECENG-1419](https://technisys.atlassian.net/browse/TECENG-1419)
- Added support for OCP deployment [TECENG-1259](https://technisys.atlassian.net/browse/TECENG-1259)
