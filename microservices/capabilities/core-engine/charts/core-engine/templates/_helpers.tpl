{{/*
Template for creating registry credentials
*/}}
{{- define "imagePullSecret" }}
{{- printf "{\"auths\": {\"%s\": { \"username\": \"%s\",\"password\": \"%s\",\"auth\": \"%s\"}}}" .Values.imageRegistry .Values.registryCredentials.username .Values.registryCredentials.password (printf "%s:%s" .Values.registryCredentials.username .Values.registryCredentials.password | b64enc) | b64enc }}
{{- end }}