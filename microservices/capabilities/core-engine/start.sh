#!/bin/bash
rm -rf ${TECHNISYS_HOME}/lib/core_entities*
rm -rf ${TECHNISYS_HOME}/lib/core-entities*

#copy entities from volume
cp -r ${ENGINE_VOLUMEN}/entities/* ${TECHNISYS_HOME}/entities/

#copy basicservices from volume
cp -r ${ENGINE_VOLUMEN}/basicservices/* ${TECHNISYS_HOME}/basicservices/

#run core engine
java -Dcore-engine.configPath=${TECHNISYS_HOME}/conf/ -cp "${TECHNISYS_HOME}/core.jar:${TECHNISYS_HOME}/lib/*:${TECHNISYS_HOME}/entities/*:${TECHNISYS_HOME}/basicservices/libs/*:${TECHNISYS_HOME}/basicservices/*" net.technisys.cyberbank.core.engine.Core
