#!/bin/bash
artifact="view-file-migrator"
base_version="3.4.0"
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/view/packages'
file_name=$artifact-$base_version.tar.gz
curl -u $1:$2 -o $file_name $repository/$artifact/$base_version/$file_name
tar -xzvf $file_name
rm $file_name
