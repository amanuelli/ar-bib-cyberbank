# Changelog

## [3.4.0]

### Changed
- Initial version on starter kit [TECENG-105](SDLC-105-Agnostic pipelines for View-File-Migrator)

