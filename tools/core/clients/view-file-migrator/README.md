# View File Migrator

# Table of Contents

- [Project Overview](#markdown-header-project-overview)
  - [Technology stack](#markdown-header-technology-stack)
- [Getting Started](#markdown-header-getting-started)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Installation](#markdown-header-installation)
- [Configuration](#markdown-configuration-files)
    

# Project Overview

This tool generates a View Filesystem from a source database. To generates the filesystem, you must follow these steps:

## Technology stack

* Maven
* Java 8

#Getting Started

## Prerequisites
* Java 8

##Installation
### Client generation
Execute run.sh in a terminal the following commands to download artifacts from artifactory and generate client folder

Linux / MacOS

``./makeClient.sh <artifactory-user> <artifactory-password>``

Windows

``makeClient.BAT <artifactory-user> <artifactory-password>``

### Client execution
Once the client is generated, execute in a terminal the following commands

Linux / MacOS

``./run.sh``

Windows

``run.BAT ``
# Configuration
## configuration.properties
Property | Description|
---|---
url_db| source database to generate filesystem.
db_user| database user.
db_password| database password.
driver| JDBC database driver.
migrationdir| full path to the generated filesystem.
institution_id| use to generate menu.