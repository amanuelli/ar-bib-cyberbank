@echo off
set artifact=view-file-migrator
set base_version=3.4.0
set repository=https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/view/packages
set file_name=%artifact%-%base_version%.tar.gz
echo %repository%/%artifact%/%base_version%/%file_name%
curl -u %1:%2 -o %file_name% %repository%/%artifact%/%base_version%/%file_name%
tar -xzvf %file_name%
del %file_name%
