#!/bin/bash
artifact="environment-manager"
base_version="2.3.13"
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/view'
sha='3541a42da6304444eca4f937da5c64a1780a4b09'
file_name=$artifact-$base_version.tar.gz
curl -u $1:$2 -o $file_name $repository/$artifact/$base_version/$file_name
tar -xzvf $file_name
rm $file_name
