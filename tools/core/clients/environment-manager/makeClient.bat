@echo off
set artifact=environment-manager
set base_version=2.3.13
set repository=https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/view
set file_name=%artifact%-%base_version%.tar.gz
curl -u %1:%2 -o %file_name% %repository%/%artifact%/%base_version%/%file_name%
tar -xzvf %file_name%
del %file_name%
