# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [2.3.11]
### Changed
- Integrate validation process to Environment Manager [RCCP-1774](https://technisys.atlassian.net/browse/RCCP-1774)

## [2.3.10]
- Update release pipeline to Profile G 3.1.0 with multiple slack message. [SDLC-301](https://technisys.atlassian.net/browse/SDLC-301)
- When performing Layout passage, the translation of the related dictionary is not passed [RCCP-1182](https://technisys.atlassian.net/browse/RCCP-1182)
- Problem in the passage of actions with the Environment Manager [RCCP-43](https://technisys.atlassian.net/browse/RCCP-43)
- Syncronize labels between Dev and Regression [RCCP-1723](https://technisys.atlassian.net/browse/RCCP-1723)
- Add environment-manager to Starterkit [SDLC-23](https://technisys.atlassian.net/browse/SDLC-23)
