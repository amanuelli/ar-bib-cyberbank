# View Environment Manager

# Table of Contents

- [Project Overview](#markdown-header-project-overview)
  - [Technology stack](#markdown-header-technology-stack)
- [Getting Started](#markdown-header-getting-started)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Installation](#markdown-header-installation)
  - [Configuration Files](#markdown-configuration-files)
    - [ambienteBatch.ini](#markdown-header-ambienteBatch.ini)
    - [applicationContext.xml](#markdown-header-applicationContext.xml)
    - [Limits Configuration](#markdown-header-limits-configuration)
    - [Tracing Configuration](#markdown-header-tracing-configuration)
- [Documentation](#markdown-header-documentation)
    

# Project Overview


### Technology stack

* Maven
* Java 8

# Getting Started


## Prerequisites
* Java 8
* In the EnvironmentManager-3.2.11 version a new sequence called impact_history_seq was added. Here is an example.
```
CREATE SEQUENCE impact_history_seq
AS [ built_in_integer_type | user-defined_integer_type ]
START WITH <constant> 
INCREMENT BY { MINVALUE [<constant> ] };
GO
```


CREATE SEQUENCE impact_history_seq
  AS [ built_in_integer_type | user-defined_integer_type ]
  START WITH <constant>
  INCREMENT BY { MINVALUE [<constant> ] };
  GO


## Client generation
Execute run.sh in a terminal the following commands to download artifacts from artifactory and generate client folder

Linux / MacOS

``./makeClient.sh <artifactory-user> <artifactory-password>``

Windows

``makeClient.BAT <artifactory-user> <artifactory-password>``

## Client execution
Once the client is generated, execute in a terminal the following commands

Linux / MacOS

``run.sh``

Windows

``run.BAT``

## Configuration Files
In Cyberbank View Environment Manager are two configuration files. 


- applicationContextEM.xml 

  This file contains all the data about the connections, between what environments are deployed, and what environments are available. Each config is done with beans. 

   - **CONNECTION BEANS**

    Attribute | Description
    --- | ---
    connectionString | JDBC connection string to the database (format jdbc:subprotocol:subname) |
    username | database connection username |
    password | database connection password |
    defaultSchema | database default schema |
    description | brief description of connection |
    driverName | JDBC driver |
    databaseType | database engine |
    hibernateDialect | hibernate dialect |  
    
  
   - **SCRIPT GENERATION BEANS**

    Attribute | Description
    --- | ---
    commandTerminator | character (one or multiples) which ends an sql commands
    fileEncoding | file encoding
    

   - **RELOAD COMPONENTS BEANS**

    Attribute | Description
    --- | ---
    hostName | Hostname or IP of server
    port | Port of server listening to reload requests
    context | Web client context
    

   - **ENVIRONMENT CONFIGURATION BEANS**
      
    Attribute | Description
    --- | ---
    sourceConnection | reference to a ConnectionBean which will be used as the source database. 
    destinationConnection | reference to a ConnectionBean which will be used as the destination database.
    reportingConnection | reference to a ConnectionBean which will be used as the database for marked and past elements.
    description | environment description. It is shown on the environment list of the login screen. 
    migrateEntities | boolean to indicate if a migration of entities definition bewtween environments can be performed. 
    sourceInstitutionId | ID of source institution.
    targetInstitutionId | ID of destination institution. 
    exportBySqlOnly | boolean to indicate if the export is done only by scripts.
    initialState | character to indicate the minimmum initial state of a component, to be analyzed and migrated. Options are: E (between development and testing), B (between testing and pre-production) and G (between pre-production and production)
    scriptGeneration | reference to ScriptGenerationBean bean to indicate how to generate the script
    reloadComponentContext | reference to ReloadComponentBean bean to indicate the context where the view components are reloaded
    

   - **ENVIRONMENT BEAN**

    Attribute | Description
    --- | ---
    environments | list of references of configured environments
    defaultEnvironment | default environment (pre-selected on environment list)
    testingEnvironment | environment used for JUnit testing
    administrationConnection | reference to ConnectionBean where translations are retrieve and the user is authenticated
    language | application language (Supported:  es,en,pt)
    


   - **USER PROFILE BEANS**

    Attribute | Description
    --- | ---
    value | user´s profile. There are two profiles that can be set, user (Only enables the tab Impact Analyzer. This profile cannot make changes to the targer database nor generate scripts) or full (Enables both tabs, View Manager y Impact Analyzer. This profile has permissions to impact changes to the target database, generate scripts and mark elements for a passage). 
    

   - **DEBUGMODE BEAN**

    Attribute | Description
    --- | ---
    value | boolean to enable debugmode
    


- __logback.xml__ 


# Documentation

All the documentation for the microservice can be found in this [Confluence.](https://technisys.atlassian.net/wiki/spaces/TC/pages/457480836/Cyberbank+View+Environment+Manager)

