# XML OperationUtilities

## Description
Project for handling operations metadata and files

## Properties

File **conf/hibernate.cfg.xml**

Properties       | Description
-----------------| -----------
connection.driver_class | Database driver, ex: com.microsoft.sqlserver.jdbc.SQLServerDriver for SQLServer 
connection.url          | JDBC string connection
connection.username     | Database username
connection.password     | Database password
dialect                 | Database dialect, ex: org.hibernate.dialect.SQLServer2012Dialect for SQLServer

## How run client?

### Linux / MacOS

#### Download client

`./makeClient.sh <artifactory-user> <artifactory-password> `

#### Run client

`./json.sh ` to generate CPG operations

`./errors.sh ` to generate CPG errors

### Windows

#### Download client

`.\makeClient.bat <artifactory-user> <artifactory-password> `

#### Run client

`./json.bat ` to generate CPG operations

`./errors.bat ` to generate CPG errors


**Note**: Errors and operations are stored in migration/error and migration/operation.

## Documentation

Ref: [Doc]()