# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).


## [3.1.6]
### Changed
- Fixed missing id on error log [TECDIGSK-738](https://technisys.atlassian.net/browse/TECDIGSK-738)
- Update release pipeline to Profile G 3.1.0 with multiple slack message. [SDLC-301](https://technisys.atlassian.net/browse/SDLC-301)

## [3.1.5]
### Changed
- Updated vulnerable dependencies [RCCP-1956](https://technisys.atlassian.net/browse/RCCP-1956)
- Initial version on starter kit [SDLC-104](https://technisys.atlassian.net/browse/SDLC-104)