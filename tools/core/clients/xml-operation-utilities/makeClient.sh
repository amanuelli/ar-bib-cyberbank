######## Download Client ########
curl_opts="-u $1:$2"
artifact='xml-operation-utilities'
base_version='3.1.6'
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core'
sha1_version='638f17a14c4d1f400ad46a2f41934e160fde5380'
file_name=$artifact-$base_version.tar.gz
curl $curl_opts -o $file_name $repository/$artifact/$base_version/$file_name
sha1sum $file_name | grep $sha1_version
tar -xzvf $file_name
rm $file_name
#################################