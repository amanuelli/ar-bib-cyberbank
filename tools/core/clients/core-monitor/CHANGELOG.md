# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [3.4.2]  2021-11-19
### Changed
- Fixed bug, show more info in the tree for heavy process. [TECDIGSK-730] (https://technisys.atlassian.net/browse/TECDIGSK-730)

## [3.4.1] 2021-11-02
### Changed
- Modified the format of the detail list view items to improve readability. [RCCP-1771] (https://technisys.atlassian.net/browse/RCCP-1771)
- Modified highlighting for search results and selected row in detail list view. [RCCP-1769] (https://technisys.atlassian.net/browse/RCCP-1769)


## [3.4.0] 2021-10-29
### Changed
- Integrate Kafka to CORE Monitor [RCCP-1435] (https://technisys.atlassian.net/browse/RCCP-1435)


### [3.3.9] 2021-10-06
#### Changed
- Search button fix in CoreMonitor [RCCP-2012] (https://technisys.atlassian.net/browse/RCCP-2012)
- Update release pipeline to Profile G 3.1.0 with multiple slack message. [SDLC-301](https://technisys.atlassian.net/browse/SDLC-301)


### [3.3.8] 2021-09-01
#### Changed
- Improve Core Monitor Dependency Check metrics [RCCP-1945] (https://technisys.atlassian.net/browse/RCCP-1945)
- Improve Core Monitor Sonar metrics [RCCP-1944] (https://technisys.atlassian.net/browse/RCCP-1944)
- Improve Core Monitor Snyk metrics [RCCP-1964] (https://technisys.atlassian.net/browse/RCCP-1964)

### [3.3.7] 2021-06-09
- Add core-monitor to Starterkit [RCCP-1576](https://technisys.atlassian.net/browse/RCCP-1576)