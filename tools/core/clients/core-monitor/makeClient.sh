######## Download Client ########
curl_opts="-u $1:$2"
artifact='core-monitor'
base_version='3.4.2'
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core'
sha1_version='095c4940204d84f70902c399a7feb3bb00f214aa'
file_name=$artifact-$base_version.tar.gz
curl $curl_opts -o $file_name $repository/$artifact/$base_version/$file_name
sha1sum $file_name | grep $sha1_version
tar -xzvf $file_name
rm $file_name
#################################