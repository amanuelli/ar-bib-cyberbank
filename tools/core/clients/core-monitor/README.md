# Core Monitor

Use for Core debbuging

# Prerequisites
As of version 3.4.0 or higher, the core-monitor can be used with kafka.

Depending on the configuration you want, configure the logging.listener.type property in monitor.properties:

Properties | Description
-----------| -----------
logging.listener.type | KAFKA, ELASTIC, SOCKET, Default:SOCKET

## Properties
File applicationContext.xml
To configure a new environment it must add a new entry in elasticsearchConnections map (`<entry key = "VISIBLE_APPLICATION_NAME" value-ref = "BEAN_NAME" />` ) and define a new bean with the following properties

Properties | Description
-----------| -----------
url        | Elasticsearch url. Example: 172.64.4.5
port       | Elasticsearch port. Default: 9200
user       | Elasticsearch user. 
password   | Elasticsearch user password. 
indexName  | Elasticsearch index pattern. Example: tec-core-3-5-2020*

Kafka:
A new entry must be added in kafka-connections map (`<entry key = "VISIBLE_APPLICATION_NAME" value-ref = "BEAN_NAME_KAFKA" />` ) and define a new bean with the following properties.

Properties | Description
-----------| -----------
Topic                    | Kafka topic. Example: core-monitor-topic
bootstrapServers         | Kafka bootstrapServers. Example: IPKAFKA_CLUSTER:9092
groupId                  | Kafka groupId. Example: core-monitor
noLogsTimeoutInSeconds   | Kafka noLogsTimeoutInSeconds password. Example :1800


## How run client?

### Linux / MacOS

#### Download client

`./makeClient.sh <artifactory-user> <artifactory-password> `

#### Run client

`./run.sh`

### Windows

#### Download client

`.\makeClient.bat <artifactory-user> <artifactory-password> `

#### Run client

`.\run.bat`

## Documentation

Ref: [Doc](https://technisys.atlassian.net/wiki/spaces/TC/pages/1057914884/Cyberbank+Core+Monitor)