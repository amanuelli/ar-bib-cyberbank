# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

### Added
### Changed
### Removed


## [1.13.9] 
- Added view-config-client to Starterkit [TECENG-1445](https://https://technisys.atlassian.net/browse/TECENG-1445) - 