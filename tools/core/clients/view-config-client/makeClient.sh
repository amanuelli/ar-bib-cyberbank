######## Download Client ########
curl_opts="-u $1:$2"
artifact='view-config-client'
version='1.13.9'
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/view/product/config'
sha1_version='1924584fde42407c83623d5cda86805d8bf548dc'
file_name=$artifact-$version.tar.gz
curl $curl_opts -o $file_name $repository/$artifact/$version/$file_name
sha1sum $file_name | grep $sha1_version
tar -xzvf $file_name
rm $artifact-$version.tar.gz
#################################