# CPGApiServer Tool

## Properties

File RESTConfig.properties

Properties       | Description
-----------------| -----------
rest.server.host | View Config Server host 
rest.server.port | View Config Server port

File viewconfig.properties

Properties       | Description
---|---
host | Core Backoffice host
port | Core Backoffice port
webAppContext | Core Backoffice application context
JIRA_URL_CONN | API URL of Jira API
JIRA_URL_SEARCH | Project in Jira
JIRA_USER_PASS | Jira credentials. Format: "user:pass"


## How run client?

### Linux / MacOS

#### Download client

`./makeClient.sh <artifactory-user> <artifactory-password> `

#### Run client

`./run.sh`

### Windows

#### Download client

`.\makeClient.bat <artifactory-user> <artifactory-password> `

#### Run client

`.\run.bat`

## Documentation

Ref: [Doc](https://technisys.atlassian.net/wiki/spaces/TC/pages/487428883/Cyberbank+View+Config)
