:: ######## Download Client ########
set curl_opts=-u %1:%2
set artifact=view-config-client
set base_version=1.13.9
set version=
set repository=https://artifactory.technisys.com/artifactory/list/libs-release-local/net/technisys/cyberbank/view/product/config
set artifact_version=
set file_name=%artifact%-%base_version%%artifact_version%.tar.gz
curl %curl_opts% -o %file_name% %repository%/%artifact%/%base_version%%version%/%file_name%
tar -xf %file_name%
del %file_name%
:: ####################################