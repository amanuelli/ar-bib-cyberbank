:: ######## Download Client ########
set curl_opts=-u %1:%2
set artifact=core-entity-generator
set base_version=3.0.6
set repository=https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core
set file_name=%artifact%-%base_version%.tar.gz
curl %curl_opts% -o %file_name% %repository%/%artifact%/%base_version%%version%/%file_name%
tar -xf %file_name%
del %file_name%
:: ####################################