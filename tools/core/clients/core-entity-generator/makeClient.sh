######## Download Client ########
curl_opts="-u $1:$2"
artifact='core-entity-generator'
base_version='3.0.6'
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core'
sha1_version='0caaca155dcdb98118db9c52667f48b69ed04d72'
file_name=$artifact-$base_version.tar.gz
curl $curl_opts -o $file_name $repository/$artifact/$base_version/$file_name
sha1sum $file_name | grep $sha1_version
tar -xzvf $file_name
rm $file_name
#################################