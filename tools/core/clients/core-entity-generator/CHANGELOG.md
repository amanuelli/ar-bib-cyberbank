# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

### Added

### Changed

### Fixed

[3.0.6]
- Add core-entity-generator to Starterkit [SDLC-79](https://technisys.atlassian.net/browse/SDLC-79)