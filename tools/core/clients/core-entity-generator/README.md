# Core Entity Generator

## How run client?

### Linux / MacOS

#### Download client

`./makeClient.sh <artifactory-user> <artifactory-password> `

#### Run client

`./run.sh`

### Windows

#### Download client

`.\makeClient.bat <artifactory-user> <artifactory-password> `

#### Run client

`.\run.bat`

