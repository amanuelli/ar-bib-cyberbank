# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [3.4.6]
#### Changed
- Fix on promotion for services without spanish description or goal. [SDLC-728](https://technisys.atlassian.net/browse/SDLC-728)
- Fix on merge errors.[SDLC-732](https://technisys.atlassian.net/browse/SDLC-732)

## [3.4.5]
#### Changed
- Error when the service does not exist on the destination. [RCCP-2053](https://technisys.atlassian.net/browse/RCCP-2053)

## [3.4.4]
#### Changed
- Integrate validation process for the flow of 'from input key'. [RCCP-2043](https://technisys.atlassian.net/browse/RCCP-2043)

## [3.4.3]
#### Changed
- Integrate validation process to CPG Merger.For the flow of 'from issue'. [RCCP-1810](https://technisys.atlassian.net/browse/RCCP-1810)
- Vulnerabilities with Snyk for CpgMerger and XmlOperationUtilities. [RCCP-1997](https://technisys.atlassian.net/browse/RCCP-1997)
- Update release pipeline to Profile G 3.1.0 with multiple slack message. [SDLC-301](https://technisys.atlassian.net/browse/SDLC-301)

## [3.4.2]
#### Added
- Add cpg-merger to Starterkit [RCCP-1578](https://technisys.atlassian.net/browse/RCCP-1578)
