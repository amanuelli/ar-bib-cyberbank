# CPG Merger

Cpg Merger is an application that allows you to migrate operations configured with Cyberbank Cpg.

## Properties

* **applicationContext.xml**: this xml is the skeleton of the configuration. Here reference is made to the database definition files (cpg_connections and view_connections.xml). The implementation of the Merger to be used and the MergerLogger are defined by AOP, to log before and after each merge.
* **cpg_connections.xml and view_connections.xml**: the connections to the cpg and view databases are defined respectively.
* **hibernate_xxx.xml**: these files are own of hibernate, they are used to add configuration of the framework. This configuration is complemented by the configuration made in cpg_connections and view_connections.
* **git.xml**: within this file the git properties are defined. To specify the protocol to be used, the gitUtils bean must be configured with the bean corresponding to the desired protocol.

Ref: For log4jdbc, see [log4jdbc](tools/core/clients/cpg-merger/docs)

## How run client?

### Linux / MacOS

#### Download client

`./makeClient.sh <artifactory-user> <artifactory-password> `

#### Run client

`./run.sh`

### Windows

#### Download client

`.\makeClient.bat <artifactory-user> <artifactory-password> `

#### Run client

`.\run.bat`

## Documentation

Ref: [Doc](https://technisys.atlassian.net/wiki/spaces/TC/pages/457482681/Cyberbank+Core+CPG+Merger+User+s+Manual+Configuration)