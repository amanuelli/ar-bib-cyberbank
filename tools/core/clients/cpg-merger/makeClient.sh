######## Download Client ########
curl_opts="-u $1:$2"
artifact='cpg-merger'
base_version='3.4.5'
version=''
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core'
sha1_version='dc813936a72df2950898154b4f045eedf259e62e'
artifact_version=''
file_name=$artifact-$base_version$artifact_version.tar.gz
curl $curl_opts -o $file_name $repository/$artifact/$base_version$version/$file_name
sha1sum $file_name | grep $sha1_version
tar -xzvf $file_name
#################################
