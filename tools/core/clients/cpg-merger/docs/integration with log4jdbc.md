Log4jdbc allows you to log the statements that the jdbc executes.
A log is generated, which then the CpgMerger, can interpret and extract the sql statements.

* The log4jdbc4-1.2.jar library must be used

* JVM parameters:
  As a JVM parameter, the following line must be added to the sh/bat of the merger:
  * **-Dlog4jdbc.debug.stack.prefix=net.technisys.cyberbank.core.cpg_merger** This makes it log in to only that package
   
  * PosgresSql: **-Dlog4jdbc.dump.booleanastruefalse** This does
  logs the boolean values ​​as true and false
  (default 1 and 0)
  	                   
  * DB2: **-Dlog4jdbc.drivers=com.ibm.db2.jcc.DB2Driver**
  
* In hibernate_source.cfg.xml or hibernate_destination_cfg.xml the driver must be replaced by the following line:
  ```
  <property name="connection.driver_class">net.sf.log4jdbc.DriverSpy</property>
  ```
  So log4jdcb, stands between hibernate and jdbc.

* The database connection url must be prepended with the prefix jdbc:log4
	Example: "jdbc:log4jdbc:postgresql://x.x.x.x/cyberbank_cpg_demo" 
	
* In the log4j.properties file add the following lines: 
  ```
    log4j.logger.jdbc.sqlonly=DEBUG
    log4j.logger.org.hibernate.SQL=ERROR
    log4j.logger.org.hibernate.type=ERROR
  ```
 
 The logs remain in the merger.log (default cpg merger log file).
 Inside the merger, in menu **Tools -> generate script from log -> generate script from file** the executed sql statements are obtained(insert, update and delete)
  	    
 **Important**:
  ```
	<property name="show_sql">false</property>
	<property name="hibernate.format_sql">false</property>
  ```
Always set these properties to false in the hibernate.cfg.xml files
if not the log parser, it will have an undefined behavior.  
 


