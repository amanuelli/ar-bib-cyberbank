:: ######## Download Client ########
set curl_opts=-u %1:%2
set artifact=cpg-merger
set base_version=3.4.5
set version=
set repository=https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core
set artifact_version=
set file_name=%artifact%-%base_version%%artifact_version%.tar.gz
curl %curl_opts% -o %file_name% %repository%/%artifact%/%base_version%%version%/%file_name%
tar -xf %file_name%
::#################################
