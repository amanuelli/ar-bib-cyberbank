# Core Task Executor Client

# Table of Contents

- [Project Overview](#markdown-header-project-overview)
  - [Technology stack](#markdown-header-technology-stack)
- [Getting Started](#markdown-header-getting-started)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Installation](#markdown-header-installation)
  - [Configuration Files](#markdown-configuration-files)
    - [ambienteBatch.ini](#markdown-header-ambienteBatch.ini)
    - [applicationContext.xml](#markdown-header-applicationContext.xml)
    - [Limits Configuration](#markdown-header-limits-configuration)
    - [Tracing Configuration](#markdown-header-tracing-configuration)
- [Documentation](#markdown-header-documentation)
    

# Project Overview

Cyberbank Core Task Executor is a powerful command-line tool designed to execute batch processes of Cyberbank Core Engine in concurrent machines. This functionality allows the system to parallelize the execution of these batch processes in multiple portions. An example is the batch process which updates the balances and generates the statistics of an account.


### Technology stack

* Maven
* Java 8

# Getting Started


## Prerequisites
* Java 8


## Client generation
Execute run.sh in a terminal the following commands to download artifacts from artifactory and generate client folder

Linux / MacOS

``./makeClient.sh <artifactory-user> <artifactory-password>``

Windows

``makeClient.BAT <artifactory-user> <artifactory-password>``

## Client execution
Once the client is generated, execute in a terminal the following commands

Linux / MacOS

``./run.sh <core_service_name> <core_service_project> <core_user>``

Windows

``run.BAT <core_service_name> <core_service_project> <core_user>``

## Configuration Files
In Cyberbank Core Task Executor are two configuration files. One of them related to the server where the processes will be executed. The other is related to basic log in information.

### ambienteBatch.ini
File with configuration and data of the server where the pre batch tasks will be executed

Property | Description|
---|---
ambiente.(class)  | net.technisys.cyberbank.core.batch.Configuracion
ambiente.puertoEngine | Engine port defined to execute batch process
ambiente.timeRefresh | Refresh time in milliseconds
ambiente.puertoBatchEngine | Engine port defined to execute batch process
ambiente.hostCore | Engine host defined to execute batch process
ambiente.logSerializerPath | temp/
ambiente.servicePath | ambiente.servicePath where the Engine's path for executing these service is set (eg: /engine/services/request)
ambiente.retry | used to attempt reconnections against the Engine in case the connection is cut
ambiente.logTopRows | 20
ambiente.logSerializerPath | temp/
ambiente.logTimeOut | 45000
ambiente.errorCodeOriginal | false
ambiente.processCheckInterval | Check interval in case of losing connection with the core in milliseconds (Default: 15000)
ambiente.httpTimeOut | timeout for http requests in milliseconds (Default: 900000)

### applicationContext.xml 
File with configuration and data related to basic information to log in and execution of batch processes

Variable | Description
 --- | ---
channeldId | Channel id used by the process
branchId | Branch used by the process
institutionId | Insititution used by the process

# Documentation

All the documentation for the microservice can be found in this [Confluence.](https://technisys.atlassian.net/wiki/spaces/TC/pages/457482437/Cyberbank+Core+Task+Executor)

