#!/bin/bash
artifact="core-task-executor"
base_version="3.3.2"
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core'
sha1_version = '287975d5bf29295efe4a9acb485fa9f51a1f45d0'
file_name=$artifact-$base_version.tar.gz
curl -u $1:$2 -o $file_name $repository/$artifact/$base_version/$file_name
sha1sum $file_name | grep $sha1_version
tar -xzvf $file_name
rm $file_name
