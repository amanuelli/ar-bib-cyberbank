# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [3.3.2] 2021-11-19
#### Changed
- Version with several improvements is released [TECDIGSK-728](https://technisys.atlassian.net/browse/TECDIGSK-728)
- Update release pipeline to Profile G 3.1.0 with multiple slack message. [SDLC-301](https://technisys.atlassian.net/browse/SDLC-301)

## [3.3.1]
#### Changed
- Improve core task executor Synk metrics [RCCP-1953](https://technisys.atlassian.net/browse/RCCP-1953)
- Improve core task executor sonar and dependency check metrics [RCCP-1949](https://technisys.atlassian.net/browse/RCCP-1949)


## [3.3.0]
#### Changed
- Initial version on starter kit [TECENG-1402](https://technisys.atlassian.net/browse/TECENG-1402)

