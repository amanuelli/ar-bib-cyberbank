# View Monitor

Use for View debbuging

## Properties
File RESTConfig.properties

Properties       | Description
-----------------| -----------
rest.server.host | View Config Server host 
rest.server.port | View Config Server port

File monitor.view.properties

Properties      | Description
--------------- | -----------
LANGUAGE        |  Application language
serverHost      |  Core Backoffice Server host
serverPort      |  Core Backoffice Server monitor port. (Default: 8888)
IS_DEBUG        |  Flag for debbuging.
IS_FILESYSTEM   |  Flag for filesystem mode
IS_JSON_MESSAGE |  Flag for json mode. If core use json messaging, it must be enables. For XML messaging it must be disabled.
MAX_XML_ROWS    |  max of xml rows
MAX_WFL_ROWS    |  max of workflow rows

File monitor.view.applicationContext.xml

Bean                        | Description
--------------------------- | -----------
ControllerRESTFactory       | View server REST
ControllerFileSystemFactory | View on Filesystem

## How run client?

### Linux / MacOS

#### Download client

`./makeClient.sh <artifactory-user> <artifactory-password> `

#### Run client

`./run.sh`

### Windows

#### Download client

`.\makeClient.bat <artifactory-user> <artifactory-password> `

#### Run client

`.\run.bat`

