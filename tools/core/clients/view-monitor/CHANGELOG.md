# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

### Added

### Changed

### Fixed

## [2.2.10]

### Added
- Add core-monitor to Starterkit [RCCP-1576](https://technisys.atlassian.net/browse/RCCP-1576)

## [2.2.9]

### Changed
- ViewMonitor con filesystem [RCCP-982](https://technisys.atlassian.net/browse/RCCP-982)
