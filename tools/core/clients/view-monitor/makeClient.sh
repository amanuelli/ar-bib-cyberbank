######## Download Client ########
curl_opts="-u $1:$2"
artifact='view-monitor'
base_version='2.2.10'
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/view'
sha1_version='158a4204361b3c5a5d1d6c1831921e7e1a5f64c3'
file_name=$artifact-$base_version.tar.gz
curl $curl_opts -o $file_name $repository/$artifact/$base_version/$file_name
sha1sum $file_name | grep $sha1_version
tar -xzvf $file_name
rm $file_name
#################################