# Core Metadata extraction

Initial backup of Core metadata services:

* Bussiness medatada
* CPG metadata
* View metadata

## How to restore a backup?

### Linux / MacOS

#### Download files Linux

`./getDatabases.sh <artifactory-user> <artifactory-password>`

### Windows

#### Download files Windows

`.\getDatabases.bat <artifactory-user> <artifactory-password>`

### Restore db from bacpac file

#### With sqlpackage

Link for download binary and documentation: [link](https://docs.microsoft.com/en-us/sql/tools/sqlpackage/sqlpackage?view=sql-server-ver15)

```bash
./sqlpackage /a:Import /tsn:tcp:<SQL_SERVER_HOST> /tdn:<NAME_CORE_DB> /tu:<DBA_USER> /tp:<DBA_PASS> /sf:core.bacpac

./sqlpackage /a:Import /tsn:tcp:<SQL_SERVER_HOST> /tdn:<NAME_CPG_DB> /tu:<DBA_USER> /tp:<DBA_PASS> /sf:cpg.bacpac

./sqlpackage /a:Import /tsn:tcp:<SQL_SERVER_HOST> /tdn:<NAME_VIEW_DB> /tu:<DBA_USER> /tp:<DBA_PASS> /sf:view.bacpac
```

#### With Azure cli

**Pre-condition**: Databases already created in Azure

Upload bacpac files to a storage account

```bash
az storage blob upload --account-name <ACCOUNT_NAME> --account-key <STORAGE_KEY> --container-name <CONTAINER_NAME> --file core.bacpac --name core.bacpac --verbose 

az storage blob upload --account-name <ACCOUNT_NAME> --account-key <STORAGE_KEY> --container-name <CONTAINER_NAME> --file cpg.bacpac --name cpg.bacpac --verbose 

az storage blob upload --account-name <ACCOUNT_NAME> --account-key <STORAGE_KEY> --container-name <CONTAINER_NAME> --file view.bacpac --name view.bacpac --verbose 
```

```bash
az sql db import -s <SQL_SERVER_HOST> -n <NAME_CORE_DB> -g <SQL_RESOURCE_GROUP> -u <DBA_USER> -p <DBA_PASS> --storage-key <STORAGE_KEY> --storage-key-type StorageAccessKey --storage-uri <bacpacLocation> --verbose

az sql db import -s <SQL_SERVER_HOST> -n <NAME_CPG_DB> -g <SQL_RESOURCE_GROUP> -u <DBA_USER> -p <DBA_PASS> --storage-key <STORAGE_KEY> --storage-key-type StorageAccessKey --storage-uri <bacpacLocation> --verbose

az sql db import -s <SQL_SERVER_HOST> -n <NAME_VIEW_DB> -g <SQL_RESOURCE_GROUP> -u <DBA_USER> -p <DBA_PASS> --storage-key <STORAGE_KEY> --storage-key-type StorageAccessKey --storage-uri <bacpacLocation> --verbose

```

**Nota**: Format bacpacLocation: `https://<ACCOUNT_NAME>.blob.core.windows.net/<CONTAINER_NAME>/core.bacpac`

### Defaults admin users for tools

Tool | User/Pass
---- | ---------
CPG  | technisys/technisys
CoreBackoffice | user/1020user
View | technisys/technisys

## How to restore Entities?

### Download and Generate Entities Linux

`./getEntities.sh <artifactory-user> <artifactory-password>`

### Download and Generate Entities Windows

`.\getEntities.bat <artifactory-user> <artifactory-password>`

### Generate entities from another database

:warning: **TO USE THE NEXT SCRIPTS**:

Ensure that you download Core Entity Generator from initializr because the next scripts asume that you have the next structure:

```bash
.
└── core
    ├── clients
    │   ├── core-entity-generator
    │   │   └── core-entity-generator files ...
    ├── core-metadata
        └── core-metadata files ...
```

If you need to generate new persistent entities using your own databasee you could use the code located in **Core-Entities-Persistent** following [this instructions.](./Core-Entities-Persistent/README.md)

To reflect the new persistent entities in the view base, run core-entity-generator tool:
[Follow instructions](../clients/core-entity-generator/README.md)

### Upload Entities

You can upload this files manually to the location where the entities are needed. If your entities exists inside Azure Files you could use the next az commands to upload them.

```bash
echo "## Upload Entities ##"
az storage file upload-batch --destination $(SHARE_NAME) --account-name $(STORAGEACCOUNT_NAME) --account-key ${STORAGEACCOUNT_KEY} --destination-path core/entities --source ./entities --pattern '*.jar'
```

Upload custom entities persistent
```bash
echo "## Upload Custom Entities ##"
az storage file upload-batch --destination $(SHARE_NAME) --account-name $(STORAGEACCOUNT_NAME) --account-key ${STORAGEACCOUNT_KEY} --destination-path core/entities --source Core-Entities-Persistent/target --pattern 'core-entities-persistent*.jar'
```

Variable | Definition
----     | ---------
STORAGEACCOUNT_NAME | Azure StorageAccount Name to use
STORAGEACCOUNT_KEY  | Secret key to upload files to the Storage Account
SHARE_NAME | Name of the share name used in the storage account

**Note**: The commands assume that the source files to upload are located where the command is executed (source=.)

## How to restore Primitives?

### Download Primitives Linux

`./getPrimitives.sh <artifactory-user> <artifactory-password>`

### Download Primitives Windows

`.\getPrimitives.bat <artifactory-user> <artifactory-password>`

You can upload this files manually to the location where the primitives are needed. If your primitives exists inside Azure Files you could use the next az commands to upload them.

```bash
echo "## Upload Primitives ##"
az storage file upload-batch --destination $(SHARE_NAME) --account-name $(STORAGEACCOUNT_NAME) --account-key ${STORAGEACCOUNT_KEY} --destination-path core/basicservices --source ./primitives --pattern 'b*-?.?.?.jar'
az storage file upload-batch --destination $(SHARE_NAME) --account-name $(STORAGEACCOUNT_NAME) --account-key ${STORAGEACCOUNT_KEY} --destination-path core/basicservices/libs --source ./primitives/lib --pattern '*.jar'
```

Variable | Definition
----     | ---------
STORAGEACCOUNT_NAME | Azure StorageAccount Name to use
STORAGEACCOUNT_KEY  | Secret key to upload files to the Storage Account
SHARE_NAME | Name of the share name used in the storage account

**Note**: The commands assume that the source files to upload are located where the command is executed (source=.)


## Observations

### Upgrade from version 3.4.1.x to 3.4.2 
Backward compatibility is ensured with the core technical components (CPG, Engine, CPG Merger, Core Task Executor, Core Entities Generator, XML Operation Utilities, Core Delivery & Core Delivery Client).
Retrocopatibility is not ensured in of top levels and orchestrated services towards components that consume them.
Note:  This component don’t follow Product’s Version Management Policy.