######## Download core-metadata ########
curl_opts="-u $1:$2"
artifact='core-metadata'
base_version=3.4.2.1
repository='https://artifactory.technisys.com/artifactory/list/libs-release/com/technisys/cyberbank/platform/starterkit'
sha1_version=2b18a7bae329c7fa3c31c2fcfc7e2fe6b938774e
file_name=$artifact-$base_version.tar.gz
curl $curl_opts -o $file_name $repository/$artifact/$base_version/$file_name
sha1sum $file_name | grep $sha1_version
tar -xzvf $file_name
rm $file_name
#################################