:: ######## Parameters ########
set curl_opts=-u %1:%2
set repository=https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core

:: ######## Version ########

set artifactCE=core-entities
set coreEntities_version=3.5.1

set artifactCEP=core-entities-persistent
set coreEntitiesPersistent_version=3.5.1

:: ## Download Entities ##
md entities

set file_name=%artifactCE%-%coreEntities_version%.tar.gz
curl %curl_opts% -o entities/%file_name% %repository%/%artifactCE%/%coreEntities_version%/%file_name%

set file_name=%artifactCEP%-%coreEntitiesPersistent_version%.tar.gz
curl %curl_opts% -o entities/%file_name% %repository%/%artifactCEP%/%coreEntitiesPersistent_version%/%file_name%

:: ####################################