trigger: none

pr: none

parameters:
- name: environment
  displayName: Environment
  type: string
  default: QA-Functional
  values:
  - QA-Functional
  - QA-Functional-Batch

pool:
  name: Azure Pipelines
  vmImage: 'Ubuntu-18.04'
  timeoutInMinutes: 55

variables:
  - group: CP-CORE-${{ parameters.environment }}
  - group: SLACK
  - group: ARTIFACTORY

resources:
  containers:
    - container: cicd_templates
      image: 'tecdigitalacr.azurecr.io/utils/invoke-cicd-templates:1.17.0'
      endpoint: 'tecdigitalacr - Container Registry'
      options: -e WORKDIR=/__w/1/tasks.py

stages:
- stage: Backup
  displayName: "Backup Databases"
  jobs:
      - job: Backup
        displayName: "Backup databases"
        steps:

      ### Backup databases ###

        - bash: |
            mkdir backups
            sqlpackage /a:Export /ssn:$(DB_SERVER_URL) /su:$(VIEW_DB_USER) /sp:$(VIEW_DB_PASSWD) /sdn:$(VIEW_DB_NAME) /tf:backups/view.bacpac         
            sqlpackage /a:Export /ssn:$(DB_SERVER_URL) /su:$(CPG_DB_USER) /sp:$(CPG_DB_PASSWD) /sdn:$(CPG_DB_NAME) /tf:backups/cpg.bacpac         
            sqlpackage /a:Export /ssn:$(DB_SERVER_URL) /su:$(ENGINE_DB_USER) /sp:$(ENGINE_DB_PASSWD) /sdn:$(ENGINE_DB_NAME) /tf:backups/core.bacpac         
          displayName: 'Create db backup using sqlpackage'
 
        - publish: $(System.DefaultWorkingDirectory)/backups
          artifact: backups
          displayName: 'Publish Artifact: dbs bacpac'
          condition: succeeded()

      ### Extract variables for release ###

        - task: DownloadSecureFile@1
          displayName: "Download Maven settings.xml"
          name: settings
          inputs:
            secureFile: 'MAVEN_SETTINGS'

        - bash: |
            python3 -m invoke common.export-variables-azure-devops
          displayName: 'Define Variables'
          env:
            POM_FILE: tools/core/core-metadata/pom.xml
            MVN_SETTINGS: $(settings.secureFilePath)
            EXPORT_VARIABLES: "PROJECT_VERSION PROJECT_ARTIFACT_ID GIT_COMMIT_SHORT"
          target:
            container: cicd_templates 

        - bash: |
            mkdir -p $(Pipeline.Workspace)/variables
            echo "Using $PROJECT_ARTIFACT_ID as microservice name..."
            echo "$PROJECT_ARTIFACT_ID" > $(Pipeline.Workspace)/variables/MS_NAME
            echo "Using $PROJECT_VERSION as microservice version..."
            echo "$PROJECT_VERSION" > $(Pipeline.Workspace)/variables/MS_VERSION
            echo "Using $GIT_COMMIT_SHORT as microservice last commit..."
            echo "$GIT_COMMIT_SHORT" > $(Pipeline.Workspace)/variables/MS_LASTCOMMIT
          displayName: 'Show shared variables'
          failOnStderr: true

        - publish: $(Pipeline.Workspace)/variables
          artifact: variables
          displayName: 'Publish shared variables'

        ### Get Entities files ###

      - job: Entities
        displayName: "Getting Entities"
        steps:
          - bash: |
              cd $(System.DefaultWorkingDirectory) && /bin/bash $(System.DefaultWorkingDirectory)/tools/core/core-metadata/getEntities.sh "${ARTIFACTORY_USER}" "${ARTIFACTORY_PASSWORD}"
            env:
              ARTIFACTORY_USER: $(ARTIFACTORY_USER)
              ARTIFACTORY_PASSWORD: $(ARTIFACTORY_PASSWORD)
            displayName: 'Download Entities from Artifactory'
            failOnStderr: false

          - publish: $(System.DefaultWorkingDirectory)/entities
            artifact: entities
            displayName: 'Publish Artifact: Entities'
        ### Get primitives files ###

      - job: Primitives
        displayName: "Getting Primitives"
        steps:
          - bash: |
              cd $(System.DefaultWorkingDirectory) && /bin/bash $(System.DefaultWorkingDirectory)/tools/core/core-metadata/getPrimitives.sh "${ARTIFACTORY_USER}" "${ARTIFACTORY_PASSWORD}"
            env:
              ARTIFACTORY_USER: $(ARTIFACTORY_USER)
              ARTIFACTORY_PASSWORD: $(ARTIFACTORY_PASSWORD)
            displayName: 'Download Primitives from Artifactory'
            failOnStderr: false

          - publish: $(System.DefaultWorkingDirectory)/primitives
            artifact: primitives
            displayName: 'Publish Artifact: Primitives'
