# Generate Core Entities Persistent

To generate Peresistent Entities using your own database first of all you need to replace some variables in this files:

- [./resources/hibernate.cfg.xml](./resources/hibernate.cfg.xml)
- [./settings.xml](./settings.xml)

Each variables description is listed next:

## Variables in [./resources/hibernate.cfg.xml](./resources/hibernate.cfg.xml)

| Variable    | Description | Example |
| ----------- | ----------- | --|
| #{DRIVER_CLASS}# | Database Driver  | com.microsoft.sqlserver.jdbc.SQLServerDriver |
| #{JDBC_CONN}#   | Connection String to access to the database | jdbc:sqlserver://project.database.windows.net:1433;database=tec-core-project-business-aks |
| #{USER}#   | User to access to the database  | dba |
| #{PASSWD}#   | Password to access to the database  | SecretPass123* |
| #{SCHEMA}#   | Schema to access  | dbo |
| #{DIALECT}#   | Database Dialect       | org.hibernate.dialect.SQLServerDialect |

## Variables in [./settings.xml](./settings.xml)

| Variable    | Description | Example |
| ----------- | ----------- | --|
| #{ARTIFACT_USER}#   | User with permissions to access and download files from artifactory  | user |
| #{ARTIFACT_PASSWORD}#   | Password for the user  | SecretPass123* |

After variables were replaced just execute the next command

```bash
mvn package -s settings.xml -Dmaven.repo.local=.
```
