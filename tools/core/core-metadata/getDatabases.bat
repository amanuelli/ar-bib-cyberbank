:: ######## Download core-metadata ########
set curl_opts=-u %1:%2
set artifact=core-metadata
set base_version=3.4.2.1
set repository=https://artifactory.technisys.com/artifactory/list/libs-release/com/technisys/cyberbank/platform/starterkit
set file_name=%artifact%-%base_version%.tar.gz
curl %curl_opts% -o %file_name% %repository%/%artifact%/%base_version%%version%/%file_name%
tar -xf %file_name%
del %file_name%
:: ####################################