:: ######## Parameters ########
set curl_opts=-u %1:%2
set repository=https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core/basicservices

:: ######## Version ########

set artifactBBS=business-basic-services
set businessBasicServices_version=3.5.1

set artifactBSF=basic-services-framework
set basicServiceFramework_version=3.5.1

set artifactBSU=basic-services-utilities
set basicServiceUtilities_version=3.5.1

:: ## Download Primitives ##
md primitives

set file_name=%artifactBBS%-%businessBasicServices_version%.tar.gz
curl %curl_opts% -o %file_name% %repository%/%artifactBBS%/%businessBasicServices_version%/%file_name%
tar -xf %file_name% -C primitives
del %file_name%

set file_name=%artifactBSF%-%basicServiceFramework_version%.tar.gz
curl %curl_opts% -o %file_name% %repository%/%artifactBSF%/%basicServiceFramework_version%/%file_name%
tar -xf %file_name% -C primitives
del %file_name%

set file_name=%artifactBSU%-%basicServiceUtilities_version%.tar.gz
curl %curl_opts% -o %file_name% %repository%/%artifactBSU%/%basicServiceUtilities_version%/%file_name%
tar -xf %file_name% -C primitives
del %file_name%

:: ####################################