######## Parameters ########
curl_opts="-u $1:$2"

repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core'

######## Versions ########

artifactCEP='core-entities-persistent'
coreEntitiesPersistent_version=3.5.1
coreEntitiesPersistent_sha1_version=4a7d5e964db0582285a41e1aa45493f45cf161cf

artifactCE='core-entities'
coreEntities_version=3.5.1
coreEntities_sha1_version=e80d9c49d83b579f95c4d104c33e62da49eb175e

#### Download Entities ####
mkdir -p entities

file_name=$artifactCEP-$coreEntitiesPersistent_version.jar
curl $curl_opts -o entities/$file_name $repository/$artifactCEP/$coreEntitiesPersistent_version/$file_name
sha1sum entities/$file_name | grep $coreEntitiesPersistent_sha1_version

file_name=$artifactCE-$coreEntities_version.jar
curl $curl_opts -o entities/$file_name $repository/$artifactCE/$coreEntities_version/$file_name
sha1sum entities/$file_name | grep $coreEntities_sha1_version

#################################
