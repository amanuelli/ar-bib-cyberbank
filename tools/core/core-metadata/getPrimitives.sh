######## Parameters ########
curl_opts="-u $1:$2"
repository='https://artifactory.technisys.com/artifactory/list/libs-release/net/technisys/cyberbank/core/basicservices'

######## Versions ########

artifactBBS='business-basic-services'
businessBasicServices_version=3.5.1
businessBasicServices_sha1_version=c08ac784637f02abe10c9eb0d15ff6e56eda2695

artifactBSF='basic-services-framework'
basicServiceFramework_version=3.5.1
basicServiceFramework_sha1_version=2e388b55576e773b6b88d40940af8e1a1246e282

artifactBSU='basic-services-utilities'
basicServiceUtilities_version=3.5.1
basicServiceUtilities_sha1_version=6bbe850ce6965c327f759b419b3f9f7359403504

#### Download Primitives ####
mkdir -p primitives

file_name=$artifactBBS-$businessBasicServices_version.tar.gz
curl $curl_opts -o $file_name $repository/$artifactBBS/$businessBasicServices_version/$file_name
sha1sum $file_name | grep $businessBasicServices_sha1_version
tar -xzvf $file_name -C primitives
rm $file_name

file_name=$artifactBSF-$basicServiceFramework_version.tar.gz
curl $curl_opts -o $file_name $repository/$artifactBSF/$basicServiceFramework_version/$file_name
sha1sum $file_name | grep $basicServiceFramework_sha1_version
tar -xzvf $file_name -C primitives
rm $file_name

file_name=$artifactBSU-$basicServiceUtilities_version.tar.gz
curl $curl_opts -o $file_name $repository/$artifactBSU/$basicServiceUtilities_version/$file_name
sha1sum $file_name | grep $basicServiceUtilities_sha1_version
tar -xzvf $file_name -C primitives
rm $file_name

#################################