# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/)

Changelog at https://technisys.atlassian.net/wiki/spaces/CPG/pages/2166170088/Released+Versions

## [3.4.2.1] - 2021-10-20
## Fixed
- Generic user in view database for ViewConfig [TECDIGSK-671](https://technisys.atlassian.net/browse/TECDIGSK-671)

## [3.4.2] - 2021-10-19
## Changed
- Release 3.4.2 [TECDIGSK-668](https://technisys.atlassian.net/browse/TECDIGSK-668)
- Leveling and Homologation Core Functional Component of Launch Pack:
  *  [TECPRODUCT-8821](https://technisys.atlassian.net/browse/TECPRODUCT-8821)
  *  [TECPRODUCT-8179](https://technisys.atlassian.net/browse/TECPRODUCT-8179)
  *  [TECPRODUCT-7339](https://technisys.atlassian.net/browse/TECPRODUCT-7339)

## [3.4.1.2] - 2021-09-24
## Added
- Add primitives [SDLC-4](https://technisys.atlassian.net/browse/SDLC-4)
- Add entities [SDLC-5](https://technisys.atlassian.net/browse/SDLC-5)

## [3.4.1.1] - 2021-09-10
## Added
- Default admins users for tools on database [SDLC-348](https://technisys.atlassian.net/browse/SDLC-348)


## [3.4.1] - 2021-09-03
## Added
- First release of functional metadata [First release of the component](https://technisys.atlassian.net/browse/SDLC-255)
