if $DEBUG_MODE; then
  java -agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:5005 -cp authorization-server.jar -Djava.security.egd=file:/dev/./urandom -Dloader.path=cpg-authserver.jar -Dloader.main=com.technisys.digital.auth.server.boot.AuthorizationServerApplication org.springframework.boot.loader.PropertiesLauncher
else
  java -cp authorization-server.jar -Djava.security.egd=file:/dev/./urandom -Dloader.path=cpg-authserver.jar -Dloader.main=com.technisys.digital.auth.server.boot.AuthorizationServerApplication org.springframework.boot.loader.PropertiesLauncher
fi
