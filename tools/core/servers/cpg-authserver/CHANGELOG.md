# Changelog


All notable changes to this project will be documented in this file.



The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),


and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unrelease]

## [2.0.0]

### Added
- Update Elastic APM endpoint with token variable [SDLC-165](https://technisys.atlassian.net/browse/SDLC-165).
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)

### Changed
- Update functional environments values [SDLC-117](https://technisys.atlassian.net/browse/SDLC-117)
- Refactor with Jars delivery on PLATFORM to coredelivery STARTERKIT [TECENG-1435](https://technisys.atlassian.net/browse/TECENG-1435)