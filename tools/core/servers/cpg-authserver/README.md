# CPG AuthServer

This is based on the latest platform release of CPG Authserver artifact and contains its configuration and deployment files.

## Code analysis

N/A. This is not a code project.

## Pipeline Status

 CICD     | 
----------|
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-core/CPG%202.0/CPGAuth/CPGAuth%20-%20Techbank%20-%20CI-CD?repoName=technisys%2Ftec-cyberbank-starterkit&branchName=TECENG-1435_cpg-auth)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=2455&repoName=technisys%2Ftec-cyberbank-starterkit&branchName=TECENG-1435_cpg-auth) |

## Table of Contents

- [Project Overview](#markdown-header-project-overview)
  - [Technology stack](#markdown-header-technology-stack)
- [Pipeline Status](#markdown-header-pipeline-status)
- [Getting Started](#markdown-header-getting-started)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Configuration](#markdown-header-configuration)
    - [Properties](#markdown-header-properties)
    - [Tracing](#markdown-header-tracing)
- [Documentation](#markdown-header-documentation)

## Project Overview

This project is based on the latest platform release of CPG Authorization Server artifact and contains its configuration and deployment files.

### Technology stack

* Maven
* Java 8


## Getting Started

## Prerequisites
* Java 11
* Helm

## Configuration 

### Properties

Env                        | Description                                       
---------------------------|---------------------------------------------------
CPG_DB_JDBCURL	| Connection string to database where CPG operates
CPG_DB_USERNAME | Database username
CPG_DB_PASSWORD | Database password
CPG_DB_SCHEMA	| Database schema in wich CPG data is located
CPG_DB_DRIVERCLASSNAME	| Database driver
CPG_DB_AUTOCOMMIT	| Default auto-commit behavior of connections in the pool 
CPG_DB_CONNECTIONTIMEOUT	| Maximum number of ms that a client will wait for a connection from the pool
CPG_DB_IDLETIMEOUT	| Maximum amount of time (in ms) that a connection is allowed to sit idle in the pool
CPG_DB_MAXIMUMPOOLSIZE	| Maximum number of connections in pool
CPG_DB_MINIMUMIDLE	| Minimum number of idle connections to maintain in the pool
AUTH_SERVER_USERACTIVITY_MAXTIME	| Maximum number in seconds of user activity
AUTH_SERVER_DECRYPTUSERCREDENTIALS  | Default values is ´false´ because frontend is not encrypting
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SECRET_TOKEN | Secret token to authorize request to the APM Server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with apm and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"

### Tracing

Tracing is implemented using the opentracing api, spring cloud opentracing and custom filters. The tracer used is the elastic apm opentracing bridge. You must have the elastic agent apm to be able to see the traces in kibana.

## Documentation

All the documentation for this can be found in this [Confluence](https://technisys.atlassian.net/wiki/spaces/CPG/pages/2208367841/Authorization%2BServer%2B-%2BInstallation%2BConfiguration).

