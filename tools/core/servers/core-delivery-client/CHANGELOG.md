# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]


## [2.3.6]
### Changed
- Property to optionally use sub-directories for basic services [RCCP-1548](https://technisys.atlassian.net/browse/RCCP-1548)
- Package component installation by artifactId [RCCP-1548](https://technisys.atlassian.net/browse/RCCP-1548)

## [2.3.5]
### Added
- Property to include library directories for primitives compilation [RCCP-338](https://technisys.atlassian.net/browse/RCCP-338)
- Logging for compilation errors [RCCP-1942](https://technisys.atlassian.net/browse/RCCP-1942)

## [2.3.4]
### Added
- Added version validation for operations, workflows and layouts on package installation. [RCCP-1727](https://technisys.atlassian.net/browse/RCCP-1727)

### Changed
- Update functional environments values [SDLC-117](https://technisys.atlassian.net/browse/SDLC-117)

## [2.3.3]

### Added
- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)
- Refactor with Jars delivery on PLATFORM to coredelivery STARTERKIT [TECENG-1354](https://technisys.atlassian.net/browse/TECENG-1354)
