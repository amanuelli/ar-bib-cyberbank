

name: core-delivery-client

labels:
  app: core-delivery-client
  lang: java
  platform: core

selectorLabels:
  app: core-delivery-client

deployment:

  image:
    repository: tecdigitalacr.azurecr.io/techbank/core-delivery-client
    version: #{MS_VERSION}#
    pullPolicy: Always

  replicaCount: 1

  imagePullSecrets:
    - name: regcred

  serviceAccountName: teccore-sa

  envFrom: 
    - configMapRef:
        name: core-delivery-client-config

  ports:
    - containerPort: 8080
      name: http

  volumeMounts:
    - mountPath: /home/technisys/volume
      name: coreengine-conf
    - mountPath: /home/technisys/config
      name: config-volume

  volumes:
    - name: coreengine-conf
      persistentVolumeClaim:
        claimName: core-delivery-client-volume-engine-qa
    - name: config-volume
      configMap:
        name: core-delivery-client-config-files

volume:
  name: core-delivery-client-volume-engine-qa
  size: 1Gi

service:
  type: LoadBalancer
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-internal: 'true'
    service.beta.kubernetes.io/azure-load-balancer-internal-subnet: subnet-aks-ilb    
  loadBalancerIP: 172.64.3.149
  ports:
    - protocol: TCP
      # This the port exposed.
      port: 8080
      # This the port to bind within the container.
      targetPort: 8080
      # This is the protocol of the binded port within the container.
      name: http

configMap:
  name: core-delivery-client-config
  JAVA_OPTS: -Xmx1024m -XX:MaxMetaspaceSize=256m
  ELASTIC_APM_SERVER_URLS: "http://apm-server.monitor-services:8200"
  ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
  ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
  ELASTIC_APM_APPLICATION_PACKAGES: "net.technisys"
  ELASTIC_APM_CENTRAL_CONFIG: "true"
  ELASTIC_APM_SERVICE_NAME: "core-delivery-client"
  ELASTIC_APM_LOG_LEVEL: "INFO"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "true"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: "net.technisys.cyberbank.core.transaction"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: "(?-i)java.*, (?-i)javax.*, (?-i)sun.*, (?-i)com.sun.*,
                                                          (?-i)jdk.*, (?-i)org.apache.tomcat.*,
                                                          (?-i)org.apache.catalina.*, (?-i)org.apache.coyote.*,
                                                          (?-i)org.eclipse.jetty.*, (?-i)org.springframework.*"
  ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc"
  JAVA_TOOL_OPTIONS: ""
  spring:
    liquibase:
      enabled: "false"
    mvc:
      view:
        prefix: /WEB-INF/jsp/
        suffix: .jsp
    jpa:
      hibernate:
        ddl_auto: none 
      generate_ddl: "false"
      properties:
        hibernate:
          hbm2ddl:
            auto: none
          temp:
            use_jdbc_metadata_defaults: "false"
    servlet:
      multipart:
        max_file_size: 32MB
        max_request_size: 32MB
    datasource:
      url: '#{DM_JDBC_URL}#'
      username: #{DM_USER}#
      password: #{DM_PASSWD}#
      driver_class_name: org.postgresql.Driver
      type: com.zaxxer.hikari.HikariDataSource 
      hikari:
        connection_timeout: "20000" 
        minimum_idle: "1" 
        maximum_pool_size: "3" 
        idle_timeout: "300000" 
        max_lifetime: "1200000" 
        auto_commit: "true"

  logging:
    level:
      net:
        technisys:
          cyberbank:
            core: INFO
      org:
        hibernate:
          orm: INFO

  server:
    port: "8080"

  core:
    path: /home/technisys/volume/core
    install: "false"
    primitives:
      dirs:
      lib:
        dirs:
  view:
    path: /home/technisys/volume/viewfs
    install:
      passivemode: "false"
      service: ""

  entities:
    db:
      url: '#{ENGINE_JDBC_URL}#'
      user: '#{ENGINE_DB_USER}#'
      pass: '#{ENGINE_DB_PASSWD}#'
      driver: com.microsoft.sqlserver.jdbc.SQLServerDriver

  reverse:
    engineering:
      environment: desarrollo

  installation:
    clear:
      issue: "true"
  coredelivery:
    host: "172.64.3.194"
    port: "8080"
    packageDir: /home/technisys/volume/installed-package
  environment:
    name: qa

configFiles:
  name: core-delivery-client-config-files
  config_folder: config
  hibernate:
    connection:
      url: jdbc:oracle:thin:@172.21.0.30:1521:DB11G
      username: {} # set empty to get default value
      password: {} # set empty to get default value
      
secrets:
  name: core-delivery-client-secrets
  elastic:
    apmtoken: "#{elastic.apmtoken}#"

azurefile_secret:
  name: core-delivery-client-storage-secret
  shareName: core-qa-fs
  secretValues:
    azurestorageaccountkey: "#{STORAGEACCOUNT_KEY}#"
    azurestorageaccountname: "#{STORAGEACCOUNT_NAME}#"