# Core Delivery Client

## Table of Contents

- [Project Overview](#markdown-header-project-overview)
  - [Technology stack](#markdown-header-technology-stack)
- [Pipeline Status](#markdown-header-pipeline-status)
- [Getting Started](#markdown-header-getting-started)
  - [Prerequisites](#markdown-header-prerequisites)
  - [Configuration](#markdown-header-configuration)
    - [Files](#markdown-header-files)
    - [Tracing](#markdown-header-tracing)
- [Documentation](#markdown-header-documentation)

## Project Overview

This project is based on the latest platform release of Core Delivery Client artifact and contains its configuration and deployment files.

### Technology stack

* Maven
* Java 8

## Pipeline Status

 CICD     | 
----------|
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-cyberbank/Techbank/MS%20-%20I18n/Master%20-%20Techbank%20I18n?repoName=technisys%2Ftec-omnichannel&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=88&branchName=master)|

## Getting Started

## Prerequisites
* Java 8
* Helm

## Configuration 

### Files

#### application.properties

General configurations of the application such as port in which it is executed, databases, etc. This properties can be overwritten by environment variables. 

Env                         | Description                                       
----------------------------|---------------------------------------------------
spring.datasource.url       | connection string of core delivery databse.
spring.datasource.username  | database user
spring.datasource.password  | database password
spring.datasource.driver-class-name | database driver
spring.datasource.hikari.xxxxx      | all properties with this pattern contains Hikari configuration.
spring.datasource.type      | always with "com.zaxxer.hikari.HikariDataSource".
spring.jpa.hibernate.ddl-auto       |  always with  "none". Check the [official documentation](https://docs.spring.io/spring-boot/docs/1.1.0.M1/reference/html/howto-database-initialization.html).
spring.jpa.properties.hibernate.default_schema  | database schema. 
spring.jpa.properties.hibernate.dialect         | database dialect. 
server.port         | application port
logging.level.xxxxx         | configuration of log level
core.path           | core installation path.
view.path           | view defintitions path locations or Core Delivery Client URL when is installed on different machine.
overrideproject     | project alias
environment.name    | environment name. It must be the same configurated on environments.xml of Core Delivery.
entities.db.url     | connection string of Business Database to execute scripts and generate entities
entities.db.user    | database password
entities.db.pass    | database user
entities.db.driver  | database driver
core.primitives.lib.dirs | directories to get libraries from for the compilation of primitives separated by a semicolon. example: /home/technisys/core/libs-A;/home/technisys/core/libs-B;
core.primitives.dirs | optional subdirectories for primitives relative to the base directory. example: custom; product;


#### hibernate.cfg.xml

Hibernate configuration file that will be used as a basis to create the end when generating entities through the reverse engineering process.

Env                         | Description                                       
----------------------------|---------------------------------------------------
hibernate.bytecode.use_reflection_optimizer  |  Default value "false". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#configurations-bytecode-enhancement)
hibernate.cache.enable_cache  |  Default value "false". Check the [official documentation]
hibernate.cache.provider_class  | Default value "org.hibernate.cache.EhCacheProvider". Check the [official documentation](https://docs.jboss.org/hibernate/stable/core.old/reference/en/html/performance-cache.html)
hibernate.cache.use_query_cache  | Default value "false". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.1/userguide/html_single/chapters/caching/Caching.html)
hibernate.cglib.use_reflection_optimizer  |  Default value "true". Check the [official documentation]
hibernate.connection.driver_class  | Default value "oracle.jdbc.OracleDriver". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#database)
hibernate.connection.username  | Default value "user". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#database)
hibernate.connection.password  | Default value "pass". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#database)
hibernate.connection.provider_class  | Default value "com.zaxxer.hikari.hibernate.HikariConnectionProvider". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#database)
hibernate.connection.url  | Default value "jdbc:oracle:thin:@localhost:1521:DB11G". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#database)
hibernate.current_session_context_class  | Default value "thread". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#architecture-current-session)
hibernate.default_schema  |  Default value "schema". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#configurations-mapping)
hibernate.dialect  | Default value "org.hibernate.dialect.Oracle8iDialect".  Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#database-dialect)
hibernate.format_sql  | Default value "true". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#configurations-logging)
hibernate.show_sql  | Default value "false". Check the [official documentation](https://docs.jboss.org/hibernate/orm/5.0/userguide/html_single/Hibernate_User_Guide.html#configurations-logging)

#### hibernate.reveng.xml

Configuration used by hibernate for the reverse engineering process. The content of the file must be configure according to the database used. 

Env                         | Description                                       
----------------------------|---------------------------------------------------
schema                      |  Database schema.
match-schema                |  Database schema. Used in the differents table filters.
type-mapping                |  Defines a mapping from a Java type to an JDBC datatype.

#### internationalization.xml

Configuration of the internationalization of the application.


#### ConfigMap files

Inside the folder **charts/core-delivery-client/config** are the properties files listed in the previous section. Those are rendered by Helm and a configMap (**charts/core-delivery-client/templates/configmap-files.yaml**) is created containing them. This configmap is mounted inside the core-delivery-client pod. 

You can edit this files directly in the folder, in either two ways:

* Using values from values.yaml, and reference those with helm syntax on the corresponding file:  

```
....
....
<property name="hibernate.connection.url">{{ .Values.configFiles.hibernate.connection.url }}</property>
<property name="hibernate.connection.username">{{ .Values.configFiles.hibernate.connection.username }}</property>
        
....
....
```

* Or you can have multiple folders with configuration of these files for each environment. For example: core-delivery-client/configDev and core-delivery-client/configQA. When installing the chart, modify the ´config_folder´ property with the correct name:

```
....
....
configFiles:
  name: core-delivery-client-config-files
  config_folder: configQA
....
....
```

### Tracing

Tracing is implemented using the opentracing api, spring cloud opentracing and custom filters. The tracer used is the elastic apm opentracing bridge. You must have the elastic agent apm to be able to see the traces in kibana.

Env                        | Description                                       
---------------------------|---------------------------------------------------
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SECRET_TOKEN | Secret token to authorize request to the APM Server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with apm and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"



## Documentation

All the documentation for this can be found in this [Confluence](https://technisys.atlassian.net/wiki/spaces/TC/pages/329880068/Cyberbank%2BCore%2BDelivery%2BClient%2BCCDC).
