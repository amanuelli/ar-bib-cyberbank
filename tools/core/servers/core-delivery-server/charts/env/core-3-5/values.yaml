
name: core-delivery-server

labels:
  app: core-delivery-server
  lang: java
  platform: core

selectorLabels:
  app: core-delivery-server

deployment:

  image:
    repository: tecdigitalacr.azurecr.io/techbank/core-delivery-server
    version: #{MS_VERSION}#
    pullPolicy: Always

  replicaCount: 1

  imagePullSecrets:
    - name: regcred

  serviceAccountName: teccore-sa

  envFrom: 
    - configMapRef:
        name: core-delivery-server-config

  ports:
    - containerPort: 8080
      name: http

  volumeMounts:
    - mountPath: /home/technisys/config/packagefiles
      subPath: packagefiles
      name: core-delivery-server-volume
    - mountPath: /home/technisys/config
      name: config-volume
      
  volumes:
    - name: core-delivery-server-volume
      persistentVolumeClaim:
        claimName: core-delivery-server-volume-dev
    - name: config-volume
      configMap:
        name: core-delivery-server-config-files

service:
  type: LoadBalancer
  annotations:
    service.beta.kubernetes.io/azure-load-balancer-internal: 'true'
    service.beta.kubernetes.io/azure-load-balancer-internal-subnet: subnet-aks-ilb
  loadBalancerIP: 172.64.3.194
  ports:
    - protocol: TCP
      # This the port exposed.
      port: 8080
      # This the port to bind within the container.
      targetPort: 8080
      # This is the protocol of the binded port within the container.
      name: http

configMap:
  name: core-delivery-server-config
  JAVA_OPTS: -Xmx1024m -XX:MaxMetaspaceSize=256m
  ELASTIC_APM_SERVER_URLS: "http://apm-server.monitor-services:8200"
  ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
  ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
  ELASTIC_APM_APPLICATION_PACKAGES: "net.technisys"
  ELASTIC_APM_CENTRAL_CONFIG: "true"
  ELASTIC_APM_SERVICE_NAME: "core-delivery-server"
  ELASTIC_APM_LOG_LEVEL: "INFO"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "true"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: "net.technisys.cyberbank.core.transaction"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: "(?-i)java.*, (?-i)javax.*, (?-i)sun.*, (?-i)com.sun.*,
                                                          (?-i)jdk.*, (?-i)org.apache.tomcat.*,
                                                          (?-i)org.apache.catalina.*, (?-i)org.apache.coyote.*,
                                                          (?-i)org.eclipse.jetty.*, (?-i)org.springframework.*"
  ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc"
  JAVA_TOOL_OPTIONS: ""

configFiles:
  name: core-delivery-server-config-files
  spring:
    servlet:
      multipart:
        max_file_size: 32MB
        max_request_size: 32MB
    mvc:
      view:
        prefix: /WEB-INF/jsp/
        suffix: .jsp

  logging:
    level:
      net:
        technisys:
          cyberbank:
            core: INFO
      org:
        hibernate:
          orm: INFO

  package:
    path: /home/technisys/config/packagefiles

  server:
    servlet:
      session:
        cookie:
          name: DMSESSIONID
    address: 0.0.0.0
    port: 8083
  
  environment_config:
    folder: config
    persistent_context:
      cpg:
        jdbcUrl: '#{CPG_JDBC_URL}#'
        username: "#{CPG_DB_USER}#"
        password: "#{CPG_DB_PASSWD}#"
      view:
        jdbcUrl: '#{VIEW_JDBC_URL}#'
        username: "#{VIEW_DB_USER}#"
        password: "#{VIEW_DB_PASSWD}#"
      core_delivery:
        jdbcUrl: '#{DM_JDBC_URL}#'
        username: "#{DM_USER}#"
        password: "#{DM_PASSWD}#"

volume:
  name: core-delivery-server-volume-dev
  size: 1Gi

secrets:
  name: "core-delivery-server-secrets"
  elastic:
    apmtoken: "#{elastic.apmtoken}#"

azurefile_secret:
  name: core-delivery-server-storage-secret
  shareName: core-delivery-server-dev
  secretValues:
    azurestorageaccountkey: "#{STORAGEACCOUNT_KEY}#"
    azurestorageaccountname: "#{STORAGEACCOUNT_NAME}#"