# Changelog

All notable changes to this project will be documented in this file.


The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [2.3.3]
## Add
- Initial db [TECDIGSK-694](https://technisys.atlassian.net/browse/TECDIGSK-694)

## [2.3.2]
### Changed
- Repositories must be updated before a package is created [RCCP-1960](https://technisys.atlassian.net/browse/RCCP-1960)
- Use artifactId from pom in package creation to avoid overriding a different component [RCCP-1548](https://technisys.atlassian.net/browse/RCCP-1548)
- Allow to exclude entities from package generation [RCCP-1744](https://technisys.atlassian.net/browse/RCCP-1744)
- Allow to exclude primitives from package generation [RCCP-1729](https://technisys.atlassian.net/browse/RCCP-1729)
- Update functional environments values [SDLC-117](https://technisys.atlassian.net/browse/SDLC-117)

## [2.3.1]
### Added

- Added ELASTIC_APM_SECRET_TOKEN to use security token in the connection with apm solution. [TECENG-1477](https://technisys.atlassian.net/browse/TECENG-1477)

### Changed

- Set environment configuration with ConfigMap's
- Refactor with Jars delivery on PLATFORM to coredelivery STARTERKIT [TECENG-1354](https://technisys.atlassian.net/browse/TECENG-1354)
