# Content


This is based on the latest platform release of Core Delivery artifact and contains its configuration and deployment files.

Bacpac is attached to instantiate initial base for Core Delivery Server


## Code analysis



N/A. This is not a code project.


## Jira project

TEC - Core Starterkit: [RCCP](https://technisys.atlassian.net/projects/RCCP/issues?filter=allissues)

### Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log 
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans 
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name
JAVA_TOOL_OPTIONS | String value to run java with to apm param and indicate where is apm-agent.jar example: "-javaagent:/PATH/elastic-apm-agent.jar"

### Default admin core-delivery-server user in initial_db
user | pass
---- | ----
admin | admin

### ConfigMap Files Properties

Inside folder **core-delivery-server/templates/config** are the properties files that are rendered by Helm and a configMap is created containing them. This configmap is mounted inside the core-delivery-server pod.

Following files are inside:

* application.properties: Springboot application properties, like HTTP port

* general-db-context.xml: Hibernate and JPA settings

* git-db-script.xml: Git script repository settings

* git-entities.xml: Git entities repository settings

* git-primitives.xml: Git primitives repository settings

* persistent-context.xml: CCD database settings

* persistent-context-core.xml: CPG database settings

* persistent-context-view.xml: View database settings

* environments.xml: environments settings

In this way you can have multiple folders with configuration of these files for each environment.
For example: **core-delivery-server/templates/configDev** and **core-delivery-server/templates/configQA**

At the moment of installing the chart core-delivery-server, it is indicated within the **configMap_properties** block in the values ​​(using the key **config_folder**: "folder_name"), the folder from which to take these files to load.

For example:
Deploy over QA using the properties folder for configQA
```
....
....
configMap_properties:
  name: core-delivery-server-config-properties
  config_folder: configQA
....
....
```

## Configuration

* **general-db-context.xml file**

    This file contains [Hibernate](https://hibernate.org/) properties like dialect, show_sql, and many others.  We define all the EntityManagers used in the application, one by each database vendor.

* **persistent-context-core.xml file**

    This file contains the CPG database configuration.  Each connection is defined in a bean, having an abstract parent with the common properties.

    We can define N database connections that we can use for reporting later.
    ```
    <bean id="CPG_DEVELOP" p:password="DB_PASSWORD" p:jdbcUrl="jdbc:postgresql://ip/DB_SERVER_NAME"    p:username="DB_USERNAME" parent="abstractDataSource" />
    ```
    We can define several connections to CPG database:
    ```
    <bean id="CPG_TESTING" p:password="DB_PASSWORD" p:jdbcUrl="jdbc:postgresql://ip/DB_SERVER_NAME" p:username="DB_USERNAME" parent="abstractDataSource" />
    ```
    Each bean must be added to the coreSources map.
    ```
    <map key-type="java.lang.String" value-type="java.lang.String">
      <entry key="CPG_DEVELOP" value-ref="CPG_DEVELOP" />
      <entry key="CPG_TESTING" value-ref="CPG_TESTING" />
    </map>
    ```

* **persistent-context-view.xml file**

    This file contains the View database configuration. Each connection is defined in a bean, having an abstract parent with common properties.

    The configuration is the same that we use in CPG connections. Each connection must be added to the viewSources bean.


* **application.properties file**

    Property                | Description
     --- | ---
    spring.servlet.multipart.max_file_size | Check this [spring documentation](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/web/servlet/MultipartProperties.html)
    spring.servlet.multipart.max_request_size | Check this [spring documentation](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/web/servlet/MultipartProperties.html)
    spring.mvc.view.prefix | Check this [spring documentation](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/web/servlet/WebMvcProperties.View.html)
    spring.mvc.view.suffix | Check this [spring documentation](https://docs.spring.io/spring-boot/docs/current/api/org/springframework/boot/autoconfigure/web/servlet/WebMvcProperties.View.html)      
    logging.level.xxxxx    | Environment_config.persistent_context.    core_delivery.
    package.path           | Target path where the packages are     generated
    server.servlet.session.cookie.name | Cookie name used by the     application
    server.servlet.address | Application address
    server.servlet.port    | Application port


* **persistent-context.xml file**
  
    Property                | Description
     --- | ---
    p:driverClassName | Database driver
    p:minimumIdle     | The minimum number of idle connections in   the   pool
    p:maximumPoolSize | The maximum size that the pool is allowed   to   reach, including both idle and in-use connections
    p:jdbcUrl         | URL database connection
    p:username        | The default authentication username
    p:password        | The default authentication password
  
  
* **environments.xml file**

    Property                | Description
     --- | ---
    p:cpgDBName   | CPG Server name database
    p:viewDBName  | View Server name database 
    p:offline     | if true, the source ot the CPG data is the   Core   Delivery Client
    p:cpgDBName   | Server name and port of the Core Delivery Client
    p:deploy      | If true, mark the environment as a source to     generate packages. There will be only one for each installation

## Documentation

All the documentation for the microservice can be found in this [Confluence](https://technisys.atlassian.net/wiki/spaces/TC/pages/330139609/Cyberbank%2BCore%2BDelivery%2BCCD)


## Pipelines Status

 CICD     | DEV         | TEST                | DEMO 
----------|-------------|---------------------|---------------
[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-core/Core%20Delivery/Core%20Delivery%20Server/Starterkit/CoreDelivery%20-%20Techbank%20-%20CI-CD?branchName=TECENG-1354_core-delivery)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=2355&branchName=TECENG-1354_core-delivery) | 