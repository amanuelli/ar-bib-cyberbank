# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [2.0.4] - Unreleased
### Added
- Fix error in override error and wrap when loading errors api [TECTF-1667](https://technisys.atlassian.net/browse/TECTF-1667)
- Fixed bugs related to errors' pagination [TECTF-1659](https://technisys.atlassian.net/browse/TECTF-1659)
- Added pagination to errors list [TECTF-198](https://technisys.atlassian.net/browse/TECTF-198)
- Added secret for hash passwords [TECDIGSK-636](https://technisys.atlassian.net/browse/TECDIGSK-636)

## [2.0.3]

- Released new version [TECDIGSK-548](https://technisys.atlassian.net/browse/TECDIGSK-548)

## [2.0.2]

- Released new version [TECDIGSK-548](https://technisys.atlassian.net/browse/TECDIGSK-548)
- Changelog at https://technisys.atlassian.net/wiki/spaces/CPG/pages/2166170088/Released+Versions

## [2.0.1]
### Changed
- Update Elastic APM endpoint with token variable and added configuration variable for Liquibase [SDLC-165](https://technisys.atlassian.net/browse/SDLC-165). 
- Updated Platform version. View changes in the component's [changelog](https://technisys.atlassian.net/wiki/spaces/CPG/pages/2166170088/Latest+released+version)

## [2.0.0]
### Changed
- Update functional environments values [SDLC-117](https://technisys.atlassian.net/browse/SDLC-117)
- Add and configure Elasticsearch APM agent [SDLC-147](https://technisys.atlassian.net/browse/SDLC-147)
- [First release of the component](https://technisys.atlassian.net/secure/RapidBoard.jspa?rapidView=920&projectKey=TECTF)
