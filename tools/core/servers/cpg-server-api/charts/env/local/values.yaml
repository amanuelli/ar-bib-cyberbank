name: cpg-server-api

labels:
  app: cpg-server-api
  lang: java
  platform: core

selectorLabels:
  app: cpg-server-api

deployment:

  image:
    repository: acrbibank.azurecr.io/techbank/cpg-server-api
    version: #{MS_VERSION}#
    pullPolicy: Always

  replicaCount: 1

  nodeSelector:
    product: core

  tolerations:
    - key: "product"
      operator: "Equal"
      value: "core"
      effect: "NoSchedule"

  envFrom:
  - configMapRef:
      name: cpg-server-api-config
  - secretRef:
      name: cpg-server-api-secrets

  imagePullSecrets:
    - name: regcred

  serviceAccountName: teccore-sa

  port: 8080

  volumeMounts:
    - mountPath: /home/technisys/CpgServerAPI/volume
      name: core-storage

  volumes:
  - name: core-storage
    persistentVolumeClaim:
      claimName: cpg-server-api-volume-dev

service:
  port: 80
  targetPort: 8080
  type: ClusterIP

configMap:
  name: cpg-server-api-config
  ELASTIC_APM_SERVER_URLS: "#{apmserver.url}#"
  ELASTIC_APM_LOG_FORMAT_SOUT: "JSON"
  ELASTIC_APM_ENABLE_LOG_CORRELATION: "true"
  ELASTIC_APM_APPLICATION_PACKAGES: "net.technisys"
  ELASTIC_APM_CENTRAL_CONFIG: "true"
  ELASTIC_APM_SERVICE_NAME: "cpg-server-api"
  ELASTIC_APM_LOG_LEVEL: "INFO"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED: "true"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES: "net.technisys.cyberbank.cpg"
  ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES: "(?-i)java.*, (?-i)javax.*, (?-i)sun.*, (?-i)com.sun.*,
                                                          (?-i)jdk.*, (?-i)org.apache.tomcat.*,
                                                          (?-i)org.apache.catalina.*, (?-i)org.apache.coyote.*,
                                                          (?-i)org.eclipse.jetty.*, (?-i)org.springframework.*"
  ELASTIC_APM_DISABLE_INSTRUMENTATIONS: "experimental, jdbc"
  JAVA_TOOL_OPTIONS: "-javaagent:/home/technisys/CpgServerAPI/elastic-apm-agent.jar"
  JAVA_OPTS: "-Duser.country=AR -Duser.language=es -Duser.timezone=America/Argentina/Buenos_Aires"

  spring:
    datasource:
      url: '#{CPG_JDBC_URL}#'
      driver_class_name: com.microsoft.sqlserver.jdbc.SQLServerDriver
    jpa:
      database_platform: org.hibernate.dialect.SQLServer2012Dialect
      properties:
        hibernate:
          default_schema: dbo
    liquibase:
      enabled: "false"
  cpg:
    authorization_server:
      introspection:
        url: cpg-authserver
    entities:
      directory: /home/technisys/CpgServerAPI/volume/core/entities
    allowed_url: http://10.12.112.70
    
secrets:
  name: cpg-server-api-secrets
  spring:
    datasource:
      username: "#{CPG_DB_USER}#"
      password: "#{CPG_DB_PASSWD}#"
  cpg:
    users:
      salt: "#{CPG_USERS_SALT}#"
  elastic:
    apmtoken: "#{elastic.apmtoken}#"

volume:
  name: cpg-server-api-volume-dev
  size: 1Gi

azurefile_secret:
  name: cpg-server-api-storage-secret
  shareName: bibank-share
  secretValues:
    azurestorageaccountkey: "#{STORAGEACCOUNT_KEY}#"
    azurestorageaccountname: "#{STORAGEACCOUNT_NAME}#"