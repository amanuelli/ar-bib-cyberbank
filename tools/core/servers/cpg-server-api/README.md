# CPGApiServer Tool

[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/tec-core/CPG%202.0/CPGApi%20-%20Platform%20-%20CI-CD?repoName=technisys%2Ftec-core-tools-cpg&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=1059&repoName=technisys%2Ftec-core-tools-cpg&branchName=master)

# Table of Contents

- [Project Overview](#markdown-header-project-overview)

# Project Overview

Please follow the [official documentation](https://technisys.atlassian.net/wiki/spaces/TC/pages/1190528421/Cyberbank+CPG+2.0). 