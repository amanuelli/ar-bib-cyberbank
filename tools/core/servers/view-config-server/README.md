# Content


This is based on the latest platform release of Core Delivery Client artifact and contains its configuration and deployment files.


## Code analysis


N/A. This is not a code project.


## Pipeline Status


 CICD     | 

----------|

[![Build Status](https://dev.azure.com/technisys/TEC%20-%20Digital/_apis/build/status/ViewConfig%20-%20Techbank%20-%20CI-CD?repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)](https://dev.azure.com/technisys/TEC%20-%20Digital/_build/latest?definitionId=2315&repoName=technisys%2Ftec-cyberbank-starterkit&branchName=master)


## Jira project


TEC - Core Starterkit: [RCCP](https://technisys.atlassian.net/projects/RCCP/issues?filter=allissues)



## Configuration


Env                        | Description                                       

---------------------------|---------------------------------------------------
SPRING_DATASOURCE_URL      | Database url Ex: tec-digital.database.windows.net
SPRING_DATASOURCE_DRIVER   | Database driver conection Ex: com.microsoft.sqlserver.jdbc.SQLServerDriver
BRIDGE_TRACING_ENABLED     | Boolean value to indicate id bridge tracing is enabled https://bitbucket.org/technisys/tec-commons-tracing/src/master/
spring.datasource.password | DB password
spring.datasource.username | DB username


