# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

## [2.1.4]

### Changed
- Update functional environments values [SDLC-117](https://technisys.atlassian.net/browse/SDLC-117)
- Refactor with Jars delivery on PLATFORM to viewconfig-server STARTERKIT [TECENG-1293](https://technisys.atlassian.net/browse/TECENG-1293)

