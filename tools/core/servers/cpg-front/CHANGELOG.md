# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

Changelog at https://technisys.atlassian.net/wiki/spaces/CPG/pages/2166170088/Released+Versions

## [2.1.1] - Unreleased
## [2.1.0] - 2021-10-27
- Error wrap fixes [TECTF-1693](https://technisys.atlassian.net/browse/TECTF-1693)
- Add instruction fixes [TECTF-1701](https://technisys.atlassian.net/browse/TECTF-1701)
- Add instruction fixes [TECTF-1698](https://technisys.atlassian.net/browse/TECTF-1698)
- Button sizes fixes [TECTF-1697](https://technisys.atlassian.net/browse/TECTF-1697)
- Fixed iteration list values [TECTF-1638](https://technisys.atlassian.net/browse/TECTF-1638)
- Changed some buttons sizes [TECTF-1697](https://technisys.atlassian.net/browse/TECTF-1697)
- Fixed menu service items disabled [TECTF-1688](https://technisys.atlassian.net/browse/TECTF-1688) 
- Fixed deleting branch adds to changes counter [TECTF-1317](https://technisys.atlassian.net/browse/TECTF-1317) 
- Fixed restore on override error [TECTF-1692](https://technisys.atlassian.net/browse/TECTF-1692) 
- Kept session when having new tabs [TECTF-1100](https://technisys.atlassian.net/browse/TECTF-1100) 
- Font size improvements [TECTF-1677](https://technisys.atlassian.net/browse/TECTF-1677) 
- Navbar improvements [TECTF-1691](https://technisys.atlassian.net/browse/TECTF-1691) 
- Fixed override error with variables [TECTF-1673](https://technisys.atlassian.net/browse/TECTF-1673) 
- Fixed wrap error save button [TECTF-1689](https://technisys.atlassian.net/browse/TECTF-1689) 
- Added refresh session [TECTF-53](https://technisys.atlassian.net/browse/TECTF-53) 
- add preview of selected error and fix error in selection wrap error [TECTF-1673](https://technisys.atlassian.net/browse/TECTF-1673)
- Fix error in wrap select errors [TECTF-1668](https://technisys.atlassian.net/browse/TECTF-1668)
- Fix error in override error and wrap when loading errors api [TECTF-1667](https://technisys.atlassian.net/browse/TECTF-1667)
- Added pagination on error override and bugfixes related to the feature [TECTF-1659](https://technisys.atlassian.net/browse/TECTF-1659)
- Upgraded AntD version [TECTF-1524](https://technisys.atlassian.net/browse/TECTF-1524)
- Released new version [TECDIGSK-656](https://technisys.atlassian.net/browse/TECDIGSK-656)
## [2.0.3] - 2021-09-07
- Released new version [TECDIGSK-595](https://technisys.atlassian.net/browse/TECDIGSK-595)
## [2.0.2] - 2021-08-18
- Released new version [TECDIGSK-548](https://technisys.atlassian.net/browse/TECDIGSK-548)
## [2.0.1] - 2021-07-29

## [2.0.0] - 2021-08-12
## Added
- First release of CPG Front with Starterkit structure [First release of the component](https://technisys.atlassian.net/secure/RapidBoard.jspa?rapidView=920&projectKey=TECTF)
