/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        FastClick.attach(document.body);

        document.addEventListener("deviceready", this.onDeviceReady.bind(this), false);
    },
    hideSplashScreen: function() {
        navigator.splashscreen.hide();
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        Promise.all([this.setupSecureStorage().then(this.setupDeviceUUID()), this.setupPush()])
            .then(function() {
                window.startApp();
            })
            .catch(function(cause) {
                app.showErrorPage(cause.type);
            });
    },
    setupDeviceUUID: function() {
        return new Promise(function(resolve) {
            if (device.platform === "iOS") {
                deviceUUID = null;
                app.getSecureValue(
                    "deviceUUID",
                    // si tiene un ID ya guardado
                    function(value) {
                        app.setDeviceUUID(value);
                        resolve(true);
                    },
                    function(error) {
                        app.setSecureValue(
                            "deviceUUID",
                            window.device.uuid,
                            function(e) {
                                console.log("uuid saved");
                            },
                            function(e) {
                                console.log("uuid not saved");
                            }
                        );
                        app.setDeviceUUID(window.device.uuid);
                        resolve(true);
                    }
                );
            } else {
                // Android no cambia el UUID
                app.setDeviceUUID(window.device.uuid);
                resolve(true);
            }
        });
    },
    setDeviceUUID: function(id) {
        this.deviceUUID = id;
    },
    getDeviceUUID: function() {
        return this.deviceUUID;
    },
    // Setup push notifications
    setupPush: function() {
        return new Promise(function(resolve) {
            app.push = PushNotification.init({
                android: {
                    sound: true,
                    vibrate: true,
                    forceShow: true
                },
                ios: {
                    sound: true,
                    vibration: true,
                    badge: true
                }
            });

            app.push.on("registration", function(data) {
                app.pushRegistrationId = data.registrationId;
            });

            app.push.on("error", function(e) {});

            app.push.on("notification", function(data) {
                if (!data.additionalData.coldstart && data.additionalData.foreground && device.platform === "iOS") {
                    cordova.plugins.notification.local.schedule({
                        foreground: true,
                        priority: 1
                    });
                }
            });

            resolve(true);
        });
    },
    showOSSettings: function(onSuccess, onError) {
        if (window.device.platform === "Android" || window.device.platform === "iOS") {
            window.cordova.plugins.settings.open(
                "application_details", // misma 'Setting constant' para ambos sistemas
                function() {
                    onSuccess(true);
                },
                function() {
                    onError(false);
                }
            );
        }
    },
    isPushEnabled: function(callback) {
        PushNotification.hasPermission(function(result) {
            callback({
                enabled: result.isEnabled,
                registrationId: app.pushRegistrationId
            });
        });
    },
    isFingerprintAvailable: function(callback) {
        if (window.device.platform === "Android") {
            FingerprintAuth.isAvailable(
                function(result) {
                    callback(result);
                },
                function(message) {
                    callback(false, message);
                }
            );
        } else if (window.device.platform === "iOS") {
            let result = {
                isAvailable: true,
                isHardwareDetected: true,
                hasEnrolledFingerprints: true
            };

            window.plugins.touchid.isAvailable(
                function() {
                    callback(result);
                },
                function(error) {
                    if (error.code === -7) {
                        result.hasEnrolledFingerprints = false;
                    } else {
                        result.isAvailable = false;
                        result.isHardwareDetected = false;
                        result.hasEnrolledFingerprints = false;
                    }

                    callback(result);
                }
            );
        }
    },
    verifyFingerprint: function(i18n, onSuccess, onError) {
        let encryptConfig = {
            clientId: "com.technisys.techbank",
            username: "techbank-user",
            password: "KQJ$pa~V`mMZ$/tE8",
            locale: i18n.locale,
            maxAttempts: 3,
            disableBackup: true,
            dialogTitle: i18n.title,
            dialogMessage: i18n.message
        };

        if (window.device.platform === "Android") {
            FingerprintAuth.encrypt(
                encryptConfig,
                function(result) {
                    onSuccess(result);
                },
                function(error) {
                    onError(error);
                }
            );
        } else if (window.device.platform === "iOS") {
            let result = {
                withFingerprint: false
            };

            window.plugins.touchid.verifyFingerprint(
                i18n.message,
                function() {
                    result.withFingerprint = true;

                    onSuccess(result);
                },
                function(error) {
                    onError(error);
                }
            );
        }
    },
    enrollFingerprintOnDevice: function(onSuccess, onError) {
        if (window.device.platform === "Android") {
            window.cordova.plugins.settings.open(
                "security",
                function() {
                    onSuccess(true);
                },
                function() {
                    onError(false);
                }
            );
        } else if (window.device.platform === "iOS") {
            window.cordova.plugins.settings.open(
                "touch",
                function() {
                    onSuccess(true);
                },
                function() {
                    onError(false);
                }
            );
        }
    },

    setupSecureStorage: function() {
        return new Promise(function(resolve, reject) {
            window.security.secureStorage.storage = new cordova.plugins.SecureStorage(
                function() {
                    resolve(true);
                },
                function(error) {
                    navigator.notification.alert(
                        "Please enable the screen lock on your device. This app cannot operate securely without it.",
                        function() {
                            window.security.secureStorage.storage.secureDevice(
                                function() {
                                    resolve(true);
                                },
                                function() {
                                    navigator.app.exitApp();
                                }
                            );
                        },
                        "Screen lock is disabled"
                    );
                },
                "techbank"
            );
        });
    },
    setSecureValue: function(key, value, onSuccess, onError) {
        if (window.security.secureStorage.storage) {
            window.security.secureStorage.storage.set(onSuccess, onError, key, value);
        } else {
            onError("Secure storage doesn't exists");
        }
    },
    getSecureValue: function(key, onSuccess, onError) {
        if (window.security.secureStorage.storage) {
            window.security.secureStorage.storage.get(onSuccess, onError, key);
        } else {
            onError("Secure storage doesn't exists");
        }
    },
    removeSecureValue: function(key, onSuccess, onError) {
        if (window.security.secureStorage.storage) {
            window.security.secureStorage.storage.remove(onSuccess, onError, key);
        } else {
            onError("Secure storage doesn't exists");
        }
    },

    getDevicePlatform: function() {
        return window.device.platform;
    },
    getDeviceId: function() {
        return window.device.uuid;
    },
    getDeviceManufacturer: function() {
        return window.device.manufacturer;
    },
    getDeviceModel: function() {
        return window.device.model;
    },

    /***** Camera *****/
    onBackButton: function(onSuccess, onError) {
        return CameraPreview.onBackButton(
            function(result) {
                onSuccess(result);
            },
            function(error) {
                onError(error);
            }
        );
    },
    startCamera: function(cameraDirection, onSuccess, onError) {
        const options = {
            camera: cameraDirection,
            tapFocus: true,
            toBack: true
        };

        return CameraPreview.startCamera(
            options,
            function(result) {
                onSuccess(result);
            },
            function(error) {
                onError(error);
            }
        );
    },
    stopCamera: function(onSuccess, onError) {
        return CameraPreview.stopCamera(
            function(result) {
                onSuccess(result);
            },
            function(error) {
                onError(error);
            }
        );
    },
    takePicture: function(onSuccess, onError) {
        const options = {
            height: 640,
            width: 480,
            quality: 50
        };

        return CameraPreview.takePicture(
            options,
            function(base64PictureData) {
                onSuccess(base64PictureData);
            },
            function(error) {
                onError(error);
            }
        );
    }
};

app.initialize();
