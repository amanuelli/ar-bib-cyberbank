window.camera = {
    start: function(direction) {
        return new Promise(function(resolve, reject) {
            window.app.startCamera(
                direction,
                function(result) {
                    resolve(result);
                },
                function(error) {
                    reject(error);
                }
            );
        });
    },
    stop: function() {
        return window.app.stopCamera(
            function(result) {
                return result;
            },
            function(error) {
                return error;
            }
        );
    },
    takePicture: function() {
        return new Promise(function(resolve, reject) {
            window.app.takePicture(
                function(result) {
                    resolve(result);
                },
                function(error) {
                    reject(error);
                }
            );
        });
    }
};

window.security = {
    fingerprint: {
        isAvailable: function() {
            return new Promise(function(resolve) {
                window.app.isFingerprintAvailable(function(availability) {
                    resolve(availability);
                });
            });
        },
        verify: function(i18n) {
            return new Promise(function(resolve, reject) {
                window.app.verifyFingerprint(
                    i18n,
                    function(result) {
                        resolve(result);
                    },
                    function(error) {
                        reject(error);
                    }
                );
            });
        },
        enrollOnDevice: function() {
            return new Promise(function(resolve, reject) {
                window.app.enrollFingerprintOnDevice(
                    function(result) {
                        resolve(result);
                    },
                    function(error) {
                        reject(error);
                    }
                );
            });
        }
    },
    secureStorage: {
        storage: null,
        set: function(key, value) {
            return new Promise(function(resolve, reject) {
                window.app.setSecureValue(
                    key,
                    value,
                    function() {
                        resolve(true);
                    },
                    function(error) {
                        reject(error);
                    }
                );
            });
        },
        get: function(key) {
            return new Promise(function(resolve, reject) {
                window.app.getSecureValue(
                    key,
                    function(value) {
                        resolve(value);
                    },
                    function(error) {
                        reject(error);
                    }
                );
            });
        },
        remove: function(key) {
            return new Promise(function(resolve, reject) {
                window.app.removeSecureValue(
                    key,
                    function() {
                        resolve(true);
                    },
                    function(error) {
                        reject(error);
                    }
                );
            });
        }
    }
};

window.deviceInfo = {
    getPlatform: function() {
        return window.app.getDevicePlatform();
    },
    getId: function() {
        return window.app.getDeviceId();
    },
    getManufacturer: function() {
        return window.app.getDeviceManufacturer();
    },
    getModel: function() {
        return window.app.getDeviceModel();
    }
};

window.pushNotifications = {
    isEnabled: function() {
        if (window.isMobileApp()) {
            return new Promise(function(resolve) {
                window.app.isPushEnabled(function(data) {
                    resolve(data);
                });
            });
        } else {
            return new Promise(function(resolve) {
                setTimeout(function() {
                    resolve({ enabled: false });
                });
            });
        }
    },
    showOSSettings: function() {
        return new Promise(function(resolve, reject) {
            window.app.showOSSettings(
                function(result) {
                    resolve(result);
                },
                function(error) {
                    reject(error);
                }
            );
        });
    }
};
