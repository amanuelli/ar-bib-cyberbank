let fs = require("fs-extra");
let path = require("path");

const rootPath = "../mobile/";
const wwwPath = path.join(rootPath, "www/");

// ********************************************************
// ********** CLEANUP AND INITIAL COPY ********************
// ********************************************************

// Cleanup
["css", "fonts", "images", "static"].forEach((folder) => {
    fs.removeSync(path.join(wwwPath, folder));
});

// Copy files
fs.copySync("build/", wwwPath);

// Remove .map files
fs.readdirSync(path.join(wwwPath, "static/js")).forEach((file, index) => {
    if (path.extname(file) === ".map") {
        fs.removeSync(path.join(wwwPath, "static/js", file));
    }
});

// Leo el archivo y empiezo a realizar modificaciones
let content = fs.readFileSync(path.join(wwwPath, "index.html"), "utf8");

// ********************************************************
// ***************** FIX PATHS ****************************
// ********************************************************

content = content.replace(/href="\//g, 'href="');
content = content.replace(/src="\//g, 'src="');

// ********************************************************
// ***************** FIX INCLUDES *************************
// ********************************************************

content = content.replace("<body>", '<body class="mobile-native">');
content = content.replace(
    '<div id="root"></div>',
    '<div id="root"></div><script type="text/javascript" src="cordova.js"></script><script src="js/fastclick-upgraded.js"></script>'
);
content = content.replace('<script src="https://www.google.com/recaptcha/api.js" async defer></script>', "");

// ********************************************************
// ***************** ADD CARRIAGE RETURN ******************
// ********************************************************

let idx = content.indexOf("<title>");

let before = content.substring(0, idx);
let after = content.substring(idx);

content = before + "\n" + after;

// ********************************************************
// ******* INCLUDE CHUNK FILES AND INDEX.JS ***************
// ********************************************************

let scripts =
    '<script type="text/javascript" src="js/plugins.js"></script><script type="text/javascript" src="js/index.js"></script>';
fs.readdirSync(path.join(wwwPath, "static/js")).forEach((file, index) => {
    if (file.endsWith(".chunk.js")) {
        scripts = `<script type="text/javascript" src="static/js/${file}"></script>` + scripts;
    }
});

content = content.replace("</body>", `${scripts}</body>`);

/*  Guardo el archivo modificado */
fs.writeFileSync(path.join(wwwPath, "index.html"), content);

// ********************************************************
// ******* CONFIGURE API AND VERSION  *********************
// ********************************************************

// Leo la versión del pom.xml
let pom = fs.readFileSync(path.join(rootPath, "pom.xml"), "utf8");
let match = /<parent>[\s\S]*?<version>(.*?)<\/version>/.exec(pom);
const version = match[1];

// Chequeo si existe el archivo config.js, de no existir renombro el template
let exists = fs.existsSync(path.join(wwwPath, "config.js"));
if (!exists) {
    fs.renameSync(path.join(wwwPath, "config_template.js"), path.join(wwwPath, "config.js"));
}

//** Leo el archivo y empiezo a realizar modificaciones */
content = fs.readFileSync(path.join(wwwPath, "config.js"), "utf8");
match = /window.API_URL\s*=\s*['"](.*?)['"]/.exec(content);
const apiURL = match[1];

if (!/window.APP_VERSION/.test(content)) {
    content += `\nwindow.APP_VERSION = "v${version}";`;
} else {
    content = content.replace(/window.APP_VERSION.*?;/g, `window.APP_VERSION = "v${version}";`);
}

/*  Guardo el archivo modificado */
fs.writeFileSync(path.join(wwwPath, "config.js"), content);

// Leo el archivo config.xml para actualizar la version
content = fs.readFileSync(path.join(rootPath, "config.xml"), "utf8");

content = content.replace(
    /<widget (.*?)version=".*?" (.*?)version-code=".*?"/,
    `<widget $1version="${version}" $2version-code="${version.replace(/\D/g, "")}"`
);
content = content.replace(/<description>([\s\S]*)\(.*?\)/, `<description>$1(${apiURL})`);

/*  Guardo el archivo modificado */
fs.writeFileSync(path.join(rootPath, "config.xml"), content);

// Quitar el / inicial a las imagenes en el css generado
fs.readdirSync(path.join(wwwPath, "static/css")).forEach((file) => {
    if (path.extname(file) === ".css") {
        content = fs.readFileSync(path.join(wwwPath, "static/css", file), "utf8");
        content = content.replace(/url\(\/static\/media/g, "url(../media");
        fs.writeFileSync(path.join(wwwPath, "static/css", file), content);
    }
});
