import * as b64toBlob from "b64-to-blob";

const fakeClick = (element) => {
    const event = document.createEvent("MouseEvents");

    event.initMouseEvent("click", true, false, window, 0, 0, 0, 0, 0, false, false, false, false, 0, null);
    element.dispatchEvent(event);
};

export const downloadPdf = (name, data) => {
    const urlObject = window.URL || window.webkitURL || window;
    const exportBlob = b64toBlob(data, "application/pdf");

    if ("msSaveBlob" in navigator) {
        /**
         * Prefer msSaveBlob if available - Edge supports a[download] but ignores the filename provided,
         * using the blob UUID instead
         * msSaveBlob will respect the provided filename
         */
        navigator.msSaveBlob(exportBlob, name);
    } else if ("download" in HTMLAnchorElement.prototype) {
        const saveLink = document.createElementNS("http://www.w3.org/1999/xhtml", "a");

        saveLink.href = urlObject.createObjectURL(exportBlob);
        saveLink.download = name;
        fakeClick(saveLink);
    } else {
        throw new Error("Neither a[download] nor msSaveBlob is available");
    }
};

export const downloadXls = (name, data) => {
    const saveLink = document.createElementNS("http://www.w3.org/1999/xhtml", "a");

    saveLink.href = `data:application/vnd.ms-excel;base64,${window.atob(data)}`;
    saveLink.download = name;
    fakeClick(saveLink);
};

export const downloadTxt = (name, data) => {
    const buff = window.Buffer.from(data, "base64");
    const text = buff.toString("ascii");

    const element = document.createElement("a");
    element.setAttribute("href", `data:text/plain;charset=utf-8,${encodeURIComponent(text)}`);
    element.setAttribute("download", name);

    element.style.display = "none";
    document.body.appendChild(element);
    element.click();
    document.body.removeChild(element);
};

export const download = (name, data) => {
    const saveLink = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
    const mediaType = "application/octet-stream";
    saveLink.href = `data:${mediaType};base64,${data}`;
    saveLink.download = name;
    fakeClick(saveLink);
};

export const downloadLink = (name, path) => {
    const saveLink = document.createElementNS("http://www.w3.org/1999/xhtml", "a");
    saveLink.href = path;
    saveLink.target = "_blank";
    saveLink.download = name;
    fakeClick(saveLink);
};
