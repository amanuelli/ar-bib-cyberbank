import * as form from "./form";

const expectTrue = (leftSide, operator, rightSide) => {
    expect(form.evalOperation(leftSide, operator, rightSide)).toBe(true);
};
const expectFalse = (leftSide, operator, rightSide) => {
    expect(form.evalOperation(leftSide, operator, rightSide)).toBe(false);
};

describe("array evaluation - selector field", () => {
    it("array contains value eq", () => {
        expectTrue([0, 1, 2], "==", 1);
        expectTrue([0, 1, 2], "==", "1");
        expectTrue(["id1", "id2", "id3"], "==", "id3");

        expectFalse([0, 1, 2], "==", -1);
        expectFalse([0, 1, 2], "==", "-1");
        expectFalse(["id1", "id2", "id3"], "==", "id4");
    });

    it("array contains value qteq", () => {
        expectTrue([0, 1, 2], ">=", 1);
        expectTrue([0, 1, 2], ">=", "1");
        expectTrue(["0", "1", "2"], ">=", "1");

        expectFalse([0, 1, 2], ">=", 3);
        expectFalse([0, 1, 2], ">=", "3");
        expectFalse(["0", "1", "2"], ">=", "3");
    });

    it("array contains value lteq", () => {
        expectTrue([0, 1, 2], "<=", 1);
        expectTrue([0, 1, 2], "<=", "1");
        expectTrue(["0", "1", "2"], "<=", "1");

        expectFalse([0, 1, 2], "<=", -1);
        expectFalse([0, 1, 2], "<=", "-1");
        expectFalse(["0", "1", "2"], "<=", "-1");
    });

    it("array does not contain value eq", () => {
        expectTrue([0, 1, 2], "!=", -1);
        expectTrue([0, 1, 2], "!=", "-1");
        expectTrue(["0", "1", "2"], "!=", "-1");

        expectFalse([0, 1, 2], "!=", 1);
        expectFalse([0, 1, 2], "!=", "1");
        expectFalse(["0", "1", "2"], "!=", "1");
    });
});

describe("mismatched types", () => {
    it("inequality is true", () => {
        expectTrue(1, "!=", true);
        expectTrue(0, "!=", false);

        expectTrue(0, "!=", "");
        expectTrue(0, "!=", {});
        expectTrue(0, "!=", []);

        expectTrue(0, "!=", null);
        expectTrue(0, "!=", NaN);
    });

    it("equality is false", () => {
        expectFalse(1, "==", true);
        expectFalse(0, "==", false);

        expectFalse(0, "==", "");
        expectFalse(0, "==", {});
        expectFalse(0, "==", []);

        expectFalse(0, "==", null);
        expectFalse(0, "==", NaN);
    });

    it("comparison is false", () => {
        expectFalse("", ">", 0);
        expectFalse({}, ">", 0);
        expectFalse(null, ">", 0);
        expectFalse(NaN, ">", 0);

        expectFalse("", ">=", 0);
        expectFalse({}, ">=", 0);
        expectFalse(null, ">=", 0);
        expectFalse(NaN, ">=", 0);

        expectFalse("", "<", 0);
        expectFalse({}, "<", 0);
        expectFalse(null, "<", 0);
        expectFalse(NaN, "<", 0);

        expectFalse("", "=<", 0);
        expectFalse({}, "=<", 0);
        expectFalse(null, "=<", 0);
        expectFalse(NaN, "=<", 0);
    });
});

describe("string evaluation", () => {
    it("evaluate string equality", () => {
        expectTrue("test", "==", "test");
        expectFalse("test", "!=", "test");
    });
    it("lt and qt is false", () => {
        expectFalse("test", ">", "test");
        expectFalse("test", ">=", "test");
        expectFalse("test", "<", "test");
        expectFalse("test", "<=", "test");
    });
});

describe("numeric evaluation", () => {
    it("evaluate number inequality", () => {
        expectFalse("1", "!=", "1");
        expectFalse(1, "!=", "1");
        expectFalse(1, "!=", 1);
        expectFalse("-1", "!=", "-1");
        expectFalse(-1, "!=", "-1");
        expectFalse(-1, "!=", -1);
        expectFalse("1.25", "!=", "1.25");
        expectFalse(1.25, "!=", "1.25");
        expectFalse(1.25, "!=", 1.25);
        expectFalse("-1.25", "!=", "-1.25");
        expectFalse(-1.25, "!=", "-1.25");
        expectFalse(-1.25, "!=", -1.25);
        expectFalse("1e-5", "!=", "1e-5");
        expectFalse(1e-5, "!=", "1e-5");
        expectFalse(1e-5, "!=", 1e-5);
        expectFalse("-1e-5", "!=", "-1e-5");
        expectFalse(-1e-5, "!=", "-1e-5");
        expectFalse(-1e-5, "!=", -1e-5);
    });

    it("evaluate number equality", () => {
        expectTrue("1", "==", "1");
        expectTrue(1, "==", "1");
        expectTrue(1, "==", 1);
        expectTrue("-1", "==", "-1");
        expectTrue(-1, "==", "-1");
        expectTrue(-1, "==", -1);
        expectTrue("1.25", "==", "1.25");
        expectTrue(1.25, "==", "1.25");
        expectTrue(1.25, "==", 1.25);
        expectTrue("-1.25", "==", "-1.25");
        expectTrue(-1.25, "==", "-1.25");
        expectTrue(-1.25, "==", -1.25);
        expectTrue("1e-5", "==", "1e-5");
        expectTrue(1e-5, "==", "1e-5");
        expectTrue(1e-5, "==", 1e-5);
        expectTrue("-1e-5", "==", "-1e-5");
        expectTrue(-1e-5, "==", "-1e-5");
        expectTrue(-1e-5, "==", -1e-5);
    });

    it("evaluate number gt", () => {
        expectTrue("1", ">", "0");
        expectTrue(1, ">", "0");
        expectTrue(1, ">", 0);

        expectTrue("1.25", ">", 0);
        expectTrue(1.25, ">", 0);
        expectTrue(1e-5, ">", 0);

        expectFalse(-1, ">", 0);
        expectFalse(-1.25, ">", 0);
        expectFalse(-1e-5, ">", 0);
    });

    it("evaluate number lt", () => {
        expectFalse("1", "<", "0");
        expectFalse(1, "<", "0");
        expectFalse(1, "<", 0);

        expectFalse("1.25", "<", 0);
        expectFalse(1.25, "<", 0);
        expectFalse(1e-5, "<", 0);

        expectTrue(-1, "<", 0);
        expectTrue(-1.25, "<", 0);
        expectTrue(-1e-5, "<", 0);
    });

    it("evaluate number lteq", () => {
        expectFalse("1", "<=", "0");
        expectFalse(1, "<=", "0");
        expectFalse(1, "<=", 0);

        expectFalse("1.25", "<=", 0);
        expectFalse(1.25, "<=", 0);
        expectFalse(1e-5, "<=", 0);

        expectTrue(-1, "<=", 0);
        expectTrue(-1.25, "<=", 0);
        expectTrue(-1e-5, "<=", 0);

        expectTrue(0, "<=", 0);
    });

    it("evaluate number gteq", () => {
        expectTrue("1", ">=", "0");
        expectTrue(1, ">=", "0");
        expectTrue(1, ">=", 0);
        expectTrue(0, ">=", 0);

        expectTrue(1.25, ">=", 0);
        expectTrue(1e-5, ">=", 0);

        expectFalse(-1, ">=", 0);
        expectFalse(-1e-5, ">=", 0);
        expectFalse(-1.25, ">=", 0);
    });
});
