export const set = (key, value) => window.security.secureStorage.set(key, value);

export const get = (key) => window.security.secureStorage.get(key);

export const remove = (key) => window.security.secureStorage.remove(key);
