import { arrayOf, shape, string, number, objectOf, oneOfType } from "prop-types";

import * as arrayUtils from "util/array";

export const smartGroupsOptionsCount = (productTypes, values) => {
    const smartGroupAmount = productTypes.length;
    const selectedSmartGroupAmount = arrayUtils.intersection(
        values,
        productTypes.map((productType) => `ALL_${productType}`),
    ).length;

    return { selectedSmartGroupAmount, smartGroupAmount };
};

export const productsOptionsCount = (products, values) => {
    const productsAmount = products.length;
    const selectedProductsAmount = arrayUtils.intersection(values, products.map(({ idProduct }) => idProduct)).length;

    return { selectedProductsAmount, productsAmount };
};

const groupShape = {
    idItem: string,
    label: string,
    ordinal: number,
};

groupShape.childrenList = arrayOf(shape(groupShape)).isRequired;

export const groupsPropType = arrayOf(shape(groupShape));

export const permissionsPropType = objectOf(oneOfType([string, arrayOf(string), objectOf(string)]));

export const signatuleLevelsCountToInt = (object) =>
    Object.entries(object).reduce((values, [key, value]) => ({ ...values, [key]: parseInt(value, 10) }), {});
