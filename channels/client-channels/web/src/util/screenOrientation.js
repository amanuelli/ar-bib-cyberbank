const types = {
    ANY: "any",
    PRIMARY: "primary",
    SECONDARY: "secondary",
};

export const setLandscape = (clockwise = false) => {
    window.screenOrientation.setLandscape(clockwise ? types.SECONDARY : types.PRIMARY);
};

export const setPortrait = (clockwise = false) => {
    window.screenOrientation.setPortrait(clockwise ? types.PRIMARY : types.SECONDARY);
};

export const reset = () => {
    window.screenOrientation.setPortrait(types.PRIMARY);
};
