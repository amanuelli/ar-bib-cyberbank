export const isPushEnabled = () => window.pushNotifications.isEnabled();

export const showOSSettings = () => window.app.showOSSettings();

export const isCurrentDeviceRegistered = (pushNotificationsConfiguredUserDevices) =>
    Object.keys(pushNotificationsConfiguredUserDevices).some(
        (deviceIndex) => pushNotificationsConfiguredUserDevices[deviceIndex].idDevice === window.app.getDeviceUUID(),
    );
