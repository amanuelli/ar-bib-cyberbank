const flowTypes = {
    onboarding: "onboarding",
    enrollment: "enrollment",
    associate: "associate",
};

export { flowTypes as default };
