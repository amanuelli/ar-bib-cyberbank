import React from "react";
import { Switch, Route, withRouter } from "react-router-dom";
import { connect } from "react-redux";

import ExternalLayout from "pages/_layouts/ExternalLayout";
import DefaultLayout from "pages/_layouts/DefaultLayout";
import NotFound from "pages/error/NotFound";

import Decition from "pages/demo/_components/Decition";
import Selection from "pages/demo/_components/Selection";
import Misc from "pages/demo/_components/Misc";
import Ticket from "pages/demo/_components/Ticket";
import LoginStep0alt from "pages/demo/_components/LoginStep0alt";
import SignupForm from "pages/demo/_components/SignupForm";
import SignupCode from "pages/demo/_components/SignupCode";
import SignupSelectImage from "pages/demo/_components/SignupSelectImage";
import SettingsDashboard from "pages/demo/_components/SettingsDashboard";
import SettingsChangeEmail from "pages/demo/_components/SettingsChangeEmail";
import SettingsChangePass from "pages/demo/_components/SettingsChangePass";
import SettingsConfigNotifications from "pages/demo/_components/SettingsConfigNotifications";
import SettingsConfigNotificationsInside from "pages/demo/_components/SettingsConfigNotificationsInside";

import LoginStep0 from "pages/demo/_components/LoginStep0";
import LoginStep1 from "pages/demo/_components/LoginStep1";
import LoginDeskStep01 from "pages/demo/_components/LoginDeskStep01";
import LoginStep1alt from "pages/demo/_components/LoginStep1alt";
import LoginStep2 from "pages/demo/_components/LoginStep2";
import LoginStep3 from "pages/demo/_components/LoginStep3";
import LoginStep4 from "pages/demo/_components/LoginStep4";
import SignupStep0 from "pages/demo/_components/SignupStep0";
import ForgotForm from "pages/demo/_components/ForgotForm";
import ForgotCode from "pages/demo/_components/ForgotCode";
import ForgotReestablish from "pages/demo/_components/ForgotReestablish";
import Congrats from "pages/demo/_components/Congrats";
import Readonly from "pages/demo/_components/Readonly";

import SettingsChangeSeal from "pages/demo/_components/SettingsChangeSeal";
import SettingsChangeSealCheck from "pages/demo/_components/SettingsChangeSealCheck";
import SettingsDeskChangeEmail from "pages/demo/_components/SettingsDeskChangeEmail";
import SettingsOtro from "pages/demo/_components/SettingsOtro";

import ProductDetailInfo from "pages/demo/_components/ProductDetailInfo";
import ProductDetailInfoTest from "pages/demo/_components/ProductDetailInfoTest";
import ProductDetail from "pages/demo/_components/ProductDetail";
import ProductList from "pages/demo/_components/ProductList";

import Entorno from "pages/demo/_components/Entorno";
import EsquemaBase from "pages/demo/_components/EsquemaBase";

import EnvironmentMobile from "pages/demo/_components/fullLayout/EnvironmentMobile";

import Demo from "pages/demo/Demo";

import UserList from "pages/demo/_components/administration/UserList";
import SetPermissions from "pages/demo/_components/administration/SetPermissions";
import SetChannels from "pages/demo/_components/administration/SetChannels";
import ConfirmationPhase from "pages/demo/_components/administration/ConfirmationPhase";
import UserDetail from "pages/demo/_components/administration/UserDetail";
import ModifySignatureEnvironment from "pages/demo/_components/administration/ModifySignatureEnvironment";
import ModifySignatureSchema from "pages/demo/_components/administration/ModifySignatureSchema";
import InviteUser from "pages/demo/_components/administration/InviteUser";
import NoProduct from "pages/demo/_components/NoProduct";
import NSOldVersion from "pages/demo/_components/NSOldVersion";
import NSSmallScreen from "pages/demo/_components/NSSmallScreen";
import NewFormLayout from "pages/demo/_components/NewFormLayout";

import EnrollmentLayout from "pages/_layouts/EnrollmentLayout";

import MailingDesktop from "pages/demo/_components/MailingDesktop";
import OnboardingStep0 from "pages/demo/_components/OnboardingStep0";
import OnboardingStep1 from "pages/demo/_components/OnboardingStep1";
import OnboardingStep2 from "pages/demo/_components/OnboardingStep2";
import OnboardingStep3 from "pages/demo/_components/OnboardingStep3";
import OnboardingStep4 from "pages/demo/_components/OnboardingStep4";
import OnboardingStep5 from "pages/demo/_components/OnboardingStep5";
import OnboardingStep6 from "pages/demo/_components/OnboardingStep6";
import OnboardingStep7 from "pages/demo/_components/OnboardingStep7";

import OnboardingLoadingPage from "pages/demo/_components/OnboardingLoadingPage";
import SuccessMessage from "pages/demo/_components/SuccessMessage";
import ErrorMessage from "pages/demo/_components/ErrorMessage";
import NotFoundSample from "pages/demo/_components/NotFoundSample";

const DemoRoutes = () => (
    <Switch>
        <ExternalLayout exact path="/demo" component={Demo} />
        <Route exact path="/demo/not-found-lala" component={NotFoundSample} />
        <DefaultLayout exact path="/demo/adm/user-list" component={UserList} />
        <DefaultLayout exact path="/demo/adm/modify-signature-schema" component={ModifySignatureSchema} />
        <DefaultLayout exact path="/demo/adm/modify-signature-environment" component={ModifySignatureEnvironment} />
        <DefaultLayout exact path="/demo/adm/invite-user" component={InviteUser} />
        <DefaultLayout exact path="/demo/adm/set-permissions" component={SetPermissions} />
        <DefaultLayout exact path="/demo/new-form-layout" component={NewFormLayout} />
        <DefaultLayout exact path="/demo/adm/set-channels" component={SetChannels} />
        <DefaultLayout exact path="/demo/adm/confirmation-phase" component={ConfirmationPhase} />
        <DefaultLayout exact path="/demo/adm/user-detail" component={UserDetail} />
        <ExternalLayout path="/demo/decition" component={Decition} />
        <ExternalLayout path="/demo/selection" component={Selection} />
        <ExternalLayout path="/demo/misc" component={Misc} />
        <ExternalLayout path="/demo/ticket" component={Ticket} />
        <ExternalLayout path="/demo/login-step-0-alt" component={LoginStep0alt} />
        <ExternalLayout path="/demo/signup-form" component={SignupForm} />
        <ExternalLayout path="/demo/signup-code" component={SignupCode} />
        <ExternalLayout path="/demo/signup-select-image" component={SignupSelectImage} />
        <DefaultLayout path="/demo/settings" component={SettingsDashboard} />
        <ExternalLayout path="/demo/login-step-0" component={LoginStep0} />
        <ExternalLayout path="/demo/login-desk-step-01" component={LoginDeskStep01} />
        <ExternalLayout path="/demo/login-step-1" component={LoginStep1} />
        <ExternalLayout path="/demo/login-step-1-alt" component={LoginStep1alt} />
        <ExternalLayout path="/demo/login-step-2" component={LoginStep2} />
        <ExternalLayout path="/demo/login-step-3" component={LoginStep3} />
        <ExternalLayout path="/demo/login-step-4" component={LoginStep4} />
        <ExternalLayout path="/demo/signup-step-0" component={SignupStep0} />
        <ExternalLayout path="/demo/forgot-form" component={ForgotForm} />
        <ExternalLayout path="/demo/forgot-code" component={ForgotCode} />
        <ExternalLayout path="/demo/forgot-reestablish" component={ForgotReestablish} />
        <ExternalLayout path="/demo/congrats" component={Congrats} />
        <ExternalLayout path="/demo/readonly" component={Readonly} />
        <DefaultLayout path="/demo/settings-changeEmail" component={SettingsChangeEmail} />
        <DefaultLayout path="/demo/settings-changePass" component={SettingsChangePass} />
        <DefaultLayout path="/demo/settings-configNotifications" component={SettingsConfigNotifications} />
        <DefaultLayout path="/demo/settings-configNotificationsInside" component={SettingsConfigNotificationsInside} />
        <DefaultLayout path="/demo/settings-changeSecurityImage" component={SettingsChangeSeal} />
        <DefaultLayout path="/demo/settings-changeSecurityImageInside" component={SettingsChangeSealCheck} />
        <DefaultLayout path="/demo/settings-deskChangeEmail" component={SettingsDeskChangeEmail} />
        <DefaultLayout path="/demo/settings-otro" component={SettingsOtro} />
        <DefaultLayout path="/demo/Entorno" component={Entorno} />
        <ExternalLayout path="/demo/EsquemaBase" component={EsquemaBase} />
        <ExternalLayout path="/demo/product-detail" component={ProductDetail} />
        <DefaultLayout path="/demo/product-detail-info" component={ProductDetailInfo} />
        <DefaultLayout path="/demo/product-detail-info-test" component={ProductDetailInfoTest} />
        <DefaultLayout path="/demo/product-list" component={ProductList} />
        <Route path="/demo/full-layout" component={EnvironmentMobile} />
        <DefaultLayout path="/demo/mailingdesktop" component={MailingDesktop} />
        <EnrollmentLayout path="/demo/onboarding" component={OnboardingStep0} />
        <EnrollmentLayout path="/demo/onboarding1" component={OnboardingStep1} />
        <EnrollmentLayout path="/demo/onboarding2" component={OnboardingStep2} />
        <EnrollmentLayout path="/demo/onboarding3" component={OnboardingStep3} />
        <EnrollmentLayout path="/demo/onboarding4" component={OnboardingStep4} />
        <EnrollmentLayout path="/demo/onboarding5" component={OnboardingStep5} />
        <EnrollmentLayout path="/demo/onboarding6" component={OnboardingStep6} />
        <EnrollmentLayout path="/demo/onboarding7" component={OnboardingStep7} />
        <EnrollmentLayout path="/demo/OnboardingLoadingPage" component={OnboardingLoadingPage} />
        <EnrollmentLayout path="/demo/SuccessMessage" component={SuccessMessage} />
        <EnrollmentLayout path="/demo/ErrorMessage" component={ErrorMessage} />
        <DefaultLayout path="/demo/NoProduct" component={NoProduct} />
        <Route path="/demo/NSOldVersion" component={NSOldVersion} />
        <Route path="/demo/NSSmallScreen" component={NSSmallScreen} />
        <ExternalLayout component={NotFound} />
    </Switch>
);

const mapStateToProps = () => ({});

export default withRouter(connect(mapStateToProps)(DemoRoutes));
