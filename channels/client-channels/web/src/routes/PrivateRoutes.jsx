import React, { Fragment, Component } from "react";
import { Route, withRouter, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { func, bool, shape, string } from "prop-types";

import { actions as statusActions } from "reducers/status";
import { selectors as sessionSelectors } from "reducers/session";

import ChangeEmail from "pages/settings/ChangeEmail";
import ChangeEmailConfirmation from "pages/settings/ChangeEmailConfirmation";
import ChangeEnvironment from "pages/settings/ChangeEnvironment";
import SetDefaultEnvironment from "pages/settings/SetDefaultEnvironment";
import ChangeLanguage from "pages/settings/ChangeLanguage";
import ChangePassword from "pages/settings/ChangePassword";
import ChangeAddresses from "pages/settings/ChangeAddresses";
import ChangePhone from "pages/settings/ChangePhone";
import ChangePhoneConfirmation from "pages/settings/ChangePhoneConfirmation";
import ChangeSecuritySeal from "pages/settings/ChangeSecuritySeal";
import ChangeSecuritySealConfirmation from "pages/settings/ChangeSecuritySealConfirmation";
import LastLogin from "pages/settings/LastLogin";
import DefaultLayout from "pages/_layouts/DefaultLayout";
import Accounts from "pages/accounts/Accounts";
import FingerprintConfiguration from "pages/settings/FingerprintConfiguration";
import FingerprintDeleteConfirmation from "pages/settings/FingerprintDeleteConfirmation";
import PushNotifications from "pages/settings/PushNotifications";
import PushNotificationsDeleteConfirmation from "pages/settings/PushNotificationsDeleteConfirmation";
import MyDevices from "pages/settings/MyDevices";
import DocumentsIndex from "pages/settings/_esign/DocumentsIndex";
import EsignDocumentsSigned from "pages/settings/_esign/EsignDocumentsSigned";
import PaperlessModify from "pages/settings/_paperless/PaperlessModify";
import NotificationsConfiguration from "pages/settings/NotificationsConfiguration";
import NotificationsConfigurationTransports from "pages/settings/NotificationsConfigurationTransports";
import Settings from "pages/settings/Settings";
import Form from "pages/forms/Form";
import Transaction from "pages/forms/Transaction";
import SessionAboutToExpire from "pages/status/SessionAboutToExpire";
import BackButtonListener from "pages/_components/BackButtonListener";
import CreditCards from "pages/creditCards";
import CreditCardDetails from "pages/creditCards/Details";
import CreditrCardMovementDetails from "pages/creditCards/_components/MovementDetails";
import CreditCardAliasForm from "pages/creditCards/EditAlias";
import CreditCardMobileFilters from "pages/creditCards/MobileFilters";
import CreditCardMobileFilterResult from "pages/creditCards/MobileFilterResult";
import AccountDetail from "pages/accounts/Details";
import MovementDetail from "pages/accounts/_components/MovementDetail";
import AccountStatements from "pages/accounts/_components/Statements";
import SetAlias from "pages/accounts/_components/SetAlias";
import Filters from "pages/accounts/_components/MobileFilters";
import Results from "pages/accounts/_components/MobileFiltersResults";
import Desktop from "pages/desktop/Desktop";
import PendingActions from "pages/desktop/PendingActions";
import Loans from "pages/loans/Loans";
import LoanDetails from "pages/loans/Details";
import LoanSetAlias from "pages/loans/_components/SetAlias";
import LoanMobileFilters from "pages/loans/_components/MobileFilters";
import LoanStatementDetail from "pages/loans/_components/StatementDetail";
import CreditCardsPaymentList from "pages/creditCardsPayment/List";
import LoansPaymentList from "pages/loansPayment/List";
import CommunicationMain from "pages/communications/Main";
import NewMessage from "pages/communications/NewMessage";
import ReadMessage from "pages/communications/ReadMessage";
import ReplyMessage from "pages/communications/ReplyMessage";
import PendingTransactionList from "pages/transactions/PendingTransactionList";
import TransactionsList from "pages/transactions/TransactionsList";
import TransactionsListFilters from "pages/transactions/_components/MobileFilters";
import TransactionsFiltersResults from "pages/transactions/_components/MobileFiltersResults";
import BankSearch from "pages/forms/_components/_fields/_bankselector/bankSearch/BankSearch";
import MobileTransfers from "pages/accounts/_components/MobileTransfers";
import PrivacyPolicy from "pages/_components/PrivacyPolicy";
import TermsAndConditions from "pages/_components/TermsAndConditions";
import SupportInfo from "pages/_components/SupportInfo";
import UserInvite from "pages/administration/UserInvite";
import UserInviteStep2 from "pages/administration/UserInviteStep2";
import UserInviteStep3 from "pages/administration/UserInviteStep3";
import Administration from "pages/administration/Administration";
import AdministrationMediumSignatureTicket from "pages/administration/medium/tickets/SignatureTicket";
import PointsOfInterest from "pages/pointsOfInterest";
import Templates from "pages/forms/_components/Templates";
import FormPaymentLines from "pages/forms/FormPaymentLines";
import TransactionPaymentLines from "pages/forms/TransactionPaymentLines";
import StepPEP from "pages/onboarding/StepPEP";
import StepIRS from "pages/enrollment/StepIRS";

class PrivateRoutes extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        hasActiveSession: bool,
        location: shape({
            pathname: string,
        }).isRequired,
    };

    static defaultProps = {
        hasActiveSession: false,
    };

    componentDidMount() {
        const { dispatch, location } = this.props;
        dispatch(statusActions.saveLastHref(location));
    }

    render() {
        const { hasActiveSession } = this.props;

        if (!hasActiveSession) {
            return <Redirect to="/" />;
        }

        return (
            <Fragment>
                <Switch>
                    <DefaultLayout exact path="/desktop" component={Desktop} />

                    <DefaultLayout exact path="/pendingActions" component={PendingActions} withoutLayout />
                    <DefaultLayout exact path="/completePEP" component={StepPEP} withoutLayout />
                    <DefaultLayout exact path="/completeIRS" component={StepIRS} withoutLayout />
                    {/* CARDS */}
                    <DefaultLayout exact path="/creditCards" component={CreditCards} />
                    <DefaultLayout
                        exact
                        path="/creditCards/:id"
                        component={CreditCardDetails}
                        transition="transition-drill-in"
                    />
                    <DefaultLayout
                        exact
                        path="/creditCards/:id/alias"
                        component={CreditCardAliasForm}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/creditCards/:id/filters"
                        component={CreditCardMobileFilters}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/creditCards/:id/filters/results"
                        component={CreditCardMobileFilterResult}
                    />
                    <DefaultLayout
                        exact
                        path="/creditCards/:id/:idStatement"
                        component={CreditrCardMovementDetails}
                        transition="transition-flow-open"
                    />
                    {/* ACCOUNTS */}
                    <DefaultLayout exact path="/accounts" component={Accounts} />
                    <DefaultLayout
                        exact
                        path="/accounts/:id"
                        component={AccountDetail}
                        transition="transition-drill-in"
                    />
                    <DefaultLayout
                        exact
                        path="/accounts/:id/movement"
                        component={MovementDetail}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/accounts/:id/setAlias"
                        component={SetAlias}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout exact path="/accounts/:id/statements" component={AccountStatements} />
                    <DefaultLayout
                        exact
                        path="/accounts/:id/filters"
                        component={Filters}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout exact path="/accounts/:id/filters/results" component={Results} />
                    <DefaultLayout exact path="/accounts/:id/transfers" component={MobileTransfers} />
                    {/* SETTINGS */}
                    <DefaultLayout exact path="/settings" component={Settings} />
                    <DefaultLayout
                        exact
                        path="/settings/changePassword"
                        component={ChangePassword}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/changeSecuritySeal"
                        component={ChangeSecuritySeal}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/changeSecuritySeal/confirmation"
                        component={ChangeSecuritySealConfirmation}
                    />
                    <DefaultLayout
                        exact
                        path="/settings/notificationsConfiguration"
                        component={NotificationsConfiguration}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        path="/settings/notificationsConfiguration/:communicationType"
                        component={NotificationsConfigurationTransports}
                    />
                    <DefaultLayout
                        exact
                        path="/settings/fingerprintConfiguration"
                        component={FingerprintConfiguration}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/fingerprintConfiguration/deleteConfirmation"
                        component={FingerprintDeleteConfirmation}
                    />
                    <DefaultLayout
                        exact
                        path="/settings/pushNotifications"
                        component={PushNotifications}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/myDevices"
                        component={MyDevices}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/esign/documents"
                        component={DocumentsIndex}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/esign/documentsSigned"
                        component={EsignDocumentsSigned}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/paperless/edit"
                        component={PaperlessModify}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/pushNotifications/deleteConfirmation"
                        component={PushNotificationsDeleteConfirmation}
                    />
                    <DefaultLayout
                        exact
                        path="/settings/changeEnvironment"
                        component={ChangeEnvironment}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/setDefaultEnvironment"
                        component={SetDefaultEnvironment}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/changeLanguage"
                        component={ChangeLanguage}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/changeEmail"
                        component={ChangeEmail}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/changeEmail/confirmation"
                        component={ChangeEmailConfirmation}
                    />
                    <DefaultLayout
                        exact
                        path="/settings/ChangeAddresses"
                        component={ChangeAddresses}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/changePhone"
                        component={ChangePhone}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/settings/changePhone/confirmation"
                        component={ChangePhoneConfirmation}
                    />
                    <DefaultLayout exact path="/settings/LastLogin" component={LastLogin} />
                    {/* LOANS */}
                    <DefaultLayout exact path="/loans" component={Loans} />
                    <DefaultLayout exact path="/loans/:id/" component={LoanDetails} transition="transition-drill-in" />
                    <DefaultLayout
                        exact
                        path="/loans/:id/setAlias"
                        component={LoanSetAlias}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/loans/:id/statement"
                        component={LoanStatementDetail}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/loans/:id/filters"
                        component={LoanMobileFilters}
                        transition="transition-flow-open"
                    />
                    {/* FORMS */}
                    <DefaultLayout exact path="/form/:idForm" component={Form} transition="transition-flow-open" />
                    <DefaultLayout
                        path="/transaction/:idTransaction"
                        component={Transaction}
                        transition="transition-flow-open"
                        exact
                    />
                    <DefaultLayout
                        path="/form/:idForm/processDetail"
                        component={FormPaymentLines}
                        transition="transition-drill-in"
                        componentProps={{ isConfirmation: true }}
                    />
                    <DefaultLayout
                        path="/transaction/:idTransaction/processDetail"
                        component={TransactionPaymentLines}
                        transition="transition-drill-in"
                    />
                    {/* PAYMENT LISTS */}
                    <DefaultLayout path="/creditCardsPayment/list" component={CreditCardsPaymentList} />
                    <DefaultLayout path="/loansPayment/list" component={LoansPaymentList} />
                    {/* MANUAL MASS PAYMENT */}
                    <DefaultLayout
                        path="/form/:idForm/manual"
                        component={FormPaymentLines}
                        transition="transition-drill-in"
                    />
                    {/* COMMUNICATIONS */}
                    <DefaultLayout exact path="/communications" component={CommunicationMain} />
                    <DefaultLayout
                        exact
                        path="/communications/compose"
                        component={NewMessage}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout exact path="/communications/reply/:id" component={ReplyMessage} />
                    <DefaultLayout
                        exact
                        path="/communications/read/:id"
                        component={ReadMessage}
                        transition="transition-flow-open"
                    />
                    {/* TRANSACTIONS */}
                    <DefaultLayout exact path="/transactions/list" component={TransactionsList} />
                    <DefaultLayout exact path="/pendingTransaction/list" component={PendingTransactionList} />
                    <DefaultLayout
                        exact
                        path="/transactions/list/filters"
                        component={TransactionsListFilters}
                        transition="transition-flow-open"
                    />
                    <DefaultLayout
                        exact
                        path="/transactions/list/filters/results"
                        component={TransactionsFiltersResults}
                    />
                    <DefaultLayout path="/form/:formId/bankSearch/:fieldId" component={BankSearch} />
                    <DefaultLayout path="/form/:formId/templates" component={Templates} />
                    {/* EXTERNAL */}
                    <DefaultLayout exact path="/privacyPolicy" component={PrivacyPolicy} />
                    <DefaultLayout exact path="/termsAndConditions" component={TermsAndConditions} />
                    <DefaultLayout exact path="/support" component={SupportInfo} />
                    {/* administration */}
                    <DefaultLayout exact path="/administration/users/invite" component={UserInvite} />
                    <DefaultLayout exact path="/administration/users/inviteStep2" component={UserInviteStep2} />
                    <DefaultLayout exact path="/administration/users/inviteStep3" component={UserInviteStep3} />
                    <DefaultLayout
                        exact
                        path="/administration/medium/signature/:idTransaction/ticket"
                        component={AdministrationMediumSignatureTicket}
                    />
                    <DefaultLayout exact path="/settings/pointsOfInterest" component={PointsOfInterest} />
                    <DefaultLayout path="/administration" component={Administration} />
                    {/* end of administration */}
                </Switch>
                <BackButtonListener />
                <Route path="/" component={SessionAboutToExpire} />
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    hasActiveSession: sessionSelectors.isLoggedIn(state),
});

export default withRouter(connect(mapStateToProps)(PrivateRoutes));
