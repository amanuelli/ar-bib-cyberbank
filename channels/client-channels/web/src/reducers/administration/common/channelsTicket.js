import { createReducer, makeActionCreator } from "util/redux";

const INITIAL_STATE = {
    fetching: true,
};

export const selectors = {
    isFetching: ({ channelsTicket }) => channelsTicket.fetching,
};

export default (name) => {
    const types = {
        LOAD_CHANNELS_TICKET_REQUEST: `${name}/LOAD_CHANNELS_TICKET_REQUEST`,
        LOAD_CHANNELS_TICKET_FAILURE: `${name}/LOAD_CHANNELS_TICKET_FAILURE`,
        LOAD_CHANNELS_TICKET_SUCCESS: `${name}/LOAD_CHANNELS_TICKET_SUCCESS`,
    };

    return {
        types,
        reducer: createReducer(INITIAL_STATE, {
            [types.LOAD_CHANNELS_TICKET_REQUEST]: () => ({
                fetching: true,
            }),
            [types.LOAD_CHANNELS_TICKET_FAILURE]: () => ({
                fetching: false,
            }),
            [types.LOAD_CHANNELS_TICKET_SUCCESS]: () => ({
                fetching: false,
            }),
        }),
        actions: {
            loadChannelsTicketRequest: makeActionCreator(types.LOAD_CHANNELS_TICKET_REQUEST, "idTransaction"),
            loadChannelsTicketFailure: makeActionCreator(types.LOAD_CHANNELS_TICKET_FAILURE),
            loadChannelsTicketSuccess: makeActionCreator(types.LOAD_CHANNELS_TICKET_SUCCESS),
        },
    };
};
