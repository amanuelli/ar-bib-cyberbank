import { createReducer, makeActionCreator } from "util/redux";

const INITIAL_STATE = {
    fetching: true,
    signatureLevel: null,
};

export const selectors = {
    isFetching: ({ signatureTicket }) => signatureTicket.fetching,
    getSignatureLevel: ({ signatureTicket }) => signatureTicket.signatureLevel,
};

export default (name) => {
    const types = {
        LOAD_SIGNATURE_TICKET_REQUEST: `${name}/LOAD_SIGNATURE_TICKET_REQUEST`,
        LOAD_SIGNATURE_TICKET_FAILURE: `${name}/LOAD_SIGNATURE_TICKET_FAILURE`,
        LOAD_SIGNATURE_TICKET_SUCCESS: `${name}/LOAD_SIGNATURE_TICKET_SUCCESS`,
    };

    return {
        types,
        reducer: createReducer(INITIAL_STATE, {
            [types.LOAD_SIGNATURE_TICKET_REQUEST]: () => ({
                fetching: true,
            }),
            [types.LOAD_SIGNATURE_TICKET_FAILURE]: () => ({
                fetching: false,
            }),
            [types.LOAD_SIGNATURE_TICKET_SUCCESS]: (state, { signatureLevel }) => ({
                ...state,
                fetching: false,
                signatureLevel,
            }),
        }),
        actions: {
            loadSignatureTicketRequest: makeActionCreator(types.LOAD_SIGNATURE_TICKET_REQUEST, "idTransaction"),
            loadSignatureTicketFailure: makeActionCreator(types.LOAD_SIGNATURE_TICKET_FAILURE),
            loadSignatureTicketSuccess: makeActionCreator(types.LOAD_SIGNATURE_TICKET_SUCCESS, "signatureLevel"),
        },
    };
};
