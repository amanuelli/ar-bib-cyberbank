import { createReducer, makeActionCreator } from "util/redux";

export const types = {
    LOAD_LIST_REQUEST: "transactions/LOAD_LIST_REQUEST",
    LOAD_LIST_FAILURE: "transactions/LOAD_LIST_FAILURE",
    LOAD_LIST_SUCCESS: "transactions/LOAD_LIST_SUCCESS",
    SET_FILTERS: "transactions/SET_FILTERS",
    LOAD_MORE_TRANSACTIONS_REQUEST: "transactions/LOAD_MORE_TRANSACTIONS_REQUEST",
    LOAD_MORE_TRANSACTIONS_SUCCESS: "transactions/LOAD_MORE_TRANSACTIONS_SUCCESS",
    DELETE_DRAFT_REQUEST: "transactions/DELETE_DRAFT_REQUEST",
    DELETE_DRAFT_FAILURE: "transactions/DELETE_DRAFT_FAILURE",
    DELETE_DRAFT_SUCCESS: "transactions/DELETE_DRAFT_SUCCESS",
    REFRESH_PENDING_TRANSACTIONS_QUANTITY_SUCCESS: "transactions/REFRESH_PENDING_TRANSACTIONS_QUANTITY_SUCCESS",
};

export const INITIAL_STATE = {
    transactions: null,
    isFetching: false,
    filters: null,
    hasMoreData: false,
    pageNumber: 1,
    isDeletingDraft: false,
    pendingTransactionsQuantity: 0,
    isFirstFetching: true,
    showFilters: false,
};

export default createReducer(INITIAL_STATE, {
    [types.LOAD_LIST_REQUEST]: (state, action) => ({
        ...state,
        isFetching: true,
        filters: action.filters,
        isFirstFetching: action.isFirstFetching === true,
    }),
    [types.LOAD_LIST_FAILURE]: (state) => ({
        ...state,
        isFetching: false,
        isFirstFetching: false,
    }),
    [types.LOAD_LIST_SUCCESS]: (state, action) => ({
        ...state,
        transactions: action.transactions,
        hasMoreData: action.pageNumber < action.totalPages,
        isFetching: false,
        isFirstFetching: false,
        pageNumber: action.pageNumber,
        showFilters: state.isFirstFetching ? action.totalPages > 1 : state.showFilters,
    }),
    [types.SET_FILTERS]: (state, action) => ({
        ...state,
        filters: action.filters,
    }),
    [types.LOAD_MORE_TRANSACTIONS_REQUEST]: (state) => ({
        ...state,
        isFetching: true,
    }),
    [types.LOAD_MORE_TRANSACTIONS_SUCCESS]: (state, action) => ({
        ...state,
        isFetching: false,
        transactions: state.transactions ? state.transactions.concat(action.transactions) : action.transactions,
        hasMoreData: action.pageNumber < action.totalPages,
        pageNumber: action.pageNumber,
    }),
    [types.DELETE_DRAFT_REQUEST]: (state) => ({
        ...state,
        isDeletingDraft: true,
    }),
    [types.DELETE_DRAFT_FAILURE]: (state) => ({
        ...state,
        isDeletingDraft: false,
    }),
    [types.DELETE_DRAFT_SUCCESS]: (state, action) => ({
        ...state,
        isDeletingDraft: false,
        transactions: state.transactions.filter((item) => item.transaction.idTransaction !== action.idTransaction),
    }),
    [types.REFRESH_PENDING_TRANSACTIONS_QUANTITY_SUCCESS]: (state, action) => ({
        ...state,
        pendingTransactionsQuantity: action.pendingTransactionsQuantity,
    }),
});

export const actions = {
    loadListRequest: makeActionCreator(
        types.LOAD_LIST_REQUEST,
        "filters",
        "onlyPendings",
        "pendingDispatch",
        "isFirstFetching",
    ),
    loadListFailure: makeActionCreator(types.LOAD_LIST_FAILURE),
    loadListSuccess: makeActionCreator(types.LOAD_LIST_SUCCESS, "transactions", "pageNumber", "totalPages"),
    setFilters: makeActionCreator(types.SET_FILTERS, "filters"),
    loadMoreTransactionsRequest: makeActionCreator(
        types.LOAD_MORE_TRANSACTIONS_REQUEST,
        "filters",
        "onlyPendings",
        "pendingDispatch",
    ),
    loadMoreTransactionsSuccess: makeActionCreator(
        types.LOAD_MORE_TRANSACTIONS_SUCCESS,
        "transactions",
        "pageNumber",
        "totalPages",
    ),
    deleteDraftRequest: makeActionCreator(types.DELETE_DRAFT_REQUEST, "idTransaction"),
    deleteDraftFailure: makeActionCreator(types.DELETE_DRAFT_FAILURE),
    deleteDraftSuccess: makeActionCreator(types.DELETE_DRAFT_SUCCESS, "idTransaction"),
};

export const selectors = {
    getTransactions: ({ transactions }) => transactions.transactions,
    getFirstFetch: ({ transactions }) => transactions.isFirstFetching,
    getShowFilters: ({ transactions }) => transactions.showFilters,
    getFetching: ({ transactions }) => transactions.isFetching,
    getHasMoreData: ({ transactions }) => transactions.hasMoreData,
    getPageNumber: ({ transactions }) => transactions.pageNumber,
    getFilters: ({ transactions }) => transactions.filters,
    isDeletingDraft: ({ transactions }) => transactions.isDeletingDraft,
    getPendingTransactionsQuantity: ({ transactions }) => transactions.pendingTransactionsQuantity,
};
