import globalTypes from "reducers/types/global";
import { types as sessionTypes } from "reducers/session";
import { types as loginTypes } from "reducers/login";

export const types = {
    SHOW_SESSION_EXPIRATION_MODAL: "status/SHOW_SESSION_EXPIRATION_MODAL",
    HIDE_SESSION_EXPIRATION_MODAL: "status/HIDE_SESSION_EXPIRATION_MODAL",
    SAVE_LAST_HREF: "status/SAVE_LAST_HREF",
    DELETE_LAST_HREF: "status/DELETE_LAST_HREF",
    RESET_COME_FROM_LOGIN: "status/RESET_COME_FROM_LOGIN",
};

export const INITIAL_STATE = {
    sessionAboutToExpire: false,
    sessionSecondsToExpire: 0,
    sessionExtendedCount: 0,
    lastHref: null,
    comeFromLogin: false,
};

export default (state = INITIAL_STATE, action = {}) => {
    switch (action.type) {
        case types.SHOW_SESSION_EXPIRATION_MODAL:
            return { ...state, sessionAboutToExpire: true, sessionSecondsToExpire: action.secondsToExpire };

        case types.SAVE_LAST_HREF:
            return { ...state, lastHref: action.lastHref };

        case types.DELETE_LAST_HREF:
            return { ...state, lastHref: null };

        // TODO: Delete this if oauth is the only flavour
        case loginTypes.LOGIN_STEP_2_REQUEST:
            return { ...state, comeFromLogin: true };

        case loginTypes.LOGIN_OAUTH_REQUEST:
            return { ...state, comeFromLogin: true };

        case types.RESET_COME_FROM_LOGIN:
            return { ...state, comeFromLogin: false };

        case sessionTypes.EXTEND_SUCCESS:
            return {
                ...state,
                sessionAboutToExpire: false,
                sessionSecondsToExpire: 0,
                sessionExtendedCount: state.sessionExtendedCount + 1,
            };

        case globalTypes.CLEAN_UP:
            return INITIAL_STATE;

        default:
            return state;
    }
};

export const actions = {
    saveLastHref: (lastHref) => ({
        type: types.SAVE_LAST_HREF,
        lastHref,
    }),
    deleteLastHref: () => ({
        type: types.DELETE_LAST_HREF,
    }),
    resetComeFromLogin: () => ({
        type: types.RESET_COME_FROM_LOGIN,
    }),
};

export const selectors = {
    getStatus: (state) => state.status,
    isComeFromLogin: (state) => state.status.comeFromLogin,
};
