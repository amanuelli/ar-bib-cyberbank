import { persistReducer } from "redux-persist";
import storage from "redux-persist/lib/storage";

import { types as fingerprintTypes } from "reducers/fingerprint";
import { createReducer, makeActionCreator } from "util/redux";

export const types = {
    RESET: "login/RESET",
    INIT_LOGIN_FLOW: "login/INIT_LOGIN_FLOW",

    FINALIZE_LOGIN: "login/FINALIZE_LOGIN",

    LOGOUT_REQUEST: "login/LOGOUT_REQUEST",

    LOGIN_STEP_1_REQUEST: "login/LOGIN_STEP_1_REQUEST",
    LOGIN_STEP_1_SUCCESS: "login/LOGIN_STEP_1_SUCCESS",

    // TODO: Delete these types if oauth is the only flavour
    LOGIN_STEP_2_REQUEST: "login/LOGIN_STEP_2_REQUEST",
    LOGIN_STEP_2_SUCCESS: "login/LOGIN_STEP_2_SUCCESS",
    LOGIN_STEP_2_FAILURE: "login/LOGIN_STEP_2_FAILURE",

    LOGIN_OAUTH_REQUEST: "login/LOGIN_OAUTH_REQUEST",
    LOGIN_OAUTH_SUCCESS: "login/LOGIN_OAUTH_SUCCESS",
    LOGIN_OAUTH_FAILURE: "login/LOGIN_OAUTH_FAILURE",
    LOGIN_LIST_ENVIRONMENTS_REQUEST: "login/LOGIN_LIST_ENVIRONMENTS_REQUEST",
    LOGIN_LIST_ENVIRONMENTS_SUCCESS: "login/LOGIN_LIST_ENVIRONMENTS_SUCCESS",

    LOGIN_STEP_3_REQUEST: "login/LOGIN_STEP_3_REQUEST",
    LOGIN_STEP_3_SUCCESS: "login/LOGIN_STEP_3_SUCCESS",
    LOGIN_STEP_4_REQUEST: "login/LOGIN_STEP_4_REQUEST",

    LOGIN_FAILURE: "login/LOGIN_FAILURE",
    LOGIN_FAILURE_REQUIRE_CAPTCHA: "LOGIN_FAILURE_REQUIRE_CAPTCHA",
    LOGIN_FINGERPRINT_FAILURE: "login/LOGIN_FINGERPRINT_FAILURE",
    LOGIN_SUCCESS: "login/LOGIN_SUCCESS",

    FINGERPRINT_LOGIN_SUBMIT: "login/FINGERPRINT_LOGIN_SUBMIT",
    FINGERPRINT_LOGIN_PRE: "login/FINGERPRINT_LOGIN_PRE",
    FINGERPRINT_LOGIN_PRE_SUCCESS: "login/FINGERPRINT_LOGIN_PRE_SUCCESS",

    SET_REMEMBERED_USER: "login/SET_REMEMBERED_USER",
    REMOVE_REMEMBERED_USER: "login/REMOVE_REMEMBERED_USER",
    SET_FROM_ONBOARDING_DATA: "login/SET_FROM_ONBOARDING_DATA",
    MARK_ENVIRONMENTS_DISABLED: "login/MARK_ENVIRONMENTS_DISABLED",
    GO_BACK_LOGIN_SHOW_MESSAGE: "login/GO_BACK_LOGIN_SHOW_MESSAGE",
    SET_FINGERPRINT_ENVIRONMENT_RESTRICTED: "login/SET_FINGERPRINT_ENVIRONMENT_RESTRICTED",
    SET_REGION: "session/SET_REGION",
    GET_CLIENT_COUNTRY_REQ: "session/GET_CLIENT_COUNTRY_REQ",
    GET_CLIENT_COUNTRY_RES: "session/GET_CLIENT_COUNTRY_RES",

    DISMISS_CAMPAIGN_REQUEST: "campaigns/DISMISS_CAMPAIGN_REQUEST",
};

export const INITIAL_STATE = {
    rememberedUser: null,
    exchangeToken: null,
    showCaptcha: false,
    fetching: false,
    lang: "",
    environments: {},
    fromOnboardingLoginData: null,
    username: null,
    userFirstName: null,
    userFullName: null,
    fingerPrintEnvironmentRestricted: false,
    idEnvironmentToAcceptConditions: null,
    region: null,
    clientCountry: null,
    hasLogged: null,
    displayCampaigns: true,
    shouldRememberEmail: false,
};

const reducer = createReducer(INITIAL_STATE, {
    [types.SET_REGION]: (state, { region }) => ({
        ...state,
        region,
    }),
    [types.GET_CLIENT_COUNTRY_RES]: (state, { clientCountry }) => ({
        ...state,
        clientCountry,
    }),
    [types.SET_FROM_ONBOARDING_DATA]: (state, { fromOnboardingLoginData }) => ({
        ...state,
        fromOnboardingLoginData,
    }),
    [types.RESET]: ({ rememberedUser, fromOnboardingLoginData, region, clientCountry }) => ({
        ...INITIAL_STATE,
        rememberedUser,
        fromOnboardingLoginData,
        region,
        clientCountry,
        fetching: false,
    }),
    [types.INIT_LOGIN_FLOW]: ({ fromOnboardingLoginData, rememberedUser, region, clientCountry }) => ({
        ...INITIAL_STATE,
        rememberedUser,
        fromOnboardingLoginData,
        region,
        clientCountry,
    }),

    [types.SET_FINGERPRINT_ENVIRONMENT_RESTRICTED]: (state) => ({ ...state, fingerPrintEnvironmentRestricted: true }),
    [types.LOGIN_STEP_1_REQUEST]: (state) => ({ ...state, fetching: true, notification: null }),
    [types.LOGIN_STEP_1_SUCCESS]: (
        state,
        { exchangeToken, securitySeal, securitySealAlt, username, hasLogged, shouldRememberEmail },
    ) => ({
        ...state,
        fetching: false,
        showCaptcha: false,
        exchangeToken,
        securitySeal,
        securitySealAlt,
        username,
        hasLogged,
        shouldRememberEmail,
    }),

    // TODO: Delete these types if oauth is the only flavour
    [types.LOGIN_STEP_2_REQUEST]: (state) => ({ ...state, fetching: true, notification: null }),
    [types.LOGIN_STEP_2_SUCCESS]: (state, { environments, lang, _userFirstName, _userFullName }) => ({
        ...state,
        fetching: false,
        showCaptcha: false,
        lang,
        _userFirstName,
        _userFullName,
        environments,
    }),
    [types.LOGIN_STEP_2_FAILURE]: (state) => ({
        ...state,
        fetching: false,
    }),

    [types.MARK_ENVIRONMENTS_DISABLED]: (state, { environments }) => ({
        ...state,
        fetching: false,
        environments,
    }),

    [types.LOGIN_OAUTH_REQUEST]: (state) => ({ ...state, fetching: true, notification: null }),
    [types.LOGIN_OAUTH_SUCCESS]: (state, { environments, lang, _userFirstName, _userFullName }) => ({
        ...state,
        fetching: false,
        showCaptcha: false,
        lang,
        _userFirstName,
        _userFullName,
        environments,
    }),
    [types.LOGIN_OAUTH_FAILURE]: (state) => ({
        ...state,
        fetching: false,
    }),

    [types.LOGIN_STEP_3_REQUEST]: (state) => ({ ...state, fetching: true, notification: null }),
    [types.LOGIN_STEP_3_SUCCESS]: (state, { termsAndConditions }) => ({
        ...state,
        fetching: false,
        termsAndConditions,
    }),

    [types.LOGIN_STEP_4_REQUEST]: (state) => ({ ...state, fetching: true, notification: null }),
    [types.LOGIN_SUCCESS]: ({ rememberedUser, region, clientCountry }) => ({
        ...INITIAL_STATE,
        rememberedUser,
        region,
        clientCountry,
    }),

    [types.REMOVE_REMEMBERED_USER]: (state) => ({ ...state, rememberedUser: INITIAL_STATE.rememberedUser }),
    [types.SET_REMEMBERED_USER]: (state, { rememberedUser }) => ({ ...state, rememberedUser }),

    [types.LOGIN_FAILURE]: (state) => ({ ...state, fetching: false, fingerprintSubmiting: false }),

    [types.LOGIN_FAILURE_REQUIRE_CAPTCHA]: (state) => ({
        ...state,
        fetching: false,
        showCaptcha: true,
        fingerprintSubmiting: false,
    }),
    [types.LOGIN_FINGERPRINT_FAILURE]: (state) => ({
        ...state,
        fetching: false,
        showCaptcha: true,
        fingerprintSubmiting: false,
        fingerprintLoginFail: true,
    }),
    [types.FINALIZE_LOGIN]: (state, { response }) => ({
        ...state,
        idEnvironmentToAcceptConditions: response.data.data.idEnvironmentToAcceptConditions,
        fetching: false,
    }),
    [types.FINGERPRINT_LOGIN_SUBMIT]: (state) => ({ ...state, fingerprintSubmiting: true }),
    [types.FINGERPRINT_LOGIN_PRE_SUCCESS]: (state, { exchangeToken }) => ({
        ...state,
        fetching: false,
        exchangeToken,
    }),

    [fingerprintTypes.FINGERPRINT_CONFIGURATION_PRE_SUCCESS]: (state, { exchangeToken }) => ({
        ...state,
        exchangeToken,
    }),
    [types.DISMISS_CAMPAIGN_REQUEST]: (state) => ({
        ...state,
        displayCampaigns: false,
    }),
});

export default persistReducer(
    {
        storage,
        key: "login",
        whitelist: ["rememberedUser", "region", "clientCountry"],
    },
    reducer,
);

export const actions = {
    dismissCampaigns: () => ({
        type: types.DISMISS_CAMPAIGN_REQUEST,
    }),
    getClientContry: () => ({
        type: types.GET_CLIENT_COUNTRY_REQ,
    }),
    setRegion: (region) => ({
        type: types.SET_REGION,
        region,
    }),
    reset: makeActionCreator(types.RESET),
    initLoginFlow: makeActionCreator(types.INIT_LOGIN_FLOW),

    loginStep1: makeActionCreator(
        types.LOGIN_STEP_1_REQUEST,
        "email",
        "shouldRememberEmail",
        "recaptchaResponse",
        "formikBag",
    ),
    // TODO: Delete this action if oauth is the only flavour
    loginStep2: makeActionCreator(types.LOGIN_STEP_2_REQUEST, "password", "recaptchaResponse", "formikBag"),
    oauth: makeActionCreator(types.LOGIN_OAUTH_REQUEST, "password", "recaptchaResponse", "formikBag"),
    loginStep3: makeActionCreator(types.LOGIN_STEP_3_REQUEST, "idEnvironment", "rememberEnvironment", "formikBag"),
    loginStep4: makeActionCreator(types.LOGIN_STEP_4_REQUEST, "acceptConditions", "formikBag"),

    fingerprintLoginPre: makeActionCreator(types.FINGERPRINT_LOGIN_PRE),

    setRememberedUser: makeActionCreator(types.SET_REMEMBERED_USER, "rememberedUser"),
    removeRememberedUser: makeActionCreator(types.REMOVE_REMEMBERED_USER),
    goBackToLoginAndShowMessage: makeActionCreator(types.GO_BACK_LOGIN_SHOW_MESSAGE, "message"),
};

export const selectors = {
    getUsername: ({ login }) => login.username,
    getFingerPrintEnvironmentRestricted: ({ login }) => login.fingerPrintEnvironmentRestricted,
    getRememberedUser: ({ login }) => login.rememberedUser,
    getShouldRememberEmail: ({ login }) => login.shouldRememberEmail,
    getUserFirstName: ({ login }) => login.userFirstName,
    getUserFullName: ({ login }) => login.userFullName,
    getSecuritySeal: ({ login }) => login.securitySeal,
    getSecuritySealAlt: ({ login }) => login.securitySealAlt,
    getShowCaptcha: ({ login }) => login.showCaptcha,
    getFetching: ({ login }) => login.fetching,
    getNotification: ({ login }) => login.notification,
    getTermsAndConditions: ({ login }) => login.termsAndConditions,
    getExchangeToken: ({ login }) => login.exchangeToken,
    getAccessToken: ({ login }) => login.accessToken,
    getFingerprintSubmiting: ({ login }) => login.fingerprintSubmiting,
    getFingerprintLoginFail: ({ login }) => login.fingerprintLoginFail,
    getEnvironments: ({ login }) => login.environments,
    getIsInFlow: ({ login }) => !!login.exchangeToken,
    getFromOnboardingLoginData: ({ login }) => login.fromOnboardingLoginData,
    getIdEnvironmentToAcceptConditions: ({ login }) => login.idEnvironmentToAcceptConditions,
    getRegion: ({ login }) => login.region,
    getHasLogged: ({ login }) => login.hasLogged,
    getDisplayCampaigns: ({ login }) => login.displayCampaigns,
};
