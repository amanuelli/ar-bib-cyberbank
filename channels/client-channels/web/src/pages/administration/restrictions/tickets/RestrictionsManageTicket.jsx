import React, { Component } from "react";
import AdministrationTicket from "pages/administration/_components/tickets/AdministrationTicket";
import RestrictionsManageTicketData from "pages/administration/restrictions/tickets/RestrictionsManageTicketData";

class RestrictionsManageTicket extends Component {
    render() {
        return (
            <AdministrationTicket {...this.props}>
                <RestrictionsManageTicketData {...this.props} />
            </AdministrationTicket>
        );
    }
}

export default RestrictionsManageTicket;
