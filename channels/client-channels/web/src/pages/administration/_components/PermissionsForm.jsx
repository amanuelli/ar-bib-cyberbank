import React, { Component, Fragment } from "react";
import { Field } from "formik";
import { connect } from "react-redux";
import { arrayOf, shape, string, func, objectOf } from "prop-types";

import { permissionsSelectors } from "reducers/administration";
import * as administrationUtils from "util/administration";

import Image from "pages/_components/Image";
import ProductLabelIcon from "pages/_components/ProductLabelIcon";
import Accordion from "pages/_components/Accordion";
import IndeterminateCheckbox from "pages/_components/fields/IndeterminateCheckbox";
import PermissionsAmount from "pages/administration/_components/PermissionsAmount";

class PermissionsForm extends Component {
    static propTypes = {
        setValues: func.isRequired,
        groups: administrationUtils.groupsPropType.isRequired,
        values: objectOf(arrayOf(string)),
        products: arrayOf(
            shape({
                productType: string,
            }),
        ).isRequired,
    };

    static defaultProps = {
        values: {},
    };

    dependsFromFields = ({ childrenList }, idItemToFind) =>
        childrenList.reduce((flattenedOptions, option) => {
            if (!option.childrenList.length) {
                if (option.dependsOn === idItemToFind) {
                    return [...flattenedOptions, option.idItem];
                }

                return flattenedOptions;
            }

            return [...flattenedOptions, ...this.dependsFromFields(option, idItemToFind)];
        }, []);

    setValues = ({ nextValue, setValues, dependsOn, values, idItem, parentOption }) => {
        let value = { [idItem]: nextValue };

        if (!nextValue.length && dependsOn) {
            value = { [idItem]: nextValue, [dependsOn]: nextValue };
        } else if (nextValue.length && !dependsOn) {
            value = [idItem, ...this.dependsFromFields(parentOption, idItem)].reduce(
                (acc, id) => ({ ...acc, [id]: nextValue }),
                {},
            );
        }

        setValues({ ...values, ...value });
    };

    permissionValue = (permissionList) => {
        if (permissionList && permissionList[0].productTypes) {
            return permissionList[0].productTypes.split(",").map((type) => `ALL_${type}`);
        }

        return ["NONE"];
    };

    fillValues = ({ permissionList, childrenList, idItem }) => {
        if (permissionList.length && permissionList[0].simpleAllowProductSelection) {
            const { products } = this.props;

            return { [idItem]: products.map(({ idProduct }) => idProduct) };
        }

        if (childrenList.length) {
            return childrenList.reduce(
                (acc, option) => ({
                    ...acc,
                    ...this.fillValues(option, acc),
                }),
                {},
            );
        }

        return { [idItem]: this.permissionValue(permissionList) };
    };

    handleCheckClick = (values, option, setValues, selectedOptionsAmount) => {
        const filled = this.fillValues(option);

        if (selectedOptionsAmount === 0) {
            setValues({
                ...values,
                ...filled,
            });
        } else {
            const keysToRemove = Object.keys(filled);

            setValues(
                Object.entries(values).reduce(
                    (filteredValues, [key, value]) => ({
                        ...filteredValues,
                        [key]: keysToRemove.includes(key) ? [] : value,
                    }),
                    {},
                ),
            );
        }
    };

    renderProductInput = ({ idProduct, selectedOptions = [], setValues, values, idItem }) => (
        <input
            name={idProduct}
            value={idProduct}
            checked={selectedOptions.includes(idProduct)}
            onChange={() =>
                setValues({
                    ...values,
                    [idItem]: selectedOptions.includes(idProduct)
                        ? selectedOptions.filter((value) => value !== idProduct)
                        : selectedOptions.concat(idProduct),
                })
            }
            type="checkbox"
            id={idProduct}
            className="c-control-input"
        />
    );

    renderInput = ({ permissionList, selectedOptions = [], ...rest }) => (
        <input
            type="checkbox"
            id={rest.idItem}
            onChange={() =>
                this.setValues({
                    ...rest,
                    nextValue: selectedOptions.length ? [] : this.permissionValue(permissionList),
                })
            }
            className="c-control-input"
            checked={selectedOptions.length}
        />
    );

    renderField = ({ idItem, label, idProduct, number, productAlias, ...option }) => (
        <Field name={idItem} key={idProduct || idItem}>
            {({ form }) => {
                const selectedOptions = form.values[idItem];

                return (
                    <div className="c-control c-control-block c-control--has-icon c-control--checkbox">
                        {!idProduct
                            ? this.renderInput({ ...option, ...form, idItem, selectedOptions })
                            : this.renderProductInput({ ...option, ...form, idItem, idProduct, selectedOptions })}
                        <label className="c-control-label" htmlFor={idProduct || idItem}>
                            <div className="c-control-icons">
                                <div className="c-control-mark">
                                    <Image src="images/check.svg" className="svg-icon svg-caret" />
                                </div>
                            </div>
                            {!idProduct ? (
                                <div>{label}</div>
                            ) : (
                                <div
                                    style={{
                                        display: "inline-flex",
                                    }}
                                    className="product-label">
                                    <ProductLabelIcon number={number} />
                                    <div className="product-label-text">
                                        <div className="product-label-name">{productAlias || label}</div>
                                        {productAlias && <div className="product-label-info">{label}</div>}
                                    </div>
                                </div>
                            )}
                        </label>
                    </div>
                );
            }}
        </Field>
    );

    renderOptions = (option, isFirstLevel) => {
        if (option.childrenList && option.childrenList.length) {
            return (
                <Fragment key={option.idItem}>
                    {!isFirstLevel && <div className="navigational-list-subtitle">{option.label}</div>}

                    {option.childrenList.map((subOption) => this.renderOptions({ ...subOption, parentOption: option }))}
                </Fragment>
            );
        }

        if (option.permissionList && option.permissionList[0].simpleAllowProductSelection) {
            const { products } = this.props;

            return products.reduce((availableProducts, product) => {
                if (option.permissionList[0].productTypes.indexOf(product.productType) === -1) {
                    return availableProducts;
                }

                return [...availableProducts, this.renderField({ ...product, idItem: option.idItem })];
            }, []);
        }

        return this.renderField(option);
    };

    render() {
        const { values, setValues, groups } = this.props;

        return (
            <PermissionsAmount permissions={values}>
                {(amountsById, totalAmountsById) => (
                    <Accordion className="list list--permissions">
                        {groups.map((group, index) => {
                            const { idItem, label, ordinal } = group;
                            const selectedOptionsAmount = amountsById[idItem] || 0;
                            const optionsAmount = totalAmountsById[idItem];
                            const options = this.renderOptions({ ...group, parentOption: group }, true);
                            const quantityText = `${selectedOptionsAmount} / ${optionsAmount}`;

                            if (options.props && typeof options.props.children === "function") {
                                return (
                                    <li key={idItem} className="navigational-list-item list-item--noChilds">
                                        {options}
                                    </li>
                                );
                            }

                            return (
                                <Accordion.Item
                                    key={idItem}
                                    id={idItem}
                                    number={index}
                                    item={
                                        <Fragment>
                                            <IndeterminateCheckbox
                                                id={`${idItem}${ordinal}`}
                                                selectedOptionsAmount={selectedOptionsAmount}
                                                optionsAmount={optionsAmount}
                                                onCheckClick={() =>
                                                    this.handleCheckClick(
                                                        values,
                                                        group,
                                                        setValues,
                                                        selectedOptionsAmount,
                                                    )
                                                }
                                            />
                                            <span>
                                                <span>{label}</span>{" "}
                                                <span className="list-item-hint">{quantityText}</span>
                                            </span>
                                        </Fragment>
                                    }>
                                    {options}
                                </Accordion.Item>
                            );
                        })}
                    </Accordion>
                )}
            </PermissionsAmount>
        );
    }
}

const mapStateToProps = (state) => ({
    groups: permissionsSelectors.getGroups(state),
    products: permissionsSelectors.getProducts(state),
});

export default connect(mapStateToProps)(PermissionsForm);
