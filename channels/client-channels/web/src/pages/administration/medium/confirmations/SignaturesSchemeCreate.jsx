import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { routerActions } from "react-router-redux/actions";

import { selectors as sessionSelectors } from "reducers/session";
import { actions as formActions } from "reducers/form";
import { signaturesSchemesSelectors } from "reducers/administration";
import { signaturesSchemesActions } from "reducers/administration/medium";

import SignaturesSchemeCreateConfirm from "pages/administration/_components/confirmations/SignaturesSchemeCreate";

const mapStateToProps = (state) => ({
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    fetching: signaturesSchemesSelectors.fetching(state),
    functionalGroups: signaturesSchemesSelectors.selectedFunctionalGroups(state),
    signature: signaturesSchemesSelectors.currentSignature(state),
    signatureLevelsCounts: signaturesSchemesSelectors.signatureLevelsCounts(state),
    environmentProducts: signaturesSchemesSelectors.environmentProducts(state),
    selectedProducts: signaturesSchemesSelectors.selectedProducts(state),
    signatureType: signaturesSchemesSelectors.signatureType(state),
    topAmount: signaturesSchemesSelectors.topAmount(state),
    credentialGroups: signaturesSchemesSelectors.getCredentialGroups(state),
    idTransaction: signaturesSchemesSelectors.getIdTransaction(state),
    idActivity: signaturesSchemesSelectors.getIdActivity(state),
    signatureDispatch: signaturesSchemesSelectors.signatureDispatch(state),
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(signaturesSchemesActions, dispatch),
    formActions: bindActionCreators(formActions, dispatch),
    routerActions: bindActionCreators(routerActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SignaturesSchemeCreateConfirm);
