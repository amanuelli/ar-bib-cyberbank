import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { routerActions } from "react-router-redux/actions";

import { permissionsSelectors } from "reducers/administration";
import { actions as formActions } from "reducers/form";
import { permissionsActions } from "reducers/administration/medium";

import PermissionsConfirm from "pages/administration/_components/PermissionsConfirm";

const mapStateToProps = (state) => ({
    permissions: permissionsSelectors.getFuturePermissions(state),
    credentialGroups: permissionsSelectors.getCredentialGroups(state),
    user: permissionsSelectors.getUser(state),
    groups: permissionsSelectors.getGroups(state),
    products: permissionsSelectors.getProducts(state),
    idTransaction: permissionsSelectors.getIdTransaction(state),
    idActivity: permissionsSelectors.getIdActivity(state),
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(permissionsActions, dispatch),
    formActions: bindActionCreators(formActions, dispatch),
    routerActions: bindActionCreators(routerActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(PermissionsConfirm);
