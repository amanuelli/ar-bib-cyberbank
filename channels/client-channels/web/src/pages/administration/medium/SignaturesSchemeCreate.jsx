import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { routerActions } from "react-router-redux";

import { selectors as sessionSelectors } from "reducers/session";
import { signaturesSchemesSelectors } from "reducers/administration";
import { signaturesSchemesActions } from "reducers/administration/medium";

import SignaturesSchemeCreate from "pages/administration/_components/SignaturesSchemeCreate";

const mapStateToProps = (state) => ({
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    credentialGroups: signaturesSchemesSelectors.getCredentialGroups(state),
    capFrequencyList: signaturesSchemesSelectors.capFrequencyList(state),
    fetching: signaturesSchemesSelectors.fetching(state),
    functionalGroups: signaturesSchemesSelectors.functionalGroups(state),
    environmentProducts: signaturesSchemesSelectors.environmentProducts(state),
    selectedProducts: signaturesSchemesSelectors.selectedProducts(state),
    masterCurrency: signaturesSchemesSelectors.masterCurrency(state),
    signatureTypeList: signaturesSchemesSelectors.signatureTypeList(state),
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(signaturesSchemesActions, dispatch),
    routerActions: bindActionCreators(routerActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SignaturesSchemeCreate);
