import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { routerActions } from "react-router-redux/actions";

import { actions as formActions } from "reducers/form";
import { selectors as sessionSelectors } from "reducers/session";
import { signaturesSchemesSelectors } from "reducers/administration";
import { signaturesSchemesActions } from "reducers/administration/advanced";

import SignaturesSchemeModifyConfirm from "pages/administration/_components/confirmations/SignaturesSchemeModify";

const mapStateToProps = (state) => ({
    credentialGroups: signaturesSchemesSelectors.getCredentialGroups(state),
    idTransaction: signaturesSchemesSelectors.getIdTransaction(state),
    idActivity: signaturesSchemesSelectors.getIdActivity(state),
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    capFrequencyList: signaturesSchemesSelectors.capFrequencyList(state),
    caps: signaturesSchemesSelectors.caps(state),
    fetching: signaturesSchemesSelectors.fetching(state),
    functionalGroups: signaturesSchemesSelectors.selectedFunctionalGroups(state),
    masterCurrency: signaturesSchemesSelectors.masterCurrency(state),
    signature: signaturesSchemesSelectors.currentSignature(state),
    signatureLevelsCounts: signaturesSchemesSelectors.signatureLevelsCounts(state),
    signatureType: signaturesSchemesSelectors.signatureType(state),
    topAmount: signaturesSchemesSelectors.topAmount(state),
    signatureDispatch: signaturesSchemesSelectors.signatureDispatch(state),
    selectedProducts: signaturesSchemesSelectors.selectedProducts(state),
    environmentProducts: signaturesSchemesSelectors.environmentProducts(state),
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(signaturesSchemesActions, dispatch),
    formActions: bindActionCreators(formActions, dispatch),
    routerActions: bindActionCreators(routerActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SignaturesSchemeModifyConfirm);
