import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { routerActions } from "react-router-redux/actions";

import { actions as formActions } from "reducers/form";
import { selectors as sessionSelectors } from "reducers/session";
import { signaturesSchemesSelectors } from "reducers/administration";
import { signaturesSchemesActions } from "reducers/administration/advanced";

import SignaturesSchemeCreateConfirm from "pages/administration/_components/confirmations/SignaturesSchemeCreate";

const mapStateToProps = (state) => ({
    idTransaction: signaturesSchemesSelectors.getIdTransaction(state),
    idActivity: signaturesSchemesSelectors.getIdActivity(state),
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    fetching: signaturesSchemesSelectors.fetching(state),
    functionalGroups: signaturesSchemesSelectors.selectedFunctionalGroups(state),
    environmentProducts: signaturesSchemesSelectors.environmentProducts(state),
    selectedProducts: signaturesSchemesSelectors.selectedProducts(state),
    signature: signaturesSchemesSelectors.currentSignature(state),
    signatureLevelsCounts: signaturesSchemesSelectors.signatureLevelsCounts(state),
    signatureType: signaturesSchemesSelectors.signatureType(state),
    masterCurrency: signaturesSchemesSelectors.masterCurrency(state),
    topAmount: signaturesSchemesSelectors.topAmount(state),
    credentialGroups: signaturesSchemesSelectors.getCredentialGroups(state),
    signatureDispatch: signaturesSchemesSelectors.signatureDispatch(state),
});

const mapDispatchToProps = (dispatch) => ({
    actions: bindActionCreators(signaturesSchemesActions, dispatch),
    formActions: bindActionCreators(formActions, dispatch),
    routerActions: bindActionCreators(routerActions, dispatch),
});

export default connect(
    mapStateToProps,
    mapDispatchToProps,
)(SignaturesSchemeCreateConfirm);
