import React, { Component, Fragment } from "react";
import Col from "react-bootstrap/lib/Col";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { withFormik, Form, Field } from "formik";
import { shape, func, bool, node, string } from "prop-types";
import * as Yup from "yup";

import { actions as fingerprintActions, selectors as fingerprintSelectors } from "reducers/fingerprint";
import { selectors as assistantSelectors } from "reducers/assistant";
import { actions as loginActions, selectors as loginSelectors } from "reducers/login";
import * as i18n from "util/i18n";

import TextField from "pages/_components/fields/TextField";
import Container from "pages/_components/Container";
import Checkbox from "pages/_components/fields/formik/Checkbox";
import I18n from "pages/_components/I18n";
import Button from "pages/_components/Button";
import AssistantAlert from "pages/login/_components/AssistantAlert";
import LanguageSelectionLink from "pages/login/_components/LanguageSelectionLink";
import RegionSelectionLink from "pages/login/_components/RegionSelectionLink";
import { Mixpanel } from "util/clickstreaming";
import { emailRegex } from "util/form";

const FORM_ID = "login.step1";

class Step1Content extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        isMobileNative: bool.isRequired,
        isDesktop: bool.isRequired,
        isFromAmazon: bool.isRequired,
        isFromGoogle: bool.isRequired,
        isFromWhatsapp: bool.isRequired,
        isFromMessenger: bool.isRequired,
        isFetching: bool.isRequired,
        className: string,
        username: string,
        userFullName: string,
        footer: node,
        values: shape({
            username: string,
            rememberEmail: bool,
        }).isRequired,
        fromOnboardingLoginData: shape({}).isRequired,
        fingerprintAvailable: shape({}).isRequired,
        fingerPrintEnvironmentRestricted: bool,
    };

    static defaultProps = {
        footer: null,
        className: "",
        username: "",
        userFullName: "",
        fingerPrintEnvironmentRestricted: false,
    };

    state = {
        fingerprintInitialized: false,
    };

    componentDidMount() {
        const { dispatch, isMobileNative } = this.props;

        if (isMobileNative) {
            window.common.hideSplashScreen();
            dispatch(fingerprintActions.fingerprintAvailability());
        }
    }

    /* eslint-disable-next-line react/sort-comp, camelcase */
    UNSAFE_componentWillReceiveProps(nextProps) {
        const { isMobileNative, dispatch, fingerPrintEnvironmentRestricted } = this.props;
        const { fingerprintInitialized } = this.state;

        if (isMobileNative && !fingerprintInitialized && nextProps.fingerprintAvailable) {
            const { isHardwareDetected, hasEnrolledFingerprints, isAvailable } = nextProps.fingerprintAvailable;

            if (isHardwareDetected && hasEnrolledFingerprints && isAvailable && !fingerPrintEnvironmentRestricted) {
                dispatch(loginActions.fingerprintLoginPre());
            }

            this.setState({ fingerprintInitialized: true });
        }
    }

    shouldComponentUpdate(nextProps) {
        const { username } = this.props;

        // when user selected to be remembered, this prevents showing user welcome message when navigating to step 2
        if (nextProps.username && username === "") {
            return false;
        }

        return true;
    }

    reset = () => {
        const { dispatch } = this.props;
        dispatch(loginActions.removeRememberedUser());
    };

    render() {
        const {
            footer,
            isDesktop,
            isFetching,
            className,
            username,
            userFullName,
            isFromAmazon,
            isFromGoogle,
            isFromMessenger,
            isFromWhatsapp,
            values,
            fromOnboardingLoginData,
        } = this.props;

        return (
            <Form className={className} noValidate="novalidate">
                {!isDesktop && (isFromAmazon || isFromGoogle || isFromMessenger || isFromWhatsapp) && (
                    <AssistantAlert />
                )}
                <Container className="flex-grow align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        {username ? (
                            <div className="text-lead">
                                <h2>
                                    {
                                        // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
                                    }
                                    <span tabIndex="0">
                                        <I18n id="login.step1.welcome" /> {userFullName}
                                    </span>
                                </h2>
                                <p>
                                    <I18n id="login.step1.areNotYou" />{" "}
                                    <Button
                                        className="btn btn-asLink"
                                        onClick={this.reset}
                                        label="login.step1.forget"
                                        block={false}
                                    />
                                </p>
                            </div>
                        ) : (
                            <Fragment>
                                <Field
                                    hideLabel={isDesktop}
                                    idForm={FORM_ID}
                                    name="username"
                                    type="email"
                                    component={TextField}
                                    autoFocus={isDesktop}
                                />
                                <Field
                                    idForm={FORM_ID}
                                    name="rememberEmail"
                                    component={Checkbox}
                                    checked={values.rememberEmail}
                                />
                            </Fragment>
                        )}
                    </Col>
                </Container>
                <Container className="align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <Button type="submit" bsStyle="primary" label="global.continue" loading={isFetching} />
                        {!fromOnboardingLoginData && (
                            <p className="text-center">
                                <I18n id="login.footer.noUser" />{" "}
                                <Link to="/noUser" onClick={() => Mixpanel.track("login.noUser")}>
                                    <I18n id="login.footer.createUser" componentProps={{ "aria-hidden": "true" }} />
                                    <span className="visually-hidden">{i18n.get("login.footer.createUser.a11y")}</span>
                                </Link>
                            </p>
                        )}
                    </Col>

                    {!isDesktop && (
                        <Col sm={12} md={9} className="col col-12 footer--mobile">
                            <LanguageSelectionLink />

                            <RegionSelectionLink />
                        </Col>
                    )}
                </Container>
                {footer}
            </Form>
        );
    }
}

const mapStateToProps = (state) => ({
    ...loginSelectors.getRememberedUser(state),
    fingerprintAvailable: fingerprintSelectors.getAvailability(state),
    isFetching: loginSelectors.getFetching(state),
    isFromAmazon: assistantSelectors.isFromAmazon(state),
    isFromGoogle: assistantSelectors.isFromGoogle(state),
    isFromMessenger: assistantSelectors.isFromMessenger(state),
    isFromWhatsapp: assistantSelectors.isFromWhatsapp(state),
    fromOnboardingLoginData: loginSelectors.getFromOnboardingLoginData(state),
    fingerPrintEnvironmentRestricted: loginSelectors.getFingerPrintEnvironmentRestricted(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        enableReinitialize: true,
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: (props) => ({
            username: props.username || "",
            rememberEmail: false,
        }),
        validationSchema: () =>
            Yup.object().shape({
                username: Yup.string()
                    .required(i18n.get("onboarding.step4.email.error.empty"))
                    .test("format", i18n.get("onboarding.step4.email.error.invalidFormat"), (value) =>
                        emailRegex.test(value),
                    ),
            }),
        handleSubmit: ({ username, rememberEmail }, formikBag) => {
            Mixpanel.identify(username);
            Mixpanel.track(FORM_ID, { rememberEmail });
            formikBag.props.dispatch(loginActions.loginStep1(username, rememberEmail, formikBag));
        },
    }),
)(Step1Content);
