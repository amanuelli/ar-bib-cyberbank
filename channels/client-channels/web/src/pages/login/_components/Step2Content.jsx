import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import Col from "react-bootstrap/lib/Col";
import { Link } from "react-router-dom";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";
import { string, bool, number } from "prop-types";

import { actions as loginActions, selectors as loginSelectors } from "reducers/login";
import * as i18n from "util/i18n";
import * as configUtils from "util/config";

import Credential from "pages/_components/fields/credentials/Credential";
import Container from "pages/_components/Container";
import I18n from "pages/_components/I18n";
import Button from "pages/_components/Button";
import Captcha from "pages/_components/fields/credentials/Captcha";
import { resizableRoute } from "pages/_components/Resizable";
import { Mixpanel } from "util/clickstreaming";

const FORM_ID = "login.step2";

class Step2Content extends Component {
    static propTypes = {
        isMobileNative: bool.isRequired,
        securitySeal: number.isRequired,
        securitySealAlt: string.isRequired,
        isDesktop: bool.isRequired,
        showCaptcha: bool.isRequired,
        isSubmitting: bool.isRequired,
    };

    render() {
        const { securitySeal, securitySealAlt, showCaptcha, isMobileNative, isDesktop, isSubmitting } = this.props;
        return (
            <Form className="above-the-fold login-desktop-wrapper">
                <Container className="container--layout flex-grow align-items-center">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <div className="text-lead security-image">
                            <div id="figureId" className="image">
                                <figure>
                                    <img
                                        aria-describedBy="verifyImage"
                                        // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
                                        tabIndex="0"
                                        src={securitySeal}
                                        alt={i18n.get(securitySealAlt)}
                                    />
                                </figure>
                            </div>
                            <p id="verifyImage">
                                <I18n id="login.step2.verifyYourImage" />
                            </p>
                        </div>
                        <Field
                            hideLabel={isDesktop}
                            idForm={FORM_ID}
                            name="password"
                            copyEnabled={false}
                            component={Credential}
                        />
                        {showCaptcha && !isMobileNative && (
                            <Field idForm={FORM_ID} name="captcha" component={Captcha} />
                        )}
                        <p>
                            <Link
                                to="/recoveryPassword/step1"
                                onClick={() => Mixpanel.track("login.recoveryPassword.step1")}>
                                <I18n id="login.step2.forgotPassword" />
                            </Link>
                        </p>
                    </Col>
                </Container>
                <Container className="align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <div className="form-group">
                            <Button type="submit" bsStyle="primary" label="login.comeIn" loading={isSubmitting} />
                        </div>
                    </Col>
                </Container>
            </Form>
        );
    }
}

const mapStateToProps = (state) => ({
    securitySeal: loginSelectors.getSecuritySeal(state),
    securitySealAlt: loginSelectors.getSecuritySealAlt(state),
    showCaptcha: loginSelectors.getShowCaptcha(state),
});

export default compose(
    connect(mapStateToProps),
    resizableRoute,
    withFormik({
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: () => ({
            password: "",
            captcha: "",
        }),
        validationSchema: ({ isMobileNative, showCaptcha }) =>
            Yup.object().shape({
                password: Yup.string().required(i18n.get(`${FORM_ID}.password.required`)),
                captcha:
                    !isMobileNative && showCaptcha
                        ? Yup.string().required(i18n.get(`${FORM_ID}.captcha.required`))
                        : Yup.string(),
            }),
        handleSubmit: ({ password, captcha }, formikBag) => {
            if (
                configUtils.get("core.sessionHandler.componentFQN") ===
                "com.technisys.omnichannel.core.session.DbSessionHandler"
            ) {
                formikBag.props.dispatch(loginActions.loginStep2(password, captcha, formikBag));
            } else {
                formikBag.props.dispatch(loginActions.oauth(password, captcha, formikBag));
            }
            Mixpanel.track(FORM_ID);
        },
    }),
)(Step2Content);
