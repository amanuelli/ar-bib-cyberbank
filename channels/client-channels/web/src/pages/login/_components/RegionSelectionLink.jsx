import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { string, bool, func } from "prop-types";
import { selectors as loginSelectors, actions as loginActions } from "reducers/login";
import * as i18n from "util/i18n";

class RegionSelectionLink extends Component {
    static propTypes = {
        dispatch: func,
        activeRegion: string,
        disabled: bool,
    };

    static defaultProps = {
        activeRegion: "",
        disabled: false,
        dispatch: () => {},
    };

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(loginActions.getClientContry());
    }

    handleClick = (event) => {
        const { disabled } = this.props;

        if (disabled) {
            event.preventDefault();
        }
    };

    render() {
        const { activeRegion } = this.props;
        const a11yMessage1 = i18n.get("settings.changeRegion.link.a11y");
        const a11yMessage2 = i18n.get(`settings.changeRegion.label.${activeRegion}.a11y`);
        if (activeRegion) {
            return (
                <Link onClick={this.handleClick} style={{ display: "flex" }} to="/regionSelection">
                    <span className="visually-hidden">{`${a11yMessage1}, ${a11yMessage2}`}</span>
                    <span aria-hidden="true">{i18n.get(`settings.regions.label.${activeRegion}`)}</span>
                </Link>
            );
        }
        return null;
    }
}

const mapStateToProps = (state) => ({
    activeRegion: loginSelectors.getRegion(state),
});

export default connect(mapStateToProps)(RegionSelectionLink);
