import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import Col from "react-bootstrap/lib/Col";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";
import { string, bool, shape } from "prop-types";

import { actions as loginActions, selectors as loginSelectors } from "reducers/login";
import { selectors as assistantSelectors } from "reducers/assistant";
import * as i18n from "util/i18n";

import I18n from "pages/_components/I18n";
import { Link } from "react-router-dom";
import Container from "pages/_components/Container";
import SwitchField from "pages/_components/fields/formik/SwitchField";
import Button from "pages/_components/Button";
import { Mixpanel } from "util/clickstreaming";

const FORM_ID = "login.step4";

class Step4Content extends Component {
    render() {
        const { termsAndConditions, isSubmitting, isDesktop } = this.props;

        return (
            <Form className="above-the-fold login-desktop-wrapper">
                <Container className="container--layout flex-grow align-items-center">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        {isDesktop && (
                            <p // eslint-disable-next-line
                                dangerouslySetInnerHTML={{ __html: termsAndConditions && termsAndConditions.body }}
                            />
                        )}

                        {!isDesktop && (
                            <Link to="/termsAndConditions">
                                <I18n component="span" id="global.termAndConditions" />
                            </Link>
                        )}
                    </Col>
                </Container>
                <Container className="align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <Field name="acceptConditions" idForm={FORM_ID} component={SwitchField} formGroup />
                        <Button type="submit" loading={isSubmitting} label="login.comeIn" bsStyle="primary" />
                    </Col>
                </Container>
            </Form>
        );
    }
}

Step4Content.propTypes = {
    isSubmitting: bool,
    isDesktop: bool,
    termsAndConditions: shape({
        body: string,
    }).isRequired,
};

Step4Content.defaultProps = {
    isSubmitting: false,
    isDesktop: true,
};

const mapStateToProps = (state) => ({
    termsAndConditions: loginSelectors.getTermsAndConditions(state),
    isAssistantLogin: assistantSelectors.isAssistantLogin(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: () => ({ acceptConditions: false }),
        validationSchema: () =>
            Yup.object().shape({
                acceptConditions: Yup.boolean().oneOf([true], i18n.get(`${FORM_ID}.acceptConditions.required`)),
            }),
        handleSubmit: ({ acceptConditions }, formikBag) => {
            Mixpanel.track(FORM_ID, { acceptConditions });
            formikBag.props.dispatch(loginActions.loginStep4(acceptConditions, formikBag));
        },
    }),
)(Step4Content);
