import React, { Component } from "react";
import Col from "react-bootstrap/lib/Col";

import Container from "pages/_components/Container";
import I18n from "pages/_components/I18n";

class FooterMobile extends Component {
    render() {
        return (
            <Container className="container--layout container--bottom" gridClassName="form-content">
                <Col sm={12} className="col col-12">
                    <p className="text-center">
                        <small>
                            <I18n id="login.footer.needHelp" />{" "}
                            <a href="/loginStep1">
                                <I18n id="login.footer.reviewFQ" />
                            </a>
                        </small>
                    </p>
                </Col>
            </Container>
        );
    }
}

export default FooterMobile;
