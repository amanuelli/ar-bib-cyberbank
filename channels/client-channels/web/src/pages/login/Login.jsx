import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import classNames from "classnames";
import { bool, func } from "prop-types";

import { actions as loginActions, selectors as loginSelectors } from "reducers/login";

import Notification from "pages/_components/Notification";
import Step0Content from "pages/login/_components/Step0Content";
import MainContainer from "pages/_components/MainContainer";
import { selectors as weatherSelectors } from "reducers/weather";

class Login extends Component {
    static propTypes = {
        isDesktop: bool.isRequired,
        isMobile: bool.isRequired,
        dispatch: func.isRequired,
    };

    state = {
        initialized: false,
    };

    componentDidMount() {
        const { dispatch } = this.props;
        this.checkInitialization(this.props);
        dispatch(loginActions.getClientContry());
    }

    checkInitialization = (props) => {
        const { initialized } = this.state;
        const { dispatch, weather } = props;

        if (!initialized && weather) {
            dispatch(loginActions.initLoginFlow());
            this.setState({ initialized: true });
            window.common.hideSplashScreen();
        }
    };

    render() {
        const { initialized } = this.state;
        const { isDesktop, isMobile } = this.props;

        const mainContainerClass = classNames({
            "main-container": true,
            "container-fluid": isDesktop,
            "login-splash": isMobile,
        });

        return (
            <Fragment>
                <Notification scopeToShow="login" />
                <MainContainer className={mainContainerClass} showLoader={!initialized}>
                    {initialized && <Step0Content />}
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    weather: weatherSelectors.getWeather(state),
    rememberedUser: loginSelectors.getRememberedUser(state),
});

export default withRouter(connect(mapStateToProps)(Login));
