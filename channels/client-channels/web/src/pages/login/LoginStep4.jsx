import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import classNames from "classnames";
import { bool, func } from "prop-types";

import { actions as loginActions, selectors as loginSelectors } from "reducers/login";
import { actions as sessionActions, selectors as sessionSelectors } from "reducers/session";

import Notification from "pages/_components/Notification";
import Step4Content from "pages/login/_components/Step4Content";
import Head from "pages/_components/Head";
import MainContainer from "pages/_components/MainContainer";
import { push } from "react-router-redux/actions";

class LoginStep4 extends Component {
    static propTypes = {
        isDesktop: bool.isRequired,
        dispatch: func.isRequired,
        termsAndConditions: bool.isRequired,
    };

    componentDidMount() {
        const { dispatch, termsAndConditions } = this.props;

        if (!termsAndConditions) {
            dispatch(push("/desktop"));
        }
    }

    onHeaderClose = () => {
        const { dispatch } = this.props;
        dispatch(loginActions.reset());
        dispatch(sessionActions.logout());
        dispatch(push("/"));
    };

    render() {
        const { isDesktop } = this.props;

        return (
            <Fragment>
                <Head title="login.step4.header" onClose={this.onHeaderClose} />
                <Notification scopeToShow="loginStep4" />
                <MainContainer className={classNames("main-container", { "container-fluid": isDesktop })}>
                    <Step4Content isDesktop={isDesktop} />
                </MainContainer>
            </Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    hasActiveSession: sessionSelectors.isLoggedIn(state),
    termsAndConditions: loginSelectors.getTermsAndConditions(state),
});
export default connect(mapStateToProps)(LoginStep4);
