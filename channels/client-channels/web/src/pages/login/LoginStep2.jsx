import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import classNames from "classnames";
import { bool, string, func, shape } from "prop-types";

import { actions as loginActions, selectors as loginSelectors } from "reducers/login";
import { selectors as sessionSelectors } from "reducers/session";

import Notification from "pages/_components/Notification";
import Step2Content from "pages/login/_components/Step2Content";
import Head from "pages/_components/Head";
import Button from "pages/_components/Button";
import MainContainer from "pages/_components/MainContainer";
import withExchangeToken from "pages/_components/withExchangeToken";
import TooltipTour from "pages/forms/_components/_fields/TooltipTour";
import * as i18n from "util/i18n";
import { push } from "react-router-redux/actions";

let stepsTour = [];
let runTooltipTour = false;

class LoginStep2 extends Component {
    componentDidMount() {
        const { dispatch, firstLog, activeEnvironment, username } = this.props;
        if (activeEnvironment) {
            dispatch(push("/desktop"));
            return;
        }
        if (!username) {
            dispatch(push("/"));
            return;
        }
        stepsTour.push({
            selector: "figureId",
            title: "tooltipTour.login.securityImage.title",
            content: i18n.get("tooltipTour.login.securityImage.content"),
        });
        runTooltipTour = firstLog;
    }

    componentWillUnmount() {
        stepsTour = [];
        runTooltipTour = false;
    }

    onHeaderClose = () => {
        const { dispatch } = this.props;
        dispatch(loginActions.reset());
        dispatch(push("/"));
    };

    render() {
        const { isDesktop, isInFlow, fromOnboardingLoginData } = this.props;
        return (
            <>
                <TooltipTour
                    steps={stepsTour}
                    run={runTooltipTour}
                    showNavigation={false}
                    showCloseButton={false}
                    showNumber={false}
                    closeWithMask={false}
                    lastStepNextButton={
                        <Button className="btn btn-outline btn-small">
                            {i18n.get("tooltipTour.lastButton.label")}
                        </Button>
                    }
                />
                <Notification scopeToShow="loginStep2" />
                {isInFlow && (
                    <>
                        {!fromOnboardingLoginData ? (
                            <Head
                                title="login.step2.header"
                                onClose={this.onHeaderClose}
                                accessibilityTextId="login.step2.header.a11y"
                            />
                        ) : (
                            <Head title="onboarding.login.isThisYourImage" onClose={this.onHeaderClose} />
                        )}
                        <MainContainer className={classNames("main-container", { "container-fluid": isDesktop })}>
                            <Step2Content />
                        </MainContainer>
                    </>
                )}
            </>
        );
    }
}

LoginStep2.propTypes = {
    isDesktop: bool.isRequired,
    isInFlow: bool.isRequired,
    dispatch: func.isRequired,
    fromOnboardingLoginData: shape({}),
    firstLog: bool.isRequired,
    username: string.isRequired,
    activeEnvironment: shape({}).isRequired,
};

LoginStep2.defaultProps = {
    fromOnboardingLoginData: null,
};

const mapStateToProps = (state) => ({
    isInFlow: loginSelectors.getIsInFlow(state),
    fromOnboardingLoginData: loginSelectors.getFromOnboardingLoginData(state),
    firstLog: !loginSelectors.getHasLogged(state),
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    username: loginSelectors.getUsername(state),
});

export default compose(withRouter, connect(mapStateToProps))(withExchangeToken(LoginStep2));
