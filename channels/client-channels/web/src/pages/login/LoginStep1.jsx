import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { routerActions } from "react-router-redux/actions";
import Grid from "react-bootstrap/lib/Grid";
import Row from "react-bootstrap/lib/Row";
import Col from "react-bootstrap/lib/Col";

import { selectors as loginSelectors } from "reducers/login";
import { selectors as assistantSelectors } from "reducers/assistant";
import { actions as sessionActions, selectors as sessionSelectors } from "reducers/session";

import I18n from "pages/_components/I18n";
import Notification from "pages/_components/Notification";
import Step1Content from "pages/login/_components/Step1Content";
import Head from "pages/_components/Head";
import MainContainer from "pages/_components/MainContainer";
import Step0Content from "pages/login/_components/Step0Content";
import AssistantAlert from "pages/login/_components/AssistantAlert";
import { bool, func, shape, string } from "prop-types";

class LoginStep1 extends Component {
    static propTypes = {
        isDesktop: bool.isRequired,
        dispatch: func.isRequired,
        isFromAmazon: bool,
        isFromGoogle: bool,
        isFromMessenger: bool,
        isFromWhatsapp: bool,
        fingerprintSubmiting: shape({}),
        rememberedUser: bool,
        activeRegion: string.isRequired,
        hasActiveSession: bool.isRequired,
    };

    static defaultProps = {
        isFromAmazon: false,
        isFromGoogle: false,
        isFromMessenger: false,
        isFromWhatsapp: false,
        fingerprintSubmiting: null,
        rememberedUser: false,
    };

    componentDidMount() {
        const { dispatch, hasActiveSession, fingerprintSubmiting } = this.props;
        if (hasActiveSession && !fingerprintSubmiting) {
            dispatch(sessionActions.logout()); // End any previous session open session
        }
    }

    onHeaderClose = () => {
        const { dispatch, isDesktop } = this.props;
        if (!isDesktop) {
            dispatch(routerActions.push("/"));
        }
    };

    mobileView = () => <Step1Content className="above-the-fold" {...this.props} />;

    desktopView = () => {
        const { isFromAmazon, isFromGoogle, isFromMessenger, isFromWhatsapp } = this.props;

        return (
            <div className="above-the-fold">
                <section className="container--layout align-items-center">
                    <Grid className="form-content">
                        {(isFromAmazon || isFromGoogle || isFromMessenger || isFromWhatsapp) && <AssistantAlert />}
                        <p className="text-lead">
                            <I18n id="login.step1.chooseLoginType" />
                        </p>
                        <Row className="justify-content-center login-desktop-cols">
                            <Col className="col col-6">
                                <Step1Content {...this.props} />
                            </Col>

                            <Col className="col col-6">
                                <Step0Content />
                            </Col>
                        </Row>
                    </Grid>
                </section>
            </div>
        );
    };

    getUserNameFormated = (firstName) => firstName.split(" ")[0];

    render() {
        const { isDesktop, fingerprintSubmiting, rememberedUser } = this.props;

        return (
            <Fragment>
                {!isDesktop ? (
                    <Head title="login.step1.header" onClose={rememberedUser === null && this.onHeaderClose} />
                ) : (
                    <div>
                        <Head title="login.step1.header" accessibilityTextId="login.step1.header.a11y" />
                    </div>
                )}

                <Notification scopeToShow="login" />
                <MainContainer showLoader={fingerprintSubmiting} shouldHideOnLoad className="main-container">
                    {isDesktop ? this.desktopView() : this.mobileView()}
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    fingerprintSubmiting: loginSelectors.getFingerprintSubmiting(state),
    isAssistantLogin: assistantSelectors.isAssistantLogin(state),
    isFromAmazon: assistantSelectors.isFromAmazon(state),
    isFromGoogle: assistantSelectors.isFromGoogle(state),
    isFromMessenger: assistantSelectors.isFromMessenger(state),
    isFromWhatsapp: assistantSelectors.isFromWhatsapp(state),
    rememberedUser: loginSelectors.getRememberedUser(state),
    activeRegion: loginSelectors.getRegion(state),
    hasActiveSession: sessionSelectors.isLoggedIn(state),
});

export default connect(mapStateToProps)(LoginStep1);
