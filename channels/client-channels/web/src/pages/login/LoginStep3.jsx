import React, { Component } from "react";
import { compose } from "redux";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import classNames from "classnames";
import { bool, string, func, number } from "prop-types";

import { actions as loginActions, selectors as loginSelectors } from "reducers/login";
import { selectors as i18nSelectors } from "reducers/i18n";
import { actions as sessionActions, selectors as sessionSelectors } from "reducers/session";

import Notification from "pages/_components/Notification";
import Step3Content from "pages/login/_components/Step3Content";
import Head from "pages/_components/Head";
import MainContainer from "pages/_components/MainContainer";
import withExchangeToken from "pages/_components/withExchangeToken";
import { push } from "react-router-redux/actions";
import WidgetLoading from "pages/_components/WidgetLoading";

class LoginStep3 extends Component {
    componentDidMount() {
        const { dispatch, activeEnvironment, username } = this.props;
        if (activeEnvironment) {
            dispatch(push("/desktop"));
        } else if (!username) {
            dispatch(push("/"));
        }
    }

    onHeaderClose = () => {
        const { dispatch } = this.props;
        dispatch(sessionActions.logout());
        dispatch(loginActions.reset());
        dispatch(push("/"));
    };

    render() {
        const { isDesktop, userFirstName, isFetchingLang } = this.props;

        return (
            <>
                {!isFetchingLang && (
                    <Head
                        title="login.step3.header"
                        replace={{
                            USER_NAME:
                                userFirstName && userFirstName.length > 1
                                    ? userFirstName
                                          .split(" ")
                                          .map((x) => x.charAt(0).toUpperCase() + x.slice(1).toLowerCase())
                                          .join(" ")
                                    : "",
                        }}
                        accessibilityTextId="activities.session.login.step3.a11y"
                        onClose={this.onHeaderClose}
                    />
                )}
                <Notification scopeToShow="loginStep3" />
                <MainContainer className={classNames("main-container", { "container-fluid": isDesktop })}>
                    {isFetchingLang ? <WidgetLoading loading /> : <Step3Content />}
                </MainContainer>
            </>
        );
    }
}

LoginStep3.propTypes = {
    isDesktop: bool.isRequired,
    userFirstName: string.isRequired,
    dispatch: func.isRequired,
    username: string.isRequired,
    activeEnvironment: number,
    isFetchingLang: bool.isRequired,
};

LoginStep3.defaultProps = {
    activeEnvironment: null,
};

const mapStateToProps = (state) => ({
    userFirstName: loginSelectors.getUserFirstName(state),
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    username: loginSelectors.getUsername(state),
    isFetchingLang: i18nSelectors.getFetching(state),
});

export default compose(withRouter, connect(mapStateToProps))(withExchangeToken(LoginStep3));
