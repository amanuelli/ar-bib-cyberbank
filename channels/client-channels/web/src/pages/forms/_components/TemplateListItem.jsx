import React, { Component } from "react";
import { shape, string, func, number } from "prop-types";

import Image from "pages/_components/Image";
import FormattedDate from "pages/_components/FormattedDate";
import List, { withList } from "pages/_components/List";
import * as utilsI18n from "util/i18n";

class TemplateItem extends Component {
    static propTypes = {
        item: shape({
            name: string.isRequired,
        }).isRequired,
        onDelete: func.isRequired,
        num: number.isRequired,
    };

    handleClick = (event) => {
        const { item, onDelete } = this.props;
        event.stopPropagation();
        onDelete(item);
    };

    render() {
        const { item, num } = this.props;
        return (
            <List.Item num={num} className="list-item--deleteable" role="option" name="Plantillas">
                {(Button) => (
                    <div className="list-item-inner">
                        <Button className="btn btn-quiet" aria-describedBy="ItemBtnDescription">
                            <small aria-hidden>
                                <FormattedDate date={item.creationDate} />
                            </small>
                            <span className="visually-hidden">{utilsI18n.get("forms.templates.apply")}</span>
                            <span>{item.name}</span>
                        </Button>
                        <span id="ItemBtnDescription" className="visually-hidden">
                            {utilsI18n.get("forms.templates.creationDate")} {item.creationDate}
                        </span>
                        <button
                            type="button"
                            className="close close--absolute "
                            aria-label={`${utilsI18n.get("file.upload.input.labelButtonRemoveItem")} ${item.name}`}
                            onClick={this.handleClick}>
                            <Image className="icon-trash" src="delete-message.svg" />
                        </button>
                    </div>
                )}
            </List.Item>
        );
    }
}

export default withList(TemplateItem);
