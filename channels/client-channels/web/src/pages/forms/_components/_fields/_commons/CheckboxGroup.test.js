import React from "react";
import { mount } from "enzyme";

import CheckboxGroup from "./CheckboxGroup";

const props = {
    values: [],
    options: [{ id: "id1", label: "label1" }, { id: "id2", label: "label2" }],
};

it("renders unchecked options", () => {
    const onChange = jest.fn();
    const group = mount(<CheckboxGroup {...props} onChange={onChange} />);
    expect(group.find("input")).toHaveLength(2);
    expect(group.find("input").filterWhere((i) => i.getElement().props.checked)).toHaveLength(0);

    group.find("input[value='id1']").simulate("click");
    expect(onChange).toHaveBeenCalledWith(["id1"]);
});

it("renders checked options", () => {
    const onChange = jest.fn();
    const group = mount(<CheckboxGroup {...props} values={["id1", "id2"]} onChange={onChange} />);
    expect(group.find("input")).toHaveLength(2);
    expect(group.find("input").filterWhere((i) => i.getElement().props.checked)).toHaveLength(2);

    group.find("input[value='id1']").simulate("click");
    expect(onChange).toHaveBeenCalledWith(["id2"]);
});
