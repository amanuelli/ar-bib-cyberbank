import React from "react";
import { mount } from "enzyme";
import { defaultFieldProps, TestForm } from "./_commons/mock";

import Termsandconditions from "./Termsandconditions";

const fieldProps = {
    showAcceptOption: true,
    showAcceptOptionTextMap: { es: "accept" },
    termsAndConditionsMap: { es: "terms" },
};

const props = { ...defaultFieldProps, component: Termsandconditions, ...fieldProps };

it("renders edit mode with accept option", () => {
    const form = mount(<TestForm {...props} />);
    expect(form.find(".input-group-text-area").text()).toBe("terms");
    expect(form.find("Checkbox").props().checked).toBe(false);
});

it.only("renders edit mode without accept option - disclaimer", () => {
    const form = mount(<TestForm {...props} showAcceptOption={false} />);
    expect(form.find(".input-group-text-area").text()).toBe("terms");
    expect(form.find("Checkbox").exists()).toBe(false);
    expect(form.find("Termsandconditions").props().value).toBe(true);
});

it("renders view mode", () => {
    const form = mount(<TestForm {...props} mode="view" defaultValue />);
    expect(form.find(".input-group-text-area").text()).toBe("terms");
    expect(form.find("Checkbox").exists()).toBe(false);
});
