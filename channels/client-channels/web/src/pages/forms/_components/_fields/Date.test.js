import React from "react";
import { mount } from "enzyme";
import { defaultFieldProps, TestForm } from "./_commons/mock";

import Date from "./Date";

const datePicker = (form) => form.find("DatePicker");
const datePickerInput = (form) => datePicker(form).find("input");
const datePickerValue = (form) => datePickerInput(form).instance().value;
const dateValue = (form) => form.find("Date").props().field.value;

describe("renders edit mode", () => {
    let form;
    const props = { ...defaultFieldProps, component: Date, dateFormat: "DD-MM-YYYY" };

    beforeEach(() => {
        form = mount(<TestForm {...props} />);
        expect(form.find("DatePicker").exists()).toBe(true);
    });

    it("displays correctly - no initial value", () => {
        expect(datePickerValue(form)).toBe("");
        expect(dateValue(form)).toBeUndefined();
    });

    it("changes selected date", () => {
        datePickerInput(form).simulate("change", { target: { value: "31-12-2017" } });
        expect(datePickerValue(form)).toBe("31-12-2017");
        expect(dateValue(form)).toBe("2017-12-31");
    });
});

it("renders edit mode - readOnly", () => {
    const props = {
        ...defaultFieldProps,
        component: Date,
        readOnly: true,
        defaultValue: "2018-01-01",
        dateFormat: "DD-MM-YYYY",
    };

    const form = mount(<TestForm {...props} />);

    expect(form.find("DatePicker").exists()).toBe(false);
    expect(form.find("Date").text()).toBe("01-01-2018");
});

it("renders view mode", () => {
    const props = {
        ...defaultFieldProps,
        component: Date,
        defaultValue: "2018-01-01",
        dateFormat: "DD-MM-YYYY",
        mode: "view",
    };

    const form = mount(<TestForm {...props} />);
    expect(form.find("Date").text()).toBe("01-01-2018");
});
