import React from "react";
import { mount } from "enzyme";
import { defaultFieldProps, TestForm } from "./_commons/mock";

import Document from "./Document";

const documentCountry = (form) => form.find("Select[name='country']");
const documentType = (form) => form.find("Select[name='documentType']");
const documentNumber = (form) => form.find("input[name='documentNumber']");
const documentValue = (form) => form.find("Document").props().value;
const selectLabel = (select) => select.find(".Select-value-label");

describe("renders edit mode - multiple countries", () => {
    let form;
    const fieldProps = {
        defaultCountry: "BR",
        defaultDocumentType: "PAS",
        data: {
            countries: [{ id: "BR", name: "Brasil" }, { id: "UY", name: "Uruguay" }],
            documentCountryMap: {
                BR: [{ id: "PAS", name: "Passaporte" }, { id: "CPF", name: "name.CPF" }],
                UY: [{ id: "PAS", name: "Pasaporte" }, { id: "CI", name: "name.CI" }],
            },
        },
    };

    const props = { ...defaultFieldProps, component: Document, ...fieldProps };

    beforeEach(() => {
        form = mount(<TestForm {...props} />);
        expect(documentCountry(form).exists()).toBe(true);
        expect(documentType(form).exists()).toBe(true);
        expect(documentNumber(form).exists()).toBe(true);

        documentNumber(form).simulate("change", { target: { value: "1234" } });

        expect(selectLabel(documentCountry(form)).text()).toBe("Brasil");
        expect(selectLabel(documentType(form)).text()).toBe("Passaporte");
        expect(documentNumber(form).instance().value).toBe("1234");
    });

    it("displays correctly", () => {
        expect(documentValue(form)).toMatchObject({
            document: "1234",
            documentType: "PAS",
            documentCountry: "BR",
        });
    });

    it("changes selected country", () => {
        // selecting second country...
        documentCountry(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentCountry(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentCountry(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 13 });

        expect(selectLabel(documentCountry(form)).text()).toBe("Uruguay");
        expect(selectLabel(documentType(form)).text()).toBe("Pasaporte");
        expect(documentNumber(form).instance().value).toBe("1234");

        expect(documentValue(form)).toMatchObject({
            document: "1234",
            documentType: "PAS",
            documentCountry: "UY",
        });
    });

    it("changes selected type", () => {
        // selecting second type value...
        documentType(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentType(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentType(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 13 });

        expect(selectLabel(documentCountry(form)).text()).toBe("Brasil");
        expect(selectLabel(documentType(form)).text()).toBe("name.CPF");
        expect(documentNumber(form).instance().value).toBe("1234");

        expect(documentValue(form)).toMatchObject({
            document: "1234",
            documentType: "CPF",
            documentCountry: "BR",
        });
    });

    it("reset document number when country change to incompatible type", () => {
        // selecting second type value...
        documentType(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentType(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentType(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 13 });

        expect(selectLabel(documentCountry(form)).text()).toBe("Brasil");
        expect(selectLabel(documentType(form)).text()).toBe("name.CPF");
        expect(documentNumber(form).instance().value).toBe("1234");

        expect(documentValue(form)).toMatchObject({
            document: "1234",
            documentType: "CPF",
            documentCountry: "BR",
        });

        // selecting second country value...
        documentCountry(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentCountry(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentCountry(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 13 });

        expect(documentValue(form)).toMatchObject({
            document: "",
            documentType: "PAS",
            documentCountry: "UY",
        });
    });

    it("keep document number when country change to incompatible type", () => {
        // selecting second country value...
        documentCountry(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentCountry(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 40 });
        documentCountry(form)
            .find(".Select-control")
            .simulate("keyDown", { keyCode: 13 });

        expect(documentValue(form)).toMatchObject({
            document: "1234",
            documentType: "PAS",
            documentCountry: "UY",
        });
    });
});

describe("renders edit mode - single country", () => {
    let form;
    const fieldProps = {
        defaultCountry: "BR",
        defaultDocumentType: "PAS",
        data: {
            countries: [{ id: "BR", name: "Brasil" }],
            documentCountryMap: {
                BR: [{ id: "PAS", name: "Passaporte" }, { id: "CPF", name: "name.CPF" }],
            },
        },
    };

    const props = { ...defaultFieldProps, component: Document, ...fieldProps };

    beforeEach(() => {
        form = mount(<TestForm {...props} />);
        expect(documentCountry(form).exists()).toBe(false);
        expect(documentType(form).exists()).toBe(true);
        expect(documentNumber(form).exists()).toBe(true);

        documentNumber(form).simulate("change", { target: { value: "1234" } });

        expect(selectLabel(documentType(form)).text()).toBe("Passaporte");
        expect(documentNumber(form).instance().value).toBe("1234");

        expect(documentValue(form)).toMatchObject({
            document: "1234",
            documentType: "PAS",
            documentCountry: "BR",
        });
    });

    it("displays correctly", () => {
        expect(documentValue(form)).toMatchObject({
            document: "1234",
            documentType: "PAS",
            documentCountry: "BR",
        });
    });
});

describe("renders view mode", () => {
    const fieldProps = {
        defaultCountry: "BR",
        defaultDocumentType: "PAS",
        data: {
            countries: [{ id: "BR", name: "Brasil" }, { id: "UY", name: "Uruguay" }],
            documentCountryMap: {
                BR: [{ id: "PAS", name: "Passaporte" }, { id: "CPF", name: "name.CPF" }],
                UY: [{ id: "PAS", name: "Pasaporte" }, { id: "CI", name: "name.CI" }],
            },
        },
    };

    const props = {
        ...defaultFieldProps,
        component: Document,
        mode: "view",
        ...fieldProps,
        defaultValue: {
            documentCountry: "BR",
            documentType: "CPF",
            document: "1234",
        },
    };

    it("displays default country correctly", () => {
        const form = mount(<TestForm {...props} />);

        expect(documentValue(form)).toMatchObject({
            document: "1234",
            documentType: "CPF",
            documentCountry: "BR",
        });

        expect(
            form
                .find("Document")
                .find("span")
                .text(),
        ).toBe("name.CPF  1234");
    });

    it("displays non-default country correctly", () => {
        const form = mount(<TestForm {...props} defaultCountry="UY" />);

        expect(documentValue(form)).toMatchObject({
            document: "1234",
            documentType: "CPF",
            documentCountry: "BR",
        });

        expect(
            form
                .find("Document")
                .find("span")
                .text(),
        ).toBe("name.CPF Brasil 1234");
    });
});
