import React from "react";
import { mount } from "enzyme";
import { defaultFieldProps, TestForm } from "./_commons/mock";

import Countrylist from "./Countrylist";

const fieldProps = {
    data: {
        countries: [{ id: "id1", name: "country1" }, { id: "id2", name: "country2" }],
    },
};

const props = { ...defaultFieldProps, component: Countrylist, ...fieldProps };

it("renders edit mode", () => {
    const form = mount(<TestForm {...props} defaultValue="id1" />);
    expect(form.find(".Select-value-label").text()).toBe("country1");

    expect(form.find("Countrylist").props().value).toBe("id1");

    // selecting second value...
    form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
    form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
    form.find(".Select-control").simulate("keyDown", { keyCode: 13 });

    expect(form.find(".Select-value-label").text()).toBe("country2");

    expect(form.find("Countrylist").props().value).toBe("id2");
});

it("renders view mode", () => {
    const form = mount(<TestForm {...props} mode="view" defaultValue="id1" />);
    expect(form.find("Countrylist").text()).toBe("country1");
});
