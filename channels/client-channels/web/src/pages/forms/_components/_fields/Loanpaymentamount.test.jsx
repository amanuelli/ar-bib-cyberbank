import React from "react";
import { mount } from "enzyme";

import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { history, store } from "store";

import { Formik, Field, Form } from "formik";
import { defaultFieldProps } from "./_commons/mock";
import LoanPaymentAmount from "./Loanpaymentamount";

const loanPayInputs = (form) => form.find("LoanPaymentAmount").find("input");
const loanPayTotal = (form) =>
    form
        .find("LoanPaymentAmount")
        .find("FormattedAmount")
        .last()
        .props().quantity;
const loanPayCurrency = (form) =>
    form
        .find("LoanPaymentAmount")
        .find("FormattedAmount")
        .last()
        .props().currency;
const loanInput = (form) => form.find("Field[name='loan']");

describe("renders edit mode", () => {
    let form;
    const props = {
        ...defaultFieldProps,
        component: LoanPaymentAmount,
        dependsOnProduct: "loan",
    };

    const fieldList = [
        {
            idField: "loan",
            data: {
                options: [
                    {
                        id: "loan1",
                        currency: "BRL",
                        statementsLoan: [
                            { importAmount: 5, feeNumber: 1, dueDate: "2018-01-01" },
                            { importAmount: 5, feeNumber: 2, dueDate: "2018-02-01" },
                            { importAmount: 5, feeNumber: 3, dueDate: "2018-03-01" },
                        ],
                    },
                    { id: "loan2" },
                ],
            },
        },
        { idField: "loanPay" },
    ];

    beforeEach(() => {
        mount(
            // eslint-disable-next-line react/jsx-filename-extension
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Formik
                        onSubmit={jest.fn}
                        initialValues={{ loan: { value: "loan1" }, loanPay: { quantity: 0, currency: "UYU" } }}>
                        <Form>
                            <Field name="loan" />
                            <Field name="loanPay" {...props} fieldList={fieldList} />
                        </Form>
                    </Formik>
                </ConnectedRouter>
            </Provider>,
        );
    });

    it("displays correctly loan with statements", () => {
        expect(loanPayInputs(form).length).toBe(3);
        expect(form.find("input[checked=true]").length).toBe(0);
        expect(loanPayTotal(form)).toBe(0);
        expect(loanPayCurrency(form)).toBe("UYU");
    });

    it("displays correctly loan without statements", () => {
        loanInput(form).simulate("change", { target: { name: "loan", value: { value: "loan2" } } });
        expect(loanPayInputs(form).length).toBe(0);
        expect(form.find("I18n[id='loanPaymentAmount.noStatements']").exists()).toBe(true);
    });

    it("displays total payment amount", () => {
        loanPayInputs(form)
            .at(0)
            .simulate("change", { target: { value: 1, checked: true } });
        expect(form.find("input[checked=true]").length).toBe(1);
        expect(loanPayTotal(form)).toBe(5);

        loanPayInputs(form)
            .at(1)
            .simulate("change", { target: { value: 2, checked: true } });
        expect(form.find("input[checked=true]").length).toBe(2);
        expect(loanPayTotal(form)).toBe(10);

        loanPayInputs(form)
            .at(2)
            .simulate("change", { target: { value: 3, checked: true } });
        expect(form.find("input[checked=true]").length).toBe(3);
        expect(loanPayTotal(form)).toBe(15);

        loanPayInputs(form)
            .at(0)
            .simulate("change", { target: { value: 1, checked: false } });
        expect(form.find("input[checked=true]").length).toBe(0);
        expect(loanPayTotal(form)).toBe(0);
    });
});

it("renders view mode", () => {
    const props = {
        ...defaultFieldProps,
        component: LoanPaymentAmount,
        dependsOnProduct: "loan",
    };

    const fieldList = [
        {
            idField: "loan",
            data: {
                options: [
                    {
                        id: "loan1",
                        currency: "BRL",
                        statementsLoan: [
                            { importAmount: 5, feeNumber: 1, dueDate: "2018-01-01" },
                            { importAmount: 5, feeNumber: 2, dueDate: "2018-02-01" },
                            { importAmount: 5, feeNumber: 3, dueDate: "2018-03-01" },
                        ],
                    },
                    { id: "loan2" },
                ],
            },
        },
        { idField: "loanPay" },
    ];

    mount(
        <Provider store={store}>
            <Formik onSubmit={jest.fn} initialValues={{ loan: "loan1", loanPay: { quantity: 0, currency: "UYU" } }}>
                <Form>
                    <Field name="loan" />
                    <Field name="loanPay" {...props} fieldList={fieldList} />
                </Form>
            </Formik>
        </Provider>,
    );
});
