import React from "react";
import { Provider } from "react-redux";
import { Formik, Field, Form } from "formik";
import { mount } from "enzyme";
import { store } from "store";
import { defaultFieldProps, mockField, mockForm } from "./_commons/mock";

import Amount from "./Amount";

const fieldProps = {
    data: {
        options: [{ id: "id1", label: "currency1" }, { id: "id2", label: "currency2" }],
        precision: 2,
        thousandsSeparator: ".",
        decimalSeparator: ",",
    },
};

const props = { ...defaultFieldProps, component: Amount, ...fieldProps };

it("renders edit mode", () => {
    const field = mount(
        <Provider store={store}>
            <Formik onSubmit={jest.fn}>
                <Form>
                    <Field name="amount" defaultValue={{ currency: "id1", quantity: 1234 }} {...props} />
                </Form>
            </Formik>
        </Provider>,
    );
    expect(field.find(".Select-value-label").text()).toBe("currency1");
    const quantityField = field.find("input[name='quantity']").instance();

    expect(quantityField.value).toBe("1.234,00");

    quantityField.value = "123";
    field
        .find("Amount")
        .instance()
        .handleChange();

    expect(quantityField.value).toEqual("123,00");

    // selecting second value...
    field.find(".Select-control").simulate("keyDown", { keyCode: 40 });
    field.find(".Select-control").simulate("keyDown", { keyCode: 40 });
    field.find(".Select-control").simulate("keyDown", { keyCode: 13 });

    expect(field.find(".Select-value-label").text()).toBe("currency2");

    expect(field.find("Amount").props().value).toMatchObject({ currency: "id2", quantity: 123 });
});

it("renders view mode", () => {
    const field = mount(
        <Provider store={store}>
            <Formik onSubmit={jest.fn}>
                <Form>
                    <Field name="amount" defaultValue={{ currency: "id1", quantity: 1234 }} {...props} mode="view" />
                </Form>
            </Formik>
        </Provider>,
    );
    expect(field.find("FormattedAmount").props()).toMatchObject({ currency: "id1", quantity: 1234 });
});
