import React from "react";
import { mount } from "enzyme";
import { defaultFieldProps, TestForm } from "./_commons/mock";

import Selector from "./Selector";

const fieldProps = {
    renderAs: "combo",
    optionList: [{ id: "id1", label: "label1" }, { id: "id2", label: "label2" }],
};

const props = { ...defaultFieldProps, component: Selector, ...fieldProps };

it("renders combo edit mode", () => {
    mount(<TestForm {...props} />);
});

it("renders check edit mode", () => {
    const form = mount(<TestForm {...props} renderAs="check" />);
    const checkboxGroup = form.find("CheckboxGroup");
    expect(checkboxGroup.exists()).toBe(true);
    expect(checkboxGroup.find("Checkbox")).toHaveLength(2);
});

it("renders radio edit mode", () => {
    const form = mount(<TestForm {...props} renderAs="radio" />);
    const radioButtonGroup = form.find("RadioButtonGroup");
    expect(radioButtonGroup.exists()).toBe(true);
    expect(radioButtonGroup.find("RadioButton")).toHaveLength(2);
});

it("renders view mode", () => {
    const form = mount(<TestForm {...props} defaultValue={["id1", "id2"]} mode="view" />);
    expect(form.find("ul").children()).toHaveLength(2);
});

it("renders view mode without selected options", () => {
    const form = mount(<TestForm {...props} mode="view" />);
    expect(form.find("Selector").children()).toHaveLength(0);
});
