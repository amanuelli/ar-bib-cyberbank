import React from "react";
import { mount } from "enzyme";

import { Provider } from "react-redux";
import { store } from "store";

import { Formik, Field, Form } from "formik";
import { defaultFieldProps, TestForm } from "./_commons/mock";

import Productselector from "./Productselector";

const fieldProps = {
    data: {
        options: [{ id: "id1", label: "label1", type: "type1" }, { id: "id2", label: "label2", type: "type2" }],
    },
};

const props = { ...defaultFieldProps, component: Productselector, ...fieldProps };

it("renders edit mode", () => {
    const form = mount(<TestForm {...props} value="id1" />);
    expect(form.find(".Select-value-label").text()).toBe("label1");

    // selecting second value...
    form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
    form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
    form.find(".Select-control").simulate("keyDown", { keyCode: 13 });

    expect(form.find(".Select-value-label").text()).toBe("label2");
});

it("renders view mode", () => {
    const form = mount(
        <Provider store={store}>
            <Formik onSubmit={jest.fn}>
                <Form>
                    <Field {...props} value="id1" name="product" mode="view" />
                </Form>
            </Formik>
        </Provider>,
    );
    expect(form.find("I18n[id='productType.type1']").exists()).toBe(true);
});
