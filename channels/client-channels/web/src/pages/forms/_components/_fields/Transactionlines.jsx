import React, { Component } from "react";
import { string, func, oneOf } from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import { routerActions } from "react-router-redux/actions";
import { withRouter } from "react-router-dom";

import { actions as formActions, selectors as formSelectors } from "reducers/form";
import { selectors as transactionLinesSelectors } from "reducers/form/transactionLines";

import Button from "pages/_components/Button";
import I18n from "pages/_components/I18n";
import FormattedAmount from "pages/_components/FormattedAmount";
import DetailBox from "pages/_components/detailBox/DetailBox";
import ManualPayment from "pages/forms/_components/_fields/_multilinefile/ManualPayment";
import formField from "pages/forms/_components/_fields/_commons/formField";

class Transactionlines extends Component {
    static propTypes = {
        setValue: func.isRequired,
        idForm: string.isRequired,
        mode: oneOf(["edit", "view"]).isRequired,
    };

    componentDidMount() {
        const { transactionLines, setValue } = this.props;
        if (!this.isTicket() && transactionLines.length) {
            setValue(transactionLines);
        }
    }

    handleDetailsClick = () => {
        const { dispatch, match, transaction } = this.props;
        if (this.isTicket()) {
            dispatch(routerActions.push(`/transaction/${transaction.idTransaction}/processDetail`));
        } else {
            const { form } = this.props;
            dispatch(routerActions.push(`/form/${match.params.idForm}/processDetail`, { shouldLoadForm: false }));
            dispatch(formActions.setData(form.values));
        }
    };

    isTicket = () => {
        const { mode } = this.props;

        return mode === "view";
    };

    render() {
        const { mode, transactionLines, totalAmount, linesWithNoAmount } = this.props;
        switch (mode) {
            case "edit": {
                return <ManualPayment {...this.props} />;
            }
            case "preview": {
                return (
                    <DetailBox>
                        {!!linesWithNoAmount && (
                            <I18n
                                component="div"
                                componentProps={{ className: "alert alert-warning mTop", role: "alert" }}
                                LINE_COUNT={linesWithNoAmount}
                                id="massive.payments.warning.noAmount"
                            />
                        )}
                        <DetailBox.Data label="forms.inputFile.massivePayments.lines">
                            <div>
                                {transactionLines.length} <I18n id="forms.inputFile.massivePayments.total.lines" />
                            </div>
                        </DetailBox.Data>
                        <DetailBox.Data label="forms.inputFile.massivePayments.totalAmount">
                            <FormattedAmount className="data-desc" {...totalAmount} />
                        </DetailBox.Data>
                        <div className="detailBox-row">
                            <Button
                                bsStyle="primary"
                                className="btn-small"
                                onClick={this.handleDetailsClick}
                                label="forms.inputFile.massivePayments.transaction.detail"
                            />
                        </div>
                    </DetailBox>
                );
            }
            default: {
                const { value } = this.props;
                const amount = value.reduce(
                    (result, line) => ({
                        quantity: result.quantity + line.creditAmountQuantity,
                        currency: line.creditAmountCurrency,
                    }),
                    { quantity: 0, currency: null },
                );

                return (
                    <DetailBox>
                        <DetailBox.Data label="forms.inputFile.massivePayments.lines">{value.length}</DetailBox.Data>
                        <DetailBox.Data label="forms.inputFile.massivePayments.totalAmount">
                            <FormattedAmount className="data-desc" {...amount} />
                        </DetailBox.Data>
                        <div className="detailBox-row">
                            <Button
                                bsStyle="primary"
                                className="btn-small"
                                onClick={this.handleDetailsClick}
                                label="forms.inputFile.massivePayments.transaction.detail"
                            />
                        </div>
                    </DetailBox>
                );
            }
        }
    }
}

const mapStateToProps = (state) => ({
    transactionLines: transactionLinesSelectors.getTransactionLines(state),
    totalAmount: transactionLinesSelectors.getTotalAmount(state),
    linesWithNoAmount: transactionLinesSelectors.getLinesWithNoAmount(state).length,
    transaction: formSelectors.getTransaction(state),
});

export default compose(
    withRouter,
    connect(mapStateToProps),
    formField({
        isValidValue: () => true,
        pureRender: true,
    }),
)(Transactionlines);
