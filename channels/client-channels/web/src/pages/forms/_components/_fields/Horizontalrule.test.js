import React from "react";
import { mount } from "enzyme";
import { defaultFieldProps, TestForm } from "./_commons/mock";

import Horizontalrule from "./Horizontalrule";

const props = { ...defaultFieldProps, component: Horizontalrule };

it("renders edit mode", () => {
    const form = mount(<TestForm {...props} />);
    expect(form.find("Horizontalrule").contains(<hr />)).toBe(true);
});

it("renders view mode", () => {
    const form = mount(<TestForm {...props} mode="view" />);
    expect(form.find("Horizontalrule").contains(<hr />)).toBe(false);
});
