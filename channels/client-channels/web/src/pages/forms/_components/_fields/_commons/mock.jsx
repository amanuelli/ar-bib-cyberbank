import React from "react";
import { Formik, Field, Form } from "formik";
import { ConnectedRouter } from "react-router-redux";
import { history, store } from "store";
import { Provider } from "react-redux";

export const defaultFieldProps = {
    optionalMessageMap: { es: "optional" },
    requiredErrorMap: { es: "required" },
    placeholderMap: { es: "placeholder" },
    labelMap: { es: "label" },
    helpMap: { es: "help" },
    hintMap: { es: "hint" },
    dateFormat: "DD-MM-YYYY",
    lang: "es",
    isRequired: false,
    mode: "edit",
};

export const mockForm = () =>
    new class MockForm {
        values = {};

        errors = {};

        touched = {};

        setErrors = (errors) => {
            this.errors = errors;
        };

        setFieldValue = (field, value) => {
            this.values[field] = value;
        };

        setFieldTouched = (field) => {
            this.touched[field] = true;
        };
    }();

export const mockField = (name = "formField") =>
    new class MockField {
        value = null;

        name = name;
    }();

export class TestForm extends React.Component {
    constructor(props) {
        super(props);
        this.formik = React.createRef();
        this.field = React.createRef();
    }

    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Formik ref={this.formik} onSubmit={jest.fn}>
                        <Form>
                            <Field ref={this.field} name="formField" {...this.props} />
                        </Form>
                    </Formik>
                </ConnectedRouter>
            </Provider>
        );
    }
}
