import React from "react";
import { mount } from "enzyme";

import { Provider } from "react-redux";
import { ConnectedRouter } from "react-router-redux";
import { history } from "store";

import { store } from "store";

import { Formik, Field, Form } from "formik";
import { defaultFieldProps } from "./_commons/mock";
import Paycreditcardamount from "./Paycreditcardamount.jsx";

const payField = (form) => form.find("Paycreditcardamount");
const quantityField = (form) => form.find("input[name='quantity']");
const minimumCheck = (form) => form.find("input[id='minimumAmount']");
const totalCheck = (form) => form.find("input[id='totalAmount']");
const selectLabel = (form) => form.find(".Select-value-label").text();

describe("renders edit mode", () => {
    let form;
    const props = {
        ...defaultFieldProps,
        component: Paycreditcardamount,
        idCreditCardSelector: "creditCard",
        data: {
            thousandsSeparator: ".",
            decimalSeparator: ",",
            precision: 2,
            options: [{ id: "BRL", label: "BRL" }, { id: "USD", label: "USD" }],
        },
    };

    const fieldList = [
        {
            idField: "creditCard",
            data: {
                options: [
                    {
                        id: "creditCard1",
                        minimumPaymentMap: {
                            BRL: 123,
                            USD: 50,
                        },
                        totalPaymentMap: {
                            BRL: 246,
                            USD: 100,
                        },
                    },
                ],
            },
        },
        { idField: "creditPay" },
    ];

    beforeEach(() => {
        form = mount(
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Formik onSubmit={jest.fn} initialValues={{ creditCard: "creditCard1" }}>
                        <Form>
                            <Field name="creditCard" />
                            <Field name="creditPay" {...props} fieldList={fieldList} />
                        </Form>
                    </Formik>
                </ConnectedRouter>
            </Provider>,
        );
    });

    it("displays correctly default total option", () => {
        expect(selectLabel(form)).toBe("BRL");
        expect(minimumCheck(form).instance().checked).toBe(false);
        expect(totalCheck(form).instance().checked).toBe(true);
        expect(quantityField(form).instance().value).toBe("246,00");

        // selecting second currency...
        form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
        form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
        form.find(".Select-control").simulate("keyDown", { keyCode: 13 });

        expect(selectLabel(form)).toBe("USD");
        expect(quantityField(form).instance().value).toBe("100,00");
    });

    it("inputs correct value for minimum payment", () => {
        expect(selectLabel(form)).toBe("BRL");
        minimumCheck(form).simulate("change", { target: { value: "minimum" } });

        expect(minimumCheck(form).instance().checked).toBe(true);
        expect(totalCheck(form).instance().checked).toBe(false);
        expect(quantityField(form).instance().value).toBe("123,00");

        // selecting second currency...
        form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
        form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
        form.find(".Select-control").simulate("keyDown", { keyCode: 13 });

        expect(selectLabel(form)).toBe("USD");
        expect(quantityField(form).instance().value).toBe("50,00");
    });

    it("input custom payment value", () => {
        quantityField(form).simulate("change", { target: { value: "200,00", focus: jest.fn() } });
        expect(quantityField(form).instance().value).toBe("200,00");
        expect(minimumCheck(form).instance().checked).toBe(false);
        expect(totalCheck(form).instance().checked).toBe(false);

        // selecting second currency...
        form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
        form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
        form.find(".Select-control").simulate("keyDown", { keyCode: 13 });
        expect(quantityField(form).instance().value).toBe("");
    });
});
