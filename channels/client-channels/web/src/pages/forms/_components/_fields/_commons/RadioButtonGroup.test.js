import React from "react";
import { mount } from "enzyme";

import RadioButtonGroup from "./RadioButtonGroup";

const props = {
    options: [{ id: "id1", label: "label1" }, { id: "id2", label: "label2" }],
};

it("renders no option selected", () => {
    const onChange = jest.fn();
    const group = mount(<RadioButtonGroup {...props} onChange={onChange} />);
    expect(group.find("input")).toHaveLength(2);
    expect(group.find("input").filterWhere((i) => i.getElement().props.checked)).toHaveLength(0);

    group.find("input[value='id1']").simulate("click");
    expect(onChange).toHaveBeenCalledWith("id1");
});

it("renders toggled option", () => {
    const onChange = jest.fn();
    const group = mount(<RadioButtonGroup {...props} value="id1" onChange={onChange} />);
    expect(group.find("input")).toHaveLength(2);
    expect(group.find("input").filterWhere((i) => i.getElement().props.checked)).toHaveLength(1);

    group.find("input[value='id1']").simulate("click");
    expect(onChange).toHaveBeenCalledWith("id1");
});
