import React from "react";
import { mount } from "enzyme";
import { defaultFieldProps, TestForm } from "./_commons/mock";

import Sectiontitle from "./Sectiontitle";

const props = { ...defaultFieldProps, component: Sectiontitle };

it("renders edit mode", () => {
    const form = mount(<TestForm {...props} />);
    expect(form.find("h3").text()).toBe("label");
});

it("renders view mode", () => {
    const form = mount(<TestForm {...props} />);
    expect(form.find("h3").text()).toBe("label");
});
