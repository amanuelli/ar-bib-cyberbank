import React from "react";
import { mount } from "enzyme";
import { defaultFieldProps, TestForm } from "./_commons/mock";

import Branchlist from "./Branchlist";

const fieldProps = {
    data: {
        branches: [{ id: "id1", name: "branch1" }, { id: "id2", name: "branch2" }],
    },
};

const props = { ...defaultFieldProps, component: Branchlist, ...fieldProps };

it("renders edit mode", () => {
    const form = mount(<TestForm {...props} defaultValue="id1" />);
    expect(form.find(".Select-value-label").text()).toBe("branch1");

    expect(form.find("Branchlist").props().value).toBe("id1");

    // selecting second value...
    form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
    form.find(".Select-control").simulate("keyDown", { keyCode: 40 });
    form.find(".Select-control").simulate("keyDown", { keyCode: 13 });

    expect(form.find(".Select-value-label").text()).toBe("branch2");

    expect(form.find("Branchlist").props().value).toBe("id2");
});

it("renders view mode", () => {
    const form = mount(<TestForm {...props} mode="view" defaultValue="id1" />);
    expect(form.find("Branchlist").text()).toBe("branch1");
});
