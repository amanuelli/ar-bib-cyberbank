import React from "react";
import { mount } from "enzyme";
import { TestForm, defaultFieldProps } from "./_commons/mock";

import Text from "./Text";

const fieldProps = {
    maxLength: 5,
    validationRegularExpresion: null,
    minLength: 0,
};

const props = { ...defaultFieldProps, component: Text, ...fieldProps };

it("renders edit mode", () => {
    const form = mount(<TestForm {...props} defaultValue="value" />);

    expect(form.find("input").instance().value).toBe("value");
    form.find("input").simulate("change", { target: { value: "123" } });

    expect(form.find("input").instance().value).toBe("123");

    const value = form.find("Text").props().value;

    expect(value).toBe("123");
});

it("validates input using regex prop", () => {
    const form = mount(<TestForm {...props} validationRegularExpresion="[0-9]+" defaultValue="123" />);

    expect(form.find("input").instance().value).toBe("123");
    form.find("input").simulate("change", { target: { value: "text" } });

    expect(form.find("input").instance().value).toBe("123");

    form.find("input").simulate("change", { target: { value: "12345" } });
    expect(form.find("input").instance().value).toBe("12345");

    const value = form.find("Text").props().value;
    expect(value).toBe("12345");
});

it("renders view mode", () => {
    const form = mount(<TestForm {...props} mode="view" defaultValue="value" />);
    expect(form.find("Text").text()).toBe("value");
});
