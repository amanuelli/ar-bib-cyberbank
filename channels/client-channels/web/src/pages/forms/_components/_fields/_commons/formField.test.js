import React from "react";
import { mount } from "enzyme";
import { defaultFieldProps, mockForm, mockField, TestForm } from "./mock";

import formField from "./formField";

class TestField extends React.Component {
    render() {
        if (this.props.editing) {
            return <div id="edit">{this.props.value}</div>;
        }
        return <div id="view">{this.props.value}</div>;
    }
}

const props = {
    ...defaultFieldProps,
    component: formField({
        formClass: "test-form-class",
        isEmptyValue: (value) => value == null || value === "",
    })(TestField),
};

it("renders edit mode without crashing", () => {
    const form = mount(<TestForm {...props} defaultValue="value" />);
    expect(form.find("[id='edit']").exists()).toBe(true);
    expect(form.find("[id='edit']").text()).toBe("value");
    expect(form.find("div.test-form-class").exists()).toBe(true);
});

it("renders view mode without crashing", () => {
    const form = mount(<TestForm {...props} defaultValue="value" mode="view" />);
    expect(form.find("[id='view']").exists()).toBe(true);
    expect(form.find("[id='view']").text()).toBe("value");
    expect(form.find("div.test-form-class").exists()).toBe(true);
});

it("does not render view mode if isEmptyValue return true", () => {
    const form = mount(<TestForm {...props} mode="view" />);
    expect(form.find("[id='view']").exists()).toBe(false);
    expect(form.find("[id='false']").exists()).toBe(false);
});

it("renders optional edit template correctly", () => {
    const form = mount(<TestForm {...props} />);

    expect(form.find("FieldLabel").prop("labelText")).toBe("label");
    expect(form.find("FieldLabel").prop("optional")).toBe("optional");
    expect(form.find("FieldHelp").prop("text")).toBe("help");
    expect(form.find("FieldHint").prop("text")).toBe("hint");
    expect(form.find("FieldError").exists()).toBe(false);
});

it("renders required edit template correctly", () => {
    const form = mount(<TestForm {...props} isRequired />);

    expect(form.find("FieldLabel").prop("labelText")).toBe("label");
    expect(form.find("FieldHelp").prop("text")).toBe("help");
    expect(form.find("FieldHint").prop("text")).toBe("hint");
    expect(form.find("FieldError").exists()).toBe(false);

    form.find("TestField")
        .props()
        .setValue("");
    form.find("TestField")
        .props()
        .setTouched();
    form.update();

    expect(form.find("FieldError").exists()).toBe(true);
    expect(form.find("FieldError").prop("error")).toBe("required");
});

it("renders FormFieldComponent custom label properly", () => {
    const field = formField({
        customLabel: (props) => <div id="customLabel" />,
    })(TestField);

    const form = mount(<TestForm {...props} component={field} />);
    expect(form.find("FieldLabel").exists()).toBe(false);
    expect(form.find("[id='customLabel']").exists()).toBe(true);
});

it("performs validation based on isValidValue", () => {
    const field = formField({
        isValidValue: (value) => value === true,
    })(TestField);

    const instance = new field({
        defaultValue: false,
        form: mockForm(),
        field: mockField(),
    });

    instance.validate = jest.fn();
    instance.props.form.setFieldValue = jest.fn();

    instance.setValue(true);
    expect(instance.validate).toHaveBeenCalledTimes(1);
    expect(instance.props.form.setFieldValue).toHaveBeenCalledTimes(1);

    instance.setValue(false);

    expect(instance.validate).toHaveBeenCalledTimes(1);
    expect(instance.props.form.setFieldValue).toHaveBeenCalledTimes(1);
});

it("sets required error if isRequired and isEmptyValue returns true", () => {
    const field = formField({
        isEmptyValue: (value) => value === "",
    })(TestField);

    const instance = new field({
        ...defaultFieldProps,
        defaultValue: false,
        isRequired: true,
        form: mockForm(),
        field: mockField(),
    });

    instance.props.form.setFieldValue = jest.fn();
    instance.setValue("");

    expect(instance.hasError()).toBe(true);
    expect(instance.errorText()).toBe("required");

    instance.setValue("not-empty");

    expect(instance.hasError()).toBe(false);
    expect(instance.errorText()).toBeFalsy();

    expect(instance.props.form.setFieldValue).toHaveBeenLastCalledWith("formField", "not-empty");
    expect(instance.props.form.setFieldValue).toHaveBeenCalledTimes(2);
});

it("field is touched on blur", () => {
    const field = formField()(TestField);

    const instance = new field({
        form: mockForm(),
        field: mockField(),
    });

    expect(instance.isTouched()).toBe(false);
    instance.onBlur();
    expect(instance.isTouched()).toBe(true);
});
