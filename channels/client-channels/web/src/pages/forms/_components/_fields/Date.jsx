import React, { Component } from "react";
import moment from "moment";
import flowRight from "lodash/flowRight";
import { bool, func, string } from "prop-types";

import formField from "pages/forms/_components/_fields/_commons/formField";
import DatePicker from "pages/_components/fields/datepicker";
import withFocus from "pages/_components/withFocus";

import { get } from "util/config";
import { format } from "date-fns";
import * as i18n from "util/i18n";

class Date extends Component {
    static propTypes = {
        dateFormat: string.isRequired,
        editing: bool.isRequired,
        setValue: func.isRequired,
        name: string.isRequired,
        onBlur: func.isRequired,
        placeholder: string.isRequired,
        toggleIsFocused: func.isRequired,
        value: string.isRequired,
        startDate: string.isRequired,
    };

    static getDerivedStateFromProps(nextProps, prevState) {
        return {
            ...prevState,
            startDate: nextProps.value ? moment(nextProps.value) : null,
        };
    }

    constructor(props) {
        super(props);

        this.state = {};
    }

    handleDateChange = (date) => {
        const { setValue } = this.props;
        setValue(date);
    };

    i;

    render() {
        const {
            name,
            value,
            editing,
            dateFormat,
            toggleIsFocused,
            placeholder,
            onBlur,
            startDate,
            ...rest
        } = this.props;

        if (editing) {
            return (
                <div className="input-group input-group-datepicker" onFocus={toggleIsFocused} onBlur={toggleIsFocused}>
                    <DatePicker
                        dateFormat={dateFormat || get("frontend.shortDateFormat").toUpperCase()}
                        className="form-control"
                        name={name}
                        onChange={this.handleDateChange}
                        selected={moment(value)}
                        autoComplete="off"
                        startDate={startDate ? moment(startDate) : null}
                        placeholderText={placeholder}
                        onBlur={onBlur}
                        onKeyDown={(e) => e.preventDefault()}
                        {...rest}
                    />
                </div>
            );
        }

        return (
            <span>
                {value
                    ? format(value, dateFormat || get("frontend.shortDateFormat").toUpperCase())
                    : i18n.get("datepicker.noDate")}
            </span>
        );
    }
}

export default flowRight(
    withFocus,
    formField({ formClass: "form-group--datepicker" }),
)(Date);
