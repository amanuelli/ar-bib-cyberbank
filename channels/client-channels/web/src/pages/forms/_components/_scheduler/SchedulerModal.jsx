import React, { Component } from "react";
import Col from "react-bootstrap/lib/Col";
import MainContainer from "pages/_components/MainContainer";
import Container from "pages/_components/Container";
import { connect } from "react-redux";
import { compose } from "redux";
import { Form, withFormik, Field } from "formik";
import { func, shape, bool, string } from "prop-types";
import Button from "pages/_components/Button";
import * as i18nUtils from "util/i18n";
import * as schedulerUtils from "util/scheduler";
import * as dateUtils from "util/date";
import Select from "pages/forms/_components/_fields/Select";
import CommonFrequencySubOption from "pages/forms/_components/_scheduler/CommonFrequencySubOption";
import CustomFrequencySubOption from "pages/forms/_components/_scheduler/CustomFrequencySubOption";
import moment from "moment";
import I18n from "pages/_components/I18n";
import { Modal } from "react-bootstrap";
import ModalNotification from "pages/forms/_components/ModalNotification";

const STARTS_ON_DATE = moment(new Date()).add(1, "days");

class SchedulerModal extends Component {
    static propTypes = {
        goBack: func,
        onScheduleClick: func,
        setTouched: func,
        setErrors: func,
        handleCloseBottomSheetClick: func,
        touched: shape({}),
        errors: shape({}),
        values: shape({}),
        isDisplayed: bool,
        idForm: string.isRequired,
    };

    static defaultProps = {
        goBack: () => {},
        onScheduleClick: () => {},
        setTouched: () => {},
        setErrors: () => {},
        handleCloseBottomSheetClick: () => {},
        touched: {},
        errors: {},
        values: {},
        isDisplayed: false,
    };

    goBack = () => {
        const { goBack } = this.props;
        goBack();
    };

    validateFields = () => {
        const { setErrors, errors, touched, setTouched, values } = this.props;
        let res = true;
        // clean errors
        setErrors({});

        const moreThan2daysaWeek = dateUtils.moreThan2daysSelected(values.days);

        // validate if it's week and days is just one day and freqValue = 1
        if (values.customFreq === schedulerUtils.WEEK) {
            if (moreThan2daysaWeek && values.customFreqValue !== "1") {
                setTouched({ ...touched, days: true });
                setErrors({
                    ...errors,
                    days: { value: i18nUtils.get(`scheduler.multipleDaysSelectedValidation`) },
                });
                res = false;
            }
        }
        // starts on must be after than today
        if (moment(values.startsOn).isBefore(moment())) {
            setTouched({ ...touched, startsOn: true });
            setErrors({
                ...errors,
                startsOn: i18nUtils.get(`scheduler.startsOnBeforeToday`),
            });
            res = false;
        }

        return res;
    };

    onScheduleClick = () => {
        const { onScheduleClick, values } = this.props;

        if (this.validateFields()) {
            onScheduleClick(values);
        }
    };

    render() {
        const {
            isDisplayed,
            handleCloseBottomSheetClick,
            values: { lapse, frequency, customFreq },
            errors,
            idForm,
        } = this.props;

        return (
            <Modal
                aria-labelledby="modalTitleID"
                aria-modal="true"
                onHide={handleCloseBottomSheetClick}
                show={isDisplayed}>
                <div className="modal-container">
                    <Modal.Header closeButton>
                        <Modal.Title id="modalTitleID">
                            <I18n id="scheduler.title" />
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form className="above-the-fold">
                            <MainContainer>
                                <div className="above-the-fold scheduler">
                                    <Container className="container--layout flex-grow">
                                        <ModalNotification idForm={idForm} errors={errors} />
                                        <Col sm={12} className="col col-12">
                                            <div className="form-text-group">
                                                <label id={`label.${idForm}.frequency`} className="control-label">
                                                    <I18n id="scheduler.frequency" />
                                                </label>
                                            </div>
                                            <div className="input-group">
                                                <Field
                                                    id={`${idForm}.frequency`}
                                                    component={Select}
                                                    name="frequency"
                                                    className="slideFromBottom"
                                                    options={schedulerUtils.listRecurrencies()}
                                                    clearable={false}
                                                    searchable={false}
                                                    aria-owns="endRulesFieldsetID"
                                                    aria-haspopup
                                                    aria-expanded={isDisplayed}
                                                />
                                            </div>
                                        </Col>
                                        {frequency !== schedulerUtils.CUSTOM ? (
                                            <CommonFrequencySubOption
                                                lapse={lapse}
                                                frequency={frequency}
                                                startsOn={STARTS_ON_DATE}
                                            />
                                        ) : (
                                            <CustomFrequencySubOption
                                                lapse={lapse}
                                                frequency={frequency}
                                                startsOn={STARTS_ON_DATE}
                                                customFreq={customFreq}
                                            />
                                        )}
                                    </Container>
                                    <Container className="container--layout align-items-center ">
                                        <Col sm={12} md={9} lg={9} className="col col-12">
                                            <Button
                                                type="button"
                                                bsStyle="primary"
                                                label="scheduler.schedule"
                                                onClick={this.onScheduleClick}
                                            />
                                        </Col>
                                    </Container>
                                </div>
                            </MainContainer>
                        </Form>
                    </Modal.Body>
                </div>
            </Modal>
        );
    }
}

const mapStateToProps = () => ({});

export default compose(
    connect(mapStateToProps),
    withFormik({
        enableReinitialize: true,
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: (props) => {
            const { value } = props;
            const newProgram = {
                frequency: schedulerUtils.ONCE,
                startsOn:
                    value && value.selectedOption === schedulerUtils.ONCE
                        ? value.valueDate.toDate()
                        : STARTS_ON_DATE.toDate(),
                lapse: {
                    date: STARTS_ON_DATE.toDate(),
                    lapse: schedulerUtils.NEVER,
                },
                customFreq: schedulerUtils.DAY,
                customFreqValue: "1",
                days: dateUtils.getDayFromDate(STARTS_ON_DATE.toDate()),
                occurrenceType: "day", // day(every month on x day ) or occurrence(every month on the "tirth" "tuesday")
            };

            if (!value || !value.program) {
                return newProgram;
            }

            const startsOn =
                typeof value.program.startsOn === "string"
                    ? moment(value.program.startsOn).toDate()
                    : value.program.startsOn;
            return {
                ...value.program,
                startsOn,
                frequency: value.program.isCustom ? schedulerUtils.CUSTOM : value.program.frequency,
                lapse: {
                    lapse: value.program.lapse,
                    date: value.program.date,
                    number: value.program.number,
                },
                customFreq: value.program.frequency,
                customFreqValue: value.program.frequencyValue,
                occurrenceType: value.program.occurrence ? "occurrence" : "day",
            };
        },
    }),
)(SchedulerModal);
