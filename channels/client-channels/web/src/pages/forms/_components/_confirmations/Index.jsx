/**
 * For every form please add the following line:
 *
 * export {default as Mixed_nameExample} from 'pages/forms/_components/_confirmations/Mixed_nameExample'
 *
 * Note: The id of this example form is: mixed.nameExample, just convert to upper case the first letter and replace the "." per "_".
 *
 * The component could be like:
 *
 * import React, { Component } from 'react';
 *
 * import { Grid, Row, Col } from 'react-bootstrap'
 *
 * class Test1 extends Component {
 *     render() {
 *         let message = "Put some nice confirmation message here";
 *
 *         if (this.props.fieldValues.field1) {
 *             message += (", and use some form field values too: " + this.props.fieldValues.field1);
 *         }
 *
 *         // We use as much html as possible, in case we should render a more sophisticated message ;)
 *         return (
 *             <Grid componentClass="section" className="form-content">
 *                 <Row className="login-row">
 *                     <Col sm={12} className="col">
 *                         <span>{message}</span>
 *                     </Col>
 *                 </Row>
 *             </Grid>
 *         )
 *     }
 * }
 *
 * export default Test1;
 */

export { default as PayCreditCard } from "pages/forms/_components/_confirmations/PayCreditCard";
export { default as PayThirdPartiesCreditCard } from "pages/forms/_components/_confirmations/PayThirdPartiesCreditCard";
export { default as TransferInternal } from "pages/forms/_components/_confirmations/TransferInternal";
export { default as TransferThirdParties } from "pages/forms/_components/_confirmations/TransferThirdParties";
export { default as PayLoan } from "pages/forms/_components/_confirmations/PayLoan";
export { default as PayThirdPartiesLoan } from "pages/forms/_components/_confirmations/PayThirdPartiesLoan";
