import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Link from "react-router-dom/Link";
import { bool, func, arrayOf, shape, string, number, oneOfType, date } from "prop-types";
import { push } from "react-router-redux";

import { actions as creditCardsActions, selectors as creditCardsSelectors } from "reducers/creditCards";
import { selectors as sessionSelectors } from "reducers/session";

import Notification from "pages/_components/Notification";
import Head from "pages/_components/Head";
import MainContainer from "pages/_components/MainContainer";
import CreditCardsList from "pages/creditCards/List";
import Container from "pages/_components/Container";
import Image from "pages/_components/Image";
import I18n from "pages/_components/I18n";
import GeneralMsg from "pages/_components/GeneralMsg";

class CreditCards extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        isRequestAvailable: bool.isRequired,
        isMobile: bool.isRequired,
        fetching: bool.isRequired,
        isDesktop: bool.isRequired,
        creditCards: arrayOf(
            shape({
                productAlias: string,
                availableBalance: number,
                balance: number,
                number: oneOfType([number, string]),
                shortLabel: string,
                availableBalanceCurrency: string,
                expirationDate: date,
                label: string,
            }),
        ).isRequired,
    };

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(creditCardsActions.listRequest("/creditCards"));
    }

    renderHeader = () => {
        const { isRequestAvailable, isMobile } = this.props;

        if (!isRequestAvailable) {
            return <Head title="menu.creditcards" />;
        }

        if (isMobile) {
            return <Head title="menu.creditcards" addLinkToLabel="creditCard.new" />;
        }

        return <Head title="menu.creditcards" addLinkTo="/form/creditCardRequest" addLinkToLabel="creditCard.new" />;
    };

    btnHandlerOnClick = () => {
        const { dispatch } = this.props;
        dispatch(push("/form/creditCardRequest/"));
    };

    render() {
        const { fetching, creditCards, isDesktop, isRequestAvailable } = this.props;
        const isLoading = fetching && !creditCards.length;

        return (
            <Fragment>
                <Notification scopeToShow="creditcards" />
                {!isLoading && this.renderHeader()}
                <MainContainer showLoader={isLoading}>
                    <div className="above-the-fold">
                        {creditCards.length ? (
                            <Fragment>
                                <CreditCardsList creditCards={creditCards} isDesktop={isDesktop} />
                                {!isDesktop && isRequestAvailable && (
                                    <Container className="container--layout align-items-center">
                                        <Container.Column className="col col-12">
                                            <Link to="/form/creditCardRequest/" className="btn btn-outline btn-block">
                                                <Image src="images/plus.svg" />
                                                <I18n id="creditCard.new" />
                                            </Link>
                                        </Container.Column>
                                    </Container>
                                )}
                            </Fragment>
                        ) : (
                            <GeneralMsg
                                imagePath="images/coloredIcons/creditCards.svg"
                                description={<I18n id="creditCards.list.empty" />}
                                callToAction={
                                    isRequestAvailable && (
                                        <Link className="btn btn-primary btn-block" to="/form/creditCardRequest">
                                            <I18n id="creditCard.new" />
                                        </Link>
                                    )
                                }
                            />
                        )}
                    </div>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    fetching: creditCardsSelectors.isFetching(state),
    creditCards: creditCardsSelectors.getList(state),
    isRequestAvailable: sessionSelectors
        .getActiveEnvironmentForms(state, "creditcards")
        .some(({ idForm }) => idForm === "creditCardRequest"),
});

export default connect(mapStateToProps)(CreditCards);
