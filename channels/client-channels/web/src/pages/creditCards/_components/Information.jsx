import React, { Component, Fragment } from "react";
import { Col, Row } from "react-bootstrap";
import { string, bool, shape, number, func } from "prop-types";
import I18n from "pages/_components/I18n";

import FormattedAmount from "pages/_components/FormattedAmount";
import FormattedDate from "pages/_components/FormattedDate";
import PaperlessSwitch from "pages/_components/fields/PaperlessSwitch";

class CreditCardInformation extends Component {
    static propTypes = {
        creditCard: shape({
            holder: string,
            creditLimit: number,
            creditLimitCurrency: string,
            closingDate: string,
            number: string,
            lastPaymentDate: string,
        }).isRequired,
        isDesktop: bool.isRequired,
        paperless: bool.isRequired,
        dispatch: func.isRequired,
    };

    state = {
        paperless: this.props.creditCard.paperless,
    };

    render() {
        const { creditCard, isDesktop, dispatch } = this.props;
        const { paperless } = this.state;
        const accountHolder = (
            <Col sm={12} md={4} lg={4} className="col col-12">
                <div className="data-wrapper">
                    <span className="data-label">
                        <I18n id="creditCard.holder" />
                    </span>
                    <span className="data-aux">{creditCard.holder}</span>
                </div>
            </Col>
        );

        const creditCardLimit = (
            <Col sm={12} md={4} lg={4} className="col col-12">
                <div className="data-wrapper">
                    <span className="data-label">
                        <I18n id="creditCard.creditLimit" />
                    </span>
                    <FormattedAmount quantity={creditCard.creditLimit} currency={creditCard.creditLimitCurrency} />
                </div>
            </Col>
        );

        const nextClosingDate = (
            <Col sm={12} md={4} lg={4} className="col col-12">
                <div className="data-wrapper">
                    <span className="data-label">
                        <I18n id="creditCard.closingDate.next" />
                    </span>
                    <span className="data-date">
                        <FormattedDate date={creditCard.closingDate} />
                    </span>
                </div>
            </Col>
        );

        const cardNumber = (
            <Col sm={12} md={4} lg={4} className="col col-12">
                <div className="data-wrapper">
                    <span className="data-label">N°</span>
                    <span className="data-aux">{`···· ···· ···· ${creditCard.number.slice(-4)}`}</span>
                </div>
            </Col>
        );

        const lastPaymentDate = (
            <Col sm={12} md={4} lg={4} className="col col-12">
                <div className="data-wrapper">
                    <span className="data-label">
                        <I18n id="creditCard.lastPayment.date" />
                    </span>
                    <span className="data-date">
                        <FormattedDate date={creditCard.lastPaymentDate} />
                    </span>
                </div>
            </Col>
        );

        const lastPaymentAmount = (
            <Col sm={12} md={4} lg={4} className="col col-12">
                <div className="data-wrapper">
                    <span className="data-label">
                        <I18n id="creditCard.lastPayment.amount" />
                    </span>
                    <FormattedAmount
                        quantity={creditCard.creditLimit * 0.47}
                        currency={creditCard.creditLimitCurrency}
                    />
                </div>
            </Col>
        );

        const paperlessSwitch = (
            <Col sm={12} md={4} lg={4} className="col col-12">
                <PaperlessSwitch
                    name="creditcard"
                    idProduct={creditCard.idProduct}
                    paperless={creditCard.paperless}
                    onClick={() => this.setState({ paperless: !paperless })}
                    dispatch={dispatch}
                />
            </Col>
        );

        if (isDesktop) {
            return (
                <Fragment>
                    <Row>
                        {accountHolder}
                        {creditCardLimit}
                        {nextClosingDate}
                        {lastPaymentDate}
                        {lastPaymentAmount}
                        {paperlessSwitch}
                    </Row>
                </Fragment>
            );
        }

        return (
            <Fragment>
                <Row>
                    {accountHolder}
                    {cardNumber}
                    {creditCardLimit}
                    {nextClosingDate}
                    {lastPaymentDate}
                    {lastPaymentAmount}
                    {paperlessSwitch}
                </Row>
            </Fragment>
        );
    }
}

export default CreditCardInformation;
