import React, { Component, Fragment } from "react";
import { string, func, bool, int, shape } from "prop-types";
import { actions as productsActions } from "reducers/products";

import EditableLabel from "pages/_components/fields/EditableLabel";
import Image from "pages/_components/Image";
import I18n from "pages/_components/I18n";

import * as creditCardsUtils from "util/creditCards";

class CreditCardDetailHeadTitle extends Component {
    static propTypes = {
        isDesktop: bool.isRequired,
        dispatch: func.isRequired,
        creditCard: shape({
            idEnvironment: int,
            idProduct: string,
            productType: string.isRequired,
            label: string,
            shortLabel: string,
            extraInfo: string,
            number: string.isRequired,
            account: string,
            holder: string,
            expirationDate: string,
            closingDate: string,
            availableBalanceCurrency: string.isRequired,
            creditLimitCurrency: string.isRequired,
            lastPaymentDate: string,
            extraInfoMarked: string,
        }).isRequired,
    };

    constructor(props) {
        super(props);
        this.titleRef = React.createRef();
    }

    componentDidMount() {
        if (this.titleRef.current) {
            this.titleRef.current.focus();
        }
    }

    componentDidUpdate() {
        if (this.titleRef.current) {
            this.titleRef.current.focus();
        }
    }

    saveAlias = (alias) => {
        const { creditCard, dispatch } = this.props;
        dispatch(productsActions.changeProductAlias(alias, creditCard.idProduct));
    };

    render() {
        const { creditCard, isDesktop } = this.props;
        const { availableBalanceCurrency, productAlias, productType, label } = creditCard;

        return (
            <Fragment>
                <div className="toolbar-item view-title">
                    <div className="visually-hidden" ref={this.titleRef} tabIndex="0">
                        <span>{`${productType} ${availableBalanceCurrency}, ${productAlias || label}`}</span>
                    </div>
                    {(isDesktop && (
                        <EditableLabel isDesktop={isDesktop} onSave={this.saveAlias} value={productAlias || label}>
                            <h1 className="data-name product-title">{productAlias || label}</h1>
                        </EditableLabel>
                    )) || <h1 className="ellipsis">{productAlias || label}</h1>}
                </div>
                <div className="toolbar-item">
                    <mark className="product-name">
                        <Fragment>
                            <span>
                                <I18n id="creditCard.label" />
                            </span>
                            <Image
                                src={`images/${creditCardsUtils.detectBrand(creditCard.number)}.svg`}
                                className="svg-logo"
                            />
                            <span>
                                <b>{`···· ${creditCard.number.slice(-4)}`}</b>
                            </span>
                        </Fragment>
                    </mark>
                </div>
            </Fragment>
        );
    }
}

export default CreditCardDetailHeadTitle;
