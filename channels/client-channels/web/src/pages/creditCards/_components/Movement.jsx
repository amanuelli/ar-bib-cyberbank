import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { shape, bool, string, number } from "prop-types";

import * as configUtils from "util/config";

import Image from "pages/_components/Image";
import FormattedAmount from "pages/_components/FormattedAmount";
import FormattedDate from "pages/_components/FormattedDate";
import ChevromRight from "pages/_components/listItem/ChevromRight";

class CreditCardMovement extends Component {
    static propTypes = {
        isDesktop: bool.isRequired,
        movement: shape({
            idProduct: string,
            idStatement: number,
            date: string,
            concept: string,
            sourceAmount: number,
            sourceAmountCurrency: string,
            amount: number,
        }).isRequired,
    };

    render() {
        const { movement, isDesktop } = this.props;
        return isDesktop ? (
            <Fragment>
                <div className="table-data">
                    <FormattedDate date={movement.date} />
                </div>
                <div className="table-data">{movement.concept}</div>
                <div className="table-data">
                    <FormattedAmount quantity={movement.amount} currency={configUtils.get("core.masterCurrency")} />
                </div>
                <div className="table-data table-data-icon">
                    {movement.note && <Image src="images/note.svg" className="svg-icon" />}
                </div>
                <ChevromRight />
            </Fragment>
        ) : (
            <Fragment>
                <div className="table-data">
                    <span className="data-date">
                        <FormattedDate date={movement.date} />
                    </span>
                    <span className="data-text">{movement.concept}</span>
                </div>
                <div className="table-data">
                    <FormattedAmount quantity={movement.amount} currency={configUtils.get("core.masterCurrency")} />
                </div>
                <ChevromRight />
            </Fragment>
        );
    }
}

const mapStateToProps = () => ({});

export default connect(mapStateToProps)(CreditCardMovement);
