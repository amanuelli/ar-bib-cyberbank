import React, { Component } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { replace } from "react-router-redux";

import { selectors as creditCardSelectors, actions as creditCardsActions } from "reducers/creditCard";

import Button from "pages/_components/Button";
import Table from "pages/_components/Table";
import I18n from "pages/_components/I18n";
import Scroll from "pages/_components/Scroll";
import CreditCardMovement from "pages/creditCards/_components/Movement";
import PageLoading from "pages/_components/PageLoading";
import Container from "pages/_components/Container";

class CreditCardMovements extends Component {
    fetchMoreMovements = () => {
        const { idProduct, selectedPeriod, pageNumber, dispatch, filters } = this.props;
        const [dateFrom, dateTo] = selectedPeriod || [null, null];

        dispatch(
            creditCardsActions.fetchMoreMovementsRequest(idProduct, {
                ...filters,
                dateFrom,
                dateTo,
                pageNumber: pageNumber + 1,
            }),
        );
    };

    handleFiletrClick = () => {
        const { dispatch, idProduct } = this.props;
        dispatch(replace(`/creditCards/${idProduct}/filters`));
    };

    renderBottom = () => {
        const { isFetchingMovements, movements, hasMoreMovements } = this.props;

        if (isFetchingMovements) {
            return null;
        }

        if (movements.length && hasMoreMovements) {
            return (
                <div className="text-center no-more-data" key="noMoreMovements">
                    <Button
                        className="btn btn-link"
                        onClick={this.fetchMoreMovements}
                        image="images/show.svg"
                        label="accounts.movements.moreMovements"
                    />
                </div>
            );
        }

        return this.renderEnd();
    };

    renderEnd = () => (
        <div className="text-center no-more-data">
            <p>
                <I18n id="accounts.movements.noMoreMovements" />
            </p>
        </div>
    );

    searchMoreButton = (IdProduct) => (
        <Button
            onClick={this.handleFiletrClick}
            className="btn btn-block btn-link"
            key="searchMoreMovementsButton"
            to={`/creditCards/${IdProduct}/filters`}
            label="accounts.searchMovements"
        />
    );

    render() {
        const { movements, isDesktop, hasMoreMovements, isFetchingMovements, idProduct, pageNumber } = this.props;

        /* If you find more than one movement
           Get the first idProduct
        */
        let mainIdProduct = idProduct;
        if (typeof idProduct === "undefined" && movements.length > 1) {
            mainIdProduct = movements[0].idProduct;
        }

        const showTableHeader = movements.length > 0;
        const list = movements.map((movement) => (
            <Table.Row
                renderAs={Link}
                to={`/creditCards/${movement.idProduct}/${movement.idStatement}`}
                key={movement.idStatement}>
                <CreditCardMovement movement={movement} isDesktop={isDesktop} />
            </Table.Row>
        ));
        if (!isDesktop) {
            return (
                <div>
                    {movements.length === 0 && pageNumber === 1 && this.searchMoreButton(mainIdProduct)}
                    {movements.length > 0 && (
                        <Scroll
                            {...this.props}
                            endOfListItem={this.renderEnd()}
                            fetchMoreData={this.fetchMoreMovements}
                            lastPage={!hasMoreMovements}
                            items={list}
                            searchMore={this.searchMoreButton(mainIdProduct)}
                            isInfiniteScroll
                            removeListenersWhenPulled
                        />
                    )}
                    {movements.length === 0 && this.renderEnd()}
                </div>
            );
        }

        return (
            <PageLoading loading={isFetchingMovements}>
                <Container className="container--layout flex-grow scrollable">
                    <div className="col col-12">
                        <div className="table-wrapper">
                            <Table>
                                {showTableHeader && (
                                    <Table.Header>
                                        <Table.HeaderData align="left">
                                            <I18n id="tableHeader.date" />
                                        </Table.HeaderData>
                                        <Table.HeaderData align="left">
                                            <I18n id="tableHeader.concept" />
                                        </Table.HeaderData>
                                        <Table.HeaderData align="right">
                                            <I18n id="tableHeader.amount" />
                                        </Table.HeaderData>
                                        <Table.HeaderData />
                                        <Table.HeaderData />
                                    </Table.Header>
                                )}
                                <Table.Body>{list}</Table.Body>
                            </Table>
                            {this.renderBottom()}
                        </div>
                    </div>
                </Container>
            </PageLoading>
        );
    }
}
const mapStateToProps = (state) => ({
    movements: creditCardSelectors.getMovements(state),
    hasMoreMovements: creditCardSelectors.isHasMoreMovements(state),
    pageNumber: creditCardSelectors.getPageNumber(state),
    isFetchingMovements: creditCardSelectors.isFetchingMovements(state),
    filters: creditCardSelectors.getFilters(state),
});

export default connect(mapStateToProps)(CreditCardMovements);
