import React, { Component } from "react";
import Col from "react-bootstrap/lib/Col";
import { push } from "react-router-redux";
import { Field, Form, withFormik } from "formik";
import * as Yup from "yup";
import moment from "moment";
import { actions as creditCardsActions } from "reducers/creditCard";

import Container from "pages/_components/Container";
import Button from "pages/_components/Button";
import { DateField } from "pages/_components/fields/DateField";
import { bool, shape, func } from "prop-types";

import * as i18n from "util/i18n";

const FORM_ID = "creditCard.movements.filters.period";

class CreditCardPeriodFilter extends Component {
    static propTypes = {
        isSubmitting: bool,
        values: shape([]),
        resetFilters: bool,
        handleResetDateFilters: func,
    };

    static defaultProps = {
        isSubmitting: false,
        values: [],
        resetFilters: false,
        handleResetDateFilters: null,
    };

    state = {
        selectedDateFrom: null,
    };

    handleChangeDateFrom = (selectedDate) => {
        this.setState({ selectedDateFrom: selectedDate });
    };

    render() {
        const { isSubmitting, values, resetFilters, handleResetDateFilters } = this.props;
        const { selectedDateFrom } = this.state;

        return (
            <Form>
                <Container className="container--layout align-items-center" gridClassName="form-content">
                    <Col sm={12} md={4} className="col col-12 col-no-pad-mobile">
                        <Field
                            component={DateField}
                            hidePlaceholder
                            idForm={FORM_ID}
                            name="dateFrom"
                            endDate={moment(values.dateTo)}
                            handleChange={this.handleChangeDateFrom}
                            resetFilters={resetFilters}
                            handleResetDateFilters={handleResetDateFilters}
                        />
                    </Col>

                    <Col sm={12} md={4} className="col col-12 col-no-pad-mobile">
                        <Field
                            component={DateField}
                            hidePlaceholder
                            idForm={FORM_ID}
                            name="dateTo"
                            endDate={moment(values.dateTo)}
                            minDate={selectedDateFrom || moment().add(-6, "months")}
                            resetFilters={resetFilters}
                            handleResetDateFilters={handleResetDateFilters}
                        />
                    </Col>
                    <Col
                        sm={12}
                        md={4}
                        className="col col-no-pad-mobile"
                        style={{
                            alignSelf: "flex-end",
                        }}>
                        <Button bsStyle="primary" label="product.filters.filter" loading={isSubmitting} type="submit" />
                    </Col>
                </Container>
            </Form>
        );
    }
}

export default withFormik({
    validateOnChange: false,
    validateOnBlur: false,
    mapPropsToValues: () => ({
        dateFrom: null,
        dateTo: null,
    }),
    validationSchema: () =>
        Yup.lazy((values) =>
            Yup.object().shape({
                dateFrom: values.dateTo
                    ? Yup.date()
                          .nullable()
                          .max(values.dateTo, i18n.get("creditCards.movements.filters.from.error"))
                    : Yup.date().nullable(),
                dateTo: values.dateFrom
                    ? Yup.date()
                          .nullable()
                          .min(values.dateFrom, i18n.get("creditCards.movements.filters.to.error"))
                    : Yup.date().nullable(),
            }),
        ),
    handleSubmit: ({ ...filters }, formikBag) => {
        const { dispatch, isDesktop, productId } = formikBag.props;

        dispatch(creditCardsActions.movementsRequest(productId, filters, formikBag));

        if (!isDesktop) {
            const selectedPeriod = [filters.dateFrom, filters.dateTo];
            dispatch(
                push({
                    pathname: `/creditCards/${productId}/filters/results`,
                    state: { filters: { filter: "period", selectedPeriod, ...filters } },
                }),
            );
        }
    },
})(CreditCardPeriodFilter);
