import React, { Component } from "react";
import { Link } from "react-router-dom";

import Button from "pages/_components/Button";
import I18n from "pages/_components/I18n";

class CreditCardOptionsAction extends Component {
    handleClick = () => {
        const { handleClick } = this.props;

        if (handleClick) {
            handleClick();
        }
    };

    render() {
        const { className, labelKey, to } = this.props;

        return (
            (to && (
                <Link className={`btn btn-quiet ${className}`} to={to}>
                    <I18n id={labelKey} />
                </Link>
            )) || <Button className={`btn btn-quiet ${className}`} label={labelKey} onClick={this.handleClick} />
        );
    }
}

export default CreditCardOptionsAction;
