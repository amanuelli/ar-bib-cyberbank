import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Grid from "react-bootstrap/lib/Grid";
import Row from "react-bootstrap/lib/Row";
import Col from "react-bootstrap/lib/Col";
import { Field, Form, withFormik } from "formik";
import { compose } from "redux";
import { goBack } from "react-router-redux";
import { func, shape, bool, string } from "prop-types";

import {
    selectors as creditCardMovementDetailsSelectors,
    actions as creditCardMovementDetailsActions,
} from "reducers/creditCardMovementDetails";

import GoogleMapContainer from "pages/settings/_components/GoogleMap";
import Notification from "pages/_components/Notification";
import Head from "pages/_components/Head";
import Button from "pages/_components/Button";
import MainContainer from "pages/_components/MainContainer";
import TextField from "pages/_components/fields/TextField";
import FormattedDate from "pages/_components/FormattedDate";
import FormattedAmount from "pages/_components/FormattedAmount";
import I18n from "pages/_components/I18n";
import Container from "pages/_components/Container";

const FORM_ID = "creditCards.movement.note.edit";

class CreditrCardMovementDetails extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        isFetching: bool,
        match: shape({
            url: string.isRequired,
            params: shape({ id: string.isRequired }),
        }).isRequired,
        movement: shape({}).isRequired,
    };

    static defaultProps = {
        isFetching: false,
    };

    state = {
        note: "",
    };

    componentDidMount() {
        const { dispatch, match } = this.props;

        dispatch(creditCardMovementDetailsActions.detailRequest(match.params.id, match.params.idStatement));
    }

    handleEditNote = () => {
        const { dispatch, movement } = this.props;
        const { note } = this.state;

        dispatch(creditCardMovementDetailsActions.editNote(movement.idProduct, movement.idStatement, note));
    };

    handleNotesChange = (event) => {
        const { value } = event.target;

        this.setState({ note: value });
    };

    handleBack = () => {
        const { dispatch } = this.props;
        dispatch(creditCardMovementDetailsActions.closeOptions());
        dispatch(goBack());
    };

    handleClose = () => {
        const { dispatch } = this.props;
        dispatch(creditCardMovementDetailsActions.closeOptions());
        dispatch(goBack());
    };

    render() {
        const { movement, isFetching } = this.props;
        const location = { latitude: 0, longitude: 0, date: movement ? movement.date : new Date() };

        if (movement && movement.latitude && movement.longitude) {
            location.latitude = movement.latitude;
            location.longitude = movement.longitude;
        } else {
            navigator.geolocation.getCurrentPosition((currentLocation) => {
                location.latitude = currentLocation.coords.latitude;
                location.longitude = currentLocation.coords.longitude;
            });
        }

        return (
            <Fragment>
                <Head title="creditCard.statement.details" onBack={this.handleBack} onClose={this.handleClose} />
                <Notification scopeToShow="creditCardMovementDetail" />
                <MainContainer showLoader={isFetching}>
                    {movement && (
                        <Form className="above-the-fold">
                            <Container className="container--layout align-items-center">
                                <Col xl="6" className="col col-12 col-lg-8 col-md-10 col-sm-12">
                                    <div className="data-item">
                                        <span className="data-label">
                                            <span>
                                                <I18n id="creditCard.movement.details.date" />
                                            </span>
                                        </span>{" "}
                                        <span>
                                            <FormattedDate date={movement.date} />
                                        </span>
                                    </div>
                                    <div className="data-item">
                                        <span className="data-label">
                                            <span>
                                                <I18n id="creditCard.movement.details.amount" />
                                            </span>
                                        </span>{" "}
                                        <span>
                                            <FormattedAmount
                                                quantity={movement.sourceAmount}
                                                currency={movement.sourceAmountCurrency}
                                            />
                                        </span>
                                    </div>
                                    <div className="data-item">
                                        <span className="data-label">
                                            <span>
                                                <I18n id="creditCards.movement.detail.description" />
                                            </span>
                                        </span>{" "}
                                        <span className="data-text">{movement.concept}</span>
                                    </div>
                                </Col>
                            </Container>
                            <section className="container--layout align-items-center flex-grow container--map">
                                <Grid>
                                    <Row className="justify-content-center googleMap">
                                        <Col className="col col-12 col-lg-8 col-md-10 col-sm-12">
                                            <div className="form-group">
                                                <GoogleMapContainer google={window.google} positions={[location]} />
                                            </div>
                                        </Col>
                                    </Row>
                                </Grid>
                            </section>
                            <Container className="container--layout align-items-center">
                                <Col className="col col-12 col-lg-8 col-md-10 col-sm-12">
                                    <div className="form-group">
                                        <Field
                                            component={TextField}
                                            hidePlaceholder
                                            idForm={FORM_ID}
                                            name="note"
                                            type="text"
                                        />
                                    </div>
                                </Col>
                            </Container>
                            <Container className="container--layout align-items-center">
                                <Col className="col">
                                    <Button
                                        type="submit"
                                        className="btn btn-primary btn-block"
                                        label="creditCards.movement.detail.saveNotes"
                                        loading={isFetching}
                                    />
                                </Col>
                            </Container>
                        </Form>
                    )}
                </MainContainer>
            </Fragment>
        );
    }
}
const mapStateToProps = (state) => ({
    movement: creditCardMovementDetailsSelectors.getMovement(state),
    isFetching: creditCardMovementDetailsSelectors.isFetching(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        enableReinitialize: true,
        mapPropsToValues: (props) => ({
            note: props.movement ? props.movement.note : "",
        }),
        handleSubmit: ({ note }, formikBag) => {
            const { props } = formikBag;
            props.dispatch(
                creditCardMovementDetailsActions.updateNoteRequest(
                    props.match.params.id,
                    props.match.params.idStatement,
                    note,
                ),
            );
        },
    }),
)(CreditrCardMovementDetails);
