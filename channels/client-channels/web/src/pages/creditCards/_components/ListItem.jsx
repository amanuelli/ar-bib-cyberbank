import React, { Component } from "react";
import { string, bool, number, shape, oneOfType } from "prop-types";

import * as creditCardsUtils from "util/creditCards";
import * as i18nUtils from "util/i18n";
import * as dateUtils from "util/date";

import ListItem from "pages/_components/listItem/ListItem";

class CreditCardsListItem extends Component {
    static propTypes = {
        creditCard: shape({
            productAlias: string,
            number: oneOfType([number, string]),
            idProduct: string,
            shortLabel: string,
        }).isRequired,
        showProductsAsCards: bool.isRequired,
    };

    render() {
        const { creditCard } = this.props;
        const { productAlias, label, idProduct, ...props } = creditCard;
        const isExpired = dateUtils.isDateLessThanToday(props.expirationDate);
        const amount = isExpired ? creditCard.minimumPayment : creditCard.availableBalance;
        const amountLabel = isExpired
            ? i18nUtils.get("creditCard.minimumPayment")
            : i18nUtils.get("creditCard.availableCredit");

        if (!creditCard) {
            return null;
        }

        return (
            <ListItem
                {...props}
                title={i18nUtils.get("creditCards.list.item.title")}
                name={productAlias || label}
                reference={creditCard.holder}
                icon={creditCardsUtils.detectBrand(creditCard.number)}
                expiredText={i18nUtils.get("creditCards.list.item.expired")}
                expirationText={i18nUtils.get("creditCards.list.item.expiration")}
                isExpired={isExpired}
                amount={amount}
                amountLabel={amountLabel}
                currency={creditCard.availableBalanceCurrency}
                path={`/creditCards/${idProduct}`}
                idProduct={idProduct}
            />
        );
    }
}

export default CreditCardsListItem;
