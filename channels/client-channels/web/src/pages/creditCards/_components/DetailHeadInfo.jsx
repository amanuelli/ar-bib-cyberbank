import React, { Component, Fragment } from "react";
import Slider from "react-slick";
import { Grid, Row, Col, Alert } from "react-bootstrap";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { node, bool, shape } from "prop-types";

import { selectors as sessionSelectors } from "reducers/session";
import I18n from "pages/_components/I18n";
import FormattedAmount from "pages/_components/FormattedAmount";
import FormattedDate from "pages/_components/FormattedDate";
import * as i18n from "util/i18n";

import { creditCardHasExpiredPayment } from "util/creditCards";

class CreditCardDetailHeadInfo extends Component {
    static propTypes = {
        children: node.isRequired,
        creditCard: shape({}).isRequired,
        isDesktop: bool,
        payCreditCardPermission: bool.isRequired,
    };

    static defaultProps = {
        isDesktop: false,
    };

    render() {
        const { children, creditCard, isDesktop, payCreditCardPermission } = this.props;

        const settings = {
            dots: true,
            infinite: false,
            speed: 200,
            slidesToShow: isDesktop ? 3 : 1,
            slidesToScroll: 1,
            arrows: false,
        };

        const amountToPay = (
            <Col sm={12} md={3} className="content-data-wrapper col">
                <span className="data-label">
                    <I18n id="creditCard.pay.total" />
                </span>
                <b>
                    <FormattedAmount
                        quantity={creditCard.balance}
                        currency={creditCard.availableBalanceCurrency}
                        className="data-amount content-data-strong"
                    />
                </b>
                {!isDesktop && (
                    <span className="data-date">
                        {(creditCardHasExpiredPayment(creditCard) && (
                            <span>
                                <I18n id="creditCard.expired" />:<FormattedDate date={creditCard.expirationDate} />
                            </span>
                        )) || (
                            <span>
                                <I18n id="creditCard.expiration" />:<FormattedDate date={creditCard.expirationDate} />
                            </span>
                        )}
                    </span>
                )}
            </Col>
        );

        const availableCredit = (
            <Col sm={12} md={3} className="content-data-wrapper col">
                <span className="data-label">
                    <I18n id="creditCard.availableCredit" />
                </span>
                <b>
                    <FormattedAmount
                        quantity={creditCard.availableBalance}
                        currency={creditCard.availableBalanceCurrency}
                        className="data-amount content-data-strong"
                    />
                </b>
            </Col>
        );

        const expiration = (
            <Col sm={12} md={3} className="content-data-wrapper col">
                <span className="data-label">
                    <I18n id="creditCard.expiration" />
                </span>
                <b>
                    <span className="data-date content-data-strong">
                        <FormattedDate date={creditCard.expirationDate} />
                    </span>
                </b>
            </Col>
        );

        const paymentButton = payCreditCardPermission && (
            <Col sm={12} md={3} className="col content-data-item">
                <Link
                    aria-label={i18n.get("creditCard.pay.a11yLabel")}
                    className="btn btn-primary btn-block"
                    to={`/form/payCreditCard?creditCard=${creditCard.idProduct}`}>
                    <I18n id="creditCard.pay" />
                </Link>
            </Col>
        );

        return (
            (isDesktop && (
                <Fragment>
                    <Grid>
                        {creditCardHasExpiredPayment(creditCard) && (
                            <Alert bsStyle="danger" className="text-center">
                                <I18n id="creditCard.expired" />
                            </Alert>
                        )}
                        <Row className="content-data">
                            {availableCredit}
                            {amountToPay}
                            {expiration}
                            {paymentButton}
                        </Row>
                    </Grid>
                    {children}
                </Fragment>
            )) || (
                <div className="slick-slider-wrapper">
                    <Slider {...settings}>
                        {amountToPay}
                        {availableCredit}
                        {expiration}
                    </Slider>
                    <div className="content-data-item">{paymentButton}</div>
                </div>
            )
        );
    }
}

const mapStateToProps = (state) => ({
    payCreditCardPermission: sessionSelectors.hasPermissions(state, ["payCreditCard"]),
});

export default connect(mapStateToProps)(CreditCardDetailHeadInfo);
