import React, { Component } from "react";
import PropTypes from "prop-types";
import Col from "react-bootstrap/lib/Col";
import classNames from "classnames";

import I18n from "pages/_components/I18n";
import ListItem from "pages/creditCards/_components/ListItem";
import Container from "pages/_components/Container";

import * as configUtil from "util/config";

class CreditCardsList extends Component {
    static propTypes = {
        creditCards: PropTypes.arrayOf(PropTypes.object).isRequired,
    };

    render() {
        const { creditCards } = this.props;
        const maxBoxDisplay = configUtil.getInteger("product.list.maxBoxDisplay", 5);
        const productAsCard = creditCards.length <= maxBoxDisplay;
        return (
            <Container className="container--layout flex-grow">
                <Col className="col col-12" sm={12} md={9} lg={9} xl={6}>
                    <div className="table table--products">
                        <div
                            role="menu"
                            className={classNames("table-body", {
                                "table-body--grid": productAsCard,
                            })}>
                            {!creditCards.length ? (
                                <I18n id="creditCards.list.empty" />
                            ) : (
                                creditCards.map((creditCard) => (
                                    <ListItem key={creditCard.idProduct} creditCard={creditCard} />
                                ))
                            )}
                        </div>
                    </div>
                </Col>
            </Container>
        );
    }
}

export default CreditCardsList;
