import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { replace } from "react-router-redux";
import classNames from "classnames";
import { func, shape, bool } from "prop-types";

import { selectors as sessionSelectors } from "reducers/session";
import { selectors as creditCardSelectors, actions as creditCardActions } from "reducers/creditCard";
import * as dateUtils from "util/date";
import * as i18nUtils from "util/i18n";

import Button from "pages/_components/Button";
import ProductDetail from "pages/_components/ProductDetail";
import ProductToolbar from "pages/_components/ProductToolbar";
import CreditCardDetailHeadTitle from "pages/creditCards/_components/DetailHeadTitle";
import CreditCardDetailHeadInfo from "pages/creditCards/_components/DetailHeadInfo";
import CreditCardInformation from "pages/creditCards/_components/Information";
import CreditCardMovements from "pages/creditCards/_components/Movements";
import CreditCardOptions from "pages/creditCards/_components/Options";
import CreditCardOptionsAction from "pages/creditCards/_components/OptionsAction";
import CreditCardOtherFilters from "pages/creditCards/_components/OtherFilters";
import Notification from "pages/_components/Notification";
import PageLoading from "pages/_components/PageLoading";

class CreditCardDetails extends Component {
    periods = {
        lastMonth: dateUtils.getLastMonthPeriod(),
        secondLastMonth: dateUtils.getSecondLastMonthPeriod(),
        currentPeriod: dateUtils.getCurrentPeriod(),
    };

    static propTypes = {
        creditCard: shape({}).isRequired,
        match: shape({}).isRequired,
        dispatch: func.isRequired,
        isDesktop: bool.isRequired,
        hasFormsPermissionsById: shape({}).isRequired,
        isFetching: bool.isRequired,
        filters: shape({}).isRequired,
    };

    state = {
        selectedFilter: "currentPeriod",
        resetDateFilters: false,
    };

    componentDidMount() {
        const { match, dispatch } = this.props;
        const [dateFrom, dateTo] = this.periods.currentPeriod;

        const selectedFilter = "currentPeriod";
        this.setState({ selectedFilter });

        dispatch(
            creditCardActions.detailRequest(match.params.id, {
                dateFrom,
                dateTo,
            }),
        );
    }

    handleClick = () => {
        const { dispatch } = this.props;
        dispatch(creditCardActions.showOptions());
    };

    handleFilterButtonsClick = (selectedFilter, idCreditCard) => {
        const { dispatch } = this.props;
        const [dateFrom, dateTo] = this.periods[selectedFilter];

        this.setState({ selectedFilter });
        this.handleResetDateFilters(true);
        dispatch(creditCardActions.movementsRequest(idCreditCard, { dateFrom, dateTo }));
    };

    handleClickDownload = (format) => {
        const { dispatch, match, filters } = this.props;
        dispatch(creditCardActions.downloadMovements(match.params.id, filters, format));
    };

    handleBack = () => {
        const { dispatch } = this.props;
        dispatch(creditCardActions.hideOptions());
        dispatch(replace("/desktop/"));
    };

    handleResetDateFilters = (resetDateFilters) => this.setState({ resetDateFilters });

    renderFilters = () => {
        const { creditCard } = this.props;
        const { selectedFilter } = this.state;
        const [date] = this.periods.secondLastMonth;

        return (
            <Fragment>
                <Button
                    label="creditCards.movements.filters.currentPeriod"
                    className={classNames("btn btn-outline", {
                        "is-active": selectedFilter === "currentPeriod",
                    })}
                    key="currentPeriod"
                    onClick={() => this.handleFilterButtonsClick("currentPeriod", creditCard.idProduct)}
                />
                <Button
                    label="creditCards.movements.filters.lastMonth"
                    className={classNames("btn btn-outline", {
                        "is-active": selectedFilter === "lastMonth",
                    })}
                    key="lastMonth"
                    onClick={() => this.handleFilterButtonsClick("lastMonth", creditCard.idProduct)}
                />
                <Button
                    className={classNames("btn btn-outline", {
                        "is-active": selectedFilter === "secondLastMonth",
                    })}
                    key="secondLastMonth"
                    onClick={() => this.handleFilterButtonsClick("secondLastMonth", creditCard.idProduct)}
                    label="accounts.movements.filters.secondLastMonth"
                    replace={{
                        SECOND_LAST_MONTH: `${
                            i18nUtils.getArray("global.months")[date.getMonth()]
                        } ${date.getFullYear().toString()}`,
                    }}
                />
            </Fragment>
        );
    };

    render() {
        const { creditCard, isDesktop, dispatch, hasFormsPermissionsById, isFetching } = this.props;
        const { selectedFilter } = this.state;

        return (
            <Fragment>
                <Notification scopeToShow="creditCardDetails" />
                <PageLoading loading={isFetching}>
                    {!isFetching && (
                        <Fragment>
                            <ProductDetail>
                                <ProductDetail.Head
                                    onBack={this.handleBack}
                                    dispatch={dispatch}
                                    handleOptionsClick={this.handleClick}
                                    productId={creditCard.productId}
                                    infoComponent={{
                                        data: (
                                            <CreditCardInformation
                                                keyLabel="creditCard.info"
                                                creditCard={creditCard}
                                                isDesktop={isDesktop}
                                                dispatch={dispatch}
                                            />
                                        ),
                                    }}
                                    onClickDownloadPDF={() => this.handleClickDownload("pdf")}
                                    onClickDownloadXLS={() => this.handleClickDownload("xls")}>
                                    <CreditCardDetailHeadTitle
                                        dispatch={dispatch}
                                        creditCard={creditCard}
                                        isDesktop={isDesktop}>
                                        <CreditCardInformation
                                            keyLabel="creditCard.info"
                                            creditCard={creditCard}
                                            isDesktop={isDesktop}
                                            dispatch={dispatch}
                                        />
                                    </CreditCardDetailHeadTitle>
                                    <CreditCardDetailHeadInfo creditCard={creditCard} isDesktop={isDesktop}>
                                        <ProductToolbar>
                                            {hasFormsPermissionsById.cashAdvance && (
                                                <CreditCardOptionsAction
                                                    isDesktop={isDesktop}
                                                    labelKey="creditCard.cashAdvance"
                                                    to={`/form/cashAdvance?creditcard=${creditCard.idProduct}`}
                                                />
                                            )}
                                            {hasFormsPermissionsById.lostOrStolenCreditCard && (
                                                <CreditCardOptionsAction
                                                    isDesktop={isDesktop}
                                                    labelKey="creditCard.theftOrLoss"
                                                    to={`/form/lostOrStolenCreditCard?creditCardSelector=${creditCard.idProduct}`}
                                                />
                                            )}
                                            {hasFormsPermissionsById.additionalCreditCardRequest && (
                                                <CreditCardOptionsAction
                                                    isDesktop={isDesktop}
                                                    labelKey="creditCard.additional"
                                                    to={`/form/additionalCreditCardRequest?creditCard=${creditCard.idProduct}`}
                                                />
                                            )}
                                        </ProductToolbar>
                                    </CreditCardDetailHeadInfo>
                                </ProductDetail.Head>
                                <ProductDetail.Body
                                    isDesktop={isDesktop}
                                    filters={this.renderFilters()}
                                    filtersKeyLabel="accounts.movements"
                                    handleResetDateFilters={this.handleResetDateFilters}
                                    moreFilters={
                                        <CreditCardOtherFilters
                                            isDesktop={isDesktop}
                                            dispatch={dispatch}
                                            productId={creditCard.idProduct}
                                            currency={creditCard.availableBalanceCurrency}
                                            resetFilters={this.state.resetDateFilters}
                                            handleResetDateFilters={this.handleResetDateFilters}
                                        />
                                    }
                                    moreFiltersClosedKeyLabel="accounts.movements.filters.more"
                                    moreFiltersOpenedKeyLabel="accounts.movements.filters.less">
                                    <CreditCardMovements
                                        selectedPeriod={this.periods[selectedFilter]}
                                        idProduct={creditCard.idProduct}
                                        keyLabel="creditCard.statements"
                                        isDesktop={isDesktop}
                                    />
                                    {!isDesktop && (
                                        <CreditCardInformation
                                            keyLabel="creditCard.info"
                                            creditCard={creditCard}
                                            dispatch={dispatch}
                                        />
                                    )}
                                </ProductDetail.Body>
                            </ProductDetail>

                            {!isDesktop && (
                                <CreditCardOptions>
                                    {hasFormsPermissionsById.creditCardRequest && (
                                        <CreditCardOptionsAction
                                            isDesktop={isDesktop}
                                            labelKey="creditCard.new"
                                            to="/form/creditCardRequest"
                                        />
                                    )}
                                    {hasFormsPermissionsById.additionalCreditCardRequest && (
                                        <CreditCardOptionsAction
                                            isDesktop={isDesktop}
                                            labelKey="creditCard.additional"
                                            to={`/form/additionalCreditCardRequest?creditCard=${creditCard.idProduct}`}
                                        />
                                    )}
                                    {hasFormsPermissionsById.reissueCreditCard && (
                                        <CreditCardOptionsAction
                                            isDesktop={isDesktop}
                                            labelKey="creditCard.reissue"
                                            to="/form/reissueCreditCard"
                                        />
                                    )}
                                    <CreditCardOptionsAction
                                        isDesktop={isDesktop}
                                        labelKey="creditCard.alias.set"
                                        to={`/creditCards/${creditCard.idProduct}/alias`}
                                    />
                                    {hasFormsPermissionsById.cashAdvance && (
                                        <CreditCardOptionsAction
                                            isDesktop={isDesktop}
                                            labelKey="creditCard.cashAdvance"
                                            to={`/form/cashAdvance?creditcard=${creditCard.idProduct}`}
                                        />
                                    )}
                                    {hasFormsPermissionsById.creditCardChangeCondition && (
                                        <CreditCardOptionsAction
                                            isDesktop={isDesktop}
                                            labelKey="creditCard.change.conditions"
                                            to={`/form/creditCardChangeCondition?creditCard=${creditCard.idProduct}`}
                                        />
                                    )}
                                    {hasFormsPermissionsById.lostOrStolenCreditCard && (
                                        <CreditCardOptionsAction
                                            isDesktop={isDesktop}
                                            labelKey="creditCard.theftOrLoss"
                                            to={`/form/lostOrStolenCreditCard?creditCardSelector=${creditCard.idProduct}`}
                                        />
                                    )}
                                </CreditCardOptions>
                            )}
                        </Fragment>
                    )}
                </PageLoading>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    creditCard: creditCardSelectors.getDetail(state),
    isFetching: creditCardSelectors.getFetching(state),
    hasFormsPermissionsById: sessionSelectors.getActiveEnvironmentForms(state, "creditcards").reduce(
        (byId, { idForm }) => ({
            ...byId,
            [idForm]: true,
        }),
        {},
    ),
    filters: creditCardSelectors.getFilters(state),
});

export default connect(mapStateToProps)(CreditCardDetails);
