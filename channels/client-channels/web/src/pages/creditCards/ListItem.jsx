import React, { Component } from "react";
import { string, number as typeNumber, bool } from "prop-types";
import * as i18nUtils from "util/i18n";
import * as creditCardsUtils from "util/creditCards";
import * as dateUtils from "util/date";
import * as configUtils from "util/config";

import ListItem from "pages/_components/listItem/ListItem";

class CreditCardsListItem extends Component {
    static propTypes = {
        number: string.isRequired,
        productAlias: string,
        minimumPayment: typeNumber,
        availableBalance: typeNumber,
        idProduct: string,
        label: string,
        expirationDate: string,
        path: string,
        hideAmountLabel: bool,
        showLink: bool,
    };

    static defaultProps = {
        productAlias: null,
        minimumPayment: null,
        availableBalance: null,
        idProduct: null,
        label: null,
        expirationDate: null,
        path: null,
        hideAmountLabel: false,
        showLink: true,
    };

    render() {
        const {
            productAlias,
            minimumPayment,
            availableBalance,
            idProduct,
            label,
            expirationDate,
            path,
            number,
            hideAmountLabel,
            showLink,
        } = this.props;
        const productName = productAlias || label;
        const icon = creditCardsUtils.detectBrand(number);
        const isExpired = dateUtils.isDateLessThanToday(expirationDate);
        const amount = isExpired ? minimumPayment : availableBalance;
        const amountLabel = isExpired
            ? i18nUtils.get("creditCard.minimumPayment")
            : i18nUtils.get("creditCard.availableCredit");
        const finalPath = path || `/creditCards/${idProduct}`;
        const productTypeTitle = i18nUtils.get("desktop.widgets.creditCard");

        return (
            <ListItem
                {...this.props}
                idProduct={idProduct}
                productTypeTitle={productTypeTitle}
                title={i18nUtils.get("creditCards.list.item.title")}
                name={productName}
                reference={`···· ${number.slice(-4)}`}
                icon={icon}
                expiredText={i18nUtils.get("creditCards.list.item.expired")}
                expirationText={i18nUtils.get("creditCards.list.item.expiration")}
                isExpired={isExpired}
                amount={amount}
                amountLabel={!hideAmountLabel && amountLabel}
                path={showLink ? finalPath : null}
                currency={!hideAmountLabel && configUtils.get("core.masterCurrency")}
            />
        );
    }
}

export default CreditCardsListItem;
