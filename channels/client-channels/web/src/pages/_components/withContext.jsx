import React from "react";

const withContext = ({ Consumer }, key) => (Component) => (props) => (
    <Consumer>{(value) => <Component {...{ [key]: { ...value } }} {...props} />}</Consumer>
);

export default withContext;
