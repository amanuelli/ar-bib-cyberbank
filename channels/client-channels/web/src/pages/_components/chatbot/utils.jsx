import { defaultTheme } from "@livechat/ui-kit";

const themes = {
    defaultTheme: {
        vars: {
            "primary-color": "#c30000",
        },
        AgentBar: {
            ...defaultTheme.AgentBar,
            css: {
                ...defaultTheme.AgentBar.css,
                backgroundColor: "rgb(249, 249, 251)",
            },
        },
        Bubble: {
            ...defaultTheme.Bubble,
            css: {
                ...defaultTheme.Bubble.css,
                borderTopLeftRadius: "6px",
                borderTopRightRadius: "6px",
                borderBottomLeftRadius: "6px",
                borderBottomRightRadius: "6px",
                fontWeight: "bold",
                backgroundColor: "rgb(249, 249, 251)",
            },
        },
        FixedWrapperMaximized: {
            css: {
                boxShadow: "0 0 1em rgba(0, 0, 0, 0.1)",
                borderRadius: "12px 12px 0px 0px",
                maxHeight: "80vh",
            },
        },
        TitleBar: {
            ...defaultTheme.TitleBar,
            css: {
                ...defaultTheme.TitleBar.css,
                padding: "0.5rem",
                fontSize: "1.175rem",
                fontWeight: "bold",
                borderRadius: "12px 12px 0px 0px",
                margin: "0px",
            },
        },
    },
};

export default themes;
