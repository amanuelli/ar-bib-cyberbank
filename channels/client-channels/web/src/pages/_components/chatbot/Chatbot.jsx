import React, { useState } from "react";

import { ThemeProvider, FixedWrapper } from "@livechat/ui-kit";
import Maximized from "./Maximized";
import Minimized from "./Minimized";
import themes from "./utils";

const Chatbot = (props) => {
    const [theme] = useState("defaultTheme");

    return (
        <ThemeProvider theme={themes[theme]}>
            <div>
                <FixedWrapper.Root>
                    <FixedWrapper.Maximized>
                        <Maximized {...props} />
                    </FixedWrapper.Maximized>
                    <FixedWrapper.Minimized>
                        <Minimized {...props} />
                    </FixedWrapper.Minimized>
                </FixedWrapper.Root>
            </div>
        </ThemeProvider>
    );
};

export default Chatbot;
