import React, { useState, useEffect } from "react";
import { func, arrayOf, shape, bool } from "prop-types";
import { connect } from "react-redux";
import * as i18nUtils from "util/i18n";
import {
    TitleBar,
    TextInput,
    MessageList,
    Message,
    MessageText,
    MessageGroup,
    TextComposer,
    Row,
    IconButton,
    SendButton,
    CloseIcon,
    Bubble,
} from "@livechat/ui-kit";

import { actions as chatbotActions, selectors as chatbotSelectors } from "reducers/chatbot";

const Maximized = (props) => {
    const { dispatch, minimize, stateMessages, isFetching } = props;

    const [messageValue, setMessageValue] = useState("");

    const handleOnChange = (e) => {
        setMessageValue(e.target.value);
    };

    const handleOnSend = () => {
        dispatch(chatbotActions.sendMessageRequest(messageValue));
    };

    useEffect(() => {
        dispatch(chatbotActions.endSession());
        dispatch(chatbotActions.sendMessageRequest("", "TECHBANK_WELCOME"));
        return () => {
            dispatch(chatbotActions.sendMessageRequest("", "TECHBANK_GOODBYE"));
            dispatch(chatbotActions.endSession());
        };
    }, [dispatch]);

    return (
        <div className="chatbot-container">
            <TitleBar
                rightIcons={[
                    <IconButton key="close" onClick={minimize}>
                        <CloseIcon />
                    </IconButton>,
                ]}
                title={i18nUtils.get("assistant.chatbot.title")}
            />
            <div className="chatbot-messages-container">
                <MessageList active>
                    {stateMessages &&
                        stateMessages.map((message) => (
                            <MessageGroup avatar={!message.own && "/favicon.png"} isOwn={message.own}>
                                <Message isOwn={message.own}>
                                    <Bubble>
                                        <MessageText>{message.text}</MessageText>
                                    </Bubble>
                                </Message>
                            </MessageGroup>
                        ))}
                </MessageList>
            </div>
            <TextComposer value={messageValue} onChange={handleOnChange} onSend={handleOnSend} active={!isFetching}>
                <Row align="center">
                    <TextInput fill />
                    <SendButton fit />
                </Row>
            </TextComposer>
        </div>
    );
};

const mapStateToProps = (state) => ({
    stateMessages: chatbotSelectors.getMessages(state),
    isFetching: chatbotSelectors.isFetching(state),
});

export default connect(mapStateToProps)(Maximized);

Maximized.propTypes = {
    minimize: func.isRequired,
    dispatch: func.isRequired,
    stateMessages: arrayOf(shape({})).isRequired,
    isFetching: bool.isRequired,
};
