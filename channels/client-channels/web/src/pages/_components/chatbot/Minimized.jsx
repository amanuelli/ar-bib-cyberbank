import * as React from "react";
import { IconButton, ChatIcon } from "@livechat/ui-kit";
import { func } from "prop-types";

const Minimized = ({ maximize }) => (
    <div onClick={maximize} className="chatbot-bubble">
        <IconButton color="#fff">
            <ChatIcon />
        </IconButton>
    </div>
);

export default Minimized;

Minimized.propTypes = {
    maximize: func.isRequired,
};
