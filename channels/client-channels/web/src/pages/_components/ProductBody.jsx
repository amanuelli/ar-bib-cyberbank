import React, { Component, Fragment } from "react";
import { node, func, bool, shape } from "prop-types";

import ProductFilters from "pages/_components/ProductFilters";
import MainContainer from "pages/_components/MainContainer";
import Tabs from "pages/_components/Tabs";

class ProductBody extends Component {
    static propTypes = {
        children: node.isRequired,
        fetching: func.isRequired,
        isDesktop: bool.isRequired,
        filters: shape().isRequired,
    };

    render() {
        const { children, fetching, filters, isDesktop, ...rest } = this.props;

        return (
            <Fragment>
                {(isDesktop && (
                    <MainContainer showLoader={fetching}>
                        <div className="above-the-fold">
                            {filters && <ProductFilters {...rest}>{filters}</ProductFilters>}
                            {children}
                        </div>
                    </MainContainer>
                )) || (
                    <div className="view-page">
                        <div className="view-content">
                            <Tabs components={children} />
                        </div>
                    </div>
                )}
            </Fragment>
        );
    }
}

export default ProductBody;
