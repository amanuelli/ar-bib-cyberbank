import React, { Component } from "react";

const withLocationChange = (onLocationChange) => (BaseComponent) =>
    class extends Component {
        static displayName = `withLocationChange${BaseComponent.name}`;

        componentDidMount() {
            onLocationChange(this.props);
        }

        componentDidUpdate(prevProps) {
            if (prevProps.location.pathname !== this.props.location.pathname) {
                onLocationChange(this.props);
            }
        }

        render() {
            return <BaseComponent {...this.props} />;
        }
    };

export default withLocationChange;
