import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import isEmpty from "lodash/isEmpty";
import Lottie from "react-lottie";
import { func, shape, bool, string, oneOfType, any } from "prop-types";

import { selectors as weatherSelectors, actions as weatherActions } from "reducers/weather";

import animations from "styles/animations/weather/weatherAnimations";

import { resizableRoute } from "pages/_components/Resizable";
import PageLoading from "pages/_components/PageLoading";
import * as i18n from "util/i18n";

class Weather extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        weather: shape({
            momentDay: string,
            weather: string,
        }),
        animationOnLoadImages: func,
        isMobile: bool.isRequired,
        location: shape({
            pathname: string.isRequired,
        }).isRequired,
        className: string,
        children: oneOfType([any]),
        style: shape({}),
    };

    static defaultProps = {
        weather: {},
        animationOnLoadImages: null,
        className: "",
        children: {},
        style: {},
    };

    constructor(props) {
        super(props);
        this.state = {
            isStopped: true,
            isPaused: true,
            downloading: true,
        };
    }

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(weatherActions.weatherRequest());
    }

    render() {
        const { weather, isMobile, location, className, children, style } = this.props;
        const { downloading, isStopped, isPaused } = this.state;

        const animationClass = isEmpty(weather) ? "" : weather.momentDay + weather.weather;

        const animationOptions = {
            loop: false,
            autoplay: false,
            animationData: animations[animationClass],
            rendererSettings: {
                preserveAspectRatio: "xMidYMid slice",
            },
        };

        const eventListeners = [
            {
                eventName: "complete",
                callback: () => {
                    this.setState({ isPaused: true, downloading: false });
                },
            },
            {
                eventName: "loaded_images",
                callback: () => {
                    const { animationOnLoadImages } = this.props;
                    if (animationOnLoadImages) {
                        animationOnLoadImages();
                    }

                    this.setState({
                        isPaused: false,
                        isStopped: false,
                        downloading: false,
                    });
                },
            },
        ];

        const getWeatherA11y = (mom, stat) =>
            `${i18n.get(`login.weather.time.${mom}.a11y`)}, ${i18n.get(`login.weather.connector.a11y`)} ${i18n.get(
                `login.weather.status.${stat ? stat.toLowerCase() : stat}.a11y`,
            )}`;

        const momentDay = !isEmpty(weather) && !downloading ? weather.momentDay + weather.weather : "";

        return (
            <Fragment>
                <PageLoading loading={downloading}>
                    <div className={`${className} ${momentDay}`} style={style}>
                        {children}
                    </div>
                </PageLoading>

                {weather && (
                    <div
                        style={{ position: "absolute" }}
                        className={` login-background-wrapper ${
                            downloading
                                ? "hidden"
                                : animationClass + (isMobile && location.pathname !== "/" ? " weatherFadeOut" : "")
                        }`}>
                        <Lottie
                            options={animationOptions}
                            isStopped={isStopped}
                            isPaused={isPaused}
                            eventListeners={eventListeners}
                            ariaLabel={getWeatherA11y(weather.momentDay, weather.weather)}
                            ariaRole="complementary"
                        />
                    </div>
                )}
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    weather: weatherSelectors.getWeather(state) || {},
});

export default withRouter(connect(mapStateToProps)(resizableRoute(Weather)));
