import React, { Component, Fragment } from "react";
import Grid from "react-bootstrap/lib/Grid";
import Row from "react-bootstrap/lib/Row";
import Col from "react-bootstrap/lib/Col";
import MainContainer from "pages/_components/MainContainer";
import Head from "pages/_components/Head";
import { getLang } from "util/i18n";
import { func, shape } from "prop-types";

class PrivacyPolicy extends Component {
    static propTypes = {
        history: shape({ goBack: func }),
    };

    static defaultProps = {
        history: null,
    };

    backButtonAction = () => {
        const { history } = this.props;
        history.goBack();
    };

    render() {
        return (
            <Fragment>
                <Head title="global.privacy" onClose={this.backButtonAction} />

                <MainContainer>
                    <div className="above-the-fold">
                        <section className="container--layout flex-grow">
                            <Grid fluid>
                                <Row className="justify-content-center">
                                    <Col sm={12} md={9} lg={6} className="col">
                                        {getLang() === "en" && (
                                            <div className="scrollable-content">
                                                Technisys S.A. built the TechBank app as a Commercial app. This SERVICE
                                                is provided by Technisys S.A. and is intended for use as is. This page
                                                is used to inform visitors regarding our policies with the collection,
                                                use, and disclosure of Personal Information if anyone decided to use our
                                                Service. If you choose to use our Service, then you agree to the
                                                collection and use of information in relation to this policy. The
                                                Personal Information that we collect is used for providing and improving
                                                the Service. We will not use or share your information with anyone
                                                except as described in this Privacy Policy. The terms used in this
                                                Privacy Policy have the same meanings as in our Terms and Conditions,
                                                which is accessible at TechBank unless otherwise defined in this Privacy
                                                Policy.
                                                <br />
                                                <br />
                                                <h3>Information Collection and Use</h3>
                                                <br />
                                                For a better experience, while using our Service, we may require you to
                                                provide us with certain personally identifiable information, including
                                                but not limited to Full Name, ID, Email, Mobile number and Biometric
                                                data (for on-boarding purposes). The information that we request will be
                                                retained by us and used as described in this privacy policy. The app
                                                does use third party services that may collect information used to
                                                identify you.
                                                <br />
                                                <br />
                                                <h3>Google Play Services</h3>
                                                <br />
                                                <h4>Log Data</h4>
                                                <br />
                                                We want to inform you that whenever you use our Service, in a case of an
                                                error in the app we collect data and information (through third party
                                                products) on your phone called Log Data. This Log Data may include
                                                information such as your device Internet Protocol (&quot;IP&quot;)
                                                address, device name, operating system version, the configuration of the
                                                app when utilizing our Service, the time and date of your use of the
                                                Service, and other statistics.
                                                <br />
                                                <br />
                                                <h4>Cookies</h4>
                                                <br />
                                                Cookies are files with a small amount of data that are commonly used as
                                                anonymous unique identifiers. These are sent to your browser from the
                                                websites that you visit and are stored on your device&apos;s internal
                                                memory. This Service does not use these &quot;cookies&quot; explicitly.
                                                However, the app may use third party code and libraries that use
                                                &quot;cookies&quot; to collect information and improve their services.
                                                You have the option to either accept or refuse these cookies and know
                                                when a cookie is being sent to your device. If you choose to refuse our
                                                cookies, you may not be able to use some portions of this Service.
                                                <br />
                                                <br />
                                                <h4>Service Providers</h4>
                                                <br />
                                                We may employ third-party companies and individuals due to the following
                                                reasons: To facilitate our Service; To provide the Service on our
                                                behalf; To perform Service-related services; or To assist us in
                                                analyzing how our Service is used. We want to inform users of this
                                                Service that these third parties have access to your Personal
                                                Information. The reason is to perform the tasks assigned to them on our
                                                behalf. However, they are obligated not to disclose or use the
                                                information for any other purpose.
                                                <br />
                                                <br />
                                                <h4>Security</h4>
                                                <br />
                                                We value your trust in providing us your Personal Information, thus we
                                                are striving to use commercially acceptable means of protecting it. But
                                                remember that no method of transmission over the internet, or method of
                                                electronic storage is 100% secure and reliable, and we cannot guarantee
                                                its absolute security.
                                                <br />
                                                <br />
                                                <h4>Links to Other Sites</h4>
                                                <br />
                                                This Service may contain links to other sites. If you click on a
                                                third-party link, you will be directed to that site. Note that these
                                                external sites are not operated by us. Therefore, we strongly advise you
                                                to review the Privacy Policy of these websites. We have no control over
                                                and assume no responsibility for the content, privacy policies, or
                                                practices of any third-party sites or services.
                                                <br />
                                                <br />
                                                <h4>Children’s Privacy</h4>
                                                <br />
                                                These Services do not address anyone under the age of 13. We do not
                                                knowingly collect personally identifiable information from children
                                                under 13. In the case we discover that a child under 13 has provided us
                                                with personal information, we immediately delete this from our servers.
                                                If you are a parent or guardian and you are aware that your child has
                                                provided us with personal information, please contact us so that we will
                                                be able to do necessary actions.
                                                <br />
                                                <br />
                                                <h4>Changes to This Privacy Policy</h4>
                                                <br />
                                                We may update our Privacy Policy from time to time. Thus, you are
                                                advised to review this page periodically for any changes. We will notify
                                                you of any changes by posting the new Privacy Policy on this page. These
                                                changes are effective immediately after they are posted on this page.
                                                <br />
                                                <br />
                                                <h4>Contact Us</h4>
                                                <br />
                                                If you have any questions or suggestions about our Privacy Policy, do
                                                not hesitate to contact us at cdp@technisys.com.
                                            </div>
                                        )}
                                        {getLang() === "es" && (
                                            <div className="scrollable-content">
                                                Technisys S.A. desarroll&oacute; la aplicaci&oacute;n TechBank como una
                                                aplicaci&oacute;n comercial. Este SERVICIO es proporcionado por
                                                Technisys S.A. y est&aacute; destinado para su uso tal cual. Esta
                                                p&aacute;gina se utiliza para informar a los visitantes sobre nuestras
                                                pol&iacute;ticas con la recopilaci&oacute;n, uso y divulgaci&oacute;n de
                                                informaci&oacute;n personal si alguien decide utilizar nuestro servicio.
                                                Si elige utilizar nuestro Servicio, entonces acepta la
                                                recopilaci&oacute;n y el uso de informaci&oacute;n en relaci&oacute;n
                                                con esta pol&iacute;tica. La informaci&oacute;n personal que recopilamos
                                                se utiliza para proporcionar y mejorar el Servicio. No utilizaremos ni
                                                compartiremos su informaci&oacute;n con nadie, excepto como se describe
                                                en esta Pol&iacute;tica de privacidad. Los t&eacute;rminos utilizados en
                                                esta Pol&iacute;tica de privacidad tienen el mismo significado que en
                                                nuestros T&eacute;rminos y condiciones, a los que se puede acceder en
                                                TechBank a menos que se defina lo contrario en esta Pol&iacute;tica de
                                                privacidad.
                                                <br />
                                                <br />
                                                <h3>Recolecci&oacute;n de informaci&oacute;n y uso</h3>
                                                <br />
                                                Para una mejor experiencia, mientras utiliza nuestro Servicio, es
                                                posible que le solicitemos que nos proporcione cierta informaci&oacute;n
                                                de identificaci&oacute;n personal, que incluye, entre otros, nombre
                                                completo, documento de identidad, correo electr&oacute;nico,
                                                n&uacute;mero de tel&eacute;fono m&oacute;vil y datos
                                                biom&eacute;tricos. La informaci&oacute;n que solicitamos ser&aacute;
                                                retenida por nosotros y utilizada como se describe en esta
                                                pol&iacute;tica de privacidad. La aplicaci&oacute;n utiliza servicios de
                                                terceros que pueden recopilar informaci&oacute;n utilizada para
                                                identificarlo.
                                                <br />
                                                <br />
                                                <h3>Google Play Services</h3>
                                                <br />
                                                <h4>Datos de Registro</h4>
                                                <br />
                                                Queremos informarle que cada vez que utiliza nuestro Servicio, en caso
                                                de un error en la aplicaci&oacute;n, recopilamos datos e
                                                informaci&oacute;n en su tel&eacute;fono llamados Datos de registro.
                                                Estos Datos de registro pueden incluir informaci&oacute;n como la
                                                direcci&oacute;n &quot;IP&quot; de su dispositivo, el nombre del
                                                dispositivo, la versi&oacute;n del sistema operativo, la
                                                configuraci&oacute;n de la aplicaci&oacute;n al utilizar nuestro
                                                Servicio, la hora y la fecha de uso del Servicio y otras
                                                estad&iacute;sticas .
                                                <br />
                                                <br />
                                                <h4>Cookies</h4>
                                                <br />
                                                Las cookies son archivos con una peque&ntilde;a cantidad de datos que se
                                                usan com&uacute;nmente como identificadores &uacute;nicos
                                                an&oacute;nimos. Estos se env&iacute;an a su navegador desde los sitios
                                                web que visita y se almacenan en la memoria interna de su dispositivo.
                                                Este servicio no utiliza estas &quot;cookies&quot;
                                                expl&iacute;citamente. Sin embargo, la aplicaci&oacute;n puede usar
                                                c&oacute;digo de terceros y bibliotecas que usan &quot;cookies&quot;
                                                para recopilar informaci&oacute;n y mejorar sus servicios. Tiene la
                                                opci&oacute;n de aceptar o rechazar estas cookies y saber cu&aacute;ndo
                                                se env&iacute;a una cookie a su dispositivo. Si elige rechazar nuestras
                                                cookies, es posible que no pueda usar algunas partes de este Servicio.
                                                <br />
                                                <br />
                                                <h4>Proveedores de Servicio</h4>
                                                <br />
                                                Podemos emplear a empresas e individuos de terceros debido a las
                                                siguientes razones: para facilitar nuestro servicio; Para proporcionar
                                                el Servicio en nuestro nombre; Para realizar servicios relacionados con
                                                el Servicio; o Para ayudarnos a analizar c&oacute;mo se utiliza nuestro
                                                Servicio. Queremos informar a los usuarios de este Servicio que estos
                                                terceros tienen acceso a su Informaci&oacute;n personal. La raz&oacute;n
                                                es realizar las tareas que se les asignaron en nuestro nombre. Sin
                                                embargo, est&aacute;n obligados a no divulgar ni utilizar la
                                                informaci&oacute;n para ning&uacute;n otro prop&oacute;sito.
                                                <br />
                                                <br />
                                                <h4>Seguridad</h4>
                                                <br />
                                                Valoramos su confianza en proporcionarnos su informaci&oacute;n
                                                personal, por lo tanto, nos esforzamos por utilizar medios
                                                comercialmente aceptables para protegerla. Pero recuerde que
                                                ning&uacute;n m&eacute;todo de transmisi&oacute;n por Internet o
                                                m&eacute;todo de almacenamiento electr&oacute;nico es 100% seguro y
                                                confiable, y no podemos garantizar su seguridad absoluta.
                                                <br />
                                                <br />
                                                <h4>Enlaces a otros sitios</h4>
                                                <br />
                                                Este servicio puede contener enlaces a otros sitios. Si hace clic en un
                                                enlace de un tercero, ser&aacute; dirigido a ese sitio. Tenga en cuenta
                                                que estos sitios externos no son operados por nosotros. Por lo tanto, le
                                                recomendamos encarecidamente que revise la pol&iacute;tica de privacidad
                                                de estos sitios web. No tenemos control ni asumimos ninguna
                                                responsabilidad por el contenido, las pol&iacute;ticas de privacidad o
                                                las pr&aacute;cticas de sitios o servicios de terceros.
                                                <br />
                                                <br />
                                                <h4>Privacidad de los Ni&ntilde;os</h4>
                                                <br />
                                                Estos Servicios no se dirigen a ninguna persona menor de 13 a&ntilde;os.
                                                No recopilamos a sabiendas informaci&oacute;n de identificaci&oacute;n
                                                personal de ni&ntilde;os menores de 13 a&ntilde;os. En el caso de que
                                                descubramos que un ni&ntilde;o menor de 13 a&ntilde;os nos ha
                                                proporcionado informaci&oacute;n personal, la eliminamos inmediatamente
                                                de nuestros servidores. Si usted es padre o tutor y sabe que su hijo nos
                                                ha proporcionado informaci&oacute;n personal, cont&aacute;ctenos para
                                                que podamos realizar las acciones necesarias.
                                                <br />
                                                <br />
                                                <h4>Cambios a esta pol&iacute;tica de privacidad</h4>
                                                <br />
                                                Podemos actualizar nuestra Pol&iacute;tica de privacidad, por lo tanto
                                                se recomienda que revise esta p&aacute;gina peri&oacute;dicamente en
                                                busca de cambios. Le notificaremos cualquier cambio publicando la nueva
                                                pol&iacute;tica de privacidad en esta p&aacute;gina. Estos cambios son
                                                efectivos inmediatamente despu&eacute;s de su publicaci&oacute;n en esta
                                                p&aacute;gina.
                                                <br />
                                                <br />
                                                <h4>Cont&aacute;ctenos</h4>
                                                <br />
                                                Si tiene alguna pregunta o sugerencia sobre nuestra pol&iacute;tica de
                                                privacidad, no dude en comunicarse con nosotros a cdp@technisys.com.
                                            </div>
                                        )}
                                        {getLang() === "pt" && (
                                            <div className="scrollable-content">
                                                Technisys S.A. desenvolveu o aplicativo TechBank como um aplicativo
                                                comercial. Este SERVI&Ccedil;O &eacute; fornecido pela Technisys S.A. e
                                                deve ser usado como est&aacute;. Esta p&aacute;gina &eacute; usada para
                                                informar os visitantes sobre nossas pol&iacute;ticas com o coleta, uso e
                                                divulga&ccedil;&atilde;o de informa&ccedil;&otilde;es pessoais, se
                                                algu&eacute;m decidir usar nosso servi&ccedil;o Se voc&ecirc; optar por
                                                usar nosso Servi&ccedil;o, aceita a coleta e o uso de
                                                informa&ccedil;&otilde;es em rela&ccedil;&atilde;o a esta
                                                pol&iacute;tica. Informa&ccedil;&otilde;es pessoais que Coletamos
                                                &eacute; usado para fornecer e melhorar o Servi&ccedil;o. N&oacute;s
                                                n&atilde;o vamos usar ou Compartilharemos suas informa&ccedil;&otilde;es
                                                com qualquer pessoa, exceto conforme descrito nesta Pol&iacute;tica de
                                                Privacidade. Os termos usados nesta Pol&iacute;tica de Privacidade
                                                t&ecirc;m o mesmo significado que em Nossos Termos e
                                                Condi&ccedil;&otilde;es, que podem ser acessados ​​no TechBank, a menos
                                                que Defina o contr&aacute;rio nesta Pol&iacute;tica de Privacidade.
                                                <br />
                                                <br />
                                                <h3>Coleta de informa&ccedil;&otilde;es e uso</h3>
                                                <br />
                                                Para uma experi&ecirc;ncia melhor, ao usar nosso Servi&ccedil;o, podemos
                                                solicitar que voc&ecirc; nos fornece determinadas
                                                informa&ccedil;&otilde;es de identifica&ccedil;&atilde;o pessoal, que
                                                incluem, entre outras, nome completo, documento de identidade, email,
                                                n&uacute;mero de celular e dados biom&eacute;tricos As
                                                informa&ccedil;&otilde;es solicitadas ser&atilde;o retidas por
                                                n&oacute;s e usadas como Est&aacute; descrito nesta pol&iacute;tica de
                                                privacidade. O aplicativo usa servi&ccedil;os de terceiros que eles
                                                podem coletar informa&ccedil;&otilde;es usadas para
                                                identific&aacute;-lo. pueden recopilar informaci&oacute;n utilizada para
                                                identificarlo.
                                                <br />
                                                <br />
                                                <h3>Google Play Services</h3>
                                                <br />
                                                <h4>Dados de Registro</h4>
                                                <br />
                                                Queremos informar que toda vez que voc&ecirc; usa nosso Servi&ccedil;o,
                                                em caso de erro no aplicativo, Coletamos dados e
                                                informa&ccedil;&otilde;es no seu telefone chamado Dados de registro.
                                                Estes dados de registro pode incluir informa&ccedil;&otilde;es como o
                                                endere&ccedil;o &quot;IP&quot; do seu dispositivo, o nome do
                                                dispositivo, a vers&atilde;o do sistema operacional, a
                                                configura&ccedil;&atilde;o do aplicativo ao usar nosso Servi&ccedil;o, a
                                                hora e a data de uso do Servi&ccedil;o e outras estat&iacute;sticas.
                                                <br />
                                                <br />
                                                <h4>Cookies</h4>
                                                <br />
                                                Cookies s&atilde;o arquivos com uma pequena quantidade de dados usados
                                                geralmente como identificadores exclusivos an&ocirc;nimos. Estes
                                                s&atilde;o enviados para o seu navegador dos sites que voc&ecirc; visita
                                                e s&atilde;o armazenados na mem&oacute;ria Interno do seu dispositivo.
                                                Este servi&ccedil;o n&atilde;o utiliza estes &quot;cookies&quot;
                                                explicitamente No entanto, o aplicativo pode usar bibliotecas e
                                                c&oacute;digos de terceiros quem usa &quot;cookies&quot; reunir
                                                informa&ccedil;&otilde;es e melhorar sua servi&ccedil;os Voc&ecirc; tem
                                                a op&ccedil;&atilde;o de aceitar ou rejeitar esses cookies e saber
                                                quando um cookie &eacute; enviado para o seu dispositivo. Se voc&ecirc;
                                                optar por rejeitar nossa Cookies, talvez voc&ecirc; n&atilde;o consiga
                                                usar algumas partes deste Servi&ccedil;o.
                                                <br />
                                                <br />
                                                <h4>Prestadores de Servi&ccedil;os</h4>
                                                <br />
                                                Podemos empregar empresas e indiv&iacute;duos de terceiros devido
                                                &agrave; seguintes raz&otilde;es: para facilitar nosso servi&ccedil;o;
                                                Para proporcionar o servi&ccedil;o em nosso nome; Para executar
                                                servi&ccedil;os relacionados com o servi&ccedil;o; o Para nos ajudar a
                                                analisar como nossos Servi&ccedil;o. Queremos informar aos
                                                usu&aacute;rios deste Servi&ccedil;o que esses Terceiros t&ecirc;m
                                                acesso &agrave;s suas informa&ccedil;&otilde;es pessoais. O motivo
                                                &eacute; realizar as tarefas atribu&iacute;das a eles em nosso nome. No
                                                entanto, eles s&atilde;o exigido a n&atilde;o divulgar ou usar as
                                                informa&ccedil;&otilde;es para qualquer outra finalidade.
                                                <br />
                                                <br />
                                                <h4>Seguran&ccedil;a</h4>
                                                <br />
                                                Valorizamos sua confian&ccedil;a em nos fornecer suas
                                                informa&ccedil;&otilde;es pessoais, Portanto, nos esfor&ccedil;amos para
                                                usar a m&iacute;dia comercialmente aceit&aacute;vel proteg&ecirc;-lo.
                                                Mas lembre-se de que nenhum m&eacute;todo de O m&eacute;todo de
                                                transmiss&atilde;o pela Internet ou armazenamento eletr&ocirc;nico
                                                &eacute; 100% Seguro e confi&aacute;vel, e n&atilde;o podemos garantir
                                                sua seguran&ccedil;a absoluta.
                                                <br />
                                                <br />
                                                <h4>Links para outros sites</h4>
                                                <br />
                                                Este servi&ccedil;o pode conter links para outros sites. Se voc&ecirc;
                                                clicar em um O link de terceiros ser&aacute; direcionado para esse site.
                                                tenha em conta que Esses sites externos n&atilde;o s&atilde;o operados
                                                por n&oacute;s. Portanto, voc&ecirc; &eacute; altamente
                                                recomend&aacute;vel que voc&ecirc; reveja a pol&iacute;tica de
                                                privacidade de Esses sites. N&atilde;o temos controle ou assumimos
                                                qualquer responsabilidade para o conte&uacute;do, pol&iacute;ticas de
                                                privacidade ou pr&aacute;ticas de sites ou servi&ccedil;os de terceiros.
                                                <br />
                                                <br />
                                                <h4>Privacidad de los Ni&ntilde;os</h4>
                                                <br />
                                                Esses servi&ccedil;os n&atilde;o s&atilde;o direcionados a menores de 13
                                                anos. N&atilde;o coletamos intencionalmente informa&ccedil;&otilde;es de
                                                identifica&ccedil;&atilde;o pessoal de Crian&ccedil;as menores de 13
                                                anos. No caso em que descobrimos que uma crian&ccedil;a com menos de 13
                                                anos nos forneceu informa&ccedil;&otilde;es pessoais, as
                                                exclu&iacute;mos imediatamente de nossos servidores. Se voc&ecirc;
                                                &eacute; pai ou respons&aacute;vel e Saiba que seu filho nos forneceu
                                                informa&ccedil;&otilde;es pessoais, entre em contato conosco para que
                                                possamos executar as a&ccedil;&otilde;es necess&aacute;rias.
                                                <br />
                                                <br />
                                                <h4>Altera&ccedil;&otilde;es nesta pol&iacute;tica de privacidade</h4>
                                                <br />
                                                Podemos atualizar nossa Pol&iacute;tica de Privacidade, portanto
                                                recomenda que voc&ecirc; verifique esta p&aacute;gina periodicamente
                                                quanto a altera&ccedil;&otilde;es. Notificaremos voc&ecirc; sobre
                                                quaisquer altera&ccedil;&otilde;es publicando a nova pol&iacute;tica de
                                                privacidade nesta p&aacute;gina. Essas altera&ccedil;&otilde;es
                                                s&atilde;o efetivas imediatamente ap&oacute;s Sua postagem nesta
                                                p&aacute;gina.
                                                <br />
                                                <br />
                                                <h4>Entre em contato conosco</h4>
                                                <br />
                                                Se voc&ecirc; tiver alguma d&uacute;vida ou sugest&atilde;o sobre nossa
                                                pol&iacute;tica de privacidade, N&atilde;o hesite em contactar-nos em
                                                cdp@technisys.com.
                                            </div>
                                        )}
                                    </Col>
                                </Row>
                            </Grid>
                        </section>
                    </div>
                </MainContainer>
            </Fragment>
        );
    }
}

export default PrivacyPolicy;
