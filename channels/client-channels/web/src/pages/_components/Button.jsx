import React, { Component, Fragment } from "react";
import { Button as BSButton } from "react-bootstrap";
import { bool, func, shape, string, node } from "prop-types";

import I18n from "pages/_components/I18n";
import Image from "pages/_components/Image";

class Button extends Component {
    static propTypes = {
        label: string,
        loading: bool,
        type: string,
        disabled: bool,
        className: string,
        image: string,
        bsStyle: string,
        onClick: func,
        href: string,
        id: string,
        replace: shape({}),
        block: bool,
        defaultLabelText: string,
        style: shape({}),
        children: node.isRequired,
    };

    static defaultProps = {
        disabled: false,
        loading: false,
        block: true,
        type: "button",
        defaultLabelText: "",
        replace: null,
        style: null,
        label: "",
        className: "",
        image: "",
        href: "",
        id: "",
        bsStyle: undefined,
        onClick: () => {},
    };

    constructor(props) {
        super(props);
        this.alreadyClicked = false;
    }

    onClick = (e) => {
        const { loading, onClick } = this.props;

        if (!loading && !this.alreadyClicked) {
            this.alreadyClicked = true;

            if (typeof onClick === "function") {
                onClick(e);
            }
            /* workaround para casos en que no se cambia el loading, que siempre está en false */
            setTimeout(this.resetLink, 0);
        }
    };

    resetLink = () => {
        this.alreadyClicked = false;
    };

    renderChildren = ({ children, image, label, defaultLabelText, ...replace }) => (
        <>
            {children ? (
                <Fragment>{children}</Fragment>
            ) : (
                <Fragment>
                    {image && <Image src={image} className="svg-icon" />}
                    <I18n id={label} {...replace} defaultValue={defaultLabelText} />
                </Fragment>
            )}
        </>
    );

    render() {
        const {
            bsStyle,
            block,
            className,
            id,
            image,
            label,
            loading = false,
            disabled,
            type,
            defaultLabelText,
            style,
            href,
            onClick,
            replace,
            children,
            ...props
        } = this.props;

        const target = href ? { href } : { onClick };

        return (
            <BSButton
                {...target}
                id={id}
                type={type}
                disabled={loading || disabled}
                block={block}
                className={`${loading ? "is-loading" : ""} ${className}`}
                bsStyle={bsStyle}
                style={style}
                {...props}>
                {loading ? <Spinner /> : this.renderChildren({ children, image, label, defaultLabelText, ...replace })}
            </BSButton>
        );
    }
}

function Spinner() {
    return (
        <Fragment>
            <span className="btn-loading-indicator">
                <span />
                <span />
                <span />
            </span>
            <span className="btn-loading-text">Loading</span>
        </Fragment>
    );
}

export default Button;
