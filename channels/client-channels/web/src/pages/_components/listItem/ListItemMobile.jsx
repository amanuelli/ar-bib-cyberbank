import React, { Component } from "react";
import { Link } from "react-router-dom";
import { bool, number, oneOfType, shape, string } from "prop-types";
import classNames from "classnames";

import FormattedAmount from "pages/_components/FormattedAmount";
import I18n from "pages/_components/I18n";
import ItemExpirationDate from "pages/_components/item/ExpirationDate";
import ItemName from "pages/_components/item/Name";

class ListItemMobile extends Component {
    static propTypes = {
        amount: number.isRequired,
        amountLabel: string.isRequired,
        currency: string.isRequired,
        expirationDate: string,
        expirationText: string,
        expiredText: string,
        icon: string,
        isExpired: bool,
        name: string.isRequired,
        overdraftEnabled: bool,
        path: oneOfType([string, shape({ pathname: string })]).isRequired,
        reference: string.isRequired,
        title: string.isRequired,
        franchise: string,
        productTypeTitle: string.isRequired,
        idProduct: string.isRequired,
    };

    static defaultProps = {
        expirationDate: null,
        expirationText: null,
        expiredText: null,
        icon: null,
        isExpired: null,
        overdraftEnabled: null,
        franchise: "",
    };

    getContent = () => {
        const {
            amount,
            amountLabel,
            currency,
            expirationDate,
            expirationText,
            expiredText,
            icon,
            isExpired,
            name,
            overdraftEnabled,
            reference,
            title,
            idProduct,
        } = this.props;
        return (
            <div className={classNames("Product", { "is-expired": isExpired })}>
                <ItemName name={name} icon={icon} />
                <div className="Product-row">
                    <p className="data-label">{title}</p>
                    <p className="data-desc">
                        <span>{reference}</span>
                    </p>
                </div>
                <div id={idProduct} className="Product-row text-right">
                    <p className="data-label">{amountLabel}</p>
                    <FormattedAmount quantity={amount} currency={currency} />
                    {overdraftEnabled && (
                        <span className="Product-label">
                            <span className="text-label quiet-label">
                                <I18n id="accounts.overdraft.enabled" />
                            </span>
                        </span>
                    )}
                    {expirationDate && (
                        <span className="Product-label">
                            <ItemExpirationDate
                                date={expirationDate}
                                expiredText={expiredText}
                                expirationText={expirationText}
                                isExpired={isExpired}
                            />
                        </span>
                    )}
                </div>
            </div>
        );
    };

    render() {
        const { name, path, franchise, productTypeTitle, idProduct } = this.props;

        if (path) {
            return (
                <Link
                    aria-label={`${productTypeTitle}: ${franchise !== "" ? `${franchise} ` : ""}${name}`}
                    aria-describedby={idProduct}
                    className="col col-sm-12"
                    to={path}
                    role="menuitem">
                    {this.getContent()}
                </Link>
            );
        }
        return this.getContent();
    }
}

export default ListItemMobile;
