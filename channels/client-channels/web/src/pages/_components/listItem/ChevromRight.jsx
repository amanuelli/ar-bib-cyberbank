import React, { Component } from "react";
import Image from "pages/_components/Image";

class ChevromRight extends Component {
    render() {
        return (
            <div className="table-data table-data-icon">
                <Image src="images/chevromRight.svg" className="svg-icon svg-caret" />
            </div>
        );
    }
}

export default ChevromRight;
