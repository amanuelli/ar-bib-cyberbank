import React from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Navbar from "react-bootstrap/lib/Navbar";
import classNames from "classnames";
import { push } from "react-router-redux/actions";
import { arrayOf, bool, func, number, objectOf, shape, string } from "prop-types";

import { actions as sessionActions, selectors as sessionSelectors } from "reducers/session";
import { selectors as communicationsSelectors } from "reducers/communications";
import { actions as notificationActions } from "reducers/notification";
import { getLastLoginDate, getLastLoginPlace } from "util/settings";
import { selectors as i18nSelectors } from "reducers/i18n";
import { get as getMessage } from "util/i18n";

import I18n from "pages/_components/I18n";
import Image from "pages/_components/Image";
import UserLink from "pages/_components/UserLink";
import Button from "pages/_components/Button";
import Badge from "pages/_components/Badge";
import Dropdown from "pages/_components/Dropdown";
import EnvironmentTag from "pages/_components/EnvironmentTag";
import Popup from "pages/_components/Popup";
import Avatar from "pages/_components/Avatar";

class NavigationBarDesktop extends React.Component {
    static propTypes = {
        activeEnvironment: shape({
            administrationScheme: string,
            forms: shape({}),
            id: number,
            name: string,
            permissions: shape({
                accounts: bool,
                creditcards: bool,
                loans: bool,
                payCreditCard: bool,
                payLoan: bool,
                payThirdPartiesCreditCard: bool,
                payThirdPartiesLoan: bool,
                requestTransactionCancellation: bool,
                transferForeign: bool,
                transferInternal: bool,
                transferLocal: bool,
                transferThirdParties: bool,
            }),
            type: string,
        }),
        dispatch: func.isRequired,
        environments: objectOf(
            shape({
                environmentType: string,
                idEnvironment: string,
                name: string,
            }),
        ),
        loggedUser: shape({
            accessToken: string,
            defaultAvatarId: string,
            email: string,
            previousLoginInfo: arrayOf(
                shape({
                    city: string,
                    country: string,
                    date: string,
                    idUser: string,
                    ip: string,
                    latitude: number,
                    longitude: number,
                }),
            ),
            securitySeal: number,
            userFullName: string,
        }),
        unreadCommunications: number,
    };

    static defaultProps = {
        activeEnvironment: null,
        environments: null,
        loggedUser: null,
        unreadCommunications: 0,
    };

    state = {
        communications: 0,
    };

    logOut = () => {
        const { dispatch } = this.props;
        dispatch(sessionActions.logout());
    };

    goToEnvironment = (idEnvironment) => {
        const { dispatch, environments } = this.props;
        if (environments[idEnvironment].allowedToAccess === "true") {
            dispatch(sessionActions.changeEnvironment(idEnvironment, false, null));
        } else {
            dispatch(
                notificationActions.showNotification(
                    getMessage("administration.restrictions.unavailableEnvironment"),
                    "error",
                    ["desktop"],
                ),
            );
            dispatch(push("/desktop"));
        }
    };

    getLastLoginInfo = () => {
        const { loggedUser } = this.props;
        const { previousLoginInfo } = loggedUser;

        if (previousLoginInfo && previousLoginInfo.length) {
            return (
                <React.Fragment>
                    <div className="last-login">
                        <p className="ellipsis data-label">
                            <I18n id="settings.lastLogin.date" />
                        </p>
                        <p className="ellipsis data-desc">
                            {getLastLoginDate(previousLoginInfo[0])}
                            <Link to="/settings/lastLogin">{getLastLoginPlace(previousLoginInfo[0])}</Link>
                        </p>
                    </div>
                </React.Fragment>
            );
        }
        return null;
    };

    onMailboxAnimationEnd = () => {
        const { unreadCommunications } = this.props;
        this.setState({ communications: unreadCommunications });
    };

    render() {
        const { environments, activeEnvironment, loggedUser, unreadCommunications } = this.props;
        const { communications } = this.state;
        const hasToAnimateMailBox = communications === unreadCommunications;

        return (
            <Navbar collapseOnSelect fluid>
                <Navbar.Header>
                    <div>
                        <Navbar.Brand>
                            <Link className="navbar-brand" to="/desktop">
                                <Image src="images/logoCompany.svg" />
                                <I18n id="global.companyName" componentProps={{ className: "visually-hidden" }} />
                            </Link>
                        </Navbar.Brand>

                        <ul className="nav navbar-nav navbar-right header-command-pallette">
                            <li>
                                <Link to="/communications">
                                    <div>
                                        <div className="rounder-content">
                                            {unreadCommunications > 0 && (
                                                <>
                                                    <Badge
                                                        count={unreadCommunications}
                                                        className={classNames({ animation: !hasToAnimateMailBox })}
                                                    />
                                                    <I18n
                                                        id="menu.chat.badge"
                                                        componentProps={{ className: "visually-hidden" }}
                                                    />
                                                </>
                                            )}
                                            <Image className="svg-icon" src="images/email.svg" />
                                        </div>
                                    </div>
                                    <I18n id="menu.chat" />
                                </Link>
                            </li>

                            {environments && Object.keys(environments).length > 1 && (
                                <li>
                                    <Dropdown
                                        buttonClass="header-dropdown-button"
                                        dropdownButtonContent={
                                            <EnvironmentTag
                                                type={activeEnvironment.type}
                                                name={activeEnvironment.name}
                                            />
                                        }
                                        pullRight>
                                        {Object.values(environments)
                                            .filter(
                                                (environment) =>
                                                    parseInt(environment.idEnvironment, 10) !== activeEnvironment.id,
                                            )
                                            .map((environment) => (
                                                <Button
                                                    role="option"
                                                    className="dropdown__item-btn header-dropdown-item"
                                                    bsStyle="link"
                                                    onClick={() => this.goToEnvironment(environment.idEnvironment)}>
                                                    <EnvironmentTag
                                                        type={environment.environmentType}
                                                        name={environment.name}
                                                    />
                                                </Button>
                                            ))}
                                    </Dropdown>
                                </li>
                            )}
                            <li>
                                <Popup
                                    popupTitleID="menu.settings.panelTitle"
                                    popupButtonContent={
                                        <>
                                            <Avatar user={loggedUser} />
                                            <I18n id="menu.settings" />
                                        </>
                                    }>
                                    <UserLink loggedUser={loggedUser} />
                                    <div
                                        style={{
                                            display: "flex",
                                        }}>
                                        {this.getLastLoginInfo()}
                                        <Button
                                            className="btn btn-link btn-small"
                                            onClick={this.logOut}
                                            label="global.logout"
                                        />
                                    </div>
                                </Popup>
                            </li>
                        </ul>
                    </div>
                </Navbar.Header>
            </Navbar>
        );
    }
}

const mapStateToProps = (state) => ({
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    environments: sessionSelectors.getEnvironments(state),
    lang: i18nSelectors.getLang(state),
    loggedUser: sessionSelectors.getUser(state),
    unreadCommunications: communicationsSelectors.getUnreadCommunications(state),
});

export default connect(mapStateToProps)(NavigationBarDesktop);
