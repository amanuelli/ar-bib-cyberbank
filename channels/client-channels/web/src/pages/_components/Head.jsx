import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Navbar from "react-bootstrap/lib/Navbar";
import Link from "react-router-dom/Link";
import { routerActions } from "react-router-redux/actions";
import { string, func, node, bool, int, shape } from "prop-types";
import classNames from "classnames";

import { selectors as sessionSelectors } from "reducers/session";
import { selectors as communicationsSelectos } from "reducers/communications";

import { resizableRoute } from "pages/_components/Resizable";
import I18n from "pages/_components/I18n";
import Menu from "pages/_components/menu/Menu";
import Image from "pages/_components/Image";
import Header from "pages/_components/header/Header";
import Badge from "pages/_components/Badge";
import Button from "pages/_components/Button";
import Dropdown from "pages/_components/Dropdown";
import ViewTitle from "pages/_components/header/components/ViewTitle";
import { Helmet } from "react-helmet";
import * as utilsI18n from "util/i18n";

import { selectors as assistantSelectors } from "reducers/assistant";

class Head extends Component {
    static propTypes = {
        // i18n head title
        title: node,
        // specify a translated text instead a i18n key
        titleText: string,
        // back link url
        backLinkTo: string,
        // back function, ignored if backLinkTo is specified
        onBack: func,
        // close link url
        closeLinkTo: string,
        // onClose function link, ignored if closeLinkTo is specified
        onClose: func,
        // link url, it links located under title
        linkTo: string,
        // I18n id of link text, it is located under title
        linkText: string,
        // head logo
        logo: node,
        // called right after button with closeLinkTo is clicked
        onCloseClick: func,
        hasCenterContent: bool,
        showPlusIcon: bool,
        accessibilityTextId: string,
        dispatch: func.isRequired,
        hasActiveSession: bool,
        isMobile: bool,
        isDesktop: bool,
        unreadCommunications: int,
        onAdd: func.isRequired,
        addLinkTo: string,
        addLinkToLabel: string,
        exportList: string,
        handleInviteClick: func.isRequired,
        environments: string,
        activeEnvironment: string,
        note: string,
        handleClick: func.isRequired,
        isFetchingExport: bool,
        children: shape().isRequired,
        replace: string,
        hideNavbarInMobile: bool,
        updateFocus: bool, // Prop which defines if component will re render if updated or not
    };

    static defaultProps = {
        title: null,
        titleText: "",
        backLinkTo: "",
        onBack: null,
        closeLinkTo: "",
        onClose: null,
        linkTo: "",
        linkText: "",
        logo: null,
        onCloseClick: () => {},
        hasCenterContent: false,
        showPlusIcon: false,
        accessibilityTextId: "",
        hasActiveSession: false,
        isMobile: false,
        isDesktop: false,
        unreadCommunications: 0,
        addLinkTo: "",
        addLinkToLabel: "",
        exportList: "",
        environments: "",
        activeEnvironment: "",
        note: "",
        isFetchingExport: false,
        replace: "",
        hideNavbarInMobile: false,
        updateFocus: true,
    };

    constructor(props) {
        super(props);
        this.titleRef = React.createRef();
    }

    componentDidMount() {
        if (this.titleRef.current) {
            this.titleRef.current.focus({
                preventScroll: true,
            });
        }
    }

    componentDidUpdate() {
        const { updateFocus } = this.props;
        if (this.titleRef.current && updateFocus) {
            this.titleRef.current.focus({
                preventScroll: true,
            });
        }
    }

    back = () => {
        const { dispatch, backLinkTo } = this.props;
        dispatch(routerActions.replace({ pathname: backLinkTo, state: { transition: "transition-drill-out" } }));
    };

    getLeftOption = () => {
        const {
            backLinkTo,
            onBack,
            closeLinkTo,
            onClose,
            hasActiveSession,
            isMobile,
            unreadCommunications,
        } = this.props;
        if (backLinkTo || onBack) {
            return (
                <div className="toolbar-item toolbar-item--fixed toolbar-item-left">
                    <Button
                        className="toolbar-btn view-back"
                        onClick={backLinkTo ? this.back : onBack}
                        image="images/arrowLeft.svg"
                        label="global.back"
                        bsStyle="link"
                    />
                </div>
            );
        }
        if (hasActiveSession && isMobile && !closeLinkTo && !onClose) {
            return (
                <div className="toolbar-item toolbar-item--fixed toolbar-item-left mailIcon">
                    <Link className="btn toolbar-btn" to="/communications">
                        <Image className="svg-icon svg-caret" src="images/email.svg" />
                        <I18n id="menu.chat" />
                        {unreadCommunications > 0 && (
                            <>
                                <Badge count={unreadCommunications} />
                                <I18n id="menu.chat.badge" componentProps={{ className: "visually-hidden" }} />
                            </>
                        )}
                    </Link>
                </div>
            );
        }

        return null;
    };

    close = () => {
        const { onCloseClick, dispatch, closeLinkTo } = this.props;
        onCloseClick();
        dispatch(routerActions.replace({ pathname: closeLinkTo, state: { transition: "transition-flow-close" } }));
    };

    add = () => {
        const { dispatch, onAdd, addLinkTo, isDesktop } = this.props;

        if (onAdd) {
            onAdd();
        }
        let routerAction = {
            pathname: addLinkTo,
            state: { transition: "transition-flow-open" },
        };

        if (isDesktop) {
            // con la transition no está funcionando en Desktop, se quita y se sigue el issue para encontrar el motivo
            routerAction = {
                pathname: addLinkTo,
            };
        }

        dispatch(routerActions.replace(routerAction));
    };

    getRightOption = () => {
        const {
            addLinkTo,
            addLinkToLabel,
            closeLinkTo,
            exportList,
            handleInviteClick,
            hasActiveSession,
            isDesktop,
            isMobile,
            onAdd,
            onClose,
            showPlusIcon,
            environments,
            activeEnvironment,
            note,
            hideNavbarInMobile,
        } = this.props;

        const noteElement = (
            <div className="toolbar-item view-note">
                <span>{note}</span>
            </div>
        );

        const closeElement = (
            <div className="toolbar-item toolbar-item--fixed toolbar-item-right">
                <Button
                    className="toolbar-btn view-close"
                    onClick={closeLinkTo ? this.close : onClose}
                    image="images/cross.svg"
                    label="global.close"
                    bsStyle="link"
                />
            </div>
        );

        if (closeLinkTo || onClose || note) {
            return (
                <>
                    {note && noteElement}
                    {(closeLinkTo || onClose) && closeElement}
                </>
            );
        }

        let buttons = [];

        if (exportList) {
            const { handleClick, isFetchingExport } = this.props;

            buttons = [
                ...buttons,
                <div className="toolbar-item toolbar-item--fixed" key="exportList">
                    <Dropdown
                        image="images/download.svg"
                        label="global.download"
                        buttonClass="btn btn-outline"
                        fetching={isFetchingExport}
                        pullRight>
                        <Button
                            onClick={() => handleClick("pdf")}
                            label="global.download.pdf"
                            className="dropdown__item-btn"
                            bsStyle="link"
                        />
                        <Button
                            onClick={() => handleClick("xls")}
                            label="global.download.xls"
                            className="dropdown__item-btn"
                            bsStyle="link"
                        />
                    </Dropdown>
                </div>,
            ];
        }

        const addButton = (
            <div className="toolbar-item toolbar-item--fixed toolbar-item-right" key="add">
                <Button
                    onClick={this.add}
                    className={classNames({ "btn-outline": isDesktop, "toolbar-btn view-close": !isDesktop })}>
                    <Image src="images/plus.svg" />
                    <I18n id={addLinkToLabel} componentProps={!isDesktop && { className: "visually-hidden" }} />
                </Button>
            </div>
        );

        if (addLinkTo || onAdd) {
            if (showPlusIcon) {
                return addButton;
            }
            buttons = [addButton, ...buttons];
        }

        if (handleInviteClick) {
            buttons = [
                <div className="toolbar-item toolbar-item--fixed toolbar-item-right" key="invite">
                    <Button
                        onClick={handleInviteClick}
                        className="btn btn-small btn-outline"
                        image="/images/plus.svg"
                        label="administration.users.list.addUser"
                    />
                </div>,
                ...buttons,
            ];
        }

        if (hasActiveSession && isMobile && !hideNavbarInMobile) {
            return (
                <Fragment>
                    <Navbar.Toggle />
                    <Navbar.Collapse>
                        {environments && Object.keys(environments).length > 1 && (
                            <Link
                                className="change-environment-link"
                                to={{
                                    pathname: "/settings/changeEnvironment",
                                }}>
                                <div className="environment-section">
                                    <Image src={`images/${activeEnvironment.type}.svg`} />
                                    <span className="data-desc">{activeEnvironment.name}</span>
                                    <Image
                                        src="images/chevromRight.svg"
                                        className="environment-section-arrow"
                                        wrapperClassName="svg-wrapper svg-wrapper-right"
                                    />
                                </div>
                            </Link>
                        )}
                        <Menu isMobile={isMobile} />
                    </Navbar.Collapse>
                </Fragment>
            );
        }

        return buttons.length ? <Fragment>{buttons}</Fragment> : null;
    };

    getCenterContent = () => {
        const { children, logo, replace, title, titleText, hasCenterContent, accessibilityTextId } = this.props;

        return (
            (title || titleText || logo || hasCenterContent || accessibilityTextId) && (
                <Fragment>
                    <Helmet>
                        <title>
                            {// eslint-disable-next-line no-nested-ternary
                            utilsI18n.get(accessibilityTextId || title) !== "*undefined*"
                                ? `Techbank - ${`${utilsI18n.get(accessibilityTextId || title)} ${replace.USER_NAME ||
                                      ""}`}`
                                : titleText !== ""
                                ? `Techbank - ${titleText}`
                                : "Techbank"}
                        </title>
                    </Helmet>
                    {(title || titleText) && (
                        <ViewTitle
                            id={title}
                            accessibilityTextId={accessibilityTextId}
                            replace={replace}
                            defaultValue={titleText}
                        />
                    )}
                    {children}
                    {this.getCenterOption()}
                </Fragment>
            )
        );
    };

    getCenterOption = () => {
        const { linkTo, linkText } = this.props;
        if (linkTo) {
            return (
                <p>
                    <Link to={linkTo}>
                        <I18n id={linkText} />
                    </Link>
                </p>
            );
        }
        return null;
    };

    render() {
        return (
            <Header>
                {this.getLeftOption()}
                {
                    // eslint-disable-next-line jsx-a11y/no-noninteractive-tabindex
                    <div className="toolbar-item view-title" tabIndex="0" ref={this.titleRef}>
                        {this.getCenterContent()}
                    </div>
                }
                {this.getRightOption()}
            </Header>
        );
    }
}

const mapStateToProps = (state) => ({
    hasActiveSession: assistantSelectors.isAssistantLogin(state) ? false : sessionSelectors.isLoggedIn(state),
    unreadCommunications: communicationsSelectos.getUnreadCommunications(state),
    environments: sessionSelectors.getEnvironments(state),
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
});

export default connect(mapStateToProps)(resizableRoute(Head));
