import React, { Component } from "react";
import { bool, func, string, array, number, shape, arrayOf } from "prop-types";

import PageLoading from "pages/_components/PageLoading";
import I18n from "pages/_components/I18n";
import Button from "pages/_components/Button";
import Image from "pages/_components/Image";
import * as i18n from "util/i18n";
import { SALARY_PAYMENT_ID_FORM } from "constants.js";
import { isArray } from "lodash-es";

class ProductList extends Component {
    static propTypes = {
        onLoadMoreClick: func.isRequired,
        renderItem: func.isRequired,
        children: func.isRequired,
        noDataText: string.isRequired,
        // we don't care about the shape of item because we don't handle the renderItem in this component
        // eslint-disable-next-line react/forbid-prop-types
        items: array,
        loadMoreText: string,
        noMoreDataText: string,
        noFiltersDataText: string,
        fetching: bool,
        lastPage: bool,
        pageNumber: number,
        noDataImage: string,
        firstFetched: bool,
        filters: shape({}),
        politeMessage: shape({}),
        match: shape({
            params: shape({ idForm: string }),
        }),
        lines: arrayOf(shape({})),
    };

    static defaultProps = {
        loadMoreText: "",
        noMoreDataText: "",
        noFiltersDataText: "accounts.movements.filters.none",
        fetching: false,
        lastPage: true,
        pageNumber: 0,
        items: [],
        noDataImage: "images/coloredIcons/folder-empty.svg",
        firstFetched: true,
        filters: {},
        politeMessage: React.createRef(),
        match: undefined,
        lines: undefined,
    };

    handleClick = () => {
        const { onLoadMoreClick, pageNumber } = this.props;
        onLoadMoreClick(pageNumber);
    };

    renderPoliteMessage = (message) => {
        const { politeMessage } = this.props;
        if (politeMessage && politeMessage.current) {
            politeMessage.current.innerText = i18n.get(message);
        }
    };

    // special case for in memory front pagination. Manuallt add lines
    manageSalaryPaymentCase = () => {
        const { fetching, loadMoreText, noMoreDataText, items, lines } = this.props;
        if ((loadMoreText || fetching) && items.length < lines.length) {
            return (
                <div className="text-center no-more-data flex">
                    <Button
                        bsStyle="link"
                        onClick={this.handleClick}
                        image="images/show.svg"
                        loading={fetching}
                        label={loadMoreText}
                        className="btn-small"
                    />
                </div>
            );
        }
        return (
            <div className="text-center no-more-data">
                <p>
                    <I18n id={noMoreDataText} />
                </p>
            </div>
        );
    };

    renderLoadMore = () => {
        const { fetching, loadMoreText, lastPage, noMoreDataText, items, match } = this.props;
        const isEmpty = items.length === 0;
        if (lastPage) {
            if (!isEmpty && noMoreDataText) {
                this.renderPoliteMessage(noMoreDataText);
            }
            return (
                !isEmpty &&
                noMoreDataText && (
                    <div className="text-center no-more-data">
                        <p>
                            <I18n id={noMoreDataText} />
                        </p>
                    </div>
                )
            );
        }

        if (match && match.params && match.params.idForm === SALARY_PAYMENT_ID_FORM) {
            return this.manageSalaryPaymentCase();
        }

        // TODO doing a workaround here to show a loader when fetching more items on mobile
        // it will be better that I pass a loading label from props and then show it when fetching
        return (
            <div className="text-center no-more-data flex">
                {(loadMoreText || fetching) && (
                    <Button
                        bsStyle="link"
                        onClick={this.handleClick}
                        image="images/show.svg"
                        loading={fetching}
                        label={loadMoreText}
                        className="btn-small"
                    />
                )}
            </div>
        );
    };

    render() {
        const {
            firstFetched,
            fetching,
            filters,
            children,
            items,
            renderItem,
            noDataText,
            noDataImage,
            noFiltersDataText,
            politeMessage,
        } = this.props;
        let withFilters = true;
        const isEmpty = items.length === 0;

        Object.keys(filters).map((key) => {
            if (filters[key]) {
                if (isArray(filters[key])) {
                    if (filters[key].length === 0) {
                        withFilters = false;
                    }
                }
            } else {
                withFilters = false;
            }
            return "";
        });

        if (withFilters && isEmpty) {
            this.renderPoliteMessage(noFiltersDataText);
        } else if (!firstFetched && isEmpty) {
            this.renderPoliteMessage(noDataText);
        }
        return (
            <PageLoading loading={isEmpty && fetching}>
                {children(items.map(renderItem), this.renderLoadMore)}
                {isEmpty && (
                    <div className="text-center no-more-data" key="noMoreMovements">
                        <div className="illustration-wrapper">
                            <Image src={noDataImage} className="svg-big-icon" />
                        </div>
                        <p className="text-lead">
                            {withFilters && isEmpty ? <I18n id={noFiltersDataText} /> : <I18n id={noDataText} />}
                        </p>
                    </div>
                )}
                <div ref={politeMessage} className="visually-hidden" aria-live="polite" />
            </PageLoading>
        );
    }
}

export default ProductList;
