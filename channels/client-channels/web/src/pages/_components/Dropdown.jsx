/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-noninteractive-element-interactions */
import React, { Component, createRef } from "react";
import { arrayOf, element, bool, string, node } from "prop-types";
import classNames from "classnames";
import { Spring, config } from "react-spring";

import Button from "pages/_components/Button";

class Dropdown extends Component {
    node = createRef();

    static propTypes = {
        children: arrayOf(element).isRequired,
        /**
         * Align the menu to the right side of the Dropdown
         */
        pullRight: bool,
        fetching: bool,
        buttonClass: string,
        label: string,
        image: string,
        bsStyle: string,
        dropdownButtonContent: node,
    };

    static defaultProps = {
        pullRight: false,
        bsStyle: null,
        fetching: false,
        buttonClass: null,
        label: null,
        image: null,
        dropdownButtonContent: null,
    };

    state = {
        isOpen: false,
        navIndex: 0,
    };

    componentDidMount() {
        document.addEventListener("mousedown", this.handleClick, false);
    }

    componentWillUnmount() {
        document.removeEventListener("mousedown", this.handleClick, false);
    }

    handleClick = ({ target }) => {
        const { isOpen } = this.state;
        if (!this.node.current.contains(target) && isOpen) {
            this.toggleOpen();
        }
    };

    toggleOpen = () => {
        this.setState((prevState) => ({ isOpen: !prevState.isOpen, navIndex: 0 }));
    };

    onChildFocus = (param) => {
        this.setState({ navIndex: param });
    };

    onBlurHandler = () => {
        this.setState({ isOpen: false });
    };

    render() {
        const { image, pullRight, children, fetching, buttonClass, label, bsStyle, dropdownButtonContent } = this.props;
        const { isOpen, navIndex } = this.state;
        const childrenLength = React.Children.toArray(children).length;

        const keyPressHandler = (ev) => {
            let aux = navIndex;
            if (ev.shiftKey && ev.key === "Tab") {
                aux -= 1;
            } else if (ev.key === "Tab") {
                aux += 1;
            }

            if (aux >= childrenLength || aux < 0) {
                this.setState({ isOpen: false });
            }
        };

        return (
            <div className="dropdown" ref={this.node}>
                <Button
                    image={image}
                    loading={fetching}
                    onClick={this.toggleOpen}
                    block={false}
                    className={buttonClass}
                    label={label}
                    bsStyle={bsStyle}
                    aria-haspopup
                    aria-expanded={isOpen}
                    onKeyDown={keyPressHandler}>
                    {dropdownButtonContent}
                </Button>

                <Spring
                    config={config.stiff}
                    from={{ height: 0 }}
                    to={{
                        height: isOpen ? "auto" : 0,
                        overflow: "hidden",
                        borderWidth: isOpen ? 1 : 0,
                    }}>
                    {(styles) =>
                        isOpen && (
                            <ul
                                style={styles}
                                className={classNames("dropdown__menu", {
                                    "dropdown__menu--right": pullRight,
                                })}>
                                {React.Children.map(children, (child, ix) => (
                                    <li
                                        className="dropdown__item"
                                        onClick={() => {
                                            // eslint-disable-next-line no-unused-expressions
                                            this.onHandleIndex;
                                            this.onBlurHandler();
                                        }}
                                        onKeyDown={keyPressHandler}>
                                        {React.cloneElement(child, {
                                            ...child.props,
                                            ix,
                                            keyPressHandler,
                                            onFocus: () => {
                                                this.onChildFocus(ix);
                                            },
                                        })}
                                    </li>
                                ))}
                            </ul>
                        )
                    }
                </Spring>
            </div>
        );
    }
}

export default Dropdown;
