import React, { Component } from "react";
import classNames from "classnames";
import moment from "moment";
import { bool, func, instanceOf, objectOf, shape, string } from "prop-types";

import FieldError from "pages/_components/fields/FieldError";
import FieldLabel from "pages/_components/fields/FieldLabel";
import DatePicker from "pages/_components/fields/datepicker";

import * as i18n from "util/i18n";

export class DateField extends Component {
    static propTypes = {
        field: shape({ name: string }).isRequired,
        form: shape({ errors: objectOf(string), touched: objectOf(bool) }).isRequired,
        handleChange: func,
        hideLabel: bool,
        hidePlaceholder: bool,
        endDate: instanceOf(moment),
        maxDate: instanceOf(moment),
        minDate: instanceOf(moment),
        dateFormat: string,
        idForm: string,
        showDisabledMonthNavigation: bool,
        startDate: instanceOf(moment),
        showYearDropdown: bool,
        showMonthDropdown: bool,
        idField: string.isRequired,
        resetFilters: bool,
        handleResetDateFilters: func,
    };

    static defaultProps = {
        handleChange: () => {},
        hideLabel: false,
        hidePlaceholder: false,
        maxDate: moment(),
        minDate: moment().add(-6, "months"),
        dateFormat: null,
        endDate: null,
        idForm: "",
        showDisabledMonthNavigation: true,
        startDate: null,
        showYearDropdown: false,
        showMonthDropdown: false,
        resetFilters: false,
        handleResetDateFilters: null,
    };

    state = {
        isFocused: false,
        selectedDate: null,
    };

    validateDate = (selectedDate) => {
        const { maxDate, minDate } = this.props;
        if (selectedDate && minDate && selectedDate.diff(moment(minDate)) < 0) {
            return false;
        }

        if (selectedDate && maxDate && selectedDate.diff(moment(maxDate) >= 0)) {
            return false;
        }

        return true;
    };

    handleChange = (selectedDate) => {
        const { field, form, handleChange } = this.props;

        if (!this.validateDate(selectedDate)) {
            return;
        }

        if (handleChange) {
            handleChange(selectedDate);
        }

        this.setState({ selectedDate });

        form.setFieldValue(field.name, selectedDate ? selectedDate.toDate() : null);
    };

    render() {
        const { isFocused, selectedDate } = this.state;
        const {
            endDate,
            field,
            form: { touched, errors, setFieldValue },
            hideLabel,
            hidePlaceholder,
            idForm,
            maxDate,
            minDate,
            showDisabledMonthNavigation,
            startDate,
            dateFormat,
            idField,
            resetFilters,
            handleResetDateFilters,
            ...rest
        } = this.props;

        if (resetFilters) {
            handleResetDateFilters(false);
            this.setState({ selectedDate: null });
            setFieldValue(field.name, null);
        }
        const hasError = touched && touched[field.name] && errors && errors[field.name];
        return (
            <div
                className={classNames("form-group", "form-group--datepicker", {
                    "has-error": hasError,
                    "has-focus": isFocused,
                })}>
                {!hideLabel && <FieldLabel labelKey={`${idForm}.${field.name}.label`} idField={field.name} />}
                <div className="input-group">
                    <div className="form-control">
                        <DatePicker
                            dateFormat={dateFormat === null ? i18n.get("datepicker.format") : dateFormat}
                            className="form-control"
                            endDate={endDate ? moment(endDate) : undefined}
                            maxDate={maxDate ? moment(maxDate) : undefined}
                            minDate={minDate ? moment(minDate) : undefined}
                            placeholderText={hidePlaceholder ? "" : i18n.get(`${idForm}.${field.name}.placeholder`)}
                            selected={selectedDate || (field.value && moment(field.value))}
                            showDisabledMonthNavigation={showDisabledMonthNavigation}
                            startDate={startDate ? moment(startDate) : undefined}
                            onChange={this.handleChange}
                            id={field.name}
                            {...rest}
                        />
                    </div>
                </div>
                {hasError && <FieldError error={errors[field.name]} />}
            </div>
        );
    }
}

export default DateField;
