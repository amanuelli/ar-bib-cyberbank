import React from "react";
import { string, bool, shape, number } from "prop-types";
import ProgressBar from "react-bootstrap/lib/ProgressBar";
import { ReactComponent as Hide } from "styles/images/hide.svg";
import { ReactComponent as Show } from "styles/images/show.svg";

import { calculateScoreStrengthPassword } from "util/settings";

import TextField from "pages/_components/fields/TextField";
import Button from "pages/_components/Button";

import I18n from "pages/_components/I18n";

class Password extends React.Component {
    inputRef = React.createRef();

    static propTypes = {
        field: shape({}).isRequired,
        form: shape({}).isRequired,
        autoComplete: string,
        maxLength: number,
        autoFocus: bool,
        showStrength: bool,
        copyEnabled: bool,
        inputRef: shape().isRequired,
    };

    static defaultProps = {
        autoComplete: "on",
        maxLength: 20,
        autoFocus: false,
        showStrength: false,
        copyEnabled: "true",
    };

    state = {
        isVisible: false,
    };

    componentDidUpdate(prevProps, prevState) {
        const { isVisible } = this.state;
        if (isVisible !== prevState.isVisible) {
            this.inputRef.current.focus();
        }
    }

    handleOnChange = (event) => {
        this.setState({
            scoreStrengthPass: calculateScoreStrengthPassword(event.target.value),
        });
    };

    toggleIsVisible = () => {
        this.setState((prevState) => ({ isVisible: !prevState.isVisible }));
    };

    revealFunction = () => {
        const { isVisible } = this.state;
        return (
            <>
                <Button className="btn-only-icon" bsStyle="link" block={false} onClick={this.toggleIsVisible}>
                    <I18n
                        id={
                            isVisible
                                ? "login.step1.secondFactor.maskPassword.a11y"
                                : "login.step1.secondFactor.unmaskPassword.a11y"
                        }
                        componentProps={{ className: "visually-hidden" }}
                    />
                    {isVisible ? <Hide className="svg-image" /> : <Show className="svg-image" />}
                </Button>
            </>
        );
    };

    render() {
        const { isVisible, scoreStrengthPass } = this.state;
        const { showStrength, copyEnabled } = this.props;
        return (
            <div className="form-group">
                <TextField
                    {...this.props}
                    inputRef={this.inputRef}
                    type={isVisible ? "text" : "password"}
                    inputFunctions={this.revealFunction()}
                    handleOnChange={this.handleOnChange}
                    autoCapitalize="none"
                    copyEnabled={copyEnabled}
                />
                {showStrength && <ProgressBar now={scoreStrengthPass} />}
            </div>
        );
    }
}

export default Password;
