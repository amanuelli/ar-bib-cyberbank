import React from "react";
import { string, object, bool, func, number } from "prop-types";

import InputField from "pages/_components/fields/InputField";

const Otp = ({ keyPrefix, inputRef, ...rest }) => (
    <InputField ref={inputRef} name={rest.id} keyprefix={keyPrefix || `form.credential.${rest.id}`} {...rest} />
);

Otp.propTypes = {
    id: string.isRequired,
    value: string,
    autoComplete: string,
    type: string,
    maxLength: number,
    minLength: number,
    keyPrefix: string,
    onChange: func,
    inputRef: func,
    errors: object,
    autoFocus: bool,
};

Otp.defaultProps = {
    value: undefined,
    autoComplete: "off",
    type: "password",
    maxLength: 6,
    minLength: 1,
    keyPrefix: "",
    onChange: null,
    inputRef: null,
    errors: null,
    autoFocus: false,
};

export default Otp;
