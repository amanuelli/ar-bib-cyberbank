import React, { Component } from "react";
import { string } from "prop-types";

class FieldOptional extends Component {
    static propTypes = {
        text: string.isRequired,
    };

    render() {
        const { text } = this.props;

        return (
            // <div className="form-group-text">
            // <small>{text}</small>
            // </div>
            <small className="text-optional">({text})</small>
        );
    }
}

export default FieldOptional;
