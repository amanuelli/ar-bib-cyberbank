import React, { useState, useEffect } from "react";
import * as i18nUtils from "util/i18n";
import { connect } from "react-redux";
import { selectors as configSelectors } from "reducers/config";
import { selectors as i18nSelectors } from "reducers/i18n";

import FieldLabel from "pages/_components/fields/FieldLabel";
import ReactPhoneInput from "react-phone-input-2";
import classNames from "classnames";
import { shape, string, bool } from "prop-types";

import es from "react-phone-input-2/lang/es.json";
import pt from "react-phone-input-2/lang/pt.json";
import FieldError from "./FieldError";

import "react-phone-input-2/lib/style.css";
import "react-phone-input-2/lib/bootstrap.css";

const PhoneInput = (props) => {
    const { mobilePhone, country, idForm, form, field, hideLabel, activeLanguage, configs } = props;
    const [value, setValue] = useState("1");
    const [valueData, setValueData] = useState({ dialCode: "1" });
    let { idField } = props;
    if (!idField) {
        idField = `${idForm}.${field.name}`;
    }

    useEffect(() => {
        if (mobilePhone && mobilePhone.length > 0) {
            setValue(mobilePhone);
        }
    }, [mobilePhone]);

    const masks = () => {
        const dbMasks = Object.keys(configs).filter((config) => config.includes("phone.mask"));
        const customMasks = {};
        dbMasks.map((mask) => {
            const countryMask = mask.split(".")[2].toLowerCase();
            if (countryMask) {
                customMasks[countryMask] = configs[mask];
            }
            return customMasks;
        });
        return customMasks;
    };

    const location = () => {
        if (activeLanguage === "es") {
            return es;
        }
        if (activeLanguage === "pt") {
            return pt;
        }
        return undefined;
    };

    const handleOnChange = (valueChanged, data, e, formattedValue) => {
        const { dialCode: previousDialCode } = valueData;
        const { dialCode } = data;
        // const { format } = data;
        let phoneValue = valueChanged;
        if (previousDialCode !== dialCode) {
            phoneValue = dialCode;
        } else if (valueChanged.length === 0) {
            phoneValue = dialCode;
        }
        setValue(phoneValue);
        setValueData(data);
        if (form) {
            let areaCode = "";
            if (previousDialCode === dialCode && formattedValue.includes("(")) {
                areaCode = formattedValue.split("(")[1].split(")")[0];
                if (dialCode.includes(areaCode)) {
                    areaCode = "";
                }
            }
            phoneValue = phoneValue.substring(dialCode.length + areaCode.length);
            form.setFieldValue(field.name, { prefix: dialCode, areaCode, value: phoneValue });
        }
    };

    const handleOnKeyDown = (e) => {
        const { dialCode } = valueData;
        const { value: targetValue } = e.currentTarget;
        const extraCharacter = targetValue.includes("(") ? 3 : 1;
        if (
            dialCode.length + extraCharacter > e.currentTarget.selectionStart &&
            targetValue.length === e.currentTarget.selectionEnd
        ) {
            e.currentTarget.selectionStart = dialCode.length + extraCharacter;
        } else if (
            dialCode.length + extraCharacter > e.currentTarget.selectionStart &&
            e.currentTarget.selectionStart === e.currentTarget.selectionEnd
        ) {
            e.currentTarget.selectionStart = targetValue.length;
        }
        if (e.keyCode === 8 && dialCode && dialCode.length > 0 && dialCode.length === value.length) {
            e.preventDefault();
        }
    };

    const handleIsValid = (validValue, validCountry) => {
        const { countryCode, dialCode, format, name } = validCountry;
        const { dialCode: previousDialCode } = valueData;
        if (previousDialCode !== dialCode) {
            setValue(validValue);
            setValueData({
                countryCode,
                dialCode,
                format,
                name,
            });
        }
        return true;
    };

    const handleErrors = (errors) =>
        Object.keys(errors).map((error) => (
            <div className="form-group has-error">
                <FieldError error={errors[error]} />
            </div>
        ));

    const { errors } = form;
    const hasError = errors && errors[field.name];
    if (!idField) {
        idField = `${idForm}.${field.name}`;
    }

    return (
        <div
            className={classNames("form-group", {
                "has-error": hasError,
            })}>
            <FieldLabel idField={`${idField}`} labelKey={`${idForm}.${field.name}.label`} hideLabel={hideLabel} />
            <ReactPhoneInput
                inputProps={{ id: idField }}
                inputStyle={{ width: "100%" }}
                searchStyle={{ width: "92%" }}
                onChange={handleOnChange}
                onKeyDown={handleOnKeyDown}
                isValid={handleIsValid}
                searchPlaceholder={i18nUtils.get("settings.changePhone.mobilePhone.search.placeholder")}
                searchNotFound={i18nUtils.get("settings.changePhone.mobilePhone.search.countryNotFound")}
                country={country}
                localization={location()}
                value={value}
                masks={masks()}
                preferredCountries={["us"]}
                enableAreaCodes
                enableAreaCodeStretch
                enableSearch
                containerClass="container-phone-input"
                buttonClass="button-phone-input"
                dropdownClass="dropdown-phone-input"
                searchClass="search-phone-input"
                {...props}
            />
            {hasError && handleErrors(errors[field.name])}
        </div>
    );
};

PhoneInput.propTypes = {
    idForm: string.isRequired,
    form: shape({}).isRequired,
    idField: string.isRequired,
    field: shape({}).isRequired,
    mobilePhone: string.isRequired,
    activeLanguage: string.isRequired,
    configs: shape({}).isRequired,
    country: string,
    hideLabel: bool,
};

PhoneInput.defaultProps = {
    country: "us",
    hideLabel: false,
};

const mapStateToProps = (state) => ({
    activeLanguage: i18nSelectors.getLang(state),
    configs: configSelectors.getConfig(state),
});

export default connect(mapStateToProps)(PhoneInput);
