import React, { Component } from "react";
import { connect } from "react-redux";
import { compose } from "redux";

import { selectors as i18nSelectors } from "reducers/i18n";
import { numberFormat, toNumber } from "util/number";

import ConnectedAmountField from "pages/_components/fields/AmountField";

class FormikAmountField extends Component {
    handleBlur = () => {
        const { field, form } = this.props;
        form.setFieldTouched(field.name);
    };

    handleCurrencyChange = ({ value }) => {
        const { field, form, onCurrencyChange } = this.props;
        if (onCurrencyChange) {
            onCurrencyChange(value);
        } else {
            form.setFieldValue(`${field.name}.currency`, value);
        }
    };

    handleInputChange = ({ target }) => {
        const { field, form, lang, onInputChange } = this.props;
        const { decimalSeparator } = numberFormat(lang);
        const number = toNumber(target.value, decimalSeparator);
        if (onInputChange) {
            onInputChange(number);
        } else {
            form.setFieldValue(`${field.name}.amount`, number);
        }
    };

    render() {
        const { field, form } = this.props;
        return (
            <ConnectedAmountField
                {...field}
                hasError={form.touched[field.name] && form.errors[field.name]}
                onCurrencyChange={this.handleCurrencyChange}
                onInputChange={this.handleInputChange}
                onBlur={this.handleBlur}
                error={form.errors[field.name] && form.errors[field.name].amount}
                {...this.props}
                {...field.value}
            />
        );
    }
}

const mapStateToProps = (state) => ({
    lang: i18nSelectors.getLang(state),
});

export default compose(connect(mapStateToProps))(FormikAmountField);
