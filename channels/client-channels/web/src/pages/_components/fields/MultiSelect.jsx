import React, { Component, Fragment } from "react";
import { string, shape, arrayOf, func, oneOf, objectOf, bool } from "prop-types";

import * as i18nUtils from "util/i18n";

import Select from "pages/_components/fields/Select";
import List from "pages/_components/List";
import Image from "pages/_components/Image";

class MultiSelect extends Component {
    static propTypes = {
        name: string.isRequired,
        options: shape({
            ids: arrayOf(string),
            byId: objectOf(
                shape({
                    label: string,
                    value: string,
                }),
            ),
        }).isRequired,
        values: arrayOf(string),
        noResultsText: string,
        placeholder: string,
        children: func.isRequired,
        onSelect: func.isRequired,
        onDelete: func.isRequired,
        mode: oneOf(["view", "edit"]),
        labelKey: string,
        valueKey: string,
        searchable: bool,
        deletable: bool,
        renderCloseButton: func,
    };

    static defaultProps = {
        noResultsText: "global.no.results",
        placeholder: "",
        mode: "edit",
        values: [],
        labelKey: "label",
        valueKey: "value",
        searchable: true,
        deletable: true,
        renderCloseButton: null,
    };

    renderCloseButton = (option) => {
        const { renderCloseButton, onDelete } = this.props;
        if (renderCloseButton) {
            return renderCloseButton(option);
        }
        return (
            <button type="button" className="close close--absolute" aria-label="Close" onClick={() => onDelete(option)}>
                <Image className="icon-trash" src="delete-message.svg" />
            </button>
        );
    };

    render() {
        const {
            options,
            values,
            noResultsText,
            placeholder,
            children,
            onSelect,
            onDelete,
            mode,
            label,
            deletable,
            ...props
        } = this.props;
        const i18nLabel = label && i18nUtils.get(label);

        if (mode === "view") {
            return (
                <div className="form-group">
                    <div className="form-group-text">
                        <span className="control-label">{label}</span>
                    </div>
                    <ul className="form-group-control-list list">
                        {values.map((id) => {
                            const { valueKey, labelKey } = this.props;
                            const option = options.byId[id];

                            return (
                                <li key={option[valueKey]} className="list-item">
                                    {option[labelKey]}
                                </li>
                            );
                        })}
                    </ul>
                </div>
            );
        }

        const valuesToRender = values.filter((value) => options.ids.includes(value));
        return (
            <Fragment>
                <Select
                    onChange={onSelect}
                    options={options.ids
                        .filter((id) => !values.some((selectedOption) => id === selectedOption))
                        .map((id) => options.byId[id])}
                    noResultsText={i18nUtils.get(noResultsText)}
                    placeholder={i18nUtils.get(placeholder)}
                    label={i18nLabel}
                    {...props}
                />
                {values.length > 0 && (
                    <List>
                        {valuesToRender.map((value, index) => (
                            <List.Item key={value} num={index} className="list-item--deleteable">
                                {() => {
                                    const option = options.byId[value];
                                    return (
                                        <div className="list-item-inner">
                                            {children(option)}
                                            {deletable && this.renderCloseButton(option)}
                                        </div>
                                    );
                                }}
                            </List.Item>
                        ))}
                    </List>
                )}
            </Fragment>
        );
    }
}

export default MultiSelect;
