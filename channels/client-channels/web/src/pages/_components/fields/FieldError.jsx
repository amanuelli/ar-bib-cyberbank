import React from "react";
import { string } from "prop-types";
import Image from "pages/_components/Image";

const FieldError = (props) => {
    const { error } = props;

    return (
        error && (
            <div className="form-group-error">
                <Image src="images/warningIcon.svg" className="svg-icon" /> <span>{error}</span>
            </div>
        )
    );
};

FieldError.propTypes = {
    error: string.isRequired,
};

export default FieldError;
