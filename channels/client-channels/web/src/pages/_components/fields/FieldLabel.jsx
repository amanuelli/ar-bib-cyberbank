import React, { Component } from "react";
import { string, bool } from "prop-types";

import classNames from "classnames";
import I18n from "pages/_components/I18n";

class FieldLabel extends Component {
    static propTypes = {
        idField: string.isRequired,
        labelKey: string,
        labelText: string,
        mode: string,
        optional: string,
        hideLabel: bool,
        isLegend: bool,
    };

    static defaultProps = {
        mode: "edit",
        hideLabel: false,
        labelKey: null,
        labelText: null,
        optional: null,
        isLegend: false,
    };

    getFocus = (id) => {
        const element = document.getElementById(id) || document.getElementById(`react-select-${id}--value`);
        if (
            element &&
            element.nodeName !== "INPUT" &&
            element.nodeName !== "TEXTAREA" &&
            element.nodeName !== "SELECT"
        ) {
            element.querySelector(".Select-input").focus();
        }
    };

    render() {
        const { idField, labelKey, labelText, mode, optional, hideLabel, isLegend } = this.props;
        const LabelTag = isLegend ? "legend" : "label";

        if (mode === "edit") {
            return (
                <div
                    className={classNames("form-group-text", { "visually-hidden": hideLabel })}
                    role="presentation"
                    onClick={() => this.getFocus(idField)}>
                    <LabelTag
                        id={`${LabelTag}.${idField}`}
                        className={classNames("control-label", { "visually-hidden": isLegend })}
                        htmlFor={idField}>
                        {labelKey ? <I18n id={labelKey} /> : labelText}
                        {optional && <small className="text-optional">{optional}</small>}
                    </LabelTag>
                    {isLegend && (
                        <label id={`labelLegend.${idField}`} className="control-label" htmlFor={idField}>
                            {labelKey ? <I18n id={labelKey} /> : labelText}
                            {optional && <small className="text-optional">{optional}</small>}
                        </label>
                    )}
                </div>
            );
        }
        return <span className="data-label">{labelKey ? <I18n id={labelKey} /> : labelText}</span>;
    }
}

export default FieldLabel;
