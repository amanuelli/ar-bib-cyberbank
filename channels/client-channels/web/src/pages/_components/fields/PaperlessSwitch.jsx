import React, { Component } from "react";
import classNames from "classnames";
import { string, func, bool } from "prop-types";
import { actions } from "reducers/settings";

import I18n from "pages/_components/I18n";
import FieldError from "pages/_components/fields/FieldError";
import * as i18nUtils from "util/i18n";

class PaperlessSwitch extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        name: string,
        onChange: func,
        onBlur: func,
        error: string,
        title: string,
        label: string,
        hideLabel: bool,
        idForm: string,
        readOnly: bool,
        formGroup: string,
        biggerLabel: bool,
        idProduct: string.isRequired,
        paperless: bool,
        fromSettings: bool,
    };

    static defaultProps = {
        name: null,
        error: null,
        title: null,
        label: null,
        hideLabel: false,
        idForm: null,
        readOnly: false,
        onChange: () => {},
        onBlur: null,
        formGroup: null,
        biggerLabel: false,
        paperless: false,
        fromSettings: false,
    };

    state = {
        value: this.props.paperless,
    };

    handleChange = () => {
        const { dispatch, idProduct } = this.props;
        const { value } = this.state;
        dispatch(actions.modifyPaperlessConfigurations(idProduct, !value));

        this.setState({ value: !value });
    };

    render() {
        const { idForm, error, hideLabel, label, formGroup, biggerLabel, title, fromSettings, ...props } = this.props;
        const { value } = this.state;

        return (
            <div
                className={classNames("c-control c-control-block c-control--switch", {
                    "form-group": formGroup,
                    "has-error": error,
                    "paperless-item": fromSettings,
                })}>
                {!title ? (
                    <I18n componentProps={{ className: "data-label" }} id="configuration.preferences.paperless.title" />
                ) : (
                    <label className="paperless-label" htmlFor={props.name}>
                        {title}
                    </label>
                )}

                <input
                    id={props.name}
                    className="c-control-input"
                    type="checkbox"
                    name={props.name}
                    onChange={this.handleChange}
                    value={!value}
                    checked={value}
                />

                <label className="c-control-label" htmlFor={props.name}>
                    {!hideLabel && !biggerLabel && (
                        <div className="form-group-text">
                            {!label ? i18nUtils.get("configuration.preferences.paperless.description") : { label }}
                        </div>
                    )}
                    {!hideLabel && biggerLabel && (
                        <h4 className="table-legend">
                            {!label ? i18nUtils.get("configuration.preferences.paperless.description") : { label }}
                        </h4>
                    )}
                </label>
                {error && <FieldError error={error} />}
            </div>
        );
    }
}

export default PaperlessSwitch;
