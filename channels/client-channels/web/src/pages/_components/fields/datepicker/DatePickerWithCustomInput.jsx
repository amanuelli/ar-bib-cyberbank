import React from "react";
import { bool } from "prop-types";

import { MainContainerContext } from "pages/_components/MainContainer";
import { resizableRoute } from "pages/_components/Resizable";
import CustomDatePicker from "pages/_components/fields/datepicker/CustomDatePicker";
import MobileCustomDatePickerInput from "pages/_components/fields/datepicker/MobileCustomDatePickerInput";

class DatePickerWithCustomInput extends React.Component {
    startRef = React.createRef();

    static propTypes = {
        isMobile: bool.isRequired,
    };

    onKeyDown = (e) => {
        if (e.keyCode === 9 || e.which === 9) {
            this.startRef.current.setOpen(false);
        }
    };

    render() {
        const { isMobile } = this.props;

        if (isMobile) {
            return (
                <MainContainerContext.Consumer>
                    {(ref) => (
                        <CustomDatePicker
                            {...this.props}
                            customInput={<MobileCustomDatePickerInput />}
                            onKeyDown={(e) => e.preventDefault()}
                            isMobile={isMobile}
                            viewContentRef={ref}
                        />
                    )}
                </MainContainerContext.Consumer>
            );
        }

        return <CustomDatePicker {...this.props} isMobile={isMobile} ref={this.startRef} onKeyDown={this.onKeyDown} />;
    }
}

export default resizableRoute(DatePickerWithCustomInput);
