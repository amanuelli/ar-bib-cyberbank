import React from "react";
import { connect } from "react-redux";
import classNames from "classnames";
import { string, func, bool, shape } from "prop-types";

import { get } from "util/i18n";

import FieldError from "pages/_components/fields/FieldError";
import FieldLabel from "pages/_components/fields/FieldLabel";

import Image from "pages/_components/Image";
import { getSafeRandomNumber } from "util/number";

class InputField extends React.Component {
    static propTypes = {
        id: string,
        keyprefix: string,
        hidelabel: bool,
        errors: shape({}),
        onChange: func,
        onFocus: func,
        kind: string,
        value: string,
        type: string.isRequired,
    };

    static defaultProps = {
        id: getSafeRandomNumber(),
        keyprefix: "",
        hidelabel: false,
        errors: {},
        onChange: null,
        onFocus: null,
        kind: null,
        value: "",
    };

    state = {
        value: this.props.value,
        showValue: false,
        isFocused: false,
    };

    componentWillReceiveProps(nextProps) {
        if (nextProps.value) {
            this.setState({
                value: nextProps.value,
            });
        }
    }

    componentDidUpdate(prevProps, prevState) {
        // Check previous value in order to avoid loop with onBlur method
        if (this.props.type === "password" && this.state.showValue !== prevState.showValue) {
            this.nameInput.focus();
        }
    }

    getValue = () => this.state.value;

    /* onResize = () => {
        if (this.state.isFocused && !this.props.isDesktop && this.props.mobileOS === "Android") {
            ReactDOM.findDOMNode(this.nameInput).scrollIntoView({ block: "center", behavior: "smooth" });
        }
    };

    componentDidMount() {
        window.addEventListener("resize", this.onResize);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.onResize);
    } */

    handleInputChange = (evt) => {
        if (this.props.kind) {
            if (this.props.kind === "numeric") {
                const re = /^[0-9]+$/;

                if (evt.target.value === "" || re.test(evt.target.value)) {
                    this.setState({ value: evt.target.value });

                    const { onChange } = this.props;
                    if (typeof onChange === "function") {
                        onChange(evt);
                    }
                }
            }
        } else {
            this.setState({ value: evt.target.value });

            const { onChange } = this.props;
            if (typeof onChange === "function") {
                onChange(evt);
            }
        }
    };

    onFocus = () => {
        this.setState({ isFocused: true });

        if (this.props.onFocus) {
            this.props.onFocus();
        }
    };

    getShowValue = () => (
        <div className="c-control c-control--icon-toggle">
            <input
                onClick={() => this.setState((prevState) => ({ showValue: !prevState.showValue, isFocused: 1 }))}
                id={`${this.props.id}ShowHide`}
                type="checkbox"
                className="c-control-input"
            />
            <label htmlFor={`${this.props.id}ShowHide`} className="c-control-label">
                <div className="c-control-mark--checked">
                    <Image src="images/show.svg" />
                </div>
                <div className="c-control-mark--unchecked">
                    <Image src="images/hide.svg" />
                </div>
            </label>
        </div>
    );

    render() {
        // Es preferible utilizar TextField en lugar de InputField.

        const { id, keyprefix, hidelabel, onChange, value, kind, type, ...rest } = this.props;

        const placeholder = get(`${keyprefix}.placeholder`);
        const { errors } = this.props;
        let error = (errors || {})[id];
        if (!error) {
            error = (errors || {})[`_${id}`];
        }

        const divClass = classNames({
            "form-group": true,
            "has-error": error,
            "has-focus": this.state.isFocused,
        });

        let currentType = type;
        if (type === "password" && this.state.showValue) {
            currentType = "text";
        }

        let other = { ...rest };

        if (kind === "numeric") {
            other = {
                ...other,
                pattern: "[0-9]*",
            };
        }

        return (
            <div className={divClass}>
                {!hidelabel && <FieldLabel id={id} labelKey={`${keyprefix}.label`} />}
                <div className="input-group">
                    <input
                        onChange={this.handleInputChange}
                        onClick={this.handleInputClick}
                        className="form-control"
                        placeholder={placeholder}
                        id={id}
                        onBlur={() => this.setState({ isFocused: false })}
                        ref={(input) => {
                            this.nameInput = input;
                        }}
                        type={currentType}
                        value={this.state.value}
                        {...other}
                        onFocus={this.onFocus}
                    />
                    {type === "password" && this.getShowValue()}
                </div>
                <FieldError error={error} />
            </div>
        );
    }
}

export default connect(null, null, null, { withRef: true })(InputField);
