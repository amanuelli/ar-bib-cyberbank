import React, { Component } from "react";
import Navbar from "react-bootstrap/lib/Navbar";

class Header extends Component {
    render() {
        return (
            <header className="view-header">
                <Navbar collapseOnSelect fluid>
                    <Navbar.Header>
                        <div className="toolbar">{this.props.children}</div>
                    </Navbar.Header>
                </Navbar>
            </header>
        );
    }
}

export default Header;
