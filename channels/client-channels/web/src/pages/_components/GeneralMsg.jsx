import React, { Component, Fragment } from "react";
import { string, node, oneOfType } from "prop-types";

import Image from "pages/_components/Image";
import Container from "pages/_components/Container";

class GeneralMsg extends Component {
    static propTypes = {
        title: oneOfType([string, node]),
        description: oneOfType([string, node]),
        imagePath: string,
        pageTitle: node,
        callToAction: node,
    };

    static defaultProps = {
        title: null,
        description: null,
        imagePath: null,
        pageTitle: null,
        callToAction: null,
    };

    render() {
        const { title, description, imagePath, callToAction, pageTitle } = this.props;

        return (
            <Fragment>
                {pageTitle}
                <Container
                    aria-live="polite"
                    className="icon-message-page container--layout align-items-center flex-grow">
                    <Container.Column sm={12} md={9} lg={6} xl={6} className="col col-12">
                        {imagePath && <Image src={imagePath} className="svg-big-icon" />}
                        {title && <p className="icon-message-page--title">{title}</p>}
                        {description && <p className="text-lead">{description}</p>}
                        {callToAction}
                    </Container.Column>
                </Container>
            </Fragment>
        );
    }
}

export default GeneralMsg;
