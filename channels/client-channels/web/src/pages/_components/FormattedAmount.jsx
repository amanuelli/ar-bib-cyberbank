import React, { Component, Fragment } from "react";
import { string, number, bool } from "prop-types";
import NumberFormat from "react-number-format";
import { connect } from "react-redux";

import { selectors as configSelectors } from "reducers/config";
import { selectors as i18nSelectors } from "reducers/i18n";
import * as i18n from "util/i18n";
import * as configUtils from "util/config";
import { countDecimalPlaces, numberFormat } from "util/number";

class FormattedAmount extends Component {
    static propTypes = {
        className: string,
        frequencyInSpan: bool,
        currency: string,
        frequency: string,
        lang: string.isRequired,
        maximumDecimals: number.isRequired,
        minimumDecimals: number.isRequired,
        quantity: number,
        showAbbreviature: bool,
    };

    static defaultProps = {
        className: "data-amount",
        frequencyInSpan: true,
        frequency: "",
        showAbbreviature: false,
        quantity: 0,
        currency: configUtils.get("core.masterCurrency"),
    };

    getAmountDetails = (quantity) => {
        const { showAbbreviature } = this.props;
        const million = 1000000;

        if (showAbbreviature) {
            if (quantity >= million) {
                return {
                    quantity: quantity / million,
                    abbreviature: i18n.get("formattedAmount.millionAbbreviature"),
                };
            }
        }

        return {
            quantity,
            abbreviature: "",
        };
    };

    renderText = (formattedValue) => {
        const { className, frequencyInSpan, currency, frequency } = this.props;

        return (
            <Fragment>
                <span className={className}>
                    <span className="data-amount-currency">{i18n.get(`currency.label.${currency}`)}</span>{" "}
                    {formattedValue} {frequencyInSpan && <span>{frequency}</span>}
                </span>
                {!frequencyInSpan && <span className="data-desc">{frequency}</span>}
            </Fragment>
        );
    };

    render() {
        const { currency, quantity, maximumDecimals, minimumDecimals, lang } = this.props;
        const { decimalSeparator, thousandSeparator } = numberFormat(lang);

        if (!currency) {
            return null;
        }

        const amount = this.getAmountDetails(quantity);
        let decimalPlaces;
        let decimalScale;

        if (amount.abbreviature.length > 0) {
            decimalScale = 1;
        } else {
            decimalPlaces = countDecimalPlaces(quantity, decimalSeparator);
            decimalScale = Math.max(Math.min(decimalPlaces, maximumDecimals), minimumDecimals);
        }

        return (
            <NumberFormat
                decimalSeparator={decimalSeparator}
                thousandSeparator={thousandSeparator}
                decimalScale={decimalScale}
                value={amount.quantity}
                displayType="text"
                renderText={(formattedValue) => this.renderText(formattedValue)}
                suffix={amount.abbreviature}
                fixedDecimalScale
            />
        );
    }
}

const mapStateToProps = (state) => ({
    maximumDecimals: Number(configSelectors.getConfig(state)["defaultDecimal.maximum"] || 0),
    minimumDecimals: Number(configSelectors.getConfig(state)["defaultDecimal.minimum"] || 0),
    lang: i18nSelectors.getLang(state),
});

export default connect(mapStateToProps)(FormattedAmount);
