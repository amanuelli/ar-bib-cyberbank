import React from "react";
import Nav from "react-bootstrap/lib/Nav";
import { connect } from "react-redux";
import { func, string, object, shape, bool, number, node } from "prop-types";

import { actions as sessionActions, selectors as sessionSelectors } from "reducers/session";
import { selectors as i18nSelectors } from "reducers/i18n";
import { selectors as transactionsSelectors } from "reducers/transactions";
import { CORPORATE_GROUP_ENVIRONMENT_TYPE } from "constants.js";
import * as i18n from "util/i18n";

import MenuFormItem from "pages/_components/menu/MenuFormItem";
import MenuItem from "pages/_components/menu/MenuItem";
import MenuSection from "pages/_components/menu/MenuSection";
import FeatureFlag from "../FeatureFlag";
import ToggleSidebar from "./ToggleSidebar";

class Menu extends React.Component {
    static propTypes = {
        dispatch: func.isRequired,
        isMobile: bool,
        isAdministrator: bool,
        activeEnvironment: shape({
            permissions: shape({
                accounts: bool,
                creditcards: bool,
                payCreditCard: bool,
                payLoan: bool,
                payThirdPartiesCreditCard: bool,
                payThirdPartiesLoan: bool,
                requestTransactionCancellation: bool,
                transferForeign: bool,
                transferInternal: bool,
                transferLocal: bool,
                transferThirdParties: bool,
            }),
            forms: object,
            administrationScheme: string,
            type: string,
        }).isRequired,
        lang: string.isRequired,
        isCollapsed: bool,
        onToggleSidebar: func.isRequired,
        pendingTransactionsQuantity: number,
    };

    static defaultProps = {
        isMobile: false,
        isAdministrator: false,
        isCollapsed: false,
        pendingTransactionsQuantity: 0,
    };

    logOut = () => {
        const { dispatch } = this.props;
        dispatch(sessionActions.logout());
    };

    buildAdministrationMenu = () => {
        const { isMobile, isAdministrator, activeEnvironment } = this.props;

        let admMenu = null;
        if (!isMobile && isAdministrator) {
            if (activeEnvironment.administrationScheme !== "simple") {
                admMenu = (
                    <MenuSection
                        titleKey="administration.menu.name"
                        key="menu.administration"
                        image="images/administration.svg">
                        <MenuItem
                            titleKey="administration.menu.users"
                            key="menu.administration.users"
                            linkTo="/administration/users"
                        />
                        {activeEnvironment.administrationScheme !== "medium" && (
                            <MenuItem
                                titleKey="administration.menu.groups"
                                key="menu.administration.groups"
                                linkTo="/administration/groups"
                            />
                        )}
                        <MenuItem
                            titleKey="administration.menu.signatureScheme"
                            key="menu.administration.signatureScheme"
                            linkTo={`/administration/${activeEnvironment.administrationScheme}/signaturesSchemes`}
                        />
                        <MenuItem
                            titleKey="administration.restrictions.environment.title"
                            key="menu.administration"
                            linkTo="/administration/restrictions"
                        />
                    </MenuSection>
                );
            } else {
                admMenu = (
                    <MenuItem
                        titleKey="administration.menu.name"
                        key="menu.administration"
                        linkTo="/administration/users"
                        image="images/administration.svg"
                    />
                );
            }
        }

        return admMenu;
    };

    constructMenuFormItems(sectionForms) {
        const { lang } = this.props;
        const menuItems = [];

        if (sectionForms && sectionForms.length > 0) {
            for (let i = 0; i < sectionForms.length; i++) {
                menuItems.push(
                    <MenuFormItem
                        link={`/form/${sectionForms[i].idForm}`}
                        name={sectionForms[i].name[lang]}
                        key={sectionForms[i].idForm}
                    />,
                );
            }
        }
        return menuItems;
    }

    constructMenuSection(activeEnvironment, menuName, titleKey, ImageSection, linkTo) {
        let menuSection = null;
        let menuItems = [];

        if (menuName !== "deposits" || activeEnvironment.permissions[menuName]) {
            // provisorio por esta iteracion
            menuItems.push(<MenuItem titleKey={titleKey} key={titleKey} linkTo={linkTo} />);
        }

        let sectionForms = activeEnvironment.forms[menuName];
        // Exception in form of credtCard Menu
        if (menuName === "creditcards" && !activeEnvironment.permissions[menuName] && sectionForms) {
            sectionForms = sectionForms.filter(
                ({ idForm }) => !["additionalCreditCardRequest", "lostOrStolenCreditCard"].includes(idForm),
            );
        }

        menuItems = menuItems.concat(this.constructMenuFormItems(sectionForms));

        if (menuName === "accounts" && activeEnvironment.permissions.accountFund) {
            menuItems.push(
                <MenuItem titleKey="menu.accounts.fund" key="menu.accounts.fund" linkTo="/form/accountFund" />,
            );
        }

        if (menuName === "loans" && activeEnvironment.permissions.requestLoan) {
            menuItems.push(
                <MenuItem titleKey="menu.request.loan" key="menu.request.loan" linkTo="/form/requestLoan" />,
            );
        }

        if (menuItems.length > 0) {
            menuSection = (
                <MenuSection
                    id={`menu.${menuName}`}
                    titleKey={`menu.${menuName}`}
                    key={`menu.${menuName}`}
                    image={ImageSection}>
                    {menuItems}
                </MenuSection>
            );
        }
        return menuSection;
    }

    render() {
        const { activeEnvironment, isMobile, onToggleSidebar, isCollapsed, pendingTransactionsQuantity } = this.props;
        let forms = null;

        // Desktop
        const desktopMenuSection = (
            <MenuItem titleKey="menu.desktop" key="menu.desktop" linkTo="/desktop" image="images/escritorio.svg" />
        );

        // Acounts
        const accountMenuSection = this.constructMenuSection(
            activeEnvironment,
            "accounts",
            "menu.accounts.myAccounts",
            "images/cuentas.svg",
            "/accounts",
        );

        // Deposits
        const depositMenuSection = this.constructMenuSection(
            activeEnvironment,
            "deposits",
            "menu.deposits.myDeposits",
            "images/depositoFijo.svg",
            "/deposits/index",
        );

        // Creditcards
        const creditcardMenuSection = this.constructMenuSection(
            activeEnvironment,
            "creditcards",
            "menu.creditcards.myCreditcards",
            "images/tarjetas.svg",
            "/creditCards",
        );

        // Loans
        const loanMenuSection = this.constructMenuSection(
            activeEnvironment,
            "loans",
            "menu.loans.myLoans",
            "images/prestamos.svg",
            "/loans",
        );

        // Transfers
        let transferMenuSection = null;
        let transferMenuItems = [];
        if (activeEnvironment.permissions.accounts) {
            if (activeEnvironment.permissions.transferInternal) {
                // TODO We must check if the feature `transfers.internal.send` is also available?
                transferMenuItems.push(
                    <MenuFormItem
                        link="/form/transferInternal"
                        key="transferInternal"
                        name={i18n.get("menu.transfers.internal")}
                    />,
                );
            }

            if (activeEnvironment.permissions.transferThirdParties) {
                // TODO We must check if the feature `transfers.transferThirdParties.send` is also available?
                transferMenuItems.push(
                    <MenuFormItem
                        link="/form/transferThirdParties"
                        key="transferThirdParties"
                        name={i18n.get("menu.transfers.thirdParties")}
                    />,
                );
            }

            if (activeEnvironment.permissions.transferLocal) {
                // TODO We must check if the feature `transfers.local.send` is also available?
                transferMenuItems.push(
                    <MenuFormItem
                        link="/form/transferLocal"
                        key="transferLocal"
                        name={i18n.get("menu.transfers.local")}
                    />,
                );
            }
            if (activeEnvironment.permissions.transferForeign) {
                // TODO We must check if the feature `transfers.foreign.send` is also available?
                transferMenuItems.push(
                    <MenuFormItem
                        link="/form/transferForeign"
                        key="transferForeign"
                        name={i18n.get("menu.transfers.foreign")}
                    />,
                );
            }
            forms = activeEnvironment.forms.transfers;
            transferMenuItems = transferMenuItems.concat(this.constructMenuFormItems(forms));
            if (transferMenuItems.length > 0) {
                transferMenuSection = (
                    <MenuSection
                        id="menu.transfers"
                        titleKey="menu.transfers"
                        key="menu.transfers"
                        image="images/transferencias.svg">
                        {transferMenuItems}
                    </MenuSection>
                );
            }
        }

        // Payments
        let paymentMenuSection = null;
        let paymentMenuItems = [];

        if (activeEnvironment.permissions.accounts) {
            if (activeEnvironment.permissions.payLoan) {
                // TODO We must check if the feature `pay.loan.send` is also available?
                paymentMenuItems.push(
                    <MenuItem titleKey="menu.payments.loan" key="menu.payments.loan" linkTo="/loansPayment/list" />,
                );
            }
            if (activeEnvironment.permissions.payThirdPartiesLoan) {
                // TODO We must check if the feature `pay.thirdPartiesLoan.send` is also available?
                paymentMenuItems.push(
                    <MenuItem
                        titleKey="menu.payments.thirdPartiesLoan"
                        key="menu.payments.thirdPartiesLoan"
                        linkTo="/form/payThirdPartiesLoan"
                    />,
                );
            }

            if (activeEnvironment.permissions.payCreditCard) {
                // TODO We must check if the feature `pay.creditcard.send` is also available?
                paymentMenuItems.push(
                    <MenuItem
                        titleKey="menu.payments.creditCard"
                        key="menu.payments.creditCard"
                        linkTo="/creditCardsPayment/list"
                    />,
                );
            }
            if (activeEnvironment.permissions.payThirdPartiesCreditCard) {
                // TODO We must check if the feature `pay.thirdPartiesCreditCard.send` is also available?
                paymentMenuItems.push(
                    <MenuItem
                        titleKey="menu.payments.thirdPartiesCreditCard"
                        key="menu.payments.thirdPartiesCreditCard"
                        linkTo="/form/payThirdPartiesCreditCard"
                    />,
                );
            }
            if (!isMobile && activeEnvironment.permissions.salaryPayment) {
                paymentMenuItems.push(
                    <MenuItem
                        titleKey="menu.multiline.salaryPayment"
                        key="menu.multiline.salaryPayment"
                        linkTo="/form/salaryPayment"
                    />,
                );
            }
            forms = activeEnvironment.forms.payments;
            paymentMenuItems = paymentMenuItems.concat(this.constructMenuFormItems(forms));
            if (paymentMenuItems.length > 0) {
                paymentMenuSection = (
                    <MenuSection titleKey="menu.payments" key="menu.payments" image="images/pagos.svg">
                        {paymentMenuItems}
                    </MenuSection>
                );
            }
        }

        // Position
        let positionMenuItem = null;
        if (activeEnvironment.permissions.position) {
            positionMenuItem = (
                <MenuItem
                    titleKey="menu.position"
                    key="menu.position"
                    linkTo="/position"
                    image="images/posicionConsolidada.svg"
                />
            );
        }

        // Transactions
        const transactionsMenuSection = (
            <MenuItem
                titleKey="menu.transactions"
                key="menu.transactions"
                linkTo="/transactions/list"
                image="images/transaction-list.svg"
            />
        );

        // Pending dispatch
        const pendingDispatch = (
            <FeatureFlag id="feature.signatureSchema.dispatchControl">
                <MenuItem
                    titleKey="menu.pendingDispatch"
                    key="menu.pendingDispatch"
                    linkTo="/pendingTransaction/list"
                    image="images/pending-dispatch.svg"
                    notificationCount={pendingTransactionsQuantity}
                    notificationCountTextID="menu.pendingDispatch.badge"
                />
            </FeatureFlag>
        );

        // Settings
        const settingsMenuItem = (
            <MenuItem titleKey="menu.settings" key="menu.settings" linkTo="/settings" image="images/perfil.svg" />
        );

        // Poi
        const PointsOfInterestMenuSection = (
            <MenuItem
                titleKey="menu.pointsofinterest"
                key="menu.pointOfInterest"
                linkTo="/settings/pointsOfInterest"
                image="images/place.svg"
            />
        );

        // Administration
        const administrationMenuSection = this.buildAdministrationMenu();

        return (
            <Nav className="menu-list" role="menu">
                {!isMobile && <ToggleSidebar isSidebarCollapsed={isCollapsed} onToggle={onToggleSidebar} />}

                <FirstLevelItem>{desktopMenuSection}</FirstLevelItem>
                <FirstLevelItem>{accountMenuSection}</FirstLevelItem>
                <FirstLevelItem>{depositMenuSection}</FirstLevelItem>
                <FirstLevelItem>{creditcardMenuSection}</FirstLevelItem>
                <FirstLevelItem>{loanMenuSection}</FirstLevelItem>
                <FirstLevelItem>{transferMenuSection}</FirstLevelItem>
                <FirstLevelItem>{paymentMenuSection}</FirstLevelItem>
                <FirstLevelItem>{positionMenuItem}</FirstLevelItem>
                <FirstLevelItem>{transactionsMenuSection}</FirstLevelItem>
                {activeEnvironment.type !== CORPORATE_GROUP_ENVIRONMENT_TYPE && (
                    <FirstLevelItem>{pendingDispatch}</FirstLevelItem>
                )}
                <FirstLevelItem>{PointsOfInterestMenuSection}</FirstLevelItem>
                <FirstLevelItem>{administrationMenuSection}</FirstLevelItem>
                {isMobile && <FirstLevelItem>{settingsMenuItem}</FirstLevelItem>}
            </Nav>
        );
    }
}
function FirstLevelItem({ children }) {
    return (
        <li role="menuitem" className="menu-list-item">
            {children}
        </li>
    );
}

FirstLevelItem.propTypes = {
    children: node.isRequired,
};

const mapStateToProps = (state) => ({
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    lang: i18nSelectors.getLang(state),
    isAdministrator: sessionSelectors.isAdministrator(state),
    pendingTransactionsQuantity: transactionsSelectors.getPendingTransactionsQuantity(state),
});

export default connect(mapStateToProps)(Menu);
