import React, { Component, Fragment, Children } from "react";
import { string, element, shape } from "prop-types";

import I18n from "pages/_components/I18n";
import Image from "pages/_components/Image";
import Dropdown from "pages/_components/Dropdown";

class MenuSection extends Component {
    static propTypes = {
        titleKey: string,
        image: string,
        imageClassName: string,
        title: element,
        children: shape({}).isRequired,
        permissions: shape({}),
    };

    static defaultProps = {
        titleKey: "",
        image: "",
        imageClassName: "",
        title: "",
        permissions: null,
    };

    getTitle = () => {
        const { image, imageClassName, titleKey, title } = this.props;
        let content = null;
        if (image) {
            content = imageClassName ? (
                <div className={imageClassName}>
                    <Image src={image} />
                </div>
            ) : (
                <Image src={image} />
            );
        }

        return (
            <Fragment>
                {content}
                {titleKey ? <I18n id={titleKey} /> : title}
            </Fragment>
        );
    };

    render() {
        const { children, permissions, image, titleKey } = this.props;

        return (
            <Dropdown image={image} label={titleKey} buttonClass="toggle-menu-button">
                {Children.map(children, (child) => child && React.cloneElement(child, { permissions }))}
            </Dropdown>
        );
    }
}

export default MenuSection;
