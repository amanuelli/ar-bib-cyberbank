import React, { Component } from "react";
import { Link } from "react-router-dom";
import { string } from "prop-types";

class MenuFormItem extends Component {
    static propTypes = {
        link: string.isRequired,
        name: string.isRequired,
    };

    render() {
        const { link, name, ...rest } = this.props;

        return (
            <Link to={link} role="menuitem" {...rest}>
                <span>{name}</span>
            </Link>
        );
    }
}

export default MenuFormItem;
