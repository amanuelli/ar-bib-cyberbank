import React from "react";
import { Link, withRouter } from "react-router-dom";
import { element, string, number, func, bool } from "prop-types";

import Image from "pages/_components/Image";
import Badge from "pages/_components/Badge";
import I18n from "pages/_components/I18n";
import { Mixpanel } from "util/clickstreaming";

class MenuItem extends React.Component {
    static propTypes = {
        title: element, // en caso de que el titulo ya venga "traducido"
        titleKey: string, // en caso de que el titulo sea una key de mensaje
        linkTo: string,
        image: string,
        onSelect: func,
        onClick: func,
        closeOnSelect: bool,
        notificationCount: number,
        notificationCountTextID: string,
    };

    static defaultProps = {
        title: null,
        titleKey: "",
        linkTo: "",
        image: "",
        onSelect: null,
        onClick: null,
        closeOnSelect: false,
        notificationCount: 0,
        notificationCountTextID: "",
    };

    onClickMenu = () => {
        const { linkTo, closeOnSelect, onSelect } = this.props;
        Mixpanel.track_user("menu.action", { linkTo });
        if (closeOnSelect) {
            return onSelect;
        }
        return null;
    };

    getContent() {
        const {
            linkTo,
            image,
            title,
            titleKey,
            onClick: handleOnClick,
            closeOnSelect,
            onSelect,
            notificationCount,
            notificationCountTextID,
            ...rest
        } = this.props;
        let content = null;

        if (linkTo) {
            content = (
                <Link
                    role="menuitem"
                    to={linkTo}
                    className={notificationCount && "with-badge"}
                    onClick={this.onClickMenu}
                    {...rest}>
                    {image && <Image src={image} />}
                    {title || <I18n id={titleKey} />}
                    {notificationCount > 0 && (
                        <>
                            <Badge count={notificationCount} />
                            <I18n id={notificationCountTextID} componentProps={{ className: "visually-hidden" }} />
                        </>
                    )}
                </Link>
            );
        } else {
            content = (
                <span
                    role="button"
                    className={notificationCount && "with-badge"}
                    onClick={() => {
                        if (closeOnSelect) {
                            onSelect();
                        }
                        handleOnClick();
                    }}>
                    {image && <Image src={image} />}
                    {title || <I18n id={titleKey} />}
                </span>
            );
        }

        return content;
    }

    render() {
        return this.getContent();
    }
}

export default withRouter(MenuItem);
