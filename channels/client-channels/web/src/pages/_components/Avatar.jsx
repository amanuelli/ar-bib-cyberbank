import React from "react";
import { shape, string, oneOf } from "prop-types";
import Gravatar from "react-gravatar";
import cx from "classnames";

import Image from "pages/_components/Image";

function Avatar({ user, size }) {
    const userInitials = user.userFullName.match(/\b(\w)/g).join("");

    let content = null;
    if (userInitials) {
        content = <Image className="svg-icon" src={`images/avatarImages/avatar-${user.defaultAvatarId}.svg`} />;
    } else if (user.email) {
        content = (
            <figure>
                <Gravatar
                    alt={user.userFullName}
                    email={user.email}
                    default="404"
                    onError={() => userInitials}
                    className="rounder-content"
                />
            </figure>
        );
    }

    return (
        <div className={cx("avatar", { "avatar-big": size === "big" })}>
            <span className="visually-hidden">{user.userFullName}</span>
            {content}
            <span className="avatar-text" aria-hidden>
                {userInitials}
            </span>
        </div>
    );
}

Avatar.propTypes = {
    size: oneOf(["normal", "big"]),
    user: shape({
        userFullName: string.isRequired,
        defaultAvatarId: string.isRequired,
    }).isRequired,
};

Avatar.defaultProps = {
    size: "normal",
};

export default Avatar;
