import React, { Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import Button from "pages/_components/Button";
import * as i18nUtils from "util/i18n";
import { Spring, config } from "react-spring";
import { bool, func } from "prop-types";
import { push } from "react-router-redux/actions";

import Image from "pages/_components/Image";

class LoginVideo extends Component {
    static propTypes = {
        isMobile: bool,
        showVideoControlsMobile: bool,
        goLoginWhenVideoFinished: bool,
        dispatch: func.isRequired,
    };

    static defaultProps = {
        isMobile: false,
        showVideoControlsMobile: false,
        goLoginWhenVideoFinished: false,
    };

    state = {
        videoRef: null,
        currentIndexVideoText: -1,
        videoTexts: [],
        isPlaying: false,
    };

    componentDidMount() {
        this.currentTimeInterval = null;

        this.setState({
            currentIndexVideoText: 0,
            videoTexts: [
                {
                    text: "onboarding.login.text1",
                    from: 0,
                    to: 1.9,
                },
                {
                    text: "onboarding.login.text2",
                    from: 1.9,
                    to: 3.2,
                },
                {
                    text: "onboarding.login.text3",
                    from: 3.2,
                    to: 5,
                },
                {
                    text: "onboarding.login.text4",
                    from: 4.5,
                    to: null,
                },
            ],
        });
    }

    onPlay = () => {
        const { videoRef } = this.state;
        this.currentTimeInterval = setInterval(() => {
            if (videoRef) {
                this.setDynamicText(videoRef.currentTime);
            }
        }, 100);
        this.setState({ isPlaying: true });
    };

    onPause = () => {
        clearInterval(this.currentTimeInterval);
        this.setState({ isPlaying: false });
    };

    onVideoEnded = () => {
        const { goLoginWhenVideoFinished } = this.props;
        if (goLoginWhenVideoFinished) {
            this.goToLoginStep1();
        }
        this.setState({ isPlaying: false });
    };

    setMetadata = (duration) => {
        const { videoTexts } = this.state;
        videoTexts[videoTexts.length - 1].to = duration;
        this.setState({ videoTexts });
    };

    setDynamicText = (currentTime) => {
        const { videoTexts, currentIndexVideoText } = this.state;
        const nextCurrentIndexVideoText = videoTexts.findIndex(
            (item) => currentTime <= item.to && currentTime >= item.from,
        );
        if (nextCurrentIndexVideoText !== currentIndexVideoText) {
            this.setState({ currentIndexVideoText: -1 }, () => {
                this.setState({ currentIndexVideoText: nextCurrentIndexVideoText });
            });
        }
    };

    handleRef = (video) => {
        this.setState({
            videoRef: video,
        });
    };

    handlePlay = () => {
        const { videoRef } = this.state;
        if (videoRef) {
            videoRef.play();
        }
    };

    handlePause = () => {
        const { videoRef } = this.state;
        if (videoRef) {
            videoRef.pause();
        }
    };

    goToLoginStep1() {
        const { dispatch } = this.props;
        dispatch(push("/loginStep1"));
    }

    render() {
        const { isMobile, showVideoControlsMobile } = this.props;
        const { videoTexts, currentIndexVideoText, isPlaying } = this.state;

        const videoPath = "videos/PIGGY_video_Text.mp4";
        return (
            <>
                {(!isMobile || showVideoControlsMobile) && (
                    <>
                        <div className="svg-image_login">
                            <Image src="images/logoCompany.svg" className="svg-image" />
                        </div>
                        <div className="video-controls">
                            <Button
                                image="images/play.svg"
                                // defaultLabelText="Play"
                                className="btn-only-icon btn-ico"
                                block={false}
                                bsStyle="link"
                                onClick={this.handlePlay}
                                disabled={isPlaying}
                            />
                            <Button
                                image="images/pause.svg"
                                // defaultLabelText="Pause"
                                className="btn-only-icon btn-ico"
                                block={false}
                                bsStyle="link"
                                onClick={this.handlePause}
                                disabled={!isPlaying}
                            />
                        </div>
                    </>
                )}
                {isMobile && showVideoControlsMobile && (
                    <Link to="/loginStep1" className="login-video-skip" onClick={this.handlePause}>
                        Skip
                    </Link>
                )}

                {currentIndexVideoText >= 0 && (!isMobile || showVideoControlsMobile) && (
                    <Spring
                        config={config.molasses}
                        from={{ opacity: 0 }}
                        to={{
                            opacity: 1,
                        }}>
                        {(styles) => (
                            <h1 style={styles} className="login-video-text">
                                {i18nUtils.get(videoTexts[currentIndexVideoText].text)}
                            </h1>
                        )}
                    </Spring>
                )}

                <div className="video-wrapper">
                    <video
                        playsInline
                        onPlay={this.onPlay}
                        onPause={this.onPause}
                        src={videoPath}
                        type="video/mp4"
                        ref={this.handleRef}
                        onLoadedMetadata={(e) => {
                            this.setMetadata(e.target.duration);
                        }}
                        onEnded={this.onVideoEnded}
                        autoPlay
                        muted="muted"
                    />
                </div>
            </>
        );
    }
}

export default connect(null)(LoginVideo);
