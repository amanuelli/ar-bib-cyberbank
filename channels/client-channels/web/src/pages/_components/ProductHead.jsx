/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/interactive-supports-focus */
import React, { Component, Fragment } from "react";
import { Col, Grid, Navbar, Row } from "react-bootstrap";
import { Link } from "react-router-dom";
import { func, node, string, bool, shape } from "prop-types";

import * as i18n from "util/i18n";

import I18n from "pages/_components/I18n";
import Image from "pages/_components/Image";
import Button from "pages/_components/Button";
import Dropdown from "pages/_components/Dropdown";
import { resizableRoute } from "pages/_components/Resizable";
import CollapsibleInfo from "pages/_components/CollapsibleInfo";
import { Helmet } from "react-helmet";

class ProductHead extends Component {
    static propTypes = {
        /**
         * back link url
         */
        backLinkTo: string,
        /**
         * back function, ignored if backLinkTo is specified
         */
        onBack: func,
        fetchingDownload: bool,
        onClickDownloadPDF: func,
        onClickDownloadXLS: func,
        renderDownload: func,
        handleOptionsClick: func,
        isDesktop: bool.isRequired,
        infoComponent: shape({
            data: node,
        }),
        children: node.isRequired,
    };

    static defaultProps = {
        onClickDownloadPDF: null,
        onClickDownloadXLS: null,
        renderDownload: null,
        handleOptionsClick: null,
        infoComponent: null,
        backLinkTo: "",
        fetchingDownload: false,
        onBack: null,
    };

    handleClick = () => {
        const { handleOptionsClick } = this.props;

        if (handleOptionsClick) {
            handleOptionsClick();
        }
        document.activeElement.blur();
    };

    renderMenuOption = () => {
        const { isDesktop, handleOptionsClick } = this.props;

        if (!isDesktop) {
            return (
                handleOptionsClick && (
                    <Button
                        className="toolbar-item toolbar-item--fixed toolbar-btn toolbar-btn-right view-options"
                        onClick={this.handleClick}
                        image="images/headerCommandsMobile.svg"
                        label="view more options"
                        block={false}
                        bsStyle="link"
                    />
                )
            );
        }
        return null;
    };

    renderDownloadDropdown = () => {
        const { fetchingDownload, onClickDownloadPDF, onClickDownloadXLS, renderDownload, isDesktop } = this.props;

        if (!isDesktop) {
            return null;
        }
        if (renderDownload) {
            return renderDownload(fetchingDownload);
        }
        return (
            <div className="toolbar-item">
                <Dropdown
                    image="images/download.svg"
                    label="global.download"
                    bsStyle="link"
                    fetching={fetchingDownload}
                    pullRight>
                    <Button
                        onClick={onClickDownloadPDF}
                        label="accounts.pdfFile"
                        className="dropdown__item-btn"
                        bsStyle="link"
                    />
                    <Button
                        onClick={onClickDownloadXLS}
                        label="accounts.xlsFile"
                        className="dropdown__item-btn"
                        bsStyle="link"
                    />
                </Dropdown>
            </div>
        );
    };

    render() {
        const { backLinkTo, children, isDesktop, infoComponent, onBack } = this.props;

        const { data } = infoComponent;
        const childrenListRender = children.slice(1);

        return (
            <Fragment>
                <header className="view-header theme-product-detail">
                    <Helmet>
                        <title>{`Techbank - ${i18n.get("activities.accounts.read")}`}</title>
                    </Helmet>
                    <Navbar collapseOnSelect fluid>
                        <Navbar.Header>
                            <div className="toolbar toolbar-product-name">
                                {backLinkTo && (
                                    <div className="toolbar-item toolbar-item--fixed toolbar-item-left">
                                        <Link className="btn btn-link toolbar-btn view-back" to={backLinkTo}>
                                            <Image className="svg-icon svg-caret" src="images/arrowLeft.svg" />
                                            <I18n id="global.back" componentProps={{ className: "visually-hidden" }} />
                                        </Link>
                                    </div>
                                )}
                                {onBack && (
                                    <div className="toolbar-item toolbar-item--fixed toolbar-item-left">
                                        <Button
                                            className="btn toolbar-btn view-back"
                                            onClick={onBack}
                                            image="images/arrowLeft.svg"
                                            block={false}
                                            label="global.back"
                                            bsStyle="link"
                                        />
                                    </div>
                                )}

                                {children[0]}
                                {this.renderMenuOption()}
                            </div>
                            {this.renderDownloadDropdown()}
                        </Navbar.Header>
                    </Navbar>

                    {isDesktop && infoComponent && (
                        <CollapsibleInfo
                            about={<span className="text-lead">{i18n.get("productHead.moreInfo.text")}</span>}>
                            {data}
                        </CollapsibleInfo>
                    )}
                </header>
                <div className="view-morphing theme-product-detail">
                    <section className="container--layout align-items-center section-content-heading">
                        <Grid fluid>
                            <Row className="justify-content-center">
                                <Col className="col">{childrenListRender}</Col>
                            </Row>
                        </Grid>
                    </section>
                </div>
            </Fragment>
        );
    }
}

export default resizableRoute(ProductHead);
