import React, { Component } from "react";
import PropTypes from "prop-types";

import Image from "pages/_components/Image";

class ProductTag extends Component {
    static propTypes = {
        children: PropTypes.node.isRequired,
        alias: PropTypes.string.isRequired,
        name: PropTypes.string.isRequired,
        icon: PropTypes.string,
    };

    static defaultProps = {
        icon: null,
    };

    render() {
        const { icon, alias, name, children } = this.props;

        return (
            <div className="newProduct">
                <div className="newProduct-row">
                    {icon && (
                        <div className="newProduct-cell newProduct-cell--icon">
                            <Image src={`images/${icon}.svg`} className="svg-icon" />
                        </div>
                    )}
                    <div className="newProduct-cell newProduct-cell--ellipsis">
                        <p className="data-name">{alias}</p>
                        <p className="data-product">{name}</p>
                    </div>
                </div>
                {children}
            </div>
        );
    }
}

export default ProductTag;
