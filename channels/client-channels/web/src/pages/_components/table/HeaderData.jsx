import React, { Component } from "react";
import { node, string } from "prop-types";

import TableData from "pages/_components/table/Data";

class TableHeaderData extends Component {
    static propTypes = {
        children: node,
        align: string,
    };

    static defaultProps = {
        children: null,
        align: "center",
    };

    render() {
        const { children, ...props } = this.props;
        return (
            <TableData className="table-data-head" {...props}>
                {children}
            </TableData>
        );
    }
}

export default TableHeaderData;
