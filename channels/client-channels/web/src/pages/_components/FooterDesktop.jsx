import React, { Children, Component } from "react";
import { Link } from "react-router-dom";
import { connect } from "react-redux";

import { selectors as i18nSelectors } from "reducers/i18n";

import Container from "pages/_components/Container";
import I18n from "pages/_components/I18n";

class FooterDesktop extends Component {
    render() {
        const { moreOptions } = this.props;

        if (!this.props.messages) {
            return null;
        }

        return (
            <React.Fragment>
                <Container className="align-items-center container--layout app-footer">
                    <div className="col-12">
                        <ul className="legal-nav list-inline">
                            <li>
                                <Link to="/privacyPolicy">
                                    <I18n id="global.privacy" />
                                </Link>
                            </li>
                            <li>
                                <Link to="/termsAndConditions">
                                    <I18n id="global.termAndConditions" />
                                </Link>
                            </li>
                            <li>
                                <Link to="/support">
                                    <I18n id="global.support" />
                                </Link>
                            </li>
                            {Children.map(moreOptions, (option) => (
                                <li>{option}</li>
                            ))}
                        </ul>
                    </div>
                </Container>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    messages: i18nSelectors.getMessages(state),
});

export default connect(mapStateToProps)(FooterDesktop);
