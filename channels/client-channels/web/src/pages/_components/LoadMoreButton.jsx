import React, { Component } from "react";
import { string, func } from "prop-types";

import Button from "pages/_components/Button";

class LoadMoreButton extends Component {
    static propTypes = {
        label: string.isRequired,
        onClick: func.isRequired,
    };

    render() {
        const { label, onClick } = this.props;

        return <Button label={label} bsStyle="primary" className="btn-small" onClick={onClick} />;
    }
}

export default LoadMoreButton;
