import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import { element, func, shape, string } from "prop-types";

import * as i18n from "util/i18n";

class UserLink extends Component {
    static propTypes = {
        children: element,
        onSelect: func,
        loggedUser: shape({
            userFullName: string.isRequired,
            email: string.isRequired,
        }).isRequired,
    };

    static defaultProps = {
        children: null,
        onSelect: null,
    };

    render() {
        const {
            children,
            onSelect,
            loggedUser: { userFullName, email },
        } = this.props;

        return (
            <Fragment>
                <p className="ellipsis">
                    <span className="text-lead">{userFullName}</span>
                </p>
                <p className="ellipsis">
                    <span className="data-email">{email}</span>
                </p>
                <Link className="btn-link" to="/settings" onClick={onSelect}>
                    {i18n.get("settings.index")}
                </Link>
                <hr />
                {children}
            </Fragment>
        );
    }
}

export default UserLink;
