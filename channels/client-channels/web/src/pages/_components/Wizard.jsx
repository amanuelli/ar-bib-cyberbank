import { Component } from "react";
import { node } from "prop-types";

import WizardTabs from "pages/_components/wizard/WizardTabs";
import WizardSteps from "pages/_components/wizard/WizardSteps";

class Wizard extends Component {
    static propTypes = {
        children: node.isRequired,
    };

    static Tabs = WizardTabs;

    static Tab = ({ children }) => children;

    static Steps = WizardSteps;

    static Step = ({ children }) => children;

    render() {
        const { children } = this.props;

        return children;
    }
}

export default Wizard;
