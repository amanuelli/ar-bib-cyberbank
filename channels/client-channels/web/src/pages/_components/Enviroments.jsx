import React, { Component } from "react";
import { shape, bool, string } from "prop-types";
import classNames from "classnames";

import Image from "pages/_components/Image";
import I18n from "pages/_components/I18n";
import FieldError from "pages/_components/fields/FieldError";
import EnvironmentTag from "pages/_components/EnvironmentTag";

class Enviroments extends Component {
    static propTypes = {
        environments: shape([]).isRequired,
        field: shape({}).isRequired,
        form: shape({}).isRequired,
        activeEnvironment: shape({}),
        fromSetDefaultEnvironmentPage: bool,
        userData: shape({}),
        legendTextID: string,
    };

    static defaultProps = {
        activeEnvironment: {},
        fromSetDefaultEnvironmentPage: false,
        userData: {},
        legendTextID: "settings.changeEnvironment",
    };

    handleClick = (value) => {
        const { field, form } = this.props;
        form.setFieldValue(field.name, value);
    };

    render() {
        const {
            environments,
            legendTextID,
            activeEnvironment,
            form: { errors },
            field,
            fromSetDefaultEnvironmentPage,
            userData,
        } = this.props;

        const environmentToCheck = fromSetDefaultEnvironmentPage ? userData.idDefaultEnvironment : activeEnvironment.id;

        return (
            <fieldset className={classNames("form-group", { "has-error": errors[field.name] })}>
                {legendTextID && (
                    <div>
                        <I18n id={legendTextID} component="legend" componentProps={{ className: "visually-hidden" }} />
                    </div>
                )}

                <div className="selection-list">
                    {environments.map(([id, environment]) => {
                        const allowedToAccess = environment.allowedToAccess.toString() === "true";
                        return (
                            <>
                                <div
                                    className={`c-control c-control--radio ${
                                        !allowedToAccess && !fromSetDefaultEnvironmentPage ? "env-disabled" : ""
                                    }`}
                                    key={id}>
                                    <input
                                        disabled={!allowedToAccess && !fromSetDefaultEnvironmentPage}
                                        defaultChecked={Number(id) === environmentToCheck}
                                        id={`environment-${id}`}
                                        type="radio"
                                        name="environment"
                                        className="c-control-input"
                                        onClick={() => this.handleClick(id)}
                                    />
                                    <label
                                        className="c-control-label c-control-label--environment"
                                        htmlFor={`environment-${id}`}>
                                        <EnvironmentTag name={environment.name} type={environment.environmentType} />
                                    </label>
                                </div>
                                {!allowedToAccess && !fromSetDefaultEnvironmentPage && (
                                    <div className="environtment-disabled-message">
                                        <Image src="images/warningIcon.svg" className="svg-icon" />
                                        <I18n id="administration.restrictions.unavailableEnvironment" />
                                    </div>
                                )}
                            </>
                        );
                    })}
                </div>
                {errors[field.name] && (
                    <div className="text-center">
                        <FieldError error={errors[field.name]} />
                    </div>
                )}
            </fieldset>
        );
    }
}

export default Enviroments;
