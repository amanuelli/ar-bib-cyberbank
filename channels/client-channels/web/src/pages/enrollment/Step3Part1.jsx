import React, { Component, Fragment } from "react";
import { Col } from "react-bootstrap";
import { connect } from "react-redux";
import { compose } from "redux";
import { Field, Form, withFormik } from "formik";
import { func, string, bool, shape } from "prop-types";
import * as Yup from "yup";
import { push } from "react-router-redux";

import { actions as enrollmentActions, selectors as enrollmentSelectors } from "reducers/enrollment";
import { selectors as onboardingSelectors } from "reducers/onboarding";
import { actions as loginActions } from "reducers/login";
import * as configUtils from "util/config";
import * as i18nUtils from "util/i18n";

import Button from "pages/_components/Button";
import Container from "pages/_components/Container";
import Head from "pages/_components/Head";
import I18n from "pages/_components/I18n";
import PasswordRules from "pages/_components/PasswordRules";
import Credential from "pages/_components/fields/credentials/Credential";
import withExchangeToken from "pages/_components/withExchangeToken";
import OnboardingStepper, { orientations } from "pages/onboarding/_components/OnboardingStepper";
import { Mixpanel } from "util/clickstreaming";
import EnrollmentStepper from "./_components/EnrollmentStepper";

const FORM_ID = "enrollment.step3";

class Step3Part1 extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        exchangeToken: string.isRequired,
        invitationCode: string.isRequired,
        isMobile: bool.isRequired,
        isSubmitting: bool.isRequired,
        documentData: shape({
            firstName: string.isRequired,
        }).isRequired,
    };

    componentDidMount() {
        const { dispatch, invitationCode, exchangeToken } = this.props;

        if (!invitationCode) {
            dispatch(enrollmentActions.goToStep0());
        }

        dispatch(enrollmentActions.requestSecuritySeals(exchangeToken));
    }

    onHeaderClose = () => {
        const { dispatch } = this.props;

        dispatch(loginActions.reset());
    };

    render() {
        const { isSubmitting, isMobile, documentData } = this.props;
        const maxLength = configUtils.getInteger("core.password.maxLength");
        const step = 9;
        return (
            <Fragment>
                <Head title="onboarding.step.9.title" onClose={isMobile && this.onHeaderClose} />
                <div className="view-page">
                    {!isMobile && documentData.firstName && (
                        <OnboardingStepper currentStep={step} className="onboarding-steps" />
                    )}
                    {!(isMobile || documentData.firstName) && (
                        <EnrollmentStepper currentStep={3} className="onboarding-steps" />
                    )}
                    <div className="view-content">
                        <main className="main-container">
                            <Form className="above-the-fold">
                                {isMobile && (
                                    <Container className="container--layout align-items-center">
                                        <Col className="col">
                                            {documentData.firstName && (
                                                <OnboardingStepper
                                                    currentStep={step}
                                                    orientation={orientations.horizontal}
                                                />
                                            )}
                                            {!documentData.firstName && (
                                                <EnrollmentStepper
                                                    currentStep={3}
                                                    orientation={orientations.horizontal}
                                                />
                                            )}
                                        </Col>
                                    </Container>
                                )}
                                <Container
                                    className="container--layout align-items-center"
                                    gridClassName="form-content">
                                    <Col className="col col-12">
                                        <I18n
                                            component="p"
                                            componentProps={{ className: "text-lead" }}
                                            id="onboarding.step.9.info"
                                        />
                                    </Col>
                                </Container>
                                <Container
                                    className="container--layout flex-grow align-items-center"
                                    gridClassName="form-content">
                                    <Col className="col col-12">
                                        <Field
                                            component={Credential}
                                            hidePlaceholder
                                            idForm={FORM_ID}
                                            name="password"
                                            showStrength
                                            maxLength={maxLength}
                                        />
                                        <PasswordRules>
                                            <I18n
                                                id={`${FORM_ID}.passwordRules.rule1`}
                                                MIN_LENGTH={configUtils.get("core.password.minLength")}
                                                MAX_LENGTH={configUtils.get("core.password.maxLength")}
                                            />
                                            <I18n id={`${FORM_ID}.passwordRules.rule2`} />
                                            <I18n id={`${FORM_ID}.passwordRules.rule3`} />
                                            <I18n id={`${FORM_ID}.passwordRules.rule4`} />
                                        </PasswordRules>
                                        <Field
                                            component={Credential}
                                            hidePlaceholder
                                            idForm={FORM_ID}
                                            name="passwordConfirmation"
                                            maxLength={maxLength}
                                        />
                                    </Col>
                                    <Col className="col col-12">
                                        <Button
                                            label="global.continue"
                                            type="submit"
                                            bsStyle="primary"
                                            loading={isSubmitting}
                                        />
                                    </Col>
                                </Container>
                                {/* <Container className="container--layout align-items-center"></Container> */}
                            </Form>
                        </main>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    fetching: enrollmentSelectors.getFetching(state),
    exchangeToken: enrollmentSelectors.getExchangeToken(state),
    invitation: enrollmentSelectors.getInvitation(state),
    invitationCode: enrollmentSelectors.getInvitationCode(state),
    documentData: onboardingSelectors.getDocumentData(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: () => ({ password: "", passwordConfirmation: "" }),
        validationSchema: () =>
            Yup.lazy((values) =>
                Yup.object().shape({
                    password: Yup.string()
                        .required(i18nUtils.get("enrollment.step3.password.error.empty"))
                        .min(
                            configUtils.getInteger("core.password.minLength"),
                            i18nUtils.get("enrollment.step3.password.error.unfulfilledRules"),
                        )
                        .max(
                            configUtils.getInteger("core.password.maxLength"),
                            i18nUtils.get("enrollment.step3.password.error.unfulfilledRules"),
                        )
                        .matches(
                            new RegExp(configUtils.get("core.userconfiguration.password.pattern"), "g"),
                            i18nUtils.get("enrollment.step3.password.error.unfulfilledRules"),
                        )
                        .oneOf(
                            [values.passwordConfirmation],
                            i18nUtils.get("enrollment.step3.password.error.passwordConfirmationMustMatch"),
                        ),
                    passwordConfirmation: Yup.string()
                        .required(i18nUtils.get("enrollment.step3.passwordConfirmation.error.empty"))
                        .min(
                            configUtils.getInteger("core.password.minLength"),
                            i18nUtils.get("enrollment.step3.passwordConfirmation.error.unfulfilledRules"),
                        )
                        .max(
                            configUtils.getInteger("core.password.maxLength"),
                            i18nUtils.get("enrollment.step3.passwordConfirmation.error.unfulfilledRules"),
                        )
                        .matches(
                            new RegExp(configUtils.get("core.userconfiguration.password.pattern"), "g"),
                            i18nUtils.get("enrollment.step3.passwordConfirmation.error.unfulfilledRules"),
                        )
                        .oneOf(
                            [values.password],
                            i18nUtils.get("enrollment.step3.passwordConfirmation.error.passwordMustMatch"),
                        ),
                }),
            ),
        handleSubmit: ({ password, passwordConfirmation }, formikBag) => {
            const { dispatch } = formikBag.props;
            dispatch(enrollmentActions.setPassword(password, passwordConfirmation));
            Mixpanel.track(`${FORM_ID}.password`);
            dispatch(push("/enrollment/step3part3"));
        },
    }),
)(withExchangeToken(Step3Part1));
