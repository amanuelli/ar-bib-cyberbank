import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { goBack } from "react-router-redux";
import { Link } from "react-router-dom";
import { compose } from "redux";
import { Field, Form, withFormik } from "formik";
import * as Yup from "yup";
import { bool, func, shape, string } from "prop-types";
import Col from "react-bootstrap/lib/Col";
import { actions as enrollmentActions, selectors as enrollmentSelectors } from "reducers/enrollment";
import { selectors as onboardingSelectors } from "reducers/onboarding";
import { actions as loginActions, selectors as loginSelectors } from "reducers/login";
import { actions as settingsActions } from "reducers/settings";
import * as configUtils from "util/config";
import * as i18nUtils from "util/i18n";
import * as maskUtils from "util/mask";
import MaskedTextField from "pages/_components/fields/MaskedTextField";
import Button from "pages/_components/Button";
import Container from "pages/_components/Container";
import Head from "pages/_components/Head";
import I18n from "pages/_components/I18n";
import OnboardingStepper, { orientations } from "pages/onboarding/_components/OnboardingStepper";
import { Mixpanel } from "util/clickstreaming";
import { selectors as i18nSelectors } from "reducers/i18n";

const FORM_ID = "enrollment.index";

class Index extends Component {
    static propTypes = {
        isDesktop: bool.isRequired,
        isSubmitting: bool.isRequired,
        dispatch: func.isRequired,
        documentData: shape({}).isRequired,
        isMobile: bool.isRequired,
        activeLanguage: string.isRequired,
        match: shape.isRequired,
    };

    handleHeaderBack = () => {
        const { dispatch } = this.props;

        dispatch(goBack());
    };

    handleHeaderClose = () => {
        const { dispatch } = this.props;

        dispatch(loginActions.reset());
    };

    componentDidMount = () => {
        const { match, dispatch, activeLanguage } = this.props;
        const linkLang = match.params.lang;
        if (linkLang !== undefined && linkLang !== null && linkLang !== activeLanguage) {
            dispatch(settingsActions.changeLanguage(linkLang));
        }
    };

    render() {
        const { isDesktop, isMobile, isSubmitting, documentData } = this.props;
        const isDigitalEnrollmentEnabled = configUtils.getBoolean("enrollment.digital.automatic");

        const fullName = documentData.firstName ? `${documentData.firstName} ${documentData.lastName}` : null;

        const LeadingText = !fullName ? (
            <I18n component="p" componentProps={{ className: "text-lead" }} id="enrollment.index.title" />
        ) : (
            <I18n
                component="p"
                componentProps={{ className: "text-lead" }}
                id="enrollment.index.titleFromOnboarding"
                USER_NAME={fullName}
            />
        );

        const step = 7;

        return (
            <Fragment>
                <Head
                    title={`${FORM_ID}.header`}
                    onBack={!isDesktop ? this.handleHeaderBack : null}
                    onClose={!isDesktop ? this.handleHeaderClose : null}
                />

                <div className="view-page">
                    {isDesktop && documentData.firstName && (
                        <OnboardingStepper currentStep={step} className="onboarding-steps" />
                    )}
                    <div className="view-content">
                        <main className="main-container">
                            <Form className="above-the-fold">
                                {isMobile && documentData.firstName && (
                                    <Container className="container--layout align-items-center">
                                        <Col className="col col-12">
                                            <OnboardingStepper
                                                currentStep={step}
                                                orientation={orientations.horizontal}
                                            />
                                        </Col>
                                    </Container>
                                )}
                                <Container className="container--layout align-items-center">
                                    <Col className="col">{LeadingText}</Col>
                                </Container>
                                <Container
                                    className="container--layout flex-grow align-items-center"
                                    gridClassName="form-content">
                                    <Col className="col">
                                        <Field
                                            autoComplete="off"
                                            component={MaskedTextField}
                                            hidePlaceholder
                                            idForm={FORM_ID}
                                            mask={maskUtils.invitationCodeMask()}
                                            maxLength={50}
                                            name="invitationCode"
                                        />
                                        <Link to="/enrollment/requestInvitationCode">
                                            <I18n id="enrollment.index.invitationCode.request" />
                                        </Link>
                                    </Col>
                                </Container>
                                <Container className="align-items-center container--layout">
                                    {isDigitalEnrollmentEnabled && (
                                        <Col className="col">
                                            <Button
                                                bsStyle="primary"
                                                label="global.continue"
                                                loading={isSubmitting}
                                                type="submit"
                                            />
                                        </Col>
                                    )}
                                </Container>
                            </Form>
                        </main>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    exchangeToken: enrollmentSelectors.getExchangeToken(state),
    invitationCode: enrollmentSelectors.getInvitationCode(state),
    documentData: onboardingSelectors.getDocumentData(state),
    activeLanguage: i18nSelectors.getLang(state),
    region: loginSelectors.getRegion(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: (props) => ({
            invitationCode: props.match.params.code !== "requestInvitationCode" ? props.match.params.code : "",
        }),
        validationSchema: () => {
            const unmaskedLength = configUtils.get("backoffice.invitationCodes.unmaskedLength");

            return Yup.object().shape({
                invitationCode: Yup.string()
                    .matches(
                        new RegExp(`^([a-zA-Z\\d]{${unmaskedLength}}(-[a-zA-Z\\d]{${unmaskedLength}}){2})$`, "g"),
                        i18nUtils.get("enrollment.index.invitationCode.invalidFormat"),
                    )
                    .required(i18nUtils.get("enrollment.index.invitationCode.empty")),
            });
        },
        handleSubmit: ({ invitationCode }, formikBag, region) => {
            const { dispatch } = formikBag.props;
            dispatch(enrollmentActions.verifyInvitationCode(invitationCode, formikBag, region));
            Mixpanel.track(FORM_ID);
        },
    }),
)(Index);
