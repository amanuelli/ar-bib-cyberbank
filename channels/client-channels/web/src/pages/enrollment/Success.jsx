import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { push } from "react-router-redux";

import { selectors as loginSelectors } from "reducers/login";
import { shape, bool, string, func } from "prop-types";

import Image from "pages/_components/Image";
import { Col } from "react-bootstrap";
import MainContainer from "pages/_components/MainContainer";
import Button from "pages/_components/Button";
import GeneralMsg from "pages/_components/GeneralMsg";
import * as i18nUtils from "util/i18n";
import Container from "pages/_components/Container";
import REGION_USA from "constants";

class Success extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        fromOnboardingLoginData: shape({
            acceptESign: bool,
            firstName: string,
            lastName: string,
        }).isRequired,
        activeRegion: string.isRequired,
    };

    handleButtonClick = () => {
        const { dispatch } = this.props;
        this.setState(() => {
            dispatch(push("/"));
        });
    };

    render() {
        const { fromOnboardingLoginData, activeRegion, dispatch } = this.props;
        const isUSA = REGION_USA === activeRegion;
        const msg = i18nUtils.get(
            !fromOnboardingLoginData.acceptESign && isUSA
                ? "login.step1.welcomeFromOnboarding.noEsign.title"
                : "login.step1.welcomeFromOnboarding.success.title",
        );

        return (
            <Fragment>
                <MainContainer className="above-the-fold">
                    <Container className="container--layout flex-grow align-items-center">
                        <Image
                            src={
                                !fromOnboardingLoginData.acceptESign && isUSA
                                    ? "images/incompleteOnboard.svg"
                                    : "images/successOnboard.svg"
                            }
                        />
                        <GeneralMsg
                            title={
                                !fromOnboardingLoginData.acceptESign && isUSA
                                    ? `${fromOnboardingLoginData.firstName}${msg}`
                                    : `${msg} ${fromOnboardingLoginData.firstName}`
                            }
                            description={`${i18nUtils.get(
                                !fromOnboardingLoginData.acceptESign && isUSA
                                    ? "login.step1.welcomeFromOnboarding.noEsign"
                                    : "login.step1.welcomeFromOnboarding.success",
                            )}`}
                        />
                    </Container>
                    <Container className="container--layout align-items-center">
                        <Col className="col col-12 col-lg-6 col-md-9 col-sm-12">
                            <Button
                                type="button"
                                bsStyle="primary"
                                label="global.continue"
                                onClick={() => dispatch(push("/"))}
                            />
                        </Col>
                    </Container>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    fromOnboardingLoginData: loginSelectors.getFromOnboardingLoginData(state),
    activeRegion: loginSelectors.getRegion(state),
});

export default connect(mapStateToProps)(Success);
