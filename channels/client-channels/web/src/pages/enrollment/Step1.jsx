import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { goBack } from "react-router-redux";
import { compose } from "redux";
import Col from "react-bootstrap/lib/Col";
import { Field, Form, withFormik } from "formik";
import { bool, func, shape, string } from "prop-types";
import * as Yup from "yup";
import { actions as enrollmentActions, selectors as enrollmentSelectors } from "reducers/enrollment";
import { selectors as onboardingSelectors } from "reducers/onboarding";
import { actions as loginActions } from "reducers/login";
import * as configUtils from "util/config";
import * as i18nUtils from "util/i18n";
import TextField from "pages/_components/fields/TextField";
import Button from "pages/_components/Button";
import Container from "pages/_components/Container";
import Head from "pages/_components/Head";
import I18n from "pages/_components/I18n";
import Notification from "pages/_components/Notification";
import OnboardingStepper, { orientations } from "pages/onboarding/_components/OnboardingStepper";
import { Link } from "react-router-dom";
import { Mixpanel } from "util/clickstreaming";
import EnrollmentStepper from "./_components/EnrollmentStepper";

const FORM_ID = "enrollment.step1";

class Step1 extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        exchangeToken: string.isRequired,
        invitationCode: string.isRequired,
        isDesktop: bool.isRequired,
        isSubmitting: bool.isRequired,
        documentData: shape({}).isRequired,
    };

    componentDidMount() {
        const { dispatch, exchangeToken, invitationCode } = this.props;

        if (!invitationCode) {
            dispatch(enrollmentActions.goToStep0());
        } else {
            dispatch(enrollmentActions.requestVerificationCodePre(invitationCode, exchangeToken));
        }
    }

    handleClick = () => {
        const { dispatch, exchangeToken, invitationCode } = this.props;

        dispatch(enrollmentActions.resendVerificationCode(invitationCode, exchangeToken));
    };

    handleHeaderBack = () => {
        const { dispatch } = this.props;

        dispatch(goBack());
    };

    handleHeaderClose = () => {
        const { dispatch } = this.props;

        dispatch(loginActions.reset());
    };

    render() {
        const { isDesktop, isSubmitting, documentData } = this.props;
        const verificationCodeLength = configUtils.getInteger("enrollment.verificationCode.length", 4);
        const step = 8;

        return (
            <Fragment>
                <Head title={`${FORM_ID}.header`} onClose={!isDesktop ? this.handleHeaderClose : null} />

                <div className="view-page">
                    {isDesktop && documentData.firstName && (
                        <OnboardingStepper currentStep={step} className="onboarding-steps" />
                    )}
                    {isDesktop && !documentData.firstName && (
                        <EnrollmentStepper currentStep={2} className="onboarding-steps" />
                    )}
                    <div className="view-content">
                        <main className="main-container">
                            <Form className="above-the-fold">
                                {!isDesktop && (
                                    <Container className="container--layout align-items-center">
                                        <Col className="col col-12">
                                            {!isDesktop && documentData.firstName && (
                                                <OnboardingStepper
                                                    currentStep={step}
                                                    orientation={orientations.horizontal}
                                                />
                                            )}
                                            {!(isDesktop || documentData.firstName) && (
                                                <EnrollmentStepper
                                                    currentStep={2}
                                                    orientation={orientations.horizontal}
                                                />
                                            )}
                                        </Col>
                                    </Container>
                                )}

                                <Container
                                    className="container--layout align-items-center"
                                    gridClassName="form-content">
                                    <Col className="col col-12">
                                        <I18n component="p" id="enrollment.step1.subtitle" />
                                    </Col>
                                </Container>

                                <Container
                                    className="container--layout flex-grow align-items-center"
                                    gridClassName="form-content">
                                    <Col className="col col-12">
                                        <Field
                                            className="form-control form-control--verification-code"
                                            autoComplete="off"
                                            component={TextField}
                                            hidePlaceholder
                                            idForm={FORM_ID}
                                            maxLength={verificationCodeLength}
                                            name="verificationCode"
                                        />
                                        <Link to="/enrollment/">
                                            <I18n id="enrollment.step1.link" />
                                        </Link>
                                    </Col>
                                </Container>
                                <Container className="align-items-center container--layout">
                                    <Col className="col col-12">
                                        <Button
                                            bsStyle="primary"
                                            label="global.continue"
                                            loading={isSubmitting}
                                            type="submit"
                                        />
                                    </Col>
                                </Container>
                            </Form>
                        </main>
                    </div>
                </div>
                <Notification scopeToShow="enrollment/step1" />
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    exchangeToken: enrollmentSelectors.getExchangeToken(state),
    invitationCode: enrollmentSelectors.getInvitationCode(state),
    documentData: onboardingSelectors.getDocumentData(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: () => ({
            verificationCode: "",
        }),
        validationSchema: () =>
            Yup.object().shape({
                verificationCode: Yup.string()
                    .min(
                        configUtils.getInteger("enrollment.verificationCode.length"),
                        i18nUtils.get("enrollment.step1.verificationCode.invalidFormat"),
                    )
                    .max(
                        configUtils.getInteger("enrollment.verificationCode.length"),
                        i18nUtils.get("enrollment.step1.verificationCode.invalidFormat"),
                    )
                    .required(i18nUtils.get("enrollment.step1.verificationCode.empty")),
            }),
        handleSubmit: ({ verificationCode }, formikBag) => {
            const { dispatch } = formikBag.props;

            Mixpanel.track(FORM_ID);
            dispatch(enrollmentActions.verifyVerificationCode(verificationCode, formikBag));
        },
    }),
)(Step1);
