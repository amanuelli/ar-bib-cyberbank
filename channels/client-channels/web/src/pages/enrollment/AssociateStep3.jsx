import React, { Component, Fragment } from "react";
import Col from "react-bootstrap/lib/Col";
import { connect } from "react-redux";
import { goBack } from "react-router-redux";

import { actions as enrollmentActions, selectors as enrollmentSelectors } from "reducers/enrollment";
import { actions as loginActions } from "reducers/login";

import Button from "pages/_components/Button";
import Container from "pages/_components/Container";
import Head from "pages/_components/Head";
import I18n from "pages/_components/I18n";
import Notification from "pages/_components/Notification";
import withExchangeToken from "pages/_components/withExchangeToken";
import { bool, func, string, number, shape } from "prop-types";
import AssociateStepper, { orientations } from "./_components/AssociateStepper";

class AssociateStep3 extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        invitationCode: string.isRequired,
        isDesktop: bool.isRequired,
        account: shape({
            balance: number.isRequired,
            currency: string.isRequired,
            idProduct: string.isRequired,
            number: string.isRequired,
            productAlias: string,
            productType: string.isRequired,
        }).isRequired,
        exchangeToken: string.isRequired,
        fetching: bool,
        client: shape({}).isRequired,
        invitation: string.isRequired,
    };

    static defaultProps = {
        fetching: true,
    };

    componentDidMount() {
        const { dispatch, invitationCode } = this.props;

        if (!invitationCode) {
            dispatch(enrollmentActions.goToStep0());
        }
    }

    handleClick = () => {
        const { exchangeToken, dispatch, invitationCode } = this.props;

        dispatch(enrollmentActions.associateStep3(invitationCode, exchangeToken));
    };

    handleHeaderBack = () => {
        const { dispatch } = this.props;

        dispatch(goBack());
    };

    handleHeaderClose = () => {
        const { dispatch } = this.props;

        dispatch(loginActions.reset());
    };

    render() {
        const { account, client, invitation, isDesktop, fetching } = this.props;
        const orientation = isDesktop ? orientations.vertical : orientations.horizontal;

        const info = {
            name:
                (client && `${client.firstName} ${client.lastName}`) ||
                (invitation && `${invitation.firstName} ${invitation.lastName}`),
            document:
                (client && `${client.documentNumber} (${client.documentType}, ${client.documentCountry})`) ||
                (invitation &&
                    `${invitation.documentNumber} (${invitation.documentType}, ${invitation.documentCountry})`),
            account: account && account.accountName,
        };

        return (
            <Fragment>
                <Notification scopeToShow="enrollment/associate/step3" />
                <Head
                    title="enrollment.associate.step3.header"
                    onBack={!isDesktop ? this.handleHeaderBack : null}
                    onClose={!isDesktop ? this.handleHeaderClose : null}
                />
                <div className="view-page">
                    {isDesktop && <AssociateStepper currentStep={3} className="onboarding-steps" />}
                    <div className="view-content">
                        <main className="main-container">
                            <div className="above-the-fold">
                                {!isDesktop && (
                                    <Container className="container--layout align-items-center">
                                        <Col className="col col-12">
                                            <AssociateStepper currentStep={3} orientation={orientation} />
                                        </Col>
                                    </Container>
                                )}
                                <Container
                                    className="container--layout flex-grow align-items-center"
                                    gridClassName="form-content">
                                    <Col className="col col-12">
                                        <p className="text-lead">
                                            <I18n id="enrollment.associate.step3.title" />
                                        </p>
                                        <div className="text-lead">
                                            <p>
                                                <I18n id="enrollment.associate.step3.name" NAME={info.name} />
                                            </p>
                                            <p>
                                                <I18n
                                                    id="enrollment.associate.step3.document"
                                                    DOCUMENT={info.document}
                                                />
                                            </p>
                                            <p>
                                                <I18n id="enrollment.associate.step3.account" ACCOUNT={info.account} />
                                            </p>
                                        </div>
                                    </Col>
                                    <Col className="col col-12">
                                        <Button
                                            bsStyle="primary"
                                            label="global.continue"
                                            onClick={this.handleClick}
                                            type="button"
                                            loading={fetching}
                                        />
                                    </Col>
                                </Container>
                                {/* <Container className="align-items-center container--layout"></Container> */}
                            </div>
                        </main>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    account: enrollmentSelectors.getAccount(state),
    client: enrollmentSelectors.getClient(state),
    exchangeToken: enrollmentSelectors.getExchangeToken(state),
    invitation: enrollmentSelectors.getInvitation(state),
    invitationCode: enrollmentSelectors.getInvitationCode(state),
    fetching: enrollmentSelectors.getFetching(state),
});

export default connect(mapStateToProps)(withExchangeToken(AssociateStep3));
