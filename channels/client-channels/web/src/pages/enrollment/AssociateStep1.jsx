import React, { Component, Fragment } from "react";
import Col from "react-bootstrap/lib/Col";
import { connect } from "react-redux";
import { goBack } from "react-router-redux";
import { compose } from "redux";
import { Field, Form, withFormik } from "formik";
import * as Yup from "yup";

import { actions as enrollmentActions, selectors as enrollmentSelectors } from "reducers/enrollment";
import { actions as loginActions } from "reducers/login";

import Button from "pages/_components/Button";
import Container from "pages/_components/Container";
import Head from "pages/_components/Head";
import I18n from "pages/_components/I18n";
import Captcha from "pages/_components/fields/credentials/Captcha";
import Credential from "pages/_components/fields/credentials/Credential";
import TextField from "pages/_components/fields/TextField";
import withExchangeToken from "pages/_components/withExchangeToken";
import { bool, func, string, shape } from "prop-types";

import * as i18nUtils from "util/i18n";
import AssociateStepper, { orientations } from "./_components/AssociateStepper";

const FORM_ID = "enrollment.associate.step1";

class AssociateStep1 extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        exchangeToken: string.isRequired,
        invitationCode: string.isRequired,
        isDesktop: bool.isRequired,
        isSubmitting: bool.isRequired,
        captchaRequired: bool,
        isMobileNative: bool,
        secondFactorAuth: string,
        account: shape({ accountName: string.isRequired }).isRequired,
    };

    static defaultProps = {
        captchaRequired: true,
        isMobileNative: false,
        secondFactorAuth: undefined,
    };

    componentDidMount() {
        const { dispatch, exchangeToken, invitationCode } = this.props;

        if (!invitationCode) {
            dispatch(enrollmentActions.goToStep0());
        } else {
            dispatch(enrollmentActions.associateStep1Pre(invitationCode, exchangeToken));
        }
    }

    handleHeaderBack = () => {
        const { dispatch } = this.props;

        dispatch(goBack());
    };

    handleHeaderClose = () => {
        const { dispatch } = this.props;

        dispatch(loginActions.reset());
    };

    render() {
        const { account, captchaRequired, isDesktop, isMobileNative, isSubmitting, secondFactorAuth } = this.props;
        const orientation = isDesktop ? orientations.vertical : orientations.horizontal;

        return (
            <Fragment>
                <Head
                    title="enrollment.associate.step1.header"
                    onBack={!isDesktop ? this.handleHeaderBack : null}
                    onClose={!isDesktop ? this.handleHeaderClose : null}
                />
                <div className="view-page">
                    {isDesktop && (
                        <AssociateStepper currentStep={1} orientation={orientation} className="onboarding-steps" />
                    )}
                    <div className="view-content">
                        <main className="main-container">
                            <Form className="above-the-fold">
                                {!isDesktop && (
                                    <Container className="container--layout align-items-center">
                                        <Col className="col col-12">
                                            <AssociateStepper currentStep={1} orientation={orientation} />
                                        </Col>
                                    </Container>
                                )}
                                <Container
                                    className="container--layout flex-grow align-items-center"
                                    gridClassName="form-content">
                                    <Col className="col col-12">
                                        <p className="text-lead">
                                            <I18n
                                                id="enrollment.associate.step1.title"
                                                ENVIRONMENT_OWNER={account && account.accountName}
                                            />
                                        </p>
                                        <Field
                                            idForm={FORM_ID}
                                            name="username"
                                            type="email"
                                            component={TextField}
                                            autoFocus={isDesktop}
                                        />
                                        {secondFactorAuth === "otp" && (
                                            <Field
                                                idForm={FORM_ID}
                                                name="secondFactor"
                                                component={Credential}
                                                type="otp"
                                            />
                                        )}
                                        {!isMobileNative && captchaRequired && (
                                            <Field name="captcha" idForm={FORM_ID} component={Captcha} />
                                        )}
                                    </Col>
                                    <Col className="col col-12">
                                        <Button
                                            bsStyle="primary"
                                            label="global.continue"
                                            loading={isSubmitting}
                                            type="submit"
                                        />
                                    </Col>
                                </Container>
                                {/* <Container className="align-items-center container--layout"></Container> */}
                            </Form>
                        </main>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    account: enrollmentSelectors.getAccount(state),
    captchaRequired: enrollmentSelectors.getCaptchaRequired(state),
    client: enrollmentSelectors.getClient(state),
    exchangeToken: enrollmentSelectors.getExchangeToken(state),
    fetching: enrollmentSelectors.getFetching(state),
    invitation: enrollmentSelectors.getInvitation(state),
    invitationCode: enrollmentSelectors.getInvitationCode(state),
    secondFactorAuth: enrollmentSelectors.getSecondFactorAuth(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: () => ({
            captcha: "",
            secondFactor: "",
            username: "",
        }),
        validationSchema: (props) => {
            const { captchaRequired, isMobileNative } = props;

            return Yup.object().shape({
                captcha:
                    !isMobileNative && captchaRequired
                        ? Yup.string().required(i18nUtils.get(`${FORM_ID}.captcha.error.required`))
                        : Yup.string(),
                secondFactor: Yup.string().required(i18nUtils.get(`${FORM_ID}.secondFactor.error.empty`)),
                username: Yup.string()
                    .email(i18nUtils.get(`${FORM_ID}.username.error.invalidFormat`))
                    .required(i18nUtils.get(`${FORM_ID}.username.error.empty`)),
            });
        },
        handleSubmit: ({ captcha, secondFactor, username }, formikBag) => {
            const { dispatch } = formikBag.props;

            dispatch(enrollmentActions.associateStep1Verify(captcha, secondFactor, username, formikBag));
        },
    }),
)(withExchangeToken(AssociateStep1));
