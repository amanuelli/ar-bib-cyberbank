import React from "react";
import Stepper, { Step, orientations } from "pages/_components/StepIndicator/index";
import I18n from "pages/_components/I18n";
import { string, number, oneOf } from "prop-types";

const AssociateStepper = ({ currentStep, orientation, className }) => (
    <Stepper className={className} orientation={orientation}>
        <Step currentStep={currentStep} stepNumber={1}>
            <I18n id="associate.steps.indicator.1" />
        </Step>
        <Step currentStep={currentStep} stepNumber={2}>
            <I18n id="associate.steps.indicator.2" />
        </Step>
        <Step currentStep={currentStep} stepNumber={3}>
            <I18n id="associate.steps.indicator.3" />
        </Step>
    </Stepper>
);

AssociateStepper.propTypes = {
    currentStep: number.isRequired,
    orientation: oneOf(Object.keys(orientations)),
    className: string,
};

AssociateStepper.defaultProps = {
    orientation: orientations.vertical,
    className: undefined,
};

export default AssociateStepper;
export { orientations };
