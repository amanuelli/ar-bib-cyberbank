/* eslint-disable jsx-a11y/interactive-supports-focus */
import React, { Component } from "react";
import { Col } from "react-bootstrap";
import { connect } from "react-redux";
import classNames from "classnames";
import { bool, func, arrayOf, string, shape, number, objectOf, oneOfType } from "prop-types";

import { selectors as sessionSelectors } from "reducers/session";
import { selectors as desktopSelectors, actions } from "reducers/desktop";
import { selectors as campaignsSelectors } from "reducers/campaigns";
import { selectors as loginSelectors } from "reducers/login";
import { CORPORATE_GROUP_ENVIRONMENT_TYPE } from "constants.js";
import * as stringUtils from "util/string";
import * as i18n from "util/i18n";

import Button from "pages/_components/Button";
import MainContainer from "pages/_components/MainContainer";
import Notification from "pages/_components/Notification";
import DraggableList from "pages/_components/DraggableList";
import Header from "pages/_components/header/Header";
import Dropdown from "pages/_components/Dropdown";
import Image from "pages/_components/Image";
import Head from "pages/_components/Head";
import * as Widgets from "pages/desktop/widgets";
import I18n from "pages/_components/I18n";
import Campaigns from "pages/campaigns/Campaigns";
import Container from "pages/_components/Container";
import GeneralMsg from "pages/_components/GeneralMsg";
import Chatbot from "pages/_components/chatbot/Chatbot";
import BiometricIdentification from "./widgets/BiometricIdentification";

import EconomicGroups from "./widgets/economicGroup/EconomicGroups";

class Desktop extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        layout: arrayOf(
            shape({
                column: number,
                id: string,
                row: number,
                uri: string,
            }),
        ).isRequired,
        isDesktop: bool,
        availableWidgets: arrayOf(
            shape({
                column: number,
                id: string,
                row: number,
                uri: string,
            }),
        ).isRequired,
        isEditable: bool,
        sessionFetching: bool,
        desktopFetching: bool,
        campaigns: arrayOf(
            shape({
                clickable: bool,
                contentType: string,
                creationDate: string,
                creator: string,
                dismissable: bool,
                endDate: string,
                endDateAsString: string,
                expired: bool,
                idCampaign: number,
                image: string,
                imageList: arrayOf(
                    shape({
                        content: string,
                        creationDate: string,
                        fileName: string,
                        idCampaign: number,
                        idImage: number,
                        imageSize: objectOf(oneOfType([number, string])).isRequired,
                    }),
                ).isRequired,
                name: string,
                priority: number,
                length: number,
                startDate: string,
                startDateAsString: string,
                suspended: bool,
                url: string,
            }),
        ).isRequired,
        activeEnvironment: shape({ type: string }).isRequired,
        displayCampaigns: bool,
        enabledAssistant: bool.isRequired,
    };

    static defaultProps = {
        isDesktop: null,
        isEditable: null,
        sessionFetching: null,
        desktopFetching: null,
        displayCampaigns: null,
    };

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(actions.loadLayoutRequest());
    }

    getColumns = (layout) =>
        layout.reduce((obj, item) => {
            const columnValue = obj[item.column] || [];

            return { ...obj, [item.column]: [...columnValue, item] };
        }, {});

    handleClick = (item) => {
        const { dispatch, layout } = this.props;
        const { column, row } = item;

        dispatch(actions.deleteWidget(layout.findIndex((widget) => widget.column === column && widget.row === row)));
    };

    handleIsEditableChange = () => {
        const { dispatch } = this.props;
        dispatch(actions.toggleIsEditable());
    };

    handleItemsPositionChange = (items) => {
        const { dispatch } = this.props;

        dispatch(actions.setLayout(items));
        dispatch(actions.saveLayoutRequest());
    };

    renderHeader = () => {
        const {
            isDesktop,
            dispatch,
            availableWidgets,
            isEditable,
            activeEnvironment: { type },
        } = this.props;
        const hasAvailableWidgets = availableWidgets.length > 0;
        const title = "menu.desktop";

        if (!isDesktop) {
            return <Head title={title} />;
        }

        return (
            <Header>
                <div className="toolbar-item view-title" tabIndex="0">
                    <I18n id={title} component="h1" componentProps={{ className: "visually-hidden" }} />
                </div>
                {type !== CORPORATE_GROUP_ENVIRONMENT_TYPE && (
                    <div className="toolbar-item desktop-edit toolbar-item--fixed">
                        {!isEditable ? (
                            <Button
                                image="images/editDashboard.svg"
                                label="desktop.editLayout.edit.label"
                                onClick={() => this.handleIsEditableChange()}
                                bsStyle="link"
                                id="editDashboardBtn"
                            />
                        ) : (
                            <div className="col-lg-12 col-md-12">
                                <div className="row">
                                    {hasAvailableWidgets ? (
                                        <Dropdown
                                            enabled={isEditable}
                                            label="desktop.selectWidget"
                                            buttonClass="btn btn-select">
                                            {availableWidgets.map((widget, index) => (
                                                // eslint-disable-next-line jsx-a11y/click-events-have-key-events
                                                <Button
                                                    key={widget.id}
                                                    onClick={() => dispatch(actions.addWidget(index))}
                                                    label={`list.addWidget.${widget.id}`}
                                                    className="dropdown__item-btn"
                                                    bsStyle="link"
                                                />
                                            ))}
                                        </Dropdown>
                                    ) : (
                                        <I18n
                                            id="desktop.widgets.empty"
                                            componentProps={{ className: "desktop-edit-empty-message" }}
                                        />
                                    )}
                                    <Button
                                        style={{ marginLeft: "1rem" }}
                                        image="images/cross.svg"
                                        className="btn-only-icon btn-circle"
                                        label="desktop.editLayout.finish.label"
                                        defaultLabelText="Terminar edición"
                                        onClick={() => this.handleIsEditableChange()}
                                    />
                                </div>
                            </div>
                        )}
                    </div>
                )}
            </Header>
        );
    };

    renderItem = (item, { draggableItemProps }) => {
        const { isEditable, isDesktop } = this.props;
        const Widget = Widgets[stringUtils.capitalizeFirstLetter(item.id)];

        const buttonActionDesc = `${i18n.get("global.close")} ${i18n.get("global.widget")}, ${i18n.get(
            `list.addWidget.${item.id}`,
        )}`;

        return (
            <Widget
                className={classNames({ "draggable-list__item": isEditable })}
                closeButton={
                    isEditable && (
                        <Button className="btn-outline widget-close-button" onClick={() => this.handleClick(item)}>
                            <Image src="images/cross.svg" />
                            <span className="visually-hidden">{buttonActionDesc}</span>
                        </Button>
                    )
                }
                draggableItemProps={draggableItemProps}
                isEditable={isEditable}
                isDesktop={isDesktop}
            />
        );
    };

    render() {
        const {
            isEditable,
            activeEnvironment: { type },
            campaigns,
            sessionFetching,
            desktopFetching,
            layout,
            isDesktop,
            displayCampaigns,
            enabledAssistant,
        } = this.props;

        return (
            <>
                <Notification scopeToShow="desktop" />

                {enabledAssistant && <Chatbot />}

                {this.renderHeader()}
                <MainContainer showLoader={sessionFetching || desktopFetching}>
                    <div className="above-the-fold">
                        {displayCampaigns && campaigns && (
                            <Container className="container--layout" gridClassName="container-fluid">
                                <Campaigns section="desktop-header" />
                            </Container>
                        )}

                        {type === CORPORATE_GROUP_ENVIRONMENT_TYPE && <EconomicGroups />}

                        {layout.length > 0 && (
                            <Container className="container--layout flex-grow" gridClassName="container-fluid">
                                <Col sm={12} className="col">
                                    <DraggableList
                                        columns={this.getColumns(layout)}
                                        isDragEnabled={isEditable}
                                        itemRenderer={this.renderItem}
                                        onItemsPositionChange={this.handleItemsPositionChange}
                                    />
                                    <BiometricIdentification />
                                </Col>
                            </Container>
                        )}

                        {layout.length === 0 && !isEditable && type !== CORPORATE_GROUP_ENVIRONMENT_TYPE && (
                            <GeneralMsg
                                imagePath="images/coloredIcons/desktop.svg"
                                title={<I18n id="widgets.list.empty.title" />}
                                description={
                                    isDesktop ? (
                                        <>
                                            <I18n id="widgets.list.empty.description" />
                                            <a href="#editDashboardBtn">
                                                <I18n id="desktop.editLayout.edit.label" />
                                            </a>
                                        </>
                                    ) : null
                                }
                            />
                        )}
                    </div>
                </MainContainer>
            </>
        );
    }
}

const mapStateToProps = (state) => ({
    availableWidgets: desktopSelectors.getAvailableWidgets(state),
    layout: desktopSelectors.getLayout(state),
    isEditable: desktopSelectors.getIsEditale(state),
    campaigns: campaignsSelectors.getCampaigns(state),
    sessionFetching: sessionSelectors.isFetching(state),
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    enabledAssistant: sessionSelectors.getEnabledAssistant(state),
    desktopFetching: desktopSelectors.isFetching(state),
    displayCampaigns: loginSelectors.getDisplayCampaigns(state),
});

export default connect(mapStateToProps)(Desktop);
