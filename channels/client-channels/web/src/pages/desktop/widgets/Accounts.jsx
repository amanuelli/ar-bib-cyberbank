import React, { Component } from "react";
import { shape, node } from "prop-types";

import * as i18nUtils from "util/i18n";

import AccountsListItem from "pages/accounts/ListItem";
import WidgetList from "pages/desktop/widgets/WidgetList";
import WidgetHeader from "./_components/WidgetHeader";

class Accounts extends Component {
    static propTypes = {
        closeButton: node,
        draggableItemProps: shape({}).isRequired,
    };

    static defaultProps = {
        closeButton: null,
    };

    render() {
        const { closeButton, draggableItemProps } = this.props;
        const uiAutomationProp = { "data-ui-automation": "widgetAccounts" };

        return (
            <WidgetList item={AccountsListItem} keyExtractor={(item) => item.idProduct} name="accounts">
                {(list) => (
                    <div
                        role="button"
                        className="widget"
                        {...uiAutomationProp}
                        {...draggableItemProps}
                        aria-roledescription={i18nUtils.get("desktop.widgets.message.roleDescription")}>
                        <WidgetHeader title={i18nUtils.get("menu.accounts")} action={closeButton} />
                        {list}
                        <div className="overlay" />
                    </div>
                )}
            </WidgetList>
        );
    }
}

export default Accounts;
