import React, { Component } from "react";
import { shape, node } from "prop-types";

import * as i18nUtils from "util/i18n";

import CreditCardsListItem from "pages/creditCards/ListItem";
import WidgetList from "pages/desktop/widgets/WidgetList";
import WidgetHeader from "./_components/WidgetHeader";

class CreditCards extends Component {
    static propTypes = {
        closeButton: node,
        draggableItemProps: shape({}).isRequired,
    };

    static defaultProps = {
        closeButton: null,
    };

    render() {
        const uiAutomationProp = { "data-ui-automation": "widgetCreditCards" };

        const { closeButton, draggableItemProps } = this.props;

        return (
            <WidgetList item={CreditCardsListItem} keyExtractor={(item) => item.idProduct} name="creditCards">
                {(list) => (
                    <div
                        role="button"
                        className="widget"
                        {...uiAutomationProp}
                        {...draggableItemProps}
                        aria-roledescription={i18nUtils.get("desktop.widgets.message.roleDescription")}>
                        <WidgetHeader title={i18nUtils.get("menu.creditcards")} action={closeButton} />
                        {list}
                        <div className="overlay" />
                    </div>
                )}
            </WidgetList>
        );
    }
}

export default CreditCards;
