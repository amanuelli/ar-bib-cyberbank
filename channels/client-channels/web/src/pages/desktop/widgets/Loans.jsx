import React, { Component } from "react";
import { shape, node } from "prop-types";

import * as i18nUtils from "util/i18n";

import LoansListItem from "pages/loans/ListItem";
import WidgetList from "pages/desktop/widgets/WidgetList";
import WidgetHeader from "./_components/WidgetHeader";

class Loans extends Component {
    static propTypes = {
        closeButton: node,
        draggableItemProps: shape({}).isRequired,
    };

    static defaultProps = {
        closeButton: null,
    };

    render() {
        const { closeButton, draggableItemProps } = this.props;
        const uiAutomationProp = { "data-ui-automation": "widgetLoans" };

        return (
            <WidgetList item={LoansListItem} keyExtractor={(item) => item.idProduct} name="loans">
                {(list) => (
                    <div
                        role="button"
                        className="widget"
                        {...uiAutomationProp}
                        {...draggableItemProps}
                        aria-roledescription={i18nUtils.get("desktop.widgets.message.roleDescription")}>
                        <WidgetHeader title={i18nUtils.get("menu.loans")} action={closeButton} />
                        {list}
                        <div className="overlay" />
                    </div>
                )}
            </WidgetList>
        );
    }
}

export default Loans;
