import React, { Component } from "react";
import { connect } from "react-redux";
import { shape, node, bool } from "prop-types";

import * as i18nUtils from "util/i18n";
import { selectors } from "reducers/widgets";

import ScheduledTransactionsListItem from "pages/desktop/widgets/_components/ScheduledTransactionsListItem";
import WidgetList from "pages/desktop/widgets/WidgetList";
import WidgetLoading from "pages/_components/WidgetLoading";
import NoProduct from "pages/desktop/widgets/_components/NoProduct";
import WidgetHeader from "./_components/WidgetHeader";

const WIDGET_NAME = "scheduledTransactions";

class ScheduledTransactions extends Component {
    static propTypes = {
        closeButton: node,
        isFetching: bool,
        draggableItemProps: shape({}).isRequired,
        isEditable: bool,
    };

    static defaultProps = {
        closeButton: null,
        isFetching: false,
        isEditable: false,
    };

    renderList = (list) => {
        const { isFetching, isEditable } = this.props;

        let scheduledTransactions = [];
        if (list.length) {
            scheduledTransactions = list.map(({ transaction, transactionAmounts }) => {
                const [currency, quantity] = Object.entries(transactionAmounts);

                return {
                    ...transaction,
                    currency,
                    quantity,
                };
            });
        }

        return (
            <WidgetLoading loading={!list.length && isFetching}>
                {list.length ? (
                    <div className="table" aria-hidden={isEditable}>
                        <div className="table-body">
                            {scheduledTransactions.map((item) => (
                                <ScheduledTransactionsListItem key={item.idTransaction} {...item} />
                            ))}
                        </div>
                    </div>
                ) : (
                    !isFetching && (
                        <div aria-hidden={isEditable}>
                            <div className="widget">
                                <NoProduct
                                    text={`desktop.widgets.${WIDGET_NAME}.empty`}
                                    imagePath="images/coloredIcons/transfer.svg"
                                />
                            </div>
                        </div>
                    )
                )}
            </WidgetLoading>
        );
    };

    render() {
        const { closeButton, draggableItemProps } = this.props;
        const uiAutomationProp = { "data-ui-automation": "widgetScheduledTransactions" };

        return (
            <WidgetList name={WIDGET_NAME} shouldMapList={false}>
                {(list) => (
                    <div
                        role="button"
                        className="widget"
                        {...uiAutomationProp}
                        {...draggableItemProps}
                        aria-roledescription={i18nUtils.get("desktop.widgets.message.roleDescription")}>
                        <div>
                            <WidgetHeader
                                title={i18nUtils.get("desktop.widgets.scheduledTransactions.title")}
                                action={closeButton}
                            />
                            {this.renderList(list)}
                            <div className="overlay" />
                        </div>
                    </div>
                )}
            </WidgetList>
        );
    }
}

const mapStateToProps = (state) => {
    const { isFetching } = selectors.getWidget(state, WIDGET_NAME);

    return { isFetching };
};

export default connect(mapStateToProps)(ScheduledTransactions);
