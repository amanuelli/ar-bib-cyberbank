export { default as Accounts } from "pages/desktop/widgets/Accounts";
export { default as CreditCards } from "pages/desktop/widgets/CreditCards";
export { default as ExchangeRates } from "pages/desktop/widgets/ExchangeRates";
export { default as Loans } from "pages/desktop/widgets/Loans";
export { default as Notifications } from "pages/desktop/widgets/Notifications";
export { default as Portfolio } from "pages/desktop/widgets/Portfolio";
export { default as PendingTransactions } from "pages/desktop/widgets/PendingTransactions";
export { default as ScheduledTransactions } from "pages/desktop/widgets/ScheduledTransactions";
