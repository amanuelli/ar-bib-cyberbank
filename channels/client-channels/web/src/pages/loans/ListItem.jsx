import React, { Component } from "react";
import queryString from "query-string";
import { string, number as numberType, bool } from "prop-types";

import * as i18nUtils from "util/i18n";
import * as dateUtils from "util/date";

import ListItem from "pages/_components/listItem/ListItem";

class LoansListItem extends Component {
    static propTypes = {
        number: string.isRequired,
        productAlias: string,
        idProduct: string,
        productType: string.isRequired,
        paymentAmount: numberType.isRequired,
        nextDueDate: string.isRequired,
        numberOfFees: numberType.isRequired,
        numberOfPaidFees: numberType.isRequired,
        showPayment: bool,
        showLink: bool,
    };

    static defaultProps = {
        productAlias: null,
        idProduct: null,
        showPayment: false,
        showLink: true,
    };

    productType = () => {
        const { productType } = this.props;
        if (productType === "PA") {
            return i18nUtils.get("loans.list.item.title.pa");
        }

        return i18nUtils.get("loans.list.item.title.pi");
    };

    render() {
        const {
            productAlias,
            number,
            paymentAmount,
            idProduct,
            nextDueDate,
            numberOfFees,
            numberOfPaidFees,
            showPayment,
            showLink,
            ...props
        } = this.props;
        const productName = productAlias || number;
        const reference = `${i18nUtils.get("global.number")} ${number}`;
        const isExpired = dateUtils.isDateLessThanToday(nextDueDate);
        const productTypeTitle = i18nUtils.get("desktop.widgets.loan");

        const finalPath =
            numberOfPaidFees < numberOfFees && showPayment
                ? {
                      pathname: "/form/payLoan",
                      search: queryString.stringify({ loan: idProduct }),
                  }
                : {
                      pathname: `/loans/${idProduct}`,
                  };

        return (
            <ListItem
                {...props}
                idProduct={idProduct}
                productTypeTitle={productTypeTitle}
                title={this.productType()}
                name={productName}
                reference={reference}
                expiredText={i18nUtils.get("loans.list.item.expired")}
                expirationText={i18nUtils.get("loans.list.item.expiration")}
                isExpired={isExpired}
                amount={paymentAmount}
                amountLabel={nextDueDate ? i18nUtils.get("loans.nextDue") : null}
                expirationDate={nextDueDate}
                path={showLink ? finalPath : null}
            />
        );
    }
}

export default LoansListItem;
