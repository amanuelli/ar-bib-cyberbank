import React, { Component, Fragment } from "react";
import { oneOfType, array, string, func, bool } from "prop-types";
import { connect } from "react-redux";
import { push } from "react-router-redux";
import classNames from "classnames";
import { Link } from "react-router-dom";
import Col from "react-bootstrap/lib/Col";

import { selectors as sessionSelectors } from "reducers/session";
import { actions as loansActions, selectors as loansSelectors } from "reducers/loans";

import Head from "pages/_components/Head";
import Notification from "pages/_components/Notification";
import MainContainer from "pages/_components/MainContainer";
import Loan from "pages/loans/_components/Loan";
import GeneralMsg from "pages/_components/GeneralMsg";
import Container from "pages/_components/Container";
import I18n from "pages/_components/I18n";

import * as configUtil from "util/config";

class ProductList extends Component {
    static propTypes = {
        isRequestAvailable: oneOfType([string, bool]),
        dispatch: func.isRequired,
        isDesktop: oneOfType([string, bool]),
        isMobile: oneOfType([string, bool]),
        fetching: oneOfType([string, bool]),
        loans: oneOfType([array]),
    };

    static defaultProps = {
        isRequestAvailable: false,
        isDesktop: false,
        isMobile: false,
        fetching: false,
        loans: [],
    };

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(loansActions.listLoans());
    }

    handleRequestLoanClick = () => {
        const { dispatch } = this.props;
        dispatch(push("/form/requestLoan"));
    };

    renderHeader = () => {
        const { isRequestAvailable, isMobile } = this.props;

        if (!isRequestAvailable) {
            return <Head title="menu.loans" />;
        }
        if (isMobile) {
            return <Head title="menu.loans" addLinkToLabel="loan.new" />;
        }

        return <Head title="menu.loans" addLinkToLabel="loan.new" addLinkTo="/form/requestLoan/" />;
    };

    btnHandlerOnClick = () => {
        const { dispatch } = this.props;
        dispatch(push("/form/requestLoan/"));
    };

    render() {
        const { loans, fetching, isRequestAvailable, isDesktop } = this.props;
        const maxBoxDisplay = configUtil.getInteger("product.list.maxBoxDisplay", 5);
        const productAsCard = loans.length < maxBoxDisplay;
        const isLoading = fetching && !loans.length;

        return (
            <Fragment>
                <Notification scopeToShow="loans" />
                {!isLoading && this.renderHeader()}
                <MainContainer showLoader={isLoading}>
                    <div className="above-the-fold">
                        {loans.length ? (
                            <Fragment>
                                <Container className="container--layout flex-grow">
                                    <Col className="col col-12" sm={12} md={9} lg={9} xl={6}>
                                        <div className="table table--products">
                                            <div
                                                role="menu"
                                                className={classNames("table-body", {
                                                    "table-body--grid": productAsCard,
                                                })}>
                                                {loans &&
                                                    Object.entries(loans).map(([id, loan]) => (
                                                        <Loan
                                                            loan={loan}
                                                            key={`${id} - ${loan.number}`}
                                                            productAsCard={productAsCard}
                                                        />
                                                    ))}
                                            </div>
                                        </div>
                                    </Col>
                                </Container>

                                {!isDesktop && isRequestAvailable && (
                                    <Container className="container--layout align-items-center">
                                        <Col className="col col-12" sm={12} md={9} lg={9} xl={6}>
                                            <Link className="btn btn-outline btn-block" to="/form/requestLoan">
                                                <I18n id="loan.new" />
                                            </Link>
                                        </Col>
                                    </Container>
                                )}
                            </Fragment>
                        ) : (
                            <GeneralMsg
                                imagePath="images/coloredIcons/loans.svg"
                                description={<I18n id="loans.list.empty" />}
                                callToAction={
                                    isRequestAvailable && (
                                        <Link className="btn btn-primary btn-block" to="/form/requestLoan">
                                            <I18n id="loan.new" />
                                        </Link>
                                    )
                                }
                            />
                        )}
                    </div>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    loans: loansSelectors.getLoans(state),
    fetching: loansSelectors.getFetching(state),
    isRequestAvailable: sessionSelectors
        .getActiveEnvironmentForms(state, "loans")
        .find(({ idForm }) => idForm === "requestLoan"),
});

export default connect(mapStateToProps)(ProductList);
