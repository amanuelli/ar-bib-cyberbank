import React, { Component, Fragment } from "react";
import Col from "react-bootstrap/lib/Col";
import { connect } from "react-redux";
import { compose } from "redux";
import { Field, Form, withFormik } from "formik";
import * as Yup from "yup";

import { actions as loansActions, selectors as loansSelectors } from "reducers/loans";
import { actions as productsActions } from "reducers/products";

import Button from "pages/_components/Button";
import Head from "pages/_components/Head";
import Notification from "pages/_components/Notification";
import MainContainer from "pages/_components/MainContainer";
import TextField from "pages/_components/fields/TextField";
import Container from "pages/_components/Container";

const FORM_ID = "loans.details.alias";

class SetAlias extends Component {
    componentDidMount() {
        const { dispatch, ...rest } = this.props;

        dispatch(loansActions.readLoan(rest.match.params.id));
        dispatch(loansActions.closeOptions());
    }

    render() {
        const { isSubmitting, history } = this.props;

        return (
            <Fragment>
                <Notification scopeToShow="loans/setAlias" />

                <Head onClose={history.goBack} title="loans.alias.setAlias.title" />
                <MainContainer>
                    <Form className="above-the-fold">
                        <Container className="align-items-center flex-grow container--layout">
                            <Col sm={12} md={9} lg={6} xl={6} className="col">
                                <Field
                                    autoFocus
                                    component={TextField}
                                    hidePlaceholder
                                    idForm={FORM_ID}
                                    name="setAlias"
                                    type="text"
                                />
                            </Col>
                        </Container>
                        <Container className="align-items-center container--layout">
                            <Col sm={12} md={9} lg={6} xl={6} className="col">
                                <Button
                                    type="submit"
                                    bsStyle="primary"
                                    label="loans.alias.setAlias.save"
                                    loading={isSubmitting}
                                />
                            </Col>
                        </Container>
                    </Form>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    loan: loansSelectors.getSelectedLoan(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        enableReinitialize: true,
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: (props) => ({
            setAlias: props.loan ? props.loan.productAlias : "",
            productId: props.match.params.id,
        }),
        validationSchema: (props) =>
            Yup.object().shape({
                setAlias: Yup.string(),
            }),
        handleSubmit: ({ productId, setAlias }, formikBag) => {
            formikBag.props.dispatch(productsActions.changeProductAlias(setAlias, productId, true), formikBag);
        },
    }),
)(SetAlias);
