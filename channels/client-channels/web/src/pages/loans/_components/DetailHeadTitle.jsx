import React, { Component, Fragment } from "react";
import { string, func, bool, int, shape } from "prop-types";
import I18n from "pages/_components/I18n";
import { actions as productsActions } from "reducers/products";
import EditableLabel from "pages/_components/fields/EditableLabel";

class DetailHeadTitle extends Component {
    static propTypes = {
        isDesktop: bool.isRequired,
        dispatch: func.isRequired,
        loan: shape({
            idEnvironment: int,
            idProduct: string,
            productType: string.isRequired,
            label: string,
            shortLabel: string,
            extraInfo: string,
            number: string.isRequired,
            nextDueDate: string,
            constitutedDate: string,
            currency: string.isRequired,
            extraInfoMarked: string,
        }).isRequired,
    };

    constructor(props) {
        super(props);
        this.titleRef = React.createRef();
    }

    componentDidMount() {
        if (this.titleRef.current) {
            this.titleRef.current.focus();
        }
    }

    componentDidUpdate() {
        if (this.titleRef.current) {
            this.titleRef.current.focus();
        }
    }

    renderProductType = (number, productType) => (
        <Fragment>
            <span>
                <I18n id={`loans.productType.${productType}`} />
            </span>
            <span>{number}</span>
        </Fragment>
    );

    saveAlias = (alias) => {
        const { loan, dispatch } = this.props;
        dispatch(productsActions.changeProductAlias(alias, loan.idProduct));
    };

    render() {
        const { loan, isDesktop } = this.props;
        const { number, productAlias, productType, currency } = loan;

        return (
            <Fragment>
                <div className="toolbar-item view-title">
                    <div className="visually-hidden" ref={this.titleRef} tabIndex="0">
                        <span>{`${productType} ${currency}, ${productAlias || number}`}</span>
                    </div>
                    {(isDesktop && (
                        <EditableLabel isDesktop={isDesktop} onSave={this.saveAlias} value={productAlias || number}>
                            <h1 className="data-name product-title">{productAlias || number}</h1>
                        </EditableLabel>
                    )) || <h1 className="ellipsis">{productAlias || number}</h1>}
                </div>
                <div className="toolbar-item">
                    <mark className="product-name">{this.renderProductType(number, productType)}</mark>
                </div>
            </Fragment>
        );
    }
}

export default DetailHeadTitle;
