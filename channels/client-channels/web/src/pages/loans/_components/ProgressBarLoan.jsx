import React, { Component, Fragment } from "react";
import { ProgressBar } from "react-bootstrap";
import { number } from "prop-types";
import * as i18nUtils from "util/i18n";

class ProgressBarLoan extends Component {
    static propTypes = {
        paidPercentage: number,
    };

    static defaultProps = {
        paidPercentage: 0,
    };

    render() {
        const { paidPercentage } = this.props;
        const classWrapper = paidPercentage > 50 ? "progress-label" : "progress-label progress-below-half";
        return (
            <Fragment>
                <div className="progress-wrapper">
                    <ProgressBar now={paidPercentage} />
                    <span className={classWrapper}>
                        <span>{`${i18nUtils.get("forms.payLoan.progressBar")} ${Math.round(paidPercentage)}%`}</span>
                    </span>
                </div>
            </Fragment>
        );
    }
}

export default ProgressBarLoan;
