import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Slider from "react-slick";
import Grid from "react-bootstrap/lib/Grid";
import Row from "react-bootstrap/lib/Row";
import Col from "react-bootstrap/lib/Col";
import { Link } from "react-router-dom";
import { shape, bool } from "prop-types";

import { selectors as sessionSelectors } from "reducers/session";
import I18n from "pages/_components/I18n";
import ProgressBarLoan from "pages/loans/_components/ProgressBarLoan";
import FormattedDate from "pages/_components/FormattedDate";
import FormattedAmount from "pages/_components/FormattedAmount";
import * as i18n from "util/i18n";

class DetailHeadInfo extends Component {
    static propTypes = {
        loan: shape({}).isRequired,
        payLoanPermission: bool.isRequired,
        isDesktop: bool,
    };

    static defaultProps = {
        isDesktop: false,
    };

    render() {
        const { loan, isDesktop, payLoanPermission } = this.props;
        const { nextDueDate, currency, paymentAmount, totalAmount, pendingBalance } = loan;

        const settings = {
            dots: true,
            infinite: false,
            speed: 200,
            slidesToShow: isDesktop ? 3 : 1,
            slidesToScroll: 1,
            arrows: false,
        };

        const loanPendingBalance = (
            <Col sm={12} md={3} className="content-data-item col">
                <span className="data-label">
                    <I18n id="loans.details.statements.pendingTotal" />
                </span>
                <FormattedAmount
                    className="data-amount content-data-strong"
                    currency={currency}
                    quantity={pendingBalance}
                />
                <I18n id="loans.information.requestedAmount" />
                &nbsp;
                <FormattedAmount currency={currency} quantity={totalAmount} />
            </Col>
        );

        const paymentAmountData = (
            <Col sm={12} md={3} className="content-data-item col">
                <span className="data-label">
                    <I18n id="loans.nextDue" />
                </span>
                <b>
                    <FormattedAmount
                        className="data-amount content-data-strong"
                        currency={currency}
                        quantity={paymentAmount}
                    />
                </b>
            </Col>
        );

        const nextDueDateData = (
            <Col sm={12} md={3} className="content-data-item col">
                <span className="data-label">
                    <I18n id="loans.details.nextDueDate" />
                </span>
                <b>
                    <span className="data-amount content-data-strong">
                        <span>
                            <FormattedDate date={nextDueDate} />
                        </span>
                    </span>
                </b>
            </Col>
        );

        const paidFeesData = (
            <Col sm={12} md={3} className="content-data-item col">
                <span className="data-label">
                    <I18n id="loans.statements.filters.paidFees" />
                </span>
                <b>
                    <span className="data-amount content-data-strong">
                        <span>
                            {loan.numberOfPaidFees} <I18n id="loans.details.of" /> {loan.numberOfFees}
                        </span>
                    </span>
                </b>
            </Col>
        );

        const paymentButton = payLoanPermission && (
            <Col sm={12} md={3} className="col content-data-item">
                <Link
                    aria-label={i18n.get("creditCard.pay.a11yLabel")}
                    className="btn btn-primary btn-block"
                    to={`/form/payLoan?loan=${loan.idProduct}`}>
                    <I18n id="loans.details.payLoan" />
                </Link>
            </Col>
        );

        return (
            (isDesktop && (
                <Grid>
                    <Row className="content-data">
                        {loanPendingBalance}
                        {nextDueDateData}
                        {paymentAmountData}
                        {loan.numberOfPaidFees < loan.numberOfFees && paymentButton}
                    </Row>
                    <ProgressBarLoan paidPercentage={loan.paidPercentage} />
                </Grid>
            )) || (
                <Fragment>
                    {loan && (
                        <div className="content-data">
                            <Slider {...settings}>
                                {paymentAmountData}
                                {nextDueDateData}
                                {paidFeesData}
                                {loanPendingBalance}
                            </Slider>
                            <div className="content-data-item">
                                <ProgressBarLoan paidPercentage={loan.paidPercentage} />
                            </div>

                            <div className="content-data-item">{paymentButton}</div>
                        </div>
                    )}
                </Fragment>
            )
        );
    }
}

const mapStateToProps = (state) => ({
    payLoanPermission: sessionSelectors.hasPermissions(state, ["payLoan"]),
});

export default connect(mapStateToProps)(DetailHeadInfo);
