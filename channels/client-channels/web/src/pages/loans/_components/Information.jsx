import React, { Component, Fragment } from "react";
import { Col, Row } from "react-bootstrap";
import { compose } from "redux";
import { connect } from "react-redux";
import { number, shape, string, func, bool } from "prop-types";

import I18n from "pages/_components/I18n";
import FormattedDate from "pages/_components/FormattedDate";
import FormattedAmount from "pages/_components/FormattedAmount";
import { resizableRoute } from "pages/_components/Resizable";
import { selectors as loansSelectors } from "reducers/loans";
import PaperlessSwitch from "pages/_components/fields/PaperlessSwitch";

class Information extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        loan: shape({
            constitutedDate: string,
            currency: string,
            interestRate: number,
            interests: number,
            pendingBalance: number,
            productType: string,
            totalAmount: number,
            idProduct: string,
            paperless: bool,
        }).isRequired,
    };

    state = {
        paperless: this.props.loan.paperless,
    };

    onClickHandler = () => {
        const { paperless } = this.state;
        this.setState({ paperless: !paperless });
    };

    renderContent = (
        { productType, currency, constitutedDate, interests, interestRate, idProduct, paperless },
        dispatch,
    ) => (
        <Row>
            <Col sm={12} md={3} lg={3} className="col col-12">
                <div className="data-wrapper">
                    <I18n componentProps={{ className: "data-label" }} id="loans.information.from" />
                    <FormattedDate date={constitutedDate} />
                </div>
            </Col>
            {productType === "PA" && (
                <Fragment>
                    <Col sm={12} md={3} lg={3} className="col col-12">
                        <div className="data-wrapper">
                            <I18n componentProps={{ className: "data-label" }} id="loans.information.interest" />
                            <FormattedAmount currency={currency} quantity={interests} />
                        </div>
                    </Col>
                </Fragment>
            )}

            <Col sm={12} md={3} lg={3} className="col col-12">
                <div className="data-wrapper">
                    <I18n componentProps={{ className: "data-label" }} id="loans.information.interestRate" />{" "}
                    <span className="data-numeric">{`${interestRate} %`}</span>
                </div>
            </Col>

            <Col sm={12} md={3} lg={3} className="col col-12">
                <PaperlessSwitch
                    name="loan"
                    idProduct={idProduct}
                    paperless={paperless}
                    onClick={this.onClickHandler}
                    dispatch={dispatch}
                />
            </Col>
        </Row>
    );

    render() {
        const { loan, dispatch } = this.props;
        return this.renderContent(loan, dispatch);
    }
}

const mapStateToProps = (state) => ({
    loan: loansSelectors.getSelectedLoan(state),
});

export default compose(connect(mapStateToProps), resizableRoute)(Information);
