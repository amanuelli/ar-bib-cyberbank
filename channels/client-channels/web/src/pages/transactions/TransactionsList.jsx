import React, { Component, Fragment } from "react";
import { bool, func, instanceOf, number, shape, string } from "prop-types";
import { CORPORATE_GROUP_ENVIRONMENT_TYPE } from "constants.js";

import { connect } from "react-redux";
import classNames from "classnames";
import { actions as transactionsActions, selectors as transactionsSelectors } from "reducers/transactions";
import { selectors as sessionSelectors } from "reducers/session";

import Head from "pages/_components/Head";
import Notification from "pages/_components/Notification";
import Button from "pages/_components/Button";
import List from "pages/transactions/_components/List";
import HiddenFilters from "pages/transactions/_components/HiddenFilters";
import ProductFilters from "pages/_components/ProductFilters";
import MainContainer from "pages/_components/MainContainer";

const LAST_TRANSACTIONS = "last";
const PREVIOUS_MONTH = "previousMonth";
const ACTUAL_MONTH = "actualMonth";

class TransactionsList extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        fetching: bool.isRequired,
        filters: shape({
            dateFrom: instanceOf(Date),
            dateTo: instanceOf(Date),
            minAmount: number,
            maxAmount: number,
            pageNumber: number,
        }),
        isDesktop: bool.isRequired,
        showFilters: bool.isRequired,
        isFirstFetching: bool.isRequired,
        activeEnvironment: shape({ type: string.isRequired }).isRequired,
    };

    static defaultProps = {
        filters: null,
    };

    state = {
        selectedFilter: LAST_TRANSACTIONS,
        defaultFilters: null,
    };

    componentDidMount = () => {
        const { filters, dispatch } = this.props;

        if (filters === null) {
            const dateFrom = new Date();
            dateFrom.setYear(dateFrom.getFullYear() - 10);
            const dateTo = new Date();
            const defaultFilters = {
                dateFrom,
                dateTo,
                pageNumber: 1,
                filter: LAST_TRANSACTIONS,
            };

            this.setState({ defaultFilters });
            dispatch(transactionsActions.loadListRequest(defaultFilters, false, false, true));
        } else {
            dispatch(transactionsActions.loadListRequest(filters, false, false, true));
        }
    };

    handleFilterButtonsClick = (idFilter) => {
        let filters = { filter: idFilter };
        this.setState({ selectedFilter: idFilter });
        switch (idFilter) {
            case LAST_TRANSACTIONS: {
                const dateFrom = new Date();
                dateFrom.setYear(dateFrom.getFullYear() - 1);
                const dateTo = new Date();
                filters = {
                    dateFrom,
                    dateTo,
                    pageNumber: 1,
                    ...filters,
                };
                break;
            }
            case PREVIOUS_MONTH: {
                const dateFrom = new Date();
                dateFrom.setMonth(dateFrom.getMonth() - 1);
                dateFrom.setDate(1);
                const dateTo = new Date();
                dateTo.setDate(0);
                filters = {
                    dateFrom,
                    dateTo,
                    pageNumber: 1,
                    ...filters,
                };
                break;
            }
            case ACTUAL_MONTH: {
                const dateFrom = new Date();
                dateFrom.setDate(1);
                const dateTo = new Date();
                dateTo.setMonth(dateFrom.getMonth() + 1);
                dateTo.setDate(0);
                filters = {
                    dateFrom,
                    dateTo,
                    pageNumber: 1,
                    ...filters,
                };
                break;
            }
            default:
                break;
        }
        const { dispatch } = this.props;
        dispatch(transactionsActions.loadListRequest(filters, false, false, false));
    };

    render() {
        const { fetching, isDesktop, dispatch, isFirstFetching, showFilters, activeEnvironment } = this.props;
        const { defaultFilters, selectedFilter } = this.state;
        const isCorporateGroup = activeEnvironment.type === CORPORATE_GROUP_ENVIRONMENT_TYPE;

        const filters = [
            <Button
                className={classNames("btn btn-outline", {
                    "is-active": selectedFilter === LAST_TRANSACTIONS,
                })}
                key={LAST_TRANSACTIONS}
                label="transactions.list.filter.last"
                onClick={() => this.handleFilterButtonsClick(LAST_TRANSACTIONS)}
            />,
            <Button
                className={classNames("btn btn-outline", {
                    "is-active": selectedFilter === ACTUAL_MONTH,
                })}
                key={ACTUAL_MONTH}
                label="transactions.list.filter.actualMonth"
                onClick={() => this.handleFilterButtonsClick(ACTUAL_MONTH)}
            />,
            <Button
                className={classNames("btn btn-outline", {
                    "is-active": selectedFilter === PREVIOUS_MONTH,
                })}
                key={PREVIOUS_MONTH}
                label="transactions.list.filter.previousMonth"
                onClick={() => this.handleFilterButtonsClick(PREVIOUS_MONTH)}
            />,
        ];

        return (
            <Fragment>
                <Notification scopeToShow="transactions" />
                <Head title="transactions.list.title" />
                <MainContainer showLoader={isFirstFetching} showChildrenWithLoader>
                    <div className="above-the-fold">
                        {isDesktop && (
                            <ProductFilters
                                closeMoreFiltersWhenSiblingClick
                                moreFilters={
                                    <HiddenFilters dispatch={dispatch} isDesktop={isDesktop} fetching={fetching} />
                                }
                                moreFiltersClosedKeyLabel="accounts.movements.filters.more"
                                moreFiltersOpenedKeyLabel="accounts.movements.filters.less"
                                isDesktop={isDesktop}>
                                {showFilters && filters}
                            </ProductFilters>
                        )}
                        <List
                            isDesktop={isDesktop}
                            showSearchButton
                            defaultFilters={defaultFilters}
                            showEnvironment={isCorporateGroup}
                            itemsAreClickeable={!isCorporateGroup}
                        />
                    </div>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    isFirstFetching: transactionsSelectors.getFirstFetch(state),
    showFilters: transactionsSelectors.getShowFilters(state),
    fetching: transactionsSelectors.getFetching(state),
    filters: transactionsSelectors.getFilters(state),
    pageNumber: transactionsSelectors.getPageNumber(state),
    hasMoreData: transactionsSelectors.getHasMoreData(state),
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
});

export default connect(mapStateToProps)(TransactionsList);
