import React, { Component, Fragment } from "react";
import { bool, func, instanceOf, number, shape } from "prop-types";

import { connect } from "react-redux";
import classNames from "classnames";
import { actions as transactionsActions, selectors as transactionsSelectors } from "reducers/transactions";

import Head from "pages/_components/Head";
import Notification from "pages/_components/Notification";
import Button from "pages/_components/Button";
import List from "pages/transactions/_components/List";
import HiddenFilters from "pages/transactions/_components/HiddenFilters";
import ProductFilters from "pages/_components/ProductFilters";
import MainContainer from "pages/_components/MainContainer";

const LAST_TRANSACTIONS = "last";

class PendingTransactionList extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        fetching: bool.isRequired,
        filters: shape({
            dateFrom: instanceOf(Date),
            dateTo: instanceOf(Date),
            minAmount: number,
            maxAmount: number,
            pageNumber: number,
        }),
        isDesktop: bool.isRequired,
        pageNumber: number.isRequired,
        hasMoreData: bool.isRequired,
    };

    static defaultProps = {
        filters: null,
    };

    state = {
        selectedFilter: LAST_TRANSACTIONS,
        defaultFilters: null,
    };

    componentDidMount = () => {
        const { filters, dispatch } = this.props;
        const pendingDispatch = false;
        if (filters === null) {
            const dateFrom = new Date();
            dateFrom.setYear(dateFrom.getFullYear() - 10);
            const dateTo = new Date();
            const defaultFilters = {
                dateFrom,
                dateTo,
                pageNumber: 1,
                filter: LAST_TRANSACTIONS,
            };
            this.setState({ defaultFilters });
            dispatch(transactionsActions.loadListRequest(defaultFilters, true, pendingDispatch));
        } else {
            dispatch(transactionsActions.loadListRequest(filters, true, pendingDispatch));
        }
    };

    handleFilterButtonsClick = (idFilter) => {
        let filters = { filter: idFilter };
        this.setState({ selectedFilter: idFilter });
        switch (idFilter) {
            case LAST_TRANSACTIONS: {
                const dateFrom = new Date();
                dateFrom.setYear(dateFrom.getFullYear() - 1);
                const dateTo = new Date();
                filters = {
                    dateFrom,
                    dateTo,
                    pageNumber: 1,
                    ...filters,
                };
                break;
            }
            case "previousMonth": {
                const dateFrom = new Date();
                dateFrom.setMonth(dateFrom.getMonth() - 1);
                dateFrom.setDate(1);
                const dateTo = new Date();
                dateTo.setDate(0);
                filters = {
                    dateFrom,
                    dateTo,
                    pageNumber: 1,
                    minAmount: 0,
                    ...filters,
                };
                break;
            }
            case "actualMonth": {
                const dateFrom = new Date();
                dateFrom.setDate(1);
                const dateTo = new Date();
                dateTo.setMonth(dateFrom.getMonth() + 1);
                dateTo.setDate(0);
                filters = {
                    dateFrom,
                    dateTo,
                    pageNumber: 1,
                    minAmount: 0,
                    ...filters,
                };
                break;
            }
            default:
                break;
        }
        const { dispatch } = this.props;
        dispatch(transactionsActions.loadListRequest(filters, true, false));
    };

    render() {
        const { fetching, isDesktop, dispatch, pageNumber, hasMoreData, ...rest } = this.props;
        const { defaultFilters, selectedFilter } = this.state;

        const showFilters = !(
            rest.filters &&
            rest.filters.filter === LAST_TRANSACTIONS &&
            !hasMoreData &&
            pageNumber === 1
        );

        const filters = [
            <Button
                className={classNames("btn btn-outline", {
                    "is-active": selectedFilter === LAST_TRANSACTIONS,
                })}
                key={LAST_TRANSACTIONS}
                label="transactions.list.filter.last"
                onClick={() => this.handleFilterButtonsClick(LAST_TRANSACTIONS)}
            />,
            <Button
                className={classNames("btn btn-outline", {
                    "is-active": selectedFilter === "actualMonth",
                })}
                key="actualMonth"
                label="transactions.list.filter.actualMonth"
                onClick={() => this.handleFilterButtonsClick("actualMonth")}
            />,
            <Button
                className={classNames("btn btn-outline", {
                    "is-active": selectedFilter === "previousMonth",
                })}
                key="previousMonth"
                label="transactions.list.filter.previousMonth"
                onClick={() => this.handleFilterButtonsClick("previousMonth")}
            />,
        ];

        return (
            <Fragment>
                <Notification scopeToShow="transactions" />
                <Head title="pendingDispatch.list.title" />
                <MainContainer showLoader={fetching} showChildrenWithLoader>
                    <div className="above-the-fold">
                        {isDesktop && (
                            <ProductFilters
                                closeMoreFiltersWhenSiblingClick
                                moreFilters={
                                    <HiddenFilters
                                        dispatch={dispatch}
                                        isDesktop={isDesktop}
                                        fetching={fetching}
                                        onlyPendings
                                    />
                                }
                                moreFiltersClosedKeyLabel="accounts.movements.filters.more"
                                moreFiltersOpenedKeyLabel="accounts.movements.filters.less"
                                isDesktop={isDesktop}>
                                {showFilters && filters}
                            </ProductFilters>
                        )}
                        <List
                            isDesktop={isDesktop}
                            showSearchButton
                            defaultFilters={defaultFilters}
                            onlyPendings
                            pendingDispatch={false}
                        />
                    </div>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    fetching: transactionsSelectors.getFetching(state),
    filters: transactionsSelectors.getFilters(state),
    pageNumber: transactionsSelectors.getPageNumber(state),
    hasMoreData: transactionsSelectors.getHasMoreData(state),
});

export default connect(mapStateToProps)(PendingTransactionList);
