import React, { Component, createElement } from "react";
import { Col, Row } from "react-bootstrap";
import Select from "react-select";
import { bool, func, shape, string } from "prop-types";
import { connect } from "react-redux";
import { CORPORATE_GROUP_ENVIRONMENT_TYPE } from "constants.js";

import I18n from "pages/_components/I18n";
import AmountFilter from "pages/transactions/_components/AmountFilter";
import PeriodFilter from "pages/transactions/_components/PeriodFilter";
import EnvironmentFilter from "pages/transactions/_components/EnvironmentFilter";
import { selectors as sessionSelectors } from "reducers/session";

import * as i18n from "util/i18n";

const components = {
    period: PeriodFilter,
    amount: AmountFilter,
    environment: EnvironmentFilter,
};

class HiddenFilters extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        fetching: bool,
        onlyPendings: bool,
        pendingDispatch: bool,
        activeEnvironment: shape({ type: string.isRequired }).isRequired,
    };

    static defaultProps = {
        fetching: false,
        onlyPendings: false,
        pendingDispatch: false,
    };

    state = {
        selectedFilter: "period",
    };

    handleChange = (selectedOption) => {
        this.setState({ selectedFilter: selectedOption.value });
    };

    renderFilter = () => {
        const { selectedFilter } = this.state;
        const { dispatch, fetching, onlyPendings, pendingDispatch } = this.props;
        let props = { dispatch, isDesktop: true, fetching, onlyPendings, pendingDispatch };

        if (selectedFilter === "amount") {
            props = { ...props };
        }

        return selectedFilter && createElement(components[selectedFilter], props);
    };

    render() {
        const { selectedFilter } = this.state;
        const { activeEnvironment } = this.props;

        let options = [
            {
                value: "period",
                label: i18n.get("accounts.movements.filters.searchBy.period"),
            },
            {
                value: "amount",
                label: i18n.get("accounts.movements.filters.searchBy.amount"),
            },
        ];

        if (activeEnvironment.type === CORPORATE_GROUP_ENVIRONMENT_TYPE) {
            options = options.concat({
                value: "environment",
                label: i18n.get("accounts.movements.filters.searchBy.client"),
            });
        }

        return (
            <div>
                <Row className="filters">
                    <Col className="col" sm={12} md={3}>
                        <div className="form-group form-group--select">
                            <div className="form-group-text">
                                <label htmlFor="searchBy" className="control-label">
                                    <I18n id="accounts.movements.filters.searchBy" />
                                </label>
                            </div>
                            <div className="input-group ">
                                <div style={{ flex: 1 }}>
                                    <Select
                                        name="searchBy"
                                        clearable={false}
                                        searchable={false}
                                        onChange={this.handleChange}
                                        options={options}
                                        value={selectedFilter}
                                        optionClassName="needsclick"
                                    />
                                </div>
                            </div>
                        </div>
                    </Col>
                    <Col className="col" sm={12} md={9}>
                        {this.renderFilter()}
                    </Col>
                </Row>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
});

export default connect(mapStateToProps)(HiddenFilters);
