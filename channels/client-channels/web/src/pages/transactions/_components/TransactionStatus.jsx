import React, { Component } from "react";

import classNames from "classnames";
import { string, oneOf, bool } from "prop-types";

import * as utils from "util/general";
import Image from "pages/_components/Image";
import I18n from "pages/_components/I18n";

class TransactionStatus extends Component {
    static propTypes = {
        idTransactionStatus: string.isRequired,
        iconFidelity: oneOf(["transaction", "ticket"]),
        showLabel: bool,
        showIcon: bool,
    };

    static defaultProps = {
        iconFidelity: "transaction",
        showLabel: false,
        showIcon: true,
    };

    render() {
        const { idTransactionStatus, iconFidelity, showIcon, showLabel } = this.props;
        return (
            <div className="bubble-wrapper">
                <div
                    className={classNames("text-label", {
                        " danger-label": idTransactionStatus === "FAILED" || idTransactionStatus === "CANCELLED",
                        " warning-label":
                            idTransactionStatus === "RETURNED" ||
                            idTransactionStatus === "PENDING" ||
                            idTransactionStatus === "PROCESSING" ||
                            idTransactionStatus === "DRAFT",
                        " success-label": idTransactionStatus === "FINISHED" || idTransactionStatus === "SCHEDULED",
                    })}>
                    {showIcon && (
                        <Image
                            src={`images/${iconFidelity}StatusIcons/${utils.getTransactionStatusIcon(
                                idTransactionStatus,
                            )}.svg`}
                            className={classNames("svg-icon", {
                                "svg-icon-big": iconFidelity === "ticket",
                            })}
                        />
                    )}

                    {showLabel && <I18n id={`transaction.status.${idTransactionStatus}`} />}
                </div>
            </div>
        );
    }
}

export default TransactionStatus;
