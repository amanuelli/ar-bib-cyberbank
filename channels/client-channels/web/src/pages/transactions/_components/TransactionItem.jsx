import React, { Component, Fragment } from "react";
import { bool, func, shape, string } from "prop-types";
import { connect } from "react-redux";
import { actions as transactionsActions, selectors as transactionsSelectors } from "reducers/transactions";
import { selectors as sessionSelectors } from "reducers/session";
import { CORPORATE_GROUP_ENVIRONMENT_TYPE } from "constants.js";

import I18n from "pages/_components/I18n";
import FormattedDate from "pages/_components/FormattedDate";
import FormattedAmount from "pages/_components/FormattedAmount";
import Button from "pages/_components/Button";
import ChevromRight from "pages/_components/listItem/ChevromRight";
import Image from "pages/_components/Image";

import TransactionStatus from "pages/transactions/_components/TransactionStatus";

class TransactionsItem extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        isDesktop: bool.isRequired,
        isDeletingDraft: bool.isRequired,
        transaction: shape({
            cancelEnabled: string.isRequired,
            transaction: shape({
                idTransaction: string,
                creationDateTime: string,
                submitDateTime: string,
                activityName: string,
                idTransactionStatus: string,
            }),
            transactionAmounts: shape(),
        }).isRequired,
        showEnvironment: bool,
        activeEnvironment: shape({ type: string.isRequired }).isRequired,
    };

    static defaultProps = {
        showEnvironment: false,
    };

    handleDeleteTransactionDraft = (event, idTransaction) => {
        event.stopPropagation();
        event.preventDefault();
        const { dispatch } = this.props;
        dispatch(transactionsActions.deleteDraftRequest(idTransaction));
    };

    render() {
        const {
            isDesktop,
            isDeletingDraft,
            transaction: propsTransaction,
            showEnvironment,
            activeEnvironment,
        } = this.props;
        const { transaction, transactionAmounts } = propsTransaction;
        const {
            idTransaction,
            creationDateTime,
            submitDateTime,
            activityName,
            idTransactionStatus,
            environmentName,
            environmentType,
        } = transaction;
        const [currency] = Object.keys(transactionAmounts);
        const amount = transactionAmounts[currency];
        const showAmount = amount !== 0;
        const isEconomicGroup = activeEnvironment.type === CORPORATE_GROUP_ENVIRONMENT_TYPE;

        const webContent = (
            <Fragment>
                <div className="table-data table-data-icon">
                    <TransactionStatus idTransactionStatus={idTransactionStatus} showLabel />
                </div>
                <div className="table-data">
                    <span className="data-aux">{activityName}</span>
                </div>
                {showEnvironment && (
                    <div className="table-data">
                        <span className="data-aux">{environmentName}</span>
                    </div>
                )}
                <div className="table-data">
                    <span className="data-date">
                        <FormattedDate date={creationDateTime} />
                    </span>
                </div>
                <div className="table-data">
                    <span className="data-date">
                        <FormattedDate date={submitDateTime} />
                    </span>
                </div>
                <div className="table-data">
                    {showAmount && <FormattedAmount currency={currency} quantity={amount} />}
                </div>
                <div className="table-data table-data-icon">
                    {idTransactionStatus === "DRAFT" && (
                        <Button
                            type="button"
                            className="btn-only-icon"
                            block={false}
                            loading={isDeletingDraft}
                            onClick={(e) => this.handleDeleteTransactionDraft(e, idTransaction)}
                            bsStyle="circle">
                            <I18n
                                id="transactions.list.history.trashIcon.a11y"
                                componentProps={{ className: "visually-hidden" }}
                            />
                            <Image src="images/trash.svg" className="svg-icon" />
                        </Button>
                    )}
                </div>
                {!isEconomicGroup && <ChevromRight />}
            </Fragment>
        );

        const mobileContent = (
            <Fragment>
                <div className="table-data">
                    <TransactionStatus idTransactionStatus={idTransactionStatus} showLabel showIcon={false} />
                    <span className="data-text">{activityName}</span>
                    {showEnvironment && (
                        <div className="data-environment">
                            <Image src={`images/${environmentType}.svg`} className="svg-icon" />
                            <span className="data-text">{environmentName}</span>
                        </div>
                    )}
                </div>
                <div className="table-data">
                    <span className="data-date">
                        <I18n id="transactions.list.header.submitDate" /> <FormattedDate date={submitDateTime} />
                    </span>
                    {showAmount && <FormattedAmount currency={currency} quantity={amount} />}
                </div>
                {!isEconomicGroup && <ChevromRight />}
            </Fragment>
        );

        return isDesktop ? webContent : mobileContent;
    }
}

const mapStateToProps = (state) => ({
    isDeletingDraft: transactionsSelectors.isDeletingDraft(state),
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
});

export default connect(mapStateToProps)(TransactionsItem);
