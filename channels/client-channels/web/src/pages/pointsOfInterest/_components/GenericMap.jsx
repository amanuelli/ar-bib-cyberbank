import { Map, GoogleApiWrapper } from "google-maps-react";
import React from "react";

function GenericMap(props) {
    const { children } = props;
    return (
        <Map google={window.google} {...props}>
            {children}
        </Map>
    );
}

const mapStateToProps = (state) => ({
    apiKey: configSelectors.getConfig(state)["googlemap.apikey"],
});

export default connect(mapStateToProps)(GoogleApiWrapper(
  (props) => ({
    apiKey: props.apiKey
  }
))(GenericMap));
