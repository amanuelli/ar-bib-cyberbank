import React, { Component } from "react";
import { string, number as numberType, bool } from "prop-types";

import * as i18nUtils from "util/i18n";

import ListItem from "pages/_components/listItem/ListItem";

class AccountsListItem extends Component {
    static propTypes = {
        number: string.isRequired,
        productAlias: string,
        idProduct: string,
        productType: string.isRequired,
        amountLabel: string.isRequired,
        balance: numberType.isRequired,
        hideAmountLabel: bool,
        showLink: bool,
    };

    static defaultProps = {
        productAlias: null,
        idProduct: null,
        hideAmountLabel: false,
        showLink: true,
    };

    productType = () => {
        const { productType } = this.props;

        if (productType === "CA") {
            return i18nUtils.get("accounts.productType.savings");
        }

        return i18nUtils.get("accounts.productType.checking");
    };

    render() {
        const {
            productAlias,
            number,
            balance,
            amountLabel = i18nUtils.get("accounts.availableBalance"),
            idProduct,
            hideAmountLabel,
            showLink,
            ...props
        } = this.props;
        const productName = productAlias || number;
        const reference = `${i18nUtils.get("global.number")} ${number}`;
        const productTypeTitle = i18nUtils.get("desktop.widgets.account");

        return (
            <ListItem
                {...props}
                idProduct={idProduct}
                productTypeTitle={productTypeTitle}
                title={this.productType()}
                name={productName}
                reference={reference}
                amount={balance}
                amountLabel={!hideAmountLabel && amountLabel}
                path={showLink ? `/accounts/${idProduct}` : null}
            />
        );
    }
}

export default AccountsListItem;
