import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Col from "react-bootstrap/lib/Col";

import ListItem from "pages/accounts/ListItem";
import Container from "pages/_components/Container";
import InfoAccount from "pages/accounts/_components/InfoAccount";

import * as configUtil from "util/config";

class AccountsList extends Component {
    static propTypes = {
        accounts: PropTypes.oneOfType([PropTypes.array]).isRequired,
    };

    renderList = () => {
        const { accounts } = this.props;

        if (!accounts.length) {
            return null;
        }

        const maxBoxDisplay = configUtil.getInteger("product.list.maxBoxDisplay", 5);

        let showAsCards = accounts.length < maxBoxDisplay;
        showAsCards = false;

        return (
            <div role="menu" className={classNames("table-body", { "table-body--grid": showAsCards })}>
                {accounts.map((account) => {
                    if (showAsCards) {
                        return <InfoAccount account={account} key={`${account.idProduct}`} productAsCard />;
                    }

                    return <ListItem {...account} key={account.idProduct} />;
                })}
            </div>
        );
    };

    render() {
        return (
            <Container className="container--layout flex-grow">
                <Col className="col col-12" sm={12} md={9} lg={9} xl={6}>
                    <div className="table table--products">{this.renderList()}</div>
                </Col>
            </Container>
        );
    }
}

export default AccountsList;
