import React, { Component, Fragment } from "react";
import Col from "react-bootstrap/lib/Col";
import { connect } from "react-redux";
import { compose } from "redux";
import { Field, Form, withFormik } from "formik";
import * as Yup from "yup";

import { actions as accountsActions, selectors as accountsSelectors } from "reducers/accounts";
import { actions as productsActions } from "reducers/products";

import Button from "pages/_components/Button";
import Head from "pages/_components/Head";
import Notification from "pages/_components/Notification";
import MainContainer from "pages/_components/MainContainer";
import TextField from "pages/_components/fields/TextField";
import Container from "pages/_components/Container";

const FORM_ID = "accounts.alias";

class SetAlias extends Component {
    componentDidMount() {
        const { dispatch, ...rest } = this.props;

        dispatch(accountsActions.readAccount(rest.match.params.id));
        dispatch(accountsActions.closeOptions());
    }

    render() {
        const { account, fetching, isSubmitting, ...rest } = this.props;

        return (
            <Fragment>
                <Notification scopeToShow="accounts/setAlias" />

                <Head closeLinkTo={`/accounts/${rest.match.params.id}`} title="accounts.alias.setAlias.title" />
                <MainContainer showLoader={fetching}>
                    <Form className="above-the-fold">
                        <Container className="align-items-center flex-grow container--layout">
                            <Col sm={12} md={9} lg={6} xl={6} className="col">
                                <Field
                                    autoFocus
                                    component={TextField}
                                    hidePlaceholder
                                    idForm={FORM_ID}
                                    name="setAlias"
                                    type="text"
                                />
                            </Col>
                        </Container>
                        <Container className="align-items-center container--layout">
                            <Col sm={12} md={9} lg={6} xl={6} className="col">
                                <Button
                                    type="submit"
                                    bsStyle="primary"
                                    label="accounts.alias.setAlias.save"
                                    loading={isSubmitting}
                                />
                            </Col>
                        </Container>
                    </Form>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    account: accountsSelectors.getSelectedAccount(state),
    fetching: accountsSelectors.getFetching(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        enableReinitialize: true,
        validateOnChange: false,
        validateOnBlur: false,
        mapPropsToValues: (props) => ({
            setAlias: props.account ? props.account.productAlias : "",
            productId: props.match.params.id,
        }),
        validationSchema: () =>
            Yup.object().shape({
                setAlias: Yup.string().nullable(),
            }),
        handleSubmit: ({ productId, setAlias }, formikBag) => {
            formikBag.props.dispatch(productsActions.changeProductAlias(setAlias, productId, true), formikBag);
        },
    }),
)(SetAlias);
