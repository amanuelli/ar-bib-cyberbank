import React, { Component } from "react";
import { Col, Row } from "react-bootstrap";
import { string, shape, func } from "prop-types";
import I18n from "pages/_components/I18n";
import PaperlessSwitch from "pages/_components/fields/PaperlessSwitch";
import FormattedAmount from "pages/_components/FormattedAmount";
import FormattedNumber from "pages/_components/FormattedNumber";
import { REGION_USA } from "constants.js";

class Information extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        account: shape({
            ownerName: string.isRequired,
            nationalIdentifier: string,
        }).isRequired,
        activeRegion: string.isRequired,
    };

    state = {
        paperless: this.props.account.paperless,
    };

    render() {
        const { account, dispatch, activeRegion } = this.props;
        const isUSA = REGION_USA === activeRegion;
        const { paperless } = this.state;

        return (
            <>
                <Row>
                    {account.ownerName && (
                        <Col sm={12} md={4} lg={3} className="col col-12">
                            <div className="data-wrapper">
                                <span className="data-label">
                                    <I18n id="accounts.accountHolder" />
                                </span>

                                <span className="data-aux">{account.ownerName}</span>
                            </div>
                        </Col>
                    )}
                    {account.nationalIdentifier && (
                        <Col sm={12} md={4} lg={3} className="col col-12">
                            <div className="data-wrapper">
                                <span className="data-label">
                                    <I18n id="accounts.nationalAcountIdentifier" />
                                </span>
                                <span className="data-aux">{account.nationalIdentifier}</span>
                            </div>
                        </Col>
                    )}

                    <Col sm={12} md={4} lg={3} className="col col-12">
                        <PaperlessSwitch
                            name="account"
                            idProduct={account.idProduct}
                            paperless={account.paperless}
                            onClick={() => this.setState({ paperless: !paperless })}
                            dispatch={dispatch}
                        />
                    </Col>
                    {isUSA && (
                        <>
                            <Col sm={12} md={4} lg={3} className="col col-12">
                                <div className="data-wrapper">
                                    <span className="data-label">
                                        <I18n id="accounts.apy.interestYTD" />
                                    </span>

                                    <span className="data-aux">
                                        <FormattedAmount
                                            className="data-amount content-data-strong"
                                            quantity={10.73}
                                            currency={account.currency}
                                        />
                                    </span>
                                </div>
                            </Col>

                            <Col sm={12} md={4} lg={3} className="col col-12">
                                <div className="data-wrapper">
                                    <span className="data-label">
                                        <I18n id="accounts.apy.annualPercentage" />
                                    </span>

                                    <span className="data-aux">
                                        <FormattedNumber fixedDecimalScale suffix=" %">
                                            {1}
                                        </FormattedNumber>
                                    </span>
                                </div>
                            </Col>
                        </>
                    )}
                </Row>
            </>
        );
    }
}

export default Information;
