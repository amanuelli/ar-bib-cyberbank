import React, { Component, Fragment } from "react";
import { string, func, bool, int, shape } from "prop-types";

import { actions as productsActions } from "reducers/products";

import EditableLabel from "pages/_components/fields/EditableLabel";
import I18n from "pages/_components/I18n";

const productTypes = { CA: "savings", CC: "checking" };

class DetailHeadTitle extends Component {
    static propTypes = {
        isDesktop: bool.isRequired,
        dispatch: func.isRequired,
        account: shape({
            idEnvironment: int,
            idProduct: string,
            productType: string.isRequired,
            productAlias: string,
            label: string,
            shortLabel: string,
            extraInfo: string,
            number: string.isRequired,
            ownerName: string,
            currency: string.isRequired,
            extraInfoMarked: string,
        }).isRequired,
    };

    constructor(props) {
        super(props);
        this.titleRef = React.createRef();
    }

    componentDidMount() {
        if (this.titleRef.current) {
            this.titleRef.current.focus();
        }
    }

    componentDidUpdate() {
        if (this.titleRef.current) {
            this.titleRef.current.focus();
        }
    }

    saveAlias = (alias) => {
        const { account, dispatch } = this.props;
        dispatch(productsActions.changeProductAlias(alias, account.idProduct));
    };

    render() {
        const { account, isDesktop } = this.props;
        const { number, productAlias, productType, currency } = account;

        return (
            <Fragment>
                <div className="toolbar-item view-title">
                    <div className="visually-hidden" ref={this.titleRef} tabIndex="0">
                        <span>{`${productType} ${currency}, ${productAlias || number}`}</span>
                    </div>
                    {(isDesktop && (
                        <EditableLabel isDesktop={isDesktop} onSave={this.saveAlias} value={productAlias || number}>
                            <h1 className="data-name product-title">{productAlias || number}</h1>
                        </EditableLabel>
                    )) || <h1 className="ellipsis">{productAlias || number}</h1>}
                </div>
                <div className="toolbar-item">
                    <mark className="product-name">
                        <span>
                            <I18n id={`accounts.productType.${productTypes[productType]}`} />
                        </span>
                        <span>{number}</span>
                    </mark>
                </div>
            </Fragment>
        );
    }
}

export default DetailHeadTitle;
