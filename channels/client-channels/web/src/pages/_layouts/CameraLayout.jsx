import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Route from "react-router-dom/Route";
import withRouter from "react-router-dom/withRouter";
import PageTransition from "react-router-page-transition";

import { getTransition } from "util/transition";

import ErrorBoundary from "pages/_components/ErrorBoundary";
import { resizableRoute } from "pages/_components/Resizable";

class CameraLayout extends Component {
    state = {
        transition: "",
    };

    componentDidMount() {
        document.body.style.backgroundColor = "transparent";
    }

    /* eslint-disable-next-line react/sort-comp, camelcase */
    UNSAFE_componentWillReceiveProps(nextProps) {
        const { location } = this.props;

        if (location.pathname !== nextProps.location.pathname) {
            this.setState({
                transition: nextProps.transition || getTransition(this.props, nextProps),
            });
        }
    }

    getMobileLayout = (matchProps) => {
        const { component: ReceivedComponent, isMobile, isDesktop, isMobileNative } = this.props;
        const extras = { isMobile, isDesktop, isMobileNative };
        const { transition } = this.state;

        return (
            <div className={transition}>
                <PageTransition timeout={600}>
                    <div className="view-wrapper transition-item camera-layout">
                        <ErrorBoundary>
                            <ReceivedComponent {...matchProps} {...extras} />
                        </ErrorBoundary>
                    </div>
                </PageTransition>
            </div>
        );
    };

    render() {
        const { component: ReceivedComponent, ...rest } = this.props;

        return <Route {...rest} render={(matchProps) => <Fragment>{this.getMobileLayout(matchProps)}</Fragment>} />;
    }
}

export default withRouter(connect(null)(resizableRoute(CameraLayout)));
