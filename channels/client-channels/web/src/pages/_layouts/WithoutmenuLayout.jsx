import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import Route from "react-router-dom/Route";
import withRouter from "react-router-dom/withRouter";
import PageTransition from "react-router-page-transition";

import { getTransition } from "util/transition";

import { resizableRoute } from "pages/_components/Resizable";
import ErrorBoundary from "pages/_components/ErrorBoundary";
import Button from "pages/_components/Button";

class EnrollmentLayout extends Component {
    state = {
        transition: "",
    };

    componentDidMount() {
        document.body.style.backgroundColor = "#f9f9fb";
    }

    /* eslint-disable-next-line react/sort-comp, camelcase */
    UNSAFE_componentWillReceiveProps(nextProps) {
        const { location } = this.props;

        if (location.pathname !== nextProps.location.pathname) {
            this.setState({
                transition: nextProps.transition || getTransition(this.props, nextProps),
            });
        }
    }

    getMobileLayout = (matchProps) => {
        const { component: ReceivedComponent, isMobile, isDesktop, isMobileNative } = this.props;
        const extras = { isMobile, isDesktop, isMobileNative };
        const { transition } = this.state;

        return (
            <div className={transition}>
                <PageTransition timeout={600}>
                    <div className="view-wrapper theme-auth transition-item enrollment-layout">
                        <ErrorBoundary>
                            <ReceivedComponent {...matchProps} {...extras} />
                        </ErrorBoundary>
                    </div>
                </PageTransition>
            </div>
        );
    };

    getDesktopLayout = (matchProps) => {
        const { component: ReceivedComponent, isMobile, isDesktop, isMobileNative } = this.props;
        const extras = { isMobile, isDesktop, isMobileNative };

        return (
            <ErrorBoundary>
                <div className="app theme-auth enrollment-layout">
                    <div className="without-menu">
                        <div className="container-fluid">
                            <Button className="btn-outline" image="images/cross.svg" label="global.logout" />
                        </div>

                        <div className="app-page">
                            <div className="app-content">
                                <div className="view-wrapper theme-auth">
                                    <ReceivedComponent {...matchProps} {...extras} />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </ErrorBoundary>
        );
    };

    render() {
        const { component: ReceivedComponent, ...rest } = this.props;

        return (
            <Route
                {...rest}
                render={(matchProps) => {
                    const { isMobile } = this.props;

                    if (isMobile) {
                        return <Fragment>{this.getMobileLayout(matchProps)}</Fragment>;
                    }

                    return <Fragment>{this.getDesktopLayout(matchProps)}</Fragment>;
                }}
            />
        );
    }
}

export default withRouter(connect()(resizableRoute(EnrollmentLayout)));
