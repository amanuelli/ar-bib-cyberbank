import React, { Component, Fragment } from "react";
import Route from "react-router-dom/Route";
import withRouter from "react-router-dom/withRouter";
import PageTransition from "react-router-page-transition";
import Grid from "react-bootstrap/lib/Grid";
import Row from "react-bootstrap/lib/Row";
import Col from "react-bootstrap/lib/Col";
import { func, shape, string, node, bool } from "prop-types";
import { getTransition } from "util/transition";

import ErrorBoundary from "pages/_components/ErrorBoundary";
import FooterDesktop from "pages/_components/FooterDesktop";
import { resizableRoute } from "pages/_components/Resizable";
import Logo from "pages/login/_components/Logo";
import LanguageSelectionLink from "pages/login/_components/LanguageSelectionLink";
import RegionSelectionLink from "pages/login/_components/RegionSelectionLink";
import Weather from "pages/_components/Weather";

class ExternalLayout extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        history: shape({}).isRequired,
        fetching: bool.isRequired,
        location: shape({}).isRequired,
        transition: string,
        component: node.isRequired,
        isDesktop: bool,
        isMobile: bool,
        isMobileNative: bool,
    };

    static defaultProps = {
        isDesktop: false,
        isMobile: false,
        isMobileNative: false,
        transition: "",
    };

    state = {
        transition: "",
        styles: {},
    };

    componentDidMount() {
        const { isMobileNative } = this.props;

        if (isMobileNative) {
            const deviceHeight = window.innerHeight;

            this.setState({
                styles: {
                    position: "absolute",
                    top: 0,
                    height: deviceHeight,
                },
            });
        }

        document.body.style.backgroundColor = "#f9f9fb";
    }

    /* eslint-disable-next-line react/sort-comp, camelcase */
    UNSAFE_componentWillReceiveProps(nextProps) {
        const { location } = this.props;

        if (location.pathname !== nextProps.location.pathname) {
            this.setState({
                transition: nextProps.transition || getTransition(this.props, nextProps),
            });
        }
    }

    getMobileLayout = (matchProps) => {
        const { component: ReceivedComponent, isMobile, isDesktop, isMobileNative } = this.props;
        const extras = { isMobile, isDesktop, isMobileNative };
        const { transition, styles } = this.state;

        return (
            <Weather className={`${transition} login-background`} style={styles}>
                <PageTransition timeout={transition ? 600 : 0}>
                    <div className="view-wrapper transition-item">
                        <ErrorBoundary>
                            <ReceivedComponent {...matchProps} {...extras} />
                        </ErrorBoundary>
                    </div>
                </PageTransition>
            </Weather>
        );
    };

    getDesktopLayout = (matchProps) => {
        const {
            component: ReceivedComponent,
            isMobile,
            isDesktop,
            isMobileNative,
            location: { pathname },
        } = this.props;
        const extras = { isMobile, isDesktop, isMobileNative };
        const { transition } = this.state;

        return (
            <Weather className={`${transition} login-background`}>
                <PageTransition
                    timeout={transition ? 600 : 0}
                    style={{
                        background: "linear-gradient(transparent, transparent 67.5%, black)",
                    }}>
                    <div className="app transition-item">
                        <ErrorBoundary>
                            <section className="container--layout align-items-center">
                                <Grid className="login-desktop-wrapper panel">
                                    <Row className="justify-content-center">
                                        <Logo className="svg-image logo" />
                                        <Col
                                            className="col col-12"
                                            style={{
                                                display: "flex",
                                                flexDirection: "column",
                                                padding: 0,
                                            }}>
                                            <ReceivedComponent {...matchProps} {...extras} />
                                        </Col>
                                        <FooterDesktop
                                            moreOptions={[
                                                <LanguageSelectionLink disabled={pathname === "/languageSelection"} />,
                                                <RegionSelectionLink disabled={pathname === "/regionSelection"} />,
                                            ]}
                                        />
                                    </Row>
                                </Grid>
                            </section>
                        </ErrorBoundary>
                    </div>
                </PageTransition>
            </Weather>
        );
    };

    render() {
        const { component: ReceivedComponent, ...rest } = this.props;

        return (
            <Route
                {...rest}
                render={(matchProps) => {
                    const { isMobile } = this.props;

                    if (isMobile) {
                        return <Fragment>{this.getMobileLayout(matchProps)}</Fragment>;
                    }

                    return <Fragment>{this.getDesktopLayout(matchProps)}</Fragment>;
                }}
            />
        );
    }
}

export default withRouter(resizableRoute(ExternalLayout));
