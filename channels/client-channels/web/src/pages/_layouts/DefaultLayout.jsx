import React, { Component, Fragment } from "react";
import { bool, node, string, shape, oneOf } from "prop-types";
import { connect } from "react-redux";
import PageTransition from "react-router-page-transition";
import Route from "react-router-dom/Route";
import withRouter from "react-router-dom/withRouter";

import { getTransition } from "util/transition";

import { resizableRoute } from "pages/_components/Resizable";
import ErrorBoundary from "pages/_components/ErrorBoundary";
import I18n from "pages/_components/I18n";
import NavigationBarDesktop from "pages/_components/NavigationBarDesktop";
import FooterDesktop from "pages/_components/FooterDesktop";
import Menu from "pages/_components/menu/Menu";

class DefaultLayout extends Component {
    static propTypes = {
        isDesktop: bool,
        isMobile: bool,
        isMobileNative: bool,
        component: node.isRequired,
        componentProps: shape({}).isRequired,
        transition: oneOf([
            "transition-drill-out",
            "transition-drill-in",
            "transition-flow-close",
            "transition-flow-open",
            "transition-change-feature",
        ]),
        location: shape({
            pathname: string.isRequired,
            state: shape({
                transition: oneOf([
                    "transition-drill-out",
                    "transition-drill-in",
                    "transition-flow-close",
                    "transition-flow-open",
                    "transition-change-feature",
                ]),
            }),
        }).isRequired,
        withoutLayout: bool,
    };

    static defaultProps = {
        isDesktop: false,
        isMobile: false,
        isMobileNative: false,
        transition: "",
        withoutLayout: false,
    };

    state = {
        transition: "",
        isSidebarCollapsed: false,
        styles: {},
    };

    componentDidMount() {
        const { isMobileNative } = this.props;

        if (isMobileNative) {
            const deviceHeight = window.innerHeight;

            this.setState({
                styles: {
                    position: "absolute",
                    top: 0,
                    height: deviceHeight,
                },
            });
        }

        document.body.style.backgroundColor = "#f9f9fb";
    }

    handleToggleSidebar = () => {
        const { isSidebarCollapsed } = this.state;

        this.setState((prevState) => ({ isSidebarCollapsed: !prevState.isSidebarCollapsed }));

        if (!isSidebarCollapsed) {
            window.dispatchEvent(new Event("resize"));
        }
    };

    /* eslint-disable-next-line react/sort-comp, camelcase */
    UNSAFE_componentWillReceiveProps(nextProps) {
        const { location } = this.props;

        if (location.pathname !== nextProps.location.pathname) {
            this.setState({
                transition:
                    (nextProps.location.state && nextProps.location.state.transition) ||
                    nextProps.transition ||
                    getTransition(this.props, nextProps),
            });
        }
    }

    getMobileLayout = (matchProps) => {
        const { component: ReceivedComponent, isMobile, isDesktop, isMobileNative } = this.props;
        const extras = { isMobile, isDesktop, isMobileNative };
        const { transition, styles } = this.state;

        return (
            <div className={transition} style={styles}>
                <PageTransition timeout={600}>
                    <div className="view-wrapper theme-auth transition-item">
                        <ErrorBoundary>
                            <ReceivedComponent {...matchProps} {...extras} />
                        </ErrorBoundary>
                    </div>
                </PageTransition>
            </div>
        );
    };

    getDesktopLayout = (matchProps) => {
        const {
            component: ReceivedComponent,
            isMobile,
            isDesktop,
            isMobileNative,
            componentProps,
            withoutLayout,
        } = this.props;
        const extras = { isMobile, isDesktop, isMobileNative, ...componentProps };
        const { isSidebarCollapsed } = this.state;
        const buildNumber = window.BUILD_NUMBER ? `(Build ${window.BUILD_NUMBER})` : "";

        return (
            <ErrorBoundary>
                {!withoutLayout && (
                    <div className="app theme-auth default-layout">
                        <header className="app-header">
                            <NavigationBarDesktop />
                        </header>

                        <div className={`app-page ${isSidebarCollapsed ? "sidebar-collapsed" : ""}`}>
                            <aside className="app-sidebar">
                                <div className="scrollable-sidebar">
                                    <Menu
                                        isMobile={isMobile}
                                        onToggleSidebar={this.handleToggleSidebar}
                                        isCollapsed={isSidebarCollapsed}
                                    />
                                    <p className="text-muted app-version">
                                        {/* TODO remove this side effect */}
                                        {/* global process */}
                                        <I18n id="global.version" component="small" /> {process.env.REACT_APP_VERSION}{" "}
                                        {buildNumber}
                                    </p>
                                </div>
                            </aside>

                            <div className="app-content">
                                <div className="view-wrapper theme-auth">
                                    <ReceivedComponent {...matchProps} {...extras} />
                                </div>

                                <div className="app-footer">
                                    <FooterDesktop />
                                </div>
                            </div>
                        </div>
                    </div>
                )}
                {withoutLayout && (
                    <div className="app-page sidebar-collapsed">
                        <div className="app-content">
                            <div className="view-wrapper theme-auth">
                                <ReceivedComponent {...matchProps} {...extras} />
                            </div>
                        </div>
                    </div>
                )}
            </ErrorBoundary>
        );
    };

    render() {
        const { component: ReceivedComponent, ...rest } = this.props;

        return (
            <Route
                {...rest}
                render={(matchProps) => {
                    const { isMobile } = this.props;

                    if (isMobile) {
                        return <Fragment>{this.getMobileLayout(matchProps)}</Fragment>;
                    }

                    return <Fragment>{this.getDesktopLayout(matchProps)}</Fragment>;
                }}
            />
        );
    }
}

export default withRouter(connect()(resizableRoute(DefaultLayout)));
