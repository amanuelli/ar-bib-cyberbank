import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { goBack, replace } from "react-router-redux";
import { compose } from "redux";
import { Field, Form, withFormik } from "formik";
import { arrayOf, bool, func, string } from "prop-types";
import { Row, Col } from "react-bootstrap";
import * as Yup from "yup";
import moment from "moment";

import { actions as onboardingActions, selectors as onboardingSelectors } from "reducers/onboarding";
import { actions as loginActions, selectors as loginSelectors } from "reducers/login";

import Button from "pages/_components/Button";
import withExchangeToken from "pages/_components/withExchangeToken";
import Container from "pages/_components/Container";
import Head from "pages/_components/Head";
import Image from "pages/_components/Image";
import * as i18n from "util/i18n";
import I18n from "pages/_components/I18n";
import TextField from "pages/_components/fields/TextField";
import { DateField } from "pages/_components/fields/DateField";
import AmountField from "pages/_components/fields/formik/AmountField";
import { format } from "date-fns";

import * as config from "util/config";
import { Mixpanel } from "util/clickstreaming";
import { MAIN_ONBOARDING_ROUTE } from "./Step0";
import OnboardingStepper, { orientations } from "./_components/OnboardingStepper";
import Selector from "../_components/fields/formik/Selector";

const FORM_ID = "onboarding.step6";

class Step6 extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        documentType: string,
        isSubmitting: bool.isRequired,
        isMobile: bool.isRequired,
        occupations: arrayOf({}).isRequired,
    };

    static defaultProps = {
        documentType: "",
    };

    componentDidMount() {
        const { dispatch, documentType } = this.props;
        dispatch(onboardingActions.listOccupationsRequest());
        if (!documentType) {
            dispatch(replace(MAIN_ONBOARDING_ROUTE));
        }
    }

    onHeaderBack = () => {
        const { dispatch } = this.props;

        dispatch(goBack());
    };

    onHeaderClose = () => {
        const { dispatch } = this.props;

        dispatch(loginActions.reset());
    };

    render() {
        const { isSubmitting, isMobile, occupations } = this.props;

        return (
            <Fragment>
                <Head title={FORM_ID} onBack={isMobile && this.onHeaderBack} onClose={isMobile && this.onHeaderClose} />
                <div className="view-page">
                    {!isMobile && <OnboardingStepper currentStep={6} className="onboarding-steps" />}
                    <div className="view-content">
                        <main className="main-container">
                            <Form className="above-the-fold">
                                {isMobile && (
                                    <Container className="container--layout align-items-center">
                                        <Col className="col col-12 z_-1">
                                            <OnboardingStepper currentStep={6} orientation={orientations.horizontal} />
                                        </Col>
                                    </Container>
                                )}
                                <Container className="container--layout flex-grow align-items-center">
                                    <Col className="col col-12">
                                        <div className="form-group">
                                            <Row>
                                                <Col className="col col-12">
                                                    <Field
                                                        component={TextField}
                                                        idForm={FORM_ID}
                                                        classNameForViewMode="data-desc"
                                                        maxLength={50}
                                                        name="companyName"
                                                    />
                                                </Col>
                                                <Col className="col col-12">
                                                    <Row>
                                                        <Col className="col col-lg-4 col-md-4 col-sm-12 col-12">
                                                            <Field
                                                                component={DateField}
                                                                idForm={FORM_ID}
                                                                classNameForViewMode="data-desc"
                                                                maxLength={50}
                                                                name="entryDateField"
                                                                showMonthDropdown
                                                                showYearDropdown
                                                                minDate={moment("2009-01-01")}
                                                            />
                                                        </Col>
                                                        <Col className="col col-lg-8 col-md-8 col-sm-12 col-12">
                                                            <Field
                                                                component={Selector}
                                                                options={
                                                                    occupations &&
                                                                    occupations.map((occupation) => ({
                                                                        value: occupation.idOccupationJob,
                                                                        label: occupation.name,
                                                                    }))
                                                                }
                                                                idForm={FORM_ID}
                                                                classNameForViewMode="data-desc"
                                                                maxLength={50}
                                                                name="occupation"
                                                            />
                                                        </Col>
                                                        <Col className="col col-lg-12 col-md-12 col-sm-12 col-12">
                                                            <Field
                                                                component={AmountField}
                                                                data={{
                                                                    options: [
                                                                        {
                                                                            id: 1,
                                                                            label: i18n.get(`currency.label.USD`),
                                                                        },
                                                                    ],
                                                                }}
                                                                disableSelect
                                                                idForm={FORM_ID}
                                                                name="income"
                                                            />
                                                        </Col>
                                                    </Row>
                                                </Col>
                                            </Row>
                                        </div>
                                    </Col>
                                    <Col className="col col-12 ob-text-info">
                                        <Image className="svg-icon svg-caret" src="images/exclamation.svg" />
                                        <I18n component="span" id={`${FORM_ID}.invCodeInfo`} />
                                    </Col>
                                </Container>
                                <Container className="container--layout align-items-center">
                                    <Col className="col col-12">
                                        <Button
                                            label="global.continue"
                                            type="submit"
                                            bsStyle="primary"
                                            loading={isSubmitting}
                                        />
                                    </Col>
                                </Container>
                            </Form>
                        </main>
                    </div>
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    documentData: onboardingSelectors.getDocumentData(state),
    documentType: onboardingSelectors.getDocumentType(state),
    exchangeToken: onboardingSelectors.getExchangeToken(state),
    fetching: onboardingSelectors.getFetching(state),
    email: onboardingSelectors.getEmail(state),
    cellPhone: onboardingSelectors.getMobileNumber(state),
    address: onboardingSelectors.getAddress(state),
    occupations: onboardingSelectors.getOccupations(state),
    region: loginSelectors.getRegion(state) || null,
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        validateOnBlur: false,
        validateOnChange: false,
        mapPropsToValues: () => ({
            companyName: "",
            occupation: "",
            income: "",
        }),
        validationSchema: () =>
            Yup.object().shape({
                companyName: Yup.string().required(i18n.get("onboarding.step6.field.error.empty")),
                occupation: Yup.string().required(i18n.get("onboarding.step6.field.error.empty")),
                income: Yup.string().required(i18n.get("onboarding.step6.field.error.empty")),
                entryDateField: Yup.date().nullable(i18n.get("onboarding.step6.field.error.empty")),
            }),

        handleSubmit: ({ companyName, occupation, income, entryDateField }, formikBag) => {
            const { dispatch, documentData, email, cellPhone, address, region } = formikBag.props;
            const entryDate = format(entryDateField, "YYYY-MM-DD");

            const jobInformation = {
                companyName,
                occupation,
                income: { quantity: income.amount, currency: config.get("core.masterCurrency") },
                entryDate,
            };
            dispatch(
                onboardingActions.uploadClientInfoAndSendInvitationCode(
                    documentData.documentNumber,
                    email,
                    documentData.firstName,
                    formikBag,
                    documentData.lastName,
                    cellPhone,
                    address,
                    jobInformation,
                    region,
                ),
            );
            Mixpanel.track(FORM_ID, { ...documentData, email, cellPhone, address, jobInformation, region });
        },
    }),
)(withExchangeToken(Step6));
