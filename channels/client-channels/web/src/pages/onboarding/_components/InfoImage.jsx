import React, { Component } from "react";
import { string } from "prop-types";

import Image from "pages/_components/Image";

const sources = {
    step2: "phone-icon",
    step3: "LifeTest",
};

class InfoImage extends Component {
    static propTypes = {
        step: string.isRequired,
    };

    render() {
        const { step } = this.props;

        return (
            <div className={sources[step]}>
                <Image src={`images/${sources[step]}.svg`} />
            </div>
        );
    }
}

export default InfoImage;
