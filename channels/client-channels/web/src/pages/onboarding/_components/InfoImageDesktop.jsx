import React, { Component } from "react";
import { string } from "prop-types";

import Image from "pages/_components/Image";

const sources = {
    step2: "frontID",
    step3: "reverseID",
    step4: "passport",
};

class InfoImage extends Component {
    static propTypes = {
        documentSide: string,
        documentType: string,
        step: string.isRequired,
    };

    static defaultProps = {
        documentSide: "front",
        documentType: "document",
    };

    render() {
        const { documentSide, documentType } = this.props;
        let { step } = this.props;

        step = documentType === "passport" ? "step4" : step;
        step = documentSide === "back" ? "step3" : step;

        return (
            <div className={sources[step]}>
                <Image src={`images/${sources[step]}.svg`} />
            </div>
        );
    }
}

export default InfoImage;
