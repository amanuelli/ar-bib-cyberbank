import React, { Component, Fragment } from "react";
import Col from "react-bootstrap/lib/Col";
import { goBack } from "react-router-redux";
import { func, string } from "prop-types";

import { actions as loginActions } from "reducers/login";

import Button from "pages/_components/Button";
import Container from "pages/_components/Container";
import Head from "pages/_components/Head";
import I18n from "pages/_components/I18n";
import MainContainer from "pages/_components/MainContainer";
import StepIndicator from "pages/_components/StepIndicator";
import InfoImage from "pages/onboarding/_components/InfoImageDesktop";

class InfoLayer extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        documentSide: string,
        documentType: string,
        onContinue: func,
        step: string.isRequired,
    };

    static defaultProps = {
        documentSide: "",
        documentType: "",
        onContinue: null,
    };

    handleClick = () => {
        const { onContinue } = this.props;

        if (onContinue) {
            onContinue();
        }
    };

    onHeaderBack = () => {
        const { dispatch } = this.props;

        dispatch(goBack());
    };

    onHeaderClose = () => {
        const { dispatch } = this.props;

        dispatch(loginActions.reset());
    };

    render() {
        const { documentSide, documentType, step } = this.props;
        const indicators = [
            <I18n id="onboarding.step3" key="onboarding.step3" />,
            <I18n id="onboarding.step4" key="onboarding.step4" />,
        ];

        if (documentType === "document") {
            indicators.unshift(
                <I18n
                    active={documentSide === "front"}
                    id="onboarding.step2.front.document"
                    key="onboarding.step2.front.document"
                />,
                <I18n
                    active={documentSide === "back"}
                    id="onboarding.step2.back.document"
                    key="onboarding.step2.back.document"
                />,
            );
        } else if (documentType === "passport") {
            indicators.unshift(<I18n id="onboarding.step2.front.passport" key="onboarding.step2.front.passport" />);
        }

        return (
            <Fragment>
                <Head
                    title={`onboarding.${step}${documentSide ? `.${documentSide}` : ""}${
                        documentType && step !== "step3" ? `.${documentType}` : ""
                    }.header`}
                />
                <MainContainer className="main-container onboarding-layout">
                    <div className="overlay" />
                    <div className="above-the-fold">
                        <Container className="container--layout justify-content-center" gridClassName="form-content">
                            <Col sm={12} md={10} lg={6} xl={6} className="col col-12">
                                <StepIndicator
                                    isDisplayed
                                    step={step}
                                    steps={
                                        documentType === "document"
                                            ? ["step2", "step2", "step3", "step4"]
                                            : ["step2", "step3", "step4"]
                                    }>
                                    {indicators}
                                </StepIndicator>
                            </Col>
                        </Container>
                        <Container
                            className="container--layout justify-content-center flex-grow align-items-center"
                            gridClassName="form-content">
                            <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                <h2 className="text-center">
                                    <I18n
                                        id={`onboarding.${step}${documentSide ? `.${documentSide}` : ""}${
                                            documentType && step !== "step3" ? `.${documentType}` : ""
                                        }.info`}
                                    />
                                </h2>
                                <p className="text-center">
                                    {" "}
                                    <I18n
                                        id={`onboarding.${step}${documentSide ? `.${documentSide}` : ""}${
                                            documentType && step !== "step3" ? `.${documentType}` : ""
                                        }.photo`}
                                    />
                                </p>
                                <Container
                                    className="container--layout justify-content-center"
                                    gridClassName="form-content">
                                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                        <Button
                                            bsStyle="primary"
                                            label="global.continue"
                                            onClick={this.handleClick}
                                            type="button"
                                        />
                                    </Col>
                                </Container>
                                <InfoImage step={step} documentSide={documentSide} documentType={documentType} />
                            </Col>
                        </Container>
                    </div>
                </MainContainer>
            </Fragment>
        );
    }
}

export default InfoLayer;
