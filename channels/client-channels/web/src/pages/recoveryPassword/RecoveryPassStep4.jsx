import React, { Component } from "react";
import classNames from "classnames";

import Notification from "pages/_components/Notification";
import Step4Content from "pages/recoveryPassword/_components/Step4Content";
import MainContainer from "pages/_components/MainContainer";
import withExchangeToken from "pages/_components/withExchangeToken";

class RecoveryPassStep4 extends Component {
    render() {
        const mainContainerClass = classNames({
            "main-container": true,
            "container-fluid": this.props.isDesktop,
        });

        return (
            <React.Fragment>
                <Notification scopeToShow="recoveryPassword" />

                <MainContainer className={mainContainerClass}>
                    <Step4Content {...this.props} />
                </MainContainer>
            </React.Fragment>
        );
    }
}

export default withExchangeToken(RecoveryPassStep4);
