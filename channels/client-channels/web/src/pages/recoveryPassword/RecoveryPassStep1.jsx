import React, { Component } from "react";
import { connect } from "react-redux";
import classNames from "classnames";

import { actions as recoveryPasswordActions, selectors as recoveryPasswordSelectors } from "reducers/recoveryPassword";
import { actions as loginActions } from "reducers/login";

import MainContainer from "pages/_components/MainContainer";
import Notification from "pages/_components/Notification";
import Step1Content from "pages/recoveryPassword/_components/Step1Content";
import Head from "pages/_components/Head";

class RecoveryPassStep1 extends Component {
    componentDidMount() {
        this.props.dispatch(recoveryPasswordActions.recoveryPassCleanUp());
    }

    onHeaderClose = () => {
        this.props.dispatch(loginActions.reset());
    };

    render() {
        const mainContainerClass = classNames({
            "main-container": true,
            "container-fluid": this.props.isDesktop,
        });

        return (
            <React.Fragment>
                <Head onClose={this.onHeaderClose} title="recoveryPassword.step1.header" />
                <Notification scopeToShow="recoveryPassword" />
                <MainContainer className={mainContainerClass}>
                    <Step1Content {...this.props} />
                </MainContainer>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    showCaptcha: recoveryPasswordSelectors.getShowCaptcha(state),
});

export default connect(mapStateToProps)(RecoveryPassStep1);
