import React, { Component } from "react";
import Col from "react-bootstrap/lib/Col";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";

import { actions as recoveryPasswordActions } from "reducers/recoveryPassword";
import * as i18n from "util/i18n";
import * as config from "util/config";
import { bool } from "prop-types";

import I18n from "pages/_components/I18n";
import Credential from "pages/_components/fields/credentials/Credential";
import Button from "pages/_components/Button";
import Container from "pages/_components/Container";
import { Mixpanel } from "util/clickstreaming";

const FORM_ID = "recoveryPassword.step3";

class Step3Content extends Component {
    getPasswordHelp = () => (
        <ul>
            <li>
                <I18n id="settings.changePassword.passwordHelp.rule1" />
            </li>
            <li>
                <I18n id="settings.changePassword.passwordHelp.rule2" />
            </li>
            <li>
                <I18n id="settings.changePassword.passwordHelp.rule3" />
            </li>
        </ul>
    );

    render() {
        const { isSubmitting } = this.props;
        const maxLength = config.getInteger("core.password.maxLength");

        return (
            <Form className="above-the-fold login-desktop-wrapper">
                <Container className="flex-grow align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <Field
                            name="password"
                            idForm={FORM_ID}
                            component={Credential}
                            showStrength
                            maxLength={maxLength}
                        />
                        <Field
                            name="passwordConfirmation"
                            idForm={FORM_ID}
                            component={Credential}
                            maxLength={maxLength}
                        />

                        {this.getPasswordHelp()}
                    </Col>
                </Container>
                <Container className="align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <Button label="global.save" loading={isSubmitting} type="submit" bsStyle="primary" />
                    </Col>
                </Container>
            </Form>
        );
    }
}

Step3Content.propTypes = {
    isSubmitting: bool,
};

Step3Content.defaultProps = {
    isSubmitting: false,
};

export default withFormik({
    validateOnChange: false,
    validateOnBlur: false,
    mapPropsToValues: () => ({ password: "", passwordConfirmation: "" }),
    validationSchema: () =>
        Yup.object().shape({
            password: Yup.string().required(i18n.get(`${FORM_ID}.password.required`)),
            passwordConfirmation: Yup.string().required(i18n.get(`${FORM_ID}.passwordConfirmation.required`)),
        }),
    handleSubmit: (values, formikBag) => {
        Mixpanel.track(FORM_ID);
        formikBag.props.dispatch(
            recoveryPasswordActions.recoveryPassStep3(
                values.password,
                values.passwordConfirmation,
                formikBag.props.exchangeToken,
                formikBag.props.resetCode,
                formikBag,
            ),
        );
    },
})(Step3Content);
