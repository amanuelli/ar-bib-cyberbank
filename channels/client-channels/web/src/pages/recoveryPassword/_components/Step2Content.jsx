import React, { Component } from "react";
import Col from "react-bootstrap/lib/Col";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";
import { func, bool } from "prop-types";

import { actions as recoveryPasswordActions } from "reducers/recoveryPassword";
import * as i18n from "util/i18n";

import I18n from "pages/_components/I18n";
import Button from "pages/_components/Button";
import Container from "pages/_components/Container";
import TextField from "pages/_components/fields/TextField";
import { Mixpanel } from "util/clickstreaming";

const FORM_ID = "recoveryPassword.step2";

class Step2Content extends Component {
    handleResendEmail = () => {
        const { dispatch } = this.props;
        dispatch(recoveryPasswordActions.backToStep1());
    };

    render() {
        const { isSubmitting, fetching } = this.props;
        return (
            <Form className="above-the-fold login-desktop-wrapper">
                <Container className="flex-grow align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <p className="text-lead">
                            <I18n id="recoveryPassword.step2.mailSent.message" />
                        </p>
                    </Col>

                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <p>
                            <I18n id="recoveryPassword.step2.resendMail.receivedQuestion" />
                        </p>
                    </Col>
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <Button
                            loading={fetching}
                            label="recoveryPassword.step2.resendMail.link"
                            className="btn-outline"
                            onClick={this.handleResendEmail}
                        />
                    </Col>
                </Container>
                <Container className="flex-grow align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <Field
                            name="resetCode"
                            idForm={FORM_ID}
                            autoComplete="off"
                            maxLength={14}
                            component={TextField}
                        />
                    </Col>
                </Container>

                <Container className="align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <Button type="submit" loading={isSubmitting} label="global.continue" bsStyle="primary" />
                    </Col>
                </Container>
            </Form>
        );
    }
}

Step2Content.propTypes = {
    isSubmitting: bool,
    dispatch: func.isRequired,
    fetching: bool,
};

Step2Content.defaultProps = {
    isSubmitting: false,
    fetching: false,
};

export default withFormik({
    validateOnChange: false,
    validateOnBlur: false,
    mapPropsToValues: () => ({ resetCode: "" }),
    validationSchema: () =>
        Yup.object().shape({
            resetCode: Yup.string().required(i18n.get(`${FORM_ID}.resetCode.required`)),
        }),
    handleSubmit: (values, formikBag) => {
        Mixpanel.track(FORM_ID);
        formikBag.props.dispatch(recoveryPasswordActions.recoveryPassStep2(values.resetCode, formikBag));
    },
})(Step2Content);
