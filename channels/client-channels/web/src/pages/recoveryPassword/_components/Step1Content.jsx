import React, { Component } from "react";
import { Link } from "react-router-dom";
import Col from "react-bootstrap/lib/Col";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";
import { func, bool } from "prop-types";

import { actions as recoveryPasswordActions } from "reducers/recoveryPassword";
import * as i18n from "util/i18n";

import I18n from "pages/_components/I18n";
import TextField from "pages/_components/fields/TextField";
import Button from "pages/_components/Button";
import Captcha from "pages/_components/fields/credentials/Captcha";
import Container from "pages/_components/Container";
import Credential from "pages/_components/fields/credentials/Credential";
import { Mixpanel } from "util/clickstreaming";

const FORM_ID = "recoveryPassword.step1";

class Step1Content extends Component {
    onVerify = (value) => {
        const { setFieldValue } = this.props;
        setFieldValue("captcha", value);
    };

    render() {
        const { isSubmitting, showCaptcha, isMobileNative } = this.props;

        return (
            <Form className="above-the-fold  login-desktop-wrapper">
                <Container className="flex-grow align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <p className="text-lead">
                            <I18n id="recoveryPassword.step1.title" />
                        </p>
                        <p>
                            <Link to="/recoveryPassword/step2">
                                <I18n id="recoveryPassword.step1.userHasCode" />
                            </Link>
                        </p>
                        <Field
                            name="username"
                            idForm={FORM_ID}
                            autoComplete="on"
                            maxLength={50}
                            component={TextField}
                        />
                        <Field name="secondFactor" idForm={FORM_ID} component={Credential} type="otp" />
                    </Col>
                </Container>
                {showCaptcha && !isMobileNative && (
                    <Container sectionClasses="align-items-center container--layout">
                        <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                            <Field name="captcha" idForm={FORM_ID} component={Captcha} />
                        </Col>
                    </Container>
                )}
                <Container className="align-items-center container--layout">
                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                        <Button label="global.continue" type="submit" bsStyle="primary" loading={isSubmitting} />
                    </Col>
                </Container>
            </Form>
        );
    }
}

Step1Content.propTypes = {
    isSubmitting: bool,
    setFieldValue: func.isRequired,
    showCaptcha: bool,
    isMobileNative: bool,
};

Step1Content.defaultProps = {
    isSubmitting: false,
    showCaptcha: true,
    isMobileNative: false,
};

export default withFormik({
    validateOnChange: false,
    validateOnBlur: false,
    mapPropsToValues: () => ({ username: "", secondFactor: "", captcha: "" }),
    validationSchema: ({ isMobileNative, showCaptcha }) =>
        Yup.object().shape({
            username: Yup.string()
                .email(i18n.get(`${FORM_ID}.username.invalid`))
                .required(i18n.get(`${FORM_ID}.username.required`)),
            secondFactor: Yup.string().required(i18n.get(`${FORM_ID}.secondFactor.required`)),
            captcha:
                !isMobileNative && showCaptcha
                    ? Yup.string().required(i18n.get(`${FORM_ID}.captcha.required`))
                    : Yup.string(),
        }),
    handleSubmit: (values, formikBag) => {
        Mixpanel.track(FORM_ID);
        formikBag.props.dispatch(
            recoveryPasswordActions.recoveryPassStep1(values.username, values.secondFactor, values.captcha, formikBag),
        );
    },
})(Step1Content);
