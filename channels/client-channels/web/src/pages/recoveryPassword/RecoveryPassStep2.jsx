import React, { Component } from "react";
import { connect } from "react-redux";
import classNames from "classnames";

import { actions as recoveryPasswordActions } from "reducers/recoveryPassword";
import { actions as loginActions } from "reducers/login";

import Notification from "pages/_components/Notification";
import Step2Content from "pages/recoveryPassword/_components/Step2Content";
import Head from "pages/_components/Head";
import MainContainer from "pages/_components/MainContainer";
import withExchangeToken from "pages/_components/withExchangeToken";

class RecoveryPassStep2 extends Component {
    componentWillUnmount() {
        this.props.dispatch(recoveryPasswordActions.recoveryPassCleanUp());
    }

    onHeaderClose = () => {
        this.props.dispatch(loginActions.reset());
    };

    render() {
        const mainContainerClass = classNames({
            "main-container": true,
            "container-fluid": this.props.isDesktop,
        });

        return (
            <React.Fragment>
                <Head onClose={this.onHeaderClose} title="recoveryPassword.step2.header" />
                <Notification scopeToShow="recoveryPassword" />
                <MainContainer className={mainContainerClass}>
                    <Step2Content {...this.props} />
                </MainContainer>
            </React.Fragment>
        );
    }
}

export default connect(null)(withExchangeToken(RecoveryPassStep2));
