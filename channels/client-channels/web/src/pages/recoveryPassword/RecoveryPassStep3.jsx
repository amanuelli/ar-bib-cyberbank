import React, { Component } from "react";
import { connect } from "react-redux";
import classNames from "classnames";

import { actions as recoveryPasswordActions, selectors as recoveryPasswordSelectors } from "reducers/recoveryPassword";
import { actions as loginActions } from "reducers/login";

import MainContainer from "pages/_components/MainContainer";
import Notification from "pages/_components/Notification";
import Step3Content from "pages/recoveryPassword/_components/Step3Content";
import Head from "pages/_components/Head";
import withExchangeToken from "pages/_components/withExchangeToken";

class RecoveryPassStep3 extends Component {
    componentWillUnmount() {
        this.props.dispatch(recoveryPasswordActions.recoveryPassCleanUp());
    }

    onHeaderClose = () => {
        this.props.dispatch(loginActions.reset());
    };

    render() {
        const mainContainerClass = classNames({
            "main-container": true,
            "container-fluid": this.props.isDesktop,
        });

        return (
            <React.Fragment>
                <Head onClose={this.onHeaderClose} title="recoveryPassword.step3.header" />
                <Notification scopeToShow="recoveryPassword" />

                <MainContainer className={mainContainerClass}>
                    <Step3Content {...this.props} />
                </MainContainer>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    exchangeToken: recoveryPasswordSelectors.getExchangeToken(state),
    resetCode: recoveryPasswordSelectors.getResetCode(state),
});

export default connect(mapStateToProps)(withExchangeToken(RecoveryPassStep3));
