import React, { Fragment } from "react";
import { Link, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { string, bool, shape, func } from "prop-types";
import { push } from "react-router-redux";

import { selectors as sessionSelectors } from "reducers/session";
import { actions as loginActions } from "reducers/login";

import GeneralMsg from "pages/_components/GeneralMsg";
import Image from "pages/_components/Image";
import Navbar from "react-bootstrap/lib/Navbar";
import ErrorBoundary from "pages/_components/ErrorBoundary";
import Button from "pages/_components/Button";
import MainContainer from "pages/_components/MainContainer";

import * as i18n from "util/i18n";

class Error extends React.Component {
    static propTypes = {
        dispatch: func.isRequired,
        hasActiveSession: bool.isRequired,
        isMobile: bool.isRequired,
        location: shape({
            code: string,
            message: string,
        }).isRequired,
    };

    componentDidMount() {
        const { dispatch, location } = this.props;
        const { code } = location;
        if (code === "COR041E") {
            dispatch(
                loginActions.goBackToLoginAndShowMessage(
                    i18n.get("administration.restrictions.unavailableEnvironment"),
                ),
            );
        }
        window.common.hideSplashScreen();
    }

    getContent = () => {
        const { hasActiveSession, location } = this.props;
        const { message } = location;
        const { code } = location;

        if (!code) {
            return <Redirect to="/desktop" />;
        }

        let errorTitle = i18n.get("error.title");
        if (!errorTitle || errorTitle === "*error.title*") {
            errorTitle = "Ha ocurrido un error";
        }

        let errorMessage = message || i18n.get(`returnCode.${code}`);
        if (!errorMessage || errorMessage === `*returnCode.${code}*`) {
            errorMessage =
                "Estimado cliente: ha ocurrido un problema procesando su solicitud. Por favor espere unos minutos y vuelva a intentarlo más tarde.";
        }

        let errorBack = i18n.get("error.back");
        if (!errorBack || errorBack === "error.back") {
            errorBack = "Volver";
        }

        const btnHandlerOnClick = () => {
            const { dispatch } = this.props;
            dispatch(push(hasActiveSession ? "/desktop" : "/"));
        };

        const button = code !== "COR086E" && (
            <Button className="btn btn-primary btn-block" defaultLabelText={errorBack} onClick={btnHandlerOnClick} />
        );

        return (
            <MainContainer>
                <div className="above-the-fold">
                    <GeneralMsg
                        title={errorTitle}
                        description={errorMessage}
                        imagePath="images/coloredIcons/no-connection.svg"
                        callToAction={button}
                    />
                </div>
            </MainContainer>
        );
    };

    getMobileLayout = () => (
        <ErrorBoundary>
            <div className="view-wrapper theme-auth">{this.getContent()}</div>
        </ErrorBoundary>
    );

    getDesktopLayout = () => (
        <ErrorBoundary>
            <div className="app theme-auth enrollment-layout">
                <header className="app-header">
                    <Navbar collapseOnSelect fluid>
                        <Navbar.Header>
                            <div className="container-navbar">
                                <Navbar.Brand>
                                    <Link className="navbar-brand" to="/desktop">
                                        <Image src="images/logoCompany.svg" />
                                    </Link>
                                </Navbar.Brand>
                            </div>
                        </Navbar.Header>
                    </Navbar>
                </header>

                <div className="app-page">
                    <div className="app-content">
                        <div className="view-wrapper theme-auth">{this.getContent()}</div>
                    </div>
                </div>
            </div>
        </ErrorBoundary>
    );

    render() {
        const { isMobile } = this.props;
        return isMobile ? (
            <Fragment>{this.getMobileLayout()}</Fragment>
        ) : (
            <Fragment>{this.getDesktopLayout()}</Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    hasActiveSession: sessionSelectors.isLoggedIn(state),
});

export default connect(mapStateToProps)(Error);
