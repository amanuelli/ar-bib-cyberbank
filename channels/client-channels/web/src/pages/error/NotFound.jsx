import React from "react";
import { Grid, Row, Col, Button } from "react-bootstrap";
import { connect } from "react-redux";
import { push } from "react-router-redux";

import * as i18n from "util/i18n";
import { selectors as sessionSelectors } from "reducers/session";
import { selectors as statusSelectors } from "reducers/status";

class NotFound extends React.Component {
    componentDidMount() {
        window.common.hideSplashScreen();
    }

    handleClickGoToSafe = () => {
        const { dispatch, hasActiveSession } = this.props;

        dispatch(hasActiveSession ? push("/desktop") : push("/"));
    };

    render() {
        if (this.props.comeFromLogin) {
            // en este caso es la URL que teniamos guardada como last
            this.props.dispatch(push("/desktop"));
            return null;
        }

        let errorTitle = i18n.get("error.pageNotFound.title");
        if (!errorTitle || errorTitle === "error.pageNotFound.title") {
            errorTitle = "Página no encontrada";
        }

        let errorMessage = i18n.get("error.pageNotFound.content");
        if (!errorMessage || errorMessage === "error.pageNotFound.content") {
            errorMessage = "No ha sido posible encontrar la página que estás buscando.";
        }

        let errorBack = i18n.get("error.back");
        if (!errorBack || errorBack === "error.back") {
            errorBack = "Volver";
        }

        return (
            <div className="app-page">
                <div role="main" className="app-content">
                    <main className="main-container">
                        <form className="above-the-fold">
                            <section className="container--layout flex-grow align-items-center">
                                <Grid className="form-content">
                                    <Row className="justify-content-center">
                                        <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                            <div className="text-lead">
                                                <h2>{errorTitle}</h2>
                                                <p>{errorMessage}</p>
                                            </div>
                                        </Col>
                                    </Row>
                                </Grid>
                            </section>

                            <section className="container--layout  align-items-center">
                                <Grid className="form-content">
                                    <Row className="justify-content-center">
                                        <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                            <Button block bsStyle="primary" onClick={this.handleClickGoToSafe}>
                                                {errorBack}
                                            </Button>
                                        </Col>
                                    </Row>
                                </Grid>
                            </section>
                        </form>
                    </main>
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({
    hasActiveSession: sessionSelectors.isLoggedIn(state),
    comeFromLogin: statusSelectors.isComeFromLogin(state),
});

export default connect(mapStateToProps)(NotFound);
