import React, { Component } from "react";
import { shape, func, string, bool } from "prop-types";
import { connect } from "react-redux";
import { Modal } from "react-bootstrap";

import { selectors } from "reducers/status";
import { actions as sessionActions } from "reducers/session";

import ModalContent from "./_components/ModalContent";
import ModalPrimaryAction from "./_components/ModalPrimaryAction";

class SessionAboutToExpire extends Component {
    static propTypes = {
        status: shape({
            sessionAboutToExpire: bool.isRequired,
        }).isRequired,
        location: shape({
            href: string.isRequired,
        }).isRequired,
        dispatch: func.isRequired,
    };

    static getDerivedStateFromProps({ status }, { seconds }) {
        if (status.sessionAboutToExpire && seconds === 0) {
            return { seconds: status.sessionSecondsToExpire };
        }

        if (!status.sessionAboutToExpire && seconds !== 0) {
            return { seconds: 0 };
        }

        return null;
    }

    state = { seconds: 0 };

    interval = null;

    componentDidUpdate() {
        const { status } = this.props;

        if (!this.interval && status.sessionAboutToExpire) {
            this.interval = setInterval(this.decreaseSeconds, 1000);
        }
    }

    componentWillUnmount() {
        if (this.interval) {
            clearInterval(this.interval);
        }
    }

    decreaseSeconds = () => {
        const { seconds } = this.state;
        const { location, dispatch } = this.props;

        if (seconds > 1) {
            this.setState((prevState) => ({ seconds: prevState.seconds - 1 }));
        } else {
            dispatch(sessionActions.expire(location.href));
        }
    };

    extendSession = () => {
        const { dispatch } = this.props;

        dispatch(sessionActions.extend());
        clearInterval(this.interval);
        this.interval = null;
    };

    render() {
        const { status } = this.props;
        const { seconds } = this.state;

        return (
            <div className="modal-container">
                <Modal show={status.sessionAboutToExpire}>
                    <Modal.Body>
                        <ModalContent seconds={seconds} />
                    </Modal.Body>
                    <Modal.Footer>
                        <ModalPrimaryAction handleClick={this.extendSession} />
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

const mapStateToProps = (state) => ({ status: selectors.getStatus(state) });

export default connect(mapStateToProps)(SessionAboutToExpire);
