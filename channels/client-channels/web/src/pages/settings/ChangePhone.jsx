import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Formik, Field } from "formik";
import { func, bool, string } from "prop-types";
import { selectors as settingsSelectors, actions as settingsActions } from "reducers/settings";
import PhoneInput from "pages/_components/fields/PhoneInput";
import Credential from "pages/_components/fields/credentials/Credential";
import I18n from "pages/_components/I18n";
import Button from "pages/_components/Button";
import Head from "pages/_components/Head";
import MainContainer from "pages/_components/MainContainer";
import Container from "pages/_components/Container";
import Notification from "pages/_components/Notification";

const FORM_ID = "settings.changePhone";
class ChangePhone extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        isDesktop: bool.isRequired,
        fetching: bool.isRequired,
        mobilePhone: string,
    };

    static defaultProps = {
        mobilePhone: "",
    };

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(settingsActions.getUserData());
    }

    handleSubmit = ({ mobilePhone, otp }, formikBag) => {
        const { dispatch } = this.props;
        dispatch(settingsActions.sendMobilePhoneCode(mobilePhone, otp, formikBag));
    };

    handleFormSubmit = (event, { handleSubmit, errors }) => {
        const canSubmit = Object.values(errors).every((error) => error === undefined);
        if (canSubmit) {
            handleSubmit(event);
        } else {
            event.preventDefault();
        }
    };

    renderForm = ({ isSubmitting, ...form }) => {
        const { isDesktop, fetching, mobilePhone } = this.props;
        return (
            <form className="above-the-fold" onSubmit={(event) => this.handleFormSubmit(event, form)}>
                <Container className="container--layout flex-grow align-items-center" gridClassName="form-content">
                    <Container.Column sm={12} md={9} lg={6} xl={6}>
                        <p className="text-lead">
                            <I18n id="settings.changePhone.explanation" />
                        </p>
                        <Field
                            component={PhoneInput}
                            idForm={FORM_ID}
                            name="mobilePhone"
                            isDesktop={isDesktop}
                            mobilePhone={mobilePhone}
                        />
                    </Container.Column>
                </Container>
                <Container className="container--layout" gridClassName="form-content">
                    <Container.Column sm={12} md={9} lg={6} xl={6}>
                        <hr />
                        <h4>
                            <I18n id="settings.changePhone.verifyWithCredential" />
                        </h4>
                        <Field idForm={FORM_ID} name="otp" component={Credential} type="otp" />
                    </Container.Column>
                </Container>
                <Container className="container--layout" gridClassName="form-content">
                    <Container.Column sm={12} md={9} lg={6} xl={6}>
                        <Button
                            type="submit"
                            bsStyle="primary"
                            label="global.modify"
                            loading={isSubmitting}
                            disabled={fetching}
                        />
                    </Container.Column>
                </Container>
            </form>
        );
    };

    render() {
        const { mobilePhone } = this.props;
        return (
            <Fragment>
                <Head title="settings.changePhone" closeLinkTo="/settings" />
                <Notification scopeToShow="changePhone" />
                <MainContainer>
                    <Formik
                        initialValues={{ mobilePhone, otp: "" }}
                        onSubmit={this.handleSubmit}
                        validateOnChange={false}
                        enableReinitialize>
                        {this.renderForm}
                    </Formik>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    mobilePhone: settingsSelectors.getMobilePhone(state) || "",
    fetching: settingsSelectors.isFetching(state),
});

export default connect(mapStateToProps)(ChangePhone);
