import React, { Component, Fragment } from "react";
import { shape, bool, func } from "prop-types";
import { compose } from "redux";
import { connect } from "react-redux";
import Grid from "react-bootstrap/lib/Grid";
import Row from "react-bootstrap/lib/Row";
import Col from "react-bootstrap/lib/Col";
import { withFormik, Form, Field } from "formik";
import * as Yup from "yup";

import { selectors as sessionSelectors } from "reducers/session";
import { actions as settingsActions, selectors as settingsSelectors } from "reducers/settings";

import Head from "pages/_components/Head";
import I18n from "pages/_components/I18n";
import * as i18n from "util/i18n";
import MainContainer from "pages/_components/MainContainer";
import Notification from "pages/_components/Notification";
import Enviroments from "pages/_components/Enviroments";
import Button from "pages/_components/Button";

const FORM_ID = "settings";

class SetDefaultEnvironment extends Component {
    static propTypes = {
        environments: shape([]).isRequired,
        activeEnvironment: shape({}),
        userData: shape({}),
        isFetching: bool.isRequired,
        isMobile: bool.isRequired,
        dispatch: func.isRequired,
    };

    static defaultProps = {
        activeEnvironment: {},
        userData: {},
    };

    handleRemoveDefaultEnvironmentClick = () => {
        this.props.dispatch(settingsActions.changeDefaultEnvironment(0));
    };

    render() {
        const { environments, userData, isFetching, activeEnvironment, isMobile } = this.props;

        return (
            <Fragment>
                <Head title="settings.defaultEnvironment" closeLinkTo="/settings" />
                <Notification scopeToShow="changeDefaultEnvironment" />
                <MainContainer>
                    <Form className="above-the-fold">
                        <section className="container--layout">
                            <Grid className="form-content">
                                <Row className="justify-content-center">
                                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                        <p className="text-lead">
                                            <I18n id="settings.defaultEnvironment.title" />
                                        </p>
                                    </Col>
                                </Row>
                            </Grid>
                        </section>
                        <section className="container--layout flex-grow align-items-center">
                            <Grid className="form-content">
                                <Row className="justify-content-center">
                                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                        <Field
                                            name="environment"
                                            environments={environments}
                                            activeEnvironment={activeEnvironment}
                                            component={Enviroments}
                                            legendTextID="settings.changeEnvironment"
                                            fromSetDefaultEnvironmentPage
                                            userData={userData}
                                        />
                                    </Col>
                                </Row>
                            </Grid>
                        </section>

                        <section className="container--layout">
                            <Grid className="form-content">
                                <Row className="justify-content-center">
                                    <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                        <p className="text-lead">
                                            <I18n id="settings.defaultEnvironment.info" />
                                        </p>
                                    </Col>
                                </Row>
                            </Grid>
                        </section>

                        <section className="container--layout">
                            <Grid className="form-content">
                                <Row className="justify-content-center">
                                    <Col sm={12} md={3} lg={6} xl={6} className="col col-12">
                                        <Button
                                            type="submit"
                                            loading={isFetching}
                                            label="global.select"
                                            bsStyle="primary"
                                        />
                                        {userData.idDefaultEnvironment && !isMobile && (
                                            <Button
                                                type="button"
                                                loading={isFetching}
                                                label="settings.defaultEnvironment.buttonRemoveDefaultEnvironment"
                                                className="btn-outline"
                                                onClick={() => this.handleRemoveDefaultEnvironmentClick()}
                                            />
                                        )}
                                    </Col>
                                </Row>
                            </Grid>
                        </section>
                        {userData.idDefaultEnvironment && isMobile && (
                            <section className="container--layout">
                                <Grid className="form-content">
                                    <Row className="justify-content-center">
                                        <Col sm={12} md={3} lg={6} xl={6} className="col col-12">
                                            <Button
                                                type="button"
                                                loading={isFetching}
                                                label="settings.defaultEnvironment.buttonRemoveDefaultEnvironment"
                                                className="btn-outline"
                                                onClick={() => this.handleRemoveDefaultEnvironmentClick()}
                                            />
                                        </Col>
                                    </Row>
                                </Grid>
                            </section>
                        )}
                    </Form>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    environments: Object.entries(sessionSelectors.getEnvironments(state)),
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    userData: sessionSelectors.getUser(state),
    isFetching: settingsSelectors.isFetching(state),
});

export default compose(
    connect(mapStateToProps),
    withFormik({
        mapPropsToValues: ({ userData }) => ({
            environment: userData.idDefaultEnvironment || "",
        }),
        validationSchema: () =>
            Yup.object().shape({
                environment: Yup.string().required(i18n.get(`${FORM_ID}.defaultEnvironment.environment.required`)),
            }),
        handleSubmit: ({ environment }, formikBag) => {
            formikBag.props.dispatch(settingsActions.changeDefaultEnvironment(environment));
        },
    }),
)(SetDefaultEnvironment);
