import React, { Component } from "react";
import { connect } from "react-redux";
import Measure from "react-measure";
import { Col, Grid, Row } from "react-bootstrap";
import Table from "rc-table";
import { oneOf, shape, func, string, array, oneOfType, bool } from "prop-types";
import queryString from "query-string";

import {
    actions as pushNotificationsActions,
    selectors as pushNotificationsSelectors,
} from "reducers/pushNotifications";
import { naturalDate } from "util/format";
import * as i18n from "util/i18n";

import Head from "pages/_components/Head";
import Container from "pages/_components/Container";
import I18n from "pages/_components/I18n";
import MainContainer from "pages/_components/MainContainer";
import Notification from "pages/_components/Notification";
import Image from "pages/_components/Image";

class MyDevices extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        location: shape({ search: string }),
        isMobile: bool.isRequired,
        pushNotificationsConfiguredUserDevices: oneOfType([array.isRequired, oneOf([null]).isRequired]),
        fetching: bool,
    };

    static defaultProps = {
        pushNotificationsConfiguredUserDevices: null,
        location: shape({ search: "" }),
        fetching: false,
    };

    state = {
        tableHeight: 0,
    };

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(pushNotificationsActions.listAllUserDevices());
    }

    renderDevices = () => {
        const { pushNotificationsConfiguredUserDevices } = this.props;

        if (pushNotificationsConfiguredUserDevices && pushNotificationsConfiguredUserDevices.length) {
            return (
                <ul className="navigational-list">
                    {pushNotificationsConfiguredUserDevices.map(
                        ({ idDevice, model, platform, modificationDate, ...rest }) => (
                            <li key={idDevice} className="navigational-list-item">
                                <div className="navigational-list-item-container">
                                    <div className="my-devices-name">{this.renderIcon(rest)}</div>
                                    <span className="devices_info">
                                        {model ? (
                                            <I18n id={`devices.apple.identifier.${model}`} defaultValue={`${model}`} />
                                        ) : (
                                            <span>{platform}</span>
                                        )}
                                        {"  "}
                                        <small>{idDevice.substring(idDevice.length - 4, idDevice.length)}</small>
                                    </span>
                                    <span>{naturalDate(modificationDate)}</span>
                                </div>
                            </li>
                        ),
                    )}
                </ul>
            );
        }
        return <I18n id="settings.pushNotifications.configuredDevices.none" />;
    };

    getMobileContent = () => (
        <form className="above-the-fold">
            <section className="container--layout flex-grow align-items-center">
                <Grid className="form-content">
                    <Row className="justify-content-center">
                        <Col sm={12} md={10} lg={8} xl={8} className="col col-12">
                            <p className="text-lead">
                                <I18n id="settings.myDevices.title" />
                            </p>
                            {this.renderDevices()}
                        </Col>
                    </Row>
                </Grid>
            </section>
        </form>
    );

    generateTableColumns = () => [
        {
            key: "icon",
            dataIndex: "icon",
            title: i18n.get("settings.myDevices.data.device"),
            width: 100,
        },
        {
            key: "description",
            dataIndex: "description",
            title: "",
            width: 100,
        },
        {
            key: "idDeviceToShow",
            dataIndex: "idDeviceToShow",
            title: i18n.get("settings.myDevices.data.id"),
            width: 200,
        },
        {
            key: "lastLogin",
            dataIndex: "lastLogin",
            title: i18n.get("settings.myDevices.data.lastLogin"),
            width: 200,
        },
    ];

    renderIcon = (data) => {
        let icon = "images/desktop.svg";

        if (data.isAndroid) {
            icon = data.isTablet ? "images/tablet-android.svg" : "images/mobile.svg";
        }

        if (data.isIOS) {
            icon = data.isTablet ? "images/tablet-ipad.svg" : "images/mobile-iphone.svg";
        }

        return <Image src={icon} className="svg-icon" />;
    };

    getTableData = () => {
        const { pushNotificationsConfiguredUserDevices } = this.props;

        return pushNotificationsConfiguredUserDevices.map(
            ({ idDevice, model, platform, modificationDate, lastEntryCountry, lastEntryCity, ...rest }) => ({
                key: idDevice,
                idDeviceToShow: `****${idDevice.substring(idDevice.length - 4, idDevice.length)}`,
                icon: <div className="my-devices-name">{this.renderIcon(rest)}</div>,
                description: model ? (
                    <I18n id={`devices.apple.identifier.${model}`} defaultValue={`${model}`} />
                ) : (
                    <I18n id={`devices.platform.${platform}`} defaultValue={`${platform}`} />
                ),
                lastLogin:
                    lastEntryCountry && lastEntryCity
                        ? `${naturalDate(modificationDate)} (${lastEntryCity}, ${lastEntryCountry})`
                        : naturalDate(modificationDate),
            }),
        );
    };

    setSlectedRow = (record) => {
        const { location } = this.props;
        const { query } = queryString.parseUrl(location.search);

        if (record.key === query.idDevice) {
            return "tableNoInteraction clicked";
        }
        return "tableNoInteraction";
    };

    getDesktopContent = () => {
        const { pushNotificationsConfiguredUserDevices } = this.props;
        const { tableHeight } = this.state;

        if (pushNotificationsConfiguredUserDevices) {
            return (
                <div className="above-the-fold">
                    <Container
                        className="container--layout container--scrollable-table flex-grow align-items-center"
                        gridClassName="form-content">
                        <Col className="col col-12">
                            <I18n id="settings.myDevices.title" component="h4" />
                            {tableHeight ? (
                                <Table
                                    columns={this.generateTableColumns()}
                                    data={this.getTableData()}
                                    scroll={{ y: tableHeight }}
                                    rowKey={(record) => record.key}
                                    rowClassName={(record) => this.setSlectedRow(record)}
                                />
                            ) : (
                                <Measure
                                    bounds
                                    onResize={({ bounds }) => this.setState({ tableHeight: bounds.height })}>
                                    {({ measureRef }) => (
                                        <div ref={measureRef} style={{ height: "100%", width: "100%" }} />
                                    )}
                                </Measure>
                            )}
                        </Col>
                    </Container>
                </div>
            );
        }
        return null;
    };

    render() {
        const { fetching, isMobile } = this.props;

        return (
            <React.Fragment>
                <Notification scopeToShow="pushNotifications" />
                <Head title="settings.myDevices" closeLinkTo="/settings" />
                <MainContainer showLoader={fetching}>
                    {isMobile ? this.getMobileContent() : this.getDesktopContent()}
                </MainContainer>
            </React.Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    fetching: pushNotificationsSelectors.getFetching(state),
    pushNotificationsConfiguredUserDevices: pushNotificationsSelectors.getPushNotificationsConfiguredUserDevices(state),
});

export default connect(mapStateToProps)(MyDevices);
