import React, { Component, Fragment } from "react";
import { Col, Grid, Row } from "react-bootstrap";
import { bool, func, arrayOf, shape } from "prop-types";
import { connect } from "react-redux";

import { actions, selectors as settingsSelectors } from "reducers/settings";
import Head from "pages/_components/Head";
import I18n from "pages/_components/I18n";
import MainContainer from "pages/_components/MainContainer";
import Notification from "pages/_components/Notification";
import PageLoading from "pages/_components/PageLoading";
import PaperlessSwitch from "pages/_components/fields/PaperlessSwitch";
import Button from "pages/_components/Button";
import { Link } from "react-router-dom";
import * as i18nUtils from "util/i18n";

import AccountsListItem from "pages/accounts/ListItem";
import CreditCardListItem from "pages/creditCards/ListItem";
import LoansListItem from "pages/loans/ListItem";

class PaperlessModify extends Component {
    static propTypes = {
        fetching: bool,
        dispatch: func.isRequired,
        products: shape({
            accounts: arrayOf(shape({})),
            creditCards: arrayOf(shape({})),
            loans: arrayOf(shape({})),
        }).isRequired,
    };

    static defaultProps = {
        fetching: false,
    };

    componentDidMount() {
        const { dispatch } = this.props;
        dispatch(actions.fetchProductsList());
    }

    setAllPaperless = () => {
        const { dispatch } = this.props;
        dispatch(actions.setAllPaperlessValue(true));
    };

    getContent() {
        const { dispatch, products } = this.props;
        return (
            <form className="above-the-fold">
                <section className="container--layout">
                    <Grid className="form-content">
                        <Row className="justify-content-center">
                            <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                <p className="text-lead">
                                    <I18n id="preferences.settings.paperless.description" />
                                </p>
                                <span className="data-desc">
                                    <Link to="#">{i18nUtils.get("preferences.settings.paperless.info")}</Link>
                                </span>
                            </Col>
                        </Row>
                    </Grid>
                </section>
                <section className="container--layout flex-grow align-items-center">
                    <Grid className="form-content">
                        <Row className="justify-content-center">
                            <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                <Button
                                    block={false}
                                    bsStyle="link"
                                    className="btn-small"
                                    label="preferences.settings.paperless.setAllPaperlessValue"
                                    onClick={this.setAllPaperless}
                                />
                            </Col>
                        </Row>
                        <Row className="justify-content-center">
                            <Col sm={12} md={9} lg={6} xl={6} className="col col-12">
                                <ul className="navigational-list">
                                    <li className="navigational-list-item">
                                        {/* ACCOUNTS */}
                                        {products.accounts ? (
                                            <ul className="navigational-list">
                                                {products.accounts.map((account) => (
                                                    <li className="navigational-list-item navigational-list-downLoad">
                                                        <PaperlessSwitch
                                                            name={account.shortLabel}
                                                            hideLabel
                                                            title={
                                                                <AccountsListItem
                                                                    productAlias={
                                                                        account.productAlias || account.number
                                                                    }
                                                                    number={account.number}
                                                                    productType={account.idProduct}
                                                                    hideAmountLabel
                                                                    idProduct={account.idProduct}
                                                                    showLink={false}
                                                                />
                                                            }
                                                            fromSettings
                                                            dispatch={dispatch}
                                                            idProduct={account.idProduct}
                                                            paperless={account.paperless}
                                                        />
                                                    </li>
                                                ))}
                                            </ul>
                                        ) : (
                                            <I18n id="desktop.widgets.accounts.empty" />
                                        )}
                                    </li>
                                    <li className="navigational-list-item">
                                        {/* CREDIT CARDS */}
                                        {products.creditCards ? (
                                            <ul className="navigational-list">
                                                {products.creditCards.map((creditCard) => (
                                                    <li className="navigational-list-item navigational-list-downLoad">
                                                        <PaperlessSwitch
                                                            hideLabel
                                                            name={creditCard.productAlias || creditCard.label}
                                                            title={
                                                                <CreditCardListItem
                                                                    label={creditCard.productAlias || creditCard.label}
                                                                    number={creditCard.number}
                                                                    minimumPayment={null}
                                                                    hideAmountLabel
                                                                    idProduct={creditCard.idProduct}
                                                                    showLink={false}
                                                                />
                                                            }
                                                            fromSettings
                                                            dispatch={dispatch}
                                                            idProduct={creditCard.idProduct}
                                                            paperless={creditCard.paperless}
                                                        />
                                                    </li>
                                                ))}
                                            </ul>
                                        ) : (
                                            <I18n id="desktop.widgets.creditCards.empty" />
                                        )}
                                    </li>
                                    <li className="navigational-list-item">
                                        {/* LOANS */}
                                        {products.loans ? (
                                            <ul className="navigational-list">
                                                {products.loans.map((loan) => (
                                                    <li className="navigational-list-item navigational-list-downLoad">
                                                        <PaperlessSwitch
                                                            hideLabel
                                                            name={loan.productAlias || loan.number}
                                                            title={
                                                                <LoansListItem
                                                                    number={loan.number}
                                                                    productAlias={loan.productAlias}
                                                                    nextDueDate={null}
                                                                    idProduct={loan.idProduct}
                                                                    showLink={false}
                                                                />
                                                            }
                                                            fromSettings
                                                            dispatch={dispatch}
                                                            idProduct={loan.idProduct}
                                                            paperless={loan.paperless}
                                                        />
                                                    </li>
                                                ))}
                                            </ul>
                                        ) : (
                                            <I18n id="desktop.widgets.loans.empty" />
                                        )}
                                    </li>
                                </ul>
                            </Col>
                        </Row>
                    </Grid>
                </section>
            </form>
        );
    }

    render() {
        const { fetching, products } = this.props;
        return (
            <Fragment>
                <PageLoading loading={fetching}>
                    <Head title="preferences.settings.paperless.title" closeLinkTo="/settings" />
                    <Notification scopeToShow="settings" />
                    <MainContainer>{products && this.getContent()}</MainContainer>
                </PageLoading>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    fetching: settingsSelectors.isFetching(state),
    products: settingsSelectors.getProductsLists(state),
});

export default connect(mapStateToProps)(PaperlessModify);
