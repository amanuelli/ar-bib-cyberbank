import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Grid, Row, Col, Button } from "react-bootstrap";
import { arrayOf, bool, func, number, objectOf, shape, string } from "prop-types";

import { actions as sessionActions, selectors as sessionSelectors } from "reducers/session";
import { selectors as loginSelectors } from "reducers/login";
import { getLastLoginDate, getLastLoginPlace } from "util/settings";
import { REGION_USA } from "constants.js";

import Head from "pages/_components/Head";
import I18n from "pages/_components/I18n";
import Image from "pages/_components/Image";
import EnvironmentTag from "pages/_components/EnvironmentTag";
import MainContainer from "pages/_components/MainContainer";
import Notification from "pages/_components/Notification";
import Avatar from "pages/_components/Avatar";
import SettingsOption from "pages/settings/_components/SettingsOption";

class Settings extends Component {
    static propTypes = {
        dispatch: func.isRequired,
        activeRegion: string.isRequired,
        environments: objectOf(
            shape({
                environmentType: string,
                idEnvironment: string,
                name: string,
            }),
        ),
        isMobile: bool.isRequired,
        user: shape({
            email: string,
            previousLoginInfo: arrayOf(
                shape({
                    city: string,
                    country: string,
                    date: string,
                    idUser: string,
                    ip: string,
                    latitude: number,
                    longitude: number,
                }),
            ),
            userFullName: string,
        }),
        activeEnvironment: shape(string).isRequired,
    };

    static defaultProps = {
        environments: null,
        user: null,
    };

    constructor(props) {
        super(props);

        this.containerRef = React.createRef();
    }

    logOut = () => {
        const { dispatch } = this.props;
        dispatch(sessionActions.logout());
    };

    getSettingsData = () => {
        const { user, activeEnvironment, isMobile } = this.props;
        const { previousLoginInfo, userFullName, email } = user;

        return (
            <Fragment>
                <Col className="col col-12">
                    <Grid>
                        <Row className="content-data-row">
                            <Col className="col content-data-wrapper col-12" sm={12} md={4} lg={4}>
                                <span className="data-text content-data-strong">{userFullName}</span>
                            </Col>
                            <Col sm={12} md={4} lg={4} className="col col-12 content-data-wrapper">
                                <span className="data-email">{email}</span>
                            </Col>
                            {isMobile && (
                                <Col sm={12} md={4} lg={4} className="col col-12 content-data-wrapper environment">
                                    <EnvironmentTag type={activeEnvironment.type} name={activeEnvironment.name} />
                                </Col>
                            )}
                            {previousLoginInfo.length > 0 && (
                                <Col sm={12} md={4} lg={4} className="col col-12 content-data-wrapper">
                                    <span className="content-data-wrapper content-data-small">
                                        <span className="data-label">
                                            <I18n id="settings.lastLogin.date" />
                                        </span>{" "}
                                    </span>
                                    <span className="content-data-wrapper content-data-small">
                                        <span className="data-desc">
                                            {getLastLoginDate(previousLoginInfo[0])},{" "}
                                            <Link to="/settings/lastLogin">
                                                {getLastLoginPlace(previousLoginInfo[0])}
                                            </Link>
                                        </span>
                                    </span>
                                </Col>
                            )}
                        </Row>
                    </Grid>
                </Col>
            </Fragment>
        );
    };

    render() {
        const { environments, isMobile, user, activeRegion } = this.props;
        const { defaultAvatarId, email, userFullName } = user;
        const buildNumber = window.BUILD_NUMBER ? `(Build ${window.BUILD_NUMBER})` : "";

        return (
            <Fragment>
                <Notification scopeToShow="settings" />
                <Head title="settings.index" />

                <div className="view-morphing">
                    <section className="container--layout align-items-center section-content-heading">
                        <Grid>
                            <Row className="justify-content-center">
                                {isMobile && (
                                    <Col className="col" sm={12}>
                                        <div style={{ display: "flex", justifyContent: "center" }}>
                                            <Avatar size="big" user={{ userFullName, defaultAvatarId, email }} />
                                        </div>
                                    </Col>
                                )}
                                {this.getSettingsData()}
                            </Row>
                        </Grid>
                    </section>
                </div>

                <MainContainer>
                    <div className="above-the-fold tight-containers" ref={this.containerRef}>
                        <section className="container--layout flex-grow align-items-center">
                            <Grid className="form-content">
                                <Row className="justify-content-center">
                                    <Col md={6} sm={12}>
                                        <h4 className="navigational-list-title">
                                            <I18n id="settings.title.personal" />
                                        </h4>
                                        <ul className="navigational-list">
                                            <SettingsOption
                                                linkTo="/settings/changeEmail"
                                                messageKey="settings.options.changeEmail"
                                            />
                                            <SettingsOption
                                                linkTo="/settings/changePhone"
                                                messageKey="settings.options.changePhone"
                                            />
                                            <SettingsOption
                                                linkTo="/settings/ChangeAddresses"
                                                messageKey="settings.options.changeAddresses"
                                            />
                                        </ul>
                                    </Col>
                                    <Col md={6} sm={12}>
                                        <h4 className="navigational-list-title">
                                            <I18n id="settings.title.security" />
                                        </h4>
                                        <ul className="navigational-list">
                                            <SettingsOption
                                                linkTo="/settings/changePassword"
                                                messageKey="settings.options.changePassword"
                                            />
                                            <SettingsOption
                                                linkTo="/settings/changeSecuritySeal"
                                                messageKey="settings.options.changeSecuritySeal"
                                            />
                                            <SettingsOption
                                                linkTo="/settings/myDevices"
                                                messageKey="settings.options.myDevices"
                                            />
                                        </ul>
                                    </Col>
                                    <Col md={6} sm={12}>
                                        <h4 className="navigational-list-title">
                                            <I18n id="settings.title.configuration" />
                                        </h4>
                                        <ul className="navigational-list">
                                            <SettingsOption
                                                linkTo="/settings/fingerprintConfiguration"
                                                messageKey="settings.options.biometricsConfiguration"
                                            />
                                            <SettingsOption
                                                linkTo="/settings/pushNotifications"
                                                messageKey="settings.options.pushNotifications"
                                            />
                                            <SettingsOption
                                                linkTo="/settings/notificationsConfiguration"
                                                messageKey="settings.options.notificationsConfiguration"
                                            />
                                            <SettingsOption
                                                linkTo="/settings/paperless/edit"
                                                messageKey="preferences.settings.paperless.title"
                                            />

                                            {environments && Object.keys(environments).length > 1 && (
                                                <SettingsOption
                                                    linkTo="/settings/setDefaultEnvironment"
                                                    messageKey="settings.options.setDefaultEnvironment"
                                                />
                                            )}

                                            <SettingsOption
                                                linkTo="/settings/changeLanguage"
                                                messageKey="settings.options.changeLanguage"
                                            />
                                        </ul>
                                    </Col>

                                    {activeRegion === REGION_USA && (
                                        <Col md={6} sm={12}>
                                            <h4 className="navigational-list-title">
                                                <I18n id="settings.title.documents" />
                                            </h4>
                                            <ul className="navigational-list">
                                                <SettingsOption
                                                    linkTo="/settings/esign/documents"
                                                    messageKey="settings.options.documents.termsAndConditions"
                                                />
                                            </ul>
                                        </Col>
                                    )}
                                </Row>
                            </Grid>
                        </section>
                        {isMobile && (
                            <section className="container--layout align-items-center ">
                                <Grid>
                                    <Row className="justify-content-center">
                                        <Col className="col col-12">
                                            <div className="flex-container">
                                                <small className="text-muted">
                                                    {/* global process */}
                                                    <I18n id="global.version" /> {process.env.REACT_APP_VERSION}{" "}
                                                    {buildNumber}
                                                </small>
                                                <Button
                                                    className="btn-link"
                                                    onClick={this.logOut}
                                                    style={{ marginRight: "-1em" }}>
                                                    <Image
                                                        src="images/cerrarSesion.svg"
                                                        className="svg-icon svg-caret"
                                                    />
                                                    <span>
                                                        <I18n id="global.logout" />
                                                    </span>
                                                </Button>
                                            </div>
                                        </Col>
                                    </Row>
                                </Grid>
                            </section>
                        )}
                    </div>
                </MainContainer>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => ({
    activeEnvironment: sessionSelectors.getActiveEnvironment(state),
    environments: sessionSelectors.getEnvironments(state),
    user: sessionSelectors.getUser(state),
    activeRegion: loginSelectors.getRegion(state),
});

export default connect(mapStateToProps)(Settings);
