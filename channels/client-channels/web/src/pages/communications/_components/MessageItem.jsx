import React, { Component } from "react";
import { func, shape, bool, number, string } from "prop-types";

import I18n from "pages/_components/I18n";
import MessageActions from "pages/communications/_components/MessageActions";

function MessageSender(props) {
    const { communication } = props;

    if (communication.direction === "BANK_TO_CUSTOMER") {
        return (
            <div>
                <I18n
                    component="span"
                    className="data-aux"
                    id={`communications.tray.${communication.idCommunicationTray}`}
                />
                <span> &gt; </span>
                <I18n component="span" id="communication.thread.message.me" />
            </div>
        );
    }
    return (
        <div>
            <I18n component="span" id="communication.thread.message.me" />
            <span> &gt; </span>
            <I18n
                component="span"
                className="data-aux"
                id={`communications.tray.${communication.idCommunicationTray}`}
            />
        </div>
    );
}

MessageSender.propTypes = {
    communication: shape({
        idCommunication: number,
        direction: string,
    }).isRequired,
};

class MessageItem extends Component {
    static propTypes = {
        index: number.isRequired,
        communication: shape({
            idCommunication: number,
            userRead: bool,
            sentDateAsString: string,
            direction: string,
            subject: string,
            body: string,
        }).isRequired,
        handleRemoveClick: func.isRequired,
        handleChangeMessageStatus: func.isRequired,
        handleSelectMessageClick: func.isRequired,
    };

    render() {
        const {
            communication,
            index,
            handleRemoveClick,
            handleChangeMessageStatus,
            handleSelectMessageClick,
        } = this.props;

        return (
            <li
                ref={index}
                onClick={() => handleSelectMessageClick(communication.idCommunication, communication.userRead, index)}
                key={communication.idCommunication}
                className={communication.userRead ? "read-message" : ""}>
                <div className="message-item">
                    <div className="flex-container">
                        <MessageSender communication={communication} />
                        <span className="data-date">{communication.sentDateAsString}</span>
                    </div>

                    <div className="flex-container">
                        <div className="ellipsis">
                            <h4 className="message-title">{communication.subject}</h4>
                            <p
                                className="data-desc list-content ellipsis"
                                // eslint-disable-next-line
                                dangerouslySetInnerHTML={{
                                    __html: communication.body,
                                }}
                            />
                        </div>
                        <MessageActions
                            communication={communication}
                            handleRemoveClick={handleRemoveClick}
                            handleChangeMessageStatus={handleChangeMessageStatus}
                            index={index}
                        />
                    </div>
                </div>
            </li>
        );
    }
}

export default MessageItem;
