import { call, put, takeLatest } from "redux-saga/effects";
import { replace } from "react-router-redux";

import { NO_TRANSITION } from "constants.js";
import * as loans from "middleware/loans";
import { actions as notificationActions } from "reducers/notification";
import { types } from "reducers/loans";
import { downloadPdf, downloadXls } from "util/download";
import * as i18n from "util/i18n";

const sagas = [
    takeLatest(types.LIST_LOANS_REQUEST, listLoansRequest),
    takeLatest(types.LOAN_DETAILS_REQUEST, loanDetails),
    takeLatest(types.LOAN_FETCH_MORE_STATEMENTS_REQUEST, fetchMoreStatements),
    takeLatest(types.LOAN_READ_REQUEST, loanRead),
    takeLatest(types.DOWNLOAD_PAYMENT_REQUEST, downloadPayment),
];

export default sagas;

const defaultFilters = {
    status: "allFees",
};

function* listLoansRequest() {
    const response = yield call(loans.listLoans);
    if (response && response.status === 200) {
        let loansList = response.data.data.amortizedLoans;
        const { requestLoanPermission } = response.data.data;
        const { amortizedLoans, propertiesLoans } = response.data.data;
        loansList = amortizedLoans !== null ? amortizedLoans.concat(propertiesLoans) : propertiesLoans;
        if (loansList.length === 1) {
            yield put(
                replace({
                    pathname: `/loans/${loansList[0].idProduct}`,
                    state: { transition: NO_TRANSITION },
                }),
            );
        } else {
            yield put({
                type: types.LIST_LOANS_SUCCESS,
                loansList,
                requestLoanPermission,
            });
        }
    }
}

function* loanDetails({ selectedLoanId, filters = defaultFilters }) {
    const response = yield call(loans.listStatements, selectedLoanId, filters);

    if (response && response.status === 200) {
        yield put({
            type: types.LOAN_DETAILS_SUCCESS,
            ...response.data.data,
        });
    }
}

function* fetchMoreStatements({ loanId, filters = defaultFilters }) {
    const response = yield call(loans.listStatements, loanId, filters);

    if (response && response.status === 200) {
        yield put({
            type: types.LOAN_FETCH_MORE_STATEMENTS_SUCCESS,
            ...response.data.data,
        });
    }
}

function* loanRead({ loanId }) {
    const response = yield call(loans.readLoan, loanId);

    if (response && response.status === 200) {
        yield put({
            type: types.LOAN_READ_SUCCESS,
            ...response.data.data,
        });
    }
}

function* downloadPayment({ idLoan, format }) {
    const { type, data } = yield call(loans.downloadPayment, idLoan, defaultFilters, format);

    if (type === "W") {
        yield put({ type: types.DOWNLOAD_PAYMENT_FAILURE });
        yield put(notificationActions.showNotification(i18n.get("global.unexpectedError"), "error", ["loan/details"]));
    } else {
        const { content, fileName } = data.data;

        if (format === "pdf") {
            downloadPdf(fileName, content);
        } else {
            downloadXls(fileName, content);
        }

        yield put({ type: types.DOWNLOAD_PAYMENT_SUCCESS });
    }
}
