import { takeLatest, takeEvery, call, put, select } from "redux-saga/effects";
import { replace, routerActions } from "react-router-redux";

import { selectors as assistantSelectors, actions as assistantActions } from "reducers/assistant";
import { actions as i18nActions } from "reducers/i18n";
import { actions as statusActions } from "reducers/status";
import globalTypes from "reducers/types/global";
import formTypes from "reducers/types/form";
import { types as enrollmentTypes } from "reducers/enrollment";
import { types, selectors, actions as loginActions } from "reducers/login";
import { types as sessionTypes, actions as sessionActions, selectors as sessionSelectors } from "reducers/session";
import { actions as notificationActions } from "reducers/notification";
import * as session from "middleware/session";
import * as i18n from "util/i18n";
import * as fingerprintUtils from "util/fingerprint";
import * as secureStorageUtils from "util/secureStorage";
import * as configUtils from "util/config";
import { adjustIdFieldErrors } from "util/form";
import currentLocation from "util/geolocation";
import { AVAILABLE_REGIONS, REGION_REST_OF_LATAM, REGION_USA } from "constants.js";

import * as deviceUtils from "util/device";

const NOTOKENLOGIN = "notokenlogin";
const sagas = [
    takeEvery(types.RESET, reset),

    takeLatest(types.LOGIN_STEP_1_REQUEST, handleLoginStep1Request),
    takeLatest(types.LOGIN_STEP_2_REQUEST, handleLoginStep2Request),
    takeLatest(types.LOGIN_OAUTH_REQUEST, handleOauthRequest),
    takeLatest(types.LOGIN_STEP_3_REQUEST, handleLoginStep3Request),
    takeLatest(types.LOGIN_STEP_4_REQUEST, handleLoginStep4Request),
    takeLatest(types.FINALIZE_LOGIN, handleFinalizeLogin),

    takeLatest(types.FINGERPRINT_LOGIN_PRE, handleFingerprintLoginPre),
    takeLatest(types.GO_BACK_LOGIN_SHOW_MESSAGE, goBackToLoginAndShowMessage),
    takeLatest(types.GET_CLIENT_COUNTRY_REQ, getClientCountry),
];

export default sagas;

function* getClientCountry() {
    const activeRegion = yield select(selectors.getRegion);
    if (!activeRegion) {
        try {
            const { type, data } = yield call(session.getClientCountry);
            if (type !== "W") {
                const { country } = data.data;
                if (country) {
                    yield put({ type: types.GET_CLIENT_COUNTRY_RES, clientCountry: country.iso_code });

                    // Verify if the client country is in our supported countries
                    if (AVAILABLE_REGIONS.indexOf(country.iso_code) !== -1) {
                        yield put({ type: types.SET_REGION, region: country.iso_code });
                    } else {
                        yield put({ type: types.SET_REGION, region: REGION_REST_OF_LATAM });
                    }
                }
            } else {
                yield put({ type: types.SET_REGION, region: REGION_REST_OF_LATAM });
            }
        } catch (e) {
            yield put({ type: types.SET_REGION, region: REGION_REST_OF_LATAM });
        }
    }
}

function* goBackToLoginAndShowMessage({ message }) {
    yield put(notificationActions.showNotification(message, "error", ["login"]));
    yield put({ type: globalTypes.BACK_TO_STEP_0 });
    yield put(replace("/"));
}

function* reset() {
    yield put(routerActions.replace({ pathname: "/loginStep1" }));
}

function* handleFinalizeLogin({ response }) {
    const activeRegion = yield select(selectors.getRegion);
    const { isAdministrator, generalConditions } = response.data.data;
    let { _accessToken: accessToken } = response.data.data;
    const isAssistantLogin = yield select(assistantSelectors.isAssistantLogin);

    if (
        configUtils.get("core.sessionHandler.componentFQN") !==
        "com.technisys.omnichannel.core.session.DbSessionHandler"
    ) {
        accessToken = yield select(sessionSelectors.getAccessToken);
    }

    if (generalConditions) {
        const {
            generalConditionsShowExpiration,
            generalConditionsText,
            generalConditionsExpirationDate,
        } = response.data.data;
        const termsAndConditions = {
            // just joining the long named variables into a single object
            showExpiration: generalConditionsShowExpiration,
            generalConditions,
            body: generalConditionsText,
            expirationDate: generalConditionsExpirationDate,
        };

        yield put({ type: types.LOGIN_STEP_3_SUCCESS, termsAndConditions });
        if (activeRegion === REGION_USA) {
            const firstName = yield select(selectors.getUserFirstName);
            const email = yield select(selectors.getUsername);
            yield put({
                type: enrollmentTypes.SET_INVITATION,
                invitation: {
                    email,
                    firstName,
                    lastName: "",
                },
            });
            yield put(replace("/enrollment/Step3Part4"));
        } else {
            yield put(replace("/loginStep4"));
        }
    } else if (isAssistantLogin) {
        if (
            configUtils.get("core.sessionHandler.componentFQN") ===
            "com.technisys.omnichannel.core.session.DbSessionHandler"
        ) {
            if (accessToken.includes("|")) {
                const [splittedAccessToken] = accessToken.split("|");
                accessToken = splittedAccessToken;

                yield put(assistantActions.setMessengerPSID(splittedAccessToken[1]));
            }
        }
        yield put(assistantActions.setAccessToken(accessToken));
        yield put(replace("/loginStep5"));
    } else {
        const loginData = yield call(processLoginSuccess, response);
        const { environments, lang } = response.data.data;
        const { lastHref } = yield select((state) => state.status);

        configUtils.setRecaptchaLang(lang);
        yield put(i18nActions.setLang(lang));
        if (
            configUtils.get("core.sessionHandler.componentFQN") ===
            "com.technisys.omnichannel.core.session.DbSessionHandler"
        ) {
            yield call(session.setAuthToken, accessToken);
        }
        yield put({
            type: types.LOGIN_SUCCESS,
            environment: loginData.environment,
            user: loginData.user,
            environments,
            isAdministrator,
        });
        const { pepCompleted, irsCompleted } = loginData.user;
        if ((!pepCompleted || !irsCompleted) && activeRegion === REGION_USA) {
            yield put(replace("/pendingActions"));
        } else if (lastHref) {
            yield put(replace(lastHref));
            yield put(statusActions.deleteLastHref());
        } else {
            yield put(replace("/desktop"));
            yield put(statusActions.resetComeFromLogin());
        }
    }
}

function* handleLoginStep1Request({ email, shouldRememberEmail }) {
    const response = yield call(session.loginStep1, email);

    const { _exchangeToken, _securitySeal, _securitySealAlt, hasLogged } = response.data.data;

    if (response.data.code === "COR020W") {
        yield put(notificationActions.showNotification(response.data.message, "error", ["login"]));
        yield put({ type: types.LOGIN_FAILURE });
    } else {
        yield put({
            type: types.LOGIN_STEP_1_SUCCESS,
            exchangeToken: _exchangeToken,
            securitySeal: _securitySeal,
            securitySealAlt: _securitySealAlt,
            username: email,
            hasLogged,
            shouldRememberEmail,
        });

        yield put(replace("/loginStep2"));
    }
}

function* handleLoginStep2Request({ password, recaptchaResponse, formikBag }) {
    const exchangeToken = yield select(selectors.getExchangeToken);

    if (exchangeToken === NOTOKENLOGIN) {
        formikBag.setSubmitting(false);
        yield put(
            notificationActions.showNotification(i18n.get("login.dataEntered.notValid"), "error", ["loginStep2"]),
        );
        yield put({
            type: formTypes.SEND_FORM_DATA_FAILURE,
            idForm: "loginStep2",
            code: "COR020W",
            errors: null,
        });
        return;
    }
    const response = yield call(session.loginStep2, exchangeToken, password, recaptchaResponse);
    if (response.type === "W") {
        formikBag.setErrors(adjustIdFieldErrors(response.data.data));
        formikBag.setSubmitting(false);

        if (
            response.data.code === "COR020W" ||
            response.data.code === "API019W" ||
            response.data.code === "API020W" ||
            response.data.code === "COR050W" ||
            response.data.code === "COR029E" ||
            response.data.code === "COR047E"
        ) {
            // Wrong credentials || capcha required || capcha invalid
            yield put(notificationActions.showNotification(response.data.message, "error", ["loginStep2"]));
            yield put({ type: types.LOGIN_STEP_2_FAILURE });
            if (response.data.code === "API020W" || response.data.code === "COR050W") {
                // Capcha required || capcha invalid
                yield put({ type: types.LOGIN_FAILURE_REQUIRE_CAPTCHA });
            }

            yield put({
                type: formTypes.SEND_FORM_DATA_FAILURE,
                idForm: "loginStep2",
                code: response.data.code,
                errors: response.data.data,
            });
        } else {
            // exchangeToken expired, restart flow
            yield put(notificationActions.showNotification(response.data.message, "error", ["login"]));
            yield put({ type: globalTypes.BACK_TO_STEP_0 });
            yield put(replace("/"));
        }
    } else {
        const {
            environments,
            selectEnvironment,
            lang,
            _userFirstName,
            _userFullName,
            defEnvironmentEnabled,
        } = response.data.data;

        if (!defEnvironmentEnabled) {
            yield put(
                notificationActions.showNotification(i18n.get("settings.defaultEnvironment.blockedMessage"), "error", [
                    "loginStep3",
                ]),
            );
        }

        const rememberUser = yield select(selectors.getShouldRememberEmail);

        // Remember user
        if (rememberUser) {
            const userName = yield select(selectors.getUsername);
            yield put(
                loginActions.setRememberedUser({
                    username: userName,
                    userFirstName: _userFirstName,
                    userFullName: _userFullName,
                }),
            );
        }

        yield put({
            type: types.LOGIN_STEP_2_SUCCESS,
            lang,
            _userFirstName,
            _userFullName,
            environments,
        });

        if (selectEnvironment && Object.keys(environments).length > 1) {
            yield put(replace("/loginStep3"));
        } else {
            yield put({
                type: types.LOGIN_STEP_3_REQUEST,
                formikBag,
            });
        }
    }
}

function* handleOauthRequest({ password, recaptchaResponse, formikBag }) {
    const exchangeToken = yield select(selectors.getExchangeToken);
    const username = yield select(selectors.getUsername);
    const isFromMessenger = yield select(assistantSelectors.isFromMessenger);
    // eslint-disable-next-line camelcase
    const { redirect_uri } = yield select(assistantSelectors.getParams);
    const isFromWhatsapp = yield select(assistantSelectors.isFromWhatsapp);

    let response;
    if (isFromMessenger || isFromWhatsapp) {
        const thirdPartyToken = yield select(assistantSelectors.getThirdPartyToken);
        response = yield call(
            session.thirdPartyOauth,
            username,
            password,
            thirdPartyToken,
            redirect_uri,
            exchangeToken,
            recaptchaResponse,
        );
        if (response.data) {
            response.data.access_token = thirdPartyToken;
        } else {
            response.data = {
                access_token: thirdPartyToken,
            };
        }
    } else {
        response = yield call(session.oauth, username, password, exchangeToken);
    }

    if (response.type === "W") {
        formikBag.setErrors(adjustIdFieldErrors({}));
        formikBag.setSubmitting(false);
        const errorMsg = i18n.get(`returnCode.${response.data.error_description}`, response.data.error_description);
        if (
            response.data.error_description === "COR020W" ||
            response.data.error_description === "API019W" ||
            response.data.error_description === "API020W" ||
            response.data.error_description === "COR050W" ||
            response.data.error_description === "COR029E" ||
            response.data.error_description === "COR047E"
        ) {
            // Wrong credentials || captcha required || captcha invalid
            yield put(notificationActions.showNotification(errorMsg, "error", ["loginStep2"]));

            if (response.data.error_description === "API020W" || response.data.error_description === "COR050W") {
                // Captcha required || captcha invalid
                yield put({ type: types.LOGIN_FAILURE_REQUIRE_CAPTCHA });
            }

            yield put({
                type: formTypes.SEND_FORM_DATA_FAILURE,
                code: response.data.error_description,
            });
            yield put({ type: types.LOGIN_OAUTH_FAILURE });
        } else {
            // exchangeToken expired, restart flow
            yield put(notificationActions.showNotification(errorMsg, "error", ["login"]));
            yield put({ type: globalTypes.BACK_TO_STEP_0 });
            yield put(replace("/"));
        }
    } else {
        // Ignored because it's the OAUTH standard
        // eslint-disable-next-line camelcase
        const { access_token, refresh_token } = response.data;
        if (!isFromMessenger && !isFromWhatsapp) {
            session.setAuthToken(access_token);
            yield put(sessionActions.setTokens(access_token, refresh_token));
        } else {
            yield put(assistantActions.setAccessToken(access_token));
            yield put(replace("/loginStep5"));
            return;
        }
        const environmentsResponse = yield call(session.listEnvironments);
        const {
            environments,
            selectEnvironment,
            lang,
            _userFirstName,
            _userFullName,
            defEnvironmentEnabled,
        } = environmentsResponse.data.data;

        if (!defEnvironmentEnabled) {
            yield put(
                notificationActions.showNotification(i18n.get("settings.defaultEnvironment.blockedMessage"), "error", [
                    "loginStep3",
                ]),
            );
        }

        const rememberUser = yield select(selectors.getShouldRememberEmail);

        // Remember user
        if (rememberUser) {
            const userName = yield select(selectors.getUsername);
            yield put(
                loginActions.setRememberedUser({
                    username: userName,
                    userFirstName: _userFirstName,
                    userFullName: _userFullName,
                }),
            );
        }

        yield put({
            type: types.LOGIN_OAUTH_SUCCESS,
            lang,
            _userFirstName,
            _userFullName,
            environments,
        });

        if (selectEnvironment && Object.keys(environments).length > 1) {
            yield put(replace("/loginStep3"));
        } else {
            yield put({
                type: types.LOGIN_STEP_3_REQUEST,
                formikBag,
            });
        }
    }
}

function* handleLoginStep3Request({ idEnvironment, rememberEnvironment, formikBag }) {
    const exchangeToken = yield select(selectors.getExchangeToken);
    const isFromMessenger = yield select(assistantSelectors.isFromMessenger);
    const isFromGoogle = yield select(assistantSelectors.isFromGoogle);
    const isFromAmazon = yield select(assistantSelectors.isFromAmazon);
    const isFromWhatsapp = yield select(assistantSelectors.isFromWhatsapp);
    const clientNumber = yield select(assistantSelectors.getClientNumber);
    const accountLinkingToken = yield select(assistantSelectors.getAccountLinkingToken);

    const position = yield call(currentLocation, { lat: 0.0, lng: 0.0 });
    const location = {
        latitude: position.lat,
        longitude: position.lng,
    };

    let extraInfo = {};
    let idDevice = null;
    let registrationId = null;

    if (window.app) {
        const { registrationId: registrationIdResult } = yield call(window.pushNotifications.isEnabled);
        extraInfo = window.device;
        extraInfo.isTablet = false;
        extraInfo.isIOS = deviceUtils.getMobileOS(deviceUtils.getDisplay()) === deviceUtils.IOS_DEVICE;
        extraInfo.isAndroid = deviceUtils.getMobileOS(deviceUtils.getDisplay()) === deviceUtils.ANDROID_DEVICE;
        extraInfo.uuid = window.app.getDeviceUUID();
        registrationId = registrationIdResult;
        idDevice = window.app.getDeviceUUID();
    } else {
        idDevice = yield deviceUtils.getDeviceFingerprint();
        extraInfo = yield deviceUtils.getExtraInfo();
    }
    yield call(session.registerUserDevice, exchangeToken, idDevice, registrationId, JSON.stringify(extraInfo));

    const response = yield call(
        session.loginStep3,
        exchangeToken,
        idEnvironment,
        rememberEnvironment,
        location,
        idDevice,
        {
            isFromMessenger,
            isFromGoogle,
            isFromAmazon,
            isFromWhatsapp,
            accountLinkingToken,
            clientNumber,
        },
    );

    if (response.type === "W") {
        const { code, data } = response.data;
        if (code === "COR020W" && data.environment) {
            // mark the environment as disabled and redirecting the user to the step 3 again
            const environments = yield select(selectors.getEnvironments);
            if (environments[idEnvironment]) {
                environments[idEnvironment].allowedToAccess = false;
                yield put({ type: types.MARK_ENVIRONMENTS_DISABLED, environments });
            }
            yield put(replace("/loginStep3"));
        } else {
            // en este paso lo unico que podria suceder es que se venciera el exchange token
            formikBag.setErrors(adjustIdFieldErrors(response.data.data));
            formikBag.setSubmitting(false);
            yield put(notificationActions.showNotification(response.data.message, "error", ["login"]));
            yield put({ type: globalTypes.BACK_TO_STEP_0 }); // por lo que borro lo que tengo de sesion y voy al step0
            yield put(replace("/"));
        }
    } else {
        const {
            data: {
                data: { enabledAssistant },
            },
        } = response;
        yield put({ type: types.FINALIZE_LOGIN, response });
        yield put({ type: sessionTypes.SET_ENABLED_ASSISTANT, enabledAssistant });
    }
}

function* handleLoginStep4Request({ acceptConditions, formikBag }) {
    const activeRegion = yield select(selectors.getRegion);
    const exchangeToken = yield select(selectors.getExchangeToken);
    const idEnvironmentToAcceptConditions = yield select(selectors.getIdEnvironmentToAcceptConditions);
    const response = yield call(session.loginStep4, exchangeToken, acceptConditions, idEnvironmentToAcceptConditions);

    if (response.type === "W") {
        if (formikBag) {
            formikBag.setErrors(adjustIdFieldErrors(response.data.data));
        }
        if (formikBag) {
            formikBag.setSubmitting(false);
        }
        yield put(notificationActions.showNotification(response.data.message, "error", ["loginStep4"]));
        yield put({ type: types.LOGIN_FAILURE, errors: response.data.data });
    } else {
        const loginData = yield call(processLoginSuccess, response);
        const { data } = response.data;
        const { lastHref } = yield select((state) => state.status);

        configUtils.setRecaptchaLang(data.lang);
        yield put(i18nActions.setLang(data.lang));

        if (
            configUtils.get("core.sessionHandler.componentFQN") ===
            "com.technisys.omnichannel.core.session.DbSessionHandler"
        ) {
            // eslint-disable-next-line no-underscore-dangle
            yield call(session.setAuthToken, data._accessToken);
        }
        yield put({
            type: types.LOGIN_SUCCESS,
            environment: loginData.environment,
            user: loginData.user,
            environments: data.environments,
            isAdministrator: data.isAdministrator,
        });

        const { pepCompleted, irsCompleted } = loginData.user;

        if ((!pepCompleted || !irsCompleted) && activeRegion === REGION_USA) {
            yield put(replace("/pendingActions"));
        } else if (lastHref) {
            // si la sesion expiró y se guardó la url en la que se estaba voy para ahi
            yield put(replace(lastHref));
            yield put(statusActions.deleteLastHref());
        } else {
            yield put(statusActions.resetComeFromLogin());
            // si no, voy a desktop
            yield put(replace("/desktop"));
        }
    }
}

function processLoginSuccess(response) {
    const {
        _accessToken,
        _securitySeal,
        username,
        userFullName,
        userId,
        previousLoginInfo,
        defaultAvatarId,
        email,
        clients,
        signedTAndCDate,
        irsDate,
        pepCompleted,
        irsCompleted,
        ssnid,
        ...data
    } = response.data.data;
    const user = {
        defaultAvatarId,
        previousLoginInfo,
        userFullName,
        userId,
        email,
        accessToken: _accessToken,
        securitySeal: _securitySeal,
        signedTAndCDate,
        irsDate,
        pepCompleted,
        irsCompleted,
        ssnid,
        idDefaultEnvironment:
            data.idDefaultEnvironment && data.idDefaultEnvironment > 0 ? data.idDefaultEnvironment : null,
    };

    let forms = null;
    if (data.forms) {
        forms = {};

        for (let i = 0; i < data.forms.length; i++) {
            let category = forms[data.forms[i].category];
            if (!category) {
                category = [];
                forms[data.forms[i].category] = category;
            }
            category.push(data.forms[i]);
        }
    }

    const environment = {
        permissions: data.permissions,
        forms,
        name: data.activeEnvironmentName,
        type: data.activeEnvironmentType,
        id: data.activeIdEnvironment,
        administrationScheme: data.administrationScheme,
        clients,
    };

    return { user, environment };
}

function* handleFingerprintLoginPre() {
    let fingerprintAuthToken = null;
    let fingerprintAuthTokenExists = true;

    const position = yield call(currentLocation, { lat: 0.0, lng: 0.0 });
    const location = {
        latitude: position.lat,
        longitude: position.lng,
    };

    try {
        fingerprintAuthToken = yield call(secureStorageUtils.get, "fingerprintAuthToken");
    } catch (error) {
        fingerprintAuthTokenExists = false;
    }

    if (fingerprintAuthTokenExists) {
        const fingerPrintLoginFail = yield select((state) => state.session.fingerprintLoginFail);

        if (!fingerPrintLoginFail) {
            try {
                yield call(fingerprintUtils.verify);

                let response;
                if (
                    configUtils.get("core.sessionHandler.componentFQN") ===
                    "com.technisys.omnichannel.core.session.DbSessionHandler"
                ) {
                    response = yield call(session.checkFingerprintSession, fingerprintAuthToken);

                    if (response && response.status === 200) {
                        const { existSessionWithFingerPrint } = response.data.data;

                        if (!existSessionWithFingerPrint) {
                            yield put(
                                notificationActions.showNotification(
                                    i18n.get("login.fingerprint.session.expired"),
                                    "error",
                                    ["login"],
                                ),
                            );
                            yield call(secureStorageUtils.remove, "fingerprintAuthToken");
                            yield call(secureStorageUtils.remove, "fingerprintUsername");
                            yield put({ type: types.BACK_TO_STEP_0 });
                            yield put(replace("/loginStep0"));
                        } else {
                            yield put({ type: types.FINGERPRINT_LOGIN_SUBMIT });

                            response = yield call(session.fingerprintLogin, location);
                            const { code, data } = response.data;
                            if (code === "COR020W") {
                                yield put({ type: types.LOGIN_FINGERPRINT_FAILURE });
                                yield put({ type: types.SET_FINGERPRINT_ENVIRONMENT_RESTRICTED });
                                yield put(notificationActions.showNotification(data.NO_FIELD, "error", ["login"]));
                            } else if (response && response.status === 200) {
                                yield put({ type: types.FINALIZE_LOGIN, response, isFingerprintLogin: true });
                            }
                        }
                    }
                } else {
                    const username = yield call(secureStorageUtils.get, "fingerprintUsername");
                    response = yield call(session.fingerprintOauth, username, fingerprintAuthToken);
                    if (response.status === 200) {
                        yield put({ type: types.FINGERPRINT_LOGIN_SUBMIT });

                        // eslint-disable-next-line camelcase
                        const { access_token, refresh_token } = response.data;
                        session.setAuthToken(access_token);
                        yield put(sessionActions.setTokens(access_token, refresh_token));

                        response = yield call(session.fingerprintLogin, location);
                        const { code, data } = response.data;

                        if (code === "COR020W") {
                            yield put({ type: types.LOGIN_FINGERPRINT_FAILURE });
                            yield put({ type: types.SET_FINGERPRINT_ENVIRONMENT_RESTRICTED });
                            yield put(notificationActions.showNotification(data.NO_FIELD, "error", ["login"]));
                        } else if (response && response.status === 200) {
                            yield put({ type: types.FINALIZE_LOGIN, response, isFingerprintLogin: true });
                        }
                    }
                }
            } catch (error) {
                if (error === fingerprintUtils.fingerprintErrors.FINGERPRINT_ERROR) {
                    yield put({ type: types.LOGIN_FINGERPRINT_FAILURE });
                    const mess = `${i18n.get("settings.fingerprintConfiguration.dialog.error_1")}\n${i18n.get(
                        "settings.fingerprintConfiguration.dialog.error_2",
                    )}`;
                    yield put(notificationActions.showNotification(mess, "error", ["login"]));
                } else if (error !== fingerprintUtils.fingerprintErrors.FINGERPRINT_CANCELLED) {
                    // eslint-disable-next-line no-console
                    console.log("Fingerprint - The following error has ocurred: ", error);
                    if (error.status && error.status === 200) {
                        yield put({ type: types.LOGIN_FINGERPRINT_FAILURE });
                        yield put(
                            notificationActions.showNotification(
                                i18n.get("login.fingerprint.session.wrongAccess"),
                                "error",
                                ["login"],
                            ),
                        );
                    } else if (error.response && error.response.status && error.response.status === 401) {
                        yield put(
                            notificationActions.showNotification(
                                i18n.get("login.fingerprint.session.expired"),
                                "error",
                                ["login"],
                            ),
                        );
                        yield call(secureStorageUtils.remove, "fingerprintAuthToken");
                        yield put({ type: types.BACK_TO_STEP_0 });
                        yield put(replace("/"));
                    }
                }
            }
        }
    }
}
