import { call, put, takeLatest } from "redux-saga/effects";
import { routerActions } from "react-router-redux";

import { actions as notificationActions } from "reducers/notification";
import { actions as userActions } from "reducers/administration/users";
import * as form from "middleware/form";
import * as i18n from "util/i18n";
import { credentialsToUnderscoreFormat } from "util/form.js";

const loadDetailsRequest = (middleware, actions) =>
    function* loadRequest({ id }) {
        const response = yield call(middleware.loadDetailsRequest, id);

        if (response.type === "W") {
            yield put(actions.loadDetailsFailure());
            yield put(
                notificationActions.showNotification(i18n.get("global.unexpectedError"), "error", ["administration"]),
            );
        } else {
            const responseCredentials = yield call(
                form.listCredentialsGroups,
                null,
                "administration.medium.modify.signature.send",
            );
            const credentialGroups = responseCredentials.data.data.groups;

            const userExtendedInfo = {
                idUser: response.data.data.user.idUser,
                status: response.data.data.userEnvStatus,
                signatureLevel: response.data.data.signatureLevel,
                massiveEnabled: response.data.data.hasMassiveEnabled,
                dispatcher: response.data.data.dispatcher,
            };
            yield put(userActions.loadSingleSuccess(userExtendedInfo));

            yield put(actions.loadChannelsSuccess({ ...response.data.data }));
            yield put(actions.loadDetailsSuccess({ ...response.data.data, credentialGroups }));
            if (actions.loadPermissionsSuccess !== undefined) {
                yield put(actions.loadPermissionsSuccess({ ...response.data.data }));
            }
            if (actions.loadGroupsSuccess !== undefined) {
                yield put(actions.loadGroupsSuccess(response.data.data));
            }
        }
    };

const updateSignatureRequest = (middleware, actions) =>
    function* updateRequest({ data, formikBag }) {
        const { signatureLevel, credentials, ...rest } = data;
        const credentialsWithUnderscore = credentialsToUnderscoreFormat(credentials);
        const response = yield call(middleware.updateSignatureRequest, {
            ...rest,
            ...credentialsWithUnderscore,
            signatureLevel,
        });
        const { setSubmitting, setErrors } = formikBag;

        setSubmitting(false);

        if (response.type === "W") {
            const errorMessage = response.data.data.NO_FIELD || i18n.get("global.unexpectedError");

            setErrors(response.data.data);
            yield put(notificationActions.showNotification(errorMessage, "error", ["administration"]));
        } else {
            yield put(notificationActions.showNotification(response.data.message, "success", ["administration"]));
            yield put(routerActions.goBack());
            yield put(actions.updateChannelsRequestSuccess());
        }
    };

const sagasCreator = (middleware, types, actions) => [
    takeLatest(types.LOAD_DETAILS_REQUEST, loadDetailsRequest(middleware, actions)),
    takeLatest(types.UPDATE_SIGNATURE_REQUEST, updateSignatureRequest(middleware, actions)),
];

export default sagasCreator;
