import { call, put, takeLatest } from "redux-saga/effects";

import { actions as notificationActions } from "reducers/notification";
import * as i18n from "util/i18n";
import * as form from "middleware/form";

const sagasCreator = (middleware, types, actions) => [
    takeLatest(types.LOAD_PERMISSIONS_TICKET_REQUEST, loadPermissionsTicketRequest(middleware, actions)),
];

export default sagasCreator;

const loadPermissionsTicketRequest = (middleware, actions) =>
    function*({ idTransaction }) {
        const transactionResponse = yield call(form.readTransaction, idTransaction);

        if (transactionResponse.type === "W") {
            yield put(actions.loadPermissionsTicketFailure());
            yield put(
                notificationActions.showNotification(i18n.get("global.unexpectedError"), "error", ["administration"]),
            );
        } else {
            const { idUser, permissions } = transactionResponse.data.data.transaction.data;
            const permissionsResponse = yield call(middleware.loadPermissionsRequest, idUser);

            if (permissionsResponse.type === "W") {
                yield put(actions.loadPermissionsTicketFailure());
                yield put(
                    notificationActions.showNotification(i18n.get("global.unexpectedError"), "error", [
                        "administration",
                    ]),
                );
            } else {
                yield put(actions.loadPermissionsTicketSuccess());
                yield put(actions.loadPermissionsSuccess({ ...permissionsResponse.data.data, permissions }));
            }
        }
    };
