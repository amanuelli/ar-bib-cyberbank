import { call, put, takeLatest } from "redux-saga/effects";
import { routerActions } from "react-router-redux/actions";

import * as form from "middleware/form";
import { actions as notificationActions } from "reducers/notification";
import * as i18n from "util/i18n";
import { credentialsToUnderscoreFormat } from "util/form.js";

const loadChannelsRequest = (middleware, actions) =>
    function* loadChannelsRequestSaga({ id }) {
        const response = yield call(middleware.loadChannelsRequest, id);

        if (response.type === "W") {
            yield put(actions.loadChannelsFailure());
            yield put(
                notificationActions.showNotification(i18n.get("global.unexpectedError"), "error", ["administration"]),
            );
        } else {
            yield put(actions.loadChannelsSuccess({ ...response.data.data }));
        }
    };

const mapChannelsFormData = ({ idUser, caps }) => ({
    idUser,
    ...Object.entries(caps).reduce(
        ({ enabledChannels, maxAmounts, capFrequencies }, [channel, { amount, frequency }]) => ({
            enabledChannels: [...enabledChannels, channel === "topAmount" ? "all" : channel],
            maxAmounts: [...maxAmounts, amount],
            capFrequencies: [...capFrequencies, frequency],
        }),
        {
            enabledChannels: [],
            maxAmounts: [],
            capFrequencies: [],
        },
    ),
});

const updateChannelsPreview = (middleware, actions) =>
    function* updateChannelsPreviewSaga({ data, setSubmitting }) {
        const response = yield call(middleware.updateChannelsPreview, mapChannelsFormData(data));

        setSubmitting(false);

        if (response.type === "W") {
            const errorMessage = response.data.data.NO_FIELD || i18n.get("global.unexpectedError");

            yield put(notificationActions.showNotification(errorMessage, "error", ["administration"]));
        } else {
            const responseCredentials = yield call(
                form.listCredentialsGroups,
                null,
                "administration.medium.modify.channels.send",
            );
            const credentialGroups = responseCredentials.data.data.groups;
            yield put(actions.onSuccess(data.idUser));
            yield put(actions.updateChannelsPreviewSuccess(data.caps, credentialGroups));
        }
    };

const updateChannelsRequest = (middleware, actions) =>
    function* updateChannelsRequestSaga({ data, formikBag }) {
        const { credentials, ...restOfParams } = data;
        const credentialsWithUnderscore = credentialsToUnderscoreFormat(credentials);
        const response = yield call(middleware.updateChannelsRequest, {
            ...credentialsWithUnderscore,
            ...mapChannelsFormData(restOfParams),
        });
        const { setSubmitting, setErrors } = formikBag;

        setSubmitting(false);

        if (response.type === "W") {
            const errorMessage = response.data.data.NO_FIELD || i18n.get("global.unexpectedError");

            setErrors(response.data.data);
            yield put(notificationActions.showNotification(errorMessage, "error", ["administration"]));
        } else {
            yield put(notificationActions.showNotification(response.data.message, "success", ["administration"]));
            yield put(routerActions.go(-2));
            yield put(actions.updateChannelsRequestSuccess());
        }
    };

const sagasCreator = (middleware, types, actions) => [
    takeLatest(types.LOAD_CHANNELS_REQUEST, loadChannelsRequest(middleware, actions)),
    takeLatest(types.UPDATE_CHANNELS_PREVIEW, updateChannelsPreview(middleware, actions)),
    takeLatest(types.UPDATE_CHANNELS_REQUEST, updateChannelsRequest(middleware, actions)),
];

export default sagasCreator;
