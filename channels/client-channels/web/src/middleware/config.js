import * as API from "middleware/api";

export const listConfiguration = () => API.executeAnonymous("/configuration.listConfiguration");
