import * as API from "middleware/api";

export const listRequest = (widget) => API.executeWithAccessToken(`/widgets.${widget}`);
