import * as API from "middleware/api";

export const detailRequest = (idCreditCard, idStatement) =>
    API.executeWithAccessToken("/creditCard.listStatementDetails", { idCreditCard, idStatement });

export const updateNoteRequest = (idProduct, idStatement, note) =>
    API.executeWithAccessToken("/notes.editStatementNote", { idProduct, idStatement, note });
