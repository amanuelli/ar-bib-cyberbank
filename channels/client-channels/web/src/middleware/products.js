import * as API from "middleware/api";

export const changeProductAlias = (alias, idProduct) =>
    API.executeWithAccessToken("/core.product.changeAlias", {
        alias,
        idProduct,
    });
