import axios from "axios";
import { version } from "../../package.json";

const getAxiosObject = (url) => {
    const axiosObject = axios.create();
    axiosObject.defaults.baseURL = url;
    axiosObject.defaults.headers.Accept = "application/json, application/octet-stream";

    if (window.cordova) {
        axiosObject.defaults.headers["x-app-version"] = version;
    }

    return axiosObject;
};
export default getAxiosObject;
