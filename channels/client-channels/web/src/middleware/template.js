import * as API from "middleware/api";

export const loadList = (idForm) => API.executeWithAccessToken("/core.listTransactionTemplates", { idForm });

export const deleteItem = ({ idTransactionTemplate }) =>
    API.executeWithAccessToken("/core.deleteTransactionTemplate", { idTransactionTemplate });
