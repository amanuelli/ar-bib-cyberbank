import * as API from "middleware/api";

export const listMessages = (lang, etag) => API.executeAnonymous("/messages.listMessages", { lang }, etag);
