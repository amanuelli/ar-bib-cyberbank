import * as API from "middleware/api";

export const get = (params) =>
    API.executeAnonymous("/pointsofinterest.list", {
        ...params,
    });

export const getTypes = () => API.executeAnonymous("/pointsofinterest.type.list");
