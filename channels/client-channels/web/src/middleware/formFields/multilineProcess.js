import * as API from "middleware/api";

export const updateProcessRequest = (data) => API.executeWithAccessToken("/pay.multiline.salary.status", data);

export default { updateProcessRequest };
