import * as API from "middleware/api";

export const listRequest = () => API.executeWithAccessToken("/creditCards.list");
