import * as API from "middleware/api";

export const loadLayoutRequest = () => API.executeWithAccessToken("/desktop.loadLayout");

export const saveLayoutRequest = (widgets) => API.executeWithAccessToken("/desktop.saveLayout", { widgets });

export const loadCorporateGroupDesktop = (filters) =>
    API.executeWithAccessToken("/desktop.corporateGroup.accounts", filters);
