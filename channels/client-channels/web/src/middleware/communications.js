import * as API from "middleware/api";

export const latestCommunications = () => API.executeWithAccessToken("/communications.latestCommunicationThreads");

export const listTypes = () => API.executeWithAccessToken("/communications.listPre");

export const list = (filters) => API.executeWithAccessToken("/communications.list", filters);
