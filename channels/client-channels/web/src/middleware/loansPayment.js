import * as API from "middleware/api";

export const loadListRequest = () => API.executeWithAccessToken("/loans.list");
