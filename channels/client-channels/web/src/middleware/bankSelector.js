import * as API from "middleware/api";

export const loadListRequest = (filters) => API.executeWithAccessToken("/banks.list", filters);
