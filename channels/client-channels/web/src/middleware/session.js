import * as API from "middleware/api";
import * as restAPI from "middleware/apiRESTful";
import * as OAUTH from "middleware/oauth";
import * as deviceUtils from "util/device";
import * as configUtils from "util/config";

export const loginStep1 = (email) =>
    API.executeAnonymous("/session.login.legacy.step1", {
        userEmail: email,
    });

export const loginStep2 = (exchangeToken, password, recaptchaResponse) =>
    API.executeWithExchangeToken(
        "/session.login.legacy.step2",
        {
            _password: password,
            _captcha: recaptchaResponse,
        },
        exchangeToken,
    );

export const oauth = (email, password, exchangeToken = "") =>
    OAUTH.executePasswordGrant(email, `${password}#${exchangeToken}#`);

export const thirdPartyOauth = (
    email,
    password,
    thirdPartyToken,
    redirectUri,
    exchangeToken = "",
    recaptchaResponse = "",
) =>
    OAUTH.executeImplicit(
        "assistant-client",
        redirectUri,
        thirdPartyToken,
        email,
        `${password}#${exchangeToken}#${recaptchaResponse}`,
    );

export const fingerprintOauth = (email, fingerprintAuthToken) =>
    OAUTH.executePasswordGrant(
        email,
        `fingerprint#${fingerprintAuthToken}#${deviceUtils.getDeviceId()}#${deviceUtils.getDeviceModel()}`,
    );

export const listEnvironments = () => API.executeWithAccessToken("session.login.oauth.step2b");

export const loginStep3 = (exchangeToken, environment, setAsDefault, location, idDevice, assistant) => {
    if (
        configUtils.get("core.sessionHandler.componentFQN") ===
        "com.technisys.omnichannel.core.session.DbSessionHandler"
    ) {
        return API.executeWithExchangeToken(
            "/session.login.legacy.step3",
            {
                environment,
                setAsDefault,
                idDevice,
                ...location,
                ...assistant,
            },
            exchangeToken,
        );
    }
    return API.executeWithAccessToken("/session.login.oauth.step3", {
        environment,
        setAsDefault,
        idDevice,
        ...location,
        ...assistant,
    });
};
export const loginStep4 = (exchangeToken, acceptConditions, idEnvironmentToAcceptConditions) => {
    if (
        configUtils.get("core.sessionHandler.componentFQN") ===
        "com.technisys.omnichannel.core.session.DbSessionHandler"
    ) {
        return API.executeWithExchangeToken(
            "/session.login.legacy.step4",
            {
                acceptConditions,
            },
            exchangeToken,
        );
    }
    return API.executeWithAccessToken("/session.login.oauth.step4", {
        acceptConditions,
        idEnvironmentToAcceptConditions,
    });
};

export const logout = () => {
    const response = API.executeWithAccessToken("/session.logout");

    API.setAuthToken(null);
    restAPI.setAuthToken(null);
    return response;
};

export const setAuthToken = (token) => {
    API.setAuthToken(token);
    restAPI.setAuthToken(token);
};

export const changeEnvironment = (idEnvironmentToChange, setAsDefault) =>
    API.executeWithAccessToken("/session.changeEnvironment", {
        idEnvironmentToChange,
        setAsDefault,
    });

export const check = () => API.executeWithAccessToken("/session.get");

export const extend = () => API.executeWithAccessToken("/session.extend");

export const checkFingerprintSession = (_accessToken) => {
    API.setAuthToken(_accessToken);
    restAPI.setAuthToken(_accessToken);

    return API.executeWithAccessToken("/fingerprint.sessionExist");
};

export const fingerprintLogin = (location) => API.executeWithAccessToken("/login.fingerprint", { ...location });

export const registerUserDevice = (exchangeToken, idDevice, pushToken, extraInfo) => {
    if (
        configUtils.get("core.sessionHandler.componentFQN") ===
        "com.technisys.omnichannel.core.session.DbSessionHandler"
    ) {
        return API.executeWithExchangeToken(
            "/session.login.legacy.registerUserDevice",
            {
                idDevice,
                pushToken,
                extraInfo,
            },
            exchangeToken,
        );
    }
    return API.executeWithAccessToken(
        "/session.login.oauth.registerUserDevice",
        {
            idDevice,
            pushToken,
            extraInfo,
        },
        exchangeToken,
    );
};

export const getClientCountry = () => API.executeAnonymous("/session.getUserCountry", null, 2000);
