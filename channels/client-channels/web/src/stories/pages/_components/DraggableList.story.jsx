import React from "react";
import Container from "pages/_components/Container";
import DraggableList from "pages/_components/DraggableList";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";

const noop = () => {};

const columns = {
    0: [
        {
            id: "example",
            uri: "widgets/example",
            column: 0,
            row: 3,
        },
        {
            id: "example2",
            uri: "widgets/example2",
            column: 0,
            row: 2,
        },
        {
            id: "example3",
            uri: "widgets/example3",
            column: 0,
            row: 1,
        },
    ],
};

newStory("Components").add(
    "DraggableList",
    () => {
        const { Column, ColumnBody } = Container;
        return (
            <Provider store={store}>
                <Container>
                    <Column>
                        <ColumnBody>
                            <DraggableList
                                columns={columns}
                                itemRenderer={(item, { draggableItemProps }) => (
                                    <div {...draggableItemProps}>{item.id}</div>
                                )}
                                onItemsPositionChange={noop}
                                isDragEnabled
                            />
                        </ColumnBody>
                    </Column>
                </Container>
            </Provider>
        );
    },
    {
        info: {
            propsTable: [DraggableList],
            propTablesExclude: [Provider, Container, Container.Column, Container.ColumnBody],
            text: `
        ## DraggableList

        A list of items that can be rearranged. 

        Items must be presented in the format: 
        ~~~js
        {
          [columnID]: [
            {
              ...itemProperties,
              column: 0,
              row: 1
            },
            {
              ...item2Properties,
              column: 0,
              row: 2
            },
            {
              ...item3Properties,
              column: 0,
              row: 3
            }
          ]
        }        
        ~~~

        Usage: 
        ~~~js

        const columns = {
          0: [
            {
              id: "example",
              uri: "widgets/example",
              column: 0,
              row: 3
            },
            {
              id: "example2",
              uri: "widgets/example2",
              column: 0,
              row: 2
            },
            {
              id: "example3",
              uri: "widgets/example3",
              column: 0,
              row: 1
            }
          ]
        };        

          <DraggableList
            columns={columns}
            itemRenderer={(item, { draggableItemProps }) => (
              <div {...draggableItemProps}>{item.id}</div>
            )}
            onItemsPositionChange={change => console.log({ change })}
            isDragEnabled
        />
        ~~~
    `,
        },
    },
);
