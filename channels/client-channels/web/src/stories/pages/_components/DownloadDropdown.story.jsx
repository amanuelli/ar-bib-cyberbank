import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";
import { store } from "store";
import { action } from "@storybook/addon-actions";

import DownloadDropdown from "pages/_components/DownloadDropdown";
import newStory from "stories/util/newStory";

newStory("Components").add(
    "DownloadDropdown",
    () => {
        const downloadFormats = ["txt", "xls", "pdf"];
        return (
            <Provider store={store}>
                <BrowserRouter>
                    <Route
                        render={() => (
                            <DownloadDropdown
                                onDownloadClick={action("Download event handler")}
                                formats={downloadFormats}
                            />
                        )}
                    />
                </BrowserRouter>
            </Provider>
        );
    },
    {
        info: {
            propsTable: [DownloadDropdown],
            propTablesExclude: [Provider, Route],
            text: `
        ## DownloadDropdown

        A dropdown used in the header of a page to download its contents in different formats

        Usage:
        ~~~js
            const downloadFormats = ["txt", "xls", "pdf"];
            function handleDownloadClick(format) {
                // Handle the download click in the received format
            }
            <DownloadDropdown onDownloadClick={handleDownloadClick} formats={downloadFormats} />
        ~~~
    `,
        },
    },
);
