import React from "react";

import newStory from "stories/util/newStory";
import Hero from "pages/_components/Hero";

newStory("Components").add("Hero", () => <Hero>Important information to show under the navigation</Hero>, {
    info: {
        propsTable: [Hero],
        text: `
        ## Hero

        A container used to show information between the page's Head and the main content.

        Usage:
        ~~~js
        <Hero>Important information to show under the navigation</Hero>
        ~~~
    `,
    },
});
