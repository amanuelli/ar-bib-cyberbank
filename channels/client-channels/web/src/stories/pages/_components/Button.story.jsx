import React from "react";
import Button from "pages/_components/Button";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { text, boolean, select } from "@storybook/addon-knobs";
import { store } from "store";

newStory("Components").add(
    "Button",
    () => (
        <Provider store={store}>
            <div className="component-wrapper">
                <Button
                    id={text("Button ID", "button")}
                    label={text("I18n-aware label", "administration.users.cantRemoveChannel")}
                    type={select("Button Type", ["button", "reset", "submit"], "button")}
                    bsStyle={select(
                        "Bootstrap Button Type",
                        ["primary", "success", "info", "warning", "danger", "link"],
                        "primary",
                    )}
                    loading={boolean("Loading?", false)}
                    disabled={boolean("Disabled?", false)}
                    replace={{
                        [text("Variable name in i18n text", "CHANNELS")]: text("Example replacement text", '"Ejemplo"'),
                    }}>
                    This is actually irrelevant
                </Button>
            </div>
        </Provider>
    ),
    {
        info: {
            propsTable: [Button],
            propTablesExclude: [Provider],
            text: `
        ## Button

        An i18n-enabled button, built on top of Bootstrap Button

        Usage: 
        ~~~js
        <Button
            id={"some-id"}
            label={"activities.loan.pay.send"}
            type={"submit"}
            bsStyle={"primary"}
        />
        ~~~
    `,
        },
    },
);
