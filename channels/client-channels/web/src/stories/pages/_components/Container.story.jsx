import React from "react";
import Container from "pages/_components/Container";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";

newStory("Components").add(
    "Container",
    () => {
        const { Column, ColumnHeader, ColumnBody } = Container;
        return (
            <Provider store={store}>
                <Container>
                    <Column>
                        <ColumnHeader title="menu.payments" />
                        <ColumnBody>Actual column content</ColumnBody>
                    </Column>
                </Container>
            </Provider>
        );
    },
    {
        info: {
            propsTable: [Container],
            propTablesExclude: [Provider],
            text: `
        ## Container

        A Bootstrap Grid container with an optional declarative header.

        Usage: 
        ~~~js
          <Container>
          <Container.Column>
            <Container.ColumnHeader title={"menu.payments"} />
            <Container.ColumnBody>Actual column content</Container.ColumnBody>
          </Container.Column>
        </Container>
        ~~~
    `,
        },
    },
);
