import React, { Fragment } from "react";

import newStory from "stories/util/newStory";
import BottomSheet from "pages/pointsOfInterest/_components/BottomSheet";
import Button from "pages/_components/Button";
import MainContainer from "pages/_components/MainContainer";
import Container from "pages/_components/Container";
import { Provider } from "react-redux";
import { BrowserRouter, Route } from "react-router-dom";
import { store } from "store";
import { boolean } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import * as i18n from "util/i18n";

newStory("Components").add(
    "Bottom Sheet",
    () => (
        <Provider store={store}>
            <BrowserRouter>
                <Route
                    render={() => {
                        const isBottomSheetDisplayed = false;

                        return (
                            <Fragment>
                                <MainContainer>
                                    <div className="above-the-fold">
                                        <Container>
                                            <Container.Column className="col col-12 col-lg-6 col-md-9 col-sm-12">
                                                <Button
                                                    bsStyle="primary"
                                                    className="btn-small"
                                                    label="pointsofinterest.map.currentPosition"
                                                    onClick={action("openBottomSheet")}
                                                />
                                            </Container.Column>
                                        </Container>
                                    </div>
                                </MainContainer>

                                <BottomSheet
                                    isDisplayed={boolean(isBottomSheetDisplayed)}
                                    handleDismiss={() => {
                                        this.handleBottomSheetClick(false);
                                    }}
                                    onChange={() => {
                                        this.toggleBottomSheet(isBottomSheetDisplayed);
                                    }}
                                    hasDismissButton
                                    title={i18n.get("pointsofinterest.bottomSheet.title", "Buttom sheet title")}>
                                    <BottomSheet.Content>The content goes here!</BottomSheet.Content>
                                </BottomSheet>
                            </Fragment>
                        );
                    }}
                />
            </BrowserRouter>
        </Provider>
    ),

    {
        info: {
            propsTable: [BottomSheet],
            text: `
        ## Action Sheet

        An action sheet is a specific style of alert that appears in response to a control or action.

        Usage:
        ~~~js
        <BottomSheet
            isDisplayed={isBottomSheetDisplayed}
            handleDismiss={() => {
                this.handleBottomSheetClick(false);
            }}
            onChange={() => {
                this.toggleBottomSheet(isBottomSheetDisplayed);
            }}
            hasDismissButton
            title={i18n.get("pointsofinterest.bottomSheet.title", "Buttom sheet title")}>
            <BottomSheet.Content>
                The content goes here!
            </BottomSheet.Content>
        </BottomSheet>
        ~~~
    `,
        },
    },
);
