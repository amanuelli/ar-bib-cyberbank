import React from "react";
import Collapse from "pages/_components/Collapse";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { text } from "@storybook/addon-knobs";
import { store } from "store";

newStory("Components").add(
    "Collapse",
    () => (
        <Provider store={store}>
            <div className="component-wrapper">
                <Collapse
                    buttonLabelOpened={text("Label Open", "menu.payments")}
                    buttonLabelClosed={text("Label Closed", "menu.payments")}
                    buttonClassName={text("Button Class Name", "")}>
                    Some toggleable content goes here
                </Collapse>
            </div>
        </Provider>
    ),
    {
        info: {
            propsTable: [Collapse],
            propTablesExclude: [Provider],
            text: `
        ## Collapse

        Used to toggle a bit of content

        Usage: 
        ~~~js
          <Collapse
            buttonLabelClosed={"menu.payments"}
            buttonLabelOpened={"menu.payments"}
          >
            Some toggleable content goes here
          </Collapse>
        ~~~
    `,
        },
    },
);
