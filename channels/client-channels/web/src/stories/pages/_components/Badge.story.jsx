import React from "react";
import Badge from "pages/_components/Badge";
import { number, text } from "@storybook/addon-knobs";
import newStory from "stories/util/newStory";

newStory("Components").add("Badge", () => <Badge count={number("Count", 10)} className={text("Class name", "")} />, {
    info: {
        propsTable: [Badge],
        text: `
        ## Badges

        Badges display a count. If this count is over 99, the displayed text reads "+99"

        Usage: 
        ~~~js
            <Badge count={81} className={"danger"} />
        ~~~
    `,
    },
});
