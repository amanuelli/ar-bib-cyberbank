import React, { Fragment } from "react";
import { Field, Formik } from "formik";
import { Provider } from "react-redux";

import { store } from "store";
import newStory from "stories/util/newStory";
import ConnectedMultilinefile, { Multilinefile } from "pages/forms/_components/_fields/Multilinefile";

const data = {
    acceptedFileTypes: ["text/plain"],
    allowMultiple: false,
    controlLimits: false,
    maxFileSizeMB: 4,
    useForTotalAmount: true,
};

const noOp = () => {};

newStory("Components/Fields").add(
    "Multilinefile",
    () => {
        const example = (mode) => (
            <Fragment>
                <h2>Component in mode {mode}</h2>
                <Provider store={store}>
                    <div className="component-wrapper">
                        <Formik onSubmit={noOp} onChange={noOp} initialValues={{}}>
                            <Field
                                data={data}
                                {...data}
                                clearable={false}
                                component={ConnectedMultilinefile}
                                idForm="some-form"
                                name="some-name"
                                mode={mode}
                            />
                        </Formik>
                    </div>
                </Provider>
            </Fragment>
        );
        return (
            <Fragment>
                {example("view")}
                {example("edit")}
            </Fragment>
        );
    },
    {
        info: {
            propTables: [Multilinefile, Formik, Field],
            propTablesExclude: [Provider],
            text: `
        ## Multilinefile

        Lets a user upload a file for mass payments

        Usage:
        ~~~js
        const data = {
            acceptedFileTypes: ["text/plain"],
            allowMultiple: false,
            controlLimits: false,
            maxFileSizeMB: 4,
            useForTotalAmount: true,
        };
        <Field
            data={data}
            {...data}
            clearable={false}
            component={ConnectedMultilinefile}
            idForm="some-form"
            name="some-name"
            mode={mode}
        />
        ~~~
    `,
        },
    },
);
