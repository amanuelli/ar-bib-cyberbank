import React from "react";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import { BrowserRouter, Route } from "react-router-dom";
import FieldWarning from "pages/_components/fields/FieldWarning";

newStory("Components/Fields").add(
    "FieldWarning",
    () => {
        const result = () => (
            <Provider store={store}>
                <div className="component-wrapper">
                    <BrowserRouter>
                        <Route
                            render={() => (
                                <div>
                                    <input type="number" min="18" max="100" id="input" />
                                    <FieldWarning warning="You must be over 18 to access the website" />
                                </div>
                            )}
                        />
                    </BrowserRouter>
                </div>
            </Provider>
        );
        result.propTypes = FieldWarning.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [FieldWarning],
            propTablesExclude: [Provider, BrowserRouter, Route],
            text: `
        ## FieldWarning

        Optional text
        
        Can be added to a label through the label's "optional" prop

        Usage: 
        ~~~js
            <FieldWarning warning={"You must be over 18 to access the website"} />
        ~~~
    `,
        },
    },
);
