import React, { Fragment } from "react";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { boolean, text } from "@storybook/addon-knobs";
import { action } from "@storybook/addon-actions";
import { store } from "store";
import { BrowserRouter, Route } from "react-router-dom";
import Field from "pages/_components/fields/Checkbox";

newStory("Components/Fields").add(
    "Checkbox",
    () => (
        <Provider store={store}>
            <div className="component-wrapper">
                <BrowserRouter>
                    <Route
                        render={() => (
                            <Fragment>
                                <Field
                                    name="checkbox-1"
                                    checked={boolean("Checkbox 1 checked", false)}
                                    label={text(
                                        "checkbox-1 text",
                                        "This is a label, text lands here without passing through 18n",
                                    )}
                                    block
                                    onChange={action("change")}
                                />
                                <Field
                                    name="checkbox-2"
                                    checked={boolean("Checkbox 2 checked", true)}
                                    label={
                                        ("checkbox-2 text",
                                        "This is a label, text lands here without passing through 18n")
                                    }
                                    block
                                />
                            </Fragment>
                        )}
                    />
                </BrowserRouter>
            </div>
        </Provider>
    ),
    {
        info: {
            propsTable: [Field],
            propTablesExclude: [BrowserRouter, Route, Provider],
            text: `
        ## Checkbox

        Allows an user to select a given option from a list of mutually exclusive ones

        Usage: 
        ~~~js
        <Checkbox
          name="checkbox"
          checked
          label={"Label for checkbox. If not provided, name is used and looked up through i18n"}
          onChange={()=>{}}
          block
        />
        ~~~
    `,
        },
    },
);
