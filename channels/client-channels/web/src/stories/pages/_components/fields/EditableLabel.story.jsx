import React from "react";
import { action } from "@storybook/addon-actions";
import { boolean } from "@storybook/addon-knobs";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import { BrowserRouter, Route } from "react-router-dom";
import EditableLabel from "pages/_components/fields/EditableLabel";

newStory("Components/Fields").add(
    "EditableLabel",
    () => {
        const result = () => (
            <Provider store={store}>
                <div className="component-wrapper inverted">
                    <p>(color scheme inverted)</p>
                    <BrowserRouter>
                        <Route
                            render={() => (
                                <div>
                                    <EditableLabel
                                        isDesktop={boolean("Is this one a desktop label?", false)}
                                        onSave={action("saved")}
                                        value="some-value">
                                        <h2 className="label">Click here to edit</h2>
                                    </EditableLabel>
                                </div>
                            )}
                        />
                    </BrowserRouter>
                </div>
            </Provider>
        );
        result.propTypes = EditableLabel.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [EditableLabel],
            propTablesExclude: [Provider, BrowserRouter, Route],
            text: `
        ## EditableLabel

        Presents the user with a label they can change

        Usage: 
        ~~~js
            <EditableLabel
            isDesktop
            onSave={()=>{}}
            value={"some-value"}
            >
            <h2 className="label">"Click here to edit"</h2>
            </EditableLabel>
        ~~~
    `,
        },
    },
);
