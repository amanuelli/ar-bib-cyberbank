import React from "react";
import { Formik, Form, Field } from "formik";
import { action } from "@storybook/addon-actions";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import MultiSelect from "pages/_components/fields/MultiSelect";

newStory("Components/Fields").add(
    "MultiSelect",
    () => {
        const result = () => (
            <Provider store={store}>
                <MultiSelect
                    name="name"
                    onSelect={action("Select")}
                    onDelete={action("Delete")}
                    values={["A", "B", "C"]}
                    options={{
                        ids: ["A", "B", "C"],
                    }}
                />
            </Provider>
        );
        result.propTypes = MultiSelect.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [MultiSelect],
            propTablesExclude: [Provider, Formik, Form, Field],
            text: `
        ## MaskedTextField

        Presents an input for data that musts map to a given format, represented as a mask.

        Usage: 
        ~~~js
        <MultiSelect
          onSelect={action("Select")}
          onDelete={action("Delete")}
          values={[
              {id: 1, label: "A", value:9},
              {id: 2, label: "B", value:8},
              {id: 3, label: "C", value:7},
              {id: 4, label: "D", value:6},
              {id: 5, label: "E", value:5},
              {id: 6, label: "F", value:4},
              {id: 7, label: "G", value:3},
              {id: 8, label: "H", value:2},
              {id: 9, label: "I", value:1},
          ]}
        />
        ~~~
    `,
        },
    },
);
