import React, { Fragment } from "react";
import { Field, Formik } from "formik";
import { Provider } from "react-redux";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import { store } from "store";
import newStory from "stories/util/newStory";
import Transactionlines from "pages/forms/_components/_fields/Transactionlines";

const noOp = () => {};

newStory("Components/Fields").add(
    "Transactionlines",
    () => {
        const example = (mode) => (
            <Fragment>
                <h2>Component in mode {mode}</h2>
                <Provider store={store}>
                    <div className="component-wrapper">
                        <BrowserRouter>
                            <Switch>
                                <Route
                                    render={() => (
                                        <Formik onSubmit={noOp} onChange={noOp} initialValues={{}}>
                                            <Field component={Transactionlines} idForm="some-form" mode={mode} />
                                        </Formik>
                                    )}
                                />
                            </Switch>
                        </BrowserRouter>
                    </div>
                </Provider>
            </Fragment>
        );
        example.propTypes = Transactionlines.propTypes;
        return (
            <Fragment>
                {example("view")}
                {example("edit")}
            </Fragment>
        );
    },
    {
        info: {
            propsTable: [Transactionlines],
            text: `
        ## Transactionlines

        Lets a user make mass payments manually

        Usage:
        ~~~js
        <Field component={Transactionlines} idForm="some-form" mode="some-mode" />
        ~~~
    `,
        },
    },
);
