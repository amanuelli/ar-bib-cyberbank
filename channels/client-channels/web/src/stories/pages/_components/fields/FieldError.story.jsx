import React from "react";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import { BrowserRouter, Route } from "react-router-dom";
import FieldError from "pages/_components/fields/FieldError";

newStory("Components/Fields").add(
    "FieldError",
    () => {
        const result = () => (
            <Provider store={store}>
                <div className="component-wrapper">
                    <BrowserRouter>
                        <Route
                            render={() => (
                                <div>
                                    <input type="text" />
                                    <FieldError error="There was an error" />
                                </div>
                            )}
                        />
                    </BrowserRouter>
                </div>
            </Provider>
        );
        result.propTypes = FieldError.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [FieldError],
            propTablesExclude: [Provider, BrowserRouter, Route],
            text: `
        ## FieldError

        Used to present the user with an error in a field

        Usage: 
        ~~~js
            <FieldError error={"There was an error"} />
        ~~~
    `,
        },
    },
);
