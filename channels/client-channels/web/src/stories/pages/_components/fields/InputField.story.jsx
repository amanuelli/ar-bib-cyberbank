import React from "react";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import { BrowserRouter, Route } from "react-router-dom";
import InputField from "pages/_components/fields/InputField";
import { action } from "@storybook/addon-actions/dist/preview";

newStory("Components/Fields").add(
    "InputField",
    () => {
        const result = () => (
            <Provider store={store}>
                <div className="component-wrapper">
                    <BrowserRouter>
                        <Route
                            render={() => (
                                <div>
                                    <InputField
                                        name="otp"
                                        keyprefix="administration.otp"
                                        kind="numeric"
                                        onChange={action("Change")}
                                    />
                                </div>
                            )}
                        />
                    </BrowserRouter>
                </div>
            </Provider>
        );
        result.propTypes = InputField.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [InputField],
            propTablesExclude: [Provider, BrowserRouter, Route],
            text: `
        ## InputField

        Presents an input for several kinds of data.

        The "kind" attribute roughly maps with the <input> tag's "type" 
        with the exception of the "numeric" kind which maps to "number" 
        but also adds html5 validation to the textbox

        Usage: 
        ~~~js
        <div>
        <InputField
          name={"otp"}
          keyprefix={"administration.otp"}
          kind="numeric"
          onChange={action("Change")}
        />
        ~~~
    `,
        },
    },
);
