import React from "react";
import { Formik, Form, Field } from "formik";
import { action } from "@storybook/addon-actions";
import { text, boolean } from "@storybook/addon-knobs";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import MaskedTextField from "pages/_components/fields/MaskedTextField";

newStory("Components/Fields").add(
    "MaskedTextField",
    () => {
        const result = () => (
            <Provider store={store}>
                <Formik
                    onSubmit={action("submit")}
                    onChange={action("change")}
                    initialValues={{
                        "some-id": {
                            currency: 2,
                            amount: 1000,
                        },
                    }}>
                    <Form name="administration">
                        <Field
                            idForm="administration"
                            autoComplete="off"
                            component={MaskedTextField}
                            hidePlaceholder
                            mask={text("Mapping mask", [
                                /* eslint-disable no-useless-escape */
                                "(",
                                /[1-9]/,
                                /\d/,
                                /\d/,
                                ")",
                                " ",
                                /\d/,
                                /\d/,
                                /\d/,
                                "-",
                                /\d/,
                                /\d/,
                                /\d/,
                                /\d/,
                                /* eslint-enable no-useless-escape */
                            ])}
                            maxLength={50}
                            name="otp"
                            autoFocus={boolean("Should autofocus?", false)}
                        />
                    </Form>
                </Formik>
            </Provider>
        );
        result.propTypes = MaskedTextField.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [MaskedTextField],
            propTablesExclude: [Provider, Formik, Form, Field],
            /* eslint-disable no-useless-escape */
            text: `
        ## MaskedTextField

        Presents an input for data that musts map to a given format, represented as a mask.

        Usage: 
        ~~~js
        <Field
            idForm={"administration"}
            autoComplete="off"
            component={MaskedTextField}
            hidePlaceholder
            mask={[
                "(",
                /[1-9]/,
                /\d/,
                /\d/,
                ")",
                " ",
                /\d/,
                /\d/,
                /\d/,
                "-",
                /\d/,
                /\d/,
                /\d/,
                /\d/
            ]}
            maxLength={50}
            name="otp"
            autoFocus
        />
        ~~~
    `,
            /* eslint-enable no-useless-escape */
        },
    },
);
