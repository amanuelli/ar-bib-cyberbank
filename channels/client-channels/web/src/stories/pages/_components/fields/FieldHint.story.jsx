import React from "react";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import { BrowserRouter, Route } from "react-router-dom";
import FieldHint from "pages/_components/fields/FieldHint";

newStory("Components/Fields").add(
    "FieldHint",
    () => {
        const result = () => (
            <Provider store={store}>
                <div className="component-wrapper">
                    <BrowserRouter>
                        <Route
                            render={() => (
                                <div>
                                    <input type="number" min="18" max="100" />
                                    <FieldHint text="(Minimum age is 18)" />
                                </div>
                            )}
                        />
                    </BrowserRouter>
                </div>
            </Provider>
        );
        result.propTypes = FieldHint.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [FieldHint],
            propTablesExclude: [Provider, BrowserRouter, Route],
            text: `
        ## FieldHint

        Used to present the user with some hints on how to fill a field

        Usage: 
        ~~~js
          <FieldHint text={"(Minimum age is 18)"} />
        ~~~
    `,
        },
    },
);
