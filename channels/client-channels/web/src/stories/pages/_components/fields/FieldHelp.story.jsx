import React from "react";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import { BrowserRouter, Route } from "react-router-dom";
import FieldHelp from "pages/_components/fields/FieldHelp";

newStory("Components/Fields").add(
    "FieldHelp",
    () => {
        const result = () => (
            <Provider store={store}>
                <div className="component-wrapper">
                    <BrowserRouter>
                        <Route
                            render={() => (
                                <div>
                                    <input type="number" />
                                    <FieldHelp text="(This is a number field)" />
                                </div>
                            )}
                        />
                    </BrowserRouter>
                </div>
            </Provider>
        );
        result.propTypes = FieldHelp.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [FieldHelp],
            propTablesExclude: [Provider, BrowserRouter, Route],
            text: `
        ## FieldHelp

        Used to present the user with help on how to fill a field

        Usage: 
        ~~~js
          <FieldHelp text={this.i18n("help")} />
        ~~~
    `,
        },
    },
);
