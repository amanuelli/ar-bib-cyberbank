import React from "react";
import { Form, Field, Formik } from "formik";
import { action } from "@storybook/addon-actions";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import Amount, { AmountField as RawField } from "pages/_components/fields/AmountField";
import * as i18n from "util/i18n";

newStory("Components/Fields").add(
    "AmountField",
    () => (
        <Provider store={store}>
            <div className="component-wrapper">
                <Formik
                    onSubmit={action("submit")}
                    onChange={action("change")}
                    initialValues={{
                        "some-id": {
                            currency: 2,
                            amount: 1000,
                        },
                    }}>
                    <Form name="some-form">
                        <Field
                            component={RawField}
                            data={{
                                options: [
                                    { id: 1, label: i18n.get(`country.name.EC`) },
                                    { id: 2, label: i18n.get(`country.name.UY`) },
                                ],
                            }}
                            clearable
                            idForm="some-form"
                            name="amount"
                            field={{ name: "some-id" }}
                            hideLabel
                        />
                    </Form>
                </Formik>
            </div>
        </Provider>
    ),
    {
        info: {
            propsTable: [Amount],
            propTablesExclude: [Provider, Formik, Form, Field],
            text: `
        ## AmountField

        Allows an user to select a currency from a list of provided options

        Usage:
        ~~~js
        <Form name="some-form">
            <Field
            component={Amount}
            data={{
                options: [
                { id: 1, label: i18n.get("country.name.EC") },
                { id: 2, label: i18n.get("country.name.UY") }
                ]
            }}
            clearable
            idForm="some-form"
            name="amount"
            field={{ name: "some-id" }}
            hideLabel
            />
        </Form>
        ~~~
    `,
        },
    },
);
