import React from "react";
import { text } from "@storybook/addon-knobs";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import { BrowserRouter, Route } from "react-router-dom";
import FieldLabel from "pages/_components/fields/FieldLabel";

newStory("Components/Fields").add(
    "FieldLabel",
    () => {
        const result = () => (
            <Provider store={store}>
                <div className="component-wrapper">
                    <BrowserRouter>
                        <Route
                            render={() => (
                                <div>
                                    <FieldLabel
                                        labelText={text("Label text", "Age")}
                                        optional={text("Optional suffix", "*")}
                                        id="input"
                                    />
                                    <input type="number" min="18" max="100" id="input" />
                                </div>
                            )}
                        />
                    </BrowserRouter>
                </div>
            </Provider>
        );
        result.propTypes = FieldLabel.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [FieldLabel],
            propTablesExclude: [Provider, BrowserRouter, Route],
            text: `
        ## FieldLabel

        Label to an input. 
        
        Can be provided with a custom labelText or an i18n id by passing a labelKey.

        Supports adding a suffix with the "optional" prop. 

        Usage: 
        ~~~js
          <FieldLabel labelText={"Age"} optional={"*"} />
        ~~~
    `,
        },
    },
);
