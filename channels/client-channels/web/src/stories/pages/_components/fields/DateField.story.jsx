import React from "react";
import { Form, Field, Formik } from "formik";
import { action } from "@storybook/addon-actions";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";

import DateInput, { DateField as RawField } from "pages/_components/fields/DateField";

newStory("Components/Fields").add(
    "DateField",
    () => (
        <Provider store={store}>
            <div className="component-wrapper">
                <Formik onSubmit={action("submit")} onChange={action("change")} initialValues={{}}>
                    <Form name="some-form">
                        <Field
                            component={DateInput}
                            endDate={null}
                            hidePlaceholder
                            idForm="some-form"
                            isClearable
                            name="dateFrom"
                            selectsStart
                            showMonthYearDropdown
                            startDate={new Date()}
                            handleChange={action("change")}
                        />
                    </Form>
                </Formik>
            </div>
        </Provider>
    ),
    {
        info: {
            propsTable: [DateInput, RawField],
            propTablesExclude: [Provider, Formik, Form, Field],
            text: `
        ## DateField

        Allows an user to select a given date

        Usage: 
        ~~~js
        <Form name="some-form">
        <Field
          component={DateField}
          endDate={null}
          hidePlaceholder
          idForm={"some-form"}
          isClearable
          name="dateFrom"
          selectsStart
          showMonthYearDropdown
          startDate={new Date()}
          handleChange={()=>{}}
        />
      </Form>
        ~~~
    `,
        },
    },
);
