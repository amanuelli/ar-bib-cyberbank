import React from "react";
import { Form, Field, Formik } from "formik";
import { action } from "@storybook/addon-actions";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";

import Document, { DocumentField as RawField } from "pages/_components/fields/DocumentField";

newStory("Components/Fields").add(
    "DocumentField",
    () => (
        <Provider store={store}>
            <div className="component-wrapper">
                <Formik
                    onSubmit={action("submit")}
                    onChange={action("change")}
                    initialValues={{
                        document: {
                            country: 1,
                        },
                    }}>
                    <Form name="some-form">
                        <Field
                            autoComplete="off"
                            clearable={false}
                            component={Document}
                            data={{
                                countries: [{ id: "1", name: "UY" }],
                                documentTypes: [{ id_document_type: "1", id_country_code: "1" }],
                            }}
                            idForm="some-form"
                            name="document"
                        />
                    </Form>
                </Formik>
            </div>
        </Provider>
    ),
    {
        info: {
            propsTable: [Document, RawField],
            propTablesExclude: [Provider, Formik, Form, Field],
            text: `
        ## DocumentField

        Lets an user select an ID according to their country

        Usage: 
        ~~~js
        <Form name="some-form">
        <Field
          component={DateField}
          endDate={null}
          hidePlaceholder
          idForm={"some-form"}
          isClearable
          name="dateFrom"
          selectsStart
          showMonthYearDropdown
          startDate={new Date()}
          handleChange={()=>{}}
        />
      </Form>
        ~~~
    `,
        },
    },
);
