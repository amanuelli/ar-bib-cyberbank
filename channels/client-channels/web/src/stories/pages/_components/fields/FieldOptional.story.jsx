import React from "react";
import newStory from "stories/util/newStory";
import { Provider } from "react-redux";
import { store } from "store";
import { BrowserRouter, Route } from "react-router-dom";
import FieldLabel from "pages/_components/fields/FieldLabel";
import FieldOptional from "pages/_components/fields/FieldOptional";

newStory("Components/Fields").add(
    "FieldOptional",
    () => {
        const result = () => (
            <Provider store={store}>
                <div className="component-wrapper">
                    <BrowserRouter>
                        <Route
                            render={() => (
                                <div>
                                    <FieldLabel labelText="Age" optional={<FieldOptional text="*" />} />
                                    <input type="number" min="18" max="100" id="input" />
                                </div>
                            )}
                        />
                    </BrowserRouter>
                </div>
            </Provider>
        );
        result.propTypes = FieldOptional.propTypes;

        return result();
    },
    {
        info: {
            propsTable: [FieldOptional],
            propTablesExclude: [Provider, BrowserRouter, Route],
            text: `
        ## FieldOptional

        Optional text
        
        Can be added to a label through the label's "optional" prop

        Usage: 
        ~~~js
          <FieldOptional labelText={"Age"} optional={<FieldOptional text="*" />} />
        ~~~
    `,
        },
    },
);
