import React from "react";
import newStory from "stories/util/newStory";
import Stepper, { Step, orientations } from "pages/_components/StepIndicator/index";
import { boolean, number } from "@storybook/addon-knobs";

newStory("Components").add(
    "StepIndicator",
    () => (
        <Stepper orientation={boolean("horizontal?", false) ? orientations.horizontal : orientations.vertical}>
            <Step
                currentStep={number("currentStep?", 2, {
                    range: true,
                    min: 1,
                    max: 5,
                    step: 1,
                })}>
                Document Front
            </Step>
            <Step
                currentStep={number("currentStep?", 2, {
                    range: true,
                    min: 1,
                    max: 5,
                    step: 1,
                })}>
                Document Back
            </Step>
            <Step
                currentStep={number("currentStep?", 2, {
                    range: true,
                    min: 1,
                    max: 5,
                    step: 1,
                })}>
                Selfie
            </Step>
            <Step
                currentStep={number("currentStep?", 2, {
                    range: true,
                    min: 1,
                    max: 5,
                    step: 1,
                })}>
                Personal info
            </Step>
            <Step
                currentStep={number("currentStep?", 2, {
                    range: true,
                    min: 1,
                    max: 5,
                    step: 1,
                })}>
                Address
            </Step>
        </Stepper>
    ),
    {
        info: {
            propsTable: [Stepper],
            text: `
            ## Stepper
            Usage:
            ~~~js
                    <Stepper orientation={orientations.vertical}>
                        <Step currentStep={2}>Document Front</Step>
                        <Step currentStep={2}>Document Back</Step>
                        <Step currentStep={2}>Selfie</Step>
                    </Stepper>
            ~~~
            `,
        },
    },
);
