import React from "react";

const MainContainerDecorator = (storyFn) => (
    <div className="app theme-auth default-layout">
        <div className="app-page">
            <div className="app-content">
                <div className="view-wrapper theme-auth">
                    <div className="view-page">
                        <div role="main" className="view-content">
                            <main className="main-container">
                                <div className="above-the-fold">
                                    <section className="container--layout">
                                        <div className="container">
                                            <div className="row">
                                                <div className="col">{storyFn()}</div>
                                            </div>
                                        </div>
                                    </section>
                                </div>
                            </main>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
);

export default MainContainerDecorator;
