import React from "react";
import { Button } from "@storybook/react/demo";
import newStory from "./util/newStory";

newStory("Button")
    .add("with text", () => <Button>Hello Button</Button>)
    .add("with some emoji", () => (
        <Button>
            <span role="img" aria-label="so cool">
                😀 😎 👍 💯
            </span>
        </Button>
    ));
