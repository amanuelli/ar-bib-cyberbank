import { storiesOf } from "@storybook/react";

export default function(...args) {
    // eslint-disable-next-line no-undef
    return storiesOf(...args, module);
}
