import React, { Component } from "react";
import { func, oneOfType, arrayOf, node, element } from "prop-types";

export default class ErrorBoundary extends Component {
    static propTypes = {
        children: oneOfType([func, node, arrayOf(node), element, arrayOf(element)]).isRequired,
    };

    constructor(props) {
        super(props);
        this.state = { hasError: false, error: undefined };
    }

    static getDerivedStateFromError(error) {
        return { hasError: true, error };
    }

    componentDidCatch(error) {
        // eslint-disable-next-line no-console
        console.error({ error });
        this.setState({ error, hasError: true });
    }

    render() {
        const { hasError, error } = this.state;
        const { children } = this.props;

        if (hasError) {
            return (
                <section className="error-message">
                    <header>
                        <h1>Component refuses to render</h1>
                        <p>Brief description follows. Check console for more info</p>
                    </header>
                    <div>
                        <pre>
                            <code>{error.message}</code>
                        </pre>
                    </div>
                </section>
            );
        }

        return children();
    }
}
