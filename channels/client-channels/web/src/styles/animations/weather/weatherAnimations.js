import * as dayClear from "styles/animations/weather/dayClear.json";
import * as dayCloudy from "styles/animations/weather/dayCloudy.json";
import * as dayRainy from "styles/animations/weather/dayCloudy.json";
import * as daySnowy from "styles/animations/weather/daySnowy.json";

import * as nightClear from "styles/animations/weather/nightClear.json";
import * as nightCloudy from "styles/animations/weather/nightCloudy.json";
import * as nightRainy from "styles/animations/weather/nightCloudy.json";
import * as nightSnowy from "styles/animations/weather/nightSnowy.json";

export default {
    dayClear: dayClear.default,
    dayCloudy: dayCloudy.default,
    dayRainy: dayRainy.default,
    daySnowy: daySnowy.default,
    nightClear: nightClear.default,
    nightCloudy: nightCloudy.default,
    nightRainy: nightRainy.default,
    nightSnowy: nightSnowy.default,
};
