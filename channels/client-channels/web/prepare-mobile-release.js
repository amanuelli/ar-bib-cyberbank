const fs = require("fs-extra");
const path = require("path");
const archiver = require("archiver");
const YAML = require("yamljs");

let rootPath = "../mobile/";
let wwwPath = path.join(rootPath, "www/");
const extrasPath = path.join(rootPath, "extras/");

const targetPath = path.join(rootPath, "target/");
const buildPath = path.join(targetPath, "build/");

// Archivo de configuracion de releases
const config = YAML.load(path.join(rootPath, "extras/config.yaml"));

// Cleanup inicial
fs.removeSync(targetPath);
fs.mkdirSync(targetPath);
fs.mkdirSync(path.join(targetPath, "dist"));

// Copy files
fs.mkdirSync(buildPath);
fs.copySync(wwwPath, path.join(buildPath, "www"));
fs.copySync(path.join(rootPath, "config.xml"), path.join(buildPath, "config.xml"));

// Cambio el path del root y www al del directorio build
rootPath = buildPath;
wwwPath = path.join(buildPath, "www/");

// Funcion preparadora de una release especifica
const prepareRelease = (release) => {
    const data = config.releases[release];

    fs.renameSync(path.join(wwwPath, "config.js"), path.join(wwwPath, "config.js"));
    // Leo el archivo config.js para modificar la API_URL
    let content = fs.readFileSync(path.join(wwwPath, "config.js"), "utf8");
    content = content.replace(/window.API_URL.*?;/g, `window.API_URL = "${data.API_URL}";`);

    // Guardo el archivo modificado
    fs.writeFileSync(path.join(wwwPath, "config.js"), content);

    // Leo el archivo config.xml para actualizar el APPID y la Descripcion
    content = fs.readFileSync(path.join(rootPath, "config.xml"), "utf8");

    content = content.replace(/<widget (.*?)id=".*?"/, `<widget $1id="${data.APP_ID}"`);
    content = content.replace(/<description>([\s\S]*)\(.*?\)/, `<description>$1(${data.API_URL})`);
    content = content.replace(/<name>(.*?)<\/name>/, `<name>${data.ENV_NAME}</name>`);

    // Guardo el archivo modificado
    fs.writeFileSync(path.join(rootPath, "config.xml"), content);

    // Copio los datos especificos de la release al www
    //  fs.copySync(path.join(extrasPath, `${release}`), wwwPath);
};

// Funcion generadora del zip para una release especifica
const generateZip = (release) =>
    new Promise((resolve, reject) => {
        const data = config.releases[release];

        // Preparo la release
        prepareRelease(release);

        const output = fs.createWriteStream(path.join(targetPath, `dist/mobile-app-${release}.zip`));
        const archive = archiver("zip", {
            zlib: { level: 9 }, // Sets the compression level
        });

        output.on("close", () => {
            resolve();
        });

        archive.on("warning", (err) => {
            if (err.code === "ENOENT") {
                console.warn(err);

                resolve();
            } else {
                reject(error);
            }
        });

        archive.on("error", (err) => {
            reject(error);
        });

        archive.pipe(output);

        archive.file(path.join(rootPath, "config.xml"), { name: "config.xml" });
        archive.directory(wwwPath, "www");

        archive.finalize();
    });

/** Genero el ZIP para cada una de las releases * */
const releases = Object.keys(config.releases);

// Funcion para llamar sincronicamente las promises
const zipRelease = (i) => {
    if (i < releases.length) {
        const release = releases[i];
        generateZip(release)
            .then(() => zipRelease(i + 1))
            .catch((error) => {
                console.error(error);
            });
    } else {
        // Limpio temporales
        fs.removeSync(buildPath);
    }
};

// Inicio el proceso de comprimido
zipRelease(0);
