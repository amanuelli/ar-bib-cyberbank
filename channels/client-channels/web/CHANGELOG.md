# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [Unreleased]

## [3.2.2] - 2021-08-16

### Changed
- channels frontend and mobile moved into new folder client-channels. Channel frontend renamed to web [MANNAZCA-14816](https://technisys.atlassian.net/browse/MANNAZCA-14816)

### Fixed
- Fix CICD pipeline to update CICD template repo [TECDIGSK-454](https://technisys.atlassian.net/browse/TECDIGSK-454)
- Fix for OCP deployment. Added securityContext in deployment manifest [TECENG-1258](https://technisys.atlassian.net/browse/TECENG-1258)

## [3.2.1]
### Added
- Implement auto-versioning of Helm Chart with version and app_version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)

### Fixed
- Fix Scheduler.jsx valueDate for non-once schedule [MANNAZCA-14607](https://technisys.atlassian.net/browse/MANNAZCA-14607)