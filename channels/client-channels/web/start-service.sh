#!/bin/bash
set -e
cp /data/www/config_template.js /data/www/config.js
sed -i 's|^window.API_URL.*|window.API_URL="'$API_URL'";|' /data/www/config.js
sed -i 's|^window.OAUTH_URL.*|window.OAUTH_URL="'$OAUTH_URL'";|' /data/www/config.js
sed -i 's|^window.DAS_URL.*|window.DAS_URL="'$DAS_URL'";|' /data/www/config.js

# exec httpd-foreground
exec nginx -g "daemon off;"
