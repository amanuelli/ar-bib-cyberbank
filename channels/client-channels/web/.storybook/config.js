import { configure } from "@storybook/react";
import addDecorators from "./addons";

function loadStories() {
  addDecorators();
  require("../src/stories");
}

configure(loadStories, module);
