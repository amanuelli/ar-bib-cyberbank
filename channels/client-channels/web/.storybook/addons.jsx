import React from "react";
import "@storybook/addon-actions/register";
import "@storybook/addon-links/register";
import "@storybook/addon-knobs/register";
import { addDecorator } from "@storybook/react";
import { withKnobs } from "@storybook/addon-knobs";
import { withInfo } from "@storybook/addon-info";
import Fence from "../src/stories/util/Fence";

export default function addDecorators() {
  addDecorator(withInfo({ inline: true, source: false }));
  addDecorator(withKnobs);
  addDecorator(fn => <Fence>{fn}</Fence>);
  addDecorator(fn => <div style={{ position: "relative" }}>{fn()}</div>);
}
