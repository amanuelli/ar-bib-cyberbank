window.isMobileApp = function() {
    return typeof window.cordova !== "undefined";
};

window.common = {
    hideSplashScreen: function() {
        if (window.isMobileApp()) {
            app.hideSplashScreen();
        }
    }
};
