const supportedVersions = {
    "Microsoft Edge": 12,
    Safari: 11,
    Chrome: 69,
    "Chrome Mobile": 69,
    Firefox: 62,
    "Firefox Mobile": 62,
    "Firefox for iOS": 14,
    "Android Browser": 4,
};
const url = window.innerWidth >= 768 ? "notSupported/NSDesktop.html" : "notSupported/NSMobile.html";

if (
    (window.cordova === undefined && supportedVersions[window.platform.name] === undefined) ||
    window.platform.version.split(".")[0] < supportedVersions[window.platform.name]
) {
    window.location.href = url;
}
