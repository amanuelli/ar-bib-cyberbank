# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).
## [1.13.17]
### Changed
- Updated Helm template and values to use in test-fs. [SDLC-460](https://technisys.atlassian.net/browse/SDLC-460)
- Two VM variables have been added for fs or db co-setting at compile time. [TECTF-1314](https://technisys.atlassian.net/browse/TECTF-1314)
- Fix translation error [TECTF-1314](https://technisys.atlassian.net/browse/TECTF-1314)
- Fix date and translation error [TECTF-1599](https://technisys.atlassian.net/browse/TECTF-1599)

## [1.13.16]
### Changed
- Update demo database for Backoffice [TECDIGSK-462](https://technisys.atlassian.net/browse/TECDIGSK-462)
- Update functional environments values [SDLC-117](https://technisys.atlassian.net/browse/SDLC-117)
- Improve Backoffice Snyk metrics [RCCP-1950] (https://technisys.atlassian.net/browse/RCCP-1950)
- Improve Backoffice Sonar metrics [RCCP-1930] (https://technisys.atlassian.net/browse/RCCP-1930)
- Errors not showing in modal [RCCP-1111] (https://technisys.atlassian.net/browse/RCCP-1111)
- Tlab Bug fix [TECTF-983] (https://technisys.atlassian.net/browse/TECTF-983)
- System Date shows with fixed format on error validation [RCCP-1856] (https://technisys.atlassian.net/browse/RCCP-1856)


## [1.13.15]
### Changed
- Dates comparison not working on DateFields [RCCP-1767] (https://technisys.atlassian.net/browse/RCCP-1767)
- Documentation of US format in reports [RCCP-1720] (https://technisys.atlassian.net/browse/RCCP-1720)
- Dates on reports from view are shown in US format [RCCP-1719] (https://technisys.atlassian.net/browse/RCCP-1719)
- El view cambia el valor del importe ingresado por pantalla [RCCP-1706] (https://technisys.atlassian.net/browse/RCCP-1706)
- Validación campo Start Time - Mensaje de alerta aparece dos veces [RCCP-1705] (https://technisys.atlassian.net/browse/RCCP-1705)
- Dates on tickets from view are shown in US format [RCCP-1694] (https://technisys.atlassian.net/browse/RCCP-1694)
- Update visualization for dates in Reports (JRXML) [RCCP-1693] (https://technisys.atlassian.net/browse/RCCP-1693)
- Update visualization for dates in Tickets [RCCP-1692] (https://technisys.atlassian.net/browse/RCCP-1692)
- Show dates in US format on View component [RCCP-1679] (https://technisys.atlassian.net/browse/RCCP-1679)
- Modify date parsing and validation [RCCP-1661] (https://technisys.atlassian.net/browse/RCCP-1661)
- Update visualization for Date Field and Collection Field [RCCP-1660] (https://technisys.atlassian.net/browse/RCCP-1660)
- Dates on View Components (TextField, tables, datepicker) in US format [RCCP-1656] (https://technisys.atlassian.net/browse/RCCP-1656)
- Diferente comportamiento entre ambiente File System y DB [RCCP-1633] (https://technisys.atlassian.net/browse/RCCP-1633)
- Advertencia con error de tipo Warning en el CPG [RCCP-1605] (https://technisys.atlassian.net/browse/RCCP-1605)
- En campo tipo float, se modifica el importe automaticamente [RCCP-1184] (https://technisys.atlassian.net/browse/RCCP-1184)
- Configurable build and runtime images [RCCP-1176] (https://technisys.atlassian.net/browse/RCCP-1176)
- Mejora en el ordenamiento del menu en View [RCCP-1164] (https://technisys.atlassian.net/browse/RCCP-1164)
- Set resources on Charts [RCCP-1113] (https://technisys.atlassian.net/browse/RCCP-1113)
- No se visualizan los errores de validación en layout tipo MODAL [RCCP-1111] (https://technisys.atlassian.net/browse/RCCP-1111)

### Changed
- Initial configuration with Profile C v2.0.0 [SDLC-26](https://technisys.atlassian.net/browse/SDLC-26)
- Configmap refactor reducing unused configurations. [SDLC-26](https://technisys.atlassian.net/browse/SDLC-26)
- Added support for OCP deployment [TECENG-1259](https://technisys.atlassian.net/browse/TECENG-1259)
