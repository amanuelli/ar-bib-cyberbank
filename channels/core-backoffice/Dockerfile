FROM acrbibank.azurecr.io/techbank/tomcat:8.5.55-jdk8

WORKDIR /usr/local/tomcat

COPY --chown=tomcat:tomcat docker-resources/additional_setenv.sh shared/

RUN mkdir -p $CATALINA_HOME/widgets

#################### BEGIN APM ####################
ARG apm_agent_jar_name='elastic-apm-agent'
ARG apm_agent_repository='https://repo1.maven.org/maven2/co/elastic/apm/elastic-apm-agent/1.18.1/elastic-apm-agent-1.18.1.jar'
ARG apm_agent_sha1_version='b779afe69763958ac7de7d0c7cd37657da068513'

RUN cat shared/additional_setenv.sh >> bin/setenv.sh && \
    rm shared/additional_setenv.sh && \
    curl -o /usr/local/tomcat/$apm_agent_jar_name.jar $apm_agent_repository && \
    sha1sum /usr/local/tomcat/$apm_agent_jar_name.jar | grep $apm_agent_sha1_version
#################### END APM ####################

USER tomcat

#################### BEGIN WIDGETS ####################
ENV widget_platform_artifact_name='BackofficeMenu'
ENV widget_platform_artifact_version='1.0.0'
ENV widget_artifact_sha1='ea890908b9ea009e96b3be6b382b5d78faf935f4'
ENV widget_file_name=$widget_platform_artifact_name-$widget_platform_artifact_version.jar
ENV widget_artifact_url="https://artifactory.technisys.com/artifactory/libs-release/net/technisys/widgets/$widget_platform_artifact_name/$widget_platform_artifact_version/$widget_file_name"
ARG curl_opts
RUN cd $CATALINA_HOME/widgets && \
    curl $curl_opts -o $widget_file_name $widget_artifact_url && \
    sha1sum $widget_file_name | grep $widget_artifact_sha1
#################### END WIDGETS ####################

#################### BEGIN CORE BACKOFFICE ####################
COPY docker-resources/config $CATALINA_HOME/config
ENV platform_artifact_name='core-backoffice'
ENV platform_artifact_version='1.13.19'
ENV artifact_sha1='767c95ecf286bc0e503591cc75f6a15adddc9ea9'
ENV file_name=$platform_artifact_name-$platform_artifact_version.war
ENV artifact_url="https://artifactory.technisys.com/artifactory/libs-release/net/technisys/cyberbank/view/product/$platform_artifact_name/$platform_artifact_version/$file_name"
ARG curl_opts
RUN cd webapps && \
    curl $curl_opts -o CoreBackoffice.war $artifact_url && \
    sha1sum CoreBackoffice.war | grep $artifact_sha1
#################### END CORE BACKOFFICE ####################

ENV DEBUG_MODE=false