# Core Backoffice


# View database
Property | Value
---|---
BO_DB_DRIVER| JDBC driver class
BO_DB_CONN| JDBC connection string
BO_DB_SCHEMA| Database schema
BO_DB_USERNAME| Database user passed as a secret
BO_DB_PASSWORD| Database password passed as a secret
BO_DIALECT| Databse dialect. E.g.:'org.hibernate.dialect.PostgreSQLDialect', 'org.hibernate.dialect.SQLServer2012Dialect'


# Core Engine Connection
Property | Value
---|---
BO_CORE_HOST| Core Engine host for HTTP connection
BO_CORE_PORT| Core Engine host for Socket connection

## HTTPS
Property | Value
---|---
BO_SSL_ENABLE| This property enables or disables secured communication (true/false)
BO_KEYSTORE_PATH| Keystore Java. Can be an absolute path or relative to the properties file
BO_KEYSTORE_PASSWORD| Keystore password
BO_KEYMANAGER_PASWORD| Certificate password (Can be the same that the Keystore)
BO_TRUSTSTORE_PATH| Truststore (Warehouse of trust certificates). Can be an absolute path or relative to the properties file
BO_TRUSTSTORE_PASSWORD| Truststore password
BO_CERT_ALIAS| Alias of the certificate to use, optional if there are more than one in the Truststore

# Date Configurations
Property | Descriptions
---|---
FRONTEND_DATEPICKER_MASK_NAME| Default mask name for the use on the Date Picker component. E.g.: 'FE DatePicker'
FRONTEND_DATEPICKER_MASK_PATTERN| Default mask to be use in case that the declared mask name does not exists. E.g.: 'dd/mm/yy'
REPORTS_MASK_NAME| Default mask name for the use on the Reports. E.g.: 'Reports Mask'

# General
Property | Value
---|---
BO_LANGUAGE| Language for core Backoffice( e.g.: es or en)
BO_COUNTRY| Country for Core Backoffice(e.g. AR or EN)
BO_MONITOR_PORT| port for view monitor
BO_MONITOR_ENABLED| enable view monitor
BO_LOAD_CACHE| enable cache
BO_LOAD_DEMAND| enable load on demand
BO_FTP_HOST| host for FTP
BO_FTP_PORT| port for FTP
BO_INSTITUTION_ID| Institution database id to use (e.g. 1111)
BO_CHANNEL_ID| Channel database id to use

## Tracing Configuration
Tracing is implemented using the [opentracing api](https://opentracing.io/), [spring cloud opentracing](https://github.com/opentracing-contrib/java-spring-cloud) and custom filters.
The tracer used is the [elastic apm opentracing bridge](https://www.elastic.co/guide/en/apm/agent/java/1.x/opentracing-bridge.html). You must have the elastic agent apm to be able to see the traces in kibana.

Environment variable | Description
 --- | ---
ELASTIC_APM_APPLICATION_PACKAGES | String value to indicate name package to tracking example: "com.technisys"
ELASTIC_APM_CENTRAL_CONFIG | Boolean value to say if config is centralize
ELASTIC_APM_DISABLE_INSTRUMENTATIONS | Array string value to disable instrumentation example> "jdbc, redis"
ELASTIC_APM_ENABLE_LOG_CORRELATION | Boolean value to enable correlation log
ELASTIC_APM_LOG_FORMAT_SOUT | String value to indicate format output log example: "JSON"
ELASTIC_APM_LOG_LEVEL | String value to indicate level log example: "INFO"
ELASTIC_APM_PROFILING_INFERRED_SPANS_ENABLED | Boolean value to enable inferred spans
ELASTIC_APM_PROFILING_INFERRED_SPANS_EXCLUDED_CLASSES | String value to indicate what class will be exclude in the span
ELASTIC_APM_PROFILING_INFERRED_SPANS_INCLUDED_CLASSES | String value to indicate what class will be include in the span
ELASTIC_APM_SERVER_URLS | String value to indicate URL of apm-server
ELASTIC_APM_SERVICE_NAME | String value to indicate name of application who implement APM
ELASTIC_APM_USE_PATH_AS_TRANSACTION_NAME | Boolean value to indicate if path will be use as transaction name

## DB or FS Configuration

There are two variables to configure the backoffice with fs, by default it is configured for DB

JAVA_OPTS = -Dview-server.classLoader=net.technisys.cyberbank.view.config.service.controller.factory.ControllerFileSystemFactory
-Dview-server.viewFsPath= /path/viewfs

**view-server.classLoader** In this variable must be filled with the factory class fs or db.

**view-server.viewFsPath** In this variable must be filled with the path of viewfs.

## Documentation

All the documentation for the microservice can be found in this [Confluence](https://technisys.atlassian.net/wiki/spaces/TC/pages/2159248073/Cyberbank+Core+-+Backoffice)

More about [Dates configurations](https://technisys.atlassian.net/wiki/spaces/TC/pages/2295398433/Dates+Pattern+configuration+in+Back+Office)
