module.exports = {
    env: {
        browser: true,
        commonjs: false,
        es6: true,
        jest: true,
        node: true,
    },
    extends: ["airbnb", "prettier", "prettier/react"],
    globals: {
        Atomics: "readonly",
        SharedArrayBuffer: "readonly",
    },
    parser: "babel-eslint",
    parserOptions: {
        ecmaFeatures: {
            generators: true,
            jsx: true,
        },
        ecmaVersion: 2018,
        sourceType: "module",
    },
    plugins: ["prettier", "react", "react-hooks"],
    settings: {
        react: {
            pragma: "React",
            version: "16.3",
        },
        "import/resolver": {
            node: {
                paths: ["src"],
            },
        },
    },
    rules: {
        camelcase: "warn",
        curly: ["error", "all"],
        "consistent-return": 0,
        "import/prefer-default-export": "off",
        indent: [
            "off",
            4,
            {
                SwitchCase: 1,
                VariableDeclarator: 1,
                outerIIFEBody: 1,
                ArrayExpression: 1,
                ObjectExpression: 1,
                ImportDeclaration: 1,
                flatTernaryExpressions: false,
                ignoredNodes: ["JSXElement", "JSXElement *"],
                FunctionDeclaration: {
                    parameters: 1,
                    body: 1,
                },
                FunctionExpression: {
                    parameters: 1,
                    body: 1,
                },
                CallExpression: {
                    arguments: 1,
                },
            },
        ],
        "jsx-a11y/label-has-for": 0,
        "lines-between-class-members": ["warn"],
        "max-len": [
            "warn",
            {
                code: 120,
                ignoreComments: false,
                ignoreTrailingComments: false,
                ignoreUrls: false,
                ignoreStrings: false,
                ignoreTemplateLiterals: false,
                ignoreRegExpLiterals: false,
            },
        ],
        "new-cap": "warn",
        "no-plusplus": ["error", { allowForLoopAfterthoughts: true }],
        quotes: ["off", "double"],
        radix: ["error", "as-needed"],
        "react/destructuring-assignment": "warn",
        "react/jsx-closing-tag-location": 0,
        "react/jsx-filename-extension": [1, { extensions: [".js", ".jsx"] }],
        "react/jsx-indent": ["off", 4],
        "react/jsx-indent-props": ["off", 4],
        "react/jsx-wrap-multilines": 0,
        "react/prefer-stateless-function": "off",
        "react/prop-types": ["warn"],
        "react-hooks/rules-of-hooks": "error", // Checks rules of Hooks
        "react-hooks/exhaustive-deps": "warn", // Checks effect dependencies
    },
};
