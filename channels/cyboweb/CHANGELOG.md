# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Version Management Policy and Source Code Management](https://technisys.atlassian.net/wiki/spaces/TECOMNI/pages/141523/Version+Management+Policy+and+Source+Code+Management).

## [1.2.4]
### Changed
- Update platform libraries: pages to v1.2.90, hooks to v1.1.13 and components library to v1.2.33 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Menu - Adjustment in the icons when opening and closing [TECDIGSK-608](https://technisys.atlassian.net/browse/TECDIGSK-608)
- Cancel option enabled when invitation code status is expired. [TECDIGSK-600](https://technisys.atlassian.net/browse/TECDIGSK-600)
- SASS - correction of errors in the inputs [TECDIGSK-619](https://technisys.atlassian.net/browse/TECDIGSK-619)
- Fix filter value when pre-loading form. [TECDIGSK-610](https://technisys.atlassian.net/browse/TECDIGSK-610)
- New UI - Change layout of User Management [TECDIGSK-642](https://technisys.atlassian.net/browse/TECDIGSK-642)
- SASS - Correction of errors in navigation, change of navigation width [TECDIGSK-657](https://technisys.atlassian.net/browse/TECDIGSK-657)
- Adjust inputs with design of the component [TECDIGSK-605](https://technisys.atlassian.net/browse/TECDIGSK-605)
- Add white background (card) to edit and detail pages [TECDIGSK-598](https://technisys.atlassian.net/browse/TECDIGSK-598)
- SCSS - Bugs and design improvements [TECDIGSK-613](https://technisys.atlassian.net/browse/TECDIGSK-613)
- SASS - Bug in pending actions, the fields were together [TECDIGSK-638](https://technisys.atlassian.net/browse/TECDIGSK-638)
- SCSS - widget (dashboard) extend card styles [TECDIGSK-621](https://technisys.atlassian.net/browse/TECDIGSK-621)
- SCSS - Fix button dropdown [TECDIGSK-655](https://technisys.atlassian.net/browse/TECDIGSK-655)
- Remove Sass files from platform/components to move them to Starterkit. Node-Sass dependency removed, using Sass library instead. [TECDIGSK-631](https://technisys.atlassian.net/browse/TECDIGSK-631)
- Fix invitation codes module access type field duplicated filed removed [TECDIGSK-601](https://technisys.atlassian.net/browse/TECDIGSK-601)

## [1.2.3]
### Changed
- Update platform libraries: pages to v1.2.65, hooks to v1.1.10 and components library to v1.2.23 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Invitation Code cancellation implemented [TECDIGSK-555](https://technisys.atlassian.net/browse/TECDIGSK-555)
- Invitation Code resend implemented [TECDIGSK-596](https://technisys.atlassian.net/browse/TECDIGSK-596)
- Page action buttons moved to the header. Footers won't contain buttons anymore and the header will always be stick to the top. [TECDIGSK-556](https://technisys.atlassian.net/browse/TECDIGSK-556)
- Workspace id added to workspace column within Invitation Codes List. [TECCDPBO-4841](https://technisys.atlassian.net/browse/TECCDPBO-4841)
- Profile picture bug fix [TECDIGSK-574](https://technisys.atlassian.net/browse/TECDIGSK-574)
- Invitation codes pop-up window message adjusted when cancelling [TECDIGSK-575](https://technisys.atlassian.net/browse/TECDIGSK-575)
- Fix SASS to remove Bootstrap and remove files the Bootstrap [TECDIGSK-583](https://technisys.atlassian.net/browse/TECDIGSK-583)
- Menu functionality settings, the submenu opens with click and not with hover like now [TECDIGSK-570](https://technisys.atlassian.net/browse/TECDIGSK-570)
- New UI details in transactions and audit [TECDIGSK-565](https://technisys.atlassian.net/browse/TECDIGSK-565)
- Menu Items Replacemente implemented for projects to be able to customize them [TECDIGSK-591](https://technisys.atlassian.net/browse/TECDIGSK-591)

## [1.2.2] - 2021-08-23
### Changed
- Update platform libraries: pages to v1.2.48, hooks to v1.1.8 and components library to v1.2.16 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Invitation Codes Send feature implemented [TECDIGSK-485](https://technisys.atlassian.net/browse/TECDIGSK-485)
- Invitation Codes Detail page implemented [TECDIGSK-532](https://technisys.atlassian.net/browse/TECDIGSK-532)
- New design in breadcrumb and yes/no radio buttons group [TECDIGSK-507](https://technisys.atlassian.net/browse/TECDIGSK-507)
- Functional error on menu fixed [TECDIGSK-518](https://technisys.atlassian.net/browse/TECDIGSK-518)
- Fix console error warnings [TECDIGSK-452](https://technisys.atlassian.net/browse/TECDIGSK-452)
- Remove styles for uppercase [TECDIGSK-511](https://technisys.atlassian.net/browse/TECDIGSK-511)
- Bug fix SASS - design improvements [TECDIGSK-513](https://technisys.atlassian.net/browse/TECDIGSK-513)
- Style bug fixes in menu icons [TECDIGSK-528](https://technisys.atlassian.net/browse/TECDIGSK-528)
- Bug fix of styles in general conditions [TECDIGSK-522](https://technisys.atlassian.net/browse/TECDIGSK-522)
- Bug fix of filter rules in campaigns [TECDIGSK-539](https://technisys.atlassian.net/browse/TECDIGSK-539)
- Bug fix field alignment in invitation codes [TECDIGSK-541](https://technisys.atlassian.net/browse/TECDIGSK-541)
- Design improvements on the invitation codes detail page [TECDIGSK-543](https://technisys.atlassian.net/browse/TECDIGSK-543)
- Fix status filter not working on Invitation Codes list [TECDIGSK-542](https://technisys.atlassian.net/browse/TECDIGSK-542)
- Fix: dates filter not showing time pick on Invitation Codes list [TECDIGSK-542](https://technisys.atlassian.net/browse/TECDIGSK-553)

## [1.2.1] - 2021-08-06
### Added
- Invitation Codes List feature implemented [TECCDPBO-4396](https://technisys.atlassian.net/browse/TECCDPBO-4396)

### Changed
- Update platform libraries: pages to v1.2.40, hooks to v1.1.7 and components library to v1.2.14 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Styles for uppercase and lowercase removed [TECDIGSK-511](https://technisys.atlassian.net/browse/TECDIGSK-511)
- Fix header to make it static when page is scrolled [TECCDPBO-4701](https://technisys.atlassian.net/browse/TECCDPBO-4701)
- SASS - Accordion component refactor [TECCDPBO-4683](https://technisys.atlassian.net/browse/TECCDPBO-4683)
- SASS - Card component refactor [TECCDPBO-4663](https://technisys.atlassian.net/browse/TECCDPBO-4663)

## [1.2.0] - 2021-08-02
### Added
-   General Conditions List feature implemented [TECCDPBO-4322](https://technisys.atlassian.net/browse/TECCDPBO-4322)

### Changed
- Update platform libraries: pages to v1.2.36, hooks to v1.1.7 and components library to v1.2.12 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Fix link display inside tables [TECDIGSK-472](https://technisys.atlassian.net/browse/TECCDPBO-472)
- New containers for list tables. Filter's primary button moved to the bottom. [TECCDPBO-4317](https://technisys.atlassian.net/browse/TECCDPBO-4317)
- Refactor on listings and filters in list pages [TECCDPBO-4359](https://technisys.atlassian.net/browse/TECCDPBO-4359)
- Fix in the top of the modal when the results are many [TECCDPBO-4209](https://technisys.atlassian.net/browse/TECCDPBO-4209)
- Icons that were not used in cybo were deleted [TECCDPBO-4370](https://technisys.atlassian.net/browse/TECCDPBO-4370)
- The new design was applied in workspace [TECCDPBO-4381](https://technisys.atlassian.net/browse/TECCDPBO-4381)
- React version upgraded to v17.0.2 [TECCDPBO-4471](https://technisys.atlassian.net/browse/TECCDPBO-4471)
- The option to deactivate was not shown in System Availability [TECCDPBO-4491](https://technisys.atlassian.net/browse/TECCDPBO-4491)
- Design bug fix [TECCDPBO-4498](https://technisys.atlassian.net/browse/TECCDPBO-4498)
- Fix bugs on new desing [TECCDPBO-4507](https://technisys.atlassian.net/browse/TECCDPBO-4507)
- New sass style for cybo menu [TECCDPBO-4479](https://technisys.atlassian.net/browse/TECCDPBO-4479)
- Bug fix SASS - Adjusted spinner alignment [TECCDPBO-3259](https://technisys.atlassian.net/browse/TECCDPBO-3259)
- Bug fix SASS - Campaigns alignment adjustment [TECCDPBO-4529](https://technisys.atlassian.net/browse/TECCDPBO-4529)
- Bug fix Menu New Item [TECCDPBO-4479](https://technisys.atlassian.net/browse/TECCDPBO-4479)
- Fix in the Workspace General Tab to accept incoming additional data in JSON format, with non-string values. [TECCDPBO-4579](https://technisys.atlassian.net/browse/TECCDPBO-4579)
- SASS - Design details in general conditions [TECCDPBO-4516](https://technisys.atlassian.net/browse/TECCDPBO-4516)
- Login page new design implemented [TECCDPBO-4477](https://technisys.atlassian.net/browse/TECCDPBO-4477)
- New desing in error pages [TECCDPBO-4594](https://technisys.atlassian.net/browse/TECCDPBO-4594)
- Dashboard page new design implemented [TECCDPBO-4646](https://technisys.atlassian.net/browse/TECCDPBO-4646)
- Bug SASS - fix in text type button when disabled [TECDIGSK-521](https://technisys.atlassian.net/browse/TECDIGSK-521)

### Added
- General Conditions List feature implemented [TECCDPBO-4322](https://technisys.atlassian.net/browse/TECCDPBO-4322)
- General Conditions styles [TECCDPBO-4323](https://technisys.atlassian.net/browse/TECCDPBO-4323)

## [1.1.5] - 2021-06-21
### Changed
- Update platform pages library to v1.1.139, components library to v1.1.62, and hooks library to v1.1.3 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Update base SASS components to adapt to the new design [TECCDPBO-4181](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Bug fix SASS components to the new design [TECCDPBO-4350](https://technisys.atlassian.net/browse/TECCDPBO-4350)
- Updated SASS component for Modal component in order to add a coloured border-top according to the modal type. [TECDIGSK-438](https://technisys.atlassian.net/browse/TECDIGSK-438)
- Fix Cyboweb Docker image repository reference [TECENG-1559](https://technisys.atlassian.net/browse/TECENG-1559)

## [1.1.4] - 2021-02-24
### Added
- Update Cybo pipelines with agnostic definition based on Docker imag [TECENG-1110](https://technisys.atlassian.net/browse/TECENG-1110)
- Added starterkit version of cybo [TECDIGSK-382](https://technisys.atlassian.net/browse/TECDIGSK-382)
- Implement auto-versioning of Helm Chart with version [TECENG-732](https://technisys.atlassian.net/browse/TECENG-732)

### Changed
- Update platform pages library to v1.1.136 and components library to v1.1.60 [TECDIGSK-387](https://technisys.atlassian.net/browse/TECDIGSK-387)
- Removed commons dependency from Helm chart and added resource limitation.[TECENG-663](https://technisys.atlassian.net/browse/TECENG-663).
- Refactor CICD pipelines to use profile templates [TECCDPBO-2550](https://technisys.atlassian.net/browse/TECCDPBO-2550)
