import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import { App } from "technisys-cybo-pages";
import { StarterKitContext } from "technisys-cybo-components";
import * as serviceWorker from "./serviceWorker";
import "styles/custom.scss";
import { locales, i18nPaths, customRoute, menuItems } from "./configurations";
import pendingActionDetailComponents from "./components/PendingActionsDetailComponents";
import { indicators as campaignIndicators } from "./components/Campaigns";
import { indicators as cohortsIndicators } from "./components/Cohorts";
import { environment } from "./components/Environment";

// TODO: Remove this conditional when completes the Frontend migration from Legacy to Cybo
if (window.location.pathname.includes(";")) {
    const [path] = window.location.pathname.split(";");
    window.location.pathname = path;
}

ReactDOM.render(
    <StarterKitContext.Provider
        value={{
            customRoute,
            pendingActionDetailComponents,
            environment,
            i18nPaths,
            locales,
            campaignIndicators,
            cohortsIndicators,
            menuItems,
        }}>
        <App />
    </StarterKitContext.Provider>,
    document.getElementById("root"),
);

// If you want your App to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
