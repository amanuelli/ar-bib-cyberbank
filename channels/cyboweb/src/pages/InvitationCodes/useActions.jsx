import { useCallback } from "react";
import { usePatch, usePost } from "technisys-cybo-hooks";
import { useStatusChange, useApiRequest } from "technisys-cybo-pages";
import { generateConfirmationDialogHandler, getNotificationValue, getI18nKey, getFullName } from "./utils";

const useActions = ({ data, requestData, url, setState }) => {
    const { onStatusChangedSuccess, onStatusChangedError, handleDialogDismiss } = useStatusChange({
        getAction: ({ action }) => action,
        requestData,
        i18nPrefix: getI18nKey(),
        setState,
        getNotificationValue,
        selectedItemLength: 1,
    });

    const [, updateStatus] = useApiRequest(usePatch, {
        url: `${url}/${data.id}/cancel`,
        onSuccess: onStatusChangedSuccess,
        onError: onStatusChangedError,
    });

    const [, resendCode] = useApiRequest(usePost, {
        url: `${url}/${data.id}/resend`,
        onSuccess: onStatusChangedSuccess,
        onError: onStatusChangedError,
    });

    const name = getFullName(data);
    const { email, documentNumber, documentType, accountName } = data;

    function handleAction(action, comments) {
        if (action === "cancel") {
            updateStatus({ ...data, name, comments }, { action });
        }
        if (action === "resend") {
            resendCode({ ...data, name, comments }, { action });
        }
    }

    const showConfirmationDialog = generateConfirmationDialogHandler(
        [{ name, email, documentNumber, documentType, accountName }],
        [{}],
        handleAction,
        handleDialogDismiss,
        setState,
    );

    const generateOptions = useCallback(() => {
        const options = [];
        if (data.status === "NOT_USED") {
            options.push({
                name: getI18nKey("actions.resend"),
                value: "resend",
            });
            options.push({
                name: getI18nKey("actions.cancel"),
                value: "cancel",
            });
        }
        return options;
    }, [data]);

    function actionsHandler(option, event) {
        event.stopPropagation();
        showConfirmationDialog(option);
    }

    return {
        actionsOptions: generateOptions(),
        actionsHandler,
    };
};

export default useActions;
