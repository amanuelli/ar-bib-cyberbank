import React, { useRef, useMemo } from "react";
import { func, shape, bool } from "prop-types";
import {
    searchFilters,
    FieldsContext,
    FormikForm,
    noop,
    Row,
    DatePickerField,
    SelectField,
    InputField,
    View,
} from "technisys-cybo-components";
import { useDismissComponent } from "technisys-cybo-hooks";
import { getI18nKey } from "../../utils";
import { validationSchema } from "./validationSchema";

function Filters(props) {
    const { visible, filters: filtersProps, formRef, onApplyFilters, handleOpenFilters, resources } = props;
    const { filters } = filtersProps;
    const defaultOptionLabelI18n = "cybo.general.filters.select.default";

    const componentRef = useRef();

    const statusOptions = useMemo(
        () =>
            resources &&
            resources.statuses.map((status) => ({
                value: status.id,
                i18n: `backoffice.invitationCodes.status.${status.id}`,
            })),
        [resources],
    );

    const onChangeDate = (id, value, { setFieldValue }) => {
        setFieldValue(id, value ? new Date(value) : null);
    };

    useDismissComponent({
        isVisible: true,
        ref: componentRef.current,
        onDismiss: handleOpenFilters,
        classes: ["advanced-search-icon", "advanced-search-inline"],
    });

    if (!visible) {
        return null;
    }

    if (!resources) {
        return <View className="filters-loading" />;
    }

    const initialValues = {
        creationDateFrom: filters.creationDateFrom,
        creationDateTo: filters.creationDateTo,
        status: filters.status,
        documentNumber: filters.documentNumber,
        email: filters.email,
        mobileNumber: filters.mobileNumber,
        productGroupName: filters.productGroupName,
    };

    const submitHandler = (values, { setErrors }) => {
        const newFilters = searchFilters(values, [
            { attribute: "creationDateFrom" },
            { attribute: "creationDateTo" },
            { attribute: "status" },
            { attribute: "documentNumber" },
            { attribute: "email" },
            { attribute: "mobileNumber" },
            { attribute: "productGroupName" },
        ]);
        onApplyFilters({ page: 1, filters: newFilters, filterValues: values }, Object.keys(newFilters), { setErrors });
    };

    return (
        <div ref={componentRef}>
            <FormikForm
                id="filters-form"
                initialValues={initialValues}
                onSubmit={submitHandler}
                formRef={formRef}
                validationSchema={validationSchema}>
                {(form) => (
                    <FieldsContext.Provider value={{ i18nPrefix: getI18nKey("search.advanced.fields") }}>
                        <Row className="row-spacing">
                            <InputField fieldName="documentNumber" width="3" />
                            <InputField fieldName="email" width="3" />
                            <InputField fieldName="mobileNumber" width="3" />
                            <InputField fieldName="productGroupName" width="3" />
                        </Row>
                        <Row>
                            <SelectField
                                fieldName="status"
                                options={statusOptions}
                                defaultOptionLabelI18n={defaultOptionLabelI18n}
                                width="3"
                                optional
                            />
                            <DatePickerField
                                name="creationDateFrom"
                                onChange={(value) => onChangeDate("creationDateFrom", value, form)}
                                timeSection
                                width="3"
                            />
                            <DatePickerField
                                name="creationDateTo"
                                onChange={(value) => onChangeDate("creationDateTo", value, form)}
                                timeSection
                                width="3"
                            />
                        </Row>
                    </FieldsContext.Provider>
                )}
            </FormikForm>
        </div>
    );
}

Filters.propTypes = {
    formRef: shape({
        current: undefined,
    }).isRequired,
    visible: bool,
    resources: shape({}),
    filters: shape({
        filters: shape({}),
    }),
    onApplyFilters: func,
    handleOpenFilters: func,
};

Filters.defaultProps = {
    visible: true,
    resources: {},
    filters: { filters: {} },
    onApplyFilters: noop,
    handleOpenFilters: noop,
};

export default Filters;
