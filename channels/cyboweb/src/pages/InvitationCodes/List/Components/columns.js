/* eslint-disable react/prop-types */
import React from "react";
import { getMessage, SortableHeader, I18n, View } from "technisys-cybo-components";
import { calculateStatusColor, calculateStatusLabel, getI18nKey } from "../../utils";

export default (props) => {
    const { messages, language, orderParams, onSortHandler, i18nPrefix, dateFormatter } = props;
    const { formatDate } = dateFormatter;

    return [
        {
            id: "productGroupName",
            Header: <I18n id={getI18nKey("list.columns.productGroupName")} />,
            Cell: ({
                row: {
                    original: { productGroupId, productGroupName },
                },
            }) => `${productGroupId} ${productGroupName}`.trim(),
        },
        {
            id: "name",
            Header: <I18n id={getI18nKey("list.columns.name")} />,
            Cell: ({
                row: {
                    original: { firstName, lastName },
                },
            }) => `${firstName} ${lastName}`.trim(),
        },
        {
            id: "document",
            Header: <I18n id={getI18nKey("list.columns.document")} />,
            Cell: ({
                row: {
                    original: { documentNumber, documentType },
                },
            }) => (
                <View className="d-flex-column">
                    <View>{documentNumber}</View>
                    <View>
                        <I18n id={`documentType.label.${documentType}`} />
                    </View>
                </View>
            ),
        },
        {
            id: "email",
            Header: <I18n id={getI18nKey("list.columns.email")} />,
            accessor: "email",
        },
        {
            id: "mobileNumber",
            Header: <I18n id={getI18nKey("list.columns.mobileNumber")} />,
            accessor: "mobileNumber",
        },
        {
            id: "creationDate",
            Header: () => {
                return (
                    <SortableHeader
                        text={getMessage(messages, `${i18nPrefix}.list.columns.creationDate`)}
                        attributeId="creation_date"
                        orderDir={orderParams && orderParams.orderBy === "creation_date" ? orderParams.orderDir : null}
                        onHeaderClick={onSortHandler}
                    />
                );
            },
            Cell: ({ cell: { value } }) => formatDate(value, language),
            accessor: "creationDate",
        },
        {
            id: "status",
            Header: <I18n id={getI18nKey("list.columns.status")} />,
            Cell: ({ row: { original } }) => {
                return (
                    <View className={`states states--${calculateStatusColor(original)}`}>
                        <I18n id={calculateStatusLabel(original)} />
                    </View>
                );
            },
            accessor: "status",
        },
    ];
};
