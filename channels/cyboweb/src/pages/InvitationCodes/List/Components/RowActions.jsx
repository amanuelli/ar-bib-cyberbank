import React from "react";
import { string, number, shape, func, bool } from "prop-types";
import { EditValueModal, DropdownButton, View } from "technisys-cybo-components";
import { useSetState } from "technisys-cybo-hooks";
import useActions from "../../useActions";

const RowActions = ({ data, requestData, url, canManage }) => {
    const { state, setState } = useSetState({
        dialog: { isOpen: false },
    });

    const { actionsOptions, actionsHandler } = useActions({ data, requestData, url, setState });

    if (data.status !== "NOT_USED" || !canManage) {
        return null;
    }

    return (
        <>
            <DropdownButton
                className="dropdown-button-single-actions"
                onOptionClick={actionsHandler}
                items={actionsOptions}
                icon="dropdown-button-single-action-icon"
                i18n
                listDownRight
                expandListWidth
                id={data.id}>
                <View />
            </DropdownButton>
            <EditValueModal {...state.dialog} />
        </>
    );
};

RowActions.propTypes = {
    data: shape({
        id: number,
        firstName: string,
        lastName: string,
    }).isRequired,
    i18nPrefix: string,
    requestData: func.isRequired,
    url: string,
    canManage: bool,
};

RowActions.defaultProps = {
    i18nPrefix: "cybo.invitationCodesManagement",
    url: "/v1/invitationcodes",
    canManage: false,
};

export default RowActions;
