import * as Yup from "yup";
import { emailRegex } from "technisys-cybo-components";
import { getI18nKey } from "../../utils";

export const validationSchema = () => {
    return Yup.lazy(() =>
        Yup.object().shape({
            documentNumber: Yup.string()
                .notRequired()
                .nullable(),
            email: Yup.string()
                .matches(emailRegex, "cybo.general.validations.format")
                .notRequired()
                .nullable(),
            mobileNumber: Yup.string()
                .notRequired()
                .nullable(),
            productGroupName: Yup.string()
                .notRequired()
                .nullable(),
            creationDateFrom: Yup.date()
                .notRequired()
                .nullable(),
            creationDateTo: Yup.date()
                .notRequired()
                .nullable()
                .when("creationDateFrom", (creationDateFrom, schema) => {
                    if (creationDateFrom === null || !(creationDateFrom instanceof Date)) {
                        return schema;
                    }

                    return schema.min(
                        creationDateFrom,
                        getI18nKey("search.advanced.validations.creationDateTo.mustBeGreater"),
                    );
                }),
        }),
    );
};
