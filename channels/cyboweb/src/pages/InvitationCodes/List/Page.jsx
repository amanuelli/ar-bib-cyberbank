import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { View, I18n, Button, I18nContext, SessionContext } from "technisys-cybo-components";
import { useResources } from "technisys-cybo-hooks";
import { Page as PageComponent, Export, useDates, useNormalizers, StaticFilter } from "technisys-cybo-pages";
import { getI18nKey, canManage } from "../utils";
import { columns, Filters, RowActions } from "./Components";

function Page() {
    const { language } = useContext(I18nContext);
    const { session } = useContext(SessionContext);
    const { formatFullDate } = useDates();
    const history = useHistory();

    const { data: resources } = useResources({
        resources: [{ name: "statuses", url: "/v1/invitationcodes/statuses" }],
    });

    const { normalizeI18n } = useNormalizers();

    const normalizeDate = (date1, date2) => {
        const from = date1 ? formatFullDate(date1, language) : "N/A";
        const to = date2 ? formatFullDate(date2, language) : "N/A";
        return `${from} - ${to}`;
    };

    const filtersChipsConfig = {
        creationDateFrom: { id: "creationDate", readonly: true, normalizer: normalizeDate, ordinal: 0 },
        creationDateTo: { id: "creationDate", readonly: true, normalizer: normalizeDate, ordinal: 1 },
        status: { id: "status", normalizer: normalizeI18n("backoffice.invitationCodes.status") },
        email: "email",
        documentNumber: "documentNumber",
        mobileNumber: "mobileNumber",
        productGroupName: "productGroupName",
    };

    const handleRowClicked = ({ original: { id } }) => {
        history.push(`/invitationcodes/${id}`);
    };

    const secondaryActions = () => {
        if (canManage(session)) {
            return (
                <View className="secondary-button-container">
                    <Button onClick={() => history.push("/invitationcodes/create-invitation-code")} primary>
                        <I18n id={getI18nKey("actions.add")} />
                    </Button>
                </View>
            );
        }
        return null;
    };

    const extraActions = (filters) => (
        <Export
            i18nPrefix={getI18nKey()}
            currentFilters={filters}
            url="v1/invitationcodes/export"
            filename="export_invitation_codes"
            defaultOrderBy="creation_date"
        />
    );

    return (
        <PageComponent
            i18nPrefix={getI18nKey()}
            tableColumns={columns}
            canManage={canManage(session)}
            rowActions={RowActions}
            filtersComponent={StaticFilter}
            filtersComponentProps={{
                resources,
                filtersChipsConfig,
            }}
            urlToConsume="/v1/invitationcodes"
            extraActions={extraActions}
            secondaryActions={secondaryActions}
            filtersFields="creationDateFrom,creationDateTo,email,documentNumber,mobileNumber,status,productGroupName"
            advancedFilters={Filters}
            onRowClicked={handleRowClicked}
            searchOnMount={false}
        />
    );
}

export default Page;
