import React, { useContext } from "react";
import { shape, func, arrayOf, bool } from "prop-types";
import { Button, I18n, Row, InputField, SelectField, SessionContext, View } from "technisys-cybo-components";
import { useGet } from "technisys-cybo-hooks";
import { get, useApiRequest } from "technisys-cybo-pages";
import { getI18nKey } from "../../utils";

function SearchUser({ data, form, hasUser, setHasUser, isLoading, searchUser, readonly }) {
    const { configuration } = useContext(SessionContext);
    const defaultType = get(configuration, "backoffice.customers.document.type.default");

    const [{ data: typeResp }, fetchDocumentTypes] = useApiRequest(useGet, {
        url: "/v1/documentTypes/customers",
        redirectOnError: false,
    });

    const documentCountries = data?.documentCountries?.map(({ id }) => ({
        value: id,
        i18n: `country.name.${id}`,
    }));

    const documentTypes = typeResp
        ? typeResp.data.map((type) => ({
              value: type,
              i18n: `documentType.label.${type}`,
          }))
        : [{ value: defaultType, i18n: `documentType.label.${defaultType}` }];

    const findUser = () => {
        const { documentCountry, documentType, documentNumber } = form.values;
        if (!documentNumber) {
            form.setFieldError("documentNumber", "cybo.general.validations.empty");
            return;
        }
        searchUser({ params: { documentCountry, documentType, documentNumber } }, form);
    };

    const clickHandler = () => {
        if (hasUser) {
            setHasUser(false);
        } else {
            findUser(form);
        }
    };

    const getAction = () => {
        return hasUser ? "change" : "search";
    };

    return (
        <>
            <View className="card__header">
                <View className="card-title">
                    <I18n id={getI18nKey(`customer.${getAction()}`)} />
                </View>
            </View>

            {!hasUser && (
                <>
                    <Row>
                        <SelectField
                            fieldName="documentCountry"
                            options={documentCountries}
                            onChange={(_, newVal) => {
                                fetchDocumentTypes({ params: { country: newVal } });
                            }}
                            width="4"
                        />
                    </Row>
                    <Row>
                        <SelectField fieldName="documentType" options={documentTypes} width="4" />
                    </Row>
                    <Row>
                        <InputField fieldName="documentNumber" width="4" onEnterKeyDown={clickHandler} />
                    </Row>
                </>
            )}
            {hasUser && (
                <>
                    <Row>
                        <InputField fieldName="name" readonly width="4" />
                        <InputField fieldName="email" readonly width="4" />
                    </Row>
                    <Row>
                        <SelectField fieldName="documentCountry" options={documentCountries} width="4" readonly />
                        <InputField fieldName="mobileNumber" readonly width="4" />
                    </Row>
                    <Row>
                        <SelectField fieldName="documentType" options={documentTypes} width="4" readonly />
                    </Row>
                    <Row>
                        <InputField fieldName="documentNumber" readonly width="4" />
                    </Row>
                </>
            )}

            {!readonly && (
                <Button onClick={clickHandler} isLoading={isLoading} primary={!hasUser} btnLink={hasUser}>
                    <I18n id={getI18nKey(`action.${getAction()}`)} />
                </Button>
            )}
        </>
    );
}

SearchUser.propTypes = {
    data: shape({
        documentCountries: arrayOf(shape({})),
    }).isRequired,
    form: shape({
        values: shape({}),
    }).isRequired,
    hasUser: bool.isRequired,
    setHasUser: func.isRequired,
    isLoading: bool.isRequired,
    searchUser: func.isRequired,
    readonly: bool,
};

SearchUser.defaultProps = {
    readonly: false,
};

export default SearchUser;
