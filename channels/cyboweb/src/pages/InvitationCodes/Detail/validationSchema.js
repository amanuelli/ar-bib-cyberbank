import * as Yup from "yup";

export const validationSchema = () => {
    return Yup.object().shape({
        documentNumber: Yup.string().required("cybo.general.validations.empty"),
        language: Yup.string().required("cybo.general.validations.empty"),
        accessType: Yup.string().required("cybo.general.validations.empty"),
        account: Yup.string().required("cybo.general.validations.empty"),
        schema: Yup.string().when(["environment"], {
            is: (environment) => !environment,
            then: Yup.string().required("cybo.general.validations.empty"),
            otherwise: Yup.string().notRequired(),
        }),
        signatureLevel: Yup.string().when(["environment"], {
            is: (environment) => !environment,
            then: Yup.string().required("cybo.general.validations.empty"),
            otherwise: Yup.string().notRequired(),
        }),
        comments: Yup.string().max(200),
    });
};
