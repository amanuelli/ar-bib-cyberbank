import React, { useState, useEffect } from "react";
import { bool, string, shape } from "prop-types";
import { useHistory } from "react-router-dom";
import {
    SuccessNotification,
    ErrorNotification,
    PageLoading,
    Card,
    EditValueModal,
    Modal,
    generateNumeredDialogProps,
    noop,
} from "technisys-cybo-components";
import { useSetState, usePost } from "technisys-cybo-hooks";
import { PageContainer, useSubmit, useNotification } from "technisys-cybo-pages";
import Form from "./Form";
import { getI18nKey } from "../utils";
import useHeaderConfigurer from "./useHeaderConfig";

const url = "/v1/invitationcodes";

const Page = ({ data, mode, formRef, fetching }) => {
    const { evaluateApprovalRequired, createNotification } = useNotification({ i18nPrefix: getI18nKey() });
    const history = useHistory();

    const [hasUser, setHasUser] = useState(false);

    useEffect(() => {
        if (data?.invitationcode) {
            setHasUser(true);
        }
    }, [data?.invitationcode]);

    const { state, setState } = useSetState({
        dialog: { isOpen: false },
    });

    function closeModalView() {
        setState({
            dialog: { isOpen: false },
        });
    }

    function onSuccess(responseData, { action, sentData }) {
        if (!evaluateApprovalRequired(responseData)) {
            createNotification(SuccessNotification, action, sentData);
        }
        history.push(`/invitationcodes/`);
    }

    const onError = () => {
        createNotification(ErrorNotification, mode);
        closeModalView();
    };

    const [{ isLoading }, submit] = useSubmit({
        hook: usePost,
        url,
        onSuccess,
        onError,
    });

    const handleCancel = () => {
        if (formRef) {
            formRef.current.resetForm();
        }
        history.goBack();
    };

    const onConfirmationHandler = (values, { setErrors }, action) => {
        const { name, email, documentNumber, documentType, accountName } = values;
        const dialog = generateNumeredDialogProps(
            getI18nKey(`modals.${action || mode}`),
            [{ name, email, documentNumber, documentType, accountName }],
            [{}],
        );
        dialog.isOpen = true;
        dialog.type = Modal.types.warning;
        dialog.action = action || mode;
        dialog.onDismiss = () => closeModalView();
        dialog.onPrimaryButtonClick = (comments) => submit({ ...values, comments }, { action, setErrors });
        setState({ dialog });
    };

    const headerConfiguration = useHeaderConfigurer({
        data: data?.invitationcode || {},
        mode,
        hasUser,
        handleCancel,
        requestData: noop,
        url,
        setState,
    });

    if (!data || fetching) {
        return <PageLoading />;
    }

    return (
        <PageContainer headerProps={headerConfiguration}>
            <Card>
                <Form
                    formId="invitation-codes-form"
                    formRef={formRef}
                    data={data}
                    mode={mode}
                    onSubmit={onConfirmationHandler}
                    hasUser={hasUser}
                    setHasUser={setHasUser}
                />
            </Card>
            <EditValueModal {...state.dialog} primaryButtonProps={{ isLoading }} />
        </PageContainer>
    );
};

Page.propTypes = {
    data: shape({}),
    mode: string.isRequired,
    formRef: shape({
        current: undefined,
    }).isRequired,
    fetching: bool.isRequired,
};

Page.defaultProps = {
    data: null,
};

export default Page;
