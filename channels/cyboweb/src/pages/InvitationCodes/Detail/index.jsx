/* eslint-disable react-hooks/rules-of-hooks */
import React from "react";
import { string, shape } from "prop-types";
import { useHistory } from "react-router-dom";
import { ErrorNotification, userPageModes as PageModes } from "technisys-cybo-components";
import { useApiRequest, useNotification } from "technisys-cybo-pages";
import { useResources, useGetEntity } from "technisys-cybo-hooks";
import Page from "./Page";
import { getI18nKey } from "../utils";

const resourcesEndpoints = [
    { name: "documentCountries", url: "/v1/countryCodes/customers" },
    { name: "schemas", url: "/v1/invitationcodes/schemas" },
];

function CreateInvitationCodePage({ id, mode, formRef }) {
    const history = useHistory();
    const { createNotification: sendNotification } = useNotification({ getI18nKey });

    function Init() {
        const onRequestError = () => {
            sendNotification(ErrorNotification, "read", {});
            history.push("/invitationcodes");
        };

        if (mode === PageModes.CREATE) {
            return useApiRequest(useResources, {
                resources: resourcesEndpoints,
                onError: onRequestError,
                redirectOnError: false,
            });
        }

        return useApiRequest(useGetEntity, {
            name: "invitationcode",
            base: `/v1/invitationcodes/${id}`,
            relations: [],
            resources: resourcesEndpoints,
            onError: onRequestError,
            redirectOnError: false,
        });
    }

    const { fetching, data, loaded } = Init();

    return <Page id={id} data={data} mode={mode} formRef={formRef} fetching={fetching || !loaded} />;
}

CreateInvitationCodePage.propTypes = {
    id: string,
    mode: string.isRequired,
    formRef: shape({
        current: undefined,
    }).isRequired,
};

CreateInvitationCodePage.defaultProps = {
    id: undefined,
};

export default CreateInvitationCodePage;
