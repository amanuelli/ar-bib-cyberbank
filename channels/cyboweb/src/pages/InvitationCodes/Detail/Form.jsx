import React, { useContext } from "react";
import { shape, string, func, arrayOf, bool } from "prop-types";
import classNames from "classnames";
import {
    I18n,
    Row,
    View,
    TextAreaField,
    FormikForm,
    FieldsContext,
    InputField,
    LabelField,
    SelectField,
    Fields,
    SessionContext,
    userPageModes as PageModes,
} from "technisys-cybo-components";
import { useGet } from "technisys-cybo-hooks";
import { get, getArray, useApiRequest, useDates } from "technisys-cybo-pages";
import { getI18nKey, getFullName, calculateStatusColor, calculateStatusLabel } from "../utils";
import { validationSchema } from "./validationSchema";
import SearchUser from "./components/SearchUser";

function Form({ data, mode, formId, formRef, onSubmit, hasUser, setHasUser }) {
    const readonly = mode === PageModes.VIEW;
    const isCreating = mode === PageModes.CREATE;
    const { formatFullDate } = useDates();
    const { configuration } = useContext(SessionContext);
    const supportedLanguages = getArray(configuration, "core.languages");
    const accessTypes = getArray(configuration, "invitation.permissions.roleList.backoffice");

    const [{ isLoading: loadingUser, data: searchResponse }, fetchUser] = useApiRequest(useGet, {
        url: "/v1/invitationcodes/search-user",
        onSuccess: ({ data: { user, client } }, { setFieldValue, setErrors }) => {
            const auxUser = user || client;
            setFieldValue("name", getFullName(auxUser));
            setFieldValue("email", auxUser.email);
            setFieldValue("mobileNumber", auxUser.mobileNumber);
            const language = auxUser.lang;
            if (language) {
                setFieldValue("language", language);
            } else {
                setFieldValue("language", "");
            }
            setErrors({});
            setFieldValue("accessType", "");
            setFieldValue("account", "");
            setFieldValue("accountName", undefined);
            setFieldValue("segment", undefined);
            setFieldValue("schema", "");
            setFieldValue("signatureLevel", "");
            setHasUser(true);
        },
        onError: (response, { setFieldError }) => {
            const { data: dataErrors } = response.data;
            Object.keys(dataErrors).forEach((field) => {
                setFieldError(field, dataErrors[field]);
            });
            setHasUser(false);
        },
    });

    const initialValues = () => {
        const defaultCountry = get(configuration, "backoffice.customers.country.code.default");
        const defaultType = get(configuration, "backoffice.customers.document.type.default");

        if (isCreating) {
            return {
                documentCountry: defaultCountry,
                documentType: defaultType,
                documentNumber: "",
                name: "",
                email: "",
                mobileNumber: "",
                language: "",
                accessType: "",
                account: "",
                accountName: undefined,
                segment: undefined,
                schema: "",
                signatureLevel: "",
                comments: "",
            };
        }

        return {
            ...data.invitationcode,
            name: getFullName(data.invitationcode),
            code: data.invitationcode.invitationCodeMasked,
            creationDate: formatFullDate(new Date(data.invitationcode.creationDate)),
            expirationDate: formatFullDate(new Date(data.invitationcode.expirationDate)),
        };
    };

    const getLanguages = () => {
        return supportedLanguages.map((lang) => ({
            value: lang,
            i18n: `cybo.general.interface.language.${lang}`,
        }));
    };

    const getAccessTypes = () => {
        return accessTypes.map((type) => ({
            value: type,
            i18n: `backoffice.invitation.role.${type}`,
        }));
    };

    const getSchemas = () => {
        if (readonly && data.invitationcode) {
            return [
                {
                    value: data.invitationcode.account,
                    i18n: data.invitationcode.accountName,
                },
            ];
        }
        return data.schemas.map((schema) => ({
            value: schema,
            i18n: getI18nKey(`fields.schema.${schema}`),
        }));
    };

    const getSignatureLevels = () => {
        return [
            {
                value: 1,
                i18n: getI18nKey(`fields.signatureLevel.simple`),
            },
            {
                value: 2,
                i18n: getI18nKey(`fields.signatureLevel.dual`),
            },
        ];
    };

    const getAccounts = () => {
        if (readonly && data.invitationcode) {
            return [
                {
                    value: data.invitationcode.account,
                    label: data.invitationcode.accountName,
                },
            ];
        }
        return searchResponse?.data?.accountList.map((account) => ({
            value: account.id,
            label: account.name,
        }));
    };

    const accountChangeHandler = ({ setFieldValue }, accountId) => {
        const { segment, name, environment } = searchResponse?.data?.accountList.find((a) => a.id === accountId);
        setFieldValue("segment", segment);
        setFieldValue("accountName", name);
        setFieldValue("environment", environment);
    };

    const showSegment = (form) => {
        return form.values.segment !== undefined;
    };

    const hasEnv = (form) => {
        if (!form.values.account) {
            return false;
        }
        return !form.values.environment;
    };

    return (
        <FormikForm
            id={formId}
            formRef={formRef}
            initialValues={initialValues()}
            onSubmit={onSubmit}
            validationSchema={validationSchema(configuration)}>
            {(form) => (
                <FieldsContext.Provider value={{ readonly, i18nPrefix: getI18nKey("fields") }}>
                    {readonly && (
                        <>
                            <h3>
                                <I18n id={getI18nKey(`codeInformation`)} />
                            </h3>
                            <Row>
                                <InputField fieldName="code" readonly width="4" />
                                <LabelField fieldName="status" width="4" noValue>
                                    <View className={`states states--${calculateStatusColor(form.values)}`}>
                                        <I18n id={calculateStatusLabel(form.values)} />
                                    </View>
                                </LabelField>
                            </Row>
                            <Row>
                                <InputField fieldName="creationDate" readonly width="4" />
                                <InputField fieldName="expirationDate" readonly width="4" />
                            </Row>
                        </>
                    )}
                    <SearchUser
                        data={data}
                        form={form}
                        hasUser={hasUser}
                        setHasUser={setHasUser}
                        isLoading={loadingUser}
                        searchUser={fetchUser}
                        readonly={readonly}
                    />
                    {hasUser && (
                        <>
                            <Fields>
                                <SelectField
                                    fieldName="language"
                                    options={getLanguages()}
                                    width="4"
                                    className={classNames({ mtop30: !readonly })}
                                />
                                <SelectField fieldName="accessType" options={getAccessTypes()} width="4" />
                                <SelectField
                                    fieldName="account"
                                    options={getAccounts()}
                                    onChange={accountChangeHandler}
                                    width="4"
                                    optional
                                />
                            </Fields>

                            {!readonly && (
                                <Fields>
                                    {showSegment(form) && <InputField fieldName="segment" readonly width="4" />}

                                    {hasEnv(form) && (
                                        <SelectField fieldName="schema" options={getSchemas()} width="4" optional />
                                    )}
                                    {hasEnv(form) && (
                                        <SelectField
                                            fieldName="signatureLevel"
                                            options={getSignatureLevels()}
                                            width="4"
                                            optional
                                        />
                                    )}
                                    <TextAreaField rows="4" fieldName="comments" maxlength={200} optional width="4" />
                                </Fields>
                            )}
                        </>
                    )}
                </FieldsContext.Provider>
            )}
        </FormikForm>
    );
}

Form.propTypes = {
    data: shape({
        documentCountries: arrayOf(shape({})),
    }).isRequired,
    mode: string.isRequired,
    formId: string.isRequired,
    formRef: shape({
        current: undefined,
    }).isRequired,
    onSubmit: func.isRequired,
    hasUser: bool.isRequired,
    setHasUser: func.isRequired,
};

export default Form;
