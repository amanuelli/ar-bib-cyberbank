import React from "react";
import { Button, I18n } from "technisys-cybo-components";
import { usePageVariables, useGoBackLinks } from "technisys-cybo-pages";
import { getFormTitle, getFullName, managePermission } from "../utils";
import useActions from "../useActions";

const useHeaderConfig = ({ mode, data, hasUser, handleCancel, requestData, url, setState }) => {
    const { readonly, canManage } = usePageVariables(managePermission, mode);

    const { actionsOptions, actionsHandler } = useActions({ data, requestData, url, setState });

    const getGoBackLinks = useGoBackLinks([
        {
            i18nKey: "cybo.general.menu.items.invitationCodes",
            action: (history) => history.push("/invitationcodes"),
        },
    ]);

    const getCodeParams = () => {
        if (!data) {
            return undefined;
        }
        return { name: getFullName(data) };
    };

    return {
        goBackLinks: getGoBackLinks(),
        title: <I18n id={getFormTitle(mode)} params={getCodeParams()} />,
        actions: hasUser && !readonly && (
            <>
                <Button secondary onClick={handleCancel}>
                    <I18n id="cybo.general.actions.cancel" />
                </Button>
                {canManage && (
                    <Button form="invitation-codes-form" primary type="submit">
                        <I18n id="cybo.general.actions.save" />
                    </Button>
                )}
            </>
        ),
        renderExtraActions: canManage && readonly && data.status === "NOT_USED",
        actionsOptions,
        actionsHandler,
    };
};

export default useHeaderConfig;
