import React from "react";
import { shape, string } from "prop-types";
import { BasicCRUDRouter } from "technisys-cybo-pages";
import Page from "./List/Page";

function InvitationCodes({ match }) {
    const InvitationCodeDetail = React.lazy(() => import("./Detail"));

    return (
        <BasicCRUDRouter
            match={match}
            RootPage={Page}
            createPage="create-invitation-code"
            ComponentDetail={InvitationCodeDetail}
        />
    );
}

InvitationCodes.propTypes = {
    match: shape({
        path: string.isRequired,
    }).isRequired,
};

export { InvitationCodes };
