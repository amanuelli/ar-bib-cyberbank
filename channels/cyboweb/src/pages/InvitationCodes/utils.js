import { hasPermission } from "technisys-cybo-pages";
import { generateNumeredDialogProps, Modal } from "technisys-cybo-components";

const i18nPrefix = "cybo.invitationCodesManagement";
export const managePermission = "backoffice.invitationCodes.manage";

export const getI18nKey = (resource) => (resource ? `${i18nPrefix}.${resource}` : i18nPrefix);

export const calculateStatusColor = ({ status, expired }) => {
    switch (status) {
        case "USED":
            return "success";
        case "NOT_USED":
            return expired ? "error" : "caution";
        default:
            return "disabled";
    }
};

export const calculateStatusLabel = ({ status, expired }) => {
    if (expired && status === "NOT_USED") {
        return "backoffice.invitationCodes.status.EXPIRED";
    }
    return `backoffice.invitationCodes.status.${status}`;
};

export const canManage = (session) => hasPermission(managePermission, session);

export const getFormTitle = (mode) => getI18nKey(`${mode}.title`);

export const getFullName = (customer) => `${customer.firstName} ${customer.lastName}`;

export const generateConfirmationDialogHandler = (data, selectedItems, handleAction, handleDialogDismiss, setState) => (
    action,
) => {
    const dialog = generateNumeredDialogProps(getI18nKey(`modals.${action}`), data, selectedItems);
    dialog.isOpen = true;
    dialog.type = Modal.types.warning;
    dialog.action = action;
    dialog.onDismiss = handleDialogDismiss;
    dialog.onPrimaryButtonClick = (comments) => handleAction(action, comments);
    setState({ dialog });
};

export const getNotificationValue = ({ sentData }) => {
    const info = sentData.data || sentData;
    return { ...info, name: getFullName(info) };
};
