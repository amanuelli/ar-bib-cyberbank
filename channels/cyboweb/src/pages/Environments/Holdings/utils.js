const i18nPrefix = "cybo.environmentsManagement.holdings";

export const getI18nKey = (resource) => (!resource ? i18nPrefix : `${i18nPrefix}.${resource}`);
