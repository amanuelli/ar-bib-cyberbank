import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { shape, func, oneOf } from "prop-types";
import {
    Card,
    FormikForm,
    Fields,
    InputField,
    I18nContext,
    FieldsContext,
    ErrorNotification,
    TextAreaField,
    View,
    I18n,
    getMessage,
    SessionContext,
    userPageModes as PageModes,
    SelectField,
} from "technisys-cybo-components";
import { SearchUserField, get } from "technisys-cybo-pages";
import classNames from "classnames";
import HoldingsMembers from "components/Environment/HoldingsMembers";
import { validationSchema } from "./validationSchema";

const getI18nKey = (resource) =>
    !resource ? "cybo.environmentsManagement.holdings" : `cybo.environmentsManagement.holdings.${resource}`;

function Form(props) {
    const { formRef, submitHandler, mode } = props;
    const history = useHistory();
    const { messages, supportedLanguages } = useContext(I18nContext);
    const { configuration, addNotification } = useContext(SessionContext);
    const readonly = mode === PageModes.VIEW;

    const languages = (supportedLanguages || []).map((lang) => ({
        value: lang,
        label: getMessage(messages, `cybo.general.language.${lang}`),
    }));

    const getFieldI18nKey = (key) => getI18nKey(`fields.${key}`);

    const onLoadError = () => {
        addNotification(ErrorNotification(messages, `cybo.environmentsManagement.actions.`));
        history.push(`/environments`);
    };

    const renderResult = (
        { user, documentCountry, documentType, documentNumber },
        adminSendAdminInvite,
        setFieldValue,
    ) => {
        const { firstName, lastName, email, mobileNumber, backofficeUser, lang } = user;
        return (
            <>
                {" "}
                <View className="user-detail">
                    <p>
                        {firstName} {lastName}
                    </p>
                    <p>
                        {" "}
                        {documentCountry} <I18n id={`documentType.label.${documentType}`} /> {documentNumber}{" "}
                    </p>
                    <p>{email}</p>
                    <p>{mobileNumber}</p>
                    {backofficeUser ? (
                        <>
                            <View
                                className={classNames("icon-checkbox", { check: adminSendAdminInvite })}
                                onClick={() => setFieldValue("adminSendAdminInvite", !adminSendAdminInvite)}
                            />
                            <View inline>
                                <I18n id={getFieldI18nKey("sendEmail")} />
                            </View>
                        </>
                    ) : (
                        <>
                            <SelectField
                                className="p-0 mt-3"
                                fieldName="lang"
                                labelClassName="label"
                                options={languages}
                                readonly={readonly}
                                value={lang}
                                onChange={(f, v) => setFieldValue("lang", v)}
                            />
                            <View inline className="label send-email">
                                <I18n id={getFieldI18nKey("notBackoffice")} />
                            </View>
                        </>
                    )}
                </View>
            </>
        );
    };

    const initialValues = () => ({
        user: {
            documentCountry: get(configuration, "backoffice.customers.country.code.default"),
            documentType: get(configuration, "backoffice.customers.document.type.default"),
            documentNumber: "",
        },
        adminSendAdminInvite: true,
        holdingMembers: [],
    });

    return (
        <Card>
            <FormikForm
                formRef={formRef}
                id="holdings-form"
                onSubmit={submitHandler}
                initialValues={initialValues()}
                validationSchema={validationSchema}>
                {({ values, setFieldValue }) => {
                    return (
                        <FieldsContext.Provider value={{ readonly, i18nPrefix: getI18nKey("fields") }}>
                            <Fields>
                                <InputField fieldName="holdingName" maxlength={50} />
                                <HoldingsMembers fieldName="holdingMembers" getI18nKey={getFieldI18nKey} />
                                <SearchUserField
                                    fieldName="user"
                                    getI18nKey={getFieldI18nKey}
                                    onLoadError={onLoadError}
                                    fetchURL="/v1/environments/holdings/users/find"
                                    renderResult={(u) => renderResult(u, values.adminSendAdminInvite, setFieldValue)}
                                    labelClassName="search-user"
                                    isHeaderRemove
                                />
                                {!readonly && <TextAreaField rows="4" fieldName="comments" maxlength={200} optional />}
                            </Fields>
                        </FieldsContext.Provider>
                    );
                }}
            </FormikForm>
        </Card>
    );
}

Form.propTypes = {
    formRef: shape({
        current: shape({}),
    }).isRequired,
    submitHandler: func.isRequired,
    mode: oneOf(Object.values(PageModes)).isRequired,
};

export default Form;
