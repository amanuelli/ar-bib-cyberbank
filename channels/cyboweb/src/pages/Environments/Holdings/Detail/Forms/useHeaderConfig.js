import React from "react";
import { Button, I18n } from "technisys-cybo-components";
import { useGoBackLinks, usePageVariables } from "technisys-cybo-pages";
import { getI18nKey } from "../../utils";

const useHeaderConfig = ({ mode, handleCancel, loadingSubmit }) => {
    const { readonly } = usePageVariables(null, mode);

    const getGoBackLinks = useGoBackLinks([
        {
            i18nKey: "cybo.environmentsManagement.title",
            action: (history) => history.push("/environments"),
        },
    ]);

    return {
        goBackLinks: getGoBackLinks(),
        titleI18nKey: getI18nKey("title"),
        actions: !readonly && (
            <>
                <Button onClick={handleCancel} secondary>
                    <I18n id={getI18nKey("actions.cancel")} />
                </Button>
                <Button form="holdings-form" primary type="submit" isLoading={loadingSubmit}>
                    <I18n id={getI18nKey("actions.add")} />
                </Button>
            </>
        ),
    };
};

export default useHeaderConfig;
