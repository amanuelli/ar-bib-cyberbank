import React, { useRef } from "react";
import { oneOf } from "prop-types";
import { useHistory } from "react-router-dom";
import { View, ErrorNotification, SuccessNotification, userPageModes as PageModes } from "technisys-cybo-components";
import { PageContainer, useApiRequest, useNotification } from "technisys-cybo-pages";
import { usePost } from "technisys-cybo-hooks";
import Form from "./Form";
import { getI18nKey } from "../../utils";
import useHeaderConfigurer from "./useHeaderConfig";

function Page({ mode }) {
    const formRef = useRef();
    const history = useHistory();
    const { evaluateApprovalRequired, createNotification } = useNotification({ i18nPrefix: getI18nKey() });

    const goBackToList = () => {
        history.push("/environments");
    };

    const handleCancel = () => {
        if (formRef && formRef.current) {
            formRef.current.resetForm();
        }
        goBackToList();
    };

    const onError = (response, { sentData, setErrors }) => {
        if (response && response.data) {
            const { data: d = {} } = response.data;
            setErrors(d);
        }
        createNotification(ErrorNotification, "actions.general.create", { name: sentData.holdingName });
    };

    function onSuccess(response, { sentData }) {
        if (!evaluateApprovalRequired(response)) {
            createNotification(SuccessNotification, "actions.general.create", { name: sentData.holdingName });
        }
        goBackToList();
    }

    const [{ isLoading: loadingSubmit }, submit] = useApiRequest(usePost, {
        url: `/v1/environments/holdings`,
        onSuccess,
        onError,
    });

    const normalizeEnvironments = (env) => env.reduce((acc, e) => (acc !== "" ? `${acc}, ${e.id}` : `${e.id}`), "");

    const submitHandler = ({ holdingMembers, lang, ...values }, { setErrors }) => {
        const payload = {
            adminAdminLanguage: lang,
            holdingMembers: normalizeEnvironments(holdingMembers),
            ...values,
        };
        submit(payload, { setErrors });
    };

    const headerConfiguration = useHeaderConfigurer({ mode, handleCancel, loadingSubmit });

    return (
        <PageContainer headerProps={headerConfiguration}>
            <View className="holdings-page">
                <Form formRef={formRef} submitHandler={submitHandler} mode={mode} />
            </View>
        </PageContainer>
    );
}

Page.propTypes = {
    mode: oneOf(Object.values(PageModes)).isRequired,
};

export default Page;
