import React from "react";
import { act } from "react-dom/test-utils";
import { mount, shallow } from "enzyme";
import { SessionContext, I18nContext, FormikForm } from "technisys-cybo-components";
import HoldingsMembers from "components/Environment/HoldingsMembers";
import Page from "./Page";

const mockHoldingMembers = { id: "2", name: "SG", segment: "SG" };

const mockSearchUser = {
    documentCountry: "UY",
    documentType: "PASS",
    documentNumber: "2354",
    user: {
        firstName: "John",
        lastName: "Doe",
        email: "john@john.com",
        mobileNumber: "454545",
        backofficeUser: true,
        lang: "es",
    },
};

const mockedOnSuccess = jest.fn();
const mockedOnError = jest.fn();

const URL = {
    base: "/v1/environments/holdings",
    members: "/v1/environments/holdings/client/read",
    users: "/v1/environments/holdings/users/find",
};

jest.mock("technisys-cybo-pages", () => ({
    ...jest.requireActual("technisys-cybo-pages"),
    useApiRequest: (hook, { url, onSuccess, onError, ...props }) =>
        hook({
            onSuccess: mockedOnSuccess(onSuccess, url),
            onError: mockedOnError(onError, url),
            ...props,
        }),
}));

const mockHistoryPush = jest.fn();

jest.mock("react-router-dom", () => ({
    ...jest.requireActual("react-router-dom"),
    useHistory: () => ({
        push: mockHistoryPush,
    }),
    useLocation: () => ({
        pathname: "/environments/create-holding",
    }),
}));

const session = {
    data: {
        first_name: "name",
        last_name: "last",
        email: "mail@dom.com",
        picture: undefined,
    },
};

const addNotification = jest.fn();

const configuration = {
    initialized: true,
    data: {
        "backoffice.customers.country.code.default": "UY",
        "backoffice.customers.document.type.default": "CI",
    },
};

const templatePage = (
    <SessionContext.Provider value={{ session, addNotification, configuration }}>
        <I18nContext.Provider value={{ language: "es", supportedLanguages: ["en", "es", "pt"] }}>
            <Page mode="create" />
        </I18nContext.Provider>
    </SessionContext.Provider>
);

const templateHoldingMembers = (
    <SessionContext.Provider value={{ session, addNotification, configuration }}>
        <I18nContext.Provider value={{ language: "es", supportedLanguages: ["en", "es", "pt"] }}>
            <FormikForm
                id="id"
                validationSchema={{}}
                initialValues={{ holdingMembers: [mockHoldingMembers], environment: "2" }}
                onSubmit={() => {}}
                formRef={{ current: undefined }}>
                <HoldingsMembers fieldName="holdingMembers" getI18nKey={(x) => x} />
            </FormikForm>
        </I18nContext.Provider>
    </SessionContext.Provider>
);

describe("<Page />", () => {
    it("Renders without crashing", () => {
        shallow(templatePage);
    });

    it("It cancels actions", () => {
        const page = mount(templatePage);

        const buttonCancel = page
            .find(".buttons-container")
            .find("Button")
            .first();

        act(() => buttonCancel.simulate("click"));

        expect(mockHistoryPush).toBeCalled();
    });

    it("Select user admin", () => {
        mockedOnSuccess.mockImplementation(
            (onSuccess, url) => url === URL.users && onSuccess({ data: mockSearchUser }),
        );
        const page = mount(templatePage);

        const user = page.find("SearchUser");

        user.props().form.setFieldValue("documentNumber", "2354");

        user.find("Button").simulate("click");
    });

    it("Select holding members ", async () => {
        mockedOnSuccess.mockImplementationOnce(
            (onSuccess, url) => url === URL.members && onSuccess({ data: mockHoldingMembers }),
        );

        const page = mount(templateHoldingMembers);

        const holdingsMembers = page.find("HoldingsMembers");

        act(() => {
            holdingsMembers.props().form.values.environment = 2;
            page.find("Button")
                .first()
                .simulate("click");
            expect(mockedOnSuccess).toBeCalled();
        });
    });

    it("Select holding members error", async () => {
        mockedOnError.mockImplementationOnce(
            (onError, url) => url === URL.members && onError({ data: mockHoldingMembers }),
        );

        const page = mount(templateHoldingMembers);

        const holdingsMembers = page.find("HoldingsMembers");

        act(() => {
            holdingsMembers.props().form.values.environment = 2;
            page.find("Button")
                .first()
                .simulate("click");
            expect(mockedOnError).toBeCalled();
        });
    });

    it("Delect selectd holding members ", async () => {
        jest.clearAllMocks();

        const page = mount(templateHoldingMembers);

        const holdingsMembers = page.find("HoldingsMembers");

        act(() => {
            page.find("Button")
                .first()
                .simulate("click");
            expect(page.find("table")).toBeTruthy();

            holdingsMembers.props().form.values.environment = 3;
            page.find("Button")
                .first()
                .simulate("click");

            page.find("Button")
                .last()
                .simulate("click");
            expect(page.contains("table")).toBeFalsy();
        });
    });

    it("Submit form", async () => {
        mockedOnSuccess.mockImplementation(
            (onSuccess, url) =>
                url === URL.base && onSuccess({ code: "" }, { sentData: { holdingName: "holdingName" } }),
        );
        const page = mount(templatePage);

        const holdingsMembers = page.find("HoldingsMembers");
        const user = page.find("SearchUser");

        act(() => {
            holdingsMembers.props().form.setFieldValue("holdingMembers", [mockHoldingMembers]);
            user.props().form.setFieldValue("holdingName", "holdingName");
            user.props().form.setFieldValue("user", mockSearchUser);
            page.find("form").simulate("submit");
        });

        expect(mockedOnSuccess).toBeCalled();
    });

    it("Submit form error", async () => {
        mockedOnError.mockImplementation(
            (onError, url) =>
                url === URL.base &&
                onError(
                    { data: { data: { holdingName: "holdingName" }, code: "COR020W" } },
                    { sentData: { holdingName: "holdingName" }, setErrors: jest.fn() },
                ),
        );
        const page = mount(templatePage);

        const holdingsMembers = page.find("HoldingsMembers");
        const user = page.find("SearchUser");

        act(() => {
            holdingsMembers.props().form.setFieldValue("holdingMembers", [mockHoldingMembers]);
            user.props().form.setFieldValue("holdingName", "holdingName");
            user.props().form.setFieldValue("user", mockSearchUser);
        });
        page.find("form").simulate("submit");
        expect(mockedOnError).toBeCalled();
    });
});
