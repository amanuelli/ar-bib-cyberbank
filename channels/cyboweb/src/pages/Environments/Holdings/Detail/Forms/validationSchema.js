import * as Yup from "yup";
import { textAndPunctuationRegex } from "technisys-cybo-components";

export const validationSchema = Yup.object().shape({
    holdingName: Yup.string()
        .matches(textAndPunctuationRegex, "cybo.environmentsManagement.general.validations.name.format")
        .required("cybo.general.validations.empty"),
    user: Yup.object().shape({
        documentCountry: Yup.string().required("cybo.general.validations.empty"),
        documentType: Yup.string().required("cybo.general.validations.empty"),
        documentNumber: Yup.string().required("cybo.general.validations.empty"),
    }),
});
