import React, { useContext } from "react";
import { func } from "prop-types";
import { View, noop, administrationSchemas as Schema } from "technisys-cybo-components";
import {
    EnvironmentContext,
    GeneralHomeCard,
    UsersHomeCard,
    ProductsHomeCard,
    SignaturesHomeCard,
    Shortcuts,
} from "technisys-cybo-pages";

function Home({ onGoToTab }) {
    const {
        environment: { administrationSchema },
        isCorporateGroup,
    } = useContext(EnvironmentContext);

    return (
        <View className="group-card">
            <GeneralHomeCard onGoToTab={onGoToTab} />
            <UsersHomeCard onGoToTab={onGoToTab} />
            {administrationSchema !== Schema.SIMPLE && !isCorporateGroup && (
                <SignaturesHomeCard onGoToTab={onGoToTab} />
            )}
            <ProductsHomeCard onGoToTab={onGoToTab} />
            <Shortcuts />
        </View>
    );
}

Home.propTypes = {
    onGoToTab: func,
};

Home.defaultProps = {
    onGoToTab: noop,
};

export default Home;
