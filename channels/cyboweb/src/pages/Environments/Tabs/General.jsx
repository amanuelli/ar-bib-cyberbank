import React, { useContext, useEffect, useRef, useState } from "react";
import { useHistory } from "react-router-dom";
import classnames from "classnames";
import { func } from "prop-types";
import * as Yup from "yup";
import {
    Button,
    Card,
    Column,
    I18n,
    Label,
    Row,
    View,
    Fields,
    FieldsContext,
    FormikForm,
    InputField,
    SelectField,
    TextAreaField,
    SessionContext,
    ErrorNotification,
    SuccessNotification,
    textAndPunctuationRegex,
} from "technisys-cybo-components";
import { usePut } from "technisys-cybo-hooks";
import { useApiRequest, useNotification, safeParseJSON, getArray, EnvironmentContext } from "technisys-cybo-pages";
import HoldingsMembers from "components/Environment/HoldingsMembers";

const validationSchema = Yup.object().shape({
    name: Yup.string()
        .matches(textAndPunctuationRegex, "cybo.environmentsManagement.general.validations.name.format")
        .required("cybo.general.validations.empty"),
});

const General = ({ getI18nKey }) => {
    const {
        environment: { id, name, administrationSchema, data: additionalData, type, clients },
        canManage,
        isCorporateGroup,
    } = useContext(EnvironmentContext);
    const [isEditing, setIsEditing] = useState(false);
    const formRef = useRef();
    const history = useHistory();
    const { evaluateApprovalRequired, createNotification } = useNotification({ i18nPrefix: getI18nKey() });
    const { configuration } = useContext(SessionContext);

    const onError = (response, { sentData, setErrors }) => {
        if (response && response.data) {
            const { data = {} } = response.data;
            setErrors(data);
        }
        createNotification(ErrorNotification, "actions.general.edit", sentData);
    };

    function onSuccess(response, { sentData }) {
        if (!evaluateApprovalRequired(response)) {
            createNotification(SuccessNotification, "actions.general.edit", sentData);
            history.push(`/environments/${id}`, {
                tab: "general",
            });
            setIsEditing(false);
        }
    }

    const [{ isLoading }, submit] = useApiRequest(usePut, {
        url: `/v2/environments/${id}/general`,
        onSuccess,
        onError,
    });

    const initialValues = {
        id,
        name,
        type,
        schema: administrationSchema,
        comments: "",
        holdingMembers: clients,
    };

    const types = getArray(configuration, "backoffice.environments.types")
        .filter((envType) => envType !== "corporateGroup")
        .map((envType) => ({ value: envType, i18n: getI18nKey(`general.type.${envType}`) }));

    const schemes = getArray(configuration, "backoffice.environments.schemes").map((scheme, index, items) => ({
        value: scheme,
        i18n: getI18nKey(`general.schema.${scheme}`),
        disabled: index < items.indexOf(initialValues.schema),
    }));

    const clearForm = () => {
        if (formRef && formRef.current) {
            formRef.current.resetForm();
        }
    };

    useEffect(() => {
        return clearForm;
    }, []);

    const getValue = (value) => {
        const splittedValue = value.toString().split("key=");
        if (splittedValue.length > 1) {
            return <I18n id={splittedValue[1]} />;
        }
        return splittedValue[0];
    };

    const handleEditButtonClick = () => {
        setIsEditing(true);
    };

    const handleCancelClick = () => {
        clearForm();
        setIsEditing(false);
    };

    const normalizeEnvironments = (env) => env.reduce((acc, e) => (acc !== "" ? `${acc}, ${e.id}` : `${e.id}`), "");

    const submitHandler = ({ schema, holdingMembers, ...values }, { setErrors }) => {
        const toAdd = normalizeEnvironments(holdingMembers.filter((c) => !clients.some((item) => item.id === c.id)));
        const toRemove = normalizeEnvironments(clients.filter((c) => !holdingMembers.some((item) => item.id === c.id)));

        const payload = {
            ...values,
            workspacesToAdd: toAdd,
            workspacesToRemove: toRemove,
            administrationSchema: schema,
        };
        submit(payload, { setErrors });
    };

    const renderAdditionalData = () => {
        const jsonData = safeParseJSON(additionalData.data);
        if (jsonData !== null) {
            return (
                <Column width={isCorporateGroup ? 5 : 7}>
                    <View className="form-group">
                        <Label className="font-bold">
                            <I18n id={getI18nKey("general.additionalData")} />
                        </Label>
                    </View>
                    {Object.keys(jsonData).map((key) => (
                        <View key={key}>
                            <Label>
                                <I18n id={getI18nKey(`general.additionalData.${key}`)} />:
                            </Label>
                            {getValue(jsonData[key])}
                        </View>
                    ))}
                </Column>
            );
        }
        return null;
    };

    const renderBody = () => (
        <Row className={classnames({ content: !canManage })}>
            <Column width={4}>
                <View className="form-group">
                    <Label>
                        <I18n id={getI18nKey("general.id")} />
                    </Label>
                    {id}
                </View>
                <View className="form-group">
                    <Label>
                        <I18n id={getI18nKey("general.type")} />
                    </Label>
                    <I18n id={getI18nKey(`general.type.${type}`)} />
                </View>
                {!isCorporateGroup && (
                    <View className="form-group">
                        <Label>
                            <I18n id={getI18nKey("general.schema")} />
                        </Label>
                        <I18n id={getI18nKey(`general.schema.${administrationSchema}`)} />
                    </View>
                )}
            </Column>
            {isCorporateGroup && (
                <Column width={2}>
                    <View className="form-group">
                        <Label>
                            <I18n id={getI18nKey("general.environment")} />
                        </Label>
                        {clients && clients.map((client) => <View>{client.name}</View>)}
                    </View>
                </Column>
            )}
            {additionalData && additionalData.data && renderAdditionalData()}
        </Row>
    );

    const getHoldingsI18nKey = (key) => getI18nKey(`holdings.fields.${key}`);

    const renderBodyEdit = () => (
        <Row className={classnames({ content: !canManage })}>
            <Column>
                <FormikForm
                    formRef={formRef}
                    id="environment-general-form"
                    initialValues={initialValues}
                    validationSchema={validationSchema}
                    onSubmit={submitHandler}>
                    <FieldsContext.Provider value={{ i18nPrefix: getI18nKey("general"), readonly: false }}>
                        {!isCorporateGroup && (
                            <Fields>
                                <Column>
                                    <View className="form-group">
                                        <Label>
                                            <I18n id={getI18nKey("general.id")} />
                                        </Label>
                                        {id}
                                    </View>
                                </Column>
                                <View className="col-md-4 pl-0">
                                    <InputField fieldName="name" maxlength={50} />
                                </View>
                                <View className="col-md-4 pl-0">
                                    <SelectField fieldName="type" options={types} />
                                </View>
                                <View className="col-md-4 pl-0">
                                    <SelectField fieldName="schema" options={schemes} />
                                </View>
                                <View className="col-md-4 pl-0">
                                    <TextAreaField fieldName="comments" maxlength={200} optional />
                                </View>
                            </Fields>
                        )}
                        {isCorporateGroup && (
                            <Fields>
                                <View className="col-md-4 pl-0">
                                    <InputField fieldName="name" maxlength={50} />
                                </View>
                                <HoldingsMembers fieldName="holdingMembers" getI18nKey={getHoldingsI18nKey} />
                                <View className="col-md-4 pl-0">
                                    <TextAreaField fieldName="comments" maxlength={200} optional />
                                </View>
                            </Fields>
                        )}
                    </FieldsContext.Provider>
                </FormikForm>
            </Column>
        </Row>
    );

    const renderEditButton = () => {
        if (isEditing) {
            return (
                <Row>
                    <Column className="add-button-container">
                        <Button onClick={handleCancelClick} btnText>
                            <I18n id={getI18nKey("actions.cancel")} />
                        </Button>
                        <Button form="environment-general-form" primary type="submit" isLoading={isLoading}>
                            <I18n id={getI18nKey("actions.general.edit.confirm")} />
                        </Button>
                    </Column>
                </Row>
            );
        }
        return (
            <Row>
                <Column className="add-button-container">
                    <Button key="edit" onClick={handleEditButtonClick} primary>
                        <I18n id={getI18nKey("actions.general.edit")} />
                    </Button>
                </Column>
            </Row>
        );
    };

    return (
        id && (
            <Card>
                {canManage && renderEditButton()}
                {!isEditing && renderBody()}
                {isEditing && renderBodyEdit()}
            </Card>
        )
    );
};

General.propTypes = {
    getI18nKey: func.isRequired,
};

export default General;
