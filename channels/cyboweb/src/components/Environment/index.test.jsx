import React from "react";
import { mount } from "enzyme";
import { MemoryRouter } from "react-router-dom";
import { StarterKitContext, administrationSchemas as Schema } from "technisys-cybo-components";
import { EnvironmentContext } from "technisys-cybo-pages";
import { environment } from "./index";

const { navItems, tabPage } = environment;

const wrapper = (Component) => (
    <MemoryRouter>
        <StarterKitContext.Provider value={{ customRoute: [] }}>
            <EnvironmentContext.Provider value={{ isCorporateGroup: false, environment: { id: 667 } }}>
                <Component getI18nKey={(x) => x} />
            </EnvironmentContext.Provider>
        </StarterKitContext.Provider>
    </MemoryRouter>
);

describe("navItems", () => {
    it("Validate items for simple", () => {
        const result = navItems({ administrationSchema: Schema.SIMPLE }, false);
        expect(result).toHaveLength(5);
    });
    it("Validate items for medium", () => {
        const noCorporate = navItems({ administrationSchema: Schema.MEDIUM }, false);
        expect(noCorporate).toHaveLength(6);

        const corporate = navItems({ administrationSchema: Schema.MEDIUM }, true);
        expect(corporate).toHaveLength(5);
    });
    it("Validate items for advanced", () => {
        const noCorporate = navItems({ administrationSchema: Schema.ADVANCED }, false);
        expect(noCorporate).toHaveLength(7);

        const corporate = navItems({ administrationSchema: Schema.ADVANCED }, true);
        expect(corporate).toHaveLength(6);
    });
    it("Validate items when caps permissions is enabled", () => {
        const result = navItems({ administrationSchema: Schema.SIMPLE }, false, true);
        expect(result).toHaveLength(6);
    });
});

describe("tabPage", () => {
    it("Validate items", () => {
        expect(Object.keys(tabPage)).toHaveLength(2);
    });

    it("Validate home", () => {
        const page = mount(wrapper(tabPage.home));
        expect(page.find("Home")).toHaveLength(1);
    });

    it("Validate general", () => {
        const page = mount(wrapper(tabPage.general));
        expect(page.find("General")).toHaveLength(1);
    });
});
