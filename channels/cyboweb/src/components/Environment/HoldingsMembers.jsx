/* eslint-disable react/prop-types */
import React from "react";
import { shape, func, string } from "prop-types";
import { InputField, View, Button, I18n, CustomField, Table } from "technisys-cybo-components";
import { useApiRequest } from "technisys-cybo-pages";
import { useGet } from "technisys-cybo-hooks";

function HoldingsMembers({ form, field, getI18nKey }) {
    const { setFieldValue, setFieldError } = form;

    const [{ isLoading: fetchingEnvironments }, apiCallSearch] = useApiRequest(useGet, {
        url: "/v1/environments/holdings/client/read",
        onSuccess: ({ data }) => {
            setFieldValue("environment", "");
            setFieldValue(field.name, [...field.value, data]);
        },
        onError: (response) => {
            const { data = {} } = response.data;
            setFieldError("environment", data.environment);
        },
        redirectOnError: false,
    });

    const handleAddEnvironment = () => {
        const env = form.values.environment;
        const isAlready = field.value.some((e) => e.id === env);
        if (isAlready) {
            setFieldError("environment", getI18nKey("alreadySelected"));
        } else {
            apiCallSearch({ params: { idClient: env } });
        }
    };

    const handleRemoveEnvironment = (id) => {
        const members = field.value.filter((m) => m.id !== id);
        setFieldValue(field.name, members);
    };

    const showPills = () => {
        return (
            field.value.length > 0 && (
                <View className="col-6 mt-4">
                    <Table
                        columns={[
                            {
                                id: "id",
                                Header: <I18n id={getI18nKey("id")} />,
                                accessor: "id",
                            },
                            {
                                id: "name",
                                Header: <I18n id={getI18nKey("name")} />,
                                accessor: "name",
                            },
                            {
                                id: "remove",
                                Cell: ({
                                    row: {
                                        original: { id },
                                    },
                                }) => (
                                    <Button
                                        className="m-0 mb-1 align-self-end"
                                        btnText
                                        onClick={() => handleRemoveEnvironment(id)}>
                                        <I18n id={getI18nKey("removeEnvironment")} />
                                    </Button>
                                ),
                            },
                        ]}
                        data={field.value}
                    />
                </View>
            )
        );
    };

    return (
        <>
            <View className="d-flex col-12 p-0">
                <View className="col-md-4 pl-0">
                    <InputField fieldName="environment" />
                </View>
                <Button
                    className="m-0 mb-1 align-self-end"
                    btnText
                    onClick={handleAddEnvironment}
                    disabled={!form.values.environment}
                    isLoading={fetchingEnvironments}>
                    <I18n id={getI18nKey("addEnvironment")} />
                </Button>
            </View>
            {showPills()}
        </>
    );
}

HoldingsMembers.propTypes = {
    form: shape({
        setErrors: func.isRequired,
        setFieldValue: func.isRequired,
    }).isRequired,
    field: shape({
        name: string.isRequired,
        value: shape([]),
    }).isRequired,
    getI18nKey: func.isRequired,
};

export default (props) => <CustomField {...props} component={HoldingsMembers} />;
