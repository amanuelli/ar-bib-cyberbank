import React from "react";
import { administrationSchemas as Schema } from "technisys-cybo-components";
import { Home, General } from "pages/Environments/Tabs";

const navItems = ({ administrationSchema }, isCorporateGroup, hasCapsListPermission) => {
    const items = [];

    items.push({ name: "home" }, { name: "general" }, { name: "users" }, { name: "products" });

    // eslint-disable-next-line default-case
    switch (administrationSchema) {
        case Schema.ADVANCED:
            items.push({ name: "groups" });
            if (!isCorporateGroup) {
                items.push({ name: "signatures" });
            }
            break;
        case Schema.MEDIUM:
            if (!isCorporateGroup) {
                items.push({ name: "signatures" });
            }
            break;
    }

    items.push({ name: "restrictions" });
    if (hasCapsListPermission) {
        items.push({ name: "caps" });
    }

    return items;
};

const tabPage = {
    home: (props) => <Home {...props} />,
    general: (props) => <General {...props} />,
};

export const environment = { navItems, tabPage };
