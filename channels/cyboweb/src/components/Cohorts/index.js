import React from "react";
import AssetsRule from "../Rules/AssetsRule";
import LiabilitiesRule from "../Rules/LiabilitiesRule";

export const indicators = (getRuleProps, errors, getI18nKey) => {
    return [
        {
            i18n: getI18nKey("rules.actives"),
            content: <AssetsRule {...getRuleProps("assets")} />,
            hasError: errors && errors.assets !== undefined,
            ordinal: 230,
        },
        {
            i18n: getI18nKey("rules.liabilities"),
            content: <LiabilitiesRule {...getRuleProps("liabilities")} />,
            hasError: errors && errors.liabilities !== undefined,
            ordinal: 260,
        },
    ];
};
