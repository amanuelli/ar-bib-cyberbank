/* this object should contain a pair value object with the mapping of the pending action id activiy and its respective
dynamic import of the component to be rendered
Example:
export default {
    "cybo.campaigns.toggleStatus": import("./DefaultComponent"),
    "cybo.groups.create": import("./DefaultComponent"),
}
 */

export default {};
