import React from "react";
import AssetsRule from "../Rules/AssetsRule";
import ClientsRule from "../Rules/ClientsRule";
import LiabilitiesRule from "../Rules/LiabilitiesRule";

export const indicators = (getRuleProps, errors, getI18nKey) => {
    return [
        {
            i18n: getI18nKey("rules.clients"),
            content: <ClientsRule {...getRuleProps("documentInFile")} getI18nKey={getI18nKey} />,
            hasError: errors && errors.documentInFile !== undefined,
            ordinal: 150,
        },
        {
            i18n: getI18nKey("rules.actives"),
            content: <AssetsRule {...getRuleProps("assets")} />,
            hasError: errors && errors.assets !== undefined,
            ordinal: 330,
        },
        {
            i18n: getI18nKey("rules.liabilities"),
            content: <LiabilitiesRule {...getRuleProps("liabilities")} />,
            hasError: errors && errors.liabilities !== undefined,
            ordinal: 360,
        },
    ];
};
