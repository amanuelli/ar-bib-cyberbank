import React from "react";
import { func, shape, arrayOf, string } from "prop-types";
import { noop } from "technisys-cybo-components";
import { AmountRule } from "technisys-cybo-pages";

const emptyIndicator = { amount: "", operator: "" };
const initialIndicators = [{ idIndicator: -1, idxIndicator: 0, idType: "liabilities", data: { ...emptyIndicator } }];

export default function LiabilitiesRule({ ...props }) {
    return <AmountRule rule="liabilities" initialIndicators={initialIndicators} {...props} />;
}

LiabilitiesRule.propTypes = {
    mode: string.isRequired,
    indicatorData: shape({
        amount: string,
        operator: string,
    }),
    onChange: func,
    onReset: func,
    indicatorTypeInfo: shape({
        operators: arrayOf(string),
    }),
};

LiabilitiesRule.defaultProps = {
    indicatorData: { amount: "", operator: "" },
    onChange: noop,
    onReset: noop,
    indicatorTypeInfo: { operators: [] },
};
