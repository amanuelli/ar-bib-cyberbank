import React, { useRef } from "react";
import { bool, func, shape, arrayOf, string } from "prop-types";
import {
    noop,
    userPageModes as PageModes,
    Column,
    Input,
    Label,
    I18n,
    Row,
    Select,
    View,
} from "technisys-cybo-components";
import { useDownload, useImport } from "technisys-cybo-hooks";
import { Card } from "technisys-cybo-pages";

function FileSelector({ label, onChange, readonly }) {
    const defaultLabel = <I18n id="cybo.campaignsManagement.rules.clients.step.1.input" />;

    const fileInputRef = useRef();

    function onSuccessRuntimeHandler({ data }) {
        const newIndicatorData = {
            idContent: data.idContent,
            filename: data.fileName,
            documents: data.processedContent,
        };
        onChange(newIndicatorData);
    }

    const [, importClientList] = useImport({
        url: "/v1/campaigns/uploadClientFile",
    });

    return (
        <>
            <Input
                name="inputGroupFile01"
                type="file"
                className="custom-file-input"
                readonly={readonly}
                onChange={(event) => {
                    importClientList(event.target.files[0], {}, onSuccessRuntimeHandler);
                    fileInputRef.current.value = "";
                }}
                inputRef={fileInputRef}
            />
            <Label className="custom-file-label" htmlFor="inputGroupFile01">
                {label || defaultLabel}
            </Label>
        </>
    );
}

FileSelector.propTypes = {
    label: string,
    onChange: func,
    readonly: bool,
};

FileSelector.defaultProps = {
    label: undefined,
    onChange: noop,
    readonly: false,
};

const emptyIndicator = { operator: "" };
const initialIndicators = [{ idIndicator: -1, idxIndicator: 0, idType: "documentInFile", data: { ...emptyIndicator } }];

export default function ClientsRule({
    mode,
    indicators,
    onNewIndicator,
    onChange,
    onReset,
    indicatorTypeInfo,
    getI18nKey,
}) {
    const indicatorsValue = indicators.length === 0 ? JSON.parse(JSON.stringify(initialIndicators)) : indicators;

    const [, downloadFile] = useDownload();

    function changeOperatorHandler(event, idxIndicator) {
        const indicator = indicatorsValue.filter((i) => i.idxIndicator === idxIndicator)[0];
        indicator.data = { ...indicator.data, operator: event.target.value };
        onChange(indicator);
    }

    function changeFileHandler(fileData, idxIndicator) {
        const indicator = indicatorsValue.filter((i) => i.idxIndicator === idxIndicator)[0];
        indicator.data = { ...indicator.data, ...fileData };
        onChange(indicator);
    }

    function generateOperatorOptions() {
        const { operators } = indicatorTypeInfo;
        return operators.map((op) => ({ value: op, i18n: getI18nKey(`rules.clients.step.2.select.${op}`) }));
    }

    const handleNewIndicator = () => {
        onNewIndicator({ ...emptyIndicator });
    };

    return (
        <Card
            mode={mode}
            title="cybo.campaignsManagement.rules.clients.title"
            onNewIndicator={handleNewIndicator}
            onReset={onReset}>
            {indicatorsValue.map((indicator) => (
                // eslint-disable-next-line react/no-array-index-key
                <Row key={`product-${indicator.idxIndicator}`}>
                    <Column width="6">
                        <p className="label">
                            <I18n id="cybo.campaignsManagement.rules.clients.step.1.explain" />
                        </p>
                        <View className="input-group input-file-box">
                            <View className="input-group-prepend" />
                            <View className="custom-file mtop-negative8">
                                <FileSelector
                                    label={indicator.data.filename}
                                    onChange={(fileData) => changeFileHandler(fileData, indicator.idxIndicator)}
                                    readonly={mode === PageModes.VIEW}
                                />
                            </View>
                        </View>
                        {indicator.data.idContent && indicator.data.idContent !== 0 && (
                            <View
                                onClick={() => {
                                    downloadFile(`/v1/campaigns/downloadClientsFile/${indicator.data.idContent}`);
                                }}>
                                <I18n id="cybo.campaignsManagement.rules.clients.download" />
                            </View>
                        )}
                    </Column>
                    <View className="form-group col-md-6 col-12 user-photo-description float-right">
                        <Select
                            name="clients-condition"
                            i18n={getI18nKey("rules.clients.step.2.explain")}
                            defaultOptionLabelI18n={getI18nKey("rules.clients.step.2.select.placeholder")}
                            options={generateOperatorOptions()}
                            onChange={(event) => changeOperatorHandler(event, indicator.idxIndicator)}
                            value={indicator.data.operator}
                            readonly={mode === PageModes.VIEW}
                        />
                    </View>
                </Row>
            ))}
        </Card>
    );
}

ClientsRule.propTypes = {
    mode: string.isRequired,
    indicators: arrayOf(
        shape({
            productTypes: arrayOf(string),
            operator: string,
        }),
    ),
    onChange: func,
    onReset: func,
    onNewIndicator: func,
    indicatorTypeInfo: shape({
        operators: arrayOf(string),
    }),
    getI18nKey: func,
};

ClientsRule.defaultProps = {
    indicators: initialIndicators,
    onChange: noop,
    onReset: noop,
    onNewIndicator: noop,
    indicatorTypeInfo: { operators: [] },
    getI18nKey: noop,
};
