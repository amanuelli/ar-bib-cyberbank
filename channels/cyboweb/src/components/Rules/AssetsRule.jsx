import React from "react";
import { func, shape, arrayOf, string } from "prop-types";
import { noop } from "technisys-cybo-components";
import { AmountRule } from "technisys-cybo-pages";

const emptyIndicator = { amount: "", operator: "" };
const initialIndicators = [{ idIndicator: -1, idxIndicator: 0, idType: "assets", data: { ...emptyIndicator } }];

export default function AssetsRule({ ...props }) {
    return <AmountRule rule="actives" initialIndicators={initialIndicators} {...props} />;
}

AssetsRule.propTypes = {
    mode: string.isRequired,
    indicators: arrayOf(
        shape({
            productTypes: arrayOf(string),
            operator: string,
        }),
    ),
    onChange: func,
    onReset: func,
    onNewIndicator: func,
    indicatorTypeInfo: shape({
        operators: arrayOf(string),
    }),
};

AssetsRule.defaultProps = {
    indicators: initialIndicators,
    onChange: noop,
    onReset: noop,
    onNewIndicator: noop,
    indicatorTypeInfo: { operators: [] },
};
