import { enUS, es, pt } from "date-fns/locale";

export const locales = {
    en: enUS,
    es,
    pt,
};
