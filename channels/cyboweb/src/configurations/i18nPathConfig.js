export const i18nPaths = {
    "/invitationcodes": [
        "cybo.invitationCodesManagement",
        "documentType.label",
        "backoffice.invitationCodes.status",
        "country.name",
        "backoffice.invitation.role",
    ],
};
