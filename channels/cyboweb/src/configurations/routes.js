import { CreateHoldingPage } from "pages/Environments";
import { InvitationCodes } from "pages/InvitationCodes";

export const customRoute = {
    privateRoutes: [
        {
            path: "/environments/create-holding",
            exact: true,
            component: CreateHoldingPage,
        },
        {
            path: "/invitationcodes",
            exact: false,
            component: InvitationCodes,
        },
    ],
};
