import "regenerator-runtime/runtime";
import { configure } from "enzyme";
import Adapter from "@wojtekmaj/enzyme-adapter-react-17";
import "jest-enzyme";
import "jest-localstorage-mock";
import { fetch } from "whatwg-fetch";
import "mutationobserver-shim";

configure({ adapter: new Adapter() });

// Initializing Jest Test
const { getComputedStyle } = window;

// Fix for @reach/dialog warinig in console
beforeAll(() => {
    window.getComputedStyle = (element) =>
        element === document.body ? { getPropertyValue: () => "1" } : getComputedStyle(element);
});

afterAll(() => {
    window.getComputedStyle = getComputedStyle;
});

global.fetch = fetch;

global.window.crypto = {
    getRandomValues: jest.fn().mockName("getRandomValuesPolyfill"),
};
