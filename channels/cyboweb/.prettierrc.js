module.exports = {
    arrowParens: "always", // Enforce always using parens on arrow functions
    bracketSpacing: true, // Enforce spacing on bracketed statements
    jsxBracketSameLine: true, // Enforce closing JSX opening-tags on the same line
    printWidth: 120, // breaks line to maintain code's readability
    semi: true, // appends semicolons to every line
    singleQuote: false, // use double quotes for everything (jsx / js)
    tabWidth: 4, // use 4 spaces for indentation
    trailingComma: "all", // append trailing comma according to ES5 rules
    useTabs: false, // use spaces for indentation
};
